package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.l;

public final class cx implements ServiceConnection, BaseGmsClient.a, BaseGmsClient.b {
    /* access modifiers changed from: package-private */

    /* renamed from: a  reason: collision with root package name */
    public volatile boolean f2491a;

    /* renamed from: b  reason: collision with root package name */
    volatile n f2492b;
    final /* synthetic */ cl c;

    protected cx(cl clVar) {
        this.c = clVar;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:22|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r3.c.q().c.a("Service connect failed to get IMeasurementService");
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x005c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onServiceConnected(android.content.ComponentName r4, android.os.IBinder r5) {
        /*
            r3 = this;
            java.lang.String r4 = "MeasurementServiceConnection.onServiceConnected"
            com.google.android.gms.common.internal.l.b(r4)
            monitor-enter(r3)
            r4 = 0
            if (r5 != 0) goto L_0x001d
            r3.f2491a = r4     // Catch:{ all -> 0x001a }
            com.google.android.gms.measurement.internal.cl r4 = r3.c     // Catch:{ all -> 0x001a }
            com.google.android.gms.measurement.internal.o r4 = r4.q()     // Catch:{ all -> 0x001a }
            com.google.android.gms.measurement.internal.q r4 = r4.c     // Catch:{ all -> 0x001a }
            java.lang.String r5 = "Service connected with null binder"
            r4.a(r5)     // Catch:{ all -> 0x001a }
            monitor-exit(r3)     // Catch:{ all -> 0x001a }
            return
        L_0x001a:
            r4 = move-exception
            goto L_0x008e
        L_0x001d:
            r0 = 0
            java.lang.String r1 = r5.getInterfaceDescriptor()     // Catch:{ RemoteException -> 0x005c }
            java.lang.String r2 = "com.google.android.gms.measurement.internal.IMeasurementService"
            boolean r2 = r2.equals(r1)     // Catch:{ RemoteException -> 0x005c }
            if (r2 == 0) goto L_0x004e
            if (r5 != 0) goto L_0x002d
            goto L_0x0040
        L_0x002d:
            java.lang.String r1 = "com.google.android.gms.measurement.internal.IMeasurementService"
            android.os.IInterface r1 = r5.queryLocalInterface(r1)     // Catch:{ RemoteException -> 0x005c }
            boolean r2 = r1 instanceof com.google.android.gms.measurement.internal.zzaj     // Catch:{ RemoteException -> 0x005c }
            if (r2 == 0) goto L_0x003a
            com.google.android.gms.measurement.internal.zzaj r1 = (com.google.android.gms.measurement.internal.zzaj) r1     // Catch:{ RemoteException -> 0x005c }
            goto L_0x003f
        L_0x003a:
            com.google.android.gms.measurement.internal.zzal r1 = new com.google.android.gms.measurement.internal.zzal     // Catch:{ RemoteException -> 0x005c }
            r1.<init>(r5)     // Catch:{ RemoteException -> 0x005c }
        L_0x003f:
            r0 = r1
        L_0x0040:
            com.google.android.gms.measurement.internal.cl r5 = r3.c     // Catch:{ RemoteException -> 0x005c }
            com.google.android.gms.measurement.internal.o r5 = r5.q()     // Catch:{ RemoteException -> 0x005c }
            com.google.android.gms.measurement.internal.q r5 = r5.k     // Catch:{ RemoteException -> 0x005c }
            java.lang.String r1 = "Bound to IMeasurementService interface"
            r5.a(r1)     // Catch:{ RemoteException -> 0x005c }
            goto L_0x0069
        L_0x004e:
            com.google.android.gms.measurement.internal.cl r5 = r3.c     // Catch:{ RemoteException -> 0x005c }
            com.google.android.gms.measurement.internal.o r5 = r5.q()     // Catch:{ RemoteException -> 0x005c }
            com.google.android.gms.measurement.internal.q r5 = r5.c     // Catch:{ RemoteException -> 0x005c }
            java.lang.String r2 = "Got binder with a wrong descriptor"
            r5.a(r2, r1)     // Catch:{ RemoteException -> 0x005c }
            goto L_0x0069
        L_0x005c:
            com.google.android.gms.measurement.internal.cl r5 = r3.c     // Catch:{ all -> 0x001a }
            com.google.android.gms.measurement.internal.o r5 = r5.q()     // Catch:{ all -> 0x001a }
            com.google.android.gms.measurement.internal.q r5 = r5.c     // Catch:{ all -> 0x001a }
            java.lang.String r1 = "Service connect failed to get IMeasurementService"
            r5.a(r1)     // Catch:{ all -> 0x001a }
        L_0x0069:
            if (r0 != 0) goto L_0x007e
            r3.f2491a = r4     // Catch:{ all -> 0x001a }
            com.google.android.gms.common.stats.a.a()     // Catch:{ IllegalArgumentException -> 0x008c }
            com.google.android.gms.measurement.internal.cl r4 = r3.c     // Catch:{ IllegalArgumentException -> 0x008c }
            android.content.Context r4 = r4.m()     // Catch:{ IllegalArgumentException -> 0x008c }
            com.google.android.gms.measurement.internal.cl r5 = r3.c     // Catch:{ IllegalArgumentException -> 0x008c }
            com.google.android.gms.measurement.internal.cx r5 = r5.f2469a     // Catch:{ IllegalArgumentException -> 0x008c }
            com.google.android.gms.common.stats.a.a(r4, r5)     // Catch:{ IllegalArgumentException -> 0x008c }
            goto L_0x008c
        L_0x007e:
            com.google.android.gms.measurement.internal.cl r4 = r3.c     // Catch:{ all -> 0x001a }
            com.google.android.gms.measurement.internal.am r4 = r4.p()     // Catch:{ all -> 0x001a }
            com.google.android.gms.measurement.internal.cy r5 = new com.google.android.gms.measurement.internal.cy     // Catch:{ all -> 0x001a }
            r5.<init>(r3, r0)     // Catch:{ all -> 0x001a }
            r4.a(r5)     // Catch:{ all -> 0x001a }
        L_0x008c:
            monitor-exit(r3)     // Catch:{ all -> 0x001a }
            return
        L_0x008e:
            monitor-exit(r3)     // Catch:{ all -> 0x001a }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.cx.onServiceConnected(android.content.ComponentName, android.os.IBinder):void");
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        l.b("MeasurementServiceConnection.onServiceDisconnected");
        this.c.q().j.a("Service disconnected");
        this.c.p().a(new cz(this, componentName));
    }

    public final void a(Bundle bundle) {
        l.b("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                this.c.p().a(new da(this, (zzaj) this.f2492b.p()));
            } catch (DeadObjectException | IllegalStateException unused) {
                this.f2492b = null;
                this.f2491a = false;
            }
        }
    }

    public final void a(int i) {
        l.b("MeasurementServiceConnection.onConnectionSuspended");
        this.c.q().j.a("Service connection suspended");
        this.c.p().a(new db(this));
    }

    public final void a(ConnectionResult connectionResult) {
        l.b("MeasurementServiceConnection.onConnectionFailed");
        ar arVar = this.c.r;
        o oVar = (arVar.f == null || !arVar.f.v()) ? null : arVar.f;
        if (oVar != null) {
            oVar.f.a("Service connection failed", connectionResult);
        }
        synchronized (this) {
            this.f2491a = false;
            this.f2492b = null;
        }
        this.c.p().a(new dc(this));
    }
}
