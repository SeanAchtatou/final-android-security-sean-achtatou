package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.server.converter.zaa;
import com.google.android.gms.common.util.c;
import com.google.android.gms.common.util.m;
import com.google.android.gms.common.util.n;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class FastJsonResponse {

    public interface a<I, O> {
        I a(O o);
    }

    public abstract Map<String, Field<?, ?>> a();

    /* access modifiers changed from: protected */
    public abstract Object b();

    /* access modifiers changed from: protected */
    public abstract boolean c();

    /* access modifiers changed from: protected */
    public final boolean a(Field field) {
        if (field.c != 11) {
            return c();
        }
        if (field.d) {
            throw new UnsupportedOperationException("Concrete type arrays not supported");
        }
        throw new UnsupportedOperationException("Concrete types not supported");
    }

    public static class Field<I, O> extends AbstractSafeParcelable {
        public static final i CREATOR = new i();

        /* renamed from: a  reason: collision with root package name */
        protected final int f1643a;

        /* renamed from: b  reason: collision with root package name */
        protected final boolean f1644b;
        protected final int c;
        protected final boolean d;
        protected final String e;
        protected final int f;
        protected final Class<? extends FastJsonResponse> g;
        zak h;
        /* access modifiers changed from: package-private */
        public a<I, O> i;
        private final int j;
        private final String k;

        Field(int i2, int i3, boolean z, int i4, boolean z2, String str, int i5, String str2, zaa zaa) {
            this.j = i2;
            this.f1643a = i3;
            this.f1644b = z;
            this.c = i4;
            this.d = z2;
            this.e = str;
            this.f = i5;
            if (str2 == null) {
                this.g = null;
                this.k = null;
            } else {
                this.g = SafeParcelResponse.class;
                this.k = str2;
            }
            if (zaa == null) {
                this.i = null;
            } else if (zaa.f1641a != null) {
                this.i = zaa.f1641a;
            } else {
                throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
            }
        }

        public final int a() {
            return this.f;
        }

        private final String c() {
            String str = this.k;
            if (str == null) {
                return null;
            }
            return str;
        }

        public final Map<String, Field<?, ?>> b() {
            l.a((Object) this.k);
            l.a(this.h);
            return this.h.a(this.k);
        }

        public String toString() {
            j.a a2 = j.a(this).a("versionCode", Integer.valueOf(this.j)).a("typeIn", Integer.valueOf(this.f1643a)).a("typeInArray", Boolean.valueOf(this.f1644b)).a("typeOut", Integer.valueOf(this.c)).a("typeOutArray", Boolean.valueOf(this.d)).a("outputFieldName", this.e).a("safeParcelFieldId", Integer.valueOf(this.f)).a("concreteTypeName", c());
            Class<? extends FastJsonResponse> cls = this.g;
            if (cls != null) {
                a2.a("concreteType.class", cls.getCanonicalName());
            }
            a<I, O> aVar = this.i;
            if (aVar != null) {
                a2.a("converterName", aVar.getClass().getCanonicalName());
            }
            return a2.toString();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
         arg types: [android.os.Parcel, int, java.lang.String, int]
         candidates:
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
         arg types: [android.os.Parcel, int, com.google.android.gms.common.server.converter.zaa, int, int]
         candidates:
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
        public void writeToParcel(Parcel parcel, int i2) {
            zaa zaa;
            int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 1, this.j);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 2, this.f1643a);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 3, this.f1644b);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 4, this.c);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 5, this.d);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 6, this.e, false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 7, this.f);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 8, c(), false);
            a<I, O> aVar = this.i;
            if (aVar == null) {
                zaa = null;
            } else {
                zaa = zaa.a(aVar);
            }
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 9, (Parcelable) zaa, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
        }
    }

    protected static <O, I> I a(Field<I, O> field, Object obj) {
        return field.i != null ? field.i.a(obj) : obj;
    }

    public String toString() {
        Map<String, Field<?, ?>> a2 = a();
        StringBuilder sb = new StringBuilder(100);
        for (String next : a2.keySet()) {
            Field field = a2.get(next);
            if (a(field)) {
                Object a3 = a(field, b(field));
                if (sb.length() == 0) {
                    sb.append("{");
                } else {
                    sb.append(",");
                }
                sb.append("\"");
                sb.append(next);
                sb.append("\":");
                if (a3 != null) {
                    switch (field.c) {
                        case 8:
                            sb.append("\"");
                            sb.append(c.a((byte[]) a3));
                            sb.append("\"");
                            continue;
                        case 9:
                            sb.append("\"");
                            sb.append(c.b((byte[]) a3));
                            sb.append("\"");
                            continue;
                        case 10:
                            n.a(sb, (HashMap) a3);
                            continue;
                        default:
                            if (!field.f1644b) {
                                a(sb, field, a3);
                                break;
                            } else {
                                ArrayList arrayList = (ArrayList) a3;
                                sb.append("[");
                                int size = arrayList.size();
                                for (int i = 0; i < size; i++) {
                                    if (i > 0) {
                                        sb.append(",");
                                    }
                                    Object obj = arrayList.get(i);
                                    if (obj != null) {
                                        a(sb, field, obj);
                                    }
                                }
                                sb.append("]");
                                continue;
                            }
                    }
                } else {
                    sb.append("null");
                }
            }
        }
        if (sb.length() > 0) {
            sb.append("}");
        } else {
            sb.append("{}");
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final Object b(Field field) {
        String str = field.e;
        if (field.g == null) {
            return b();
        }
        b();
        l.a(true, "Concrete field shouldn't be value object: %s", field.e);
        try {
            char upperCase = Character.toUpperCase(str.charAt(0));
            String substring = str.substring(1);
            StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 4);
            sb.append("get");
            sb.append(upperCase);
            sb.append(substring);
            return getClass().getMethod(sb.toString(), new Class[0]).invoke(this, new Object[0]);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void a(StringBuilder sb, Field field, Object obj) {
        if (field.f1643a == 11) {
            sb.append(((FastJsonResponse) field.g.cast(obj)).toString());
        } else if (field.f1643a == 7) {
            sb.append("\"");
            sb.append(m.a((String) obj));
            sb.append("\"");
        } else {
            sb.append(obj);
        }
    }
}
