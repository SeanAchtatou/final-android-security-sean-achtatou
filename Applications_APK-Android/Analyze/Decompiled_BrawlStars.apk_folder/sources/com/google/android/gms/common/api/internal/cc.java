package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.d;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.signin.e;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;

final class cc implements az {

    /* renamed from: a  reason: collision with root package name */
    final am f1451a;

    /* renamed from: b  reason: collision with root package name */
    final am f1452b;
    Bundle c;
    ConnectionResult d = null;
    ConnectionResult e = null;
    boolean f = false;
    final Lock g;
    private final Context h;
    private final ag i;
    private final Looper j;
    private final Map<a.c<?>, am> k;
    private final Set<Object> l = Collections.newSetFromMap(new WeakHashMap());
    private final a.f m;
    private int n = 0;

    public static cc a(Context context, ag agVar, Lock lock, Looper looper, d dVar, Map<a.c<?>, a.f> map, b bVar, Map<a<?>, Boolean> map2, a.C0111a<? extends e, com.google.android.gms.signin.a> aVar, ArrayList<ca> arrayList) {
        Map<a<?>, Boolean> map3 = map2;
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        for (Map.Entry next : map.entrySet()) {
            a.f fVar = (a.f) next.getValue();
            if (fVar.d()) {
                arrayMap.put((a.c) next.getKey(), fVar);
            } else {
                arrayMap2.put((a.c) next.getKey(), fVar);
            }
        }
        l.a(!arrayMap.isEmpty(), "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        ArrayMap arrayMap3 = new ArrayMap();
        ArrayMap arrayMap4 = new ArrayMap();
        for (a next2 : map2.keySet()) {
            a.c<?> b2 = next2.b();
            if (arrayMap.containsKey(b2)) {
                arrayMap3.put(next2, map3.get(next2));
            } else if (arrayMap2.containsKey(b2)) {
                arrayMap4.put(next2, map3.get(next2));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = arrayList;
        int size = arrayList4.size();
        int i2 = 0;
        while (i2 < size) {
            Object obj = arrayList4.get(i2);
            i2++;
            ca caVar = (ca) obj;
            if (arrayMap3.containsKey(caVar.f1449a)) {
                arrayList2.add(caVar);
            } else if (arrayMap4.containsKey(caVar.f1449a)) {
                arrayList3.add(caVar);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new cc(context, agVar, lock, looper, dVar, arrayMap, arrayMap2, bVar, aVar, null, arrayList2, arrayList3, arrayMap3, arrayMap4);
    }

    private cc(Context context, ag agVar, Lock lock, Looper looper, d dVar, Map<a.c<?>, a.f> map, Map<a.c<?>, a.f> map2, b bVar, a.C0111a<? extends e, com.google.android.gms.signin.a> aVar, a.f fVar, ArrayList<ca> arrayList, ArrayList<ca> arrayList2, Map<a<?>, Boolean> map3, Map<a<?>, Boolean> map4) {
        this.h = context;
        this.i = agVar;
        this.g = lock;
        this.j = looper;
        this.m = fVar;
        Context context2 = context;
        Lock lock2 = lock;
        Looper looper2 = looper;
        d dVar2 = dVar;
        am amVar = r2;
        am amVar2 = new am(context2, this.i, lock2, looper2, dVar2, map2, null, map4, null, arrayList2, new cd(this, (byte) 0));
        this.f1451a = amVar;
        this.f1452b = new am(context2, this.i, lock2, looper2, dVar2, map, bVar, map3, aVar, arrayList, new ce(this, (byte) 0));
        ArrayMap arrayMap = new ArrayMap();
        for (a.c<?> put : map2.keySet()) {
            arrayMap.put(put, this.f1451a);
        }
        for (a.c<?> put2 : map.keySet()) {
            arrayMap.put(put2, this.f1452b);
        }
        this.k = Collections.unmodifiableMap(arrayMap);
    }

    public final void a() {
        this.n = 2;
        this.f = false;
        this.e = null;
        this.d = null;
        this.f1451a.a();
        this.f1452b.a();
    }

    public final void b() {
        this.e = null;
        this.d = null;
        this.n = 0;
        this.f1451a.b();
        this.f1452b.b();
        c();
    }

    public final boolean d() {
        this.g.lock();
        try {
            boolean z = true;
            if (!this.f1451a.d() || (!this.f1452b.d() && !g() && this.n != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.g.unlock();
        }
    }

    public final boolean e() {
        this.g.lock();
        try {
            return this.n == 2;
        } finally {
            this.g.unlock();
        }
    }

    public final void f() {
        this.f1451a.f();
        this.f1452b.f();
    }

    private final void a(ConnectionResult connectionResult) {
        int i2 = this.n;
        if (i2 != 1) {
            if (i2 != 2) {
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                this.n = 0;
            }
            this.i.a(connectionResult);
        }
        c();
        this.n = 0;
    }

    private final void c() {
        Iterator<Object> it = this.l.iterator();
        while (it.hasNext()) {
            it.next();
        }
        this.l.clear();
    }

    private final boolean g() {
        ConnectionResult connectionResult = this.e;
        return connectionResult != null && connectionResult.f1353b == 4;
    }

    private static boolean b(ConnectionResult connectionResult) {
        return connectionResult != null && connectionResult.b();
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append((CharSequence) "authClient").println(":");
        this.f1452b.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append((CharSequence) str).append((CharSequence) "anonClient").println(":");
        this.f1451a.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final <A extends com.google.android.gms.common.api.a.b, T extends com.google.android.gms.common.api.internal.c.a<? extends com.google.android.gms.common.api.g, A>> T a(T r8) {
        /*
            r7 = this;
            com.google.android.gms.common.api.a$c<A> r0 = r8.f1447a
            java.util.Map<com.google.android.gms.common.api.a$c<?>, com.google.android.gms.common.api.internal.am> r1 = r7.k
            boolean r1 = r1.containsKey(r0)
            java.lang.String r2 = "GoogleApiClient is not configured to use the API required for this call."
            com.google.android.gms.common.internal.l.b(r1, r2)
            java.util.Map<com.google.android.gms.common.api.a$c<?>, com.google.android.gms.common.api.internal.am> r1 = r7.k
            java.lang.Object r0 = r1.get(r0)
            com.google.android.gms.common.api.internal.am r0 = (com.google.android.gms.common.api.internal.am) r0
            com.google.android.gms.common.api.internal.am r1 = r7.f1452b
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x004f
            boolean r0 = r7.g()
            if (r0 == 0) goto L_0x0048
            com.google.android.gms.common.api.Status r0 = new com.google.android.gms.common.api.Status
            r1 = 4
            com.google.android.gms.common.api.a$f r2 = r7.m
            r3 = 0
            if (r2 != 0) goto L_0x002d
            r2 = r3
            goto L_0x0041
        L_0x002d:
            android.content.Context r2 = r7.h
            com.google.android.gms.common.api.internal.ag r4 = r7.i
            int r4 = java.lang.System.identityHashCode(r4)
            com.google.android.gms.common.api.a$f r5 = r7.m
            android.content.Intent r5 = r5.e()
            r6 = 134217728(0x8000000, float:3.85186E-34)
            android.app.PendingIntent r2 = android.app.PendingIntent.getActivity(r2, r4, r5, r6)
        L_0x0041:
            r0.<init>(r1, r3, r2)
            r8.a(r0)
            return r8
        L_0x0048:
            com.google.android.gms.common.api.internal.am r0 = r7.f1452b
            com.google.android.gms.common.api.internal.c$a r8 = r0.a(r8)
            return r8
        L_0x004f:
            com.google.android.gms.common.api.internal.am r0 = r7.f1451a
            com.google.android.gms.common.api.internal.c$a r8 = r0.a(r8)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.cc.a(com.google.android.gms.common.api.internal.c$a):com.google.android.gms.common.api.internal.c$a");
    }

    static /* synthetic */ void a(cc ccVar) {
        if (b(ccVar.d)) {
            if (b(ccVar.e) || ccVar.g()) {
                int i2 = ccVar.n;
                if (i2 != 1) {
                    if (i2 != 2) {
                        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                        ccVar.n = 0;
                        return;
                    }
                    ccVar.i.a(ccVar.c);
                }
                ccVar.c();
                ccVar.n = 0;
                return;
            }
            ConnectionResult connectionResult = ccVar.e;
            if (connectionResult == null) {
                return;
            }
            if (ccVar.n == 1) {
                ccVar.c();
                return;
            }
            ccVar.a(connectionResult);
            ccVar.f1451a.b();
        } else if (ccVar.d == null || !b(ccVar.e)) {
            ConnectionResult connectionResult2 = ccVar.d;
            if (connectionResult2 != null && ccVar.e != null) {
                if (ccVar.f1452b.l < ccVar.f1451a.l) {
                    connectionResult2 = ccVar.e;
                }
                ccVar.a(connectionResult2);
            }
        } else {
            ccVar.f1452b.b();
            ccVar.a(ccVar.d);
        }
    }

    static /* synthetic */ void a(cc ccVar, int i2, boolean z) {
        ccVar.i.a(i2, z);
        ccVar.e = null;
        ccVar.d = null;
    }
}
