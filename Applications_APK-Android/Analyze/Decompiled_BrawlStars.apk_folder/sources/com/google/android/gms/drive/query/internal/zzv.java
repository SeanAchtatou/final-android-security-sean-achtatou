package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public final class zzv extends zza {
    public static final Parcelable.Creator<zzv> CREATOR = new m();

    /* renamed from: a  reason: collision with root package name */
    private final FilterHolder f1764a;

    zzv(FilterHolder filterHolder) {
        this.f1764a = filterHolder;
    }

    public final <T> T a(g<T> gVar) {
        return gVar.a(this.f1764a.f1749a.a(gVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.FilterHolder, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, (Parcelable) this.f1764a, i, false);
        a.b(parcel, a2);
    }
}
