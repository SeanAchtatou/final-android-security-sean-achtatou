package com.google.android.gms.internal.measurement;

import android.content.SharedPreferences;
import java.util.Iterator;

final /* synthetic */ class ea implements SharedPreferences.OnSharedPreferenceChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final dz f2178a;

    ea(dz dzVar) {
        this.f2178a = dzVar;
    }

    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        dz dzVar = this.f2178a;
        synchronized (dzVar.f2175b) {
            dzVar.c = null;
            ds.a();
        }
        synchronized (dzVar) {
            Iterator<Object> it = dzVar.d.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }
}
