package com.google.android.gms.games.request;

import android.os.Parcel;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public final class zzb extends d implements GameRequest {
    private final int c;

    public final int a(String str) {
        for (int i = this.f1533b; i < this.f1533b + this.c; i++) {
            int a2 = this.f1532a.a(i);
            if (this.f1532a.c("recipient_external_player_id", i, a2).equals(str)) {
                return this.f1532a.b("recipient_status", i, a2);
            }
        }
        return -1;
    }

    public final /* synthetic */ Object a() {
        return new GameRequestEntity(this);
    }

    public final String b() {
        return e("external_request_id");
    }

    public final Game c() {
        return new GameRef(this.f1532a, this.f1533b);
    }

    public final Player d() {
        return new PlayerRef(this.f1532a, c_(), "sender_");
    }

    public final int describeContents() {
        return 0;
    }

    public final List<Player> e() {
        ArrayList arrayList = new ArrayList(this.c);
        for (int i = 0; i < this.c; i++) {
            arrayList.add(new PlayerRef(this.f1532a, this.f1533b + i, "recipient_"));
        }
        return arrayList;
    }

    public final boolean equals(Object obj) {
        return GameRequestEntity.a(this, obj);
    }

    public final byte[] f() {
        return g(ShareConstants.WEB_DIALOG_PARAM_DATA);
    }

    public final int g() {
        return c("type");
    }

    public final long h() {
        return b("creation_timestamp");
    }

    public final int hashCode() {
        return GameRequestEntity.a(this);
    }

    public final long i() {
        return b("expiration_timestamp");
    }

    public final int j() {
        return c("status");
    }

    public final String toString() {
        return GameRequestEntity.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((GameRequestEntity) ((GameRequest) a())).writeToParcel(parcel, i);
    }
}
