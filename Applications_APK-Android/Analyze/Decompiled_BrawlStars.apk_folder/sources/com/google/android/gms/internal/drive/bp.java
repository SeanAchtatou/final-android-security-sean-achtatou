package com.google.android.gms.internal.drive;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.k;
import java.util.Arrays;

public final class bp extends k<DriveId> {

    /* renamed from: a  reason: collision with root package name */
    public static final bp f1898a = new bp();

    private bp() {
        super("driveId", Arrays.asList("sqlId", "resourceId", "mimeType"), Arrays.asList("dbInstanceId"), 4100000);
    }

    public final boolean b(DataHolder dataHolder, int i, int i2) {
        for (String a2 : this.c) {
            if (!dataHolder.a(a2)) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        long j = dataHolder.d.getLong("dbInstanceId");
        boolean equals = "application/vnd.google-apps.folder".equals(dataHolder.c(au.x.f1731b, i, i2));
        String c = dataHolder.c("resourceId", i, i2);
        return new DriveId("generated-android-null".equals(c) ? null : c, Long.valueOf(dataHolder.a("sqlId", i, i2)).longValue(), j, equals ? 1 : 0);
    }
}
