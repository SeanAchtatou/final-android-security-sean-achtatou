package com.google.android.gms.tasks;

import android.app.Activity;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import com.google.android.gms.common.api.internal.g;
import com.google.android.gms.common.internal.l;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;

public final class ab<TResult> extends g<TResult> {

    /* renamed from: a  reason: collision with root package name */
    private final Object f2675a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private final z<TResult> f2676b = new z<>();
    private boolean c;
    private volatile boolean d;
    private TResult e;
    private Exception f;

    ab() {
    }

    public final boolean a() {
        boolean z;
        synchronized (this.f2675a) {
            z = this.c;
        }
        return z;
    }

    static class a extends LifecycleCallback {

        /* renamed from: b  reason: collision with root package name */
        final List<WeakReference<y<?>>> f2677b = new ArrayList();

        public static a b(Activity activity) {
            g a2 = a(activity);
            a aVar = (a) a2.a("TaskOnStopCallback", a.class);
            return aVar == null ? new a(a2) : aVar;
        }

        private a(g gVar) {
            super(gVar);
            this.f1381a.a("TaskOnStopCallback", this);
        }

        public final void d() {
            synchronized (this.f2677b) {
                for (WeakReference<y<?>> weakReference : this.f2677b) {
                    y yVar = (y) weakReference.get();
                    if (yVar != null) {
                        yVar.a();
                    }
                }
                this.f2677b.clear();
            }
        }
    }

    public final boolean c() {
        return this.d;
    }

    public final boolean b() {
        boolean z;
        synchronized (this.f2675a) {
            z = this.c && !this.d && this.f == null;
        }
        return z;
    }

    public final TResult d() {
        TResult tresult;
        synchronized (this.f2675a) {
            g();
            i();
            if (this.f == null) {
                tresult = this.e;
            } else {
                throw new RuntimeExecutionException(this.f);
            }
        }
        return tresult;
    }

    public final <X extends Throwable> TResult a(Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.f2675a) {
            g();
            i();
            if (cls.isInstance(this.f)) {
                throw ((Throwable) cls.cast(this.f));
            } else if (this.f == null) {
                tresult = this.e;
            } else {
                throw new RuntimeExecutionException(this.f);
            }
        }
        return tresult;
    }

    public final Exception e() {
        Exception exc;
        synchronized (this.f2675a) {
            exc = this.f;
        }
        return exc;
    }

    public final g<TResult> a(Executor executor, e eVar) {
        this.f2676b.a(new u(executor, eVar));
        j();
        return this;
    }

    public final g<TResult> a(Activity activity, e eVar) {
        u uVar = new u(i.f2679a, eVar);
        this.f2676b.a(uVar);
        a b2 = a.b(activity);
        synchronized (b2.f2677b) {
            b2.f2677b.add(new WeakReference(uVar));
        }
        j();
        return this;
    }

    public final g<TResult> a(Executor executor, d dVar) {
        this.f2676b.a(new s(executor, dVar));
        j();
        return this;
    }

    public final g<TResult> a(Executor executor, c<TResult> cVar) {
        this.f2676b.a(new q(executor, cVar));
        j();
        return this;
    }

    public final <TContinuationResult> g<TContinuationResult> a(Executor executor, a aVar) {
        ab abVar = new ab();
        this.f2676b.a(new k(executor, aVar, abVar));
        j();
        return abVar;
    }

    public final g<TResult> a(Executor executor, b bVar) {
        this.f2676b.a(new o(executor, bVar));
        j();
        return this;
    }

    public final <TContinuationResult> g<TContinuationResult> b(Executor executor, a<TResult, g<TContinuationResult>> aVar) {
        ab abVar = new ab();
        this.f2676b.a(new m(executor, aVar, abVar));
        j();
        return abVar;
    }

    public final <TContinuationResult> g<TContinuationResult> a(Executor executor, f fVar) {
        ab abVar = new ab();
        this.f2676b.a(new w(executor, fVar, abVar));
        j();
        return abVar;
    }

    public final void a(TResult tresult) {
        synchronized (this.f2675a) {
            h();
            this.c = true;
            this.e = tresult;
        }
        this.f2676b.a(this);
    }

    public final boolean b(Object obj) {
        synchronized (this.f2675a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.e = obj;
            this.f2676b.a(this);
            return true;
        }
    }

    public final void a(Exception exc) {
        l.a(exc, "Exception must not be null");
        synchronized (this.f2675a) {
            h();
            this.c = true;
            this.f = exc;
        }
        this.f2676b.a(this);
    }

    public final boolean b(Exception exc) {
        l.a(exc, "Exception must not be null");
        synchronized (this.f2675a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.f = exc;
            this.f2676b.a(this);
            return true;
        }
    }

    public final boolean f() {
        synchronized (this.f2675a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = true;
            this.f2676b.a(this);
            return true;
        }
    }

    private final void g() {
        l.a(this.c, "Task is not yet complete");
    }

    private final void h() {
        l.a(!this.c, "Task is already complete");
    }

    private final void i() {
        if (this.d) {
            throw new CancellationException("Task is already canceled.");
        }
    }

    private final void j() {
        synchronized (this.f2675a) {
            if (this.c) {
                this.f2676b.a(this);
            }
        }
    }
}
