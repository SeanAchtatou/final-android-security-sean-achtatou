package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;

final class cs implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ boolean f2481a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ boolean f2482b;
    private final /* synthetic */ zzag c;
    private final /* synthetic */ zzk d;
    private final /* synthetic */ String e;
    private final /* synthetic */ cl f;

    cs(cl clVar, boolean z, boolean z2, zzag zzag, zzk zzk, String str) {
        this.f = clVar;
        this.f2481a = z;
        this.f2482b = z2;
        this.c = zzag;
        this.d = zzk;
        this.e = str;
    }

    public final void run() {
        zzaj zzaj = this.f.f2470b;
        if (zzaj == null) {
            this.f.q().c.a("Discarding data. Failed to send event to service");
            return;
        }
        if (this.f2481a) {
            this.f.a(zzaj, this.f2482b ? null : this.c, this.d);
        } else {
            try {
                if (TextUtils.isEmpty(this.e)) {
                    zzaj.a(this.c, this.d);
                } else {
                    zzaj.a(this.c, this.e, this.f.q().f_());
                }
            } catch (RemoteException e2) {
                this.f.q().c.a("Failed to send event to the service", e2);
            }
        }
        this.f.y();
    }
}
