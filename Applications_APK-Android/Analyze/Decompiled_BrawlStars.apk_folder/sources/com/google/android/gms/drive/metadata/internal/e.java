package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.a;
import com.google.android.gms.internal.drive.au;
import com.google.android.gms.internal.drive.bg;
import com.google.android.gms.internal.drive.bi;
import com.google.android.gms.internal.drive.bq;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<String, a<?>> f1738a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    private static final Map<String, f> f1739b = new HashMap();

    static {
        a(au.f1893a);
        a(au.G);
        a(au.x);
        a(au.E);
        a(au.H);
        a(au.n);
        a(au.m);
        a(au.o);
        a(au.p);
        a(au.q);
        a(au.k);
        a(au.s);
        a(au.t);
        a(au.u);
        a(au.C);
        a(au.f1894b);
        a(au.z);
        a(au.d);
        a(au.l);
        a(au.e);
        a(au.f);
        a(au.g);
        a(au.h);
        a(au.w);
        a(au.r);
        a(au.y);
        a(au.A);
        a(au.B);
        a(au.D);
        a(au.I);
        a(au.J);
        a(au.j);
        a(au.i);
        a(au.F);
        a(au.v);
        a(au.c);
        a(au.K);
        a(au.L);
        a(au.M);
        a(au.N);
        a(au.O);
        a(au.P);
        a(au.Q);
        a(bi.f1896a);
        a(bi.c);
        a(bi.d);
        a(bi.e);
        a(bi.f1897b);
        a(bi.f);
        a(bq.f1899a);
        a(bq.f1900b);
        a(m.f1740a);
        a(bg.f1895a);
    }

    public static a<?> a(String str) {
        return f1738a.get(str);
    }

    public static Collection<a<?>> a() {
        return Collections.unmodifiableCollection(f1738a.values());
    }

    public static void a(DataHolder dataHolder) {
        for (f a2 : f1739b.values()) {
            a2.a(dataHolder);
        }
    }

    private static void a(a<?> aVar) {
        if (f1738a.containsKey(aVar.a())) {
            String valueOf = String.valueOf(aVar.a());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Duplicate field name registered: ".concat(valueOf) : new String("Duplicate field name registered: "));
        } else {
            f1738a.put(aVar.a(), aVar);
        }
    }

    private static void a(f fVar) {
        if (f1739b.put(fVar.a(), fVar) != null) {
            String a2 = fVar.a();
            StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 46);
            sb.append("A cleaner for key ");
            sb.append(a2);
            sb.append(" has already been registered");
            throw new IllegalStateException(sb.toString());
        }
    }
}
