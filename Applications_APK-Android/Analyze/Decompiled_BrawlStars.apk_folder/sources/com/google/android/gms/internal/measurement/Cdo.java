package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.support.v4.content.PermissionChecker;

/* renamed from: com.google.android.gms.internal.measurement.do  reason: invalid class name */
final class Cdo implements dl {

    /* renamed from: a  reason: collision with root package name */
    static Cdo f2166a;

    /* renamed from: b  reason: collision with root package name */
    final Context f2167b;

    static Cdo a(Context context) {
        Cdo doVar;
        synchronized (Cdo.class) {
            if (f2166a == null) {
                f2166a = PermissionChecker.checkSelfPermission(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new Cdo(context) : new Cdo();
            }
            doVar = f2166a;
        }
        return doVar;
    }

    private Cdo(Context context) {
        this.f2167b = context;
        this.f2167b.getContentResolver().registerContentObserver(de.f2157a, true, new dq());
    }

    private Cdo() {
        this.f2167b = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final String a(String str) {
        if (this.f2167b == null) {
            return null;
        }
        try {
            return (String) dm.a(new dp(this, str));
        } catch (SecurityException unused) {
            String valueOf = String.valueOf(str);
            if (valueOf.length() != 0) {
                "Unable to read GServices for: ".concat(valueOf);
            } else {
                new String("Unable to read GServices for: ");
            }
            return null;
        }
    }
}
