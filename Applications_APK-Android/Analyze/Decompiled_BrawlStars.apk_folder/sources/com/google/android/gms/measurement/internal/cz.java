package com.google.android.gms.measurement.internal;

import android.content.ComponentName;

final class cz implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ComponentName f2495a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ cx f2496b;

    cz(cx cxVar, ComponentName componentName) {
        this.f2496b = cxVar;
        this.f2495a = componentName;
    }

    public final void run() {
        cl.a(this.f2496b.c, this.f2495a);
    }
}
