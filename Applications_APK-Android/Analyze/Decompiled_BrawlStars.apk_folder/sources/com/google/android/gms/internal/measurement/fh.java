package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fj;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class fh<FieldDescriptorType extends fj<FieldDescriptorType>> {
    private static final fh d = new fh((byte) 0);

    /* renamed from: a  reason: collision with root package name */
    final hm<FieldDescriptorType, Object> f2206a = hm.a(16);

    /* renamed from: b  reason: collision with root package name */
    boolean f2207b;
    boolean c = false;

    private fh() {
    }

    private fh(byte b2) {
        b();
    }

    public static <T extends fj<T>> fh<T> a() {
        return d;
    }

    public final void b() {
        if (!this.f2207b) {
            this.f2206a.a();
            this.f2207b = true;
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof fh)) {
            return false;
        }
        return this.f2206a.equals(((fh) obj).f2206a);
    }

    public final int hashCode() {
        return this.f2206a.hashCode();
    }

    public final Iterator<Map.Entry<FieldDescriptorType, Object>> c() {
        if (this.c) {
            return new ga(this.f2206a.entrySet().iterator());
        }
        return this.f2206a.entrySet().iterator();
    }

    private final Object a(FieldDescriptorType fielddescriptortype) {
        Object obj = this.f2206a.get(fielddescriptortype);
        return obj instanceof fy ? fy.a() : obj;
    }

    private final void b(FieldDescriptorType fielddescriptortype, Object obj) {
        if (!fielddescriptortype.d()) {
            a(fielddescriptortype.b(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList2.get(i);
                i++;
                a(fielddescriptortype.b(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof fy) {
            this.c = true;
        }
        this.f2206a.put(fielddescriptortype, obj);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if ((r3 instanceof com.google.android.gms.internal.measurement.fy) == false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        if ((r3 instanceof com.google.android.gms.internal.measurement.ft) == false) goto L_0x0041;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(com.google.android.gms.internal.measurement.ip r2, java.lang.Object r3) {
        /*
            com.google.android.gms.internal.measurement.fs.a(r3)
            int[] r0 = com.google.android.gms.internal.measurement.fi.f2208a
            com.google.android.gms.internal.measurement.iu r2 = r2.s
            int r2 = r2.ordinal()
            r2 = r0[r2]
            r0 = 1
            r1 = 0
            switch(r2) {
                case 1: goto L_0x003e;
                case 2: goto L_0x003b;
                case 3: goto L_0x0038;
                case 4: goto L_0x0035;
                case 5: goto L_0x0032;
                case 6: goto L_0x002f;
                case 7: goto L_0x0026;
                case 8: goto L_0x001c;
                case 9: goto L_0x0013;
                default: goto L_0x0012;
            }
        L_0x0012:
            goto L_0x0041
        L_0x0013:
            boolean r2 = r3 instanceof com.google.android.gms.internal.measurement.gt
            if (r2 != 0) goto L_0x0024
            boolean r2 = r3 instanceof com.google.android.gms.internal.measurement.fy
            if (r2 == 0) goto L_0x0041
            goto L_0x0024
        L_0x001c:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x0024
            boolean r2 = r3 instanceof com.google.android.gms.internal.measurement.ft
            if (r2 == 0) goto L_0x0041
        L_0x0024:
            r1 = 1
            goto L_0x0041
        L_0x0026:
            boolean r2 = r3 instanceof com.google.android.gms.internal.measurement.ej
            if (r2 != 0) goto L_0x0024
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L_0x0041
            goto L_0x0024
        L_0x002f:
            boolean r0 = r3 instanceof java.lang.String
            goto L_0x0040
        L_0x0032:
            boolean r0 = r3 instanceof java.lang.Boolean
            goto L_0x0040
        L_0x0035:
            boolean r0 = r3 instanceof java.lang.Double
            goto L_0x0040
        L_0x0038:
            boolean r0 = r3 instanceof java.lang.Float
            goto L_0x0040
        L_0x003b:
            boolean r0 = r3 instanceof java.lang.Long
            goto L_0x0040
        L_0x003e:
            boolean r0 = r3 instanceof java.lang.Integer
        L_0x0040:
            r1 = r0
        L_0x0041:
            if (r1 == 0) goto L_0x0044
            return
        L_0x0044:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            goto L_0x004d
        L_0x004c:
            throw r2
        L_0x004d:
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.fh.a(com.google.android.gms.internal.measurement.ip, java.lang.Object):void");
    }

    public final boolean d() {
        for (int i = 0; i < this.f2206a.b(); i++) {
            if (!c(this.f2206a.b(i))) {
                return false;
            }
        }
        for (Map.Entry<FieldDescriptorType, Object> c2 : this.f2206a.c()) {
            if (!c(c2)) {
                return false;
            }
        }
        return true;
    }

    private static boolean c(Map.Entry<FieldDescriptorType, Object> entry) {
        fj fjVar = (fj) entry.getKey();
        if (fjVar.c() == iu.MESSAGE) {
            if (fjVar.d()) {
                for (gt f : (List) entry.getValue()) {
                    if (!f.f()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof gt) {
                    if (!((gt) value).f()) {
                        return false;
                    }
                } else if (value instanceof fy) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    private static Object a(Object obj) {
        if (obj instanceof gz) {
            return ((gz) obj).a();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    /* access modifiers changed from: package-private */
    public final void a(Map.Entry<FieldDescriptorType, Object> entry) {
        Object obj;
        fj fjVar = (fj) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof fy) {
            value = fy.a();
        }
        if (fjVar.d()) {
            Object a2 = a(fjVar);
            if (a2 == null) {
                a2 = new ArrayList();
            }
            for (Object a3 : (List) value) {
                ((List) a2).add(a(a3));
            }
            this.f2206a.put(fjVar, a2);
        } else if (fjVar.c() == iu.MESSAGE) {
            Object a4 = a(fjVar);
            if (a4 == null) {
                this.f2206a.put(fjVar, a(value));
                return;
            }
            if (a4 instanceof gz) {
                obj = fjVar.g();
            } else {
                ((gt) a4).i();
                obj = fjVar.f().d();
            }
            this.f2206a.put(fjVar, obj);
        } else {
            this.f2206a.put(fjVar, a(value));
        }
    }

    static void a(zztv zztv, ip ipVar, int i, Object obj) throws IOException {
        if (ipVar == ip.GROUP) {
            fs.a();
            zztv.a(i, 3);
            ((gt) obj).a(zztv);
            zztv.a(i, 4);
            return;
        }
        zztv.a(i, ipVar.t);
        switch (fi.f2209b[ipVar.ordinal()]) {
            case 1:
                zztv.a(((Double) obj).doubleValue());
                return;
            case 2:
                zztv.a(((Float) obj).floatValue());
                return;
            case 3:
                zztv.a(((Long) obj).longValue());
                return;
            case 4:
                zztv.a(((Long) obj).longValue());
                return;
            case 5:
                zztv.a(((Integer) obj).intValue());
                return;
            case 6:
                zztv.c(((Long) obj).longValue());
                return;
            case 7:
                zztv.d(((Integer) obj).intValue());
                return;
            case 8:
                zztv.a(((Boolean) obj).booleanValue());
                return;
            case 9:
                ((gt) obj).a(zztv);
                return;
            case 10:
                zztv.a((gt) obj);
                return;
            case 11:
                if (obj instanceof ej) {
                    zztv.a((ej) obj);
                    return;
                } else {
                    zztv.a((String) obj);
                    return;
                }
            case 12:
                if (obj instanceof ej) {
                    zztv.a((ej) obj);
                    return;
                }
                byte[] bArr = (byte[]) obj;
                zztv.a(bArr, bArr.length);
                return;
            case 13:
                zztv.b(((Integer) obj).intValue());
                return;
            case 14:
                zztv.d(((Integer) obj).intValue());
                return;
            case 15:
                zztv.c(((Long) obj).longValue());
                return;
            case 16:
                zztv.c(((Integer) obj).intValue());
                return;
            case 17:
                zztv.b(((Long) obj).longValue());
                return;
            case 18:
                if (obj instanceof ft) {
                    zztv.a(((ft) obj).a());
                    return;
                } else {
                    zztv.a(((Integer) obj).intValue());
                    return;
                }
            default:
                return;
        }
    }

    static int b(Map.Entry<FieldDescriptorType, Object> entry) {
        fj fjVar = (fj) entry.getKey();
        Object value = entry.getValue();
        if (fjVar.c() != iu.MESSAGE || fjVar.d() || fjVar.e()) {
            return a(fjVar, value);
        }
        if (value instanceof fy) {
            return zztv.b(((fj) entry.getKey()).a(), (fy) value);
        }
        return zztv.d(((fj) entry.getKey()).a(), (gt) value);
    }

    static int a(ip ipVar, int i, Object obj) {
        int l = zztv.l(i);
        if (ipVar == ip.GROUP) {
            fs.a();
            l <<= 1;
        }
        return l + b(ipVar, obj);
    }

    private static int b(ip ipVar, Object obj) {
        switch (fi.f2209b[ipVar.ordinal()]) {
            case 1:
                ((Double) obj).doubleValue();
                return zztv.f();
            case 2:
                ((Float) obj).floatValue();
                return zztv.e();
            case 3:
                return zztv.d(((Long) obj).longValue());
            case 4:
                return zztv.e(((Long) obj).longValue());
            case 5:
                return zztv.m(((Integer) obj).intValue());
            case 6:
                ((Long) obj).longValue();
                return zztv.c();
            case 7:
                ((Integer) obj).intValue();
                return zztv.a();
            case 8:
                ((Boolean) obj).booleanValue();
                return zztv.g();
            case 9:
                return zztv.c((gt) obj);
            case 10:
                if (obj instanceof fy) {
                    return zztv.a((fy) obj);
                }
                return zztv.b((gt) obj);
            case 11:
                if (obj instanceof ej) {
                    return zztv.b((ej) obj);
                }
                return zztv.b((String) obj);
            case 12:
                if (obj instanceof ej) {
                    return zztv.b((ej) obj);
                }
                return zztv.b((byte[]) obj);
            case 13:
                return zztv.n(((Integer) obj).intValue());
            case 14:
                ((Integer) obj).intValue();
                return zztv.b();
            case 15:
                ((Long) obj).longValue();
                return zztv.d();
            case 16:
                return zztv.o(((Integer) obj).intValue());
            case 17:
                return zztv.f(((Long) obj).longValue());
            case 18:
                if (obj instanceof ft) {
                    return zztv.p(((ft) obj).a());
                }
                return zztv.p(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    static int a(fj<?> fjVar, Object obj) {
        ip b2 = fjVar.b();
        int a2 = fjVar.a();
        if (!fjVar.d()) {
            return a(b2, a2, obj);
        }
        int i = 0;
        if (fjVar.e()) {
            for (Object b3 : (List) obj) {
                i += b(b2, b3);
            }
            return zztv.l(a2) + i + zztv.q(i);
        }
        for (Object a3 : (List) obj) {
            i += a(b2, a2, a3);
        }
        return i;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        fh fhVar = new fh();
        for (int i = 0; i < this.f2206a.b(); i++) {
            Map.Entry<FieldDescriptorType, Object> b2 = this.f2206a.b(i);
            fhVar.b((fj) b2.getKey(), b2.getValue());
        }
        for (Map.Entry next : this.f2206a.c()) {
            fhVar.b((fj) next.getKey(), next.getValue());
        }
        fhVar.c = this.c;
        return fhVar;
    }
}
