package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.Uri;
import com.google.android.gms.tagmanager.c;
import java.util.Map;

final class be implements c.b {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2640a;

    public be(Context context) {
        this.f2640a = context;
    }

    public final void a(Map<String, Object> map) {
        String queryParameter;
        Object obj;
        Object obj2 = map.get("gtm.url");
        if (obj2 == null && (obj = map.get("gtm")) != null && (obj instanceof Map)) {
            obj2 = ((Map) obj).get("url");
        }
        if (obj2 != null && (obj2 instanceof String) && (queryParameter = Uri.parse((String) obj2).getQueryParameter("referrer")) != null) {
            z.b(this.f2640a, queryParameter);
        }
    }
}
