package com.google.android.gms.common.api.internal;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.base.a;
import com.google.android.gms.internal.base.zab;

public interface IStatusCallback extends IInterface {

    public static abstract class Stub extends zab implements IStatusCallback {
        public Stub() {
            super("com.google.android.gms.common.api.internal.IStatusCallback");
        }

        public static class zaa extends com.google.android.gms.internal.base.zaa implements IStatusCallback {
            public final void a(Status status) throws RemoteException {
                Parcel a2 = a();
                a.a(a2, status);
                try {
                    this.f1880a.transact(1, a2, null, 1);
                } finally {
                    a2.recycle();
                }
            }
        }

        public final boolean a(int i, Parcel parcel, Parcel parcel2) throws RemoteException {
            if (i != 1) {
                return false;
            }
            a((Status) a.a(parcel, Status.CREATOR));
            return true;
        }
    }

    void a(Status status) throws RemoteException;
}
