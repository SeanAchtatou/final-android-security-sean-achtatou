package com.google.android.gms.internal.ads_identifier;

import android.os.IInterface;
import android.os.RemoteException;

public interface zze extends IInterface {
    String a() throws RemoteException;

    boolean b() throws RemoteException;
}
