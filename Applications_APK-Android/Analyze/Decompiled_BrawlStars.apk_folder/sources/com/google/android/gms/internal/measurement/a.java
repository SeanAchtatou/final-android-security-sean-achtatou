package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.l;
import java.util.HashMap;
import java.util.Map;

public final class a extends l<a> {

    /* renamed from: a  reason: collision with root package name */
    public Map<Integer, Double> f2030a = new HashMap(4);

    public final String toString() {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.f2030a.entrySet()) {
            String valueOf = String.valueOf(next.getKey());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 6);
            sb.append("metric");
            sb.append(valueOf);
            hashMap.put(sb.toString(), next.getValue());
        }
        return a((Object) hashMap);
    }

    public final /* synthetic */ void a(l lVar) {
        ((a) lVar).f2030a.putAll(this.f2030a);
    }
}
