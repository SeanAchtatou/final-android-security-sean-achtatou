package com.google.android.gms.measurement.internal;

import android.os.Handler;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.measurement.cc;

abstract class et {

    /* renamed from: b  reason: collision with root package name */
    private static volatile Handler f2557b;

    /* renamed from: a  reason: collision with root package name */
    private final bo f2558a;
    private final Runnable c;
    /* access modifiers changed from: private */
    public volatile long d;

    et(bo boVar) {
        l.a(boVar);
        this.f2558a = boVar;
        this.c = new eu(this, boVar);
    }

    public abstract void a();

    public final void a(long j) {
        c();
        if (j >= 0) {
            this.d = this.f2558a.l().a();
            if (!d().postDelayed(this.c, j)) {
                this.f2558a.q().c.a("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    public final boolean b() {
        return this.d != 0;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.d = 0;
        d().removeCallbacks(this.c);
    }

    private final Handler d() {
        Handler handler;
        if (f2557b != null) {
            return f2557b;
        }
        synchronized (et.class) {
            if (f2557b == null) {
                f2557b = new cc(this.f2558a.m().getMainLooper());
            }
            handler = f2557b;
        }
        return handler;
    }
}
