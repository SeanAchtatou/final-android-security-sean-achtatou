package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

final class bb implements Callable<List<zzo>> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzk f2409a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2410b;
    private final /* synthetic */ String c;
    private final /* synthetic */ zzby d;

    bb(zzby zzby, zzk zzk, String str, String str2) {
        this.d = zzby;
        this.f2409a = zzk;
        this.f2410b = str;
        this.c = str2;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.d.f2597a.k();
        return this.d.f2597a.d().b(this.f2409a.f2601a, this.f2410b, this.c);
    }
}
