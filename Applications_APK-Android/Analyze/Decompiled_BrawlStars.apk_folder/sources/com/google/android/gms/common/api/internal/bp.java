package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.d;
import com.google.android.gms.tasks.h;

abstract class bp<T> extends bd {

    /* renamed from: a  reason: collision with root package name */
    protected final h<T> f1430a;

    public bp(int i, h<T> hVar) {
        super(4);
        this.f1430a = hVar;
    }

    public void a(m mVar, boolean z) {
    }

    /* access modifiers changed from: protected */
    public abstract void d(d.a<?> aVar) throws RemoteException;

    public void a(Status status) {
        this.f1430a.b(new ApiException(status));
    }

    public void a(RuntimeException runtimeException) {
        this.f1430a.b(runtimeException);
    }

    public final void a(d.a<?> aVar) throws DeadObjectException {
        try {
            d(aVar);
        } catch (DeadObjectException e) {
            a(ai.a(e));
            throw e;
        } catch (RemoteException e2) {
            a(ai.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }
}
