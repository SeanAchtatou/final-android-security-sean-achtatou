package com.google.android.gms.internal.measurement;

final class gq {

    /* renamed from: a  reason: collision with root package name */
    private static final go f2248a = c();

    /* renamed from: b  reason: collision with root package name */
    private static final go f2249b = new gp();

    static go a() {
        return f2248a;
    }

    static go b() {
        return f2249b;
    }

    private static go c() {
        try {
            return (go) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
