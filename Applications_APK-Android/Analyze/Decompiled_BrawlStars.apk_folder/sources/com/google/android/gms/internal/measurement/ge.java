package com.google.android.gms.internal.measurement;

import java.util.List;

abstract class ge {

    /* renamed from: a  reason: collision with root package name */
    private static final ge f2237a = new gf((byte) 0);

    /* renamed from: b  reason: collision with root package name */
    private static final ge f2238b = new gg((byte) 0);

    private ge() {
    }

    /* access modifiers changed from: package-private */
    public abstract <L> List<L> a(Object obj, long j);

    /* access modifiers changed from: package-private */
    public abstract <L> void a(Object obj, Object obj2, long j);

    /* access modifiers changed from: package-private */
    public abstract void b(Object obj, long j);

    static ge a() {
        return f2237a;
    }

    static ge b() {
        return f2238b;
    }

    /* synthetic */ ge(byte b2) {
        this();
    }
}
