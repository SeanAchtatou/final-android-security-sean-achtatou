package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.games.internal.player.zza;
import com.google.android.gms.games.internal.player.zzb;
import java.util.Arrays;

public final class PlayerEntity extends GamesDowngradeableSafeParcel implements Player {
    public static final Parcelable.Creator<PlayerEntity> CREATOR = new a();

    /* renamed from: b  reason: collision with root package name */
    private String f1787b;
    private String c;
    private final Uri d;
    private final Uri e;
    private final long f;
    private final int g;
    private final long h;
    private final String i;
    private final String j;
    private final String k;
    private final zzb l;
    private final PlayerLevelInfo m;
    private final boolean n;
    private final boolean o;
    private final String p;
    private final String q;
    private final Uri r;
    private final String s;
    private final Uri t;
    private final String u;
    private final int v;
    private final long w;
    private final boolean x;

    static final class a extends h {
        a() {
        }

        public final PlayerEntity a(Parcel parcel) {
            if (PlayerEntity.b(PlayerEntity.b_()) || PlayerEntity.a(PlayerEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            return new PlayerEntity(readString, readString2, readString3 == null ? null : Uri.parse(readString3), readString4 == null ? null : Uri.parse(readString4), parcel.readLong(), -1, -1, null, null, null, null, null, true, false, parcel.readString(), parcel.readString(), null, null, null, null, -1, -1, false);
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }
    }

    public PlayerEntity(Player player) {
        this(player, (byte) 0);
    }

    private PlayerEntity(Player player, byte b2) {
        this.f1787b = player.b();
        this.c = player.c();
        this.d = player.g();
        this.i = player.getIconImageUrl();
        this.e = player.h();
        this.j = player.getHiResImageUrl();
        this.f = player.i();
        this.g = player.k();
        this.h = player.j();
        this.k = player.m();
        this.n = player.l();
        zza o2 = player.o();
        this.l = o2 == null ? null : new zzb(o2);
        this.m = player.n();
        this.o = player.f();
        this.p = player.d();
        this.q = player.e();
        this.r = player.p();
        this.s = player.getBannerImageLandscapeUrl();
        this.t = player.q();
        this.u = player.getBannerImagePortraitUrl();
        this.v = player.r();
        this.w = player.s();
        this.x = player.t();
        com.google.android.gms.common.internal.a.a(this.f1787b);
        com.google.android.gms.common.internal.a.a(this.c);
        com.google.android.gms.common.internal.a.a(this.f > 0);
    }

    PlayerEntity(String str, String str2, Uri uri, Uri uri2, long j2, int i2, long j3, String str3, String str4, String str5, zzb zzb, PlayerLevelInfo playerLevelInfo, boolean z, boolean z2, String str6, String str7, Uri uri3, String str8, Uri uri4, String str9, int i3, long j4, boolean z3) {
        this.f1787b = str;
        this.c = str2;
        this.d = uri;
        this.i = str3;
        this.e = uri2;
        this.j = str4;
        this.f = j2;
        this.g = i2;
        this.h = j3;
        this.k = str5;
        this.n = z;
        this.l = zzb;
        this.m = playerLevelInfo;
        this.o = z2;
        this.p = str6;
        this.q = str7;
        this.r = uri3;
        this.s = str8;
        this.t = uri4;
        this.u = str9;
        this.v = i3;
        this.w = j4;
        this.x = z3;
    }

    static boolean a(Player player, Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        if (player == obj) {
            return true;
        }
        Player player2 = (Player) obj;
        return j.a(player2.b(), player.b()) && j.a(player2.c(), player.c()) && j.a(Boolean.valueOf(player2.f()), Boolean.valueOf(player.f())) && j.a(player2.g(), player.g()) && j.a(player2.h(), player.h()) && j.a(Long.valueOf(player2.i()), Long.valueOf(player.i())) && j.a(player2.m(), player.m()) && j.a(player2.n(), player.n()) && j.a(player2.d(), player.d()) && j.a(player2.e(), player.e()) && j.a(player2.p(), player.p()) && j.a(player2.q(), player.q()) && j.a(Integer.valueOf(player2.r()), Integer.valueOf(player.r())) && j.a(Long.valueOf(player2.s()), Long.valueOf(player.s())) && j.a(Boolean.valueOf(player2.t()), Boolean.valueOf(player.t()));
    }

    static /* synthetic */ boolean a(String str) {
        return true;
    }

    static String b(Player player) {
        return j.a(player).a("PlayerId", player.b()).a("DisplayName", player.c()).a("HasDebugAccess", Boolean.valueOf(player.f())).a("IconImageUri", player.g()).a("IconImageUrl", player.getIconImageUrl()).a("HiResImageUri", player.h()).a("HiResImageUrl", player.getHiResImageUrl()).a("RetrievedTimestamp", Long.valueOf(player.i())).a("Title", player.m()).a("LevelInfo", player.n()).a("GamerTag", player.d()).a("Name", player.e()).a("BannerImageLandscapeUri", player.p()).a("BannerImageLandscapeUrl", player.getBannerImageLandscapeUrl()).a("BannerImagePortraitUri", player.q()).a("BannerImagePortraitUrl", player.getBannerImagePortraitUrl()).a("GamerFriendStatus", Integer.valueOf(player.r())).a("GamerFriendUpdateTimestamp", Long.valueOf(player.s())).a("IsMuted", Boolean.valueOf(player.t())).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final String b() {
        return this.f1787b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.p;
    }

    public final String e() {
        return this.q;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final boolean f() {
        return this.o;
    }

    public final Uri g() {
        return this.d;
    }

    public final String getBannerImageLandscapeUrl() {
        return this.s;
    }

    public final String getBannerImagePortraitUrl() {
        return this.u;
    }

    public final String getHiResImageUrl() {
        return this.j;
    }

    public final String getIconImageUrl() {
        return this.i;
    }

    public final Uri h() {
        return this.e;
    }

    public final int hashCode() {
        return a(this);
    }

    public final long i() {
        return this.f;
    }

    public final long j() {
        return this.h;
    }

    public final int k() {
        return this.g;
    }

    public final boolean l() {
        return this.n;
    }

    public final String m() {
        return this.k;
    }

    public final PlayerLevelInfo n() {
        return this.m;
    }

    public final zza o() {
        return this.l;
    }

    public final Uri p() {
        return this.r;
    }

    public final Uri q() {
        return this.t;
    }

    public final int r() {
        return this.v;
    }

    public final long s() {
        return this.w;
    }

    public final boolean t() {
        return this.x;
    }

    public final String toString() {
        return b(this);
    }

    static int a(Player player) {
        return Arrays.hashCode(new Object[]{player.b(), player.c(), Boolean.valueOf(player.f()), player.g(), player.h(), Long.valueOf(player.i()), player.m(), player.n(), player.d(), player.e(), player.p(), player.q(), Integer.valueOf(player.r()), Long.valueOf(player.s()), Boolean.valueOf(player.t())});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.internal.player.zzb, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.PlayerLevelInfo, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        if (!this.f1575a) {
            int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 1, this.f1787b, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 2, this.c, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 3, (Parcelable) this.d, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 4, (Parcelable) this.e, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 5, this.f);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 6, this.g);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 7, this.h);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 8, getIconImageUrl(), false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 9, getHiResImageUrl(), false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 14, this.k, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 15, (Parcelable) this.l, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 16, (Parcelable) this.m, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 18, this.n);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 19, this.o);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20, this.p, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 21, this.q, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 22, (Parcelable) this.r, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 23, getBannerImageLandscapeUrl(), false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 24, (Parcelable) this.t, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 25, getBannerImagePortraitUrl(), false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 26, this.v);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 27, this.w);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 28, this.x);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
            return;
        }
        parcel.writeString(this.f1787b);
        parcel.writeString(this.c);
        Uri uri = this.d;
        String str = null;
        parcel.writeString(uri == null ? null : uri.toString());
        Uri uri2 = this.e;
        if (uri2 != null) {
            str = uri2.toString();
        }
        parcel.writeString(str);
        parcel.writeLong(this.f);
    }
}
