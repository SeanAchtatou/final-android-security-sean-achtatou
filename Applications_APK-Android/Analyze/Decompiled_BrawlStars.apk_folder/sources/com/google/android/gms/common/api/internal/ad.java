package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.d;

final class ad implements d.b, d.c {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ u f1388a;

    private ad(u uVar) {
        this.f1388a = uVar;
    }

    public final void onConnectionSuspended(int i) {
    }

    public final void onConnected(Bundle bundle) {
        this.f1388a.e.a(new ab(this.f1388a));
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.f1388a.f1497b.lock();
        try {
            if (this.f1388a.a(connectionResult)) {
                this.f1388a.f();
                this.f1388a.e();
            } else {
                this.f1388a.b(connectionResult);
            }
        } finally {
            this.f1388a.f1497b.unlock();
        }
    }

    /* synthetic */ ad(u uVar, byte b2) {
        this(uVar);
    }
}
