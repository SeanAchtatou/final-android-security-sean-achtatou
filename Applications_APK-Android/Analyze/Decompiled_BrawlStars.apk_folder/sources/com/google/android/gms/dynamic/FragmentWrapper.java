package com.google.android.gms.dynamic;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.google.android.gms.dynamic.IFragmentWrapper;

public final class FragmentWrapper extends IFragmentWrapper.Stub {

    /* renamed from: a  reason: collision with root package name */
    private Fragment f1774a;

    private static FragmentWrapper a(Fragment fragment) {
        if (fragment != null) {
            return new FragmentWrapper(fragment);
        }
        return null;
    }

    private FragmentWrapper(Fragment fragment) {
        this.f1774a = fragment;
    }

    public final IObjectWrapper a() {
        return ObjectWrapper.a(this.f1774a.getActivity());
    }

    public final Bundle b() {
        return this.f1774a.getArguments();
    }

    public final int c() {
        return this.f1774a.getId();
    }

    public final IFragmentWrapper d() {
        return a(this.f1774a.getParentFragment());
    }

    public final IObjectWrapper e() {
        return ObjectWrapper.a(this.f1774a.getResources());
    }

    public final boolean f() {
        return this.f1774a.getRetainInstance();
    }

    public final String g() {
        return this.f1774a.getTag();
    }

    public final IFragmentWrapper h() {
        return a(this.f1774a.getTargetFragment());
    }

    public final int i() {
        return this.f1774a.getTargetRequestCode();
    }

    public final boolean j() {
        return this.f1774a.getUserVisibleHint();
    }

    public final IObjectWrapper k() {
        return ObjectWrapper.a(this.f1774a.getView());
    }

    public final boolean l() {
        return this.f1774a.isAdded();
    }

    public final boolean m() {
        return this.f1774a.isDetached();
    }

    public final boolean n() {
        return this.f1774a.isHidden();
    }

    public final boolean o() {
        return this.f1774a.isInLayout();
    }

    public final boolean p() {
        return this.f1774a.isRemoving();
    }

    public final boolean q() {
        return this.f1774a.isResumed();
    }

    public final boolean r() {
        return this.f1774a.isVisible();
    }

    public final void a(IObjectWrapper iObjectWrapper) {
        this.f1774a.registerForContextMenu((View) ObjectWrapper.a(iObjectWrapper));
    }

    public final void a(boolean z) {
        this.f1774a.setHasOptionsMenu(z);
    }

    public final void b(boolean z) {
        this.f1774a.setMenuVisibility(z);
    }

    public final void c(boolean z) {
        this.f1774a.setRetainInstance(z);
    }

    public final void d(boolean z) {
        this.f1774a.setUserVisibleHint(z);
    }

    public final void a(Intent intent) {
        this.f1774a.startActivity(intent);
    }

    public final void a(Intent intent, int i) {
        this.f1774a.startActivityForResult(intent, i);
    }

    public final void b(IObjectWrapper iObjectWrapper) {
        this.f1774a.unregisterForContextMenu((View) ObjectWrapper.a(iObjectWrapper));
    }
}
