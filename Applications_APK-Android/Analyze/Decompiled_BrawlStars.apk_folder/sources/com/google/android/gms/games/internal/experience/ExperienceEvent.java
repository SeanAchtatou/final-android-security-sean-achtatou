package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Game;

public interface ExperienceEvent extends Parcelable, e<ExperienceEvent> {
    String b();

    Game c();

    String d();

    String e();

    Uri f();

    long g();

    @Deprecated
    String getIconImageUrl();

    long h();

    long i();

    int j();

    int k();
}
