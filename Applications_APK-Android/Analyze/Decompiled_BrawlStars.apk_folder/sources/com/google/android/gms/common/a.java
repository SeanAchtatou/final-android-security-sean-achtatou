package com.google.android.gms.common;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class a implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    public boolean f1359a = false;

    /* renamed from: b  reason: collision with root package name */
    public final BlockingQueue<IBinder> f1360b = new LinkedBlockingQueue();

    public void onServiceDisconnected(ComponentName componentName) {
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f1360b.add(iBinder);
    }
}
