package com.google.android.gms.internal.measurement;

import java.util.Arrays;

final class en implements ep {
    private en() {
    }

    public final byte[] a(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }

    /* synthetic */ en(byte b2) {
        this();
    }
}
