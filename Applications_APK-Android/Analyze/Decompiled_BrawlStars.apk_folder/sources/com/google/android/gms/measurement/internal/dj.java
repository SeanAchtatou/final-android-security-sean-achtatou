package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.util.e;
import com.google.android.gms.internal.measurement.cc;

public final class dj extends df {

    /* renamed from: a  reason: collision with root package name */
    long f2511a = l().b();

    /* renamed from: b  reason: collision with root package name */
    long f2512b = this.f2511a;
    final et c = new dk(this, this.r);
    final et d = new dl(this, this.r);
    final et e = new dm(this, this.r);
    private Handler f;

    dj(ar arVar) {
        super(arVar);
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public final void v() {
        synchronized (this) {
            if (this.f == null) {
                this.f = new cc(Looper.getMainLooper());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.dj.a(long, boolean):void
     arg types: [long, int]
     candidates:
      com.google.android.gms.measurement.internal.dj.a(boolean, boolean):boolean
      com.google.android.gms.measurement.internal.dj.a(long, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(long j) {
        c();
        v();
        a(j, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: package-private */
    public final void a(long j, boolean z) {
        c();
        v();
        this.c.c();
        this.d.c();
        if (s().h(f().v()) || s().i(f().v())) {
            this.e.c();
        }
        if (r().a(j)) {
            r().n.a(true);
            r().p.a(0);
        }
        if (r().n.a()) {
            b(j);
            return;
        }
        this.d.a(Math.max(0L, 3600000 - r().p.a()));
        if (z && s().k(f().v())) {
            r().o.a(j);
            if (s().h(f().v()) || s().i(f().v())) {
                this.e.c();
                this.e.a(r().m.a());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(long j) {
        c();
        q().k.a("Session started, time", Long.valueOf(l().b()));
        long j2 = null;
        Long valueOf = s().h(f().v()) ? Long.valueOf(j / 1000) : null;
        if (s().i(f().v())) {
            j2 = -1L;
        }
        long j3 = j;
        e().a("auto", "_sid", valueOf, j3);
        e().a("auto", "_sno", j2, j3);
        r().n.a(false);
        Bundle bundle = new Bundle();
        if (s().h(f().v())) {
            bundle.putLong("_sid", valueOf.longValue());
        }
        e().a("auto", "_s", j, bundle);
        r().o.a(j);
    }

    /* access modifiers changed from: package-private */
    public final long w() {
        long b2 = l().b();
        long j = b2 - this.f2512b;
        this.f2512b = b2;
        return j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.cg, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final boolean a(boolean z, boolean z2) {
        c();
        D();
        long b2 = l().b();
        r().o.a(l().a());
        long j = b2 - this.f2511a;
        if (z || j >= 1000) {
            r().p.a(j);
            q().k.a("Recording user engagement, ms", Long.valueOf(j));
            Bundle bundle = new Bundle();
            bundle.putLong("_et", j);
            ch.a(h().v(), bundle, true);
            if (s().l(f().v())) {
                if (s().c(f().v(), h.ao)) {
                    if (!z2) {
                        w();
                    }
                } else if (z2) {
                    bundle.putLong("_fr", 1);
                } else {
                    w();
                }
            }
            if (!s().c(f().v(), h.ao) || !z2) {
                e().a("auto", "_e", bundle);
            }
            this.f2511a = b2;
            this.d.c();
            this.d.a(Math.max(0L, 3600000 - r().p.a()));
            return true;
        }
        q().k.a("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j));
        return false;
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ a d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ bv e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ i f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ cl g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ ch h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ k i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ dj j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.dj.a(boolean, boolean):boolean
     arg types: [int, int]
     candidates:
      com.google.android.gms.measurement.internal.dj.a(long, boolean):void
      com.google.android.gms.measurement.internal.dj.a(boolean, boolean):boolean */
    static /* synthetic */ void a(dj djVar) {
        djVar.c();
        djVar.a(false, false);
        djVar.d().a(djVar.l().b());
    }
}
