package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.e;
import com.google.android.gms.common.api.f;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.api.i;
import com.google.android.gms.common.api.j;
import com.google.android.gms.common.api.k;
import com.google.android.gms.common.internal.l;
import java.lang.ref.WeakReference;

public final class bj<R extends g> extends k<R> implements h<R> {

    /* renamed from: a  reason: collision with root package name */
    j<? super R, ? extends g> f1422a;

    /* renamed from: b  reason: collision with root package name */
    bj<? extends g> f1423b;
    volatile i<? super R> c;
    e<R> d;
    final Object e;
    Status f;
    final WeakReference<d> g;
    final bl h;
    boolean i;

    public final void a(R r) {
        synchronized (this.e) {
            if (!r.b().a()) {
                a(r.b());
                b((g) r);
            } else if (this.f1422a != null) {
                be.a().submit(new bk(this, r));
            } else if (a()) {
                i<? super R> iVar = this.c;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Status status) {
        synchronized (this.e) {
            this.f = status;
            b(this.f);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(Status status) {
        synchronized (this.e) {
            if (this.f1422a != null) {
                l.a(status, "onFailure must not return null");
                this.f1423b.a(status);
            } else if (a()) {
                i<? super R> iVar = this.c;
            }
        }
    }

    private final boolean a() {
        return (this.c == null || this.g.get() == null) ? false : true;
    }

    static void b(g gVar) {
        if (gVar instanceof f) {
            try {
                ((f) gVar).a();
            } catch (RuntimeException unused) {
                String valueOf = String.valueOf(gVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                sb.toString();
            }
        }
    }
}
