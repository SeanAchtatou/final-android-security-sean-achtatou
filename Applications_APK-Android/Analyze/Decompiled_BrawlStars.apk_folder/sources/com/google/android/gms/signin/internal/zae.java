package com.google.android.gms.signin.internal;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.base.a;
import com.google.android.gms.internal.base.zab;

public abstract class zae extends zab implements zad {
    public zae() {
        super("com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    public final boolean a(int i, Parcel parcel, Parcel parcel2) throws RemoteException {
        if (i == 3) {
            a.a(parcel, ConnectionResult.CREATOR);
            a.a(parcel, zaa.CREATOR);
        } else if (i == 4) {
            a.a(parcel, Status.CREATOR);
        } else if (i == 6) {
            a.a(parcel, Status.CREATOR);
        } else if (i == 7) {
            a.a(parcel, Status.CREATOR);
            a.a(parcel, GoogleSignInAccount.CREATOR);
        } else if (i != 8) {
            return false;
        } else {
            a((zaj) a.a(parcel, zaj.CREATOR));
        }
        parcel2.writeNoException();
        return true;
    }
}
