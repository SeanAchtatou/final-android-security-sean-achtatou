package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.eb;
import com.google.android.gms.internal.measurement.ec;

public abstract class ec<MessageType extends eb<MessageType, BuilderType>, BuilderType extends ec<MessageType, BuilderType>> implements gu {
    /* renamed from: a */
    public abstract BuilderType clone();

    /* access modifiers changed from: protected */
    public abstract BuilderType a(eb ebVar);

    public final /* synthetic */ gu a(gt gtVar) {
        if (k().getClass().isInstance(gtVar)) {
            return a((eb) gtVar);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
