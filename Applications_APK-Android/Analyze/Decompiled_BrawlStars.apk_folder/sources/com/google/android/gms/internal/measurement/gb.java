package com.google.android.gms.internal.measurement;

public class gb {
    private static final fd d = fd.a();

    /* renamed from: a  reason: collision with root package name */
    ej f2234a;

    /* renamed from: b  reason: collision with root package name */
    volatile gt f2235b;
    volatile ej c;

    public int hashCode() {
        return 1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof gb)) {
            return false;
        }
        gb gbVar = (gb) obj;
        gt gtVar = this.f2235b;
        gt gtVar2 = gbVar.f2235b;
        if (gtVar == null && gtVar2 == null) {
            return c().equals(gbVar.c());
        }
        if (gtVar != null && gtVar2 != null) {
            return gtVar.equals(gtVar2);
        }
        if (gtVar != null) {
            return gtVar.equals(gbVar.a(gtVar.k()));
        }
        return a(gtVar2.k()).equals(gtVar2);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.internal.measurement.gt a(com.google.android.gms.internal.measurement.gt r2) {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.gt r0 = r1.f2235b
            if (r0 != 0) goto L_0x001d
            monitor-enter(r1)
            com.google.android.gms.internal.measurement.gt r0 = r1.f2235b     // Catch:{ all -> 0x001a }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x000b:
            r1.f2235b = r2     // Catch:{ zzuv -> 0x0012 }
            com.google.android.gms.internal.measurement.ej r0 = com.google.android.gms.internal.measurement.ej.f2184a     // Catch:{ zzuv -> 0x0012 }
            r1.c = r0     // Catch:{ zzuv -> 0x0012 }
            goto L_0x0018
        L_0x0012:
            r1.f2235b = r2     // Catch:{ all -> 0x001a }
            com.google.android.gms.internal.measurement.ej r2 = com.google.android.gms.internal.measurement.ej.f2184a     // Catch:{ all -> 0x001a }
            r1.c = r2     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x001a:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            throw r2
        L_0x001d:
            com.google.android.gms.internal.measurement.gt r2 = r1.f2235b
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.gb.a(com.google.android.gms.internal.measurement.gt):com.google.android.gms.internal.measurement.gt");
    }

    public final int b() {
        if (this.c != null) {
            return this.c.a();
        }
        if (this.f2235b != null) {
            return this.f2235b.h();
        }
        return 0;
    }

    public final ej c() {
        if (this.c != null) {
            return this.c;
        }
        synchronized (this) {
            if (this.c != null) {
                ej ejVar = this.c;
                return ejVar;
            }
            if (this.f2235b == null) {
                this.c = ej.f2184a;
            } else {
                this.c = this.f2235b.d();
            }
            ej ejVar2 = this.c;
            return ejVar2;
        }
    }
}
