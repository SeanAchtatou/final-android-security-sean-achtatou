package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fq;

final class hh implements gr {

    /* renamed from: a  reason: collision with root package name */
    final gt f2261a;

    /* renamed from: b  reason: collision with root package name */
    final String f2262b;
    final Object[] c;
    private final int d;

    hh(gt gtVar, String str, Object[] objArr) {
        this.f2261a = gtVar;
        this.f2262b = str;
        this.c = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.d = charAt;
            return;
        }
        char c2 = charAt & 8191;
        int i = 13;
        int i2 = 1;
        while (true) {
            int i3 = i2 + 1;
            char charAt2 = str.charAt(i2);
            if (charAt2 >= 55296) {
                c2 |= (charAt2 & 8191) << i;
                i += 13;
                i2 = i3;
            } else {
                this.d = c2 | (charAt2 << i);
                return;
            }
        }
    }

    public final gt c() {
        return this.f2261a;
    }

    public final int a() {
        return (this.d & 1) == 1 ? fq.e.h : fq.e.i;
    }

    public final boolean b() {
        return (this.d & 2) == 2;
    }
}
