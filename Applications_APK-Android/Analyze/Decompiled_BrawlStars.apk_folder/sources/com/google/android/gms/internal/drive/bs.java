package com.google.android.gms.internal.drive;

import com.google.android.gms.internal.drive.bs;
import java.io.IOException;

public abstract class bs<M extends bs<M>> extends bx {
    protected bu f;

    public final /* synthetic */ bx b() throws CloneNotSupportedException {
        return (bs) clone();
    }

    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        bs bsVar = (bs) super.clone();
        bw.a(this, bsVar);
        return bsVar;
    }

    /* access modifiers changed from: protected */
    public int a() {
        if (this.f == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.f.f1904b; i2++) {
            i += this.f.f1903a[i2].a();
        }
        return i;
    }

    public void a(br brVar) throws IOException {
        if (this.f != null) {
            for (int i = 0; i < this.f.f1904b; i++) {
                this.f.f1903a[i].a(brVar);
            }
        }
    }
}
