package com.google.android.gms.internal.a;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static final a f1864a;

    /* renamed from: b  reason: collision with root package name */
    private static volatile a f1865b;

    public static a a() {
        return f1865b;
    }

    static {
        c cVar = new c((byte) 0);
        f1864a = cVar;
        f1865b = cVar;
    }
}
