package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import java.util.Arrays;

public final class ParticipantEntity extends GamesDowngradeableSafeParcel implements Participant {
    public static final Parcelable.Creator<ParticipantEntity> CREATOR = new a();

    /* renamed from: b  reason: collision with root package name */
    private final String f1827b;
    private final String c;
    private final Uri d;
    private final Uri e;
    private final int f;
    private final String g;
    private final boolean h;
    private final PlayerEntity i;
    private final int j;
    private final ParticipantResult k;
    private final String l;
    private final String m;

    static final class a extends e {
        a() {
        }

        public final ParticipantEntity a(Parcel parcel) {
            if (ParticipantEntity.b(ParticipantEntity.b_()) || ParticipantEntity.a(ParticipantEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            Uri parse = readString3 == null ? null : Uri.parse(readString3);
            String readString4 = parcel.readString();
            Uri parse2 = readString4 == null ? null : Uri.parse(readString4);
            int readInt = parcel.readInt();
            String readString5 = parcel.readString();
            boolean z = true;
            boolean z2 = parcel.readInt() > 0;
            if (parcel.readInt() <= 0) {
                z = false;
            }
            return new ParticipantEntity(readString, readString2, parse, parse2, readInt, readString5, z2, z ? PlayerEntity.CREATOR.createFromParcel(parcel) : null, 7, null, null, null);
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }
    }

    public ParticipantEntity(Participant participant) {
        this.f1827b = participant.i();
        this.c = participant.f();
        this.d = participant.g();
        this.e = participant.h();
        this.f = participant.b();
        this.g = participant.c();
        this.h = participant.e();
        Player j2 = participant.j();
        this.i = j2 == null ? null : new PlayerEntity(j2);
        this.j = participant.d();
        this.k = participant.k();
        this.l = participant.getIconImageUrl();
        this.m = participant.getHiResImageUrl();
    }

    ParticipantEntity(String str, String str2, Uri uri, Uri uri2, int i2, String str3, boolean z, PlayerEntity playerEntity, int i3, ParticipantResult participantResult, String str4, String str5) {
        this.f1827b = str;
        this.c = str2;
        this.d = uri;
        this.e = uri2;
        this.f = i2;
        this.g = str3;
        this.h = z;
        this.i = playerEntity;
        this.j = i3;
        this.k = participantResult;
        this.l = str4;
        this.m = str5;
    }

    static boolean a(Participant participant, Object obj) {
        if (!(obj instanceof Participant)) {
            return false;
        }
        if (participant == obj) {
            return true;
        }
        Participant participant2 = (Participant) obj;
        return j.a(participant2.j(), participant.j()) && j.a(Integer.valueOf(participant2.b()), Integer.valueOf(participant.b())) && j.a(participant2.c(), participant.c()) && j.a(Boolean.valueOf(participant2.e()), Boolean.valueOf(participant.e())) && j.a(participant2.f(), participant.f()) && j.a(participant2.g(), participant.g()) && j.a(participant2.h(), participant.h()) && j.a(Integer.valueOf(participant2.d()), Integer.valueOf(participant.d())) && j.a(participant2.k(), participant.k()) && j.a(participant2.i(), participant.i());
    }

    static /* synthetic */ boolean a(String str) {
        return true;
    }

    static String b(Participant participant) {
        return j.a(participant).a("ParticipantId", participant.i()).a("Player", participant.j()).a("Status", Integer.valueOf(participant.b())).a("ClientAddress", participant.c()).a("ConnectedToRoom", Boolean.valueOf(participant.e())).a("DisplayName", participant.f()).a("IconImage", participant.g()).a("IconImageUrl", participant.getIconImageUrl()).a("HiResImage", participant.h()).a("HiResImageUrl", participant.getHiResImageUrl()).a("Capabilities", Integer.valueOf(participant.d())).a("Result", participant.k()).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final int b() {
        return this.f;
    }

    public final String c() {
        return this.g;
    }

    public final int d() {
        return this.j;
    }

    public final boolean e() {
        return this.h;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final String f() {
        PlayerEntity playerEntity = this.i;
        return playerEntity == null ? this.c : playerEntity.c();
    }

    public final Uri g() {
        PlayerEntity playerEntity = this.i;
        return playerEntity == null ? this.d : playerEntity.g();
    }

    public final String getHiResImageUrl() {
        PlayerEntity playerEntity = this.i;
        return playerEntity == null ? this.m : playerEntity.getHiResImageUrl();
    }

    public final String getIconImageUrl() {
        PlayerEntity playerEntity = this.i;
        return playerEntity == null ? this.l : playerEntity.getIconImageUrl();
    }

    public final Uri h() {
        PlayerEntity playerEntity = this.i;
        return playerEntity == null ? this.e : playerEntity.h();
    }

    public final int hashCode() {
        return a(this);
    }

    public final String i() {
        return this.f1827b;
    }

    public final Player j() {
        return this.i;
    }

    public final ParticipantResult k() {
        return this.k;
    }

    public final String toString() {
        return b(this);
    }

    static int a(Participant participant) {
        return Arrays.hashCode(new Object[]{participant.j(), Integer.valueOf(participant.b()), participant.c(), Boolean.valueOf(participant.e()), participant.f(), participant.g(), participant.h(), Integer.valueOf(participant.d()), participant.k(), participant.i()});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.PlayerEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.multiplayer.ParticipantResult, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int i3 = 1;
        if (!this.f1575a) {
            int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 1, this.f1827b, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 2, f(), false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 3, (Parcelable) g(), i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 4, (Parcelable) h(), i2, false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 5, this.f);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 6, this.g, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 7, this.h);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 8, (Parcelable) this.i, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 9, this.j);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 10, (Parcelable) this.k, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 11, getIconImageUrl(), false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 12, getHiResImageUrl(), false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
            return;
        }
        parcel.writeString(this.f1827b);
        parcel.writeString(this.c);
        Uri uri = this.d;
        String str = null;
        parcel.writeString(uri == null ? null : uri.toString());
        Uri uri2 = this.e;
        if (uri2 != null) {
            str = uri2.toString();
        }
        parcel.writeString(str);
        parcel.writeInt(this.f);
        parcel.writeString(this.g);
        parcel.writeInt(this.h ? 1 : 0);
        if (this.i == null) {
            i3 = 0;
        }
        parcel.writeInt(i3);
        PlayerEntity playerEntity = this.i;
        if (playerEntity != null) {
            playerEntity.writeToParcel(parcel, i2);
        }
    }
}
