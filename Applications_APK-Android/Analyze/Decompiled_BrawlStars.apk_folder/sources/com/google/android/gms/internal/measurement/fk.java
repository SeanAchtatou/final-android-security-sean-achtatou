package com.google.android.gms.internal.measurement;

import java.lang.reflect.Type;

public enum fk {
    DOUBLE(0, fm.SCALAR, fx.DOUBLE),
    FLOAT(1, fm.SCALAR, fx.FLOAT),
    INT64(2, fm.SCALAR, fx.LONG),
    UINT64(3, fm.SCALAR, fx.LONG),
    INT32(4, fm.SCALAR, fx.INT),
    FIXED64(5, fm.SCALAR, fx.LONG),
    FIXED32(6, fm.SCALAR, fx.INT),
    BOOL(7, fm.SCALAR, fx.BOOLEAN),
    STRING(8, fm.SCALAR, fx.STRING),
    MESSAGE(9, fm.SCALAR, fx.MESSAGE),
    BYTES(10, fm.SCALAR, fx.BYTE_STRING),
    UINT32(11, fm.SCALAR, fx.INT),
    ENUM(12, fm.SCALAR, fx.ENUM),
    SFIXED32(13, fm.SCALAR, fx.INT),
    SFIXED64(14, fm.SCALAR, fx.LONG),
    SINT32(15, fm.SCALAR, fx.INT),
    SINT64(16, fm.SCALAR, fx.LONG),
    GROUP(17, fm.SCALAR, fx.MESSAGE),
    DOUBLE_LIST(18, fm.VECTOR, fx.DOUBLE),
    FLOAT_LIST(19, fm.VECTOR, fx.FLOAT),
    INT64_LIST(20, fm.VECTOR, fx.LONG),
    UINT64_LIST(21, fm.VECTOR, fx.LONG),
    INT32_LIST(22, fm.VECTOR, fx.INT),
    FIXED64_LIST(23, fm.VECTOR, fx.LONG),
    FIXED32_LIST(24, fm.VECTOR, fx.INT),
    BOOL_LIST(25, fm.VECTOR, fx.BOOLEAN),
    STRING_LIST(26, fm.VECTOR, fx.STRING),
    MESSAGE_LIST(27, fm.VECTOR, fx.MESSAGE),
    BYTES_LIST(28, fm.VECTOR, fx.BYTE_STRING),
    UINT32_LIST(29, fm.VECTOR, fx.INT),
    ENUM_LIST(30, fm.VECTOR, fx.ENUM),
    SFIXED32_LIST(31, fm.VECTOR, fx.INT),
    SFIXED64_LIST(32, fm.VECTOR, fx.LONG),
    SINT32_LIST(33, fm.VECTOR, fx.INT),
    SINT64_LIST(34, fm.VECTOR, fx.LONG),
    DOUBLE_LIST_PACKED(35, fm.PACKED_VECTOR, fx.DOUBLE),
    FLOAT_LIST_PACKED(36, fm.PACKED_VECTOR, fx.FLOAT),
    INT64_LIST_PACKED(37, fm.PACKED_VECTOR, fx.LONG),
    UINT64_LIST_PACKED(38, fm.PACKED_VECTOR, fx.LONG),
    INT32_LIST_PACKED(39, fm.PACKED_VECTOR, fx.INT),
    FIXED64_LIST_PACKED(40, fm.PACKED_VECTOR, fx.LONG),
    FIXED32_LIST_PACKED(41, fm.PACKED_VECTOR, fx.INT),
    BOOL_LIST_PACKED(42, fm.PACKED_VECTOR, fx.BOOLEAN),
    UINT32_LIST_PACKED(43, fm.PACKED_VECTOR, fx.INT),
    ENUM_LIST_PACKED(44, fm.PACKED_VECTOR, fx.ENUM),
    SFIXED32_LIST_PACKED(45, fm.PACKED_VECTOR, fx.INT),
    SFIXED64_LIST_PACKED(46, fm.PACKED_VECTOR, fx.LONG),
    SINT32_LIST_PACKED(47, fm.PACKED_VECTOR, fx.INT),
    SINT64_LIST_PACKED(48, fm.PACKED_VECTOR, fx.LONG),
    GROUP_LIST(49, fm.VECTOR, fx.MESSAGE),
    MAP(50, fm.MAP, fx.VOID);
    
    private static final fk[] ae;
    private static final Type[] af = new Type[0];
    private final fx aa;
    private final fm ab;
    private final Class<?> ac;
    private final boolean ad;
    final int c;

    private fk(int i, fm fmVar, fx fxVar) {
        int i2;
        this.c = i;
        this.ab = fmVar;
        this.aa = fxVar;
        int i3 = fl.f2212a[fmVar.ordinal()];
        if (i3 == 1) {
            this.ac = fxVar.k;
        } else if (i3 != 2) {
            this.ac = null;
        } else {
            this.ac = fxVar.k;
        }
        boolean z = false;
        if (!(fmVar != fm.SCALAR || (i2 = fl.f2213b[fxVar.ordinal()]) == 1 || i2 == 2 || i2 == 3)) {
            z = true;
        }
        this.ad = z;
    }

    static {
        fk[] values = values();
        ae = new fk[values.length];
        for (fk fkVar : values) {
            ae[fkVar.c] = fkVar;
        }
    }
}
