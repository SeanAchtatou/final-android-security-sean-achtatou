package com.google.android.gms.common.util.a;

import com.google.android.gms.common.internal.l;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class c implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final String f1669a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1670b;
    private final AtomicInteger c;
    private final ThreadFactory d;

    public c(String str) {
        this(str, (byte) 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T */
    private c(String str, byte b2) {
        this.c = new AtomicInteger();
        this.d = Executors.defaultThreadFactory();
        this.f1669a = (String) l.a((Object) str, (Object) "Name must not be null");
        this.f1670b = 0;
    }

    public Thread newThread(Runnable runnable) {
        Thread newThread = this.d.newThread(new d(runnable, 0));
        String str = this.f1669a;
        int andIncrement = this.c.getAndIncrement();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 13);
        sb.append(str);
        sb.append("[");
        sb.append(andIncrement);
        sb.append("]");
        newThread.setName(sb.toString());
        return newThread;
    }
}
