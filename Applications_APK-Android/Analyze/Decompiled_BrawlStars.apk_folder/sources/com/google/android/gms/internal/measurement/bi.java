package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.util.e;

public final class bi {

    /* renamed from: a  reason: collision with root package name */
    private final long f2075a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2076b;
    private double c;
    private long d;
    private final Object e;
    private final String f;
    private final e g;

    private bi(String str, e eVar) {
        this.e = new Object();
        this.f2076b = 60;
        this.c = (double) this.f2076b;
        this.f2075a = 2000;
        this.f = str;
        this.g = eVar;
    }

    public bi(String str, e eVar, byte b2) {
        this(str, eVar);
    }

    public final boolean a() {
        synchronized (this.e) {
            long a2 = this.g.a();
            if (this.c < ((double) this.f2076b)) {
                double d2 = (double) (a2 - this.d);
                double d3 = (double) this.f2075a;
                Double.isNaN(d2);
                Double.isNaN(d3);
                double d4 = d2 / d3;
                if (d4 > 0.0d) {
                    this.c = Math.min((double) this.f2076b, this.c + d4);
                }
            }
            this.d = a2;
            if (this.c >= 1.0d) {
                this.c -= 1.0d;
                return true;
            }
            String str = this.f;
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 34);
            sb.append("Excessive ");
            sb.append(str);
            sb.append(" detected; call ignored.");
            bj.a(sb.toString());
            return false;
        }
    }
}
