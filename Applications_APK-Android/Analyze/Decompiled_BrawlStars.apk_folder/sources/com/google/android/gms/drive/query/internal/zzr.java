package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import java.util.ArrayList;
import java.util.List;

public final class zzr extends zza {
    public static final Parcelable.Creator<zzr> CREATOR = new k();

    /* renamed from: a  reason: collision with root package name */
    private final zzx f1762a;

    /* renamed from: b  reason: collision with root package name */
    private final List<FilterHolder> f1763b;

    zzr(zzx zzx, List<FilterHolder> list) {
        this.f1762a = zzx;
        this.f1763b = list;
    }

    public final <T> T a(g<T> gVar) {
        ArrayList arrayList = new ArrayList();
        for (FilterHolder filterHolder : this.f1763b) {
            arrayList.add(filterHolder.f1749a.a(gVar));
        }
        return gVar.a(this.f1762a, arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzx, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, (Parcelable) this.f1762a, i, false);
        a.b(parcel, 2, this.f1763b, false);
        a.b(parcel, a2);
    }
}
