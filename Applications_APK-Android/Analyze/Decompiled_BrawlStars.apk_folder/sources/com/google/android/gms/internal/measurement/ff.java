package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fd;
import com.google.android.gms.internal.measurement.fq;
import java.io.IOException;
import java.util.Map;

final class ff extends fe<Object> {
    ff() {
    }

    /* access modifiers changed from: package-private */
    public final boolean a(gt gtVar) {
        return gtVar instanceof fq.c;
    }

    /* access modifiers changed from: package-private */
    public final fh<Object> a(Object obj) {
        return ((fq.c) obj).zzbyj;
    }

    /* access modifiers changed from: package-private */
    public final fh<Object> b(Object obj) {
        fq.c cVar = (fq.c) obj;
        if (cVar.zzbyj.f2207b) {
            cVar.zzbyj = (fh) cVar.zzbyj.clone();
        }
        return cVar.zzbyj;
    }

    /* access modifiers changed from: package-private */
    public final void c(Object obj) {
        a(obj).b();
    }

    /* access modifiers changed from: package-private */
    public final <UT, UB> UB a() throws IOException {
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final int a(Map.Entry<?, ?> entry) {
        entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final void b(Map.Entry<?, ?> entry) throws IOException {
        entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final void b() throws IOException {
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final void c() throws IOException {
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final Object a(fd fdVar, gt gtVar, int i) {
        return fdVar.f2201b.get(new fd.a(gtVar, i));
    }
}
