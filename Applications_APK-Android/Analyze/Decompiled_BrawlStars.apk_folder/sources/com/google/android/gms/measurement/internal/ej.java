package com.google.android.gms.measurement.internal;

import android.os.Looper;

public final class ej {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f2549a = false;

    ej() {
    }

    public static boolean a() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
