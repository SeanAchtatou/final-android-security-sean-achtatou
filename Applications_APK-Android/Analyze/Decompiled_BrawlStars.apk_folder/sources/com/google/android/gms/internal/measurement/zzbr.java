package com.google.android.gms.internal.measurement;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbr implements Parcelable {
    @Deprecated
    public static final Parcelable.Creator<zzbr> CREATOR = new ap();

    /* renamed from: a  reason: collision with root package name */
    String f2344a;

    /* renamed from: b  reason: collision with root package name */
    String f2345b;
    private String c;

    @Deprecated
    public final int describeContents() {
        return 0;
    }

    @Deprecated
    public zzbr() {
    }

    @Deprecated
    zzbr(Parcel parcel) {
        this.f2344a = parcel.readString();
        this.c = parcel.readString();
        this.f2345b = parcel.readString();
    }

    @Deprecated
    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2344a);
        parcel.writeString(this.c);
        parcel.writeString(this.f2345b);
    }
}
