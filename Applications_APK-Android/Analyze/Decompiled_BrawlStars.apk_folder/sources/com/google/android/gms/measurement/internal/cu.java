package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

final class cu implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AtomicReference f2485a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2486b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ zzk e;
    private final /* synthetic */ cl f;

    cu(cl clVar, AtomicReference atomicReference, String str, String str2, String str3, zzk zzk) {
        this.f = clVar;
        this.f2485a = atomicReference;
        this.f2486b = str;
        this.c = str2;
        this.d = str3;
        this.e = zzk;
    }

    public final void run() {
        synchronized (this.f2485a) {
            try {
                zzaj zzaj = this.f.f2470b;
                if (zzaj == null) {
                    this.f.q().c.a("Failed to get conditional properties", o.a(this.f2486b), this.c, this.d);
                    this.f2485a.set(Collections.emptyList());
                    this.f2485a.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.f2486b)) {
                    this.f2485a.set(zzaj.a(this.c, this.d, this.e));
                } else {
                    this.f2485a.set(zzaj.a(this.f2486b, this.c, this.d));
                }
                this.f.y();
                this.f2485a.notify();
            } catch (RemoteException e2) {
                try {
                    this.f.q().c.a("Failed to get conditional properties", o.a(this.f2486b), this.c, e2);
                    this.f2485a.set(Collections.emptyList());
                    this.f2485a.notify();
                } catch (Throwable th) {
                    this.f2485a.notify();
                    throw th;
                }
            }
        }
    }
}
