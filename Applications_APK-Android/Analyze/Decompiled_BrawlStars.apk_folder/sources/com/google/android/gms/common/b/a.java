package com.google.android.gms.common.b;

import java.util.concurrent.ScheduledExecutorService;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static C0114a f1512a;

    /* renamed from: com.google.android.gms.common.b.a$a  reason: collision with other inner class name */
    public interface C0114a {
        ScheduledExecutorService a();
    }

    public static synchronized C0114a a() {
        C0114a aVar;
        synchronized (a.class) {
            if (f1512a == null) {
                f1512a = new b();
            }
            aVar = f1512a;
        }
        return aVar;
    }
}
