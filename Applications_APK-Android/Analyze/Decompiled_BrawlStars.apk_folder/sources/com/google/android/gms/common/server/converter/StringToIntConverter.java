package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.HashMap;

public final class StringToIntConverter extends AbstractSafeParcelable implements FastJsonResponse.a<String, Integer> {
    public static final Parcelable.Creator<StringToIntConverter> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final int f1637a;

    /* renamed from: b  reason: collision with root package name */
    private final HashMap<String, Integer> f1638b;
    private final SparseArray<String> c;
    private final ArrayList<zaa> d;

    StringToIntConverter(int i, ArrayList<zaa> arrayList) {
        this.f1637a = i;
        this.f1638b = new HashMap<>();
        this.c = new SparseArray<>();
        this.d = null;
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i2 = 0;
        while (i2 < size) {
            Object obj = arrayList2.get(i2);
            i2++;
            zaa zaa2 = (zaa) obj;
            String str = zaa2.f1639a;
            int i3 = zaa2.f1640b;
            this.f1638b.put(str, Integer.valueOf(i3));
            this.c.put(i3, str);
        }
    }

    public static final class zaa extends AbstractSafeParcelable {
        public static final Parcelable.Creator<zaa> CREATOR = new c();

        /* renamed from: a  reason: collision with root package name */
        final String f1639a;

        /* renamed from: b  reason: collision with root package name */
        final int f1640b;
        private final int c;

        zaa(int i, String str, int i2) {
            this.c = i;
            this.f1639a = str;
            this.f1640b = i2;
        }

        zaa(String str, int i) {
            this.c = 1;
            this.f1639a = str;
            this.f1640b = i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
         arg types: [android.os.Parcel, int, java.lang.String, int]
         candidates:
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
          com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
        public final void writeToParcel(Parcel parcel, int i) {
            int a2 = a.a(parcel, 20293);
            a.b(parcel, 1, this.c);
            a.a(parcel, 2, this.f1639a, false);
            a.b(parcel, 3, this.f1640b);
            a.b(parcel, a2);
        }
    }

    public StringToIntConverter() {
        this.f1637a = 1;
        this.f1638b = new HashMap<>();
        this.c = new SparseArray<>();
        this.d = null;
    }

    public final /* synthetic */ Object a(Object obj) {
        String str = this.c.get(((Integer) obj).intValue());
        return (str != null || !this.f1638b.containsKey("gms_unknown")) ? str : "gms_unknown";
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.f1637a);
        ArrayList arrayList = new ArrayList();
        for (String next : this.f1638b.keySet()) {
            arrayList.add(new zaa(next, this.f1638b.get(next).intValue()));
        }
        a.b(parcel, 2, arrayList, false);
        a.b(parcel, a2);
    }
}
