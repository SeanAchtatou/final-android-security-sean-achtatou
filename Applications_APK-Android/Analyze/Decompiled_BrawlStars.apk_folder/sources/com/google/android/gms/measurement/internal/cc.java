package com.google.android.gms.measurement.internal;

final class cc implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ boolean f2458a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ bv f2459b;

    cc(bv bvVar, boolean z) {
        this.f2459b = bvVar;
        this.f2458a = z;
    }

    public final void run() {
        boolean r = this.f2459b.r.r();
        boolean o = this.f2459b.r.o();
        this.f2459b.r.a(this.f2458a);
        if (o == this.f2458a) {
            this.f2459b.r.q().k.a("Default data collection state already set to", Boolean.valueOf(this.f2458a));
        }
        if (this.f2459b.r.r() == r || this.f2459b.r.r() != this.f2459b.r.o()) {
            this.f2459b.r.q().h.a("Default data collection is different than actual status", Boolean.valueOf(this.f2458a), Boolean.valueOf(r));
        }
        bv.a(this.f2459b);
    }
}
