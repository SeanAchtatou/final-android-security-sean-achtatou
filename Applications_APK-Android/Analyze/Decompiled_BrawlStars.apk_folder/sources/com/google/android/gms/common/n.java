package com.google.android.gms.common;

import java.util.Arrays;

final class n extends m {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f1631a;

    n(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.f1631a = bArr;
    }

    /* access modifiers changed from: package-private */
    public final byte[] c() {
        return this.f1631a;
    }
}
