package com.google.android.gms.internal.measurement;

import java.util.Arrays;

final class jg {

    /* renamed from: a  reason: collision with root package name */
    final int f2313a;

    /* renamed from: b  reason: collision with root package name */
    final byte[] f2314b;

    jg(int i, byte[] bArr) {
        this.f2313a = i;
        this.f2314b = bArr;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof jg)) {
            return false;
        }
        jg jgVar = (jg) obj;
        return this.f2313a == jgVar.f2313a && Arrays.equals(this.f2314b, jgVar.f2314b);
    }

    public final int hashCode() {
        return ((this.f2313a + 527) * 31) + Arrays.hashCode(this.f2314b);
    }
}
