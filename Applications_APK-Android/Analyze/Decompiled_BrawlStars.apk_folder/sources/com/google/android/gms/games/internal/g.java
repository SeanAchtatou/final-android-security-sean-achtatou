package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.common.internal.BaseGmsClient;

final class g implements c.b<Status> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ BaseGmsClient.e f1810a;

    g(BaseGmsClient.e eVar) {
        this.f1810a = eVar;
    }

    public final void a(Status status) {
        this.f1810a.a();
    }

    public final /* synthetic */ void a(Object obj) {
        this.f1810a.a();
    }
}
