package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.common.d;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.signin.e;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public final class cg implements az {

    /* renamed from: a  reason: collision with root package name */
    final Map<a.c<?>, cf<?>> f1455a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    final Map<a.c<?>, cf<?>> f1456b = new HashMap();
    final ag c;
    final Lock d;
    final Condition e;
    final boolean f;
    boolean g;
    Map<bs<?>, ConnectionResult> h;
    Map<bs<?>, ConnectionResult> i;
    ConnectionResult j;
    private final Map<a<?>, Boolean> k;
    private final d l;
    private final Looper m;
    private final d n;
    private final b o;
    private final boolean p;
    private final Queue<c.a<?, ?>> q = new LinkedList();
    private l r;

    public cg(Context context, Lock lock, Looper looper, d dVar, Map<a.c<?>, a.f> map, b bVar, Map<a<?>, Boolean> map2, a.C0111a<? extends e, com.google.android.gms.signin.a> aVar, ArrayList<ca> arrayList, ag agVar, boolean z) {
        this.d = lock;
        this.m = looper;
        this.e = lock.newCondition();
        this.n = dVar;
        this.c = agVar;
        this.k = map2;
        this.o = bVar;
        this.p = z;
        HashMap hashMap = new HashMap();
        for (a next : map2.keySet()) {
            hashMap.put(next.b(), next);
        }
        HashMap hashMap2 = new HashMap();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i2 = 0;
        while (i2 < size) {
            Object obj = arrayList2.get(i2);
            i2++;
            ca caVar = (ca) obj;
            hashMap2.put(caVar.f1449a, caVar);
        }
        for (Map.Entry next2 : map.entrySet()) {
            a aVar2 = (a) hashMap.get(next2.getKey());
            a.f fVar = (a.f) next2.getValue();
            this.k.get(aVar2).booleanValue();
            cf cfVar = r1;
            cf cfVar2 = new cf(context, aVar2, looper, fVar, (ca) hashMap2.get(aVar2), bVar, aVar);
            this.f1455a.put((a.c) next2.getKey(), cfVar);
            if (fVar.d()) {
                this.f1456b.put((a.c) next2.getKey(), cfVar);
            }
        }
        this.f = false;
        this.l = d.a();
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public final void f() {
    }

    public final void a() {
        this.d.lock();
        try {
            if (!this.g) {
                this.g = true;
                this.h = null;
                this.i = null;
                this.r = null;
                this.j = null;
                this.l.b();
                d dVar = this.l;
                bu buVar = new bu(this.f1455a.values());
                dVar.i.sendMessage(dVar.i.obtainMessage(2, buVar));
                buVar.f1439b.f2678a.a(new com.google.android.gms.common.util.a.a(this.m), new ch(this, (byte) 0));
                this.d.unlock();
            }
        } finally {
            this.d.unlock();
        }
    }

    public final void b() {
        this.d.lock();
        try {
            this.g = false;
            this.h = null;
            this.i = null;
            if (this.r != null) {
                this.r = null;
            }
            this.j = null;
            while (!this.q.isEmpty()) {
                c.a remove = this.q.remove();
                remove.a((bo) null);
                remove.b();
            }
            this.e.signalAll();
        } finally {
            this.d.unlock();
        }
    }

    private final ConnectionResult a(a.c<?> cVar) {
        this.d.lock();
        try {
            cf cfVar = this.f1455a.get(cVar);
            if (this.h != null && cfVar != null) {
                return this.h.get(cfVar.f1374b);
            }
            this.d.unlock();
            return null;
        } finally {
            this.d.unlock();
        }
    }

    public final boolean d() {
        this.d.lock();
        try {
            return this.h != null && this.j == null;
        } finally {
            this.d.unlock();
        }
    }

    public final boolean e() {
        this.d.lock();
        try {
            return this.h == null && this.g;
        } finally {
            this.d.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        b bVar = this.o;
        if (bVar == null) {
            this.c.c = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(bVar.f1596b);
        Map<a<?>, b.C0115b> map = this.o.d;
        for (a next : map.keySet()) {
            ConnectionResult a2 = a(next.b());
            if (a2 != null && a2.b()) {
                hashSet.addAll(map.get(next).f1599a);
            }
        }
        this.c.c = hashSet;
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        while (!this.q.isEmpty()) {
            a(this.q.remove());
        }
        this.c.a((Bundle) null);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(cf<?> cfVar, ConnectionResult connectionResult) {
        return !connectionResult.b() && !connectionResult.a() && this.k.get(cfVar.f1373a).booleanValue() && this.n.a(connectionResult.f1353b);
    }

    /* access modifiers changed from: package-private */
    public final ConnectionResult h() {
        ConnectionResult connectionResult = null;
        ConnectionResult connectionResult2 = null;
        int i2 = 0;
        int i3 = 0;
        for (cf next : this.f1455a.values()) {
            a<O> aVar = next.f1373a;
            ConnectionResult connectionResult3 = this.h.get(next.f1374b);
            if (!connectionResult3.b() && (!this.k.get(aVar).booleanValue() || connectionResult3.a() || this.n.a(connectionResult3.f1353b))) {
                if (connectionResult3.f1353b != 4 || !this.p) {
                    int a2 = aVar.f1371a.a();
                    if (connectionResult == null || i2 > a2) {
                        connectionResult = connectionResult3;
                        i2 = a2;
                    }
                } else {
                    int a3 = aVar.f1371a.a();
                    if (connectionResult2 == null || i3 > a3) {
                        connectionResult2 = connectionResult3;
                        i3 = a3;
                    }
                }
            }
        }
        return (connectionResult == null || connectionResult2 == null || i2 <= i3) ? connectionResult : connectionResult2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final <A extends com.google.android.gms.common.api.a.b, T extends com.google.android.gms.common.api.internal.c.a<? extends com.google.android.gms.common.api.g, A>> T a(T r9) {
        /*
            r8 = this;
            com.google.android.gms.common.api.a$c<A> r0 = r9.f1447a
            boolean r1 = r8.p
            if (r1 == 0) goto L_0x0059
            com.google.android.gms.common.api.a$c<A> r1 = r9.f1447a
            com.google.android.gms.common.ConnectionResult r2 = r8.a(r1)
            if (r2 == 0) goto L_0x0055
            int r2 = r2.f1353b
            r3 = 4
            if (r2 != r3) goto L_0x0055
            com.google.android.gms.common.api.Status r2 = new com.google.android.gms.common.api.Status
            com.google.android.gms.common.api.internal.d r4 = r8.l
            java.util.Map<com.google.android.gms.common.api.a$c<?>, com.google.android.gms.common.api.internal.cf<?>> r5 = r8.f1455a
            java.lang.Object r1 = r5.get(r1)
            com.google.android.gms.common.api.internal.cf r1 = (com.google.android.gms.common.api.internal.cf) r1
            com.google.android.gms.common.api.internal.bs<O> r1 = r1.f1374b
            com.google.android.gms.common.api.internal.ag r5 = r8.c
            int r5 = java.lang.System.identityHashCode(r5)
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r6 = r4.f
            java.lang.Object r1 = r6.get(r1)
            com.google.android.gms.common.api.internal.d$a r1 = (com.google.android.gms.common.api.internal.d.a) r1
            r6 = 0
            if (r1 != 0) goto L_0x0034
        L_0x0032:
            r1 = r6
            goto L_0x004d
        L_0x0034:
            com.google.android.gms.common.api.internal.zace r7 = r1.e
            if (r7 != 0) goto L_0x003a
            r1 = r6
            goto L_0x003e
        L_0x003a:
            com.google.android.gms.common.api.internal.zace r1 = r1.e
            com.google.android.gms.signin.e r1 = r1.f1509a
        L_0x003e:
            if (r1 != 0) goto L_0x0041
            goto L_0x0032
        L_0x0041:
            android.content.Context r4 = r4.c
            android.content.Intent r1 = r1.e()
            r7 = 134217728(0x8000000, float:3.85186E-34)
            android.app.PendingIntent r1 = android.app.PendingIntent.getActivity(r4, r5, r1, r7)
        L_0x004d:
            r2.<init>(r3, r6, r1)
            r9.a(r2)
            r1 = 1
            goto L_0x0056
        L_0x0055:
            r1 = 0
        L_0x0056:
            if (r1 == 0) goto L_0x0059
            return r9
        L_0x0059:
            com.google.android.gms.common.api.internal.ag r1 = r8.c
            com.google.android.gms.common.api.internal.bm r1 = r1.e
            r1.a(r9)
            java.util.Map<com.google.android.gms.common.api.a$c<?>, com.google.android.gms.common.api.internal.cf<?>> r1 = r8.f1455a
            java.lang.Object r0 = r1.get(r0)
            com.google.android.gms.common.api.internal.cf r0 = (com.google.android.gms.common.api.internal.cf) r0
            com.google.android.gms.common.api.internal.c$a r9 = r0.a(r9)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.cg.a(com.google.android.gms.common.api.internal.c$a):com.google.android.gms.common.api.internal.c$a");
    }
}
