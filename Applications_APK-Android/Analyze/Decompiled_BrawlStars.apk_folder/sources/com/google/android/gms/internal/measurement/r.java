package com.google.android.gms.internal.measurement;

public abstract class r extends q {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2329a;

    protected r(t tVar) {
        super(tVar);
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public final boolean l() {
        return this.f2329a;
    }

    /* access modifiers changed from: protected */
    public final void m() {
        if (!l()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void n() {
        a();
        this.f2329a = true;
    }
}
