package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public final class zzh extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzh> CREATOR = new h();

    /* renamed from: a  reason: collision with root package name */
    private final long f1769a;

    /* renamed from: b  reason: collision with root package name */
    private final long f1770b;

    public zzh(long j, long j2) {
        this.f1769a = j;
        this.f1770b = j2;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, this.f1769a);
        a.a(parcel, 3, this.f1770b);
        a.b(parcel, a2);
    }
}
