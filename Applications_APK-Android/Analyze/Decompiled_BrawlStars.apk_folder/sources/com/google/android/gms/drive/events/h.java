package com.google.android.gms.drive.events;

import android.os.Looper;
import com.google.android.gms.drive.events.DriveEventService;
import java.util.concurrent.CountDownLatch;

final class h extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CountDownLatch f1717a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ DriveEventService f1718b;

    h(DriveEventService driveEventService, CountDownLatch countDownLatch) {
        this.f1718b = driveEventService;
        this.f1717a = countDownLatch;
    }

    public final void run() {
        try {
            Looper.prepare();
            this.f1718b.f1713a = new DriveEventService.a(this.f1718b, (byte) 0);
            this.f1718b.f1714b = false;
            this.f1717a.countDown();
            Looper.loop();
        } finally {
            if (this.f1718b.e != null) {
                this.f1718b.e.countDown();
            }
        }
    }
}
