package com.google.android.gms.internal.measurement;

import java.util.Map;

public class bk extends r {

    /* renamed from: a  reason: collision with root package name */
    private static bk f2078a;

    public bk(t tVar) {
        super(tVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        synchronized (bk.class) {
            f2078a = this;
        }
    }

    public static bk b() {
        return f2078a;
    }

    public final void a(bh bhVar, String str) {
        String bhVar2 = bhVar != null ? bhVar.toString() : "no hit data";
        String valueOf = String.valueOf(str);
        d(valueOf.length() != 0 ? "Discarding hit. ".concat(valueOf) : new String("Discarding hit. "), bhVar2);
    }

    public final void a(Map<String, String> map, String str) {
        String str2;
        if (map != null) {
            StringBuilder sb = new StringBuilder();
            for (Map.Entry next : map.entrySet()) {
                if (sb.length() > 0) {
                    sb.append(',');
                }
                sb.append((String) next.getKey());
                sb.append('=');
                sb.append((String) next.getValue());
            }
            str2 = sb.toString();
        } else {
            str2 = "no hit data";
        }
        String valueOf = String.valueOf(str);
        d(valueOf.length() != 0 ? "Discarding hit. ".concat(valueOf) : new String("Discarding hit. "), str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0088  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(int r8, java.lang.String r9, java.lang.Object r10, java.lang.Object r11, java.lang.Object r12) {
        /*
            r7 = this;
            monitor-enter(r7)
            com.google.android.gms.common.internal.l.a(r9)     // Catch:{ all -> 0x0100 }
            r0 = 0
            if (r8 >= 0) goto L_0x0008
            r8 = 0
        L_0x0008:
            r1 = 9
            if (r8 < r1) goto L_0x000e
            r8 = 8
        L_0x000e:
            com.google.android.gms.internal.measurement.au r1 = r7.h()     // Catch:{ all -> 0x0100 }
            boolean r1 = r1.a()     // Catch:{ all -> 0x0100 }
            if (r1 == 0) goto L_0x001b
            r1 = 67
            goto L_0x001d
        L_0x001b:
            r1 = 99
        L_0x001d:
            java.lang.String r2 = "01VDIWEA?"
            char r8 = r2.charAt(r8)     // Catch:{ all -> 0x0100 }
            java.lang.String r2 = com.google.android.gms.internal.measurement.s.f2330a     // Catch:{ all -> 0x0100 }
            java.lang.String r10 = a(r10)     // Catch:{ all -> 0x0100 }
            java.lang.String r11 = a(r11)     // Catch:{ all -> 0x0100 }
            java.lang.String r12 = a(r12)     // Catch:{ all -> 0x0100 }
            java.lang.String r9 = c(r9, r10, r11, r12)     // Catch:{ all -> 0x0100 }
            java.lang.String r10 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0100 }
            int r10 = r10.length()     // Catch:{ all -> 0x0100 }
            int r10 = r10 + 4
            java.lang.String r11 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x0100 }
            int r11 = r11.length()     // Catch:{ all -> 0x0100 }
            int r10 = r10 + r11
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x0100 }
            r11.<init>(r10)     // Catch:{ all -> 0x0100 }
            java.lang.String r10 = "3"
            r11.append(r10)     // Catch:{ all -> 0x0100 }
            r11.append(r8)     // Catch:{ all -> 0x0100 }
            r11.append(r1)     // Catch:{ all -> 0x0100 }
            r11.append(r2)     // Catch:{ all -> 0x0100 }
            java.lang.String r8 = ":"
            r11.append(r8)     // Catch:{ all -> 0x0100 }
            r11.append(r9)     // Catch:{ all -> 0x0100 }
            java.lang.String r8 = r11.toString()     // Catch:{ all -> 0x0100 }
            int r9 = r8.length()     // Catch:{ all -> 0x0100 }
            r10 = 1024(0x400, float:1.435E-42)
            if (r9 <= r10) goto L_0x0073
            java.lang.String r8 = r8.substring(r0, r10)     // Catch:{ all -> 0x0100 }
        L_0x0073:
            com.google.android.gms.internal.measurement.t r9 = r7.c     // Catch:{ all -> 0x0100 }
            com.google.android.gms.internal.measurement.bo r10 = r9.g     // Catch:{ all -> 0x0100 }
            if (r10 == 0) goto L_0x0085
            com.google.android.gms.internal.measurement.bo r10 = r9.g     // Catch:{ all -> 0x0100 }
            boolean r10 = r10.l()     // Catch:{ all -> 0x0100 }
            if (r10 != 0) goto L_0x0082
            goto L_0x0085
        L_0x0082:
            com.google.android.gms.internal.measurement.bo r9 = r9.g     // Catch:{ all -> 0x0100 }
            goto L_0x0086
        L_0x0085:
            r9 = 0
        L_0x0086:
            if (r9 == 0) goto L_0x00fe
            com.google.android.gms.internal.measurement.bp r9 = r9.f2086b     // Catch:{ all -> 0x0100 }
            long r10 = r9.c()     // Catch:{ all -> 0x0100 }
            r1 = 0
            int r12 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r12 != 0) goto L_0x0097
            r9.a()     // Catch:{ all -> 0x0100 }
        L_0x0097:
            if (r8 != 0) goto L_0x009b
            java.lang.String r8 = ""
        L_0x009b:
            monitor-enter(r9)     // Catch:{ all -> 0x0100 }
            com.google.android.gms.internal.measurement.bo r10 = r9.f2087a     // Catch:{ all -> 0x00fb }
            android.content.SharedPreferences r10 = r10.f2085a     // Catch:{ all -> 0x00fb }
            java.lang.String r11 = r9.d()     // Catch:{ all -> 0x00fb }
            long r10 = r10.getLong(r11, r1)     // Catch:{ all -> 0x00fb }
            r3 = 1
            int r12 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r12 > 0) goto L_0x00c9
            com.google.android.gms.internal.measurement.bo r10 = r9.f2087a     // Catch:{ all -> 0x00fb }
            android.content.SharedPreferences r10 = r10.f2085a     // Catch:{ all -> 0x00fb }
            android.content.SharedPreferences$Editor r10 = r10.edit()     // Catch:{ all -> 0x00fb }
            java.lang.String r11 = r9.e()     // Catch:{ all -> 0x00fb }
            r10.putString(r11, r8)     // Catch:{ all -> 0x00fb }
            java.lang.String r8 = r9.d()     // Catch:{ all -> 0x00fb }
            r10.putLong(r8, r3)     // Catch:{ all -> 0x00fb }
            r10.apply()     // Catch:{ all -> 0x00fb }
            monitor-exit(r9)     // Catch:{ all -> 0x00fb }
            goto L_0x00fe
        L_0x00c9:
            java.util.UUID r12 = java.util.UUID.randomUUID()     // Catch:{ all -> 0x00fb }
            long r1 = r12.getLeastSignificantBits()     // Catch:{ all -> 0x00fb }
            r5 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            long r1 = r1 & r5
            long r10 = r10 + r3
            long r5 = r5 / r10
            int r12 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r12 >= 0) goto L_0x00de
            r0 = 1
        L_0x00de:
            com.google.android.gms.internal.measurement.bo r12 = r9.f2087a     // Catch:{ all -> 0x00fb }
            android.content.SharedPreferences r12 = r12.f2085a     // Catch:{ all -> 0x00fb }
            android.content.SharedPreferences$Editor r12 = r12.edit()     // Catch:{ all -> 0x00fb }
            if (r0 == 0) goto L_0x00ef
            java.lang.String r0 = r9.e()     // Catch:{ all -> 0x00fb }
            r12.putString(r0, r8)     // Catch:{ all -> 0x00fb }
        L_0x00ef:
            java.lang.String r8 = r9.d()     // Catch:{ all -> 0x00fb }
            r12.putLong(r8, r10)     // Catch:{ all -> 0x00fb }
            r12.apply()     // Catch:{ all -> 0x00fb }
            monitor-exit(r9)     // Catch:{ all -> 0x00fb }
            goto L_0x00fe
        L_0x00fb:
            r8 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x00fb }
            throw r8     // Catch:{ all -> 0x0100 }
        L_0x00fe:
            monitor-exit(r7)
            return
        L_0x0100:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.bk.a(int, java.lang.String, java.lang.Object, java.lang.Object, java.lang.Object):void");
    }

    private static String a(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Integer) {
            obj = Long.valueOf((long) ((Integer) obj).intValue());
        }
        String str = "-";
        if (obj instanceof Long) {
            Long l = (Long) obj;
            if (Math.abs(l.longValue()) < 100) {
                return String.valueOf(obj);
            }
            if (String.valueOf(obj).charAt(0) != '-') {
                str = "";
            }
            String valueOf = String.valueOf(Math.abs(l.longValue()));
            return str + Math.round(Math.pow(10.0d, (double) (valueOf.length() - 1))) + "..." + str + Math.round(Math.pow(10.0d, (double) valueOf.length()) - 1.0d);
        } else if (obj instanceof Boolean) {
            return String.valueOf(obj);
        } else {
            return obj instanceof Throwable ? obj.getClass().getCanonicalName() : str;
        }
    }
}
