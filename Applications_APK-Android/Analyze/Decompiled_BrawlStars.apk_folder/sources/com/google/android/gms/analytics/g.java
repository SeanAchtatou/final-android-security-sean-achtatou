package com.google.android.gms.analytics;

import android.content.BroadcastReceiver;

final class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ BroadcastReceiver.PendingResult f1325a;

    g(BroadcastReceiver.PendingResult pendingResult) {
        this.f1325a = pendingResult;
    }

    public final void run() {
        BroadcastReceiver.PendingResult pendingResult = this.f1325a;
        if (pendingResult != null) {
            pendingResult.finish();
        }
    }
}
