package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.google.android.gms.analytics.l;
import java.util.HashMap;

public final class hy extends l<hy> {

    /* renamed from: a  reason: collision with root package name */
    public String f2277a;

    /* renamed from: b  reason: collision with root package name */
    public String f2278b;
    public String c;
    public String d;

    public final void a(hy hyVar) {
        if (!TextUtils.isEmpty(this.f2277a)) {
            hyVar.f2277a = this.f2277a;
        }
        if (!TextUtils.isEmpty(this.f2278b)) {
            hyVar.f2278b = this.f2278b;
        }
        if (!TextUtils.isEmpty(this.c)) {
            hyVar.c = this.c;
        }
        if (!TextUtils.isEmpty(this.d)) {
            hyVar.d = this.d;
        }
    }

    public final String toString() {
        HashMap hashMap = new HashMap();
        hashMap.put("appName", this.f2277a);
        hashMap.put("appVersion", this.f2278b);
        hashMap.put("appId", this.c);
        hashMap.put("appInstallerId", this.d);
        return a((Object) hashMap);
    }
}
