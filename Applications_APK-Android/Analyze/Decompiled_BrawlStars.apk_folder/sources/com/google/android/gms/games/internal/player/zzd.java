package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.d;

public final class zzd extends d implements zza {
    private final b c;

    public zzd(DataHolder dataHolder, int i, b bVar) {
        super(dataHolder, i);
        this.c = bVar;
    }

    public final /* synthetic */ Object a() {
        return new zzb(this);
    }

    public final String b() {
        return e(this.c.s);
    }

    public final String c() {
        return e(this.c.t);
    }

    public final long d() {
        return b(this.c.u);
    }

    public final int describeContents() {
        return 0;
    }

    public final Uri e() {
        return h(this.c.v);
    }

    public final boolean equals(Object obj) {
        return zzb.a(this, obj);
    }

    public final Uri f() {
        return h(this.c.w);
    }

    public final Uri g() {
        return h(this.c.x);
    }

    public final int hashCode() {
        return zzb.a(this);
    }

    public final String toString() {
        return zzb.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((zzb) ((zza) a())).writeToParcel(parcel, i);
    }
}
