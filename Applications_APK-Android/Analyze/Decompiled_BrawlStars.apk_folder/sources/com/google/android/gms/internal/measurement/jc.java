package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

final class jc implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    ja<?, ?> f2309a;

    /* renamed from: b  reason: collision with root package name */
    Object f2310b;
    List<jg> c = new ArrayList();

    jc() {
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        Object obj = this.f2310b;
        if (obj != null) {
            ja<?, ?> jaVar = this.f2309a;
            if (!jaVar.c) {
                return jaVar.a(obj);
            }
            int length = Array.getLength(obj);
            int i = 0;
            for (int i2 = 0; i2 < length; i2++) {
                Object obj2 = Array.get(obj, i2);
                if (obj2 != null) {
                    i += jaVar.a(obj2);
                }
            }
            return i;
        }
        int i3 = 0;
        for (jg next : this.c) {
            i3 += iy.c(next.f2313a) + 0 + next.f2314b.length;
        }
        return i3;
    }

    /* access modifiers changed from: package-private */
    public final void a(iy iyVar) throws IOException {
        Object obj = this.f2310b;
        if (obj != null) {
            ja<?, ?> jaVar = this.f2309a;
            if (jaVar.c) {
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    Object obj2 = Array.get(obj, i);
                    if (obj2 != null) {
                        jaVar.a(obj2, iyVar);
                    }
                }
                return;
            }
            jaVar.a(obj, iyVar);
            return;
        }
        for (jg next : this.c) {
            iyVar.b(next.f2313a);
            byte[] bArr = next.f2314b;
            int length2 = bArr.length;
            if (iyVar.f2301a.remaining() >= length2) {
                iyVar.f2301a.put(bArr, 0, length2);
            } else {
                throw new zzyb(iyVar.f2301a.position(), iyVar.f2301a.limit());
            }
        }
    }

    public final boolean equals(Object obj) {
        List<jg> list;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof jc)) {
            return false;
        }
        jc jcVar = (jc) obj;
        if (this.f2310b == null || jcVar.f2310b == null) {
            List<jg> list2 = this.c;
            if (list2 != null && (list = jcVar.c) != null) {
                return list2.equals(list);
            }
            try {
                return Arrays.equals(b(), jcVar.b());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else {
            ja<?, ?> jaVar = this.f2309a;
            if (jaVar != jcVar.f2309a) {
                return false;
            }
            if (!jaVar.f2305a.isArray()) {
                return this.f2310b.equals(jcVar.f2310b);
            }
            Object obj2 = this.f2310b;
            if (obj2 instanceof byte[]) {
                return Arrays.equals((byte[]) obj2, (byte[]) jcVar.f2310b);
            }
            if (obj2 instanceof int[]) {
                return Arrays.equals((int[]) obj2, (int[]) jcVar.f2310b);
            }
            if (obj2 instanceof long[]) {
                return Arrays.equals((long[]) obj2, (long[]) jcVar.f2310b);
            }
            if (obj2 instanceof float[]) {
                return Arrays.equals((float[]) obj2, (float[]) jcVar.f2310b);
            }
            if (obj2 instanceof double[]) {
                return Arrays.equals((double[]) obj2, (double[]) jcVar.f2310b);
            }
            if (obj2 instanceof boolean[]) {
                return Arrays.equals((boolean[]) obj2, (boolean[]) jcVar.f2310b);
            }
            return Arrays.deepEquals((Object[]) obj2, (Object[]) jcVar.f2310b);
        }
    }

    public final int hashCode() {
        try {
            return Arrays.hashCode(b()) + 527;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private final byte[] b() throws IOException {
        byte[] bArr = new byte[a()];
        a(iy.a(bArr, bArr.length));
        return bArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final jc clone() {
        jc jcVar = new jc();
        try {
            jcVar.f2309a = this.f2309a;
            if (this.c == null) {
                jcVar.c = null;
            } else {
                jcVar.c.addAll(this.c);
            }
            if (this.f2310b != null) {
                if (this.f2310b instanceof je) {
                    jcVar.f2310b = (je) ((je) this.f2310b).clone();
                } else if (this.f2310b instanceof byte[]) {
                    jcVar.f2310b = ((byte[]) this.f2310b).clone();
                } else {
                    int i = 0;
                    if (this.f2310b instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.f2310b;
                        byte[][] bArr2 = new byte[bArr.length][];
                        jcVar.f2310b = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.f2310b instanceof boolean[]) {
                        jcVar.f2310b = ((boolean[]) this.f2310b).clone();
                    } else if (this.f2310b instanceof int[]) {
                        jcVar.f2310b = ((int[]) this.f2310b).clone();
                    } else if (this.f2310b instanceof long[]) {
                        jcVar.f2310b = ((long[]) this.f2310b).clone();
                    } else if (this.f2310b instanceof float[]) {
                        jcVar.f2310b = ((float[]) this.f2310b).clone();
                    } else if (this.f2310b instanceof double[]) {
                        jcVar.f2310b = ((double[]) this.f2310b).clone();
                    } else if (this.f2310b instanceof je[]) {
                        je[] jeVarArr = (je[]) this.f2310b;
                        je[] jeVarArr2 = new je[jeVarArr.length];
                        jcVar.f2310b = jeVarArr2;
                        while (i < jeVarArr.length) {
                            jeVarArr2[i] = (je) jeVarArr[i].clone();
                            i++;
                        }
                    }
                }
            }
            return jcVar;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }
}
