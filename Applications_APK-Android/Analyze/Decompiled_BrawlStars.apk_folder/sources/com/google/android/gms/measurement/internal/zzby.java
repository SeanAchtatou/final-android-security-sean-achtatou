package com.google.android.gms.measurement.internal;

import android.os.Binder;
import android.text.TextUtils;
import com.google.android.gms.common.f;
import com.google.android.gms.common.g;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.r;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public final class zzby extends zzak {
    /* access modifiers changed from: package-private */

    /* renamed from: a  reason: collision with root package name */
    public final du f2597a;

    /* renamed from: b  reason: collision with root package name */
    private Boolean f2598b;
    private String c;

    public zzby(du duVar) {
        this(duVar, (byte) 0);
    }

    private zzby(du duVar, byte b2) {
        l.a(duVar);
        this.f2597a = duVar;
        this.c = null;
    }

    public final void b(zzk zzk) {
        e(zzk);
        a(new at(this, zzk));
    }

    public final void a(zzag zzag, zzk zzk) {
        l.a(zzag);
        e(zzk);
        a(new be(this, zzag, zzk));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void */
    public final void a(zzag zzag, String str, String str2) {
        l.a(zzag);
        l.a(str);
        a(str, true);
        a(new bf(this, zzag, str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void */
    public final byte[] a(zzag zzag, String str) {
        l.a(str);
        l.a(zzag);
        a(str, true);
        this.f2597a.q().j.a("Log and bundle. event", this.f2597a.f2528b.f().a(zzag.f2595a));
        long c2 = this.f2597a.l().c() / 1000000;
        try {
            byte[] bArr = (byte[]) this.f2597a.p().b(new bg(this, zzag, str)).get();
            if (bArr == null) {
                this.f2597a.q().c.a("Log and bundle returned null. appId", o.a(str));
                bArr = new byte[0];
            }
            this.f2597a.q().j.a("Log and bundle processed. event, size, time_ms", this.f2597a.f2528b.f().a(zzag.f2595a), Integer.valueOf(bArr.length), Long.valueOf((this.f2597a.l().c() / 1000000) - c2));
            return bArr;
        } catch (InterruptedException | ExecutionException e) {
            this.f2597a.q().c.a("Failed to log and bundle. appId, event, error", o.a(str), this.f2597a.f2528b.f().a(zzag.f2595a), e);
            return null;
        }
    }

    public final void a(zzfv zzfv, zzk zzk) {
        l.a(zzfv);
        e(zzk);
        if (zzfv.a() == null) {
            a(new bh(this, zzfv, zzk));
        } else {
            a(new bi(this, zzfv, zzk));
        }
    }

    public final List<zzfv> a(zzk zzk, boolean z) {
        e(zzk);
        try {
            List<ec> list = (List) this.f2597a.p().a(new bj(this, zzk)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (ec ecVar : list) {
                if (z || !ed.e(ecVar.c)) {
                    arrayList.add(new zzfv(ecVar));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.f2597a.q().c.a("Failed to get user attributes. appId", o.a(zzk.f2601a), e);
            return null;
        }
    }

    public final void a(zzk zzk) {
        e(zzk);
        a(new bk(this, zzk));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void */
    private final void e(zzk zzk) {
        l.a(zzk);
        a(zzk.f2601a, false);
        this.f2597a.f2528b.e().b(zzk.f2602b, zzk.r);
    }

    private final void a(String str, boolean z) {
        boolean z2;
        if (!TextUtils.isEmpty(str)) {
            if (z) {
                try {
                    if (this.f2598b == null) {
                        if (!"com.google.android.gms".equals(this.c) && !r.a(this.f2597a.m(), Binder.getCallingUid())) {
                            if (!g.a(this.f2597a.m()).a(Binder.getCallingUid())) {
                                z2 = false;
                                this.f2598b = Boolean.valueOf(z2);
                            }
                        }
                        z2 = true;
                        this.f2598b = Boolean.valueOf(z2);
                    }
                    if (this.f2598b.booleanValue()) {
                        return;
                    }
                } catch (SecurityException e) {
                    this.f2597a.q().c.a("Measurement Service called with invalid calling package. appId", o.a(str));
                    throw e;
                }
            }
            if (this.c == null && f.a(this.f2597a.m(), Binder.getCallingUid(), str)) {
                this.c = str;
            }
            if (!str.equals(this.c)) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", str));
            }
            return;
        }
        this.f2597a.q().c.a("Measurement Service called without app package");
        throw new SecurityException("Measurement Service called without app package");
    }

    public final void a(long j, String str, String str2, String str3) {
        a(new bl(this, str2, str3, str, j));
    }

    public final String c(zzk zzk) {
        e(zzk);
        return this.f2597a.d(zzk);
    }

    public final void a(zzo zzo, zzk zzk) {
        l.a(zzo);
        l.a(zzo.c);
        e(zzk);
        zzo zzo2 = new zzo(zzo);
        zzo2.f2603a = zzk.f2601a;
        if (zzo.c.a() == null) {
            a(new av(this, zzo2, zzk));
        } else {
            a(new aw(this, zzo2, zzk));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void */
    public final void a(zzo zzo) {
        l.a(zzo);
        l.a(zzo.c);
        a(zzo.f2603a, true);
        zzo zzo2 = new zzo(zzo);
        if (zzo.c.a() == null) {
            a(new ax(this, zzo2));
        } else {
            a(new ay(this, zzo2));
        }
    }

    public final List<zzfv> a(String str, String str2, boolean z, zzk zzk) {
        e(zzk);
        try {
            List<ec> list = (List) this.f2597a.p().a(new az(this, zzk, str, str2)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (ec ecVar : list) {
                if (z || !ed.e(ecVar.c)) {
                    arrayList.add(new zzfv(ecVar));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.f2597a.q().c.a("Failed to get user attributes. appId", o.a(zzk.f2601a), e);
            return Collections.emptyList();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void */
    public final List<zzfv> a(String str, String str2, String str3, boolean z) {
        a(str, true);
        try {
            List<ec> list = (List) this.f2597a.p().a(new ba(this, str, str2, str3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (ec ecVar : list) {
                if (z || !ed.e(ecVar.c)) {
                    arrayList.add(new zzfv(ecVar));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.f2597a.q().c.a("Failed to get user attributes. appId", o.a(str), e);
            return Collections.emptyList();
        }
    }

    public final List<zzo> a(String str, String str2, zzk zzk) {
        e(zzk);
        try {
            return (List) this.f2597a.p().a(new bb(this, zzk, str, str2)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.f2597a.q().c.a("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void */
    public final List<zzo> a(String str, String str2, String str3) {
        a(str, true);
        try {
            return (List) this.f2597a.p().a(new bc(this, str, str2, str3)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.f2597a.q().c.a("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzby.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzk, boolean):java.util.List<com.google.android.gms.measurement.internal.zzfv>
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzfv, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzo, com.google.android.gms.measurement.internal.zzk):void
      com.google.android.gms.measurement.internal.zzaj.a(com.google.android.gms.measurement.internal.zzag, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.zzby.a(java.lang.String, boolean):void */
    public final void d(zzk zzk) {
        a(zzk.f2601a, false);
        a(new bd(this, zzk));
    }

    private final void a(Runnable runnable) {
        l.a(runnable);
        if (!h.ab.a().booleanValue() || !this.f2597a.p().f()) {
            this.f2597a.p().a(runnable);
        } else {
            runnable.run();
        }
    }
}
