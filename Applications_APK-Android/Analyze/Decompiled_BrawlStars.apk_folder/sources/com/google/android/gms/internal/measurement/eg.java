package com.google.android.gms.internal.measurement;

final class eg {

    /* renamed from: a  reason: collision with root package name */
    private static final Class<?> f2181a = a("libcore.io.Memory");

    /* renamed from: b  reason: collision with root package name */
    private static final boolean f2182b = (a("org.robolectric.Robolectric") != null);

    static boolean a() {
        return f2181a != null && !f2182b;
    }

    static Class<?> b() {
        return f2181a;
    }

    private static <T> Class<T> a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable unused) {
            return null;
        }
    }
}
