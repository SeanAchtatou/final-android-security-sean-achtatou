package com.google.android.gms.internal.measurement;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public final class ie extends AbstractList<String> implements gd, RandomAccess {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final gd f2284a;

    public ie(gd gdVar) {
        this.f2284a = gdVar;
    }

    public final gd e() {
        return this;
    }

    public final Object b(int i) {
        return this.f2284a.b(i);
    }

    public final int size() {
        return this.f2284a.size();
    }

    public final void a(ej ejVar) {
        throw new UnsupportedOperationException();
    }

    public final ListIterator<String> listIterator(int i) {
        return new Cif(this, i);
    }

    public final Iterator<String> iterator() {
        return new ig(this);
    }

    public final List<?> d() {
        return this.f2284a.d();
    }

    public final /* synthetic */ Object get(int i) {
        return (String) this.f2284a.get(i);
    }
}
