package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.f;

public final class g extends f<Integer> {
    public g(String str) {
        super(str, 4300000);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        return Integer.valueOf(dataHolder.b(this.f1731b, i, i2));
    }

    public final /* synthetic */ void a(Bundle bundle, Object obj) {
        bundle.putInt(this.f1731b, ((Integer) obj).intValue());
    }

    public final /* synthetic */ Object b(Bundle bundle) {
        return Integer.valueOf(bundle.getInt(this.f1731b));
    }
}
