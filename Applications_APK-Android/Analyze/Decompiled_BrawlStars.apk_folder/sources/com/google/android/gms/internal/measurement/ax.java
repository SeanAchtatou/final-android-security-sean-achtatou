package com.google.android.gms.internal.measurement;

import android.os.Looper;

final class ax implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ aw f2061a;

    ax(aw awVar) {
        this.f2061a = awVar;
    }

    public final void run() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            this.f2061a.f2060a.b().a(this);
            return;
        }
        boolean c = this.f2061a.c();
        long unused = this.f2061a.d = 0;
        if (c) {
            this.f2061a.a();
        }
    }
}
