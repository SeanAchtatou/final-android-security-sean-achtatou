package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

public final class VideoCapabilities extends zzd {
    public static final Parcelable.Creator<VideoCapabilities> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final boolean f1852a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f1853b;
    private final boolean c;
    private final boolean[] d;
    private final boolean[] e;

    public VideoCapabilities(boolean z, boolean z2, boolean z3, boolean[] zArr, boolean[] zArr2) {
        this.f1852a = z;
        this.f1853b = z2;
        this.c = z3;
        this.d = zArr;
        this.e = zArr2;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.d, this.e, Boolean.valueOf(this.f1852a), Boolean.valueOf(this.f1853b), Boolean.valueOf(this.c)});
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof VideoCapabilities)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        VideoCapabilities videoCapabilities = (VideoCapabilities) obj;
        return j.a(videoCapabilities.d, this.d) && j.a(videoCapabilities.e, this.e) && j.a(Boolean.valueOf(videoCapabilities.f1852a), Boolean.valueOf(this.f1852a)) && j.a(Boolean.valueOf(videoCapabilities.f1853b), Boolean.valueOf(this.f1853b)) && j.a(Boolean.valueOf(videoCapabilities.c), Boolean.valueOf(this.c));
    }

    public final String toString() {
        return j.a(this).a("SupportedCaptureModes", this.d).a("SupportedQualityLevels", this.e).a("CameraSupported", Boolean.valueOf(this.f1852a)).a("MicSupported", Boolean.valueOf(this.f1853b)).a("StorageWriteSupported", Boolean.valueOf(this.c)).toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
     arg types: [android.os.Parcel, int, boolean[], int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, this.f1852a);
        a.a(parcel, 2, this.f1853b);
        a.a(parcel, 3, this.c);
        a.a(parcel, 4, this.d, false);
        a.a(parcel, 5, this.e, false);
        a.b(parcel, a2);
    }
}
