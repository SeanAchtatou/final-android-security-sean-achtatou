package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

final class gp implements go {
    gp() {
    }

    public final Map<?, ?> a(Object obj) {
        return (gn) obj;
    }

    public final gm<?, ?> b() {
        throw new NoSuchMethodError();
    }

    public final Map<?, ?> b(Object obj) {
        return (gn) obj;
    }

    public final boolean c(Object obj) {
        return !((gn) obj).f2247a;
    }

    public final Object d(Object obj) {
        ((gn) obj).f2247a = false;
        return obj;
    }

    public final Object a() {
        return gn.a().b();
    }

    public final Object a(Object obj, Object obj2) {
        gn gnVar = (gn) obj;
        gn gnVar2 = (gn) obj2;
        if (!gnVar2.isEmpty()) {
            if (!gnVar.f2247a) {
                gnVar = gnVar.b();
            }
            gnVar.c();
            if (!gnVar2.isEmpty()) {
                gnVar.putAll(gnVar2);
            }
        }
        return gnVar;
    }

    public final int e(Object obj) {
        gn gnVar = (gn) obj;
        if (gnVar.isEmpty()) {
            return 0;
        }
        Iterator it = gnVar.entrySet().iterator();
        if (!it.hasNext()) {
            return 0;
        }
        Map.Entry entry = (Map.Entry) it.next();
        entry.getKey();
        entry.getValue();
        throw new NoSuchMethodError();
    }
}
