package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.internal.zzd;

public final class zza extends zzd implements SnapshotContents {
    public static final Parcelable.Creator<zza> CREATOR = new b();

    /* renamed from: b  reason: collision with root package name */
    private static final Object f1846b = new Object();

    /* renamed from: a  reason: collision with root package name */
    Contents f1847a;

    public zza(Contents contents) {
        this.f1847a = contents;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.Contents, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, (Parcelable) this.f1847a, i, false);
        a.b(parcel, a2);
    }
}
