package com.google.android.gms.measurement.internal;

final class bf implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzag f2417a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2418b;
    private final /* synthetic */ zzby c;

    bf(zzby zzby, zzag zzag, String str) {
        this.c = zzby;
        this.f2417a = zzag;
        this.f2418b = str;
    }

    public final void run() {
        this.c.f2597a.k();
        this.c.f2597a.a(this.f2417a, this.f2418b);
    }
}
