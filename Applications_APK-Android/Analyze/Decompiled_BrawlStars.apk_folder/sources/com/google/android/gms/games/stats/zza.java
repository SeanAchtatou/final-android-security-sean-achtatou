package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

public final class zza extends zzd implements PlayerStats {
    public static final Parcelable.Creator<zza> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final float f1850a;

    /* renamed from: b  reason: collision with root package name */
    private final float f1851b;
    private final int c;
    private final int d;
    private final int e;
    private final float f;
    private final float g;
    private final Bundle h;
    private final float i;
    private final float j;
    private final float k;

    zza(float f2, float f3, int i2, int i3, int i4, float f4, float f5, Bundle bundle, float f6, float f7, float f8) {
        this.f1850a = f2;
        this.f1851b = f3;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = f4;
        this.g = f5;
        this.h = bundle;
        this.i = f6;
        this.j = f7;
        this.k = f8;
    }

    public zza(PlayerStats playerStats) {
        this.f1850a = playerStats.b();
        this.f1851b = playerStats.c();
        this.c = playerStats.d();
        this.d = playerStats.e();
        this.e = playerStats.f();
        this.f = playerStats.g();
        this.g = playerStats.h();
        this.i = playerStats.i();
        this.j = playerStats.j();
        this.k = playerStats.k();
        this.h = playerStats.l();
    }

    static boolean a(PlayerStats playerStats, Object obj) {
        if (!(obj instanceof PlayerStats)) {
            return false;
        }
        if (playerStats == obj) {
            return true;
        }
        PlayerStats playerStats2 = (PlayerStats) obj;
        return j.a(Float.valueOf(playerStats2.b()), Float.valueOf(playerStats.b())) && j.a(Float.valueOf(playerStats2.c()), Float.valueOf(playerStats.c())) && j.a(Integer.valueOf(playerStats2.d()), Integer.valueOf(playerStats.d())) && j.a(Integer.valueOf(playerStats2.e()), Integer.valueOf(playerStats.e())) && j.a(Integer.valueOf(playerStats2.f()), Integer.valueOf(playerStats.f())) && j.a(Float.valueOf(playerStats2.g()), Float.valueOf(playerStats.g())) && j.a(Float.valueOf(playerStats2.h()), Float.valueOf(playerStats.h())) && j.a(Float.valueOf(playerStats2.i()), Float.valueOf(playerStats.i())) && j.a(Float.valueOf(playerStats2.j()), Float.valueOf(playerStats.j())) && j.a(Float.valueOf(playerStats2.k()), Float.valueOf(playerStats.k()));
    }

    static String b(PlayerStats playerStats) {
        return j.a(playerStats).a("AverageSessionLength", Float.valueOf(playerStats.b())).a("ChurnProbability", Float.valueOf(playerStats.c())).a("DaysSinceLastPlayed", Integer.valueOf(playerStats.d())).a("NumberOfPurchases", Integer.valueOf(playerStats.e())).a("NumberOfSessions", Integer.valueOf(playerStats.f())).a("SessionPercentile", Float.valueOf(playerStats.g())).a("SpendPercentile", Float.valueOf(playerStats.h())).a("SpendProbability", Float.valueOf(playerStats.i())).a("HighSpenderProbability", Float.valueOf(playerStats.j())).a("TotalSpendNext28Days", Float.valueOf(playerStats.k())).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final float b() {
        return this.f1850a;
    }

    public final float c() {
        return this.f1851b;
    }

    public final int d() {
        return this.c;
    }

    public final int e() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final int f() {
        return this.e;
    }

    public final float g() {
        return this.f;
    }

    public final float h() {
        return this.g;
    }

    public final int hashCode() {
        return a(this);
    }

    public final float i() {
        return this.i;
    }

    public final float j() {
        return this.j;
    }

    public final float k() {
        return this.k;
    }

    public final Bundle l() {
        return this.h;
    }

    public final String toString() {
        return b(this);
    }

    static int a(PlayerStats playerStats) {
        return Arrays.hashCode(new Object[]{Float.valueOf(playerStats.b()), Float.valueOf(playerStats.c()), Integer.valueOf(playerStats.d()), Integer.valueOf(playerStats.e()), Integer.valueOf(playerStats.f()), Float.valueOf(playerStats.g()), Float.valueOf(playerStats.h()), Float.valueOf(playerStats.i()), Float.valueOf(playerStats.j()), Float.valueOf(playerStats.k())});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, this.f1850a);
        a.a(parcel, 2, this.f1851b);
        a.b(parcel, 3, this.c);
        a.b(parcel, 4, this.d);
        a.b(parcel, 5, this.e);
        a.a(parcel, 6, this.f);
        a.a(parcel, 7, this.g);
        a.a(parcel, 8, this.h, false);
        a.a(parcel, 9, this.i);
        a.a(parcel, 10, this.j);
        a.a(parcel, 11, this.k);
        a.b(parcel, a2);
    }
}
