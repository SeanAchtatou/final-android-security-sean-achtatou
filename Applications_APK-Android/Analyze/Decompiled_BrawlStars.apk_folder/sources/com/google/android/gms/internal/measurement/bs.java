package com.google.android.gms.internal.measurement;

final /* synthetic */ class bs implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final br f2093a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2094b;
    private final bk c;

    bs(br brVar, int i, bk bkVar) {
        this.f2093a = brVar;
        this.f2094b = i;
        this.c = bkVar;
    }

    public final void run() {
        br brVar = this.f2093a;
        int i = this.f2094b;
        bk bkVar = this.c;
        if (((bv) brVar.f2092b).a(i)) {
            bkVar.b("Local AnalyticsService processed last dispatch request");
        }
    }
}
