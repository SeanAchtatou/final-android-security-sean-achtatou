package com.google.android.gms.analytics;

import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.UserDataStore;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.analytics.a.c;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.measurement.a;
import com.google.android.gms.internal.measurement.b;
import com.google.android.gms.internal.measurement.bh;
import com.google.android.gms.internal.measurement.bx;
import com.google.android.gms.internal.measurement.d;
import com.google.android.gms.internal.measurement.e;
import com.google.android.gms.internal.measurement.g;
import com.google.android.gms.internal.measurement.h;
import com.google.android.gms.internal.measurement.hy;
import com.google.android.gms.internal.measurement.i;
import com.google.android.gms.internal.measurement.ix;
import com.google.android.gms.internal.measurement.j;
import com.google.android.gms.internal.measurement.ji;
import com.google.android.gms.internal.measurement.q;
import com.google.android.gms.internal.measurement.s;
import com.google.android.gms.internal.measurement.t;
import com.google.android.gms.internal.measurement.w;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class f extends q implements q {

    /* renamed from: a  reason: collision with root package name */
    private static DecimalFormat f1323a;

    /* renamed from: b  reason: collision with root package name */
    private final t f1324b;
    private final String d;
    private final Uri e;

    public f(t tVar, String str) {
        this(tVar, str, (byte) 0);
    }

    private f(t tVar, String str, byte b2) {
        super(tVar);
        l.a(str);
        this.f1324b = tVar;
        this.d = str;
        this.e = a(this.d);
    }

    static Uri a(String str) {
        l.a(str);
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(ShareConstants.MEDIA_URI);
        builder.authority("google-analytics.com");
        builder.path(str);
        return builder.build();
    }

    public final Uri a() {
        return this.e;
    }

    public final void a(k kVar) {
        l.a(kVar);
        l.b(kVar.f1329b, "Can't deliver not submitted measurement");
        l.c("deliver should be called on worker thread");
        k a2 = kVar.a();
        g gVar = (g) a2.b(g.class);
        if (TextUtils.isEmpty(gVar.f2231a)) {
            this.c.a().a(b(a2), "Ignoring measurement without type");
        } else if (TextUtils.isEmpty(gVar.f2232b)) {
            this.c.a().a(b(a2), "Ignoring measurement without client id");
        } else if (!this.f1324b.d().c) {
            double d2 = gVar.h;
            if (bx.a(d2, gVar.f2232b)) {
                b("Sampling enabled. Hit sampled out. sampling rate", Double.valueOf(d2));
                return;
            }
            Map<String, String> b2 = b(a2);
            b2.put("v", AppEventsConstants.EVENT_PARAM_VALUE_YES);
            b2.put("_v", s.f2331b);
            b2.put("tid", this.d);
            if (this.f1324b.d().f1313b) {
                StringBuilder sb = new StringBuilder();
                for (Map.Entry next : b2.entrySet()) {
                    if (sb.length() != 0) {
                        sb.append(", ");
                    }
                    sb.append((String) next.getKey());
                    sb.append("=");
                    sb.append((String) next.getValue());
                }
                c("Dry run is enabled. GoogleAnalytics would have sent", sb.toString());
                return;
            }
            HashMap hashMap = new HashMap();
            bx.a(hashMap, "uid", gVar.c);
            hy hyVar = (hy) kVar.a(hy.class);
            if (hyVar != null) {
                bx.a(hashMap, "an", hyVar.f2277a);
                bx.a(hashMap, "aid", hyVar.c);
                bx.a(hashMap, "av", hyVar.f2278b);
                bx.a(hashMap, "aiid", hyVar.d);
            }
            b2.put("_s", String.valueOf(this.c.c().a(new w(gVar.f2232b, this.d, !TextUtils.isEmpty(gVar.d), 0, hashMap))));
            this.c.c().a(new bh(this.c.a(), b2, kVar.c, true));
        }
    }

    private static Map<String, String> b(k kVar) {
        HashMap hashMap = new HashMap();
        b bVar = (b) kVar.a(b.class);
        if (bVar != null) {
            for (Map.Entry entry : Collections.unmodifiableMap(bVar.f2064a).entrySet()) {
                Object value = entry.getValue();
                String str = null;
                if (value != null) {
                    if (value instanceof String) {
                        String str2 = (String) value;
                        if (!TextUtils.isEmpty(str2)) {
                            str = str2;
                        }
                    } else if (value instanceof Double) {
                        Double d2 = (Double) value;
                        if (d2.doubleValue() != 0.0d) {
                            str = a(d2.doubleValue());
                        }
                    } else if (!(value instanceof Boolean)) {
                        str = String.valueOf(value);
                    } else if (value != Boolean.FALSE) {
                        str = AppEventsConstants.EVENT_PARAM_VALUE_YES;
                    }
                }
                if (str != null) {
                    hashMap.put((String) entry.getKey(), str);
                }
            }
        }
        g gVar = (g) kVar.a(g.class);
        if (gVar != null) {
            a(hashMap, "t", gVar.f2231a);
            a(hashMap, "cid", gVar.f2232b);
            a(hashMap, "uid", gVar.c);
            a(hashMap, "sc", gVar.f);
            a(hashMap, "sf", gVar.h);
            a((Map<String, String>) hashMap, "ni", gVar.g);
            a(hashMap, "adid", gVar.d);
            a((Map<String, String>) hashMap, "ate", gVar.e);
        }
        h hVar = (h) kVar.a(h.class);
        if (hVar != null) {
            a(hashMap, "cd", hVar.f2254a);
            a(hashMap, "a", (double) hVar.f2255b);
            a(hashMap, "dr", hVar.c);
        }
        e eVar = (e) kVar.a(e.class);
        if (eVar != null) {
            a(hashMap, "ec", eVar.f2176a);
            a(hashMap, "ea", eVar.f2177b);
            a(hashMap, "el", eVar.c);
            a(hashMap, "ev", (double) eVar.d);
        }
        ix ixVar = (ix) kVar.a(ix.class);
        if (ixVar != null) {
            a(hashMap, "cn", ixVar.f2299a);
            a(hashMap, "cs", ixVar.f2300b);
            a(hashMap, "cm", ixVar.c);
            a(hashMap, "ck", ixVar.d);
            a(hashMap, "cc", ixVar.e);
            a(hashMap, "ci", ixVar.f);
            a(hashMap, "anid", ixVar.g);
            a(hashMap, "gclid", ixVar.h);
            a(hashMap, "dclid", ixVar.i);
            a(hashMap, "aclid", ixVar.j);
        }
        com.google.android.gms.internal.measurement.f fVar = (com.google.android.gms.internal.measurement.f) kVar.a(com.google.android.gms.internal.measurement.f.class);
        if (fVar != null) {
            a(hashMap, "exd", fVar.f2196a);
            a((Map<String, String>) hashMap, "exf", fVar.f2197b);
        }
        i iVar = (i) kVar.a(i.class);
        if (iVar != null) {
            a(hashMap, "sn", iVar.f2280a);
            a(hashMap, "sa", iVar.f2281b);
            a(hashMap, UserDataStore.STATE, iVar.c);
        }
        j jVar = (j) kVar.a(j.class);
        if (jVar != null) {
            a(hashMap, "utv", jVar.f2303a);
            a(hashMap, "utt", (double) jVar.f2304b);
            a(hashMap, "utc", jVar.c);
            a(hashMap, "utl", jVar.d);
        }
        ji jiVar = (ji) kVar.a(ji.class);
        if (jiVar != null) {
            for (Map.Entry entry2 : Collections.unmodifiableMap(jiVar.f2317a).entrySet()) {
                String a2 = h.a("cd", ((Integer) entry2.getKey()).intValue());
                if (!TextUtils.isEmpty(a2)) {
                    hashMap.put(a2, (String) entry2.getValue());
                }
            }
        }
        a aVar = (a) kVar.a(a.class);
        if (aVar != null) {
            for (Map.Entry entry3 : Collections.unmodifiableMap(aVar.f2030a).entrySet()) {
                String a3 = h.a("cm", ((Integer) entry3.getKey()).intValue());
                if (!TextUtils.isEmpty(a3)) {
                    hashMap.put(a3, a(((Double) entry3.getValue()).doubleValue()));
                }
            }
        }
        d dVar = (d) kVar.a(d.class);
        if (dVar != null) {
            com.google.android.gms.analytics.a.b bVar2 = dVar.d;
            if (bVar2 != null) {
                for (Map.Entry next : bVar2.a().entrySet()) {
                    if (((String) next.getKey()).startsWith("&")) {
                        hashMap.put(((String) next.getKey()).substring(1), (String) next.getValue());
                    } else {
                        hashMap.put((String) next.getKey(), (String) next.getValue());
                    }
                }
            }
            int i = 1;
            for (c a4 : Collections.unmodifiableList(dVar.f2150b)) {
                hashMap.putAll(a4.a(h.a(NotificationCompat.CATEGORY_PROMO, i)));
                i++;
            }
            int i2 = 1;
            for (com.google.android.gms.analytics.a.a a5 : Collections.unmodifiableList(dVar.f2149a)) {
                hashMap.putAll(a5.a(h.a("pr", i2)));
                i2++;
            }
            int i3 = 1;
            for (Map.Entry next2 : dVar.c.entrySet()) {
                String a6 = h.a("il", i3);
                int i4 = 1;
                for (com.google.android.gms.analytics.a.a aVar2 : (List) next2.getValue()) {
                    String valueOf = String.valueOf(a6);
                    String valueOf2 = String.valueOf(h.a("pi", i4));
                    hashMap.putAll(aVar2.a(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf)));
                    i4++;
                }
                if (!TextUtils.isEmpty((CharSequence) next2.getKey())) {
                    String valueOf3 = String.valueOf(a6);
                    hashMap.put("nm".length() != 0 ? valueOf3.concat("nm") : new String(valueOf3), (String) next2.getKey());
                }
                i3++;
            }
        }
        com.google.android.gms.internal.measurement.c cVar = (com.google.android.gms.internal.measurement.c) kVar.a(com.google.android.gms.internal.measurement.c.class);
        if (cVar != null) {
            a(hashMap, "ul", cVar.f2106a);
            a(hashMap, "sd", (double) cVar.f2107b);
            a(hashMap, "sr", cVar.c, cVar.d);
            a(hashMap, "vp", cVar.e, cVar.f);
        }
        hy hyVar = (hy) kVar.a(hy.class);
        if (hyVar != null) {
            a(hashMap, "an", hyVar.f2277a);
            a(hashMap, "aid", hyVar.c);
            a(hashMap, "aiid", hyVar.d);
            a(hashMap, "av", hyVar.f2278b);
        }
        return hashMap;
    }

    private static void a(Map<String, String> map, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            map.put(str, str2);
        }
    }

    private static String a(double d2) {
        if (f1323a == null) {
            f1323a = new DecimalFormat("0.######");
        }
        return f1323a.format(d2);
    }

    private static void a(Map<String, String> map, String str, double d2) {
        if (d2 != 0.0d) {
            map.put(str, a(d2));
        }
    }

    private static void a(Map<String, String> map, String str, boolean z) {
        if (z) {
            map.put(str, AppEventsConstants.EVENT_PARAM_VALUE_YES);
        }
    }

    private static void a(Map<String, String> map, String str, int i, int i2) {
        if (i > 0 && i2 > 0) {
            StringBuilder sb = new StringBuilder(23);
            sb.append(i);
            sb.append("x");
            sb.append(i2);
            map.put(str, sb.toString());
        }
    }
}
