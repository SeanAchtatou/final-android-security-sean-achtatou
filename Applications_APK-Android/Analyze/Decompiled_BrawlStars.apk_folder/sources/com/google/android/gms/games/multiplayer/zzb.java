package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.Game;
import java.util.ArrayList;

public final class zzb extends d implements Invitation {
    private final Game c;
    private final ParticipantRef d;
    private final ArrayList<Participant> e;

    public final /* synthetic */ Object a() {
        return new InvitationEntity(this);
    }

    public final Game b() {
        return this.c;
    }

    public final String c() {
        return e("external_invitation_id");
    }

    public final Participant d() {
        return this.d;
    }

    public final int describeContents() {
        return 0;
    }

    public final long e() {
        return Math.max(b("creation_timestamp"), b("last_modified_timestamp"));
    }

    public final boolean equals(Object obj) {
        return InvitationEntity.a(this, obj);
    }

    public final int f() {
        return c("type");
    }

    public final int g() {
        return c("variant");
    }

    public final int h() {
        if (!d("has_automatch_criteria")) {
            return 0;
        }
        return c("automatch_max_players");
    }

    public final int hashCode() {
        return InvitationEntity.a(this);
    }

    public final ArrayList<Participant> i() {
        return this.e;
    }

    public final String toString() {
        return InvitationEntity.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((InvitationEntity) ((Invitation) a())).writeToParcel(parcel, i);
    }
}
