package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.common.internal.f;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.signin.e;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.locks.Lock;

public final class ag extends d implements ba {

    /* renamed from: a  reason: collision with root package name */
    final Queue<c.a<?, ?>> f1391a = new LinkedList();

    /* renamed from: b  reason: collision with root package name */
    final Map<a.c<?>, a.f> f1392b;
    Set<Scope> c = new HashSet();
    Set<bj> d = null;
    final bm e;
    private final Lock f;
    private boolean g;
    private final f h;
    private az i = null;
    private final int j;
    private final Context k;
    private final Looper l;
    private volatile boolean m;
    private long n = 120000;
    private long o = 5000;
    private final aj p;
    private final com.google.android.gms.common.c q;
    private zabq r;
    private final b s;
    private final Map<a<?>, Boolean> t;
    private final a.C0111a<? extends e, com.google.android.gms.signin.a> u;
    private final i v = new i();
    private final ArrayList<ca> w;
    private Integer x = null;
    private final f.a y = new ah(this);

    public ag(Context context, Lock lock, Looper looper, b bVar, com.google.android.gms.common.c cVar, a.C0111a<? extends e, com.google.android.gms.signin.a> aVar, Map<a<?>, Boolean> map, List<d.b> list, List<d.c> list2, Map<a.c<?>, a.f> map2, int i2, int i3, ArrayList<ca> arrayList) {
        this.k = context;
        this.f = lock;
        this.g = false;
        this.h = new f(looper, this.y);
        this.l = looper;
        this.p = new aj(this, looper);
        this.q = cVar;
        this.j = i2;
        if (this.j >= 0) {
            this.x = Integer.valueOf(i3);
        }
        this.t = map;
        this.f1392b = map2;
        this.w = arrayList;
        this.e = new bm(this.f1392b);
        for (d.b next : list) {
            f fVar = this.h;
            l.a(next);
            synchronized (fVar.i) {
                if (fVar.f1602b.contains(next)) {
                    String valueOf = String.valueOf(next);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 62);
                    sb.append("registerConnectionCallbacks(): listener ");
                    sb.append(valueOf);
                    sb.append(" is already registered");
                    sb.toString();
                } else {
                    fVar.f1602b.add(next);
                }
            }
            if (fVar.f1601a.b()) {
                fVar.h.sendMessage(fVar.h.obtainMessage(1, next));
            }
        }
        for (d.c a2 : list2) {
            this.h.a(a2);
        }
        this.s = bVar;
        this.u = aVar;
    }

    private static String c(int i2) {
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? "UNKNOWN" : "SIGN_IN_MODE_NONE" : "SIGN_IN_MODE_OPTIONAL" : "SIGN_IN_MODE_REQUIRED";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
     arg types: [C, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T */
    public final <C extends a.f> C a(a.c<C> cVar) {
        C c2 = (a.f) this.f1392b.get(cVar);
        l.a((Object) c2, (Object) "Appropriate Api was not requested.");
        return c2;
    }

    public final boolean a(a<?> aVar) {
        return this.f1392b.containsKey(aVar.b());
    }

    public final boolean b(a<?> aVar) {
        a.f fVar;
        if (e() && (fVar = this.f1392b.get(aVar.b())) != null && fVar.b()) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.ag.a(java.lang.Iterable<com.google.android.gms.common.api.a$f>, boolean):int
     arg types: [java.util.Collection<com.google.android.gms.common.api.a$f>, int]
     candidates:
      com.google.android.gms.common.api.internal.ag.a(int, boolean):void
      com.google.android.gms.common.api.internal.ba.a(int, boolean):void
      com.google.android.gms.common.api.internal.ag.a(java.lang.Iterable<com.google.android.gms.common.api.a$f>, boolean):int */
    public final void b() {
        this.f.lock();
        try {
            boolean z = false;
            if (this.j >= 0) {
                if (this.x != null) {
                    z = true;
                }
                l.a(z, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.x == null) {
                this.x = Integer.valueOf(a((Iterable<a.f>) this.f1392b.values(), false));
            } else if (this.x.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            a(this.x.intValue());
        } finally {
            this.f.unlock();
        }
    }

    public final void a(int i2) {
        this.f.lock();
        boolean z = true;
        if (!(i2 == 3 || i2 == 1 || i2 == 2)) {
            z = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i2);
            l.b(z, sb.toString());
            b(i2);
            k();
        } finally {
            this.f.unlock();
        }
    }

    public final void c() {
        this.f.lock();
        try {
            this.e.a();
            if (this.i != null) {
                this.i.b();
            }
            i iVar = this.v;
            for (h<?> hVar : iVar.f1480a) {
                hVar.f1477a = null;
            }
            iVar.f1480a.clear();
            for (c.a next : this.f1391a) {
                next.a((bo) null);
                next.b();
            }
            this.f1391a.clear();
            if (this.i != null) {
                h();
                this.h.a();
                this.f.unlock();
            }
        } finally {
            this.f.unlock();
        }
    }

    public final void d() {
        c();
        b();
    }

    public final boolean e() {
        az azVar = this.i;
        return azVar != null && azVar.d();
    }

    public final boolean f() {
        az azVar = this.i;
        return azVar != null && azVar.e();
    }

    private final void b(int i2) {
        Integer num = this.x;
        if (num == null) {
            this.x = Integer.valueOf(i2);
        } else if (num.intValue() != i2) {
            String c2 = c(i2);
            String c3 = c(this.x.intValue());
            StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 51 + String.valueOf(c3).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(c2);
            sb.append(". Mode was already set to ");
            sb.append(c3);
            throw new IllegalStateException(sb.toString());
        }
        if (this.i == null) {
            boolean z = false;
            for (a.f d2 : this.f1392b.values()) {
                if (d2.d()) {
                    z = true;
                }
            }
            int intValue = this.x.intValue();
            if (intValue != 1) {
                if (intValue == 2) {
                    if (z) {
                        if (this.g) {
                            this.i = new cg(this.k, this.f, this.l, this.q, this.f1392b, this.s, this.t, this.u, this.w, this, true);
                            return;
                        } else {
                            this.i = cc.a(this.k, this, this.f, this.l, this.q, this.f1392b, this.s, this.t, this.u, this.w);
                            return;
                        }
                    }
                }
            } else if (!z) {
                throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
            }
            if (this.g) {
                this.i = new cg(this.k, this.f, this.l, this.q, this.f1392b, this.s, this.t, this.u, this.w, this, false);
            } else {
                this.i = new am(this.k, this, this.f, this.l, this.q, this.f1392b, this.s, this.t, this.u, this.w, this);
            }
        }
    }

    private final void k() {
        this.h.e = true;
        this.i.a();
    }

    /* access modifiers changed from: package-private */
    public final boolean h() {
        if (!this.m) {
            return false;
        }
        this.m = false;
        this.p.removeMessages(2);
        this.p.removeMessages(1);
        zabq zabq = this.r;
        if (zabq != null) {
            zabq.a();
            this.r = null;
        }
        return true;
    }

    public final void a(d.c cVar) {
        this.h.a(cVar);
    }

    public final void b(d.c cVar) {
        f fVar = this.h;
        l.a(cVar);
        synchronized (fVar.i) {
            if (!fVar.d.remove(cVar)) {
                String valueOf = String.valueOf(cVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 57);
                sb.append("unregisterConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" not found");
                sb.toString();
            }
        }
    }

    public final void a(Bundle bundle) {
        while (!this.f1391a.isEmpty()) {
            a(this.f1391a.remove());
        }
        f fVar = this.h;
        boolean z = true;
        l.a(Looper.myLooper() == fVar.h.getLooper(), "onConnectionSuccess must only be called on the Handler thread");
        synchronized (fVar.i) {
            l.a(!fVar.g);
            fVar.h.removeMessages(1);
            fVar.g = true;
            if (fVar.c.size() != 0) {
                z = false;
            }
            l.a(z);
            ArrayList arrayList = new ArrayList(fVar.f1602b);
            int i2 = fVar.f.get();
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList2.get(i3);
                i3++;
                d.b bVar = (d.b) obj;
                if (!fVar.e || !fVar.f1601a.b() || fVar.f.get() != i2) {
                    break;
                } else if (!fVar.c.contains(bVar)) {
                    bVar.onConnected(bundle);
                }
            }
            fVar.c.clear();
            fVar.g = false;
        }
    }

    public final void a(ConnectionResult connectionResult) {
        if (!com.google.android.gms.common.d.b(this.k, connectionResult.f1353b)) {
            h();
        }
        if (!this.m) {
            f fVar = this.h;
            int i2 = 0;
            l.a(Looper.myLooper() == fVar.h.getLooper(), "onConnectionFailure must only be called on the Handler thread");
            fVar.h.removeMessages(1);
            synchronized (fVar.i) {
                ArrayList arrayList = new ArrayList(fVar.d);
                int i3 = fVar.f.get();
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    Object obj = arrayList2.get(i2);
                    i2++;
                    d.c cVar = (d.c) obj;
                    if (!fVar.e) {
                        break;
                    } else if (fVar.f.get() != i3) {
                        break;
                    } else if (fVar.d.contains(cVar)) {
                        cVar.onConnectionFailed(connectionResult);
                    }
                }
            }
            this.h.a();
        }
    }

    public final void a(int i2, boolean z) {
        if (i2 == 1 && !z && !this.m) {
            this.m = true;
            if (this.r == null) {
                this.r = this.q.a(this.k.getApplicationContext(), new ak(this));
            }
            aj ajVar = this.p;
            ajVar.sendMessageDelayed(ajVar.obtainMessage(1), this.n);
            aj ajVar2 = this.p;
            ajVar2.sendMessageDelayed(ajVar2.obtainMessage(2), this.o);
        }
        for (BasePendingResult b2 : (BasePendingResult[]) this.e.c.toArray(bm.f1428b)) {
            b2.b(bm.f1427a);
        }
        f fVar = this.h;
        l.a(Looper.myLooper() == fVar.h.getLooper(), "onUnintentionalDisconnection must only be called on the Handler thread");
        fVar.h.removeMessages(1);
        synchronized (fVar.i) {
            fVar.g = true;
            ArrayList arrayList = new ArrayList(fVar.f1602b);
            int i3 = fVar.f.get();
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i4 = 0;
            while (i4 < size) {
                Object obj = arrayList2.get(i4);
                i4++;
                d.b bVar = (d.b) obj;
                if (!fVar.e || fVar.f.get() != i3) {
                    break;
                } else if (fVar.f1602b.contains(bVar)) {
                    bVar.onConnectionSuspended(i2);
                }
            }
            fVar.c.clear();
            fVar.g = false;
        }
        this.h.a();
        if (i2 == 2) {
            k();
        }
    }

    public final Looper a() {
        return this.l;
    }

    public final void a(bj bjVar) {
        this.f.lock();
        try {
            if (this.d == null) {
                this.d = new HashSet();
            }
            this.d.add(bjVar);
        } finally {
            this.f.unlock();
        }
    }

    public final void b(bj bjVar) {
        this.f.lock();
        try {
            if (this.d == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!this.d.remove(bjVar)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!i()) {
                this.i.f();
            }
        } finally {
            this.f.unlock();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public final boolean i() {
        this.f.lock();
        try {
            if (this.d == null) {
                this.f.unlock();
                return false;
            }
            boolean z = !this.d.isEmpty();
            this.f.unlock();
            return z;
        } catch (Throwable th) {
            this.f.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public final String j() {
        StringWriter stringWriter = new StringWriter();
        a("", null, new PrintWriter(stringWriter), null);
        return stringWriter.toString();
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append((CharSequence) "mContext=").println(this.k);
        printWriter.append((CharSequence) str).append((CharSequence) "mResuming=").print(this.m);
        printWriter.append((CharSequence) " mWorkQueue.size()=").print(this.f1391a.size());
        printWriter.append((CharSequence) " mUnconsumedApiCalls.size()=").println(this.e.c.size());
        az azVar = this.i;
        if (azVar != null) {
            azVar.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    public static int a(Iterable<a.f> iterable, boolean z) {
        boolean z2 = false;
        for (a.f d2 : iterable) {
            if (d2.d()) {
                z2 = true;
            }
        }
        if (z2) {
            return 1;
        }
        return 3;
    }

    public final <A extends a.b, T extends c.a<? extends g, A>> T a(T t2) {
        String str;
        l.b(t2.f1447a != null, "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.f1392b.containsKey(t2.f1447a);
        if (t2.f1448b != null) {
            str = t2.f1448b.f1372b;
        } else {
            str = "the API";
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(str);
        sb.append(" required for this call.");
        l.b(containsKey, sb.toString());
        this.f.lock();
        try {
            if (this.i == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            } else if (this.m) {
                this.f1391a.add(t2);
                while (!this.f1391a.isEmpty()) {
                    c.a remove = this.f1391a.remove();
                    this.e.a(remove);
                    remove.a(Status.c);
                }
                return t2;
            } else {
                T a2 = this.i.a(t2);
                this.f.unlock();
                return a2;
            }
        } finally {
            this.f.unlock();
        }
    }

    static /* synthetic */ void a(ag agVar) {
        agVar.f.lock();
        try {
            if (agVar.m) {
                agVar.k();
            }
        } finally {
            agVar.f.unlock();
        }
    }

    static /* synthetic */ void b(ag agVar) {
        agVar.f.lock();
        try {
            if (agVar.h()) {
                agVar.k();
            }
        } finally {
            agVar.f.unlock();
        }
    }
}
