package com.google.android.gms.internal.measurement;

import android.content.ContentResolver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.support.v4.util.ArrayMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class dh implements dl {

    /* renamed from: a  reason: collision with root package name */
    static final Map<Uri, dh> f2161a = new ArrayMap();
    private static final String[] g = {"key", "value"};

    /* renamed from: b  reason: collision with root package name */
    final Object f2162b = new Object();
    volatile Map<String, String> c;
    final List<Object> d = new ArrayList();
    private final ContentResolver e;
    private final Uri f;

    private dh(ContentResolver contentResolver, Uri uri) {
        this.e = contentResolver;
        this.f = uri;
        this.e.registerContentObserver(uri, false, new dj(this));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:2|3|(5:5|6|7|8|9)|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0018 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.measurement.dh a(android.content.ContentResolver r3, android.net.Uri r4) {
        /*
            java.lang.Class<com.google.android.gms.internal.measurement.dh> r0 = com.google.android.gms.internal.measurement.dh.class
            monitor-enter(r0)
            java.util.Map<android.net.Uri, com.google.android.gms.internal.measurement.dh> r1 = com.google.android.gms.internal.measurement.dh.f2161a     // Catch:{ all -> 0x001a }
            java.lang.Object r1 = r1.get(r4)     // Catch:{ all -> 0x001a }
            com.google.android.gms.internal.measurement.dh r1 = (com.google.android.gms.internal.measurement.dh) r1     // Catch:{ all -> 0x001a }
            if (r1 != 0) goto L_0x0018
            com.google.android.gms.internal.measurement.dh r2 = new com.google.android.gms.internal.measurement.dh     // Catch:{ SecurityException -> 0x0018 }
            r2.<init>(r3, r4)     // Catch:{ SecurityException -> 0x0018 }
            java.util.Map<android.net.Uri, com.google.android.gms.internal.measurement.dh> r3 = com.google.android.gms.internal.measurement.dh.f2161a     // Catch:{ SecurityException -> 0x0017 }
            r3.put(r4, r2)     // Catch:{ SecurityException -> 0x0017 }
        L_0x0017:
            r1 = r2
        L_0x0018:
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
            return r1
        L_0x001a:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.dh.a(android.content.ContentResolver, android.net.Uri):com.google.android.gms.internal.measurement.dh");
    }

    public final Map<String, String> a() {
        Map<String, String> map = this.c;
        if (map == null) {
            synchronized (this.f2162b) {
                map = this.c;
                if (map == null) {
                    map = c();
                    this.c = map;
                }
            }
        }
        if (map != null) {
            return map;
        }
        return Collections.emptyMap();
    }

    private final Map<String, String> c() {
        try {
            return (Map) dm.a(new di(this));
        } catch (SQLiteException | SecurityException unused) {
            return null;
        }
    }

    public final /* synthetic */ Object a(String str) {
        return a().get(str);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Map b() {
        Map map;
        Cursor query = this.e.query(this.f, g, null, null, null);
        if (query == null) {
            return Collections.emptyMap();
        }
        try {
            int count = query.getCount();
            if (count == 0) {
                return Collections.emptyMap();
            }
            if (count <= 256) {
                map = new ArrayMap(count);
            } else {
                map = new HashMap(count, 1.0f);
            }
            while (query.moveToNext()) {
                map.put(query.getString(0), query.getString(1));
            }
            query.close();
            return map;
        } finally {
            query.close();
        }
    }
}
