package com.google.android.gms.common.internal;

import android.content.Intent;
import com.google.android.gms.common.api.internal.g;

final class s extends d {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Intent f1619a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ g f1620b;
    private final /* synthetic */ int c;

    s(Intent intent, g gVar, int i) {
        this.f1619a = intent;
        this.f1620b = gVar;
        this.c = i;
    }

    public final void a() {
        Intent intent = this.f1619a;
        if (intent != null) {
            this.f1620b.startActivityForResult(intent, this.c);
        }
    }
}
