package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.zzu;

public final class zzo extends zzu implements DriveEvent {
    public static final Parcelable.Creator<zzo> CREATOR = new k();

    /* renamed from: a  reason: collision with root package name */
    public final DataHolder f1722a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f1723b;
    private final int c;

    public zzo(DataHolder dataHolder, boolean z, int i) {
        this.f1722a = dataHolder;
        this.f1723b = z;
        this.c = i;
    }

    public final int a() {
        return 3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.data.DataHolder, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void a(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, (Parcelable) this.f1722a, i, false);
        a.a(parcel, 3, this.f1723b);
        a.b(parcel, 4, this.c);
        a.b(parcel, a2);
    }
}
