package com.google.android.gms.games.quest;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Game;
import java.util.List;

@Deprecated
public interface Quest extends Parcelable, e<Quest> {
    public static final int[] QUEST_STATE_ALL = {1, 2, 3, 4, 6, 5};
    public static final String[] QUEST_STATE_NON_TERMINAL = {Integer.toString(1), Integer.toString(2), Integer.toString(3)};

    String b();

    String c();

    String d();

    Uri e();

    Uri f();

    List<Milestone> g();

    @Deprecated
    String getBannerImageUrl();

    @Deprecated
    String getIconImageUrl();

    Game h();

    int i();

    int j();

    long k();

    long l();

    long m();

    long n();

    long o();
}
