package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class u<TResult> implements y<TResult> {

    /* renamed from: a  reason: collision with root package name */
    final Object f2702a = new Object();

    /* renamed from: b  reason: collision with root package name */
    e<? super TResult> f2703b;
    private final Executor c;

    public u(Executor executor, e<? super TResult> eVar) {
        this.c = executor;
        this.f2703b = eVar;
    }

    public final void a(g<TResult> gVar) {
        if (gVar.b()) {
            synchronized (this.f2702a) {
                if (this.f2703b != null) {
                    this.c.execute(new v(this, gVar));
                }
            }
        }
    }

    public final void a() {
        synchronized (this.f2702a) {
            this.f2703b = null;
        }
    }
}
