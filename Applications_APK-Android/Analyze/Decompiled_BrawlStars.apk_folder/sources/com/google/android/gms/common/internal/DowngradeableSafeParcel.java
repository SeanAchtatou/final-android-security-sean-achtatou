package com.google.android.gms.common.internal;

import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public abstract class DowngradeableSafeParcel extends AbstractSafeParcelable implements ReflectedParcelable {

    /* renamed from: b  reason: collision with root package name */
    private static final Object f1574b = new Object();
    private static ClassLoader c = null;
    private static Integer d = null;

    /* renamed from: a  reason: collision with root package name */
    protected boolean f1575a = false;

    public static Integer b_() {
        synchronized (f1574b) {
        }
        return null;
    }
}
