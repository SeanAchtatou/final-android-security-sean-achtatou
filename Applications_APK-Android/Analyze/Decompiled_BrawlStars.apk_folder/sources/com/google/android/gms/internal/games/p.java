package com.google.android.gms.internal.games;

import android.content.Intent;
import com.google.android.gms.common.api.d;
import com.google.android.gms.games.video.a;

public final class p implements a {
    public final Intent a(d dVar) {
        return com.google.android.gms.games.a.a(dVar).u();
    }

    public final boolean b(d dVar) {
        return com.google.android.gms.games.a.a(dVar).v();
    }
}
