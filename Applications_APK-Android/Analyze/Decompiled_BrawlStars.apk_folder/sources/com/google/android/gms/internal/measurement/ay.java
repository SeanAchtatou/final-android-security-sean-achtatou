package com.google.android.gms.internal.measurement;

import android.util.DisplayMetrics;
import java.util.Locale;

public final class ay extends r {
    ay(t tVar) {
        super(tVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    public final c b() {
        m();
        DisplayMetrics displayMetrics = this.c.b().f1331a.getResources().getDisplayMetrics();
        c cVar = new c();
        cVar.f2106a = bx.a(Locale.getDefault());
        cVar.c = displayMetrics.widthPixels;
        cVar.d = displayMetrics.heightPixels;
        return cVar;
    }

    public final String c() {
        m();
        c b2 = b();
        int i = b2.c;
        int i2 = b2.d;
        StringBuilder sb = new StringBuilder(23);
        sb.append(i);
        sb.append("x");
        sb.append(i2);
        return sb.toString();
    }
}
