package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.InputDeviceCompat;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import java.util.Arrays;

public final class zzr extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzr> CREATOR = new k();

    /* renamed from: a  reason: collision with root package name */
    private String f1771a;

    /* renamed from: b  reason: collision with root package name */
    private int f1772b;
    private String c;
    private String d;
    private int e;
    private boolean f;

    public zzr(String str, int i, String str2, String str3, int i2, boolean z) {
        this.f1771a = str;
        this.f1772b = i;
        this.c = str2;
        this.d = str3;
        this.e = i2;
        this.f = z;
    }

    private static boolean a(int i) {
        switch (i) {
            case 256:
            case InputDeviceCompat.SOURCE_KEYBOARD:
            case 258:
                return true;
            default:
                return false;
        }
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == getClass()) {
            if (obj == this) {
                return true;
            }
            zzr zzr = (zzr) obj;
            return j.a(this.f1771a, zzr.f1771a) && this.f1772b == zzr.f1772b && this.e == zzr.e && this.f == zzr.f;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        boolean z = false;
        a.a(parcel, 2, !a(this.f1772b) ? null : this.f1771a, false);
        int i2 = -1;
        a.b(parcel, 3, !a(this.f1772b) ? -1 : this.f1772b);
        a.a(parcel, 4, this.c, false);
        a.a(parcel, 5, this.d, false);
        int i3 = this.e;
        if (i3 == 0 || i3 == 1 || i3 == 2 || i3 == 3) {
            z = true;
        }
        if (z) {
            i2 = this.e;
        }
        a.b(parcel, 6, i2);
        a.a(parcel, 7, this.f);
        a.b(parcel, a2);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f1771a, Integer.valueOf(this.f1772b), Integer.valueOf(this.e), Boolean.valueOf(this.f)});
    }
}
