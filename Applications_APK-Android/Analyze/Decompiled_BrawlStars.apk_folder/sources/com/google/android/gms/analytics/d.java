package com.google.android.gms.analytics;

import android.support.v7.widget.ActivityChooserView;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.measurement.bi;
import com.google.android.gms.internal.measurement.bx;
import com.google.android.gms.internal.measurement.r;
import com.google.android.gms.internal.measurement.t;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class d extends r {

    /* renamed from: a  reason: collision with root package name */
    final bi f1319a;

    /* renamed from: b  reason: collision with root package name */
    final a f1320b;
    private boolean d;
    private final Map<String, String> e = new HashMap();
    private final Map<String, String> f = new HashMap();

    d(t tVar, String str) {
        super(tVar);
        if (str != null) {
            this.e.put("&tid", str);
        }
        this.e.put("useSecure", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        this.e.put("&a", Integer.toString(new Random().nextInt(ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED) + 1));
        this.f1319a = new bi("tracking", this.c.c, (byte) 0);
        this.f1320b = new a(tVar);
    }

    class a extends r {

        /* renamed from: a  reason: collision with root package name */
        private long f1321a = -1;

        /* renamed from: b  reason: collision with root package name */
        private boolean f1322b;

        protected a(t tVar) {
            super(tVar);
        }

        public final void a() {
        }

        public final synchronized boolean b() {
            boolean z;
            z = this.f1322b;
            this.f1322b = false;
            return z;
        }
    }

    public final void a() {
        this.f1320b.n();
        String c = this.c.e().c();
        if (c != null) {
            a("&an", c);
        }
        String b2 = this.c.e().b();
        if (b2 != null) {
            a("&av", b2);
        }
    }

    private static String a(Map.Entry<String, String> entry) {
        String key = entry.getKey();
        if (!(key.startsWith("&") && key.length() >= 2)) {
            return null;
        }
        return entry.getKey().substring(1);
    }

    private static void a(Map<String, String> map, Map<String, String> map2) {
        l.a(map2);
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                String a2 = a((Map.Entry<String, String>) next);
                if (a2 != null) {
                    map2.put(a2, (String) next.getValue());
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T */
    public final void a(String str, String str2) {
        l.a((Object) str, (Object) "Key should be non-null");
        if (!TextUtils.isEmpty(str)) {
            this.e.put(str, str2);
        }
    }

    public final void a(double d2) {
        a("&sf", Double.toString(1.0d));
    }

    public final void a(String str) {
        a("&cd", str);
    }

    public final void a(Map<String, String> map) {
        long a2 = this.c.c.a();
        if (this.c.d().c) {
            c("AppOptOut is set to true. Not sending Google Analytics hit");
            return;
        }
        boolean z = this.c.d().f1313b;
        HashMap hashMap = new HashMap();
        a(this.e, hashMap);
        a(map, hashMap);
        boolean c = bx.c(this.e.get("useSecure"));
        Map<String, String> map2 = this.f;
        l.a(hashMap);
        if (map2 != null) {
            for (Map.Entry next : map2.entrySet()) {
                String a3 = a((Map.Entry<String, String>) next);
                if (a3 != null && !hashMap.containsKey(a3)) {
                    hashMap.put(a3, (String) next.getValue());
                }
            }
        }
        this.f.clear();
        String str = (String) hashMap.get("t");
        if (TextUtils.isEmpty(str)) {
            this.c.a().a(hashMap, "Missing hit type parameter");
            return;
        }
        String str2 = (String) hashMap.get("tid");
        if (TextUtils.isEmpty(str2)) {
            this.c.a().a(hashMap, "Missing tracking id parameter");
            return;
        }
        boolean z2 = this.d;
        synchronized (this) {
            if ("screenview".equalsIgnoreCase(str) || "pageview".equalsIgnoreCase(str) || "appview".equalsIgnoreCase(str) || TextUtils.isEmpty(str)) {
                int parseInt = Integer.parseInt(this.e.get("&a")) + 1;
                if (parseInt >= Integer.MAX_VALUE) {
                    parseInt = 1;
                }
                this.e.put("&a", Integer.toString(parseInt));
            }
        }
        this.c.b().a(new r(this, hashMap, z2, str, a2, z, c, str2));
    }
}
