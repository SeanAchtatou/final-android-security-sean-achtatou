package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.DriveId;

public final class c implements Parcelable.Creator<ChangeEvent> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new ChangeEvent[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        DriveId driveId = null;
        int i = 0;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 2) {
                driveId = (DriveId) SafeParcelReader.a(parcel, readInt, DriveId.CREATOR);
            } else if (i2 != 3) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                i = SafeParcelReader.d(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new ChangeEvent(driveId, i);
    }
}
