package com.google.android.gms.internal.measurement;

import android.os.Binder;

public final /* synthetic */ class dm {
    public static <V> V a(dn<V> dnVar) {
        long clearCallingIdentity;
        try {
            return dnVar.a();
        } catch (SecurityException unused) {
            clearCallingIdentity = Binder.clearCallingIdentity();
            V a2 = dnVar.a();
            Binder.restoreCallingIdentity(clearCallingIdentity);
            return a2;
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(clearCallingIdentity);
            throw th;
        }
    }
}
