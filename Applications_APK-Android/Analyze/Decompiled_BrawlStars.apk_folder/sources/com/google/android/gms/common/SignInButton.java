package com.google.android.gms.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.base.R;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.SignInButtonImpl;
import com.google.android.gms.common.internal.m;
import com.google.android.gms.dynamic.RemoteCreator;

public final class SignInButton extends FrameLayout implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private int f1357a;

    /* renamed from: b  reason: collision with root package name */
    private int f1358b;
    private View c;
    private View.OnClickListener d;

    public SignInButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: finally extract failed */
    public SignInButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.d = null;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.SignInButton, 0, 0);
        try {
            this.f1357a = obtainStyledAttributes.getInt(R.styleable.SignInButton_buttonSize, 0);
            this.f1358b = obtainStyledAttributes.getInt(R.styleable.SignInButton_colorScheme, 2);
            obtainStyledAttributes.recycle();
            a(this.f1357a, this.f1358b);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    public final void setSize(int i) {
        a(i, this.f1358b);
    }

    public final void setColorScheme(int i) {
        a(this.f1357a, i);
    }

    @Deprecated
    public final void setScopes(Scope[] scopeArr) {
        a(this.f1357a, this.f1358b);
    }

    private void a(int i, int i2) {
        this.f1357a = i;
        this.f1358b = i2;
        Context context = getContext();
        View view = this.c;
        if (view != null) {
            removeView(view);
        }
        try {
            this.c = m.a(context, this.f1357a, this.f1358b);
        } catch (RemoteCreator.RemoteCreatorException unused) {
            int i3 = this.f1357a;
            int i4 = this.f1358b;
            SignInButtonImpl signInButtonImpl = new SignInButtonImpl(context);
            signInButtonImpl.a(context.getResources(), i3, i4);
            this.c = signInButtonImpl;
        }
        addView(this.c);
        this.c.setEnabled(isEnabled());
        this.c.setOnClickListener(this);
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.d = onClickListener;
        View view = this.c;
        if (view != null) {
            view.setOnClickListener(this);
        }
    }

    public final void setEnabled(boolean z) {
        super.setEnabled(z);
        this.c.setEnabled(z);
    }

    public final void onClick(View view) {
        View.OnClickListener onClickListener = this.d;
        if (onClickListener != null && view == this.c) {
            onClickListener.onClick(this);
        }
    }
}
