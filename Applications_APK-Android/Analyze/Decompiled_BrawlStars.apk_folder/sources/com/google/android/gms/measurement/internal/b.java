package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import com.google.android.gms.common.util.e;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public final class b extends bn {

    /* renamed from: a  reason: collision with root package name */
    private long f2405a;

    /* renamed from: b  reason: collision with root package name */
    private String f2406b;
    private Boolean c;

    b(ar arVar) {
        super(arVar);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        Calendar instance = Calendar.getInstance();
        this.f2405a = TimeUnit.MINUTES.convert((long) (instance.get(15) + instance.get(16)), TimeUnit.MILLISECONDS);
        Locale locale = Locale.getDefault();
        String lowerCase = locale.getLanguage().toLowerCase(Locale.ENGLISH);
        String lowerCase2 = locale.getCountry().toLowerCase(Locale.ENGLISH);
        StringBuilder sb = new StringBuilder(String.valueOf(lowerCase).length() + 1 + String.valueOf(lowerCase2).length());
        sb.append(lowerCase);
        sb.append("-");
        sb.append(lowerCase2);
        this.f2406b = sb.toString();
        return false;
    }

    public final long e_() {
        w();
        return this.f2405a;
    }

    public final String f() {
        w();
        return this.f2406b;
    }

    public final boolean a(Context context) {
        if (this.c == null) {
            this.c = false;
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager != null) {
                    packageManager.getPackageInfo("com.google.android.gms", 128);
                    this.c = true;
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return this.c.booleanValue();
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }
}
