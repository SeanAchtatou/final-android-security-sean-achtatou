package com.google.android.gms.common.data;

import java.util.Iterator;

public abstract class a<T> implements b<T> {

    /* renamed from: a  reason: collision with root package name */
    protected final DataHolder f1529a;

    protected a(DataHolder dataHolder) {
        this.f1529a = dataHolder;
    }

    public final int b() {
        DataHolder dataHolder = this.f1529a;
        if (dataHolder == null) {
            return 0;
        }
        return dataHolder.f;
    }

    public Iterator<T> iterator() {
        return new c(this);
    }

    public void a() {
        DataHolder dataHolder = this.f1529a;
        if (dataHolder != null) {
            dataHolder.close();
        }
    }
}
