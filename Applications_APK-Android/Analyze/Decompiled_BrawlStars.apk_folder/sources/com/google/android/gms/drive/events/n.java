package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.drive.zzh;
import java.util.ArrayList;

public final class n implements Parcelable.Creator<zzv> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzv[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        ArrayList arrayList = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 3) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                arrayList = SafeParcelReader.c(parcel, readInt, zzh.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzv(arrayList);
    }
}
