package com.google.android.gms.analytics;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.internal.measurement.bk;
import com.google.android.gms.internal.measurement.br;
import com.google.android.gms.internal.measurement.bt;
import com.google.android.gms.internal.measurement.bv;
import com.google.android.gms.internal.measurement.t;

public final class AnalyticsJobService extends JobService implements bv {

    /* renamed from: a  reason: collision with root package name */
    private br<AnalyticsJobService> f1308a;

    private final br<AnalyticsJobService> a() {
        if (this.f1308a == null) {
            this.f1308a = new br<>(this);
        }
        return this.f1308a;
    }

    public final boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    public final void onCreate() {
        super.onCreate();
        a().a();
    }

    public final void onDestroy() {
        a().b();
        super.onDestroy();
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        return a().a(intent, i2);
    }

    public final boolean onStartJob(JobParameters jobParameters) {
        br<AnalyticsJobService> a2 = a();
        bk a3 = t.a((Context) a2.f2092b).a();
        String string = jobParameters.getExtras().getString(NativeProtocol.WEB_DIALOG_ACTION);
        a3.a("Local AnalyticsJobService called. action", string);
        if (!"com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(string)) {
            return true;
        }
        a2.a(new bt(a2, a3, jobParameters));
        return true;
    }

    public final boolean a(int i) {
        return stopSelfResult(i);
    }

    public final void a(JobParameters jobParameters) {
        jobFinished(jobParameters, false);
    }
}
