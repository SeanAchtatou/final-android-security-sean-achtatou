package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.IInterface;
import android.os.Looper;
import android.support.v7.widget.ActivityChooserView;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.l;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public final class a<O extends d> {

    /* renamed from: a  reason: collision with root package name */
    public final C0111a<?, O> f1371a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1372b;
    private final i<?, O> c = null;
    private final g<?> d;
    private final j<?> e;

    /* renamed from: com.google.android.gms.common.api.a$a  reason: collision with other inner class name */
    public static abstract class C0111a<T extends f, O> extends e<T, O> {
        public abstract T a(Context context, Looper looper, com.google.android.gms.common.internal.b bVar, O o, d.b bVar2, d.c cVar);
    }

    public interface b {
    }

    public static class c<C extends b> {
    }

    public interface d {

        /* renamed from: com.google.android.gms.common.api.a$d$a  reason: collision with other inner class name */
        public interface C0112a extends c, C0113d {
            Account a();
        }

        public interface b extends c {
            GoogleSignInAccount a();
        }

        public interface c extends d {
        }

        /* renamed from: com.google.android.gms.common.api.a$d$d  reason: collision with other inner class name */
        public interface C0113d extends d {
        }

        public interface e extends c, C0113d {
        }
    }

    public interface f extends b {
        void a();

        void a(BaseGmsClient.c cVar);

        void a(BaseGmsClient.e eVar);

        void a(IAccountAccessor iAccountAccessor, Set<Scope> set);

        void a(String str, PrintWriter printWriter);

        boolean b();

        boolean c();

        boolean d();

        Intent e();

        String f();

        int g();

        Feature[] h();
    }

    public static final class g<C extends f> extends c<C> {
    }

    public interface h<T extends IInterface> extends b {
        String a();

        String b();

        T c();
    }

    public static abstract class i<T extends h, O> extends e<T, O> {
    }

    public static final class j<C extends h> extends c<C> {
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.google.android.gms.common.api.a$a<C, O>, java.lang.Object, com.google.android.gms.common.api.a$a<?, O>] */
    /* JADX WARN: Type inference failed for: r4v0, types: [com.google.android.gms.common.api.a$g<C>, java.lang.Object, com.google.android.gms.common.api.a$g<?>] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <C extends com.google.android.gms.common.api.a.f> a(java.lang.String r2, com.google.android.gms.common.api.a.C0111a<C, O> r3, com.google.android.gms.common.api.a.g<C> r4) {
        /*
            r1 = this;
            r1.<init>()
            java.lang.String r0 = "Cannot construct an Api with a null ClientBuilder"
            com.google.android.gms.common.internal.l.a(r3, r0)
            java.lang.String r0 = "Cannot construct an Api with a null ClientKey"
            com.google.android.gms.common.internal.l.a(r4, r0)
            r1.f1372b = r2
            r1.f1371a = r3
            r2 = 0
            r1.c = r2
            r1.d = r4
            r1.e = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.a.<init>(java.lang.String, com.google.android.gms.common.api.a$a, com.google.android.gms.common.api.a$g):void");
    }

    public static abstract class e<T extends b, O> {
        public int a() {
            return ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        }

        public List<Scope> b() {
            return Collections.emptyList();
        }
    }

    public final C0111a<?, O> a() {
        l.a(this.f1371a != null, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.f1371a;
    }

    public final c<?> b() {
        g<?> gVar = this.d;
        if (gVar != null) {
            return gVar;
        }
        throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
    }
}
