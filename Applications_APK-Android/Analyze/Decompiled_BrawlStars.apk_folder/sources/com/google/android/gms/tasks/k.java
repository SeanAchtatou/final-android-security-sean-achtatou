package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class k<TResult, TContinuationResult> implements y<TResult> {

    /* renamed from: a  reason: collision with root package name */
    final a<TResult, TContinuationResult> f2683a;

    /* renamed from: b  reason: collision with root package name */
    final ab<TContinuationResult> f2684b;
    private final Executor c;

    public k(Executor executor, a<TResult, TContinuationResult> aVar, ab<TContinuationResult> abVar) {
        this.c = executor;
        this.f2683a = aVar;
        this.f2684b = abVar;
    }

    public final void a(g<TResult> gVar) {
        this.c.execute(new l(this, gVar));
    }

    public final void a() {
        throw new UnsupportedOperationException();
    }
}
