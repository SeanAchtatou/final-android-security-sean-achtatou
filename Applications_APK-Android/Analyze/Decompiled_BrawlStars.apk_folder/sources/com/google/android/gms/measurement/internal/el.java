package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.common.c.c;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;
import com.google.android.gms.common.util.p;
import com.google.android.gms.measurement.internal.h;
import java.lang.reflect.InvocationTargetException;

public final class el extends bm {

    /* renamed from: a  reason: collision with root package name */
    en f2550a = em.f2552a;

    /* renamed from: b  reason: collision with root package name */
    private Boolean f2551b;
    private Boolean c;

    el(ar arVar) {
        super(arVar);
        h.a(arVar);
    }

    static String d() {
        return h.i.a();
    }

    public final int a(String str) {
        return b(str, h.w);
    }

    public final boolean e() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    ApplicationInfo applicationInfo = m().getApplicationInfo();
                    String a2 = p.a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.c = Boolean.valueOf(str != null && str.equals(a2));
                    }
                    if (this.c == null) {
                        this.c = Boolean.TRUE;
                        q().c.a("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.c.booleanValue();
    }

    public final long a(String str, h.a<Long> aVar) {
        if (str == null) {
            return aVar.a().longValue();
        }
        String a2 = this.f2550a.a(str, aVar.f2563a);
        if (TextUtils.isEmpty(a2)) {
            return aVar.a().longValue();
        }
        try {
            return aVar.a(Long.valueOf(Long.parseLong(a2))).longValue();
        } catch (NumberFormatException unused) {
            return aVar.a().longValue();
        }
    }

    public final int b(String str, h.a<Integer> aVar) {
        if (str == null) {
            return aVar.a().intValue();
        }
        String a2 = this.f2550a.a(str, aVar.f2563a);
        if (TextUtils.isEmpty(a2)) {
            return aVar.a().intValue();
        }
        try {
            return aVar.a((Long) Integer.valueOf(Integer.parseInt(a2))).intValue();
        } catch (NumberFormatException unused) {
            return aVar.a().intValue();
        }
    }

    public final boolean c(String str, h.a<Boolean> aVar) {
        if (str == null) {
            return aVar.a().booleanValue();
        }
        String a2 = this.f2550a.a(str, aVar.f2563a);
        if (TextUtils.isEmpty(a2)) {
            return aVar.a().booleanValue();
        }
        return aVar.a((Long) Boolean.valueOf(Boolean.parseBoolean(a2))).booleanValue();
    }

    public final boolean a(h.a<Boolean> aVar) {
        return c(null, aVar);
    }

    /* access modifiers changed from: package-private */
    public final Boolean b(String str) {
        l.a(str);
        try {
            if (m().getPackageManager() == null) {
                q().c.a("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo a2 = c.a(m()).a(m().getPackageName(), 128);
            if (a2 == null) {
                q().c.a("Failed to load metadata: ApplicationInfo is null");
                return null;
            } else if (a2.metaData == null) {
                q().c.a("Failed to load metadata: Metadata bundle is null");
                return null;
            } else if (!a2.metaData.containsKey(str)) {
                return null;
            } else {
                return Boolean.valueOf(a2.metaData.getBoolean(str));
            }
        } catch (PackageManager.NameNotFoundException e) {
            q().c.a("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    public final boolean f() {
        Boolean b2 = b("firebase_analytics_collection_deactivated");
        return b2 != null && b2.booleanValue();
    }

    public static long g() {
        return h.L.a().longValue();
    }

    public static long h() {
        return h.l.a().longValue();
    }

    public final String i() {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, "debug.firebase.analytics.app", "");
        } catch (ClassNotFoundException e) {
            q().c.a("Could not find SystemProperties class", e);
            return "";
        } catch (NoSuchMethodException e2) {
            q().c.a("Could not find SystemProperties.get() method", e2);
            return "";
        } catch (IllegalAccessException e3) {
            q().c.a("Could not access SystemProperties.get()", e3);
            return "";
        } catch (InvocationTargetException e4) {
            q().c.a("SystemProperties.get() threw an exception", e4);
            return "";
        }
    }

    public static boolean j() {
        return h.h.a().booleanValue();
    }

    public final boolean c(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(this.f2550a.a(str, "gaia_collection_enabled"));
    }

    /* access modifiers changed from: package-private */
    public final boolean d(String str) {
        return c(str, h.V);
    }

    /* access modifiers changed from: package-private */
    public final boolean e(String str) {
        return c(str, h.aa);
    }

    /* access modifiers changed from: package-private */
    public final boolean f(String str) {
        return c(str, h.ac);
    }

    /* access modifiers changed from: package-private */
    public final boolean g(String str) {
        return c(str, h.ag);
    }

    /* access modifiers changed from: package-private */
    public final boolean t() {
        if (this.f2551b == null) {
            this.f2551b = b("app_measurement_lite");
            if (this.f2551b == null) {
                this.f2551b = false;
            }
        }
        if (this.f2551b.booleanValue() || !this.r.d) {
            return true;
        }
        return false;
    }

    static boolean u() {
        return h.ah.a().booleanValue();
    }

    /* access modifiers changed from: package-private */
    public final boolean h(String str) {
        return c(str, h.ai);
    }

    /* access modifiers changed from: package-private */
    public final boolean i(String str) {
        return c(str, h.aj);
    }

    /* access modifiers changed from: package-private */
    public final boolean j(String str) {
        return c(str, h.ak);
    }

    /* access modifiers changed from: package-private */
    public final boolean k(String str) {
        return c(str, h.al);
    }

    /* access modifiers changed from: package-private */
    public final boolean l(String str) {
        return c(str, h.an);
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }
}
