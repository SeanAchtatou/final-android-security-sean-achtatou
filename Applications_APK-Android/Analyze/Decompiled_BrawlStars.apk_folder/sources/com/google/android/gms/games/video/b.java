package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class b implements Parcelable.Creator<VideoCapabilities> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new VideoCapabilities[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        boolean[] zArr = null;
        boolean[] zArr2 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 1) {
                z = SafeParcelReader.c(parcel, readInt);
            } else if (i == 2) {
                z2 = SafeParcelReader.c(parcel, readInt);
            } else if (i == 3) {
                z3 = SafeParcelReader.c(parcel, readInt);
            } else if (i == 4) {
                zArr = SafeParcelReader.p(parcel, readInt);
            } else if (i != 5) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                zArr2 = SafeParcelReader.p(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new VideoCapabilities(z, z2, z3, zArr, zArr2);
    }
}
