package com.google.android.gms.games;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

public final class PlayerLevelInfo extends zzd {
    public static final Parcelable.Creator<PlayerLevelInfo> CREATOR = new j();

    /* renamed from: a  reason: collision with root package name */
    private final long f1790a;

    /* renamed from: b  reason: collision with root package name */
    private final long f1791b;
    private final PlayerLevel c;
    private final PlayerLevel d;

    public PlayerLevelInfo(long j, long j2, PlayerLevel playerLevel, PlayerLevel playerLevel2) {
        l.a(j != -1);
        l.a(playerLevel);
        l.a(playerLevel2);
        this.f1790a = j;
        this.f1791b = j2;
        this.c = playerLevel;
        this.d = playerLevel2;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof PlayerLevelInfo)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        PlayerLevelInfo playerLevelInfo = (PlayerLevelInfo) obj;
        return j.a(Long.valueOf(this.f1790a), Long.valueOf(playerLevelInfo.f1790a)) && j.a(Long.valueOf(this.f1791b), Long.valueOf(playerLevelInfo.f1791b)) && j.a(this.c, playerLevelInfo.c) && j.a(this.d, playerLevelInfo.d);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.f1790a), Long.valueOf(this.f1791b), this.c, this.d});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.PlayerLevel, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, this.f1790a);
        a.a(parcel, 2, this.f1791b);
        a.a(parcel, 3, (Parcelable) this.c, i, false);
        a.a(parcel, 4, (Parcelable) this.d, i, false);
        a.b(parcel, a2);
    }
}
