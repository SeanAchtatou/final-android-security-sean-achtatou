package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.drive.metadata.g;
import java.util.ArrayList;
import java.util.Collection;

public class j<T extends Parcelable> extends g<T> {
    public j(String str, Collection<String> collection, Collection<String> collection2, int i) {
        super(str, collection, collection2, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public Collection<T> b(Bundle bundle) {
        return bundle.getParcelableArrayList(this.f1731b);
    }

    public final /* synthetic */ void a(Bundle bundle, Object obj) {
        Collection collection = (Collection) obj;
        bundle.putParcelableArrayList(this.f1731b, collection instanceof ArrayList ? (ArrayList) collection : new ArrayList(collection));
    }
}
