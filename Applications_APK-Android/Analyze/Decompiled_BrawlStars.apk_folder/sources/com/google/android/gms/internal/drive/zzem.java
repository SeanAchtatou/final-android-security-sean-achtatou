package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.zzr;
import java.util.List;

public final class zzem extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzem> CREATOR = new p();

    /* renamed from: a  reason: collision with root package name */
    private final List<zzr> f1932a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1933b;

    public zzem(List<zzr> list, int i) {
        this.f1932a = list;
        this.f1933b = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 2, this.f1932a, false);
        a.b(parcel, 3, this.f1933b);
        a.b(parcel, a2);
    }
}
