package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.l;

public final class bt {

    /* renamed from: a  reason: collision with root package name */
    final Context f2438a;

    /* renamed from: b  reason: collision with root package name */
    String f2439b;
    String c;
    String d;
    boolean e = true;
    Boolean f;
    j g;

    public bt(Context context, j jVar) {
        l.a(context);
        Context applicationContext = context.getApplicationContext();
        l.a(applicationContext);
        this.f2438a = applicationContext;
        if (jVar != null) {
            this.g = jVar;
            this.f2439b = jVar.f;
            this.c = jVar.e;
            this.d = jVar.d;
            this.e = jVar.c;
            if (jVar.g != null) {
                this.f = Boolean.valueOf(jVar.g.getBoolean("dataCollectionDefaultEnabled", true));
            }
        }
    }
}
