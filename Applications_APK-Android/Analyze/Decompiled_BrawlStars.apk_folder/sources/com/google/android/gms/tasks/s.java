package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class s<TResult> implements y<TResult> {

    /* renamed from: a  reason: collision with root package name */
    final Object f2698a = new Object();

    /* renamed from: b  reason: collision with root package name */
    d f2699b;
    private final Executor c;

    public s(Executor executor, d dVar) {
        this.c = executor;
        this.f2699b = dVar;
    }

    public final void a(g<TResult> gVar) {
        if (!gVar.b() && !gVar.c()) {
            synchronized (this.f2698a) {
                if (this.f2699b != null) {
                    this.c.execute(new t(this, gVar));
                }
            }
        }
    }

    public final void a() {
        synchronized (this.f2698a) {
            this.f2699b = null;
        }
    }
}
