package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.google.android.gms.analytics.l;
import java.util.HashMap;

public final class f extends l<f> {

    /* renamed from: a  reason: collision with root package name */
    public String f2196a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f2197b;

    public final String toString() {
        HashMap hashMap = new HashMap();
        hashMap.put("description", this.f2196a);
        hashMap.put("fatal", Boolean.valueOf(this.f2197b));
        return a((Object) hashMap);
    }

    public final /* synthetic */ void a(l lVar) {
        f fVar = (f) lVar;
        if (!TextUtils.isEmpty(this.f2196a)) {
            fVar.f2196a = this.f2196a;
        }
        boolean z = this.f2197b;
        if (z) {
            fVar.f2197b = z;
        }
    }
}
