package com.google.android.gms.measurement.internal;

final class cy implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzaj f2493a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ cx f2494b;

    cy(cx cxVar, zzaj zzaj) {
        this.f2494b = cxVar;
        this.f2493a = zzaj;
    }

    public final void run() {
        synchronized (this.f2494b) {
            boolean unused = this.f2494b.f2491a = false;
            if (!this.f2494b.c.v()) {
                this.f2494b.c.q().k.a("Connected to service");
                this.f2494b.c.a(this.f2493a);
            }
        }
    }
}
