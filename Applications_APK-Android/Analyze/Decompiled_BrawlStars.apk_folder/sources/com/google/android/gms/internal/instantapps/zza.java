package com.google.android.gms.internal.instantapps;

import android.os.IBinder;
import android.os.IInterface;

public class zza implements IInterface {

    /* renamed from: a  reason: collision with root package name */
    private final IBinder f2005a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2006b;

    protected zza(IBinder iBinder, String str) {
        this.f2005a = iBinder;
        this.f2006b = str;
    }

    public IBinder asBinder() {
        return this.f2005a;
    }
}
