package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cz extends iz<cz> {
    private static volatile cz[] f;

    /* renamed from: a  reason: collision with root package name */
    public Long f2147a = null;

    /* renamed from: b  reason: collision with root package name */
    public String f2148b = null;
    public String c = null;
    public Long d = null;
    public Double e = null;
    private Float g = null;

    public static cz[] a() {
        if (f == null) {
            synchronized (jd.f2312b) {
                if (f == null) {
                    f = new cz[0];
                }
            }
        }
        return f;
    }

    public cz() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cz)) {
            return false;
        }
        cz czVar = (cz) obj;
        Long l = this.f2147a;
        if (l == null) {
            if (czVar.f2147a != null) {
                return false;
            }
        } else if (!l.equals(czVar.f2147a)) {
            return false;
        }
        String str = this.f2148b;
        if (str == null) {
            if (czVar.f2148b != null) {
                return false;
            }
        } else if (!str.equals(czVar.f2148b)) {
            return false;
        }
        String str2 = this.c;
        if (str2 == null) {
            if (czVar.c != null) {
                return false;
            }
        } else if (!str2.equals(czVar.c)) {
            return false;
        }
        Long l2 = this.d;
        if (l2 == null) {
            if (czVar.d != null) {
                return false;
            }
        } else if (!l2.equals(czVar.d)) {
            return false;
        }
        Float f2 = this.g;
        if (f2 == null) {
            if (czVar.g != null) {
                return false;
            }
        } else if (!f2.equals(czVar.g)) {
            return false;
        }
        Double d2 = this.e;
        if (d2 == null) {
            if (czVar.e != null) {
                return false;
            }
        } else if (!d2.equals(czVar.e)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return czVar.L == null || czVar.L.a();
        }
        return this.L.equals(czVar.L);
    }

    public final int hashCode() {
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Long l = this.f2147a;
        int i = 0;
        int hashCode2 = (hashCode + (l == null ? 0 : l.hashCode())) * 31;
        String str = this.f2148b;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.c;
        int hashCode4 = (hashCode3 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Long l2 = this.d;
        int hashCode5 = (hashCode4 + (l2 == null ? 0 : l2.hashCode())) * 31;
        Float f2 = this.g;
        int hashCode6 = (hashCode5 + (f2 == null ? 0 : f2.hashCode())) * 31;
        Double d2 = this.e;
        int hashCode7 = (hashCode6 + (d2 == null ? 0 : d2.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode7 + i;
    }

    public final void a(iy iyVar) throws IOException {
        Long l = this.f2147a;
        if (l != null) {
            iyVar.b(1, l.longValue());
        }
        String str = this.f2148b;
        if (str != null) {
            iyVar.a(2, str);
        }
        String str2 = this.c;
        if (str2 != null) {
            iyVar.a(3, str2);
        }
        Long l2 = this.d;
        if (l2 != null) {
            iyVar.b(4, l2.longValue());
        }
        Float f2 = this.g;
        if (f2 != null) {
            iyVar.a(5, f2.floatValue());
        }
        Double d2 = this.e;
        if (d2 != null) {
            iyVar.a(6, d2.doubleValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        Long l = this.f2147a;
        if (l != null) {
            b2 += iy.c(1, l.longValue());
        }
        String str = this.f2148b;
        if (str != null) {
            b2 += iy.b(2, str);
        }
        String str2 = this.c;
        if (str2 != null) {
            b2 += iy.b(3, str2);
        }
        Long l2 = this.d;
        if (l2 != null) {
            b2 += iy.c(4, l2.longValue());
        }
        Float f2 = this.g;
        if (f2 != null) {
            f2.floatValue();
            b2 += iy.c(40) + 4;
        }
        Double d2 = this.e;
        if (d2 == null) {
            return b2;
        }
        d2.doubleValue();
        return b2 + iy.c(48) + 8;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                this.f2147a = Long.valueOf(iwVar.e());
            } else if (a2 == 18) {
                this.f2148b = iwVar.c();
            } else if (a2 == 26) {
                this.c = iwVar.c();
            } else if (a2 == 32) {
                this.d = Long.valueOf(iwVar.e());
            } else if (a2 == 45) {
                this.g = Float.valueOf(Float.intBitsToFloat(iwVar.f()));
            } else if (a2 == 49) {
                this.e = Double.valueOf(Double.longBitsToDouble(iwVar.g()));
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
