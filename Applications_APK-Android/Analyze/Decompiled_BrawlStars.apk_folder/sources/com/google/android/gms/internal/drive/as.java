package com.google.android.gms.internal.drive;

import java.io.IOException;

public final class as extends bs<as> {

    /* renamed from: a  reason: collision with root package name */
    public int f1889a = 1;

    /* renamed from: b  reason: collision with root package name */
    public long f1890b = -1;
    public long c = -1;
    public long d = -1;

    public as() {
        this.f = null;
        this.g = -1;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return super.a() + br.b(1, this.f1889a) + br.b(2, this.f1890b) + br.b(3, this.c) + br.b(4, this.d);
    }

    public final void a(br brVar) throws IOException {
        brVar.a(1, this.f1889a);
        brVar.a(2, this.f1890b);
        brVar.a(3, this.c);
        brVar.a(4, this.d);
        super.a(brVar);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof as)) {
            return false;
        }
        as asVar = (as) obj;
        if (this.f1889a == asVar.f1889a && this.f1890b == asVar.f1890b && this.c == asVar.c && this.d == asVar.d) {
            return (this.f == null || this.f.a()) ? asVar.f == null || asVar.f.a() : this.f.equals(asVar.f);
        }
        return false;
    }

    public final int hashCode() {
        long j = this.f1890b;
        long j2 = this.c;
        long j3 = this.d;
        return ((((((((((getClass().getName().hashCode() + 527) * 31) + this.f1889a) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((this.f == null || this.f.a()) ? 0 : this.f.hashCode());
    }
}
