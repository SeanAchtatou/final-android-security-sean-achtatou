package com.google.android.gms.games.event;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Player;

public interface Event extends Parcelable, e<Event> {
    String b();

    String c();

    String d();

    Uri e();

    Player f();

    long g();

    @Deprecated
    String getIconImageUrl();

    String h();

    boolean i();
}
