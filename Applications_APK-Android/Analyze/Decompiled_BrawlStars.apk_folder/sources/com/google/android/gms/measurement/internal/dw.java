package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.Map;

final class dw implements t {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2533a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ du f2534b;

    dw(du duVar, String str) {
        this.f2534b = duVar;
        this.f2533a = str;
    }

    public final void a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        this.f2534b.a(i, th, bArr, this.f2533a);
    }
}
