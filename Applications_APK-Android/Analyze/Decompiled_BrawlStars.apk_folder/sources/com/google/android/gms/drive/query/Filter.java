package com.google.android.gms.drive.query;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.query.internal.g;

public interface Filter extends SafeParcelable {
    <T> T a(g<T> gVar);
}
