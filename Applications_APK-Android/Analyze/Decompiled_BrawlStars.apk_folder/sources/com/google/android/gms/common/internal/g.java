package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import java.util.Arrays;

public abstract class g {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f1603a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static g f1604b;

    /* access modifiers changed from: protected */
    public abstract boolean a(a aVar, ServiceConnection serviceConnection, String str);

    /* access modifiers changed from: protected */
    public abstract void b(a aVar, ServiceConnection serviceConnection, String str);

    public static g a(Context context) {
        synchronized (f1603a) {
            if (f1604b == null) {
                f1604b = new ac(context.getApplicationContext());
            }
        }
        return f1604b;
    }

    protected static final class a {

        /* renamed from: a  reason: collision with root package name */
        final String f1605a;

        /* renamed from: b  reason: collision with root package name */
        final ComponentName f1606b = null;
        final int c;
        private final String d;

        public a(String str, String str2, int i) {
            this.d = l.a(str);
            this.f1605a = l.a(str2);
            this.c = i;
        }

        public final String toString() {
            String str = this.d;
            return str == null ? this.f1606b.flattenToString() : str;
        }

        public final Intent a() {
            String str = this.d;
            if (str != null) {
                return new Intent(str).setPackage(this.f1605a);
            }
            return new Intent().setComponent(this.f1606b);
        }

        public final int hashCode() {
            return Arrays.hashCode(new Object[]{this.d, this.f1605a, this.f1606b, Integer.valueOf(this.c)});
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return j.a(this.d, aVar.d) && j.a(this.f1605a, aVar.f1605a) && j.a(this.f1606b, aVar.f1606b) && this.c == aVar.c;
        }
    }

    public final void a(String str, String str2, int i, ServiceConnection serviceConnection, String str3) {
        b(new a(str, str2, i), serviceConnection, str3);
    }
}
