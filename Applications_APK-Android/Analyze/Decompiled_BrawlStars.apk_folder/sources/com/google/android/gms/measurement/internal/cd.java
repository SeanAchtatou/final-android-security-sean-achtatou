package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

final class cd implements Application.ActivityLifecycleCallbacks {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ bv f2460a;

    private cd(bv bvVar) {
        this.f2460a = bvVar;
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, long, java.lang.Object):void
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.util.Map<java.lang.String, java.lang.Object>
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, long, android.os.Bundle):void
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, java.lang.Object, long):void
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void */
    public final void onActivityCreated(Activity activity, Bundle bundle) {
        Bundle bundle2;
        Uri data;
        String str;
        try {
            this.f2460a.q().k.a("onActivityCreated");
            Intent intent = activity.getIntent();
            if (!(intent == null || (data = intent.getData()) == null || !data.isHierarchical())) {
                if (bundle == null) {
                    Bundle a2 = this.f2460a.o().a(data);
                    this.f2460a.o();
                    if (ed.a(intent)) {
                        str = "gs";
                    } else {
                        str = "auto";
                    }
                    if (a2 != null) {
                        this.f2460a.a(str, "_cmp", a2);
                    }
                }
                String queryParameter = data.getQueryParameter("referrer");
                if (!TextUtils.isEmpty(queryParameter)) {
                    if (!(queryParameter.contains("gclid") && (queryParameter.contains("utm_campaign") || queryParameter.contains("utm_source") || queryParameter.contains("utm_medium") || queryParameter.contains("utm_term") || queryParameter.contains("utm_content")))) {
                        this.f2460a.q().j.a("Activity created with data 'referrer' param without gclid and at least one utm field");
                        return;
                    }
                    this.f2460a.q().j.a("Activity created with referrer", queryParameter);
                    if (!TextUtils.isEmpty(queryParameter)) {
                        this.f2460a.a("auto", "_ldl", (Object) queryParameter, true);
                    }
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            this.f2460a.q().c.a("Throwable caught in onActivityCreated", e);
        }
        ch h = this.f2460a.h();
        if (bundle != null && (bundle2 = bundle.getBundle("com.google.app_measurement.screen_service")) != null) {
            h.d.put(activity, new cg(bundle2.getString("name"), bundle2.getString("referrer_name"), bundle2.getLong("id")));
        }
    }

    public final void onActivityDestroyed(Activity activity) {
        this.f2460a.h().d.remove(activity);
    }

    public final void onActivityPaused(Activity activity) {
        ch h = this.f2460a.h();
        cg a2 = h.a(activity);
        h.c = h.f2464b;
        h.f2464b = null;
        h.p().a(new ck(h, a2));
        dj j = this.f2460a.j();
        j.p().a(new Cdo(j, j.l().b()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void
     arg types: [android.app.Activity, com.google.android.gms.measurement.internal.cg, int]
     candidates:
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void */
    public final void onActivityResumed(Activity activity) {
        ch h = this.f2460a.h();
        h.a(activity, h.a(activity), false);
        a d = h.d();
        d.p().a(new bu(d, d.l().b()));
        dj j = this.f2460a.j();
        j.p().a(new dn(j, j.l().b()));
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        cg cgVar;
        ch h = this.f2460a.h();
        if (bundle != null && (cgVar = h.d.get(activity)) != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("id", cgVar.c);
            bundle2.putString("name", cgVar.f2461a);
            bundle2.putString("referrer_name", cgVar.f2462b);
            bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
        }
    }

    /* synthetic */ cd(bv bvVar, byte b2) {
        this(bvVar);
    }
}
