package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.google.android.gms.common.c.c;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;
import com.google.android.gms.common.util.g;
import com.google.android.gms.internal.measurement.ds;
import com.google.android.gms.measurement.AppMeasurement;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class ar implements bo {
    private static volatile ar l;
    private boolean A = false;
    private Boolean B;
    private long C;
    private volatile Boolean D;
    private Boolean E;
    private Boolean F;
    private AtomicInteger G = new AtomicInteger(0);

    /* renamed from: a  reason: collision with root package name */
    final String f2387a;

    /* renamed from: b  reason: collision with root package name */
    final String f2388b;
    final String c;
    final boolean d;
    final el e;
    final o f;
    final am g;
    public final AppMeasurement h;
    ae i;
    int j;
    final long k;
    private final Context m;
    private final ej n;
    private final z o;
    private final dj p;
    private final ed q;
    private final m r;
    private final e s;
    private final ch t;
    private final bv u;
    private final a v;
    private k w;
    private cl x;
    private b y;
    private i z;

    private ar(bt btVar) {
        l.a(btVar);
        this.n = new ej();
        h.a(this.n);
        this.m = btVar.f2438a;
        this.f2387a = btVar.f2439b;
        this.f2388b = btVar.c;
        this.c = btVar.d;
        this.d = btVar.e;
        this.D = btVar.f;
        j jVar = btVar.g;
        if (!(jVar == null || jVar.g == null)) {
            Object obj = jVar.g.get("measurementEnabled");
            if (obj instanceof Boolean) {
                this.E = (Boolean) obj;
            }
            Object obj2 = jVar.g.get("measurementDeactivated");
            if (obj2 instanceof Boolean) {
                this.F = (Boolean) obj2;
            }
        }
        ds.a(this.m);
        this.s = g.d();
        this.k = this.s.a();
        this.e = new el(this);
        z zVar = new z(this);
        zVar.x();
        this.o = zVar;
        o oVar = new o(this);
        oVar.x();
        this.f = oVar;
        ed edVar = new ed(this);
        edVar.x();
        this.q = edVar;
        m mVar = new m(this);
        mVar.x();
        this.r = mVar;
        this.v = new a(this);
        ch chVar = new ch(this);
        chVar.E();
        this.t = chVar;
        bv bvVar = new bv(this);
        bvVar.E();
        this.u = bvVar;
        this.h = new AppMeasurement(this);
        dj djVar = new dj(this);
        djVar.E();
        this.p = djVar;
        am amVar = new am(this);
        amVar.x();
        this.g = amVar;
        if (this.m.getApplicationContext() instanceof Application) {
            bv d2 = d();
            if (d2.m().getApplicationContext() instanceof Application) {
                Application application = (Application) d2.m().getApplicationContext();
                if (d2.f2442a == null) {
                    d2.f2442a = new cd(d2, (byte) 0);
                }
                application.unregisterActivityLifecycleCallbacks(d2.f2442a);
                application.registerActivityLifecycleCallbacks(d2.f2442a);
                d2.q().k.a("Registered activity lifecycle callback");
            }
        } else {
            q().f.a("Application context is not an Application");
        }
        this.g.a(new as(this, btVar));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        p().c();
        if (b().d.a() == 0) {
            b().d.a(this.s.a());
        }
        if (Long.valueOf(b().i.a()).longValue() == 0) {
            q().k.a("Persisting first open", Long.valueOf(this.k));
            b().i.a(this.k);
        }
        if (u()) {
            if (!TextUtils.isEmpty(k().w()) || !TextUtils.isEmpty(k().x())) {
                e();
                if (ed.a(k().w(), b().g(), k().x(), b().h())) {
                    q().i.a("Rechecking which service to use due to a GMP App Id change");
                    b().j();
                    k g2 = g();
                    g2.c();
                    try {
                        int delete = g2.w().delete("messages", null, null) + 0;
                        if (delete > 0) {
                            g2.q().k.a("Reset local analytics data. records", Integer.valueOf(delete));
                        }
                    } catch (SQLiteException e2) {
                        g2.q().c.a("Error resetting local analytics data. error", e2);
                    }
                    this.x.A();
                    this.x.z();
                    b().i.a(this.k);
                    b().k.a(null);
                }
                b().c(k().w());
                b().d(k().x());
                if (this.e.j(k().v())) {
                    this.p.a(this.k);
                }
            }
            bv d2 = d();
            ad adVar = b().k;
            if (!adVar.f2367b) {
                adVar.f2367b = true;
                adVar.c = adVar.d.f().getString(adVar.f2366a, null);
            }
            d2.a(adVar.c);
            if (!TextUtils.isEmpty(k().w()) || !TextUtils.isEmpty(k().x())) {
                boolean r2 = r();
                if (!b().f2593b.contains("deferred_analytics_collection") && !this.e.f()) {
                    b().c(!r2);
                }
                if (!this.e.e(k().v()) || r2) {
                    d().v();
                }
                i().a(new AtomicReference());
            }
        } else if (r()) {
            if (!e().d("android.permission.INTERNET")) {
                q().c.a("App is missing INTERNET permission");
            }
            if (!e().d("android.permission.ACCESS_NETWORK_STATE")) {
                q().c.a("App is missing ACCESS_NETWORK_STATE permission");
            }
            if (!c.a(this.m).a() && !this.e.t()) {
                if (!ah.a(this.m)) {
                    q().c.a("AppMeasurementReceiver not registered/enabled");
                }
                if (!ed.a(this.m)) {
                    q().c.a("AppMeasurementService not registered/enabled");
                }
            }
            q().c.a("Uploading is not possible. App measurement disabled");
        }
    }

    public final z b() {
        a((bm) this.o);
        return this.o;
    }

    public final o q() {
        a((bn) this.f);
        return this.f;
    }

    public final am p() {
        a((bn) this.g);
        return this.g;
    }

    public final dj c() {
        a((df) this.p);
        return this.p;
    }

    public final bv d() {
        a((df) this.u);
        return this.u;
    }

    public final ed e() {
        a((bm) this.q);
        return this.q;
    }

    public final m f() {
        a((bm) this.r);
        return this.r;
    }

    public final k g() {
        a((df) this.w);
        return this.w;
    }

    public final Context m() {
        return this.m;
    }

    public final e l() {
        return this.s;
    }

    public final ch h() {
        a((df) this.t);
        return this.t;
    }

    public final cl i() {
        a((df) this.x);
        return this.x;
    }

    public final b j() {
        a((bn) this.y);
        return this.y;
    }

    public final i k() {
        a((df) this.z);
        return this.z;
    }

    public final a n() {
        a aVar = this.v;
        if (aVar != null) {
            return aVar;
        }
        throw new IllegalStateException("Component not created");
    }

    public static ar a(Context context, j jVar) {
        if (jVar != null && (jVar.e == null || jVar.f == null)) {
            jVar = new j(jVar.f2567a, jVar.f2568b, jVar.c, jVar.d, null, null, jVar.g);
        }
        l.a(context);
        l.a(context.getApplicationContext());
        if (l == null) {
            synchronized (ar.class) {
                if (l == null) {
                    l = new ar(new bt(context, jVar));
                }
            }
        } else if (!(jVar == null || jVar.g == null || !jVar.g.containsKey("dataCollectionDefaultEnabled"))) {
            l.a(jVar.g.getBoolean("dataCollectionDefaultEnabled"));
        }
        return l;
    }

    private final void v() {
        if (!this.A) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }

    private static void a(bn bnVar) {
        if (bnVar == null) {
            throw new IllegalStateException("Component not created");
        } else if (!bnVar.v()) {
            String valueOf = String.valueOf(bnVar.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    private static void a(df dfVar) {
        if (dfVar == null) {
            throw new IllegalStateException("Component not created");
        } else if (!dfVar.C()) {
            String valueOf = String.valueOf(dfVar.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    private static void a(bm bmVar) {
        if (bmVar == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z2) {
        this.D = Boolean.valueOf(z2);
    }

    public final boolean o() {
        return this.D != null && this.D.booleanValue();
    }

    public final boolean r() {
        boolean z2;
        p().c();
        v();
        if (this.e.a(h.aq)) {
            if (this.e.f()) {
                return false;
            }
            Boolean bool = this.F;
            if (bool != null && bool.booleanValue()) {
                return false;
            }
            Boolean t2 = b().t();
            if (t2 != null) {
                return t2.booleanValue();
            }
            Boolean b2 = this.e.b("firebase_analytics_collection_enabled");
            if (b2 != null) {
                return b2.booleanValue();
            }
            Boolean bool2 = this.E;
            if (bool2 != null) {
                return bool2.booleanValue();
            }
            if (com.google.android.gms.common.api.internal.e.b()) {
                return false;
            }
            if (!this.e.a(h.am) || this.D == null) {
                return true;
            }
            return this.D.booleanValue();
        } else if (this.e.f()) {
            return false;
        } else {
            Boolean b3 = this.e.b("firebase_analytics_collection_enabled");
            if (b3 != null) {
                z2 = b3.booleanValue();
            } else {
                z2 = !com.google.android.gms.common.api.internal.e.b();
                if (z2 && this.D != null && h.am.a().booleanValue()) {
                    z2 = this.D.booleanValue();
                }
            }
            return b().b(z2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void s() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    /* access modifiers changed from: package-private */
    public final void t() {
        this.G.incrementAndGet();
    }

    /* access modifiers changed from: protected */
    public final boolean u() {
        v();
        p().c();
        Boolean bool = this.B;
        if (bool == null || this.C == 0 || (bool != null && !bool.booleanValue() && Math.abs(this.s.b() - this.C) > 1000)) {
            this.C = this.s.b();
            boolean z2 = true;
            this.B = Boolean.valueOf(e().d("android.permission.INTERNET") && e().d("android.permission.ACCESS_NETWORK_STATE") && (c.a(this.m).a() || this.e.t() || (ah.a(this.m) && ed.a(this.m))));
            if (this.B.booleanValue()) {
                if (!e().b(k().w(), k().x()) && TextUtils.isEmpty(k().x())) {
                    z2 = false;
                }
                this.B = Boolean.valueOf(z2);
            }
        }
        return this.B.booleanValue();
    }

    static /* synthetic */ void a(ar arVar, bt btVar) {
        String str;
        q qVar;
        arVar.p().c();
        el.d();
        b bVar = new b(arVar);
        bVar.x();
        arVar.y = bVar;
        i iVar = new i(arVar);
        iVar.E();
        arVar.z = iVar;
        k kVar = new k(arVar);
        kVar.E();
        arVar.w = kVar;
        cl clVar = new cl(arVar);
        clVar.E();
        arVar.x = clVar;
        arVar.q.y();
        arVar.o.y();
        arVar.i = new ae(arVar);
        arVar.z.F();
        arVar.q().i.a("App measurement is starting up, version", 14710L);
        arVar.q().i.a("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        String v2 = iVar.v();
        if (TextUtils.isEmpty(arVar.f2387a)) {
            if (arVar.e().f(v2)) {
                qVar = arVar.q().i;
                str = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.";
            } else {
                qVar = arVar.q().i;
                String valueOf = String.valueOf(v2);
                str = valueOf.length() != 0 ? "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(valueOf) : new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ");
            }
            qVar.a(str);
        }
        arVar.q().j.a("Debug-level message logging enabled");
        if (arVar.j != arVar.G.get()) {
            arVar.q().c.a("Not all components initialized", Integer.valueOf(arVar.j), Integer.valueOf(arVar.G.get()));
        }
        arVar.A = true;
    }
}
