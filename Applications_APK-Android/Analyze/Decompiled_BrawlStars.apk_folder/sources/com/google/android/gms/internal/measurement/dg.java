package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.os.UserManager;

public class dg {

    /* renamed from: a  reason: collision with root package name */
    private static volatile UserManager f2159a;

    /* renamed from: b  reason: collision with root package name */
    private static volatile boolean f2160b = (!a());

    private dg() {
    }

    public static boolean a() {
        return Build.VERSION.SDK_INT >= 24;
    }

    public static boolean a(Context context) {
        return !a() || b(context);
    }

    private static boolean b(Context context) {
        boolean z = f2160b;
        if (!z) {
            boolean z2 = z;
            int i = 1;
            while (true) {
                if (i > 2) {
                    break;
                }
                UserManager c = c(context);
                if (c == null) {
                    f2160b = true;
                    return true;
                }
                try {
                    if (!c.isUserUnlocked()) {
                        if (c.isUserRunning(Process.myUserHandle())) {
                            z2 = false;
                            f2160b = z2;
                        }
                    }
                    z2 = true;
                    f2160b = z2;
                } catch (NullPointerException unused) {
                    f2159a = null;
                    i++;
                }
            }
            z = z2;
            if (z) {
                f2159a = null;
            }
        }
        return z;
    }

    private static UserManager c(Context context) {
        UserManager userManager = f2159a;
        if (userManager == null) {
            synchronized (dg.class) {
                userManager = f2159a;
                if (userManager == null) {
                    UserManager userManager2 = (UserManager) context.getSystemService(UserManager.class);
                    f2159a = userManager2;
                    userManager = userManager2;
                }
            }
        }
        return userManager;
    }
}
