package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.zzt;

public final class al implements Parcelable.Creator<zzgm> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgm[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        DriveId driveId = null;
        int i = 0;
        zzt zzt = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 2) {
                driveId = (DriveId) SafeParcelReader.a(parcel, readInt, DriveId.CREATOR);
            } else if (i2 == 3) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i2 != 4) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                zzt = (zzt) SafeParcelReader.a(parcel, readInt, zzt.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzgm(driveId, i, zzt);
    }
}
