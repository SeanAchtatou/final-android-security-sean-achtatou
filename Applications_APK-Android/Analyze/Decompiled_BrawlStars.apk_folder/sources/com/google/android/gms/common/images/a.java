package com.google.android.gms.common.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    final b f1551a;

    /* renamed from: b  reason: collision with root package name */
    protected int f1552b;

    /* access modifiers changed from: protected */
    public abstract void a(Drawable drawable, boolean z, boolean z2);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.images.a.a(android.graphics.drawable.Drawable, boolean, boolean):void
     arg types: [android.graphics.drawable.BitmapDrawable, int, int]
     candidates:
      com.google.android.gms.common.images.a.a(android.content.Context, android.graphics.Bitmap, boolean):void
      com.google.android.gms.common.images.a.a(android.graphics.drawable.Drawable, boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(Context context, Bitmap bitmap, boolean z) {
        com.google.android.gms.common.internal.a.a(bitmap);
        a((Drawable) new BitmapDrawable(context.getResources(), bitmap), false, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.images.a.a(android.graphics.drawable.Drawable, boolean, boolean):void
     arg types: [android.graphics.drawable.Drawable, int, int]
     candidates:
      com.google.android.gms.common.images.a.a(android.content.Context, android.graphics.Bitmap, boolean):void
      com.google.android.gms.common.images.a.a(android.graphics.drawable.Drawable, boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(Context context, boolean z) {
        int i = this.f1552b;
        a(i != 0 ? context.getResources().getDrawable(i) : null, false, false);
    }
}
