package com.google.android.gms.instantapps;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class h implements Parcelable.Creator<zzj> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzj[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        while (parcel.dataPosition() < a2) {
            SafeParcelReader.b(parcel, parcel.readInt());
        }
        SafeParcelReader.x(parcel, a2);
        return new zzj();
    }
}
