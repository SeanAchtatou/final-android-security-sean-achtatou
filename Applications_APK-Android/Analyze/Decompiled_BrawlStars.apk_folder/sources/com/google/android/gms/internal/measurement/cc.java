package com.google.android.gms.internal.measurement;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public final class cc extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private static volatile cd f2109a;

    public cc() {
    }

    public cc(Looper looper) {
        super(looper);
    }

    public final void dispatchMessage(Message message) {
        super.dispatchMessage(message);
    }
}
