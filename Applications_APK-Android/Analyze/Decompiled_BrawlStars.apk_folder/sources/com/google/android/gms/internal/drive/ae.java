package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;

public final class ae implements Parcelable.Creator<zzfx> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzfx[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        ArrayList<String> arrayList = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                arrayList = SafeParcelReader.u(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzfx(arrayList);
    }
}
