package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.facebook.internal.AnalyticsEvents;
import com.google.android.gms.analytics.l;
import java.util.HashMap;

public final class h extends l<h> {

    /* renamed from: a  reason: collision with root package name */
    public String f2254a;

    /* renamed from: b  reason: collision with root package name */
    public int f2255b;
    public String c;
    private int d;
    private String e;
    private boolean f;
    private boolean g;

    public h() {
        this((byte) 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private h(byte r5) {
        /*
            r4 = this;
            java.util.UUID r5 = java.util.UUID.randomUUID()
            long r0 = r5.getLeastSignificantBits()
            r2 = 2147483647(0x7fffffff, double:1.060997895E-314)
            long r0 = r0 & r2
            int r1 = (int) r0
            if (r1 == 0) goto L_0x0010
            goto L_0x001c
        L_0x0010:
            long r0 = r5.getMostSignificantBits()
            long r0 = r0 & r2
            int r1 = (int) r0
            if (r1 == 0) goto L_0x0019
            goto L_0x001c
        L_0x0019:
            r1 = 2147483647(0x7fffffff, float:NaN)
        L_0x001c:
            r4.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.h.<init>(byte):void");
    }

    private h(int i) {
        com.google.android.gms.common.internal.l.a(i);
        this.f2255b = i;
        this.g = false;
    }

    public final String toString() {
        HashMap hashMap = new HashMap();
        hashMap.put("screenName", this.f2254a);
        hashMap.put("interstitial", Boolean.valueOf(this.f));
        hashMap.put(AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_AUTOMATIC, Boolean.valueOf(this.g));
        hashMap.put("screenId", Integer.valueOf(this.f2255b));
        hashMap.put("referrerScreenId", Integer.valueOf(this.d));
        hashMap.put("referrerScreenName", this.e);
        hashMap.put("referrerUri", this.c);
        return a((Object) hashMap);
    }

    public final /* synthetic */ void a(l lVar) {
        h hVar = (h) lVar;
        if (!TextUtils.isEmpty(this.f2254a)) {
            hVar.f2254a = this.f2254a;
        }
        int i = this.f2255b;
        if (i != 0) {
            hVar.f2255b = i;
        }
        int i2 = this.d;
        if (i2 != 0) {
            hVar.d = i2;
        }
        if (!TextUtils.isEmpty(this.e)) {
            hVar.e = this.e;
        }
        if (!TextUtils.isEmpty(this.c)) {
            String str = this.c;
            if (TextUtils.isEmpty(str)) {
                hVar.c = null;
            } else {
                hVar.c = str;
            }
        }
        boolean z = this.f;
        if (z) {
            hVar.f = z;
        }
        boolean z2 = this.g;
        if (z2) {
            hVar.g = z2;
        }
    }
}
