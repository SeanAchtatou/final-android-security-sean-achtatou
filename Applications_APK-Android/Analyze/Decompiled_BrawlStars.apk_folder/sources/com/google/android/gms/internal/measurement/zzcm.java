package com.google.android.gms.internal.measurement;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;
import java.util.Map;

public final class zzcm extends zzq implements zzcl {
    zzcm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.analytics.internal.IAnalyticsService");
    }

    public final void a(Map map, long j, String str, List<zzbr> list) throws RemoteException {
        Parcel a2 = a();
        a2.writeMap(map);
        a2.writeLong(j);
        a2.writeString(str);
        a2.writeTypedList(list);
        b(1, a2);
    }
}
