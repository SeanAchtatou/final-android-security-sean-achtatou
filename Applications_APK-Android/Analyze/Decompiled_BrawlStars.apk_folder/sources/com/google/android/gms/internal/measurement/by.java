package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import java.util.Locale;

public final class by extends r {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f2102a;

    /* renamed from: b  reason: collision with root package name */
    protected int f2103b;
    private String d;
    private String e;
    private int f;
    private boolean g;
    private boolean h;

    public by(t tVar) {
        super(tVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        ApplicationInfo applicationInfo;
        int i;
        bg bgVar;
        int i2;
        Context g2 = g();
        try {
            applicationInfo = g2.getPackageManager().getApplicationInfo(g2.getPackageName(), 128);
        } catch (PackageManager.NameNotFoundException e2) {
            d("PackageManager doesn't know about the app package", e2);
            applicationInfo = null;
        }
        if (applicationInfo == null) {
            e("Couldn't get ApplicationInfo to load global config");
            return;
        }
        Bundle bundle = applicationInfo.metaData;
        if (bundle != null && (i = bundle.getInt("com.google.android.gms.analytics.globalConfigResource")) > 0 && (bgVar = (bg) new be(this.c).a(i)) != null) {
            b("Loading global XML config values");
            boolean z = false;
            if (bgVar.f2071a != null) {
                String str = bgVar.f2071a;
                this.e = str;
                b("XML config - app name", str);
            }
            if (bgVar.f2072b != null) {
                String str2 = bgVar.f2072b;
                this.d = str2;
                b("XML config - app version", str2);
            }
            if (bgVar.c != null) {
                String lowerCase = bgVar.c.toLowerCase(Locale.US);
                if ("verbose".equals(lowerCase)) {
                    i2 = 0;
                } else if ("info".equals(lowerCase)) {
                    i2 = 1;
                } else if ("warning".equals(lowerCase)) {
                    i2 = 2;
                } else {
                    i2 = "error".equals(lowerCase) ? 3 : -1;
                }
                if (i2 >= 0) {
                    this.f = i2;
                    a("XML config - log level", Integer.valueOf(i2));
                }
            }
            if (bgVar.d >= 0) {
                int i3 = bgVar.d;
                this.f2103b = i3;
                this.f2102a = true;
                b("XML config - dispatch period (sec)", Integer.valueOf(i3));
            }
            if (bgVar.e != -1) {
                if (bgVar.e == 1) {
                    z = true;
                }
                this.h = z;
                this.g = true;
                b("XML config - dry run", Boolean.valueOf(z));
            }
        }
    }

    public final String b() {
        m();
        return this.d;
    }

    public final String c() {
        m();
        return this.e;
    }

    public final boolean d() {
        m();
        return false;
    }

    public final boolean e() {
        m();
        return this.g;
    }

    public final boolean o() {
        m();
        return this.h;
    }
}
