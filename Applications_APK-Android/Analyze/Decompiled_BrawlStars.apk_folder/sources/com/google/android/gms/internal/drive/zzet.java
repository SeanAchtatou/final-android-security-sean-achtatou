package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzet extends zzb implements zzes {
    public zzet() {
        super("com.google.android.gms.drive.internal.IEventCallback");
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i, Parcel parcel, Parcel parcel2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a((zzfj) g.a(parcel, zzfj.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
