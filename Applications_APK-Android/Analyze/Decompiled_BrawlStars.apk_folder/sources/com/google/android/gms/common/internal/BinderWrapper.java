package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public final class BinderWrapper implements Parcelable {
    public static final Parcelable.Creator<BinderWrapper> CREATOR = new z();

    /* renamed from: a  reason: collision with root package name */
    private IBinder f1571a;

    public BinderWrapper() {
        this.f1571a = null;
    }

    public final int describeContents() {
        return 0;
    }

    public BinderWrapper(IBinder iBinder) {
        this.f1571a = null;
        this.f1571a = iBinder;
    }

    private BinderWrapper(Parcel parcel) {
        this.f1571a = null;
        this.f1571a = parcel.readStrongBinder();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeStrongBinder(this.f1571a);
    }

    /* synthetic */ BinderWrapper(Parcel parcel, byte b2) {
        this(parcel);
    }
}
