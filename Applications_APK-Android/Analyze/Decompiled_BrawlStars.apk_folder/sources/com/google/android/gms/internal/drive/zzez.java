package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.zza;
import com.google.android.gms.drive.zzu;
import java.util.List;

public final class zzez extends zzu {
    public static final Parcelable.Creator<zzez> CREATOR = new s();

    /* renamed from: a  reason: collision with root package name */
    private final DataHolder f1935a;

    /* renamed from: b  reason: collision with root package name */
    private final List<DriveId> f1936b;
    private final zza c;
    private final boolean d;

    public zzez(DataHolder dataHolder, List<DriveId> list, zza zza, boolean z) {
        this.f1935a = dataHolder;
        this.f1936b = list;
        this.c = zza;
        this.d = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.data.DataHolder, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.zza, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void a(Parcel parcel, int i) {
        int i2 = i | 1;
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, (Parcelable) this.f1935a, i2, false);
        a.b(parcel, 3, this.f1936b, false);
        a.a(parcel, 4, (Parcelable) this.c, i2, false);
        a.a(parcel, 5, this.d);
        a.b(parcel, a2);
    }
}
