package com.google.android.gms.internal.measurement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

public final class gc extends ef<String> implements gd, RandomAccess {

    /* renamed from: b  reason: collision with root package name */
    private static final gc f2236b;
    private static final gd c = f2236b;
    private final List<Object> d;

    public gc() {
        this(10);
    }

    public gc(int i) {
        this(new ArrayList(i));
    }

    private gc(ArrayList<Object> arrayList) {
        this.d = arrayList;
    }

    public final int size() {
        return this.d.size();
    }

    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    public final boolean addAll(int i, Collection<? extends String> collection) {
        c();
        if (collection instanceof gd) {
            collection = ((gd) collection).d();
        }
        boolean addAll = this.d.addAll(i, collection);
        this.modCount++;
        return addAll;
    }

    public final void clear() {
        c();
        this.d.clear();
        this.modCount++;
    }

    public final void a(ej ejVar) {
        c();
        this.d.add(ejVar);
        this.modCount++;
    }

    public final Object b(int i) {
        return this.d.get(i);
    }

    private static String a(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof ej) {
            return ((ej) obj).b();
        }
        return fs.b((byte[]) obj);
    }

    public final List<?> d() {
        return Collections.unmodifiableList(this.d);
    }

    public final gd e() {
        return a() ? new ie(this) : this;
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        c();
        return a(this.d.set(i, (String) obj));
    }

    public final /* bridge */ /* synthetic */ boolean retainAll(Collection collection) {
        return super.retainAll(collection);
    }

    public final /* bridge */ /* synthetic */ boolean removeAll(Collection collection) {
        return super.removeAll(collection);
    }

    public final /* bridge */ /* synthetic */ boolean remove(Object obj) {
        return super.remove(obj);
    }

    public final /* synthetic */ Object remove(int i) {
        c();
        Object remove = this.d.remove(i);
        this.modCount++;
        return a(remove);
    }

    public final /* bridge */ /* synthetic */ boolean a() {
        return super.a();
    }

    public final /* synthetic */ void add(int i, Object obj) {
        c();
        this.d.add(i, (String) obj);
        this.modCount++;
    }

    public final /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    public final /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    public final /* synthetic */ fw a(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.d);
            return new gc(arrayList);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ Object get(int i) {
        Object obj = this.d.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof ej) {
            ej ejVar = (ej) obj;
            String b2 = ejVar.b();
            if (ejVar.c()) {
                this.d.set(i, b2);
            }
            return b2;
        }
        byte[] bArr = (byte[]) obj;
        String b3 = fs.b(bArr);
        if (fs.a(bArr)) {
            this.d.set(i, b3);
        }
        return b3;
    }

    static {
        gc gcVar = new gc();
        f2236b = gcVar;
        gcVar.f2180a = false;
    }
}
