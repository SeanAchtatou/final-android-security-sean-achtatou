package com.google.android.gms.drive.query;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.query.internal.zzf;
import java.util.List;
import java.util.Locale;

public class SortOrder extends AbstractSafeParcelable {
    public static final Parcelable.Creator<SortOrder> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final List<zzf> f1747a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f1748b;

    SortOrder(List<zzf> list, boolean z) {
        this.f1747a = list;
        this.f1748b = z;
    }

    public String toString() {
        return String.format(Locale.US, "SortOrder[%s, %s]", TextUtils.join(",", this.f1747a), Boolean.valueOf(this.f1748b));
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.f1747a, false);
        a.a(parcel, 2, this.f1748b);
        a.b(parcel, a2);
    }
}
