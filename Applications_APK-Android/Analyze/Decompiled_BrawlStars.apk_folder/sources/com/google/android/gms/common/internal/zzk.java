package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.common.zza;

public final class zzk extends zza implements zzi {
    zzk(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    public final IObjectWrapper a() throws RemoteException {
        Parcel a2 = a(1, s());
        IObjectWrapper a3 = IObjectWrapper.Stub.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final int b() throws RemoteException {
        Parcel a2 = a(2, s());
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }
}
