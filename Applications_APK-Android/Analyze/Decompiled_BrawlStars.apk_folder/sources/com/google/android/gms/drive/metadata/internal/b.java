package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.f;
import java.util.Collection;

public class b extends f<Boolean> {
    public b(String str, int i) {
        super(str, i);
    }

    public b(String str, Collection<String> collection, Collection<String> collection2) {
        super(str, collection, collection2, 7000000);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a_ */
    public Boolean c(DataHolder dataHolder, int i, int i2) {
        return Boolean.valueOf(dataHolder.d(this.f1731b, i, i2));
    }

    public final /* synthetic */ void a(Bundle bundle, Object obj) {
        bundle.putBoolean(this.f1731b, ((Boolean) obj).booleanValue());
    }

    public final /* synthetic */ Object b(Bundle bundle) {
        return Boolean.valueOf(bundle.getBoolean(this.f1731b));
    }
}
