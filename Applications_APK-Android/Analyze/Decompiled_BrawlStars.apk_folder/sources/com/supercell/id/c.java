package com.supercell.id;

import kotlin.d.a.a;
import kotlin.d.b.k;

public final class c extends k implements a<Boolean> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ IdConfiguration f4867a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(IdConfiguration idConfiguration) {
        super(0);
        this.f4867a = idConfiguration;
    }

    public final Object invoke() {
        return Boolean.valueOf(this.f4867a.getGameAccountToken().length() > 0);
    }
}
