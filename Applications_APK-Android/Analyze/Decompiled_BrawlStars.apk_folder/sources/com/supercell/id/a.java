package com.supercell.id;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.ShareConstants;
import kotlin.d.b.j;

public final class a implements Parcelable.Creator<IdAccount> {
    public final IdAccount createFromParcel(Parcel parcel) {
        j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
        return new IdAccount(parcel);
    }

    public final IdAccount[] newArray(int i) {
        return new IdAccount[i];
    }
}
