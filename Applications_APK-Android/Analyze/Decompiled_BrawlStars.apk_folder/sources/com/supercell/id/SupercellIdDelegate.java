package com.supercell.id;

public interface SupercellIdDelegate {
    void avatarImageData(byte[] bArr, int i, int i2, int i3, String str);

    void bindAccount(String str, String str2, String str3, String str4, boolean z);

    void clearPendingLogin();

    void clearPendingRegistration();

    void forgetAccount(String str, String str2);

    void friends(IdFriend[] idFriendArr);

    void friendsChanged(IdFriend[] idFriendArr);

    void friendsFailed();

    IdAccount[] getAccounts();

    IdConfiguration getConfig();

    IdAccount getCurrentAccount();

    IdIngameFriend[] getIngameFriends();

    IdPendingLogin getPendingLogin();

    IdPendingRegistration getPendingRegistration();

    boolean isSelfHelpPortalAvailable();

    boolean isTutorialComplete();

    void loadAccount(String str, String str2, String str3, boolean z);

    void logOut();

    void openSelfHelpPortal();

    void setPendingLoginWithEmail(String str, boolean z);

    void setPendingLoginWithPhone(String str, boolean z);

    void setPendingRegistrationWithEmail(String str, boolean z);

    void setPendingRegistrationWithPhone(String str);

    void setTutorialComplete();

    void windowDidDismiss();
}
