package com.supercell.id;

import b.a.a.a.a;
import kotlin.d.b.j;

public final class IdIngameFriend {

    /* renamed from: a  reason: collision with root package name */
    public final String f4858a;

    /* renamed from: b  reason: collision with root package name */
    public final String f4859b;

    public IdIngameFriend(String str, String str2) {
        j.b(str, "supercellId");
        j.b(str2, "username");
        this.f4858a = str;
        this.f4859b = str2;
    }

    public static /* synthetic */ IdIngameFriend copy$default(IdIngameFriend idIngameFriend, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = idIngameFriend.f4858a;
        }
        if ((i & 2) != 0) {
            str2 = idIngameFriend.f4859b;
        }
        return idIngameFriend.copy(str, str2);
    }

    public final String component1() {
        return this.f4858a;
    }

    public final String component2() {
        return this.f4859b;
    }

    public final IdIngameFriend copy(String str, String str2) {
        j.b(str, "supercellId");
        j.b(str2, "username");
        return new IdIngameFriend(str, str2);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IdIngameFriend)) {
            return false;
        }
        IdIngameFriend idIngameFriend = (IdIngameFriend) obj;
        return j.a(this.f4858a, idIngameFriend.f4858a) && j.a(this.f4859b, idIngameFriend.f4859b);
    }

    public final String getSupercellId() {
        return this.f4858a;
    }

    public final String getUsername() {
        return this.f4859b;
    }

    public final int hashCode() {
        String str = this.f4858a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f4859b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        StringBuilder a2 = a.a("IdIngameFriend(supercellId=");
        a2.append(this.f4858a);
        a2.append(", username=");
        return a.a(a2, this.f4859b, ")");
    }
}
