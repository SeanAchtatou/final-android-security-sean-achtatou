package com.supercell.id;

import kotlin.d.a.a;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class d extends k implements a<Boolean> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ IdConfiguration f4868a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(IdConfiguration idConfiguration) {
        super(0);
        this.f4868a = idConfiguration;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final Object invoke() {
        return Boolean.valueOf(j.a((Object) this.f4868a.getEnvironment(), (Object) "prod"));
    }
}
