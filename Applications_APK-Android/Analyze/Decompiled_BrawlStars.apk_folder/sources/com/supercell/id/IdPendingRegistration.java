package com.supercell.id;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import com.facebook.places.model.PlaceFields;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class IdPendingRegistration implements Parcelable {
    public static final a CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public final String f4862a;

    /* renamed from: b  reason: collision with root package name */
    public final String f4863b;
    public final boolean c;

    public static final class a implements Parcelable.Creator<IdPendingRegistration> {
        public static IdPendingRegistration a(String str) {
            j.b(str, PlaceFields.PHONE);
            return new IdPendingRegistration(null, str, false, null);
        }

        public static IdPendingRegistration a(String str, boolean z) {
            j.b(str, NotificationCompat.CATEGORY_EMAIL);
            return new IdPendingRegistration(str, null, z, null);
        }

        public final Object createFromParcel(Parcel parcel) {
            j.b(parcel, "parcel");
            return new IdPendingRegistration(parcel);
        }

        public final Object[] newArray(int i) {
            return new IdPendingRegistration[i];
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public IdPendingRegistration(Parcel parcel) {
        this(parcel.readString(), parcel.readString(), parcel.readByte() != 0);
        j.b(parcel, "parcel");
    }

    public IdPendingRegistration(String str, String str2, boolean z) {
        this.f4862a = str;
        this.f4863b = str2;
        this.c = z;
    }

    public /* synthetic */ IdPendingRegistration(String str, String str2, boolean z, g gVar) {
        this.f4862a = str;
        this.f4863b = str2;
        this.c = z;
    }

    public static /* synthetic */ IdPendingRegistration copy$default(IdPendingRegistration idPendingRegistration, String str, String str2, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            str = idPendingRegistration.f4862a;
        }
        if ((i & 2) != 0) {
            str2 = idPendingRegistration.f4863b;
        }
        if ((i & 4) != 0) {
            z = idPendingRegistration.c;
        }
        return idPendingRegistration.copy(str, str2, z);
    }

    public static final IdPendingRegistration initWithEmail(String str, boolean z) {
        return a.a(str, z);
    }

    public static final IdPendingRegistration initWithPhone(String str) {
        return a.a(str);
    }

    public final String component1() {
        return this.f4862a;
    }

    public final String component2() {
        return this.f4863b;
    }

    public final boolean component3() {
        return this.c;
    }

    public final IdPendingRegistration copy(String str, String str2, boolean z) {
        return new IdPendingRegistration(str, str2, z);
    }

    public final int describeContents() {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof IdPendingRegistration) {
                IdPendingRegistration idPendingRegistration = (IdPendingRegistration) obj;
                if (j.a((Object) this.f4862a, (Object) idPendingRegistration.f4862a) && j.a((Object) this.f4863b, (Object) idPendingRegistration.f4863b)) {
                    if (this.c == idPendingRegistration.c) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final boolean getAcceptMarketing() {
        return this.c;
    }

    public final String getEmail() {
        return this.f4862a;
    }

    public final String getPhone() {
        return this.f4863b;
    }

    public final int hashCode() {
        String str = this.f4862a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f4863b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        int i2 = (hashCode + i) * 31;
        boolean z = this.c;
        if (z) {
            z = true;
        }
        return i2 + (z ? 1 : 0);
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdPendingRegistration(email=");
        a2.append(this.f4862a);
        a2.append(", phone=");
        a2.append(this.f4863b);
        a2.append(", acceptMarketing=");
        a2.append(this.c);
        a2.append(")");
        return a2.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        j.b(parcel, "parcel");
        parcel.writeString(this.f4862a);
        parcel.writeString(this.f4863b);
        parcel.writeByte(this.c ? (byte) 1 : 0);
    }
}
