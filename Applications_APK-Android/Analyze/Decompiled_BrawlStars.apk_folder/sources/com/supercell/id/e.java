package com.supercell.id;

import android.support.v4.text.TextUtilsCompat;
import kotlin.d.a.a;
import kotlin.d.b.k;

public final class e extends k implements a<Boolean> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ IdConfiguration f4869a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(IdConfiguration idConfiguration) {
        super(0);
        this.f4869a = idConfiguration;
    }

    public final Object invoke() {
        boolean z = true;
        if (TextUtilsCompat.getLayoutDirectionFromLocale(this.f4869a.getLocale()) != 1) {
            z = false;
        }
        return Boolean.valueOf(z);
    }
}
