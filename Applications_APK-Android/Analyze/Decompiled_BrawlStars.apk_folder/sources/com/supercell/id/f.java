package com.supercell.id;

import java.util.Locale;
import kotlin.TypeCastException;
import kotlin.d.a.a;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class f extends k implements a<Locale> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ IdConfiguration f4870a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(IdConfiguration idConfiguration) {
        super(0);
        this.f4870a = idConfiguration;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Object invoke() {
        String language = this.f4870a.getLanguage();
        Locale locale = Locale.ENGLISH;
        j.a((Object) locale, "Locale.ENGLISH");
        if (language != null) {
            String lowerCase = language.toLowerCase(locale);
            j.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            int hashCode = lowerCase.hashCode();
            if (hashCode != 3179) {
                if (hashCode != 3398) {
                    if (hashCode != 3431) {
                        if (hashCode == 98665 && lowerCase.equals("cnt")) {
                            return Locale.TRADITIONAL_CHINESE;
                        }
                    } else if (lowerCase.equals("kr")) {
                        return Locale.KOREAN;
                    }
                } else if (lowerCase.equals("jp")) {
                    return Locale.JAPANESE;
                }
            } else if (lowerCase.equals("cn")) {
                return Locale.SIMPLIFIED_CHINESE;
            }
            return new Locale(lowerCase);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
}
