package com.supercell.id;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.os.ParcelCompat;
import com.facebook.places.model.PlaceFields;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class IdPendingLogin implements Parcelable {
    public static final a CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public final String f4860a;

    /* renamed from: b  reason: collision with root package name */
    public final String f4861b;
    public final boolean c;

    public static final class a implements Parcelable.Creator<IdPendingLogin> {
        public static IdPendingLogin a(String str, boolean z) {
            j.b(str, NotificationCompat.CATEGORY_EMAIL);
            return new IdPendingLogin(str, null, z, null);
        }

        public static IdPendingLogin b(String str, boolean z) {
            j.b(str, PlaceFields.PHONE);
            return new IdPendingLogin(null, str, z, null);
        }

        public final Object createFromParcel(Parcel parcel) {
            j.b(parcel, "parcel");
            return new IdPendingLogin(parcel);
        }

        public final Object[] newArray(int i) {
            return new IdPendingLogin[i];
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public IdPendingLogin(Parcel parcel) {
        this(parcel.readString(), parcel.readString(), ParcelCompat.readBoolean(parcel));
        j.b(parcel, "parcel");
    }

    public IdPendingLogin(String str, String str2, boolean z) {
        this.f4860a = str;
        this.f4861b = str2;
        this.c = z;
    }

    public /* synthetic */ IdPendingLogin(String str, String str2, boolean z, g gVar) {
        this.f4860a = str;
        this.f4861b = str2;
        this.c = z;
    }

    public static /* synthetic */ IdPendingLogin copy$default(IdPendingLogin idPendingLogin, String str, String str2, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            str = idPendingLogin.f4860a;
        }
        if ((i & 2) != 0) {
            str2 = idPendingLogin.f4861b;
        }
        if ((i & 4) != 0) {
            z = idPendingLogin.c;
        }
        return idPendingLogin.copy(str, str2, z);
    }

    public static final IdPendingLogin initWithEmail(String str, boolean z) {
        return a.a(str, z);
    }

    public static final IdPendingLogin initWithPhone(String str, boolean z) {
        return a.b(str, z);
    }

    public final String component1() {
        return this.f4860a;
    }

    public final String component2() {
        return this.f4861b;
    }

    public final boolean component3() {
        return this.c;
    }

    public final IdPendingLogin copy(String str, String str2, boolean z) {
        return new IdPendingLogin(str, str2, z);
    }

    public final int describeContents() {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof IdPendingLogin) {
                IdPendingLogin idPendingLogin = (IdPendingLogin) obj;
                if (j.a((Object) this.f4860a, (Object) idPendingLogin.f4860a) && j.a((Object) this.f4861b, (Object) idPendingLogin.f4861b)) {
                    if (this.c == idPendingLogin.c) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final String getEmail() {
        return this.f4860a;
    }

    public final String getPhone() {
        return this.f4861b;
    }

    public final boolean getRemember() {
        return this.c;
    }

    public final int hashCode() {
        String str = this.f4860a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f4861b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        int i2 = (hashCode + i) * 31;
        boolean z = this.c;
        if (z) {
            z = true;
        }
        return i2 + (z ? 1 : 0);
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdPendingLogin(email=");
        a2.append(this.f4860a);
        a2.append(", phone=");
        a2.append(this.f4861b);
        a2.append(", remember=");
        a2.append(this.c);
        a2.append(")");
        return a2.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        j.b(parcel, "parcel");
        parcel.writeString(this.f4860a);
        parcel.writeString(this.f4861b);
        ParcelCompat.writeBoolean(parcel, this.c);
    }
}
