package com.supercell.id;

import android.content.Context;
import java.lang.ref.WeakReference;
import kotlin.d.b.j;

public final class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public static final k f4875a = new k();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void run() {
        Context context;
        WeakReference access$getWeakContext$p = SupercellId.f4865b;
        if (access$getWeakContext$p != null && (context = (Context) access$getWeakContext$p.get()) != null) {
            j.a((Object) context, "weakContext?.get() ?: return@execute");
            SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(context);
        }
    }
}
