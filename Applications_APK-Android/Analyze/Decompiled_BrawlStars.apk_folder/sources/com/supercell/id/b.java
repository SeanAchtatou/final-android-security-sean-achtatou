package com.supercell.id;

import java.text.Collator;
import kotlin.d.a.a;
import kotlin.d.b.k;

public final class b extends k implements a<Collator> {

    /* renamed from: a  reason: collision with root package name */
    public static final b f4866a = new b();

    public b() {
        super(0);
    }

    public final Object invoke() {
        Collator instance = Collator.getInstance(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getLocale());
        instance.setStrength(1);
        return instance;
    }
}
