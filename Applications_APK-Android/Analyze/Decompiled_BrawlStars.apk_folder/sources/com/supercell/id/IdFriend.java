package com.supercell.id;

import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.a.ah;
import kotlin.d.b.j;
import kotlin.g.c;
import kotlin.g.d;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class IdFriend {
    public static final a Companion = new a((byte) 0);

    /* renamed from: a  reason: collision with root package name */
    public final String f4856a;

    /* renamed from: b  reason: collision with root package name */
    public final String f4857b;
    public final String c;

    public IdFriend(String str, String str2, String str3) {
        j.b(str, "supercellId");
        j.b(str2, "name");
        j.b(str3, "avatarImage");
        this.f4856a = str;
        this.f4857b = str2;
        this.c = str3;
    }

    public static /* synthetic */ IdFriend copy$default(IdFriend idFriend, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = idFriend.f4856a;
        }
        if ((i & 2) != 0) {
            str2 = idFriend.f4857b;
        }
        if ((i & 4) != 0) {
            str3 = idFriend.c;
        }
        return idFriend.copy(str, str2, str3);
    }

    public final String component1() {
        return this.f4856a;
    }

    public final String component2() {
        return this.f4857b;
    }

    public final String component3() {
        return this.c;
    }

    public final IdFriend copy(String str, String str2, String str3) {
        j.b(str, "supercellId");
        j.b(str2, "name");
        j.b(str3, "avatarImage");
        return new IdFriend(str, str2, str3);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IdFriend)) {
            return false;
        }
        IdFriend idFriend = (IdFriend) obj;
        return j.a(this.f4856a, idFriend.f4856a) && j.a(this.f4857b, idFriend.f4857b) && j.a(this.c, idFriend.c);
    }

    public final String getAvatarImage() {
        return this.c;
    }

    public final String getName() {
        return this.f4857b;
    }

    public final String getSupercellId() {
        return this.f4856a;
    }

    public final int hashCode() {
        String str = this.f4856a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f4857b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdFriend(supercellId=");
        a2.append(this.f4856a);
        a2.append(", name=");
        a2.append(this.f4857b);
        a2.append(", avatarImage=");
        return b.a.a.a.a.a(a2, this.c, ")");
    }

    public static final class a {
        public a() {
        }

        public /* synthetic */ a(byte b2) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public static List<IdFriend> a(JSONArray jSONArray) {
            j.b(jSONArray, ShareConstants.WEB_DIALOG_PARAM_DATA);
            c b2 = d.b(0, jSONArray.length());
            ArrayList arrayList = new ArrayList();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                IdFriend idFriend = null;
                try {
                    JSONObject optJSONObject = jSONArray.optJSONObject(((ah) it).a());
                    if (optJSONObject != null) {
                        a aVar = IdFriend.Companion;
                        String string = optJSONObject.getString("scid");
                        j.a((Object) string, "jsonObject.getString(\"scid\")");
                        Object opt = optJSONObject.opt("name");
                        if (opt == null || j.a(opt, JSONObject.NULL)) {
                            opt = null;
                        }
                        String str = (opt == null || !(opt instanceof String)) ? null : (String) opt;
                        if (str == null) {
                            str = "";
                        }
                        Object opt2 = optJSONObject.opt("avatarImage");
                        if (opt2 == null || j.a(opt2, JSONObject.NULL)) {
                            opt2 = null;
                        }
                        String str2 = (opt2 == null || !(opt2 instanceof String)) ? null : (String) opt2;
                        if (str2 == null) {
                            str2 = "";
                        }
                        idFriend = new IdFriend(string, str, str2);
                    }
                } catch (JSONException e) {
                    StringBuilder a2 = b.a.a.a.a.a("JSON Parse error ");
                    a2.append(e.getLocalizedMessage());
                    a2.toString();
                }
                if (idFriend != null) {
                    arrayList.add(idFriend);
                }
            }
            return arrayList;
        }
    }
}
