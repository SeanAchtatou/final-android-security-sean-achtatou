package com.supercell.id.view;

import android.view.View;

public final class y implements View.OnLayoutChangeListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ RootFrameLayout f4960a;

    public y(RootFrameLayout rootFrameLayout) {
        this.f4960a = rootFrameLayout;
    }

    public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        RootFrameLayout rootFrameLayout = this.f4960a;
        rootFrameLayout.post(rootFrameLayout.getPropagateSystemWindowInsets());
    }
}
