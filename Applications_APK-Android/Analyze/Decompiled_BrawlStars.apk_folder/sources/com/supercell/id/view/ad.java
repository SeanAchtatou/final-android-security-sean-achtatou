package com.supercell.id.view;

import android.support.design.widget.TabLayout;

public final class ad implements TabLayout.OnTabSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ SubPageTabLayout f4932a;

    public ad(SubPageTabLayout subPageTabLayout) {
        this.f4932a = subPageTabLayout;
    }

    public final void onTabReselected(TabLayout.Tab tab) {
    }

    public final void onTabSelected(TabLayout.Tab tab) {
        if (tab != null) {
            SubPageTabLayout.a(tab, true);
        }
    }

    public final void onTabUnselected(TabLayout.Tab tab) {
        if (tab != null) {
            SubPageTabLayout.a(tab, false);
        }
    }
}
