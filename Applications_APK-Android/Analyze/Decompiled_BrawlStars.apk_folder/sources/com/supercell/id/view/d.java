package com.supercell.id.view;

import android.animation.ValueAnimator;

public final class d implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ AvatarView f4936a;

    public d(AvatarView avatarView) {
        this.f4936a = avatarView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f4936a.invalidate();
    }
}
