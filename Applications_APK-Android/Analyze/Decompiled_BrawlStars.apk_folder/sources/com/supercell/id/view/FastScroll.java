package com.supercell.id.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import b.b.a.k.b;
import com.supercell.id.R;
import java.util.HashMap;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class FastScroll extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    public RecyclerView f4907a;

    /* renamed from: b  reason: collision with root package name */
    public final l f4908b;
    public final k c;
    public final h d;
    public final Runnable e;
    public ObjectAnimator f;
    public HashMap g;

    public FastScroll(Context context) {
        this(context, null, 0, 6, null);
    }

    public FastScroll(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FastScroll(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        j.b(context, "context");
        this.f4908b = new l(this);
        this.c = new k(this);
        this.d = new h(this);
        this.e = new i(this);
        setOrientation(0);
        setClipChildren(false);
        LayoutInflater.from(context).inflate(R.layout.fastscroll, this);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FastScroll(Context context, AttributeSet attributeSet, int i, int i2, g gVar) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* access modifiers changed from: private */
    public void a() {
        ObjectAnimator duration = ObjectAnimator.ofFloat((FrameLayout) a(R.id.fastscroll_bubble), View.ALPHA, 1.0f, 0.0f).setDuration(100L);
        duration.addListener(new j(this));
        duration.start();
        this.f = duration;
    }

    private final b getIndexedAdapter() {
        RecyclerView recyclerView = this.f4907a;
        RecyclerView.Adapter adapter = recyclerView != null ? recyclerView.getAdapter() : null;
        if (!(adapter instanceof b)) {
            adapter = null;
        }
        return (b) adapter;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x010a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void setRecyclerViewPosition(float r9) {
        /*
            r8 = this;
            android.support.v7.widget.RecyclerView r0 = r8.f4907a
            if (r0 == 0) goto L_0x0111
            android.support.v7.widget.RecyclerView$Adapter r1 = r0.getAdapter()
            r2 = 0
            if (r1 == 0) goto L_0x0010
            int r1 = r1.getItemCount()
            goto L_0x0011
        L_0x0010:
            r1 = 0
        L_0x0011:
            if (r1 != 0) goto L_0x0014
            return
        L_0x0014:
            int r3 = com.supercell.id.R.id.fastscroll_thumb
            android.view.View r3 = r8.a(r3)
            android.widget.ImageView r3 = (android.widget.ImageView) r3
            java.lang.String r4 = "fastscroll_thumb"
            kotlin.d.b.j.a(r3, r4)
            int r3 = r3.getHeight()
            r5 = 2
            int r3 = r3 / r5
            float r6 = (float) r3
            int r7 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r7 > 0) goto L_0x002e
            r9 = 0
            goto L_0x0052
        L_0x002e:
            int r7 = r8.getHeight()
            int r7 = r7 - r3
            float r3 = (float) r7
            int r3 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            if (r3 < 0) goto L_0x003b
            r9 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0052
        L_0x003b:
            float r9 = r9 - r6
            int r3 = r8.getHeight()
            int r6 = com.supercell.id.R.id.fastscroll_thumb
            android.view.View r6 = r8.a(r6)
            android.widget.ImageView r6 = (android.widget.ImageView) r6
            kotlin.d.b.j.a(r6, r4)
            int r4 = r6.getHeight()
            int r3 = r3 - r4
            float r3 = (float) r3
            float r9 = r9 / r3
        L_0x0052:
            float r3 = (float) r1
            float r9 = r9 * r3
            int r9 = (int) r9
            int r1 = r1 + -1
            int r3 = kotlin.d.b.j.a(r9, r2)
            if (r3 >= 0) goto L_0x0060
            r9 = 0
            goto L_0x0067
        L_0x0060:
            int r3 = kotlin.d.b.j.a(r9, r1)
            if (r3 <= 0) goto L_0x0067
            r9 = r1
        L_0x0067:
            r0.scrollToPosition(r9)
            b.b.a.k.b r0 = r8.getIndexedAdapter()
            if (r0 == 0) goto L_0x00a2
            b.b.a.i.d1$b r0 = (b.b.a.i.d1.b) r0
            java.util.List r0 = r0.a()
            java.lang.Object r9 = r0.get(r9)
            b.b.a.i.d1$e r9 = (b.b.a.i.d1.e) r9
            boolean r0 = r9 instanceof b.b.a.i.d1.e.a
            if (r0 == 0) goto L_0x0081
            goto L_0x00a2
        L_0x0081:
            boolean r0 = r9 instanceof b.b.a.i.d1.e.b
            if (r0 == 0) goto L_0x0086
            goto L_0x00a2
        L_0x0086:
            boolean r0 = r9 instanceof b.b.a.i.d1.e.c
            if (r0 == 0) goto L_0x008f
            b.b.a.i.d1$e$c r9 = (b.b.a.i.d1.e.c) r9
            java.lang.String r9 = r9.f216a
            goto L_0x00a3
        L_0x008f:
            boolean r0 = r9 instanceof b.b.a.i.d1.e.d
            if (r0 == 0) goto L_0x009c
            b.b.a.i.d1$e$d r9 = (b.b.a.i.d1.e.d) r9
            b.b.a.j.p0 r9 = r9.f217a
            java.lang.String r9 = r9.a()
            goto L_0x00a3
        L_0x009c:
            kotlin.NoWhenBranchMatchedException r9 = new kotlin.NoWhenBranchMatchedException
            r9.<init>()
            throw r9
        L_0x00a2:
            r9 = 0
        L_0x00a3:
            if (r9 == 0) goto L_0x010a
            int r0 = com.supercell.id.R.id.fastscroll_bubble
            android.view.View r0 = r8.a(r0)
            android.widget.FrameLayout r0 = (android.widget.FrameLayout) r0
            java.lang.String r1 = "fastscroll_bubble"
            kotlin.d.b.j.a(r0, r1)
            int r0 = r0.getVisibility()
            r3 = 4
            if (r0 != r3) goto L_0x00f9
            android.animation.ObjectAnimator r0 = r8.f
            if (r0 == 0) goto L_0x00c0
            r0.cancel()
        L_0x00c0:
            android.os.Handler r0 = r8.getHandler()
            java.lang.Runnable r3 = r8.e
            r0.removeCallbacks(r3)
            b.b.a.k.b r0 = r8.getIndexedAdapter()
            if (r0 == 0) goto L_0x00f9
            int r0 = com.supercell.id.R.id.fastscroll_bubble
            android.view.View r0 = r8.a(r0)
            android.widget.FrameLayout r0 = (android.widget.FrameLayout) r0
            android.util.Property r3 = android.view.View.ALPHA
            float[] r4 = new float[r5]
            r4 = {0, 1065353216} // fill-array
            android.animation.ObjectAnimator r0 = android.animation.ObjectAnimator.ofFloat(r0, r3, r4)
            r3 = 100
            android.animation.ObjectAnimator r0 = r0.setDuration(r3)
            int r3 = com.supercell.id.R.id.fastscroll_bubble
            android.view.View r3 = r8.a(r3)
            android.widget.FrameLayout r3 = (android.widget.FrameLayout) r3
            kotlin.d.b.j.a(r3, r1)
            r3.setVisibility(r2)
            r0.start()
        L_0x00f9:
            int r0 = com.supercell.id.R.id.fastscroll_bubble_button
            android.view.View r0 = r8.a(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r1 = "fastscroll_bubble_button"
            kotlin.d.b.j.a(r0, r1)
            r0.setText(r9)
            goto L_0x0111
        L_0x010a:
            android.animation.ObjectAnimator r9 = r8.f
            if (r9 != 0) goto L_0x0111
            r8.a()
        L_0x0111:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.id.view.FastScroll.setRecyclerViewPosition(float):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* access modifiers changed from: private */
    public final void setScrollPosition(float f2) {
        float height = f2 / ((float) getHeight());
        ImageView imageView = (ImageView) a(R.id.fastscroll_thumb);
        j.a((Object) imageView, "fastscroll_thumb");
        int height2 = imageView.getHeight();
        FrameLayout frameLayout = (FrameLayout) a(R.id.fastscroll_bubble);
        j.a((Object) frameLayout, "fastscroll_bubble");
        int height3 = frameLayout.getHeight();
        ImageView imageView2 = (ImageView) a(R.id.fastscroll_bubble_tail);
        j.a((Object) imageView2, "fastscroll_bubble_tail");
        double y = (double) imageView2.getY();
        ImageView imageView3 = (ImageView) a(R.id.fastscroll_bubble_tail);
        j.a((Object) imageView3, "fastscroll_bubble_tail");
        double height4 = (double) imageView3.getHeight();
        Double.isNaN(height4);
        Double.isNaN(y);
        double d2 = (height4 * 0.5d) + y;
        ImageView imageView4 = (ImageView) a(R.id.fastscroll_thumb);
        j.a((Object) imageView4, "fastscroll_thumb");
        int height5 = (int) (((float) (getHeight() - height2)) * height);
        int height6 = getHeight() - height2;
        if (j.a(height5, 0) < 0) {
            height5 = 0;
        } else if (j.a(height5, height6) > 0) {
            height5 = height6;
        }
        imageView4.setY((float) height5);
        FrameLayout frameLayout2 = (FrameLayout) a(R.id.fastscroll_bubble);
        j.a((Object) frameLayout2, "fastscroll_bubble");
        ImageView imageView5 = (ImageView) a(R.id.fastscroll_thumb);
        j.a((Object) imageView5, "fastscroll_thumb");
        double y2 = (double) imageView5.getY();
        double d3 = (double) height2;
        Double.isNaN(d3);
        Double.isNaN(y2);
        int i = (int) (((d3 * 0.5d) + y2) - d2);
        int height7 = getHeight() - height3;
        if (j.a(i, 0) < 0) {
            i = 0;
        } else if (j.a(i, height7) > 0) {
            i = height7;
        }
        frameLayout2.setY((float) i);
    }

    public final View a(int i) {
        if (this.g == null) {
            this.g = new HashMap();
        }
        View view = (View) this.g.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this.g.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final RecyclerView getRecyclerView() {
        return this.f4907a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006c, code lost:
        if (r8.getX() <= b.b.a.b.a(40)) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0015, code lost:
        if (r0 != 3) goto L_0x0080;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean onTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            java.lang.String r0 = "event"
            kotlin.d.b.j.b(r8, r0)
            int r0 = r8.getAction()
            java.lang.String r1 = "fastscroll_thumb"
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L_0x003a
            if (r0 == r3) goto L_0x0020
            r4 = 2
            if (r0 == r4) goto L_0x0018
            r4 = 3
            if (r0 == r4) goto L_0x0020
            goto L_0x0080
        L_0x0018:
            float r8 = r8.getY()
            r7.setRecyclerViewPosition(r8)
            goto L_0x0084
        L_0x0020:
            int r8 = com.supercell.id.R.id.fastscroll_thumb
            android.view.View r8 = r7.a(r8)
            android.widget.ImageView r8 = (android.widget.ImageView) r8
            kotlin.d.b.j.a(r8, r1)
            r8.setSelected(r2)
            android.os.Handler r8 = r7.getHandler()
            java.lang.Runnable r0 = r7.e
            r1 = 100
            r8.postDelayed(r0, r1)
            goto L_0x0084
        L_0x003a:
            int r0 = android.support.v4.view.ViewCompat.getLayoutDirection(r7)
            if (r0 != r3) goto L_0x0042
            r0 = 1
            goto L_0x0043
        L_0x0042:
            r0 = 0
        L_0x0043:
            r4 = 40
            if (r0 != 0) goto L_0x0059
            float r0 = r8.getX()
            int r5 = r7.getWidth()
            float r5 = (float) r5
            float r6 = b.b.a.b.a(r4)
            float r5 = r5 - r6
            int r0 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x006e
        L_0x0059:
            int r0 = android.support.v4.view.ViewCompat.getLayoutDirection(r7)
            if (r0 != r3) goto L_0x0060
            r2 = 1
        L_0x0060:
            if (r2 == 0) goto L_0x0080
            float r0 = r8.getX()
            float r2 = b.b.a.b.a(r4)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x0080
        L_0x006e:
            int r8 = com.supercell.id.R.id.fastscroll_thumb
            android.view.View r8 = r7.a(r8)
            android.widget.ImageView r8 = (android.widget.ImageView) r8
            kotlin.d.b.j.a(r8, r1)
            r8.setSelected(r3)
            r7.requestDisallowInterceptTouchEvent(r3)
            goto L_0x0084
        L_0x0080:
            boolean r3 = super.onTouchEvent(r8)
        L_0x0084:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.id.view.FastScroll.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public final void setRecyclerView(RecyclerView recyclerView) {
        RecyclerView.Adapter adapter;
        RecyclerView.Adapter adapter2;
        RecyclerView recyclerView2 = this.f4907a;
        if (recyclerView2 != null) {
            recyclerView2.removeOnScrollListener(this.f4908b);
        }
        RecyclerView recyclerView3 = this.f4907a;
        if (!(recyclerView3 == null || (adapter2 = recyclerView3.getAdapter()) == null)) {
            adapter2.unregisterAdapterDataObserver(this.d);
        }
        this.f4907a = recyclerView;
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(this.f4908b);
        }
        if (!(recyclerView == null || (adapter = recyclerView.getAdapter()) == null)) {
            adapter.registerAdapterDataObserver(this.d);
        }
        if (recyclerView != null) {
            recyclerView.addOnLayoutChangeListener(this.c);
        }
    }
}
