package com.supercell.id.view;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.supercell.id.R;
import java.util.Map;
import kotlin.a.ai;
import kotlin.d.a.b;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class SubPageTabLayout extends TabLayout {

    /* renamed from: a  reason: collision with root package name */
    public ViewPager f4920a;

    /* renamed from: b  reason: collision with root package name */
    public b<? super Integer, String> f4921b;
    public b<? super Integer, ? extends Map<String, String>> c;
    public final ad d;

    public SubPageTabLayout(Context context) {
        this(context, null, 0, 6, null);
    }

    public SubPageTabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubPageTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        j.b(context, "context");
        this.f4921b = ab.f4930a;
        this.c = ac.f4931a;
        this.d = new ad(this);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SubPageTabLayout(Context context, AttributeSet attributeSet, int i, int i2, g gVar) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* access modifiers changed from: private */
    public static void a(TabLayout.Tab tab, boolean z) {
        View customView = tab.getCustomView();
        if (customView != null) {
            j.a((Object) customView, "view");
            ((TextView) customView.findViewById(R.id.tab_title)).setTextColor(ContextCompat.getColor(customView.getContext(), z ? R.color.black : R.color.gray40));
        }
    }

    private void b() {
        int i;
        Context context;
        PagerAdapter adapter;
        ViewPager viewPager = this.f4920a;
        if (viewPager == null || !(viewPager == null || (adapter = viewPager.getAdapter()) == null || adapter.getCount() != 1)) {
            context = getContext();
            i = R.color.gray91;
        } else {
            context = getContext();
            i = R.color.black;
        }
        setSelectedTabIndicatorColor(ContextCompat.getColor(context, i));
    }

    public final void a() {
        this.f4920a = null;
        super.setupWithViewPager(null);
        removeOnTabSelectedListener(this.d);
        addTab(newTab());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void addTab(TabLayout.Tab tab, int i, boolean z) {
        String invoke;
        j.b(tab, "tab");
        super.addTab(tab, i, z);
        tab.setCustomView(R.layout.subpage_tab_button);
        View customView = tab.getCustomView();
        if (!(customView == null || (invoke = this.f4921b.invoke(Integer.valueOf(i))) == null)) {
            j.a((Object) customView, "view");
            TextView textView = (TextView) customView.findViewById(R.id.tab_title);
            j.a((Object) textView, "view.tab_title");
            Map map = (Map) this.c.invoke(Integer.valueOf(i));
            if (map == null) {
                map = ai.a();
            }
            b.b.a.i.x1.j.a(textView, invoke, map);
        }
        TabLayout.TabView tabView = tab.view;
        if (!(tabView instanceof View)) {
            tabView = null;
        }
        boolean z2 = false;
        if (tabView != null) {
            tabView.setSoundEffectsEnabled(false);
            tabView.setOnClickListener(new aa(this));
        }
        ViewPager viewPager = this.f4920a;
        if (i == (viewPager != null ? viewPager.getCurrentItem() : 0)) {
            z2 = true;
        }
        a(tab, z2);
        b();
    }

    public final b<Integer, String> getGetTitleKey() {
        return this.f4921b;
    }

    public final b<Integer, Map<String, String>> getGetTitleReplacements() {
        return this.c;
    }

    public final void setGetTitleKey(b<? super Integer, String> bVar) {
        j.b(bVar, "<set-?>");
        this.f4921b = bVar;
    }

    public final void setGetTitleReplacements(b<? super Integer, ? extends Map<String, String>> bVar) {
        j.b(bVar, "<set-?>");
        this.c = bVar;
    }

    public final void setupWithViewPager(ViewPager viewPager, boolean z) {
        this.f4920a = viewPager;
        super.setupWithViewPager(viewPager, z);
        removeOnTabSelectedListener(this.d);
        addOnTabSelectedListener(this.d);
        b();
    }
}
