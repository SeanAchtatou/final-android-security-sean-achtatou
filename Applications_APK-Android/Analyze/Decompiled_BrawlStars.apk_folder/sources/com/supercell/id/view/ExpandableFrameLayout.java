package com.supercell.id.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import java.util.HashMap;
import kotlin.d.a.c;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.m;

public final class ExpandableFrameLayout extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    public float f4901a;

    /* renamed from: b  reason: collision with root package name */
    public b f4902b;
    public ValueAnimator c;
    public c<? super Float, ? super b, m> d;
    public HashMap e;

    public final class a implements Animator.AnimatorListener {

        /* renamed from: a  reason: collision with root package name */
        public boolean f4903a;

        /* renamed from: b  reason: collision with root package name */
        public final int f4904b;

        public a(int i) {
            this.f4904b = i;
        }

        public final void onAnimationCancel(Animator animator) {
            j.b(animator, "animation");
            this.f4903a = true;
        }

        public final void onAnimationEnd(Animator animator) {
            j.b(animator, "animation");
            if (!this.f4903a) {
                ExpandableFrameLayout.this.f4902b = this.f4904b == 0 ? b.COLLAPSED : b.EXPANDED;
                ExpandableFrameLayout.this.setExpansion((float) this.f4904b);
            }
        }

        public final void onAnimationRepeat(Animator animator) {
            j.b(animator, "animation");
        }

        public final void onAnimationStart(Animator animator) {
            j.b(animator, "animation");
            ExpandableFrameLayout.this.f4902b = this.f4904b == 0 ? b.COLLAPSING : b.EXPANDING;
        }
    }

    public enum b {
        COLLAPSED,
        COLLAPSING,
        EXPANDING,
        EXPANDED
    }

    public ExpandableFrameLayout(Context context) {
        this(context, null, 2, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.supercell.id.view.ExpandableFrameLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.supercell.id.view.ExpandableFrameLayout.a(com.supercell.id.view.ExpandableFrameLayout, float):void
      com.supercell.id.view.ExpandableFrameLayout.a(boolean, boolean):void */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExpandableFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        j.b(context, "context");
        this.f4902b = b.COLLAPSED;
        if (isInEditMode()) {
            a(true, false);
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ExpandableFrameLayout(Context context, AttributeSet attributeSet, int i, g gVar) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setExpansion(float r4) {
        /*
            r3 = this;
            float r0 = r3.f4901a
            int r1 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r1 != 0) goto L_0x0007
            return
        L_0x0007:
            float r0 = r4 - r0
            r1 = 0
            int r2 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r2 != 0) goto L_0x0013
            com.supercell.id.view.ExpandableFrameLayout$b r0 = com.supercell.id.view.ExpandableFrameLayout.b.COLLAPSED
        L_0x0010:
            r3.f4902b = r0
            goto L_0x002a
        L_0x0013:
            r2 = 1065353216(0x3f800000, float:1.0)
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 != 0) goto L_0x001c
            com.supercell.id.view.ExpandableFrameLayout$b r0 = com.supercell.id.view.ExpandableFrameLayout.b.EXPANDED
            goto L_0x0010
        L_0x001c:
            int r2 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r2 >= 0) goto L_0x0023
            com.supercell.id.view.ExpandableFrameLayout$b r0 = com.supercell.id.view.ExpandableFrameLayout.b.COLLAPSING
            goto L_0x0010
        L_0x0023:
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x002a
            com.supercell.id.view.ExpandableFrameLayout$b r0 = com.supercell.id.view.ExpandableFrameLayout.b.EXPANDING
            goto L_0x0010
        L_0x002a:
            com.supercell.id.view.ExpandableFrameLayout$b r0 = r3.f4902b
            com.supercell.id.view.ExpandableFrameLayout$b r1 = com.supercell.id.view.ExpandableFrameLayout.b.COLLAPSED
            if (r0 != r1) goto L_0x0033
            r0 = 8
            goto L_0x0034
        L_0x0033:
            r0 = 0
        L_0x0034:
            r3.setVisibility(r0)
            r3.f4901a = r4
            r3.requestLayout()
            kotlin.d.a.c<? super java.lang.Float, ? super com.supercell.id.view.ExpandableFrameLayout$b, kotlin.m> r0 = r3.d
            if (r0 == 0) goto L_0x0049
            java.lang.Float r4 = java.lang.Float.valueOf(r4)
            com.supercell.id.view.ExpandableFrameLayout$b r1 = r3.f4902b
            r0.invoke(r4, r1)
        L_0x0049:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.id.view.ExpandableFrameLayout.setExpansion(float):void");
    }

    public final View a(int i) {
        if (this.e == null) {
            this.e = new HashMap();
        }
        View view = (View) this.e.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this.e.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a(boolean z, boolean z2) {
        if (z != a()) {
            ValueAnimator valueAnimator = this.c;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            this.c = null;
            if (z2) {
                ValueAnimator ofFloat = ValueAnimator.ofFloat(this.f4901a, z ? 1.0f : 0.0f);
                ofFloat.setInterpolator(b.b.a.f.a.f62a);
                ofFloat.setDuration(300L);
                ofFloat.addUpdateListener(new g(this));
                ofFloat.addListener(new a(z));
                ofFloat.start();
                this.c = ofFloat;
                return;
            }
            setExpansion(z ? 1.0f : 0.0f);
        }
    }

    public final boolean a() {
        b bVar = this.f4902b;
        return bVar == b.EXPANDING || bVar == b.EXPANDED;
    }

    public final float getExpansionFraction() {
        return this.f4901a;
    }

    public final b getState() {
        return this.f4902b;
    }

    public final void onConfigurationChanged(Configuration configuration) {
        j.b(configuration, "newConfig");
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        super.onConfigurationChanged(configuration);
    }

    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        setVisibility((this.f4901a == 0.0f && measuredHeight == 0) ? 8 : 0);
        setMeasuredDimension(measuredWidth, measuredHeight - (measuredHeight - kotlin.e.a.a(((float) measuredHeight) * this.f4901a)));
    }

    public final void onRestoreInstanceState(Parcelable parcelable) {
        j.b(parcelable, "parcelable");
        Bundle bundle = (Bundle) parcelable;
        this.f4901a = bundle.getFloat("expansionFraction");
        this.f4902b = this.f4901a == 1.0f ? b.EXPANDED : b.COLLAPSED;
        super.onRestoreInstanceState(bundle.getParcelable("super_state"));
    }

    public final Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        this.f4901a = a() ? 1.0f : 0.0f;
        bundle.putFloat("expansionFraction", this.f4901a);
        bundle.putParcelable("super_state", onSaveInstanceState);
        return bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.supercell.id.view.ExpandableFrameLayout.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.supercell.id.view.ExpandableFrameLayout.a(com.supercell.id.view.ExpandableFrameLayout, float):void
      com.supercell.id.view.ExpandableFrameLayout.a(boolean, boolean):void */
    public final void setExpanded(boolean z) {
        a(z, true);
    }

    public final void setOnStateChangeListener(c<? super Float, ? super b, m> cVar) {
        j.b(cVar, "listener");
        this.d = cVar;
    }
}
