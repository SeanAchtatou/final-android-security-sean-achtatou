package com.supercell.id.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import java.util.ArrayList;
import java.util.List;
import kotlin.d.b.j;

public final class RootFrameLayout extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    public Rect f4918a = new Rect();

    /* renamed from: b  reason: collision with root package name */
    public final List<a> f4919b = new ArrayList();
    public final View.OnLayoutChangeListener c = new y(this);
    public final Runnable d = new z(this);

    public interface a {
        void a(Rect rect);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RootFrameLayout(Context context) {
        super(context);
        j.b(context, "context");
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RootFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        j.b(context, "context");
        j.b(attributeSet, "attrs");
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RootFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        j.b(context, "context");
        j.b(attributeSet, "attrs");
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RootFrameLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        j.b(context, "context");
        j.b(attributeSet, "attrs");
    }

    public final void a(a aVar) {
        j.b(aVar, "listener");
        this.f4919b.add(aVar);
        aVar.a(this.f4918a);
    }

    public final void b(a aVar) {
        j.b(aVar, "listener");
        this.f4919b.remove(aVar);
    }

    public final boolean fitSystemWindows(Rect rect) {
        j.b(rect, "insets");
        this.f4918a = rect;
        for (a a2 : this.f4919b) {
            a2.a(this.f4918a);
        }
        return false;
    }

    public final View.OnLayoutChangeListener getLayoutChangeListener() {
        return this.c;
    }

    public final Runnable getPropagateSystemWindowInsets() {
        return this.d;
    }

    public final Rect getSystemWindowInsets() {
        return this.f4918a;
    }

    public final void onAttachedToWindow() {
        addOnLayoutChangeListener(this.c);
        super.onAttachedToWindow();
    }

    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeOnLayoutChangeListener(this.c);
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if ((motionEvent != null ? motionEvent.getPointerCount() : 0) > 1) {
            return true;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }
}
