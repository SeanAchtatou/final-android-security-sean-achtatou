package com.supercell.id.view;

import android.animation.Animator;
import com.supercell.id.view.AvatarView;

public final class c implements Animator.AnimatorListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ AvatarView f4935a;

    public c(AvatarView avatarView) {
        this.f4935a = avatarView;
    }

    public final void onAnimationCancel(Animator animator) {
        this.f4935a.imageAnimationState = AvatarView.a.NONE;
    }

    public final void onAnimationEnd(Animator animator) {
        this.f4935a.imageAnimationState = AvatarView.a.NONE;
    }

    public final void onAnimationRepeat(Animator animator) {
    }

    public final void onAnimationStart(Animator animator) {
    }
}
