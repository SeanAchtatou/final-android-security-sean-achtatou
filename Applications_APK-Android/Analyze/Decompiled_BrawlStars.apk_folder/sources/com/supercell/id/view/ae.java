package com.supercell.id.view;

import android.widget.CompoundButton;

public final class ae implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ Switch f4933a;

    public ae(Switch switchR) {
        this.f4933a = switchR;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        CompoundButton.OnCheckedChangeListener onCheckedChangeListener = this.f4933a.f4922a;
        if (onCheckedChangeListener != null) {
            onCheckedChangeListener.onCheckedChanged(compoundButton, z);
        }
    }
}
