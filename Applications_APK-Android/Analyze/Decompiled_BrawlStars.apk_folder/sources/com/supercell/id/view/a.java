package com.supercell.id.view;

import android.graphics.drawable.Drawable;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class a extends k implements c<Drawable, b.b.a.i.x1.c, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ AvatarView f4928a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(AvatarView avatarView) {
        super(2);
        this.f4928a = avatarView;
    }

    public final Object invoke(Object obj, Object obj2) {
        Drawable drawable = (Drawable) obj;
        j.b(drawable, "avatarAtlas");
        j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
        this.f4928a.setAtlas(drawable);
        return m.f5330a;
    }
}
