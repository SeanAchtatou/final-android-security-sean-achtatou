package com.supercell.id.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.view.animation.PathInterpolatorCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;
import b.b.a.j.q0;
import com.supercell.id.SupercellId;
import java.util.HashMap;
import java.util.List;
import kotlin.d.b.j;

public final class AvatarView extends AppCompatImageView {
    public static final long ANIMATION_DURATION = 200;
    public static final b Companion = new b();
    public static final int INTRINSIC_POINT_SIZE = 160;
    public HashMap _$_findViewCache;
    public ObjectAnimator animateBg;
    public ObjectAnimator animateImage;
    public Drawable atlas;
    public String avatarName = "";
    public a bgAnimationState;
    public float bgPosition;
    public Rect canvasBounds = new Rect();
    public GradientDrawable currentBackground;
    public Bitmap currentImage;
    public Rect currentImageBounds = new Rect();
    public Rect enteringBgBounds = new Rect();
    public GradientDrawable exitingBackground;
    public Bitmap exitingImage;
    public Rect exitingImageBounds = new Rect();
    public a imageAnimationState;
    public float imagePosition;
    public Paint maskPaint;
    public Path maskPath;

    public enum a {
        NONE,
        FROM_LEFT,
        FROM_RIGHT
    }

    public static final class b {
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AvatarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        j.b(context, "context");
        a aVar = a.NONE;
        this.imageAnimationState = aVar;
        this.bgAnimationState = aVar;
        Paint paint = new Paint(1);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        this.maskPaint = paint;
        this.maskPath = new Path();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("portraits.png", new a(this));
        setLayerType(1, null);
    }

    public static /* synthetic */ void bgPosition$annotations() {
    }

    public static /* synthetic */ void imagePosition$annotations() {
    }

    /* access modifiers changed from: private */
    public final void setAtlas(Drawable drawable) {
        this.atlas = drawable;
        setAvatar$default(this, this.avatarName, null, 2, null);
    }

    public static /* synthetic */ void setAvatar$default(AvatarView avatarView, String str, a aVar, int i, Object obj) {
        if ((i & 2) != 0) {
            aVar = a.NONE;
        }
        avatarView.setAvatar(str, aVar);
    }

    public static /* synthetic */ void setBackgroundGradient$default(AvatarView avatarView, int i, int i2, a aVar, int i3, Object obj) {
        if ((i3 & 4) != 0) {
            aVar = a.NONE;
        }
        avatarView.setBackgroundGradient(i, i2, aVar);
    }

    public final void _$_clearFindViewByIdCache() {
        HashMap hashMap = this._$_findViewCache;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final float getBgPosition() {
        return this.bgPosition;
    }

    public final float getImagePosition() {
        return this.imagePosition;
    }

    public final void onDraw(Canvas canvas) {
        int i;
        Rect rect;
        if (canvas != null) {
            super.onDraw(canvas);
            canvas.getClipBounds(this.canvasBounds);
            canvas.getClipBounds(this.currentImageBounds);
            canvas.getClipBounds(this.exitingImageBounds);
            canvas.getClipBounds(this.enteringBgBounds);
            GradientDrawable gradientDrawable = this.currentBackground;
            if (gradientDrawable != null) {
                a aVar = this.bgAnimationState;
                if (aVar != a.NONE) {
                    float f = this.bgPosition;
                    if (!(f == 0.0f || this.exitingBackground == null)) {
                        if (aVar == a.FROM_LEFT) {
                            rect = this.enteringBgBounds;
                            i = (int) f;
                        } else {
                            rect = this.enteringBgBounds;
                            i = -((int) f);
                        }
                        rect.left = i;
                        rect.right = getWidth() + rect.left;
                        GradientDrawable gradientDrawable2 = this.exitingBackground;
                        if (gradientDrawable2 != null) {
                            gradientDrawable2.setBounds(this.canvasBounds);
                            gradientDrawable2.draw(canvas);
                        }
                        gradientDrawable.setBounds(this.enteringBgBounds);
                        gradientDrawable.setShape(1);
                        gradientDrawable.draw(canvas);
                        "bounds " + this.enteringBgBounds.left;
                    }
                }
                gradientDrawable.setBounds(this.canvasBounds);
                gradientDrawable.draw(canvas);
            }
            Bitmap bitmap = this.currentImage;
            if (bitmap != null) {
                Bitmap bitmap2 = this.exitingImage;
                a aVar2 = this.imageAnimationState;
                if (aVar2 != a.NONE) {
                    float f2 = this.imagePosition;
                    if (!(f2 == 0.0f || bitmap2 == null)) {
                        if (aVar2 == a.FROM_LEFT) {
                            Rect rect2 = this.currentImageBounds;
                            rect2.left = (int) f2;
                            rect2.right = getWidth() + rect2.left;
                            Rect rect3 = this.exitingImageBounds;
                            rect3.left = this.currentImageBounds.right;
                            rect3.right = getWidth() + rect3.left;
                        } else {
                            Rect rect4 = this.currentImageBounds;
                            rect4.left = -((int) f2);
                            rect4.right = getWidth() + rect4.left;
                            Rect rect5 = this.exitingImageBounds;
                            rect5.right = this.currentImageBounds.left;
                            rect5.left = rect5.right - getWidth();
                        }
                        canvas.drawBitmap(bitmap, (Rect) null, this.currentImageBounds, (Paint) null);
                        canvas.drawBitmap(bitmap2, (Rect) null, this.exitingImageBounds, (Paint) null);
                    }
                }
                canvas.drawBitmap(bitmap, (Rect) null, this.canvasBounds, (Paint) null);
            }
            this.maskPath.moveTo(0.0f, 0.0f);
            canvas.drawPath(this.maskPath, this.maskPaint);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0054  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r9, int r10) {
        /*
            r8 = this;
            int r0 = android.view.View.MeasureSpec.getMode(r9)
            int r1 = android.view.View.MeasureSpec.getMode(r10)
            r2 = 1073741824(0x40000000, float:2.0)
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 160(0xa0, float:2.24E-43)
            r5 = 1065353216(0x3f800000, float:1.0)
            if (r0 == r3) goto L_0x0025
            if (r0 == 0) goto L_0x001c
            if (r0 == r2) goto L_0x0017
            goto L_0x001c
        L_0x0017:
            int r9 = android.view.View.MeasureSpec.getSize(r9)
            goto L_0x003f
        L_0x001c:
            float r9 = b.b.a.b.a(r4)
        L_0x0020:
            int r9 = kotlin.e.a.a(r9)
            goto L_0x003f
        L_0x0025:
            int r9 = android.view.View.MeasureSpec.getSize(r9)
            float r9 = (float) r9
            float r6 = b.b.a.b.a(r4)
            int r7 = java.lang.Float.compare(r9, r5)
            if (r7 >= 0) goto L_0x0037
            r9 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0020
        L_0x0037:
            int r7 = java.lang.Float.compare(r9, r6)
            if (r7 <= 0) goto L_0x0020
            r9 = r6
            goto L_0x0020
        L_0x003f:
            if (r1 == r3) goto L_0x0054
            if (r1 == 0) goto L_0x004b
            if (r1 == r2) goto L_0x0046
            goto L_0x004b
        L_0x0046:
            int r10 = android.view.View.MeasureSpec.getSize(r10)
            goto L_0x006e
        L_0x004b:
            float r10 = b.b.a.b.a(r4)
        L_0x004f:
            int r10 = kotlin.e.a.a(r10)
            goto L_0x006e
        L_0x0054:
            int r10 = android.view.View.MeasureSpec.getSize(r10)
            float r10 = (float) r10
            float r1 = b.b.a.b.a(r4)
            int r4 = java.lang.Float.compare(r10, r5)
            if (r4 >= 0) goto L_0x0066
            r10 = 1065353216(0x3f800000, float:1.0)
            goto L_0x004f
        L_0x0066:
            int r4 = java.lang.Float.compare(r10, r1)
            if (r4 <= 0) goto L_0x004f
            r10 = r1
            goto L_0x004f
        L_0x006e:
            if (r0 == r2) goto L_0x0074
            if (r0 != r3) goto L_0x0073
            goto L_0x0074
        L_0x0073:
            r9 = r10
        L_0x0074:
            r8.setMeasuredDimension(r9, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.id.view.AvatarView.onMeasure(int, int):void");
    }

    public final void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        this.maskPath = path;
        float f = (float) i;
        float f2 = f / 2.0f;
        float f3 = (float) i2;
        float f4 = f3 / 2.0f;
        float max = Math.max(f2, f4);
        float f5 = -f;
        this.maskPath.addRect(f5, 0.0f, f * 2.0f, f3, Path.Direction.CCW);
        this.maskPath.addCircle(f2, f4, max, Path.Direction.CCW);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "imagePosition", f5, 0.0f);
        ofFloat.setInterpolator(PathInterpolatorCompat.create(0.5f, 0.0f, 0.65f, 1.0f));
        ofFloat.setDuration(200L);
        ofFloat.addUpdateListener(new b(this));
        ofFloat.addListener(new c(this));
        this.animateImage = ofFloat;
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this, "bgPosition", f5, 0.0f);
        ofFloat2.setInterpolator(PathInterpolatorCompat.create(0.5f, 0.0f, 0.65f, 1.0f));
        ofFloat2.setDuration(200L);
        ofFloat2.addUpdateListener(new d(this));
        ofFloat2.addListener(new e(this));
        this.animateBg = ofFloat2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Bitmap, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void setAvatar(String str, a aVar) {
        Bitmap bitmap;
        j.b(str, "avatarName");
        j.b(aVar, "animate");
        this.avatarName = str;
        Drawable drawable = this.atlas;
        if (drawable != null) {
            List<String> d = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().d(q0.NAMES);
            int indexOf = d != null ? d.indexOf(str) : -1;
            if (aVar != a.NONE) {
                this.exitingImage = this.currentImage;
                this.imageAnimationState = aVar;
                ObjectAnimator objectAnimator = this.animateImage;
                if (objectAnimator != null) {
                    objectAnimator.start();
                }
            }
            j.b(drawable, "atlas");
            if (indexOf < 0 || !(drawable instanceof BitmapDrawable)) {
                bitmap = null;
            } else {
                Bitmap bitmap2 = ((BitmapDrawable) drawable).getBitmap();
                j.a((Object) bitmap2, "bitmapAtlas");
                float width = (((float) bitmap2.getWidth()) / 10.0f) / 90.0f;
                int i = (int) (width * 90.0f);
                bitmap = Bitmap.createBitmap(bitmap2, (int) (((float) ((indexOf % 10) * 90)) * width), (int) (((float) ((indexOf / 10) * 90)) * width), i, i);
            }
            this.currentImage = bitmap;
            invalidate();
        }
    }

    public final void setBackgroundGradient(int i, int i2, a aVar) {
        j.b(aVar, "animate");
        if (aVar != a.NONE) {
            this.exitingBackground = this.currentBackground;
            this.bgAnimationState = aVar;
            ObjectAnimator objectAnimator = this.animateBg;
            if (objectAnimator != null) {
                objectAnimator.start();
            }
        }
        this.currentBackground = new GradientDrawable(GradientDrawable.Orientation.TL_BR, new int[]{i, i2});
        invalidate();
    }

    public final void setBgPosition(float f) {
        this.bgPosition = f;
    }

    public final void setImagePosition(float f) {
        this.imagePosition = f;
    }
}
