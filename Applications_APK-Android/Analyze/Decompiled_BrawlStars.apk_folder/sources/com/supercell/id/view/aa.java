package com.supercell.id.view;

import android.view.View;
import b.b.a.e.a;
import com.supercell.id.SupercellId;
import kotlin.d.b.j;

public final class aa implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ SubPageTabLayout f4929a;

    public aa(SubPageTabLayout subPageTabLayout) {
        this.f4929a = subPageTabLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onClick(View view) {
        if (this.f4929a.getTabCount() > 1) {
            SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.TAB_SWITCH);
        }
        j.a((Object) view, "view");
        view.setSelected(true);
    }
}
