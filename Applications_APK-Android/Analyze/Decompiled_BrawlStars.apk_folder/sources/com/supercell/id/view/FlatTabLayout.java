package com.supercell.id.view;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.supercell.id.R;
import kotlin.d.a.b;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class FlatTabLayout extends TabLayout {

    /* renamed from: a  reason: collision with root package name */
    public ViewPager f4909a;

    /* renamed from: b  reason: collision with root package name */
    public b<? super Integer, String> f4910b;
    public b<? super Integer, String> c;
    public b<? super Integer, String> d;
    public final q e;

    public FlatTabLayout(Context context) {
        this(context, null, 0, 6, null);
    }

    public FlatTabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlatTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        j.b(context, "context");
        this.f4910b = p.f4948a;
        this.c = o.f4947a;
        this.d = n.f4946a;
        this.e = new q(this);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FlatTabLayout(Context context, AttributeSet attributeSet, int i, int i2, g gVar) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String, int, int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void */
    /* access modifiers changed from: private */
    public void a(TabLayout.Tab tab, boolean z) {
        View customView = tab.getCustomView();
        if (customView != null) {
            j.a((Object) customView, "view");
            ((TextView) customView.findViewById(R.id.tab_title)).setTextColor(ContextCompat.getColor(customView.getContext(), z ? R.color.black : R.color.gray40));
            String str = (String) (z ? this.c : this.d).invoke(Integer.valueOf(tab.getPosition()));
            if (str != null) {
                EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) customView.findViewById(R.id.tab_icon);
                j.a((Object) edgeAntialiasingImageView, "view.tab_icon");
                b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView, str, false, 2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void addTab(TabLayout.Tab tab, int i, boolean z) {
        String invoke;
        j.b(tab, "tab");
        super.addTab(tab, i, z);
        tab.setCustomView(R.layout.flat_tab_button);
        View customView = tab.getCustomView();
        if (!(customView == null || (invoke = this.f4910b.invoke(Integer.valueOf(i))) == null)) {
            j.a((Object) customView, "view");
            TextView textView = (TextView) customView.findViewById(R.id.tab_title);
            j.a((Object) textView, "view.tab_title");
            b.b.a.i.x1.j.a(textView, invoke, (b) null, 2);
        }
        TabLayout.TabView tabView = tab.view;
        if (!(tabView instanceof View)) {
            tabView = null;
        }
        boolean z2 = false;
        if (tabView != null) {
            tabView.setSoundEffectsEnabled(false);
            tabView.setOnClickListener(m.f4945a);
        }
        ViewPager viewPager = this.f4909a;
        if (viewPager != null && i == viewPager.getCurrentItem()) {
            z2 = true;
        }
        a(tab, z2);
    }

    public final b<Integer, String> getGetIconDisabledKey() {
        return this.d;
    }

    public final b<Integer, String> getGetIconKey() {
        return this.c;
    }

    public final b<Integer, String> getGetTitleKey() {
        return this.f4910b;
    }

    public final void setGetIconDisabledKey(b<? super Integer, String> bVar) {
        j.b(bVar, "<set-?>");
        this.d = bVar;
    }

    public final void setGetIconKey(b<? super Integer, String> bVar) {
        j.b(bVar, "<set-?>");
        this.c = bVar;
    }

    public final void setGetTitleKey(b<? super Integer, String> bVar) {
        j.b(bVar, "<set-?>");
        this.f4910b = bVar;
    }

    public final void setupWithViewPager(ViewPager viewPager, boolean z) {
        this.f4909a = viewPager;
        super.setupWithViewPager(viewPager, z);
        removeOnTabSelectedListener(this.e);
        addOnTabSelectedListener(this.e);
    }
}
