package com.supercell.id.view;

import com.supercell.id.view.RootFrameLayout;

public final class z implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ RootFrameLayout f4961a;

    public z(RootFrameLayout rootFrameLayout) {
        this.f4961a = rootFrameLayout;
    }

    public final void run() {
        for (RootFrameLayout.a a2 : this.f4961a.f4919b) {
            a2.a(this.f4961a.getSystemWindowInsets());
        }
    }
}
