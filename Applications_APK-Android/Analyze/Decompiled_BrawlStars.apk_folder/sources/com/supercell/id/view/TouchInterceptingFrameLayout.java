package com.supercell.id.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import kotlin.d.a.c;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class TouchInterceptingFrameLayout extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    public c<? super TouchInterceptingFrameLayout, ? super MotionEvent, Boolean> f4923a;

    public TouchInterceptingFrameLayout(Context context) {
        this(context, null, 0, 0, 14, null);
    }

    public TouchInterceptingFrameLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 12, null);
    }

    public TouchInterceptingFrameLayout(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0, 8, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TouchInterceptingFrameLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i);
        j.b(context, "context");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ TouchInterceptingFrameLayout(Context context, AttributeSet attributeSet, int i, int i2, int i3, g gVar) {
        this(context, (i3 & 2) != 0 ? null : attributeSet, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 0 : i2);
    }

    public final c<TouchInterceptingFrameLayout, MotionEvent, Boolean> getTouchInterceptor() {
        return this.f4923a;
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        c<? super TouchInterceptingFrameLayout, ? super MotionEvent, Boolean> cVar = this.f4923a;
        if (cVar == null || !cVar.invoke(this, motionEvent).booleanValue()) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return true;
    }

    public final void setTouchInterceptor(c<? super TouchInterceptingFrameLayout, ? super MotionEvent, Boolean> cVar) {
        this.f4923a = cVar;
    }
}
