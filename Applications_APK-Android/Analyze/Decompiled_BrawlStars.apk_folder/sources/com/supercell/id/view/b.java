package com.supercell.id.view;

import android.animation.ValueAnimator;

public final class b implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ AvatarView f4934a;

    public b(AvatarView avatarView) {
        this.f4934a = avatarView;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f4934a.invalidate();
    }
}
