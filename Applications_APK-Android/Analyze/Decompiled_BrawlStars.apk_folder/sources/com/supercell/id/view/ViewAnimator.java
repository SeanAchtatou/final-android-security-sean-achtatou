package com.supercell.id.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import b.b.a.k.h;
import b.b.a.k.i;
import com.facebook.internal.NativeProtocol;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.e.a;

public class ViewAnimator extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    public int f4924a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f4925b;
    public ValueAnimator c;
    public int d;
    public boolean e;

    public ViewAnimator(Context context) {
        this(context, null, 2, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ViewAnimator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        j.b(context, "context");
        this.f4925b = true;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ViewAnimator(Context context, AttributeSet attributeSet, int i, g gVar) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    private final int getDisplayedChild() {
        return this.f4924a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.ViewPropertyAnimator, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    private final void setDisplayedChild(int i) {
        this.f4924a = i;
        boolean z = true;
        if (i >= getChildCount()) {
            this.f4924a = 0;
        } else if (i < 0) {
            this.f4924a = getChildCount() - 1;
        }
        int i2 = this.f4924a;
        if (this.f4925b && !this.e) {
            z = false;
        }
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if (i3 == i2) {
                if (z) {
                    childAt.animate().setDuration(100).alpha(1.0f).setListener(null).setStartDelay(100).start();
                } else {
                    childAt.animate().cancel();
                    j.a((Object) childAt, "child");
                    childAt.setAlpha(1.0f);
                }
                j.a((Object) childAt, "child");
                childAt.setVisibility(0);
                this.f4925b = false;
            } else {
                if (z) {
                    j.a((Object) childAt, "child");
                    if (childAt.getVisibility() == 0) {
                        ViewPropertyAnimator alpha = childAt.animate().setDuration(100).alpha(0.0f);
                        j.a((Object) alpha, "child.animate()\n        …               .alpha(0f)");
                        ViewPropertyAnimator listener = alpha.setListener(new i(childAt));
                        j.a((Object) listener, "setListener(object : Ani…d = true\n        }\n    })");
                        listener.start();
                    }
                }
                childAt.animate().cancel();
                j.a((Object) childAt, "child");
                childAt.setAlpha(0.0f);
                childAt.setVisibility(8);
            }
        }
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        this.c = null;
        if (z) {
            this.d = getHeight();
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            ofFloat.setDuration(200L);
            ofFloat.addUpdateListener(new b.b.a.k.g(this));
            ofFloat.addListener(new h(this));
            ofFloat.start();
            this.c = ofFloat;
        }
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        j.b(view, "child");
        j.b(layoutParams, NativeProtocol.WEB_DIALOG_PARAMS);
        super.addView(view, i, layoutParams);
        view.setVisibility(getChildCount() == 1 ? 0 : 8);
        int i2 = this.f4924a;
        if (i >= 0 && i2 >= i) {
            setDisplayedChild(i2 + 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public CharSequence getAccessibilityClassName() {
        String name = android.widget.ViewAnimator.class.getName();
        j.a((Object) name, "ViewAnimator::class.java.name");
        return name;
    }

    public int getBaseline() {
        View currentView = getCurrentView();
        return currentView != null ? currentView.getBaseline() : super.getBaseline();
    }

    public final View getCurrentView() {
        return getChildAt(this.f4924a);
    }

    public void onConfigurationChanged(Configuration configuration) {
        j.b(configuration, "newConfig");
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        this.c = null;
        super.onConfigurationChanged(configuration);
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int measuredWidth = getMeasuredWidth();
        View currentView = getCurrentView();
        int measuredHeight = currentView != null ? currentView.getMeasuredHeight() : 0;
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            float animatedFraction = valueAnimator.getAnimatedFraction();
            int i3 = this.d;
            measuredHeight = a.a((((float) (measuredHeight - i3)) * animatedFraction) + ((float) i3));
        }
        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    public void removeAllViews() {
        super.removeAllViews();
        this.f4924a = 0;
        this.f4925b = true;
    }

    public void removeView(View view) {
        j.b(view, "view");
        int indexOfChild = indexOfChild(view);
        if (indexOfChild >= 0) {
            removeViewAt(indexOfChild);
        }
    }

    public void removeViewAt(int i) {
        super.removeViewAt(i);
        int childCount = getChildCount();
        if (childCount == 0) {
            this.f4924a = 0;
            this.f4925b = true;
            return;
        }
        int i2 = this.f4924a;
        if (i2 >= childCount) {
            setDisplayedChild(childCount - 1);
        } else if (i2 == i) {
            setDisplayedChild(i2);
        }
    }

    public void removeViewInLayout(View view) {
        j.b(view, "view");
        removeView(view);
    }

    public void removeViews(int i, int i2) {
        super.removeViews(i, i2);
        if (getChildCount() == 0) {
            this.f4924a = 0;
            this.f4925b = true;
            return;
        }
        int i3 = this.f4924a;
        if (i3 >= i && i3 < i + i2) {
            setDisplayedChild(i3);
        }
    }

    public void removeViewsInLayout(int i, int i2) {
        removeViews(i, i2);
    }

    public final void setCurrentView(View view) {
        Integer valueOf = Integer.valueOf(indexOfChild(view));
        if (!(valueOf.intValue() > -1)) {
            valueOf = null;
        }
        if (valueOf != null) {
            setDisplayedChild(valueOf.intValue());
        }
    }
}
