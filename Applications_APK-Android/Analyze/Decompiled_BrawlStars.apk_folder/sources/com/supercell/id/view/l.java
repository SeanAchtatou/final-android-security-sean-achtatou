package com.supercell.id.view;

import android.support.v7.widget.RecyclerView;
import kotlin.d.b.j;

public final class l extends RecyclerView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ FastScroll f4944a;

    public l(FastScroll fastScroll) {
        this.f4944a = fastScroll;
    }

    public final void onScrolled(RecyclerView recyclerView, int i, int i2) {
        float f;
        j.b(recyclerView, "recyclerView");
        int i3 = 0;
        int childAdapterPosition = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(0));
        int childCount = recyclerView.getChildCount() + childAdapterPosition;
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null) {
            i3 = adapter.getItemCount();
        }
        if (childAdapterPosition == 0) {
            f = 0.0f;
        } else if (childCount == i3) {
            f = (float) i3;
        } else {
            float f2 = (float) childAdapterPosition;
            f = f2 / ((((float) i3) + f2) - ((float) childCount));
        }
        FastScroll fastScroll = this.f4944a;
        fastScroll.setScrollPosition(((float) fastScroll.getHeight()) * f);
    }
}
