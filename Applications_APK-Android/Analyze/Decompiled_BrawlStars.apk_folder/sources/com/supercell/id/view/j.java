package com.supercell.id.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.widget.FrameLayout;
import com.supercell.id.R;

public final class j extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ FastScroll f4942a;

    public j(FastScroll fastScroll) {
        this.f4942a = fastScroll;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationCancel(Animator animator) {
        super.onAnimationCancel(animator);
        FrameLayout frameLayout = (FrameLayout) this.f4942a.a(R.id.fastscroll_bubble);
        kotlin.d.b.j.a((Object) frameLayout, "fastscroll_bubble");
        frameLayout.setVisibility(4);
        this.f4942a.f = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        FrameLayout frameLayout = (FrameLayout) this.f4942a.a(R.id.fastscroll_bubble);
        kotlin.d.b.j.a((Object) frameLayout, "fastscroll_bubble");
        frameLayout.setVisibility(4);
        this.f4942a.f = null;
    }
}
