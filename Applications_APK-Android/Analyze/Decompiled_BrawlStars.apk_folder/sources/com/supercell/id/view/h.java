package com.supercell.id.view;

import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import b.b.a.k.a;
import com.supercell.id.R;

public final class h extends RecyclerView.AdapterDataObserver {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ FastScroll f4940a;

    public h(FastScroll fastScroll) {
        this.f4940a = fastScroll;
    }

    public final void onChanged() {
        FastScroll fastScroll = this.f4940a;
        RecyclerView recyclerView = fastScroll.f4907a;
        if (recyclerView != null) {
            ((ImageView) fastScroll.a(R.id.fastscroll_thumb)).post(new a(recyclerView, fastScroll));
        }
    }
}
