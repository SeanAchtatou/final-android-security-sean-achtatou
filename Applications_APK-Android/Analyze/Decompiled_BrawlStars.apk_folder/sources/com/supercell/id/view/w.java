package com.supercell.id.view;

import android.view.View;
import android.view.ViewGroup;
import kotlin.d.b.j;

public final class w implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f4956a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ int f4957b;

    public w(View view, int i) {
        this.f4956a = view;
        this.f4957b = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void run() {
        View view = this.f4956a;
        j.a((Object) view, "it");
        View view2 = this.f4956a;
        j.a((Object) view2, "it");
        ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
        int i = this.f4957b;
        layoutParams.height = i;
        layoutParams.width = (int) (((float) i) * 0.3090909f);
        view.setLayoutParams(layoutParams);
    }
}
