package com.supercell.id.view;

import android.content.Context;
import android.support.v7.appcompat.R;
import android.support.v7.widget.AppCompatButton;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import b.b.a.b;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class WidthAdjustingMultilineButton extends AppCompatButton {

    /* renamed from: a  reason: collision with root package name */
    public Float f4926a;

    public WidthAdjustingMultilineButton(Context context) {
        this(context, null, 0, 6, null);
    }

    public WidthAdjustingMultilineButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WidthAdjustingMultilineButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        j.b(context, "context");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WidthAdjustingMultilineButton(Context context, AttributeSet attributeSet, int i, int i2, g gVar) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? R.attr.buttonStyle : i);
    }

    public final void onMeasure(int i, int i2) {
        Layout layout;
        super.onMeasure(i, i2);
        int mode = View.MeasureSpec.getMode(i);
        if (mode != 0 && getLineCount() > 1) {
            float textSize = getTextSize();
            float a2 = b.a(10);
            float a3 = b.a(1);
            while (textSize > a2 && getLineCount() > 1 && (r5 = getLayout()) != null && b.a(r5)) {
                textSize -= a3;
                setTextSize(0, textSize);
                super.onMeasure(i, i2);
            }
        }
        if (mode == Integer.MIN_VALUE && getLineCount() > 1 && (layout = getLayout()) != null) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(getCompoundPaddingRight() + getCompoundPaddingLeft() + ((int) ((float) Math.ceil((double) b.b(layout)))), Integer.MIN_VALUE), i2);
        }
    }

    public final void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        super.setText(charSequence, bufferType);
        Float f = this.f4926a;
        if (f != null) {
            setTextSize(0, f.floatValue());
        }
    }

    public final void setTextSize(int i, float f) {
        super.setTextSize(i, f);
        if (this.f4926a == null) {
            this.f4926a = Float.valueOf(getTextSize());
        }
    }
}
