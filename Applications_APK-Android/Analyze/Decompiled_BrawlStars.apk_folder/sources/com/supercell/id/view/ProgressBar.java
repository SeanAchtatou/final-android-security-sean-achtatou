package com.supercell.id.view;

import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.View;
import b.b.a.k.e;
import com.supercell.id.SupercellId;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.a.ab;
import kotlin.a.ah;
import kotlin.a.m;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.g.c;
import kotlin.g.d;
import kotlin.h;

public final class ProgressBar extends LinearLayoutCompat {

    /* renamed from: a  reason: collision with root package name */
    public final Map<e, h<String, String>> f4916a;

    /* renamed from: b  reason: collision with root package name */
    public final Map<String, Drawable> f4917b;
    public boolean c;
    public AnimatorSet d;

    public ProgressBar(Context context) {
        this(context, null, 0, 6, null);
    }

    public ProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ProgressBar(Context context, AttributeSet attributeSet, int i, int i2, g gVar) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    private void a() {
        c b2 = d.b(0, getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            int a2 = ((ah) it).a();
            View childAt = getChildAt(a2);
            AnimatorSet animatorSet = null;
            if (!(childAt instanceof e)) {
                childAt = null;
            }
            e eVar = (e) childAt;
            if (eVar != null) {
                animatorSet = eVar.a();
                double childCount = (double) getChildCount();
                Double.isNaN(childCount);
                double d2 = (double) a2;
                Double.isNaN(d2);
                animatorSet.setStartDelay((long) ((240.0d / childCount) * d2));
            }
            if (animatorSet != null) {
                arrayList.add(animatorSet);
            }
        }
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.setStartDelay(36);
        animatorSet2.playTogether(arrayList);
        animatorSet2.addListener(new x(animatorSet2, this));
        animatorSet2.start();
        this.d = animatorSet2;
    }

    public final void onAttachedToWindow() {
        this.c = true;
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            WeakReference weakReference = new WeakReference(this);
            for (String str : getMissingDrawables()) {
                SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(str, new b.b.a.k.d(str, weakReference));
            }
        }
    }

    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.c = false;
        AnimatorSet animatorSet = this.d;
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        this.d = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onMeasure(int i, int i2) {
        int i3;
        super.onMeasure(i, i2);
        int measuredHeight = getMeasuredHeight();
        if (measuredHeight != 0) {
            Drawable dividerDrawable = getDividerDrawable();
            if (!(dividerDrawable instanceof ShapeDrawable)) {
                dividerDrawable = null;
            }
            ShapeDrawable shapeDrawable = (ShapeDrawable) dividerDrawable;
            if (!(shapeDrawable == null || shapeDrawable.getIntrinsicWidth() == (i3 = (int) ((((float) measuredHeight) * 5.0f) / 110.0f)))) {
                shapeDrawable.setIntrinsicWidth(i3);
                setDividerDrawable(null);
                setDividerDrawable(shapeDrawable);
            }
            c b2 = d.b(0, getChildCount());
            ArrayList<View> arrayList = new ArrayList<>(m.a(b2, 10));
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                arrayList.add(getChildAt(((ah) it).a()));
            }
            for (View view : arrayList) {
                j.a((Object) view, "it");
                if (view.getLayoutParams().height != measuredHeight) {
                    view.post(new w(view, measuredHeight));
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.ArrayList, java.util.List]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean */
    private final List<String> getMissingDrawables() {
        List list;
        c b2 = d.b(0, getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            View childAt = getChildAt(((ah) it).a());
            String str = null;
            if (!(childAt instanceof e)) {
                childAt = null;
            }
            e eVar = (e) childAt;
            if (eVar != null) {
                String[] strArr = new String[2];
                h hVar = this.f4916a.get(eVar);
                strArr[0] = hVar != null ? (String) hVar.f5262a : null;
                h hVar2 = this.f4916a.get(eVar);
                if (hVar2 != null) {
                    str = (String) hVar2.f5263b;
                }
                strArr[1] = str;
                list = m.d(strArr);
                if (list != null) {
                    m.a((Collection) arrayList, (Iterable) list);
                }
            }
            list = ab.f5210a;
            m.a((Collection) arrayList, (Iterable) list);
        }
        ArrayList arrayList2 = new ArrayList();
        for (Object next : arrayList) {
            if (!this.f4917b.containsKey((String) next)) {
                arrayList2.add(next);
            }
        }
        return arrayList2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.ArrayList, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ProgressBar(android.content.Context r9, android.util.AttributeSet r10, int r11) {
        /*
            r8 = this;
            java.lang.String r0 = "context"
            kotlin.d.b.j.b(r9, r0)
            r8.<init>(r9, r10, r11)
            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap
            r0.<init>()
            r8.f4917b = r0
            r0 = 0
            r8.setOrientation(r0)
            r1 = 1
            r8.setHorizontalGravity(r1)
            android.graphics.drawable.ShapeDrawable r2 = new android.graphics.drawable.ShapeDrawable
            r2.<init>()
            android.content.res.Resources r3 = r8.getResources()
            java.lang.String r4 = "resources"
            kotlin.d.b.j.a(r3, r4)
            android.util.DisplayMetrics r3 = r3.getDisplayMetrics()
            r5 = 1084227584(0x40a00000, float:5.0)
            float r3 = android.util.TypedValue.applyDimension(r1, r5, r3)
            int r3 = (int) r3
            r2.setIntrinsicWidth(r3)
            r2.setAlpha(r0)
            r8.setDividerDrawable(r2)
            r2 = 7
            r8.setShowDividers(r2)
            r8.setClipChildren(r0)
            android.content.res.Resources r2 = r8.getResources()
            kotlin.d.b.j.a(r2, r4)
            android.util.DisplayMetrics r2 = r2.getDisplayMetrics()
            r3 = 1107820544(0x42080000, float:34.0)
            float r2 = android.util.TypedValue.applyDimension(r1, r3, r2)
            int r2 = (int) r2
            android.content.res.Resources r3 = r8.getResources()
            kotlin.d.b.j.a(r3, r4)
            android.util.DisplayMetrics r3 = r3.getDisplayMetrics()
            r4 = 1121714176(0x42dc0000, float:110.0)
            float r3 = android.util.TypedValue.applyDimension(r1, r4, r3)
            int r3 = (int) r3
            r4 = 0
        L_0x0065:
            r5 = 3
            r6 = 16
            if (r4 >= r5) goto L_0x007c
            b.b.a.k.e r5 = new b.b.a.k.e
            r5.<init>(r9, r10, r11)
            android.support.v7.widget.LinearLayoutCompat$LayoutParams r7 = new android.support.v7.widget.LinearLayoutCompat$LayoutParams
            r7.<init>(r2, r3)
            r7.gravity = r6
            r8.addView(r5, r7)
            int r4 = r4 + 1
            goto L_0x0065
        L_0x007c:
            int r9 = r8.getChildCount()
            kotlin.g.c r9 = kotlin.g.d.b(r0, r9)
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            java.util.Iterator r9 = r9.iterator()
        L_0x008d:
            boolean r11 = r9.hasNext()
            if (r11 == 0) goto L_0x00d1
            r11 = r9
            kotlin.a.ah r11 = (kotlin.a.ah) r11
            int r11 = r11.a()
            android.view.View r0 = r8.getChildAt(r11)
            boolean r2 = r0 instanceof b.b.a.k.e
            r3 = 0
            if (r2 != 0) goto L_0x00a4
            r0 = r3
        L_0x00a4:
            b.b.a.k.e r0 = (b.b.a.k.e) r0
            if (r0 == 0) goto L_0x00ca
            if (r11 == 0) goto L_0x00c2
            if (r11 == r1) goto L_0x00ba
            r2 = 2
            if (r11 == r2) goto L_0x00b0
            goto L_0x00ca
        L_0x00b0:
            kotlin.j r3 = new kotlin.j
            java.lang.String r11 = "loader_icon_3.png"
            java.lang.String r2 = "loader_icon_3_alt.png"
            r3.<init>(r0, r11, r2)
            goto L_0x00ca
        L_0x00ba:
            kotlin.j r11 = new kotlin.j
            java.lang.String r2 = "loader_icon_2.png"
            r11.<init>(r0, r2, r3)
            goto L_0x00cb
        L_0x00c2:
            kotlin.j r11 = new kotlin.j
            java.lang.String r2 = "loader_icon_1.png"
            r11.<init>(r0, r2, r3)
            goto L_0x00cb
        L_0x00ca:
            r11 = r3
        L_0x00cb:
            if (r11 == 0) goto L_0x008d
            r10.add(r11)
            goto L_0x008d
        L_0x00d1:
            r9 = 10
            int r9 = kotlin.a.m.a(r10, r9)
            int r9 = kotlin.a.ai.a(r9)
            int r9 = kotlin.g.d.c(r9, r6)
            java.util.LinkedHashMap r11 = new java.util.LinkedHashMap
            r11.<init>(r9)
            java.util.Iterator r9 = r10.iterator()
        L_0x00e8:
            boolean r10 = r9.hasNext()
            if (r10 == 0) goto L_0x010b
            java.lang.Object r10 = r9.next()
            kotlin.j r10 = (kotlin.j) r10
            A r0 = r10.f5299a
            kotlin.h r1 = new kotlin.h
            B r2 = r10.f5300b
            C r10 = r10.c
            r1.<init>(r2, r10)
            kotlin.h r10 = kotlin.k.a(r0, r1)
            A r0 = r10.f5262a
            B r10 = r10.f5263b
            r11.put(r0, r10)
            goto L_0x00e8
        L_0x010b:
            r8.f4916a = r11
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.id.view.ProgressBar.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public static final /* synthetic */ void a(ProgressBar progressBar, Drawable drawable, String str) {
        boolean isEmpty;
        synchronized (drawable) {
            progressBar.f4917b.put(str, drawable);
            isEmpty = progressBar.getMissingDrawables().isEmpty();
        }
        if (isEmpty && progressBar.c) {
            c b2 = d.b(0, progressBar.getChildCount());
            ArrayList<e> arrayList = new ArrayList<>();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                View childAt = progressBar.getChildAt(((ah) it).a());
                if (!(childAt instanceof e)) {
                    childAt = null;
                }
                e eVar = (e) childAt;
                if (eVar != null) {
                    arrayList.add(eVar);
                }
            }
            for (e eVar2 : arrayList) {
                h hVar = progressBar.f4916a.get(eVar2);
                if (hVar != null) {
                    eVar2.f1211b.setAlpha(1.0f);
                    eVar2.f1210a.setImageDrawable(progressBar.f4917b.get(hVar.f5262a));
                    String str2 = (String) hVar.f5263b;
                    if (str2 != null) {
                        eVar2.m = progressBar.f4917b.get(str2);
                    }
                }
            }
            progressBar.a();
        }
    }
}
