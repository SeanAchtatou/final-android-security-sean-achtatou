package com.supercell.id.view;

import android.animation.ValueAnimator;
import kotlin.TypeCastException;
import kotlin.d.b.j;

public final class g implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ExpandableFrameLayout f4939a;

    public g(ExpandableFrameLayout expandableFrameLayout) {
        this.f4939a = expandableFrameLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.animation.ValueAnimator, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        ExpandableFrameLayout expandableFrameLayout = this.f4939a;
        j.a((Object) valueAnimator, "valueAnimator");
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            expandableFrameLayout.setExpansion(((Float) animatedValue).floatValue());
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
    }
}
