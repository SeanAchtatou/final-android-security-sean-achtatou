package com.supercell.id.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import b.b.a.b;
import b.b.a.g.e;
import b.b.a.j.q1;
import com.supercell.id.R;
import java.lang.reflect.Field;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class Switch extends SwitchCompat {

    /* renamed from: a  reason: collision with root package name */
    public CompoundButton.OnCheckedChangeListener f4922a;

    public Switch(Context context) {
        this(context, null, 0, 6, null);
    }

    public Switch(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Switch(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        j.b(context, "context");
        int color = ContextCompat.getColor(context, R.color.gray95);
        int color2 = ContextCompat.getColor(context, R.color.accent_green);
        setLayerType(q1.c(this), null);
        ViewCompat.setBackground(this, null);
        setThumbDrawable(ContextCompat.getDrawable(context, R.drawable.switch_thumb));
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.setEnterFadeDuration(ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        stateListDrawable.setExitFadeDuration(ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        e eVar = e.f75b;
        Resources resources = getResources();
        j.a((Object) resources, "resources");
        stateListDrawable.addState(new int[]{16842912}, eVar.a(resources, color2, b.a(0), b.a(1), b.a(1), 0.1f, b.a(16)));
        e eVar2 = e.f75b;
        Resources resources2 = getResources();
        j.a((Object) resources2, "resources");
        float a2 = b.a(0);
        float a3 = b.a(1);
        stateListDrawable.addState(new int[0], eVar2.a(resources2, color, a2, a3, b.a(1), 0.1f, b.a(16)));
        setTrackDrawable(stateListDrawable);
        super.setOnCheckedChangeListener(new ae(this));
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Switch(Context context, AttributeSet attributeSet, int i, int i2, g gVar) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? android.support.v7.appcompat.R.attr.switchStyle : i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.reflect.Field, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        try {
            Field declaredField = SwitchCompat.class.getDeclaredField("mSwitchWidth");
            j.a((Object) declaredField, "switchWidth");
            declaredField.setAccessible(true);
            declaredField.setInt(this, getMeasuredWidth());
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }

    public final void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        this.f4922a = onCheckedChangeListener;
    }
}
