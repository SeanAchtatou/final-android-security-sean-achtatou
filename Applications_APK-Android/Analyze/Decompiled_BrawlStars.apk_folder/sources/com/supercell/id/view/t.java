package com.supercell.id.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;
import kotlin.TypeCastException;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.m;

public final class t implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ PinEntryView f4953a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Context f4954b;

    public t(PinEntryView pinEntryView, Context context) {
        this.f4953a = pinEntryView;
        this.f4954b = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.text.Editable, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void afterTextChanged(Editable editable) {
        PinEntryView pinEntryView = this.f4953a;
        pinEntryView.a(editable, pinEntryView.f.hasFocus());
        if ((editable != null ? editable.length() : 0) == this.f4953a.getDigits() && this.f4953a.f.hasFocus()) {
            Object systemService = this.f4954b.getSystemService("input_method");
            if (systemService != null) {
                ((InputMethodManager) systemService).hideSoftInputFromWindow(this.f4953a.f.getWindowToken(), 0);
                this.f4953a.f.clearFocus();
                this.f4953a.f.setFocusable(false);
                this.f4953a.f.post(new u(this));
            } else {
                throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
            }
        }
        PinEntryView pinEntryView2 = this.f4953a;
        c<? super PinEntryView, ? super CharSequence, m> cVar = pinEntryView2.i;
        if (cVar != null) {
            Editable text = pinEntryView2.f.getText();
            j.a((Object) text, "editText.text");
            cVar.invoke(pinEntryView2, text);
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
