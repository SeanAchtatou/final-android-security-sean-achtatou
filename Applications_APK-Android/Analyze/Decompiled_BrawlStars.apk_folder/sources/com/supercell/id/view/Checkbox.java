package com.supercell.id.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.ImageView;
import b.b.a.j.q1;
import b.b.a.k.c;
import com.supercell.id.R;
import kotlin.d.a.b;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.m;

public final class Checkbox extends AppCompatImageButton implements Checkable {

    /* renamed from: a  reason: collision with root package name */
    public static final int[] f4897a = {16842912};

    /* renamed from: b  reason: collision with root package name */
    public b<? super Boolean, m> f4898b;
    public boolean c;

    public Checkbox(Context context) {
        this(context, null, 0, 6, null);
    }

    public Checkbox(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Checkbox(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        j.b(context, "context");
        ViewCompat.setAccessibilityDelegate(this, new f(this));
        setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        setBackgroundResource(R.color.gray95);
        b(isChecked(), false, false);
        q1.a(this, 0.0f, 1.0f, 1.0f, 0.1f, 8.0f);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Checkbox(Context context, AttributeSet attributeSet, int i, int i2, g gVar) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? android.support.v7.appcompat.R.attr.imageButtonStyle : i);
    }

    private void b(boolean z, boolean z2, boolean z3) {
        this.c = z;
        if (z2) {
            AnimatedVectorDrawableCompat create = AnimatedVectorDrawableCompat.create(getContext(), z ? R.drawable.checkmark_check : R.drawable.checkmark_uncheck);
            setImageDrawable(create);
            if (create != null) {
                create.start();
            }
        } else if (isChecked()) {
            setImageResource(R.drawable.checkmark);
        } else {
            setImageDrawable(null);
        }
        refreshDrawableState();
        if (z3) {
            b<? super Boolean, m> bVar = this.f4898b;
            if (bVar != null) {
                bVar.invoke(Boolean.valueOf(z));
            }
            sendAccessibilityEvent(2048);
        }
    }

    public final void a(boolean z, boolean z2, boolean z3) {
        if (this.c != z) {
            b(z, z2, z3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.drawable.Drawable, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Drawable getBackground() {
        Drawable drawable;
        Drawable background = super.getBackground();
        c cVar = (c) (!(background instanceof c) ? null : background);
        if (cVar != null && (drawable = cVar.f1207a) != null) {
            return drawable;
        }
        j.a((Object) background, "background");
        return background;
    }

    public final boolean isChecked() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [int[], java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final int[] onCreateDrawableState(int i) {
        if (this.c) {
            int[] mergeDrawableStates = View.mergeDrawableStates(super.onCreateDrawableState(i + f4897a.length), f4897a);
            j.a((Object) mergeDrawableStates, "View.mergeDrawableStates…, DRAWABLE_STATE_CHECKED)");
            return mergeDrawableStates;
        }
        int[] onCreateDrawableState = super.onCreateDrawableState(i);
        j.a((Object) onCreateDrawableState, "super.onCreateDrawableState(extraSpace)");
        return onCreateDrawableState;
    }

    public final void onRestoreInstanceState(Parcelable parcelable) {
        j.b(parcelable, "parcelable");
        Bundle bundle = (Bundle) parcelable;
        b(bundle.getBoolean("checked"), false, false);
        super.onRestoreInstanceState(bundle.getParcelable("super_state"));
    }

    public final Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putBoolean("checked", this.c);
        bundle.putParcelable("super_state", onSaveInstanceState);
        return bundle;
    }

    public final boolean performClick() {
        toggle();
        return super.performClick();
    }

    public final void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(new c(drawable, getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom()));
    }

    public final void setChecked(boolean z) {
        a(z, true, true);
    }

    public final void setOnCheckedChangeListener(b<? super Boolean, m> bVar) {
        this.f4898b = bVar;
    }

    public final void toggle() {
        setChecked(!this.c);
    }
}
