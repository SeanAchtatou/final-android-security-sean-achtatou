package com.supercell.id.view;

import android.content.Context;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.AppCompatTextView;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import b.b.a.b;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class WidthAdjustingMultilineTextView extends AppCompatTextView {

    /* renamed from: a  reason: collision with root package name */
    public Float f4927a;

    public WidthAdjustingMultilineTextView(Context context) {
        this(context, null, 0, 6, null);
    }

    public WidthAdjustingMultilineTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WidthAdjustingMultilineTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        j.b(context, "context");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WidthAdjustingMultilineTextView(Context context, AttributeSet attributeSet, int i, int i2, g gVar) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 16842884 : i);
    }

    public final void onMeasure(int i, int i2) {
        Layout layout;
        super.onMeasure(i, i2);
        int mode = View.MeasureSpec.getMode(i);
        if (mode == 0 || getLineCount() <= 1) {
            Float f = this.f4927a;
            if (f != null) {
                float floatValue = f.floatValue();
                if (getTextSize() != floatValue) {
                    setTextSize(0, floatValue);
                    super.onMeasure(i, i2);
                }
            }
        } else {
            float textSize = getTextSize();
            float a2 = b.a(10);
            while (textSize > a2 && (getLineCount() > TextViewCompat.getMaxLines(this) || ((r5 = getLayout()) != null && b.a(r5)))) {
                textSize -= 1.0f;
                setTextSize(0, textSize);
                super.onMeasure(i, i2);
            }
        }
        if (mode == Integer.MIN_VALUE && getLineCount() > 1 && (layout = getLayout()) != null) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(getCompoundPaddingRight() + getCompoundPaddingLeft() + ((int) ((float) Math.ceil((double) b.b(layout)))), Integer.MIN_VALUE), i2);
        }
    }

    public final void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        if (this.f4927a == null) {
            this.f4927a = Float.valueOf(getTextSize());
        }
        Float f = this.f4927a;
        if (f != null) {
            float floatValue = f.floatValue();
            if (getTextSize() != floatValue) {
                setTextSize(0, floatValue);
            }
        }
        super.setText(charSequence, bufferType);
    }
}
