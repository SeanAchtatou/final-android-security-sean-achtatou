package com.supercell.id.view;

import android.view.MotionEvent;
import android.view.View;

public final class r implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ PinEntryView f4950a;

    public r(PinEntryView pinEntryView) {
        this.f4950a = pinEntryView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        this.f4950a.onTouchEvent(motionEvent);
        return true;
    }
}
