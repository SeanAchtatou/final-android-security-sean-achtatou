package com.supercell.id.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import b.b.a.k.a;
import com.supercell.id.R;

public final class k implements View.OnLayoutChangeListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ FastScroll f4943a;

    public k(FastScroll fastScroll) {
        this.f4943a = fastScroll;
    }

    public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        if (view != null) {
            view.removeOnLayoutChangeListener(this);
        }
        FastScroll fastScroll = this.f4943a;
        RecyclerView recyclerView = fastScroll.f4907a;
        if (recyclerView != null) {
            ((ImageView) fastScroll.a(R.id.fastscroll_thumb)).post(new a(recyclerView, fastScroll));
        }
    }
}
