package com.supercell.id.view;

import android.animation.Animator;
import android.support.design.widget.TabLayout;
import android.view.View;
import b.b.a.b;
import com.supercell.id.R;
import kotlin.d.b.j;

public final class q implements TabLayout.OnTabSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ FlatTabLayout f4949a;

    public q(FlatTabLayout flatTabLayout) {
        this.f4949a = flatTabLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onTabReselected(TabLayout.Tab tab) {
        View customView;
        if (tab != null && (customView = tab.getCustomView()) != null) {
            j.a((Object) customView, "view");
            EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) customView.findViewById(R.id.tab_icon);
            j.a((Object) edgeAntialiasingImageView, "view.tab_icon");
            j.b(edgeAntialiasingImageView, "icon");
            Animator a2 = b.a(edgeAntialiasingImageView, 6);
            a2.setStartDelay(20);
            a2.start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onTabSelected(TabLayout.Tab tab) {
        View customView;
        if (tab != null) {
            this.f4949a.a(tab, true);
        }
        if (tab != null && (customView = tab.getCustomView()) != null) {
            j.a((Object) customView, "view");
            EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) customView.findViewById(R.id.tab_icon);
            j.a((Object) edgeAntialiasingImageView, "view.tab_icon");
            j.b(edgeAntialiasingImageView, "icon");
            Animator a2 = b.a(edgeAntialiasingImageView, 6);
            a2.setStartDelay(20);
            a2.start();
        }
    }

    public final void onTabUnselected(TabLayout.Tab tab) {
        if (tab != null) {
            this.f4949a.a(tab, false);
        }
    }
}
