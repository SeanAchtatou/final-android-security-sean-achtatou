package com.supercell.id.view;

import android.os.Parcel;
import android.os.Parcelable;
import com.supercell.id.view.PinEntryView;
import kotlin.d.b.j;

public final class v implements Parcelable.Creator<PinEntryView.d> {
    public final Object createFromParcel(Parcel parcel) {
        j.b(parcel, "in");
        return new PinEntryView.d(parcel);
    }

    public final Object[] newArray(int i) {
        return new PinEntryView.d[i];
    }
}
