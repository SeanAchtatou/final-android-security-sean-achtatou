package com.supercell.id.view;

import android.animation.Animator;
import android.animation.AnimatorSet;

public final class x implements Animator.AnimatorListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ AnimatorSet f4958a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ ProgressBar f4959b;

    public x(AnimatorSet animatorSet, ProgressBar progressBar) {
        this.f4958a = animatorSet;
        this.f4959b = progressBar;
    }

    public final void onAnimationCancel(Animator animator) {
    }

    public final void onAnimationEnd(Animator animator) {
        if (this.f4959b.c) {
            this.f4958a.start();
        }
    }

    public final void onAnimationRepeat(Animator animator) {
    }

    public final void onAnimationStart(Animator animator) {
    }
}
