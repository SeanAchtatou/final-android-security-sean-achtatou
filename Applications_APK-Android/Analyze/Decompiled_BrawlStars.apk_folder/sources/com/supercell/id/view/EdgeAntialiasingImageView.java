package com.supercell.id.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import b.b.a.b;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class EdgeAntialiasingImageView extends AppCompatImageView {

    /* renamed from: a  reason: collision with root package name */
    public final Paint f4899a;

    /* renamed from: b  reason: collision with root package name */
    public final Matrix f4900b;
    public BitmapShader c;

    public EdgeAntialiasingImageView(Context context) {
        this(context, null, 0, 6, null);
    }

    public EdgeAntialiasingImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public EdgeAntialiasingImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        j.b(context, "context");
        this.f4899a = new Paint();
        this.f4900b = new Matrix();
        this.f4899a.setAntiAlias(true);
        this.f4899a.setFilterBitmap(true);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ EdgeAntialiasingImageView(Context context, AttributeSet attributeSet, int i, int i2, g gVar) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Bitmap, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onDraw(Canvas canvas) {
        BitmapShader bitmapShader;
        Drawable drawable = getDrawable();
        if (!(drawable instanceof BitmapDrawable)) {
            drawable = null;
        }
        BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
        if (bitmapDrawable != null && (bitmapShader = this.c) != null) {
            float width = ((float) getWidth()) - b.a(2);
            float height = ((float) getHeight()) - b.a(2);
            Bitmap bitmap = bitmapDrawable.getBitmap();
            this.f4900b.reset();
            j.a((Object) bitmap, "bitmap");
            if (bitmap.getWidth() > bitmap.getHeight()) {
                float height2 = height / ((float) bitmap.getHeight());
                this.f4900b.setScale(height2, height2);
                this.f4900b.postTranslate((width - (((float) bitmap.getWidth()) * height2)) * 0.5f, 0.0f);
            } else {
                float width2 = width / ((float) bitmap.getWidth());
                this.f4900b.setScale(width2, width2);
                this.f4900b.postTranslate(0.0f, (height - (((float) bitmap.getHeight()) * width2)) * 0.5f);
            }
            bitmapShader.setLocalMatrix(this.f4900b);
            this.f4899a.setShader(bitmapShader);
            if (canvas != null) {
                canvas.save();
            }
            if (canvas != null) {
                canvas.translate(b.a(1), b.a(1));
            }
            if (canvas != null) {
                canvas.drawRect(0.0f, 0.0f, width, height, this.f4899a);
            }
            if (canvas != null) {
                canvas.restore();
            }
        }
    }

    public final void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        BitmapShader bitmapShader = null;
        if (!(drawable instanceof BitmapDrawable)) {
            drawable = null;
        }
        BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
        if (bitmapDrawable != null) {
            Bitmap bitmap = bitmapDrawable.getBitmap();
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            bitmapShader = new BitmapShader(bitmap, tileMode, tileMode);
        }
        this.c = bitmapShader;
    }
}
