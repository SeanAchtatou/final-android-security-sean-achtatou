package com.supercell.id.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import com.supercell.id.R;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class FlowPhaseIndicator extends View {

    /* renamed from: a  reason: collision with root package name */
    public int f4911a;

    /* renamed from: b  reason: collision with root package name */
    public Drawable f4912b;
    public float c;
    public boolean d;

    public FlowPhaseIndicator(Context context) {
        this(context, null, 0, 0, 14, null);
    }

    public FlowPhaseIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 12, null);
    }

    public FlowPhaseIndicator(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0, 8, null);
    }

    /* JADX INFO: finally extract failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlowPhaseIndicator(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i);
        j.b(context, "context");
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.FlowPhaseIndicator, i, i2);
        try {
            setIndicator(obtainStyledAttributes.getDrawable(R.styleable.FlowPhaseIndicator_indicator));
            this.f4911a = obtainStyledAttributes.getInteger(R.styleable.FlowPhaseIndicator_android_orientation, 0);
            obtainStyledAttributes.recycle();
            this.d = ViewCompat.getLayoutDirection(this) != 1 ? false : true;
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FlowPhaseIndicator(Context context, AttributeSet attributeSet, int i, int i2, int i3, g gVar) {
        this(context, (i3 & 2) != 0 ? null : attributeSet, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 0 : i2);
    }

    private void a(int i, int i2) {
        int i3 = 0;
        if (this.f4911a == 1) {
            int paddingLeft = (i - getPaddingLeft()) - getPaddingRight();
            Drawable drawable = this.f4912b;
            if (drawable != null) {
                i3 = drawable.getIntrinsicWidth();
            }
            int i4 = (paddingLeft - i3) / 2;
            Drawable drawable2 = this.f4912b;
            if (drawable2 != null) {
                b(i2, drawable2, i4);
                return;
            }
            return;
        }
        int paddingTop = (i2 - getPaddingTop()) - getPaddingBottom();
        Drawable drawable3 = this.f4912b;
        if (drawable3 != null) {
            i3 = drawable3.getIntrinsicHeight();
        }
        int i5 = (paddingTop - i3) / 2;
        Drawable drawable4 = this.f4912b;
        if (drawable4 != null) {
            a(i, drawable4, i5);
        }
    }

    private void a(int i, Drawable drawable, int i2) {
        int i3;
        int paddingLeft = (i - getPaddingLeft()) - getPaddingRight();
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        int i4 = paddingLeft - intrinsicWidth;
        float f = this.c;
        if (Float.compare(f, 0.0f) < 0) {
            f = 0.0f;
        } else if (Float.compare(f, 1.0f) > 0) {
            f = 1.0f;
        }
        boolean z = true;
        if (ViewCompat.getLayoutDirection(this) != 1) {
            z = false;
        }
        if (z) {
            f = 1.0f - f;
        }
        int i5 = (int) ((f * ((float) i4)) + 0.5f);
        if (i2 == Integer.MIN_VALUE) {
            Rect bounds = drawable.getBounds();
            int i6 = bounds.top;
            i3 = bounds.bottom;
            i2 = i6;
        } else {
            i3 = intrinsicHeight + i2;
        }
        drawable.setBounds(i5, i2, intrinsicWidth + i5, i3);
    }

    private void b(int i, Drawable drawable, int i2) {
        int i3;
        int paddingTop = (i - getPaddingTop()) - getPaddingBottom();
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        int i4 = paddingTop - intrinsicHeight;
        float f = this.c;
        if (Float.compare(f, 0.0f) < 0) {
            f = 0.0f;
        } else if (Float.compare(f, 1.0f) > 0) {
            f = 1.0f;
        }
        int i5 = (int) ((f * ((float) i4)) + 0.5f);
        if (i2 == Integer.MIN_VALUE) {
            Rect bounds = drawable.getBounds();
            int i6 = bounds.left;
            i3 = bounds.right;
            i2 = i6;
        } else {
            i3 = intrinsicWidth + i2;
        }
        drawable.setBounds(i2, i5, i3, intrinsicHeight + i5);
    }

    private final void setIndicator(Drawable drawable) {
        boolean z;
        Drawable drawable2 = this.f4912b;
        if (drawable2 == null || drawable2 == drawable) {
            z = false;
        } else {
            drawable2.setCallback(null);
            z = true;
        }
        if (drawable != null) {
            drawable.setCallback(this);
            if (Build.VERSION.SDK_INT >= 23 && canResolveLayoutDirection()) {
                drawable.setLayoutDirection(getLayoutDirection());
            }
            Drawable drawable3 = this.f4912b;
            if (!(drawable3 == null || !z || (drawable.getIntrinsicWidth() == drawable3.getIntrinsicWidth() && drawable.getIntrinsicHeight() == drawable3.getIntrinsicHeight()))) {
                requestLayout();
            }
        }
        this.f4912b = drawable;
        invalidate();
        if (z) {
            a(getWidth(), getHeight());
            if (drawable != null && drawable.isStateful()) {
                drawable.setState(getDrawableState());
            }
        }
    }

    public final void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.f4912b;
        if (drawable != null && drawable.isStateful() && drawable.setState(getDrawableState())) {
            invalidateDrawable(drawable);
        }
    }

    public final float getProgress() {
        return this.c;
    }

    public final void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.f4912b;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    public final synchronized void onDraw(Canvas canvas) {
        j.b(canvas, "canvas");
        super.onDraw(canvas);
        Drawable drawable = this.f4912b;
        if (drawable != null) {
            int save = canvas.save();
            canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
            drawable.draw(canvas);
            canvas.restoreToCount(save);
        }
    }

    public final synchronized void onMeasure(int i, int i2) {
        Drawable drawable = this.f4912b;
        setMeasuredDimension(View.resolveSizeAndState(getPaddingRight() + getPaddingLeft() + 0, i, 0), View.resolveSizeAndState(getPaddingBottom() + getPaddingTop() + (drawable != null ? drawable.getIntrinsicHeight() : 0), i2, 0));
    }

    public final void onRtlPropertiesChanged(int i) {
        super.onRtlPropertiesChanged(i);
        boolean z = ViewCompat.getLayoutDirection(this) == 1;
        if (this.d != z) {
            this.d = z;
            Drawable drawable = this.f4912b;
            if (drawable != null) {
                if (this.f4911a == 1) {
                    b(getHeight(), drawable, Integer.MIN_VALUE);
                } else {
                    a(getWidth(), drawable, Integer.MIN_VALUE);
                }
                invalidate();
            }
        }
    }

    public final void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        a(i, i2);
    }

    public final void setProgress(float f) {
        this.c = f;
        Drawable drawable = this.f4912b;
        if (drawable != null) {
            float f2 = this.c;
            drawable.setAlpha((f2 < 0.0f || f2 > 1.0f) ? (f2 < -0.5f || f2 > 0.0f) ? 0 : (int) ((1.0f - (this.c * -2.0f)) * 255.0f) : 255);
            if (this.f4911a == 1) {
                b(getHeight(), drawable, Integer.MIN_VALUE);
            } else {
                a(getWidth(), drawable, Integer.MIN_VALUE);
            }
            invalidate();
        }
    }

    public final boolean verifyDrawable(Drawable drawable) {
        j.b(drawable, "who");
        return drawable == this.f4912b || super.verifyDrawable(drawable);
    }
}
