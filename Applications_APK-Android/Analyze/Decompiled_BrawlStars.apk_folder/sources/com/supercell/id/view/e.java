package com.supercell.id.view;

import android.animation.Animator;
import android.graphics.drawable.GradientDrawable;
import com.supercell.id.view.AvatarView;

public final class e implements Animator.AnimatorListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ AvatarView f4937a;

    public e(AvatarView avatarView) {
        this.f4937a = avatarView;
    }

    public final void onAnimationCancel(Animator animator) {
        this.f4937a.bgAnimationState = AvatarView.a.NONE;
        GradientDrawable access$getCurrentBackground$p = this.f4937a.currentBackground;
        if (access$getCurrentBackground$p != null) {
            access$getCurrentBackground$p.setShape(0);
        }
    }

    public final void onAnimationEnd(Animator animator) {
        this.f4937a.bgAnimationState = AvatarView.a.NONE;
        GradientDrawable access$getCurrentBackground$p = this.f4937a.currentBackground;
        if (access$getCurrentBackground$p != null) {
            access$getCurrentBackground$p.setShape(0);
        }
    }

    public final void onAnimationRepeat(Animator animator) {
    }

    public final void onAnimationStart(Animator animator) {
    }
}
