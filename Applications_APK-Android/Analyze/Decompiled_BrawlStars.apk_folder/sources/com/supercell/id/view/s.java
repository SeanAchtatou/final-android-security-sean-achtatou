package com.supercell.id.view;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import kotlin.TypeCastException;

public final class s implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ PinEntryView f4951a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Context f4952b;

    public s(PinEntryView pinEntryView, Context context) {
        this.f4951a = pinEntryView;
        this.f4952b = context;
    }

    public final void onFocusChange(View view, boolean z) {
        Object systemService = this.f4952b.getSystemService("input_method");
        if (systemService != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) systemService;
            if (z) {
                inputMethodManager.showSoftInput(this.f4951a.f, 0);
            } else {
                inputMethodManager.hideSoftInputFromWindow(this.f4951a.f.getWindowToken(), 0);
            }
            PinEntryView pinEntryView = this.f4951a;
            pinEntryView.a(pinEntryView.f.getText(), z);
            this.f4951a.f.setSelection(this.f4951a.f.getText().length());
            PinEntryView pinEntryView2 = this.f4951a;
            View.OnFocusChangeListener onFocusChangeListener = pinEntryView2.h;
            if (onFocusChangeListener != null) {
                onFocusChangeListener.onFocusChange(pinEntryView2, z);
                return;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }
}
