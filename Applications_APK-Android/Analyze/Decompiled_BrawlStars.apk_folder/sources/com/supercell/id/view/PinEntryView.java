package com.supercell.id.view;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import b.b.a.b;
import b.b.a.f.a;
import b.b.a.g.e;
import b.b.a.j.q1;
import com.supercell.id.R;
import io.github.inflationx.calligraphy3.CalligraphyUtils;
import io.github.inflationx.calligraphy3.HasTypeface;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.a.ah;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.j.o;
import kotlin.j.t;
import kotlin.m;

public final class PinEntryView extends ViewGroup implements HasTypeface {

    /* renamed from: a  reason: collision with root package name */
    public final int f4913a;

    /* renamed from: b  reason: collision with root package name */
    public final float f4914b;
    public final float c;
    public final float d;
    public final float e;
    public final EditText f;
    public final List<TextView> g;
    public View.OnFocusChangeListener h;
    public c<? super PinEntryView, ? super CharSequence, m> i;

    public static final class d extends View.BaseSavedState {
        public static final Parcelable.Creator<d> CREATOR = new v();

        /* renamed from: a  reason: collision with root package name */
        public String f4915a;

        public /* synthetic */ d(Parcel parcel) {
            super(parcel);
            this.f4915a = parcel.readString();
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(Parcelable parcelable) {
            super(parcelable);
            j.b(parcelable, "superState");
        }

        public final void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            if (parcel != null) {
                parcel.writeString(this.f4915a);
            }
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public PinEntryView(Context context) {
        this(context, null, 0);
        j.b(context, "context");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public PinEntryView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        j.b(context, "context");
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PinEntryView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Context context2 = context;
        j.b(context2, "context");
        int i3 = 1;
        if (isInEditMode()) {
            Resources resources = context.getResources();
            j.a((Object) resources, "context.resources");
            j.b(resources, "resources");
            b.f16a = TypedValue.applyDimension(1, 1.0f, resources.getDisplayMetrics());
        }
        setClipToPadding(false);
        setClipChildren(false);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.PinEntryView, i2, 0);
        try {
            this.f4913a = obtainStyledAttributes.getInteger(R.styleable.PinEntryView_digits, 6);
            this.f4914b = obtainStyledAttributes.getDimension(R.styleable.PinEntryView_digitSpacing, b.a(7));
            this.c = obtainStyledAttributes.getDimension(R.styleable.PinEntryView_digitCenterExtraSpacing, b.a(5));
            this.d = obtainStyledAttributes.getDimension(R.styleable.PinEntryView_digitWidth, b.a(50));
            this.e = obtainStyledAttributes.getDimension(R.styleable.PinEntryView_digitHeight, b.a(48));
            obtainStyledAttributes.recycle();
            int color = isInEditMode() ? -1 : ContextCompat.getColor(context2, R.color.white);
            int color2 = isInEditMode() ? -16776961 : ContextCompat.getColor(context2, R.color.text_blue);
            int color3 = isInEditMode() ? ViewCompat.MEASURED_STATE_MASK : ContextCompat.getColor(context2, R.color.black);
            int color4 = isInEditMode() ? -3355444 : ContextCompat.getColor(context2, R.color.gray95);
            ColorStateList colorStateList = new ColorStateList(new int[][]{new int[]{16842913}, new int[0]}, new int[]{color, color3});
            kotlin.g.c b2 = kotlin.g.d.b(0, this.f4913a);
            ArrayList<TextView> arrayList = new ArrayList<>(kotlin.a.m.a(b2, 10));
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                ((ah) it).a();
                TextView textView = new TextView(context2);
                textView.setTextColor(colorStateList);
                textView.setTextSize(i3, 24.0f);
                textView.setGravity(17);
                textView.setLayerType(q1.c(textView), null);
                StateListDrawable stateListDrawable = new StateListDrawable();
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setCornerRadius(b.a(8));
                gradientDrawable.setColor(color2);
                int[] iArr = new int[i3];
                iArr[0] = 16842913;
                stateListDrawable.addState(iArr, gradientDrawable);
                e eVar = e.f75b;
                Resources resources2 = textView.getResources();
                j.a((Object) resources2, "resources");
                float a2 = b.a(i3);
                float a3 = b.a(i3);
                StateListDrawable stateListDrawable2 = stateListDrawable;
                stateListDrawable2.addState(new int[0], eVar.a(resources2, color4, 0.0f, a2, a3, 0.1f, b.a(8)));
                TextView textView2 = textView;
                ViewCompat.setBackground(textView2, stateListDrawable2);
                arrayList.add(textView2);
                colorStateList = colorStateList;
                i3 = 1;
            }
            for (TextView addView : arrayList) {
                addView(addView);
            }
            this.g = arrayList;
            this.f = new EditText(context2);
            this.f.setBackgroundColor(ContextCompat.getColor(context2, 17170445));
            this.f.setTextColor(ContextCompat.getColor(context2, 17170445));
            this.f.setCursorVisible(false);
            this.f.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(this.f4913a)});
            if (Build.VERSION.SDK_INT >= 26) {
                this.f.setImportantForAutofill(2);
            }
            this.f.setInputType(18);
            this.f.setImeOptions(268435456);
            this.f.setOnTouchListener(new r(this));
            this.f.setOnFocusChangeListener(new s(this, context2));
            this.f.addTextChangedListener(new t(this, context2));
            this.f.setGravity(80);
            addView(this.f);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    private float a(float f2) {
        float f3 = f2 - this.c;
        int i2 = this.f4913a;
        return (f3 - (((float) (i2 - 1)) * this.f4914b)) / ((float) i2);
    }

    private float b(float f2) {
        int i2 = this.f4913a;
        return (((float) (i2 - 1)) * this.f4914b) + (((float) i2) * f2) + this.c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        if (r1 == r6) goto L_0x0043;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.CharSequence r9, boolean r10) {
        /*
            r8 = this;
            r0 = 0
            if (r9 == 0) goto L_0x0008
            int r1 = r9.length()
            goto L_0x0009
        L_0x0008:
            r1 = 0
        L_0x0009:
            java.util.List<android.widget.TextView> r2 = r8.g
            java.util.Iterator r2 = r2.iterator()
            r3 = 0
        L_0x0010:
            boolean r4 = r2.hasNext()
            if (r4 == 0) goto L_0x004b
            java.lang.Object r4 = r2.next()
            int r5 = r3 + 1
            if (r3 >= 0) goto L_0x0021
            kotlin.a.m.a()
        L_0x0021:
            android.widget.TextView r4 = (android.widget.TextView) r4
            if (r1 <= r3) goto L_0x0032
            if (r9 == 0) goto L_0x0032
            java.lang.CharSequence r6 = r9.subSequence(r3, r5)
            java.lang.String r6 = r6.toString()
            if (r6 == 0) goto L_0x0032
            goto L_0x0034
        L_0x0032:
            java.lang.String r6 = ""
        L_0x0034:
            r4.setText(r6)
            if (r10 == 0) goto L_0x0045
            if (r3 == r1) goto L_0x0043
            int r6 = r8.f4913a
            int r7 = r6 + -1
            if (r3 != r7) goto L_0x0045
            if (r1 != r6) goto L_0x0045
        L_0x0043:
            r3 = 1
            goto L_0x0046
        L_0x0045:
            r3 = 0
        L_0x0046:
            r4.setSelected(r3)
            r3 = r5
            goto L_0x0010
        L_0x004b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.id.view.PinEntryView.a(java.lang.CharSequence, boolean):void");
    }

    public final boolean a() {
        return this.f.getText().length() == this.f4913a;
    }

    public final void b() {
        ClipData primaryClip;
        ClipData.Item itemAt;
        CharSequence coerceToText;
        String a2;
        try {
            Object systemService = getContext().getSystemService("clipboard");
            if (!(systemService instanceof ClipboardManager)) {
                systemService = null;
            }
            ClipboardManager clipboardManager = (ClipboardManager) systemService;
            if (clipboardManager != null && (primaryClip = clipboardManager.getPrimaryClip()) != null && (itemAt = primaryClip.getItemAt(0)) != null && (coerceToText = itemAt.coerceToText(getContext())) != null && (a2 = new o("[ \\[\\]]").a(coerceToText, "")) != null && !a() && a2.length() == this.f4913a && t.a(a2) != null) {
                setPin(a2);
                this.f.clearFocus();
                setScaleX(1.4f);
                setScaleY(1.4f);
                animate().scaleX(1.0f).scaleY(1.0f).setInterpolator(a.c).setDuration(700).start();
            }
        } catch (Exception unused) {
        }
    }

    public final int getDigits() {
        return this.f4913a;
    }

    public final View.OnFocusChangeListener getOnFocusChangeListener() {
        return this.h;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.text.Editable, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final CharSequence getText() {
        Editable text = this.f.getText();
        j.a((Object) text, "editText.text");
        return text;
    }

    public final void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6 = i4 - i2;
        int i7 = i5 - i3;
        int a2 = kotlin.e.a.a(a((float) i6));
        int i8 = 0;
        for (T next : this.g) {
            int i9 = i8 + 1;
            if (i8 < 0) {
                kotlin.a.m.a();
            }
            TextView textView = (TextView) next;
            int a3 = kotlin.e.a.a(((((float) a2) + this.f4914b) * ((float) i8)) + (i8 >= this.f4913a / 2 ? this.c : 0.0f));
            textView.layout(a3, 0, a3 + a2, i7);
            i8 = i9;
        }
        this.f.layout(0, -kotlin.e.a.a(this.f4914b), i6, i7 + kotlin.e.a.a(this.f4914b));
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x008f A[LOOP:0: B:26:0x0089->B:28:0x008f, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r6, int r7) {
        /*
            r5 = this;
            int r0 = android.view.View.MeasureSpec.getMode(r6)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 1073741824(0x40000000, float:2.0)
            if (r0 == r1) goto L_0x001f
            if (r0 == 0) goto L_0x0014
            if (r0 == r2) goto L_0x000f
            goto L_0x0014
        L_0x000f:
            int r6 = android.view.View.MeasureSpec.getSize(r6)
            goto L_0x003d
        L_0x0014:
            float r6 = r5.d
        L_0x0016:
            float r6 = r5.b(r6)
            int r6 = kotlin.e.a.a(r6)
            goto L_0x003d
        L_0x001f:
            int r6 = android.view.View.MeasureSpec.getSize(r6)
            float r6 = (float) r6
            float r6 = r5.a(r6)
            float r0 = r5.d
            r3 = 1065353216(0x3f800000, float:1.0)
            int r4 = java.lang.Float.compare(r6, r3)
            if (r4 >= 0) goto L_0x0035
            r6 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0016
        L_0x0035:
            int r3 = java.lang.Float.compare(r6, r0)
            if (r3 <= 0) goto L_0x0016
            r6 = r0
            goto L_0x0016
        L_0x003d:
            int r0 = android.view.View.MeasureSpec.getMode(r7)
            r3 = 1
            if (r0 == r1) goto L_0x0055
            if (r0 == 0) goto L_0x004e
            if (r0 == r2) goto L_0x0049
            goto L_0x004e
        L_0x0049:
            int r3 = android.view.View.MeasureSpec.getSize(r7)
            goto L_0x006f
        L_0x004e:
            float r7 = r5.e
            int r3 = kotlin.e.a.a(r7)
            goto L_0x006f
        L_0x0055:
            int r7 = android.view.View.MeasureSpec.getSize(r7)
            float r0 = r5.e
            int r0 = kotlin.e.a.a(r0)
            int r1 = kotlin.d.b.j.a(r7, r3)
            if (r1 >= 0) goto L_0x0066
            goto L_0x006f
        L_0x0066:
            int r1 = kotlin.d.b.j.a(r7, r0)
            if (r1 <= 0) goto L_0x006e
            r3 = r0
            goto L_0x006f
        L_0x006e:
            r3 = r7
        L_0x006f:
            r5.setMeasuredDimension(r6, r3)
            float r7 = (float) r6
            float r7 = r5.a(r7)
            int r7 = kotlin.e.a.a(r7)
            int r7 = android.view.View.MeasureSpec.makeMeasureSpec(r7, r2)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r2)
            java.util.List<android.widget.TextView> r1 = r5.g
            java.util.Iterator r1 = r1.iterator()
        L_0x0089:
            boolean r4 = r1.hasNext()
            if (r4 == 0) goto L_0x0099
            java.lang.Object r4 = r1.next()
            android.widget.TextView r4 = (android.widget.TextView) r4
            r4.measure(r7, r0)
            goto L_0x0089
        L_0x0099:
            android.widget.EditText r7 = r5.f
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r6, r2)
            float r0 = r5.f4914b
            int r0 = kotlin.e.a.a(r0)
            int r0 = r0 * 2
            int r0 = r0 + r3
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r2)
            r7.measure(r6, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.id.view.PinEntryView.onMeasure(int, int):void");
    }

    public final void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof d)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        d dVar = (d) parcelable;
        super.onRestoreInstanceState(dVar.getSuperState());
        String str = dVar.f4915a;
        if (str != null) {
            setText(str);
        }
        EditText editText = this.f;
        String str2 = dVar.f4915a;
        editText.setSelection(str2 != null ? str2.length() : 0);
    }

    public final Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (onSaveInstanceState == null) {
            return null;
        }
        d dVar = new d(onSaveInstanceState);
        dVar.f4915a = this.f.getText().toString();
        return dVar;
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent == null || motionEvent.getAction() != 0) {
            return super.onTouchEvent(motionEvent);
        }
        if (this.f.hasFocus()) {
            Object systemService = getContext().getSystemService("input_method");
            if (systemService != null) {
                ((InputMethodManager) systemService).showSoftInput(this.f, 0);
                return true;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
        }
        this.f.requestFocus();
        return true;
    }

    public final void setOnFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.h = onFocusChangeListener;
    }

    public final void setOnPinChangedListener(c<? super PinEntryView, ? super CharSequence, m> cVar) {
        this.i = cVar;
    }

    public final void setPin(String str) {
        j.b(str, "pin");
        setText(str);
    }

    public final void setText(CharSequence charSequence) {
        j.b(charSequence, "value");
        int length = charSequence.length();
        int i2 = this.f4913a;
        if (length > i2) {
            charSequence = charSequence.subSequence(0, i2);
        }
        this.f.setText(charSequence);
    }

    public final void setTypeface(Typeface typeface) {
        for (TextView applyFontToTextView : this.g) {
            CalligraphyUtils.applyFontToTextView(applyFontToTextView, typeface);
        }
    }

    public final boolean shouldDelayChildPressedState() {
        return false;
    }
}
