package com.supercell.id;

import b.b.a.j.f0;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class n extends k implements b<f0, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ String f4878a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(String str) {
        super(1);
        this.f4878a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final Object invoke(Object obj) {
        SupercellIdDelegate access$getDelegate$p;
        j.b((f0) obj, "it");
        String str = this.f4878a;
        IdAccount idAccount = SupercellId.INSTANCE.getSharedServices$supercellId_release().i;
        if (j.a((Object) str, (Object) (idAccount != null ? idAccount.getScidToken() : null)) && (access$getDelegate$p = SupercellId.c) != null) {
            access$getDelegate$p.friendsFailed();
        }
        return m.f5330a;
    }
}
