package com.supercell.id;

import android.os.Parcel;
import android.os.Parcelable;
import b.b.a.j.a0;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.j.t;

public final class IdAccount implements a0 {
    public static final Parcelable.Creator<IdAccount> CREATOR = new a();
    public static final a Companion = new a();

    /* renamed from: a  reason: collision with root package name */
    public final boolean f4852a;

    /* renamed from: b  reason: collision with root package name */
    public final String f4853b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;

    public static final class a {
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public IdAccount(android.os.Parcel r9) {
        /*
            r8 = this;
            java.lang.String r0 = "parcel"
            kotlin.d.b.j.b(r9, r0)
            java.lang.String r2 = r9.readString()
            if (r2 != 0) goto L_0x000e
            kotlin.d.b.j.a()
        L_0x000e:
            java.lang.String r3 = r9.readString()
            java.lang.String r4 = r9.readString()
            java.lang.String r5 = r9.readString()
            java.lang.String r6 = r9.readString()
            if (r6 != 0) goto L_0x0023
            kotlin.d.b.j.a()
        L_0x0023:
            java.lang.String r7 = r9.readString()
            r1 = r8
            r1.<init>(r2, r3, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.id.IdAccount.<init>(android.os.Parcel):void");
    }

    public IdAccount(String str, String str2, String str3, String str4, String str5, String str6) {
        j.b(str, "supercellId");
        j.b(str5, "scidToken");
        this.f4853b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.f4852a = !t.a((CharSequence) this.f);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ IdAccount(String str, String str2, String str3, String str4, String str5, String str6, int i, g gVar) {
        this(str, str2, str3, str4, str5, (i & 32) != 0 ? null : str6);
    }

    public static /* synthetic */ IdAccount copy$default(IdAccount idAccount, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
        if ((i & 1) != 0) {
            str = idAccount.f4853b;
        }
        if ((i & 2) != 0) {
            str2 = idAccount.c;
        }
        String str7 = str2;
        if ((i & 4) != 0) {
            str3 = idAccount.d;
        }
        String str8 = str3;
        if ((i & 8) != 0) {
            str4 = idAccount.e;
        }
        String str9 = str4;
        if ((i & 16) != 0) {
            str5 = idAccount.f;
        }
        String str10 = str5;
        if ((i & 32) != 0) {
            str6 = idAccount.g;
        }
        return idAccount.copy(str, str7, str8, str9, str10, str6);
    }

    public final String component1() {
        return this.f4853b;
    }

    public final String component2() {
        return this.c;
    }

    public final String component3() {
        return this.d;
    }

    public final String component4() {
        return this.e;
    }

    public final String component5() {
        return this.f;
    }

    public final String component6() {
        return this.g;
    }

    public final IdAccount copy(String str, String str2, String str3, String str4, String str5, String str6) {
        j.b(str, "supercellId");
        j.b(str5, "scidToken");
        return new IdAccount(str, str2, str3, str4, str5, str6);
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IdAccount)) {
            return false;
        }
        IdAccount idAccount = (IdAccount) obj;
        return j.a(this.f4853b, idAccount.f4853b) && j.a(this.c, idAccount.c) && j.a(this.d, idAccount.d) && j.a(this.e, idAccount.e) && j.a(this.f, idAccount.f) && j.a(this.g, idAccount.g);
    }

    public final boolean getCanShowProfile$supercellId_release() {
        return this.f4852a;
    }

    public final String getEmail() {
        return this.c;
    }

    public final String getError() {
        return this.g;
    }

    public final String getPhone() {
        return this.d;
    }

    public final String getPlayerId() {
        return this.e;
    }

    public final String getScidToken() {
        return this.f;
    }

    public final String getSupercellId() {
        return this.f4853b;
    }

    public final int hashCode() {
        String str = this.f4853b;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.c;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.d;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.e;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.f;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.g;
        if (str6 != null) {
            i = str6.hashCode();
        }
        return hashCode5 + i;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdAccount(supercellId=");
        a2.append(this.f4853b);
        a2.append(", email=");
        a2.append(this.c);
        a2.append(", phone=");
        a2.append(this.d);
        a2.append(", playerId=");
        a2.append(this.e);
        a2.append(", scidToken=");
        a2.append(this.f);
        a2.append(", error=");
        return b.a.a.a.a.a(a2, this.g, ")");
    }

    public final void writeToParcel(Parcel parcel, int i) {
        j.b(parcel, "dest");
        parcel.writeString(this.f4853b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
    }
}
