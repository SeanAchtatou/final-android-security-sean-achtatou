package com.supercell.id;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.TypedValue;
import b.b.a.b;
import b.b.a.i.b;
import b.b.a.j.c;
import b.b.a.j.i;
import b.b.a.j.j;
import b.b.a.j.j0;
import b.b.a.j.k0;
import b.b.a.j.p;
import b.b.a.j.p1;
import b.b.a.j.q0;
import b.b.a.j.x;
import com.facebook.places.model.PlaceFields;
import com.supercell.id.IdConfiguration;
import com.supercell.id.ui.MainActivity;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;
import kotlin.d;
import kotlin.d.b.r;
import kotlin.d.b.t;
import kotlin.e;
import kotlin.h.h;
import kotlin.m;
import nl.komponents.kovenant.ap;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;
import org.json.JSONException;
import org.json.JSONObject;

public final class SupercellId {
    public static final SupercellId INSTANCE = new SupercellId();

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ h[] f4864a = {t.a(new r(t.a(SupercellId.class), "sharedServices", "getSharedServices$supercellId_release()Lcom/supercell/id/util/IdServices;"))};

    /* renamed from: b  reason: collision with root package name */
    public static WeakReference<Context> f4865b;
    public static SupercellIdDelegate c;
    public static final d d = e.a(p.f4880a);
    public static final WeakHashMap<a, Integer> e = new WeakHashMap<>();
    public static j f;
    public static String g;
    public static ap<String, String> h;
    public static int i;
    public static final String j;

    public interface a {
        void a(boolean z);
    }

    static {
        StringBuilder a2 = b.a.a.a.a.a("2991-A");
        a2.append(kotlin.d.b.j.a("release", "debug") ? " debug" : "");
        j = a2.toString();
    }

    public static /* synthetic */ m forgetAccount$supercellId_release$default(SupercellId supercellId, String str, String str2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            str2 = null;
        }
        return supercellId.forgetAccount$supercellId_release(str, str2);
    }

    public final void accountAlreadyBound() {
        ap<String, String> apVar = h;
        if (apVar != null) {
            apVar.f("accountAlreadyBound");
        }
        h = null;
    }

    public final void accountBindingFailed() {
        ap<String, String> apVar = h;
        if (apVar != null) {
            apVar.f("accountBindingFailed");
        }
        h = null;
    }

    public final void accountBound(String str) {
        kotlin.d.b.j.b(str, "token");
        ap<String, String> apVar = h;
        if (apVar != null) {
            apVar.e(str);
        }
        h = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final void addMaintenanceModeListener$supercellId_release(a aVar) {
        kotlin.d.b.j.b(aVar, "listener");
        synchronized (e) {
            boolean z = false;
            e.put(aVar, 0);
            if (INSTANCE.getRemoteConfiguration$supercellId_release().a(q0.MAINTENANCE) || kotlin.d.b.j.a((Object) g, (Object) "maintenance")) {
                z = true;
            }
            aVar.a(z);
            m mVar = m.f5330a;
        }
    }

    public final bw<String, String> bindAccount$supercellId_release(String str, String str2, String str3, String str4, boolean z) {
        kotlin.d.b.j.b(str, "token");
        kotlin.d.b.j.b(str2, "scidToken");
        ap<String, String> a2 = bb.f5389a;
        h = a2;
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate != null) {
            supercellIdDelegate.bindAccount(str, str2, str3, str4, z);
        }
        return a2.i();
    }

    public final void clearAssetsFromDisk() {
        AsyncTask.execute(g.f4871a);
    }

    public final void clearAssetsFromMemoryCache() {
        AsyncTask.execute(h.f4872a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void clearCaches() {
        Context context;
        WeakReference<Context> weakReference = f4865b;
        if (weakReference != null && (context = weakReference.get()) != null) {
            INSTANCE.getSharedServices$supercellId_release().a();
            i.b bVar = i.d;
            kotlin.d.b.j.a((Object) context, "it");
            bVar.a(context);
            j0.d.a(context);
        }
    }

    public final void clearImageAssetsFromMemoryCache() {
        AsyncTask.execute(i.f4873a);
    }

    public final m clearPendingLogin$supercellId_release() {
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate == null) {
            return null;
        }
        supercellIdDelegate.clearPendingLogin();
        return m.f5330a;
    }

    public final m clearPendingRegistration$supercellId_release() {
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate == null) {
            return null;
        }
        supercellIdDelegate.clearPendingRegistration();
        return m.f5330a;
    }

    public final void dismiss$supercellId_release(Activity activity) {
        String supercellId;
        kotlin.d.b.j.b(activity, "activity");
        activity.finish();
        if (isInitialized$supercellId_release()) {
            x sharedServices$supercellId_release = getSharedServices$supercellId_release();
            IdAccount idAccount = sharedServices$supercellId_release.i;
            if (!(idAccount == null || (supercellId = idAccount.getSupercellId()) == null)) {
                j0 j0Var = sharedServices$supercellId_release.f1189a;
                if (j0Var == null) {
                    kotlin.d.b.j.a("profile");
                }
                j0Var.b(sharedServices$supercellId_release.m, supercellId);
                i iVar = sharedServices$supercellId_release.c;
                if (iVar == null) {
                    kotlin.d.b.j.a("clientState");
                }
                iVar.b(sharedServices$supercellId_release.m, supercellId);
                "saved to persistent storage " + supercellId;
            }
            getSharedServices$supercellId_release().f.a();
            SupercellIdDelegate supercellIdDelegate = c;
            if (supercellIdDelegate != null) {
                supercellIdDelegate.windowDidDismiss();
            }
        }
    }

    public final m forgetAccount$supercellId_release(String str, String str2) {
        kotlin.d.b.j.b(str, "supercellId");
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate == null) {
            return null;
        }
        supercellIdDelegate.forgetAccount(str, str2);
        return m.f5330a;
    }

    public final IdAccount[] getAccounts() {
        IdAccount[] accounts;
        SupercellIdDelegate supercellIdDelegate = c;
        return (supercellIdDelegate == null || (accounts = supercellIdDelegate.getAccounts()) == null) ? new IdAccount[0] : accounts;
    }

    public final String getForcedView$supercellId_release() {
        return g;
    }

    public final IdIngameFriend[] getIngameFriends() {
        IdIngameFriend[] ingameFriends;
        SupercellIdDelegate supercellIdDelegate = c;
        return (supercellIdDelegate == null || (ingameFriends = supercellIdDelegate.getIngameFriends()) == null) ? new IdIngameFriend[0] : ingameFriends;
    }

    public final IdPendingLogin getPendingLogin$supercellId_release() {
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate != null) {
            return supercellIdDelegate.getPendingLogin();
        }
        return null;
    }

    public final IdPendingRegistration getPendingRegistration$supercellId_release() {
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate != null) {
            return supercellIdDelegate.getPendingRegistration();
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final j getRemoteConfiguration$supercellId_release() {
        j jVar;
        Context context;
        if (f == null) {
            WeakReference<Context> weakReference = f4865b;
            JSONObject jSONObject = null;
            if (weakReference == null || (context = weakReference.get()) == null) {
                jVar = new j(null);
            } else {
                kotlin.d.b.j.a((Object) context, "weakContext?.get() ?: return Configuration(null)");
                String string = context.getSharedPreferences("MyPreferences", 0).getString("storedConfiguration", null);
                if (string != null) {
                    try {
                        jSONObject = new JSONObject(string);
                    } catch (JSONException unused) {
                    }
                }
                jVar = new j(jSONObject);
            }
            f = jVar;
        }
        j jVar2 = f;
        if (jVar2 == null) {
            kotlin.d.b.j.a();
        }
        return jVar2;
    }

    public final x getSharedServices$supercellId_release() {
        return (x) d.a();
    }

    public final String getVersionString() {
        return j;
    }

    public final boolean isInitialized$supercellId_release() {
        return (f4865b == null || c == null) ? false : true;
    }

    public final boolean isSelfHelpPortalAvailable$supercellId_release() {
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate != null) {
            return supercellIdDelegate.isSelfHelpPortalAvailable();
        }
        return false;
    }

    public final boolean isTutorialComplete$supercellId_release() {
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate != null) {
            return supercellIdDelegate.isTutorialComplete();
        }
        return false;
    }

    public final m loadAccount$supercellId_release(String str, String str2, String str3, boolean z) {
        kotlin.d.b.j.b(str3, "scidToken");
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate == null) {
            return null;
        }
        supercellIdDelegate.loadAccount(str, str2, str3, z);
        return m.f5330a;
    }

    public final void logout$supercellId_release() {
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate != null) {
            supercellIdDelegate.logOut();
        }
    }

    public final void onWindowClientStart$supercellId_release() {
        if (i == 0) {
            reloadAssetsToMemoryCache();
        }
        i++;
    }

    public final void onWindowClientStop$supercellId_release() {
        i--;
        if (i == 0) {
            clearAssetsFromMemoryCache();
        }
    }

    public final void openSelfHelpPortal$supercellId_release() {
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate != null) {
            supercellIdDelegate.openSelfHelpPortal();
        }
    }

    public final void reloadAssetsToMemoryCache() {
        AsyncTask.execute(k.f4875a);
    }

    public final Integer removeMaintenanceModeListener$supercellId_release(a aVar) {
        Integer remove;
        kotlin.d.b.j.b(aVar, "listener");
        synchronized (e) {
            remove = e.remove(aVar);
        }
        return remove;
    }

    public final void requestImageDataForAvatarString(String str) {
        k0.f1078b.a(str, new o(str));
    }

    public final m setPendingLoginWithEmail$supercellId_release(String str, boolean z) {
        kotlin.d.b.j.b(str, NotificationCompat.CATEGORY_EMAIL);
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate == null) {
            return null;
        }
        supercellIdDelegate.setPendingLoginWithEmail(str, z);
        return m.f5330a;
    }

    public final m setPendingLoginWithPhone$supercellId_release(String str, boolean z) {
        kotlin.d.b.j.b(str, PlaceFields.PHONE);
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate == null) {
            return null;
        }
        supercellIdDelegate.setPendingLoginWithPhone(str, z);
        return m.f5330a;
    }

    public final m setPendingRegistrationWithEmail$supercellId_release(String str, boolean z) {
        kotlin.d.b.j.b(str, NotificationCompat.CATEGORY_EMAIL);
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate == null) {
            return null;
        }
        supercellIdDelegate.setPendingRegistrationWithEmail(str, z);
        return m.f5330a;
    }

    public final m setPendingRegistrationWithPhone$supercellId_release(String str) {
        kotlin.d.b.j.b(str, PlaceFields.PHONE);
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate == null) {
            return null;
        }
        supercellIdDelegate.setPendingRegistrationWithPhone(str);
        return m.f5330a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void setRemoteConfiguration$supercellId_release(j jVar) {
        Context context;
        kotlin.d.b.j.b(jVar, "value");
        f = jVar;
        synchronized (e) {
            Iterator<Map.Entry<a, Integer>> it = e.entrySet().iterator();
            while (true) {
                boolean z = false;
                if (!it.hasNext()) {
                    break;
                }
                a aVar = (a) it.next().getKey();
                if (jVar.a(q0.MAINTENANCE) || kotlin.d.b.j.a((Object) g, (Object) "maintenance")) {
                    z = true;
                }
                aVar.a(z);
            }
            m mVar = m.f5330a;
        }
        getSharedServices$supercellId_release().e().e = jVar.e(q0.FRIENDS_CACHE_LIFETIME);
        WeakReference<Context> weakReference = f4865b;
        if (weakReference != null && (context = weakReference.get()) != null) {
            kotlin.d.b.j.a((Object) context, "weakContext?.get() ?: return");
            SharedPreferences.Editor edit = context.getSharedPreferences("MyPreferences", 0).edit();
            j jVar2 = f;
            edit.putString("storedConfiguration", jVar2 != null ? String.valueOf(jVar2.f1058a) : null);
            edit.apply();
        }
    }

    public final void setTutorialComplete$supercellId_release() {
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate != null) {
            supercellIdDelegate.setTutorialComplete();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void setupWithDelegate(Context context, SupercellIdDelegate supercellIdDelegate) {
        kotlin.d.b.j.b(context, "context");
        kotlin.d.b.j.b(supercellIdDelegate, "delegate");
        f4865b = new WeakReference<>(context.getApplicationContext());
        c = supercellIdDelegate;
        ViewPump.init(ViewPump.builder().addInterceptor(new b.b.a.i.x1.m()).addInterceptor(c.f1019a).addInterceptor(new CalligraphyInterceptor(p.f1115a)).addInterceptor(new p1()).build());
        Context applicationContext = context.getApplicationContext();
        kotlin.d.b.j.a((Object) applicationContext, "context.applicationContext");
        Resources resources = applicationContext.getResources();
        kotlin.d.b.j.a((Object) resources, "context.applicationContext.resources");
        kotlin.d.b.j.b(resources, "resources");
        b.f16a = TypedValue.applyDimension(1, 1.0f, resources.getDisplayMetrics());
        nl.komponents.kovenant.a.d.a();
    }

    public final void showMuteInvites(Activity activity) {
        kotlin.d.b.j.b(activity, "activity");
        preload();
        new b.b.a.i.l1.a(activity).show();
    }

    public final void showNotification(Activity activity, String str, String str2, long j2) {
        kotlin.d.b.j.b(activity, "activity");
        kotlin.d.b.j.b(str, "scid");
        kotlin.d.b.j.b(str2, "game");
        preload();
        nl.komponents.kovenant.c.m.a(getSharedServices$supercellId_release().h.c(str), new q(activity, j2, str2));
    }

    public final void present(Activity activity, String str, String str2) {
        kotlin.d.b.j.b(activity, "activity");
        preload();
        g = str;
        MainActivity.a aVar = MainActivity.c;
        kotlin.d.b.j.b(activity, "activity");
        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra("ORIENTATION", activity.getRequestedOrientation());
        intent.putExtra("DEEPLINK", str2);
        activity.startActivity(intent);
    }

    public final void enterProfileState$supercellId_release(MainActivity mainActivity) {
        IdConfiguration idConfiguration;
        kotlin.d.b.j.b(mainActivity, "mainActivity");
        x sharedServices$supercellId_release = getSharedServices$supercellId_release();
        SupercellIdDelegate supercellIdDelegate = c;
        if (supercellIdDelegate == null || (idConfiguration = supercellIdDelegate.getConfig()) == null) {
            IdConfiguration.a aVar = IdConfiguration.Companion;
            idConfiguration = IdConfiguration.u;
        }
        SupercellIdDelegate supercellIdDelegate2 = c;
        sharedServices$supercellId_release.a(idConfiguration, supercellIdDelegate2 != null ? supercellIdDelegate2.getCurrentAccount() : null);
        g = null;
        b.a[] i2 = mainActivity.i();
        mainActivity.a((b.a[]) Arrays.copyOf(i2, i2.length));
        mainActivity.b(true);
    }

    public final void preload() {
        IdConfiguration idConfiguration;
        WeakReference<Context> weakReference = f4865b;
        if (weakReference != null && weakReference.get() != null) {
            x sharedServices$supercellId_release = getSharedServices$supercellId_release();
            SupercellIdDelegate supercellIdDelegate = c;
            if (supercellIdDelegate == null || (idConfiguration = supercellIdDelegate.getConfig()) == null) {
                IdConfiguration.a aVar = IdConfiguration.Companion;
                idConfiguration = IdConfiguration.u;
            }
            SupercellIdDelegate supercellIdDelegate2 = c;
            sharedServices$supercellId_release.a(idConfiguration, supercellIdDelegate2 != null ? supercellIdDelegate2.getCurrentAccount() : null);
            getSharedServices$supercellId_release().k.a(j.f4874a);
        }
    }

    public final void requestFriends() {
        IdConfiguration idConfiguration;
        WeakReference<Context> weakReference = f4865b;
        if (weakReference != null && weakReference.get() != null) {
            x sharedServices$supercellId_release = getSharedServices$supercellId_release();
            SupercellIdDelegate supercellIdDelegate = c;
            if (supercellIdDelegate == null || (idConfiguration = supercellIdDelegate.getConfig()) == null) {
                IdConfiguration.a aVar = IdConfiguration.Companion;
                idConfiguration = IdConfiguration.u;
            }
            SupercellIdDelegate supercellIdDelegate2 = c;
            String str = null;
            sharedServices$supercellId_release.a(idConfiguration, supercellIdDelegate2 != null ? supercellIdDelegate2.getCurrentAccount() : null);
            IdAccount idAccount = getSharedServices$supercellId_release().i;
            if (idAccount != null) {
                str = idAccount.getScidToken();
            }
            if (str == null) {
                SupercellIdDelegate supercellIdDelegate3 = c;
                if (supercellIdDelegate3 != null) {
                    supercellIdDelegate3.friendsFailed();
                    return;
                }
                return;
            }
            nl.komponents.kovenant.c.m.b(nl.komponents.kovenant.c.m.a(getSharedServices$supercellId_release().e().d(new l(str)), new m(str)), new n(str));
        }
    }

    public final void dismiss() {
        MainActivity.a aVar = MainActivity.c;
        WeakReference<MainActivity> weakReference = MainActivity.f4884b;
        MainActivity mainActivity = weakReference != null ? weakReference.get() : null;
        if (mainActivity != null) {
            mainActivity.e();
        }
    }
}
