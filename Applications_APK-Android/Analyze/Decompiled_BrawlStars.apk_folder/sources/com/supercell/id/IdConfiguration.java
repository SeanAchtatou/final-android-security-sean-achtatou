package com.supercell.id;

import java.text.Collator;
import java.util.Locale;
import kotlin.d;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.r;
import kotlin.d.b.t;
import kotlin.e;
import kotlin.h.h;
import kotlin.m;

public final class IdConfiguration {
    public static final a Companion = new a();
    public static final /* synthetic */ h[] t;
    public static final IdConfiguration u = new IdConfiguration("", "", "", "", "", "", "EN", "", "", "", false, false, "");

    /* renamed from: a  reason: collision with root package name */
    public final d f4854a = e.a(new f(this));

    /* renamed from: b  reason: collision with root package name */
    public final String f4855b;
    public final d c;
    public final d d;
    public final d e;
    public final d f;
    public final String g;
    public final String h;
    public final String i;
    public final String j;
    public final String k;
    public final String l;
    public final String m;
    public final String n;
    public final String o;
    public final String p;
    public final boolean q;
    public final boolean r;
    public final String s;

    public static final class a {
    }

    static {
        Class<IdConfiguration> cls = IdConfiguration.class;
        t = new h[]{t.a(new r(t.a(cls), "locale", "getLocale()Ljava/util/Locale;")), t.a(new r(t.a(cls), "collator", "getCollator()Ljava/text/Collator;")), t.a(new r(t.a(cls), "isRTL", "isRTL()Z")), t.a(new r(t.a(cls), "hasGameAccountToken", "getHasGameAccountToken()Z")), t.a(new r(t.a(cls), "isProdEnvironment", "isProdEnvironment()Z"))};
    }

    public IdConfiguration(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, boolean z, boolean z2, String str11) {
        j.b(str, "apiUrl");
        j.b(str2, "socialApiUrl");
        j.b(str3, "assetsUrl");
        j.b(str4, "gaTrackingId");
        j.b(str5, "gameAccountToken");
        j.b(str6, "gameAccountNickname");
        j.b(str7, "language");
        j.b(str8, "game");
        j.b(str9, "environment");
        j.b(str11, "versionSuffix");
        this.g = str;
        this.h = str2;
        this.i = str3;
        this.j = str4;
        this.k = str5;
        this.l = str6;
        this.m = str7;
        this.n = str8;
        this.o = str9;
        this.p = str10;
        this.q = z;
        this.r = z2;
        this.s = str11;
        StringBuilder a2 = b.a.a.a.a.a("game_name_");
        a2.append(this.n);
        this.f4855b = a2.toString();
        this.c = e.a(b.f4866a);
        this.d = e.a(new e(this));
        this.e = e.a(new c(this));
        this.f = e.a(new d(this));
    }

    public static /* synthetic */ IdConfiguration copy$default(IdConfiguration idConfiguration, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, boolean z, boolean z2, String str11, int i2, Object obj) {
        IdConfiguration idConfiguration2 = idConfiguration;
        int i3 = i2;
        return idConfiguration.copy((i3 & 1) != 0 ? idConfiguration2.g : str, (i3 & 2) != 0 ? idConfiguration2.h : str2, (i3 & 4) != 0 ? idConfiguration2.i : str3, (i3 & 8) != 0 ? idConfiguration2.j : str4, (i3 & 16) != 0 ? idConfiguration2.k : str5, (i3 & 32) != 0 ? idConfiguration2.l : str6, (i3 & 64) != 0 ? idConfiguration2.m : str7, (i3 & 128) != 0 ? idConfiguration2.n : str8, (i3 & 256) != 0 ? idConfiguration2.o : str9, (i3 & 512) != 0 ? idConfiguration2.p : str10, (i3 & 1024) != 0 ? idConfiguration2.q : z, (i3 & 2048) != 0 ? idConfiguration2.r : z2, (i3 & 4096) != 0 ? idConfiguration2.s : str11);
    }

    public static final IdConfiguration getNONE() {
        return u;
    }

    public final String component1() {
        return this.g;
    }

    public final String component10() {
        return this.p;
    }

    public final boolean component11() {
        return this.q;
    }

    public final boolean component12() {
        return this.r;
    }

    public final String component13() {
        return this.s;
    }

    public final String component2() {
        return this.h;
    }

    public final String component3() {
        return this.i;
    }

    public final String component4() {
        return this.j;
    }

    public final String component5() {
        return this.k;
    }

    public final String component6() {
        return this.l;
    }

    public final String component7() {
        return this.m;
    }

    public final String component8() {
        return this.n;
    }

    public final String component9() {
        return this.o;
    }

    public final IdConfiguration copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, boolean z, boolean z2, String str11) {
        String str12 = str;
        j.b(str12, "apiUrl");
        String str13 = str2;
        j.b(str13, "socialApiUrl");
        String str14 = str3;
        j.b(str14, "assetsUrl");
        String str15 = str4;
        j.b(str15, "gaTrackingId");
        String str16 = str5;
        j.b(str16, "gameAccountToken");
        String str17 = str6;
        j.b(str17, "gameAccountNickname");
        String str18 = str7;
        j.b(str18, "language");
        String str19 = str8;
        j.b(str19, "game");
        String str20 = str9;
        j.b(str20, "environment");
        String str21 = str11;
        j.b(str21, "versionSuffix");
        return new IdConfiguration(str12, str13, str14, str15, str16, str17, str18, str19, str20, str10, z, z2, str21);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof IdConfiguration) {
                IdConfiguration idConfiguration = (IdConfiguration) obj;
                if (j.a((Object) this.g, (Object) idConfiguration.g) && j.a((Object) this.h, (Object) idConfiguration.h) && j.a((Object) this.i, (Object) idConfiguration.i) && j.a((Object) this.j, (Object) idConfiguration.j) && j.a((Object) this.k, (Object) idConfiguration.k) && j.a((Object) this.l, (Object) idConfiguration.l) && j.a((Object) this.m, (Object) idConfiguration.m) && j.a((Object) this.n, (Object) idConfiguration.n) && j.a((Object) this.o, (Object) idConfiguration.o) && j.a((Object) this.p, (Object) idConfiguration.p)) {
                    if (this.q == idConfiguration.q) {
                        if (!(this.r == idConfiguration.r) || !j.a((Object) this.s, (Object) idConfiguration.s)) {
                            return false;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final void gameLocalizedName(b<? super String, m> bVar) {
        j.b(bVar, "callback");
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(this.f4855b, bVar);
    }

    public final String getApiUrl() {
        return this.g;
    }

    public final String getAssetsUrl() {
        return this.i;
    }

    public final Collator getCollator() {
        return (Collator) this.c.a();
    }

    public final String getEnvironment() {
        return this.o;
    }

    public final String getGaTrackingId() {
        return this.j;
    }

    public final String getGame() {
        return this.n;
    }

    public final String getGameAccountNickname() {
        return this.l;
    }

    public final String getGameAccountToken() {
        return this.k;
    }

    public final String getGameHelpLink() {
        return this.p;
    }

    public final String getGameLocalizedNameKey() {
        return this.f4855b;
    }

    public final boolean getHasGameAccountToken() {
        return ((Boolean) this.e.a()).booleanValue();
    }

    public final String getInstantGameLocalizedName() {
        String b2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().j.b(this.f4855b);
        return b2 != null ? b2 : "";
    }

    public final String getLanguage() {
        return this.m;
    }

    public final Locale getLocale() {
        return (Locale) this.f4854a.a();
    }

    public final boolean getSfxEnabled() {
        return this.q;
    }

    public final String getSocialApiUrl() {
        return this.h;
    }

    public final boolean getSocialFeatureEnabled() {
        return this.r;
    }

    public final String getVersionSuffix() {
        return this.s;
    }

    public final int hashCode() {
        String str = this.g;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.h;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.i;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.j;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.k;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.l;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.m;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.n;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.o;
        int hashCode9 = (hashCode8 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.p;
        int hashCode10 = (hashCode9 + (str10 != null ? str10.hashCode() : 0)) * 31;
        boolean z = this.q;
        if (z) {
            z = true;
        }
        int i3 = (hashCode10 + (z ? 1 : 0)) * 31;
        boolean z2 = this.r;
        if (z2) {
            z2 = true;
        }
        int i4 = (i3 + (z2 ? 1 : 0)) * 31;
        String str11 = this.s;
        if (str11 != null) {
            i2 = str11.hashCode();
        }
        return i4 + i2;
    }

    public final boolean isProdEnvironment() {
        return ((Boolean) this.f.a()).booleanValue();
    }

    public final boolean isRTL() {
        return ((Boolean) this.d.a()).booleanValue();
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdConfiguration(apiUrl=");
        a2.append(this.g);
        a2.append(", socialApiUrl=");
        a2.append(this.h);
        a2.append(", assetsUrl=");
        a2.append(this.i);
        a2.append(", gaTrackingId=");
        a2.append(this.j);
        a2.append(", gameAccountToken=");
        a2.append(this.k);
        a2.append(", gameAccountNickname=");
        a2.append(this.l);
        a2.append(", language=");
        a2.append(this.m);
        a2.append(", game=");
        a2.append(this.n);
        a2.append(", environment=");
        a2.append(this.o);
        a2.append(", gameHelpLink=");
        a2.append(this.p);
        a2.append(", sfxEnabled=");
        a2.append(this.q);
        a2.append(", socialFeatureEnabled=");
        a2.append(this.r);
        a2.append(", versionSuffix=");
        return b.a.a.a.a.a(a2, this.s, ")");
    }
}
