package com.supercell.id.ui;

import b.b.a.i.b;
import b.b.a.i.r0;
import java.util.Arrays;

public final class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ MainActivity f4890a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ boolean f4891b;

    public g(MainActivity mainActivity, boolean z) {
        this.f4890a = mainActivity;
        this.f4891b = z;
    }

    public final void run() {
        boolean z = MainActivity.b(this.f4890a).a() instanceof r0.a;
        if (this.f4891b && !z) {
            this.f4890a.a(new r0.a());
        } else if (!this.f4891b && z) {
            MainActivity mainActivity = this.f4890a;
            b.a[] i = mainActivity.i();
            mainActivity.a((b.a[]) Arrays.copyOf(i, i.length));
        }
    }
}
