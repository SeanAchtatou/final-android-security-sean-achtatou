package com.supercell.id.ui;

import kotlin.d.a.a;
import kotlin.d.b.h;
import kotlin.d.b.t;
import kotlin.h.d;
import kotlin.m;

public final /* synthetic */ class e extends h implements a<m> {
    public e(MainActivity mainActivity) {
        super(0, mainActivity);
    }

    public final String getName() {
        return "animatePageChange";
    }

    public final d getOwner() {
        return t.a(MainActivity.class);
    }

    public final String getSignature() {
        return "animatePageChange()V";
    }

    public final Object invoke() {
        MainActivity.a((MainActivity) this.receiver);
        return m.f5330a;
    }
}
