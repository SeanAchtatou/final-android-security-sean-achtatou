package com.supercell.id.ui;

import android.animation.AnimatorSet;
import kotlin.d.b.k;

public final class a extends k implements kotlin.d.a.a<AnimatorSet> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ MainActivity f4885a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(MainActivity mainActivity) {
        super(0);
        this.f4885a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.b.a(android.support.v4.app.Fragment, b.b.a.i.g$b, boolean):nl.komponents.kovenant.bw
     arg types: [android.support.v4.app.Fragment, b.b.a.i.g$b, int]
     candidates:
      b.b.a.b.a(android.support.v4.app.Fragment, java.lang.String, android.os.Parcelable):T
      b.b.a.b.a(android.support.v4.app.Fragment, java.lang.String, java.lang.String):T
      b.b.a.b.a(android.text.SpannableStringBuilder, java.lang.CharSequence, kotlin.h<? extends java.lang.Object, java.lang.Integer>[]):android.text.SpannableStringBuilder
      b.b.a.b.a(android.support.v4.app.Fragment, b.b.a.i.g$a, boolean):kotlin.m
      b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>):void
      b.b.a.b.a(android.view.View, java.lang.String, kotlin.h<java.lang.String, java.lang.String>[]):void
      b.b.a.b.a(android.widget.TextView, java.lang.String, android.graphics.Rect):void
      b.b.a.b.a(android.support.v4.app.Fragment, b.b.a.i.g$b, boolean):nl.komponents.kovenant.bw */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00f7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invoke() {
        /*
            r9 = this;
            com.supercell.id.ui.MainActivity r0 = r9.f4885a
            b.b.a.i.b r0 = com.supercell.id.ui.MainActivity.b(r0)
            b.b.a.i.b$a r1 = r0.a()
            r2 = 0
            if (r1 == 0) goto L_0x0054
            android.support.v4.app.FragmentManager r3 = r0.d
            android.content.Context r4 = r0.c
            android.content.res.Resources r4 = r4.getResources()
            java.lang.String r5 = "context.resources"
            kotlin.d.b.j.a(r4, r5)
            java.lang.String r4 = r1.b(r4)
            android.support.v4.app.Fragment r3 = r3.findFragmentByTag(r4)
            if (r3 == 0) goto L_0x0029
            b.b.a.i.g$b r4 = b.b.a.i.g.b.EXIT
            b.b.a.b.a(r3, r4, r2)
        L_0x0029:
            android.support.v4.app.FragmentManager r3 = r0.d
            android.content.Context r4 = r0.c
            android.content.res.Resources r4 = r4.getResources()
            kotlin.d.b.j.a(r4, r5)
            java.lang.String r4 = r1.f(r4)
            android.support.v4.app.Fragment r3 = r3.findFragmentByTag(r4)
            if (r3 == 0) goto L_0x0043
            b.b.a.i.g$b r4 = b.b.a.i.g.b.EXIT
            b.b.a.b.a(r3, r4, r2)
        L_0x0043:
            android.support.v4.app.FragmentManager r0 = r0.d
            java.lang.String r1 = r1.b()
            android.support.v4.app.Fragment r0 = r0.findFragmentByTag(r1)
            if (r0 == 0) goto L_0x0054
            b.b.a.i.g$b r1 = b.b.a.i.g.b.EXIT
            b.b.a.b.a(r0, r1, r2)
        L_0x0054:
            com.supercell.id.ui.MainActivity r0 = r9.f4885a
            android.content.res.Resources r0 = r0.getResources()
            java.lang.String r1 = "resources"
            kotlin.d.b.j.a(r0, r1)
            boolean r0 = b.b.a.b.a(r0)
            java.lang.String r3 = "content"
            r4 = 0
            r5 = 1
            r6 = 2
            if (r0 != 0) goto L_0x00a3
            com.supercell.id.ui.MainActivity r0 = r9.f4885a
            android.content.res.Resources r0 = r0.getResources()
            kotlin.d.b.j.a(r0, r1)
            boolean r0 = b.b.a.b.c(r0)
            if (r0 == 0) goto L_0x007a
            goto L_0x00a3
        L_0x007a:
            com.supercell.id.ui.MainActivity r0 = r9.f4885a
            int r7 = com.supercell.id.R.id.content
            android.view.View r0 = r0.a(r7)
            android.widget.FrameLayout r0 = (android.widget.FrameLayout) r0
            float[] r7 = new float[r6]
            r7[r2] = r4
            com.supercell.id.ui.MainActivity r4 = r9.f4885a
            int r8 = com.supercell.id.R.id.content
            android.view.View r4 = r4.a(r8)
            android.widget.FrameLayout r4 = (android.widget.FrameLayout) r4
            kotlin.d.b.j.a(r4, r3)
            int r3 = r4.getHeight()
            float r3 = (float) r3
            r7[r5] = r3
            java.lang.String r3 = "translationY"
            android.animation.ObjectAnimator r0 = android.animation.ObjectAnimator.ofFloat(r0, r3, r7)
            goto L_0x00cb
        L_0x00a3:
            com.supercell.id.ui.MainActivity r0 = r9.f4885a
            int r7 = com.supercell.id.R.id.content
            android.view.View r0 = r0.a(r7)
            android.widget.FrameLayout r0 = (android.widget.FrameLayout) r0
            float[] r7 = new float[r6]
            r7[r2] = r4
            com.supercell.id.ui.MainActivity r4 = r9.f4885a
            int r8 = com.supercell.id.R.id.content
            android.view.View r4 = r4.a(r8)
            android.widget.FrameLayout r4 = (android.widget.FrameLayout) r4
            kotlin.d.b.j.a(r4, r3)
            int r3 = r4.getWidth()
            float r3 = (float) r3
            r7[r5] = r3
            java.lang.String r3 = "translationX"
            android.animation.ObjectAnimator r0 = android.animation.ObjectAnimator.ofFloat(r0, r3, r7)
        L_0x00cb:
            android.view.animation.Interpolator r3 = b.b.a.f.a.c
            r0.setInterpolator(r3)
            com.supercell.id.ui.MainActivity r3 = r9.f4885a
            int r4 = com.supercell.id.R.id.dimmer
            android.view.View r3 = r3.a(r4)
            float[] r4 = new float[r6]
            r4 = {1065353216, 0} // fill-array
            java.lang.String r7 = "alpha"
            android.animation.ObjectAnimator r3 = android.animation.ObjectAnimator.ofFloat(r3, r7, r4)
            android.animation.AnimatorSet r4 = new android.animation.AnimatorSet
            r4.<init>()
            com.supercell.id.ui.MainActivity r7 = r9.f4885a
            android.content.res.Resources r7 = r7.getResources()
            kotlin.d.b.j.a(r7, r1)
            boolean r7 = b.b.a.b.a(r7)
            if (r7 != 0) goto L_0x010a
            com.supercell.id.ui.MainActivity r7 = r9.f4885a
            android.content.res.Resources r7 = r7.getResources()
            kotlin.d.b.j.a(r7, r1)
            boolean r1 = b.b.a.b.c(r7)
            if (r1 == 0) goto L_0x0107
            goto L_0x010a
        L_0x0107:
            r7 = 300(0x12c, double:1.48E-321)
            goto L_0x010c
        L_0x010a:
            r7 = 500(0x1f4, double:2.47E-321)
        L_0x010c:
            r4.setDuration(r7)
            android.animation.Animator[] r1 = new android.animation.Animator[r6]
            r1[r2] = r0
            r1[r5] = r3
            r4.playTogether(r1)
            b.b.a.i.n0 r1 = new b.b.a.i.n0
            r1.<init>(r9, r0, r3)
            r4.addListener(r1)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.id.ui.a.invoke():java.lang.Object");
    }
}
