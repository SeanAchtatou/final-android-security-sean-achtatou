package com.supercell.id.ui;

import b.b.a.h.h;
import java.lang.ref.WeakReference;
import kotlin.d.b.k;
import kotlin.m;

public final class b extends k implements kotlin.d.a.b<h, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f4886a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ boolean f4887b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(WeakReference weakReference, boolean z) {
        super(1);
        this.f4886a = weakReference;
        this.f4887b = z;
    }

    public final Object invoke(Object obj) {
        Object obj2 = this.f4886a.get();
        if (obj2 != null) {
            MainActivity.a((MainActivity) obj2, (h) obj, this.f4887b);
        }
        return m.f5330a;
    }
}
