package com.supercell.id.ui;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.net.UrlQuerySanitizer;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import b.b.a.g.i;
import b.b.a.i.a2.c;
import b.b.a.i.b;
import b.b.a.i.g1.a;
import b.b.a.i.i1.a;
import b.b.a.i.k1.c;
import b.b.a.i.m0;
import b.b.a.i.o1.p;
import b.b.a.i.q1.a;
import b.b.a.i.r0;
import b.b.a.i.r1.n;
import b.b.a.i.s1.h;
import b.b.a.i.u;
import b.b.a.i.u1.b;
import b.b.a.i.v;
import b.b.a.i.v1.j;
import b.b.a.i.w1.n;
import b.b.a.i.y1.a;
import b.b.a.j.f0;
import b.b.a.j.q0;
import b.b.a.j.q1;
import com.mobileapptracker.MATEvent;
import com.supercell.id.IdAccount;
import com.supercell.id.IdPendingLogin;
import com.supercell.id.IdPendingRegistration;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.view.RootFrameLayout;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.WeakHashMap;
import kotlin.TypeCastException;
import kotlin.a.m;
import kotlin.d;
import kotlin.d.b.j;
import kotlin.d.b.r;
import kotlin.d.b.t;
import kotlin.e;
import kotlin.h.h;
import nl.komponents.kovenant.bw;

public final class MainActivity extends AppCompatActivity implements SupercellId.a {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ h[] f4883a;

    /* renamed from: b  reason: collision with root package name */
    public static WeakReference<MainActivity> f4884b;
    public static final a c = new a();
    public b d;
    public final d e = e.a(new i(this));
    public final WeakHashMap<u, Integer> f = new WeakHashMap<>();
    public final d g = e.a(new j(this));
    public Rect h;
    public final d i = e.a(new h(this));
    public final d j = e.a(new a(this));
    public Animator k;
    public Animator l;
    public HashMap m;

    public static final class a {
    }

    static {
        Class<MainActivity> cls = MainActivity.class;
        f4883a = new h[]{t.a(new r(t.a(cls), "topAreaBackground", "getTopAreaBackground()Lcom/supercell/id/drawable/TopAreaBackgroundDrawable;")), t.a(new r(t.a(cls), "topAreaShadowWidth", "getTopAreaShadowWidth()I")), t.a(new r(t.a(cls), "systemWindowInsetsChangedListener", "getSystemWindowInsetsChangedListener()Lcom/supercell/id/ui/MainActivity$systemWindowInsetsChangedListener$2$1;")), t.a(new r(t.a(cls), "dismissAnimator", "getDismissAnimator()Landroid/animation/AnimatorSet;"))};
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.FragmentManager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* access modifiers changed from: private */
    public void a(b.b.a.i.e eVar, String str) {
        j.b(eVar, "dialogFragment");
        j.b(str, "tag");
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        j.a((Object) supportFragmentManager, "supportFragmentManager");
        if (supportFragmentManager.isStateSaved()) {
            MainActivity.class.getCanonicalName();
            return;
        }
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        Fragment findFragmentByTag = getSupportFragmentManager().findFragmentByTag(str);
        if (findFragmentByTag != null) {
            beginTransaction.remove(findFragmentByTag);
        }
        eVar.show(beginTransaction, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ void a(MainActivity mainActivity) {
        FrameLayout frameLayout = (FrameLayout) mainActivity.a(R.id.content);
        j.a((Object) frameLayout, "content");
        q1.a(frameLayout, new m0(mainActivity));
    }

    public static final /* synthetic */ b b(MainActivity mainActivity) {
        b bVar = mainActivity.d;
        if (bVar == null) {
            j.a("backStack");
        }
        return bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ void c(MainActivity mainActivity) {
        Resources resources = mainActivity.getResources();
        j.a((Object) resources, "resources");
        if (b.b.a.b.b(resources)) {
            Integer o = mainActivity.o();
            if (o != null) {
                int intValue = o.intValue();
                FrameLayout frameLayout = (FrameLayout) mainActivity.a(R.id.top_area);
                j.a((Object) frameLayout, "top_area");
                frameLayout.getLayoutParams().width = intValue;
            } else {
                return;
            }
        } else {
            Integer n = mainActivity.n();
            if (n != null) {
                int intValue2 = n.intValue();
                FrameLayout frameLayout2 = (FrameLayout) mainActivity.a(R.id.top_area);
                j.a((Object) frameLayout2, "top_area");
                frameLayout2.getLayoutParams().height = intValue2;
            } else {
                return;
            }
        }
        ((FrameLayout) mainActivity.a(R.id.top_area)).requestLayout();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ void e(MainActivity mainActivity) {
        Resources resources = mainActivity.getResources();
        j.a((Object) resources, "resources");
        if (b.b.a.b.b(resources)) {
            FrameLayout frameLayout = (FrameLayout) mainActivity.a(R.id.content);
            j.a((Object) frameLayout, "content");
            ViewGroup.MarginLayoutParams d2 = q1.d(frameLayout);
            if (d2 != null) {
                d2.leftMargin = mainActivity.l();
            }
        } else {
            Resources resources2 = mainActivity.getResources();
            j.a((Object) resources2, "resources");
            if (!b.b.a.b.c(resources2)) {
                FrameLayout frameLayout2 = (FrameLayout) mainActivity.a(R.id.content);
                j.a((Object) frameLayout2, "content");
                ViewGroup.MarginLayoutParams d3 = q1.d(frameLayout2);
                if (d3 != null) {
                    d3.topMargin = mainActivity.j();
                }
                mainActivity.g().a(mainActivity.k());
            } else {
                return;
            }
        }
        ((FrameLayout) mainActivity.a(R.id.content)).requestLayout();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    private int m() {
        Resources resources = getResources();
        j.a((Object) resources, "resources");
        if (!b.b.a.b.b(resources)) {
            return Math.abs(getResources().getDimensionPixelSize(R.dimen.bottom_area_overdraw));
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.RootFrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* access modifiers changed from: private */
    public Integer n() {
        b bVar = this.d;
        if (bVar == null) {
            j.a("backStack");
        }
        b.a a2 = bVar.a();
        if (a2 == null) {
            return null;
        }
        Resources resources = getResources();
        j.a((Object) resources, "resources");
        RootFrameLayout rootFrameLayout = (RootFrameLayout) a(R.id.root_layout);
        j.a((Object) rootFrameLayout, "root_layout");
        return Integer.valueOf(a2.b(resources, rootFrameLayout.getHeight() - j(), Math.max(0, ((RootFrameLayout) a(R.id.root_layout)).getSystemWindowInsets().top - j()), ((RootFrameLayout) a(R.id.root_layout)).getSystemWindowInsets().bottom));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.RootFrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* access modifiers changed from: private */
    public Integer o() {
        b bVar = this.d;
        if (bVar == null) {
            j.a("backStack");
        }
        b.a a2 = bVar.a();
        if (a2 == null) {
            return null;
        }
        int l2 = l();
        boolean z = false;
        int max = Math.max(0, ((RootFrameLayout) a(R.id.root_layout)).getSystemWindowInsets().left - l2);
        int i2 = ((RootFrameLayout) a(R.id.root_layout)).getSystemWindowInsets().right;
        int i3 = ViewCompat.getLayoutDirection((RootFrameLayout) a(R.id.root_layout)) == 1 ? i2 : max;
        if (ViewCompat.getLayoutDirection((RootFrameLayout) a(R.id.root_layout)) == 1) {
            z = true;
        }
        if (!z) {
            max = i2;
        }
        Resources resources = getResources();
        j.a((Object) resources, "resources");
        RootFrameLayout rootFrameLayout = (RootFrameLayout) a(R.id.root_layout);
        j.a((Object) rootFrameLayout, "root_layout");
        return Integer.valueOf(h() + a2.c(resources, rootFrameLayout.getWidth() - l2, i3, max));
    }

    public final View a(int i2) {
        if (this.m == null) {
            this.m = new HashMap();
        }
        View view = (View) this.m.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i2);
        this.m.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final List<b.a> a() {
        b bVar = this.d;
        if (bVar == null) {
            j.a("backStack");
        }
        return bVar.f181a;
    }

    public final void a(b.a aVar) {
        j.b(aVar, "entry");
        b bVar = this.d;
        if (bVar == null) {
            j.a("backStack");
        }
        bVar.c(aVar);
    }

    public final void a(u uVar) {
        j.b(uVar, "listener");
        this.f.put(uVar, 0);
    }

    public final void a(String str) {
        j.b(str, "url");
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        intent.putExtra("com.android.browser.application_id", getPackageName());
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException unused) {
        }
    }

    public final void a(boolean z) {
        runOnUiThread(new g(this, z));
    }

    public final void a(b.a... aVarArr) {
        j.b(aVarArr, "stack");
        b bVar = this.d;
        if (bVar == null) {
            j.a("backStack");
        }
        bVar.a((b.a[]) Arrays.copyOf(aVarArr, aVarArr.length));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void attachBaseContext(Context context) {
        j.b(context, "newBase");
        if (!SupercellId.INSTANCE.isInitialized$supercellId_release()) {
            super.attachBaseContext(context);
            return;
        }
        Locale locale = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getLocale();
        Resources resources = context.getResources();
        j.a((Object) resources, "res");
        Configuration configuration = new Configuration(resources.getConfiguration());
        if (Build.VERSION.SDK_INT >= 17) {
            configuration.setLocale(locale);
            context = context.createConfigurationContext(configuration);
        } else {
            configuration.locale = locale;
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }
        super.attachBaseContext(ViewPumpContextWrapper.wrap(context));
    }

    public final void b(u uVar) {
        j.b(uVar, "listener");
        if (this.f.containsKey(uVar)) {
            this.f.remove(uVar);
        }
    }

    public final boolean c() {
        return l() == 0;
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        j.b(motionEvent, "ev");
        b bVar = this.d;
        if (bVar == null) {
            j.a("backStack");
        }
        if (bVar.f182b) {
            return true;
        }
        j.b(this, "$this$doDispatchTouchEvent");
        return super.dispatchTouchEvent(motionEvent);
    }

    public final void e() {
        if (!isFinishing()) {
            b.b.a.b.a((Activity) this);
            ((AnimatorSet) this.j.a()).start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.RootFrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final int f() {
        a.C0020a.b bVar = a.C0020a.h;
        RootFrameLayout rootFrameLayout = (RootFrameLayout) a(R.id.root_layout);
        j.a((Object) rootFrameLayout, "root_layout");
        return bVar.a(rootFrameLayout.getHeight(), ((RootFrameLayout) a(R.id.root_layout)).getSystemWindowInsets().top, ((RootFrameLayout) a(R.id.root_layout)).getSystemWindowInsets().bottom);
    }

    public final void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    public final i g() {
        return (i) this.e.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final int h() {
        Resources resources = getResources();
        j.a((Object) resources, "resources");
        if (b.b.a.b.b(resources)) {
            return ((Number) this.g.a()).intValue();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final b.a[] i() {
        b.a[] aVarArr;
        if (j.a((Object) SupercellId.INSTANCE.getForcedView$supercellId_release(), (Object) "maintenance") || SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().a(q0.MAINTENANCE)) {
            return new r0.a[]{new r0.a()};
        }
        j.a aVar = null;
        if (kotlin.d.b.j.a((Object) SupercellId.INSTANCE.getForcedView$supercellId_release(), (Object) MATEvent.LOGIN)) {
            return new p.a[]{new p.a(null, null, null, 7)};
        }
        if (SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getHasGameAccountToken()) {
            IdAccount idAccount = SupercellId.INSTANCE.getSharedServices$supercellId_release().i;
            if (idAccount == null || !idAccount.getCanShowProfile$supercellId_release()) {
                IdPendingLogin pendingLogin$supercellId_release = SupercellId.INSTANCE.getPendingLogin$supercellId_release();
                IdPendingRegistration pendingRegistration$supercellId_release = SupercellId.INSTANCE.getPendingRegistration$supercellId_release();
                if (pendingLogin$supercellId_release != null) {
                    aVarArr = new b.a[]{new a.C0020a(), new p.a(pendingLogin$supercellId_release, null, null, 6)};
                } else if (pendingRegistration$supercellId_release != null) {
                    return new b.a[]{new a.C0020a(), new n.a(pendingRegistration$supercellId_release)};
                } else if (!SupercellId.INSTANCE.isTutorialComplete$supercellId_release()) {
                    return new c.a[]{new c.a(true)};
                } else {
                    return new b.a[]{new a.C0020a()};
                }
            } else if (!SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getSocialFeatureEnabled()) {
                return new h.a[]{new h.a()};
            } else {
                String forcedView$supercellId_release = SupercellId.INSTANCE.getForcedView$supercellId_release();
                if (forcedView$supercellId_release != null) {
                    int hashCode = forcedView$supercellId_release.hashCode();
                    if (hashCode != -1871933635) {
                        if (hashCode != -521964439) {
                            if (hashCode == 325368558 && forcedView$supercellId_release.equals("invite-players")) {
                                return new c.a[]{new c.a()};
                            }
                        } else if (forcedView$supercellId_release.equals("add-friends")) {
                            return new a.C0026a[]{new a.C0026a()};
                        }
                    } else if (forcedView$supercellId_release.equals("scan-code")) {
                        return new b.a[]{new n.a(), new a.C0095a()};
                    }
                }
                b.a[] aVarArr2 = new b.a[2];
                aVarArr2[0] = new n.a();
                String stringExtra = getIntent().getStringExtra("DEEPLINK");
                if (stringExtra != null) {
                    UrlQuerySanitizer urlQuerySanitizer = new UrlQuerySanitizer();
                    urlQuerySanitizer.setAllowUnregisteredParamaters(true);
                    urlQuerySanitizer.parseQuery(stringExtra);
                    String value = urlQuerySanitizer.getValue("p");
                    if (value != null) {
                        aVar = new j.a(value, null, null, null, null, null, false);
                    }
                }
                aVarArr2[1] = aVar;
                Object[] array = m.d(aVarArr2).toArray(new b.a[0]);
                if (array != null) {
                    return (b.a[]) array;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            IdPendingLogin pendingLogin$supercellId_release2 = SupercellId.INSTANCE.getPendingLogin$supercellId_release();
            if (pendingLogin$supercellId_release2 != null) {
                if (!(SupercellId.INSTANCE.getAccounts().length == 0)) {
                    aVarArr = new b.a[]{new b.a(), new p.a(pendingLogin$supercellId_release2, null, null, 6)};
                }
            }
            if (!(SupercellId.INSTANCE.getAccounts().length == 0)) {
                return new b.a[]{new b.a()};
            }
            return new p.a[]{new p.a(pendingLogin$supercellId_release2, null, null, 6)};
        }
        return aVarArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.RootFrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final int j() {
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (!b.b.a.b.a(resources)) {
            Resources resources2 = getResources();
            kotlin.d.b.j.a((Object) resources2, "resources");
            if (!b.b.a.b.c(resources2)) {
                b.b.a.i.b bVar = this.d;
                if (bVar == null) {
                    kotlin.d.b.j.a("backStack");
                }
                b.a a2 = bVar.a();
                if (a2 != null) {
                    Resources resources3 = getResources();
                    kotlin.d.b.j.a((Object) resources3, "resources");
                    RootFrameLayout rootFrameLayout = (RootFrameLayout) a(R.id.root_layout);
                    kotlin.d.b.j.a((Object) rootFrameLayout, "root_layout");
                    return a2.a(resources3, rootFrameLayout.getHeight(), ((RootFrameLayout) a(R.id.root_layout)).getSystemWindowInsets().top, ((RootFrameLayout) a(R.id.root_layout)).getSystemWindowInsets().bottom);
                }
            }
        }
        return 0;
    }

    public final float k() {
        if (j() == 0) {
            return 0.0f;
        }
        return b.b.a.b.a(12);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.RootFrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final int l() {
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (b.b.a.b.b(resources)) {
            b.b.a.i.b bVar = this.d;
            if (bVar == null) {
                kotlin.d.b.j.a("backStack");
            }
            b.a a2 = bVar.a();
            if (a2 != null) {
                RootFrameLayout rootFrameLayout = (RootFrameLayout) a(R.id.root_layout);
                kotlin.d.b.j.a((Object) rootFrameLayout, "root_layout");
                return a2.a(rootFrameLayout.getWidth(), ((RootFrameLayout) a(R.id.root_layout)).getSystemWindowInsets().left, ((RootFrameLayout) a(R.id.root_layout)).getSystemWindowInsets().right);
            }
        }
        return 0;
    }

    public final void onBackPressed() {
        d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.FragmentManager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.b.a(android.support.v4.app.Fragment, b.b.a.i.g$a, boolean):kotlin.m
     arg types: [android.support.v4.app.Fragment, b.b.a.i.g$a, int]
     candidates:
      b.b.a.b.a(android.support.v4.app.Fragment, java.lang.String, android.os.Parcelable):T
      b.b.a.b.a(android.support.v4.app.Fragment, java.lang.String, java.lang.String):T
      b.b.a.b.a(android.text.SpannableStringBuilder, java.lang.CharSequence, kotlin.h<? extends java.lang.Object, java.lang.Integer>[]):android.text.SpannableStringBuilder
      b.b.a.b.a(android.support.v4.app.Fragment, b.b.a.i.g$b, boolean):nl.komponents.kovenant.bw
      b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>):void
      b.b.a.b.a(android.view.View, java.lang.String, kotlin.h<java.lang.String, java.lang.String>[]):void
      b.b.a.b.a(android.widget.TextView, java.lang.String, android.graphics.Rect):void
      b.b.a.b.a(android.support.v4.app.Fragment, b.b.a.i.g$a, boolean):kotlin.m */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.RootFrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00f6, code lost:
        if (r3 == null) goto L_0x00f9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r9)
            com.supercell.id.ui.MainActivity.f4884b = r0
            super.onCreate(r10)     // Catch:{ Exception -> 0x0259 }
            com.supercell.id.SupercellId r0 = com.supercell.id.SupercellId.INSTANCE
            boolean r0 = r0.isInitialized$supercellId_release()
            if (r0 != 0) goto L_0x0018
            com.supercell.id.SupercellId r10 = com.supercell.id.SupercellId.INSTANCE
            r10.dismiss$supercellId_release(r9)
            return
        L_0x0018:
            r0 = 1
            android.support.v7.app.AppCompatDelegate.setCompatVectorFromResourcesEnabled(r0)
            int r1 = com.supercell.id.R.layout.activity_main
            r9.setContentView(r1)
            int r1 = com.supercell.id.R.id.top_area_background
            android.view.View r1 = r9.a(r1)
            b.b.a.g.i r2 = r9.g()
            android.support.v4.view.ViewCompat.setBackground(r1, r2)
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 18
            r3 = 0
            if (r1 >= r2) goto L_0x003e
            int r1 = com.supercell.id.R.id.top_area_background
            android.view.View r1 = r9.a(r1)
            r1.setLayerType(r0, r3)
        L_0x003e:
            int r1 = com.supercell.id.R.id.content
            android.view.View r1 = r9.a(r1)
            android.widget.FrameLayout r1 = (android.widget.FrameLayout) r1
            int r2 = com.supercell.id.R.id.root_layout
            android.view.View r2 = r9.a(r2)
            com.supercell.id.view.RootFrameLayout r2 = (com.supercell.id.view.RootFrameLayout) r2
            android.view.View$OnLayoutChangeListener r2 = r2.getLayoutChangeListener()
            r1.addOnLayoutChangeListener(r2)
            android.content.Intent r1 = r9.getIntent()
            java.lang.String r2 = "ORIENTATION"
            boolean r1 = r1.hasExtra(r2)
            if (r1 == 0) goto L_0x0073
            int r1 = android.os.Build.VERSION.SDK_INT
            r4 = 26
            if (r1 == r4) goto L_0x0073
            android.content.Intent r1 = r9.getIntent()
            r4 = -1
            int r1 = r1.getIntExtra(r2, r4)
            r9.setRequestedOrientation(r1)
        L_0x0073:
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 21
            if (r1 < r2) goto L_0x0082
            android.view.Window r1 = r9.getWindow()
            r2 = 67108864(0x4000000, float:1.5046328E-36)
            r1.clearFlags(r2)
        L_0x0082:
            android.content.res.Resources r1 = r9.getResources()
            java.lang.String r2 = "resources"
            kotlin.d.b.j.a(r1, r2)
            boolean r1 = b.b.a.b.b(r1)
            if (r1 == 0) goto L_0x00a1
            android.view.Window r1 = r9.getWindow()
            r4 = 1024(0x400, float:1.435E-42)
            r1.addFlags(r4)
            android.view.Window r1 = r9.getWindow()
            r4 = 32
            goto L_0x00a7
        L_0x00a1:
            android.view.Window r1 = r9.getWindow()
            r4 = 16
        L_0x00a7:
            r1.setSoftInputMode(r4)
            r1 = 0
            java.lang.String r4 = "supportFragmentManager"
            if (r10 == 0) goto L_0x00f9
            android.support.v4.app.FragmentManager r5 = r9.getSupportFragmentManager()
            kotlin.d.b.j.a(r5, r4)
            com.supercell.id.ui.d r6 = new com.supercell.id.ui.d
            r6.<init>(r9)
            java.lang.String r7 = "$this$getBackStack"
            kotlin.d.b.j.b(r10, r7)
            java.lang.String r7 = "context"
            kotlin.d.b.j.b(r9, r7)
            kotlin.d.b.j.b(r5, r4)
            java.lang.String r7 = "animateChangeCallback"
            kotlin.d.b.j.b(r6, r7)
            java.lang.String r7 = "backstack"
            java.util.ArrayList r7 = r10.getParcelableArrayList(r7)     // Catch:{ Exception -> 0x00f5 }
            if (r7 == 0) goto L_0x00f6
            b.b.a.i.b$a[] r8 = new b.b.a.i.b.a[r1]     // Catch:{ Exception -> 0x00f5 }
            java.lang.Object[] r7 = r7.toArray(r8)     // Catch:{ Exception -> 0x00f5 }
            if (r7 == 0) goto L_0x00ed
            b.b.a.i.b$a[] r7 = (b.b.a.i.b.a[]) r7     // Catch:{ Exception -> 0x00f5 }
            int r8 = r7.length     // Catch:{ Exception -> 0x00f5 }
            java.lang.Object[] r7 = java.util.Arrays.copyOf(r7, r8)     // Catch:{ Exception -> 0x00f5 }
            b.b.a.i.b$a[] r7 = (b.b.a.i.b.a[]) r7     // Catch:{ Exception -> 0x00f5 }
            b.b.a.i.b r8 = new b.b.a.i.b     // Catch:{ Exception -> 0x00f5 }
            r8.<init>(r9, r5, r6, r7)     // Catch:{ Exception -> 0x00f5 }
            r3 = r8
            goto L_0x00f6
        L_0x00ed:
            kotlin.TypeCastException r5 = new kotlin.TypeCastException     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r6 = "null cannot be cast to non-null type kotlin.Array<T>"
            r5.<init>(r6)     // Catch:{ Exception -> 0x00f5 }
            throw r5     // Catch:{ Exception -> 0x00f5 }
        L_0x00f5:
        L_0x00f6:
            if (r3 == 0) goto L_0x00f9
            goto L_0x0115
        L_0x00f9:
            b.b.a.i.b r3 = new b.b.a.i.b
            android.support.v4.app.FragmentManager r5 = r9.getSupportFragmentManager()
            kotlin.d.b.j.a(r5, r4)
            com.supercell.id.ui.e r4 = new com.supercell.id.ui.e
            r4.<init>(r9)
            b.b.a.i.b$a[] r6 = r9.i()
            int r7 = r6.length
            java.lang.Object[] r6 = java.util.Arrays.copyOf(r6, r7)
            b.b.a.i.b$a[] r6 = (b.b.a.i.b.a[]) r6
            r3.<init>(r9, r5, r4, r6)
        L_0x0115:
            r9.d = r3
            java.lang.String r3 = "root_layout"
            if (r10 != 0) goto L_0x0185
            b.b.a.i.b r10 = r9.d
            if (r10 != 0) goto L_0x0124
            java.lang.String r4 = "backStack"
            kotlin.d.b.j.a(r4)
        L_0x0124:
            b.b.a.i.b$a r4 = r10.a()
            if (r4 == 0) goto L_0x0171
            android.support.v4.app.FragmentManager r5 = r10.d
            android.content.Context r6 = r10.c
            android.content.res.Resources r6 = r6.getResources()
            java.lang.String r7 = "context.resources"
            kotlin.d.b.j.a(r6, r7)
            java.lang.String r6 = r4.b(r6)
            android.support.v4.app.Fragment r5 = r5.findFragmentByTag(r6)
            if (r5 == 0) goto L_0x0146
            b.b.a.i.g$a r6 = b.b.a.i.g.a.ENTRY
            b.b.a.b.a(r5, r6, r0)
        L_0x0146:
            android.support.v4.app.FragmentManager r5 = r10.d
            android.content.Context r6 = r10.c
            android.content.res.Resources r6 = r6.getResources()
            kotlin.d.b.j.a(r6, r7)
            java.lang.String r6 = r4.f(r6)
            android.support.v4.app.Fragment r5 = r5.findFragmentByTag(r6)
            if (r5 == 0) goto L_0x0160
            b.b.a.i.g$a r6 = b.b.a.i.g.a.ENTRY
            b.b.a.b.a(r5, r6, r0)
        L_0x0160:
            android.support.v4.app.FragmentManager r10 = r10.d
            java.lang.String r4 = r4.b()
            android.support.v4.app.Fragment r10 = r10.findFragmentByTag(r4)
            if (r10 == 0) goto L_0x0171
            b.b.a.i.g$a r4 = b.b.a.i.g.a.ENTRY
            b.b.a.b.a(r10, r4, r0)
        L_0x0171:
            int r10 = com.supercell.id.R.id.root_layout
            android.view.View r10 = r9.a(r10)
            com.supercell.id.view.RootFrameLayout r10 = (com.supercell.id.view.RootFrameLayout) r10
            kotlin.d.b.j.a(r10, r3)
            b.b.a.i.g0 r0 = new b.b.a.i.g0
            r0.<init>(r9)
            b.b.a.j.q1.a(r10, r0)
            goto L_0x0198
        L_0x0185:
            int r10 = com.supercell.id.R.id.root_layout
            android.view.View r10 = r9.a(r10)
            com.supercell.id.view.RootFrameLayout r10 = (com.supercell.id.view.RootFrameLayout) r10
            kotlin.d.b.j.a(r10, r3)
            b.b.a.i.p0 r0 = new b.b.a.i.p0
            r0.<init>(r9)
            b.b.a.j.q1.a(r10, r0)
        L_0x0198:
            r9.b(r1)
            com.supercell.id.SupercellId r10 = com.supercell.id.SupercellId.INSTANCE
            r10.addMaintenanceModeListener$supercellId_release(r9)
            int r10 = r9.h()
            if (r10 <= 0) goto L_0x01ea
            int r10 = com.supercell.id.R.id.top_area
            android.view.View r10 = r9.a(r10)
            android.widget.FrameLayout r10 = (android.widget.FrameLayout) r10
            android.view.ViewGroup$MarginLayoutParams r0 = b.b.a.j.q1.d(r10)
            if (r0 == 0) goto L_0x01bb
            int r1 = r9.h()
            int r1 = -r1
            r0.rightMargin = r1
        L_0x01bb:
            int r0 = r10.getPaddingLeft()
            int r1 = r10.getPaddingTop()
            int r3 = r10.getPaddingRight()
            int r4 = r9.h()
            int r4 = r4 + r3
            int r3 = r10.getPaddingBottom()
            r10.setPadding(r0, r1, r4, r3)
            int r10 = com.supercell.id.R.id.top_area_background
            android.view.View r10 = r9.a(r10)
            java.lang.String r0 = "top_area_background"
            kotlin.d.b.j.a(r10, r0)
            android.view.ViewGroup$MarginLayoutParams r10 = b.b.a.j.q1.d(r10)
            if (r10 == 0) goto L_0x01ea
            int r0 = r9.h()
            r10.rightMargin = r0
        L_0x01ea:
            int r10 = r9.m()
            if (r10 <= 0) goto L_0x021d
            int r10 = com.supercell.id.R.id.bottom_area
            android.view.View r10 = r9.a(r10)
            android.widget.FrameLayout r10 = (android.widget.FrameLayout) r10
            android.view.ViewGroup$MarginLayoutParams r0 = b.b.a.j.q1.d(r10)
            if (r0 == 0) goto L_0x0205
            int r1 = r9.m()
            int r1 = -r1
            r0.topMargin = r1
        L_0x0205:
            int r0 = r10.getPaddingLeft()
            int r1 = r10.getPaddingTop()
            int r3 = r9.m()
            int r3 = r3 + r1
            int r1 = r10.getPaddingRight()
            int r4 = r10.getPaddingBottom()
            r10.setPadding(r0, r3, r1, r4)
        L_0x021d:
            android.content.res.Resources r10 = r9.getResources()
            kotlin.d.b.j.a(r10, r2)
            boolean r10 = b.b.a.b.c(r10)
            if (r10 != 0) goto L_0x0238
            int r10 = com.supercell.id.R.id.dimmer
            android.view.View r10 = r9.a(r10)
            com.supercell.id.ui.f r0 = new com.supercell.id.ui.f
            r0.<init>(r9)
            r10.setOnClickListener(r0)
        L_0x0238:
            android.content.res.Resources r10 = r9.getResources()
            kotlin.d.b.j.a(r10, r2)
            boolean r10 = b.b.a.b.b(r10)
            if (r10 == 0) goto L_0x0258
            int r10 = com.supercell.id.R.id.root_layout
            android.view.View r10 = r9.a(r10)
            com.supercell.id.view.RootFrameLayout r10 = (com.supercell.id.view.RootFrameLayout) r10
            kotlin.d r0 = r9.i
            java.lang.Object r0 = r0.a()
            b.b.a.i.q0 r0 = (b.b.a.i.q0) r0
            r10.a(r0)
        L_0x0258:
            return
        L_0x0259:
            java.lang.Class<com.supercell.id.ui.MainActivity> r10 = com.supercell.id.ui.MainActivity.class
            r10.getCanonicalName()
            com.supercell.id.SupercellId r10 = com.supercell.id.SupercellId.INSTANCE
            r10.dismiss$supercellId_release(r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.id.ui.MainActivity.onCreate(android.os.Bundle):void");
    }

    public final void onDestroy() {
        SupercellId.INSTANCE.removeMaintenanceModeListener$supercellId_release(this);
        RootFrameLayout rootFrameLayout = (RootFrameLayout) a(R.id.root_layout);
        if (rootFrameLayout != null) {
            rootFrameLayout.b((b.b.a.i.q0) this.i.a());
        }
        super.onDestroy();
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        kotlin.d.b.j.b(this, "$this$doOnKeyUp");
        return super.onKeyUp(i2, keyEvent);
    }

    public final void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            b.b.a.i.b bVar = this.d;
            if (bVar == null) {
                kotlin.d.b.j.a("backStack");
            }
            kotlin.d.b.j.b(bundle, "$this$putBackStack");
            kotlin.d.b.j.b(bVar, "backStack");
            bundle.putParcelableArrayList("backstack", bVar.f181a);
        }
        super.onSaveInstanceState(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.g.i.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      b.b.a.g.i.a(b.b.a.g.i, int):void
      b.b.a.g.i.a(boolean, float):void
      b.b.a.g.i.a(boolean, boolean):void */
    public final void onStart() {
        boolean z;
        boolean z2;
        SupercellId.INSTANCE.onWindowClientStart$supercellId_release();
        b.b.a.i.b bVar = this.d;
        if (bVar == null) {
            kotlin.d.b.j.a("backStack");
        }
        b.a a2 = bVar.a();
        i g2 = g();
        if (a2 != null) {
            Resources resources = getResources();
            kotlin.d.b.j.a((Object) resources, "resources");
            z = a2.d(resources);
        } else {
            z = false;
        }
        g2.c(z);
        i g3 = g();
        if (a2 != null) {
            Resources resources2 = getResources();
            kotlin.d.b.j.a((Object) resources2, "resources");
            z2 = a2.c(resources2);
        } else {
            z2 = true;
        }
        g3.a(z2, false);
        super.onStart();
    }

    public final void onStop() {
        g().c(false);
        SupercellId.INSTANCE.onWindowClientStop$supercellId_release();
        super.onStop();
    }

    public final void onTrimMemory(int i2) {
        super.onTrimMemory(i2);
        if (i2 == 5 || i2 == 10 || i2 == 15 || i2 == 20 || i2 == 40 || i2 == 60 || i2 == 80) {
            SupercellId.INSTANCE.clearImageAssetsFromMemoryCache();
        }
    }

    public final int b() {
        b.b.a.i.b bVar = this.d;
        if (bVar == null) {
            kotlin.d.b.j.a("backStack");
        }
        return bVar.f181a.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.b.a(java.util.ArrayList, int):java.util.ArrayList<T>
     arg types: [java.util.ArrayList<b.b.a.i.b$a>, int]
     candidates:
      b.b.a.b.a(int, float):int
      b.b.a.b.a(android.support.v7.widget.RecyclerView, int):int
      b.b.a.b.a(java.lang.String, java.lang.String):int
      b.b.a.b.a(android.widget.ImageView, int):android.animation.Animator
      b.b.a.b.a(java.util.ArrayList, java.lang.Object):java.util.ArrayList<T>
      b.b.a.b.a(java.util.Map, java.util.Map):java.util.Map<K, V>
      b.b.a.b.a(b.b.a.i.x1.f, java.lang.String):nl.komponents.kovenant.bw
      b.b.a.b.a(com.supercell.id.ui.MainActivity, android.view.View):void
      b.b.a.b.a(com.supercell.id.ui.MainActivity, com.supercell.id.IdAccount):void
      b.b.a.b.a(java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>):void
      b.b.a.b.a(java.util.List<? extends b.b.a.j.r0>, int):boolean
      b.b.a.b.a(java.util.ArrayList, int):java.util.ArrayList<T> */
    public final void d() {
        b.b.a.b.a((Activity) this);
        b.b.a.i.b bVar = this.d;
        if (bVar == null) {
            kotlin.d.b.j.a("backStack");
        }
        boolean z = true;
        if (bVar.f181a.size() <= 1) {
            z = false;
        } else if (bVar.d.isStateSaved()) {
            b.b.a.i.b.class.getCanonicalName();
        } else {
            ArrayList<b.a> arrayList = bVar.f181a;
            bVar.f181a = b.b.a.b.a((ArrayList) arrayList, 1);
            bVar.a(arrayList, bVar.f181a, false, true);
        }
        if (!z) {
            e();
        }
    }

    public final void a(Exception exc, kotlin.d.a.b<? super b.b.a.i.e, kotlin.m> bVar) {
        kotlin.d.b.j.b(exc, "error");
        "Showing error message caused by: " + Log.getStackTraceString(exc);
        v a2 = v.f.a(f0.e.a(exc));
        a2.f234a = bVar;
        a(a2, "errorDialog");
    }

    public final void a(String str, kotlin.d.a.b<? super b.b.a.i.e, kotlin.m> bVar) {
        kotlin.d.b.j.b(str, "error");
        v a2 = v.f.a(f0.e.a(str));
        a2.f234a = bVar;
        a(a2, "errorDialog");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final void b(boolean z) {
        IdAccount idAccount;
        boolean z2 = true;
        if (!(!kotlin.d.b.j.a((Object) SupercellId.INSTANCE.getForcedView$supercellId_release(), (Object) "maintenance")) || SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().a(q0.MAINTENANCE) || !(!kotlin.d.b.j.a((Object) SupercellId.INSTANCE.getForcedView$supercellId_release(), (Object) MATEvent.LOGIN)) || !SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getHasGameAccountToken() || (idAccount = SupercellId.INSTANCE.getSharedServices$supercellId_release().i) == null || !idAccount.getCanShowProfile$supercellId_release() || !SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getSocialFeatureEnabled()) {
            z2 = false;
        }
        if (z2) {
            bw<b.b.a.h.h, Exception> a2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().f().a();
            WeakReference weakReference = new WeakReference(this);
            nl.komponents.kovenant.c.m.a(a2, new b(weakReference, z));
            nl.komponents.kovenant.c.m.b(a2, new c(weakReference));
        }
    }

    public static final /* synthetic */ void a(MainActivity mainActivity, b.b.a.h.h hVar, boolean z) {
        if (!mainActivity.isFinishing() && !(m.d((List) mainActivity.a()) instanceof a.C0048a) && hVar.b()) {
            mainActivity.a(new a.C0048a(hVar, mainActivity.a(), z));
        }
    }
}
