package com.supercell.id.ui;

import b.b.a.i.o0;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class c extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f4888a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(WeakReference weakReference) {
        super(1);
        this.f4888a = weakReference;
    }

    public final Object invoke(Object obj) {
        Object obj2 = this.f4888a.get();
        if (obj2 != null) {
            MainActivity mainActivity = (MainActivity) obj2;
            mainActivity.a((Exception) obj, new o0(mainActivity));
        }
        return m.f5330a;
    }
}
