package com.supercell.id.ui;

import com.supercell.id.R;
import kotlin.d.a.a;
import kotlin.d.b.k;

public final class j extends k implements a<Integer> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ MainActivity f4894a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(MainActivity mainActivity) {
        super(0);
        this.f4894a = mainActivity;
    }

    public final Object invoke() {
        return Integer.valueOf(this.f4894a.getResources().getDimensionPixelSize(R.dimen.top_area_shadow_width));
    }
}
