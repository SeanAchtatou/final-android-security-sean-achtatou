package com.supercell.id;

import java.util.List;
import kotlin.TypeCastException;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class l extends k implements b<List<? extends IdFriend>, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ String f4876a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(String str) {
        super(1);
        this.f4876a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final Object invoke(Object obj) {
        SupercellIdDelegate access$getDelegate$p;
        List list = (List) obj;
        j.b(list, "it");
        String str = this.f4876a;
        IdAccount idAccount = SupercellId.INSTANCE.getSharedServices$supercellId_release().i;
        if (j.a((Object) str, (Object) (idAccount != null ? idAccount.getScidToken() : null)) && (access$getDelegate$p = SupercellId.c) != null) {
            Object[] array = list.toArray(new IdFriend[0]);
            if (array != null) {
                access$getDelegate$p.friendsChanged((IdFriend[]) array);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return m.f5330a;
    }
}
