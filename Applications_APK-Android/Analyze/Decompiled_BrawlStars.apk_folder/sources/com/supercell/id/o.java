package com.supercell.id;

import kotlin.d.a.e;
import kotlin.d.b.k;
import kotlin.m;

public final class o extends k implements e<byte[], Integer, Integer, Integer, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ String f4879a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(String str) {
        super(4);
        this.f4879a = str;
    }

    public final Object a(Object obj, Object obj2, Object obj3, Object obj4) {
        byte[] bArr = (byte[]) obj;
        int intValue = ((Number) obj2).intValue();
        int intValue2 = ((Number) obj3).intValue();
        int intValue3 = ((Number) obj4).intValue();
        SupercellIdDelegate access$getDelegate$p = SupercellId.c;
        if (access$getDelegate$p != null) {
            access$getDelegate$p.avatarImageData(bArr, intValue, intValue2, intValue3, this.f4879a);
        }
        return m.f5330a;
    }
}
