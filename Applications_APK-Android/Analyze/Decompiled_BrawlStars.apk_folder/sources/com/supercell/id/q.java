package com.supercell.id;

import android.app.Activity;
import b.b.a.h.i;
import b.b.a.i.m1.a;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class q extends k implements b<i, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ Activity f4881a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ long f4882b;
    public final /* synthetic */ String c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q(Activity activity, long j, String str) {
        super(1);
        this.f4881a = activity;
        this.f4882b = j;
        this.c = str;
    }

    public final Object invoke(Object obj) {
        i iVar = (i) obj;
        j.b(iVar, "it");
        new a(this.f4881a, this.f4882b, iVar.f123b, this.c).show();
        return m.f5330a;
    }
}
