package com.supercell.id;

import android.content.Context;
import b.b.a.j.x;
import com.supercell.id.IdConfiguration;
import java.lang.ref.WeakReference;
import kotlin.d.a.a;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class p extends k implements a<x> {

    /* renamed from: a  reason: collision with root package name */
    public static final p f4880a = new p();

    public p() {
        super(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Object invoke() {
        Context context;
        IdConfiguration idConfiguration;
        WeakReference access$getWeakContext$p = SupercellId.f4865b;
        if (access$getWeakContext$p == null || (context = (Context) access$getWeakContext$p.get()) == null) {
            throw new IllegalStateException("No context available");
        }
        j.a((Object) context, "weakContext?.get() ?: th…n(\"No context available\")");
        SupercellIdDelegate access$getDelegate$p = SupercellId.c;
        if (access$getDelegate$p == null || (idConfiguration = access$getDelegate$p.getConfig()) == null) {
            IdConfiguration.a aVar = IdConfiguration.Companion;
            idConfiguration = IdConfiguration.u;
        }
        SupercellIdDelegate access$getDelegate$p2 = SupercellId.c;
        return new x(context, idConfiguration, access$getDelegate$p2 != null ? access$getDelegate$p2.getCurrentAccount() : null);
    }
}
