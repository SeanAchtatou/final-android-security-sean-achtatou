package com.supercell.brawlstars;

public final class BuildConfig {
    public static final String APPCENTER_APPSECRET = "89dd7c2d-1cb4-4de6-8d75-48f6635201e2";
    public static final String APPLICATION_ID = "com.supercell.brawlstars";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "googlePlay";
    public static final int UNBOTIFY_LABEL = 0;
    public static final int VERSION_CODE = 111;
    public static final String VERSION_NAME = "25.130";
}
