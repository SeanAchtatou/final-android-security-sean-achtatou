package com.supercell.brawlstars;

import android.content.Intent;
import android.os.Bundle;
import com.linecorp.nova.android.NovaNative;

public class GameApp extends com.supercell.titan.GameApp {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        NovaNative.setActivity(this);
    }

    public void onPause() {
        super.onPause();
        NovaNative.onPause();
    }

    public void onResume() {
        super.onResume();
        NovaNative.onResume();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        NovaNative.onActivityResult(i, i2, intent);
    }
}
