package com.supercell.titan;

import android.os.Bundle;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;

/* compiled from: NativeFacebookRequestWhoAmI */
public class cw implements Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final GameApp f5098a;

    public cw(GameApp gameApp) {
        this.f5098a = gameApp;
    }

    public void run() {
        if (AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired()) {
            try {
                cx cxVar = new cx(this);
                Bundle bundle = new Bundle(1);
                bundle.putString(GraphRequest.FIELDS_PARAM, "id,name,picture,first_name,last_name,installed");
                new GraphRequest(AccessToken.getCurrentAccessToken(), "me", bundle, null, cxVar).executeAsync();
            } catch (IllegalStateException e) {
                GameApp.debuggerException(e);
            }
        }
    }
}
