package com.supercell.titan;

import android.view.GestureDetector;
import com.supercell.titan.TitanWebView;

/* compiled from: TitanWebView */
class ec implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TitanWebView f5149a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ TitanWebView f5150b;

    ec(TitanWebView titanWebView, TitanWebView titanWebView2) {
        this.f5150b = titanWebView;
        this.f5149a = titanWebView2;
    }

    public void run() {
        this.f5150b.f4995b.setOnTouchListener(new ed(this, new GestureDetector(GameApp.getInstance(), new TitanWebView.a(this.f5149a))));
    }
}
