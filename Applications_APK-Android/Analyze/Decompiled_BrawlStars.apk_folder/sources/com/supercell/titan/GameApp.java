package com.supercell.titan;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.Signature;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.mobileapptracker.MobileAppTracker;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;
import org.OpenUDID.OpenUDID_manager;
import org.fmod.FMOD;
import org.json.JSONArray;
import org.json.JSONObject;

public class GameApp extends Activity {
    private static GameApp i;
    private static UnbotifyManager o;
    private static int t = ((int) (System.currentTimeMillis() / 1000));
    private static final Vector<b> u = new Vector<>();
    private static final Vector<Long> v = new Vector<>();
    private final Class<?> A;
    private SecurePreferences B;
    private boolean C;
    private GoogleServiceClient D;
    private MobileAppTracker E;
    private boolean F;
    private final Vector<String> G;
    private int H;
    private boolean I;

    /* renamed from: a  reason: collision with root package name */
    Vector<a> f4965a;

    /* renamed from: b  reason: collision with root package name */
    i f4966b;
    boolean c;
    String d;
    PurchaseManager e;
    SecurePreferences f;
    SecurePreferences g;
    public int h;
    private WifiManager.WifiLock j;
    private boolean k;
    private boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    private Thread n;
    private String p;
    private DisplayManager.DisplayListener q;
    private boolean r;
    private boolean s;
    private AlarmManager w;
    /* access modifiers changed from: private */
    public String x;
    private final int y;
    private Bundle z;

    public interface a {
        void onRequestPermissionsResult(int i, String[] strArr, int[] iArr);
    }

    public static native boolean backButtonPressed();

    public static native String createGameMain(AssetManager assetManager, String str, String str2, String str3, long j2, int i2, int i3, int i4, int i5, Activity activity);

    public static native void deinit();

    public static native void dialogDismissed(int i2, int i3);

    public static native void handleDeeplinkURL(String str);

    public static native boolean init(String str);

    public static native boolean isScreenResizeSupported();

    public static native void logDebuggerException(String str);

    public static native boolean nOnActivityResult(int i2, int i3, Intent intent);

    public static native void nOnConfigurationChanged(Configuration configuration);

    public static native void nOnCreate();

    public static native void nOnDestroy();

    public static native void nOnDisplayAdded(int i2);

    public static native void nOnDisplayChanged(int i2);

    public static native void nOnDisplayRemoved(int i2);

    public static native void nOnPause();

    public static native void nOnRestart();

    public static native void nOnResume();

    public static native void nOnStart();

    public static native void nOnStop();

    public static native void nOnSurfaceChanged(Surface surface, int i2, int i3);

    public static native void nOnSurfaceCreated(Surface surface);

    public static native void nOnSurfaceDestroyed(Surface surface);

    public static native void nOnTouchEvent(int i2, int i3, int i4, int i5);

    public static native void nOnWindowFocusChanged(boolean z2);

    public static native void nRunFromUiThread(long j2);

    public static native void nSetKunlunSwitchAccountRequested();

    public static native void setDeviceVerificationResult(boolean z2, boolean z3, String str);

    public static native void setPushNotificationValues(int i2, String str, String str2);

    public static native void setSafeMargins(int i2, int i3, int i4, int i5);

    public static native void setTencentDiffLogin();

    public static native void setTencentLaunchParameter(String str, int i2, String str2);

    public static native void setTencentLoggedOut(int i2);

    public static native void setTencentLogin(String str, String str2, int i2);

    public static native void setTencentShareResult(int i2);

    public static native void setTencentUserInfo(String str);

    public static native void setTencentWaiting(boolean z2);

    public static native void start(String str);

    public static native void stop();

    public static boolean isNativeLibraryLoaded() {
        return i.r;
    }

    static class b {

        /* renamed from: a  reason: collision with root package name */
        PendingIntent f4967a;

        /* renamed from: b  reason: collision with root package name */
        String f4968b;
        int c;
        String d;
        String e;
        String f;
        String g;
        String h;
        String i;
        String j;
        int k;

        private b() {
            this.f4968b = "";
            this.d = "";
            this.e = "";
            this.f = "";
            this.g = "";
            this.h = "";
            this.i = "";
            this.j = "";
        }

        /* synthetic */ b(byte b2) {
            this();
        }
    }

    public static GameApp getInstance() {
        return i;
    }

    public GameApp() {
        this(TimeAlarm.class, (Class<?>) null);
    }

    private GameApp(Class<?> cls, Class<?> cls2) {
        this(-1, cls);
    }

    private GameApp(int i2, Class<?> cls) {
        this.q = null;
        this.d = "";
        this.x = "this game";
        this.C = true;
        this.G = new Vector<>();
        this.H = -1;
        this.y = -1;
        this.A = cls;
    }

    public boolean initNovaSdk() {
        try {
            Class<?> cls = Class.forName("com.linecorp.nova.android.NovaNative");
            cls.getMethod("setActivity", Activity.class).invoke(cls, this);
            if (((Boolean) cls.getMethod("loadNativeModules", new Class[0]).invoke(cls, new Object[0])) == Boolean.TRUE) {
                return true;
            }
            return false;
        } catch (ClassNotFoundException e2) {
            "Nova sdk not initialised ClassNotFoundException: " + e2.getMessage();
            return false;
        } catch (IllegalAccessException e3) {
            "Nova sdk not initialised IllegalAccessException: " + e3.getMessage();
            return false;
        } catch (InvocationTargetException e4) {
            "Nova sdk not initialised InvocationTargetException: " + e4.getTargetException().getMessage();
            return false;
        } catch (NoSuchMethodException e5) {
            "Nova sdk not initialised NoSuchMethodException: " + e5.getMessage();
            return false;
        }
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        GoogleServiceClient googleServiceClient = this.D;
        if (googleServiceClient != null) {
            googleServiceClient.onActivityResult(i2, i3, intent);
        }
        PurchaseManager purchaseManager = this.e;
        if (purchaseManager != null && i2 == 10000004) {
            ((da) purchaseManager).a(i3, intent);
        }
        if (NativeFacebookManager.getInstance() != null) {
            NativeFacebookManager.getInstance().f4980a.onActivityResult(i2, i3, intent);
        }
        if (this.r) {
            nOnActivityResult(i2, i3, intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (NativeFacebookManager.getInstance() != null) {
            NativeFacebookManager.getInstance();
        }
    }

    private String b(String str) {
        int identifier = getResources().getIdentifier(str, "string", getPackageName());
        return identifier == 0 ? "" : getString(identifier);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        long j2;
        String str;
        boolean z2;
        super.onCreate(bundle);
        i = this;
        o = new UnbotifyManager();
        this.d = b("googleplay_clientid");
        if (this.y != -1) {
            this.x = getResources().getString(this.y);
        } else {
            String str2 = this.x;
            if (str2 == null || str2.isEmpty() || this.x.equals("this game")) {
                this.x = b(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING);
                if (this.x.isEmpty()) {
                    this.x = "this game";
                }
            }
        }
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            Fragment findFragmentByTag = fragmentManager.findFragmentByTag("NativeDialog");
            if (findFragmentByTag instanceof NativeDialogManager) {
                ((NativeDialogManager) findFragmentByTag).a();
                NativeDialogManager.f4976a = false;
            }
        }
        this.z = bundle;
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService("wifi");
        if (wifiManager != null) {
            this.j = wifiManager.createWifiLock(1, getPackageName());
            try {
                wifiManager.reconnect();
            } catch (Exception unused) {
            }
        }
        this.w = (AlarmManager) getSystemService(NotificationCompat.CATEGORY_ALARM);
        Window window = getWindow();
        window.setFlags(1024, 1024);
        if (Build.VERSION.SDK_INT >= 28) {
            window.getAttributes().layoutInDisplayCutoutMode = 1;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            window.setFlags(134217728, 134217728);
        }
        this.h = getRequestedOrientation();
        if ("6.0".equals(Build.VERSION.RELEASE) && this.h == 7) {
            setRequestedOrientation(6);
            setRequestedOrientation(this.h);
        }
        com.a.a.a.a a2 = com.a.a.a.a.a(this);
        a2.f1294b = false;
        a2.f1293a = false;
        Thread thread = new Thread(new com.a.a.a.b(a2, new k(this)));
        thread.start();
        this.n = thread;
        if (!this.s) {
            this.s = true;
            Context applicationContext = getApplicationContext();
            String str3 = applicationContext.getApplicationInfo().dataDir;
            String absolutePath = applicationContext.getCacheDir().getAbsolutePath();
            String b2 = b(this);
            if (b2 != null) {
                long d2 = d(b2);
                if (d2 <= 0) {
                    str = "";
                } else {
                    str = b2;
                }
                j2 = d2;
            } else {
                j2 = 0;
                str = "";
            }
            String string = Settings.Secure.getString(getContentResolver(), "android_id");
            if (string == null || string.isEmpty()) {
                string = getPackageName().toUpperCase(Locale.ENGLISH);
            }
            this.f = new SecurePreferences(this, "storage", string, true);
            String string2 = Settings.Secure.getString(applicationContext.getContentResolver(), "android_id");
            if (string2 == null || string2.isEmpty()) {
                string2 = string + "titan";
            }
            this.B = new SecurePreferences(this, "localPrefs", string2, true);
            e();
            try {
                System.loadLibrary("fmod");
                z2 = true;
            } catch (UnsatisfiedLinkError unused2) {
                z2 = false;
            }
            if (!z2) {
                try {
                    System.loadLibrary("fmodL");
                    z2 = true;
                } catch (UnsatisfiedLinkError unused3) {
                }
            }
            if (!z2) {
                a(applicationContext);
            } else {
                try {
                    System.loadLibrary("g");
                    this.r = true;
                    FMOD.init(this);
                    g();
                    NativeHTTPClientManager.getInstance();
                    NativeFacebookManager.createInstance(this);
                    HelpshiftTitan.getNotificationCount();
                    try {
                        OpenUDID_manager.a(this);
                    } catch (Exception e2) {
                        debuggerException(e2);
                    }
                    Display defaultDisplay = getWindowManager().getDefaultDisplay();
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    defaultDisplay.getMetrics(displayMetrics);
                    int i2 = displayMetrics.densityDpi;
                    Point point = new Point();
                    defaultDisplay.getSize(point);
                    this.e = new da(this, createGameMain(applicationContext.getAssets(), str3, absolutePath, str, j2, point.x, point.y, i2, 2, this));
                    this.f4966b = new i(applicationContext);
                    this.p = b(getIntent());
                    this.f4966b.setFocusable(true);
                    this.f4966b.setFocusableInTouchMode(true);
                    setContentView(this.f4966b);
                    a();
                    this.D = new GoogleServiceClient(this);
                    if (Build.VERSION.SDK_INT >= 28) {
                        this.f4966b.setOnApplyWindowInsetsListener(new l(this));
                    } else if (hasHuaweiNotchInScreen(getApplicationContext())) {
                        int i3 = getHuaweiNotchSize(getApplicationContext())[1];
                        int i4 = h() ? i3 : 0;
                        if (h()) {
                            i3 = 0;
                        }
                        setSafeMargins(i4, i4, i3, i3);
                    } else if (getApplicationContext().getPackageManager().hasSystemFeature("com.oppo.feature.screen.heteromorphism")) {
                        int i5 = 100;
                        int i6 = h() ? 100 : 0;
                        if (h()) {
                            i5 = 0;
                        }
                        setSafeMargins(i6, i6, i5, i5);
                    }
                    if (this.r) {
                        nOnCreate();
                    }
                } catch (UnsatisfiedLinkError unused4) {
                    a(applicationContext);
                }
            }
        }
        if (this.r) {
            handleDeeplinkURL(a(getIntent()));
        }
    }

    private int d() {
        try {
            for (Signature byteArray : getPackageManager().getPackageInfo(getPackageName(), 64).signatures) {
                MessageDigest instance = MessageDigest.getInstance("SHA");
                instance.update(byteArray.toByteArray());
                String encodeToString = Base64.encodeToString(instance.digest(), 2);
                if ("RWEg0wzahyAlW2DQMkx9FUMH9SU=".equals(encodeToString) || "LhjT+HJrHeYxMicWUY+yrsLr654=".equals(encodeToString) || "xWj3NbEpQjAUk4KDgJo23qjr06Q=".equals(encodeToString) || "HnxASw7gdJz5NmBsPsNM+dMoO+M=".equals(encodeToString) || "dMbXe1xYkjRdd7GNjy67sFY8ZEQ=".equals(encodeToString) || "gHww8K7HZ/6JahuUjUc5UYIWPxw=".equals(encodeToString) || "vnSakMlp3zat4BU5Zq+5+GaQxNs=".equals(encodeToString) || "KKyU591coRU1qKAWHNWOHCTcZCE=".equals(encodeToString) || "d1MWeErwW+CGhfyQn6H31+iyFNc=".equals(encodeToString) || "8FU+TGRw3LTOfWAxLrArYg==".equals(encodeToString) || "lxlqmPxo9M7RQV9WroxWKfCja/g=".equals(encodeToString)) {
                    return 0;
                }
            }
            return 1;
        } catch (Exception unused) {
            return 1;
        }
    }

    public static boolean isSignatureValid() {
        return getInstance() != null && getInstance().d() == 0;
    }

    public static void reloadApp() {
        GameApp instance = getInstance();
        instance.requestFinish();
        instance.startActivity(instance.getIntent());
    }

    public static boolean isEmulator() {
        try {
            getInstance().n.join();
        } catch (Exception unused) {
        }
        return getInstance().m;
    }

    public static void testDeviceIntegrity(byte[] bArr) {
        if (getInstance() != null) {
            getInstance();
        }
    }

    private void e() {
        int[] iArr = {102, 76, 120, 89, 66, 57, 77, 56, 52, 65, 98, 101, 117, 115, 69, 82, 77, 89, 57, 89, 70, 122, 86, 71};
        String packageName = getPackageName();
        int length = packageName.length();
        String str = "";
        while (true) {
            length--;
            if (length >= 0) {
                str = str + ((char) (((packageName.charAt(length) ^ iArr[length % 24]) & 31) + '0'));
            } else {
                this.g = new SecurePreferences(this, "storage_new", str, true);
                return;
            }
        }
    }

    private String f() {
        String b2 = b("sc_manifest_package_name");
        if (b2.isEmpty()) {
            return getPackageName() + ".BuildConfig";
        }
        return b2 + ".BuildConfig";
    }

    private Object c(String str) {
        try {
            Class<?> cls = Class.forName(f());
            return cls.getField(str).get(cls);
        } catch (Exception unused) {
            "Cannot find value for project BuildConfig key: " + str;
            return null;
        }
    }

    private void g() {
        try {
            Class<?> cls = Class.forName("com.supercell.titan.CrashReporter");
            String f2 = f();
            try {
                Class<?> cls2 = Class.forName(f2);
                String str = (String) cls2.getField("APPCENTER_APPSECRET").get(cls2);
                if (!"NOT SET".equals(str)) {
                    Object c2 = c("APPCENTER_MULTIPLECRASHHANDLERS");
                    boolean booleanValue = c2 != null ? ((Boolean) c2).booleanValue() : false;
                    "AppCenter: multiple handlers: " + booleanValue;
                    cls.getMethod("start", Application.class, String.class, String.class, Boolean.TYPE).invoke(null, getApplication(), str, a("AppCenterUserId"), Boolean.valueOf(booleanValue));
                    "AppCenter started with appSecret: " + str;
                }
            } catch (ClassNotFoundException unused) {
                "AppCenter cannot find BuildConfig class: " + f2;
            } catch (NoSuchFieldException e2) {
                "AppCenter can't find APPCENTER_APPSECRET key from BuildConfig (" + f2 + ") " + e2.getMessage();
            } catch (Exception e3) {
                "AppCenter Cannot start CrashReporter " + e3.getMessage();
            }
        } catch (ClassNotFoundException unused2) {
        }
    }

    private boolean h() {
        int orientation = ((WindowManager) getApplicationContext().getSystemService("window")).getDefaultDisplay().getOrientation();
        return orientation == 0 || orientation == 2;
    }

    public static int[] getHuaweiNotchSize(Context context) {
        int[] iArr = {0, 0};
        try {
            Class<?> loadClass = context.getClassLoader().loadClass("com.huawei.android.util.HwNotchSizeUtil");
            return (int[]) loadClass.getMethod("getNotchSize", new Class[0]).invoke(loadClass, new Object[0]);
        } catch (Exception unused) {
            return iArr;
        }
    }

    public static boolean hasHuaweiNotchInScreen(Context context) {
        try {
            Class<?> loadClass = context.getClassLoader().loadClass("com.huawei.android.util.HwNotchSizeUtil");
            return ((Boolean) loadClass.getMethod("hasNotchInScreen", new Class[0]).invoke(loadClass, new Object[0])).booleanValue();
        } catch (Exception unused) {
            return false;
        }
    }

    private static String a(Intent intent) {
        if (intent == null || intent.getData() == null) {
            return null;
        }
        return intent.getData().toString();
    }

    public final void a() {
        Bundle bundle;
        if (this.f4966b != null) {
            if (this.H == -1) {
                int i2 = Build.VERSION.SDK_INT;
                boolean z2 = false;
                if (i2 >= 19) {
                    if (i2 >= 22) {
                        z2 = true;
                    } else {
                        try {
                            Object systemService = getApplicationContext().getSystemService("user");
                            if (!(systemService == null || (bundle = (Bundle) systemService.getClass().getMethod("getUserRestrictions", new Class[0]).invoke(systemService, new Object[0])) == null)) {
                                z2 = bundle.isEmpty();
                            }
                        } catch (Exception unused) {
                        }
                    }
                }
                if (z2) {
                    this.H = 5894;
                } else {
                    this.H = 1024;
                }
            }
            this.f4966b.setSystemUiVisibility(this.H);
        }
    }

    private void a(Context context) {
        this.f4966b = new i(context);
        setContentView(this.f4966b);
        this.f4966b.post(new n(this));
    }

    public final void b() {
        try {
            new BackupManager(this).dataChanged();
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        if (this.f4966b != null && intent != null) {
            this.p = b(intent);
            String a2 = a(intent);
            if (a2 != null) {
                handleDeeplinkURL(a2);
            }
        }
    }

    private static String b(Intent intent) {
        Bundle extras;
        String string;
        return (intent == null || (extras = intent.getExtras()) == null || (string = extras.getString("userId")) == null) ? "" : string;
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        if (this.r) {
            nOnRestart();
        }
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.r) {
            this.c = false;
            if (this.F) {
                this.I = true;
            } else {
                this.I = false;
                this.F = false;
                a(false);
            }
            if (this.q == null && Build.VERSION.SDK_INT >= 23) {
                this.q = new p(this);
                ((DisplayManager) getSystemService(ServerProtocol.DIALOG_PARAM_DISPLAY)).registerDisplayListener(this.q, new Handler(Looper.getMainLooper()));
            }
            nOnStart();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.r) {
            isFinishing();
            if (NativeFacebookManager.getInstance() != null) {
                NativeFacebookManager.getInstance();
            }
            UnbotifyManager.onPause();
            this.k = true;
            nOnPause();
        }
    }

    public final void a(boolean z2) {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService("wifi");
        if (wifiManager != null) {
            try {
                wifiManager.reconnect();
                if (this.j != null && !this.j.isHeld()) {
                    this.j.acquire();
                }
            } catch (Exception unused) {
            }
        }
        if (!VirtualKeyboardHandler.f5002a) {
            VirtualKeyboardHandler.hideKeyboard();
        }
        this.D.onStart();
        NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        if (notificationManager != null) {
            try {
                notificationManager.cancelAll();
            } catch (Exception unused2) {
            }
        }
        AlarmManager alarmManager = i.w;
        synchronized (u) {
            for (int i2 = 0; i2 < u.size(); i2++) {
                alarmManager.cancel(u.get(i2).f4967a);
            }
        }
        if (NativeFacebookManager.getInstance() != null) {
            NativeFacebookManager.getInstance();
        }
        da daVar = (da) this.e;
        if (!daVar.q) {
            daVar.h();
        } else if (!daVar.p && daVar.e() > 0) {
            PurchaseManager.updateDetails();
        }
        this.e.f();
        HelpshiftTitan.onResume();
        if (this.f4966b != null) {
            start(this.p);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.r) {
            UnbotifyManager.onResume();
            if (NativeFacebookManager.getInstance() != null) {
                NativeFacebookManager.getInstance();
            }
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(findViewById(16908290).getWindowToken(), 0);
            nOnResume();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        boolean z2;
        this.c = true;
        GoogleServiceClient googleServiceClient = this.D;
        if (googleServiceClient != null) {
            googleServiceClient.onStop();
        }
        if (NativeFacebookManager.getInstance() != null) {
            NativeFacebookManager.getInstance();
        }
        WifiManager.WifiLock wifiLock = this.j;
        if (wifiLock != null && wifiLock.isHeld()) {
            this.j.release();
        }
        if (this.B != null) {
            long currentTimeMillis = System.currentTimeMillis();
            synchronized (u) {
                int size = u.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    } else if (currentTimeMillis >= v.get(size).longValue()) {
                        u.remove(size);
                        v.remove(size);
                    }
                }
            }
            AlarmManager alarmManager = i.w;
            JSONArray jSONArray = new JSONArray();
            synchronized (u) {
                z2 = false;
                for (int i2 = 0; i2 < u.size(); i2++) {
                    long longValue = v.get(i2).longValue();
                    if (currentTimeMillis < longValue) {
                        b bVar = u.get(i2);
                        alarmManager.set(0, longValue, bVar.f4967a);
                        JSONObject jSONObject = new JSONObject();
                        try {
                            jSONObject.putOpt("title", bVar.e);
                            jSONObject.putOpt("sound", bVar.f);
                            jSONObject.put(NotificationCompat.CATEGORY_MESSAGE, bVar.d);
                            jSONObject.put("userId", bVar.f4968b);
                            jSONObject.put("id", bVar.c);
                            jSONObject.put("imageURL", bVar.g);
                            jSONArray.put(jSONObject);
                            z2 = true;
                        } catch (Exception e2) {
                            debuggerException(e2);
                        }
                    }
                }
            }
            if (z2) {
                this.B.a("localNotifications", jSONArray.toString());
            } else {
                this.B.a("localNotifications");
            }
        }
        this.k = false;
        if (this.r) {
            nOnStop();
        }
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.r) {
            nOnDestroy();
            NativeFacebookManager.destructInstance();
            FMOD.close();
        }
        PurchaseManager purchaseManager = this.e;
        if (purchaseManager != null) {
            purchaseManager.a();
        }
        PushMessageService.onDestroy(this);
    }

    public void requestFinish() {
        if (!this.l) {
            this.l = true;
            finish();
        }
    }

    public void onBackPressed() {
        if (TitanWebView.isInCustomView()) {
            TitanWebView.hideCustomView();
        } else if (this.f4966b != null && !backButtonPressed()) {
            finish();
        }
    }

    public static int createNotification(String str, String str2, int i2, long j2, int i3, String str3, String str4, String str5, String str6, String str7, String str8) {
        int i4 = i2;
        long j3 = 0;
        if (j2 > 0) {
            j3 = j2 * 1000;
        } else if (i4 > 0) {
            j3 = System.currentTimeMillis() + (((long) i4) * 1000);
        }
        int i5 = t + 1;
        t = i5;
        cancelNotification(i5);
        b a2 = a(str, str2, str3, str8, i5, str4, str5, str6, str7, i3);
        long currentTimeMillis = System.currentTimeMillis();
        if (j3 <= currentTimeMillis) {
            String.valueOf(j3);
            i.w.set(0, currentTimeMillis + 1000, a2.f4967a);
            return 0;
        }
        synchronized (u) {
            u.add(a2);
            v.add(Long.valueOf(j3));
        }
        return i5;
    }

    private static b a(String str, String str2, String str3, String str4, int i2, String str5, String str6, String str7, String str8, int i3) {
        GameApp gameApp = i;
        Intent intent = new Intent(gameApp, gameApp.A);
        intent.putExtra(NotificationCompat.CATEGORY_MESSAGE, str2);
        intent.putExtra("id", i2);
        intent.putExtra("title", str3);
        intent.putExtra("sound", str4);
        intent.putExtra("userId", str);
        intent.putExtra("imageURL", str5);
        intent.putExtra("channelId", str6);
        intent.putExtra("channelName", str7);
        intent.putExtra("channelDesc", str8);
        intent.putExtra("color", i3);
        b bVar = new b((byte) 0);
        bVar.c = i2;
        bVar.d = str2;
        bVar.e = str3;
        bVar.f4968b = str;
        bVar.f = str4;
        bVar.h = str6;
        bVar.i = str7;
        bVar.j = str8;
        bVar.k = i3;
        bVar.f4967a = PendingIntent.getBroadcast(i, i2, intent, 1073741824);
        return bVar;
    }

    public static void cancelAllNotifications() {
        AlarmManager alarmManager = i.w;
        synchronized (u) {
            u.clear();
            v.clear();
        }
        String b2 = i.B.b("localNotifications");
        if (!b2.isEmpty()) {
            i.B.a("localNotifications");
            try {
                JSONArray jSONArray = new JSONArray(b2);
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i2);
                    String string = jSONObject.getString("userId");
                    String string2 = jSONObject.getString("sound");
                    alarmManager.cancel(a(string, jSONObject.getString(NotificationCompat.CATEGORY_MESSAGE), jSONObject.optString("title"), string2, jSONObject.getInt("id"), jSONObject.optString("imageURL"), jSONObject.optString("channelId"), jSONObject.optString("channelName"), jSONObject.optString("channelDesc"), jSONObject.optInt("color")).f4967a);
                }
            } catch (Exception e2) {
                debuggerException(e2);
            }
        }
    }

    public static void cancelNotification(int i2) {
        SecurePreferences securePreferences;
        synchronized (u) {
            int size = u.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                } else if (u.get(size).c == i2) {
                    u.remove(size);
                    v.remove(size);
                }
            }
        }
        GameApp gameApp = i;
        if (gameApp != null && (securePreferences = gameApp.B) != null) {
            String b2 = securePreferences.b("localNotifications");
            if (!b2.isEmpty()) {
                AlarmManager alarmManager = i.w;
                try {
                    JSONArray jSONArray = new JSONArray(b2);
                    JSONArray jSONArray2 = new JSONArray();
                    for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                        JSONObject jSONObject = jSONArray.getJSONObject(i3);
                        if (i2 == jSONObject.getInt("id")) {
                            String string = jSONObject.getString("userId");
                            String string2 = jSONObject.getString("sound");
                            String optString = jSONObject.optString("title");
                            String optString2 = jSONObject.optString("imageURL");
                            String optString3 = jSONObject.optString("channelId");
                            String optString4 = jSONObject.optString("channelName");
                            String optString5 = jSONObject.optString("channelDesc");
                            int optInt = jSONObject.optInt("color");
                            String string3 = jSONObject.getString(NotificationCompat.CATEGORY_MESSAGE);
                            if (alarmManager != null) {
                                alarmManager.cancel(a(string, string3, optString, string2, i2, optString2, optString3, optString4, optString5, optInt).f4967a);
                            }
                        } else {
                            jSONArray2.put(jSONObject);
                        }
                    }
                    int length = jSONArray2.length();
                    if (length == jSONArray.length()) {
                        return;
                    }
                    if (length == 0) {
                        i.B.a("localNotifications");
                    } else {
                        i.B.a("localNotifications", jSONArray2.toString());
                    }
                } catch (Exception e2) {
                    debuggerException(e2);
                }
            }
        }
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 == 25 || i2 == 24 || i2 == 164 || i2 == 4) {
            a();
            a(5000);
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (this.r) {
            nOnConfigurationChanged(configuration);
        }
        super.onConfigurationChanged(configuration);
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2) {
            c();
            a();
        }
        this.F = !z2;
        if (this.r) {
            nOnWindowFocusChanged(z2);
        }
    }

    public final void a(long j2) {
        new Handler().postDelayed(new q(this), j2);
    }

    public final void c() {
        if (this.F && this.I) {
            this.I = false;
            runOnUiThread(new s(this));
        }
    }

    public static void mobileAppTrackerStart(String str, String str2, boolean z2, String str3) {
        try {
            if (i.E == null) {
                MobileAppTracker.init(i.getApplicationContext(), str, str2);
                MobileAppTracker instance = MobileAppTracker.getInstance();
                i.E = instance;
                instance.setReferralSources(i);
                if (!str3.isEmpty()) {
                    instance.setUserId(str3);
                }
                new Thread(new al(i, instance)).start();
            }
            if (i.E != null) {
                i.E.setExistingUser(z2);
                i.E.measureSession();
            }
        } catch (Exception e2) {
            debuggerException(e2);
        }
    }

    public static void debuggerWarning(String str) {
        if (str != null) {
            i.G.add(str);
        }
    }

    public static void debuggerException(Exception exc) {
        if (exc != null && i != null) {
            StringWriter stringWriter = new StringWriter();
            exc.printStackTrace(new PrintWriter(stringWriter));
            i.G.add(stringWriter.toString());
        }
    }

    public final void a(Runnable runnable) {
        if (runnable != null) {
            i iVar = this.f4966b;
            if (iVar != null) {
                iVar.post(runnable);
            } else if (this.r) {
                logDebuggerException("runOnView when view = null, " + runnable.toString());
            }
        }
    }

    private static long d(String str) {
        try {
            StatFs statFs = new StatFs(str);
            if (Build.VERSION.SDK_INT >= 18) {
                return statFs.getAvailableBytes();
            }
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Exception unused) {
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0034 A[Catch:{ Exception -> 0x004f }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0057 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String b(android.content.Context r10) {
        /*
            java.io.File r0 = r10.getCacheDir()
            r1 = 0
            r3 = 0
            if (r0 == 0) goto L_0x001d
            boolean r4 = r0.canWrite()
            if (r4 == 0) goto L_0x001d
            java.lang.String r4 = r0.toString()
            long r4 = d(r4)
            int r6 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r6 <= 0) goto L_0x001d
            r1 = r4
            goto L_0x001e
        L_0x001d:
            r0 = r3
        L_0x001e:
            if (r0 == 0) goto L_0x002c
            r4 = 1073741824(0x40000000, double:5.304989477E-315)
            int r6 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r6 < 0) goto L_0x002c
            java.lang.String r10 = r0.toString()
            return r10
        L_0x002c:
            java.io.File[] r10 = android.support.v4.content.ContextCompat.getExternalCacheDirs(r10)     // Catch:{ Exception -> 0x004f }
            int r4 = r10.length     // Catch:{ Exception -> 0x004f }
            r5 = 0
        L_0x0032:
            if (r5 >= r4) goto L_0x0050
            r6 = r10[r5]     // Catch:{ Exception -> 0x004f }
            if (r6 == 0) goto L_0x004c
            boolean r7 = r6.canWrite()     // Catch:{ Exception -> 0x004f }
            if (r7 == 0) goto L_0x004c
            java.lang.String r7 = r6.toString()     // Catch:{ Exception -> 0x004f }
            long r7 = d(r7)     // Catch:{ Exception -> 0x004f }
            int r9 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r9 <= 0) goto L_0x004c
            r0 = r6
            r1 = r7
        L_0x004c:
            int r5 = r5 + 1
            goto L_0x0032
        L_0x004f:
        L_0x0050:
            if (r0 == 0) goto L_0x0057
            java.lang.String r10 = r0.toString()
            return r10
        L_0x0057:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.titan.GameApp.b(android.content.Context):java.lang.String");
    }

    static boolean a(String str, String str2, SecurePreferences securePreferences) {
        if (securePreferences.b(str).equals(str2)) {
            return false;
        }
        if (str2.isEmpty()) {
            securePreferences.a(str);
        } else {
            securePreferences.a(str, str2);
        }
        return true;
    }

    public final String a(String str) {
        String b2 = this.g.b(str);
        if (!b2.isEmpty()) {
            return b2;
        }
        String b3 = this.f.b(str);
        if (!b3.isEmpty()) {
            this.g.a(str, b3);
            b();
        }
        return b3;
    }

    public static void vibrateDevice() {
        try {
            Vibrator vibrator = (Vibrator) i.getSystemService("vibrator");
            if (vibrator == null) {
                return;
            }
            if (vibrator.hasVibrator()) {
                vibrator.vibrate(400);
            }
        } catch (Exception unused) {
        }
    }

    public final void a(a aVar) {
        Vector<a> vector = this.f4965a;
        if (vector != null && aVar != null) {
            vector.remove(aVar);
        }
    }

    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i2, strArr, iArr);
        Vector<a> vector = this.f4965a;
        if (vector != null) {
            Iterator<a> it = vector.iterator();
            while (it.hasNext()) {
                it.next().onRequestPermissionsResult(i2, strArr, iArr);
            }
        }
    }

    public static boolean isPlayingUserMusic() {
        AudioManager audioManager;
        if (getInstance() == null || (audioManager = (AudioManager) getInstance().getSystemService("audio")) == null) {
            return false;
        }
        return audioManager.isMusicActive();
    }

    public static void queuePushNotificationValueUpdate(int i2, String str, String str2) {
        if (getInstance() != null && !getInstance().c) {
            getInstance().a(new t(i2, str, str2));
        }
    }

    public static String getAPKPath() {
        if (getInstance() != null) {
            return getInstance().getApplicationInfo().sourceDir;
        }
        return null;
    }

    public static void hapticFeedback(int i2) {
        if (getInstance() != null) {
            getInstance().runOnUiThread(new u(i2));
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        UnbotifyManager.dispatchTouchEvent(motionEvent);
        return super.dispatchTouchEvent(motionEvent);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        UnbotifyManager.dispatchKeyEvent(keyEvent);
        return super.dispatchKeyEvent(keyEvent);
    }

    public static Display.Mode[] getDisplayModes() {
        if (Build.VERSION.SDK_INT >= 23) {
            return getInstance().getWindowManager().getDefaultDisplay().getSupportedModes();
        }
        return null;
    }

    public static void setDisplayMode(int i2) {
        "Setting mode: " + i2;
        if (Build.VERSION.SDK_INT >= 23) {
            getInstance().runOnUiThread(new j(i2));
        }
    }

    public void beforeLogicCallback() {
        this.e.c();
        NativeHTTPClientManager.getInstance();
        NativeHTTPClientManager.updateBeforeFrame();
    }
}
