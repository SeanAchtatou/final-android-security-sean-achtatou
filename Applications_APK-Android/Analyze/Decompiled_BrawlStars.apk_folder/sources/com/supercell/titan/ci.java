package com.supercell.titan;

import com.facebook.FacebookException;
import com.facebook.GraphRequest;

/* compiled from: NativeFacebookManager */
class ci implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GraphRequest f5076a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ NativeFacebookManager f5077b;

    ci(NativeFacebookManager nativeFacebookManager, GraphRequest graphRequest) {
        this.f5077b = nativeFacebookManager;
        this.f5076a = graphRequest;
    }

    public void run() {
        try {
            GraphRequest.executeBatchAsync(this.f5076a);
        } catch (FacebookException e) {
            GameApp.debuggerException(e);
        } catch (IllegalStateException e2) {
            GameApp.debuggerException(e2);
        }
    }
}
