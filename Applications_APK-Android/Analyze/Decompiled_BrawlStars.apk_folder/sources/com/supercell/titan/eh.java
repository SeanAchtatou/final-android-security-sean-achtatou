package com.supercell.titan;

import android.webkit.WebView;

/* compiled from: TitanWebView */
class eh implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5158a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ TitanWebView f5159b;

    eh(TitanWebView titanWebView, String str) {
        this.f5159b = titanWebView;
        this.f5158a = str;
    }

    public void run() {
        WebView c = this.f5159b.f4995b;
        c.loadUrl("javascript:" + this.f5158a);
    }
}
