package com.supercell.titan;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.view.InputDeviceCompat;
import android.util.DisplayMetrics;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Locale;
import java.util.concurrent.FutureTask;
import org.OpenUDID.OpenUDID_manager;

public class ApplicationUtilBase {

    /* renamed from: a  reason: collision with root package name */
    private static int f4964a = -267522035;

    public static void changeKunlunAccount() {
    }

    public static String getIMEI() {
        return "";
    }

    public static String getKunlunSSO() {
        return "";
    }

    public static String getKunlunUID() {
        return "";
    }

    public static boolean isAmazonDeviceMessagingSupported() {
        return false;
    }

    public static void setKunlunPlayerInfo(String str, int i, int i2, String str2, boolean z, boolean z2, boolean z3) {
    }

    public static void showKunlunExitScreen() {
    }

    public static boolean canOpenURL(String str) {
        PackageManager packageManager = GameApp.getInstance().getPackageManager();
        if (str.startsWith("fb://")) {
            try {
                if (packageManager.getPackageInfo("com.facebook.katana", 0).versionCode >= 3002850) {
                    return true;
                }
                return false;
            } catch (PackageManager.NameNotFoundException unused) {
                return false;
            }
        } else if (packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(str)), 0).size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void openURL(String str) {
        GameApp instance = GameApp.getInstance();
        if (str.startsWith("https://") || str.startsWith("http://") || str.startsWith("hockeyapp://") || str.startsWith("market://") || str.startsWith("line://")) {
            try {
                instance.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str.replace("market://play.google.com/store/apps/details?id=", "market://details?id="))));
            } catch (ActivityNotFoundException e) {
                GameApp.debuggerException(e);
            }
        } else if (str.startsWith("mailto:")) {
            try {
                instance.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            } catch (ActivityNotFoundException unused) {
                Uri parse = Uri.parse(str);
                Intent intent = new Intent("android.intent.action.SENDTO");
                intent.setData(parse);
                instance.startActivity(Intent.createChooser(intent, "Send email"));
            }
        } else if (str.startsWith("settings://")) {
            try {
                instance.startActivity(new Intent(str.substring(str.indexOf("settings://") + 11)));
            } catch (ActivityNotFoundException e2) {
                GameApp.debuggerException(e2);
            }
        } else if (str.startsWith("fb://")) {
            instance.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } else {
            try {
                instance.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://" + str)));
            } catch (ActivityNotFoundException e3) {
                GameApp.debuggerException(e3);
            }
        }
    }

    public static void copyString(String str) {
        GameApp instance = GameApp.getInstance();
        instance.runOnUiThread(new b(instance, str));
    }

    public static String pasteString() {
        GameApp instance = GameApp.getInstance();
        FutureTask futureTask = new FutureTask(new c(instance));
        instance.runOnUiThread(futureTask);
        try {
            return (String) futureTask.get();
        } catch (Exception unused) {
            return "";
        }
    }

    public static void openMarketURL() {
        GameApp instance = GameApp.getInstance();
        try {
            GameApp.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + instance.getPackageName())));
        } catch (ActivityNotFoundException e) {
            GameApp.debuggerException(e);
        }
    }

    public static String getPreferredLanguage() {
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (country == null || country.isEmpty()) {
            return language;
        }
        return language + "-" + country;
    }

    public static String getBundleID() {
        return GameApp.getInstance().getPackageName();
    }

    public static int getTotalMemory() {
        if (f4964a == -267522035) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader("/proc/meminfo"), 8192);
                f4964a = Integer.parseInt(bufferedReader.readLine().split("\\s+")[1]) >> 10;
                bufferedReader.close();
            } catch (Exception e) {
                f4964a = 0;
                GameApp.debuggerException(e);
            }
        }
        return f4964a;
    }

    public static String getKeyValue(String str) {
        return GameApp.getInstance().a(str);
    }

    public static void removeKeyValue(String str) {
        storeKeyValue(str, "");
    }

    public static void storeKeyValue(String str, String str2) {
        if (str2 == null) {
            str2 = "";
        }
        GameApp instance = GameApp.getInstance();
        if (str != null && instance != null && instance.f != null) {
            if (GameApp.a(str, str2, instance.f) || (GameApp.a(str, str2, instance.g) | false)) {
                instance.b();
            }
        }
    }

    public static String getOpenUDID() {
        String a2;
        if (!OpenUDID_manager.b() || (a2 = OpenUDID_manager.a()) == null) {
            return "";
        }
        return a2;
    }

    public static String getAndroidID() {
        return Settings.Secure.getString(GameApp.getInstance().getContentResolver(), "android_id");
    }

    public static void setKeepScreenOn(boolean z) {
        GameApp instance = GameApp.getInstance();
        i iVar = instance.f4966b;
        if (iVar != null) {
            instance.runOnUiThread(new d(iVar, z));
        }
    }

    public static String getAppVersion() {
        GameApp instance = GameApp.getInstance();
        try {
            return instance.getPackageManager().getPackageInfo(instance.getPackageName(), 64).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            GameApp.debuggerException(e);
            return "";
        }
    }

    public static String getPlatformDetail(int i) {
        String str;
        GameApp instance = GameApp.getInstance();
        switch (i) {
            case 256:
                str = Build.BOARD;
                break;
            case InputDeviceCompat.SOURCE_KEYBOARD:
                str = Build.BOOTLOADER;
                break;
            case 258:
                str = Build.BRAND;
                break;
            case 259:
                str = Build.CPU_ABI;
                break;
            case 260:
                str = Build.CPU_ABI2;
                break;
            case 261:
                str = Build.DEVICE;
                break;
            case 262:
                str = Build.DISPLAY;
                break;
            case 263:
                str = Build.FINGERPRINT;
                break;
            case 264:
                str = Build.HARDWARE;
                break;
            case 265:
                str = Build.MANUFACTURER;
                break;
            case 266:
                str = Build.MODEL;
                break;
            case 267:
                str = Build.PRODUCT;
                break;
            case 268:
                str = Build.TAGS;
                break;
            case 269:
                str = Build.TYPE;
                break;
            case 270:
                str = Build.VERSION.RELEASE;
                break;
            case 271:
                DisplayMetrics displayMetrics = new DisplayMetrics();
                instance.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                str = String.valueOf(displayMetrics.xdpi);
                break;
            case 272:
                DisplayMetrics displayMetrics2 = new DisplayMetrics();
                instance.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics2);
                str = String.valueOf(displayMetrics2.ydpi);
                break;
            case 273:
                DisplayMetrics displayMetrics3 = new DisplayMetrics();
                instance.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics3);
                str = String.valueOf(displayMetrics3.density);
                break;
            case 274:
                str = String.valueOf(getTotalMemory() * 1024 * 1024);
                break;
            default:
                str = null;
                break;
        }
        return str == null ? "" : str;
    }

    public static boolean isLowLatencyDevice() {
        if (GameApp.getInstance() == null) {
            return false;
        }
        boolean hasSystemFeature = GameApp.getInstance().getPackageManager().hasSystemFeature("android.hardware.audio.low_latency");
        boolean hasSystemFeature2 = GameApp.getInstance().getPackageManager().hasSystemFeature("android.hardware.audio.pro");
        if (hasSystemFeature || hasSystemFeature2) {
            return true;
        }
        return false;
    }
}
