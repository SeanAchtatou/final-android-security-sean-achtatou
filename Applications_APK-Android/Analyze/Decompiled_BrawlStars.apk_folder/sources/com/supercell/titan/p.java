package com.supercell.titan;

import android.hardware.display.DisplayManager;

/* compiled from: GameApp */
class p implements DisplayManager.DisplayListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5196a;

    p(GameApp gameApp) {
        this.f5196a = gameApp;
    }

    public void onDisplayAdded(int i) {
        GameApp.nOnDisplayAdded(i);
    }

    public void onDisplayRemoved(int i) {
        GameApp.nOnDisplayRemoved(i);
    }

    public void onDisplayChanged(int i) {
        GameApp.nOnDisplayChanged(i);
    }
}
