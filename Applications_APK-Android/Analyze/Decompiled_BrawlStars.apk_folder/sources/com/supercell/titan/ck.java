package com.supercell.titan;

import android.widget.Toast;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.share.Sharer;

/* compiled from: NativeFacebookManager */
class ck implements FacebookCallback<Sharer.Result> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NativeFacebookManager f5080a;

    ck(NativeFacebookManager nativeFacebookManager) {
        this.f5080a = nativeFacebookManager;
    }

    public /* synthetic */ void onSuccess(Object obj) {
        if (((Sharer.Result) obj).getPostId() != null) {
            Toast.makeText(this.f5080a.f4981b, "Posted story", 0).show();
        }
    }

    public void onCancel() {
        Toast.makeText(this.f5080a.f4981b.getApplicationContext(), "Publish cancelled", 0).show();
    }

    public void onError(FacebookException facebookException) {
        if (facebookException instanceof FacebookOperationCanceledException) {
            Toast.makeText(this.f5080a.f4981b.getApplicationContext(), "Publish cancelled", 0).show();
        } else {
            Toast.makeText(this.f5080a.f4981b.getApplicationContext(), "Error posting story", 0).show();
        }
    }
}
