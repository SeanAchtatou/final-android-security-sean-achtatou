package com.supercell.titan;

import android.text.Editable;

/* compiled from: KeyboardDialog */
final class bj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5047a;

    bj(String str) {
        this.f5047a = str;
    }

    public final void run() {
        if (bc.j == null || bc.j.d == null) {
            String unused = bc.g = this.f5047a;
            return;
        }
        dt b2 = bc.j.d;
        String str = this.f5047a;
        b2.f5136a = true;
        Editable editableText = b2.getEditableText();
        editableText.replace(0, editableText.length(), str);
        b2.f5136a = false;
    }
}
