package com.supercell.titan;

import android.location.Criteria;
import android.location.Location;

/* compiled from: LocationService */
class bm implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocationService f5050a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ LocationService f5051b;

    bm(LocationService locationService, LocationService locationService2) {
        this.f5051b = locationService;
        this.f5050a = locationService2;
    }

    public void run() {
        try {
            Location d = LocationService.d(this.f5051b);
            if (d != null) {
                "Sending last known location: " + d;
                this.f5051b.onLocationChanged(d);
            }
            Criteria criteria = new Criteria();
            criteria.setAccuracy(1);
            String unused = this.f5051b.f = this.f5051b.c.getBestProvider(criteria, true);
            if (this.f5051b.f != null) {
                this.f5051b.c.requestLocationUpdates(this.f5051b.f, 1000, 10.0f, this.f5050a);
            }
        } catch (SecurityException unused2) {
        }
    }
}
