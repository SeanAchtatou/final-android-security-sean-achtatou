package com.supercell.titan;

import com.facebook.FacebookException;
import com.facebook.GraphRequest;

/* compiled from: NativeFacebookManager */
class cj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GraphRequest f5078a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ NativeFacebookManager f5079b;

    cj(NativeFacebookManager nativeFacebookManager, GraphRequest graphRequest) {
        this.f5079b = nativeFacebookManager;
        this.f5078a = graphRequest;
    }

    public void run() {
        try {
            this.f5078a.executeAsync();
        } catch (FacebookException e) {
            GameApp.debuggerException(e);
        } catch (IllegalStateException e2) {
            GameApp.debuggerException(e2);
        } catch (Exception e3) {
            GameApp.debuggerException(e3);
        }
    }
}
