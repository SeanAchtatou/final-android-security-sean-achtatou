package com.supercell.titan;

import android.app.AlertDialog;

/* compiled from: GameApp */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5194a;

    n(GameApp gameApp) {
        this.f5194a = gameApp;
    }

    public void run() {
        o oVar = new o(this);
        String replace = this.f5194a.getResources().getString(R.string.UnsatisfiedLinkErrorReinstallPrompt).replace("{GAME}", this.f5194a.x);
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f5194a);
        builder.setMessage(replace);
        builder.setPositiveButton("OK", oVar);
        builder.setNegativeButton("Uninstall", oVar);
        builder.setCancelable(false);
        builder.create().show();
    }
}
