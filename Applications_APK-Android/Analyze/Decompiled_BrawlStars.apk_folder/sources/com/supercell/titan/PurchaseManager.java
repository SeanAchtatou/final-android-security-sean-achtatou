package com.supercell.titan;

import android.content.Intent;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public abstract class PurchaseManager {
    private static PurchaseManager p;

    /* renamed from: a  reason: collision with root package name */
    protected final Vector<String> f4984a = new Vector<>();

    /* renamed from: b  reason: collision with root package name */
    protected final Vector<b> f4985b = new Vector<>();
    protected final Vector<a> c = new Vector<>();
    protected final Vector<String> d = new Vector<>();
    protected String e = "";
    protected String f = "";
    protected int g = -1;
    protected final Vector<String> h = new Vector<>();
    protected final Map<String, String> i = new HashMap();
    protected final GameApp j;
    protected int k;
    protected final Vector<b> l = new Vector<>();
    protected boolean m;
    protected String n = "";
    protected boolean o;
    private boolean q = true;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public String f4986a = "";

        /* renamed from: b  reason: collision with root package name */
        public String f4987b = "";
        public int c;
    }

    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public String f4988a = "";

        /* renamed from: b  reason: collision with root package name */
        public String f4989b = "";
        public String c = "";
        public String d = "";
        public String e = "";
        public boolean f = true;
    }

    public static void addProduct(String str, String str2, int i2, int i3, String str3) {
    }

    public static native void billingKunlunPurchaseWindowClosed(String str, String str2, int i2);

    public static native void billingProductBought(String str, String str2, String str3, String str4, String str5, boolean z);

    public static native void billingProductCanceled(String str);

    public static native void billingProductFailed(String str, String str2, int i2);

    public static native void billingSetMarketplace(String str);

    public static native void sendPurchasingEvent(String str);

    public static native void updateBillingProductDetails(String str, String str2, int i2);

    public void a(int i2, Intent intent) {
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public abstract String c(String str);

    /* access modifiers changed from: protected */
    public abstract void d(String str);

    /* access modifiers changed from: protected */
    public abstract boolean d();

    /* access modifiers changed from: protected */
    public abstract void e(String str);

    public void f() {
    }

    public void g() {
    }

    protected PurchaseManager(GameApp gameApp) {
        this.j = gameApp;
        p = this;
    }

    public void a() {
        p = null;
    }

    public static void cppBillingCreated() {
        p.m = true;
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }
        if (this.h.isEmpty() || this.h.contains(str)) {
            return true;
        }
        return false;
    }

    public static void cppBillingDestroyed() {
        p.m = false;
    }

    public static void addSku(String str, boolean z) {
        if (!p.h.contains(str)) {
            p.h.add(str);
            if (z) {
                p.i.put(str, "subs");
            }
        }
    }

    public final String b(String str) {
        String str2 = this.i.get(str);
        return str2 == null ? "inapp" : str2;
    }

    public static String getProductDetails(String str) {
        return p.c(str);
    }

    public static void updateDetails() {
        p.b();
    }

    public static void buyProduct(String str) {
        p.d(str);
    }

    public final void c() {
        int size = this.f4984a.size();
        for (int i2 = 0; i2 < size; i2++) {
            sendPurchasingEvent(this.f4984a.remove(0));
        }
        if (this.m) {
            if (this.o) {
                this.o = false;
                billingSetMarketplace(this.n);
            }
            if (this.e == null) {
                this.e = "";
            }
            if (this.f == null) {
                this.f = "";
            }
            if (!this.e.isEmpty() || !this.f.isEmpty()) {
                if (this.f.isEmpty()) {
                    updateBillingProductDetails(this.e, "", this.g);
                } else {
                    updateBillingProductDetails("", "", this.g);
                }
                this.e = "";
                this.f = "";
                this.g = -1;
            }
            if (this.q) {
                int size2 = this.f4985b.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    b remove = this.f4985b.remove(0);
                    if (remove != null) {
                        billingProductBought(remove.c, remove.f4989b, remove.f4988a, remove.d, remove.e, remove.f);
                        int i4 = 0;
                        while (true) {
                            if (i4 >= this.l.size()) {
                                break;
                            } else if (this.l.get(i4).c.equals(remove.c)) {
                                this.l.remove(i4);
                                break;
                            } else {
                                i4++;
                            }
                        }
                        if (this.l.size() >= 100) {
                            this.l.remove(0);
                        }
                        int i5 = 0;
                        while (true) {
                            if (i5 >= this.l.size()) {
                                break;
                            } else if (this.l.get(i5).c.equals(remove.c)) {
                                this.l.remove(i5);
                                break;
                            } else {
                                i5++;
                            }
                        }
                        this.l.add(remove);
                    }
                }
                int size3 = this.c.size();
                for (int i6 = 0; i6 < size3; i6++) {
                    a remove2 = this.c.remove(0);
                    if (remove2 != null) {
                        billingProductFailed(remove2.f4986a, remove2.f4987b, remove2.c);
                    }
                }
                int size4 = this.d.size();
                for (int i7 = 0; i7 < size4; i7++) {
                    billingProductCanceled(this.d.remove(0));
                }
            }
        }
    }

    public static boolean isWaitingForTransaction() {
        boolean z;
        synchronized (p) {
            z = p.k > 0;
        }
        return (!p.l.isEmpty()) | z;
    }

    public static boolean arePaymentsAvailable() {
        return p.d();
    }

    public static void stopBillingTransactions() {
        p.q = false;
    }

    public static void startBillingTransactions() {
        PurchaseManager purchaseManager = p;
        if (!purchaseManager.q) {
            purchaseManager.q = true;
            purchaseManager.f4985b.addAll(purchaseManager.l);
            p.l.clear();
        }
    }

    public static void finishTransaction(String str) {
        p.e(str);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        "PurchaseManager.removePendingTransaction(" + i2 + ")";
        this.l.remove(i2);
    }

    public final int e() {
        return this.h.size();
    }

    public static void setAccountId(String str, String str2) {
        p.a(str, str2);
    }
}
