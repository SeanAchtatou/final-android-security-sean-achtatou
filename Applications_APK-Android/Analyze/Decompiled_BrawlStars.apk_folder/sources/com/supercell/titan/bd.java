package com.supercell.titan;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: KeyboardDialog */
class bd implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bc f5039a;

    bd(bc bcVar) {
        this.f5039a = bcVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        motionEvent.setLocation(motionEvent.getRawX(), motionEvent.getRawY());
        return this.f5039a.f5037a.f4966b.dispatchTouchEvent(motionEvent);
    }
}
