package com.supercell.titan;

import android.content.Intent;
import android.content.IntentSender;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.e;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.games.a;
import java.util.List;

public class GoogleServiceClient implements d.b, d.c {
    /* access modifiers changed from: private */
    public static boolean i;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public GameApp f4969a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public d f4970b;
    private volatile boolean c;
    private volatile boolean d;
    private volatile String e = "";
    private volatile String f = "";
    /* access modifiers changed from: private */
    public volatile String g = "";
    /* access modifiers changed from: private */
    public volatile boolean h = false;
    private Object j;
    /* access modifiers changed from: private */
    public boolean k;

    public static native void onSignIn();

    public static native void onSignInCanceled();

    public static native void onSignInFailed();

    public static native void onSignOut();

    public static native void updateNativeInstance(GoogleServiceClient googleServiceClient);

    public GoogleServiceClient(GameApp gameApp) {
        this.f4969a = gameApp;
        d.a aVar = new d.a(gameApp);
        a<a.C0117a> aVar2 = com.google.android.gms.games.a.d;
        l.a(aVar2, "Api must not be null");
        aVar.d.put(aVar2, null);
        List<Scope> b2 = aVar2.f1371a.b();
        aVar.f1377b.addAll(b2);
        aVar.f1376a.addAll(b2);
        l.a(this, "Listener must not be null");
        aVar.e.add(this);
        l.a(this, "Listener must not be null");
        aVar.f.add(this);
        i iVar = gameApp.f4966b;
        l.a(iVar, "View must not be null");
        aVar.c = iVar;
        this.f4970b = aVar.b();
        updateNativeInstance(this);
    }

    public void onStart() {
        int a2 = e.a(this.f4969a.getApplicationContext());
        if (a2 == 0) {
            this.d = true;
            connect();
        } else if (a2 == 1 || a2 == 2 || a2 == 3) {
            this.d = false;
        }
    }

    public void onStop() {
        try {
            this.f4970b.c();
        } catch (Exception e2) {
            GameApp.debuggerException(e2);
        }
    }

    public void connect() {
        if (!this.f4970b.f()) {
            try {
                if (this.f4970b.e()) {
                    b();
                } else {
                    this.f4970b.b();
                }
            } catch (Exception e2) {
                GameApp.debuggerException(e2);
            }
        }
    }

    public boolean isAvailable() {
        return this.d;
    }

    public boolean isSignedIn() {
        return this.c;
    }

    public void signIn(boolean z) {
        int a2 = e.a(this.f4969a.getApplicationContext());
        if (a2 == 0) {
            this.h = z;
            connect();
        } else if (z) {
            e.a(a2, this.f4969a, 0).show();
        }
    }

    public void signOut() {
        if (this.f4970b.e()) {
            try {
                com.google.android.gms.games.a.b(this.f4970b).a(new v(this));
            } catch (Exception e2) {
                GameApp.debuggerException(e2);
            }
        }
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1100043 && i3 == 10001) {
            c();
        } else if (i2 == 1100042) {
            if (i3 == -1) {
                onStart();
            } else if (i3 != 0) {
                switch (i3) {
                    case 10001:
                        this.f4969a.a(new ac(this));
                        return;
                    case 10002:
                        this.f4969a.a(new ab(this));
                        return;
                    case 10003:
                        this.f4969a.a(new ae(this));
                        return;
                    case 10004:
                        this.f4969a.a(new ad(this));
                        return;
                    default:
                        return;
                }
            } else {
                this.f4969a.a(new aa(this));
            }
        }
    }

    public String getAuthCode() {
        return this.g;
    }

    public String getPlayerId() {
        return this.e;
    }

    public String getPlayerDisplayName() {
        return this.f;
    }

    public void onConnectionSuspended(int i2) {
        this.c = false;
    }

    public void onConnected(Bundle bundle) {
        this.c = true;
        try {
            this.e = com.google.android.gms.games.a.m.a(this.f4970b);
            this.f = com.google.android.gms.games.a.m.b(this.f4970b).c();
        } catch (Exception e2) {
            GameApp.debuggerException(e2);
        }
        b();
        if (Build.VERSION.SDK_INT < 21) {
            i = false;
        } else {
            new af(this).execute(new Void[0]);
        }
    }

    private void b() {
        new ag(this).execute(new Void[0]);
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (this.c) {
            this.f4969a.a(new aj(this));
        }
        this.c = false;
        if (connectionResult.a() && this.h) {
            try {
                GameApp gameApp = this.f4969a;
                if (connectionResult.a()) {
                    gameApp.startIntentSenderForResult(connectionResult.c.getIntentSender(), 1100042, null, 0, 0, 0);
                }
            } catch (IntentSender.SendIntentException e2) {
                GameApp.debuggerException(e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.c = false;
        this.e = "";
        this.f = "";
        this.g = "";
        try {
            this.f4970b.c();
        } catch (Exception e2) {
            GameApp.debuggerException(e2);
        }
        this.f4969a.a(new w(this));
    }

    public void forNative_signIn(boolean z) {
        this.f4969a.runOnUiThread(new x(this, this, z));
    }

    public void forNative_signOut() {
        this.f4969a.runOnUiThread(new y(this, this));
    }

    public void unlockAchievement(String str) {
        try {
            if (this.f4970b.e()) {
                com.google.android.gms.games.a.g.a(this.f4970b, str);
            } else if (this.c) {
                connect();
            }
        } catch (Exception e2) {
            GameApp.debuggerException(e2);
        }
    }

    public void showAchievements() {
        try {
            if (isSignedIn() && this.f4970b.e()) {
                this.f4969a.startActivityForResult(com.google.android.gms.games.a.g.a(this.f4970b), 1100043);
            }
        } catch (SecurityException e2) {
            GameApp.debuggerException(e2);
            this.f4970b.d();
        } catch (Exception e3) {
            GameApp.debuggerException(e3);
        }
    }

    public boolean isScreenRecordingAvailable() {
        return i;
    }

    public boolean isRecordingControlsVisible() {
        return i && this.k;
    }

    public void showVideoRecordingControls() {
        if (isScreenRecordingAvailable()) {
            if (this.f4970b.e()) {
                Object obj = this.j;
                if (obj == null && obj == null) {
                    try {
                        if (Build.VERSION.SDK_INT >= 21) {
                            this.j = this.f4969a.getSystemService("camera");
                            ((CameraManager) this.j).registerAvailabilityCallback(new z(this), new Handler(Looper.getMainLooper()));
                        }
                    } catch (Exception e2) {
                        GameApp.debuggerException(e2);
                    }
                }
                this.f4969a.startActivityForResult(com.google.android.gms.games.a.s.a(this.f4970b), 1100044);
                return;
            }
            this.f4970b.d();
        }
    }
}
