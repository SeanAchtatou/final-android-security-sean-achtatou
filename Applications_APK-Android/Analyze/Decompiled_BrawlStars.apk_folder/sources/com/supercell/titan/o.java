package com.supercell.titan;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

/* compiled from: GameApp */
class o implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f5195a;

    o(n nVar) {
        this.f5195a = nVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == -2 && Build.VERSION.SDK_INT >= 9) {
            Intent intent = new Intent();
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", GameApp.getInstance().getPackageName(), null));
            this.f5195a.f5194a.startActivity(intent);
        }
        this.f5195a.f5194a.finish();
    }
}
