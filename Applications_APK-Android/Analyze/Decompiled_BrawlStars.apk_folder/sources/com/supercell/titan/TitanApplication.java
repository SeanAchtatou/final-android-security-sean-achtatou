package com.supercell.titan;

import android.app.Application;

public class TitanApplication extends Application {
    public void onCreate() {
        super.onCreate();
        BranchIntegration.ApplicationInit(false, this);
    }
}
