package com.supercell.titan;

import android.net.Uri;
import io.branch.referral.Branch;

public class BranchIntegration {
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void ApplicationInit(boolean r2, android.content.Context r3) {
        /*
            java.lang.String r0 = "io.branch.referral.Branch"
            java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            if (r3 != 0) goto L_0x000f
            com.supercell.titan.GameApp r3 = com.supercell.titan.GameApp.getInstance()     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            android.app.Application r3 = r3.getApplication()     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
        L_0x000f:
            if (r3 != 0) goto L_0x0012
            return
        L_0x0012:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            r0.<init>()     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            java.lang.String r1 = r3.getPackageName()     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            r0.append(r1)     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            java.lang.String r1 = ".BuildConfig"
            r0.append(r1)     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            java.lang.String r0 = r0.toString()     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            java.lang.String r1 = "branchKey"
            java.lang.reflect.Field r1 = r0.getField(r1)     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            if (r2 == 0) goto L_0x0046
            java.lang.String r2 = "branchKeyTest"
            java.lang.reflect.Field r2 = r0.getField(r2)     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            java.lang.Object r2 = r2.get(r0)     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            r1 = r2
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
        L_0x0046:
            io.branch.referral.Branch.getAutoInstance(r3, r1)     // Catch:{ ClassNotFoundException -> 0x0076, NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x004a }
            goto L_0x008b
        L_0x004a:
            r2 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r0 = "Cannot initialise branch sdk, api has changed ? "
            r3.append(r0)
            java.lang.String r2 = r2.getMessage()
            r3.append(r2)
            r3.toString()
            goto L_0x008b
        L_0x0060:
            r2 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r0 = "Cannot find branch key, forget branchKey in BuildConfig ? "
            r3.append(r0)
            java.lang.String r2 = r2.getMessage()
            r3.append(r2)
            r3.toString()
            goto L_0x008b
        L_0x0076:
            r2 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r0 = "Branch sdk not linked: "
            r3.append(r0)
            java.lang.String r2 = r2.getMessage()
            r3.append(r2)
            r3.toString()
        L_0x008b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.titan.BranchIntegration.ApplicationInit(boolean, android.content.Context):void");
    }

    public static void ActivityOnStart(String str, String str2) {
        Uri uri;
        try {
            Class.forName("io.branch.referral.Branch");
            Uri uri2 = null;
            if (str == null) {
                uri = null;
            } else {
                uri = Uri.parse(str);
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Branch init: ");
            sb.append(uri == null ? "null" : uri.toString());
            sb.toString();
            Branch instance = Branch.getInstance();
            GameApp instance2 = GameApp.getInstance();
            if (instance == null) {
                return;
            }
            if (instance2 != null) {
                if (uri == null) {
                    if (instance2.getIntent() != null) {
                        uri2 = instance2.getIntent().getData();
                    }
                    uri = uri2;
                }
                if (str2 != null && !str2.isEmpty()) {
                    Branch.getInstance().setRequestMetadata("sc_branch_account_id", str2);
                }
                instance.initSession(new e(instance2), uri, instance2);
            }
        } catch (ClassNotFoundException e) {
            "Branch sdk not linked: " + e.getMessage();
        }
    }
}
