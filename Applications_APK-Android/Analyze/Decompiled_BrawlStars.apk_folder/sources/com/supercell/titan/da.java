package com.supercell.titan;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import com.android.vending.billing.IInAppBillingService;
import com.supercell.titan.PurchaseManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: PurchaseManagerGoogle */
public class da extends PurchaseManager {
    private String A = "";
    private String B = "";
    private final BroadcastReceiver C = new db(this);
    private final ServiceConnection D = new dc(this);
    boolean p;
    boolean q;
    /* access modifiers changed from: private */
    public IInAppBillingService r;
    /* access modifiers changed from: private */
    public final Vector<String> s = new Vector<>();
    /* access modifiers changed from: private */
    public String t = "";
    /* access modifiers changed from: private */
    public int u = 0;
    private String v = "";
    private String w = "";
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public boolean y;
    /* access modifiers changed from: private */
    public String z = "";

    /* access modifiers changed from: private */
    public void i() {
        if (this.r != null) {
            b();
        }
        if (this.r != null) {
            j();
        }
        if (!this.w.isEmpty()) {
            if (this.r != null) {
                d(this.w);
            }
            this.w = "";
        }
    }

    public da(GameApp gameApp, String str) {
        super(gameApp);
        this.z = str;
        h();
    }

    public final void h() {
        this.x = false;
        this.p = true;
        this.y = true;
        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        this.q = this.j.bindService(intent, this.D, 1);
        this.y = this.q;
        this.j.registerReceiver(this.C, new IntentFilter("com.android.vending.billing.PURCHASES_UPDATED"));
    }

    public final void g() {
        if (!this.p && this.x && this.r != null) {
            i();
        }
    }

    public final void f() {
        super.f();
        j();
    }

    public final void a() {
        if (this.x) {
            this.j.unbindService(this.D);
        }
        this.j.unregisterReceiver(this.C);
        super.a();
    }

    public final String c(String str) {
        ArrayList<String> stringArrayList;
        if (this.r == null) {
            return "";
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(str);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("ITEM_ID_LIST", arrayList);
        try {
            Bundle skuDetails = this.r.getSkuDetails(3, this.j.getPackageName(), b(str), bundle);
            if (a(skuDetails, "RESPONSE_CODE") == 0 && (stringArrayList = skuDetails.getStringArrayList("DETAILS_LIST")) != null && !stringArrayList.isEmpty()) {
                return stringArrayList.get(0);
            }
        } catch (Exception e) {
            GameApp.debuggerException(e);
        }
        return "";
    }

    /* access modifiers changed from: private */
    public static String c(int i) {
        switch (i) {
            case 0:
                return "BILLING_RESPONSE_RESULT_OK";
            case 1:
                return "BILLING_RESPONSE_RESULT_USER_CANCELED";
            case 2:
            default:
                return "Unknown billing error " + i;
            case 3:
                return "BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE";
            case 4:
                return "BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE";
            case 5:
                return "BILLING_RESPONSE_RESULT_DEVELOPER_ERROR";
            case 6:
                return "BILLING_RESPONSE_RESULT_ERROR";
            case 7:
                return "BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED";
            case 8:
                return "BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED";
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.t = "";
        this.u = 0;
        if (this.r == null) {
            this.e = "";
            this.f = "No Billing service available";
            if (!this.q) {
                this.g = -100;
            } else {
                this.g = -101;
            }
        } else {
            if (this.s.isEmpty()) {
                this.s.addAll(this.h);
            }
            a(new JSONArray());
        }
    }

    private ArrayList<String> f(String str) {
        ArrayList<String> arrayList;
        synchronized (this.s) {
            int min = Math.min(this.s.size(), 20);
            arrayList = new ArrayList<>(min);
            Iterator<String> it = this.s.iterator();
            while (it.hasNext()) {
                String next = it.next();
                if (b(next).equals(str)) {
                    arrayList.add(next);
                    it.remove();
                    if (arrayList.size() >= min) {
                        break;
                    }
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void a(JSONArray jSONArray) {
        if (!this.s.isEmpty()) {
            ArrayList<String> f = f("inapp");
            ArrayList<String> f2 = f("subs");
            if (!f.isEmpty() || !f2.isEmpty()) {
                new de(this, jSONArray, f, f2).start();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void d(String str) {
        if (this.r == null) {
            this.w = str;
            return;
        }
        this.v = str;
        String b2 = b(str);
        synchronized (this) {
            this.k++;
        }
        new df(this, str, b2).start();
    }

    /* access modifiers changed from: private */
    public Bundle g(String str) {
        String str2 = this.A;
        if (str2 != null && !str2.isEmpty()) {
            try {
                String packageName = this.j.getPackageName();
                String b2 = b(str);
                if (this.r.a(6, packageName, b2) == 0) {
                    Bundle bundle = new Bundle();
                    bundle.putString("accountId", this.A);
                    if (this.B != null) {
                        this.B.isEmpty();
                    }
                    return this.r.a(6, packageName, str, b2, this.z, bundle);
                }
            } catch (Exception unused) {
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00b2 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r9, android.content.Intent r10) {
        /*
            r8 = this;
            com.supercell.titan.PurchaseManager$a r0 = new com.supercell.titan.PurchaseManager$a
            r0.<init>()
            r0.c = r9
            java.lang.String r1 = r8.v
            r0.f4986a = r1
            r2 = 0
            r3 = 1
            java.lang.String r4 = ""
            r5 = -1
            if (r9 != r5) goto L_0x008f
            java.lang.String r9 = "INAPP_PURCHASE_DATA"
            java.lang.String r9 = r10.getStringExtra(r9)
            java.lang.String r1 = "INAPP_DATA_SIGNATURE"
            java.lang.String r10 = r10.getStringExtra(r1)
            if (r9 == 0) goto L_0x00a0
            if (r10 == 0) goto L_0x00a0
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0084 }
            r1.<init>(r9)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r5 = "productId"
            java.lang.String r4 = r1.getString(r5)     // Catch:{ Exception -> 0x0084 }
            if (r4 == 0) goto L_0x0031
            r0.f4986a = r4     // Catch:{ Exception -> 0x0084 }
        L_0x0031:
            boolean r5 = r10.isEmpty()     // Catch:{ Exception -> 0x0084 }
            if (r5 != 0) goto L_0x00a0
            java.lang.String r5 = "orderId"
            java.lang.String r6 = ""
            java.lang.String r5 = r1.optString(r5, r6)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r6 = "purchaseToken"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r7 = "purchaseState"
            int r1 = r1.getInt(r7)     // Catch:{ Exception -> 0x0084 }
            if (r4 == 0) goto L_0x00a0
            boolean r7 = r4.isEmpty()     // Catch:{ Exception -> 0x0084 }
            if (r7 != 0) goto L_0x00a0
            if (r6 == 0) goto L_0x00a0
            boolean r7 = r6.isEmpty()     // Catch:{ Exception -> 0x0084 }
            if (r7 != 0) goto L_0x00a0
            com.supercell.titan.PurchaseManager$b r7 = new com.supercell.titan.PurchaseManager$b     // Catch:{ Exception -> 0x0084 }
            r7.<init>()     // Catch:{ Exception -> 0x0084 }
            r7.c = r5     // Catch:{ Exception -> 0x0084 }
            r7.f4989b = r4     // Catch:{ Exception -> 0x0084 }
            r7.f4988a = r9     // Catch:{ Exception -> 0x0084 }
            r7.d = r10     // Catch:{ Exception -> 0x0084 }
            r7.e = r6     // Catch:{ Exception -> 0x0084 }
            r7.f = r3     // Catch:{ Exception -> 0x0084 }
            boolean r9 = r8.a(r4)     // Catch:{ Exception -> 0x0084 }
            if (r9 != 0) goto L_0x0073
            goto L_0x00a0
        L_0x0073:
            if (r1 != 0) goto L_0x007c
            java.util.Vector r9 = r8.f4985b     // Catch:{ Exception -> 0x0084 }
            r9.add(r7)     // Catch:{ Exception -> 0x0084 }
            r9 = 1
            goto L_0x00a1
        L_0x007c:
            if (r1 != r3) goto L_0x007f
            goto L_0x009d
        L_0x007f:
            java.lang.String r9 = "refunded"
            r0.f4987b = r9     // Catch:{ Exception -> 0x0084 }
            goto L_0x00a0
        L_0x0084:
            r9 = move-exception
            java.lang.String r10 = r9.getMessage()
            r0.f4987b = r10
            com.supercell.titan.GameApp.debuggerException(r9)
            goto L_0x00a0
        L_0x008f:
            if (r9 != 0) goto L_0x00a0
            boolean r9 = r1.isEmpty()
            if (r9 != 0) goto L_0x00a0
            java.lang.String r4 = r8.v
            java.lang.String r9 = ""
            r8.v = r9
        L_0x009d:
            r9 = 0
            r10 = 1
            goto L_0x00a2
        L_0x00a0:
            r9 = 0
        L_0x00a1:
            r10 = 0
        L_0x00a2:
            if (r10 == 0) goto L_0x00aa
            java.util.Vector r9 = r8.d
            r9.add(r4)
            goto L_0x00b1
        L_0x00aa:
            if (r9 != 0) goto L_0x00b1
            java.util.Vector r9 = r8.c
            r9.add(r0)
        L_0x00b1:
            monitor-enter(r8)
            int r9 = r8.k     // Catch:{ all -> 0x00bd }
            int r9 = r9 - r3
            int r9 = java.lang.Math.max(r9, r2)     // Catch:{ all -> 0x00bd }
            r8.k = r9     // Catch:{ all -> 0x00bd }
            monitor-exit(r8)     // Catch:{ all -> 0x00bd }
            return
        L_0x00bd:
            r9 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00bd }
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.titan.da.a(int, android.content.Intent):void");
    }

    private void a(Bundle bundle) {
        if (bundle != null) {
            int a2 = a(bundle, "RESPONSE_CODE");
            if (a2 == 0) {
                this.p = true;
                ArrayList<String> stringArrayList = bundle.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList<String> stringArrayList2 = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                ArrayList<String> stringArrayList3 = bundle.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                if (stringArrayList != null && stringArrayList2 != null && stringArrayList3 != null) {
                    for (int i = 0; i < stringArrayList.size(); i++) {
                        String str = stringArrayList.get(i);
                        if (a(str)) {
                            String str2 = stringArrayList2.get(i);
                            String str3 = stringArrayList3.get(i);
                            try {
                                JSONObject jSONObject = new JSONObject(str2);
                                String string = jSONObject.getString("productId");
                                if (str.equals(string) && str3 != null && !str3.isEmpty()) {
                                    String optString = jSONObject.optString("orderId", "");
                                    String string2 = jSONObject.getString("purchaseToken");
                                    int i2 = jSONObject.getInt("purchaseState");
                                    if (string != null && !string.isEmpty() && string2 != null && !string2.isEmpty()) {
                                        PurchaseManager.b bVar = new PurchaseManager.b();
                                        bVar.c = optString;
                                        bVar.f4989b = string;
                                        bVar.f4988a = str2;
                                        bVar.d = str3;
                                        bVar.e = string2;
                                        bVar.f = false;
                                        if (i2 == 0) {
                                            if (!h(optString)) {
                                                this.f4985b.add(bVar);
                                            }
                                        } else if (i2 == 1) {
                                            this.d.add(string);
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                GameApp.debuggerException(e);
                            }
                        }
                    }
                }
            } else if (a2 == 3) {
                this.p = false;
            } else {
                c(a2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.r != null) {
            k();
        }
    }

    private void k() {
        try {
            a(this.r.a(3, this.j.getPackageName(), "inapp", null));
            a(this.r.a(3, this.j.getPackageName(), "subs", null));
        } catch (Exception e) {
            GameApp.debuggerException(e);
        }
    }

    /* access modifiers changed from: protected */
    public final void e(String str) {
        for (int i = 0; i < this.l.size(); i++) {
            PurchaseManager.b bVar = (PurchaseManager.b) this.l.get(i);
            if (str.equals(bVar.c)) {
                a(i);
                new dg(this, bVar).start();
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public static int a(Bundle bundle, String str) {
        Object obj = bundle.get(str);
        if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        }
        if (obj instanceof Long) {
            return ((Long) obj).intValue();
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return this.y && this.p && this.x;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        this.A = str;
        this.B = str2;
    }

    private boolean h(String str) {
        Iterator it = this.f4985b.iterator();
        while (it.hasNext()) {
            PurchaseManager.b bVar = (PurchaseManager.b) it.next();
            if (bVar != null && bVar.c.equals(str)) {
                return true;
            }
        }
        return false;
    }

    static /* synthetic */ void a(da daVar, JSONArray jSONArray, String str, ArrayList arrayList) {
        if (!arrayList.isEmpty()) {
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("ITEM_ID_LIST", arrayList);
            try {
                Bundle skuDetails = daVar.r.getSkuDetails(3, daVar.j.getPackageName(), str, bundle);
                int a2 = a(skuDetails, "RESPONSE_CODE");
                if (a2 == 0) {
                    daVar.p = true;
                    ArrayList<String> stringArrayList = skuDetails.getStringArrayList("DETAILS_LIST");
                    if (stringArrayList != null) {
                        try {
                            Iterator<String> it = stringArrayList.iterator();
                            while (it.hasNext()) {
                                jSONArray.put(new JSONObject(it.next()));
                            }
                        } catch (JSONException e) {
                            daVar.t = e.getMessage();
                            daVar.u = -2;
                            GameApp.debuggerException(e);
                        }
                    } else {
                        daVar.t = "<>";
                        daVar.u = -3;
                    }
                } else if (a2 == -1) {
                    daVar.t = "UNABLE TO GET PRODUCTS";
                    daVar.u = -4;
                } else {
                    if (a2 == 3) {
                        daVar.p = false;
                    }
                    daVar.t = c(a2);
                    daVar.u = a2;
                }
            } catch (Exception e2) {
                daVar.t = e2.getMessage();
                daVar.u = -5;
                GameApp.debuggerException(e2);
            }
        }
    }

    static /* synthetic */ void a(da daVar, PurchaseManager.b bVar) {
        IInAppBillingService iInAppBillingService = daVar.r;
        if (iInAppBillingService != null) {
            try {
                iInAppBillingService.b(3, daVar.j.getPackageName(), bVar.e);
            } catch (Exception e) {
                GameApp.debuggerException(e);
            }
        }
    }
}
