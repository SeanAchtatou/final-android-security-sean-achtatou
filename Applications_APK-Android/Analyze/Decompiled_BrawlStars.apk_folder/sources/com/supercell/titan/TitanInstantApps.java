package com.supercell.titan;

import com.google.android.gms.instantapps.a;

public class TitanInstantApps {
    public static void showInstallPrompt() {
    }

    public static void saveCookie(String str) {
        try {
            a.a(GameApp.getInstance()).a(str.isEmpty() ? null : str.getBytes("UTF-8"));
        } catch (Exception unused) {
        }
    }

    public static String getCookie() {
        try {
            byte[] a2 = a.a(GameApp.getInstance()).a();
            if (a2 != null) {
                return new String(a2, 0, a2.length, "UTF-8");
            }
            return "";
        } catch (Exception unused) {
            return "";
        }
    }
}
