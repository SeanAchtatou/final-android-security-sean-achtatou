package com.supercell.titan;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import com.facebook.places.model.PlaceFields;
import com.supercell.titan.GameApp;
import java.util.concurrent.FutureTask;

public class LocationService implements LocationListener, GameApp.a {

    /* renamed from: a  reason: collision with root package name */
    private static Location f4974a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final String[] f4975b = {"android.permission.ACCESS_FINE_LOCATION"};
    /* access modifiers changed from: private */
    public final LocationManager c = ((LocationManager) this.d.getSystemService(PlaceFields.LOCATION));
    /* access modifiers changed from: private */
    public final GameApp d = GameApp.getInstance();
    /* access modifiers changed from: private */
    public long e;
    /* access modifiers changed from: private */
    public String f;

    public static native void locationChanged(long j, double d2, double d3);

    public void init(long j) {
        this.e = j;
    }

    public void startUpdatingLocation() {
        GameApp.getInstance().runOnUiThread(new bl(this, this));
    }

    /* access modifiers changed from: private */
    public void a() {
        GameApp.getInstance().runOnUiThread(new bm(this, this));
    }

    public void stopUpdatingLocation() {
        GameApp.getInstance().runOnUiThread(new bn(this, this));
    }

    public boolean isEnabled() {
        int i;
        if (Build.VERSION.SDK_INT < 19) {
            return !TextUtils.isEmpty(Settings.Secure.getString(this.d.getContentResolver(), "location_providers_allowed"));
        }
        try {
            i = Settings.Secure.getInt(this.d.getContentResolver(), "location_mode");
        } catch (Settings.SettingNotFoundException unused) {
            i = 0;
        }
        if (i != 0) {
            return true;
        }
        return false;
    }

    public boolean showSystemSettings() {
        try {
            FutureTask futureTask = new FutureTask(new bo(this));
            this.d.runOnUiThread(futureTask);
            try {
                return ((Boolean) futureTask.get()).booleanValue();
            } catch (Exception e2) {
                e2.getCause();
                return false;
            }
        } catch (Exception unused) {
            return false;
        }
    }

    public void onLocationChanged(Location location) {
        f4974a = location;
        this.d.a(new bp(this, location));
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
        "onStatusChanged provider = " + str + "status = " + i;
    }

    public void onProviderEnabled(String str) {
        "onProviderEnabled = " + str;
    }

    public void onProviderDisabled(String str) {
        "onProviderDisabled = " + str;
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        boolean z;
        "Permission results are in from settings " + i;
        if (i == 1) {
            this.d.a(this);
            for (String str : this.f4975b) {
                int i2 = 0;
                while (true) {
                    if (i2 < strArr.length) {
                        if (strArr[i2].equals(str) && iArr[i2] == 0) {
                            z = true;
                            break;
                        }
                        i2++;
                    } else {
                        z = false;
                        break;
                    }
                }
                if (z) {
                    a();
                }
            }
        }
    }

    private boolean a(Location location, Location location2) {
        if (location2 == null && location != null) {
            return true;
        }
        if (location == null) {
            return false;
        }
        long time = location.getTime() - location2.getTime();
        boolean z = time > 120000;
        boolean z2 = time < -120000;
        boolean z3 = time > 0;
        if (z) {
            return true;
        }
        if (z2) {
            return false;
        }
        int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
        boolean z4 = accuracy > 0;
        boolean z5 = accuracy < 0;
        boolean z6 = accuracy > 200;
        boolean a2 = a(location.getProvider(), location2.getProvider());
        if (z5) {
            return true;
        }
        if (!z3 || z4) {
            return z3 && !z6 && a2;
        }
        return true;
    }

    private static boolean a(String str, String str2) {
        if (str == null) {
            return str2 == null;
        }
        return str.equals(str2);
    }

    static /* synthetic */ Location d(LocationService locationService) {
        Location lastKnownLocation = locationService.c.getLastKnownLocation("network");
        Location lastKnownLocation2 = locationService.c.getLastKnownLocation("gps");
        if (locationService.a(lastKnownLocation2, f4974a)) {
            f4974a = lastKnownLocation2;
        }
        if (locationService.a(lastKnownLocation, f4974a)) {
            f4974a = lastKnownLocation;
        }
        return f4974a;
    }
}
