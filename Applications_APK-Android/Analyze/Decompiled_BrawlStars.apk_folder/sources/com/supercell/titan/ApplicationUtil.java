package com.supercell.titan;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.util.concurrent.atomic.AtomicBoolean;

public class ApplicationUtil extends ApplicationUtilBase {

    /* renamed from: a  reason: collision with root package name */
    public static final AtomicBoolean f4962a = new AtomicBoolean(false);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static String f4963b = "";
    /* access modifiers changed from: private */
    public static boolean c;
    /* access modifiers changed from: private */
    public static AdvertisingIdClient.Info d;

    public static boolean getAdvertiserTrackingEnabled() {
        return c;
    }

    public static String getAdvertiserID() {
        String str = f4963b;
        str.isEmpty();
        return str;
    }

    public static void requestAdvertiserInfoOnNewThread() {
        new a().start();
    }
}
