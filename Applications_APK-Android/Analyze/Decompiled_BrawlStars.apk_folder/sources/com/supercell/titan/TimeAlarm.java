package com.supercell.titan;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.facebook.internal.NativeProtocol;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class TimeAlarm extends BroadcastReceiver {
    public static void a(Context context, Intent intent, Class<?> cls) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String str = null;
            if (extras.containsKey("title")) {
                str = extras.getString("title");
            }
            String str2 = str;
            String string = extras.getString(NotificationCompat.CATEGORY_MESSAGE);
            String string2 = extras.getString("sound");
            int i = extras.getInt("id");
            String string3 = extras.getString("userId");
            String string4 = extras.getString("imageURL");
            String string5 = extras.getString("channelId");
            String string6 = extras.getString("channelName");
            String string7 = extras.getString("channelDesc");
            int i2 = extras.getInt("color");
            if (GameApp.getInstance() == null || GameApp.getInstance().c) {
                a(context, string, str2, string2, string3, string4, string5, string6, string7, i2, cls);
            }
            GameApp.cancelNotification(i);
        }
    }

    private static byte[] a(String str) throws IOException {
        InputStream inputStream = null;
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.setReadTimeout(5000);
            httpURLConnection.getDoInput();
            httpURLConnection.connect();
            inputStream = httpURLConnection.getInputStream();
            return a(inputStream);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    private static byte[] a(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[16384];
        while (true) {
            int read = inputStream.read(bArr, 0, 16384);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.flush();
                return byteArrayOutputStream.toByteArray();
            }
        }
    }

    private static String a(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int identifier = context.getResources().getIdentifier(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, "string", context.getPackageName());
        if (identifier == 0) {
            identifier = applicationInfo.labelRes;
        }
        return identifier == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(identifier);
    }

    public static void a(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, int i, Class<?> cls) {
        int hashCode;
        int i2;
        String str9;
        Context context2 = context;
        String str10 = str;
        String str11 = str4;
        String str12 = str5;
        String str13 = str8;
        NotificationManager notificationManager = (NotificationManager) context2.getSystemService("notification");
        if (notificationManager != null) {
            ApplicationInfo applicationInfo = context.getApplicationInfo();
            String packageName = context.getPackageName();
            Resources resources = context.getResources();
            String a2 = a(context);
            String str14 = (Build.VERSION.SDK_INT >= 24 || a2 == null) ? str2 : a2;
            if (str14 == null) {
                str14 = "";
            }
            int i3 = applicationInfo.icon;
            Intent intent = new Intent(context2, cls);
            intent.setFlags(603979776);
            if (str11 == null || str4.isEmpty()) {
                hashCode = !str14.isEmpty() ? str14.hashCode() : str.hashCode();
            } else {
                intent.putExtra("userId", str11);
                hashCode = str4.hashCode();
            }
            int i4 = hashCode;
            String str15 = (str6 == null || str6.isEmpty()) ? "default" : str6;
            String str16 = "";
            if (Build.VERSION.SDK_INT >= 26) {
                if (str7 != null && !str7.isEmpty()) {
                    a2 = str7;
                } else if (a2 == null) {
                    a2 = context2.getString(17039375);
                }
                NotificationChannel notificationChannel = new NotificationChannel(str15, a2, 2);
                if (str13 != null) {
                    notificationChannel.setDescription(str13);
                }
                notificationManager.createNotificationChannel(notificationChannel);
            }
            boolean z = false;
            NotificationCompat.Builder autoCancel = new NotificationCompat.Builder(context2, str15).setContentIntent(PendingIntent.getActivity(context2, i4, intent, 0)).setSmallIcon(i3).setGroup(context.getPackageName()).setAutoCancel(true);
            try {
                int identifier = resources.getIdentifier("ic_notification", "drawable", packageName);
                if (identifier != 0) {
                    autoCancel.setSmallIcon(identifier);
                }
            } catch (Resources.NotFoundException unused) {
            }
            if (str12 != null && !str5.isEmpty()) {
                try {
                    byte[] a3 = a(str5);
                    autoCancel.setLargeIcon(BitmapFactory.decodeByteArray(a3, 0, a3.length));
                    z = true;
                } catch (IOException e) {
                    "Could not download image from " + str12 + ". Error: " + e.getLocalizedMessage();
                }
            }
            if (!z) {
                try {
                    int identifier2 = resources.getIdentifier("ic_icon", "drawable", packageName);
                    if (identifier2 != 0) {
                        autoCancel.setLargeIcon(BitmapFactory.decodeResource(resources, identifier2));
                    }
                } catch (Resources.NotFoundException unused2) {
                }
            }
            if (str14 != null && !str14.isEmpty()) {
                autoCancel.setContentTitle(str14);
            }
            if (str10 != null && !str.isEmpty()) {
                autoCancel.setContentText(str10);
                autoCancel.setTicker(str10);
                autoCancel.setStyle(new NotificationCompat.BigTextStyle().bigText(str10));
            }
            String str17 = str3;
            if (str17 == null || str3.isEmpty()) {
                i2 = i;
                str9 = str17;
            } else {
                str9 = str17.replaceAll("res/", str16).replaceAll("\\.wav", "\\.ogg").replaceAll("\\.caf", "\\.ogg");
                autoCancel.setSound(Uri.parse("content://" + (context.getPackageName() + ".assetprovider") + "/" + str9));
                i2 = i;
            }
            if (i2 != 0) {
                autoCancel.setColor(i2);
            }
            Notification build = autoCancel.build();
            if (str9 == null || str9.isEmpty()) {
                build.defaults |= 1;
            }
            build.defaults |= 6;
            build.flags |= 16;
            try {
                notificationManager.notify(i4, build);
            } catch (SecurityException unused3) {
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        try {
            new g(context, intent, Class.forName(context.getPackageManager().getLaunchIntentForPackage(context.getPackageName()).getComponent().getClassName())).execute(new String[0]);
        } catch (Exception unused) {
        }
    }
}
