package com.supercell.titan;

import com.facebook.Profile;
import com.facebook.ProfileTracker;

/* compiled from: NativeFacebookManager */
class cf extends ProfileTracker {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5073a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ NativeFacebookManager f5074b;

    cf(NativeFacebookManager nativeFacebookManager, GameApp gameApp) {
        this.f5074b = nativeFacebookManager;
        this.f5073a = gameApp;
    }

    public void onCurrentProfileChanged(Profile profile, Profile profile2) {
        if (profile2 != null) {
            this.f5074b.a(1);
        } else {
            this.f5073a.a(new cg(this));
        }
    }
}
