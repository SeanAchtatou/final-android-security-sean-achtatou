package com.supercell.titan;

import com.supercell.id.SupercellId;

/* compiled from: SupercellId */
class dn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5127a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ GameApp f5128b;
    final /* synthetic */ String c;
    final /* synthetic */ SupercellId d;

    dn(SupercellId supercellId, String str, GameApp gameApp, String str2) {
        this.d = supercellId;
        this.f5127a = str;
        this.f5128b = gameApp;
        this.c = str2;
    }

    public void run() {
        "SupercellId.openWindow(" + this.f5127a + ")";
        SupercellId.INSTANCE.present(this.f5128b, this.f5127a, this.c);
    }
}
