package com.supercell.titan;

import android.os.Build;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

/* compiled from: VirtualKeyboardHandler */
final class et implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5177a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ boolean f5178b;
    final /* synthetic */ String c;

    et(GameApp gameApp, boolean z, String str) {
        this.f5177a = gameApp;
        this.f5178b = z;
        this.c = str;
    }

    public final void run() {
        if (VirtualKeyboardHandler.f == null || !VirtualKeyboardHandler.f.isShowing()) {
            bc unused = VirtualKeyboardHandler.f = new bc(this.f5177a);
        } else {
            "VirtualKeyboardHandler.showKeyboard Dialog reference exists. Dialog visible = " + VirtualKeyboardHandler.f.isShowing();
        }
        bc a2 = VirtualKeyboardHandler.f;
        boolean z = this.f5178b;
        String str = this.c;
        a2.a();
        if (z) {
            a2.d.setBackgroundResource(R.drawable.edit_text_style);
            a2.d.setTextAppearance(a2.f5037a, R.style.largeEdittextText);
            a2.d.setWidth(a2.f);
            a2.d.setTypeface(a2.e);
            int b2 = bc.b(8);
            int b3 = bc.b(6);
            a2.d.setPadding(b2, b3, b2, b3);
            if (str != null && str.trim().length() > 0) {
                a2.f5037a.a(new bg(a2, str));
            }
            int b4 = bc.b(14);
            a2.f5038b.setBackgroundResource(R.drawable.edit_text_style);
            a2.f5038b.setText("✔");
            a2.f5038b.setPadding(b4, 0, b4, 0);
            int b5 = bc.b(2);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(b5, 0, 0, 0);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams2.addRule(12);
            layoutParams.addRule(12);
            if (Build.VERSION.SDK_INT < 17) {
                layoutParams2.addRule(9);
                layoutParams2.addRule(0, a2.f5038b.getId());
                layoutParams.addRule(11);
                layoutParams.addRule(6, a2.d.getId());
            } else {
                layoutParams.addRule(21);
                layoutParams2.addRule(16, a2.f5038b.getId());
            }
            a2.f5038b.setLayoutParams(layoutParams);
            a2.f5038b.setHeight(a2.d.getHeight());
            a2.d.setLayoutParams(layoutParams2);
        } else {
            a2.f5038b.setBackgroundDrawable(null);
            a2.f5038b.setText("");
            a2.f5038b.setHeight(1);
            a2.d.setBackgroundDrawable(null);
            a2.d.setTextAppearance(a2.f5037a, R.style.invisibleText);
            a2.d.setWidth(1);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams3.addRule(12);
            a2.d.setLayoutParams(layoutParams3);
        }
        a2.getWindow().setSoftInputMode(16);
        a2.show();
        a2.getWindow().setSoftInputMode(4);
        a2.d.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) a2.f5037a.getSystemService("input_method");
        if (!inputMethodManager.showSoftInput(a2.c, 0) && !inputMethodManager.showSoftInput(a2.d, 2)) {
            inputMethodManager.toggleSoftInput(0, 1);
        }
        VirtualKeyboardHandler.f5002a = true;
    }
}
