package com.supercell.titan;

import java.util.ArrayList;
import org.json.JSONArray;

/* compiled from: PurchaseManagerGoogle */
class de extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ JSONArray f5113a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ArrayList f5114b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ da d;

    de(da daVar, JSONArray jSONArray, ArrayList arrayList, ArrayList arrayList2) {
        this.d = daVar;
        this.f5113a = jSONArray;
        this.f5114b = arrayList;
        this.c = arrayList2;
    }

    public void run() {
        da.a(this.d, this.f5113a, "inapp", this.f5114b);
        da.a(this.d, this.f5113a, "subs", this.c);
        String unused = this.d.t;
        if (this.d.s.isEmpty() || this.d.u != 0) {
            synchronized (this.f5113a) {
                synchronized (this.d.e) {
                    this.d.e = this.f5113a.toString();
                }
            }
            da daVar = this.d;
            daVar.f = daVar.t;
            da daVar2 = this.d;
            daVar2.g = daVar2.u;
            String unused2 = this.d.t = "";
            int unused3 = this.d.u = 0;
            return;
        }
        this.d.a(this.f5113a);
    }
}
