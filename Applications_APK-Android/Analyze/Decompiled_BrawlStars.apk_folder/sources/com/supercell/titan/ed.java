package com.supercell.titan;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/* compiled from: TitanWebView */
class ed implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GestureDetector f5151a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ec f5152b;

    ed(ec ecVar, GestureDetector gestureDetector) {
        this.f5152b = ecVar;
        this.f5151a = gestureDetector;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f5151a.onTouchEvent(motionEvent);
    }
}
