package com.supercell.titan;

import android.content.ClipData;
import android.content.ClipboardManager;

/* compiled from: ApplicationUtilBase */
final class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5032a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f5033b;

    b(GameApp gameApp, String str) {
        this.f5032a = gameApp;
        this.f5033b = str;
    }

    public final void run() {
        try {
            ((ClipboardManager) this.f5032a.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("text", this.f5033b));
        } catch (Exception e) {
            GameApp.debuggerException(e);
        }
    }
}
