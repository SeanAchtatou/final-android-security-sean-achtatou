package com.supercell.titan;

import android.os.Process;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import java.io.IOException;

/* compiled from: ApplicationUtil */
final class a extends Thread {
    a() {
    }

    public final void run() {
        Process.setThreadPriority(10);
        try {
            String unused = ApplicationUtil.f4963b = "";
            boolean unused2 = ApplicationUtil.c = false;
            AdvertisingIdClient.Info unused3 = ApplicationUtil.d = AdvertisingIdClient.getAdvertisingIdInfo(GameApp.getInstance());
            if (ApplicationUtil.d != null) {
                String id = ApplicationUtil.d.getId();
                if (id != null) {
                    String unused4 = ApplicationUtil.f4963b = id;
                }
                boolean unused5 = ApplicationUtil.c = !ApplicationUtil.d.isLimitAdTrackingEnabled();
            }
        } catch (IOException unused6) {
            AdvertisingIdClient.Info unused7 = ApplicationUtil.d = (AdvertisingIdClient.Info) null;
        } catch (GooglePlayServicesNotAvailableException unused8) {
            AdvertisingIdClient.Info unused9 = ApplicationUtil.d = (AdvertisingIdClient.Info) null;
        } catch (Exception unused10) {
            AdvertisingIdClient.Info unused11 = ApplicationUtil.d = (AdvertisingIdClient.Info) null;
        }
        ApplicationUtil.f4962a.compareAndSet(false, true);
    }
}
