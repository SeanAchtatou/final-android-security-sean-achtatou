package com.supercell.titan;

import android.content.Context;
import android.view.KeyEvent;
import android.widget.RelativeLayout;

/* compiled from: KeyboardDialog */
class bk extends RelativeLayout {
    public bk(Context context) {
        super(context);
    }

    public boolean dispatchKeyEventPreIme(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4 && keyEvent.getAction() == 1) {
            VirtualKeyboardHandler.a(true, false);
        }
        return super.dispatchKeyEventPreIme(keyEvent);
    }
}
