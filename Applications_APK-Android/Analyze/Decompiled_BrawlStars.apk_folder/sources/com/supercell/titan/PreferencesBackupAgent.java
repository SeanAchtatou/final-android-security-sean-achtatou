package com.supercell.titan;

import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;

public class PreferencesBackupAgent extends BackupAgentHelper {
    public void onCreate() {
        try {
            addHelper("app_prefs", new SharedPreferencesBackupHelper(this, "storage", "storage_new"));
        } catch (Exception unused) {
        }
    }
}
