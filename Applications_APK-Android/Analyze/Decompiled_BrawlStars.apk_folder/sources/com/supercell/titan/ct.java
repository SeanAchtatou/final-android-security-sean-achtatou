package com.supercell.titan;

import android.os.Bundle;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: NativeFacebookRequestLinkStatisticsCallback */
public class ct implements GraphRequest.Callback {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final String f5093a;

    public ct(String str) {
        this.f5093a = str;
    }

    public void onCompleted(GraphResponse graphResponse) {
        if (AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired()) {
            try {
                JSONObject jSONObject = graphResponse.getJSONObject();
                int i = jSONObject.getJSONObject("og_object").getJSONObject("likes").getJSONObject("summary").getInt("total_count");
                int i2 = jSONObject.getJSONObject("og_object").getJSONObject("reactions").getJSONObject("summary").getInt("total_count");
                Bundle bundle = new Bundle();
                bundle.putString(GraphRequest.FIELDS_PARAM, "id");
                bundle.putString("object", this.f5093a);
                GraphRequest.executeBatchAsync(new GraphRequest(AccessToken.getCurrentAccessToken(), "me/og.likes", bundle, HttpMethod.GET, new cu(this, i, i2)));
            } catch (JSONException e) {
                GameApp.debuggerException(e);
            } catch (Exception e2) {
                GameApp.debuggerException(e2);
            }
        }
    }
}
