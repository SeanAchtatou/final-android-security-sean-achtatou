package com.supercell.titan;

import android.text.InputFilter;
import java.io.UnsupportedEncodingException;

public class VirtualKeyboardHandler {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f5002a = false;

    /* renamed from: b  reason: collision with root package name */
    private static int f5003b = -1;
    private static boolean c = false;
    private static int d = 1;
    private static String e;
    /* access modifiers changed from: private */
    public static bc f;
    private static final InputFilter g = new ev();

    public static native String getFontPath(String str);

    public static native void inputKeyboardDismissed();

    public static native void inputOkPressed();

    public static native void inputSelectionChanged(int i, int i2);

    public static native void inputTextChanged(String str);

    public static native void keyboardSizeChanged(float f2, float f3);

    public static void showKeyboard() {
        showKeyboard(c, e);
    }

    public static void showKeyboard(boolean z, String str) {
        GameApp instance = GameApp.getInstance();
        c = z;
        e = str;
        instance.runOnUiThread(new et(instance, z, str));
    }

    protected static void a(boolean z, boolean z2) {
        if (f != null) {
            GameApp.getInstance().runOnUiThread(new eu(z, z2));
        }
        f5002a = false;
    }

    public static void hideKeyboard() {
        a(false, true);
    }

    public static void setText(byte[] bArr) {
        try {
            bc.a(new String(bArr, "UTF-8"));
        } catch (UnsupportedEncodingException e2) {
            GameApp.debuggerException(e2);
        }
    }

    public static void setMaxTextLength(int i, int i2) {
        char c2 = 1;
        int i3 = i2 == 0 ? 1 : 0;
        if (i >= 0) {
            i3++;
        }
        f5003b = i;
        d = i2;
        InputFilter[] inputFilterArr = new InputFilter[i3];
        if (i >= 0) {
            inputFilterArr[0] = new InputFilter.LengthFilter(i);
        } else {
            c2 = 0;
        }
        int i4 = 301989894;
        if (i2 == 0) {
            inputFilterArr[c2] = g;
            i4 = -1845493754;
        }
        bc.a(inputFilterArr);
        bc.c(i4);
    }

    public static void updateUIFlags() {
        bc bcVar = f;
        if (bcVar != null) {
            bcVar.a();
        }
    }

    public static float getKeyboardSize(int i) {
        return bc.a(i);
    }
}
