package com.supercell.titan;

import android.view.KeyEvent;
import android.view.MotionEvent;

public class UnbotifyManager {
    public static void dispatchKeyEvent(KeyEvent keyEvent) {
    }

    public static void dispatchTouchEvent(MotionEvent motionEvent) {
    }

    public static void enable(boolean z, int i, boolean z2) {
    }

    public static void endContext() {
    }

    public static void onCustomEvent(int i) {
    }

    public static void onPause() {
    }

    public static void onResume() {
    }

    public static native void sendReports(byte[] bArr);

    public static void setAppToken(String str) {
    }

    public static void setKeyValue(String str, String str2) {
    }

    public static void startContext(String str) {
    }
}
