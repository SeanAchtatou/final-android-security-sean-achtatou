package com.supercell.titan;

import android.os.Process;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.mobileapptracker.MobileAppTracker;
import java.io.IOException;

/* compiled from: GoogleUtils */
final class al implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5014a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f5015b;

    al(GameApp gameApp, MobileAppTracker mobileAppTracker) {
        this.f5014a = gameApp;
        this.f5015b = mobileAppTracker;
    }

    public final void run() {
        Process.setThreadPriority(10);
        try {
            AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.f5014a);
            this.f5015b.setGoogleAdvertisingId(advertisingIdInfo.getId(), advertisingIdInfo.isLimitAdTrackingEnabled());
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException unused) {
        }
    }
}
