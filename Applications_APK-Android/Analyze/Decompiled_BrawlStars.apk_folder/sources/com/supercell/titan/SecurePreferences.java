package com.supercell.titan;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class SecurePreferences {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f4990a;

    /* renamed from: b  reason: collision with root package name */
    private final Cipher f4991b;
    private final Cipher c;
    private final Cipher d;
    private final Cipher e;
    private final SharedPreferences f;

    public static class SecurePreferencesException extends RuntimeException {
        public SecurePreferencesException(Throwable th) {
            super(th);
        }
    }

    public SecurePreferences(Context context, String str, String str2, boolean z) throws SecurePreferencesException {
        try {
            this.f4991b = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.d = Cipher.getInstance("AES/ECB/PKCS5Padding");
            this.e = Cipher.getInstance("AES/ECB/PKCS5Padding");
            byte[] bArr = new byte[this.f4991b.getBlockSize()];
            System.arraycopy("fldsjfodasjifudslfjdsaofshaufihadsf".getBytes(), 0, bArr, 0, this.f4991b.getBlockSize());
            IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.reset();
            SecretKeySpec secretKeySpec = new SecretKeySpec(instance.digest(str2.getBytes("UTF-8")), "AES/CBC/PKCS5Padding");
            this.f4991b.init(1, secretKeySpec, ivParameterSpec);
            this.c.init(2, secretKeySpec, ivParameterSpec);
            this.d.init(1, secretKeySpec);
            this.e.init(2, secretKeySpec);
            this.f = context.getSharedPreferences(str, 0);
            this.f4990a = true;
        } catch (GeneralSecurityException e2) {
            GameApp.debuggerException(e2);
            throw new SecurePreferencesException(e2);
        } catch (UnsupportedEncodingException e3) {
            GameApp.debuggerException(e3);
            throw new SecurePreferencesException(e3);
        }
    }

    public final void a(String str, String str2) {
        String d2 = d(str);
        if (str2 != null && !str2.isEmpty()) {
            b(d2, str2);
        } else if (this.f.contains(d2)) {
            this.f.edit().remove(d2).apply();
        }
    }

    public final void a(String str) {
        if (this.f.contains(str)) {
            this.f.edit().remove(str).apply();
        }
        String d2 = d(str);
        if (this.f.contains(d2)) {
            this.f.edit().remove(d2).apply();
        }
    }

    public final String b(String str) {
        String d2 = d(str);
        if (!this.f.contains(d2)) {
            return "";
        }
        String c2 = c(this.f.getString(d2, ""));
        if (c2.isEmpty()) {
            this.f.edit().remove(d2).apply();
        }
        return c2;
    }

    private String c(String str) {
        return b(str, this.c);
    }

    private String d(String str) {
        return this.f4990a ? a(str, this.d) : str;
    }

    private void b(String str, String str2) throws SecurePreferencesException {
        this.f.edit().putString(str, a(str2, this.f4991b)).apply();
    }

    private static String a(String str, Cipher cipher) throws SecurePreferencesException {
        try {
            return Base64.encodeToString(a(cipher, str.getBytes("UTF-8")), 2);
        } catch (Exception e2) {
            GameApp.debuggerException(e2);
            throw new SecurePreferencesException(e2);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String b(java.lang.String r2, javax.crypto.Cipher r3) {
        /*
            java.lang.String r0 = ""
            r1 = 2
            byte[] r2 = android.util.Base64.decode(r2, r1)     // Catch:{ Exception -> 0x0013 }
            byte[] r2 = a(r3, r2)     // Catch:{  }
            java.lang.String r3 = new java.lang.String     // Catch:{  }
            java.lang.String r1 = "UTF-8"
            r3.<init>(r2, r1)     // Catch:{  }
            return r3
        L_0x0013:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.titan.SecurePreferences.b(java.lang.String, javax.crypto.Cipher):java.lang.String");
    }

    private static byte[] a(Cipher cipher, byte[] bArr) throws SecurePreferencesException {
        try {
            return cipher.doFinal(bArr);
        } catch (Exception e2) {
            throw new SecurePreferencesException(e2);
        }
    }
}
