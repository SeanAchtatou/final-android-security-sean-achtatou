package com.supercell.titan;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: TitanEditText */
class dw implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ dt f5138a;

    /* renamed from: b  reason: collision with root package name */
    private String f5139b = "";

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    dw(dt dtVar) {
        this.f5138a = dtVar;
    }

    public void afterTextChanged(Editable editable) {
        String obj = editable.toString();
        if (!obj.equals(this.f5139b) && !this.f5138a.f5136a) {
            GameApp.getInstance().a(new dx(this, obj));
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.f5139b = charSequence.toString();
    }
}
