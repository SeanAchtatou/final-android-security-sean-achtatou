package com.supercell.titan;

import android.support.v4.app.ActivityCompat;
import java.util.Vector;

/* compiled from: LocationService */
class bl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocationService f5048a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ LocationService f5049b;

    bl(LocationService locationService, LocationService locationService2) {
        this.f5049b = locationService;
        this.f5048a = locationService2;
    }

    public void run() {
        if (ActivityCompat.checkSelfPermission(this.f5049b.d, "android.permission.ACCESS_FINE_LOCATION") != 0) {
            GameApp a2 = this.f5049b.d;
            LocationService locationService = this.f5048a;
            if (a2.f4965a == null) {
                a2.f4965a = new Vector<>();
            }
            a2.f4965a.add(locationService);
            ActivityCompat.requestPermissions(this.f5049b.d, this.f5049b.f4975b, 1);
            return;
        }
        this.f5049b.a();
    }
}
