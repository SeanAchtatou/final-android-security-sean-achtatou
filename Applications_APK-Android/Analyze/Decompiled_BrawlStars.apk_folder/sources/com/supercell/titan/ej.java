package com.supercell.titan;

import android.view.animation.TranslateAnimation;

/* compiled from: TitanWebView */
class ej implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ float f5162a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ float f5163b;
    final /* synthetic */ float c;
    final /* synthetic */ float d;
    final /* synthetic */ float e;
    final /* synthetic */ boolean f;
    final /* synthetic */ GameApp g;
    final /* synthetic */ TitanWebView h;

    ej(TitanWebView titanWebView, float f2, float f3, float f4, float f5, float f6, boolean z, GameApp gameApp) {
        this.h = titanWebView;
        this.f5162a = f2;
        this.f5163b = f3;
        this.c = f4;
        this.d = f5;
        this.e = f6;
        this.f = z;
        this.g = gameApp;
    }

    public void run() {
        TranslateAnimation translateAnimation = new TranslateAnimation(this.f5162a, this.f5163b, this.c, this.d);
        translateAnimation.setDuration((long) ((int) (this.e * 1000.0f)));
        translateAnimation.setFillAfter(true);
        translateAnimation.setAnimationListener(new ek(this));
        this.h.f4995b.startAnimation(translateAnimation);
    }
}
