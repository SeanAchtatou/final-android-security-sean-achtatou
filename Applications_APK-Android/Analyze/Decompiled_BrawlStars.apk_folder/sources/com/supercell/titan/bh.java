package com.supercell.titan;

import android.graphics.Typeface;

/* compiled from: KeyboardDialog */
class bh implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Typeface f5044a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ bg f5045b;

    bh(bg bgVar, Typeface typeface) {
        this.f5045b = bgVar;
        this.f5044a = typeface;
    }

    public void run() {
        this.f5045b.f5043b.d.setTypeface(this.f5044a);
    }
}
