package com.supercell.titan;

import android.view.inputmethod.InputMethodManager;

/* compiled from: VirtualKeyboardHandler */
final class eu implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f5179a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ boolean f5180b;

    eu(boolean z, boolean z2) {
        this.f5179a = z;
        this.f5180b = z2;
    }

    public final void run() {
        if (VirtualKeyboardHandler.f != null) {
            bc a2 = VirtualKeyboardHandler.f;
            boolean z = this.f5179a;
            boolean z2 = this.f5180b;
            InputMethodManager inputMethodManager = (InputMethodManager) a2.f5037a.getSystemService("input_method");
            if (!(inputMethodManager == null || a2.d == null)) {
                inputMethodManager.hideSoftInputFromWindow(a2.d.getWindowToken(), 0);
                a2.d.clearFocus();
                if (z && !z2) {
                    a2.f5037a.a(new bi(a2));
                }
                a2.f5037a.a();
                a2.f5037a.a(2000);
                a2.dismiss();
            }
            bc unused = VirtualKeyboardHandler.f = null;
        }
    }
}
