package com.supercell.titan;

import android.view.KeyEvent;
import android.widget.TextView;

/* compiled from: TitanEditText */
class dv implements TextView.OnEditorActionListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ dt f5137a;

    dv(dt dtVar) {
        this.f5137a = dtVar;
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        GameApp.getInstance().a(dt.f5135b);
        return false;
    }
}
