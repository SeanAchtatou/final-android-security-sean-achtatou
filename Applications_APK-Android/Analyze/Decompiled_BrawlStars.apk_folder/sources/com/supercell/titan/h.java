package com.supercell.titan;

import android.content.Context;
import android.graphics.Typeface;
import java.util.HashMap;
import java.util.Map;

/* compiled from: KeyboardDialog */
class h {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<String, Typeface> f5184a = new HashMap();

    static Typeface a(Context context, String str) {
        if (f5184a.containsKey(str)) {
            return f5184a.get(str);
        }
        Typeface createFromAsset = Typeface.createFromAsset(context.getAssets(), str);
        f5184a.put(str, createFromAsset);
        return createFromAsset;
    }
}
