package com.supercell.titan;

import com.facebook.share.model.GameRequestContent;
import com.facebook.share.widget.GameRequestDialog;

/* compiled from: NativeFacebookManager */
class cn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5084a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f5085b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ GameRequestDialog e;
    final /* synthetic */ NativeFacebookManager f;

    cn(NativeFacebookManager nativeFacebookManager, String str, String str2, String str3, String str4, GameRequestDialog gameRequestDialog) {
        this.f = nativeFacebookManager;
        this.f5084a = str;
        this.f5085b = str2;
        this.c = str3;
        this.d = str4;
        this.e = gameRequestDialog;
    }

    public void run() {
        GameRequestContent.Builder builder = new GameRequestContent.Builder();
        if (!this.f5084a.isEmpty()) {
            builder.setMessage(this.f5084a);
        }
        if (!this.f5085b.isEmpty()) {
            builder.setTitle(this.f5085b);
        }
        if (!this.c.isEmpty()) {
            builder.setData(this.c);
        }
        if (!this.d.isEmpty()) {
            builder.setTo(this.d);
        }
        this.e.show(builder.build());
    }
}
