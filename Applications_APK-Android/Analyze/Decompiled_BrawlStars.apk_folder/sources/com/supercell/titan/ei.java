package com.supercell.titan;

import android.view.View;
import android.view.ViewGroup;

/* compiled from: TitanWebView */
class ei implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f5160a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ int f5161b;
    final /* synthetic */ int c;
    final /* synthetic */ int d;
    final /* synthetic */ TitanWebView e;

    ei(TitanWebView titanWebView, int i, int i2, int i3, int i4) {
        this.e = titanWebView;
        this.f5160a = i;
        this.f5161b = i2;
        this.c = i3;
        this.d = i4;
    }

    public void run() {
        "TitanWebView.show(" + this.f5160a + "," + this.f5161b + "," + this.c + "," + this.d + ")";
        this.e.f4995b.setVisibility(0);
        ((View) this.e.f4995b.getParent()).setVisibility(0);
        if (this.e.f4995b.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.e.f4995b.getLayoutParams();
            marginLayoutParams.width = this.c;
            marginLayoutParams.height = this.d;
            marginLayoutParams.leftMargin = this.f5160a;
            marginLayoutParams.topMargin = this.f5161b;
            this.e.f4995b.setLayoutParams(marginLayoutParams);
        }
    }
}
