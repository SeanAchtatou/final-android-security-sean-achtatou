package com.supercell.titan;

import com.supercell.id.SupercellId;

/* compiled from: SupercellId */
class dj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5121a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SupercellId f5122b;
    final /* synthetic */ SupercellId c;

    dj(SupercellId supercellId, GameApp gameApp, SupercellId supercellId2) {
        this.c = supercellId;
        this.f5121a = gameApp;
        this.f5122b = supercellId2;
    }

    public void run() {
        SupercellId.INSTANCE.setupWithDelegate(this.f5121a, this.f5122b);
    }
}
