package com.supercell.titan;

import android.location.Location;

/* compiled from: LocationService */
class bp implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Location f5055a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ LocationService f5056b;

    bp(LocationService locationService, Location location) {
        this.f5056b = locationService;
        this.f5055a = location;
    }

    public void run() {
        LocationService.locationChanged(this.f5056b.e, this.f5055a.getLatitude(), this.f5055a.getLongitude());
    }
}
