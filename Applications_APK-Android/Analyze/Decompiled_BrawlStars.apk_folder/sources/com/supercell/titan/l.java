package com.supercell.titan;

import android.view.DisplayCutout;
import android.view.View;
import android.view.WindowInsets;

/* compiled from: GameApp */
class l implements View.OnApplyWindowInsetsListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5191a;

    l(GameApp gameApp) {
        this.f5191a = gameApp;
    }

    public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        DisplayCutout displayCutout = windowInsets.getDisplayCutout();
        if (displayCutout != null) {
            this.f5191a.a(new m(this, displayCutout));
        }
        return windowInsets;
    }
}
