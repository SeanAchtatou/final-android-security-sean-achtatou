package com.supercell.titan;

import android.view.DisplayCutout;

/* compiled from: GameApp */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DisplayCutout f5192a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ l f5193b;

    m(l lVar, DisplayCutout displayCutout) {
        this.f5193b = lVar;
        this.f5192a = displayCutout;
    }

    public void run() {
        GameApp.setSafeMargins(this.f5192a.getSafeInsetBottom(), this.f5192a.getSafeInsetTop(), this.f5192a.getSafeInsetLeft(), this.f5192a.getSafeInsetRight());
    }
}
