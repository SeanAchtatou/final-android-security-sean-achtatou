package com.supercell.titan;

import android.net.Uri;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

/* compiled from: NativeFacebookManager */
class cl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5081a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f5082b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ ShareDialog e;
    final /* synthetic */ NativeFacebookManager f;

    cl(NativeFacebookManager nativeFacebookManager, String str, String str2, String str3, String str4, ShareDialog shareDialog) {
        this.f = nativeFacebookManager;
        this.f5081a = str;
        this.f5082b = str2;
        this.c = str3;
        this.d = str4;
        this.e = shareDialog;
    }

    public void run() {
        if (ShareDialog.canShow((Class<? extends ShareContent>) ShareLinkContent.class)) {
            ShareLinkContent.Builder builder = new ShareLinkContent.Builder();
            if (!this.f5081a.isEmpty()) {
                builder.setContentTitle(this.f5081a);
            }
            if (!this.f5082b.isEmpty()) {
                builder.setContentDescription(this.f5082b);
            }
            if (!this.c.isEmpty()) {
                builder.setContentUrl(Uri.parse(this.c));
            }
            if (!this.d.isEmpty()) {
                builder.setImageUrl(Uri.parse(this.d));
            }
            this.e.show(builder.build());
        }
    }
}
