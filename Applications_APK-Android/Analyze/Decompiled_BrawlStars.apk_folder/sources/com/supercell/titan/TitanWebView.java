package com.supercell.titan;

import android.graphics.Bitmap;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

public class TitanWebView {
    /* access modifiers changed from: private */
    public static b f;
    /* access modifiers changed from: private */
    public static View g;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f4994a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public WebView f4995b;
    /* access modifiers changed from: private */
    public b c;
    /* access modifiers changed from: private */
    public FrameLayout d;
    /* access modifiers changed from: private */
    public WebChromeClient.CustomViewCallback e;
    /* access modifiers changed from: private */
    public long h = 0;

    public native void hideNative();

    public native void onPageFinished(String str);

    public native void onPageStarted(String str);

    public native void onReceivedError(String str);

    public native void onSwipeRight();

    public native boolean shouldOverrideUrlLoading(String str);

    public class b extends WebChromeClient {

        /* renamed from: b  reason: collision with root package name */
        private TitanWebView f4999b;

        public b(TitanWebView titanWebView) {
            this.f4999b = titanWebView;
        }

        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
            if (TitanWebView.g != null) {
                customViewCallback.onCustomViewHidden();
                return;
            }
            View unused = TitanWebView.g = view;
            b unused2 = TitanWebView.f = this;
            TitanWebView.this.d.setVisibility(0);
            TitanWebView.this.d.addView(view);
            WebChromeClient.CustomViewCallback unused3 = TitanWebView.this.e = customViewCallback;
        }

        public void onHideCustomView() {
            super.onHideCustomView();
            if (TitanWebView.g != null) {
                TitanWebView.this.d.setVisibility(8);
                TitanWebView.g.setVisibility(8);
                TitanWebView.this.d.removeView(TitanWebView.g);
                TitanWebView.this.e.onCustomViewHidden();
                View unused = TitanWebView.g = (View) null;
                b unused2 = TitanWebView.f = (b) null;
            }
        }
    }

    public class c extends WebViewClient {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public TitanWebView f5001b;

        public c(TitanWebView titanWebView) {
            this.f5001b = titanWebView;
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            "TitanWebViewClient.onPageStarted url:" + str;
            GameApp.getInstance().a(new ep(this, str));
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            "TitanWebViewClient.onPageFinished url:" + str;
            GameApp instance = GameApp.getInstance();
            instance.runOnUiThread(new eq(this));
            instance.a(new er(this, str));
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            "TitanWebViewClient.onReceivedError errorCode:" + i + " description:" + str + " failingUrl:" + str2;
            GameApp.getInstance().a(new es(this, str2));
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            super.shouldOverrideUrlLoading(webView, str);
            "TitanWebViewClient.shouldOverrideUrlLoading url:" + str;
            GameApp.getInstance();
            return this.f5001b.shouldOverrideUrlLoading(str);
        }
    }

    public long getObjPtr() {
        return this.h;
    }

    public void setObjPtr(long j) {
        this.h = j;
    }

    public static boolean isInCustomView() {
        return g != null;
    }

    public static void hideCustomView() {
        b bVar = f;
        if (bVar != null) {
            bVar.onHideCustomView();
        }
    }

    public void init(boolean z) {
        GameApp instance = GameApp.getInstance();
        instance.runOnUiThread(new dz(this, instance, this, z));
    }

    public void close() {
        GameApp.getInstance().runOnUiThread(new ee(this));
    }

    public void loadURL(String str) {
        GameApp.getInstance().runOnUiThread(new ef(this, str));
    }

    public void loadHTML(String str) {
        GameApp.getInstance().runOnUiThread(new eg(this, str));
    }

    public void eval(String str) {
        GameApp.getInstance().runOnUiThread(new eh(this, str));
    }

    public void show(int i, int i2, int i3, int i4) {
        GameApp.getInstance().runOnUiThread(new ei(this, i, i2, i3, i4));
    }

    public void moveView(float f2, float f3, float f4, float f5, float f6, float f7, float f8, boolean z) {
        GameApp instance = GameApp.getInstance();
        instance.runOnUiThread(new ej(this, f2, f4, f3, f5, f8, z, instance));
    }

    public void slideUp(View view) {
        view.setVisibility(0);
    }

    public void setAlpha(float f2) {
        GameApp.getInstance().runOnUiThread(new em(this, f2));
    }

    public void hide() {
        GameApp.getInstance().runOnUiThread(new en(this));
    }

    public boolean isVisible() {
        return this.f4995b.getVisibility() == 0;
    }

    public void goBack() {
        GameApp.getInstance().runOnUiThread(new ea(this));
    }

    public void reload() {
        GameApp.getInstance().runOnUiThread(new eb(this));
    }

    public boolean canGoBack() {
        return this.f4994a;
    }

    public void addSwipeRightRecognizer() {
        GameApp.getInstance().runOnUiThread(new ec(this, this));
    }

    class a extends GestureDetector.SimpleOnGestureListener {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public TitanWebView f4997b;

        public a(TitanWebView titanWebView) {
            this.f4997b = titanWebView;
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            if (motionEvent2.getX() - motionEvent.getX() <= 120.0f || Math.abs(f) <= 200.0f) {
                return false;
            }
            GameApp.getInstance().a(new eo(this));
            return false;
        }
    }

    static /* synthetic */ void e(TitanWebView titanWebView) {
        GameApp instance = GameApp.getInstance();
        InputMethodManager inputMethodManager = (InputMethodManager) instance.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(titanWebView.f4995b.getWindowToken(), 0);
        }
        titanWebView.f4995b.clearFocus();
        instance.a();
        instance.a(2000);
    }
}
