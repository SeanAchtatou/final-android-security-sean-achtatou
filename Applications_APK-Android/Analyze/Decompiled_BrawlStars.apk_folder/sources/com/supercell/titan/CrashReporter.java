package com.supercell.titan;

import android.app.Application;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.microsoft.appcenter.f;

public class CrashReporter {
    public static native void initializeGoogleBreakpad(String str, boolean z);

    public static void start(Application application, String str, String str2, boolean z) {
        try {
            f.a().a(application, str, new Class[]{Analytics.class, Crashes.class});
            f.a().a(str2);
            Crashes.l().a(new f(z));
        } catch (Exception unused) {
        }
    }
}
