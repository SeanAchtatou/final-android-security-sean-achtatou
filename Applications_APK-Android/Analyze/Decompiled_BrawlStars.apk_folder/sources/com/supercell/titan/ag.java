package com.supercell.titan;

import android.os.AsyncTask;
import com.google.android.gms.games.a;

/* compiled from: GoogleServiceClient */
class ag extends AsyncTask<Void, Void, String> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GoogleServiceClient f5010a;

    ag(GoogleServiceClient googleServiceClient) {
        this.f5010a = googleServiceClient;
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ Object doInBackground(Object[] objArr) {
        return a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.supercell.titan.GoogleServiceClient.a(com.supercell.titan.GoogleServiceClient, boolean):boolean
     arg types: [com.supercell.titan.GoogleServiceClient, int]
     candidates:
      com.supercell.titan.GoogleServiceClient.a(com.supercell.titan.GoogleServiceClient, java.lang.String):java.lang.String
      com.supercell.titan.GoogleServiceClient.a(com.supercell.titan.GoogleServiceClient, boolean):boolean */
    /* access modifiers changed from: protected */
    public /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        boolean unused = this.f5010a.h = false;
        if (str == null || str.length() <= 0) {
            this.f5010a.f4970b.c();
            this.f5010a.f4969a.a(new ai(this));
            return;
        }
        String unused2 = this.f5010a.g = str;
        this.f5010a.f4969a.a(new ah(this));
        this.f5010a.f4969a.e.g();
    }

    private String a() {
        try {
            a.b a2 = a.a(this.f5010a.f4970b, this.f5010a.f4969a.d).a();
            if (a2.b().a()) {
                return a2.a();
            }
            return "";
        } catch (Exception e) {
            GameApp.debuggerException(e);
            return "";
        }
    }
}
