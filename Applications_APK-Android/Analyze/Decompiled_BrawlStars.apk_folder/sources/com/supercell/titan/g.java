package com.supercell.titan;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

/* compiled from: TimeAlarm */
class g extends AsyncTask<String, Void, Void> {

    /* renamed from: a  reason: collision with root package name */
    Context f5182a;

    /* renamed from: b  reason: collision with root package name */
    Intent f5183b;
    Class c;

    g(Context context, Intent intent, Class<?> cls) {
        this.f5182a = context;
        this.f5183b = intent;
        this.c = cls;
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ Object doInBackground(Object[] objArr) {
        TimeAlarm.a(this.f5182a, this.f5183b, this.c);
        return null;
    }
}
