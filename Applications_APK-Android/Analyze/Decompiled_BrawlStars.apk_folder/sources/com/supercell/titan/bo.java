package com.supercell.titan;

import android.content.Intent;
import java.util.concurrent.Callable;

/* compiled from: LocationService */
class bo implements Callable<Boolean> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocationService f5054a;

    bo(LocationService locationService) {
        this.f5054a = locationService;
    }

    public /* synthetic */ Object call() throws Exception {
        this.f5054a.d.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
        return true;
    }
}
