package com.supercell.titan;

public final class BuildConfig {
    @Deprecated
    public static final String APPLICATION_ID = "com.supercell.titan";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "googlePlayFirebaseBrawlstars";
    public static final String FLAVOR_game = "brawlstars";
    public static final String FLAVOR_messaging = "firebase";
    public static final String FLAVOR_store = "googlePlay";
    public static final String LIBRARY_PACKAGE_NAME = "com.supercell.titan";
    public static final int VERSION_CODE = -1;
    public static final String VERSION_NAME = "";
    public static final int storeCode = 2;
}
