package com.supercell.titan;

import android.text.InputFilter;
import android.text.Spanned;

/* compiled from: VirtualKeyboardHandler */
final class ev implements InputFilter {
    ev() {
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        while (i < i2) {
            if (charSequence.charAt(i) >= 128) {
                return "";
            }
            i++;
        }
        return null;
    }
}
