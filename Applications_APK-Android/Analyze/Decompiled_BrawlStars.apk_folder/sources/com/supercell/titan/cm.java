package com.supercell.titan;

import android.widget.Toast;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.share.widget.GameRequestDialog;

/* compiled from: NativeFacebookManager */
class cm implements FacebookCallback<GameRequestDialog.Result> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NativeFacebookManager f5083a;

    cm(NativeFacebookManager nativeFacebookManager) {
        this.f5083a = nativeFacebookManager;
    }

    public /* synthetic */ void onSuccess(Object obj) {
        if (((GameRequestDialog.Result) obj).getRequestId() != null) {
            Toast.makeText(this.f5083a.f4981b.getApplicationContext(), "Request sent", 0).show();
        }
    }

    public void onCancel() {
        Toast.makeText(this.f5083a.f4981b.getApplicationContext(), "Request cancelled", 0).show();
    }

    public void onError(FacebookException facebookException) {
        if (facebookException instanceof FacebookOperationCanceledException) {
            Toast.makeText(this.f5083a.f4981b.getApplicationContext(), "Request cancelled", 0).show();
        } else {
            Toast.makeText(this.f5083a.f4981b.getApplicationContext(), "Network Error", 0).show();
        }
    }
}
