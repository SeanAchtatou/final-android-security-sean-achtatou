package com.supercell.titan;

/* compiled from: GameApp */
final class t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f5199a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f5200b;
    final /* synthetic */ String c;

    t(int i, String str, String str2) {
        this.f5199a = i;
        this.f5200b = str;
        this.c = str2;
    }

    public final void run() {
        if (GameApp.getInstance() != null && GameApp.isNativeLibraryLoaded() && !GameApp.getInstance().c) {
            GameApp.setPushNotificationValues(this.f5199a, this.f5200b, this.c);
        }
    }
}
