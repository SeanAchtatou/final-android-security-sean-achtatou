package com.supercell.titan;

import android.view.Window;
import android.view.WindowManager;

/* compiled from: GameApp */
final class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f5189a;

    j(int i) {
        this.f5189a = i;
    }

    public final void run() {
        Window window = GameApp.getInstance().getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.preferredDisplayModeId = this.f5189a;
        window.setAttributes(attributes);
    }
}
