package com.supercell.titan;

import android.hardware.camera2.CameraManager;

/* compiled from: GoogleServiceClient */
class z extends CameraManager.AvailabilityCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GoogleServiceClient f5208a;

    z(GoogleServiceClient googleServiceClient) {
        this.f5208a = googleServiceClient;
    }

    public void onCameraAvailable(String str) {
        boolean unused = this.f5208a.k = false;
    }

    public void onCameraUnavailable(String str) {
        boolean unused = this.f5208a.k = true;
    }
}
