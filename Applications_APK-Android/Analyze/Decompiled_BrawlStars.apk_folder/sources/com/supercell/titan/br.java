package com.supercell.titan;

import android.content.DialogInterface;

/* compiled from: NativeDialogManager */
class br implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NativeDialogManager f5059a;

    br(NativeDialogManager nativeDialogManager) {
        this.f5059a = nativeDialogManager;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        int i2 = i == -1 ? 1 : i == -2 ? 2 : 0;
        NativeDialogManager nativeDialogManager = this.f5059a;
        nativeDialogManager.a(nativeDialogManager.e, i2);
    }
}
