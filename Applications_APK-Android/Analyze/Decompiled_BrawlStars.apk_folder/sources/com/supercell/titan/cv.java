package com.supercell.titan;

/* compiled from: NativeFacebookRequestLinkStatisticsCallback */
class cv implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f5096a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ cu f5097b;

    cv(cu cuVar, boolean z) {
        this.f5097b = cuVar;
        this.f5096a = z;
    }

    public void run() {
        NativeFacebookManager.facebookLinkStatistics(this.f5096a, Math.max(this.f5097b.f5094a, this.f5097b.f5095b), this.f5097b.c.f5093a);
    }
}
