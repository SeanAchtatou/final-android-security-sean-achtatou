package com.supercell.titan;

/* compiled from: NativeFacebookRequestFriendInfoCallback */
class cq implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5088a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ cp f5089b;

    cq(cp cpVar, String str) {
        this.f5089b = cpVar;
        this.f5088a = str;
    }

    public void run() {
        NativeFacebookManager.facebookUserInfo(this.f5088a);
    }
}
