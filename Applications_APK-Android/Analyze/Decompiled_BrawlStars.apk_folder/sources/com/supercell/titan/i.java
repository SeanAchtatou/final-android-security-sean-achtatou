package com.supercell.titan;

import android.content.Context;
import android.support.v4.view.InputDeviceCompat;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/* compiled from: GL2JNISurfaceView */
public class i extends SurfaceView implements SurfaceHolder.Callback {

    /* renamed from: a  reason: collision with root package name */
    private boolean f5185a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f5186b;
    private Surface c;
    private final Set<Integer> d = new HashSet(10);
    private final HashMap<Integer, Object> e = new HashMap<>();

    /* compiled from: GL2JNISurfaceView */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public int f5187a;

        /* renamed from: b  reason: collision with root package name */
        public int f5188b;
        public int c;
        public int d;
    }

    public i(Context context) {
        super(context);
        this.f5185a = false;
        this.f5186b = false;
        getHolder().addCallback(this);
        for (int i = 0; i < 10; i++) {
            this.d.add(Integer.valueOf(i));
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.f5185a = true;
        if (GameApp.isNativeLibraryLoaded()) {
            GameApp.nOnSurfaceCreated(surfaceHolder.getSurface());
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        this.c = surfaceHolder.getSurface();
        if (GameApp.isNativeLibraryLoaded()) {
            GameApp.nOnSurfaceChanged(this.c, i2, i3);
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (GameApp.isNativeLibraryLoaded()) {
            GameApp.nOnSurfaceDestroyed(this.c);
        }
        this.f5185a = false;
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        if ((motionEvent.getSource() & InputDeviceCompat.SOURCE_JOYSTICK) != 16777232 || motionEvent.getAction() != 2) {
            return super.onGenericMotionEvent(motionEvent);
        }
        motionEvent.getHistorySize();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        if (r3 != 6) goto L_0x0085;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            boolean r0 = com.supercell.titan.GameApp.isNativeLibraryLoaded()
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            com.supercell.titan.i$a r0 = new com.supercell.titan.i$a
            r0.<init>()
            int r2 = r8.getActionIndex()
            int r3 = r8.getActionMasked()
            int r4 = r8.getPointerId(r2)
            r5 = 1
            if (r3 == 0) goto L_0x0073
            if (r3 == r5) goto L_0x0060
            r6 = 2
            if (r3 == r6) goto L_0x0030
            r6 = 3
            if (r3 == r6) goto L_0x002b
            r6 = 5
            if (r3 == r6) goto L_0x0073
            r1 = 6
            if (r3 == r1) goto L_0x0060
            goto L_0x0085
        L_0x002b:
            r0.c = r6
            r0.d = r4
            goto L_0x0085
        L_0x0030:
            int r2 = r8.getPointerCount()
            if (r1 >= r2) goto L_0x0085
            com.supercell.titan.i$a r0 = new com.supercell.titan.i$a
            r0.<init>()
            r0.c = r6
            int r2 = r8.getPointerId(r1)
            r0.d = r2
            float r2 = r8.getX(r1)
            int r2 = (int) r2
            r0.f5187a = r2
            float r2 = r8.getY(r1)
            int r2 = (int) r2
            r0.f5188b = r2
            int r2 = r0.c
            int r3 = r0.f5187a
            int r4 = r0.f5188b
            int r0 = r0.d
            com.supercell.titan.GameApp.nOnTouchEvent(r2, r3, r4, r0)
            r0 = 0
            int r1 = r1 + 1
            goto L_0x0030
        L_0x0060:
            r0.c = r5
            float r1 = r8.getX(r2)
            int r1 = (int) r1
            r0.f5187a = r1
            float r8 = r8.getY(r2)
            int r8 = (int) r8
            r0.f5188b = r8
            r0.d = r4
            goto L_0x0085
        L_0x0073:
            r0.c = r1
            float r1 = r8.getX(r2)
            int r1 = (int) r1
            r0.f5187a = r1
            float r8 = r8.getY(r2)
            int r8 = (int) r8
            r0.f5188b = r8
            r0.d = r4
        L_0x0085:
            if (r0 == 0) goto L_0x0092
            int r8 = r0.c
            int r1 = r0.f5187a
            int r2 = r0.f5188b
            int r0 = r0.d
            com.supercell.titan.GameApp.nOnTouchEvent(r8, r1, r2, r0)
        L_0x0092:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.titan.i.onTouchEvent(android.view.MotionEvent):boolean");
    }
}
