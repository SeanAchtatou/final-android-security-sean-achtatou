package com.supercell.titan;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.o;
import com.google.firebase.messaging.FirebaseMessagingService;

public class PushMessageService extends FirebaseMessagingService {
    public static void onDestroy(GameApp gameApp) {
    }

    public static void register() {
        requestToken();
    }

    public static void requestToken() {
        if (GameApp.getInstance() != null) {
            FirebaseInstanceId a2 = FirebaseInstanceId.a();
            a2.a(o.a(a2.c), "*").a(GameApp.getInstance(), new dh());
        }
    }

    public final void a(String str) {
        super.a(str);
        "FCM onNewToken: " + str;
        GameApp.queuePushNotificationValueUpdate(0, str, "");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.google.firebase.messaging.RemoteMessage r14) {
        /*
            r13 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "FCM Message from: "
            r0.append(r1)
            android.os.Bundle r1 = r14.f2837a
            java.lang.String r2 = "from"
            java.lang.String r1 = r1.getString(r2)
            r0.append(r1)
            r0.toString()
            java.util.Map r0 = r14.a()
            java.lang.String r1 = "origin"
            boolean r2 = r0.containsKey(r1)
            if (r2 == 0) goto L_0x006b
            java.lang.Object r1 = r0.get(r1)
            java.lang.String r2 = "helpshift"
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x006b
            com.supercell.titan.GameApp r14 = com.supercell.titan.GameApp.getInstance()
            if (r14 != 0) goto L_0x0037
            return
        L_0x0037:
            android.content.res.Resources r14 = r13.getResources()
            int r1 = com.supercell.titan.R.string.helpshift_apiKey
            java.lang.String r14 = r14.getString(r1)
            android.content.res.Resources r1 = r13.getResources()
            int r2 = com.supercell.titan.R.string.helpshift_domain
            java.lang.String r1 = r1.getString(r2)
            android.content.res.Resources r2 = r13.getResources()
            int r3 = com.supercell.titan.R.string.helpshift_appId
            java.lang.String r2 = r2.getString(r3)
            com.supercell.titan.HelpshiftTitan.callInit()
            com.supercell.titan.HelpshiftTitan.start(r14, r1, r2)
            com.supercell.titan.GameApp r14 = com.supercell.titan.GameApp.getInstance()
            com.supercell.titan.di r1 = new com.supercell.titan.di
            r1.<init>(r13, r13, r0)
            r14.runOnUiThread(r1)
            com.supercell.titan.HelpshiftTitan.requestNotificationCount()
            return
        L_0x006b:
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x0081
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Message data payload: "
            r1.append(r2)
            r1.append(r0)
            r1.toString()
        L_0x0081:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Notification Message Body: "
            r1.append(r2)
            com.google.firebase.messaging.RemoteMessage$a r2 = r14.b()
            java.lang.String r2 = r2.f2840b
            r1.append(r2)
            r1.toString()
            java.lang.String r1 = "userId"
            java.lang.Object r1 = r0.get(r1)
            r6 = r1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.String r1 = "channelId"
            java.lang.Object r1 = r0.get(r1)
            r8 = r1
            java.lang.String r8 = (java.lang.String) r8
            java.lang.String r1 = "channelName"
            java.lang.Object r1 = r0.get(r1)
            r9 = r1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.String r1 = "channelDesc"
            java.lang.Object r1 = r0.get(r1)
            r10 = r1
            java.lang.String r10 = (java.lang.String) r10
            java.lang.String r1 = "imageURL"
            java.lang.Object r1 = r0.get(r1)
            r7 = r1
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r1 = "color"
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            r1 = 0
            if (r0 == 0) goto L_0x00d5
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x00e2 }
        L_0x00d3:
            r11 = r0
            goto L_0x00e3
        L_0x00d5:
            com.google.firebase.messaging.RemoteMessage$a r0 = r14.b()
            java.lang.String r0 = r0.d
            if (r0 == 0) goto L_0x00e2
            int r0 = android.graphics.Color.parseColor(r0)     // Catch:{  }
            goto L_0x00d3
        L_0x00e2:
            r11 = 0
        L_0x00e3:
            android.content.Context r2 = r13.getApplicationContext()
            com.google.firebase.messaging.RemoteMessage$a r0 = r14.b()
            java.lang.String r3 = r0.f2840b
            com.google.firebase.messaging.RemoteMessage$a r0 = r14.b()
            java.lang.String r4 = r0.f2839a
            com.google.firebase.messaging.RemoteMessage$a r14 = r14.b()
            java.lang.String r5 = r14.c
            java.lang.Class r12 = r13.a()
            com.supercell.titan.TimeAlarm.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.titan.PushMessageService.a(com.google.firebase.messaging.RemoteMessage):void");
    }

    private Class a() {
        try {
            return Class.forName(getApplicationContext().getPackageManager().getLaunchIntentForPackage(getApplicationContext().getPackageName()).getComponent().getClassName());
        } catch (Exception unused) {
            return null;
        }
    }
}
