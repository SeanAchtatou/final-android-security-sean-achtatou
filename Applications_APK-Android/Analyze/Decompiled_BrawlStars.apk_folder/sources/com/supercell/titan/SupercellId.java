package com.supercell.titan;

import com.supercell.id.IdAccount;
import com.supercell.id.IdConfiguration;
import com.supercell.id.IdFriend;
import com.supercell.id.IdIngameFriend;
import com.supercell.id.IdPendingLogin;
import com.supercell.id.IdPendingRegistration;
import com.supercell.id.SupercellIdDelegate;

public class SupercellId implements SupercellIdDelegate {

    /* renamed from: a  reason: collision with root package name */
    private static SupercellId f4992a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public long f4993b = 0;

    public native void avatarImageData(byte[] bArr, int i, int i2, int i3, String str);

    public native void bindAccount(String str, String str2, String str3, String str4, boolean z);

    public native void clearPendingLogin();

    public native void clearPendingRegistration();

    public native void forgetAccount(String str, String str2);

    public native void friends(IdFriend[] idFriendArr);

    public native void friendsChanged(IdFriend[] idFriendArr);

    public native void friendsFailed();

    public native IdAccount[] getAccounts();

    public native IdConfiguration getConfig();

    public native IdAccount getCurrentAccount();

    public native IdIngameFriend[] getIngameFriends();

    public native IdPendingLogin getPendingLogin();

    public native IdPendingRegistration getPendingRegistration();

    public native boolean isSelfHelpPortalAvailable();

    public native boolean isTutorialComplete();

    public native boolean isWindowOpen();

    public native void loadAccount(String str, String str2, String str3, boolean z);

    public native void logOut();

    public native void openSelfHelpPortal();

    public native void setPendingLoginWithEmail(String str, boolean z);

    public native void setPendingLoginWithPhone(String str, boolean z);

    public native void setPendingRegistrationWithEmail(String str, boolean z);

    public native void setPendingRegistrationWithPhone(String str);

    public native void setTutorialComplete();

    public native void windowDidDismiss();

    public void init(boolean z) {
        f4992a = this;
        GameApp instance = GameApp.getInstance();
        instance.runOnUiThread(new dj(this, instance, this));
    }

    public void preload() {
        GameApp.getInstance().runOnUiThread(new dl(this));
    }

    public void requestFriends() {
        GameApp.getInstance().runOnUiThread(new dm(this));
    }

    public void openWindow(String str, String str2) {
        GameApp instance = GameApp.getInstance();
        instance.runOnUiThread(new dn(this, str, instance, str2));
    }

    public void closeWindow() {
        GameApp.getInstance().runOnUiThread(new Cdo(this));
    }

    public void accountBound(String str) {
        GameApp.getInstance().runOnUiThread(new dp(this, str));
    }

    public void accountAlreadyBound() {
        GameApp.getInstance().runOnUiThread(new dq(this));
    }

    public void accountBindingFailed() {
        GameApp.getInstance().runOnUiThread(new dr(this));
    }

    public void close() {
        f4992a = null;
        GameApp.getInstance().runOnUiThread(new ds(this));
    }

    public String getVersionString() {
        return com.supercell.id.SupercellId.INSTANCE.getVersionString();
    }

    public void requestImageDataForAvatarString(String str) {
        GameApp.getInstance().runOnUiThread(new dk(this, str));
    }

    public long getObjPtr() {
        return this.f4993b;
    }

    public void setObjPtr(long j) {
        this.f4993b = j;
    }

    public static SupercellId getInstance() {
        return f4992a;
    }
}
