package com.supercell.titan;

import android.os.Handler;
import android.os.Message;
import com.supercell.titan.cz;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NativeHTTPClientManager {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Vector<cz> f4982a = new Vector<>();

    /* renamed from: b  reason: collision with root package name */
    private static int f4983b = 1234;
    private static final NativeHTTPClientManager e = new NativeHTTPClientManager();
    private final a c = new a((byte) 0);
    private final ExecutorService d = Executors.newCachedThreadPool();

    public static native void getFinished(boolean z, int i, byte[] bArr, int i2);

    public static native void postFinished(boolean z, int i, byte[] bArr, int i2);

    static class a extends Handler {
        private a() {
        }

        /* synthetic */ a(byte b2) {
            this();
        }

        public void handleMessage(Message message) {
            try {
                NativeHTTPClientManager.f4982a.add((cz) message.obj);
            } catch (Exception e) {
                GameApp.debuggerException(e);
            }
        }
    }

    public static void updateBeforeFrame() {
        int size = f4982a.size();
        for (int i = 0; i < size; i++) {
            cz remove = f4982a.remove(0);
            boolean z = remove.f == cz.b.OK;
            int i2 = remove.f5102a;
            byte[] bArr = remove.g;
            if (remove.d == cz.a.GET) {
                getFinished(z, i2, bArr, remove.h);
            } else {
                postFinished(z, i2, bArr, remove.h);
            }
        }
    }

    private NativeHTTPClientManager() {
    }

    public static NativeHTTPClientManager getInstance() {
        return e;
    }

    private void a(Runnable runnable) {
        this.d.execute(runnable);
    }

    public static int startGetRequest(String str, String str2, String str3, String str4) {
        NativeHTTPClientManager instance = getInstance();
        cz czVar = new cz(instance.c, str3, str4);
        czVar.d = cz.a.GET;
        czVar.f5103b = str;
        czVar.e = null;
        czVar.c = str2;
        int i = f4983b;
        f4983b = i + 1;
        czVar.f5102a = i;
        instance.a(czVar);
        return czVar.f5102a;
    }

    public static int startPostRequest(String str, String str2, byte[] bArr) {
        NativeHTTPClientManager instance = getInstance();
        cz czVar = new cz(instance.c, "", "");
        czVar.d = cz.a.POST;
        czVar.f5103b = str;
        czVar.e = bArr;
        czVar.c = str2;
        int i = f4983b;
        f4983b = i + 1;
        czVar.f5102a = i;
        instance.a(czVar);
        return czVar.f5102a;
    }
}
