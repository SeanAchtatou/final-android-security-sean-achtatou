package com.supercell.titan;

import android.os.Handler;
import android.os.Message;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: NativeHTTPConnection */
public class cz implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public int f5102a;

    /* renamed from: b  reason: collision with root package name */
    String f5103b;
    String c;
    a d;
    byte[] e;
    b f;
    byte[] g;
    int h;
    private final String i;
    private final Handler j;
    private final String k;

    /* compiled from: NativeHTTPConnection */
    public enum a {
        GET,
        POST
    }

    /* compiled from: NativeHTTPConnection */
    public enum b {
        UNDEFINED(-1),
        IDLE(0),
        ACTIVE(1),
        OK(2),
        FAILED(3);
        
        private final int f;

        private b(int i) {
            this.f = i;
        }
    }

    public cz() {
        this(new Handler(), "", "");
    }

    public cz(Handler handler, String str, String str2) {
        this.f5103b = "";
        this.c = "";
        this.f = b.IDLE;
        this.j = handler;
        this.i = str;
        this.k = str2;
    }

    private static MessageDigest a() {
        try {
            return MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e2) {
            GameApp.debuggerException(e2);
            return null;
        }
    }

    private boolean b() throws IOException {
        HttpURLConnection a2 = a(this.f5103b);
        a2.setDoOutput(true);
        a2.setDoInput(true);
        a2.setRequestMethod("POST");
        a2.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        try {
            a2.setRequestProperty("X-timestamp", this.c);
            a2.setFixedLengthStreamingMode(this.e.length);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(a2.getOutputStream());
            bufferedOutputStream.write(this.e);
            bufferedOutputStream.close();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(a2.getInputStream());
            this.g = a(bufferedInputStream);
            bufferedInputStream.close();
            this.h = a2.getResponseCode();
            return true;
        } finally {
            a2.disconnect();
        }
    }

    private static HttpURLConnection a(String str) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setConnectTimeout(15000);
        httpURLConnection.setRequestProperty("charset", "utf-8");
        httpURLConnection.setRequestProperty("Connection", "close");
        return httpURLConnection;
    }

    private static boolean a(File file, File file2) {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read > 0) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileInputStream.close();
                    fileOutputStream.close();
                    return true;
                }
            }
        } catch (Exception unused) {
            file2.delete();
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b4 A[Catch:{ Exception -> 0x00ad, all -> 0x00df }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00cd A[Catch:{ Exception -> 0x00ad, all -> 0x00df }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00db A[SYNTHETIC, Splitter:B:55:0x00db] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean c() throws java.io.IOException {
        /*
            r11 = this;
            java.lang.String r0 = r11.f5103b
            java.net.HttpURLConnection r0 = a(r0)
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ all -> 0x00df }
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ all -> 0x00df }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ all -> 0x00df }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ all -> 0x00df }
            r2.<init>(r3)     // Catch:{ all -> 0x00df }
            int r3 = r0.getResponseCode()     // Catch:{ all -> 0x00df }
            r11.h = r3     // Catch:{ all -> 0x00df }
            java.lang.String r3 = r11.k     // Catch:{ all -> 0x00df }
            boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x00df }
            if (r3 == 0) goto L_0x002e
            byte[] r2 = a(r2)     // Catch:{ all -> 0x00df }
            r11.g = r2     // Catch:{ all -> 0x00df }
            goto L_0x00d1
        L_0x002e:
            java.lang.String r3 = r11.i     // Catch:{ all -> 0x00df }
            boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x00df }
            r4 = 0
            if (r3 != 0) goto L_0x003c
            java.security.MessageDigest r3 = a()     // Catch:{ all -> 0x00df }
            goto L_0x003d
        L_0x003c:
            r3 = r4
        L_0x003d:
            com.supercell.titan.GameApp r5 = com.supercell.titan.GameApp.getInstance()     // Catch:{ all -> 0x00df }
            java.lang.String r6 = "download"
            java.lang.String r7 = ".tmp"
            java.io.File r5 = r5.getCacheDir()     // Catch:{ all -> 0x00df }
            java.io.File r5 = java.io.File.createTempFile(r6, r7, r5)     // Catch:{ all -> 0x00df }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ all -> 0x00d7 }
            r6.<init>(r5)     // Catch:{ all -> 0x00d7 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r4]     // Catch:{ all -> 0x00d5 }
        L_0x0056:
            r8 = 0
            int r9 = r2.read(r7, r8, r4)     // Catch:{ all -> 0x00d5 }
            r10 = -1
            if (r9 == r10) goto L_0x0067
            r6.write(r7, r8, r9)     // Catch:{ all -> 0x00d5 }
            if (r3 == 0) goto L_0x0056
            r3.update(r7, r8, r9)     // Catch:{ all -> 0x00d5 }
            goto L_0x0056
        L_0x0067:
            r6.close()     // Catch:{ all -> 0x00df }
            java.lang.String r2 = r11.i     // Catch:{ all -> 0x00df }
            boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x00df }
            if (r2 != 0) goto L_0x00b1
            if (r3 == 0) goto L_0x00b1
            byte[] r2 = r3.digest()     // Catch:{ Exception -> 0x00ad }
            if (r2 != 0) goto L_0x007b
            goto L_0x00b1
        L_0x007b:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ad }
            r4 = 40
            r3.<init>(r4)     // Catch:{ Exception -> 0x00ad }
            int r4 = r2.length     // Catch:{ Exception -> 0x00ad }
            r6 = 0
        L_0x0084:
            if (r6 >= r4) goto L_0x009e
            byte r7 = r2[r6]     // Catch:{ Exception -> 0x00ad }
            java.lang.String r9 = "%02x"
            java.lang.Object[] r10 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x00ad }
            r7 = r7 & 255(0xff, float:3.57E-43)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x00ad }
            r10[r8] = r7     // Catch:{ Exception -> 0x00ad }
            java.lang.String r7 = java.lang.String.format(r9, r10)     // Catch:{ Exception -> 0x00ad }
            r3.append(r7)     // Catch:{ Exception -> 0x00ad }
            int r6 = r6 + 1
            goto L_0x0084
        L_0x009e:
            java.lang.String r2 = r3.toString()     // Catch:{ Exception -> 0x00ad }
            java.lang.String r3 = r11.i     // Catch:{ Exception -> 0x00ad }
            boolean r2 = r2.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x00ad }
            if (r2 == 0) goto L_0x00ab
            goto L_0x00b1
        L_0x00ab:
            r2 = 0
            goto L_0x00b2
        L_0x00ad:
            r2 = move-exception
            com.supercell.titan.GameApp.debuggerException(r2)     // Catch:{ all -> 0x00df }
        L_0x00b1:
            r2 = 1
        L_0x00b2:
            if (r2 == 0) goto L_0x00cb
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x00df }
            java.lang.String r4 = r11.k     // Catch:{ all -> 0x00df }
            r3.<init>(r4)     // Catch:{ all -> 0x00df }
            boolean r4 = r5.renameTo(r3)     // Catch:{ all -> 0x00df }
            if (r4 != 0) goto L_0x00ca
            boolean r3 = a(r5, r3)     // Catch:{ all -> 0x00df }
            if (r3 == 0) goto L_0x00c8
            goto L_0x00cb
        L_0x00c8:
            r2 = 0
            goto L_0x00cb
        L_0x00ca:
            r1 = 0
        L_0x00cb:
            if (r1 == 0) goto L_0x00d0
            r5.delete()     // Catch:{ all -> 0x00df }
        L_0x00d0:
            r1 = r2
        L_0x00d1:
            r0.disconnect()
            return r1
        L_0x00d5:
            r1 = move-exception
            goto L_0x00d9
        L_0x00d7:
            r1 = move-exception
            r6 = r4
        L_0x00d9:
            if (r6 == 0) goto L_0x00de
            r6.close()     // Catch:{ all -> 0x00df }
        L_0x00de:
            throw r1     // Catch:{ all -> 0x00df }
        L_0x00df:
            r1 = move-exception
            r0.disconnect()
            goto L_0x00e5
        L_0x00e4:
            throw r1
        L_0x00e5:
            goto L_0x00e4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.titan.cz.c():boolean");
    }

    public void run() {
        this.f = b.ACTIVE;
        boolean z = false;
        if (this.d == a.GET) {
            try {
                z = c();
            } catch (IOException e2) {
                GameApp.debuggerException(e2);
            }
            if (z) {
                this.f = b.OK;
            } else {
                this.f = b.FAILED;
            }
        } else if (this.d == a.POST) {
            try {
                z = b();
            } catch (IOException unused) {
            }
            if (z) {
                this.f = b.OK;
            } else {
                this.f = b.FAILED;
            }
        } else {
            this.f = b.FAILED;
        }
        Message message = new Message();
        message.obj = this;
        this.j.sendMessage(message);
    }

    private static byte[] a(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr, 0, 1024);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }
}
