package com.supercell.titan;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import org.json.JSONObject;

/* compiled from: NativeFacebookRequestWhoAmI */
class cx implements GraphRequest.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cw f5099a;

    cx(cw cwVar) {
        this.f5099a = cwVar;
    }

    public void onCompleted(GraphResponse graphResponse) {
        JSONObject jSONObject = graphResponse.getJSONObject();
        if (jSONObject != null) {
            String jSONObject2 = jSONObject.toString();
            if (AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired()) {
                String token = AccessToken.getCurrentAccessToken().getToken();
                if (token == null) {
                    token = "";
                }
                this.f5099a.f5098a.a(new cy(this, jSONObject2, token));
            }
        }
    }
}
