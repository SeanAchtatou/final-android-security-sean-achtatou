package com.supercell.titan;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;

class HelpshiftTitan$1$2 implements LifecycleObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f4973a;

    HelpshiftTitan$1$2(am amVar) {
        this.f4973a = amVar;
    }

    /* access modifiers changed from: package-private */
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onBackground() {
        try {
            Class.forName("com.helpshift.CoreInternal").getMethod("onAppBackground", new Class[0]).invoke(null, new Object[0]);
        } catch (ClassNotFoundException | Exception | NoSuchMethodException unused) {
        }
    }

    /* access modifiers changed from: package-private */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onForeground() {
        try {
            Class.forName("com.helpshift.CoreInternal").getMethod("onAppForeground", new Class[0]).invoke(null, new Object[0]);
        } catch (ClassNotFoundException | Exception | NoSuchMethodException unused) {
        }
    }
}
