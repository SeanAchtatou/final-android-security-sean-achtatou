package com.supercell.titan;

/* compiled from: PurchaseManagerGoogle */
class df extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5115a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f5116b;
    final /* synthetic */ da c;

    df(da daVar, String str, String str2) {
        this.c = daVar;
        this.f5115a = str;
        this.f5116b = str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            com.supercell.titan.PurchaseManager$a r0 = new com.supercell.titan.PurchaseManager$a
            r0.<init>()
            java.lang.String r1 = r12.f5115a
            r0.f4986a = r1
            r2 = 1
            r3 = 0
            com.supercell.titan.da r4 = r12.c     // Catch:{ Exception -> 0x0079 }
            android.os.Bundle r1 = r4.g(r1)     // Catch:{ Exception -> 0x0079 }
            if (r1 != 0) goto L_0x0030
            com.supercell.titan.da r1 = r12.c     // Catch:{ Exception -> 0x0079 }
            com.android.vending.billing.IInAppBillingService r4 = r1.r     // Catch:{ Exception -> 0x0079 }
            r5 = 3
            com.supercell.titan.da r1 = r12.c     // Catch:{ Exception -> 0x0079 }
            com.supercell.titan.GameApp r1 = r1.j     // Catch:{ Exception -> 0x0079 }
            java.lang.String r6 = r1.getPackageName()     // Catch:{ Exception -> 0x0079 }
            java.lang.String r7 = r12.f5115a     // Catch:{ Exception -> 0x0079 }
            java.lang.String r8 = r12.f5116b     // Catch:{ Exception -> 0x0079 }
            com.supercell.titan.da r1 = r12.c     // Catch:{ Exception -> 0x0079 }
            java.lang.String r9 = r1.z     // Catch:{ Exception -> 0x0079 }
            android.os.Bundle r1 = r4.a(r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0079 }
        L_0x0030:
            com.supercell.titan.da r4 = r12.c     // Catch:{ Exception -> 0x0079 }
            java.lang.String r5 = "RESPONSE_CODE"
            int r4 = com.supercell.titan.da.a(r1, r5)     // Catch:{ Exception -> 0x0079 }
            r0.c = r4     // Catch:{ Exception -> 0x0079 }
            if (r4 != 0) goto L_0x0072
            java.lang.String r4 = "BUY_INTENT"
            android.os.Parcelable r1 = r1.getParcelable(r4)     // Catch:{ Exception -> 0x0079 }
            android.app.PendingIntent r1 = (android.app.PendingIntent) r1     // Catch:{ Exception -> 0x0079 }
            if (r1 == 0) goto L_0x0083
            com.supercell.titan.da r4 = r12.c     // Catch:{ SendIntentException -> 0x0067 }
            com.supercell.titan.GameApp r5 = r4.j     // Catch:{ SendIntentException -> 0x0067 }
            android.content.IntentSender r6 = r1.getIntentSender()     // Catch:{ SendIntentException -> 0x0067 }
            r7 = 10000004(0x989684, float:1.401299E-38)
            android.content.Intent r8 = new android.content.Intent     // Catch:{ SendIntentException -> 0x0067 }
            r8.<init>()     // Catch:{ SendIntentException -> 0x0067 }
            r9 = 0
            r10 = 0
            r11 = 0
            r5.startIntentSenderForResult(r6, r7, r8, r9, r10, r11)     // Catch:{ SendIntentException -> 0x0067 }
            com.supercell.titan.da r1 = r12.c     // Catch:{ SendIntentException -> 0x0067 }
            java.util.Vector r1 = r1.f4984a     // Catch:{ SendIntentException -> 0x0067 }
            java.lang.String r4 = r12.f5115a     // Catch:{ SendIntentException -> 0x0067 }
            r1.add(r4)     // Catch:{ SendIntentException -> 0x0067 }
            r1 = 1
            goto L_0x0084
        L_0x0067:
            r1 = move-exception
            java.lang.String r4 = r1.getMessage()     // Catch:{ Exception -> 0x0079 }
            r0.f4987b = r4     // Catch:{ Exception -> 0x0079 }
            com.supercell.titan.GameApp.debuggerException(r1)     // Catch:{ Exception -> 0x0079 }
            goto L_0x0083
        L_0x0072:
            java.lang.String r1 = com.supercell.titan.da.c(r4)     // Catch:{ Exception -> 0x0079 }
            r0.f4987b = r1     // Catch:{ Exception -> 0x0079 }
            goto L_0x0083
        L_0x0079:
            r1 = move-exception
            java.lang.String r4 = r1.getMessage()
            r0.f4987b = r4
            com.supercell.titan.GameApp.debuggerException(r1)
        L_0x0083:
            r1 = 0
        L_0x0084:
            if (r1 != 0) goto L_0x00a2
            com.supercell.titan.da r1 = r12.c
            monitor-enter(r1)
            com.supercell.titan.da r4 = r12.c     // Catch:{ all -> 0x009f }
            com.supercell.titan.da r5 = r12.c     // Catch:{ all -> 0x009f }
            int r5 = r5.k     // Catch:{ all -> 0x009f }
            int r5 = r5 - r2
            int r2 = java.lang.Math.max(r5, r3)     // Catch:{ all -> 0x009f }
            r4.k = r2     // Catch:{ all -> 0x009f }
            monitor-exit(r1)     // Catch:{ all -> 0x009f }
            com.supercell.titan.da r1 = r12.c
            java.util.Vector r1 = r1.c
            r1.add(r0)
            goto L_0x00a2
        L_0x009f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x009f }
            throw r0
        L_0x00a2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.titan.df.run():void");
    }
}
