package com.supercell.titan;

import android.content.ClipboardManager;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.Callable;

/* compiled from: ApplicationUtilBase */
final class c implements Callable<String> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5065a;

    c(GameApp gameApp) {
        this.f5065a = gameApp;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String call() throws Exception {
        try {
            ClipboardManager clipboardManager = (ClipboardManager) this.f5065a.getSystemService("clipboard");
            if (!clipboardManager.hasPrimaryClip() || clipboardManager.getPrimaryClip().getItemCount() <= 0) {
                return "";
            }
            String charSequence = clipboardManager.getPrimaryClip().getItemAt(0).getText().toString();
            try {
                charSequence.getBytes("UTF-8");
                return charSequence;
            } catch (UnsupportedEncodingException unused) {
                return "";
            }
        } catch (Exception e) {
            GameApp.debuggerException(e);
            return "";
        }
    }
}
