package com.supercell.titan;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

/* compiled from: NativeFacebookRequestFriendInfoCallback */
public class cp implements GraphRequest.Callback {

    /* renamed from: a  reason: collision with root package name */
    private final GameApp f5087a;

    public cp(GameApp gameApp) {
        this.f5087a = gameApp;
    }

    public void onCompleted(GraphResponse graphResponse) {
        String jSONObject = graphResponse.getJSONObject() != null ? graphResponse.getJSONObject().toString() : "";
        if (graphResponse.getError() == null) {
            this.f5087a.a(new cq(this, jSONObject));
        }
    }
}
