package com.supercell.titan;

import com.microsoft.appcenter.f;
import com.microsoft.appcenter.p;

public class AppCenter {
    public static void setStringCustomProperty(String str, String str2) {
        p pVar = new p();
        pVar.a(str, str2);
        f.a().a(pVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.p.a(java.lang.String, java.lang.Number):com.microsoft.appcenter.p
     arg types: [java.lang.String, java.lang.Float]
     candidates:
      com.microsoft.appcenter.p.a(java.lang.String, java.lang.Object):void
      com.microsoft.appcenter.p.a(java.lang.String, java.lang.String):com.microsoft.appcenter.p
      com.microsoft.appcenter.p.a(java.lang.String, java.lang.Number):com.microsoft.appcenter.p */
    public static void setFloatCustomProperty(String str, float f) {
        p pVar = new p();
        pVar.a(str, (Number) Float.valueOf(f));
        f.a().a(pVar);
    }

    public static void setUserId(String str) {
        f.a().a(str);
    }
}
