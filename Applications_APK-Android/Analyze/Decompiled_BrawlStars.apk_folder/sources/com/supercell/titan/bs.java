package com.supercell.titan;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import java.util.Iterator;
import java.util.Set;

/* compiled from: NativeFacebookManager */
class bs implements FacebookCallback<LoginResult> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5060a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ NativeFacebookManager f5061b;

    bs(NativeFacebookManager nativeFacebookManager, GameApp gameApp) {
        this.f5061b = nativeFacebookManager;
        this.f5060a = gameApp;
    }

    public /* synthetic */ void onSuccess(Object obj) {
        LoginResult loginResult = (LoginResult) obj;
        if (loginResult.getAccessToken() != null) {
            Set<String> recentlyDeniedPermissions = loginResult.getRecentlyDeniedPermissions();
            Iterator it = this.f5061b.d.iterator();
            while (it.hasNext()) {
                if (recentlyDeniedPermissions.contains((String) it.next())) {
                    LoginManager.getInstance().logInWithReadPermissions(this.f5060a, this.f5061b.d);
                }
            }
        }
        if (this.f5061b.e != 0) {
            NativeFacebookManager nativeFacebookManager = this.f5061b;
            nativeFacebookManager.a(nativeFacebookManager.e);
        }
    }

    public void onCancel() {
        this.f5060a.a(new bt(this));
    }

    public void onError(FacebookException facebookException) {
        this.f5060a.a(new bu(this));
    }
}
