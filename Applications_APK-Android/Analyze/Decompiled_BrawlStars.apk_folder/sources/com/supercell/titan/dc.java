package com.supercell.titan;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.android.vending.billing.IInAppBillingService;

/* compiled from: PurchaseManagerGoogle */
class dc implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ da f5111a;

    dc(da daVar) {
        this.f5111a = daVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.supercell.titan.da.a(com.supercell.titan.da, boolean):boolean
     arg types: [com.supercell.titan.da, int]
     candidates:
      com.supercell.titan.da.a(android.os.Bundle, java.lang.String):int
      com.supercell.titan.da.a(com.supercell.titan.da, int):int
      com.supercell.titan.da.a(com.supercell.titan.da, com.android.vending.billing.IInAppBillingService):com.android.vending.billing.IInAppBillingService
      com.supercell.titan.da.a(com.supercell.titan.da, java.lang.String):java.lang.String
      com.supercell.titan.da.a(com.supercell.titan.da, com.supercell.titan.PurchaseManager$b):void
      com.supercell.titan.da.a(com.supercell.titan.da, org.json.JSONArray):void
      com.supercell.titan.da.a(int, android.content.Intent):void
      com.supercell.titan.da.a(java.lang.String, java.lang.String):void
      com.supercell.titan.PurchaseManager.a(int, android.content.Intent):void
      com.supercell.titan.PurchaseManager.a(java.lang.String, java.lang.String):void
      com.supercell.titan.da.a(com.supercell.titan.da, boolean):boolean */
    public void onServiceDisconnected(ComponentName componentName) {
        IInAppBillingService unused = this.f5111a.r = (IInAppBillingService) null;
        boolean unused2 = this.f5111a.x = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.supercell.titan.da.a(com.supercell.titan.da, boolean):boolean
     arg types: [com.supercell.titan.da, int]
     candidates:
      com.supercell.titan.da.a(android.os.Bundle, java.lang.String):int
      com.supercell.titan.da.a(com.supercell.titan.da, int):int
      com.supercell.titan.da.a(com.supercell.titan.da, com.android.vending.billing.IInAppBillingService):com.android.vending.billing.IInAppBillingService
      com.supercell.titan.da.a(com.supercell.titan.da, java.lang.String):java.lang.String
      com.supercell.titan.da.a(com.supercell.titan.da, com.supercell.titan.PurchaseManager$b):void
      com.supercell.titan.da.a(com.supercell.titan.da, org.json.JSONArray):void
      com.supercell.titan.da.a(int, android.content.Intent):void
      com.supercell.titan.da.a(java.lang.String, java.lang.String):void
      com.supercell.titan.PurchaseManager.a(int, android.content.Intent):void
      com.supercell.titan.PurchaseManager.a(java.lang.String, java.lang.String):void
      com.supercell.titan.da.a(com.supercell.titan.da, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.supercell.titan.da.b(com.supercell.titan.da, boolean):boolean
     arg types: [com.supercell.titan.da, int]
     candidates:
      com.supercell.titan.da.b(com.supercell.titan.da, java.lang.String):android.os.Bundle
      com.supercell.titan.da.b(com.supercell.titan.da, boolean):boolean */
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        IInAppBillingService unused = this.f5111a.r = IInAppBillingService.Stub.a(iBinder);
        boolean unused2 = this.f5111a.x = true;
        if (this.f5111a.r == null) {
            boolean unused3 = this.f5111a.y = false;
        } else {
            boolean unused4 = this.f5111a.y = true;
        }
        GameApp.getInstance().a(new dd(this));
    }
}
