package com.supercell.titan;

import com.supercell.id.SupercellId;

/* compiled from: SupercellId */
class dk implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5123a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SupercellId f5124b;

    dk(SupercellId supercellId, String str) {
        this.f5124b = supercellId;
        this.f5123a = str;
    }

    public void run() {
        SupercellId.INSTANCE.requestImageDataForAvatarString(this.f5123a);
    }
}
