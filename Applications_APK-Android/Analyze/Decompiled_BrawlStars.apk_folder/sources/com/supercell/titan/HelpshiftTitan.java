package com.supercell.titan;

import android.arch.lifecycle.LifecycleObserver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import com.helpshift.CoreInternal;
import com.helpshift.support.ab;
import com.helpshift.support.b;
import com.helpshift.support.g;
import com.helpshift.support.v;
import com.helpshift.support.z;
import com.helpshift.util.a.b;
import com.helpshift.util.q;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class HelpshiftTitan {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static int f4971a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f4972b;
    /* access modifiers changed from: private */
    public static final HashMap<String, Object> c = new HashMap<>();
    private static final Map<String, String[]> d = new HashMap();
    /* access modifiers changed from: private */
    public static String[] e = new String[0];
    private static Map<String, Object> f = new HashMap();
    /* access modifiers changed from: private */
    public static String[] g = new String[0];
    private static String[] h = new String[0];
    /* access modifiers changed from: private */
    public static boolean i;
    private static int j = z.b.c.intValue();
    private static boolean k = false;
    /* access modifiers changed from: private */
    public static Handler l;
    /* access modifiers changed from: private */
    public static boolean m;
    private static boolean n;
    /* access modifiers changed from: private */
    public static LifecycleObserver o = null;
    /* access modifiers changed from: private */
    public static a p = null;

    public static void handlePushNotification(String str) {
    }

    static class a implements InvocationHandler {
        private a() {
        }

        /* synthetic */ a(byte b2) {
            this();
        }

        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            "HelpshiftDelegate, calling: " + method.getName();
            try {
                if (method.getName().equals("sessionBegan")) {
                    boolean unused = HelpshiftTitan.m = true;
                    return null;
                } else if (method.getName().equals("sessionEnded")) {
                    boolean unused2 = HelpshiftTitan.m = false;
                    return null;
                } else if (!method.getName().equals("authenticationFailed")) {
                    return null;
                } else {
                    ((com.helpshift.l.a) objArr[1]).name();
                    return null;
                }
            } catch (Exception e) {
                throw new RuntimeException("unexpected invocation exception: " + e.getMessage());
            }
        }
    }

    public static void callInit() {
        if (!n) {
            CoreInternal.a(z.c.f4224a);
            n = true;
        }
    }

    public static void start(String str, String str2, String str3) {
        if (GameApp.getInstance() != null && !f4972b) {
            f4972b = true;
            GameApp.getInstance().runOnUiThread(new am(str, str2, str3));
        }
    }

    public static void setMetadataCallback() {
        GameApp.getInstance().runOnUiThread(new av());
    }

    public static void login(String str, String str2, String str3) {
        GameApp.getInstance().runOnUiThread(new aw(str, str2, str3));
    }

    public static void loginWithIdentifier(String str) {
        GameApp.getInstance().runOnUiThread(new ax(str));
    }

    public static void setUserIdentifier(String str) {
        GameApp.getInstance().runOnUiThread(new ay(str));
    }

    public static void setNameAndEmail(String str, String str2) {
        GameApp.getInstance().runOnUiThread(new az(str, str2));
    }

    public static void setPushNotificationToken(String str) {
        GameApp instance = GameApp.getInstance();
        instance.runOnUiThread(new ba(instance, str));
    }

    public static void showConversation() {
        GameApp.getInstance().a(new bb(h()));
    }

    private static b h() {
        b.a aVar = new b.a();
        HashMap hashMap = new HashMap(c);
        String[] strArr = e;
        if (strArr.length > 0) {
            hashMap.put("hs-tags", strArr);
        }
        aVar.d = new v(hashMap);
        aVar.a(false);
        aVar.f3889a = true;
        aVar.f3890b = true;
        aVar.a(Integer.valueOf(j));
        aVar.f = d;
        aVar.e = f;
        String[] strArr2 = h;
        if (strArr2.length > 0) {
            aVar.c = new g("or", strArr2);
        }
        return aVar.a();
    }

    public static void showFAQ() {
        GameApp.getInstance().a(new ao(h()));
    }

    public static void showSingleFAQ(String str) {
        GameApp.getInstance().a(new ap(str, h()));
    }

    public static void showFAQSection(String str) {
        GameApp.getInstance().a(new aq(str, h()));
    }

    public static void setCustomIssueField(String str, String str2, String str3) {
        d.put(str, new String[]{str2, str3});
    }

    public static void setMetadata(String str, String str2) {
        c.put(str, str2);
    }

    public static void setMetadataTags(String str) {
        e = str.split(",");
        g = e;
    }

    public static void setInitialUserMessage(String str) {
        f.put("initialUserMessage", str);
    }

    public static void setWithTagsMatching(String str) {
        h = str.split(",");
    }

    public static void clearMetadata() {
        c.clear();
        e = new String[0];
        h = new String[0];
        d.clear();
        f.clear();
    }

    public static void leaveBreadcrumb(String str) {
        GameApp.getInstance().runOnUiThread(new ar(str));
    }

    public static void setSDKLanguage(String str) {
        GameApp.getInstance().runOnUiThread(new as(str));
    }

    public static void requestNotificationCount() {
        if (f4972b) {
            GameApp.getInstance().runOnUiThread(new at());
        }
    }

    public static int getNotificationCount() {
        return f4971a;
    }

    public static void onResume() {
        if (i) {
            i = false;
            requestNotificationCount();
        }
    }

    public static void setContactUsMode(int i2) {
        j = i2;
    }

    public static boolean isVisible() {
        return m;
    }

    public static void enableChat() {
        k = true;
    }

    public static void handlePushInternal(Context context, Intent intent) {
        try {
            callInit();
            CoreInternal.a(context, intent);
            requestNotificationCount();
        } catch (Exception unused) {
        }
    }

    static /* synthetic */ void b() {
        au auVar = new au();
        if (q.d()) {
            b.a.f4235a.a(new ab(auVar));
        }
    }
}
