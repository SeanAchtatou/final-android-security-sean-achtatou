package com.supercell.titan;

import android.view.View;

/* compiled from: TitanWebView */
class ee implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TitanWebView f5153a;

    ee(TitanWebView titanWebView) {
        this.f5153a = titanWebView;
    }

    public void run() {
        this.f5153a.hide();
        this.f5153a.f4995b.clearHistory();
        this.f5153a.f4995b.clearCache(true);
        this.f5153a.f4995b.loadUrl("about:blank");
        this.f5153a.f4995b.freeMemory();
        ((View) this.f5153a.f4995b.getParent()).setVisibility(8);
        long unused = this.f5153a.h = 0;
    }
}
