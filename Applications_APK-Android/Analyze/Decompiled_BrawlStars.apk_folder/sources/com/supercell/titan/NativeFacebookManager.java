package com.supercell.titan;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.share.widget.GameRequestDialog;
import com.facebook.share.widget.ShareDialog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

public class NativeFacebookManager {
    /* access modifiers changed from: private */
    public static NativeFacebookManager c;
    private static final Uri i = Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider");

    /* renamed from: a  reason: collision with root package name */
    CallbackManager f4980a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final GameApp f4981b;
    /* access modifiers changed from: private */
    public final ArrayList<String> d = new ArrayList<>(1);
    /* access modifiers changed from: private */
    public int e = 0;
    /* access modifiers changed from: private */
    public String f = "";
    private boolean g;
    private ProfileTracker h;

    static /* synthetic */ void b(NativeFacebookManager nativeFacebookManager, String str) {
    }

    static /* synthetic */ void f(NativeFacebookManager nativeFacebookManager) {
    }

    public static native void facebookFriends(String str);

    public static native void facebookLinkStatistics(boolean z, int i2, String str);

    public static native void facebookLogged(String str, String str2);

    public static native void facebookLoginFailedWithError(String str, String str2);

    public static native void facebookLogout();

    public static native void facebookReceivedAppRequest(String str);

    public static native void facebookSentAppRequest(String str, String str2);

    public static native void facebookUserInfo(String str);

    public static void jniActivateApp() {
    }

    public static void onPause() {
    }

    public static void onResume() {
    }

    public static void onSaveInstanceState$79e5e33f() {
    }

    public static void onStart() {
    }

    public static void onStop() {
    }

    private NativeFacebookManager(GameApp gameApp) {
        this.f4981b = gameApp;
        this.d.add("user_friends");
        FacebookSdk.setLimitEventAndDataUsage(gameApp, true);
        FacebookSdk.sdkInitialize(gameApp);
        this.f4980a = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(this.f4980a, new bs(this, gameApp));
        this.h = new cf(this, gameApp);
    }

    public static void createInstance(GameApp gameApp) {
        c = new NativeFacebookManager(gameApp);
    }

    public static NativeFacebookManager getInstance() {
        return c;
    }

    private static boolean b() {
        AccessToken currentAccessToken = AccessToken.getCurrentAccessToken();
        return currentAccessToken != null && !currentAccessToken.isExpired();
    }

    private static boolean c() {
        Set<String> permissions = AccessToken.getCurrentAccessToken().getPermissions();
        if (permissions == null) {
            return false;
        }
        return permissions.contains("publish_actions");
    }

    private static boolean d() {
        AccessToken currentAccessToken = AccessToken.getCurrentAccessToken();
        if (currentAccessToken == null) {
            return false;
        }
        return !currentAccessToken.isExpired();
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        GraphRequest graphRequest;
        "NativeFacebookManager.handleRequest, type " + i2;
        this.e = 0;
        boolean z = this.g;
        if (z) {
            if (z) {
                this.g = false;
            } else {
                e();
            }
            this.e = i2;
            return;
        }
        if (i2 == 3) {
            cp cpVar = new cp(this.f4981b);
            Bundle bundle = new Bundle();
            bundle.putString(GraphRequest.FIELDS_PARAM, "id,name,picture,first_name,installed");
            if (d()) {
                graphRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), this.f, bundle, HttpMethod.GET, cpVar);
                this.f = "";
            } else {
                return;
            }
        } else {
            if (i2 == 2) {
                if (d()) {
                    GraphRequest newMyFriendsRequest = GraphRequest.newMyFriendsRequest(AccessToken.getCurrentAccessToken(), new cr(this.f4981b));
                    Bundle bundle2 = new Bundle();
                    bundle2.putString(GraphRequest.FIELDS_PARAM, "id, name, picture, first_name");
                    bundle2.putString("limit", "5000");
                    newMyFriendsRequest.setParameters(bundle2);
                    this.f4981b.runOnUiThread(new ci(this, newMyFriendsRequest));
                } else {
                    return;
                }
            } else if (i2 == 1) {
                GameApp gameApp = this.f4981b;
                gameApp.runOnUiThread(new cw(gameApp));
            }
            graphRequest = null;
        }
        if (graphRequest != null) {
            this.f4981b.runOnUiThread(new cj(this, graphRequest));
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (!c()) {
            this.g = true;
            LoginManager.getInstance().logInWithPublishPermissions(this.f4981b, Arrays.asList("publish_actions"));
        }
    }

    public static void destructInstance() {
        NativeFacebookManager nativeFacebookManager = c;
        if (nativeFacebookManager != null) {
            nativeFacebookManager.h.stopTracking();
            c = null;
        }
    }

    public static boolean jniIsLogged() {
        return b();
    }

    public static void jniAuthorize() {
        GameApp.getInstance().runOnUiThread(new bv());
    }

    public static void jniLogout() {
        GameApp.getInstance().runOnUiThread(new bw());
    }

    public static void jniRequestFriends() {
        GameApp.getInstance().runOnUiThread(new bx());
    }

    public static void jniRequestUserInfo(String str) {
        if (str != null) {
            GameApp.getInstance().runOnUiThread(new by(str));
        }
    }

    public static void jniCheckAppRequests() {
        GameApp.getInstance().runOnUiThread(new bz());
    }

    public static void jniFeedDialog(String str, String str2, String str3, String str4, String str5) {
        GameApp.getInstance().runOnUiThread(new ca(str, str2, str3, str4, str5));
    }

    public static void jniAppRequestDialog(String str, String str2, String str3, String str4) {
        GameApp.getInstance().runOnUiThread(new cb(str, str2, str3, str4));
    }

    public static void jniDeleteAppRequest(String str) {
        GameApp.getInstance().runOnUiThread(new cc(str));
    }

    public static void jniLinkStatistics(String str) {
        GameApp.getInstance().runOnUiThread(new cd(str));
    }

    public static void jniLike(String str) {
        GameApp.getInstance().runOnUiThread(new ce(str));
    }

    public static boolean jniCanPublish() {
        return c();
    }

    public static String jniGetAttributionID() {
        Cursor query = GameApp.getInstance().getContentResolver().query(i, new String[]{"aid"}, null, null, null);
        if (query != null && query.moveToFirst()) {
            String string = query.getString(query.getColumnIndex("aid"));
            query.close();
            if (string != null) {
                return string;
            }
        }
        return "";
    }

    public static void jniRequestNewPublishPermissions() {
        GameApp.getInstance().runOnUiThread(new ch());
    }

    static /* synthetic */ void d(NativeFacebookManager nativeFacebookManager) {
        if (b()) {
            nativeFacebookManager.a(1);
        } else {
            LoginManager.getInstance().logInWithReadPermissions(nativeFacebookManager.f4981b, nativeFacebookManager.d);
        }
    }

    static /* synthetic */ void a(NativeFacebookManager nativeFacebookManager, String str, String str2, String str3, String str4, String str5) {
        ShareDialog shareDialog = new ShareDialog(nativeFacebookManager.f4981b);
        shareDialog.registerCallback(nativeFacebookManager.f4980a, new ck(nativeFacebookManager));
        nativeFacebookManager.f4981b.runOnUiThread(new cl(nativeFacebookManager, str2, str3, str4, str5, shareDialog));
    }

    static /* synthetic */ void a(NativeFacebookManager nativeFacebookManager, String str, String str2, String str3, String str4) {
        GameRequestDialog gameRequestDialog = new GameRequestDialog(nativeFacebookManager.f4981b);
        gameRequestDialog.registerCallback(nativeFacebookManager.f4980a, new cm(nativeFacebookManager));
        nativeFacebookManager.f4981b.runOnUiThread(new cn(nativeFacebookManager, str, str2, str3, str4, gameRequestDialog));
    }

    static /* synthetic */ void c(NativeFacebookManager nativeFacebookManager, String str) {
        if (d()) {
            try {
                Bundle bundle = new Bundle();
                bundle.putString("id", str);
                bundle.putString(GraphRequest.FIELDS_PARAM, "og_object{likes.summary(true).limit(0),reactions.summary(true).limit(0)}");
                GraphRequest graphRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/", bundle, HttpMethod.GET, new ct(str));
                graphRequest.setParameters(bundle);
                GraphRequest.executeBatchAsync(graphRequest);
            } catch (FacebookException e2) {
                GameApp.debuggerException(e2);
            } catch (IllegalStateException e3) {
                GameApp.debuggerException(e3);
            }
        }
    }

    static /* synthetic */ void d(NativeFacebookManager nativeFacebookManager, String str) {
        if (d()) {
            Bundle bundle = new Bundle();
            bundle.putString("object", str);
            try {
                GraphRequest.executeBatchAsync(new GraphRequest(AccessToken.getCurrentAccessToken(), "me/og.likes", bundle, HttpMethod.POST, new co(nativeFacebookManager)));
            } catch (FacebookException e2) {
                GameApp.debuggerException(e2);
            } catch (IllegalStateException e3) {
                GameApp.debuggerException(e3);
            }
        }
    }
}
