package com.supercell.titan;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.share.internal.ShareConstants;
import org.json.JSONException;

/* compiled from: NativeFacebookRequestLinkStatisticsCallback */
class cu implements GraphRequest.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f5094a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ int f5095b;
    final /* synthetic */ ct c;

    cu(ct ctVar, int i, int i2) {
        this.c = ctVar;
        this.f5094a = i;
        this.f5095b = i2;
    }

    public void onCompleted(GraphResponse graphResponse) {
        try {
            GameApp.getInstance().a(new cv(this, graphResponse.getJSONObject().getJSONArray(ShareConstants.WEB_DIALOG_PARAM_DATA).length() > 0));
        } catch (JSONException e) {
            GameApp.debuggerException(e);
        } catch (Exception e2) {
            GameApp.debuggerException(e2);
        }
    }
}
