package com.supercell.titan;

import android.graphics.Typeface;

/* compiled from: KeyboardDialog */
class bg implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5042a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ bc f5043b;

    bg(bc bcVar, String str) {
        this.f5043b = bcVar;
        this.f5042a = str;
    }

    public void run() {
        Typeface a2;
        String fontPath = VirtualKeyboardHandler.getFontPath(this.f5042a);
        if (fontPath != null && fontPath.trim().length() > 0 && (a2 = h.a(this.f5043b.f5037a, fontPath)) != null) {
            this.f5043b.f5037a.runOnUiThread(new bh(this, a2));
        }
    }
}
