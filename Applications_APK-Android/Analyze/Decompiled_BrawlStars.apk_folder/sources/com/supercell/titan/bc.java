package com.supercell.titan;

import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.facebook.internal.NativeProtocol;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: KeyboardDialog */
public class bc extends Dialog {
    /* access modifiers changed from: private */
    public static String g;
    private static InputFilter[] h;
    private static int i;
    /* access modifiers changed from: private */
    public static bc j;
    private static final Rect k = new Rect();
    private static final AtomicInteger l = new AtomicInteger(1);
    /* access modifiers changed from: package-private */

    /* renamed from: a  reason: collision with root package name */
    public final GameApp f5037a;

    /* renamed from: b  reason: collision with root package name */
    final Button f5038b;
    final bk c;
    /* access modifiers changed from: package-private */
    public final dt d;
    Typeface e;
    final int f;

    public bc(GameApp gameApp) {
        super(gameApp, R.style.KeyboardDialog);
        this.f5037a = gameApp;
        requestWindowFeature(1);
        this.d = new dt(gameApp);
        this.f5038b = new Button(gameApp);
        this.c = new bk(gameApp);
        Point point = new Point();
        gameApp.getWindowManager().getDefaultDisplay().getSize(point);
        this.f = point.x;
        j = this;
    }

    public static float a(int i2) {
        bc bcVar;
        dt dtVar;
        if (!VirtualKeyboardHandler.f5002a || (bcVar = j) == null || (dtVar = bcVar.d) == null) {
            return 0.0f;
        }
        dtVar.getWindowVisibleDisplayFrame(k);
        float height = (float) ((i2 - (k.bottom - k.top)) + j.d.getHeight());
        if (height < 0.0f) {
            return 0.0f;
        }
        return height;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int i2;
        int i3;
        super.onCreate(bundle);
        this.e = this.d.getTypeface();
        this.d.setFocusable(true);
        this.d.setInputType(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REPLY);
        this.d.setSingleLine();
        this.c.setOnTouchListener(new bd(this));
        if (Build.VERSION.SDK_INT < 17) {
            Button button = this.f5038b;
            do {
                i2 = l.get();
                i3 = i2 + 1;
                if (i3 > 16777215) {
                    i3 = 1;
                }
            } while (!l.compareAndSet(i2, i3));
            button.setId(i2);
        } else {
            this.f5038b.setId(View.generateViewId());
        }
        this.f5038b.setOnClickListener(new be(this));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        this.c.addView(this.f5038b);
        this.c.addView(this.d);
        this.c.setFitsSystemWindows(true);
        setContentView(this.c, layoutParams);
        String str = g;
        if (str != null) {
            this.d.setText(str);
            g = null;
        }
        InputFilter[] inputFilterArr = h;
        if (inputFilterArr != null) {
            this.d.setFilters(inputFilterArr);
            h = null;
        }
        this.d.setImeOptions(i);
        a();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        g = null;
        j = null;
    }

    public final void a() {
        Window window = getWindow();
        window.clearFlags(2);
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.setLayout(-1, -1);
        if (Build.VERSION.SDK_INT >= 19) {
            window.setFlags(134217728, 134217728);
            window.setFlags(67108864, 67108864);
        }
        window.setFlags(1024, 1024);
    }

    static int b(int i2) {
        GameApp.getInstance().getResources();
        return (int) ((((float) i2) * Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }

    static void a(String str) {
        if (j == null) {
            g = str;
        } else if (GameApp.getInstance() != null) {
            GameApp.getInstance().runOnUiThread(new bj(str));
        }
    }

    static void a(InputFilter[] inputFilterArr) {
        dt dtVar;
        h = inputFilterArr;
        bc bcVar = j;
        if (bcVar != null && (dtVar = bcVar.d) != null) {
            dtVar.setFilters(inputFilterArr);
        }
    }

    public static void c(int i2) {
        dt dtVar;
        i = i2;
        bc bcVar = j;
        if (bcVar != null && (dtVar = bcVar.d) != null) {
            dtVar.setImeOptions(i2);
        }
    }
}
