package com.supercell.titan;

/* compiled from: HelpshiftTitan */
final class am implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5016a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f5017b;
    final /* synthetic */ String c;

    am(String str, String str2, String str3) {
        this.f5016a = str;
        this.f5017b = str2;
        this.c = str3;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|(1:7)|8|9|10|11|12|13|14|(2:16|(1:18)(1:19))|20|(2:22|27)(1:26)) */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ea, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00eb, code lost:
        com.supercell.titan.GameApp.debuggerException(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0094 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0030 */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x009a A[Catch:{ Exception -> 0x00ea }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00d2 A[Catch:{ Exception -> 0x00ea }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r17 = this;
            r1 = r17
            com.supercell.titan.an r0 = new com.supercell.titan.an
            r0.<init>(r1)
            android.os.Handler unused = com.supercell.titan.HelpshiftTitan.l = r0
            com.helpshift.l$a r0 = new com.helpshift.l$a     // Catch:{ Exception -> 0x00ea }
            r0.<init>()     // Catch:{ Exception -> 0x00ea }
            r2 = 1
            r0.f3733a = r2     // Catch:{ Exception -> 0x00ea }
            com.supercell.titan.GameApp r3 = com.supercell.titan.GameApp.getInstance()     // Catch:{ NotFoundException -> 0x0030 }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ NotFoundException -> 0x0030 }
            java.lang.String r4 = "ic_notification"
            java.lang.String r5 = "drawable"
            com.supercell.titan.GameApp r6 = com.supercell.titan.GameApp.getInstance()     // Catch:{ NotFoundException -> 0x0030 }
            java.lang.String r6 = r6.getPackageName()     // Catch:{ NotFoundException -> 0x0030 }
            int r3 = r3.getIdentifier(r4, r5, r6)     // Catch:{ NotFoundException -> 0x0030 }
            if (r3 == 0) goto L_0x0030
            if (r3 == 0) goto L_0x0030
            r0.f3734b = r3     // Catch:{ NotFoundException -> 0x0030 }
        L_0x0030:
            com.supercell.titan.HelpshiftTitan.callInit()     // Catch:{ Exception -> 0x00ea }
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ Exception -> 0x00ea }
            r3.<init>()     // Catch:{ Exception -> 0x00ea }
            java.lang.String r4 = "manualLifecycleTracking"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x00ea }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x00ea }
            r0.k = r3     // Catch:{ Exception -> 0x00ea }
            r3 = 0
            r0.h = r3     // Catch:{ Exception -> 0x00ea }
            com.helpshift.l r16 = new com.helpshift.l     // Catch:{ Exception -> 0x00ea }
            boolean r5 = r0.f3733a     // Catch:{ Exception -> 0x00ea }
            int r6 = r0.f3734b     // Catch:{ Exception -> 0x00ea }
            int r7 = r0.c     // Catch:{ Exception -> 0x00ea }
            int r8 = r0.d     // Catch:{ Exception -> 0x00ea }
            boolean r9 = r0.f     // Catch:{ Exception -> 0x00ea }
            boolean r10 = r0.e     // Catch:{ Exception -> 0x00ea }
            java.lang.String r11 = r0.g     // Catch:{ Exception -> 0x00ea }
            boolean r12 = r0.h     // Catch:{ Exception -> 0x00ea }
            int r13 = r0.i     // Catch:{ Exception -> 0x00ea }
            java.lang.String r14 = r0.j     // Catch:{ Exception -> 0x00ea }
            java.util.Map<java.lang.String, java.lang.Object> r15 = r0.k     // Catch:{ Exception -> 0x00ea }
            r4 = r16
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x00ea }
            com.supercell.titan.GameApp r0 = com.supercell.titan.GameApp.getInstance()     // Catch:{ Exception -> 0x00ea }
            android.app.Application r0 = r0.getApplication()     // Catch:{ Exception -> 0x00ea }
            java.lang.String r4 = r1.f5016a     // Catch:{ Exception -> 0x00ea }
            java.lang.String r5 = r1.f5017b     // Catch:{ Exception -> 0x00ea }
            java.lang.String r6 = r1.c     // Catch:{ Exception -> 0x00ea }
            java.util.HashMap r7 = new java.util.HashMap     // Catch:{ Exception -> 0x00ea }
            r7.<init>()     // Catch:{ Exception -> 0x00ea }
            java.util.Map r8 = r16.a()     // Catch:{ Exception -> 0x00ea }
            r7.putAll(r8)     // Catch:{ Exception -> 0x00ea }
            com.helpshift.CoreInternal.a(r0, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00ea }
            java.lang.String r0 = "com.helpshift.CoreInternal"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException | Exception | NoSuchMethodException -> 0x0094 }
            java.lang.String r4 = "onAppForeground"
            java.lang.Class[] r5 = new java.lang.Class[r3]     // Catch:{ ClassNotFoundException | Exception | NoSuchMethodException -> 0x0094 }
            java.lang.reflect.Method r0 = r0.getMethod(r4, r5)     // Catch:{ ClassNotFoundException | Exception | NoSuchMethodException -> 0x0094 }
            r4 = 0
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ ClassNotFoundException | Exception | NoSuchMethodException -> 0x0094 }
            r0.invoke(r4, r5)     // Catch:{ ClassNotFoundException | Exception | NoSuchMethodException -> 0x0094 }
        L_0x0094:
            com.supercell.titan.HelpshiftTitan$a r0 = com.supercell.titan.HelpshiftTitan.p     // Catch:{ Exception -> 0x00ea }
            if (r0 != 0) goto L_0x00c9
            com.supercell.titan.HelpshiftTitan$a r0 = new com.supercell.titan.HelpshiftTitan$a     // Catch:{ Exception -> 0x00ea }
            r0.<init>(r3)     // Catch:{ Exception -> 0x00ea }
            com.supercell.titan.HelpshiftTitan.a unused = com.supercell.titan.HelpshiftTitan.p = r0     // Catch:{ Exception -> 0x00ea }
            java.lang.Class<com.helpshift.support.z$a> r0 = com.helpshift.support.z.a.class
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ Exception -> 0x00ea }
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x00ea }
            java.lang.Class<com.helpshift.support.z$a> r4 = com.helpshift.support.z.a.class
            r2[r3] = r4     // Catch:{ Exception -> 0x00ea }
            com.supercell.titan.HelpshiftTitan$a r3 = com.supercell.titan.HelpshiftTitan.p     // Catch:{ Exception -> 0x00ea }
            java.lang.Object r0 = java.lang.reflect.Proxy.newProxyInstance(r0, r2, r3)     // Catch:{ Exception -> 0x00ea }
            com.helpshift.support.z$a r0 = (com.helpshift.support.z.a) r0     // Catch:{ Exception -> 0x00ea }
            boolean r2 = com.helpshift.util.q.d()     // Catch:{ Exception -> 0x00ea }
            if (r2 != 0) goto L_0x00bf
            goto L_0x00c9
        L_0x00bf:
            com.helpshift.util.a.a r2 = com.helpshift.util.a.b.a.f4235a     // Catch:{ Exception -> 0x00ea }
            com.helpshift.support.ac r3 = new com.helpshift.support.ac     // Catch:{ Exception -> 0x00ea }
            r3.<init>(r0)     // Catch:{ Exception -> 0x00ea }
            r2.a(r3)     // Catch:{ Exception -> 0x00ea }
        L_0x00c9:
            com.supercell.titan.HelpshiftTitan.b()     // Catch:{ Exception -> 0x00ea }
            android.arch.lifecycle.LifecycleObserver r0 = com.supercell.titan.HelpshiftTitan.o     // Catch:{ Exception -> 0x00ea }
            if (r0 != 0) goto L_0x00ee
            com.supercell.titan.HelpshiftTitan$1$2 r0 = new com.supercell.titan.HelpshiftTitan$1$2     // Catch:{ Exception -> 0x00ea }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00ea }
            android.arch.lifecycle.LifecycleObserver unused = com.supercell.titan.HelpshiftTitan.o = r0     // Catch:{ Exception -> 0x00ea }
            android.arch.lifecycle.LifecycleOwner r0 = android.arch.lifecycle.ProcessLifecycleOwner.get()     // Catch:{ Exception -> 0x00ea }
            android.arch.lifecycle.Lifecycle r0 = r0.getLifecycle()     // Catch:{ Exception -> 0x00ea }
            android.arch.lifecycle.LifecycleObserver r2 = com.supercell.titan.HelpshiftTitan.o     // Catch:{ Exception -> 0x00ea }
            r0.addObserver(r2)     // Catch:{ Exception -> 0x00ea }
            goto L_0x00ee
        L_0x00ea:
            r0 = move-exception
            com.supercell.titan.GameApp.debuggerException(r0)
        L_0x00ee:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.supercell.titan.am.run():void");
    }
}
