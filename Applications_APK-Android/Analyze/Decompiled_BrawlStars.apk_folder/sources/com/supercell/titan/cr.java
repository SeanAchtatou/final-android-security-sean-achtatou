package com.supercell.titan;

import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import org.json.JSONArray;

/* compiled from: NativeFacebookRequestFriendsCallback */
public class cr implements GraphRequest.GraphJSONArrayCallback {

    /* renamed from: a  reason: collision with root package name */
    private final GameApp f5090a;

    public cr(GameApp gameApp) {
        this.f5090a = gameApp;
    }

    public void onCompleted(JSONArray jSONArray, GraphResponse graphResponse) {
        FacebookRequestError error = graphResponse.getError();
        if (error != null) {
            GameApp.debuggerWarning("NativeFacebookRequestFriendsCallback: " + error.getErrorMessage());
            return;
        }
        this.f5090a.a(new cs(this, graphResponse));
    }
}
