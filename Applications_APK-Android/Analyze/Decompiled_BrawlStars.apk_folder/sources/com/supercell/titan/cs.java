package com.supercell.titan;

import com.facebook.GraphResponse;
import org.json.JSONObject;

/* compiled from: NativeFacebookRequestFriendsCallback */
class cs implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GraphResponse f5091a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ cr f5092b;

    cs(cr crVar, GraphResponse graphResponse) {
        this.f5092b = crVar;
        this.f5091a = graphResponse;
    }

    public void run() {
        JSONObject jSONObject = this.f5091a.getJSONObject();
        NativeFacebookManager.facebookFriends(jSONObject != null ? jSONObject.toString() : "");
    }
}
