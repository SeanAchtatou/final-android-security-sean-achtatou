package com.supercell.titan;

import android.os.Build;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.supercell.titan.TitanWebView;

/* compiled from: TitanWebView */
class dz implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameApp f5144a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ TitanWebView f5145b;
    final /* synthetic */ boolean c;
    final /* synthetic */ TitanWebView d;

    dz(TitanWebView titanWebView, GameApp gameApp, TitanWebView titanWebView2, boolean z) {
        this.d = titanWebView;
        this.f5144a = gameApp;
        this.f5145b = titanWebView2;
        this.c = z;
    }

    public void run() {
        if (this.d.f4995b == null) {
            FrameLayout frameLayout = new FrameLayout(this.f5144a);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
            if (Build.VERSION.SDK_INT >= 17) {
                frameLayout.setLayoutDirection(0);
            }
            this.f5144a.getWindow().addContentView(frameLayout, layoutParams);
            WebView unused = this.d.f4995b = new WebView(this.f5144a);
            this.d.f4995b.setVisibility(4);
            this.d.f4995b.setBackgroundColor(0);
            this.d.f4995b.setAlpha(0.0f);
            this.d.f4995b.getSettings().setJavaScriptEnabled(true);
            this.d.f4995b.getSettings().setDomStorageEnabled(true);
            this.d.f4995b.setWebViewClient(new TitanWebView.c(this.f5145b));
            TitanWebView titanWebView = this.d;
            TitanWebView.b unused2 = titanWebView.c = new TitanWebView.b(this.f5145b);
            this.d.f4995b.setWebChromeClient(this.d.c);
            frameLayout.addView(this.d.f4995b);
            FrameLayout unused3 = this.d.d = new FrameLayout(this.f5144a);
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, -1);
            this.d.d.setVisibility(8);
            if (Build.VERSION.SDK_INT >= 17) {
                layoutParams2.setLayoutDirection(0);
            }
            this.f5144a.getWindow().addContentView(this.d.d, layoutParams2);
            if (Build.VERSION.SDK_INT >= 19 && this.c) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }
    }
}
