package com.supercell.titan;

import android.content.Context;
import android.text.Editable;
import android.widget.EditText;

/* compiled from: TitanEditText */
class dt extends EditText {
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final Runnable f5135b = new du();
    /* access modifiers changed from: package-private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f5136a;

    public dt(Context context) {
        super(context);
        setOnEditorActionListener(new dv(this));
        addTextChangedListener(new dw(this));
    }

    private static int a(String str) {
        int length = str.length();
        int i = 0;
        int i2 = 0;
        while (i < length) {
            i += Character.charCount(str.codePointAt(i));
            i2++;
        }
        return i2;
    }

    /* access modifiers changed from: protected */
    public void onSelectionChanged(int i, int i2) {
        int i3;
        super.onSelectionChanged(i, i2);
        GameApp instance = GameApp.getInstance();
        Editable text = getText();
        int a2 = a(text.subSequence(0, i).toString());
        if (i2 == i) {
            i3 = a2;
        } else {
            i3 = a(text.subSequence(0, i2).toString());
        }
        instance.a(new dy(this, a2, i3));
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            boolean z2 = VirtualKeyboardHandler.f5002a;
            if (hasFocus() && z2) {
                VirtualKeyboardHandler.showKeyboard();
            }
            GameApp instance = GameApp.getInstance();
            if (instance != null) {
                instance.c();
                VirtualKeyboardHandler.updateUIFlags();
            }
        }
    }
}
