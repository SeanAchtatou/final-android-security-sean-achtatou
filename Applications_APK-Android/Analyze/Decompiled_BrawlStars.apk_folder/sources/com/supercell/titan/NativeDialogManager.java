package com.supercell.titan;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import com.facebook.share.internal.ShareConstants;
import java.util.Vector;

public class NativeDialogManager extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f4976a = false;

    /* renamed from: b  reason: collision with root package name */
    public static int f4977b = 0;
    public static int c = 0;
    private static int d = 1000;
    /* access modifiers changed from: private */
    public static NativeDialogManager g;
    private static final Vector<a> h = new Vector<>();
    /* access modifiers changed from: private */
    public int e = -1;
    private boolean f;

    static class a {

        /* renamed from: a  reason: collision with root package name */
        String f4978a;

        /* renamed from: b  reason: collision with root package name */
        String f4979b;
        String c;
        String d;
        String e;
        int f;

        private a() {
            this.f4978a = "";
            this.f4979b = "";
            this.c = "";
            this.d = "";
            this.e = "";
        }

        /* synthetic */ a(byte b2) {
            this();
        }
    }

    public static int ShowDialog(String str, String str2, String str3, String str4, String str5) {
        int i = d + 1;
        d = i;
        if (g == null) {
            a(str, str2, str3, str4, str5, i);
        } else {
            a aVar = new a((byte) 0);
            aVar.f4978a = str;
            aVar.f4979b = str2;
            aVar.c = str3;
            aVar.d = str4;
            aVar.e = str5;
            aVar.f = i;
            h.add(aVar);
        }
        return i;
    }

    private static void a(String str, String str2, String str3, String str4, String str5, int i) {
        GameApp instance = GameApp.getInstance();
        instance.runOnUiThread(new bq(str, str2, str3, str4, str5, i, instance));
    }

    public Dialog onCreateDialog(Bundle bundle) {
        GameApp instance = GameApp.getInstance();
        if (instance == null) {
            return null;
        }
        br brVar = new br(this);
        String string = getArguments().getString("title");
        String string2 = getArguments().getString("button");
        String string3 = getArguments().getString("button2");
        String string4 = getArguments().getString("button3");
        String string5 = getArguments().getString(ShareConstants.WEB_DIALOG_PARAM_MESSAGE);
        this.e = getArguments().getInt("id");
        AlertDialog.Builder neutralButton = new AlertDialog.Builder(instance, 4).setTitle(string).setMessage(string5).setNeutralButton(string2, brVar);
        if (!string3.isEmpty()) {
            neutralButton.setPositiveButton(string3, brVar);
        }
        if (!string4.isEmpty()) {
            neutralButton.setNegativeButton(string4, brVar);
        }
        return neutralButton.create();
    }

    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        GameApp.getInstance().a();
        a(this.e, 0);
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2) {
        if (!this.f) {
            this.f = true;
            g = null;
            GameApp instance = GameApp.getInstance();
            if (!h.isEmpty()) {
                a remove = h.remove(0);
                a(remove.f4978a, remove.f4979b, remove.c, remove.d, remove.e, remove.f);
            } else {
                WifiManager wifiManager = (WifiManager) instance.getSystemService("wifi");
                if (wifiManager != null) {
                    try {
                        wifiManager.reconnect();
                    } catch (Exception unused) {
                    }
                }
            }
            f4976a = true;
            c = i;
            f4977b = i2;
            GameApp.dialogDismissed(c, f4977b);
        }
    }

    public static boolean isDialogVisible() {
        return g != null;
    }

    public static void nativeDialogDismissAll() {
        h.clear();
        NativeDialogManager nativeDialogManager = g;
        g = null;
        if (nativeDialogManager != null) {
            nativeDialogManager.a();
        }
        f4976a = false;
        GameApp.getInstance();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f = true;
        try {
            dismiss();
        } catch (Exception e2) {
            GameApp.debuggerException(e2);
        }
    }

    public static void ShowPostDialog(String str, String str2) {
        GameApp instance = GameApp.getInstance();
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.addFlags(524288);
            intent.putExtra("android.intent.extra.TEXT", str);
            instance.startActivity(Intent.createChooser(intent, str2));
        } catch (ActivityNotFoundException e2) {
            GameApp.debuggerException(e2);
        }
    }

    public static void ShowPostURLDialog(String str, String str2, String str3) {
        GameApp instance = GameApp.getInstance();
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.addFlags(524288);
            intent.putExtra("android.intent.extra.SUBJECT", str);
            intent.putExtra("android.intent.extra.TEXT", str3);
            instance.startActivity(Intent.createChooser(intent, str2));
        } catch (ActivityNotFoundException e2) {
            GameApp.debuggerException(e2);
        }
    }
}
