package com.supercell.titan;

import android.os.Bundle;
import com.facebook.share.internal.ShareConstants;

/* compiled from: NativeDialogManager */
final class bq implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f5057a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f5058b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ String e;
    final /* synthetic */ int f;
    final /* synthetic */ GameApp g;

    bq(String str, String str2, String str3, String str4, String str5, int i, GameApp gameApp) {
        this.f5057a = str;
        this.f5058b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = i;
        this.g = gameApp;
    }

    public final void run() {
        NativeDialogManager nativeDialogManager = new NativeDialogManager();
        nativeDialogManager.setStyle(0, 16974126);
        Bundle bundle = new Bundle();
        bundle.putString("title", this.f5057a);
        bundle.putString(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, this.f5058b);
        bundle.putString("button", this.c);
        bundle.putString("button2", this.d);
        bundle.putString("button3", this.e);
        bundle.putInt("id", this.f);
        nativeDialogManager.setArguments(bundle);
        try {
            nativeDialogManager.show(this.g.getFragmentManager(), "NativeDialog");
            NativeDialogManager unused = NativeDialogManager.g = nativeDialogManager;
        } catch (Exception e2) {
            GameApp.debuggerException(e2);
        }
    }
}
