package com.linecorp.nova.android;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcel;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class NovaAdvertisingIdHelper {
    private static final String AD_INTERFACE_NAME = "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService";
    private static final String GOOGLE_GMS_INTENT = "com.google.android.gms.ads.identifier.service.START";
    private static final String GOOGLE_GMS_PACKAGE = "com.google.android.gms";
    private static final String TAG = "NovaAdvertisingIdHelper";
    /* access modifiers changed from: private */
    public static IBinder binder = null;
    private static a connection = new a((byte) 0);
    private static Object object = new Object();

    private static IBinder getBinder(Context context) {
        IBinder iBinder = binder;
        if (iBinder != null) {
            return iBinder;
        }
        Intent intent = new Intent(GOOGLE_GMS_INTENT);
        intent.setPackage(GOOGLE_GMS_PACKAGE);
        try {
            if (!context.bindService(intent, connection, 1) || a.a() == null || !a.a().tryAcquire(500, TimeUnit.MILLISECONDS)) {
                return null;
            }
            return binder;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getASIdentifier(Context context) {
        Parcel obtain;
        Parcel obtain2;
        synchronized (object) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                return "";
            }
            try {
                if (getBinder(context) == null) {
                    return "";
                }
                obtain = Parcel.obtain();
                obtain2 = Parcel.obtain();
                obtain.writeInterfaceToken(AD_INTERFACE_NAME);
                getBinder(context).transact(1, obtain, obtain2, 0);
                obtain2.readException();
                String readString = obtain2.readString();
                obtain2.recycle();
                obtain.recycle();
                return readString;
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            } catch (Throwable th) {
                obtain2.recycle();
                obtain.recycle();
                throw th;
            }
        }
    }

    public static boolean getASIdentifierEnabled(Context context) {
        Parcel obtain;
        Parcel obtain2;
        synchronized (object) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                return false;
            }
            try {
                if (getBinder(context) == null) {
                    return false;
                }
                obtain = Parcel.obtain();
                obtain2 = Parcel.obtain();
                obtain.writeInterfaceToken(AD_INTERFACE_NAME);
                boolean z = true;
                obtain.writeInt(1);
                getBinder(context).transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z = false;
                }
                obtain2.recycle();
                obtain.recycle();
                return z;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } catch (Throwable th) {
                obtain2.recycle();
                obtain.recycle();
                throw th;
            }
        }
    }

    static final class a implements ServiceConnection {

        /* renamed from: a  reason: collision with root package name */
        private static Semaphore f4522a = new Semaphore(0);

        private a() {
        }

        /* synthetic */ a(byte b2) {
            this();
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            IBinder unused = NovaAdvertisingIdHelper.binder = iBinder;
            f4522a.release();
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            IBinder unused = NovaAdvertisingIdHelper.binder = null;
        }

        public static Semaphore a() {
            return f4522a;
        }
    }
}
