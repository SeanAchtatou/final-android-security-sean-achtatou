package com.linecorp.nova.android;

import android.content.Intent;

/* compiled from: NovaNative */
final class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Intent f4523a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ int f4524b;

    a(Intent intent, int i) {
        this.f4523a = intent;
        this.f4524b = i;
    }

    public final void run() {
        NovaNative.m_activity.startActivityForResult(this.f4523a, this.f4524b);
    }
}
