package com.linecorp.nova.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class NovaNative {
    private static final String NOVA_LIB_DEFAULT = "nova_core_cpp";
    private static final String NOVA_LIB_KEY = "android.app.nova_lib";
    private static final String TAG = "NovaNative";
    /* access modifiers changed from: private */
    public static Activity m_activity = null;
    private static boolean m_loaded = false;
    private static ArrayList<Runnable> m_lostActions = new ArrayList<>();
    public static Object m_mainActivityMutex = new Object();
    private static boolean m_started = false;

    static void nativeInitialized(boolean z, String str) {
    }

    private static native void nativeOnActivityEnterBackground();

    private static native void nativeOnActivityPaused();

    private static native void nativeOnActivityResult(int i, int i2, Intent intent);

    private static native void nativeOnActivityResume();

    /* access modifiers changed from: private */
    public static native void nativeOnAndroidUiThread(long j);

    public static Activity activity() {
        return m_activity;
    }

    public static void setActivity(Activity activity) {
        m_activity = activity;
    }

    public static Context getApplicationContext() {
        Activity activity = m_activity;
        if (activity == null) {
            return null;
        }
        return activity.getApplicationContext();
    }

    public static boolean loadNativeModules() throws Exception {
        if (m_loaded) {
            return true;
        }
        Activity activity = m_activity;
        if (activity == null) {
            return false;
        }
        ActivityInfo activityInfo = activity.getPackageManager().getActivityInfo(m_activity.getComponentName(), 128);
        System.loadLibrary(activityInfo.metaData.containsKey(NOVA_LIB_KEY) ? activityInfo.metaData.getString(NOVA_LIB_KEY) : NOVA_LIB_DEFAULT);
        m_loaded = true;
        return m_loaded;
    }

    public static void onTrimMemory(int i) {
        if (started() && i >= 20) {
            nativeOnActivityEnterBackground();
        }
    }

    public static void onPause() {
        if (started()) {
            nativeOnActivityPaused();
        }
    }

    public static void onResume() {
        ArrayList arrayList;
        if (started()) {
            synchronized (m_mainActivityMutex) {
                arrayList = new ArrayList(getLostActions());
                clearLostActions();
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                m_activity.runOnUiThread((Runnable) it.next());
            }
            nativeOnActivityResume();
        }
    }

    public static void onActivityResult(int i, int i2, Intent intent) {
        if (started()) {
            nativeOnActivityResult(i, i2, intent);
        }
    }

    public static void startIntent(Intent intent, int i) {
        m_activity.startActivityForResult(intent, i);
    }

    public static void startIntentOnUiThread(Intent intent, int i) {
        m_activity.runOnUiThread(new a(intent, i));
    }

    static boolean started() {
        return m_started;
    }

    static void setStarted(boolean z) {
        m_started = z;
    }

    private static ClassLoader classLoader() {
        return m_activity.getClassLoader();
    }

    private static Resources getResources() {
        return m_activity.getResources();
    }

    private static AssetManager getAssets() {
        return m_activity.getAssets();
    }

    private static ArrayList<Runnable> getLostActions() {
        return m_lostActions;
    }

    private static void clearLostActions() {
        m_lostActions.clear();
    }

    private static boolean runAction(Runnable runnable) {
        Activity activity = m_activity;
        if (activity == null) {
            synchronized (m_mainActivityMutex) {
                m_lostActions.add(runnable);
            }
        } else {
            activity.runOnUiThread(runnable);
        }
        return m_activity != null;
    }

    private static void runOnUiThread(long j) {
        runAction(new b(j));
    }

    private static String getWifiMacAddress() {
        try {
            for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (networkInterface.getName().equalsIgnoreCase("wlan0")) {
                    byte[] hardwareAddress = networkInterface.getHardwareAddress();
                    if (hardwareAddress == null) {
                        return "";
                    }
                    StringBuilder sb = new StringBuilder();
                    int length = hardwareAddress.length;
                    for (int i = 0; i < length; i++) {
                        sb.append(String.format("%02X:", Byte.valueOf(hardwareAddress[i])));
                    }
                    if (sb.length() > 0) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    "mac: " + sb.toString();
                    return sb.toString();
                }
            }
            return "";
        } catch (Exception unused) {
            return "";
        }
    }
}
