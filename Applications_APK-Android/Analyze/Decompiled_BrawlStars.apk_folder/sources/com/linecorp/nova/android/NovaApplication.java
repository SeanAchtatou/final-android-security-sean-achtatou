package com.linecorp.nova.android;

import android.app.Application;

public class NovaApplication extends Application {
    public void onTrimMemory(int i) {
        NovaNative.onTrimMemory(i);
    }
}
