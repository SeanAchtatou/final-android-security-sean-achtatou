package com.linecorp.nova.interop.line.auth;

import android.app.Activity;
import android.content.Context;
import com.linecorp.linesdk.LineAccessToken;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineCredential;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.api.LineApiClientBuilder;
import com.linecorp.linesdk.auth.LineAuthenticationConfig;
import com.linecorp.linesdk.auth.LineLoginApi;
import java.util.List;

public class LineLoginProxy {
    private static final int REQUEST_CODE = 10001;
    private static final String TAG = "LineLoginProxy";
    private final Activity activity;
    private final String channelId;
    private final LineApiClient lineApiClient;
    private final AccessTokenCache tokenCache;

    private Context getContext() {
        return this.activity.getApplicationContext();
    }

    public LineLoginProxy(Activity activity2, String str) {
        this.activity = activity2;
        this.channelId = str;
        this.lineApiClient = new LineApiClientBuilder(getContext(), str).build();
        EncryptorHolder.initializeOnWorkerThread(getContext());
        this.tokenCache = new AccessTokenCache(getContext(), str);
    }

    public void login(List<String> list) {
        try {
            this.activity.startActivityForResult(LineLoginApi.getLoginIntent(getContext(), new LineAuthenticationConfig.Builder(this.channelId).build(), list), REQUEST_CODE);
        } catch (Exception e) {
            "login exception: " + e.toString();
        }
    }

    public LineApiResponse<LineAccessToken> getCurrentAccessToken() {
        return this.lineApiClient.getCurrentAccessToken();
    }

    public LineApiResponse<LineAccessToken> refreshAccessToken() {
        return this.lineApiClient.refreshAccessToken();
    }

    public LineApiResponse<LineCredential> verifyAccessToken() {
        return this.lineApiClient.verifyToken();
    }

    public LineApiResponse<?> revokeAccessToken() {
        return this.lineApiClient.logout();
    }

    public String getRefreshToken() {
        InternalAccessToken accessToken = this.tokenCache.getAccessToken();
        if (accessToken == null) {
            return null;
        }
        return accessToken.getRefreshToken();
    }
}
