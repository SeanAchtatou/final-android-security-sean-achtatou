package com.linecorp.nova.interop.line.auth;

import android.content.Context;
import com.linecorp.nova.security.StringCipher;
import java.util.concurrent.Executors;

public class EncryptorHolder {
    private static final int DEFAULT_ITERATION_COUNT = 5000;
    private static final String ENCRYPTION_SALT_SHARED_PREFERENCE_NAME = "com.linecorp.linesdk.sharedpreference.encryptionsalt";
    /* access modifiers changed from: private */
    public static final StringCipher ENCRYPTOR = new StringCipher(ENCRYPTION_SALT_SHARED_PREFERENCE_NAME, DEFAULT_ITERATION_COUNT, true);
    private static volatile boolean s_isInitializationStarted = false;

    private EncryptorHolder() {
    }

    public static void initializeOnWorkerThread(Context context) {
        if (!s_isInitializationStarted) {
            s_isInitializationStarted = true;
            Executors.newSingleThreadExecutor().execute(new a(context.getApplicationContext()));
        }
    }

    public static StringCipher getEncryptor() {
        return ENCRYPTOR;
    }

    static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final Context f4526a;

        a(Context context) {
            this.f4526a = context;
        }

        public void run() {
            EncryptorHolder.ENCRYPTOR.initialize(this.f4526a);
        }
    }
}
