package com.linecorp.a.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final Object f4466a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private final String f4467b;
    private final int c;
    private final SecureRandom d;
    private final SecretKeyFactory e;
    private final Cipher f;
    private final Mac g;
    private a h;

    public b(String str) {
        this.f4467b = str;
        this.c = 5000;
        try {
            this.d = new SecureRandom();
            this.e = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            this.f = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.g = Mac.getInstance("HmacSHA256");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e2) {
            throw new RuntimeException(e2);
        }
    }

    public final void a(Context context) {
        synchronized (this.f4466a) {
            if (this.h == null) {
                this.h = b(context);
            }
        }
    }

    public final String a(Context context, String str) {
        String encodeToString;
        synchronized (this.f4466a) {
            a(context);
            try {
                byte[] bArr = new byte[this.f.getBlockSize()];
                this.d.nextBytes(bArr);
                this.f.init(1, this.h.f4468a, new IvParameterSpec(bArr));
                byte[] doFinal = this.f.doFinal(str.getBytes("UTF-8"));
                byte[] bArr2 = new byte[(bArr.length + doFinal.length + 32)];
                System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
                int length = bArr.length + 0;
                System.arraycopy(doFinal, 0, bArr2, length, doFinal.length);
                this.g.init(this.h.f4469b);
                this.g.update(bArr2, 0, bArr.length + doFinal.length);
                byte[] doFinal2 = this.g.doFinal();
                System.arraycopy(doFinal2, 0, bArr2, length + doFinal.length, doFinal2.length);
                encodeToString = Base64.encodeToString(bArr2, 0);
            } catch (BadPaddingException e2) {
                throw new a(e2);
            } catch (UnsupportedEncodingException e3) {
                e = e3;
                throw new RuntimeException(e);
            } catch (InvalidKeyException e4) {
                e = e4;
                throw new RuntimeException(e);
            } catch (IllegalBlockSizeException e5) {
                e = e5;
                throw new RuntimeException(e);
            } catch (InvalidAlgorithmParameterException e6) {
                e = e6;
                throw new RuntimeException(e);
            }
        }
        return encodeToString;
    }

    public final String b(Context context, String str) {
        String str2;
        synchronized (this.f4466a) {
            a(context);
            try {
                byte[] decode = Base64.decode(str, 0);
                byte[] copyOfRange = Arrays.copyOfRange(decode, decode.length - 32, decode.length);
                this.g.init(this.h.f4469b);
                this.g.update(decode, 0, decode.length - 32);
                if (MessageDigest.isEqual(this.g.doFinal(), copyOfRange)) {
                    this.f.init(2, this.h.f4468a, new IvParameterSpec(decode, 0, 16));
                    str2 = new String(this.f.doFinal(decode, 16, (decode.length - 16) - 32), "UTF-8");
                } else {
                    throw new a("Cipher text has been tampered with.");
                }
            } catch (BadPaddingException e2) {
                throw new a(e2);
            } catch (UnsupportedEncodingException | InvalidAlgorithmParameterException | InvalidKeyException | IllegalBlockSizeException e3) {
                throw new RuntimeException(e3);
            }
        }
        return str2;
    }

    static class a {

        /* renamed from: a  reason: collision with root package name */
        final SecretKey f4468a;

        /* renamed from: b  reason: collision with root package name */
        final SecretKey f4469b;

        a(SecretKey secretKey, SecretKey secretKey2) {
            this.f4468a = secretKey;
            this.f4469b = secretKey2;
        }
    }

    private a b(Context context) {
        byte[] bArr;
        String str = Build.MODEL + Build.MANUFACTURER + Build.SERIAL + Settings.Secure.getString(context.getContentResolver(), "android_id") + context.getPackageName();
        SharedPreferences sharedPreferences = context.getSharedPreferences(this.f4467b, 0);
        String string = sharedPreferences.getString("salt", null);
        if (!TextUtils.isEmpty(string)) {
            bArr = Base64.decode(string, 0);
        } else {
            byte[] bArr2 = new byte[16];
            this.d.nextBytes(bArr2);
            sharedPreferences.edit().putString("salt", Base64.encodeToString(bArr2, 0)).apply();
            bArr = bArr2;
        }
        try {
            byte[] encoded = this.e.generateSecret(new PBEKeySpec(str.toCharArray(), bArr, this.c, 512)).getEncoded();
            return new a(new SecretKeySpec(Arrays.copyOfRange(encoded, 0, 32), "AES"), new SecretKeySpec(Arrays.copyOfRange(encoded, 32, encoded.length), "HmacSHA256"));
        } catch (InvalidKeySpecException e2) {
            throw new RuntimeException(e2);
        }
    }
}
