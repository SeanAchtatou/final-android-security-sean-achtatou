package com.linecorp.linesdk.api.a;

import android.net.Uri;
import android.text.TextUtils;
import com.linecorp.linesdk.LineAccessToken;
import com.linecorp.linesdk.LineApiError;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineApiResponseCode;
import com.linecorp.linesdk.LineCredential;
import com.linecorp.linesdk.LineProfile;
import com.linecorp.linesdk.a.a;
import com.linecorp.linesdk.a.a.d;
import com.linecorp.linesdk.a.g;
import com.linecorp.linesdk.api.LineApiClient;
import java.util.Collections;
import java.util.HashMap;

public final class b implements LineApiClient {

    /* renamed from: a  reason: collision with root package name */
    private final String f4499a;

    /* renamed from: b  reason: collision with root package name */
    private final com.linecorp.linesdk.a.a.b f4500b;
    private final d c;
    private final a d;

    public b(String str, com.linecorp.linesdk.a.a.b bVar, d dVar, a aVar) {
        this.f4499a = str;
        this.f4500b = bVar;
        this.c = dVar;
        this.d = aVar;
    }

    public final LineApiResponse<?> logout() {
        com.linecorp.linesdk.a.d b2 = this.d.b();
        if (b2 == null) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError("access token is null"));
        }
        com.linecorp.linesdk.a.a.b bVar = this.f4500b;
        LineApiResponse<?> a2 = bVar.g.a(bVar.f.buildUpon().appendPath("oauth").appendPath("revoke").build(), Collections.emptyMap(), Collections.singletonMap("refresh_token", b2.d), com.linecorp.linesdk.a.a.b.e);
        if (a2.isSuccess()) {
            this.d.a();
        }
        return a2;
    }

    public final LineApiResponse<LineAccessToken> refreshAccessToken() {
        String str;
        com.linecorp.linesdk.a.d b2 = this.d.b();
        if (b2 == null || TextUtils.isEmpty(b2.d)) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError("access token or refresh token is not found."));
        }
        com.linecorp.linesdk.a.a.b bVar = this.f4500b;
        String str2 = this.f4499a;
        Uri build = bVar.f.buildUpon().appendPath("oauth").appendPath("accessToken").build();
        HashMap hashMap = new HashMap(3);
        hashMap.put("grant_type", "refresh_token");
        hashMap.put("refresh_token", b2.d);
        hashMap.put("client_id", str2);
        LineApiResponse a2 = bVar.g.a(build, Collections.emptyMap(), hashMap, com.linecorp.linesdk.a.a.b.d);
        if (!a2.isSuccess()) {
            return LineApiResponse.createAsError(a2.getResponseCode(), a2.getErrorData());
        }
        g gVar = (g) a2.getResponseData();
        if (TextUtils.isEmpty(gVar.c)) {
            str = b2.d;
        } else {
            str = gVar.c;
        }
        com.linecorp.linesdk.a.d dVar = new com.linecorp.linesdk.a.d(gVar.f4495a, gVar.f4496b, System.currentTimeMillis(), str);
        this.d.a(dVar);
        return LineApiResponse.createAsSuccess(new LineAccessToken(dVar.f4489a, dVar.f4490b, dVar.c));
    }

    public final LineApiResponse<LineCredential> verifyToken() {
        com.linecorp.linesdk.a.d b2 = this.d.b();
        if (b2 == null) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError("access token is null"));
        }
        com.linecorp.linesdk.a.a.b bVar = this.f4500b;
        Uri build = bVar.f.buildUpon().appendPath("oauth").appendPath("verify").build();
        HashMap hashMap = new HashMap(1);
        hashMap.put("access_token", b2.f4489a);
        LineApiResponse a2 = bVar.g.a(build, Collections.emptyMap(), hashMap, com.linecorp.linesdk.a.a.b.c);
        if (!a2.isSuccess()) {
            return LineApiResponse.createAsError(a2.getResponseCode(), a2.getErrorData());
        }
        com.linecorp.linesdk.a.b bVar2 = (com.linecorp.linesdk.a.b) a2.getResponseData();
        long currentTimeMillis = System.currentTimeMillis();
        this.d.a(new com.linecorp.linesdk.a.d(b2.f4489a, bVar2.f4484a, currentTimeMillis, b2.d));
        return LineApiResponse.createAsSuccess(new LineCredential(new LineAccessToken(b2.f4489a, bVar2.f4484a, currentTimeMillis), bVar2.f4485b));
    }

    public final LineApiResponse<LineAccessToken> getCurrentAccessToken() {
        com.linecorp.linesdk.a.d b2 = this.d.b();
        if (b2 == null) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError("The cached access token does not exist."));
        }
        return LineApiResponse.createAsSuccess(new LineAccessToken(b2.f4489a, b2.f4490b, b2.c));
    }

    @c
    public final LineApiResponse<LineProfile> getProfile() {
        com.linecorp.linesdk.a.d b2 = this.d.b();
        if (b2 == null) {
            return LineApiResponse.createAsError(LineApiResponseCode.INTERNAL_ERROR, new LineApiError("access token is null"));
        }
        return this.c.a(b2);
    }
}
