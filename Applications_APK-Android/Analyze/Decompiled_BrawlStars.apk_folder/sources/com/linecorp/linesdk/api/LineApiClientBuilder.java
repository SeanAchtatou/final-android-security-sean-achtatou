package com.linecorp.linesdk.api;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.linecorp.linesdk.BuildConfig;
import com.linecorp.linesdk.a.a;
import com.linecorp.linesdk.a.a.d;
import com.linecorp.linesdk.a.c;
import com.linecorp.linesdk.api.a.a;
import com.linecorp.linesdk.api.a.b;
import java.lang.reflect.Proxy;

public class LineApiClientBuilder {
    private final String channelId;
    private final Context context;
    private Uri endPointBaseUri;
    private boolean isEncryptorPreparationDisabled;
    private boolean isTokenAutoRefreshDisabled;

    public LineApiClientBuilder(Context context2, String str) {
        if (!TextUtils.isEmpty(str)) {
            this.context = context2.getApplicationContext();
            this.channelId = str;
            this.endPointBaseUri = Uri.parse(BuildConfig.AUTH_SERVER_BASE_URI);
            return;
        }
        throw new IllegalArgumentException("channel id is empty");
    }

    public LineApiClientBuilder disableTokenAutoRefresh() {
        this.isTokenAutoRefreshDisabled = true;
        return this;
    }

    public LineApiClientBuilder disableEncryptorPreparation() {
        this.isEncryptorPreparationDisabled = true;
        return this;
    }

    /* access modifiers changed from: package-private */
    public LineApiClientBuilder endPointBaseUri(Uri uri) {
        if (uri == null) {
            uri = Uri.parse(BuildConfig.AUTH_SERVER_BASE_URI);
        }
        this.endPointBaseUri = uri;
        return this;
    }

    public LineApiClient build() {
        if (!this.isEncryptorPreparationDisabled) {
            c.a(this.context);
        }
        b bVar = new b(this.channelId, new com.linecorp.linesdk.a.a.b(this.context, this.endPointBaseUri), new d(this.context, this.endPointBaseUri), new a(this.context, this.channelId));
        if (this.isTokenAutoRefreshDisabled) {
            return bVar;
        }
        return (LineApiClient) Proxy.newProxyInstance(bVar.getClass().getClassLoader(), new Class[]{LineApiClient.class}, new a.C0152a(bVar, (byte) 0));
    }
}
