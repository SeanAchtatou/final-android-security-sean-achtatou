package com.linecorp.linesdk.api.a;

import com.linecorp.linesdk.LineAccessToken;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.api.LineApiClient;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class a {

    /* renamed from: com.linecorp.linesdk.api.a.a$a  reason: collision with other inner class name */
    public static class C0152a implements InvocationHandler {

        /* renamed from: a  reason: collision with root package name */
        private final LineApiClient f4497a;

        /* renamed from: b  reason: collision with root package name */
        private final Map<Method, Boolean> f4498b;

        public /* synthetic */ C0152a(LineApiClient lineApiClient, byte b2) {
            this(lineApiClient);
        }

        private C0152a(LineApiClient lineApiClient) {
            this.f4497a = lineApiClient;
            this.f4498b = new ConcurrentHashMap(0);
        }

        public final Object invoke(Object obj, Method method, Object[] objArr) {
            try {
                Object invoke = method.invoke(this.f4497a, objArr);
                if (a(method)) {
                    if ((invoke instanceof LineApiResponse) && ((LineApiResponse) invoke).getErrorData().getHttpResponseCode() == 403) {
                        LineApiResponse<LineAccessToken> refreshAccessToken = this.f4497a.refreshAccessToken();
                        if (!refreshAccessToken.isSuccess()) {
                            return refreshAccessToken.isNetworkError() ? refreshAccessToken : invoke;
                        }
                        try {
                            return method.invoke(this.f4497a, objArr);
                        } catch (InvocationTargetException e) {
                            throw e.getTargetException();
                        }
                    }
                }
                return invoke;
            } catch (InvocationTargetException e2) {
                throw e2.getTargetException();
            }
        }

        private boolean a(Method method) {
            Boolean bool = this.f4498b.get(method);
            if (bool != null) {
                return bool.booleanValue();
            }
            String name = method.getName();
            Class<?>[] parameterTypes = method.getParameterTypes();
            Class<?> cls = this.f4497a.getClass();
            while (cls != null) {
                try {
                    if (((c) cls.getDeclaredMethod(name, parameterTypes).getAnnotation(c.class)) != null) {
                        this.f4498b.put(method, true);
                        return true;
                    }
                    cls = cls.getSuperclass();
                } catch (NoSuchMethodException unused) {
                }
            }
            this.f4498b.put(method, false);
            return false;
        }
    }
}
