package com.linecorp.linesdk;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.linecorp.linesdk";
    public static final String AUTH_SERVER_BASE_URI = "https://access.line.me/v2";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final String LINE_APP_PACKAGE_NAME = "jp.naver.line.android";
    public static final int VERSION_CODE = 40008;
    public static final String VERSION_NAME = "4.0.8";
    public static final String WEB_LOGIN_PAGE_URL = "https://access.line.me/dialog/oauth/weblogin";
}
