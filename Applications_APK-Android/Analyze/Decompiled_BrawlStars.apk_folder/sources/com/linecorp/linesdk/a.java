package com.linecorp.linesdk;

import android.os.Parcel;
import android.os.Parcelable;

final class a implements Parcelable.Creator<LineAccessToken> {
    a() {
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new LineAccessToken[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new LineAccessToken(parcel, null);
    }
}
