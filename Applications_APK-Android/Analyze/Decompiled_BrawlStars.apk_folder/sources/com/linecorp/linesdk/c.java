package com.linecorp.linesdk;

import android.os.Parcel;
import android.os.Parcelable;

final class c implements Parcelable.Creator<LineCredential> {
    c() {
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new LineCredential[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new LineCredential(parcel, (c) null);
    }
}
