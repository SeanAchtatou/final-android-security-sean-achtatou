package com.linecorp.linesdk;

import android.os.Parcel;
import android.os.Parcelable;

public class LineAccessToken implements Parcelable {
    public static final Parcelable.Creator<LineAccessToken> CREATOR = new a();
    private final String accessToken;
    private final long expiresInMillis;
    private final long issuedClientTimeMillis;

    public int describeContents() {
        return 0;
    }

    /* synthetic */ LineAccessToken(Parcel parcel, a aVar) {
        this(parcel);
    }

    public LineAccessToken(String str, long j, long j2) {
        this.accessToken = str;
        this.expiresInMillis = j;
        this.issuedClientTimeMillis = j2;
    }

    private LineAccessToken(Parcel parcel) {
        this.accessToken = parcel.readString();
        this.expiresInMillis = parcel.readLong();
        this.issuedClientTimeMillis = parcel.readLong();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.accessToken);
        parcel.writeLong(this.expiresInMillis);
        parcel.writeLong(this.issuedClientTimeMillis);
    }

    public String getAccessToken() {
        return this.accessToken;
    }

    public long getExpiresInMillis() {
        return this.expiresInMillis;
    }

    public long getIssuedClientTimeMillis() {
        return this.issuedClientTimeMillis;
    }

    public long getEstimatedExpirationTimeMillis() {
        return getIssuedClientTimeMillis() + getExpiresInMillis();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        LineAccessToken lineAccessToken = (LineAccessToken) obj;
        if (this.expiresInMillis == lineAccessToken.expiresInMillis && this.issuedClientTimeMillis == lineAccessToken.issuedClientTimeMillis) {
            return this.accessToken.equals(lineAccessToken.accessToken);
        }
        return false;
    }

    public int hashCode() {
        long j = this.expiresInMillis;
        long j2 = this.issuedClientTimeMillis;
        return (((this.accessToken.hashCode() * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "LineAccessToken{accessToken='#####', expiresInMillis=" + this.expiresInMillis + ", issuedClientTimeMillis=" + this.issuedClientTimeMillis + '}';
    }
}
