package com.linecorp.linesdk;

import android.os.Parcel;
import android.os.Parcelable;

final class b implements Parcelable.Creator<LineApiError> {
    b() {
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new LineApiError[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new LineApiError(parcel, (b) null);
    }
}
