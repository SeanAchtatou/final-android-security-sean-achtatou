package com.linecorp.linesdk.a;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    public final String f4489a;

    /* renamed from: b  reason: collision with root package name */
    public final long f4490b;
    public final long c;
    public final String d;

    public d(String str, long j, long j2, String str2) {
        this.f4489a = str;
        this.f4490b = j;
        this.c = j2;
        this.d = str2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        d dVar = (d) obj;
        if (this.f4490b == dVar.f4490b && this.c == dVar.c && this.f4489a.equals(dVar.f4489a)) {
            return this.d.equals(dVar.d);
        }
        return false;
    }

    public final int hashCode() {
        long j = this.f4490b;
        long j2 = this.c;
        return (((((this.f4489a.hashCode() * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.d.hashCode();
    }

    public final String toString() {
        return "InternalAccessToken{accessToken='#####', expiresInMillis=" + this.f4490b + ", issuedClientTimeMillis=" + this.c + ", refreshToken='" + this.d + '\'' + '}';
    }
}
