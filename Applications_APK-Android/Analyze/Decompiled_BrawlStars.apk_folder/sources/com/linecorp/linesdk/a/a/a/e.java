package com.linecorp.linesdk.a.a.a;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import java.util.Locale;

final class e {

    /* renamed from: a  reason: collision with root package name */
    private final PackageInfo f4477a;

    /* renamed from: b  reason: collision with root package name */
    private final String f4478b;
    private String c;

    e(Context context, String str) {
        this.f4477a = a(context);
        this.f4478b = str;
    }

    public final String a() {
        String str = this.c;
        if (str != null) {
            return str;
        }
        PackageInfo packageInfo = this.f4477a;
        String str2 = "UNK";
        String str3 = packageInfo == null ? str2 : packageInfo.packageName;
        PackageInfo packageInfo2 = this.f4477a;
        if (packageInfo2 != null) {
            str2 = packageInfo2.versionName;
        }
        Locale locale = Locale.getDefault();
        this.c = str3 + "/" + str2 + " ChannelSDK/" + this.f4478b + " (Linux; U; Android " + Build.VERSION.RELEASE + "; " + locale.getLanguage() + "-" + locale.getCountry() + "; " + Build.MODEL + " Build/" + Build.ID + ")";
        return this.c;
    }

    private static PackageInfo a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 128);
        } catch (PackageManager.NameNotFoundException unused) {
            throw null;
        }
    }
}
