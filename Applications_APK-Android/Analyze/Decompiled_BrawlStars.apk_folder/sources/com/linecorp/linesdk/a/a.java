package com.linecorp.linesdk.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.linecorp.a.a.a.b;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final Context f4470a;

    /* renamed from: b  reason: collision with root package name */
    private final String f4471b;
    private final b c;

    public a(Context context, String str) {
        this(context.getApplicationContext(), str, c.a());
    }

    private a(Context context, String str, b bVar) {
        this.f4470a = context;
        this.f4471b = "com.linecorp.linesdk.accesstoken." + str;
        this.c = bVar;
    }

    public final void a() {
        this.f4470a.getSharedPreferences(this.f4471b, 0).edit().clear().apply();
    }

    public final void a(d dVar) {
        this.f4470a.getSharedPreferences(this.f4471b, 0).edit().putString("accessToken", a(dVar.f4489a)).putString("expiresIn", a(dVar.f4490b)).putString("issuedClientTime", a(dVar.c)).putString("refreshToken", a(dVar.d)).apply();
    }

    public final d b() {
        SharedPreferences sharedPreferences = this.f4470a.getSharedPreferences(this.f4471b, 0);
        try {
            String b2 = b(sharedPreferences.getString("accessToken", null));
            long c2 = c(sharedPreferences.getString("expiresIn", null));
            long c3 = c(sharedPreferences.getString("issuedClientTime", null));
            if (TextUtils.isEmpty(b2) || c2 == -1 || c3 == -1) {
                return null;
            }
            String b3 = b(sharedPreferences.getString("refreshToken", null));
            if (b3 == null) {
                b3 = "";
            }
            return new d(b2, c2, c3, b3);
        } catch (com.linecorp.a.a.a.a unused) {
            a();
            return null;
        }
    }

    private String a(String str) {
        return this.c.a(this.f4470a, str);
    }

    private String a(long j) {
        return this.c.a(this.f4470a, String.valueOf(j));
    }

    private String b(String str) {
        if (str == null) {
            return null;
        }
        return this.c.b(this.f4470a, str);
    }

    private long c(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.valueOf(this.c.b(this.f4470a, str)).longValue();
        } catch (NumberFormatException unused) {
            return -1;
        }
    }
}
