package com.linecorp.linesdk.a;

import android.content.Context;
import com.linecorp.a.a.a.b;
import java.util.concurrent.Executors;

public final class c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final b f4486a = new b("com.linecorp.linesdk.sharedpreference.encryptionsalt");

    /* renamed from: b  reason: collision with root package name */
    private static volatile boolean f4487b = false;

    public static void a(Context context) {
        if (!f4487b) {
            f4487b = true;
            Executors.newSingleThreadExecutor().execute(new a(context.getApplicationContext()));
        }
    }

    public static b a() {
        return f4486a;
    }

    static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final Context f4488a;

        a(Context context) {
            this.f4488a = context;
        }

        public final void run() {
            c.f4486a.a(this.f4488a);
        }
    }
}
