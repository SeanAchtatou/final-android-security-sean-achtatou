package com.linecorp.linesdk.a;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    public final String f4495a;

    /* renamed from: b  reason: collision with root package name */
    public final long f4496b;
    public final String c;

    public g(String str, long j, String str2) {
        this.f4495a = str;
        this.f4496b = j;
        this.c = str2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            g gVar = (g) obj;
            if (this.f4496b != gVar.f4496b || !this.f4495a.equals(gVar.f4495a)) {
                return false;
            }
            String str = this.c;
            String str2 = gVar.c;
            if (str != null) {
                return str.equals(str2);
            }
            if (str2 == null) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        long j = this.f4496b;
        int hashCode = ((this.f4495a.hashCode() * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        String str = this.c;
        return hashCode + (str != null ? str.hashCode() : 0);
    }

    public final String toString() {
        return "RefreshTokenResult{accessToken='#####', expiresInMillis=" + this.f4496b + ", refreshToken='#####'}";
    }
}
