package com.linecorp.linesdk.a;

import java.util.Collections;
import java.util.List;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    public final d f4491a;

    /* renamed from: b  reason: collision with root package name */
    public final List<String> f4492b;

    public e(d dVar, List<String> list) {
        this.f4491a = dVar;
        this.f4492b = Collections.unmodifiableList(list);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        if (!this.f4491a.equals(eVar.f4491a)) {
            return false;
        }
        return this.f4492b.equals(eVar.f4492b);
    }

    public final int hashCode() {
        return (this.f4491a.hashCode() * 31) + this.f4492b.hashCode();
    }

    public final String toString() {
        return "IssueAccessTokenResult{accessToken=" + ((Object) "#####") + ", permissions=" + this.f4492b + '}';
    }
}
