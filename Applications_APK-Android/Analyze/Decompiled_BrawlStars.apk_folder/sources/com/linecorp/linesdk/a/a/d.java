package com.linecorp.linesdk.a.a;

import android.content.Context;
import android.net.Uri;
import com.linecorp.linesdk.BuildConfig;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LineProfile;
import com.linecorp.linesdk.a.a.a.c;
import java.util.Collections;
import java.util.HashMap;
import org.json.JSONObject;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final c<LineProfile> f4482a = new a();

    /* renamed from: b  reason: collision with root package name */
    private final Uri f4483b;
    private final com.linecorp.linesdk.a.a.a.a c;

    public d(Context context, Uri uri) {
        this(uri, new com.linecorp.linesdk.a.a.a.a(context, BuildConfig.VERSION_NAME));
    }

    private d(Uri uri, com.linecorp.linesdk.a.a.a.a aVar) {
        this.f4483b = uri;
        this.c = aVar;
    }

    public final LineApiResponse<LineProfile> a(com.linecorp.linesdk.a.d dVar) {
        Uri build = this.f4483b.buildUpon().appendPath("profile").build();
        HashMap hashMap = new HashMap(1);
        hashMap.put("Authorization", "Bearer " + dVar.f4489a);
        return this.c.b(build, hashMap, Collections.emptyMap(), f4482a);
    }

    static class a extends a<LineProfile> {
        a() {
        }

        /* access modifiers changed from: package-private */
        public final /* synthetic */ Object a(JSONObject jSONObject) {
            Uri uri;
            String optString = jSONObject.optString("pictureUrl", null);
            String string = jSONObject.getString("userId");
            String string2 = jSONObject.getString("displayName");
            if (optString == null) {
                uri = null;
            } else {
                uri = Uri.parse(optString);
            }
            return new LineProfile(string, string2, uri, jSONObject.optString("statusMessage", null));
        }
    }
}
