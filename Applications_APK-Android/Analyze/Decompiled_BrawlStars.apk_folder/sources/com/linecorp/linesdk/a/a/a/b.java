package com.linecorp.linesdk.a.a.a;

import java.io.IOException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;

public final class b implements c<JSONObject> {

    /* renamed from: a  reason: collision with root package name */
    private final d f4475a = new d();

    /* renamed from: b */
    public final JSONObject a(InputStream inputStream) {
        try {
            return new JSONObject(this.f4475a.a(inputStream));
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }
}
