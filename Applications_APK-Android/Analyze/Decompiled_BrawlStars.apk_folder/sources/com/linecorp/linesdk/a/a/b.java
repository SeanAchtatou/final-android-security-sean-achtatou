package com.linecorp.linesdk.a.a;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.linecorp.linesdk.BuildConfig;
import com.linecorp.linesdk.a.e;
import com.linecorp.linesdk.a.f;
import com.linecorp.linesdk.a.g;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public static final com.linecorp.linesdk.a.a.a.c<f> f4479a = new C0151b((byte) 0);

    /* renamed from: b  reason: collision with root package name */
    public static final com.linecorp.linesdk.a.a.a.c<e> f4480b = new a((byte) 0);
    public static final com.linecorp.linesdk.a.a.a.c<com.linecorp.linesdk.a.b> c = new d((byte) 0);
    public static final com.linecorp.linesdk.a.a.a.c<g> d = new c((byte) 0);
    public static final com.linecorp.linesdk.a.a.a.c<?> e = new c();
    public final Uri f;
    public final com.linecorp.linesdk.a.a.a.a g;

    public b(Context context, Uri uri) {
        this(uri, new com.linecorp.linesdk.a.a.a.a(context, BuildConfig.VERSION_NAME));
    }

    private b(Uri uri, com.linecorp.linesdk.a.a.a.a aVar) {
        this.f = uri;
        this.g = aVar;
    }

    /* renamed from: com.linecorp.linesdk.a.a.b$b  reason: collision with other inner class name */
    static class C0151b extends a<f> {
        private C0151b() {
        }

        /* synthetic */ C0151b(byte b2) {
            this();
        }

        /* access modifiers changed from: package-private */
        public final /* synthetic */ Object a(JSONObject jSONObject) {
            return new f(jSONObject.getString("otpId"), jSONObject.getString("otp"));
        }
    }

    static class a extends a<e> {
        private a() {
        }

        /* synthetic */ a(byte b2) {
            this();
        }

        /* access modifiers changed from: package-private */
        public final /* synthetic */ Object a(JSONObject jSONObject) {
            List list;
            String string = jSONObject.getString("token_type");
            if ("Bearer".equals(string)) {
                String optString = jSONObject.optString("scope");
                com.linecorp.linesdk.a.d dVar = new com.linecorp.linesdk.a.d(jSONObject.getString("access_token"), jSONObject.getLong(AccessToken.EXPIRES_IN_KEY) * 1000, System.currentTimeMillis(), jSONObject.getString("refresh_token"));
                if (TextUtils.isEmpty(optString)) {
                    list = Collections.emptyList();
                } else {
                    list = Arrays.asList(optString.split(" "));
                }
                return new e(dVar, list);
            }
            throw new JSONException("Illegal token type. token_type=" + string);
        }
    }

    static class d extends a<com.linecorp.linesdk.a.b> {
        private d() {
        }

        /* synthetic */ d(byte b2) {
            this();
        }

        /* access modifiers changed from: package-private */
        public final /* synthetic */ Object a(JSONObject jSONObject) {
            List list;
            String optString = jSONObject.optString("scope");
            String string = jSONObject.getString("client_id");
            long j = jSONObject.getLong(AccessToken.EXPIRES_IN_KEY) * 1000;
            if (TextUtils.isEmpty(optString)) {
                list = Collections.emptyList();
            } else {
                list = Arrays.asList(optString.split(" "));
            }
            return new com.linecorp.linesdk.a.b(string, j, list);
        }
    }

    static class c extends a<g> {
        private c() {
        }

        /* synthetic */ c(byte b2) {
            this();
        }

        /* access modifiers changed from: package-private */
        public final /* synthetic */ Object a(JSONObject jSONObject) {
            String string = jSONObject.getString("token_type");
            if ("Bearer".equals(string)) {
                return new g(jSONObject.getString("access_token"), jSONObject.getLong(AccessToken.EXPIRES_IN_KEY) * 1000, jSONObject.optString("refresh_token"));
            }
            throw new JSONException("Illegal token type. token_type=" + string);
        }
    }
}
