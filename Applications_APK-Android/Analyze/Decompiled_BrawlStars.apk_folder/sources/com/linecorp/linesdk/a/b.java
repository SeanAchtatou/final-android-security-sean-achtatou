package com.linecorp.linesdk.a;

import java.util.Collections;
import java.util.List;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public final long f4484a;

    /* renamed from: b  reason: collision with root package name */
    public final List<String> f4485b;
    private final String c;

    public b(String str, long j, List<String> list) {
        this.c = str;
        this.f4484a = j;
        this.f4485b = Collections.unmodifiableList(list);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        if (this.c.equals(bVar.c) && this.f4484a == bVar.f4484a) {
            return this.f4485b.equals(bVar.f4485b);
        }
        return false;
    }

    public final int hashCode() {
        long j = this.f4484a;
        return (((Integer.valueOf(this.c).intValue() * 31) + ((int) (j ^ (j >>> 32)))) * 31) + this.f4485b.hashCode();
    }

    public final String toString() {
        return "AccessTokenVerificationResult{expiresInMillis=" + this.f4484a + ", channelId=" + this.c + ", permissions=" + this.f4485b + '}';
    }
}
