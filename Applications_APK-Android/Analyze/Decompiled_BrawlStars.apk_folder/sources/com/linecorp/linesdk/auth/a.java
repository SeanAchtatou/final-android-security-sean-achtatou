package com.linecorp.linesdk.auth;

import android.os.Parcel;
import android.os.Parcelable;

final class a implements Parcelable.Creator<LineAuthenticationConfig> {
    a() {
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new LineAuthenticationConfig[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new LineAuthenticationConfig(parcel, (a) null);
    }
}
