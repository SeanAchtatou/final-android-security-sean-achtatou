package com.linecorp.linesdk.auth.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.linecorp.linesdk.LineApiError;
import com.linecorp.linesdk.LineApiResponseCode;
import com.linecorp.linesdk.R;
import com.linecorp.linesdk.auth.LineAuthenticationConfig;
import com.linecorp.linesdk.auth.LineLoginResult;
import com.linecorp.linesdk.auth.internal.a;
import com.linecorp.linesdk.auth.internal.c;
import com.linecorp.linesdk.auth.internal.d;
import java.util.List;

public class LineAuthenticationActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4501a = false;

    /* renamed from: b  reason: collision with root package name */
    private d f4502b;
    private c c;

    public static Intent a(Context context, LineAuthenticationConfig lineAuthenticationConfig, List<String> list) {
        Intent intent = new Intent(context, LineAuthenticationActivity.class);
        intent.putExtra("authentication_config", lineAuthenticationConfig);
        intent.putExtra(NativeProtocol.RESULT_ARGS_PERMISSIONS, (String[]) list.toArray(new String[list.size()]));
        return intent;
    }

    public static LineLoginResult a(Intent intent) {
        LineLoginResult lineLoginResult = (LineLoginResult) intent.getParcelableExtra("authentication_result");
        return lineLoginResult == null ? new LineLoginResult(LineApiResponseCode.INTERNAL_ERROR, new LineApiError("Authentication result is not found.")) : lineLoginResult;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        d dVar;
        super.onCreate(bundle);
        setContentView(R.layout.linesdk_activity_lineauthentication);
        Intent intent = getIntent();
        LineAuthenticationConfig lineAuthenticationConfig = (LineAuthenticationConfig) intent.getParcelableExtra("authentication_config");
        if (lineAuthenticationConfig == null) {
            a(new LineLoginResult(LineApiResponseCode.INTERNAL_ERROR, new LineApiError("The requested parameter is illegal.")));
            return;
        }
        if (bundle == null) {
            dVar = new d();
        } else {
            dVar = (d) bundle.getParcelable("authenticationStatus");
            if (dVar == null) {
                dVar = new d();
            }
        }
        this.f4502b = dVar;
        this.c = new c(this, lineAuthenticationConfig, this.f4502b, intent.getStringArrayExtra(NativeProtocol.RESULT_ARGS_PERMISSIONS));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f4502b.d == d.a.f4520a) {
            c cVar = this.c;
            cVar.h.d = d.a.f4521b;
            new c.C0154c(cVar, (byte) 0).execute(new Void[0]);
        } else if (this.f4502b.d != d.a.c) {
            new Handler(Looper.getMainLooper()).postDelayed(new c.b(this.c, (byte) 0), 1000);
        }
        this.f4501a = false;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.f4501a = true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        a.c cVar;
        super.onNewIntent(intent);
        if (this.f4502b.d == d.a.f4521b) {
            c cVar2 = this.c;
            cVar2.h.d = d.a.c;
            a aVar = cVar2.e;
            Uri data = intent.getData();
            if (data == null) {
                cVar = a.c.a("Illegal redirection from external application.");
            } else {
                String str = aVar.f4504b.c;
                String queryParameter = data.getQueryParameter(ServerProtocol.DIALOG_PARAM_STATE);
                if (str == null || !str.equals(queryParameter)) {
                    cVar = a.c.a("Illegal parameter value of 'state'.");
                } else {
                    String queryParameter2 = data.getQueryParameter("code");
                    cVar = !TextUtils.isEmpty(queryParameter2) ? new a.c(queryParameter2, null, null, null) : new a.c(null, data.getQueryParameter("error"), data.getQueryParameter(NativeProtocol.BRIDGE_ARG_ERROR_DESCRIPTION), null);
                }
            }
            if (!cVar.a()) {
                cVar2.h.d = d.a.d;
                cVar2.f4513a.a(new LineLoginResult(cVar.b() ? LineApiResponseCode.AUTHENTICATION_AGENT_ERROR : LineApiResponseCode.INTERNAL_ERROR, cVar.c()));
                return;
            }
            c.a aVar2 = new c.a(cVar2, (byte) 0);
            String[] strArr = new String[1];
            if (!TextUtils.isEmpty(cVar.f4509a)) {
                strArr[0] = cVar.f4509a;
                aVar2.execute(strArr);
                return;
            }
            throw new UnsupportedOperationException("requestToken is null. Please check result by isSuccess before.");
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (this.f4502b.d == d.a.f4521b) {
            c cVar = this.c;
            if (i == 3 && cVar.h.d != d.a.c) {
                new Handler(Looper.getMainLooper()).postDelayed(new c.b(cVar, (byte) 0), 1000);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("authenticationStatus", this.f4502b);
    }

    /* access modifiers changed from: package-private */
    public final void a(LineLoginResult lineLoginResult) {
        d dVar = this.f4502b;
        if (dVar == null) {
            finish();
        } else if ((dVar.d == d.a.f4521b && !this.f4501a) || this.f4502b.d == d.a.d) {
            Intent intent = new Intent();
            intent.putExtra("authentication_result", lineLoginResult);
            setResult(-1, intent);
            finish();
        }
    }
}
