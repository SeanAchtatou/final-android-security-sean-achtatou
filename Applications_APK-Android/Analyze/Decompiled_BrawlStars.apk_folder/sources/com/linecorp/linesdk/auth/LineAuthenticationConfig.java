package com.linecorp.linesdk.auth;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.linecorp.linesdk.BuildConfig;

public class LineAuthenticationConfig implements Parcelable {
    public static final Parcelable.Creator<LineAuthenticationConfig> CREATOR = new a();
    private static int FLAGS_ENCRYPTOR_PREPARATION_DISABLED = 2;
    private static int FLAGS_LINE_APP_AUTHENTICATION_DISABLED = 1;
    private final String channelId;
    private final Uri endPointBaseUrl;
    private final boolean isEncryptorPreparationDisabled;
    private final boolean isLineAppAuthenticationDisabled;
    private final Uri webLoginPageUrl;

    public int describeContents() {
        return 0;
    }

    /* synthetic */ LineAuthenticationConfig(Parcel parcel, a aVar) {
        this(parcel);
    }

    /* synthetic */ LineAuthenticationConfig(Builder builder, a aVar) {
        this(builder);
    }

    private LineAuthenticationConfig(Builder builder) {
        this.channelId = builder.channelId;
        this.endPointBaseUrl = builder.endPointBaseUrl;
        this.webLoginPageUrl = builder.webLoginPageUrl;
        this.isLineAppAuthenticationDisabled = builder.isLineAppAuthenticationDisabled;
        this.isEncryptorPreparationDisabled = builder.isEncryptorPreparationDisabled;
    }

    private LineAuthenticationConfig(Parcel parcel) {
        this.channelId = parcel.readString();
        this.endPointBaseUrl = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.webLoginPageUrl = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        int readInt = parcel.readInt();
        boolean z = true;
        this.isLineAppAuthenticationDisabled = (FLAGS_LINE_APP_AUTHENTICATION_DISABLED & readInt) > 0;
        this.isEncryptorPreparationDisabled = (readInt & FLAGS_ENCRYPTOR_PREPARATION_DISABLED) <= 0 ? false : z;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.channelId);
        parcel.writeParcelable(this.endPointBaseUrl, i);
        parcel.writeParcelable(this.webLoginPageUrl, i);
        int i2 = 0;
        int i3 = (this.isLineAppAuthenticationDisabled ? FLAGS_LINE_APP_AUTHENTICATION_DISABLED : 0) | 0;
        if (this.isEncryptorPreparationDisabled) {
            i2 = FLAGS_ENCRYPTOR_PREPARATION_DISABLED;
        }
        parcel.writeInt(i3 | i2);
    }

    public String getChannelId() {
        return this.channelId;
    }

    public Uri getEndPointBaseUrl() {
        return this.endPointBaseUrl;
    }

    public Uri getWebLoginPageUrl() {
        return this.webLoginPageUrl;
    }

    public boolean isLineAppAuthenticationDisabled() {
        return this.isLineAppAuthenticationDisabled;
    }

    public boolean isEncryptorPreparationDisabled() {
        return this.isEncryptorPreparationDisabled;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        LineAuthenticationConfig lineAuthenticationConfig = (LineAuthenticationConfig) obj;
        if (this.isLineAppAuthenticationDisabled == lineAuthenticationConfig.isLineAppAuthenticationDisabled && this.isEncryptorPreparationDisabled == lineAuthenticationConfig.isEncryptorPreparationDisabled && this.channelId.equals(lineAuthenticationConfig.channelId) && this.endPointBaseUrl.equals(lineAuthenticationConfig.endPointBaseUrl)) {
            return this.webLoginPageUrl.equals(lineAuthenticationConfig.webLoginPageUrl);
        }
        return false;
    }

    public int hashCode() {
        return (((((((this.channelId.hashCode() * 31) + this.endPointBaseUrl.hashCode()) * 31) + this.webLoginPageUrl.hashCode()) * 31) + (this.isLineAppAuthenticationDisabled ? 1 : 0)) * 31) + (this.isEncryptorPreparationDisabled ? 1 : 0);
    }

    public String toString() {
        return "LineAuthenticationConfig{channelId=" + this.channelId + ", endPointBaseUrl=" + this.endPointBaseUrl + ", webLoginPageUrl=" + this.webLoginPageUrl + ", isLineAppAuthenticationDisabled=" + this.isLineAppAuthenticationDisabled + ", isEncryptorPreparationDisabled=" + this.isEncryptorPreparationDisabled + '}';
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public final String channelId;
        /* access modifiers changed from: private */
        public Uri endPointBaseUrl;
        /* access modifiers changed from: private */
        public boolean isEncryptorPreparationDisabled;
        /* access modifiers changed from: private */
        public boolean isLineAppAuthenticationDisabled;
        /* access modifiers changed from: private */
        public Uri webLoginPageUrl;

        public Builder(String str) {
            if (!TextUtils.isEmpty(str)) {
                this.channelId = str;
                this.endPointBaseUrl = Uri.parse(BuildConfig.AUTH_SERVER_BASE_URI);
                this.webLoginPageUrl = Uri.parse(BuildConfig.WEB_LOGIN_PAGE_URL);
                return;
            }
            throw new IllegalArgumentException("channelId is empty.");
        }

        /* access modifiers changed from: package-private */
        public Builder endPointBaseUrl(Uri uri) {
            if (uri == null) {
                uri = Uri.parse(BuildConfig.AUTH_SERVER_BASE_URI);
            }
            this.endPointBaseUrl = uri;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder webLoginPageUrl(Uri uri) {
            if (uri == null) {
                uri = Uri.parse(BuildConfig.WEB_LOGIN_PAGE_URL);
            }
            this.webLoginPageUrl = uri;
            return this;
        }

        public Builder disableLineAppAuthentication() {
            this.isLineAppAuthenticationDisabled = true;
            return this;
        }

        public Builder disableEncryptorPreparation() {
            this.isEncryptorPreparationDisabled = true;
            return this;
        }

        public LineAuthenticationConfig build() {
            return new LineAuthenticationConfig(this, (a) null);
        }
    }
}
