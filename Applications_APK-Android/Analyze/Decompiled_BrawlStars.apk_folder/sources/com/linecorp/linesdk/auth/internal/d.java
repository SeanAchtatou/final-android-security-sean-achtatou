package com.linecorp.linesdk.auth.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.linecorp.linesdk.a.f;

final class d implements Parcelable {
    public static final Parcelable.Creator<d> CREATOR = new e();

    /* renamed from: a  reason: collision with root package name */
    f f4518a;

    /* renamed from: b  reason: collision with root package name */
    String f4519b;
    String c;
    int d;

    public final int describeContents() {
        return 0;
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    static final class a extends Enum<a> {

        /* renamed from: a  reason: collision with root package name */
        public static final int f4520a = 1;

        /* renamed from: b  reason: collision with root package name */
        public static final int f4521b = 2;
        public static final int c = 3;
        public static final int d = 4;
        private static final /* synthetic */ int[] e = {f4520a, f4521b, c, d};

        public static int[] a() {
            return (int[]) e.clone();
        }
    }

    /* synthetic */ d(Parcel parcel, byte b2) {
        this(parcel);
    }

    d() {
        this.d = a.f4520a;
    }

    private d(Parcel parcel) {
        this.d = a.f4520a;
        String readString = parcel.readString();
        String readString2 = parcel.readString();
        this.f4518a = (TextUtils.isEmpty(readString) || TextUtils.isEmpty(readString2)) ? null : new f(readString, readString2);
        this.f4519b = parcel.readString();
        this.d = a.a()[parcel.readByte()];
        this.c = parcel.readString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        f fVar = this.f4518a;
        String str = null;
        parcel.writeString(fVar == null ? null : fVar.f4493a);
        f fVar2 = this.f4518a;
        if (fVar2 != null) {
            str = fVar2.f4494b;
        }
        parcel.writeString(str);
        parcel.writeString(this.f4519b);
        parcel.writeByte((byte) (this.d - 1));
        parcel.writeString(this.c);
    }
}
