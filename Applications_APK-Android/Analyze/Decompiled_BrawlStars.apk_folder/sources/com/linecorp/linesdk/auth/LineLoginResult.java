package com.linecorp.linesdk.auth;

import android.os.Parcel;
import android.os.Parcelable;
import com.linecorp.linesdk.LineAccessToken;
import com.linecorp.linesdk.LineApiError;
import com.linecorp.linesdk.LineApiResponseCode;
import com.linecorp.linesdk.LineCredential;
import com.linecorp.linesdk.LineProfile;

public class LineLoginResult implements Parcelable {
    public static final LineLoginResult CANCEL = new LineLoginResult(LineApiResponseCode.CANCEL, LineApiError.DEFAULT);
    public static final Parcelable.Creator<LineLoginResult> CREATOR = new b();
    private final LineApiError errorData;
    private final LineCredential lineCredential;
    private final LineProfile lineProfile;
    private final LineApiResponseCode responseCode;

    public int describeContents() {
        return 0;
    }

    /* synthetic */ LineLoginResult(Parcel parcel, b bVar) {
        this(parcel);
    }

    public LineLoginResult(LineProfile lineProfile2, LineCredential lineCredential2) {
        this(LineApiResponseCode.SUCCESS, lineProfile2, lineCredential2, LineApiError.DEFAULT);
    }

    public LineLoginResult(LineApiResponseCode lineApiResponseCode, LineApiError lineApiError) {
        this(lineApiResponseCode, null, null, lineApiError);
    }

    LineLoginResult(LineApiResponseCode lineApiResponseCode, LineProfile lineProfile2, LineCredential lineCredential2, LineApiError lineApiError) {
        this.responseCode = lineApiResponseCode;
        this.lineProfile = lineProfile2;
        this.lineCredential = lineCredential2;
        this.errorData = lineApiError;
    }

    private LineLoginResult(Parcel parcel) {
        this.responseCode = (LineApiResponseCode) parcel.readSerializable();
        this.lineProfile = (LineProfile) parcel.readParcelable(LineProfile.class.getClassLoader());
        this.lineCredential = (LineCredential) parcel.readParcelable(LineAccessToken.class.getClassLoader());
        this.errorData = (LineApiError) parcel.readParcelable(LineApiError.class.getClassLoader());
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(this.responseCode);
        parcel.writeParcelable(this.lineProfile, i);
        parcel.writeParcelable(this.lineCredential, i);
        parcel.writeParcelable(this.errorData, i);
    }

    public boolean isSuccess() {
        return this.responseCode == LineApiResponseCode.SUCCESS;
    }

    public LineApiResponseCode getResponseCode() {
        return this.responseCode;
    }

    public LineProfile getLineProfile() {
        return this.lineProfile;
    }

    public LineCredential getLineCredential() {
        return this.lineCredential;
    }

    public LineApiError getErrorData() {
        return this.errorData;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        LineLoginResult lineLoginResult = (LineLoginResult) obj;
        if (this.responseCode != lineLoginResult.responseCode) {
            return false;
        }
        LineProfile lineProfile2 = this.lineProfile;
        if (lineProfile2 == null ? lineLoginResult.lineProfile != null : !lineProfile2.equals(lineLoginResult.lineProfile)) {
            return false;
        }
        LineCredential lineCredential2 = this.lineCredential;
        if (lineCredential2 == null ? lineLoginResult.lineCredential == null : lineCredential2.equals(lineLoginResult.lineCredential)) {
            return this.errorData.equals(lineLoginResult.errorData);
        }
        return false;
    }

    public int hashCode() {
        int hashCode = this.responseCode.hashCode() * 31;
        LineProfile lineProfile2 = this.lineProfile;
        int i = 0;
        int hashCode2 = (hashCode + (lineProfile2 != null ? lineProfile2.hashCode() : 0)) * 31;
        LineCredential lineCredential2 = this.lineCredential;
        if (lineCredential2 != null) {
            i = lineCredential2.hashCode();
        }
        return ((hashCode2 + i) * 31) + this.errorData.hashCode();
    }

    public String toString() {
        return "LineLoginResult{errorData=" + this.errorData + ", responseCode=" + this.responseCode + ", lineProfile=" + this.lineProfile + ", lineCredential=" + this.lineCredential + '}';
    }
}
