package com.linecorp.linesdk.auth;

import android.os.Parcel;
import android.os.Parcelable;

final class b implements Parcelable.Creator<LineLoginResult> {
    b() {
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new LineLoginResult[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new LineLoginResult(parcel, (b) null);
    }
}
