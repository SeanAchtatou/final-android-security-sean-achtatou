package com.linecorp.linesdk.auth.internal;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.linecorp.linesdk.LineApiError;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

final class a {

    /* renamed from: a  reason: collision with root package name */
    static final b f4503a = new b(6, 9, 0);

    /* renamed from: b  reason: collision with root package name */
    final d f4504b;

    static class b {

        /* renamed from: a  reason: collision with root package name */
        final Intent f4507a;

        /* renamed from: b  reason: collision with root package name */
        final Bundle f4508b;
        final String c;
        final boolean d;

        b(Intent intent, Bundle bundle, String str, boolean z) {
            this.f4507a = intent;
            this.f4508b = bundle;
            this.c = str;
            this.d = z;
        }
    }

    a(d dVar) {
        this.f4504b = dVar;
    }

    static List<Intent> a(Uri uri, Collection<ResolveInfo> collection, Bundle bundle) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (ResolveInfo resolveInfo : collection) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(uri);
            intent.setPackage(resolveInfo.activityInfo.packageName);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            arrayList.add(intent);
        }
        return arrayList;
    }

    static class c {

        /* renamed from: a  reason: collision with root package name */
        final String f4509a;

        /* renamed from: b  reason: collision with root package name */
        private final String f4510b;
        private final String c;
        private final String d;

        c(String str, String str2, String str3, String str4) {
            this.f4509a = str;
            this.f4510b = str2;
            this.c = str3;
            this.d = str4;
        }

        static c a(String str) {
            return new c(null, null, null, str);
        }

        /* access modifiers changed from: package-private */
        public final boolean a() {
            return !TextUtils.isEmpty(this.f4509a);
        }

        /* access modifiers changed from: package-private */
        public final boolean b() {
            return TextUtils.isEmpty(this.d) && !a();
        }

        /* access modifiers changed from: package-private */
        public final LineApiError c() {
            if (!b()) {
                return new LineApiError(this.d);
            }
            try {
                return new LineApiError(new JSONObject().putOpt("error", this.f4510b).putOpt(NativeProtocol.BRIDGE_ARG_ERROR_DESCRIPTION, this.c).toString());
            } catch (JSONException e) {
                return new LineApiError(e);
            }
        }
    }

    /* renamed from: com.linecorp.linesdk.auth.internal.a$a  reason: collision with other inner class name */
    static class C0153a {

        /* renamed from: a  reason: collision with root package name */
        final Intent f4505a;

        /* renamed from: b  reason: collision with root package name */
        final Bundle f4506b;
        final boolean c;

        C0153a(Intent intent, Bundle bundle, boolean z) {
            this.f4505a = intent;
            this.f4506b = bundle;
            this.c = z;
        }
    }
}
