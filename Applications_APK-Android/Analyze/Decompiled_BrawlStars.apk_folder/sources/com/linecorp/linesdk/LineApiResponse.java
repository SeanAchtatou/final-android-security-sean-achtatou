package com.linecorp.linesdk;

import java.util.NoSuchElementException;

public class LineApiResponse<R> {
    private static final LineApiResponse<?> EMPTY_RESULT_SUCCESS = new LineApiResponse<>(LineApiResponseCode.SUCCESS, null, LineApiError.DEFAULT);
    private final LineApiError errorData;
    private final LineApiResponseCode responseCode;
    private final R responseData;

    private LineApiResponse(LineApiResponseCode lineApiResponseCode, R r, LineApiError lineApiError) {
        this.responseCode = lineApiResponseCode;
        this.responseData = r;
        this.errorData = lineApiError;
    }

    public static <T> LineApiResponse<T> createAsSuccess(T t) {
        return t == null ? EMPTY_RESULT_SUCCESS : new LineApiResponse<>(LineApiResponseCode.SUCCESS, t, LineApiError.DEFAULT);
    }

    public static <T> LineApiResponse<T> createAsError(LineApiResponseCode lineApiResponseCode, LineApiError lineApiError) {
        return new LineApiResponse<>(lineApiResponseCode, null, lineApiError);
    }

    public boolean isSuccess() {
        return this.responseCode == LineApiResponseCode.SUCCESS;
    }

    public boolean isNetworkError() {
        return this.responseCode == LineApiResponseCode.NETWORK_ERROR;
    }

    public boolean isServerError() {
        return this.responseCode == LineApiResponseCode.SERVER_ERROR;
    }

    public LineApiResponseCode getResponseCode() {
        return this.responseCode;
    }

    public R getResponseData() {
        R r = this.responseData;
        if (r != null) {
            return r;
        }
        throw new NoSuchElementException("response data is null. Please check result by isSuccess before.");
    }

    public LineApiError getErrorData() {
        return this.errorData;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: R
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            if (r3 != r4) goto L_0x0004
            r4 = 1
            return r4
        L_0x0004:
            r0 = 0
            if (r4 == 0) goto L_0x0036
            java.lang.Class r1 = r3.getClass()
            java.lang.Class r2 = r4.getClass()
            if (r1 == r2) goto L_0x0012
            goto L_0x0036
        L_0x0012:
            com.linecorp.linesdk.LineApiResponse r4 = (com.linecorp.linesdk.LineApiResponse) r4
            com.linecorp.linesdk.LineApiResponseCode r1 = r3.responseCode
            com.linecorp.linesdk.LineApiResponseCode r2 = r4.responseCode
            if (r1 == r2) goto L_0x001b
            return r0
        L_0x001b:
            R r1 = r3.responseData
            if (r1 == 0) goto L_0x0028
            R r2 = r4.responseData
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x002d
            goto L_0x002c
        L_0x0028:
            R r1 = r4.responseData
            if (r1 == 0) goto L_0x002d
        L_0x002c:
            return r0
        L_0x002d:
            com.linecorp.linesdk.LineApiError r0 = r3.errorData
            com.linecorp.linesdk.LineApiError r4 = r4.errorData
            boolean r4 = r0.equals(r4)
            return r4
        L_0x0036:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.linecorp.linesdk.LineApiResponse.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int hashCode = this.responseCode.hashCode() * 31;
        R r = this.responseData;
        return ((hashCode + (r != null ? r.hashCode() : 0)) * 31) + this.errorData.hashCode();
    }

    public String toString() {
        return "LineApiResponse{errorData=" + this.errorData + ", responseCode=" + this.responseCode + ", responseData=" + ((Object) this.responseData) + '}';
    }
}
