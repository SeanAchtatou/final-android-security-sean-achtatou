package com.linecorp.linesdk;

import android.os.Parcel;
import android.os.Parcelable;

final class d implements Parcelable.Creator<LineProfile> {
    d() {
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new LineProfile[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new LineProfile(parcel, null);
    }
}
