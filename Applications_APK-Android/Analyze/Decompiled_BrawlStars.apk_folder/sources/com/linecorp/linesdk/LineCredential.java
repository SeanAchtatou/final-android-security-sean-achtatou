package com.linecorp.linesdk;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LineCredential implements Parcelable {
    public static final Parcelable.Creator<LineCredential> CREATOR = new c();
    private final LineAccessToken accessToken;
    private final List<String> permission;

    public int describeContents() {
        return 0;
    }

    /* synthetic */ LineCredential(Parcel parcel, c cVar) {
        this(parcel);
    }

    public LineCredential(LineAccessToken lineAccessToken, List<String> list) {
        this.accessToken = lineAccessToken;
        this.permission = list;
    }

    private LineCredential(Parcel parcel) {
        this.accessToken = (LineAccessToken) parcel.readParcelable(LineAccessToken.class.getClassLoader());
        ArrayList arrayList = new ArrayList(8);
        parcel.readStringList(arrayList);
        this.permission = Collections.unmodifiableList(arrayList);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.accessToken, i);
        parcel.writeStringList(this.permission);
    }

    public LineAccessToken getAccessToken() {
        return this.accessToken;
    }

    public List<String> getPermission() {
        return this.permission;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        LineCredential lineCredential = (LineCredential) obj;
        if (!this.accessToken.equals(lineCredential.accessToken)) {
            return false;
        }
        return this.permission.equals(lineCredential.permission);
    }

    public int hashCode() {
        return (this.accessToken.hashCode() * 31) + this.permission.hashCode();
    }

    public String toString() {
        return "LineCredential{accessToken=" + ((Object) "#####") + ", permission=" + this.permission + '}';
    }
}
