package com.a.a.a;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.ServerProtocol;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.MessengerShareContentUtility;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/* compiled from: EmulatorDetector */
public final class a {
    private static final String[] c = {"15555215554", "15555215556", "15555215558", "15555215560", "15555215562", "15555215564", "15555215566", "15555215568", "15555215570", "15555215572", "15555215574", "15555215576", "15555215578", "15555215580", "15555215582", "15555215584"};
    private static final String[] d = {"000000000000000", "e21833235b6eef10", "012345678912345"};
    private static final String[] e = {"310260000000000"};
    private static final String[] f = {"/dev/socket/genyd", "/dev/socket/baseband_genyd"};
    private static final String[] g = {"goldfish"};
    private static final String[] h = {"/dev/socket/qemud", "/dev/qemu_pipe"};
    private static final String[] i = {"ueventd.android_x86.rc", "x86.prop", "ueventd.ttVM_x86.rc", "init.ttVM_x86.rc", "fstab.ttVM_x86", "fstab.vbox86", "init.vbox86.rc", "ueventd.vbox86.rc"};
    private static final String[] j = {"fstab.andy", "ueventd.andy.rc"};
    private static final String[] k = {"fstab.nox", "init.nox.rc", "ueventd.nox.rc", "/system/bin/nox-prop"};
    private static final c[] l = {new c("init.svc.qemud", null), new c("init.svc.qemu-props", null), new c("qemu.hw.mainkeys", null), new c("qemu.sf.fake_camera", null), new c("qemu.sf.lcd_density", null), new c("ro.bootloader", "unknown"), new c("ro.bootmode", "unknown"), new c("ro.hardware", "goldfish"), new c("ro.kernel.android.qemud", null), new c("ro.kernel.qemu.gles", null), new c("ro.kernel.qemu", AppEventsConstants.EVENT_PARAM_VALUE_YES), new c("ro.product.device", MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE), new c("ro.product.model", ServerProtocol.DIALOG_PARAM_SDK_VERSION), new c("ro.product.name", ServerProtocol.DIALOG_PARAM_SDK_VERSION), new c("ro.serialno", null)};
    private static a p;

    /* renamed from: a  reason: collision with root package name */
    public boolean f1293a = false;

    /* renamed from: b  reason: collision with root package name */
    public boolean f1294b = false;
    private final Context m;
    private boolean n = true;
    private List<String> o = new ArrayList();

    /* renamed from: com.a.a.a.a$a  reason: collision with other inner class name */
    /* compiled from: EmulatorDetector */
    public interface C0108a {
        void a(boolean z);
    }

    public static a a(Context context) {
        if (p == null) {
            p = new a(context.getApplicationContext());
        }
        return p;
    }

    private a(Context context) {
        this.m = context;
        this.o.add("com.google.android.launcher.layouts.genymotion");
        this.o.add("com.bluestacks");
        this.o.add("com.bignox.app");
    }

    private boolean a() {
        if (this.n && !this.o.isEmpty()) {
            PackageManager packageManager = this.m.getPackageManager();
            for (String launchIntentForPackage : this.o) {
                Intent launchIntentForPackage2 = packageManager.getLaunchIntentForPackage(launchIntentForPackage);
                if (launchIntentForPackage2 != null && !packageManager.queryIntentActivities(launchIntentForPackage2, 65536).isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean b() {
        String line1Number = ((TelephonyManager) this.m.getSystemService(PlaceFields.PHONE)).getLine1Number();
        for (String equalsIgnoreCase : c) {
            if (equalsIgnoreCase.equalsIgnoreCase(line1Number)) {
                a(" check phone number is detected");
                return true;
            }
        }
        return false;
    }

    private boolean c() {
        String deviceId = ((TelephonyManager) this.m.getSystemService(PlaceFields.PHONE)).getDeviceId();
        for (String equalsIgnoreCase : d) {
            if (equalsIgnoreCase.equalsIgnoreCase(deviceId)) {
                a("Check device id is detected");
                return true;
            }
        }
        return false;
    }

    private boolean d() {
        String subscriberId = ((TelephonyManager) this.m.getSystemService(PlaceFields.PHONE)).getSubscriberId();
        for (String equalsIgnoreCase : e) {
            if (equalsIgnoreCase.equalsIgnoreCase(subscriberId)) {
                a("Check imsi is detected");
                return true;
            }
        }
        return false;
    }

    private boolean e() {
        File[] fileArr = {new File("/proc/tty/drivers"), new File("/proc/cpuinfo")};
        for (int i2 = 0; i2 < 2; i2++) {
            File file = fileArr[i2];
            if (file.exists() && file.canRead()) {
                byte[] bArr = new byte[1024];
                try {
                    FileInputStream fileInputStream = new FileInputStream(file);
                    fileInputStream.read(bArr);
                    fileInputStream.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                String str = new String(bArr);
                for (String contains : g) {
                    if (str.contains(contains)) {
                        a("Check QEmuDrivers is detected");
                        return true;
                    }
                }
                continue;
            }
        }
        return false;
    }

    private boolean a(String[] strArr, String str) {
        for (String file : strArr) {
            if (new File(file).exists()) {
                a("Check " + str + " is detected");
                return true;
            }
        }
        return false;
    }

    private boolean f() {
        int i2 = 0;
        for (c cVar : l) {
            String a2 = a(this.m, cVar.f1297a);
            if (cVar.f1298b == null && a2 != null) {
                i2++;
            }
            if (cVar.f1298b != null && a2.contains(cVar.f1298b)) {
                i2++;
            }
        }
        if (i2 < 5) {
            return false;
        }
        a("Check QEmuProps is detected");
        return true;
    }

    private boolean g() {
        if (ContextCompat.checkSelfPermission(this.m, "android.permission.INTERNET") != 0) {
            return false;
        }
        String[] strArr = {"/system/bin/netcfg"};
        StringBuilder sb = new StringBuilder();
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(strArr);
            processBuilder.directory(new File("/system/bin/"));
            processBuilder.redirectErrorStream(true);
            InputStream inputStream = processBuilder.start().getInputStream();
            byte[] bArr = new byte[1024];
            while (inputStream.read(bArr) != -1) {
                sb.append(new String(bArr));
            }
            inputStream.close();
        } catch (Exception unused) {
        }
        String sb2 = sb.toString();
        a("netcfg data -> " + sb2);
        if (TextUtils.isEmpty(sb2)) {
            return false;
        }
        for (String str : sb2.split("\n")) {
            if ((str.contains("wlan0") || str.contains("tunl0") || str.contains("eth0")) && str.contains("10.0.2.15")) {
                a("Check IP is detected");
                return true;
            }
        }
        return false;
    }

    private static String a(Context context, String str) {
        try {
            Class<?> loadClass = context.getClassLoader().loadClass("android.os.SystemProperties");
            return (String) loadClass.getMethod("get", String.class).invoke(loadClass, str);
        } catch (Exception unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (this.f1293a) {
            getClass().getName();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01a8, code lost:
        if (r0 != false) goto L_0x01aa;
     */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x020d  */
    /* JADX WARNING: Removed duplicated region for block: B:91:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ boolean a(com.a.a.a.a r6) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Build.PRODUCT: "
            r0.append(r1)
            java.lang.String r1 = android.os.Build.PRODUCT
            r0.append(r1)
            java.lang.String r1 = "\nBuild.MANUFACTURER: "
            r0.append(r1)
            java.lang.String r1 = android.os.Build.MANUFACTURER
            r0.append(r1)
            java.lang.String r1 = "\nBuild.BRAND: "
            r0.append(r1)
            java.lang.String r1 = android.os.Build.BRAND
            r0.append(r1)
            java.lang.String r1 = "\nBuild.DEVICE: "
            r0.append(r1)
            java.lang.String r1 = android.os.Build.DEVICE
            r0.append(r1)
            java.lang.String r1 = "\nBuild.MODEL: "
            r0.append(r1)
            java.lang.String r1 = android.os.Build.MODEL
            r0.append(r1)
            java.lang.String r1 = "\nBuild.HARDWARE: "
            r0.append(r1)
            java.lang.String r1 = android.os.Build.HARDWARE
            r0.append(r1)
            java.lang.String r1 = "\nBuild.FINGERPRINT: "
            r0.append(r1)
            java.lang.String r1 = android.os.Build.FINGERPRINT
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r6.a(r0)
            java.lang.String r0 = android.os.Build.FINGERPRINT
            java.lang.String r1 = "generic"
            boolean r0 = r0.startsWith(r1)
            java.lang.String r2 = "google_sdk"
            r3 = 1
            r4 = 0
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.MODEL
            boolean r0 = r0.contains(r2)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.MODEL
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r5 = "droid4x"
            boolean r0 = r0.contains(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.MODEL
            java.lang.String r5 = "Emulator"
            boolean r0 = r0.contains(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.MODEL
            java.lang.String r5 = "Android SDK built for x86"
            boolean r0 = r0.contains(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.MANUFACTURER
            java.lang.String r5 = "Genymotion"
            boolean r0 = r0.contains(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.HARDWARE
            java.lang.String r5 = "goldfish"
            boolean r0 = r0.equals(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.HARDWARE
            java.lang.String r5 = "vbox86"
            boolean r0 = r0.equals(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.PRODUCT
            java.lang.String r5 = "sdk"
            boolean r0 = r0.equals(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.PRODUCT
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.PRODUCT
            java.lang.String r5 = "sdk_x86"
            boolean r0 = r0.equals(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.PRODUCT
            java.lang.String r5 = "vbox86p"
            boolean r0 = r0.equals(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.BOARD
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r5 = "nox"
            boolean r0 = r0.contains(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.BOOTLOADER
            java.lang.String r0 = r0.toLowerCase()
            boolean r0 = r0.contains(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.HARDWARE
            java.lang.String r0 = r0.toLowerCase()
            boolean r0 = r0.contains(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.PRODUCT
            java.lang.String r0 = r0.toLowerCase()
            boolean r0 = r0.contains(r5)
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = android.os.Build.SERIAL
            java.lang.String r0 = r0.toLowerCase()
            boolean r0 = r0.contains(r5)
            if (r0 == 0) goto L_0x010d
            goto L_0x010f
        L_0x010d:
            r0 = 0
            goto L_0x0110
        L_0x010f:
            r0 = 1
        L_0x0110:
            if (r0 == 0) goto L_0x0114
        L_0x0112:
            r0 = 1
            goto L_0x0132
        L_0x0114:
            java.lang.String r0 = android.os.Build.BRAND
            boolean r0 = r0.startsWith(r1)
            if (r0 == 0) goto L_0x0126
            java.lang.String r0 = android.os.Build.DEVICE
            boolean r0 = r0.startsWith(r1)
            if (r0 == 0) goto L_0x0126
            r0 = 1
            goto L_0x0127
        L_0x0126:
            r0 = 0
        L_0x0127:
            r0 = r0 | r4
            if (r0 == 0) goto L_0x012b
            goto L_0x0112
        L_0x012b:
            java.lang.String r0 = android.os.Build.PRODUCT
            boolean r0 = r2.equals(r0)
            r0 = r0 | r4
        L_0x0132:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Check basic "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            r6.a(r1)
            if (r0 != 0) goto L_0x020b
            android.content.Context r0 = r6.m
            java.lang.String r1 = "android.permission.READ_PHONE_STATE"
            int r0 = android.support.v4.content.ContextCompat.checkSelfPermission(r0, r1)
            if (r0 != 0) goto L_0x01ac
            boolean r0 = r6.f1294b
            if (r0 == 0) goto L_0x01ac
            android.content.Context r0 = r6.m
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            java.lang.String r1 = "android.hardware.telephony"
            boolean r0 = r0.hasSystemFeature(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Supported TelePhony: "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            r6.a(r1)
            if (r0 == 0) goto L_0x01ac
            boolean r0 = r6.b()
            if (r0 != 0) goto L_0x01aa
            boolean r0 = r6.c()
            if (r0 != 0) goto L_0x01aa
            boolean r0 = r6.d()
            if (r0 != 0) goto L_0x01aa
            android.content.Context r0 = r6.m
            java.lang.String r1 = "phone"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            java.lang.String r0 = r0.getNetworkOperatorName()
            java.lang.String r1 = "android"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x01a7
            java.lang.String r0 = "Check operator name android is detected"
            r6.a(r0)
            r0 = 1
            goto L_0x01a8
        L_0x01a7:
            r0 = 0
        L_0x01a8:
            if (r0 == 0) goto L_0x01ac
        L_0x01aa:
            r0 = 1
            goto L_0x01ad
        L_0x01ac:
            r0 = 0
        L_0x01ad:
            if (r0 != 0) goto L_0x01f6
            java.lang.String[] r0 = com.a.a.a.a.f
            java.lang.String r1 = "Geny"
            boolean r0 = r6.a(r0, r1)
            if (r0 != 0) goto L_0x01f6
            java.lang.String[] r0 = com.a.a.a.a.j
            java.lang.String r1 = "Andy"
            boolean r0 = r6.a(r0, r1)
            if (r0 != 0) goto L_0x01f6
            java.lang.String[] r0 = com.a.a.a.a.k
            java.lang.String r1 = "Nox"
            boolean r0 = r6.a(r0, r1)
            if (r0 != 0) goto L_0x01f6
            boolean r0 = r6.e()
            if (r0 != 0) goto L_0x01f6
            java.lang.String[] r0 = com.a.a.a.a.h
            java.lang.String r1 = "Pipes"
            boolean r0 = r6.a(r0, r1)
            if (r0 != 0) goto L_0x01f6
            boolean r0 = r6.g()
            if (r0 != 0) goto L_0x01f6
            boolean r0 = r6.f()
            if (r0 == 0) goto L_0x01f4
            java.lang.String[] r0 = com.a.a.a.a.i
            java.lang.String r1 = "X86"
            boolean r0 = r6.a(r0, r1)
            if (r0 == 0) goto L_0x01f4
            goto L_0x01f6
        L_0x01f4:
            r0 = 0
            goto L_0x01f7
        L_0x01f6:
            r0 = 1
        L_0x01f7:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Check Advanced "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            r6.a(r1)
        L_0x020b:
            if (r0 != 0) goto L_0x0225
            boolean r0 = r6.a()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Check Package Name "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            r6.a(r1)
        L_0x0225:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.a.a.a(com.a.a.a.a):boolean");
    }
}
