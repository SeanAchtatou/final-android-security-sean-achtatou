package com.journeyapps.barcodescanner;

import android.graphics.SurfaceTexture;
import android.view.TextureView;

/* compiled from: CameraPreview */
class d implements TextureView.SurfaceTextureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CameraPreview f4437a;

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    d(CameraPreview cameraPreview) {
        this.f4437a = cameraPreview;
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        onSurfaceTextureSizeChanged(surfaceTexture, i, i2);
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        ad unused = this.f4437a.q = new ad(i, i2);
        this.f4437a.h();
    }
}
