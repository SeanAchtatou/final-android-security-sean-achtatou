package com.journeyapps.barcodescanner;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import com.google.zxing.d;
import java.util.HashMap;

public class BarcodeView extends CameraPreview {
    /* access modifiers changed from: package-private */

    /* renamed from: a  reason: collision with root package name */
    public a f4376a = a.NONE;
    /* access modifiers changed from: package-private */

    /* renamed from: b  reason: collision with root package name */
    public a f4377b = null;
    private u e;
    private s f;
    private Handler g;
    private final Handler.Callback h = new c(this);

    enum a {
        NONE,
        SINGLE,
        CONTINUOUS
    }

    public BarcodeView(Context context) {
        super(context);
        h();
    }

    public BarcodeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        h();
    }

    public BarcodeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        h();
    }

    private void h() {
        this.f = new x();
        this.g = new Handler(this.h);
    }

    public void setDecoderFactory(s sVar) {
        af.a();
        this.f = sVar;
        u uVar = this.e;
        if (uVar != null) {
            uVar.f4456a = i();
        }
    }

    private r i() {
        if (this.f == null) {
            this.f = new x();
        }
        t tVar = new t();
        HashMap hashMap = new HashMap();
        hashMap.put(d.NEED_RESULT_POINT_CALLBACK, tVar);
        r a2 = this.f.a(hashMap);
        tVar.f4455a = a2;
        return a2;
    }

    public s getDecoderFactory() {
        return this.f;
    }

    public final void a(a aVar) {
        this.f4376a = a.SINGLE;
        this.f4377b = aVar;
        j();
    }

    private void j() {
        b();
        if (this.f4376a != a.NONE && this.d) {
            this.e = new u(getCameraInstance(), i(), this.g);
            this.e.f4457b = getPreviewFramingRect();
            this.e.a();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        j();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        u uVar = this.e;
        if (uVar != null) {
            uVar.b();
            this.e = null;
        }
    }

    public final void c() {
        b();
        super.c();
    }
}
