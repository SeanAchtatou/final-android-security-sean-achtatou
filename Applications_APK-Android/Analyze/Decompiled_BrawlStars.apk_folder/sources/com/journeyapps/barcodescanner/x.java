package com.journeyapps.barcodescanner;

import com.google.zxing.a;
import com.google.zxing.d;
import com.google.zxing.i;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;

/* compiled from: DefaultDecoderFactory */
public class x implements s {

    /* renamed from: a  reason: collision with root package name */
    private Collection<a> f4460a;

    /* renamed from: b  reason: collision with root package name */
    private Map<d, ?> f4461b;
    private String c;
    private int d;

    public x() {
    }

    public x(Collection<a> collection) {
        this.f4460a = collection;
    }

    public x(Collection<a> collection, Map<d, ?> map, String str, int i) {
        this.f4460a = collection;
        this.f4461b = map;
        this.c = str;
        this.d = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.google.zxing.d, java.util.Collection<com.google.zxing.a>]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.google.zxing.d, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    public final r a(Map<d, ?> map) {
        EnumMap enumMap = new EnumMap(d.class);
        enumMap.putAll(map);
        Map<d, ?> map2 = this.f4461b;
        if (map2 != null) {
            enumMap.putAll(map2);
        }
        if (this.f4460a != null) {
            enumMap.put((Object) d.POSSIBLE_FORMATS, (Object) this.f4460a);
        }
        if (this.c != null) {
            enumMap.put((Object) d.CHARACTER_SET, (Object) this.c);
        }
        i iVar = new i();
        iVar.a(enumMap);
        int i = this.d;
        if (i == 0) {
            return new r(iVar);
        }
        if (i == 1) {
            return new y(iVar);
        }
        if (i != 2) {
            return new r(iVar);
        }
        return new z(iVar);
    }
}
