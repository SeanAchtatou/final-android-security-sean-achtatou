package com.journeyapps.barcodescanner;

import com.journeyapps.barcodescanner.CameraPreview;

/* compiled from: CameraPreview */
class i implements CameraPreview.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CameraPreview f4442a;

    i(CameraPreview cameraPreview) {
        this.f4442a = cameraPreview;
    }

    public final void a() {
        for (CameraPreview.a a2 : this.f4442a.k) {
            a2.a();
        }
    }

    public final void b() {
        for (CameraPreview.a b2 : this.f4442a.k) {
            b2.b();
        }
    }

    public final void c() {
        for (CameraPreview.a c : this.f4442a.k) {
            c.c();
        }
    }

    public final void a(Exception exc) {
        for (CameraPreview.a a2 : this.f4442a.k) {
            a2.a(exc);
        }
    }

    public final void d() {
        for (CameraPreview.a d : this.f4442a.k) {
            d.d();
        }
    }
}
