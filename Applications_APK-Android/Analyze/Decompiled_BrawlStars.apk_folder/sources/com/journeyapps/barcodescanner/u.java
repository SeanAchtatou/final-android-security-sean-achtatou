package com.journeyapps.barcodescanner;

import android.graphics.Rect;
import android.os.Handler;
import android.os.HandlerThread;
import com.journeyapps.barcodescanner.a.e;

/* compiled from: DecoderThread */
public class u {
    private static final String c = u.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    r f4456a;

    /* renamed from: b  reason: collision with root package name */
    Rect f4457b;
    private e d;
    private HandlerThread e;
    /* access modifiers changed from: private */
    public Handler f;
    private Handler g;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public final Object i = new Object();
    private final Handler.Callback j = new v(this);
    private final com.journeyapps.barcodescanner.a.u k = new w(this);

    public u(e eVar, r rVar, Handler handler) {
        af.a();
        this.d = eVar;
        this.f4456a = rVar;
        this.g = handler;
    }

    public final void a() {
        af.a();
        this.e = new HandlerThread(c);
        this.e.start();
        this.f = new Handler(this.e.getLooper(), this.j);
        this.h = true;
        c();
    }

    public final void b() {
        af.a();
        synchronized (this.i) {
            this.h = false;
            this.f.removeCallbacksAndMessages(null);
            this.e.quit();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.d.a(this.k);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x009e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(com.journeyapps.barcodescanner.u r20, com.journeyapps.barcodescanner.ae r21) {
        /*
            r0 = r20
            r1 = r21
            long r2 = java.lang.System.currentTimeMillis()
            android.graphics.Rect r4 = r0.f4457b
            r1.f = r4
            r5 = 0
            if (r4 != 0) goto L_0x0012
            r4 = r5
            goto L_0x00c0
        L_0x0012:
            int r4 = r1.e
            byte[] r6 = r1.f4431a
            int r7 = r1.f4432b
            int r8 = r1.c
            if (r4 == 0) goto L_0x0074
            r9 = 90
            r10 = 0
            if (r4 == r9) goto L_0x0058
            r9 = 180(0xb4, float:2.52E-43)
            if (r4 == r9) goto L_0x0047
            r9 = 270(0x10e, float:3.78E-43)
            if (r4 == r9) goto L_0x002a
            goto L_0x0074
        L_0x002a:
            int r4 = r7 * r8
            byte[] r9 = new byte[r4]
            int r4 = r4 + -1
        L_0x0030:
            if (r10 >= r7) goto L_0x0045
            int r11 = r8 + -1
        L_0x0034:
            if (r11 < 0) goto L_0x0042
            int r12 = r11 * r7
            int r12 = r12 + r10
            byte r12 = r6[r12]
            r9[r4] = r12
            int r4 = r4 + -1
            int r11 = r11 + -1
            goto L_0x0034
        L_0x0042:
            int r10 = r10 + 1
            goto L_0x0030
        L_0x0045:
            r12 = r9
            goto L_0x0075
        L_0x0047:
            int r7 = r7 * r8
            byte[] r4 = new byte[r7]
            int r8 = r7 + -1
        L_0x004d:
            if (r10 >= r7) goto L_0x0072
            byte r9 = r6[r10]
            r4[r8] = r9
            int r8 = r8 + -1
            int r10 = r10 + 1
            goto L_0x004d
        L_0x0058:
            int r4 = r7 * r8
            byte[] r4 = new byte[r4]
            r9 = 0
        L_0x005d:
            if (r10 >= r7) goto L_0x0072
            int r11 = r8 + -1
        L_0x0061:
            if (r11 < 0) goto L_0x006f
            int r12 = r11 * r7
            int r12 = r12 + r10
            byte r12 = r6[r12]
            r4[r9] = r12
            int r9 = r9 + 1
            int r11 = r11 + -1
            goto L_0x0061
        L_0x006f:
            int r10 = r10 + 1
            goto L_0x005d
        L_0x0072:
            r12 = r4
            goto L_0x0075
        L_0x0074:
            r12 = r6
        L_0x0075:
            boolean r4 = r21.a()
            if (r4 == 0) goto L_0x009e
            com.google.zxing.l r4 = new com.google.zxing.l
            int r13 = r1.c
            int r14 = r1.f4432b
            android.graphics.Rect r6 = r1.f
            int r15 = r6.left
            android.graphics.Rect r6 = r1.f
            int r6 = r6.top
            android.graphics.Rect r7 = r1.f
            int r17 = r7.width()
            android.graphics.Rect r7 = r1.f
            int r18 = r7.height()
            r19 = 0
            r11 = r4
            r16 = r6
            r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)
            goto L_0x00c0
        L_0x009e:
            com.google.zxing.l r4 = new com.google.zxing.l
            int r13 = r1.f4432b
            int r14 = r1.c
            android.graphics.Rect r6 = r1.f
            int r15 = r6.left
            android.graphics.Rect r6 = r1.f
            int r6 = r6.top
            android.graphics.Rect r7 = r1.f
            int r17 = r7.width()
            android.graphics.Rect r7 = r1.f
            int r18 = r7.height()
            r19 = 0
            r11 = r4
            r16 = r6
            r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)
        L_0x00c0:
            if (r4 == 0) goto L_0x00c8
            com.journeyapps.barcodescanner.r r5 = r0.f4456a
            com.google.zxing.n r5 = r5.a(r4)
        L_0x00c8:
            if (r5 == 0) goto L_0x0101
            long r6 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r8 = "Found barcode in "
            r4.append(r8)
            long r6 = r6 - r2
            r4.append(r6)
            java.lang.String r2 = " ms"
            r4.append(r2)
            r4.toString()
            android.os.Handler r2 = r0.g
            if (r2 == 0) goto L_0x010e
            com.journeyapps.barcodescanner.b r2 = new com.journeyapps.barcodescanner.b
            r2.<init>(r5, r1)
            android.os.Handler r1 = r0.g
            int r3 = com.google.zxing.client.android.R.id.zxing_decode_succeeded
            android.os.Message r1 = android.os.Message.obtain(r1, r3, r2)
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            r1.setData(r2)
            r1.sendToTarget()
            goto L_0x010e
        L_0x0101:
            android.os.Handler r1 = r0.g
            if (r1 == 0) goto L_0x010e
            int r2 = com.google.zxing.client.android.R.id.zxing_decode_failed
            android.os.Message r1 = android.os.Message.obtain(r1, r2)
            r1.sendToTarget()
        L_0x010e:
            android.os.Handler r1 = r0.g
            if (r1 == 0) goto L_0x0126
            com.journeyapps.barcodescanner.r r1 = r0.f4456a
            java.util.ArrayList r2 = new java.util.ArrayList
            java.util.List<com.google.zxing.p> r1 = r1.f4453a
            r2.<init>(r1)
            android.os.Handler r1 = r0.g
            int r3 = com.google.zxing.client.android.R.id.zxing_possible_result_points
            android.os.Message r1 = android.os.Message.obtain(r1, r3, r2)
            r1.sendToTarget()
        L_0x0126:
            r20.c()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.journeyapps.barcodescanner.u.a(com.journeyapps.barcodescanner.u, com.journeyapps.barcodescanner.ae):void");
    }
}
