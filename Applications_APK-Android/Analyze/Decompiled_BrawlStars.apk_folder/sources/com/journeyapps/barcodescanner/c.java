package com.journeyapps.barcodescanner;

import android.os.Handler;
import android.os.Message;
import com.google.zxing.client.android.R;
import com.journeyapps.barcodescanner.BarcodeView;
import java.util.List;

/* compiled from: BarcodeView */
class c implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BarcodeView f4436a;

    c(BarcodeView barcodeView) {
        this.f4436a = barcodeView;
    }

    public boolean handleMessage(Message message) {
        if (message.what == R.id.zxing_decode_succeeded) {
            b bVar = (b) message.obj;
            if (!(bVar == null || this.f4436a.f4377b == null || this.f4436a.f4376a == BarcodeView.a.NONE)) {
                this.f4436a.f4377b.barcodeResult(bVar);
                if (this.f4436a.f4376a == BarcodeView.a.SINGLE) {
                    BarcodeView barcodeView = this.f4436a;
                    barcodeView.f4376a = BarcodeView.a.NONE;
                    barcodeView.f4377b = null;
                    barcodeView.b();
                }
            }
            return true;
        } else if (message.what == R.id.zxing_decode_failed) {
            return true;
        } else {
            if (message.what != R.id.zxing_possible_result_points) {
                return false;
            }
            List list = (List) message.obj;
            if (!(this.f4436a.f4377b == null || this.f4436a.f4376a == BarcodeView.a.NONE)) {
                this.f4436a.f4377b.possibleResultPoints(list);
            }
            return true;
        }
    }
}
