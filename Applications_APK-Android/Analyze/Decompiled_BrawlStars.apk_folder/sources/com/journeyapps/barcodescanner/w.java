package com.journeyapps.barcodescanner;

import com.google.zxing.client.android.R;
import com.journeyapps.barcodescanner.a.u;

/* compiled from: DecoderThread */
class w implements u {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f4459a;

    w(u uVar) {
        this.f4459a = uVar;
    }

    public final void a(ae aeVar) {
        synchronized (this.f4459a.i) {
            if (this.f4459a.h) {
                this.f4459a.f.obtainMessage(R.id.zxing_decode, aeVar).sendToTarget();
            }
        }
    }

    public final void a() {
        synchronized (this.f4459a.i) {
            if (this.f4459a.h) {
                this.f4459a.f.obtainMessage(R.id.zxing_preview_failed).sendToTarget();
            }
        }
    }
}
