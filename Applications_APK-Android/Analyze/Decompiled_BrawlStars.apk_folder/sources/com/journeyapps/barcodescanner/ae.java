package com.journeyapps.barcodescanner;

import android.graphics.Rect;

/* compiled from: SourceData */
public class ae {

    /* renamed from: a  reason: collision with root package name */
    byte[] f4431a;

    /* renamed from: b  reason: collision with root package name */
    int f4432b;
    int c;
    int d;
    int e;
    Rect f;

    public ae(byte[] bArr, int i, int i2, int i3, int i4) {
        this.f4431a = bArr;
        this.f4432b = i;
        this.c = i2;
        this.e = i4;
        this.d = i3;
        if (i * i2 > bArr.length) {
            throw new IllegalArgumentException("Image data does not match the resolution. " + i + "x" + i2 + " > " + bArr.length);
        }
    }

    public final boolean a() {
        return this.e % 180 != 0;
    }
}
