package com.journeyapps.barcodescanner;

/* compiled from: Size */
public class ad implements Comparable<ad> {

    /* renamed from: a  reason: collision with root package name */
    public final int f4429a;

    /* renamed from: b  reason: collision with root package name */
    public final int f4430b;

    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        ad adVar = (ad) obj;
        int i = this.f4430b * this.f4429a;
        int i2 = adVar.f4430b * adVar.f4429a;
        if (i2 < i) {
            return 1;
        }
        return i2 > i ? -1 : 0;
    }

    public ad(int i, int i2) {
        this.f4429a = i;
        this.f4430b = i2;
    }

    public final ad a() {
        return new ad(this.f4430b, this.f4429a);
    }

    public final ad a(ad adVar) {
        int i = this.f4429a;
        int i2 = adVar.f4430b;
        int i3 = i * i2;
        int i4 = adVar.f4429a;
        int i5 = this.f4430b;
        if (i3 >= i4 * i5) {
            return new ad(i4, (i5 * i4) / i);
        }
        return new ad((i * i2) / i5, i2);
    }

    public final ad b(ad adVar) {
        int i = this.f4429a;
        int i2 = adVar.f4430b;
        int i3 = i * i2;
        int i4 = adVar.f4429a;
        int i5 = this.f4430b;
        if (i3 <= i4 * i5) {
            return new ad(i4, (i5 * i4) / i);
        }
        return new ad((i * i2) / i5, i2);
    }

    public String toString() {
        return this.f4429a + "x" + this.f4430b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ad adVar = (ad) obj;
        if (this.f4429a == adVar.f4429a && this.f4430b == adVar.f4430b) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.f4429a * 31) + this.f4430b;
    }
}
