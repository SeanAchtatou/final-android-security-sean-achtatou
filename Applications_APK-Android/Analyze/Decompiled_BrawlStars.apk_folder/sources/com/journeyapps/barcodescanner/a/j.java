package com.journeyapps.barcodescanner.a;

import com.google.zxing.client.android.R;

/* compiled from: CameraInstance */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f4403a;

    j(e eVar) {
        this.f4403a = eVar;
    }

    public void run() {
        try {
            String unused = e.f;
            m a2 = this.f4403a.f4396b;
            if (a2.f4406a != null) {
                a2.b();
                if (this.f4403a.c != null) {
                    this.f4403a.c.obtainMessage(R.id.zxing_prewiew_size_ready, e.e(this.f4403a)).sendToTarget();
                    return;
                }
                return;
            }
            throw new RuntimeException("Camera not open");
        } catch (Exception e) {
            e.a(this.f4403a, e);
            String unused2 = e.f;
        }
    }
}
