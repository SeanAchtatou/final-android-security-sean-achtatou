package com.journeyapps.barcodescanner.a;

import android.hardware.SensorManager;
import com.google.zxing.client.android.R;
import com.google.zxing.client.android.a;

/* compiled from: CameraInstance */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f4405a;

    l(e eVar) {
        this.f4405a = eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.journeyapps.barcodescanner.a.e.a(com.journeyapps.barcodescanner.a.e, boolean):boolean
     arg types: [com.journeyapps.barcodescanner.a.e, int]
     candidates:
      com.journeyapps.barcodescanner.a.e.a(com.journeyapps.barcodescanner.a.e, java.lang.Exception):void
      com.journeyapps.barcodescanner.a.e.a(com.journeyapps.barcodescanner.a.e, boolean):boolean */
    public void run() {
        try {
            String unused = e.f;
            m a2 = this.f4405a.f4396b;
            if (a2.c != null) {
                a2.c.b();
                a2.c = null;
            }
            if (a2.d != null) {
                a aVar = a2.d;
                if (aVar.f2946b != null) {
                    ((SensorManager) aVar.c.getSystemService("sensor")).unregisterListener(aVar);
                    aVar.f2946b = null;
                }
                a2.d = null;
            }
            if (a2.f4406a != null && a2.e) {
                a2.f4406a.stopPreview();
                a2.k.f4408a = null;
                a2.e = false;
            }
            m a3 = this.f4405a.f4396b;
            if (a3.f4406a != null) {
                a3.f4406a.release();
                a3.f4406a = null;
            }
        } catch (Exception unused2) {
            String unused3 = e.f;
        }
        boolean unused4 = this.f4405a.e = true;
        this.f4405a.c.sendEmptyMessage(R.id.zxing_camera_closed);
        p c = this.f4405a.g;
        synchronized (c.f4417b) {
            c.f4416a--;
            if (c.f4416a == 0) {
                c.b();
            }
        }
    }
}
