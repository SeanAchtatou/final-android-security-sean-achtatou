package com.journeyapps.barcodescanner.a;

import android.graphics.Rect;
import com.journeyapps.barcodescanner.ad;

/* compiled from: FitXYStrategy */
public class t extends v {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4422a = t.class.getSimpleName();

    private static float a(float f) {
        return f < 1.0f ? 1.0f / f : f;
    }

    /* access modifiers changed from: protected */
    public final float a(ad adVar, ad adVar2) {
        if (adVar.f4429a <= 0 || adVar.f4430b <= 0) {
            return 0.0f;
        }
        float a2 = (1.0f / a((((float) adVar.f4429a) * 1.0f) / ((float) adVar2.f4429a))) / a((((float) adVar.f4430b) * 1.0f) / ((float) adVar2.f4430b));
        float a3 = a(((((float) adVar.f4429a) * 1.0f) / ((float) adVar.f4430b)) / ((((float) adVar2.f4429a) * 1.0f) / ((float) adVar2.f4430b)));
        return a2 * (((1.0f / a3) / a3) / a3);
    }

    public final Rect b(ad adVar, ad adVar2) {
        return new Rect(0, 0, adVar2.f4429a, adVar2.f4430b);
    }
}
