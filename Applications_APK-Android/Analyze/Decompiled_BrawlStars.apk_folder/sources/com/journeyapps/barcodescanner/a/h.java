package com.journeyapps.barcodescanner.a;

import android.hardware.Camera;

/* compiled from: CameraInstance */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f4401a;

    h(g gVar) {
        this.f4401a = gVar;
    }

    public void run() {
        m a2 = this.f4401a.f4400b.f4396b;
        u uVar = this.f4401a.f4399a;
        Camera camera = a2.f4406a;
        if (camera != null && a2.e) {
            a2.k.f4408a = uVar;
            camera.setOneShotPreviewCallback(a2.k);
        }
    }
}
