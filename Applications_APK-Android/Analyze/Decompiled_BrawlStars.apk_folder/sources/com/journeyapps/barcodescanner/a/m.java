package com.journeyapps.barcodescanner.a;

import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import com.journeyapps.barcodescanner.ad;
import com.journeyapps.barcodescanner.ae;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: CameraManager */
public final class m {
    /* access modifiers changed from: private */
    public static final String l = m.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    Camera f4406a;

    /* renamed from: b  reason: collision with root package name */
    Camera.CameraInfo f4407b;
    a c;
    com.google.zxing.client.android.a d;
    boolean e;
    n f = new n();
    public r g;
    ad h;
    int i = -1;
    Context j;
    final a k;
    private String m;
    private ad n;

    /* compiled from: CameraManager */
    final class a implements Camera.PreviewCallback {

        /* renamed from: a  reason: collision with root package name */
        u f4408a;

        /* renamed from: b  reason: collision with root package name */
        ad f4409b;

        public a() {
        }

        public final void onPreviewFrame(byte[] bArr, Camera camera) {
            ad adVar = this.f4409b;
            u uVar = this.f4408a;
            if (adVar == null || uVar == null) {
                String unused = m.l;
                if (uVar != null) {
                    new Exception("No resolution available");
                    uVar.a();
                }
            } else if (bArr != null) {
                try {
                    byte[] bArr2 = bArr;
                    uVar.a(new ae(bArr2, adVar.f4429a, adVar.f4430b, camera.getParameters().getPreviewFormat(), m.this.i));
                } catch (RuntimeException unused2) {
                    String unused3 = m.l;
                    uVar.a();
                }
            } else {
                throw new NullPointerException("No preview data received");
            }
        }
    }

    public m(Context context) {
        this.j = context;
        this.k = new a();
    }

    public final boolean a() {
        int i2 = this.i;
        if (i2 != -1) {
            return i2 % 180 != 0;
        }
        throw new IllegalStateException("Rotation not calculated yet. Call configure() first.");
    }

    private static List<ad> a(Camera.Parameters parameters) {
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        ArrayList arrayList = new ArrayList();
        if (supportedPreviewSizes == null) {
            Camera.Size previewSize = parameters.getPreviewSize();
            if (previewSize != null) {
                arrayList.add(new ad(previewSize.width, previewSize.height));
            }
            return arrayList;
        }
        for (Camera.Size next : supportedPreviewSizes) {
            arrayList.add(new ad(next.width, next.height));
        }
        return arrayList;
    }

    private int d() {
        int i2;
        int i3 = this.g.f4420b;
        int i4 = 0;
        if (i3 != 0) {
            if (i3 == 1) {
                i4 = 90;
            } else if (i3 == 2) {
                i4 = 180;
            } else if (i3 == 3) {
                i4 = 270;
            }
        }
        if (this.f4407b.facing == 1) {
            i2 = (360 - ((this.f4407b.orientation + i4) % 360)) % 360;
        } else {
            i2 = ((this.f4407b.orientation - i4) + 360) % 360;
        }
        "Camera Display Orientation: " + i2;
        return i2;
    }

    private void a(int i2) {
        this.f4406a.setDisplayOrientation(i2);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        try {
            this.i = d();
            a(this.i);
        } catch (Exception unused) {
        }
        try {
            b(false);
        } catch (Exception unused2) {
            try {
                b(true);
            } catch (Exception unused3) {
            }
        }
        Camera.Size previewSize = this.f4406a.getParameters().getPreviewSize();
        if (previewSize == null) {
            this.h = this.n;
        } else {
            this.h = new ad(previewSize.width, previewSize.height);
        }
        this.k.f4409b = this.h;
    }

    public final void a(boolean z) {
        if (this.f4406a != null) {
            try {
                if (z != e()) {
                    if (this.c != null) {
                        this.c.b();
                    }
                    Camera.Parameters parameters = this.f4406a.getParameters();
                    com.google.zxing.client.android.a.a.a(parameters, z);
                    if (this.f.f) {
                        com.google.zxing.client.android.a.a.b(parameters, z);
                    }
                    this.f4406a.setParameters(parameters);
                    if (this.c != null) {
                        this.c.a();
                    }
                }
            } catch (RuntimeException unused) {
            }
        }
    }

    private boolean e() {
        String flashMode;
        Camera.Parameters parameters = this.f4406a.getParameters();
        if (parameters == null || (flashMode = parameters.getFlashMode()) == null) {
            return false;
        }
        if ("on".equals(flashMode) || "torch".equals(flashMode)) {
            return true;
        }
        return false;
    }

    private void b(boolean z) {
        Camera.Parameters parameters = this.f4406a.getParameters();
        String str = this.m;
        if (str == null) {
            this.m = parameters.flatten();
        } else {
            parameters.unflatten(str);
        }
        if (parameters != null) {
            "Initial camera parameters: " + parameters.flatten();
            com.google.zxing.client.android.a.a.a(parameters, this.f.h, z);
            if (!z) {
                com.google.zxing.client.android.a.a.a(parameters, false);
                if (this.f.f4411b) {
                    com.google.zxing.client.android.a.a.f(parameters);
                }
                if (this.f.c) {
                    com.google.zxing.client.android.a.a.e(parameters);
                }
                if (this.f.d && Build.VERSION.SDK_INT >= 15) {
                    com.google.zxing.client.android.a.a.d(parameters);
                    com.google.zxing.client.android.a.a.b(parameters);
                    com.google.zxing.client.android.a.a.c(parameters);
                }
            }
            List<ad> a2 = a(parameters);
            ad adVar = null;
            if (a2.size() == 0) {
                this.n = null;
            } else {
                r rVar = this.g;
                boolean a3 = a();
                if (rVar.f4419a != null) {
                    if (a3) {
                        adVar = rVar.f4419a.a();
                    } else {
                        adVar = rVar.f4419a;
                    }
                }
                v vVar = rVar.c;
                if (adVar != null) {
                    Collections.sort(a2, new w(vVar, adVar));
                }
                "Viewfinder size: " + adVar;
                "Preview in order of preference: " + a2;
                this.n = a2.get(0);
                parameters.setPreviewSize(this.n.f4429a, this.n.f4430b);
            }
            if (Build.DEVICE.equals("glass-1")) {
                com.google.zxing.client.android.a.a.a(parameters);
            }
            "Final camera parameters: " + parameters.flatten();
            this.f4406a.setParameters(parameters);
        }
    }
}
