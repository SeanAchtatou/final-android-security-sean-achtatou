package com.journeyapps.barcodescanner.a;

import android.os.Handler;
import android.os.HandlerThread;

/* compiled from: CameraThread */
class p {
    private static final String c = p.class.getSimpleName();
    private static p d;

    /* renamed from: a  reason: collision with root package name */
    int f4416a = 0;

    /* renamed from: b  reason: collision with root package name */
    final Object f4417b = new Object();
    private Handler e;
    private HandlerThread f;

    public static p a() {
        if (d == null) {
            d = new p();
        }
        return d;
    }

    private p() {
    }

    /* access modifiers changed from: protected */
    public final void a(Runnable runnable) {
        synchronized (this.f4417b) {
            c();
            this.e.post(runnable);
        }
    }

    private void c() {
        synchronized (this.f4417b) {
            if (this.e == null) {
                if (this.f4416a > 0) {
                    this.f = new HandlerThread("CameraThread");
                    this.f.start();
                    this.e = new Handler(this.f.getLooper());
                } else {
                    throw new IllegalStateException("CameraThread is not open");
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        synchronized (this.f4417b) {
            this.f.quit();
            this.f = null;
            this.e = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void b(Runnable runnable) {
        synchronized (this.f4417b) {
            this.f4416a++;
            a(runnable);
        }
    }
}
