package com.journeyapps.barcodescanner.a;

import android.graphics.Rect;
import com.journeyapps.barcodescanner.ad;

/* compiled from: CenterCropStrategy */
public class q extends v {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4418a = q.class.getSimpleName();

    /* access modifiers changed from: protected */
    public final float a(ad adVar, ad adVar2) {
        if (adVar.f4429a <= 0 || adVar.f4430b <= 0) {
            return 0.0f;
        }
        ad b2 = adVar.b(adVar2);
        float f = (((float) b2.f4429a) * 1.0f) / ((float) adVar.f4429a);
        if (f > 1.0f) {
            f = (float) Math.pow((double) (1.0f / f), 1.1d);
        }
        float f2 = ((((float) b2.f4429a) * 1.0f) / ((float) adVar2.f4429a)) + ((((float) b2.f4430b) * 1.0f) / ((float) adVar2.f4430b));
        return f * ((1.0f / f2) / f2);
    }

    public final Rect b(ad adVar, ad adVar2) {
        ad b2 = adVar.b(adVar2);
        "Preview: " + adVar + "; Scaled: " + b2 + "; Want: " + adVar2;
        int i = (b2.f4429a - adVar2.f4429a) / 2;
        int i2 = (b2.f4430b - adVar2.f4430b) / 2;
        return new Rect(-i, -i2, b2.f4429a - i, b2.f4430b - i2);
    }
}
