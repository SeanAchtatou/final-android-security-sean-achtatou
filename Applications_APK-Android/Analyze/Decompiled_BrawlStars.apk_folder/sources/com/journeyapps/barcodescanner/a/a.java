package com.journeyapps.barcodescanner.a;

import android.hardware.Camera;
import android.os.Handler;
import java.util.ArrayList;
import java.util.Collection;

/* compiled from: AutoFocusManager */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4390a = a.class.getSimpleName();
    private static final Collection<String> h = new ArrayList(2);

    /* renamed from: b  reason: collision with root package name */
    private boolean f4391b;
    /* access modifiers changed from: private */
    public boolean c;
    private final boolean d;
    private final Camera e;
    /* access modifiers changed from: private */
    public Handler f = new Handler(this.i);
    /* access modifiers changed from: private */
    public int g = 1;
    private final Handler.Callback i = new b(this);
    private final Camera.AutoFocusCallback j = new c(this);

    static {
        h.add("auto");
        h.add("macro");
    }

    public a(Camera camera, n nVar) {
        boolean z = true;
        this.e = camera;
        String focusMode = camera.getParameters().getFocusMode();
        this.d = (!nVar.e || !h.contains(focusMode)) ? false : z;
        "Current focus mode '" + focusMode + "'; use auto focus? " + this.d;
        a();
    }

    /* access modifiers changed from: private */
    public synchronized void c() {
        if (!this.f4391b && !this.f.hasMessages(this.g)) {
            this.f.sendMessageDelayed(this.f.obtainMessage(this.g), 2000);
        }
    }

    public final void a() {
        this.f4391b = false;
        d();
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.d && !this.f4391b && !this.c) {
            try {
                this.e.autoFocus(this.j);
                this.c = true;
            } catch (RuntimeException unused) {
                c();
            }
        }
    }

    public final void b() {
        this.f4391b = true;
        this.c = false;
        this.f.removeMessages(this.g);
        if (this.d) {
            try {
                this.e.cancelAutoFocus();
            } catch (RuntimeException unused) {
            }
        }
    }
}
