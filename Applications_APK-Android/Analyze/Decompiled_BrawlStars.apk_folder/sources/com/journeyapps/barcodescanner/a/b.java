package com.journeyapps.barcodescanner.a;

import android.os.Handler;
import android.os.Message;

/* compiled from: AutoFocusManager */
class b implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f4392a;

    b(a aVar) {
        this.f4392a = aVar;
    }

    public boolean handleMessage(Message message) {
        if (message.what != this.f4392a.g) {
            return false;
        }
        this.f4392a.d();
        return true;
    }
}
