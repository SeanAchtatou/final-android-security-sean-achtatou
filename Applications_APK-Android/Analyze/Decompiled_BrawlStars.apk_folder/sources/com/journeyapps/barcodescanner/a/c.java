package com.journeyapps.barcodescanner.a;

import android.hardware.Camera;

/* compiled from: AutoFocusManager */
class c implements Camera.AutoFocusCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f4393a;

    c(a aVar) {
        this.f4393a = aVar;
    }

    public void onAutoFocus(boolean z, Camera camera) {
        this.f4393a.f.post(new d(this));
    }
}
