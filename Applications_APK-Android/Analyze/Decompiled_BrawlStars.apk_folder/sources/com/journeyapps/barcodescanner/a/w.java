package com.journeyapps.barcodescanner.a;

import com.journeyapps.barcodescanner.ad;
import java.util.Comparator;

/* compiled from: PreviewScalingStrategy */
class w implements Comparator<ad> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f4424a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ v f4425b;

    w(v vVar, ad adVar) {
        this.f4425b = vVar;
        this.f4424a = adVar;
    }

    public /* synthetic */ int compare(Object obj, Object obj2) {
        return Float.compare(this.f4425b.a((ad) obj2, this.f4424a), this.f4425b.a((ad) obj, this.f4424a));
    }
}
