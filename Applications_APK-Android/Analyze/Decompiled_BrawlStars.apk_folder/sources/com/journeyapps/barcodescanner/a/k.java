package com.journeyapps.barcodescanner.a;

import android.hardware.Camera;
import android.hardware.SensorManager;
import com.google.zxing.client.android.a;

/* compiled from: CameraInstance */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f4404a;

    k(e eVar) {
        this.f4404a = eVar;
    }

    public void run() {
        try {
            String unused = e.f;
            m a2 = this.f4404a.f4396b;
            o f = this.f4404a.f4395a;
            Camera camera = a2.f4406a;
            if (f.f4414a != null) {
                camera.setPreviewDisplay(f.f4414a);
            } else {
                camera.setPreviewTexture(f.f4415b);
            }
            m a3 = this.f4404a.f4396b;
            Camera camera2 = a3.f4406a;
            if (camera2 != null && !a3.e) {
                camera2.startPreview();
                a3.e = true;
                a3.c = new a(a3.f4406a, a3.f);
                a3.d = new a(a3.j, a3, a3.f);
                a aVar = a3.d;
                if (aVar.f2945a.g) {
                    SensorManager sensorManager = (SensorManager) aVar.c.getSystemService("sensor");
                    aVar.f2946b = sensorManager.getDefaultSensor(5);
                    if (aVar.f2946b != null) {
                        sensorManager.registerListener(aVar, aVar.f2946b, 3);
                    }
                }
            }
        } catch (Exception e) {
            e.a(this.f4404a, e);
            String unused2 = e.f;
        }
    }
}
