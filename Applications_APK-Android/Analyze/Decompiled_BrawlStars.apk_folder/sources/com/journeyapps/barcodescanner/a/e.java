package com.journeyapps.barcodescanner.a;

import android.content.Context;
import android.os.Handler;
import com.google.zxing.client.android.R;
import com.journeyapps.barcodescanner.ad;
import com.journeyapps.barcodescanner.af;

/* compiled from: CameraInstance */
public class e {
    /* access modifiers changed from: private */
    public static final String f = e.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    public o f4395a;

    /* renamed from: b  reason: collision with root package name */
    public m f4396b;
    public Handler c;
    public r d;
    public boolean e = true;
    /* access modifiers changed from: private */
    public p g;
    /* access modifiers changed from: private */
    public boolean h = false;
    private Handler i;
    private n j = new n();
    private Runnable k = new i(this);
    private Runnable l = new j(this);
    private Runnable m = new k(this);
    private Runnable n = new l(this);

    public e(Context context) {
        af.a();
        this.g = p.a();
        this.f4396b = new m(context);
        this.f4396b.f = this.j;
        this.i = new Handler();
    }

    public final void a(n nVar) {
        if (!this.h) {
            this.j = nVar;
            this.f4396b.f = nVar;
        }
    }

    public final void a() {
        af.a();
        this.h = true;
        this.e = false;
        this.g.b(this.k);
    }

    public final void b() {
        af.a();
        f();
        this.g.a(this.l);
    }

    public final void c() {
        af.a();
        f();
        this.g.a(this.m);
    }

    public final void a(boolean z) {
        af.a();
        if (this.h) {
            this.g.a(new f(this, z));
        }
    }

    public final void d() {
        af.a();
        if (this.h) {
            this.g.a(this.n);
        } else {
            this.e = true;
        }
        this.h = false;
    }

    public final void a(u uVar) {
        this.i.post(new g(this, uVar));
    }

    private void f() {
        if (!this.h) {
            throw new IllegalStateException("CameraInstance is not open");
        }
    }

    static /* synthetic */ void a(e eVar, Exception exc) {
        Handler handler = eVar.c;
        if (handler != null) {
            handler.obtainMessage(R.id.zxing_camera_error, exc).sendToTarget();
        }
    }

    static /* synthetic */ ad e(e eVar) {
        m mVar = eVar.f4396b;
        if (mVar.h == null) {
            return null;
        }
        if (mVar.a()) {
            return mVar.h.a();
        }
        return mVar.h;
    }
}
