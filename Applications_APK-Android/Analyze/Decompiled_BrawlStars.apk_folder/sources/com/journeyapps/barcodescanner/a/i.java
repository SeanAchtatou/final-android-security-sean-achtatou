package com.journeyapps.barcodescanner.a;

import android.hardware.Camera;
import com.google.zxing.client.android.a.a.a;

/* compiled from: CameraInstance */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f4402a;

    i(e eVar) {
        this.f4402a = eVar;
    }

    public void run() {
        try {
            String unused = e.f;
            m a2 = this.f4402a.f4396b;
            a2.f4406a = a.b(a2.f.f4410a);
            if (a2.f4406a != null) {
                int a3 = a.a(a2.f.f4410a);
                a2.f4407b = new Camera.CameraInfo();
                Camera.getCameraInfo(a3, a2.f4407b);
                return;
            }
            throw new RuntimeException("Failed to open camera");
        } catch (Exception e) {
            e.a(this.f4402a, e);
            String unused2 = e.f;
        }
    }
}
