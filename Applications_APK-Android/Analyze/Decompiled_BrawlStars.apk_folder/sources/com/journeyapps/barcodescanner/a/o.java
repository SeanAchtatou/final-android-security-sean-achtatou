package com.journeyapps.barcodescanner.a;

import android.graphics.SurfaceTexture;
import android.view.SurfaceHolder;

/* compiled from: CameraSurface */
public class o {

    /* renamed from: a  reason: collision with root package name */
    SurfaceHolder f4414a;

    /* renamed from: b  reason: collision with root package name */
    SurfaceTexture f4415b;

    public o(SurfaceHolder surfaceHolder) {
        if (surfaceHolder != null) {
            this.f4414a = surfaceHolder;
            return;
        }
        throw new IllegalArgumentException("surfaceHolder may not be null");
    }

    public o(SurfaceTexture surfaceTexture) {
        if (surfaceTexture != null) {
            this.f4415b = surfaceTexture;
            return;
        }
        throw new IllegalArgumentException("surfaceTexture may not be null");
    }
}
