package com.journeyapps.barcodescanner.a;

import android.graphics.Rect;
import com.journeyapps.barcodescanner.ad;

/* compiled from: PreviewScalingStrategy */
public abstract class v {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4423a = v.class.getSimpleName();

    /* access modifiers changed from: protected */
    public float a(ad adVar, ad adVar2) {
        return 0.5f;
    }

    public abstract Rect b(ad adVar, ad adVar2);
}
