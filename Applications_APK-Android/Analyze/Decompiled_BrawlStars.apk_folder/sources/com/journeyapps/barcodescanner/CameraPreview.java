package com.journeyapps.barcodescanner;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.google.zxing.client.android.R;
import com.journeyapps.barcodescanner.a.e;
import com.journeyapps.barcodescanner.a.n;
import com.journeyapps.barcodescanner.a.o;
import com.journeyapps.barcodescanner.a.q;
import com.journeyapps.barcodescanner.a.r;
import com.journeyapps.barcodescanner.a.s;
import com.journeyapps.barcodescanner.a.t;
import com.journeyapps.barcodescanner.a.v;
import java.util.ArrayList;
import java.util.List;

public class CameraPreview extends ViewGroup {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f4380a = CameraPreview.class.getSimpleName();
    /* access modifiers changed from: private */
    public final a A = new i(this);

    /* renamed from: b  reason: collision with root package name */
    private WindowManager f4381b;
    e c;
    boolean d = false;
    /* access modifiers changed from: private */
    public Handler e;
    private boolean f = false;
    private SurfaceView g;
    private TextureView h;
    private ab i;
    private int j = -1;
    /* access modifiers changed from: private */
    public List<a> k = new ArrayList();
    private r l;
    private n m = new n();
    private ad n;
    private ad o;
    private Rect p;
    /* access modifiers changed from: private */
    public ad q;
    private Rect r = null;
    private Rect s = null;
    private ad t = null;
    private double u = 0.1d;
    private v v = null;
    private boolean w = false;
    private final SurfaceHolder.Callback x = new e(this);
    private final Handler.Callback y = new f(this);
    private aa z = new g(this);

    public interface a {
        void a();

        void a(Exception exc);

        void b();

        void c();

        void d();
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    private TextureView.SurfaceTextureListener b() {
        return new d(this);
    }

    public CameraPreview(Context context) {
        super(context);
        a(context, (AttributeSet) null);
    }

    public CameraPreview(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public CameraPreview(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        if (getBackground() == null) {
            setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        }
        a(attributeSet);
        this.f4381b = (WindowManager) context.getSystemService("window");
        this.e = new Handler(this.y);
        this.i = new ab();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f) {
            this.h = new TextureView(getContext());
            this.h.setSurfaceTextureListener(b());
            addView(this.h);
            return;
        }
        this.g = new SurfaceView(getContext());
        this.g.getHolder().addCallback(this.x);
        addView(this.g);
    }

    /* access modifiers changed from: protected */
    public final void a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.styleable.zxing_camera_preview);
        int dimension = (int) obtainStyledAttributes.getDimension(R.styleable.zxing_camera_preview_zxing_framing_rect_width, -1.0f);
        int dimension2 = (int) obtainStyledAttributes.getDimension(R.styleable.zxing_camera_preview_zxing_framing_rect_height, -1.0f);
        if (dimension > 0 && dimension2 > 0) {
            this.t = new ad(dimension, dimension2);
        }
        this.f = obtainStyledAttributes.getBoolean(R.styleable.zxing_camera_preview_zxing_use_texture_view, true);
        int integer = obtainStyledAttributes.getInteger(R.styleable.zxing_camera_preview_zxing_preview_scaling_strategy, -1);
        if (integer == 1) {
            this.v = new q();
        } else if (integer == 2) {
            this.v = new s();
        } else if (integer == 3) {
            this.v = new t();
        }
        obtainStyledAttributes.recycle();
    }

    public final void a(a aVar) {
        this.k.add(aVar);
    }

    public void setTorch(boolean z2) {
        this.w = z2;
        e eVar = this.c;
        if (eVar != null) {
            eVar.a(z2);
        }
    }

    public void setPreviewScalingStrategy(v vVar) {
        this.v = vVar;
    }

    public v getPreviewScalingStrategy() {
        v vVar = this.v;
        if (vVar != null) {
            return vVar;
        }
        if (this.h != null) {
            return new q();
        }
        return new s();
    }

    private static Matrix a(ad adVar, ad adVar2) {
        float f2;
        float f3 = ((float) adVar.f4429a) / ((float) adVar.f4430b);
        float f4 = ((float) adVar2.f4429a) / ((float) adVar2.f4430b);
        float f5 = 1.0f;
        if (f3 < f4) {
            f5 = f4 / f3;
            f2 = 1.0f;
        } else {
            f2 = f3 / f4;
        }
        Matrix matrix = new Matrix();
        matrix.setScale(f5, f2);
        matrix.postTranslate((((float) adVar.f4429a) - (((float) adVar.f4429a) * f5)) / 2.0f, (((float) adVar.f4430b) - (((float) adVar.f4430b) * f2)) / 2.0f);
        return matrix;
    }

    /* access modifiers changed from: private */
    public void h() {
        Rect rect;
        ad adVar = this.q;
        if (adVar != null && this.o != null && (rect = this.p) != null) {
            if (this.g == null || !adVar.equals(new ad(rect.width(), this.p.height()))) {
                TextureView textureView = this.h;
                if (textureView != null && textureView.getSurfaceTexture() != null) {
                    if (this.o != null) {
                        this.h.setTransform(a(new ad(this.h.getWidth(), this.h.getHeight()), this.o));
                    }
                    a(new o(this.h.getSurfaceTexture()));
                    return;
                }
                return;
            }
            a(new o(this.g.getHolder()));
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        ad adVar = new ad(i4 - i2, i5 - i3);
        this.n = adVar;
        e eVar = this.c;
        if (eVar != null && eVar.d == null) {
            this.l = new r(getDisplayRotation(), adVar);
            this.l.c = getPreviewScalingStrategy();
            e eVar2 = this.c;
            r rVar = this.l;
            eVar2.d = rVar;
            eVar2.f4396b.g = rVar;
            this.c.b();
            boolean z3 = this.w;
            if (z3) {
                this.c.a(z3);
            }
        }
        SurfaceView surfaceView = this.g;
        if (surfaceView != null) {
            Rect rect = this.p;
            if (rect == null) {
                surfaceView.layout(0, 0, getWidth(), getHeight());
            } else {
                surfaceView.layout(rect.left, this.p.top, this.p.right, this.p.bottom);
            }
        } else {
            TextureView textureView = this.h;
            if (textureView != null) {
                textureView.layout(0, 0, getWidth(), getHeight());
            }
        }
    }

    public Rect getFramingRect() {
        return this.r;
    }

    public Rect getPreviewFramingRect() {
        return this.s;
    }

    public n getCameraSettings() {
        return this.m;
    }

    public void setCameraSettings(n nVar) {
        this.m = nVar;
    }

    public final void d() {
        af.a();
        i();
        if (this.q != null) {
            h();
        } else {
            SurfaceView surfaceView = this.g;
            if (surfaceView != null) {
                surfaceView.getHolder().addCallback(this.x);
            } else {
                TextureView textureView = this.h;
                if (textureView != null) {
                    if (textureView.isAvailable()) {
                        b().onSurfaceTextureAvailable(this.h.getSurfaceTexture(), this.h.getWidth(), this.h.getHeight());
                    } else {
                        this.h.setSurfaceTextureListener(b());
                    }
                }
            }
        }
        requestLayout();
        this.i.a(getContext(), this.z);
    }

    public void c() {
        TextureView textureView;
        SurfaceView surfaceView;
        af.a();
        this.j = -1;
        e eVar = this.c;
        if (eVar != null) {
            eVar.d();
            this.c = null;
            this.d = false;
        } else {
            this.e.sendEmptyMessage(R.id.zxing_camera_closed);
        }
        if (this.q == null && (surfaceView = this.g) != null) {
            surfaceView.getHolder().removeCallback(this.x);
        }
        if (this.q == null && (textureView = this.h) != null) {
            textureView.setSurfaceTextureListener(null);
        }
        this.n = null;
        this.o = null;
        this.s = null;
        this.i.a();
        this.A.c();
    }

    public final void e() {
        e cameraInstance = getCameraInstance();
        c();
        long nanoTime = System.nanoTime();
        while (cameraInstance != null && !cameraInstance.e && System.nanoTime() - nanoTime <= 2000000000) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException unused) {
                return;
            }
        }
    }

    public ad getFramingRectSize() {
        return this.t;
    }

    public void setFramingRectSize(ad adVar) {
        this.t = adVar;
    }

    public double getMarginFraction() {
        return this.u;
    }

    public void setMarginFraction(double d2) {
        if (d2 < 0.5d) {
            this.u = d2;
            return;
        }
        throw new IllegalArgumentException("The margin fraction must be less than 0.5");
    }

    public void setUseTextureView(boolean z2) {
        this.f = z2;
    }

    /* access modifiers changed from: protected */
    public final boolean f() {
        return this.c != null;
    }

    private int getDisplayRotation() {
        return this.f4381b.getDefaultDisplay().getRotation();
    }

    private void i() {
        if (this.c == null) {
            this.c = j();
            e eVar = this.c;
            eVar.c = this.e;
            eVar.a();
            this.j = getDisplayRotation();
        }
    }

    private e j() {
        e eVar = new e(getContext());
        eVar.a(this.m);
        return eVar;
    }

    private void a(o oVar) {
        e eVar;
        if (!this.d && (eVar = this.c) != null) {
            eVar.f4395a = oVar;
            eVar.c();
            this.d = true;
            a();
            this.A.b();
        }
    }

    public e getCameraInstance() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable("super", onSaveInstanceState);
        bundle.putBoolean("torch", this.w);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof Bundle)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        Bundle bundle = (Bundle) parcelable;
        super.onRestoreInstanceState(bundle.getParcelable("super"));
        setTorch(bundle.getBoolean("torch"));
    }

    static /* synthetic */ void b(CameraPreview cameraPreview, ad adVar) {
        ad adVar2;
        cameraPreview.o = adVar;
        ad adVar3 = cameraPreview.n;
        if (adVar3 == null) {
            return;
        }
        if (adVar3 == null || (adVar2 = cameraPreview.o) == null || cameraPreview.l == null) {
            cameraPreview.s = null;
            cameraPreview.r = null;
            cameraPreview.p = null;
            throw new IllegalStateException("containerSize or previewSize is not set yet");
        }
        int i2 = adVar2.f4429a;
        int i3 = cameraPreview.o.f4430b;
        int i4 = cameraPreview.n.f4429a;
        int i5 = cameraPreview.n.f4430b;
        r rVar = cameraPreview.l;
        cameraPreview.p = rVar.c.b(cameraPreview.o, rVar.f4419a);
        Rect rect = new Rect(0, 0, i4, i5);
        Rect rect2 = cameraPreview.p;
        Rect rect3 = new Rect(rect);
        rect3.intersect(rect2);
        if (cameraPreview.t != null) {
            rect3.inset(Math.max(0, (rect3.width() - cameraPreview.t.f4429a) / 2), Math.max(0, (rect3.height() - cameraPreview.t.f4430b) / 2));
        } else {
            double width = (double) rect3.width();
            double d2 = cameraPreview.u;
            Double.isNaN(width);
            double d3 = width * d2;
            double height = (double) rect3.height();
            double d4 = cameraPreview.u;
            Double.isNaN(height);
            int min = (int) Math.min(d3, height * d4);
            rect3.inset(min, min);
            if (rect3.height() > rect3.width()) {
                rect3.inset(0, (rect3.height() - rect3.width()) / 2);
            }
        }
        cameraPreview.r = rect3;
        Rect rect4 = new Rect(cameraPreview.r);
        rect4.offset(-cameraPreview.p.left, -cameraPreview.p.top);
        cameraPreview.s = new Rect((rect4.left * i2) / cameraPreview.p.width(), (rect4.top * i3) / cameraPreview.p.height(), (rect4.right * i2) / cameraPreview.p.width(), (rect4.bottom * i3) / cameraPreview.p.height());
        if (cameraPreview.s.width() <= 0 || cameraPreview.s.height() <= 0) {
            cameraPreview.s = null;
            cameraPreview.r = null;
        } else {
            cameraPreview.A.a();
        }
        cameraPreview.requestLayout();
        cameraPreview.h();
    }

    static /* synthetic */ void c(CameraPreview cameraPreview) {
        if (cameraPreview.f() && cameraPreview.getDisplayRotation() != cameraPreview.j) {
            cameraPreview.c();
            cameraPreview.d();
        }
    }
}
