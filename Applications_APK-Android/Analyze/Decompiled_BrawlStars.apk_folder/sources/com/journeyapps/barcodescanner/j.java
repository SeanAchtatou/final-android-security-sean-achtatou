package com.journeyapps.barcodescanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.helper.ItemTouchHelper;
import com.google.zxing.a;
import com.google.zxing.client.android.R;
import com.google.zxing.client.android.c;
import com.google.zxing.client.android.f;
import com.google.zxing.client.android.g;
import com.google.zxing.client.android.h;
import com.google.zxing.d;
import com.google.zxing.i;
import com.google.zxing.o;
import com.journeyapps.barcodescanner.CameraPreview;
import com.journeyapps.barcodescanner.a.n;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/* compiled from: CaptureManager */
public class j {

    /* renamed from: a  reason: collision with root package name */
    static int f4443a = ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION;
    /* access modifiers changed from: private */
    public static final String j = j.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    Activity f4444b;
    /* access modifiers changed from: package-private */
    public DecoratedBarcodeView c;
    int d = -1;
    boolean e = false;
    h f;
    /* access modifiers changed from: package-private */
    public Handler g;
    a h = new k(this);
    boolean i = false;
    private boolean k = false;
    /* access modifiers changed from: private */
    public c l;
    /* access modifiers changed from: private */
    public boolean m = false;
    private final CameraPreview.a n = new m(this);

    public j(Activity activity, DecoratedBarcodeView decoratedBarcodeView) {
        this.f4444b = activity;
        this.c = decoratedBarcodeView;
        decoratedBarcodeView.getBarcodeView().a(this.n);
        this.g = new Handler();
        this.f = new h(activity, new n(this));
        this.l = new c(activity);
    }

    public final void a(Intent intent, Bundle bundle) {
        int intExtra;
        int i2;
        this.f4444b.getWindow().addFlags(128);
        if (bundle != null) {
            this.d = bundle.getInt("SAVED_ORIENTATION_LOCK", -1);
        }
        if (intent != null) {
            if (intent.getBooleanExtra("SCAN_ORIENTATION_LOCKED", true)) {
                if (this.d == -1) {
                    int rotation = this.f4444b.getWindowManager().getDefaultDisplay().getRotation();
                    int i3 = this.f4444b.getResources().getConfiguration().orientation;
                    if (i3 == 2) {
                        if (!(rotation == 0 || rotation == 1)) {
                            i2 = 8;
                            this.d = i2;
                        }
                    } else if (i3 == 1) {
                        i2 = (rotation == 0 || rotation == 3) ? 1 : 9;
                        this.d = i2;
                    }
                    i2 = 0;
                    this.d = i2;
                }
                this.f4444b.setRequestedOrientation(this.d);
            }
            if ("com.google.zxing.client.android.SCAN".equals(intent.getAction())) {
                DecoratedBarcodeView decoratedBarcodeView = this.c;
                Set<a> a2 = f.a(intent);
                Map<d, Object> a3 = g.a(intent);
                n nVar = new n();
                if (intent.hasExtra("SCAN_CAMERA_ID") && (intExtra = intent.getIntExtra("SCAN_CAMERA_ID", -1)) >= 0) {
                    nVar.f4410a = intExtra;
                }
                String stringExtra = intent.getStringExtra("PROMPT_MESSAGE");
                if (stringExtra != null) {
                    decoratedBarcodeView.setStatusText(stringExtra);
                }
                int intExtra2 = intent.getIntExtra("SCAN_TYPE", 0);
                String stringExtra2 = intent.getStringExtra("CHARACTER_SET");
                new i().a(a3);
                decoratedBarcodeView.f4384a.setCameraSettings(nVar);
                decoratedBarcodeView.f4384a.setDecoderFactory(new x(a2, a3, stringExtra2, intExtra2));
            }
            if (!intent.getBooleanExtra("BEEP_ENABLED", true)) {
                this.l.f2952a = false;
            }
            if (intent.hasExtra("TIMEOUT")) {
                this.g.postDelayed(new o(this), intent.getLongExtra("TIMEOUT", 0));
            }
            if (intent.getBooleanExtra("BARCODE_IMAGE_ENABLED", false)) {
                this.k = true;
            }
        }
    }

    public final void a(int i2, int[] iArr) {
        if (i2 != f4443a) {
            return;
        }
        if (iArr.length <= 0 || iArr[0] != 0) {
            b();
        } else {
            this.c.f4384a.d();
        }
    }

    private String b(b bVar) {
        if (this.k) {
            Bitmap a2 = bVar.a();
            try {
                File createTempFile = File.createTempFile("barcodeimage", ".jpg", this.f4444b.getCacheDir());
                FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
                a2.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                fileOutputStream.close();
                return createTempFile.getAbsolutePath();
            } catch (IOException e2) {
                "Unable to create temporary file and store bitmap! " + e2;
            }
        }
        return null;
    }

    private void d() {
        BarcodeView barcodeView = this.c.getBarcodeView();
        if (barcodeView.c == null || barcodeView.c.e) {
            this.f4444b.finish();
        } else {
            this.m = true;
        }
        this.c.f4384a.c();
        this.f.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public final void a() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("TIMEOUT", true);
        this.f4444b.setResult(0, intent);
        d();
    }

    /* access modifiers changed from: protected */
    public final void a(b bVar) {
        String b2 = b(bVar);
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.addFlags(524288);
        intent.putExtra("SCAN_RESULT", bVar.toString());
        intent.putExtra("SCAN_RESULT_FORMAT", bVar.d().toString());
        byte[] c2 = bVar.c();
        if (c2 != null && c2.length > 0) {
            intent.putExtra("SCAN_RESULT_BYTES", c2);
        }
        Map<o, Object> e2 = bVar.e();
        if (e2 != null) {
            if (e2.containsKey(o.UPC_EAN_EXTENSION)) {
                intent.putExtra("SCAN_RESULT_UPC_EAN_EXTENSION", e2.get(o.UPC_EAN_EXTENSION).toString());
            }
            Number number = (Number) e2.get(o.ORIENTATION);
            if (number != null) {
                intent.putExtra("SCAN_RESULT_ORIENTATION", number.intValue());
            }
            String str = (String) e2.get(o.ERROR_CORRECTION_LEVEL);
            if (str != null) {
                intent.putExtra("SCAN_RESULT_ERROR_CORRECTION_LEVEL", str);
            }
            Iterable<byte[]> iterable = (Iterable) e2.get(o.BYTE_SEGMENTS);
            if (iterable != null) {
                int i2 = 0;
                for (byte[] putExtra : iterable) {
                    intent.putExtra("SCAN_RESULT_BYTE_SEGMENTS_" + i2, putExtra);
                    i2++;
                }
            }
        }
        if (b2 != null) {
            intent.putExtra("SCAN_RESULT_IMAGE_PATH", b2);
        }
        this.f4444b.setResult(-1, intent);
        d();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (!this.f4444b.isFinishing() && !this.e && !this.m) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.f4444b);
            builder.setTitle(this.f4444b.getString(R.string.zxing_app_name));
            builder.setMessage(this.f4444b.getString(R.string.zxing_msg_camera_framework_bug));
            builder.setPositiveButton(R.string.zxing_button_ok, new p(this));
            builder.setOnCancelListener(new q(this));
            builder.show();
        }
    }
}
