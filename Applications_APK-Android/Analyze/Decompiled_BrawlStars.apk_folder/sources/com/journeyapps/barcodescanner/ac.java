package com.journeyapps.barcodescanner;

import android.content.Context;
import android.view.OrientationEventListener;
import android.view.WindowManager;

/* compiled from: RotationListener */
class ac extends OrientationEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f4428a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ac(ab abVar, Context context, int i) {
        super(context, 3);
        this.f4428a = abVar;
    }

    public void onOrientationChanged(int i) {
        int rotation;
        WindowManager windowManager = this.f4428a.f4427b;
        aa aaVar = this.f4428a.c;
        if (this.f4428a.f4427b != null && aaVar != null && (rotation = windowManager.getDefaultDisplay().getRotation()) != this.f4428a.f4426a) {
            this.f4428a.f4426a = rotation;
            aaVar.a();
        }
    }
}
