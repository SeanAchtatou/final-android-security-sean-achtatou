package com.journeyapps.barcodescanner;

import com.google.zxing.c;
import com.google.zxing.common.j;
import com.google.zxing.h;
import com.google.zxing.m;

/* compiled from: MixedDecoder */
public class z extends r {

    /* renamed from: b  reason: collision with root package name */
    private boolean f4462b = true;

    public z(m mVar) {
        super(mVar);
    }

    /* access modifiers changed from: protected */
    public final c b(h hVar) {
        if (this.f4462b) {
            this.f4462b = false;
            return new c(new j(hVar.c()));
        }
        this.f4462b = true;
        return new c(new j(hVar));
    }
}
