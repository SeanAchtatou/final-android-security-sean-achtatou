package com.journeyapps.barcodescanner;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.google.zxing.client.android.R;
import com.google.zxing.p;
import java.util.List;

public class DecoratedBarcodeView extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    BarcodeView f4384a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ViewfinderView f4385b;
    private TextView c;
    private a d;

    public interface a {
    }

    class b implements a {

        /* renamed from: b  reason: collision with root package name */
        private a f4387b;

        public b(a aVar) {
            this.f4387b = aVar;
        }

        public void barcodeResult(b bVar) {
            this.f4387b.barcodeResult(bVar);
        }

        public void possibleResultPoints(List<p> list) {
            for (p a2 : list) {
                DecoratedBarcodeView.this.f4385b.a(a2);
            }
            this.f4387b.possibleResultPoints(list);
        }
    }

    public DecoratedBarcodeView(Context context) {
        super(context);
        a((AttributeSet) null);
    }

    public DecoratedBarcodeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet);
    }

    public DecoratedBarcodeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(attributeSet);
    }

    private void a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.styleable.zxing_view);
        int resourceId = obtainStyledAttributes.getResourceId(R.styleable.zxing_view_zxing_scanner_layout, R.layout.zxing_barcode_scanner);
        obtainStyledAttributes.recycle();
        inflate(getContext(), resourceId, this);
        this.f4384a = (BarcodeView) findViewById(R.id.zxing_barcode_surface);
        BarcodeView barcodeView = this.f4384a;
        if (barcodeView != null) {
            barcodeView.a(attributeSet);
            this.f4385b = (ViewfinderView) findViewById(R.id.zxing_viewfinder_view);
            ViewfinderView viewfinderView = this.f4385b;
            if (viewfinderView != null) {
                viewfinderView.setCameraPreview(this.f4384a);
                this.c = (TextView) findViewById(R.id.zxing_status_view);
                return;
            }
            throw new IllegalArgumentException("There is no a com.journeyapps.barcodescanner.ViewfinderView on provided layout with the id \"zxing_viewfinder_view\".");
        }
        throw new IllegalArgumentException("There is no a com.journeyapps.barcodescanner.BarcodeView on provided layout with the id \"zxing_barcode_surface\".");
    }

    public void setStatusText(String str) {
        TextView textView = this.c;
        if (textView != null) {
            textView.setText(str);
        }
    }

    public BarcodeView getBarcodeView() {
        return (BarcodeView) findViewById(R.id.zxing_barcode_surface);
    }

    public ViewfinderView getViewFinder() {
        return this.f4385b;
    }

    public TextView getStatusView() {
        return this.c;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 24) {
            this.f4384a.setTorch(true);
            return true;
        } else if (i == 25) {
            this.f4384a.setTorch(false);
            return true;
        } else if (i == 27 || i == 80) {
            return true;
        } else {
            return super.onKeyDown(i, keyEvent);
        }
    }

    public void setTorchListener(a aVar) {
        this.d = aVar;
    }
}
