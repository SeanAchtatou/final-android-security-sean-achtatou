package com.journeyapps.barcodescanner;

import com.google.zxing.c;
import com.google.zxing.common.j;
import com.google.zxing.d;
import com.google.zxing.h;
import com.google.zxing.i;
import com.google.zxing.m;
import com.google.zxing.n;
import com.google.zxing.p;
import com.google.zxing.q;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: Decoder */
public class r implements q {

    /* renamed from: a  reason: collision with root package name */
    List<p> f4453a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private m f4454b;

    public r(m mVar) {
        this.f4454b = mVar;
    }

    public final n a(h hVar) {
        return a(b(hVar));
    }

    /* access modifiers changed from: protected */
    public c b(h hVar) {
        return new c(new j(hVar));
    }

    private n a(c cVar) {
        n a2;
        this.f4453a.clear();
        try {
            if (this.f4454b instanceof i) {
                i iVar = (i) this.f4454b;
                if (iVar.f3146a == null) {
                    iVar.a((Map<d, ?>) null);
                }
                a2 = iVar.b(cVar);
            } else {
                a2 = this.f4454b.a(cVar);
            }
            return a2;
        } catch (Exception unused) {
            return null;
        } finally {
            this.f4454b.a();
        }
    }

    public final void a(p pVar) {
        this.f4453a.add(pVar);
    }
}
