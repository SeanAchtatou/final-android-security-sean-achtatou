package com.journeyapps.barcodescanner;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import com.google.zxing.a;
import com.google.zxing.n;
import com.google.zxing.o;
import java.io.ByteArrayOutputStream;
import java.util.Map;

/* compiled from: BarcodeResult */
public class b {

    /* renamed from: a  reason: collision with root package name */
    protected n f4434a;

    /* renamed from: b  reason: collision with root package name */
    protected ae f4435b;
    private final int c = 2;

    public b(n nVar, ae aeVar) {
        this.f4434a = nVar;
        this.f4435b = aeVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public final Bitmap a() {
        ae aeVar = this.f4435b;
        Rect rect = aeVar.f;
        if (aeVar.a()) {
            rect = new Rect(rect.top, rect.left, rect.bottom, rect.right);
        }
        YuvImage yuvImage = new YuvImage(aeVar.f4431a, aeVar.d, aeVar.f4432b, aeVar.c, null);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(rect, 90, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
        if (aeVar.e == 0) {
            return decodeByteArray;
        }
        Matrix matrix = new Matrix();
        matrix.postRotate((float) aeVar.e);
        return Bitmap.createBitmap(decodeByteArray, 0, 0, decodeByteArray.getWidth(), decodeByteArray.getHeight(), matrix, false);
    }

    public final String b() {
        return this.f4434a.f3149a;
    }

    public final byte[] c() {
        return this.f4434a.f3150b;
    }

    public final a d() {
        return this.f4434a.d;
    }

    public final Map<o, Object> e() {
        return this.f4434a.e;
    }

    public String toString() {
        return this.f4434a.f3149a;
    }
}
