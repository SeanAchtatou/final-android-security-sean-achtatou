package com.journeyapps.barcodescanner;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import com.google.zxing.client.android.R;
import com.google.zxing.p;
import com.supercell.id.view.AvatarView;
import java.util.ArrayList;
import java.util.List;

public class ViewfinderView extends View {

    /* renamed from: a  reason: collision with root package name */
    protected static final String f4388a = ViewfinderView.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    protected static final int[] f4389b = {0, 64, 128, 192, 255, 192, 128, 64};
    protected final Paint c = new Paint(1);
    protected Bitmap d;
    protected final int e;
    protected final int f;
    protected final int g;
    protected final int h;
    protected int i;
    protected List<p> j;
    protected List<p> k;
    protected CameraPreview l;
    protected Rect m;
    protected Rect n;

    public ViewfinderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Resources resources = getResources();
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.styleable.zxing_finder);
        this.e = obtainStyledAttributes.getColor(R.styleable.zxing_finder_zxing_viewfinder_mask, resources.getColor(R.color.zxing_viewfinder_mask));
        this.f = obtainStyledAttributes.getColor(R.styleable.zxing_finder_zxing_result_view, resources.getColor(R.color.zxing_result_view));
        this.g = obtainStyledAttributes.getColor(R.styleable.zxing_finder_zxing_viewfinder_laser, resources.getColor(R.color.zxing_viewfinder_laser));
        this.h = obtainStyledAttributes.getColor(R.styleable.zxing_finder_zxing_possible_result_points, resources.getColor(R.color.zxing_possible_result_points));
        obtainStyledAttributes.recycle();
        this.i = 0;
        this.j = new ArrayList(20);
        this.k = new ArrayList(20);
    }

    public void setCameraPreview(CameraPreview cameraPreview) {
        this.l = cameraPreview;
        cameraPreview.a(new ag(this));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        CameraPreview cameraPreview = this.l;
        if (cameraPreview != null) {
            Rect framingRect = cameraPreview.getFramingRect();
            Rect previewFramingRect = this.l.getPreviewFramingRect();
            if (framingRect != null && previewFramingRect != null) {
                this.m = framingRect;
                this.n = previewFramingRect;
            }
        }
    }

    public void onDraw(Canvas canvas) {
        Rect rect;
        a();
        Rect rect2 = this.m;
        if (rect2 != null && (rect = this.n) != null) {
            int width = canvas.getWidth();
            int height = canvas.getHeight();
            this.c.setColor(this.d != null ? this.f : this.e);
            float f2 = (float) width;
            canvas.drawRect(0.0f, 0.0f, f2, (float) rect2.top, this.c);
            canvas.drawRect(0.0f, (float) rect2.top, (float) rect2.left, (float) (rect2.bottom + 1), this.c);
            float f3 = f2;
            canvas.drawRect((float) (rect2.right + 1), (float) rect2.top, f3, (float) (rect2.bottom + 1), this.c);
            canvas.drawRect(0.0f, (float) (rect2.bottom + 1), f3, (float) height, this.c);
            if (this.d != null) {
                this.c.setAlpha(AvatarView.INTRINSIC_POINT_SIZE);
                canvas.drawBitmap(this.d, (Rect) null, rect2, this.c);
                return;
            }
            this.c.setColor(this.g);
            this.c.setAlpha(f4389b[this.i]);
            this.i = (this.i + 1) % f4389b.length;
            int height2 = (rect2.height() / 2) + rect2.top;
            canvas.drawRect((float) (rect2.left + 2), (float) (height2 - 1), (float) (rect2.right - 1), (float) (height2 + 2), this.c);
            float width2 = ((float) rect2.width()) / ((float) rect.width());
            float height3 = ((float) rect2.height()) / ((float) rect.height());
            int i2 = rect2.left;
            int i3 = rect2.top;
            if (!this.k.isEmpty()) {
                this.c.setAlpha(80);
                this.c.setColor(this.h);
                for (p next : this.k) {
                    canvas.drawCircle((float) (((int) (next.f3153a * width2)) + i2), (float) (((int) (next.f3154b * height3)) + i3), 3.0f, this.c);
                }
                this.k.clear();
            }
            if (!this.j.isEmpty()) {
                this.c.setAlpha(AvatarView.INTRINSIC_POINT_SIZE);
                this.c.setColor(this.h);
                for (p next2 : this.j) {
                    canvas.drawCircle((float) (((int) (next2.f3153a * width2)) + i2), (float) (((int) (next2.f3154b * height3)) + i3), 6.0f, this.c);
                }
                List<p> list = this.j;
                this.j = this.k;
                this.k = list;
                this.j.clear();
            }
            postInvalidateDelayed(80, rect2.left - 6, rect2.top - 6, rect2.right + 6, rect2.bottom + 6);
        }
    }

    public final void a(p pVar) {
        if (this.j.size() < 20) {
            this.j.add(pVar);
        }
    }
}
