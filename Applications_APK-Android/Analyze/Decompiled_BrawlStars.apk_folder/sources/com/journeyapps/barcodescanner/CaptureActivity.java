package com.journeyapps.barcodescanner;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import com.google.zxing.client.android.R;
import com.google.zxing.client.android.h;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

public class CaptureActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private j f4382a;

    /* renamed from: b  reason: collision with root package name */
    private DecoratedBarcodeView f4383b;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.zxing_capture);
        this.f4383b = (DecoratedBarcodeView) findViewById(R.id.zxing_barcode_scanner);
        this.f4382a = new j(this, this.f4383b);
        this.f4382a.a(getIntent(), bundle);
        j jVar = this.f4382a;
        DecoratedBarcodeView decoratedBarcodeView = jVar.c;
        decoratedBarcodeView.f4384a.a(new DecoratedBarcodeView.b(jVar.h));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        j jVar = this.f4382a;
        if (Build.VERSION.SDK_INT < 23) {
            jVar.c.f4384a.d();
        } else if (ContextCompat.checkSelfPermission(jVar.f4444b, "android.permission.CAMERA") == 0) {
            jVar.c.f4384a.d();
        } else if (!jVar.i) {
            ActivityCompat.requestPermissions(jVar.f4444b, new String[]{"android.permission.CAMERA"}, j.f4443a);
            jVar.i = true;
        }
        h hVar = jVar.f;
        if (!hVar.c) {
            hVar.f2959a.registerReceiver(hVar.f2960b, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            hVar.c = true;
        }
        hVar.a();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        j jVar = this.f4382a;
        jVar.f.b();
        jVar.c.f4384a.e();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        j jVar = this.f4382a;
        jVar.e = true;
        jVar.f.b();
        jVar.g.removeCallbacksAndMessages(null);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("SAVED_ORIENTATION_LOCK", this.f4382a.d);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        this.f4382a.a(i, iArr);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return this.f4383b.onKeyDown(i, keyEvent) || super.onKeyDown(i, keyEvent);
    }
}
