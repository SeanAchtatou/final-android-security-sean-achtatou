package com.journeyapps.barcodescanner;

import android.os.Handler;
import android.os.Message;
import com.google.zxing.client.android.R;

/* compiled from: CameraPreview */
class f implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CameraPreview f4439a;

    f(CameraPreview cameraPreview) {
        this.f4439a = cameraPreview;
    }

    public boolean handleMessage(Message message) {
        if (message.what == R.id.zxing_prewiew_size_ready) {
            CameraPreview.b(this.f4439a, (ad) message.obj);
            return true;
        } else if (message.what == R.id.zxing_camera_error) {
            Exception exc = (Exception) message.obj;
            if (!this.f4439a.f()) {
                return false;
            }
            this.f4439a.c();
            this.f4439a.A.a(exc);
            return false;
        } else if (message.what != R.id.zxing_camera_closed) {
            return false;
        } else {
            this.f4439a.A.d();
            return false;
        }
    }
}
