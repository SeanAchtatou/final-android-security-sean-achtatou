package com.journeyapps.barcodescanner;

import android.view.SurfaceHolder;

/* compiled from: CameraPreview */
class e implements SurfaceHolder.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CameraPreview f4438a;

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
    }

    e(CameraPreview cameraPreview) {
        this.f4438a = cameraPreview;
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        ad unused = this.f4438a.q = (ad) null;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        if (surfaceHolder == null) {
            String unused = CameraPreview.f4380a;
            return;
        }
        ad unused2 = this.f4438a.q = new ad(i2, i3);
        this.f4438a.h();
    }
}
