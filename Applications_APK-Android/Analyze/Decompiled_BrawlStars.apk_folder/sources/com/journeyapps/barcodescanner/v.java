package com.journeyapps.barcodescanner;

import android.os.Handler;
import android.os.Message;
import com.google.zxing.client.android.R;

/* compiled from: DecoderThread */
class v implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f4458a;

    v(u uVar) {
        this.f4458a = uVar;
    }

    public boolean handleMessage(Message message) {
        if (message.what == R.id.zxing_decode) {
            u.a(this.f4458a, (ae) message.obj);
            return true;
        } else if (message.what != R.id.zxing_preview_failed) {
            return true;
        } else {
            this.f4458a.c();
            return true;
        }
    }
}
