package com.journeyapps.barcodescanner;

import android.content.Context;
import android.view.OrientationEventListener;
import android.view.WindowManager;

/* compiled from: RotationListener */
public class ab {

    /* renamed from: a  reason: collision with root package name */
    int f4426a;

    /* renamed from: b  reason: collision with root package name */
    WindowManager f4427b;
    aa c;
    private OrientationEventListener d;

    public final void a(Context context, aa aaVar) {
        a();
        Context applicationContext = context.getApplicationContext();
        this.c = aaVar;
        this.f4427b = (WindowManager) applicationContext.getSystemService("window");
        this.d = new ac(this, applicationContext, 3);
        this.d.enable();
        this.f4426a = this.f4427b.getDefaultDisplay().getRotation();
    }

    public final void a() {
        OrientationEventListener orientationEventListener = this.d;
        if (orientationEventListener != null) {
            orientationEventListener.disable();
        }
        this.d = null;
        this.f4427b = null;
        this.c = null;
    }
}
