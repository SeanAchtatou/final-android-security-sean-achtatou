package com.helpshift.i.c;

/* compiled from: RootServerConfig */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public final boolean f3433a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3434b;
    public final boolean c;
    public final boolean d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final int h;
    public final int i;
    public final String j;
    public final a k;
    public final boolean l;
    public final String m;
    public final boolean n;
    public final boolean o;
    public final boolean p;
    public final Long q;
    public final Long r;
    public final boolean s;
    public final long t;
    public final long u;
    public final boolean v;

    public b(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i2, int i3, String str, a aVar, boolean z8, String str2, boolean z9, boolean z10, boolean z11, Long l2, Long l3, boolean z12, long j2, long j3, boolean z13) {
        this.f3433a = z;
        this.f3434b = z2;
        this.c = z3;
        this.d = z4;
        this.e = z5;
        this.f = z6;
        this.g = z7;
        this.h = i2;
        this.i = i3;
        this.j = str;
        this.k = aVar;
        this.l = z8;
        this.m = str2;
        this.n = z9;
        this.o = z10;
        this.p = z11;
        this.q = l2;
        this.r = l3;
        this.s = z12;
        this.t = j2;
        this.u = j3;
        this.v = z13;
    }
}
