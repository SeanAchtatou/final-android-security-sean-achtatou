package com.helpshift.i.b;

import com.helpshift.common.g.f;
import com.helpshift.common.k;
import java.util.Map;

/* compiled from: RootApiConfig */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public final Boolean f3421a;

    /* renamed from: b  reason: collision with root package name */
    public final Boolean f3422b;
    public final Boolean c;
    public final Boolean d;
    public final Boolean e;
    public final Boolean f;
    public final C0137a g;
    public final String h;
    public final Boolean i;
    public final Boolean j;
    public final Boolean k;
    public final String l;

    public a(Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6, C0137a aVar, String str, Boolean bool7, Boolean bool8, Boolean bool9, String str2) {
        this.g = aVar;
        this.f3421a = bool;
        this.f3422b = bool2;
        this.c = bool3;
        this.h = str;
        this.d = bool4;
        this.e = bool5;
        this.f = bool6;
        this.i = bool7;
        this.j = bool8;
        this.k = bool9;
        this.l = str2;
    }

    /* renamed from: com.helpshift.i.b.a$a  reason: collision with other inner class name */
    /* compiled from: RootApiConfig */
    public enum C0137a {
        ALWAYS(0),
        NEVER(1),
        AFTER_VIEWING_FAQS(2),
        AFTER_MARKING_ANSWER_UNHELPFUL(3);
        
        public final int e;

        private C0137a(int i) {
            this.e = i;
        }

        public static C0137a a(int i) {
            for (C0137a aVar : values()) {
                if (aVar.e == i) {
                    return aVar;
                }
            }
            return null;
        }

        public final int a() {
            return this.e;
        }
    }

    /* compiled from: RootApiConfig */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        private Boolean f3425a;

        /* renamed from: b  reason: collision with root package name */
        private Boolean f3426b;
        private Boolean c;
        private Boolean d;
        private Boolean e;
        private Boolean f;
        private C0137a g;
        private String h = "";
        private Boolean i;
        private Boolean j;
        private Boolean k;
        private String l = "";

        public final b a(Map<String, Object> map) {
            Integer num = (Integer) f.a(map, "enableContactUs", Integer.class, null);
            if (num != null) {
                this.g = C0137a.a(num.intValue());
            }
            String str = "gotoConversationAfterContactUs";
            if (!map.containsKey(str)) {
                str = map.containsKey("gotoCoversationAfterContactUs") ? "gotoCoversationAfterContactUs" : "";
            }
            this.f3425a = (Boolean) f.a(map, str, Boolean.class, this.f3425a);
            this.f3426b = (Boolean) f.a(map, "requireEmail", Boolean.class, this.f3426b);
            this.c = (Boolean) f.a(map, "hideNameAndEmail", Boolean.class, this.c);
            this.d = (Boolean) f.a(map, "enableFullPrivacy", Boolean.class, this.d);
            this.e = (Boolean) f.a(map, "showSearchOnNewConversation", Boolean.class, this.e);
            this.f = (Boolean) f.a(map, "showConversationResolutionQuestion", Boolean.class, this.f);
            this.h = (String) f.a(map, "conversationPrefillText", String.class, this.h);
            if (k.a(this.h)) {
                this.h = "";
            }
            this.i = (Boolean) f.a(map, "showConversationInfoScreen", Boolean.class, this.i);
            this.j = (Boolean) f.a(map, "enableTypingIndicator", Boolean.class, this.j);
            this.k = (Boolean) f.a(map, "enableDefaultConversationalFiling", Boolean.class, this.k);
            this.l = (String) f.a(map, "initialUserMessage", String.class, this.l);
            this.l = this.l.trim();
            if (k.a(this.l)) {
                this.l = "";
            }
            return this;
        }

        public final a a() {
            return new a(this.f3425a, this.f3426b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l);
        }
    }
}
