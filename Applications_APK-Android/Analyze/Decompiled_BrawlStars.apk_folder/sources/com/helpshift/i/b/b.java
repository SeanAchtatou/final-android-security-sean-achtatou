package com.helpshift.i.b;

import com.helpshift.common.g.f;
import java.util.Map;

/* compiled from: RootInstallConfig */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public final Boolean f3427a;

    /* renamed from: b  reason: collision with root package name */
    public final Boolean f3428b;
    public final Boolean c;
    public final Boolean d;
    public final Boolean e;
    public final Boolean f;
    public final Boolean g;
    public final Boolean h;
    public final Integer i;
    public final Integer j;
    public final Integer k;
    public final String l;
    public final String m;
    public final String n;
    public final String o;
    public final String p;

    public b(Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, Boolean bool6, Boolean bool7, Boolean bool8, Integer num, Integer num2, Integer num3, String str, String str2, String str3, String str4, String str5) {
        this.f3427a = bool;
        this.d = bool4;
        this.e = bool5;
        this.f = bool6;
        this.g = bool7;
        this.h = bool8;
        this.i = num;
        this.j = num2;
        this.k = num3;
        this.f3428b = bool2;
        this.c = bool3;
        this.l = str;
        this.m = str2;
        this.n = str3;
        this.o = str4;
        this.p = str5;
    }

    /* compiled from: RootInstallConfig */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        private Boolean f3429a;

        /* renamed from: b  reason: collision with root package name */
        private Boolean f3430b;
        private Boolean c;
        private Boolean d;
        private Boolean e;
        private Boolean f;
        private Boolean g;
        private Boolean h;
        private Integer i;
        private Integer j;
        private Integer k;
        private String l;
        private String m;
        private String n;
        private String o;
        private String p;

        public final a a(Map<String, Object> map) {
            this.f3429a = (Boolean) f.a(map, "enableInAppNotification", Boolean.class, this.f3429a);
            this.f3430b = (Boolean) f.a(map, "enableDefaultFallbackLanguage", Boolean.class, this.f3430b);
            this.c = (Boolean) f.a(map, "enableInboxPolling", Boolean.class, this.c);
            this.d = (Boolean) f.a(map, "enableNotificationMute", Boolean.class, this.d);
            this.e = (Boolean) f.a(map, "disableHelpshiftBranding", Boolean.class, this.e);
            this.g = (Boolean) f.a(map, "disableErrorLogging", Boolean.class, this.g);
            this.h = (Boolean) f.a(map, "disableAppLaunchEvent", Boolean.class, this.h);
            this.f = (Boolean) f.a(map, "disableAnimations", Boolean.class, this.f);
            this.i = (Integer) f.a(map, "notificationIcon", Integer.class, this.i);
            this.j = (Integer) f.a(map, "largeNotificationIcon", Integer.class, this.j);
            this.k = (Integer) f.a(map, "notificationSound", Integer.class, this.k);
            this.l = (String) f.a(map, "font", String.class, this.l);
            this.m = (String) f.a(map, "sdkType", String.class, this.m);
            this.n = (String) f.a(map, "pluginVersion", String.class, this.n);
            this.o = (String) f.a(map, "runtimeVersion", String.class, this.o);
            this.p = (String) f.a(map, "supportNotificationChannelId", String.class, this.p);
            return this;
        }

        public final b a() {
            return new b(this.f3429a, this.f3430b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p);
        }
    }
}
