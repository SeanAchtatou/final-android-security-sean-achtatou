package com.helpshift.i.a;

import com.helpshift.a.b.c;
import com.helpshift.a.b.e;
import com.helpshift.common.c.b.f;
import com.helpshift.common.c.b.h;
import com.helpshift.common.c.b.n;
import com.helpshift.common.c.b.o;
import com.helpshift.common.c.b.p;
import com.helpshift.common.c.b.s;
import com.helpshift.common.c.j;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.k;
import com.helpshift.common.e.aa;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.i.b.a;
import com.helpshift.i.c.b;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;

/* compiled from: SDKConfigurationDM */
public class a extends Observable {

    /* renamed from: a  reason: collision with root package name */
    public static final Long f3419a = 43200L;
    private static final Long c = 60L;

    /* renamed from: b  reason: collision with root package name */
    public final aa f3420b;
    private final j d;
    private final ab e;
    private final k f;

    public a(j jVar, ab abVar) {
        this.d = jVar;
        this.e = abVar;
        this.f = abVar.l();
        this.f3420b = abVar.o();
    }

    public final b a(e eVar) {
        c a2 = eVar.a();
        String str = n.d;
        try {
            com.helpshift.common.e.a.j a3 = new com.helpshift.common.c.b.j(new f(new s(new h(str, this.d, this.e), this.e), this.e, str)).a(new i(o.a(eVar != null ? eVar.a() : null)));
            if (a3.f3323b == null) {
                com.helpshift.util.n.a("Helpshift_SDKConfigDM", "SDK config data fetched but nothing to update.", (Throwable) null, (com.helpshift.p.b.a[]) null);
                l();
                return null;
            }
            com.helpshift.util.n.a("Helpshift_SDKConfigDM", "SDK config data updated successfully", (Throwable) null, (com.helpshift.p.b.a[]) null);
            b n = this.f.n(a3.f3323b);
            HashMap hashMap = new HashMap();
            hashMap.put("requireNameAndEmail", Boolean.valueOf(n.f3433a));
            hashMap.put("profileFormEnable", Boolean.valueOf(n.f3434b));
            hashMap.put("showAgentName", Boolean.valueOf(n.c));
            hashMap.put("customerSatisfactionSurvey", Boolean.valueOf(n.d));
            hashMap.put("disableInAppConversation", Boolean.valueOf(n.e));
            hashMap.put("disableHelpshiftBrandingAgent", Boolean.valueOf(n.f));
            hashMap.put("debugLogLimit", Integer.valueOf(n.h));
            hashMap.put("breadcrumbLimit", Integer.valueOf(n.i));
            hashMap.put("reviewUrl", n.j);
            com.helpshift.i.c.a aVar = n.k;
            if (aVar == null) {
                aVar = new com.helpshift.i.c.a(false, 0, null);
            }
            hashMap.put("periodicReviewEnabled", Boolean.valueOf(aVar.f3431a));
            hashMap.put("periodicReviewInterval", Integer.valueOf(aVar.f3432b));
            hashMap.put("periodicReviewType", aVar.c);
            hashMap.put("conversationGreetingMessage", n.m);
            hashMap.put("conversationalIssueFiling", Boolean.valueOf(n.l));
            hashMap.put("enableTypingIndicatorAgent", Boolean.valueOf(n.n));
            hashMap.put("showConversationResolutionQuestionAgent", Boolean.valueOf(n.o));
            hashMap.put("showConversationHistoryAgent", Boolean.valueOf(n.p));
            hashMap.put("allowUserAttachments", Boolean.valueOf(n.s));
            hashMap.put("periodicFetchInterval", Long.valueOf(n.t));
            hashMap.put("preissueResetInterval", Long.valueOf(n.u));
            hashMap.put("autoFillFirstPreIssueMessage", Boolean.valueOf(n.v));
            this.f3420b.a(hashMap);
            setChanged();
            notifyObservers();
            eVar.a(a2, n.g);
            l();
            return n;
        } catch (RootAPIException e2) {
            if ((e2.c instanceof com.helpshift.common.exception.b) && ((com.helpshift.common.exception.b) e2.c).u == p.i.intValue()) {
                l();
            }
            throw e2;
        }
    }

    private void l() {
        this.f3420b.a("lastSuccessfulConfigFetchTime", Long.valueOf(System.currentTimeMillis() / 1000));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
     arg types: [java.lang.String, long]
     candidates:
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long */
    public final Long a() {
        return this.f3420b.b("lastSuccessfulConfigFetchTime", (Long) 0L);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean */
    public final boolean a(String str) {
        char c2;
        boolean z = true;
        switch (str.hashCode()) {
            case -1703140188:
                if (str.equals("conversationalIssueFiling")) {
                    c2 = 7;
                    break;
                }
                c2 = 65535;
                break;
            case -591814160:
                if (str.equals("profileFormEnable")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case -423624397:
                if (str.equals("showConversationResolutionQuestionAgent")) {
                    c2 = 4;
                    break;
                }
                c2 = 65535;
                break;
            case -366496336:
                if (str.equals("enableTypingIndicatorAgent")) {
                    c2 = 5;
                    break;
                }
                c2 = 65535;
                break;
            case -338380156:
                if (str.equals("enableInAppNotification")) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            case 1262906910:
                if (str.equals("defaultFallbackLanguageEnable")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case 1423623260:
                if (str.equals("allowUserAttachments")) {
                    c2 = 6;
                    break;
                }
                c2 = 65535;
                break;
            case 1952413875:
                if (str.equals("showAgentName")) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                break;
            case 7:
                z = this.f3420b.b("enableDefaultConversationalFiling", (Boolean) false).booleanValue();
                break;
            default:
                z = false;
                break;
        }
        return this.f3420b.b(str, Boolean.valueOf(z)).booleanValue();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Integer b(java.lang.String r4) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            r1 = -71624118(0xfffffffffbbb1a4a, float:-1.9429854E36)
            r2 = 1
            if (r0 == r1) goto L_0x001a
            r1 = 1384494456(0x5285b578, float:2.87137595E11)
            if (r0 == r1) goto L_0x0010
            goto L_0x0024
        L_0x0010:
            java.lang.String r0 = "breadcrumbLimit"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0024
            r0 = 1
            goto L_0x0025
        L_0x001a:
            java.lang.String r0 = "debugLogLimit"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0024
            r0 = 0
            goto L_0x0025
        L_0x0024:
            r0 = -1
        L_0x0025:
            if (r0 == 0) goto L_0x002b
            if (r0 == r2) goto L_0x002b
            r0 = 0
            goto L_0x0031
        L_0x002b:
            r0 = 100
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
        L_0x0031:
            com.helpshift.common.e.aa r1 = r3.f3420b
            java.lang.Integer r4 = r1.a(r4, r0)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.i.a.a.b(java.lang.String):java.lang.Integer");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String c(java.lang.String r5) {
        /*
            r4 = this;
            int r0 = r5.hashCode()
            r1 = -340534862(0xffffffffebb3d9b2, float:-4.348515E26)
            r2 = 2
            r3 = 1
            if (r0 == r1) goto L_0x002a
            r1 = 493025015(0x1d62f6f7, float:3.0038529E-21)
            if (r0 == r1) goto L_0x0020
            r1 = 1948062356(0x741d1294, float:4.9778285E31)
            if (r0 == r1) goto L_0x0016
            goto L_0x0034
        L_0x0016:
            java.lang.String r0 = "sdkType"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = 2
            goto L_0x0035
        L_0x0020:
            java.lang.String r0 = "reviewUrl"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = 0
            goto L_0x0035
        L_0x002a:
            java.lang.String r0 = "sdkLanguage"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = 1
            goto L_0x0035
        L_0x0034:
            r0 = -1
        L_0x0035:
            if (r0 == 0) goto L_0x0040
            if (r0 == r3) goto L_0x0040
            if (r0 == r2) goto L_0x003d
            r0 = 0
            goto L_0x0042
        L_0x003d:
            java.lang.String r0 = "android"
            goto L_0x0042
        L_0x0040:
            java.lang.String r0 = ""
        L_0x0042:
            com.helpshift.common.e.aa r1 = r4.f3420b
            java.lang.String r5 = r1.b(r5, r0)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.i.a.a.c(java.lang.String):java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.e.aa.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.common.e.aa.a(java.lang.String, java.io.Serializable):void
      com.helpshift.common.e.aa.a(java.lang.String, java.lang.Boolean):void
      com.helpshift.common.e.aa.a(java.lang.String, java.lang.Float):void
      com.helpshift.common.e.aa.a(java.lang.String, java.lang.Long):void
      com.helpshift.common.e.aa.a(java.lang.String, java.lang.String):void
      com.helpshift.common.e.aa.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    public final com.helpshift.i.c.a b() {
        return new com.helpshift.i.c.a(this.f3420b.b("periodicReviewEnabled", (Boolean) false).booleanValue(), this.f3420b.a("periodicReviewInterval", (Integer) 0).intValue(), this.f3420b.b("periodicReviewType", ""));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean */
    public final boolean c() {
        if (this.f3420b.b("disableHelpshiftBranding", (Boolean) false).booleanValue() || this.f3420b.b("disableHelpshiftBrandingAgent", (Boolean) false).booleanValue()) {
            return true;
        }
        return false;
    }

    public final boolean d() {
        return a("showConversationResolutionQuestionAgent") || a("showConversationResolutionQuestion");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.e.aa.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.common.e.aa.a(java.lang.String, java.io.Serializable):void
      com.helpshift.common.e.aa.a(java.lang.String, java.lang.Boolean):void
      com.helpshift.common.e.aa.a(java.lang.String, java.lang.Float):void
      com.helpshift.common.e.aa.a(java.lang.String, java.lang.Long):void
      com.helpshift.common.e.aa.a(java.lang.String, java.lang.String):void
      com.helpshift.common.e.aa.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    public final a.C0137a e() {
        return a.C0137a.a(this.f3420b.a("enableContactUs", (Integer) 0).intValue());
    }

    public final void a(boolean z) {
        this.f3420b.a("app_reviewed", Boolean.valueOf(z));
    }

    public final void d(String str) {
        this.f3420b.a("sdkLanguage", str);
    }

    public final boolean f() {
        return a("enableTypingIndicatorAgent") || a("enableTypingIndicator");
    }

    public final boolean g() {
        return a("fullPrivacy") || ((!a("requireNameAndEmail") || !a("hideNameAndEmail")) && !a("profileFormEnable"));
    }

    public final int h() {
        return this.e.y();
    }

    public static void a(Map<String, Serializable> map) {
        Iterator<Map.Entry<String, Serializable>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            if (it.next().getValue() == null) {
                it.remove();
            }
        }
    }

    public final boolean i() {
        if (!a("showConversationHistoryAgent") || !a("conversationalIssueFiling")) {
            return false;
        }
        return !a("fullPrivacy");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
     arg types: [java.lang.String, long]
     candidates:
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long */
    public final long j() {
        return Math.max(this.f3420b.b("periodicFetchInterval", (Long) 0L).longValue(), c.longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean */
    public final boolean k() {
        return this.f3420b.b("autoFillFirstPreIssueMessage", (Boolean) false).booleanValue();
    }
}
