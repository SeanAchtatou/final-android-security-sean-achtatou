package com.helpshift;

/* compiled from: HelpshiftUser */
public class k {

    /* renamed from: a  reason: collision with root package name */
    public String f3727a;

    /* renamed from: b  reason: collision with root package name */
    public String f3728b;
    public String c;
    public String d;

    /* synthetic */ k(a aVar, byte b2) {
        this(aVar);
    }

    private k(a aVar) {
        this.f3727a = aVar.f3729a;
        this.f3728b = aVar.f3730b;
        this.c = aVar.c;
        this.d = aVar.d;
    }

    public final String a() {
        return this.c;
    }

    /* compiled from: HelpshiftUser */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        String f3729a = null;

        /* renamed from: b  reason: collision with root package name */
        String f3730b = null;
        public String c;
        public String d;

        /* JADX WARNING: Removed duplicated region for block: B:11:0x002e A[ADDED_TO_REGION] */
        /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public a(java.lang.String r5, java.lang.String r6) {
            /*
                r4 = this;
                r4.<init>()
                r0 = 0
                r4.f3729a = r0
                r4.f3730b = r0
                r0 = 1
                r1 = 0
                if (r5 == 0) goto L_0x002b
                int r2 = r5.length()
                if (r2 != 0) goto L_0x0013
                goto L_0x002b
            L_0x0013:
                java.lang.String r2 = r5.trim()
                int r3 = r5.length()
                int r2 = r2.length()
                if (r3 == r2) goto L_0x0023
            L_0x0021:
                r2 = 0
                goto L_0x002c
            L_0x0023:
                int r2 = r5.length()
                r3 = 750(0x2ee, float:1.051E-42)
                if (r2 > r3) goto L_0x0021
            L_0x002b:
                r2 = 1
            L_0x002c:
                if (r2 == 0) goto L_0x0062
                if (r6 == 0) goto L_0x005c
                int r2 = r6.length()
                if (r2 != 0) goto L_0x0037
                goto L_0x005c
            L_0x0037:
                java.lang.String r0 = r6.trim()
                int r2 = r6.length()
                int r0 = r0.length()
                if (r2 == r0) goto L_0x0047
            L_0x0045:
                r0 = 0
                goto L_0x005c
            L_0x0047:
                int r0 = r6.length()
                r2 = 256(0x100, float:3.59E-43)
                if (r0 <= r2) goto L_0x0050
                goto L_0x0045
            L_0x0050:
                java.util.regex.Pattern r0 = com.helpshift.util.o.c()
                java.util.regex.Matcher r0 = r0.matcher(r6)
                boolean r0 = r0.matches()
            L_0x005c:
                if (r0 == 0) goto L_0x0062
                r4.f3729a = r5
                r4.f3730b = r6
            L_0x0062:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.helpshift.k.a.<init>(java.lang.String, java.lang.String):void");
        }

        public final a a(String str) {
            this.c = str;
            return this;
        }

        public final k a() {
            return new k(this, (byte) 0);
        }
    }
}
