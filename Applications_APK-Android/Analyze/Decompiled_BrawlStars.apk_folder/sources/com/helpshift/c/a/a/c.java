package com.helpshift.c.a.a;

import com.helpshift.c.a.a.a.e;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: DownloadManager */
public class c implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3227a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f3228b;

    public c(b bVar, a aVar) {
        this.f3228b = bVar;
        this.f3227a = aVar;
    }

    public final void a(boolean z, String str, Object obj) {
        if (z && this.f3227a.f3211b) {
            this.f3228b.e.a(str, obj.toString());
        }
        ConcurrentLinkedQueue concurrentLinkedQueue = this.f3228b.f3218a.get(str);
        if (concurrentLinkedQueue != null) {
            Iterator it = concurrentLinkedQueue.iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                if (eVar != null) {
                    eVar.a(z, str, obj);
                }
            }
            this.f3228b.f3218a.remove(str);
            this.f3228b.f3219b.remove(str);
        }
    }
}
