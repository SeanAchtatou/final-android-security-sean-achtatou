package com.helpshift.c.a.a;

import com.helpshift.c.a.a.a.f;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: DownloadManager */
public class d implements f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3231a;

    public d(b bVar) {
        this.f3231a = bVar;
    }

    public final void a(String str, int i) {
        ConcurrentLinkedQueue concurrentLinkedQueue = this.f3231a.f3219b.get(str);
        if (concurrentLinkedQueue != null) {
            Iterator it = concurrentLinkedQueue.iterator();
            while (it.hasNext()) {
                f fVar = (f) it.next();
                if (fVar != null) {
                    fVar.a(str, i);
                }
            }
        }
    }
}
