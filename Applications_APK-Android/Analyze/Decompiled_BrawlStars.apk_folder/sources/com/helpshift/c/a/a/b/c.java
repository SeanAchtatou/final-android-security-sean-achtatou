package com.helpshift.c.a.a.b;

import android.content.Context;
import com.helpshift.c.a.a.a.b;
import com.helpshift.c.a.a.a.d;
import com.helpshift.c.a.a.a.e;
import com.helpshift.c.a.a.a.f;
import java.io.File;

/* compiled from: FilteredViewExternalStorageDownloadRunnable */
public class c extends g {

    /* renamed from: b  reason: collision with root package name */
    private Context f3223b;

    public final boolean e() {
        return false;
    }

    public c(Context context, b bVar, com.helpshift.c.a.a.c.b bVar2, d dVar, f fVar, e eVar) {
        super(bVar, bVar2, dVar, fVar, eVar);
        this.f3223b = context;
    }

    public final File d() {
        return this.f3223b.getExternalFilesDir(null);
    }
}
