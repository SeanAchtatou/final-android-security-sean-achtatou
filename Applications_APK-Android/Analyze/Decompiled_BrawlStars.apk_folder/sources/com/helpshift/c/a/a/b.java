package com.helpshift.c.a.a;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import com.helpshift.c.a.a.a.c;
import com.helpshift.c.a.a.a.e;
import com.helpshift.c.a.a.a.f;
import com.helpshift.util.n;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadPoolExecutor;

/* compiled from: DownloadManager */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public ConcurrentHashMap<String, ConcurrentLinkedQueue<e>> f3218a = new ConcurrentHashMap<>();

    /* renamed from: b  reason: collision with root package name */
    public ConcurrentHashMap<String, ConcurrentLinkedQueue<f>> f3219b = new ConcurrentHashMap<>();
    public ThreadPoolExecutor c;
    public c d;
    public com.helpshift.c.a.a.c.c e;
    public Context f;

    public b(Context context, c cVar, ThreadPoolExecutor threadPoolExecutor) {
        this.f = context;
        this.d = cVar;
        this.c = threadPoolExecutor;
        this.e = new com.helpshift.c.a.a.c.c(cVar);
    }

    public boolean a() {
        try {
            if (this.f.getPackageManager().checkPermission("android.permission.WRITE_EXTERNAL_STORAGE", this.f.getPackageName()) == 0) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            n.a("Helpshift_DownloadMngr", "Error checking for permission : " + "android.permission.WRITE_EXTERNAL_STORAGE", e2);
            return false;
        }
    }

    public static boolean b() {
        if (Build.VERSION.SDK_INT >= 29) {
            return !Environment.isExternalStorageLegacy();
        }
        return false;
    }
}
