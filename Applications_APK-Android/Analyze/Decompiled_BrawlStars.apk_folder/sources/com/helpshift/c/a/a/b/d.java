package com.helpshift.c.a.a.b;

import android.content.Context;
import com.helpshift.c.a.a.a.b;
import com.helpshift.c.a.a.a.e;
import com.helpshift.c.a.a.a.f;
import java.io.File;

/* compiled from: InternalStorageDownloadRunnable */
public class d extends g {

    /* renamed from: b  reason: collision with root package name */
    private Context f3224b;

    public final boolean e() {
        return true;
    }

    public d(Context context, b bVar, com.helpshift.c.a.a.c.b bVar2, com.helpshift.c.a.a.a.d dVar, f fVar, e eVar) {
        super(bVar, bVar2, dVar, fVar, eVar);
        this.f3224b = context;
    }

    public final File d() {
        return this.f3224b.getFilesDir();
    }
}
