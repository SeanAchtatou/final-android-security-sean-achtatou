package com.helpshift.c.a.a;

import com.helpshift.util.n;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: HelpshiftSSLSocketFactory */
public class f extends SSLSocketFactory {

    /* renamed from: a  reason: collision with root package name */
    private SSLSocketFactory f3233a;

    /* renamed from: b  reason: collision with root package name */
    private List<String> f3234b;
    private List<String> c;
    private List<Socket> d = new ArrayList();

    public f(SSLSocketFactory sSLSocketFactory, List<String> list, List<String> list2) {
        this.f3233a = sSLSocketFactory;
        this.f3234b = list;
        this.c = list2;
    }

    public String[] getDefaultCipherSuites() {
        return this.f3233a.getDefaultCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        return this.f3233a.getSupportedCipherSuites();
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return a(this.f3233a.createSocket(socket, str, i, z));
    }

    public Socket createSocket() throws IOException {
        return a(this.f3233a.createSocket());
    }

    public Socket createSocket(String str, int i) throws IOException {
        return a(this.f3233a.createSocket(str, i));
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException {
        return a(this.f3233a.createSocket(str, i, inetAddress, i2));
    }

    public Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        return a(this.f3233a.createSocket(inetAddress, i));
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        return a(this.f3233a.createSocket(inetAddress, i, inetAddress2, i2));
    }

    private Socket a(Socket socket) {
        this.d.add(socket);
        if (socket != null && (socket instanceof SSLSocket)) {
            SSLSocket sSLSocket = (SSLSocket) socket;
            String[] enabledProtocols = sSLSocket.getEnabledProtocols();
            if (enabledProtocols == null) {
                return sSLSocket;
            }
            ArrayList arrayList = new ArrayList(Arrays.asList(enabledProtocols));
            String[] supportedProtocols = sSLSocket.getSupportedProtocols();
            List arrayList2 = new ArrayList();
            if (supportedProtocols != null) {
                arrayList2 = Arrays.asList(supportedProtocols);
            }
            ArrayList arrayList3 = new ArrayList();
            List<String> list = this.f3234b;
            if (list != null && list.size() > 0) {
                for (String next : this.f3234b) {
                    if (!arrayList.contains(next) && arrayList2.contains(next)) {
                        arrayList3.add(next);
                    }
                }
            }
            arrayList.addAll(arrayList3);
            List<String> list2 = this.c;
            if (list2 != null && list2.size() > 0) {
                arrayList.removeAll(this.c);
            }
            sSLSocket.setEnabledProtocols((String[]) arrayList.toArray(new String[0]));
        }
        return socket;
    }

    public final void a() {
        try {
            for (Socket next : this.d) {
                if (next != null) {
                    next.close();
                }
            }
        } catch (Exception e) {
            n.b("hs_ssl_factory", "Exception on closing open sockets: " + e);
        } catch (Throwable th) {
            this.d.clear();
            throw th;
        }
        this.d.clear();
    }
}
