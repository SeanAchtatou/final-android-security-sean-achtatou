package com.helpshift.c.a.a.b;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import com.helpshift.c.a.a.a.d;
import com.helpshift.c.a.a.a.e;
import com.helpshift.c.a.a.a.f;
import java.io.File;

/* compiled from: ExternalStorageDownloadRunnable */
public class b extends g {

    /* renamed from: b  reason: collision with root package name */
    private Context f3222b;
    private String c;
    private boolean d;

    public b(Context context, com.helpshift.c.a.a.a.b bVar, String str, boolean z, com.helpshift.c.a.a.c.b bVar2, d dVar, f fVar, e eVar) {
        super(bVar, bVar2, dVar, fVar, eVar);
        this.f3222b = context;
        this.c = str;
        this.d = z;
    }

    public final File d() {
        if (TextUtils.isEmpty(this.c)) {
            return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        }
        return Environment.getExternalStoragePublicDirectory(this.c);
    }

    public final boolean e() {
        if (this.c != null) {
            return this.d;
        }
        return false;
    }
}
