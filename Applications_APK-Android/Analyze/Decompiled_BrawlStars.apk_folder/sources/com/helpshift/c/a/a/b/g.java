package com.helpshift.c.a.a.b;

import android.text.TextUtils;
import com.helpshift.c.a.a.a.d;
import com.helpshift.c.a.a.a.e;
import com.helpshift.c.a.a.a.f;
import com.helpshift.c.a.a.c.b;
import com.helpshift.util.n;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: StorageDownloadRunnable */
public abstract class g extends a {

    /* renamed from: b  reason: collision with root package name */
    private b f3226b;

    /* access modifiers changed from: protected */
    public final boolean c() {
        return false;
    }

    public abstract File d();

    public abstract boolean e();

    g(com.helpshift.c.a.a.a.b bVar, b bVar2, d dVar, f fVar, e eVar) {
        super(bVar, dVar, fVar, eVar);
        this.f3226b = bVar2;
    }

    /* access modifiers changed from: protected */
    public final long a() {
        File f = f();
        if (f != null) {
            return f.length();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        File f = f();
        this.f3226b.c();
        if (f != null) {
            try {
                f.delete();
            } catch (Exception e) {
                n.c("Helpshift_InterDownRun", "Exception in deleting file ", e);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, java.lang.String]
     candidates:
      com.helpshift.c.a.a.b.g.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public final void a(InputStream inputStream, int i) throws IOException {
        FileOutputStream fileOutputStream;
        long a2 = a();
        File f = f();
        if (f == null) {
            File d = d();
            if (!d.exists()) {
                d.mkdirs();
            }
            if (e()) {
                a(d);
            }
            File file = new File(d, "Support_" + System.currentTimeMillis() + this.f3220a.f3216a.substring(this.f3220a.f3216a.lastIndexOf("/") + 1));
            this.f3226b.c(file.getAbsolutePath());
            f = file;
        }
        try {
            fileOutputStream = new FileOutputStream(f, true);
            try {
                byte[] bArr = new byte[8192];
                long j = 0;
                while (true) {
                    int read = inputStream.read(bArr, 0, 8192);
                    if (read == -1) {
                        this.f3226b.c();
                        String absolutePath = f.getAbsolutePath();
                        n.a("Helpshift_InterDownRun", "Download finished : " + this.f3220a.f3216a);
                        a(true, (Object) absolutePath);
                        a(fileOutputStream);
                        return;
                    } else if (read >= 0) {
                        fileOutputStream.write(bArr, 0, read);
                        long length = (long) ((((float) f.length()) / ((float) (((long) i) + a2))) * 100.0f);
                        if (length != j) {
                            a((int) length);
                            j = length;
                        }
                    } else {
                        throw new EOFException();
                    }
                }
            } catch (Throwable th) {
                th = th;
                a(fileOutputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            a(fileOutputStream);
            throw th;
        }
    }

    private File f() {
        String b2 = this.f3226b.b();
        if (TextUtils.isEmpty(b2)) {
            return null;
        }
        File file = new File(b2);
        if (file.exists() && file.canWrite()) {
            return file;
        }
        this.f3226b.c();
        return null;
    }

    private static void a(File file) {
        try {
            File file2 = new File(file, ".nomedia");
            if (!file2.exists()) {
                file2.createNewFile();
            }
        } catch (IOException unused) {
        }
    }
}
