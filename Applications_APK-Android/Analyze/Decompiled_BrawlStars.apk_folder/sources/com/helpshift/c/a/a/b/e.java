package com.helpshift.c.a.a.b;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.helpshift.c.a.a.a.d;
import com.helpshift.c.a.a.a.f;
import com.helpshift.c.a.a.c.b;
import com.helpshift.c.a.a.g;
import com.helpshift.util.n;
import java.io.Closeable;
import java.io.EOFException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

/* compiled from: MediaStoreDownloadRunnable */
public class e extends a {

    /* renamed from: b  reason: collision with root package name */
    private Context f3225b;
    private b c;

    /* access modifiers changed from: protected */
    public final boolean c() {
        return false;
    }

    public e(Context context, com.helpshift.c.a.a.a.b bVar, b bVar2, d dVar, f fVar, com.helpshift.c.a.a.a.e eVar) {
        super(bVar, dVar, fVar, eVar);
        this.f3225b = context;
        this.c = bVar2;
    }

    /* access modifiers changed from: protected */
    public final long a() {
        Uri d = d();
        if (d == null) {
            return 0;
        }
        try {
            ParcelFileDescriptor openFileDescriptor = this.f3225b.getContentResolver().openFileDescriptor(d, "r");
            if (openFileDescriptor != null) {
                return openFileDescriptor.getStatSize();
            }
            return 0;
        } catch (Exception e) {
            n.c("Helpshift_mediaRun", "Exception while getting file size via Uri", e);
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        Uri d = d();
        this.c.c();
        if (d != null) {
            try {
                this.f3225b.getContentResolver().delete(d, null, null);
            } catch (Exception e) {
                n.c("Helpshift_mediaRun", "Error when deleting a file via uri", e);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.helpshift.c.a.a.b.e.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, android.net.Uri]
     candidates:
      com.helpshift.c.a.a.b.e.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public final void a(InputStream inputStream, int i) throws IOException {
        FileOutputStream fileOutputStream;
        ParcelFileDescriptor parcelFileDescriptor;
        int i2;
        Uri uri;
        long a2 = a();
        Uri d = d();
        if (d == null) {
            String str = "Support_" + System.currentTimeMillis() + this.f3220a.f3216a.substring(this.f3220a.f3216a.lastIndexOf("/") + 1);
            String str2 = this.f3220a.c;
            if (Build.VERSION.SDK_INT < 29) {
                d = null;
            } else {
                ContentValues contentValues = new ContentValues();
                ContentResolver contentResolver = this.f3225b.getContentResolver();
                if (b(str2)) {
                    contentValues.put("_display_name", str);
                    contentValues.put("mime_type", str2);
                    contentValues.put("is_pending", (Integer) 1);
                    uri = MediaStore.Images.Media.getContentUri("external_primary");
                } else {
                    contentValues.put("_display_name", str);
                    contentValues.put("mime_type", str2);
                    contentValues.put("is_pending", (Integer) 1);
                    uri = MediaStore.Downloads.getContentUri("external_primary");
                }
                d = contentResolver.insert(uri, contentValues);
            }
        }
        int i3 = 0;
        if (d == null) {
            a(false, (Object) null);
            return;
        }
        this.c.c(d.toString());
        try {
            parcelFileDescriptor = this.f3225b.getContentResolver().openFileDescriptor(d, "w");
            if (parcelFileDescriptor == null) {
                try {
                    a(false, (Object) null);
                    a((Closeable) null);
                    g.a(parcelFileDescriptor);
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream = null;
                    a(fileOutputStream);
                    g.a(parcelFileDescriptor);
                    throw th;
                }
            } else {
                try {
                    fileOutputStream = new FileOutputStream(parcelFileDescriptor.getFileDescriptor());
                    i2 = 8192;
                } catch (Throwable th2) {
                    th = th2;
                    fileOutputStream = null;
                    a(fileOutputStream);
                    g.a(parcelFileDescriptor);
                    throw th;
                }
                try {
                    byte[] bArr = new byte[8192];
                    long j = 0;
                    InputStream inputStream2 = inputStream;
                    while (true) {
                        int read = inputStream2.read(bArr, i3, i2);
                        if (read == -1) {
                            String str3 = this.f3220a.c;
                            if (Build.VERSION.SDK_INT >= 29) {
                                ContentValues contentValues2 = new ContentValues();
                                if (b(str3)) {
                                    contentValues2.put("is_pending", (Integer) 0);
                                } else {
                                    contentValues2.put("is_pending", (Integer) 0);
                                }
                                this.f3225b.getContentResolver().update(d, contentValues2, null, null);
                            }
                            this.c.c();
                            n.a("Helpshift_mediaRun", "Download finished : " + this.f3220a.f3216a + "\n URI : " + d);
                            a(true, (Object) d);
                            a(fileOutputStream);
                            g.a(parcelFileDescriptor);
                            return;
                        } else if (read >= 0) {
                            fileOutputStream.write(bArr, i3, read);
                            byte[] bArr2 = bArr;
                            long statSize = (long) ((((float) parcelFileDescriptor.getStatSize()) / ((float) (((long) i) + a2))) * 100.0f);
                            if (statSize != j) {
                                a((int) statSize);
                                j = statSize;
                            }
                            bArr = bArr2;
                            i3 = 0;
                            i2 = 8192;
                        } else {
                            throw new EOFException();
                        }
                    }
                } catch (Throwable th3) {
                    th = th3;
                    a(fileOutputStream);
                    g.a(parcelFileDescriptor);
                    throw th;
                }
            }
        } catch (Throwable th4) {
            th = th4;
            parcelFileDescriptor = null;
            fileOutputStream = null;
            a(fileOutputStream);
            g.a(parcelFileDescriptor);
            throw th;
        }
    }

    private Uri d() {
        String b2 = this.c.b();
        if (TextUtils.isEmpty(b2)) {
            return null;
        }
        Uri a2 = a(b2);
        if (a2 != null) {
            return a2;
        }
        this.c.c();
        return null;
    }

    private Uri a(String str) {
        if (!g.a(this.f3225b, str)) {
            return null;
        }
        try {
            return Uri.parse(str);
        } catch (Exception e) {
            n.c("Helpshift_mediaRun", "Error while converting filePath to uri", e);
            return null;
        }
    }

    private static boolean b(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            return Pattern.compile("image/.*").matcher(str).matches();
        } catch (Exception e) {
            n.c("Helpshift_mediaRun", "Error when check image mime type", e);
            return false;
        }
    }
}
