package com.helpshift.c.a.a.b;

import com.helpshift.c.a.a.a.b;
import com.helpshift.c.a.a.a.d;
import com.helpshift.c.a.a.a.e;

/* compiled from: RawResponseDownloadRunnable */
public class f extends a {
    /* access modifiers changed from: protected */
    public final long a() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void b() {
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        return true;
    }

    public f(b bVar, d dVar, com.helpshift.c.a.a.a.f fVar, e eVar) {
        super(bVar, dVar, fVar, eVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, org.json.JSONObject]
     candidates:
      com.helpshift.c.a.a.b.f.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, org.json.JSONArray]
     candidates:
      com.helpshift.c.a.a.b.f.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, java.lang.StringBuilder]
     candidates:
      com.helpshift.c.a.a.b.f.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:0|(3:1|2|(1:4)(1:14))|7|8|9|10|11|17) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003c, code lost:
        a(true, (java.lang.Object) r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.io.InputStream r3, int r4) {
        /*
            r2 = this;
            java.io.InputStreamReader r4 = new java.io.InputStreamReader
            r4.<init>(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.io.BufferedReader r0 = new java.io.BufferedReader
            r0.<init>(r4)
        L_0x000f:
            java.lang.String r4 = r0.readLine()     // Catch:{ IOException -> 0x0019 }
            if (r4 == 0) goto L_0x0021
            r3.append(r4)     // Catch:{ IOException -> 0x0019 }
            goto L_0x000f
        L_0x0019:
            r4 = move-exception
            java.lang.String r0 = "Helpshift_RawDownRun"
            java.lang.String r1 = "IO Exception while reading response"
            com.helpshift.util.n.a(r0, r1, r4)
        L_0x0021:
            r4 = 1
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x002f }
            java.lang.String r1 = r3.toString()     // Catch:{ JSONException -> 0x002f }
            r0.<init>(r1)     // Catch:{ JSONException -> 0x002f }
            r2.a(r4, r0)     // Catch:{ JSONException -> 0x002f }
            goto L_0x003f
        L_0x002f:
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ JSONException -> 0x003c }
            java.lang.String r1 = r3.toString()     // Catch:{ JSONException -> 0x003c }
            r0.<init>(r1)     // Catch:{ JSONException -> 0x003c }
            r2.a(r4, r0)     // Catch:{ JSONException -> 0x003c }
            goto L_0x003f
        L_0x003c:
            r2.a(r4, r3)
        L_0x003f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.c.a.a.b.f.a(java.io.InputStream, int):void");
    }
}
