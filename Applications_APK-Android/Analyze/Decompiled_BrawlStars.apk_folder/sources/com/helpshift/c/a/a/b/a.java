package com.helpshift.c.a.a.b;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.share.internal.ShareConstants;
import com.helpshift.c.a.a.a.b;
import com.helpshift.c.a.a.a.d;
import com.helpshift.c.a.a.a.e;
import com.helpshift.c.a.a.a.f;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: BaseDownloadRunnable */
public abstract class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    protected b f3220a;

    /* renamed from: b  reason: collision with root package name */
    private d f3221b;
    private f c;
    private e d;

    /* access modifiers changed from: protected */
    public abstract long a() throws FileNotFoundException;

    /* access modifiers changed from: protected */
    public abstract void a(InputStream inputStream, int i) throws IOException;

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public abstract boolean c();

    a(b bVar, d dVar, f fVar, e eVar) {
        this.f3220a = bVar;
        this.f3221b = dVar;
        this.c = fVar;
        this.d = eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, java.io.IOException]
     candidates:
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, java.lang.Exception]
     candidates:
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, java.security.GeneralSecurityException]
     candidates:
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, java.net.MalformedURLException]
     candidates:
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void
     arg types: [int, java.lang.InterruptedException]
     candidates:
      com.helpshift.c.a.a.b.a.a(java.lang.CharSequence, java.lang.Iterable):java.lang.String
      com.helpshift.c.a.a.b.a.a(java.io.InputStream, int):void
      com.helpshift.c.a.a.b.a.a(boolean, java.lang.Object):void */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0103, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        a(false, (java.lang.Object) r6);
        com.helpshift.util.n.b("Helpshift_DownloadRun", "Exception in closing download response", r6, com.helpshift.p.b.d.a("route", r12.f3220a.f3216a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0126, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0157, code lost:
        if (r6 != null) goto L_0x0159;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x015d, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        a(false, (java.lang.Object) r6);
        com.helpshift.util.n.b("Helpshift_DownloadRun", "Exception in closing download response", r6, com.helpshift.p.b.d.a("route", r12.f3220a.f3216a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0170, code lost:
        r5.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0173, code lost:
        throw r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x017a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x017b, code lost:
        a(false, (java.lang.Object) r0);
        com.helpshift.util.n.b("Helpshift_DownloadRun", "Unknown Exception", r0, com.helpshift.p.b.d.a("route", r12.f3220a.f3216a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0190, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0191, code lost:
        a(false, (java.lang.Object) r0);
        com.helpshift.util.n.b("Helpshift_DownloadRun", "GeneralSecurityException", r0, com.helpshift.p.b.d.a("route", r12.f3220a.f3216a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01a6, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01a7, code lost:
        a(false, (java.lang.Object) r0);
        com.helpshift.util.n.b("Helpshift_DownloadRun", "Exception IO", r0, com.helpshift.p.b.d.a("route", r12.f3220a.f3216a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01bc, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01bd, code lost:
        a(false, (java.lang.Object) r0);
        com.helpshift.util.n.b("Helpshift_DownloadRun", "MalformedURLException", r0, com.helpshift.p.b.d.a("route", r12.f3220a.f3216a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01d2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01d3, code lost:
        a(false, (java.lang.Object) r0);
        com.helpshift.util.n.b("Helpshift_DownloadRun", "Exception Interrupted", r0, com.helpshift.p.b.d.a("route", r12.f3220a.f3216a));
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x017a A[ExcHandler: Exception (r0v6 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:1:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0190 A[ExcHandler: GeneralSecurityException (r0v5 'e' java.security.GeneralSecurityException A[CUSTOM_DECLARE]), Splitter:B:1:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01bc A[ExcHandler: MalformedURLException (r0v3 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:1:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01d2 A[ExcHandler: InterruptedException (r0v1 'e' java.lang.InterruptedException A[CUSTOM_DECLARE]), Splitter:B:1:0x0025] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            java.lang.String r0 = "Exception in closing download response"
            java.lang.String r1 = "route"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Starting download : "
            r2.append(r3)
            com.helpshift.c.a.a.a.b r3 = r12.f3220a
            java.lang.String r3 = r3.f3216a
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = "Helpshift_DownloadRun"
            com.helpshift.util.n.a(r3, r2)
            r2 = 10
            android.os.Process.setThreadPriority(r2)
            r2 = 1
            r4 = 0
            boolean r5 = java.lang.Thread.interrupted()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            if (r5 != 0) goto L_0x0174
            java.net.URL r5 = r12.d()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            java.lang.String r6 = "https"
            java.lang.String r7 = r5.getProtocol()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            boolean r6 = r6.equals(r7)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            if (r6 == 0) goto L_0x0071
            java.net.URLConnection r5 = r5.openConnection()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            javax.net.ssl.HttpsURLConnection r5 = (javax.net.ssl.HttpsURLConnection) r5     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r6 = r5
            javax.net.ssl.HttpsURLConnection r6 = (javax.net.ssl.HttpsURLConnection) r6     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r8 = 16
            if (r7 < r8) goto L_0x0077
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r8 = 19
            if (r7 > r8) goto L_0x0077
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r7.<init>()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            java.lang.String r8 = "TLSv1.2"
            r7.add(r8)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r8.<init>()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            java.lang.String r9 = "SSLv3"
            r8.add(r9)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            javax.net.ssl.SSLSocketFactory r9 = r6.getSSLSocketFactory()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.c.a.a.f r10 = new com.helpshift.c.a.a.f     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r10.<init>(r9, r7, r8)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r6.setSSLSocketFactory(r10)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            goto L_0x0077
        L_0x0071:
            java.net.URLConnection r5 = r5.openConnection()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
        L_0x0077:
            r5.setInstanceFollowRedirects(r2)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r6 = 0
            long r7 = r12.a()     // Catch:{ IOException -> 0x0128 }
            java.lang.String r9 = "Range"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0128 }
            r10.<init>()     // Catch:{ IOException -> 0x0128 }
            java.lang.String r11 = "bytes="
            r10.append(r11)     // Catch:{ IOException -> 0x0128 }
            r10.append(r7)     // Catch:{ IOException -> 0x0128 }
            java.lang.String r7 = "-"
            r10.append(r7)     // Catch:{ IOException -> 0x0128 }
            java.lang.String r7 = r10.toString()     // Catch:{ IOException -> 0x0128 }
            r5.setRequestProperty(r9, r7)     // Catch:{ IOException -> 0x0128 }
            int r7 = r5.getResponseCode()     // Catch:{ IOException -> 0x0128 }
            r8 = 416(0x1a0, float:5.83E-43)
            if (r7 == r8) goto L_0x011b
            java.io.InputStream r6 = r5.getInputStream()     // Catch:{ IOException -> 0x0128 }
            boolean r7 = r12.c()     // Catch:{ IOException -> 0x0128 }
            if (r7 == 0) goto L_0x00f3
            java.util.Map r7 = r5.getHeaderFields()     // Catch:{ IOException -> 0x0128 }
            java.util.Set r7 = r7.entrySet()     // Catch:{ IOException -> 0x0128 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ IOException -> 0x0128 }
        L_0x00b8:
            boolean r8 = r7.hasNext()     // Catch:{ IOException -> 0x0128 }
            if (r8 == 0) goto L_0x00f3
            java.lang.Object r8 = r7.next()     // Catch:{ IOException -> 0x0128 }
            java.util.Map$Entry r8 = (java.util.Map.Entry) r8     // Catch:{ IOException -> 0x0128 }
            java.lang.Object r9 = r8.getKey()     // Catch:{ IOException -> 0x0128 }
            if (r9 == 0) goto L_0x00b8
            java.lang.Object r9 = r8.getKey()     // Catch:{ IOException -> 0x0128 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ IOException -> 0x0128 }
            java.lang.String r10 = "Content-Encoding"
            boolean r9 = r9.equals(r10)     // Catch:{ IOException -> 0x0128 }
            if (r9 == 0) goto L_0x00b8
            java.lang.Object r8 = r8.getValue()     // Catch:{ IOException -> 0x0128 }
            java.util.List r8 = (java.util.List) r8     // Catch:{ IOException -> 0x0128 }
            java.lang.Object r8 = r8.get(r4)     // Catch:{ IOException -> 0x0128 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ IOException -> 0x0128 }
            java.lang.String r9 = "gzip"
            boolean r8 = r8.equalsIgnoreCase(r9)     // Catch:{ IOException -> 0x0128 }
            if (r8 == 0) goto L_0x00b8
            java.util.zip.GZIPInputStream r8 = new java.util.zip.GZIPInputStream     // Catch:{ IOException -> 0x0128 }
            r8.<init>(r6)     // Catch:{ IOException -> 0x0128 }
            r6 = r8
            goto L_0x00b8
        L_0x00f3:
            int r7 = r5.getContentLength()     // Catch:{ IOException -> 0x0128 }
            r12.a(r6, r7)     // Catch:{ IOException -> 0x0128 }
            java.lang.Thread.interrupted()     // Catch:{ IOException -> 0x0128 }
            if (r6 == 0) goto L_0x0116
            r6.close()     // Catch:{ IOException -> 0x0103, InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            goto L_0x0116
        L_0x0103:
            r6 = move-exception
            r12.a(r4, r6)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.p.b.a[] r7 = new com.helpshift.p.b.a[r2]     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.c.a.a.a.b r8 = r12.f3220a     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            java.lang.String r8 = r8.f3216a     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.p.b.a r8 = com.helpshift.p.b.d.a(r1, r8)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r7[r4] = r8     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.util.n.b(r3, r0, r6, r7)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
        L_0x0116:
            r5.disconnect()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            goto L_0x01ee
        L_0x011b:
            r12.b()     // Catch:{ IOException -> 0x0128 }
            java.io.IOException r7 = new java.io.IOException     // Catch:{ IOException -> 0x0128 }
            java.lang.String r8 = "Requested Range Not Satisfiable, failed with 416 status"
            r7.<init>(r8)     // Catch:{ IOException -> 0x0128 }
            throw r7     // Catch:{ IOException -> 0x0128 }
        L_0x0126:
            r7 = move-exception
            goto L_0x0157
        L_0x0128:
            r7 = move-exception
            r12.a(r4, r7)     // Catch:{ all -> 0x0126 }
            java.lang.String r8 = "Exception in download"
            com.helpshift.p.b.a[] r9 = new com.helpshift.p.b.a[r2]     // Catch:{ all -> 0x0126 }
            com.helpshift.c.a.a.a.b r10 = r12.f3220a     // Catch:{ all -> 0x0126 }
            java.lang.String r10 = r10.f3216a     // Catch:{ all -> 0x0126 }
            com.helpshift.p.b.a r10 = com.helpshift.p.b.d.a(r1, r10)     // Catch:{ all -> 0x0126 }
            r9[r4] = r10     // Catch:{ all -> 0x0126 }
            com.helpshift.util.n.b(r3, r8, r7, r9)     // Catch:{ all -> 0x0126 }
            if (r6 == 0) goto L_0x0116
            r6.close()     // Catch:{ IOException -> 0x0143, InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            goto L_0x0116
        L_0x0143:
            r6 = move-exception
            r12.a(r4, r6)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.p.b.a[] r7 = new com.helpshift.p.b.a[r2]     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.c.a.a.a.b r8 = r12.f3220a     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            java.lang.String r8 = r8.f3216a     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.p.b.a r8 = com.helpshift.p.b.d.a(r1, r8)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r7[r4] = r8     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.util.n.b(r3, r0, r6, r7)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            goto L_0x0116
        L_0x0157:
            if (r6 == 0) goto L_0x0170
            r6.close()     // Catch:{ IOException -> 0x015d, InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            goto L_0x0170
        L_0x015d:
            r6 = move-exception
            r12.a(r4, r6)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.p.b.a[] r8 = new com.helpshift.p.b.a[r2]     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.c.a.a.a.b r9 = r12.f3220a     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            java.lang.String r9 = r9.f3216a     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.p.b.a r9 = com.helpshift.p.b.d.a(r1, r9)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r8[r4] = r9     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            com.helpshift.util.n.b(r3, r0, r6, r8)     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
        L_0x0170:
            r5.disconnect()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            throw r7     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
        L_0x0174:
            java.lang.InterruptedException r0 = new java.lang.InterruptedException     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            r0.<init>()     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
            throw r0     // Catch:{ InterruptedException -> 0x01d2, MalformedURLException -> 0x01bc, IOException -> 0x01a6, GeneralSecurityException -> 0x0190, Exception -> 0x017a }
        L_0x017a:
            r0 = move-exception
            r12.a(r4, r0)
            com.helpshift.p.b.a[] r2 = new com.helpshift.p.b.a[r2]
            com.helpshift.c.a.a.a.b r5 = r12.f3220a
            java.lang.String r5 = r5.f3216a
            com.helpshift.p.b.a r1 = com.helpshift.p.b.d.a(r1, r5)
            r2[r4] = r1
            java.lang.String r1 = "Unknown Exception"
            com.helpshift.util.n.b(r3, r1, r0, r2)
            goto L_0x01ee
        L_0x0190:
            r0 = move-exception
            r12.a(r4, r0)
            com.helpshift.p.b.a[] r2 = new com.helpshift.p.b.a[r2]
            com.helpshift.c.a.a.a.b r5 = r12.f3220a
            java.lang.String r5 = r5.f3216a
            com.helpshift.p.b.a r1 = com.helpshift.p.b.d.a(r1, r5)
            r2[r4] = r1
            java.lang.String r1 = "GeneralSecurityException"
            com.helpshift.util.n.b(r3, r1, r0, r2)
            goto L_0x01ee
        L_0x01a6:
            r0 = move-exception
            r12.a(r4, r0)
            com.helpshift.p.b.a[] r2 = new com.helpshift.p.b.a[r2]
            com.helpshift.c.a.a.a.b r5 = r12.f3220a
            java.lang.String r5 = r5.f3216a
            com.helpshift.p.b.a r1 = com.helpshift.p.b.d.a(r1, r5)
            r2[r4] = r1
            java.lang.String r1 = "Exception IO"
            com.helpshift.util.n.b(r3, r1, r0, r2)
            goto L_0x01ee
        L_0x01bc:
            r0 = move-exception
            r12.a(r4, r0)
            com.helpshift.p.b.a[] r2 = new com.helpshift.p.b.a[r2]
            com.helpshift.c.a.a.a.b r5 = r12.f3220a
            java.lang.String r5 = r5.f3216a
            com.helpshift.p.b.a r1 = com.helpshift.p.b.d.a(r1, r5)
            r2[r4] = r1
            java.lang.String r1 = "MalformedURLException"
            com.helpshift.util.n.b(r3, r1, r0, r2)
            goto L_0x01ee
        L_0x01d2:
            r0 = move-exception
            r12.a(r4, r0)
            com.helpshift.p.b.a[] r2 = new com.helpshift.p.b.a[r2]
            com.helpshift.c.a.a.a.b r5 = r12.f3220a
            java.lang.String r5 = r5.f3216a
            com.helpshift.p.b.a r1 = com.helpshift.p.b.d.a(r1, r5)
            r2[r4] = r1
            java.lang.String r1 = "Exception Interrupted"
            com.helpshift.util.n.b(r3, r1, r0, r2)
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
        L_0x01ee:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.c.a.a.b.a.run():void");
    }

    private URL d() throws MalformedURLException, URISyntaxException, GeneralSecurityException {
        URI uri;
        if (this.f3220a.f3217b) {
            URI uri2 = new URI(this.f3220a.f3216a);
            String path = uri2.getPath();
            Map<String, String> a2 = a(uri2.getQuery());
            a2.put("v", AppEventsConstants.EVENT_PARAM_VALUE_YES);
            a2.put(ShareConstants.MEDIA_URI, path);
            Map<String, String> a3 = this.f3221b.a(a2);
            ArrayList arrayList = new ArrayList();
            for (Map.Entry next : a3.entrySet()) {
                arrayList.add(((String) next.getKey()) + "=" + ((String) next.getValue()));
            }
            uri = new URI(uri2.getScheme(), uri2.getAuthority(), uri2.getPath(), a("&", arrayList), null);
        } else {
            uri = new URI(this.f3220a.f3216a);
        }
        return new URL(uri.toASCIIString());
    }

    private static Map<String, String> a(String str) {
        String[] split = str.split("&");
        HashMap hashMap = new HashMap();
        for (String split2 : split) {
            String[] split3 = split2.split("=");
            if (split3.length == 2) {
                hashMap.put(split3[0], split3[1]);
            }
        }
        return hashMap;
    }

    private static String a(CharSequence charSequence, Iterable iterable) {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Object next : iterable) {
            if (z) {
                z = false;
            } else {
                sb.append(charSequence);
            }
            sb.append(next);
        }
        return sb.toString();
    }

    static void a(Closeable closeable) throws IOException {
        if (closeable != null) {
            closeable.close();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        f fVar = this.c;
        if (fVar != null) {
            fVar.a(this.f3220a.f3216a, i);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z, Object obj) {
        e eVar = this.d;
        if (eVar != null) {
            eVar.a(z, this.f3220a.f3216a, obj);
        }
    }
}
