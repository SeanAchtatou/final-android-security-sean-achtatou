package com.helpshift.c.a.a.c;

import com.helpshift.c.a.a.a.c;
import java.util.HashMap;

/* compiled from: BaseCacheDbStorage */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private c f3229a;

    /* access modifiers changed from: package-private */
    public abstract String a();

    public a(c cVar) {
        this.f3229a = cVar;
    }

    public final String a(String str) {
        HashMap hashMap = (HashMap) this.f3229a.a(a());
        if (hashMap == null) {
            return null;
        }
        return (String) hashMap.get(str);
    }

    public final void a(String str, String str2) {
        HashMap hashMap = (HashMap) this.f3229a.a(a());
        if (hashMap == null) {
            hashMap = new HashMap();
        }
        hashMap.put(str, str2);
        this.f3229a.a(a(), hashMap);
    }

    public final void b(String str) {
        HashMap hashMap = (HashMap) this.f3229a.a(a());
        if (hashMap != null) {
            hashMap.remove(str);
            this.f3229a.a(a(), hashMap);
        }
    }
}
