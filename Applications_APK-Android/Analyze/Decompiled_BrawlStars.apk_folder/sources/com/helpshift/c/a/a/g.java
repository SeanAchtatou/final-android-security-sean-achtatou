package com.helpshift.c.a.a;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import java.io.IOException;

/* compiled from: HsUriUtils */
public class g {
    public static boolean a(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        return str.startsWith("content://");
    }

    public static boolean a(Context context, String str) {
        boolean z = false;
        if (!a(str)) {
            return false;
        }
        ParcelFileDescriptor parcelFileDescriptor = null;
        try {
            parcelFileDescriptor = context.getContentResolver().openFileDescriptor(Uri.parse(str), "r");
            if (parcelFileDescriptor != null) {
                z = true;
            }
        } catch (Exception unused) {
        } finally {
            a(parcelFileDescriptor);
        }
        return z;
    }

    public static void a(ParcelFileDescriptor parcelFileDescriptor) {
        if (parcelFileDescriptor != null) {
            try {
                parcelFileDescriptor.close();
            } catch (IOException unused) {
            }
        }
    }
}
