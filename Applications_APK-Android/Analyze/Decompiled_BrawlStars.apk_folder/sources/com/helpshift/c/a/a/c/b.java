package com.helpshift.c.a.a.c;

import com.helpshift.c.a.a.a.c;

/* compiled from: DownloadInProgressCacheDbStorage */
public class b extends a {

    /* renamed from: a  reason: collision with root package name */
    private com.helpshift.c.a.a.a.b f3230a;

    /* access modifiers changed from: package-private */
    public final String a() {
        return "kDownloadManagerCachedFiles";
    }

    public b(com.helpshift.c.a.a.a.b bVar, c cVar) {
        super(cVar);
        this.f3230a = bVar;
    }

    public final String b() {
        return a(this.f3230a.f3216a);
    }

    public final void c(String str) {
        a(this.f3230a.f3216a, str);
    }

    public final void c() {
        b(this.f3230a.f3216a);
    }
}
