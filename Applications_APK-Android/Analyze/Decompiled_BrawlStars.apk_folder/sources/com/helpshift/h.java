package com.helpshift;

import android.content.Context;
import com.helpshift.util.n;

/* compiled from: CoreInternal */
final class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3409a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Context f3410b;

    h(String str, Context context) {
        this.f3409a = str;
        this.f3410b = context;
    }

    public final void run() {
        n.a("Helpshift_CoreInternal", "Registering push token : " + this.f3409a);
        CoreInternal.f3155a.a(this.f3410b, this.f3409a);
    }
}
