package com.helpshift;

import com.helpshift.a.b.c;
import com.helpshift.a.b.d;
import com.helpshift.a.b.e;
import com.helpshift.common.b;
import com.helpshift.common.c.j;
import com.helpshift.common.c.l;
import com.helpshift.common.c.t;
import com.helpshift.common.e.ab;
import com.helpshift.common.h;
import com.helpshift.common.k;
import com.helpshift.i.a.a;
import com.helpshift.j.a.v;
import com.helpshift.j.i.av;
import com.helpshift.j.i.aw;
import com.helpshift.j.i.bo;
import com.helpshift.l.b;
import com.helpshift.l.c;
import com.helpshift.util.aa;
import com.helpshift.util.n;
import java.util.HashMap;

/* compiled from: JavaCore */
public class m implements b {

    /* renamed from: a  reason: collision with root package name */
    final a f3754a;

    /* renamed from: b  reason: collision with root package name */
    final com.helpshift.b.a.a f3755b;
    final ab c;
    j d;
    e e;
    private final t f;
    private final com.helpshift.q.a g;
    private boolean h = false;

    public m(ab abVar) {
        this.c = abVar;
        this.d = new j(abVar);
        this.e = this.d.h;
        this.f = this.d.f3284a;
        this.f3754a = this.d.f3285b;
        this.f3755b = this.d.c;
        this.g = this.d.d;
    }

    public final j a() {
        return this.d;
    }

    public final aw a(av avVar) {
        ab abVar = this.c;
        j jVar = this.d;
        return new aw(abVar, jVar, jVar.i.a(), avVar);
    }

    public final bo a(v vVar) {
        return new bo(this.d, vVar);
    }

    public final com.helpshift.j.g.a a(com.helpshift.j.a.c.a aVar) {
        return new com.helpshift.j.g.a(this.c, this.d, this.e.k(), aVar);
    }

    public final boolean d() {
        return this.h;
    }

    public final void a(b bVar) {
        j jVar = this.d;
        if (bVar != null) {
            jVar.e.f3738b = bVar;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0114  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean a(com.helpshift.k r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            com.helpshift.a.b.d r0 = new com.helpshift.a.b.d     // Catch:{ all -> 0x011d }
            com.helpshift.common.c.j r1 = r7.d     // Catch:{ all -> 0x011d }
            com.helpshift.common.e.ab r2 = r7.c     // Catch:{ all -> 0x011d }
            r0.<init>(r7, r1, r2)     // Catch:{ all -> 0x011d }
            com.helpshift.b r1 = r0.f3180a     // Catch:{ all -> 0x011d }
            com.helpshift.a.b.e r1 = r1.o()     // Catch:{ all -> 0x011d }
            boolean r2 = com.helpshift.common.i.a(r8)     // Catch:{ all -> 0x011d }
            r3 = 1
            r4 = 0
            if (r2 != 0) goto L_0x001a
        L_0x0018:
            r2 = 0
            goto L_0x006e
        L_0x001a:
            com.helpshift.a.b.c r2 = r1.f     // Catch:{ all -> 0x011d }
            if (r2 != 0) goto L_0x0024
            com.helpshift.a.a.j r2 = r1.f3182a     // Catch:{ all -> 0x011d }
            com.helpshift.a.b.c r2 = r2.a()     // Catch:{ all -> 0x011d }
        L_0x0024:
            if (r2 != 0) goto L_0x0027
            goto L_0x0018
        L_0x0027:
            java.lang.String r5 = r8.f3727a     // Catch:{ all -> 0x011d }
            boolean r5 = com.helpshift.common.k.b(r5)     // Catch:{ all -> 0x011d }
            if (r5 == 0) goto L_0x0040
            java.lang.String r5 = r2.f3177b     // Catch:{ all -> 0x011d }
            boolean r5 = com.helpshift.common.k.b(r5)     // Catch:{ all -> 0x011d }
            if (r5 == 0) goto L_0x0018
            java.lang.String r5 = r8.f3728b     // Catch:{ all -> 0x011d }
            java.lang.String r2 = r2.c     // Catch:{ all -> 0x011d }
            boolean r2 = r5.equals(r2)     // Catch:{ all -> 0x011d }
            goto L_0x006e
        L_0x0040:
            java.lang.String r5 = r8.f3728b     // Catch:{ all -> 0x011d }
            boolean r5 = com.helpshift.common.k.b(r5)     // Catch:{ all -> 0x011d }
            if (r5 == 0) goto L_0x0059
            java.lang.String r5 = r2.c     // Catch:{ all -> 0x011d }
            boolean r5 = com.helpshift.common.k.b(r5)     // Catch:{ all -> 0x011d }
            if (r5 == 0) goto L_0x0018
            java.lang.String r5 = r8.f3727a     // Catch:{ all -> 0x011d }
            java.lang.String r2 = r2.f3177b     // Catch:{ all -> 0x011d }
            boolean r2 = r5.equals(r2)     // Catch:{ all -> 0x011d }
            goto L_0x006e
        L_0x0059:
            java.lang.String r5 = r8.f3727a     // Catch:{ all -> 0x011d }
            java.lang.String r6 = r2.f3177b     // Catch:{ all -> 0x011d }
            boolean r5 = r5.equals(r6)     // Catch:{ all -> 0x011d }
            if (r5 == 0) goto L_0x0018
            java.lang.String r5 = r8.f3728b     // Catch:{ all -> 0x011d }
            java.lang.String r2 = r2.c     // Catch:{ all -> 0x011d }
            boolean r2 = r5.equals(r2)     // Catch:{ all -> 0x011d }
            if (r2 == 0) goto L_0x0018
            r2 = 1
        L_0x006e:
            if (r2 == 0) goto L_0x00b1
            com.helpshift.a.b.c r2 = r1.a()     // Catch:{ all -> 0x011d }
            java.lang.String r5 = r2.i     // Catch:{ all -> 0x011d }
            if (r5 != 0) goto L_0x007d
            java.lang.String r6 = r8.d     // Catch:{ all -> 0x011d }
            if (r6 != 0) goto L_0x007d
            goto L_0x008d
        L_0x007d:
            if (r5 == 0) goto L_0x0087
            java.lang.String r4 = r8.d     // Catch:{ all -> 0x011d }
            boolean r4 = r5.equals(r4)     // Catch:{ all -> 0x011d }
            if (r4 != 0) goto L_0x008c
        L_0x0087:
            java.lang.String r4 = r8.d     // Catch:{ all -> 0x011d }
            r1.a(r2, r4)     // Catch:{ all -> 0x011d }
        L_0x008c:
            r4 = 1
        L_0x008d:
            java.lang.String r5 = r2.d     // Catch:{ all -> 0x011d }
            boolean r6 = com.helpshift.common.k.a(r5)     // Catch:{ all -> 0x011d }
            if (r6 == 0) goto L_0x009d
            java.lang.String r6 = r8.c     // Catch:{ all -> 0x011d }
            boolean r6 = com.helpshift.common.k.a(r6)     // Catch:{ all -> 0x011d }
            if (r6 != 0) goto L_0x010f
        L_0x009d:
            boolean r6 = com.helpshift.common.k.a(r5)     // Catch:{ all -> 0x011d }
            if (r6 != 0) goto L_0x00ab
            java.lang.String r6 = r8.c     // Catch:{ all -> 0x011d }
            boolean r5 = r5.equals(r6)     // Catch:{ all -> 0x011d }
            if (r5 != 0) goto L_0x010f
        L_0x00ab:
            java.lang.String r8 = r8.c     // Catch:{ all -> 0x011d }
            r1.b(r2, r8)     // Catch:{ all -> 0x011d }
            goto L_0x010f
        L_0x00b1:
            com.helpshift.b r2 = r0.f3180a     // Catch:{ all -> 0x011d }
            boolean r2 = r2.d()     // Catch:{ all -> 0x011d }
            if (r2 == 0) goto L_0x00c3
            java.lang.String r8 = "Helpshift_ULoginM"
            java.lang.String r0 = "Login should be called before starting a Helpshift session"
            r1 = 0
            com.helpshift.util.n.a(r8, r0, r1, r1)     // Catch:{ all -> 0x011d }
            r3 = 0
            goto L_0x011b
        L_0x00c3:
            r0.a()     // Catch:{ all -> 0x011d }
            r1.a(r8)     // Catch:{ all -> 0x011d }
            com.helpshift.a.a.j r8 = r1.f3182a     // Catch:{ all -> 0x011d }
            java.util.List r8 = r8.c()     // Catch:{ all -> 0x011d }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x011d }
            r1.<init>()     // Catch:{ all -> 0x011d }
            boolean r2 = com.helpshift.common.j.a(r8)     // Catch:{ all -> 0x011d }
            if (r2 == 0) goto L_0x00db
            goto L_0x00f7
        L_0x00db:
            java.util.Iterator r8 = r8.iterator()     // Catch:{ all -> 0x011d }
        L_0x00df:
            boolean r2 = r8.hasNext()     // Catch:{ all -> 0x011d }
            if (r2 == 0) goto L_0x00f7
            java.lang.Object r2 = r8.next()     // Catch:{ all -> 0x011d }
            com.helpshift.a.b.c r2 = (com.helpshift.a.b.c) r2     // Catch:{ all -> 0x011d }
            boolean r4 = r2.g     // Catch:{ all -> 0x011d }
            if (r4 != 0) goto L_0x00df
            boolean r4 = r2.f     // Catch:{ all -> 0x011d }
            if (r4 != 0) goto L_0x00df
            r1.add(r2)     // Catch:{ all -> 0x011d }
            goto L_0x00df
        L_0x00f7:
            java.util.Iterator r8 = r1.iterator()     // Catch:{ all -> 0x011d }
        L_0x00fb:
            boolean r1 = r8.hasNext()     // Catch:{ all -> 0x011d }
            if (r1 == 0) goto L_0x010b
            java.lang.Object r1 = r8.next()     // Catch:{ all -> 0x011d }
            com.helpshift.a.b.c r1 = (com.helpshift.a.b.c) r1     // Catch:{ all -> 0x011d }
            r0.a(r1)     // Catch:{ all -> 0x011d }
            goto L_0x00fb
        L_0x010b:
            r0.b()     // Catch:{ all -> 0x011d }
            r4 = 1
        L_0x010f:
            r0.d()     // Catch:{ all -> 0x011d }
            if (r4 == 0) goto L_0x011b
            com.helpshift.common.c.j r8 = r0.f3181b     // Catch:{ all -> 0x011d }
            com.helpshift.common.b r8 = r8.g     // Catch:{ all -> 0x011d }
            r8.b()     // Catch:{ all -> 0x011d }
        L_0x011b:
            monitor-exit(r7)
            return r3
        L_0x011d:
            r8 = move-exception
            monitor-exit(r7)
            goto L_0x0121
        L_0x0120:
            throw r8
        L_0x0121:
            goto L_0x0120
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.m.a(com.helpshift.k):boolean");
    }

    public final synchronized boolean e() {
        return new d(this, this.d, this.c).c();
    }

    public final void a(String str) {
        if (str != null && !str.equals(this.c.d().x())) {
            this.c.d().a(str);
            this.e.e();
            this.e.g();
        }
    }

    public final void f() {
        this.h = true;
        c cVar = this.d.e;
        if (cVar.f3738b != null) {
            cVar.f3737a.c(new com.helpshift.l.d(cVar));
        }
    }

    public final void g() {
        this.h = false;
        c cVar = this.d.e;
        if (cVar.f3738b != null) {
            cVar.f3737a.c(new com.helpshift.l.e(cVar));
        }
    }

    public final void h() {
        a(new n(this));
    }

    public final void a(com.helpshift.i.b.b bVar) {
        a aVar = this.f3754a;
        HashMap hashMap = new HashMap();
        hashMap.put("supportNotificationChannelId", bVar.p);
        hashMap.put("fontPath", bVar.l);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("enableInAppNotification", bVar.f3427a);
        hashMap2.put("defaultFallbackLanguageEnable", bVar.f3428b);
        hashMap2.put("inboxPollingEnable", bVar.c);
        hashMap2.put("notificationMute", bVar.d);
        hashMap2.put("disableAnimations", bVar.f);
        hashMap2.put("disableHelpshiftBranding", bVar.e);
        hashMap2.put("disableErrorLogging", bVar.g);
        hashMap2.put("disableAppLaunchEvent", bVar.h);
        hashMap2.put("notificationSoundId", bVar.k);
        hashMap2.put("notificationIconId", bVar.i);
        hashMap2.put("notificationLargeIconId", bVar.j);
        hashMap2.put("sdkType", bVar.m);
        hashMap2.put("pluginVersion", bVar.n);
        hashMap2.put("runtimeVersion", bVar.o);
        a.a(hashMap2);
        hashMap2.putAll(hashMap);
        aVar.f3420b.a(hashMap2);
    }

    public final void a(com.helpshift.i.b.a aVar) {
        a aVar2 = this.f3754a;
        HashMap hashMap = new HashMap();
        hashMap.put("conversationPrefillText", aVar.h);
        hashMap.put("initialUserMessageToAutoSendInPreissue", aVar.l);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("fullPrivacy", aVar.d);
        hashMap2.put("hideNameAndEmail", aVar.c);
        hashMap2.put("requireEmail", aVar.f3422b);
        hashMap2.put("showSearchOnNewConversation", aVar.e);
        hashMap2.put("gotoConversationAfterContactUs", aVar.f3421a);
        hashMap2.put("showConversationResolutionQuestion", aVar.f);
        hashMap2.put("showConversationInfoScreen", aVar.i);
        hashMap2.put("enableTypingIndicator", aVar.j);
        if (aVar.g != null) {
            hashMap2.put("enableContactUs", Integer.valueOf(aVar.g.e));
        }
        hashMap2.put("enableDefaultConversationalFiling", aVar.k);
        a.a(hashMap2);
        hashMap2.putAll(hashMap);
        aVar2.f3420b.a(hashMap2);
        if (aVar.d != null && aVar.d.booleanValue()) {
            d dVar = new d(this, this.d, this.c);
            if (dVar.f3180a.d()) {
                n.a("Helpshift_ULoginM", "clear PII should not be called after starting a Helpshift session", (Throwable) null, (com.helpshift.p.b.a[]) null);
                return;
            }
            e o = dVar.f3180a.o();
            com.helpshift.a.b.c a2 = o.a();
            if (!k.a(a2.f3177b)) {
                c.a aVar3 = new c.a(a2);
                aVar3.f3178a = null;
                aVar3.f3179b = null;
                com.helpshift.a.b.c a3 = aVar3.a();
                if (o.f3182a.b(a3)) {
                    o.a(a2, a3);
                }
                dVar.f3180a.u().a((String) null);
                dVar.f3180a.u().b((String) null);
            } else if (dVar.c()) {
                dVar.a(a2);
            }
        }
    }

    public final void j() {
        a(new o(this));
    }

    public final com.helpshift.b.a.a k() {
        return this.f3755b;
    }

    public final void l() {
        a(new p(this));
    }

    public final com.helpshift.l.c m() {
        return this.d.e;
    }

    public final e o() {
        return this.e;
    }

    public final com.helpshift.q.a p() {
        return this.g;
    }

    public final com.helpshift.h.a q() {
        return this.d.d();
    }

    public final a r() {
        return this.f3754a;
    }

    public final void a(h<aa<Integer, Boolean>, Object> hVar) {
        this.d.b(new q(this, hVar));
    }

    public final com.helpshift.n.b v() {
        return this.d.f();
    }

    public final com.helpshift.o.a.a w() {
        return this.d.f;
    }

    public final com.helpshift.common.c.a x() {
        return this.d.h();
    }

    public final com.helpshift.common.b y() {
        return this.d.g;
    }

    public final void z() {
        this.d.b(new s(this));
    }

    public final void A() {
        this.d.i.b();
    }

    public final com.helpshift.p.a B() {
        return this.d.i();
    }

    private void a(l lVar) {
        this.f.a(lVar).a();
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:19:0x006a */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:8:0x0020 */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v12 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.helpshift.j.i.b a(boolean r18, java.lang.Long r19, com.helpshift.j.a.q r20, boolean r21) {
        /*
            r17 = this;
            r0 = r17
            com.helpshift.common.c.j r1 = r0.d
            com.helpshift.j.c.l r1 = r1.i
            com.helpshift.j.c.a r1 = r1.a()
            r2 = 0
            if (r18 == 0) goto L_0x0055
            com.helpshift.j.a.x r3 = r1.j()
            if (r3 == 0) goto L_0x001f
            com.helpshift.j.a.x$a r4 = r3.a()
            com.helpshift.j.a.x$a r5 = com.helpshift.j.a.x.a.SINGLE
            if (r4 != r5) goto L_0x001f
            r1.e()
            goto L_0x0020
        L_0x001f:
            r2 = r3
        L_0x0020:
            if (r2 != 0) goto L_0x008d
            com.helpshift.j.e.c r2 = new com.helpshift.j.e.c
            com.helpshift.common.e.ab r4 = r1.d
            com.helpshift.a.b.c r5 = r1.e
            com.helpshift.j.e.e r6 = r1.u
            r7 = 100
            r3 = r2
            r3.<init>(r4, r5, r6, r7)
            com.helpshift.j.a.z r9 = new com.helpshift.j.a.z
            com.helpshift.common.e.ab r4 = r1.d
            com.helpshift.common.c.j r5 = r1.f
            com.helpshift.a.b.c r6 = r1.e
            com.helpshift.j.a.b r8 = r1.c
            r3 = r9
            r7 = r2
            r3.<init>(r4, r5, r6, r7, r8)
            r9.f()
            java.util.List r2 = r9.j()
            boolean r2 = com.helpshift.common.j.a(r2)
            if (r2 == 0) goto L_0x0053
            com.helpshift.j.a.b.a r2 = r1.l()
            r9.b(r2)
        L_0x0053:
            r7 = r9
            goto L_0x008e
        L_0x0055:
            r3 = r19
            com.helpshift.j.a.x r4 = r1.a(r3)
            if (r4 == 0) goto L_0x0069
            com.helpshift.j.a.x$a r5 = r4.a()
            com.helpshift.j.a.x$a r6 = com.helpshift.j.a.x.a.HISTORY
            if (r5 != r6) goto L_0x0069
            r1.e()
            goto L_0x006a
        L_0x0069:
            r2 = r4
        L_0x006a:
            if (r2 != 0) goto L_0x008d
            com.helpshift.j.e.g r7 = new com.helpshift.j.e.g
            com.helpshift.common.e.ab r11 = r1.d
            com.helpshift.a.b.c r12 = r1.e
            com.helpshift.j.e.e r14 = r1.u
            r15 = 100
            r10 = r7
            r13 = r19
            r10.<init>(r11, r12, r13, r14, r15)
            com.helpshift.j.a.aa r2 = new com.helpshift.j.a.aa
            com.helpshift.common.e.ab r4 = r1.d
            com.helpshift.common.c.j r5 = r1.f
            com.helpshift.a.b.c r6 = r1.e
            com.helpshift.j.a.b r8 = r1.c
            r3 = r2
            r3.<init>(r4, r5, r6, r7, r8)
            r2.f()
        L_0x008d:
            r7 = r2
        L_0x008e:
            com.helpshift.j.a.r r2 = r1.k
            r7.a(r2)
            r1.a(r7)
            com.helpshift.j.i.b r1 = new com.helpshift.j.i.b
            com.helpshift.common.e.ab r4 = r0.c
            com.helpshift.common.c.j r5 = r0.d
            com.helpshift.j.c.l r2 = r5.i
            com.helpshift.j.c.a r6 = r2.a()
            r3 = r1
            r8 = r20
            r9 = r18
            r10 = r21
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.m.a(boolean, java.lang.Long, com.helpshift.j.a.q, boolean):com.helpshift.j.i.b");
    }

    public final com.helpshift.j.a.b.a b() {
        return this.d.i.a().k();
    }

    public final com.helpshift.j.a.b.a c() {
        com.helpshift.j.c.a a2 = this.d.i.a();
        com.helpshift.j.a.b.a k = a2.k();
        return (k != null || !a2.j.a("conversationalIssueFiling")) ? k : a2.l();
    }

    public final void a(String str, String str2) {
        this.d.i.a().a(str);
        this.d.i.a().b(str2);
    }

    public final void i() {
        this.d.i.a().l.b(false);
    }

    public final void n() {
        this.d.f();
        e eVar = this.e;
        this.d.i.a();
        eVar.k();
        com.helpshift.common.b bVar = this.d.g;
        if (bVar.c.compareAndSet(false, true)) {
            bVar.d.add(b.a.MIGRATION);
            bVar.d.add(b.a.SYNC_USER);
            bVar.d.add(b.a.PUSH_TOKEN);
            bVar.d.add(b.a.CLEAR_USER);
            bVar.d.add(b.a.CONVERSATION);
            bVar.d.add(b.a.FAQ);
            bVar.d.add(b.a.ANALYTICS);
            bVar.f3244a.b(new com.helpshift.common.c(bVar));
        }
    }

    public final com.helpshift.j.a s() {
        return this.d.i.a().l;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.c.a.a(java.lang.Long, java.lang.String, int, java.lang.String, boolean):void
     arg types: [java.lang.Long, java.lang.String, int, java.lang.String, int]
     candidates:
      com.helpshift.j.c.a.a(java.util.List<com.helpshift.j.a.b.a>, java.util.List<com.helpshift.j.a.b.a>, java.util.Set<com.helpshift.j.a.b.a>, java.util.Set<com.helpshift.j.a.b.a>, java.util.Map<java.lang.Long, com.helpshift.j.a.p>):void
      com.helpshift.j.c.a.a(java.lang.Long, java.lang.String, int, java.lang.String, boolean):void */
    public final void a(String str, String str2, String str3) {
        com.helpshift.j.a.b.a aVar;
        String str4;
        int i;
        com.helpshift.j.c.a a2 = this.d.i.a();
        if ("issue".equals(str)) {
            aVar = a2.g.a(str2);
        } else if ("preissue".equals(str)) {
            aVar = a2.g.b(str2);
        } else {
            n.a("Helpshift_ConvInboxDM", "Cannot handle push for unknown issue type. " + str, (Throwable[]) null, (com.helpshift.p.b.a[]) null);
            return;
        }
        if (aVar != null) {
            if (k.a(str3)) {
                str3 = a2.d.d().f();
            }
            String str5 = str3;
            com.helpshift.j.b.d a3 = a2.h.a(aVar.e);
            if (a3 == null) {
                str4 = str5;
                i = 1;
            } else {
                str4 = a3.f3554b;
                i = a3.f3553a + 1;
            }
            a2.h.a(aVar.e, new com.helpshift.j.b.d(i, str4));
            if (i > 0 && a2.a(aVar)) {
                a2.a(aVar.f3504b, aVar.e, i, str5, false);
            }
            a2.i();
        }
    }

    public final int t() {
        return this.d.i.a().p();
    }

    public final com.helpshift.j.c.a u() {
        return this.d.i.a();
    }

    public final void C() {
        this.e.j();
        this.e.k().b();
    }
}
