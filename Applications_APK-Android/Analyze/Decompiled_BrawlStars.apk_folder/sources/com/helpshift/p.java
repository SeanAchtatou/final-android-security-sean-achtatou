package com.helpshift;

import com.helpshift.a.b.c;
import com.helpshift.b.a.a;
import com.helpshift.b.b;
import com.helpshift.common.c.l;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.UUID;

/* compiled from: JavaCore */
class p extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f3786a;

    p(m mVar) {
        this.f3786a = mVar;
    }

    public final void a() {
        a aVar = this.f3786a.f3755b;
        c a2 = this.f3786a.e.a();
        if (!aVar.f3202b.a("disableAppLaunchEvent")) {
            String uuid = UUID.randomUUID().toString();
            DecimalFormat decimalFormat = a.f3201a;
            double currentTimeMillis = (double) System.currentTimeMillis();
            Double.isNaN(currentTimeMillis);
            aVar.a(Collections.singletonList(new com.helpshift.b.b.a(uuid, b.APP_START, null, decimalFormat.format(currentTimeMillis / 1000.0d))), a2);
        }
    }
}
