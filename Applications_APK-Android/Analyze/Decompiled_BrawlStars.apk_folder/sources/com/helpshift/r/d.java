package com.helpshift.r;

import android.support.v4.app.NotificationCompat;
import com.helpshift.a.b.c;
import com.helpshift.common.c.b.g;
import com.helpshift.common.c.b.q;
import com.helpshift.common.c.b.s;
import com.helpshift.common.c.b.u;
import com.helpshift.common.c.b.v;
import com.helpshift.common.c.j;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.k;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/* compiled from: RemoteDataMigrator */
public class d {

    /* renamed from: a  reason: collision with root package name */
    j f3823a;

    /* renamed from: b  reason: collision with root package name */
    WeakReference<a> f3824b;
    private ab c;
    private c d;
    private b e;

    /* compiled from: RemoteDataMigrator */
    public interface a {
        void a(c cVar);
    }

    public d(ab abVar, j jVar, c cVar, a aVar) {
        this.c = abVar;
        this.f3823a = jVar;
        this.d = cVar;
        this.f3824b = new WeakReference<>(aVar);
        this.e = abVar.B();
    }

    public final c a() {
        if (k.a(this.d.f3177b)) {
            return c.COMPLETED;
        }
        com.helpshift.r.a.a b2 = this.e.b(this.d.f3177b);
        if (b2 == null) {
            return c.COMPLETED;
        }
        return b2.e;
    }

    public final void b() {
        c a2 = a();
        if (a2 != c.COMPLETED && a2 != c.IN_PROGRESS) {
            this.f3823a.b(new e(this));
        }
    }

    public void c() {
        com.helpshift.r.a.a b2;
        c a2 = a();
        if (a2 != c.COMPLETED && a2 != c.IN_PROGRESS && (b2 = this.e.b(this.d.f3177b)) != null) {
            c cVar = b2.e;
            if (cVar == c.NOT_STARTED || cVar == c.FAILED) {
                com.helpshift.common.c.b.j jVar = new com.helpshift.common.c.b.j(new g(new u(new v(new s(new q("/migrate-profile/", this.f3823a, this.c), this.c)))));
                HashMap hashMap = new HashMap();
                hashMap.put("profile-id", b2.d);
                hashMap.put("did", this.d.e);
                if (!k.a(this.d.f3177b)) {
                    hashMap.put("uid", this.d.f3177b);
                }
                if (!k.a(this.d.c)) {
                    hashMap.put(NotificationCompat.CATEGORY_EMAIL, this.d.c);
                }
                a(cVar, c.IN_PROGRESS);
                try {
                    jVar.a(new i(hashMap));
                    a(cVar, c.COMPLETED);
                } catch (RootAPIException e2) {
                    if (e2.c == b.USER_PRE_CONDITION_FAILED || e2.c == b.USER_NOT_FOUND) {
                        a(cVar, c.COMPLETED);
                    } else if (e2.c == b.NON_RETRIABLE) {
                        a(cVar, c.COMPLETED);
                    } else {
                        a(cVar, c.FAILED);
                        throw e2;
                    }
                }
            }
        }
    }

    public void a(c cVar, c cVar2) {
        if (cVar2 == c.COMPLETED) {
            this.e.a(this.d.f3177b);
        } else {
            this.e.a(this.d.f3177b, cVar2);
        }
        this.f3823a.a(new f(this, cVar, cVar2));
    }
}
