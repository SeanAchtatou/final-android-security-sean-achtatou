package com.helpshift;

import com.helpshift.a.b.c;
import com.helpshift.common.c.l;
import com.helpshift.i.c.b;
import com.helpshift.w.a;
import com.helpshift.w.h;

/* compiled from: JavaCore */
class n extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f3760a;

    n(m mVar) {
        this.f3760a = mVar;
    }

    public final void a() {
        c a2 = this.f3760a.e.a();
        b a3 = this.f3760a.f3754a.a(this.f3760a.e);
        if (a3 != null) {
            a aVar = new a(this.f3760a.c, this.f3760a.d);
            Long l = a3.r;
            Long l2 = a3.q;
            com.helpshift.j.c.a a4 = aVar.f4274a.i.a(a2);
            Long f = a4.g.f(a4.e.f3176a.longValue());
            boolean z = true;
            if (f != null && (l == null || l.longValue() > f.longValue())) {
                aVar.a(a2, h.USER);
            } else {
                Long k = a4.h.k(a4.e.f3176a.longValue());
                if (l2 == null || (k != null && k.longValue() >= l2.longValue())) {
                    z = false;
                }
                if (z) {
                    aVar.a(a2, h.CONVERSATION);
                }
            }
            if (l2 != null) {
                a4.h.a(a4.e.f3176a.longValue(), l2.longValue());
            }
        }
    }
}
