package com.helpshift.views;

import android.support.v4.view.MenuItemCompat;
import android.view.MenuItem;
import android.view.View;
import com.helpshift.util.n;
import com.helpshift.util.q;

/* compiled from: HSMenuItemCompat */
public class b {
    public static <T extends MenuItem.OnActionExpandListener & MenuItemCompat.OnActionExpandListener> void a(MenuItem menuItem, T t) {
        if (com.helpshift.util.b.a(q.a(), 26)) {
            try {
                menuItem.setOnActionExpandListener(t);
            } catch (UnsupportedOperationException e) {
                n.c("HSMenuItemCompat", "Exception thrown while calling MenuItem#setOnActionExpandListener: ", e);
                MenuItemCompat.setOnActionExpandListener(menuItem, (MenuItemCompat.OnActionExpandListener) t);
            }
        } else {
            MenuItemCompat.setOnActionExpandListener(menuItem, (MenuItemCompat.OnActionExpandListener) t);
        }
    }

    public static View a(MenuItem menuItem) {
        if (com.helpshift.util.b.a(q.a(), 26)) {
            return menuItem.getActionView();
        }
        return MenuItemCompat.getActionView(menuItem);
    }

    public static boolean b(MenuItem menuItem) {
        if (com.helpshift.util.b.a(q.a(), 26)) {
            return menuItem.isActionViewExpanded();
        }
        return MenuItemCompat.isActionViewExpanded(menuItem);
    }

    public static void c(MenuItem menuItem) {
        if (com.helpshift.util.b.a(q.a(), 26)) {
            menuItem.collapseActionView();
        } else {
            MenuItemCompat.collapseActionView(menuItem);
        }
    }

    public static void d(MenuItem menuItem) {
        if (com.helpshift.util.b.a(q.a(), 26)) {
            menuItem.expandActionView();
        } else {
            MenuItemCompat.expandActionView(menuItem);
        }
    }
}
