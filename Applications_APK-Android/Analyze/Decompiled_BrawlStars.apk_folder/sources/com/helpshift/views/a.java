package com.helpshift.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import com.helpshift.s.b;
import com.helpshift.util.n;
import java.lang.ref.WeakReference;

/* compiled from: FontApplier */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static Typeface f4263a = null;

    /* renamed from: b  reason: collision with root package name */
    private static e f4264b = null;
    private static boolean c = false;

    public static void a(TextView textView) {
        a(textView.getContext());
        Typeface typeface = f4263a;
        if (typeface != null) {
            textView.setTypeface(typeface);
        }
    }

    public static void a(Dialog dialog) {
        a(dialog.findViewById(16908290));
    }

    public static void a(View view) {
        a(view.getContext());
        if (f4263a != null) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new C0148a(view));
        }
    }

    static void b(View view) {
        if (view instanceof TextView) {
            a((TextView) view);
        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                b(viewGroup.getChildAt(i));
            }
        }
    }

    public static e a() {
        Typeface typeface = f4263a;
        if (typeface != null && f4264b == null) {
            f4264b = new e(typeface);
        }
        return f4264b;
    }

    private static void a(Context context) {
        String b2 = b();
        if (b2 != null && f4263a == null && !c) {
            try {
                f4263a = Typeface.createFromAsset(context.getAssets(), b2);
            } catch (Exception e) {
                n.b("HS_FontApplier", "Typeface initialisation failed. Using default typeface. " + e.getMessage());
            } catch (Throwable th) {
                c = true;
                throw th;
            }
            c = true;
        }
    }

    /* renamed from: com.helpshift.views.a$a  reason: collision with other inner class name */
    /* compiled from: FontApplier */
    static class C0148a implements ViewTreeObserver.OnGlobalLayoutListener {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<View> f4265a;

        public C0148a(View view) {
            this.f4265a = new WeakReference<>(view);
        }

        public void onGlobalLayout() {
            View view = this.f4265a.get();
            if (view != null) {
                a.b(view);
            }
        }
    }

    public static String b() {
        return b.a.f3833a.f3831a.l;
    }
}
