package com.helpshift.views.bottomsheet;

import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.view.ViewCompat;
import android.view.View;

/* compiled from: HSBottomSheet */
public class c extends BottomSheetBehavior.BottomSheetCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f4272a;

    public c(a aVar) {
        this.f4272a = aVar;
    }

    public void onStateChanged(View view, int i) {
        if (this.f4272a.f != null) {
            if (i == 3) {
                this.f4272a.f.setClickable(true);
            } else if (i == 4) {
                this.f4272a.f.setClickable(false);
            }
        }
        if (this.f4272a.g.size() > 0) {
            for (BottomSheetBehavior.BottomSheetCallback onStateChanged : this.f4272a.g) {
                onStateChanged.onStateChanged(view, i);
            }
        }
    }

    public void onSlide(View view, float f) {
        if (this.f4272a.h && this.f4272a.f != null) {
            float f2 = 0.0f;
            if (f > 0.0f) {
                f2 = f;
            }
            this.f4272a.f.setBackgroundColor(ColorUtils.blendARGB(0, ViewCompat.MEASURED_STATE_MASK, f2 * this.f4272a.i));
        }
        if (this.f4272a.g.size() > 0) {
            for (BottomSheetBehavior.BottomSheetCallback onSlide : this.f4272a.g) {
                onSlide.onSlide(view, f);
            }
        }
    }
}
