package com.helpshift.views.bottomsheet;

import android.content.Context;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class HSBottomSheetBehaviour<V extends View> extends BottomSheetBehavior<V> {

    /* renamed from: a  reason: collision with root package name */
    boolean f4266a = true;

    public HSBottomSheetBehaviour() {
    }

    public HSBottomSheetBehaviour(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (!this.f4266a) {
            return false;
        }
        return super.onInterceptTouchEvent(coordinatorLayout, v, motionEvent);
    }

    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i, int i2) {
        if (!this.f4266a) {
            return false;
        }
        return super.onStartNestedScroll(coordinatorLayout, v, view, view2, i, i2);
    }

    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, V v, View view, int i) {
        if (this.f4266a) {
            super.onStopNestedScroll(coordinatorLayout, v, view, i);
        }
    }

    public boolean onNestedPreFling(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2) {
        if (!this.f4266a) {
            return false;
        }
        return super.onNestedPreFling(coordinatorLayout, v, view, f, f2);
    }

    public void onNestedScroll(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int i3, int i4, int i5) {
        if (this.f4266a) {
            super.onNestedScroll(coordinatorLayout, v, view, i, i2, i3, i4, i5);
        }
    }

    public boolean onTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (!this.f4266a) {
            return false;
        }
        return super.onTouchEvent(coordinatorLayout, v, motionEvent);
    }
}
