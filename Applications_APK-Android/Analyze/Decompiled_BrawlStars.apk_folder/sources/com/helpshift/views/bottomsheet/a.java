package com.helpshift.views.bottomsheet;

import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import java.util.ArrayList;
import java.util.List;

/* compiled from: HSBottomSheet */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public final View f4267a;

    /* renamed from: b  reason: collision with root package name */
    public final View f4268b;
    public final View c;
    public final ViewGroup d;
    public final Window e;
    final View f;
    List<BottomSheetBehavior.BottomSheetCallback> g = new ArrayList();
    final boolean h;
    final float i;

    public a(View view, Window window, View view2, View view3, boolean z, float f2, View view4, ViewGroup viewGroup) {
        this.f4267a = view;
        this.e = window;
        this.f4268b = view2;
        this.f = view3;
        this.h = z;
        this.i = f2;
        this.c = view4;
        this.d = viewGroup;
    }

    public final void a() {
        if (ViewCompat.isAttachedToWindow(this.c)) {
            ((ViewGroup) this.c.getParent()).removeView(this.c);
        }
        View view = this.f;
        if (view != null && ViewCompat.isAttachedToWindow(view)) {
            ((ViewGroup) this.f.getParent()).removeView(this.f);
        }
    }

    public final void a(BottomSheetBehavior.BottomSheetCallback bottomSheetCallback) {
        this.g.add(bottomSheetCallback);
    }

    public final void b() {
        this.g.clear();
    }

    public final void c() {
        e();
        a(d());
    }

    private ViewGroup.LayoutParams d() {
        ViewGroup.LayoutParams layoutParams = this.f4267a.getLayoutParams();
        layoutParams.height = -1;
        layoutParams.width = this.f4268b.getWidth();
        return layoutParams;
    }

    private void a(ViewGroup.LayoutParams layoutParams) {
        this.e.addContentView(this.c, layoutParams);
    }

    private void e() {
        int i2;
        View findViewById;
        int[] iArr = new int[2];
        this.f4268b.getLocationInWindow(iArr);
        View decorView = this.e.getDecorView();
        if (decorView == null || (findViewById = decorView.findViewById(16908290)) == null) {
            i2 = 0;
        } else {
            int[] iArr2 = new int[2];
            findViewById.getLocationInWindow(iArr2);
            i2 = iArr2[0];
        }
        this.c.setX((float) Math.max(0, iArr[0] - i2));
    }

    /* renamed from: com.helpshift.views.bottomsheet.a$a  reason: collision with other inner class name */
    /* compiled from: HSBottomSheet */
    public static class C0149a {

        /* renamed from: a  reason: collision with root package name */
        public Window f4269a;

        /* renamed from: b  reason: collision with root package name */
        public int f4270b;
        public View c;
        public View d;
        public boolean e;
        public float f = 1.0f;

        public C0149a(Window window) {
            this.f4269a = window;
        }
    }

    public final void a(boolean z) {
        ((HSBottomSheetBehaviour) BottomSheetBehavior.from(this.d)).f4266a = z;
    }
}
