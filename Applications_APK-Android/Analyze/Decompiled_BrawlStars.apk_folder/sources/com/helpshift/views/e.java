package com.helpshift.views;

import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

/* compiled from: HSTypefaceSpan */
public class e extends MetricAffectingSpan {

    /* renamed from: a  reason: collision with root package name */
    private final Typeface f4273a;

    e(Typeface typeface) {
        this.f4273a = typeface;
    }

    public void updateMeasureState(TextPaint textPaint) {
        textPaint.setTypeface(this.f4273a);
    }

    public void updateDrawState(TextPaint textPaint) {
        textPaint.setTypeface(this.f4273a);
    }
}
