package com.helpshift.views;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.util.AttributeSet;

public class HSSearchView extends SearchView {
    public HSSearchView(Context context) {
        super(context);
        a.a(this);
    }

    public HSSearchView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a.a(this);
    }

    public HSSearchView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a.a(this);
    }
}
