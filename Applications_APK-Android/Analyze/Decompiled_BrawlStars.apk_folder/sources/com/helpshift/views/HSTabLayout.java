package com.helpshift.views;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;

public class HSTabLayout extends TabLayout {
    public HSTabLayout(Context context) {
        super(context);
        a.a(this);
    }

    public HSTabLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a.a(this);
    }

    public HSTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a.a(this);
    }
}
