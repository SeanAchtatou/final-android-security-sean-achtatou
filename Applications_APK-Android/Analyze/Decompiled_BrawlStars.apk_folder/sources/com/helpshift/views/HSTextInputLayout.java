package com.helpshift.views;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

public class HSTextInputLayout extends TextInputLayout {
    public HSTextInputLayout(Context context) {
        super(context);
        a.a(this);
    }

    public HSTextInputLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a.a(this);
    }

    public HSTextInputLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a.a(this);
    }
}
