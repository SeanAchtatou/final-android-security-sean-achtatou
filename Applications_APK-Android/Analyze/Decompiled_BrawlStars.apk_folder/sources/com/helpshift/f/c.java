package com.helpshift.f;

import android.app.Application;
import android.content.Context;
import com.helpshift.util.a.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: HSAppLifeCycleController */
public class c implements f {

    /* renamed from: b  reason: collision with root package name */
    public static final Object f3396b = new Object();
    private static c d = new c();

    /* renamed from: a  reason: collision with root package name */
    public List<f> f3397a = new ArrayList();
    public a c;

    private c() {
    }

    public static c a() {
        return d;
    }

    public final synchronized void a(Application application, boolean z) {
        if (this.c == null) {
            if (z) {
                this.c = new g(application);
            } else {
                this.c = new b(application);
            }
            this.c.f3393a = this;
        }
    }

    public final void a(Context context) {
        b.a.f4235a.a(new d(this, context));
    }

    public final void b(Context context) {
        b.a.f4235a.a(new e(this, context));
    }
}
