package com.helpshift.f;

import android.content.Context;

/* compiled from: HSAppLifeCycleController */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3398a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ c f3399b;

    d(c cVar, Context context) {
        this.f3399b = cVar;
        this.f3398a = context;
    }

    public void run() {
        synchronized (c.f3396b) {
            for (f a2 : this.f3399b.f3397a) {
                a2.a(this.f3398a);
            }
        }
    }
}
