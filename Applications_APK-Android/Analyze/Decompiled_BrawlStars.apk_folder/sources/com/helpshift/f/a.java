package com.helpshift.f;

import android.content.Context;

/* compiled from: BaseAppLifeCycleTracker */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    f f3393a;

    /* renamed from: b  reason: collision with root package name */
    private Context f3394b;

    public abstract void c();

    public abstract void d();

    a(Context context) {
        this.f3394b = context;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        f fVar = this.f3393a;
        if (fVar != null) {
            fVar.a(this.f3394b);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        f fVar = this.f3393a;
        if (fVar != null) {
            fVar.b(this.f3394b);
        }
    }
}
