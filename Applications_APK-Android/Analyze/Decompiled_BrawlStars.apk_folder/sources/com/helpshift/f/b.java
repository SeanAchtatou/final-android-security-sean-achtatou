package com.helpshift.f;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.helpshift.t;
import com.helpshift.util.n;

/* compiled from: DefaultAppLifeCycleTracker */
class b extends a implements Application.ActivityLifecycleCallbacks {
    private static String f = "DALCTracker";

    /* renamed from: b  reason: collision with root package name */
    private int f3395b;
    private int c;
    private boolean d;
    private boolean e = false;

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    b(Application application) {
        super(application);
        application.unregisterActivityLifecycleCallbacks(this);
        application.registerActivityLifecycleCallbacks(this);
        if (t.a()) {
            this.f3395b++;
            this.d = true;
        }
    }

    public final void c() {
        n.b(f, "Install API is called with manualLifeCycleTracking config as false, Ignore this event");
    }

    public final void d() {
        n.b(f, "Install API is called with manualLifeCycleTracking config as false, Ignore this event");
    }

    public void onActivityStarted(Activity activity) {
        this.f3395b++;
        if (!this.e) {
            if (!this.d) {
                a();
            }
            this.d = true;
        }
        this.e = false;
    }

    public void onActivityStopped(Activity activity) {
        boolean z = true;
        this.c++;
        if (activity == null || !activity.isChangingConfigurations()) {
            z = false;
        }
        this.e = z;
        if (!this.e && this.f3395b == this.c) {
            this.d = false;
            b();
        }
    }
}
