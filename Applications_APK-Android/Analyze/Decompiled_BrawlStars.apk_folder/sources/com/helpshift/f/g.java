package com.helpshift.f;

import android.content.Context;
import com.helpshift.util.n;
import com.helpshift.util.q;

/* compiled from: ManualAppLifeCycleTracker */
class g extends a {

    /* renamed from: b  reason: collision with root package name */
    private static String f3402b = "MALCTracker";
    private boolean c = false;

    g(Context context) {
        super(context);
    }

    public final void c() {
        if (this.c) {
            n.a(f3402b, "Application is already in foreground, so ignore this event");
        } else if (q.f4255a.get()) {
            this.c = true;
            a();
        } else {
            n.b(f3402b, "onManualAppForegroundAPI is called without calling install API");
        }
    }

    public final void d() {
        if (!this.c) {
            n.a(f3402b, "Application is already in background, so ignore this event");
        } else if (q.f4255a.get()) {
            this.c = false;
            b();
        } else {
            n.b(f3402b, "onManualAppBackgroundAPI is called without calling install API");
        }
    }
}
