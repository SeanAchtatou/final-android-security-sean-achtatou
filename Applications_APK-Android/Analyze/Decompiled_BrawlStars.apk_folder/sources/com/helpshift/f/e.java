package com.helpshift.f;

import android.content.Context;

/* compiled from: HSAppLifeCycleController */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3400a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ c f3401b;

    e(c cVar, Context context) {
        this.f3401b = cVar;
        this.f3400a = context;
    }

    public void run() {
        synchronized (c.f3396b) {
            for (f b2 : this.f3401b.f3397a) {
                b2.b(this.f3400a);
            }
        }
    }
}
