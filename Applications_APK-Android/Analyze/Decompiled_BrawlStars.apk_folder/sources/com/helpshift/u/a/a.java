package com.helpshift.u.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.common.a.a.b;
import com.helpshift.common.a.c;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.util.List;

/* compiled from: ConversationDBHelper */
public class a extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private final c f4230a;

    public a(Context context, c cVar) {
        super(context, "__hs__db_issues", (SQLiteDatabase.CursorFactory) null, c.f3242a.intValue());
        this.f4230a = cVar;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase.isOpen()) {
            try {
                List<String> a2 = this.f4230a.a();
                sQLiteDatabase.beginTransaction();
                for (String execSQL : a2) {
                    sQLiteDatabase.execSQL(execSQL);
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    n.c("Helpshift_ConversationDB", "Error in onCreate inside finally block, ", e, new com.helpshift.p.b.a[0]);
                }
            } catch (Exception e2) {
                n.c("Helpshift_ConversationDB", "Exception while creating tables: version: " + c.f3242a, e2, new com.helpshift.p.b.a[0]);
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    n.c("Helpshift_ConversationDB", "Error in onCreate inside finally block, ", e3, new com.helpshift.p.b.a[0]);
                }
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e4) {
                    n.c("Helpshift_ConversationDB", "Error in onCreate inside finally block, ", e4, new com.helpshift.p.b.a[0]);
                }
                throw th;
            }
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        com.helpshift.common.d.a aVar;
        if (i < 6) {
            a(sQLiteDatabase);
        } else if (sQLiteDatabase.isOpen()) {
            boolean z = false;
            try {
                sQLiteDatabase.beginTransaction();
                for (int i3 = i; i3 < i2; i3++) {
                    if (i3 == 6) {
                        aVar = new com.helpshift.common.a.a.a(sQLiteDatabase);
                    } else if (i3 == 7) {
                        aVar = new b(sQLiteDatabase);
                    } else {
                        throw new IllegalStateException("Unsupported version for database migration");
                    }
                    aVar.a();
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    n.c("Helpshift_ConversationDB", "Exception while migrating conversationDB inside finally block, ", e, new com.helpshift.p.b.a[0]);
                }
            } catch (Exception e2) {
                n.c("Helpshift_ConversationDB", "Exception while migrating conversationDB, old: " + i + ", new: " + i2, e2, new com.helpshift.p.b.a[0]);
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    n.c("Helpshift_ConversationDB", "Exception while migrating conversationDB inside finally block, ", e3, new com.helpshift.p.b.a[0]);
                }
                z = true;
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e4) {
                    n.c("Helpshift_ConversationDB", "Exception while migrating conversationDB inside finally block, ", e4, new com.helpshift.p.b.a[0]);
                }
                throw th;
            }
            if (z) {
                a(sQLiteDatabase);
                q.c().C();
            }
        }
    }

    public final void a(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase.isOpen()) {
            try {
                List<String> b2 = this.f4230a.b();
                sQLiteDatabase.beginTransaction();
                for (String execSQL : b2) {
                    sQLiteDatabase.execSQL(execSQL);
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    n.c("Helpshift_ConversationDB", "Error in dropAndCreateDatabase inside finally block, ", e, new com.helpshift.p.b.a[0]);
                }
            } catch (Exception e2) {
                n.c("Helpshift_ConversationDB", "Exception while upgrading tables, version: " + c.f3242a, e2, new com.helpshift.p.b.a[0]);
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    n.c("Helpshift_ConversationDB", "Error in dropAndCreateDatabase inside finally block, ", e3, new com.helpshift.p.b.a[0]);
                }
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e4) {
                    n.c("Helpshift_ConversationDB", "Error in dropAndCreateDatabase inside finally block, ", e4, new com.helpshift.p.b.a[0]);
                }
                throw th;
            }
        }
    }
}
