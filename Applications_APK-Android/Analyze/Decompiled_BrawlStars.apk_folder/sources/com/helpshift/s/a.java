package com.helpshift.s;

import com.helpshift.util.w;
import com.helpshift.y.e;

/* compiled from: AppInfoModel */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public String f3829a = ((String) this.m.a("apiKey"));

    /* renamed from: b  reason: collision with root package name */
    public String f3830b = ((String) this.m.a("domainName"));
    public String c;
    public Integer d;
    public Integer e;
    public Integer f;
    public Boolean g;
    public Boolean h;
    public Boolean i;
    public Boolean j;
    public Integer k;
    public String l;
    public e m;

    protected a(e eVar) {
        this.m = eVar;
        String str = this.f3830b;
        if (str != null && !w.b(str)) {
            this.f3830b = null;
        }
        this.c = (String) this.m.a("platformId");
        String str2 = this.c;
        if (str2 != null && !w.a(str2)) {
            this.c = null;
        }
        this.l = (String) this.m.a("font");
        this.d = (Integer) this.m.a("notificationSound");
        this.e = (Integer) this.m.a("notificationIcon");
        this.f = (Integer) this.m.a("largeNotificationIcon");
        this.g = (Boolean) this.m.a("disableHelpshiftBranding");
        this.h = (Boolean) this.m.a("enableInboxPolling");
        this.i = (Boolean) this.m.a("muteNotifications");
        this.j = (Boolean) this.m.a("disableAnimations");
        this.k = (Integer) this.m.a("screenOrientation");
    }
}
