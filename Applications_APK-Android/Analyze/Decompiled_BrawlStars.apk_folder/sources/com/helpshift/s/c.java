package com.helpshift.s;

import com.helpshift.common.b.a;
import com.helpshift.common.e.ab;
import com.helpshift.y.e;
import java.util.HashMap;

/* compiled from: SdkInfoModel */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private HashMap<String, String> f3834a = ((HashMap) this.f3835b.a("etags"));

    /* renamed from: b  reason: collision with root package name */
    private e f3835b;
    private a c;

    protected c(e eVar, ab abVar) {
        this.f3835b = eVar;
        this.c = abVar.k();
        if (this.f3834a == null) {
            this.f3834a = new HashMap<>();
        }
        String str = (String) this.f3835b.a("hs-device-id");
        if (str != null) {
            this.c.a("hs-device-id", str);
        }
        String str2 = (String) this.f3835b.a("hs-synced-user-id");
        if (str2 != null) {
            this.c.a("hs-synced-user-id", str2);
        }
    }

    public final void a(String str) {
        if (this.f3834a.containsKey(str)) {
            this.f3834a.remove(str);
            this.f3835b.a("etags", this.f3834a);
        }
    }

    public final Integer a() {
        return (Integer) this.f3835b.a("sdk-theme");
    }
}
