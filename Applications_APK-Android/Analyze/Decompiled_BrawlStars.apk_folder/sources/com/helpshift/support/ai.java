package com.helpshift.support;

import android.app.Activity;
import java.util.Map;

/* compiled from: Support */
public final class ai implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f3873a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f3874b;
    final /* synthetic */ Map c;

    public ai(Activity activity, String str, Map map) {
        this.f3873a = activity;
        this.f3874b = str;
        this.c = map;
    }

    public final void run() {
        al.b(this.f3873a, this.f3874b, this.c);
    }
}
