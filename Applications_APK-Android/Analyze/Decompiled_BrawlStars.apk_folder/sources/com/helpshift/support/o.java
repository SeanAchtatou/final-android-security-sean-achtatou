package com.helpshift.support;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import com.helpshift.R;
import com.helpshift.b.b;
import com.helpshift.util.q;
import com.helpshift.views.a;
import java.util.HashMap;

/* compiled from: HSReviewFragment */
public final class o extends DialogFragment {

    /* renamed from: b  reason: collision with root package name */
    private static a f4190b;

    /* renamed from: a  reason: collision with root package name */
    String f4191a = "";
    private final String c = "Helpshift_ReviewFrag";
    private boolean d = true;

    public final Dialog onCreateDialog(Bundle bundle) {
        FragmentActivity activity = getActivity();
        Bundle extras = activity.getIntent().getExtras();
        if (extras != null) {
            this.d = extras.getBoolean("disableReview", true);
            this.f4191a = extras.getString("rurl");
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.hs__review_message);
        AlertDialog create = builder.create();
        create.setTitle(R.string.hs__review_title);
        create.setCanceledOnTouchOutside(false);
        create.setButton(-1, getResources().getString(R.string.hs__rate_button), new p(this));
        create.setButton(-3, getResources().getString(R.string.hs__feedback_button), new q(this));
        create.setButton(-2, getResources().getString(R.string.hs__review_close_button), new r(this));
        a.a(create);
        return create;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        a("later");
        f4190b = null;
    }

    public final void onDestroyView() {
        super.onDestroyView();
        if (this.d) {
            q.c().r().a(true);
        }
        getActivity().finish();
    }

    static void a(int i) {
        f4190b = null;
    }

    static void a(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", "periodic");
        hashMap.put("response", str);
        q.c().k().a(b.REVIEWED_APP, hashMap);
    }
}
