package com.helpshift.support.j;

import java.io.Serializable;

/* compiled from: FuzzySearchToken */
public class c implements Serializable {
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    public final String f4141a;

    /* renamed from: b  reason: collision with root package name */
    public final String f4142b;

    public c(String str, String str2) {
        this.f4141a = str;
        this.f4142b = str2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        String str = this.f4141a;
        if (str != null ? !str.equals(cVar.f4141a) : cVar.f4141a != null) {
            return false;
        }
        String str2 = this.f4142b;
        String str3 = cVar.f4142b;
        if (str2 != null) {
            return str2.equals(str3);
        }
        if (str3 == null) {
            return true;
        }
        return false;
    }
}
