package com.helpshift.support.j;

/* compiled from: AttachmentFileSize */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    public double f4138a;

    /* renamed from: b  reason: collision with root package name */
    public String f4139b;

    public a(double d) {
        if (d < 1024.0d) {
            this.f4138a = d;
            this.f4139b = " B";
        } else if (d < 1048576.0d) {
            this.f4138a = d / 1024.0d;
            this.f4139b = " KB";
        } else {
            this.f4138a = d / 1048576.0d;
            this.f4139b = " MB";
        }
    }
}
