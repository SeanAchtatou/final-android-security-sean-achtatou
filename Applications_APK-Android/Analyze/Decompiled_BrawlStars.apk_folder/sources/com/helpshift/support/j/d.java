package com.helpshift.support.j;

import java.io.Serializable;

/* compiled from: TfIdfSearchToken */
public class d implements Serializable {
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    public final String f4143a;

    /* renamed from: b  reason: collision with root package name */
    public final int f4144b;

    public d(String str, int i) {
        this.f4143a = str;
        this.f4144b = i;
    }

    public String toString() {
        return "value: " + this.f4143a + ", type: " + this.f4144b;
    }
}
