package com.helpshift.support;

import android.os.Parcel;
import android.os.Parcelable;
import com.helpshift.n.a;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public final class Faq implements Parcelable {
    public static final Parcelable.Creator<Faq> CREATOR = new f();

    /* renamed from: a  reason: collision with root package name */
    public final String f3836a;

    /* renamed from: b  reason: collision with root package name */
    public final String f3837b;
    public final String c;
    public final String d;
    public final String e;
    public final int f;
    public final Boolean g;
    public long h;
    public ArrayList<String> i;
    public String j;
    private String k;
    private List<String> l;
    private List<String> m;

    public final int describeContents() {
        return 0;
    }

    public Faq(long j2, String str, String str2, String str3, String str4, String str5, String str6, int i2, Boolean bool, List<String> list, List<String> list2) {
        this.h = j2;
        this.j = str;
        this.f3836a = str5;
        this.f3837b = str2;
        this.c = str3;
        this.k = "faq";
        this.d = str4;
        this.e = str6;
        this.f = i2;
        this.g = bool;
        this.l = list;
        this.m = list2;
    }

    public Faq(a aVar, String str) {
        this.h = 0;
        this.j = aVar.f3761a;
        this.f3837b = aVar.f3762b;
        this.c = aVar.c;
        this.d = str;
        this.f3836a = aVar.e;
        this.e = aVar.f;
        this.f = aVar.g;
        this.g = aVar.h;
        this.l = aVar.i;
        this.m = aVar.j;
    }

    Faq(Parcel parcel) {
        this.j = parcel.readString();
        this.f3836a = parcel.readString();
        this.f3837b = parcel.readString();
        this.c = parcel.readString();
        this.k = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readInt();
        this.g = Boolean.valueOf(parcel.readByte() != 0);
        if (this.i == null) {
            this.i = new ArrayList<>();
        }
        if (this.l == null) {
            this.l = new ArrayList();
        }
        if (this.m == null) {
            this.m = new ArrayList();
        }
        parcel.readStringList(this.i);
        parcel.readStringList(this.l);
        parcel.readStringList(this.m);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.j);
        parcel.writeString(this.f3836a);
        parcel.writeString(this.f3837b);
        parcel.writeString(this.c);
        parcel.writeString(this.k);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
        parcel.writeByte(this.g.booleanValue() ? (byte) 1 : 0);
        parcel.writeStringList(this.i);
        parcel.writeStringList(this.l);
        parcel.writeStringList(this.m);
    }

    public final List<String> a() {
        List<String> list = this.l;
        return list == null ? new ArrayList() : list;
    }

    public final List<String> b() {
        List<String> list = this.m;
        return list == null ? new ArrayList() : list;
    }

    /* access modifiers changed from: protected */
    public final void a(ArrayList<String> arrayList) {
        ArrayList<String> arrayList2 = this.i;
        HashSet hashSet = new HashSet();
        if (arrayList2 != null) {
            hashSet.addAll(arrayList2);
        }
        if (arrayList != null) {
            hashSet.addAll(arrayList);
        }
        this.i = new ArrayList<>(hashSet);
    }

    public final boolean equals(Object obj) {
        Faq faq = (Faq) obj;
        if (faq != null && this.j.equals(faq.j) && this.f3836a.equals(faq.f3836a) && this.e.equals(faq.e) && this.f3837b.equals(faq.f3837b) && this.c.equals(faq.c) && this.d.equals(faq.d) && this.g == faq.g && this.f == faq.f && this.l.equals(faq.l) && this.m.equals(faq.m)) {
            return true;
        }
        return false;
    }

    public final String toString() {
        return this.f3836a;
    }
}
