package com.helpshift.support.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class DotView extends View implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    private int f4203a;

    /* renamed from: b  reason: collision with root package name */
    private Paint f4204b;
    private float c;
    private float d;
    private float e;
    private RectF f;

    public DotView(Context context, int i) {
        super(context);
        this.c = -1.0f;
        this.d = -1.0f;
        this.f4203a = i;
        this.f = new RectF();
        this.f4204b = new Paint();
        this.f4204b.setAntiAlias(true);
        this.f4204b.setColor(this.f4203a);
    }

    public DotView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DotView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.c = -1.0f;
        this.d = -1.0f;
    }

    public void setDotColor(int i) {
        this.f4203a = i;
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.drawOval(this.f, this.f4204b);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.c = (float) (getWidth() / 2);
        this.d = (float) (getHeight() / 2);
        this.e = Math.min(this.c, this.d);
        RectF rectF = this.f;
        float f2 = this.c;
        float f3 = this.e;
        rectF.left = f2 - f3;
        rectF.right = f2 + f3;
        float f4 = this.d;
        rectF.top = f4 - f3;
        rectF.bottom = f4 + f3;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f4203a = Color.argb(((Integer) valueAnimator.getAnimatedValue()).intValue(), Color.red(this.f4203a), Color.green(this.f4203a), Color.blue(this.f4203a));
        this.f4204b.setColor(this.f4203a);
        invalidate();
    }
}
