package com.helpshift.support.views;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.helpshift.j.a.a.a.b;
import java.util.List;

/* compiled from: HSAdjustableSelectOptionsViewInflater */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public Context f4209a;

    /* renamed from: b  reason: collision with root package name */
    public LinearLayout f4210b;
    public int c;
    public int d;
    public int e;
    public int f;
    public View.OnClickListener g;
    public List<b.a> h;
    public double i;
    public int j;

    public a(Context context, double d2, int i2, LinearLayout linearLayout, int i3, int i4, int i5, int i6, List<b.a> list, View.OnClickListener onClickListener) {
        this.f4209a = context;
        this.i = d2;
        this.j = i2;
        this.f4210b = linearLayout;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = i6;
        this.g = onClickListener;
        this.h = list;
    }
}
