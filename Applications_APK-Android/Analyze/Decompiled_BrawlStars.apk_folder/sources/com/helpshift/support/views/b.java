package com.helpshift.support.views;

import android.animation.Animator;

/* compiled from: HSTypingIndicatorView */
class b implements Animator.AnimatorListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HSTypingIndicatorView f4211a;

    public void onAnimationCancel(Animator animator) {
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationStart(Animator animator) {
    }

    b(HSTypingIndicatorView hSTypingIndicatorView) {
        this.f4211a = hSTypingIndicatorView;
    }

    public void onAnimationEnd(Animator animator) {
        animator.setStartDelay(450);
        animator.start();
    }
}
