package com.helpshift.support.views;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import com.helpshift.R;
import java.util.Iterator;

public class HSTypingIndicatorView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    Animator[] f4207a;

    /* renamed from: b  reason: collision with root package name */
    AnimatorSet f4208b;
    private final long c;
    private final long d;
    private float e;
    private DotView[] f;
    private int g;
    private float h;

    public HSTypingIndicatorView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public HSTypingIndicatorView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.c = 900;
        this.d = 450;
        this.f4207a = new Animator[3];
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.HSTypingIndicatorView, 0, 0);
        int color = obtainStyledAttributes.getColor(R.styleable.HSTypingIndicatorView_hs__dotColor, 0);
        this.g = Color.argb(76, Color.red(color), Color.green(color), Color.blue(color));
        this.h = obtainStyledAttributes.getDimension(R.styleable.HSTypingIndicatorView_hs__interDotPadding, 0.0f);
        this.e = obtainStyledAttributes.getDimension(R.styleable.HSTypingIndicatorView_hs__dotDiameter, 0.0f);
        obtainStyledAttributes.recycle();
        b();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        if (i != 0) {
            a();
        } else if (this.f4208b == null) {
            this.f4208b = new AnimatorSet();
            this.f4208b.playTogether(this.f4207a);
            this.f4208b.addListener(new b(this));
            this.f4208b.start();
        }
    }

    private void a() {
        AnimatorSet animatorSet = this.f4208b;
        if (animatorSet != null) {
            Iterator<Animator> it = animatorSet.getChildAnimations().iterator();
            while (it.hasNext()) {
                it.next().cancel();
            }
            this.f4208b.cancel();
            this.f4208b.removeAllListeners();
            this.f4208b = null;
            for (DotView dotColor : this.f) {
                dotColor.setDotColor(this.g);
            }
        }
    }

    private void b() {
        this.f = new DotView[3];
        for (int i = 0; i < 3; i++) {
            this.f[i] = new DotView(getContext(), this.g);
            float f2 = this.h;
            float f3 = f2 / 2.0f;
            float f4 = f2 / 2.0f;
            long j = 0;
            if (i == 0) {
                f3 = 0.0f;
            } else if (i != 1) {
                j = 450;
                f4 = 0.0f;
            } else {
                j = 225;
            }
            float f5 = this.e;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) f5, (int) f5);
            layoutParams.setMargins((int) f3, 0, (int) f4, 0);
            addView(this.f[i], layoutParams);
            Animator[] animatorArr = this.f4207a;
            DotView dotView = this.f[i];
            ValueAnimator ofInt = ValueAnimator.ofInt(76, 179, 76);
            ofInt.setStartDelay(j);
            ofInt.setDuration(900L);
            ofInt.setInterpolator(new LinearInterpolator());
            ofInt.addUpdateListener(dotView);
            animatorArr[i] = ofInt;
        }
    }
}
