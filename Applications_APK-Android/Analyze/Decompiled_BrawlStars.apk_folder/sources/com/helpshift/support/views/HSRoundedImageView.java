package com.helpshift.support.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.helpshift.R;
import com.helpshift.support.m.b;

public class HSRoundedImageView extends AppCompatImageView {

    /* renamed from: a  reason: collision with root package name */
    private final Matrix f4205a;

    /* renamed from: b  reason: collision with root package name */
    private ImageView.ScaleType f4206b;
    private Bitmap c;
    private RectF d;
    private RectF e;
    private Paint f;
    private Paint g;
    private Paint h;
    private BitmapShader i;
    private float j;
    private float k;
    private boolean[] l;
    private String m;
    private Bitmap n;

    public HSRoundedImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public HSRoundedImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f4205a = new Matrix();
        this.f4206b = ImageView.ScaleType.CENTER_CROP;
        this.d = new RectF();
        this.e = new RectF();
        this.l = new boolean[4];
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.HSRoundedImageView, 0, 0);
        int color = obtainStyledAttributes.getColor(R.styleable.HSRoundedImageView_hs__borderColor, -1);
        int color2 = obtainStyledAttributes.getColor(R.styleable.HSRoundedImageView_hs__backgroundColor, -1);
        this.j = obtainStyledAttributes.getDimension(R.styleable.HSRoundedImageView_hs__borderWidth, 0.0f);
        if (this.j < 0.0f) {
            this.j = 0.0f;
        }
        this.k = obtainStyledAttributes.getDimension(R.styleable.HSRoundedImageView_hs__cornerRadius, 0.0f);
        this.l[0] = obtainStyledAttributes.getBoolean(R.styleable.HSRoundedImageView_hs__roundedTopLeft, true);
        this.l[1] = obtainStyledAttributes.getBoolean(R.styleable.HSRoundedImageView_hs__roundedTopRight, true);
        this.l[2] = obtainStyledAttributes.getBoolean(R.styleable.HSRoundedImageView_hs__roundedBottomLeft, true);
        this.l[3] = obtainStyledAttributes.getBoolean(R.styleable.HSRoundedImageView_hs__roundedBottomRight, true);
        Drawable drawable = obtainStyledAttributes.getDrawable(R.styleable.HSRoundedImageView_hs__placeholder);
        if (drawable instanceof BitmapDrawable) {
            this.n = ((BitmapDrawable) drawable).getBitmap();
        }
        obtainStyledAttributes.recycle();
        this.f = new Paint();
        this.f.setStyle(Paint.Style.FILL);
        this.f.setAntiAlias(true);
        this.g = new Paint();
        this.g.setStyle(Paint.Style.STROKE);
        this.g.setAntiAlias(true);
        this.g.setColor(color);
        this.g.setStrokeWidth(this.j);
        if (color2 != -1) {
            this.h = new Paint();
            this.h.setStyle(Paint.Style.FILL);
            this.h.setColor(color2);
            this.h.setAntiAlias(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        a();
    }

    private void a() {
        b();
        Bitmap bitmap = this.c;
        if (bitmap != null) {
            a(bitmap);
            return;
        }
        Bitmap bitmap2 = this.n;
        if (bitmap2 != null) {
            a(bitmap2);
        } else {
            invalidate();
        }
    }

    private void a(Bitmap bitmap) {
        if (bitmap != null && getWidth() > 0 && getHeight() > 0) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            this.e.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
            this.d.set(this.e);
            RectF rectF = this.e;
            float f2 = this.j;
            rectF.inset(f2 / 2.0f, f2 / 2.0f);
            RectF rectF2 = this.d;
            float f3 = this.j;
            rectF2.inset(f3, f3);
            this.i = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            a(this.i, width, height);
            invalidate();
        }
    }

    private void b() {
        if (TextUtils.isEmpty(this.m) || getWidth() <= 0) {
            this.c = null;
        } else {
            this.c = b.a(this.m, getWidth(), isHardwareAccelerated());
        }
    }

    public final void a(String str) {
        if (str != null) {
            String trim = str.trim();
            if (!trim.equals(this.m)) {
                this.m = trim;
                a();
            } else if (this.c == null) {
                a();
            }
        } else {
            this.m = null;
            a();
        }
    }

    public ImageView.ScaleType getScaleType() {
        return this.f4206b;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.f.setShader(this.i);
        float f2 = this.j;
        if (f2 > 0.0f) {
            Paint paint = this.h;
            if (paint != null) {
                RectF rectF = this.d;
                float f3 = this.k;
                canvas.drawRoundRect(rectF, f3 - f2, f3 - f2, paint);
            }
            RectF rectF2 = this.d;
            float f4 = this.k;
            float f5 = this.j;
            canvas.drawRoundRect(rectF2, f4 - f5, f4 - f5, this.f);
            RectF rectF3 = this.e;
            float f6 = this.k;
            canvas.drawRoundRect(rectF3, f6, f6, this.g);
            a(canvas, this.h);
            a(canvas, this.f);
            if (this.k > 0.0f) {
                float f7 = this.e.left;
                float f8 = this.e.top;
                float width = f7 + this.e.width();
                float height = f8 + this.e.height();
                float f9 = this.k;
                float f10 = this.j;
                if (!this.l[0]) {
                    canvas.drawLine(f7 - f10, f8, f7 + f9, f8, this.g);
                    canvas.drawLine(f7, f8 - f10, f7, f8 + f9, this.g);
                }
                if (!this.l[1]) {
                    Canvas canvas2 = canvas;
                    float f11 = width;
                    canvas2.drawLine((width - f9) - f10, f8, f11, f8, this.g);
                    canvas2.drawLine(width, f8 - f10, f11, f8 + f9, this.g);
                }
                if (!this.l[3]) {
                    Canvas canvas3 = canvas;
                    float f12 = height;
                    canvas3.drawLine((width - f9) - f10, height, width + f10, f12, this.g);
                    canvas3.drawLine(width, height - f9, width, f12, this.g);
                }
                if (!this.l[2]) {
                    canvas.drawLine(f7 - f10, height, f7 + f9, height, this.g);
                    canvas.drawLine(f7, height - f9, f7, height, this.g);
                    return;
                }
                return;
            }
            return;
        }
        Paint paint2 = this.h;
        if (paint2 != null) {
            RectF rectF4 = this.d;
            float f13 = this.k;
            canvas.drawRoundRect(rectF4, f13, f13, paint2);
        }
        RectF rectF5 = this.d;
        float f14 = this.k;
        canvas.drawRoundRect(rectF5, f14, f14, this.f);
        a(canvas, this.h);
        a(canvas, this.f);
    }

    private void a(Canvas canvas, Paint paint) {
        if (this.k > 0.0f && paint != null) {
            float f2 = this.d.left;
            float f3 = this.d.top;
            float width = this.d.width() + f2;
            float height = this.d.height() + f3;
            float f4 = this.k;
            RectF rectF = new RectF();
            if (!this.l[0]) {
                rectF.set(f2, f3, f2 + f4, f3 + f4);
                canvas.drawRect(rectF, paint);
            }
            if (!this.l[1]) {
                rectF.set(width - f4, f3, width, f3 + f4);
                canvas.drawRect(rectF, paint);
            }
            if (!this.l[2]) {
                rectF.set(f2, height - f4, f2 + f4, height);
                canvas.drawRect(rectF, paint);
            }
            if (!this.l[3]) {
                rectF.set(width - f4, height - f4, width, height);
                canvas.drawRect(rectF, paint);
            }
        }
    }

    private void a(BitmapShader bitmapShader, int i2, int i3) {
        float f2;
        float f3;
        if (getWidth() > 0 && getHeight() > 0) {
            float f4 = 0.0f;
            if (i2 > i3) {
                f3 = this.d.height() / ((float) i3);
                f2 = (this.d.width() - (((float) i2) * f3)) * 0.5f;
            } else {
                f3 = this.d.width() / ((float) i2);
                f4 = (this.d.height() - (((float) i3) * f3)) * 0.5f;
                f2 = 0.0f;
            }
            this.f4205a.setScale(f3, f3);
            Matrix matrix = this.f4205a;
            float f5 = this.j;
            matrix.postTranslate(((float) ((int) (f2 + 0.5f))) + f5, ((float) ((int) (f4 + 0.5f))) + f5);
            bitmapShader.setLocalMatrix(this.f4205a);
        }
    }
}
