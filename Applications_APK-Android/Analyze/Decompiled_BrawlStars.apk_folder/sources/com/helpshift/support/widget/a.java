package com.helpshift.support.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.b.b;
import com.helpshift.support.m.l;
import com.helpshift.util.q;

/* compiled from: CSATDialog */
public class a extends Dialog implements DialogInterface.OnDismissListener, DialogInterface.OnShowListener, View.OnClickListener, RatingBar.OnRatingBarChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f4218a;

    /* renamed from: b  reason: collision with root package name */
    private CSATView f4219b;
    private RatingBar c;
    private TextView d;
    private EditText e;
    private float f;
    private boolean g = false;

    public a(Context context) {
        super(context);
        this.f4218a = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        super.onCreate(bundle);
        setContentView(R.layout.hs__csat_dialog);
        setOnShowListener(this);
        setOnDismissListener(this);
        this.c = (RatingBar) findViewById(R.id.ratingBar);
        l.b(getContext(), this.c.getProgressDrawable());
        this.c.setOnRatingBarChangeListener(this);
        this.d = (TextView) findViewById(R.id.like_status);
        this.e = (EditText) findViewById(R.id.additional_feedback);
        ((Button) findViewById(R.id.submit)).setOnClickListener(this);
    }

    public void onShow(DialogInterface dialogInterface) {
        q.c().k().a(b.START_CSAT_RATING);
        a(this.f);
    }

    public void onDismiss(DialogInterface dialogInterface) {
        if (this.g) {
            CSATView cSATView = this.f4219b;
            cSATView.setVisibility(8);
            cSATView.f4216a = null;
            return;
        }
        q.c().k().a(b.CANCEL_CSAT_RATING);
        this.f4219b.getRatingBar().setRating(0.0f);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.submit) {
            CSATView cSATView = this.f4219b;
            float rating = this.c.getRating();
            String obj = this.e.getText().toString();
            if (cSATView.f4217b != null) {
                cSATView.f4217b.a(Math.round(rating), obj);
            }
            this.g = true;
            dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(CSATView cSATView) {
        this.f4219b = cSATView;
        this.f = cSATView.getRatingBar().getRating();
        show();
    }

    public void onRatingChanged(RatingBar ratingBar, float f2, boolean z) {
        if (z) {
            float f3 = 1.0f;
            if (f2 >= 1.0f) {
                f3 = f2;
            }
            a(f3);
        }
    }

    private void a(float f2) {
        this.c.setRating(f2);
        double d2 = (double) f2;
        if (d2 > 4.0d) {
            this.d.setText(R.string.hs__csat_like_message);
        } else if (d2 > 3.0d) {
            this.d.setText(R.string.hs__csat_liked_rating_message);
        } else if (d2 > 2.0d) {
            this.d.setText(R.string.hs__csat_ok_rating_message);
        } else if (d2 > 1.0d) {
            this.d.setText(R.string.hs__csat_disliked_rating_message);
        } else {
            this.d.setText(R.string.hs__csat_dislike_message);
        }
        int i = (int) f2;
        this.c.setContentDescription(this.f4218a.getResources().getQuantityString(R.plurals.hs__csat_rating_value, i, Integer.valueOf(i)));
    }
}
