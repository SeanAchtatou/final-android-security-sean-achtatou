package com.helpshift.support.widget;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import com.helpshift.common.e.y;
import com.helpshift.j.d.d;
import com.helpshift.support.widget.b.a;
import com.helpshift.util.g;
import com.helpshift.util.n;
import com.helpshift.util.q;
import com.helpshift.util.s;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashSet;

/* compiled from: ImagePicker */
public class b<T extends Fragment & a> {

    /* renamed from: a  reason: collision with root package name */
    public Bundle f4220a;

    /* renamed from: b  reason: collision with root package name */
    private final String f4221b = "key_extra_data";
    private y c = q.b().d();
    private WeakReference<T> d;

    /* compiled from: ImagePicker */
    public interface a {
        void a(int i, Long l);

        void a(d dVar, Bundle bundle);

        void e();
    }

    public b(T t) {
        this.d = new WeakReference<>(t);
    }

    public final void a(Bundle bundle) {
        n.a("Helpshift_ImagePicker", "Checking permission before launching attachment picker");
        int i = c.f4222a[this.c.a(y.b.READ_STORAGE).ordinal()];
        if (i == 1) {
            a(bundle, 1);
        } else if (i == 2) {
            n.a("Helpshift_ImagePicker", "READ_STORAGE permission is not granted and can't be requested, starting alternate flow");
            a(bundle, 2);
        } else if (i == 3) {
            n.a("Helpshift_ImagePicker", "READ_STORAGE permission is not granted but can be requested");
            Fragment fragment = (Fragment) this.d.get();
            if (fragment != null) {
                ((a) fragment).e();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a(Bundle bundle, int i) {
        Intent intent;
        this.f4220a = bundle;
        n.a("Helpshift_ImagePicker", "Launching attachment picker now, flowRequestCode: " + i);
        Context a2 = q.a();
        int d2 = q.b().d().d();
        if (i != 2 || d2 < 19) {
            Intent intent2 = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            if (intent2.resolveActivity(a2.getPackageManager()) != null) {
                a(intent2, i);
                return;
            }
            Intent intent3 = new Intent("android.intent.action.GET_CONTENT");
            intent3.setType("image/*");
            if (d2 >= 11) {
                intent3.putExtra("android.intent.extra.LOCAL_ONLY", true);
            }
            intent = intent3;
        } else {
            intent = new Intent("android.intent.action.OPEN_DOCUMENT");
            intent.setType("image/*");
            intent.addFlags(1);
            intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        }
        if (intent.resolveActivity(a2.getPackageManager()) == null) {
            n.b("Helpshift_ImagePicker", "No app found for handling image pick intent " + intent);
            a(-4, (Long) null);
            return;
        }
        a(intent, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.widget.b.a(int, java.lang.Long):void
     arg types: [int, long]
     candidates:
      com.helpshift.support.widget.b.a(android.content.Intent, int):void
      com.helpshift.support.widget.b.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.widget.b.a(android.os.Bundle, int):void
      com.helpshift.support.widget.b.a(int, java.lang.Long):void */
    public void a(Uri uri) {
        if (c(uri)) {
            Context a2 = q.a();
            if (g.a(uri, a2)) {
                d b2 = b(uri);
                Long l = b2.f3591b;
                if (l == null || l.longValue() <= 26214400 || s.a(uri, a2)) {
                    n.a("Helpshift_ImagePicker", "Image picker result success, path: " + uri);
                    a(b2, this.f4220a);
                    return;
                }
                n.a("Helpshift_ImagePicker", "Image picker file size limit exceeded (in bytes): " + l + ", returning failure");
                a(-3, (Long) 26214400L);
                return;
            }
            n.a("Helpshift_ImagePicker", "Image picker file reading error, returning failure");
            a(-1, (Long) null);
            return;
        }
        n.a("Helpshift_ImagePicker", "Image picker file invalid mime type, returning failure");
        a(-2, (Long) null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.helpshift.j.d.d b(android.net.Uri r7) {
        /*
            android.content.Context r0 = com.helpshift.util.q.a()
            android.content.ContentResolver r1 = r0.getContentResolver()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r2 = r7
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6)
            r1 = 0
            if (r0 == 0) goto L_0x0068
            boolean r2 = r0.moveToFirst()     // Catch:{ all -> 0x0061 }
            if (r2 == 0) goto L_0x0068
            java.lang.String r2 = "_display_name"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ all -> 0x0061 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0061 }
            boolean r3 = com.helpshift.common.k.a(r2)     // Catch:{ all -> 0x0061 }
            if (r3 == 0) goto L_0x0032
            java.util.UUID r2 = java.util.UUID.randomUUID()     // Catch:{ all -> 0x0061 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0061 }
        L_0x0032:
            java.lang.String r3 = "_size"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ all -> 0x0061 }
            boolean r4 = r0.isNull(r3)     // Catch:{ all -> 0x0061 }
            if (r4 != 0) goto L_0x0069
            java.lang.String r3 = r0.getString(r3)     // Catch:{ all -> 0x0061 }
            if (r3 == 0) goto L_0x0069
            java.lang.Long r1 = java.lang.Long.valueOf(r3)     // Catch:{ NumberFormatException -> 0x0049 }
            goto L_0x0069
        L_0x0049:
            r3 = move-exception
            java.lang.String r4 = "Helpshift_ImagePicker"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0061 }
            r5.<init>()     // Catch:{ all -> 0x0061 }
            java.lang.String r6 = "Error getting size due to "
            r5.append(r6)     // Catch:{ all -> 0x0061 }
            r5.append(r3)     // Catch:{ all -> 0x0061 }
            java.lang.String r3 = r5.toString()     // Catch:{ all -> 0x0061 }
            com.helpshift.util.n.a(r4, r3)     // Catch:{ all -> 0x0061 }
            goto L_0x0069
        L_0x0061:
            r7 = move-exception
            if (r0 == 0) goto L_0x0067
            r0.close()
        L_0x0067:
            throw r7
        L_0x0068:
            r2 = r1
        L_0x0069:
            if (r0 == 0) goto L_0x006e
            r0.close()
        L_0x006e:
            com.helpshift.j.d.d r0 = new com.helpshift.j.d.d
            r0.<init>(r7, r2, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.widget.b.b(android.net.Uri):com.helpshift.j.d.d");
    }

    private static boolean c(Uri uri) {
        return new HashSet(Arrays.asList("image/jpeg", "image/png", "image/gif", "image/x-png", "image/x-citrix-pjpeg", "image/x-citrix-gif", "image/pjpeg")).contains(q.a().getContentResolver().getType(uri));
    }

    private void a(d dVar, Bundle bundle) {
        Fragment fragment = (Fragment) this.d.get();
        if (fragment != null) {
            ((a) fragment).a(dVar, bundle);
        }
    }

    private void a(int i, Long l) {
        Fragment fragment = (Fragment) this.d.get();
        if (fragment != null) {
            ((a) fragment).a(i, l);
        }
    }

    private void a(Intent intent, int i) {
        try {
            Fragment fragment = (Fragment) this.d.get();
            if (fragment != null && fragment.getActivity() != null) {
                fragment.startActivityForResult(intent, i);
            }
        } catch (ActivityNotFoundException e) {
            n.b("Helpshift_ImagePicker", "Error occurred while starting app for handling image pick intent " + e);
            a(-4, (Long) null);
        }
    }
}
