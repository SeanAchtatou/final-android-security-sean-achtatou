package com.helpshift.support.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import com.helpshift.R;
import com.helpshift.support.m.l;

public class CSATView extends RelativeLayout implements RatingBar.OnRatingBarChangeListener {

    /* renamed from: a  reason: collision with root package name */
    a f4216a;

    /* renamed from: b  reason: collision with root package name */
    a f4217b = null;
    private RatingBar c;

    public interface a {
        void a(int i, String str);
    }

    public CSATView(Context context) {
        super(context);
        a(context);
    }

    public CSATView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public CSATView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    private void a(Context context) {
        View.inflate(context, R.layout.hs__csat_view, this);
        this.f4216a = new a(context);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (RatingBar) findViewById(R.id.ratingBar);
        l.b(getContext(), this.c.getProgressDrawable());
        this.c.setOnRatingBarChangeListener(this);
    }

    public void onRatingChanged(RatingBar ratingBar, float f, boolean z) {
        if (z) {
            this.f4216a.a(this);
        }
    }

    /* access modifiers changed from: protected */
    public RatingBar getRatingBar() {
        return this.c;
    }

    public void setCSATListener(a aVar) {
        this.f4217b = aVar;
    }
}
