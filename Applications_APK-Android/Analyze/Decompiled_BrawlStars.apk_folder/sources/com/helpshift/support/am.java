package com.helpshift.support;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.helpshift.common.h;
import com.helpshift.util.aa;

/* compiled from: SupportInternal */
final class am implements h<aa<Integer, Boolean>, Object> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Handler f3883a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Handler f3884b;

    am(Handler handler, Handler handler2) {
        this.f3883a = handler;
        this.f3884b = handler2;
    }

    public final /* synthetic */ void a(Object obj) {
        aa aaVar = (aa) obj;
        if (aaVar != null) {
            Message obtainMessage = this.f3883a.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putInt("value", ((Integer) aaVar.f4242a).intValue());
            bundle.putBoolean("cache", ((Boolean) aaVar.f4243b).booleanValue());
            obtainMessage.obj = bundle;
            this.f3883a.sendMessage(obtainMessage);
        }
    }

    public final void b(Object obj) {
        Handler handler = this.f3884b;
        if (handler != null) {
            Message obtainMessage = handler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putInt("value", -1);
            obtainMessage.obj = bundle;
            this.f3884b.sendMessage(obtainMessage);
        }
    }
}
