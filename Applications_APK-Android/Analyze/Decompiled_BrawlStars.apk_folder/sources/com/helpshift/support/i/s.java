package com.helpshift.support.i;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.support.a.e;
import com.helpshift.support.g;
import java.util.ArrayList;

/* compiled from: SectionListFragment */
public class s extends f {

    /* renamed from: a  reason: collision with root package name */
    private RecyclerView f4123a;

    public final boolean h_() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__section_list_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        ArrayList parcelableArrayList = getArguments().getParcelableArrayList("sections");
        this.f4123a = (RecyclerView) view.findViewById(R.id.section_list);
        this.f4123a.setLayoutManager(new LinearLayoutManager(view.getContext()));
        this.f4123a.setAdapter(new e(parcelableArrayList, new t(this, parcelableArrayList, (g) getArguments().getSerializable("withTagsMatching"))));
    }

    public void onDestroyView() {
        this.f4123a.setAdapter(null);
        this.f4123a = null;
        super.onDestroyView();
    }
}
