package com.helpshift.support.i;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.support.Faq;
import com.helpshift.support.d.b;
import com.helpshift.support.d.c;
import com.helpshift.support.g;
import com.helpshift.support.h;
import com.helpshift.support.s;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* compiled from: SearchFragment */
public class l extends f {

    /* renamed from: a  reason: collision with root package name */
    h f4112a;

    /* renamed from: b  reason: collision with root package name */
    g f4113b;
    RecyclerView c;
    String d;
    View.OnClickListener e;
    View.OnClickListener f;
    private final Handler g = new m(this);
    private String h;

    public final boolean h_() {
        return true;
    }

    public static l a(Bundle bundle) {
        l lVar = new l();
        lVar.setArguments(bundle);
        return lVar;
    }

    public final c a() {
        return ((b) getParentFragment()).a();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f4113b = (g) arguments.getSerializable("withTagsMatching");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__search_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.c = (RecyclerView) view.findViewById(R.id.search_list);
        this.c.setLayoutManager(new LinearLayoutManager(view.getContext()));
        this.e = new n(this);
        this.f = new o(this);
        if (getArguments() != null) {
            this.h = getArguments().getString("sectionPublishId");
        }
        a(this.d, this.h);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.f4112a = new h(context);
        this.f4112a.f();
    }

    public void onDestroyView() {
        this.c.setAdapter(null);
        this.c = null;
        super.onDestroyView();
    }

    public final void a(String str, String str2) {
        String str3;
        this.h = str2;
        if (this.c != null) {
            String c2 = q.c().r().c("sdkLanguage");
            if (TextUtils.isEmpty(c2)) {
                c2 = Locale.getDefault().getLanguage();
            }
            boolean z = c2.startsWith("zh") || c2.equals("ja") || c2.equals("ko");
            if (str == null) {
                str3 = "";
            } else {
                str3 = str.trim();
            }
            String str4 = str3;
            this.d = str4;
            new Thread(new a(str4, z, str2, this.g), "HS-search-query").start();
            n.a("Helpshift_SearchFrag", "Performing search : Query : " + this.d);
        }
    }

    public final int c() {
        com.helpshift.support.a.c cVar;
        RecyclerView recyclerView = this.c;
        if (recyclerView == null || (cVar = (com.helpshift.support.a.c) recyclerView.getAdapter()) == null) {
            return -1;
        }
        return cVar.getItemCount() - 1;
    }

    /* compiled from: SearchFragment */
    class a implements Runnable {

        /* renamed from: b  reason: collision with root package name */
        private String f4115b;
        private boolean c;
        private String d;
        private Handler e;

        public a(String str, boolean z, String str2, Handler handler) {
            this.f4115b = str;
            this.c = z;
            this.d = str2;
            this.e = handler;
        }

        public void run() {
            List<Faq> list;
            if (TextUtils.isEmpty(this.f4115b) || (this.f4115b.length() < 3 && !this.c)) {
                list = l.this.f4112a.a(l.this.f4113b);
            } else {
                list = l.this.f4112a.a(this.f4115b, s.a.FULL_SEARCH, l.this.f4113b);
            }
            if (!TextUtils.isEmpty(this.d)) {
                ArrayList arrayList = new ArrayList();
                for (Faq faq : list) {
                    if (faq.d.equals(this.d)) {
                        arrayList.add(faq);
                    }
                }
                list = arrayList;
            }
            Message message = new Message();
            message.obj = list;
            Bundle bundle = new Bundle();
            bundle.putString("key_search_query", this.f4115b);
            message.setData(bundle);
            this.e.sendMessage(message);
        }
    }
}
