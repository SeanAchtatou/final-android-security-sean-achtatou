package com.helpshift.support.i;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.common.h;
import com.helpshift.support.activities.ParentActivity;
import com.helpshift.support.b.a;
import com.helpshift.support.b.c;
import com.helpshift.support.d;
import com.helpshift.support.d.f;
import com.helpshift.support.e.a;
import com.helpshift.support.f.ar;
import com.helpshift.support.f.d;
import com.helpshift.support.i.i;
import com.helpshift.support.m.e;
import com.helpshift.support.m.k;
import com.helpshift.support.s;
import com.helpshift.support.widget.b;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: SupportFragment */
public class x extends f implements MenuItem.OnMenuItemClickListener, View.OnClickListener, h<Integer, Integer>, f, e, b.a {

    /* renamed from: a  reason: collision with root package name */
    public boolean f4133a;

    /* renamed from: b  reason: collision with root package name */
    final List<String> f4134b = Collections.synchronizedList(new ArrayList());
    MenuItem c;
    public com.helpshift.support.e.b d;
    public View e;
    public View f;
    public View g;
    boolean h;
    MenuItem l;
    SearchView m;
    public Toolbar n;
    public int o;
    public boolean p;
    public Bundle q;
    public WeakReference<d> r;
    private MenuItem s;
    private MenuItem t;
    private MenuItem u;
    private boolean v;
    private int w = 0;
    private int x;
    private List<Integer> y;
    private b z;

    public final /* bridge */ /* synthetic */ void b(Object obj) {
    }

    public final boolean h_() {
        return false;
    }

    public static x a(Bundle bundle) {
        x xVar = new x();
        xVar.setArguments(bundle);
        return xVar;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        q.b().a(getContext());
        setRetainInstance(true);
        com.helpshift.support.e.b bVar = this.d;
        if (bVar == null) {
            this.d = new com.helpshift.support.e.b(q.a(), this, r(), getArguments());
        } else {
            bVar.c = r();
        }
        if (!this.j) {
            q.c().s().a(true);
        }
    }

    public void onStart() {
        com.helpshift.b.b bVar;
        super.onStart();
        if (getArguments() == null) {
            Activity a2 = a((Fragment) this);
            if (a2 instanceof ParentActivity) {
                a2.finish();
            } else {
                ((AppCompatActivity) a2).getSupportFragmentManager().beginTransaction().remove(this).commit();
            }
        } else {
            if (!this.j) {
                n.a("Helpshift_SupportFrag", "Helpshift session began.");
                s.a();
                if (getArguments().getInt("support_mode", 0) == 0) {
                    bVar = com.helpshift.b.b.LIBRARY_OPENED;
                } else {
                    bVar = com.helpshift.b.b.LIBRARY_OPENED_DECOMP;
                }
                q.c().k().a(bVar);
                if (this.p) {
                    this.d.b(this.q);
                    this.p = false;
                }
                q.c().f();
            }
            this.f4133a = true;
        }
    }

    public void onPause() {
        d dVar;
        if (!a((Fragment) this).isChangingConfigurations() && (dVar = (d) r().findFragmentByTag("HSConversationFragment")) != null) {
            dVar.j();
        }
        super.onPause();
    }

    private void a(Menu menu) {
        this.l = menu.findItem(R.id.hs__search);
        this.m = (SearchView) com.helpshift.views.b.a(this.l);
        this.c = menu.findItem(R.id.hs__contact_us);
        this.c.setTitle(R.string.hs__contact_us_btn);
        this.c.setOnMenuItemClickListener(this);
        com.helpshift.views.b.a(this.c).setOnClickListener(new y(this));
        this.s = menu.findItem(R.id.hs__action_done);
        this.s.setOnMenuItemClickListener(this);
        this.t = menu.findItem(R.id.hs__start_new_conversation);
        this.t.setOnMenuItemClickListener(this);
        this.u = menu.findItem(R.id.hs__attach_screenshot);
        this.u.setOnMenuItemClickListener(this);
        this.h = true;
        a((a) null);
        d();
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.hs__contact_us) {
            this.d.a((String) null);
            return true;
        } else if (itemId == R.id.hs__action_done) {
            this.d.f();
            return true;
        } else if (itemId == R.id.hs__start_new_conversation) {
            a(c.START_NEW_CONVERSATION);
            return true;
        } else if (itemId != R.id.hs__attach_screenshot) {
            return false;
        } else {
            a(c.SCREENSHOT_ATTACHMENT);
            return true;
        }
    }

    private void a(c cVar) {
        WeakReference<d> weakReference = this.r;
        if (weakReference != null && weakReference.get() != null) {
            this.r.get().a(cVar);
        }
    }

    public final void a(a aVar) {
        b a2;
        if (this.h) {
            if (aVar == null && (a2 = e.a(r())) != null) {
                aVar = a2.f4092a;
            }
            if (aVar != null) {
                com.helpshift.views.b.a(this.l, aVar);
                this.m.setOnQueryTextListener(aVar);
            }
        }
    }

    private void f() {
        Context context = getContext();
        com.helpshift.util.x.a(context, this.l.getIcon());
        com.helpshift.util.x.a(context, this.c.getIcon());
        com.helpshift.util.x.a(context, ((TextView) com.helpshift.views.b.a(this.c).findViewById(R.id.hs__notification_badge)).getBackground());
        com.helpshift.util.x.a(context, this.s.getIcon());
        com.helpshift.util.x.a(context, this.t.getIcon());
        com.helpshift.util.x.a(context, this.u.getIcon());
    }

    private void g() {
        this.l.setVisible(false);
        this.c.setVisible(false);
        this.s.setVisible(false);
        this.t.setVisible(false);
        this.u.setVisible(false);
    }

    public final void c() {
        this.v = true;
        if (!this.h) {
            return;
        }
        if (this.f4134b.contains(com.helpshift.support.b.a.class.getName()) || this.f4134b.contains(g.class.getName())) {
            d(true);
        }
    }

    public final void d() {
        if (this.h) {
            g();
            f();
            synchronized (this.f4134b) {
                for (String next : this.f4134b) {
                    if (next.equals(com.helpshift.support.b.a.class.getName())) {
                        n();
                    } else if (next.equals(l.class.getName())) {
                        m();
                    } else {
                        if (next.equals(u.class.getName() + 1)) {
                            l();
                        } else if (next.equals(c.class.getName())) {
                            k();
                        } else if (next.equals(g.class.getName())) {
                            j();
                        } else {
                            if (!next.equals(ar.class.getName())) {
                                if (!next.equals(d.class.getName())) {
                                    if (next.equals(u.class.getName() + 2)) {
                                        h();
                                    } else if (next.equals(a.class.getName())) {
                                        o();
                                    } else if (next.equals(com.helpshift.support.f.c.a.class.getName()) || next.equals(com.helpshift.support.f.a.class.getName())) {
                                        b(true);
                                        d(false);
                                        c(false);
                                    }
                                }
                            }
                            i();
                        }
                    }
                }
            }
        }
    }

    private void h() {
        this.s.setVisible(true);
    }

    private void i() {
        b(true);
        d(false);
        c(false);
        com.helpshift.support.f.b bVar = (com.helpshift.support.f.b) r().findFragmentByTag("HSNewConversationFragment");
        if (bVar == null) {
            bVar = (com.helpshift.support.f.b) r().findFragmentByTag("HSConversationFragment");
        }
        if (bVar != null) {
            this.s.setVisible(false);
        }
    }

    private void j() {
        d(this.v);
        c(com.helpshift.support.d.a(d.a.ACTION_BAR));
    }

    private void k() {
        d(true);
        c(com.helpshift.support.d.a(d.a.ACTION_BAR));
    }

    private void b(boolean z2) {
        b bVar = (b) r().findFragmentByTag("Helpshift_FaqFlowFrag");
        if (bVar != null && bVar.f4092a != null) {
            bVar.f4092a.f = z2;
        }
    }

    private void m() {
        l b2;
        b a2 = e.a(r());
        if (!(a2 == null || (b2 = e.b(a2.r())) == null)) {
            c(b2.d);
        }
        c(com.helpshift.support.d.a(d.a.ACTION_BAR));
        b(false);
    }

    private void n() {
        d(this.v);
        c(com.helpshift.support.d.a(d.a.ACTION_BAR));
    }

    private void o() {
        b(true);
        c(false);
        d(false);
    }

    private void c(boolean z2) {
        if (com.helpshift.views.b.b(this.l)) {
            this.c.setVisible(false);
        } else {
            this.c.setVisible(z2);
        }
        p();
    }

    private void d(boolean z2) {
        if (com.helpshift.views.b.b(this.l) && !this.f4134b.contains(l.class.getName())) {
            com.helpshift.views.b.c(this.l);
        }
        this.l.setVisible(z2);
    }

    private void c(String str) {
        if (!com.helpshift.views.b.b(this.l)) {
            com.helpshift.views.b.d(this.l);
        }
        if (!TextUtils.isEmpty(str)) {
            this.m.setQuery(str, false);
        }
    }

    private void p() {
        View a2;
        MenuItem menuItem = this.c;
        if (menuItem != null && menuItem.isVisible() && (a2 = com.helpshift.views.b.a(this.c)) != null) {
            TextView textView = (TextView) a2.findViewById(R.id.hs__notification_badge);
            View findViewById = a2.findViewById(R.id.hs__notification_badge_padding);
            int i = this.w;
            if (i != 0) {
                textView.setText(String.valueOf(i));
                findViewById.setVisibility(8);
                textView.setVisibility(0);
                return;
            }
            textView.setVisibility(8);
            findViewById.setVisibility(0);
        }
    }

    public final void a(String str) {
        Toolbar toolbar = this.n;
        if (toolbar != null) {
            toolbar.setTitle(str);
            return;
        }
        ActionBar supportActionBar = ((AppCompatActivity) a((Fragment) this)).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle(str);
        }
    }

    public final void a(boolean z2) {
        if (Build.VERSION.SDK_INT >= 21) {
            e(z2);
        } else {
            f(z2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.util.x.a(android.content.Context, float):float
     arg types: [android.content.Context, int]
     candidates:
      com.helpshift.util.x.a(android.content.Context, int):int
      com.helpshift.util.x.a(android.content.Context, android.graphics.drawable.Drawable):void
      com.helpshift.util.x.a(android.content.Context, float):float */
    private void e(boolean z2) {
        Toolbar toolbar = this.n;
        if (toolbar == null) {
            ActionBar supportActionBar = ((AppCompatActivity) a((Fragment) this)).getSupportActionBar();
            if (supportActionBar == null) {
                return;
            }
            if (z2) {
                supportActionBar.setElevation(com.helpshift.util.x.a(getContext(), 4.0f));
            } else {
                supportActionBar.setElevation(0.0f);
            }
        } else if (z2) {
            toolbar.setElevation(com.helpshift.util.x.a(getContext(), 4.0f));
        } else {
            toolbar.setElevation(0.0f);
        }
    }

    private void f(boolean z2) {
        FrameLayout frameLayout = (FrameLayout) a((Fragment) this).findViewById(R.id.flow_fragment_container);
        if (frameLayout == null) {
            return;
        }
        if (z2) {
            frameLayout.setForeground(getResources().getDrawable(R.drawable.hs__actionbar_compat_shadow));
        } else {
            frameLayout.setForeground(new ColorDrawable(0));
        }
    }

    public final void a() {
        if (getActivity() instanceof ParentActivity) {
            getActivity().finish();
        } else {
            e.a(getActivity().getSupportFragmentManager(), this);
        }
    }

    public final void a(boolean z2, Bundle bundle) {
        if (z2) {
            q().a(bundle);
        } else {
            q().a(bundle, 1);
        }
    }

    public void a(Integer num) {
        this.w = num.intValue();
        p();
    }

    public void onClick(View view) {
        b a2;
        com.helpshift.support.b.a aVar;
        if (view.getId() == R.id.button_retry && (a2 = e.a(r())) != null && (aVar = (com.helpshift.support.b.a) e.a(a2.r(), com.helpshift.support.b.a.class)) != null) {
            if (aVar.f3891a == 0) {
                aVar.a(0);
            }
            aVar.d.b(new a.b(aVar), new a.C0142a(aVar), aVar.c);
        }
    }

    private synchronized b q() {
        if (this.z == null) {
            this.z = new b(this);
        }
        return this.z;
    }

    public final void a(c cVar, boolean z2) {
        MenuItem menuItem;
        int i = z.f4136a[cVar.ordinal()];
        if (i == 1) {
            MenuItem menuItem2 = this.t;
            if (menuItem2 != null) {
                menuItem2.setVisible(z2);
            }
        } else if (i == 2 && (menuItem = this.u) != null) {
            menuItem.setVisible(z2);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if ((i == 1 || i == 2) && intent != null && i2 == -1) {
            b q2 = q();
            Uri data = intent.getData();
            if (i == 1) {
                n.a("Helpshift_ImagePicker", "Processing image uri with flow when permissions are available");
                q2.a(data);
            } else if (i == 2) {
                n.a("Helpshift_ImagePicker", "Processing image uri with flow when permissions are not available");
                int flags = intent.getFlags() & 1;
                if (Build.VERSION.SDK_INT >= 19) {
                    q.a().getContentResolver().takePersistableUriPermission(data, flags);
                }
                q2.a(data);
            }
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        List<Fragment> fragments = r().getFragments();
        if (fragments != null) {
            for (Fragment next : fragments) {
                if (next != null && next.isVisible() && (next instanceof com.helpshift.support.f.b)) {
                    next.onRequestPermissionsResult(i, strArr, iArr);
                    return;
                }
            }
        }
        super.onRequestPermissionsResult(i, strArr, iArr);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.x = arguments.getInt("toolbarId");
        }
        if (this.x == 0) {
            setHasOptionsMenu(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__support_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        Toolbar toolbar;
        super.onViewCreated(view, bundle);
        this.e = view.findViewById(R.id.view_no_faqs);
        this.f = view.findViewById(R.id.view_faqs_loading);
        this.g = view.findViewById(R.id.view_faqs_load_error);
        ((Button) view.findViewById(R.id.button_retry)).setOnClickListener(this);
        if (q.c().r().c()) {
            ((ImageView) view.findViewById(R.id.hs_logo)).setVisibility(8);
        }
        int i = this.x;
        if (i != 0) {
            Toolbar toolbar2 = null;
            if (i != 0) {
                Toolbar toolbar3 = (Toolbar) a((Fragment) this).findViewById(i);
                if (toolbar3 == null) {
                    Fragment parentFragment = getParentFragment();
                    int i2 = 5;
                    while (true) {
                        int i3 = i2 - 1;
                        if (i2 <= 0 || parentFragment == null) {
                            break;
                        }
                        View view2 = parentFragment.getView();
                        if (view2 != null && (toolbar = (Toolbar) view2.findViewById(i)) != null) {
                            toolbar2 = toolbar;
                            break;
                        } else {
                            parentFragment = parentFragment.getParentFragment();
                            i2 = i3;
                        }
                    }
                } else {
                    toolbar2 = toolbar3;
                }
            }
            this.n = toolbar2;
            Toolbar toolbar4 = this.n;
            if (toolbar4 == null) {
                n.b("Helpshift_SupportFrag", "Unable to retrieve toolbarView from dev provided toolbarId via ApiConfig");
                return;
            }
            Menu menu = toolbar4.getMenu();
            ArrayList arrayList = new ArrayList();
            for (int i4 = 0; i4 < menu.size(); i4++) {
                arrayList.add(Integer.valueOf(menu.getItem(i4).getItemId()));
            }
            this.n.inflateMenu(R.menu.hs__support_fragment);
            a(this.n.getMenu());
            Menu menu2 = this.n.getMenu();
            this.y = new ArrayList();
            for (int i5 = 0; i5 < menu2.size(); i5++) {
                int itemId = menu2.getItem(i5).getItemId();
                if (!arrayList.contains(Integer.valueOf(itemId))) {
                    this.y.add(Integer.valueOf(itemId));
                }
            }
        }
    }

    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        if (bundle != null) {
            com.helpshift.support.e.b bVar = this.d;
            if (bVar != null && !bVar.e) {
                if (bundle.containsKey("key_support_controller_started")) {
                    bVar.e = bundle.containsKey("key_support_controller_started");
                    bVar.f = bVar.f3909b.getInt("support_mode", 0);
                    if (bVar.c != null) {
                        i iVar = (i) bVar.c.findFragmentByTag("ScreenshotPreviewFragment");
                        if (iVar != null) {
                            iVar.d = bVar;
                        }
                        p pVar = (p) bVar.c.findFragmentByTag("HSSearchResultFragment");
                        if (pVar != null) {
                            pVar.f4119a = bVar;
                        }
                        a aVar = (a) bVar.c.findFragmentByTag("HSDynamicFormFragment");
                        if (aVar != null) {
                            aVar.f4090a = bVar;
                        }
                    }
                }
                if (bundle.containsKey("key_conversation_bundle") && bundle.containsKey("key_conversation_add_to_back_stack")) {
                    bVar.d = bundle.getBundle("key_conversation_bundle");
                    bVar.h = bundle.getBoolean("key_conversation_add_to_back_stack");
                }
            }
            b q2 = q();
            if (bundle.containsKey("key_extra_data")) {
                q2.f4220a = bundle.getBundle("key_extra_data");
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(android.os.Bundle, boolean, java.util.List<com.helpshift.support.h.g>):void
     arg types: [android.os.Bundle, int, java.util.List<com.helpshift.support.h.g>]
     candidates:
      com.helpshift.support.e.b.a(boolean, java.lang.Long, java.util.Map<java.lang.String, java.lang.Boolean>):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, android.os.Bundle, com.helpshift.support.i.i$a):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.List<com.helpshift.support.h.g>, boolean):void
      com.helpshift.support.e.b.a(android.os.Bundle, boolean, java.util.List<com.helpshift.support.h.g>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void
     arg types: [java.util.List<com.helpshift.support.h.g>, int]
     candidates:
      com.helpshift.support.e.b.a(android.os.Bundle, boolean):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.d.d.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.d.e.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(android.os.Bundle, boolean):void
     arg types: [android.os.Bundle, int]
     candidates:
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void
      com.helpshift.support.d.d.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.d.e.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.e.b.a(android.os.Bundle, boolean):void */
    public void onResume() {
        super.onResume();
        com.helpshift.support.e.b bVar = this.d;
        if (!bVar.e) {
            bVar.f = bVar.f3909b.getInt("support_mode", 0);
            int i = bVar.f;
            if (i == 1) {
                bVar.a(bVar.f3909b, false);
            } else if (i != 4) {
                bVar.a(bVar.f3909b, false, com.helpshift.support.h.b.a());
            } else {
                bVar.a(com.helpshift.support.h.d.a(), false);
            }
        }
        bVar.e = true;
        b(getString(R.string.hs__help_header));
        a(true);
        q.c().u().m = new AtomicReference<>(this);
        com.helpshift.support.f.d dVar = (com.helpshift.support.f.d) r().findFragmentByTag("HSConversationFragment");
        if (!(dVar == null || dVar.d == null)) {
            dVar.d.C();
        }
        a(Integer.valueOf(q.c().t()));
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        com.helpshift.support.e.b bVar = this.d;
        if (bVar != null) {
            bundle.putBoolean("key_support_controller_started", bVar.e);
            bundle.putBundle("key_conversation_bundle", bVar.d);
            bundle.putBoolean("key_conversation_add_to_back_stack", bVar.h);
        }
        bundle.putBundle("key_extra_data", q().f4220a);
    }

    public void onDestroyView() {
        k.a(getView());
        Toolbar toolbar = this.n;
        if (!(toolbar == null || this.y == null)) {
            Menu menu = toolbar.getMenu();
            for (Integer intValue : this.y) {
                menu.removeItem(intValue.intValue());
            }
        }
        this.g = null;
        this.f = null;
        this.e = null;
        super.onDestroyView();
    }

    public void onDetach() {
        q.b().a((Object) null);
        com.helpshift.util.b.a();
        if (!this.j) {
            q.c().s().a(true);
        }
        super.onDetach();
    }

    public final void e() {
        com.helpshift.support.f.b bVar = (com.helpshift.support.f.b) r().findFragmentByTag("HSConversationFragment");
        if (bVar == null) {
            bVar = (com.helpshift.support.f.b) r().findFragmentByTag("HSNewConversationFragment");
        }
        if (bVar != null) {
            bVar.a(true, 2);
        }
    }

    public final void a(int i, Long l2) {
        if (i == -4) {
            k.a(getView(), R.string.hs__network_error_msg, -1);
        } else if (i == -3) {
            k.a(getView(), String.format(getResources().getString(R.string.hs__screenshot_limit_error), Float.valueOf(((float) l2.longValue()) / 1048576.0f)), -1);
        } else if (i == -2) {
            k.a(getView(), R.string.hs__screenshot_upload_error_msg, -1);
        } else if (i == -1) {
            k.a(getView(), R.string.hs__screenshot_cloud_attach_error, -1);
        }
    }

    public void onStop() {
        if (!this.j) {
            n.a("Helpshift_SupportFrag", "Helpshift session ended.");
            com.helpshift.b c2 = q.c();
            s.b();
            c2.k().a(com.helpshift.b.b.LIBRARY_QUIT);
            this.f4133a = false;
            c2.j();
            c2.g();
        }
        q.c().u().m = null;
        super.onStop();
    }

    private void l() {
        if (!this.k) {
            b(true);
            d(false);
        }
        c(com.helpshift.support.d.a(d.a.QUESTION_ACTION_BAR));
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.hs__support_fragment, menu);
        a(menu);
        WeakReference<d> weakReference = this.r;
        if (!(weakReference == null || weakReference.get() == null)) {
            this.r.get().k();
        }
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    public final void a(com.helpshift.j.d.d dVar, Bundle bundle) {
        this.d.a(dVar, bundle, i.a.GALLERY_APP);
    }
}
