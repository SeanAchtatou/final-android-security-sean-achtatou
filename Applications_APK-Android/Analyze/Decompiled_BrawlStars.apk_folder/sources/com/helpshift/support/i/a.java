package com.helpshift.support.i;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.support.e.b;
import com.helpshift.support.h.c;
import com.helpshift.support.h.e;
import com.helpshift.support.h.f;
import com.helpshift.support.h.g;
import com.helpshift.support.h.h;
import com.helpshift.util.q;
import java.util.List;

/* compiled from: DynamicFormFragment */
public class a extends f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public b f4090a;

    /* renamed from: b  reason: collision with root package name */
    private RecyclerView f4091b;
    private List<g> c;
    private boolean d = true;
    private String e;

    public final boolean h_() {
        return true;
    }

    public static a a(Bundle bundle, List<g> list, b bVar) {
        a aVar = new a();
        aVar.setArguments(bundle);
        aVar.c = list;
        aVar.f4090a = bVar;
        return aVar;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.e = arguments.getString("flow_title");
            if (TextUtils.isEmpty(this.e)) {
                this.e = getString(R.string.hs__help_header);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__dynamic_form_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.f4091b = (RecyclerView) view.findViewById(R.id.flow_list);
        this.f4091b.setLayoutManager(new LinearLayoutManager(view.getContext()));
    }

    public void onResume() {
        super.onResume();
        b(this.e);
        List<g> list = this.c;
        if (list != null) {
            this.f4091b.setAdapter(new com.helpshift.support.a.a(list, this));
        }
    }

    public void onClick(View view) {
        g gVar = this.c.get(((Integer) view.getTag()).intValue());
        this.d = false;
        if (gVar instanceof com.helpshift.support.h.a) {
            ((com.helpshift.support.h.a) gVar).f4076a = this.f4090a;
        } else if (gVar instanceof e) {
            ((e) gVar).f4082a = this.f4090a;
        } else if (gVar instanceof h) {
            ((h) gVar).f4086a = this.f4090a;
        } else if (gVar instanceof c) {
            ((c) gVar).f4079a = this.f4090a;
        } else if (gVar instanceof f) {
            ((f) gVar).f4084a = this.f4090a;
        }
        gVar.c();
    }

    public void onStart() {
        super.onStart();
        if (!this.j && this.d) {
            q.c().k().a(com.helpshift.b.b.DYNAMIC_FORM_OPEN);
        }
        this.d = true;
    }

    public void onStop() {
        super.onStop();
        if (!this.j && this.d) {
            q.c().k().a(com.helpshift.b.b.DYNAMIC_FORM_CLOSE);
        }
    }

    public void onDestroyView() {
        this.f4091b = null;
        super.onDestroyView();
    }
}
