package com.helpshift.support.i;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.support.a.d;
import com.helpshift.support.d.e;
import java.util.List;

/* compiled from: SearchResultFragment */
public class p extends f {

    /* renamed from: a  reason: collision with root package name */
    public e f4119a;

    /* renamed from: b  reason: collision with root package name */
    RecyclerView f4120b;
    private View.OnClickListener c;
    private View.OnClickListener d;

    public final boolean h_() {
        return true;
    }

    public static p a(Bundle bundle, e eVar) {
        p pVar = new p();
        pVar.setArguments(bundle);
        pVar.f4119a = eVar;
        return pVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__search_result_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.f4120b = (RecyclerView) view.findViewById(R.id.search_result);
        this.f4120b.setLayoutManager(new LinearLayoutManager(view.getContext()));
        this.c = new q(this);
        this.d = new r(this);
    }

    public void onResume() {
        super.onResume();
        b(getString(R.string.hs__search_result_title));
        List parcelableArrayList = getArguments().getParcelableArrayList("search_fragment_results");
        if (parcelableArrayList != null && parcelableArrayList.size() > 3) {
            parcelableArrayList = parcelableArrayList.subList(0, 3);
        }
        this.f4120b.setAdapter(new d(parcelableArrayList, this.c, this.d));
    }
}
