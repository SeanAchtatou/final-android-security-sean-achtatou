package com.helpshift.support.i;

import android.content.Context;
import android.text.TextUtils;
import com.helpshift.R;
import com.helpshift.common.c.l;
import com.helpshift.support.Faq;
import com.helpshift.support.m.g;
import com.helpshift.util.q;
import com.helpshift.util.x;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: SingleQuestionFragment */
class v extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f4130a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ u f4131b;

    v(u uVar, ArrayList arrayList) {
        this.f4131b = uVar;
        this.f4130a = arrayList;
    }

    public final void a() {
        Faq faq;
        u uVar = this.f4131b;
        Context context = uVar.getContext();
        Faq a2 = this.f4131b.f4127b;
        ArrayList arrayList = this.f4130a;
        if (arrayList == null || arrayList.size() <= 0) {
            faq = null;
        } else {
            Collections.sort(arrayList);
            Collections.reverse(arrayList);
            String str = a2.f3836a;
            String str2 = a2.e;
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            String b2 = x.b(context, R.attr.hs__searchHighlightColor);
            if (!(g.a(str).equals(str) && g.a(str2).equals(str2))) {
                int length = str.length();
                ArrayList arrayList2 = new ArrayList();
                String str3 = "";
                String str4 = str3;
                for (int i = 0; i < length; i++) {
                    String a3 = g.a(str.charAt(i) + str3);
                    for (int i2 = 0; i2 < a3.length(); i2++) {
                        str4 = str4 + a3.charAt(i2);
                        arrayList2.add(Integer.valueOf(i));
                    }
                }
                String lowerCase = str4.toLowerCase();
                int length2 = str2.length();
                g.a(str2);
                ArrayList arrayList3 = new ArrayList();
                String str5 = str3;
                int i3 = 0;
                while (i3 < length2) {
                    int i4 = length2;
                    String a4 = g.a(str2.charAt(i3) + str3);
                    String str6 = str3;
                    String str7 = str5;
                    for (int i5 = 0; i5 < a4.length(); i5++) {
                        str7 = str7 + a4.charAt(i5);
                        arrayList3.add(Integer.valueOf(i3));
                    }
                    i3++;
                    str5 = str7;
                    length2 = i4;
                    str3 = str6;
                }
                String lowerCase2 = str5.toLowerCase();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    String str8 = (String) it.next();
                    if (str8.length() >= 3) {
                        String lowerCase3 = str8.toLowerCase();
                        for (int indexOf = TextUtils.indexOf(lowerCase, lowerCase3, 0); indexOf >= 0; indexOf = TextUtils.indexOf(lowerCase, lowerCase3, indexOf + lowerCase3.length())) {
                            linkedHashSet.add(str.substring(((Integer) arrayList2.get(indexOf)).intValue(), ((Integer) arrayList2.get((lowerCase3.length() + indexOf) - 1)).intValue() + 1));
                        }
                        for (int indexOf2 = TextUtils.indexOf(lowerCase2, lowerCase3, 0); indexOf2 >= 0; indexOf2 = TextUtils.indexOf(lowerCase2, lowerCase3, indexOf2 + lowerCase3.length())) {
                            linkedHashSet.add(str2.substring(((Integer) arrayList3.get(indexOf2)).intValue(), ((Integer) arrayList3.get((indexOf2 + lowerCase3.length()) - 1)).intValue() + 1));
                        }
                    }
                }
            } else {
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    String str9 = (String) it2.next();
                    if (str9.length() >= 3) {
                        linkedHashSet.add(str9);
                    }
                }
            }
            String str10 = ">" + str2 + "<";
            String str11 = ">" + str + "<";
            Pattern compile = Pattern.compile(">[^<]+<");
            Iterator it3 = linkedHashSet.iterator();
            while (it3.hasNext()) {
                String str12 = (String) it3.next();
                Matcher matcher = compile.matcher(str11);
                String str13 = str11;
                while (matcher.find()) {
                    Iterator it4 = it3;
                    String substring = str11.substring(matcher.start(), matcher.end());
                    str13 = str13.replace(substring, substring.replaceAll("(?i)(" + str12 + ")", "<span style=\"background-color: " + b2 + "\">$1</span>"));
                    it3 = it4;
                }
                Iterator it5 = it3;
                String str14 = str10;
                for (Matcher matcher2 = compile.matcher(str10); matcher2.find(); matcher2 = matcher2) {
                    String substring2 = str10.substring(matcher2.start(), matcher2.end());
                    str14 = str14.replace(substring2, substring2.replaceAll("(?i)(" + str12 + ")", "<span style=\"background-color: " + b2 + "\">$1</span>"));
                }
                str10 = str14;
                str11 = str13;
                it3 = it5;
            }
            faq = new Faq(1, a2.j, a2.f3837b, a2.c, a2.d, str11.substring(1, str11.length() - 1), str10.substring(1, str10.length() - 1), a2.f, a2.g, a2.a(), a2.b());
        }
        Faq unused = uVar.x = faq;
        q.c().a().c(new w(this));
    }
}
