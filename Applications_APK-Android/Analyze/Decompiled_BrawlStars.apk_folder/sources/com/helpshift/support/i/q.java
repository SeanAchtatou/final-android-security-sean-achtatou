package com.helpshift.support.i;

import android.view.View;
import com.helpshift.support.Faq;
import com.helpshift.support.a.d;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: SearchResultFragment */
class q implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f4121a;

    q(p pVar) {
        this.f4121a = pVar;
    }

    public void onClick(View view) {
        Faq faq;
        String str = (String) view.getTag();
        d dVar = (d) this.f4121a.f4120b.getAdapter();
        ArrayList<String> arrayList = null;
        if (dVar.f3852a != null) {
            Iterator<Faq> it = dVar.f3852a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                faq = it.next();
                if (faq.f3837b.equals(str)) {
                    break;
                }
            }
        }
        faq = null;
        if (faq != null) {
            arrayList = faq.i;
        }
        this.f4121a.f4119a.a(str, arrayList);
    }
}
