package com.helpshift.support.i;

import android.os.Handler;
import android.os.Message;
import com.helpshift.support.a.c;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SearchFragment */
class m extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f4116a;

    m(l lVar) {
        this.f4116a = lVar;
    }

    public void handleMessage(Message message) {
        if (message != null && message.getData() != null && message.obj != null) {
            String string = message.getData().getString("key_search_query");
            if (this.f4116a.d != null && this.f4116a.d.equals(string)) {
                List list = (List) message.obj;
                if (list == null) {
                    list = new ArrayList();
                }
                l lVar = this.f4116a;
                if (lVar.c != null) {
                    c cVar = new c(lVar.d, list, lVar.e, lVar.f);
                    cVar.setHasStableIds(true);
                    if (lVar.c.getAdapter() == null) {
                        lVar.c.setAdapter(cVar);
                    } else {
                        lVar.c.swapAdapter(new c(lVar.d, list, lVar.e, lVar.f), true);
                    }
                }
            }
        }
    }
}
