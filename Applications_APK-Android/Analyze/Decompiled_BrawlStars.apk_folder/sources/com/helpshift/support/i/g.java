package com.helpshift.support.i;

import android.content.Context;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.support.Faq;
import com.helpshift.support.Section;
import com.helpshift.support.h;
import com.helpshift.support.l;
import com.helpshift.support.m.e;
import com.helpshift.support.m.k;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: QuestionListFragment */
public class g extends f {

    /* renamed from: a  reason: collision with root package name */
    h f4098a;

    /* renamed from: b  reason: collision with root package name */
    com.helpshift.support.g f4099b;
    String c;
    /* access modifiers changed from: package-private */
    public RecyclerView d;
    View.OnClickListener e;
    private String f;
    private boolean g = false;
    private boolean h = false;

    public static g a(Bundle bundle) {
        g gVar = new g();
        gVar.setArguments(bundle);
        return gVar;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.f4098a = new h(context);
        this.f = getString(R.string.hs__help_header);
    }

    public void onStart() {
        super.onStart();
        this.h = this.j;
        this.g = false;
    }

    public final boolean h_() {
        return getParentFragment() instanceof b;
    }

    public void setUserVisibleHint(boolean z) {
        super.setUserVisibleHint(z);
        a();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f4099b = (com.helpshift.support.g) arguments.getSerializable("withTagsMatching");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__question_list_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        String str;
        super.onViewCreated(view, bundle);
        this.d = (RecyclerView) view.findViewById(R.id.question_list);
        this.d.setLayoutManager(new LinearLayoutManager(view.getContext()));
        this.e = new h(this);
        String string = getArguments().getString("sectionPublishId");
        if (this.k) {
            Section b2 = this.f4098a.b(string);
            if (b2 != null) {
                str = b2.f3840b;
            } else {
                str = null;
            }
            if (!TextUtils.isEmpty(str)) {
                this.f = str;
            }
        }
        b bVar = new b(this);
        a aVar = new a(this);
        if (getArguments().getInt("support_mode", 0) != 2) {
            h hVar = this.f4098a;
            if (TextUtils.isEmpty(string)) {
                aVar.sendMessage(aVar.obtainMessage());
            } else {
                try {
                    Section a2 = hVar.c.a(string);
                    if (a2 != null) {
                        Message obtainMessage = bVar.obtainMessage();
                        obtainMessage.obj = a2;
                        bVar.sendMessage(obtainMessage);
                    } else {
                        aVar.sendMessage(aVar.obtainMessage());
                    }
                } catch (SQLException e2) {
                    n.c("Helpshift_ApiData", "Database exception in getting section data ", e2);
                }
            }
        } else {
            h hVar2 = this.f4098a;
            com.helpshift.support.g gVar = this.f4099b;
            try {
                if (TextUtils.isEmpty(string)) {
                    aVar.sendMessage(aVar.obtainMessage());
                } else {
                    Section a3 = hVar2.c.a(string);
                    if (a3 != null) {
                        Message obtainMessage2 = bVar.obtainMessage();
                        obtainMessage2.obj = a3;
                        bVar.sendMessage(obtainMessage2);
                    }
                    hVar2.a(new l(hVar2, string, bVar), aVar, gVar);
                }
            } catch (SQLException e3) {
                n.c("Helpshift_ApiData", "Database exception in getting section data ", e3);
            }
        }
        n.a("Helpshift_QstnListFrag", "FAQ section loaded : Name : " + this.f);
    }

    public void onResume() {
        super.onResume();
        b(getString(R.string.hs__help_header));
        if (this.k) {
            b(this.f);
            Fragment parentFragment = getParentFragment();
            if (parentFragment instanceof b) {
                ((b) parentFragment).b(true);
            }
        }
        a();
    }

    public void onDestroyView() {
        k.a(getView());
        this.d.setAdapter(null);
        this.d = null;
        super.onDestroyView();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (getUserVisibleHint() && !this.g && !this.h && !TextUtils.isEmpty(this.c)) {
            q.c().k().a(com.helpshift.b.b.BROWSED_FAQ_LIST, this.c);
            this.g = true;
        }
    }

    /* compiled from: QuestionListFragment */
    static class b extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<g> f4101a;

        public b(g gVar) {
            this.f4101a = new WeakReference<>(gVar);
        }

        public void handleMessage(Message message) {
            Section b2;
            g gVar = this.f4101a.get();
            if (gVar != null && !gVar.isDetached()) {
                if (message.obj != null) {
                    Section section = (Section) message.obj;
                    if (gVar.d != null) {
                        ArrayList<Faq> a2 = gVar.f4098a.a(section.c, gVar.f4099b);
                        if (a2 != null && !a2.isEmpty()) {
                            gVar.d.setAdapter(new com.helpshift.support.a.b(a2, gVar.e));
                            x a3 = e.a(gVar);
                            if (a3 != null) {
                                a3.c();
                            }
                            if (TextUtils.isEmpty(gVar.c) && (b2 = gVar.f4098a.b(gVar.getArguments().getString("sectionPublishId"))) != null) {
                                gVar.c = b2.f3839a;
                            }
                            gVar.a();
                        } else if (!gVar.isDetached()) {
                            k.a(103, gVar.getView());
                        }
                    }
                    n.a("Helpshift_QstnListFrag", "FAQ section loaded : SectionSuccessHandler : " + section.f3840b);
                    return;
                }
                RecyclerView a4 = gVar.d;
                if (a4 == null || a4.getAdapter() == null || a4.getAdapter().getItemCount() == 0) {
                    k.a(103, gVar.getView());
                }
            }
        }
    }

    /* compiled from: QuestionListFragment */
    static class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<g> f4100a;

        public a(g gVar) {
            this.f4100a = new WeakReference<>(gVar);
        }

        public void handleMessage(Message message) {
            g gVar = this.f4100a.get();
            if (gVar != null && !gVar.isDetached()) {
                RecyclerView a2 = gVar.d;
                if (a2 == null || a2.getAdapter() == null || a2.getAdapter().getItemCount() == 0) {
                    com.helpshift.common.exception.a aVar = null;
                    if (message.obj instanceof com.helpshift.common.exception.a) {
                        aVar = (com.helpshift.common.exception.a) message.obj;
                    }
                    if (aVar == null || message.what == com.helpshift.support.c.a.f) {
                        k.a(103, gVar.getView());
                    } else {
                        k.a(aVar, gVar.getView());
                    }
                }
            }
        }
    }

    public void onStop() {
        if (this.k) {
            b(getString(R.string.hs__help_header));
        }
        super.onStop();
    }
}
