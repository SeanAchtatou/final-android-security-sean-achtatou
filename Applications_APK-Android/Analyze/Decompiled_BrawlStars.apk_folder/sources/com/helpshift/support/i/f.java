package com.helpshift.support.i;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import com.helpshift.R;
import com.helpshift.s.b;
import com.helpshift.support.m.e;
import com.helpshift.support.m.l;
import com.helpshift.util.b;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.lang.reflect.Field;

/* compiled from: MainFragment */
public abstract class f extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4096a = x.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static boolean f4097b;
    private FragmentManager c;
    protected String i = getClass().getName();
    protected boolean j;
    boolean k;

    public abstract boolean h_();

    public final FragmentManager r() {
        if (!f4097b) {
            return getChildFragmentManager();
        }
        if (this.c == null) {
            this.c = getChildFragmentManager();
        }
        return this.c;
    }

    public Context getContext() {
        Context context = super.getContext();
        if (context != null) {
            return context;
        }
        return q.a();
    }

    public void onAttach(Context context) {
        super.onAttach(b.f(context));
        try {
            setRetainInstance(true);
        } catch (Exception unused) {
            f4097b = true;
        }
        if (q.a() == null) {
            q.a(context.getApplicationContext());
        }
        this.k = l.a(getContext());
        if (f4097b && this.c != null) {
            try {
                Field declaredField = Fragment.class.getDeclaredField("mChildFragmentManager");
                declaredField.setAccessible(true);
                declaredField.set(this, this.c);
            } catch (NoSuchFieldException e) {
                n.a(f4096a, "NoSuchFieldException", e);
            } catch (IllegalAccessException e2) {
                n.a(f4096a, "IllegalAccessException", e2);
            }
        }
    }

    public void onStart() {
        x a2;
        super.onStart();
        if (h_() && (a2 = e.a(this)) != null) {
            a2.f4134b.add(this.i);
            a2.d();
        }
    }

    public void onPause() {
        this.j = a(this).isChangingConfigurations();
        super.onPause();
    }

    public void onStop() {
        x a2;
        if (h_() && (a2 = e.a(this)) != null) {
            a2.f4134b.remove(this.i);
        }
        super.onStop();
    }

    public static Activity a(Fragment fragment) {
        while (fragment.getParentFragment() != null) {
            fragment = fragment.getParentFragment();
        }
        return fragment.getActivity();
    }

    public final void b(String str) {
        x a2 = e.a(this);
        if (a2 != null) {
            a2.a(str);
        }
    }

    public Animation onCreateAnimation(int i2, boolean z, int i3) {
        if (b.a.f3833a.f3831a.j.booleanValue() || z || isRemoving()) {
            return super.onCreateAnimation(i2, z, i3);
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 1.0f);
        alphaAnimation.setDuration((long) getResources().getInteger(R.integer.hs_animation_duration));
        return alphaAnimation;
    }
}
