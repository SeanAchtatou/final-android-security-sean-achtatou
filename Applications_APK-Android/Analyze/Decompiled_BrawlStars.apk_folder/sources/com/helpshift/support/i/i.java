package com.helpshift.support.i;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.helpshift.R;
import com.helpshift.common.c.a;
import com.helpshift.j.a.v;
import com.helpshift.j.d.d;
import com.helpshift.j.i.bo;
import com.helpshift.support.l.e;
import com.helpshift.support.m.a;
import com.helpshift.support.m.k;
import com.helpshift.util.q;

/* compiled from: ScreenshotPreviewFragment */
public class i extends f implements View.OnClickListener, a.C0135a, v {
    private static final a.C0146a g = a.C0146a.SCREENSHOT_PREVIEW;

    /* renamed from: a  reason: collision with root package name */
    public d f4103a;

    /* renamed from: b  reason: collision with root package name */
    ProgressBar f4104b;
    public a c;
    public com.helpshift.support.d.d d;
    public int e;
    public String f;
    private ImageView h;
    private Button l;
    private View m;
    private View n;
    private bo o;

    /* compiled from: ScreenshotPreviewFragment */
    public enum a {
        ATTACHMENT_DRAFT,
        GALLERY_APP
    }

    /* compiled from: ScreenshotPreviewFragment */
    public enum b {
        ADD,
        SEND,
        REMOVE,
        CHANGE
    }

    public final boolean h_() {
        return true;
    }

    public static i a(com.helpshift.support.d.d dVar) {
        i iVar = new i();
        iVar.d = dVar;
        return iVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__screenshot_preview_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.o = q.c().a(this);
        this.h = (ImageView) view.findViewById(R.id.screenshot_preview);
        ((Button) view.findViewById(R.id.change)).setOnClickListener(this);
        this.l = (Button) view.findViewById(R.id.secondary_button);
        this.l.setOnClickListener(this);
        this.f4104b = (ProgressBar) view.findViewById(R.id.screenshot_loading_indicator);
        this.m = view.findViewById(R.id.button_containers);
        this.n = view.findViewById(R.id.buttons_separator);
    }

    public void onResume() {
        String str;
        super.onResume();
        Button button = this.l;
        int i = this.e;
        Resources resources = button.getResources();
        if (i == 1) {
            str = resources.getString(R.string.hs__screenshot_add);
        } else if (i == 2) {
            str = resources.getString(R.string.hs__screenshot_remove);
        } else if (i != 3) {
            str = "";
        } else {
            str = resources.getString(R.string.hs__send_msg_btn);
        }
        button.setText(str);
        c();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
    }

    public void onDestroyView() {
        this.o.b();
        super.onDestroyView();
    }

    public void onStart() {
        super.onStart();
        e.a.f4163a.a("current_open_screen", g);
    }

    public void onPause() {
        k.a(getView());
        super.onPause();
    }

    public void onStop() {
        super.onStop();
        a.C0146a aVar = (a.C0146a) e.a.f4163a.a("current_open_screen");
        if (aVar != null && aVar.equals(g)) {
            e.a.f4163a.b("current_open_screen");
        }
    }

    public void c() {
        if (isResumed()) {
            d dVar = this.f4103a;
            if (dVar == null) {
                com.helpshift.support.d.d dVar2 = this.d;
                if (dVar2 != null) {
                    dVar2.b();
                }
            } else if (dVar.d != null) {
                a(this.f4103a.d);
            } else if (this.f4103a.c != null) {
                a(true);
                q.c().x().a(this.f4103a, this.f, this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        Bitmap a2 = com.helpshift.support.m.b.a(str, -1, getView().isHardwareAccelerated());
        if (a2 != null) {
            this.h.setImageBitmap(a2);
            return;
        }
        com.helpshift.support.d.d dVar = this.d;
        if (dVar != null) {
            dVar.b();
        }
    }

    public void onClick(View view) {
        d dVar;
        int id = view.getId();
        if (id == R.id.secondary_button && (dVar = this.f4103a) != null) {
            int i = this.e;
            if (i == 1) {
                this.d.a(dVar);
            } else if (i == 2) {
                q.c().x();
                com.helpshift.common.c.a.a(this.f4103a);
                this.d.a();
            } else if (i == 3) {
                this.d.a(dVar, this.f);
            }
        } else if (id == R.id.change) {
            if (this.e == 2) {
                this.e = 1;
            }
            q.c().x();
            com.helpshift.common.c.a.a(this.f4103a);
            Bundle bundle = new Bundle();
            bundle.putInt("key_screenshot_mode", this.e);
            bundle.putString("key_refers_id", this.f);
            this.d.a(bundle);
        }
    }

    public final void a() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new j(this));
        }
    }

    public final void a(d dVar) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new k(this, dVar));
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        if (z) {
            this.f4104b.setVisibility(0);
            this.m.setVisibility(8);
            this.n.setVisibility(8);
            this.h.setVisibility(8);
            return;
        }
        this.f4104b.setVisibility(8);
        this.m.setVisibility(0);
        this.n.setVisibility(0);
        this.h.setVisibility(0);
    }

    public final void b() {
        com.helpshift.support.e.b bVar = ((x) getParentFragment()).d;
        if (bVar != null) {
            bVar.e();
        }
    }
}
