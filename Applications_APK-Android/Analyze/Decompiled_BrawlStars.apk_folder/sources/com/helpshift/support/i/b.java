package com.helpshift.support.i;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.support.d.a;
import com.helpshift.support.d.c;
import com.helpshift.support.h.g;
import com.helpshift.support.m.e;
import java.util.List;

/* compiled from: FaqFlowFragment */
public class b extends f implements a {

    /* renamed from: a  reason: collision with root package name */
    public com.helpshift.support.e.a f4092a;

    /* renamed from: b  reason: collision with root package name */
    public List<g> f4093b;
    private View c;
    private View d;

    public final boolean h_() {
        return false;
    }

    public static b a(Bundle bundle, List<g> list) {
        b bVar = new b();
        bVar.setArguments(bundle);
        bVar.f4093b = list;
        return bVar;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        com.helpshift.support.e.a aVar = this.f4092a;
        if (aVar == null) {
            this.f4092a = new com.helpshift.support.e.a(this, context, r(), getArguments());
        } else {
            aVar.d = r();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__faq_flow_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.c = view.findViewById(R.id.vertical_divider);
        this.d = view.findViewById(R.id.select_question_view);
    }

    public void onViewStateRestored(Bundle bundle) {
        com.helpshift.support.e.a aVar;
        super.onViewStateRestored(bundle);
        if (bundle != null && (aVar = this.f4092a) != null && !aVar.e && bundle.containsKey("key_faq_controller_state")) {
            aVar.e = bundle.getBoolean("key_faq_controller_state");
        }
    }

    public void onResume() {
        super.onResume();
        com.helpshift.support.h.b.a(this.f4093b);
        ((x) getParentFragment()).a(this.f4092a);
        com.helpshift.support.e.a aVar = this.f4092a;
        if (!aVar.e) {
            int i = aVar.c.getInt("support_mode", 0);
            if (i == 2) {
                e.b(aVar.d, R.id.list_fragment_container, g.a(aVar.c), null, false);
            } else if (i != 3) {
                e.b(aVar.d, R.id.list_fragment_container, com.helpshift.support.b.a.a(aVar.c), null, true);
            } else {
                int i2 = R.id.list_fragment_container;
                if (aVar.f3907b) {
                    i2 = R.id.single_question_container;
                }
                aVar.f3906a.c().d.g = true;
                e.b(aVar.d, i2, u.a(aVar.c, 1, aVar.f3907b, null), null, false);
            }
        }
        aVar.e = true;
        d();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        com.helpshift.support.e.a aVar = this.f4092a;
        if (aVar != null) {
            bundle.putBoolean("key_faq_controller_state", aVar.e);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.f4092a = null;
        this.c = null;
        this.d = null;
        x xVar = (x) getParentFragment();
        if (xVar.h) {
            com.helpshift.views.b.a(xVar.l, null);
            xVar.m.setOnQueryTextListener(null);
        }
    }

    public final void a(boolean z) {
        View view = this.d;
        if (view == null) {
            return;
        }
        if (z) {
            view.setVisibility(0);
        } else {
            view.setVisibility(8);
        }
    }

    public final x c() {
        return (x) getParentFragment();
    }

    public final void b(boolean z) {
        View view = this.c;
        if (view == null) {
            return;
        }
        if (z) {
            view.setVisibility(0);
        } else {
            view.setVisibility(8);
        }
    }

    public final void d() {
        if (this.k && this.d != null) {
            if (r().findFragmentById(R.id.details_fragment_container) == null) {
                a(true);
            } else {
                a(false);
            }
        }
    }

    public final c a() {
        return this.f4092a;
    }
}
