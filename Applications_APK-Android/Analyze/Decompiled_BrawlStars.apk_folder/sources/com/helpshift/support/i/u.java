package com.helpshift.support.i;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.support.Faq;
import com.helpshift.support.d;
import com.helpshift.support.h;
import com.helpshift.support.m;
import com.helpshift.support.m.e;
import com.helpshift.support.m.k;
import com.helpshift.support.webkit.CustomWebView;
import com.helpshift.support.webkit.b;
import com.helpshift.util.n;
import com.helpshift.util.p;
import com.helpshift.util.q;
import com.helpshift.util.x;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/* compiled from: SingleQuestionFragment */
public class u extends f implements View.OnClickListener, b.a {

    /* renamed from: a  reason: collision with root package name */
    boolean f4126a;

    /* renamed from: b  reason: collision with root package name */
    public Faq f4127b;
    public String c;
    private int d = 1;
    private h e;
    private CustomWebView f;
    private View g;
    private TextView h;
    private Button l;
    private Button m;
    private Button n;
    private String o;
    private String p;
    private boolean q;
    private View r;
    private com.helpshift.support.e.b s;
    private boolean t;
    private int u = 0;
    private boolean v = false;
    private b w;
    /* access modifiers changed from: private */
    public Faq x;

    /* compiled from: SingleQuestionFragment */
    public interface b {
        void a(String str);
    }

    public final boolean h_() {
        return true;
    }

    public static u a(Bundle bundle, int i, boolean z, b bVar) {
        u uVar = new u();
        uVar.setArguments(bundle);
        uVar.d = i;
        uVar.v = z;
        uVar.w = bVar;
        return uVar;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.e = new h(context);
        x a2 = e.a(this);
        if (a2 != null) {
            this.s = a2.d;
        }
        this.i = getClass().getName() + this.d;
    }

    public void onStart() {
        super.onStart();
        if (!this.j) {
            this.f4126a = false;
        }
    }

    public void onPause() {
        super.onPause();
        this.f.onPause();
    }

    public void onStop() {
        super.onStop();
        if (this.t || !this.k) {
            b(getString(R.string.hs__help_header));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.t = arguments.getBoolean("decomp", false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int i = R.layout.hs__single_question_fragment;
        if (this.v) {
            i = R.layout.hs__single_question_layout_with_cardview;
        }
        return layoutInflater.inflate(i, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        Faq faq;
        super.onViewCreated(view, bundle);
        this.f = (CustomWebView) view.findViewById(R.id.web_view);
        this.f.setWebViewClient(new com.helpshift.support.webkit.b(q.a(), this));
        this.f.setWebChromeClient(new com.helpshift.support.webkit.a(getActivity().getWindow().getDecorView(), view.findViewById(R.id.faq_content_view)));
        this.l = (Button) view.findViewById(R.id.helpful_button);
        this.l.setOnClickListener(this);
        this.m = (Button) view.findViewById(R.id.unhelpful_button);
        this.m.setOnClickListener(this);
        this.g = view.findViewById(R.id.question_footer);
        this.h = (TextView) view.findViewById(R.id.question_footer_message);
        this.n = (Button) view.findViewById(R.id.contact_us_button);
        this.n.setOnClickListener(this);
        if (Build.VERSION.SDK_INT >= 24) {
            this.l.setText(R.string.hs__mark_yes);
            this.m.setText(R.string.hs__mark_no);
            this.n.setText(R.string.hs__contact_us_btn);
        }
        if (this.d == 2) {
            this.n.setText(getResources().getString(R.string.hs__send_anyway));
        }
        this.c = getArguments().getString("questionPublishId");
        int i = getArguments().getInt("support_mode");
        String string = getArguments().getString("questionLanguage", "");
        boolean z = true;
        boolean z2 = this.d == 3;
        if (!z2 && i != 3) {
            z = false;
        }
        h hVar = this.e;
        c cVar = new c(this);
        a aVar = new a(this);
        String str = this.c;
        if (TextUtils.isEmpty(str)) {
            aVar.sendMessage(aVar.obtainMessage());
        } else {
            if (z2) {
                faq = (Faq) q.b().g().a(str, string);
                if (faq == null) {
                    faq = hVar.d.a(str, string);
                }
            } else {
                faq = hVar.d.b(str);
            }
            Message obtainMessage = cVar.obtainMessage();
            obtainMessage.obj = faq;
            cVar.sendMessage(obtainMessage);
            if (faq == null || z) {
                q.c().v().a(new m(hVar, cVar, z2, aVar, str), str, string, z2);
            }
        }
        this.r = view.findViewById(R.id.progress_bar);
    }

    public void onResume() {
        super.onResume();
        if (this.k) {
            Fragment parentFragment = getParentFragment();
            if (parentFragment instanceof b) {
                ((b) parentFragment).a(false);
            }
        }
        this.f.onResume();
        if (this.t || !this.k) {
            b(getString(R.string.hs__question_header));
        }
        Faq faq = this.f4127b;
        if (faq != null && !TextUtils.isEmpty(faq.j) && !this.f4126a) {
            d();
        }
    }

    public void onDestroyView() {
        k.a(getView());
        this.g = null;
        this.f.setWebViewClient(null);
        this.f = null;
        this.m = null;
        this.l = null;
        this.n = null;
        super.onDestroyView();
    }

    /* access modifiers changed from: package-private */
    public final void a(Faq faq) {
        String str;
        StringBuilder sb;
        this.f4127b = faq;
        if (this.f != null) {
            Context context = getContext();
            int i = 16842907;
            if (Build.VERSION.SDK_INT >= 21) {
                i = 16843829;
            }
            this.o = x.b(context, 16842806);
            this.p = x.b(context, i);
            CustomWebView customWebView = this.f;
            String b2 = com.helpshift.views.a.b();
            String str2 = "";
            if (!TextUtils.isEmpty(b2)) {
                str2 = "@font-face {    font-family: custom;    src: url('" + ("file:///android_asset/" + b2) + "');}";
                str = "font-family: custom, sans-serif;";
            } else {
                str = str2;
            }
            String str3 = faq.e;
            String str4 = faq.f3836a;
            if (faq.g.booleanValue()) {
                sb = new StringBuilder("<html dir=\"rtl\">");
            } else {
                sb = new StringBuilder("<html>");
            }
            sb.append("<head>");
            sb.append("    <style type='text/css'>");
            sb.append(str2);
            sb.append("        img,");
            sb.append("        object,");
            sb.append("        embed {");
            sb.append("            max-width: 100%;");
            sb.append("        }");
            sb.append("        a,");
            sb.append("        a:visited,");
            sb.append("        a:active,");
            sb.append("        a:hover {");
            sb.append("            color: ");
            sb.append(this.p);
            sb.append(";");
            sb.append("        }");
            sb.append("        body {");
            sb.append("            background-color: transparent;");
            sb.append("            margin: 0;");
            sb.append("            padding: ");
            sb.append(16 + "px " + 16 + "px " + 96 + "px " + 16 + "px;");
            sb.append("            font-size: ");
            sb.append("16px");
            sb.append(";");
            sb.append(str);
            sb.append("            line-height: ");
            sb.append("1.5");
            sb.append(";");
            sb.append("            white-space: normal;");
            sb.append("            word-wrap: break-word;");
            sb.append("            color: ");
            sb.append(this.o);
            sb.append(";");
            sb.append("        }");
            sb.append("        .title {");
            sb.append("            display: block;");
            sb.append("            margin: 0;");
            sb.append("            padding: 0 0 ");
            sb.append(16);
            sb.append(" 0;");
            sb.append("            font-size: ");
            sb.append("24px");
            sb.append(";");
            sb.append(str);
            sb.append("            line-height: ");
            sb.append("32px");
            sb.append(";");
            sb.append("        }");
            sb.append("        h1, h2, h3 { ");
            sb.append("            line-height: 1.4; ");
            sb.append("        }");
            sb.append("    </style>");
            sb.append("    <script language='javascript'>");
            sb.append("     window.onload = function () {");
            sb.append("        var w = window,");
            sb.append("            d = document,");
            sb.append("            e = d.documentElement,");
            sb.append("            g = d.getElementsByTagName('body')[0],");
            sb.append("            sWidth = Math.min (w.innerWidth || Infinity, e.clientWidth || Infinity, g.clientWidth || Infinity),");
            sb.append("            sHeight = Math.min (w.innerHeight || Infinity, e.clientHeight || Infinity, g.clientHeight || Infinity);");
            sb.append("        var frame, fw, fh;");
            sb.append("        var iframes = document.getElementsByTagName('iframe');");
            sb.append("        var padding = ");
            sb.append(32);
            sb.append(";");
            sb.append("        for (var i=0; i < iframes.length; i++) {");
            sb.append("            frame = iframes[i];");
            sb.append("            fw = frame.offsetWidth;");
            sb.append("            fh = frame.offsetHeight;");
            sb.append("            if (fw >= fh && fw > (sWidth - padding)) {");
            sb.append("                frame.style.width = sWidth - padding;");
            sb.append("                frame.style.height = ((sWidth - padding) * fh/fw).toString();");
            sb.append("            }");
            sb.append("        }");
            sb.append("        document.addEventListener('click', function (event) {");
            sb.append("            if (event.target instanceof HTMLImageElement) {");
            sb.append("                event.preventDefault();");
            sb.append("                event.stopPropagation();");
            sb.append("            }");
            sb.append("        }, false);");
            sb.append("    };");
            sb.append("    </script>");
            sb.append("</head>");
            sb.append("<body>");
            sb.append("    <strong class='title'> ");
            sb.append(str4);
            sb.append(" </strong> ");
            sb.append(str3);
            sb.append("</body>");
            sb.append("</html>");
            customWebView.loadDataWithBaseURL(null, sb.toString(), "text/html", "utf-8", null);
        }
    }

    private void a(boolean z) {
        Faq faq = this.f4127b;
        if (faq != null) {
            String str = faq.j;
            this.e.d.a(str, Boolean.valueOf(z));
            q.c().v().a(str, z);
        }
    }

    public void onClick(View view) {
        com.helpshift.support.d.c cVar;
        x a2;
        if (view.getId() == R.id.helpful_button) {
            a(true);
            a(1);
            if (this.d == 2 && (a2 = e.a(this)) != null) {
                a2.d.f();
            }
        } else if (view.getId() == R.id.unhelpful_button) {
            a(false);
            a(-1);
        } else if (view.getId() == R.id.contact_us_button && this.s != null) {
            if (this.d == 1) {
                com.helpshift.support.d.b bVar = (com.helpshift.support.d.b) getParentFragment();
                if (bVar != null) {
                    cVar = bVar.a();
                } else {
                    cVar = null;
                }
                if (cVar != null) {
                    cVar.a((String) null);
                    return;
                }
                return;
            }
            x a3 = e.a(this);
            if (a3 != null) {
                a3.d.c();
            }
        }
    }

    private void a(int i) {
        if (i != 0) {
            this.u = i;
        }
        e();
    }

    public final void a() {
        b(true);
        this.f.setBackgroundColor(0);
    }

    public final void c() {
        if (isVisible()) {
            b(false);
            a(this.f4127b.f);
            if (this.q) {
                this.q = false;
            } else {
                this.q = true;
                q.c().a().b(new v(this, getArguments().getStringArrayList("searchTerms")));
            }
            this.f.setBackgroundColor(0);
        }
    }

    private void b(boolean z) {
        View view = this.r;
        if (view == null) {
            return;
        }
        if (z) {
            view.setVisibility(0);
        } else {
            view.setVisibility(8);
        }
    }

    private void e() {
        if (this.d == 3) {
            f();
            return;
        }
        int i = this.u;
        if (i == -1) {
            i();
        } else if (i == 0) {
            g();
        } else if (i == 1) {
            h();
        }
    }

    private void f() {
        this.g.setVisibility(8);
    }

    private void g() {
        this.g.setVisibility(0);
        this.h.setText(getResources().getString(R.string.hs__mark_yes_no_question));
        this.n.setVisibility(8);
        this.l.setVisibility(0);
        this.m.setVisibility(0);
    }

    private void h() {
        this.g.setVisibility(0);
        this.h.setText(getResources().getString(R.string.hs__question_helpful_message));
        this.h.setGravity(17);
        this.n.setVisibility(8);
        this.l.setVisibility(8);
        this.m.setVisibility(8);
    }

    private void i() {
        this.g.setVisibility(0);
        this.h.setText(getResources().getString(R.string.hs__question_unhelpful_message));
        j();
        this.l.setVisibility(8);
        this.m.setVisibility(8);
    }

    private void j() {
        if (d.a(d.a.QUESTION_FOOTER)) {
            this.n.setVisibility(0);
        } else {
            this.n.setVisibility(8);
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        HashMap hashMap = new HashMap();
        hashMap.put("id", this.f4127b.j);
        hashMap.put("nt", Boolean.valueOf(p.a(getContext())));
        q.c().k().a(com.helpshift.b.b.READ_FAQ, hashMap);
        b bVar = this.w;
        if (bVar != null) {
            bVar.a(this.f4127b.j);
        }
        this.f4126a = true;
    }

    /* compiled from: SingleQuestionFragment */
    static class c extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<u> f4129a;

        public c(u uVar) {
            this.f4129a = new WeakReference<>(uVar);
        }

        public void handleMessage(Message message) {
            Faq faq;
            super.handleMessage(message);
            u uVar = this.f4129a.get();
            if (uVar != null && (faq = (Faq) message.obj) != null) {
                uVar.a(faq);
                String str = faq.j;
                n.a("Helpshift_SingleQstn", "FAQ question loaded : " + faq.f3836a);
                if (!uVar.f4126a && !TextUtils.isEmpty(str)) {
                    uVar.d();
                }
            }
        }
    }

    /* compiled from: SingleQuestionFragment */
    static class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<u> f4128a;

        public a(u uVar) {
            this.f4128a = new WeakReference<>(uVar);
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            u uVar = this.f4128a.get();
            if (uVar != null && !uVar.isDetached() && uVar.f4127b == null) {
                k.a(102, uVar.getView());
            }
        }
    }
}
