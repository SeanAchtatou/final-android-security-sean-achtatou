package com.helpshift.support.i;

import android.os.Bundle;
import android.view.View;
import com.helpshift.support.d.b;
import com.helpshift.support.g;
import java.util.ArrayList;

/* compiled from: SectionListFragment */
class t implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f4124a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ g f4125b;
    final /* synthetic */ s c;

    t(s sVar, ArrayList arrayList, g gVar) {
        this.c = sVar;
        this.f4124a = arrayList;
        this.f4125b = gVar;
    }

    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("sections", this.f4124a);
        bundle.putString("sectionPublishId", (String) view.getTag());
        bundle.putSerializable("withTagsMatching", this.f4125b);
        ((b) this.c.getParentFragment()).a().a(bundle);
    }
}
