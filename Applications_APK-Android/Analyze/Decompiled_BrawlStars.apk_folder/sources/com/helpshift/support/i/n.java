package com.helpshift.support.i;

import android.view.View;
import com.helpshift.support.Faq;
import com.helpshift.support.a.c;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: SearchFragment */
class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f4117a;

    n(l lVar) {
        this.f4117a = lVar;
    }

    public void onClick(View view) {
        Faq faq;
        String str = (String) view.getTag();
        c cVar = (c) this.f4117a.c.getAdapter();
        ArrayList<String> arrayList = null;
        if (cVar.f3847a != null) {
            Iterator<Faq> it = cVar.f3847a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                faq = it.next();
                if (faq.f3837b.equals(str)) {
                    break;
                }
            }
        }
        faq = null;
        if (faq != null) {
            arrayList = faq.i;
        }
        this.f4117a.a().a(str, arrayList);
    }
}
