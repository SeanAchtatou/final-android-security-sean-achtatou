package com.helpshift.support.g;

/* compiled from: DoubleMetaphone */
public class a {

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f4070b = {"GN", "KN", "PN", "WR", "PS"};
    private static final String[] c = {"L", "R", "N", "M", "B", "H", "F", "V", "W", " "};
    private static final String[] d = {"ES", "EP", "EB", "EL", "EY", "IB", "IL", "IN", "IE", "EI", "ER"};
    private static final String[] e = {"L", "T", "K", "S", "N", "M", "B", "Z"};

    /* renamed from: a  reason: collision with root package name */
    int f4071a = 4;

    private static boolean a(String str, int i, int i2, String[] strArr) {
        int i3;
        if (i < 0 || (i3 = i2 + i) > str.length()) {
            return false;
        }
        String substring = str.substring(i, i3);
        for (String equals : strArr) {
            if (substring.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v2, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v1, resolved type: com.helpshift.support.g.a$a} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v5, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v28, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v6, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v32, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v33, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v44, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v45, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v11, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v80, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v25, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v26, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v115, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v122, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v37, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v54, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v430, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v55, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v57, resolved type: int} */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x022f, code lost:
        if (a(r1, r7, 2, new java.lang.String[]{"WH"}) != false) goto L_0x0231;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x034e, code lost:
        if (a(r1, r4) == 'V') goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x0562, code lost:
        if (a(r1, r7 + 1, 1, new java.lang.String[]{"M", "N", "L", "W"}) == false) goto L_0x0564;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:354:0x082c, code lost:
        if (a(r1, r4) != 'R') goto L_0x01e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:363:0x085b, code lost:
        if (a(r1, r4) == 'Q') goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:389:0x08ec, code lost:
        if (a(r1, r4) == 'N') goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:404:0x093f, code lost:
        if (a(r1, r7 + 2, 2, new java.lang.String[]{"ER"}) != false) goto L_0x091c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:406:0x0943, code lost:
        if (r4 == false) goto L_0x0144;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:412:0x096d, code lost:
        if (a(r1, r7 - 1, 4, new java.lang.String[]{"ILLO", "ILLA", "ALLE"}) != false) goto L_0x096f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:413:0x096f, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:417:0x0999, code lost:
        if (a(r1, r1.length() - 1, 1, new java.lang.String[]{"A", "O"}) != false) goto L_0x099b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:419:0x09a8, code lost:
        if (a(r1, r7 - 1, 4, new java.lang.String[]{"ALLE"}) != false) goto L_0x096f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:420:0x09ab, code lost:
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:421:0x09ac, code lost:
        if (r4 == false) goto L_0x09c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:423:0x09b6, code lost:
        if (r8.f4072a.length() >= r8.c) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:424:0x09b8, code lost:
        r8.f4072a.append('L');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:426:0x09c9, code lost:
        if (r8.f4072a.length() >= r8.c) goto L_0x09d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:427:0x09cb, code lost:
        r8.f4072a.append('L');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:429:0x09da, code lost:
        if (r8.f4073b.length() >= r8.c) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:430:0x09dc, code lost:
        r8.f4073b.append('L');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:445:0x0a32, code lost:
        if (a(r1, r4) == r9) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:491:0x0b2a, code lost:
        if (a(r1, r4) == 'J') goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:552:0x0c6a, code lost:
        if (a(r1, r7 - 2, 1, new java.lang.String[]{"B", "H", "D"}) == false) goto L_0x0c6c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:555:0x0c82, code lost:
        if (a(r1, r7 - 3, 1, new java.lang.String[]{"B", "H", "D"}) == false) goto L_0x0c84;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:558:0x0c96, code lost:
        if (a(r1, r7 - 4, 1, new java.lang.String[]{"B", "H"}) != false) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:707:0x1031, code lost:
        if (a(r1, r4) == r9) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:754:0x1152, code lost:
        if (a(r1, r9, 6, new java.lang.String[]{"BACHER", "MACHER"}) != false) goto L_0x1113;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:781:0x11e8, code lost:
        if (a(r1, r9, 3, new java.lang.String[]{"HOR", "HYM", "HIA", "HEM"}) == false) goto L_0x11bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0192, code lost:
        if (a(r1, r7 - 2, 2, new java.lang.String[]{"AU", "OU"}) == false) goto L_0x0194;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:952:0x15ef, code lost:
        if (a(r1, r4) == 'B') goto L_0x00bb;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x077f  */
    /* JADX WARNING: Removed duplicated region for block: B:756:0x1157  */
    /* JADX WARNING: Removed duplicated region for block: B:765:0x1180  */
    /* JADX WARNING: Removed duplicated region for block: B:786:0x11fc  */
    /* JADX WARNING: Removed duplicated region for block: B:795:0x122f  */
    /* JADX WARNING: Removed duplicated region for block: B:802:0x1254  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01e2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(java.lang.String r18, boolean r19) {
        /*
            r17 = this;
            r0 = r17
            if (r18 != 0) goto L_0x0006
            r1 = 0
            return r1
        L_0x0006:
            java.lang.String r1 = r18.trim()
            int r2 = r1.length()
            if (r2 != 0) goto L_0x0012
            r1 = 0
            return r1
        L_0x0012:
            java.util.Locale r2 = java.util.Locale.ENGLISH
            java.lang.String r1 = r1.toUpperCase(r2)
            r2 = 87
            int r2 = r1.indexOf(r2)
            r3 = -1
            r4 = 75
            r5 = 0
            r6 = 1
            if (r2 > r3) goto L_0x003e
            int r2 = r1.indexOf(r4)
            if (r2 > r3) goto L_0x003e
            java.lang.String r2 = "CZ"
            int r2 = r1.indexOf(r2)
            if (r2 > r3) goto L_0x003e
            java.lang.String r2 = "WITZ"
            int r2 = r1.indexOf(r2)
            if (r2 <= r3) goto L_0x003c
            goto L_0x003e
        L_0x003c:
            r2 = 0
            goto L_0x003f
        L_0x003e:
            r2 = 1
        L_0x003f:
            java.lang.String[] r7 = com.helpshift.support.g.a.f4070b
            int r8 = r7.length
            r9 = 0
        L_0x0043:
            if (r9 >= r8) goto L_0x0052
            r10 = r7[r9]
            boolean r10 = r1.startsWith(r10)
            if (r10 == 0) goto L_0x004f
            r7 = 1
            goto L_0x0053
        L_0x004f:
            int r9 = r9 + 1
            goto L_0x0043
        L_0x0052:
            r7 = 0
        L_0x0053:
            com.helpshift.support.g.a$a r8 = new com.helpshift.support.g.a$a
            int r9 = r0.f4071a
            r8.<init>(r9)
        L_0x005a:
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r10 = r8.c
            if (r9 < r10) goto L_0x006e
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r10 = r8.c
            if (r9 >= r10) goto L_0x1663
        L_0x006e:
            int r9 = r1.length()
            int r9 = r9 - r6
            if (r7 > r9) goto L_0x1663
            char r9 = r1.charAt(r7)
            r10 = 199(0xc7, float:2.79E-43)
            r11 = 83
            if (r9 == r10) goto L_0x1641
            r10 = 209(0xd1, float:2.93E-43)
            if (r9 == r10) goto L_0x161b
            r10 = 84
            java.lang.String r12 = "AEIOUY"
            r13 = 72
            r15 = 74
            r4 = 3
            r14 = 2
            switch(r9) {
                case 65: goto L_0x15f3;
                case 66: goto L_0x15c3;
                case 67: goto L_0x1106;
                case 68: goto L_0x1035;
                case 69: goto L_0x15f3;
                case 70: goto L_0x1008;
                case 71: goto L_0x0bc8;
                case 72: goto L_0x0b8e;
                case 73: goto L_0x15f3;
                case 74: goto L_0x0a36;
                case 75: goto L_0x0a09;
                case 76: goto L_0x0947;
                case 77: goto L_0x08f0;
                case 78: goto L_0x08c2;
                case 79: goto L_0x15f3;
                case 80: goto L_0x085f;
                case 81: goto L_0x0830;
                case 82: goto L_0x07c7;
                case 83: goto L_0x0479;
                case 84: goto L_0x0352;
                case 85: goto L_0x15f3;
                case 86: goto L_0x0323;
                case 87: goto L_0x01e7;
                case 88: goto L_0x0147;
                case 89: goto L_0x15f3;
                case 90: goto L_0x0095;
                default: goto L_0x0090;
            }
        L_0x0090:
            int r7 = r7 + 1
        L_0x0092:
            r4 = 75
            goto L_0x005a
        L_0x0095:
            int r9 = r7 + 1
            char r12 = a(r1, r9)
            if (r12 != r13) goto L_0x00be
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00ac
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
        L_0x00ac:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r15)
        L_0x00bb:
            int r7 = r7 + 2
            goto L_0x0092
        L_0x00be:
            java.lang.String[] r4 = new java.lang.String[r4]
            java.lang.String r12 = "ZO"
            r4[r5] = r12
            java.lang.String r12 = "ZI"
            r4[r6] = r12
            java.lang.String r12 = "ZA"
            r4[r14] = r12
            boolean r4 = a(r1, r9, r14, r4)
            if (r4 != 0) goto L_0x00fe
            if (r2 == 0) goto L_0x00df
            if (r7 <= 0) goto L_0x00df
            int r4 = r7 + -1
            char r4 = a(r1, r4)
            if (r4 == r10) goto L_0x00df
            goto L_0x00fe
        L_0x00df:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x00ee
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x00ee:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x013a
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x013a
        L_0x00fe:
            int r4 = r8.c
            java.lang.StringBuilder r10 = r8.f4072a
            int r10 = r10.length()
            int r4 = r4 - r10
            if (r6 > r4) goto L_0x0111
            java.lang.StringBuilder r4 = r8.f4072a
            java.lang.String r10 = "S"
            r4.append(r10)
            goto L_0x011c
        L_0x0111:
            java.lang.StringBuilder r10 = r8.f4072a
            java.lang.String r11 = "S"
            java.lang.String r4 = r11.substring(r5, r4)
            r10.append(r4)
        L_0x011c:
            int r4 = r8.c
            java.lang.StringBuilder r10 = r8.f4073b
            int r10 = r10.length()
            int r4 = r4 - r10
            if (r14 > r4) goto L_0x012f
            java.lang.StringBuilder r4 = r8.f4073b
            java.lang.String r10 = "TS"
            r4.append(r10)
            goto L_0x013a
        L_0x012f:
            java.lang.StringBuilder r10 = r8.f4073b
            java.lang.String r11 = "TS"
            java.lang.String r4 = r11.substring(r5, r4)
            r10.append(r4)
        L_0x013a:
            char r4 = a(r1, r9)
            r10 = 90
            if (r4 != r10) goto L_0x0144
            int r9 = r7 + 2
        L_0x0144:
            r7 = r9
            goto L_0x0092
        L_0x0147:
            if (r7 != 0) goto L_0x0169
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0158
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x0158:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x0090
        L_0x0169:
            int r9 = r1.length()
            int r9 = r9 - r6
            if (r7 != r9) goto L_0x0194
            int r9 = r7 + -3
            java.lang.String[] r10 = new java.lang.String[r14]
            java.lang.String r11 = "IAU"
            r10[r5] = r11
            java.lang.String r11 = "EAU"
            r10[r6] = r11
            boolean r4 = a(r1, r9, r4, r10)
            if (r4 != 0) goto L_0x01d0
            int r4 = r7 + -2
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r10 = "AU"
            r9[r5] = r10
            java.lang.String r10 = "OU"
            r9[r6] = r10
            boolean r4 = a(r1, r4, r14, r9)
            if (r4 != 0) goto L_0x01d0
        L_0x0194:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x01a7
            java.lang.StringBuilder r4 = r8.f4072a
            java.lang.String r9 = "KS"
            r4.append(r9)
            goto L_0x01b2
        L_0x01a7:
            java.lang.StringBuilder r9 = r8.f4072a
            java.lang.String r10 = "KS"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x01b2:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x01c5
            java.lang.StringBuilder r4 = r8.f4073b
            java.lang.String r9 = "KS"
            r4.append(r9)
            goto L_0x01d0
        L_0x01c5:
            java.lang.StringBuilder r9 = r8.f4073b
            java.lang.String r10 = "KS"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x01d0:
            int r4 = r7 + 1
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r10 = "C"
            r9[r5] = r10
            java.lang.String r10 = "X"
            r9[r6] = r10
            boolean r9 = a(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x01e4
            int r4 = r7 + 2
        L_0x01e4:
            r7 = r4
            goto L_0x0092
        L_0x01e7:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "WR"
            r9[r5] = r10
            boolean r9 = a(r1, r7, r14, r9)
            if (r9 == 0) goto L_0x0217
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0204
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 82
            r4.append(r9)
        L_0x0204:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 82
            r4.append(r9)
            goto L_0x00bb
        L_0x0217:
            if (r7 != 0) goto L_0x0283
            int r9 = r7 + 1
            char r10 = a(r1, r9)
            int r10 = r12.indexOf(r10)
            if (r10 != r3) goto L_0x0231
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "WH"
            r10[r5] = r11
            boolean r10 = a(r1, r7, r14, r10)
            if (r10 == 0) goto L_0x0283
        L_0x0231:
            char r4 = a(r1, r9)
            int r4 = r12.indexOf(r4)
            if (r4 == r3) goto L_0x025f
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x024c
            java.lang.StringBuilder r4 = r8.f4072a
            r7 = 65
            r4.append(r7)
        L_0x024c:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x0144
            java.lang.StringBuilder r4 = r8.f4073b
            r7 = 70
            r4.append(r7)
            goto L_0x0144
        L_0x025f:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x0270
            java.lang.StringBuilder r4 = r8.f4072a
            r7 = 65
            r4.append(r7)
        L_0x0270:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x0144
            java.lang.StringBuilder r4 = r8.f4073b
            r7 = 65
            r4.append(r7)
            goto L_0x0144
        L_0x0283:
            int r9 = r1.length()
            int r9 = r9 - r6
            if (r7 != r9) goto L_0x0296
            int r9 = r7 + -1
            char r9 = a(r1, r9)
            int r9 = r12.indexOf(r9)
            if (r9 != r3) goto L_0x0310
        L_0x0296:
            int r9 = r7 + -1
            r10 = 5
            r11 = 4
            java.lang.String[] r12 = new java.lang.String[r11]
            java.lang.String r11 = "EWSKI"
            r12[r5] = r11
            java.lang.String r11 = "EWSKY"
            r12[r6] = r11
            java.lang.String r11 = "OWSKI"
            r12[r14] = r11
            java.lang.String r11 = "OWSKY"
            r12[r4] = r11
            boolean r9 = a(r1, r9, r10, r12)
            if (r9 != 0) goto L_0x0310
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "SCH"
            r9[r5] = r10
            boolean r4 = a(r1, r5, r4, r9)
            if (r4 == 0) goto L_0x02bf
            goto L_0x0310
        L_0x02bf:
            java.lang.String[] r4 = new java.lang.String[r14]
            java.lang.String r9 = "WICZ"
            r4[r5] = r9
            java.lang.String r9 = "WITZ"
            r4[r6] = r9
            r9 = 4
            boolean r4 = a(r1, r7, r9, r4)
            if (r4 == 0) goto L_0x0090
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x02e3
            java.lang.StringBuilder r4 = r8.f4072a
            java.lang.String r9 = "TS"
            r4.append(r9)
            goto L_0x02ee
        L_0x02e3:
            java.lang.StringBuilder r9 = r8.f4072a
            java.lang.String r10 = "TS"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x02ee:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x0301
            java.lang.StringBuilder r4 = r8.f4073b
            java.lang.String r9 = "FX"
            r4.append(r9)
            goto L_0x030c
        L_0x0301:
            java.lang.StringBuilder r9 = r8.f4073b
            java.lang.String r10 = "FX"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x030c:
            int r7 = r7 + 4
            goto L_0x0092
        L_0x0310:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 70
            r4.append(r9)
            goto L_0x0090
        L_0x0323:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0335
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 70
            r4.append(r9)
            goto L_0x0337
        L_0x0335:
            r9 = 70
        L_0x0337:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x0346
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
        L_0x0346:
            int r4 = r7 + 1
            char r9 = a(r1, r4)
            r10 = 86
            if (r9 != r10) goto L_0x01e4
            goto L_0x00bb
        L_0x0352:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "TION"
            r9[r5] = r11
            r11 = 4
            boolean r9 = a(r1, r7, r11, r9)
            if (r9 == 0) goto L_0x0386
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0371
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 88
            r4.append(r9)
            goto L_0x0373
        L_0x0371:
            r9 = 88
        L_0x0373:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x0382
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
        L_0x0382:
            int r7 = r7 + 3
            goto L_0x0092
        L_0x0386:
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r11 = "TIA"
            r9[r5] = r11
            java.lang.String r11 = "TCH"
            r9[r6] = r11
            boolean r9 = a(r1, r7, r4, r9)
            if (r9 == 0) goto L_0x03ba
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x03a8
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 88
            r4.append(r9)
            goto L_0x03aa
        L_0x03a8:
            r9 = 88
        L_0x03aa:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x0382
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x0382
        L_0x03ba:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "TH"
            r9[r5] = r11
            boolean r9 = a(r1, r7, r14, r9)
            if (r9 != 0) goto L_0x0407
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "TTH"
            r9[r5] = r11
            boolean r9 = a(r1, r7, r4, r9)
            if (r9 == 0) goto L_0x03d3
            goto L_0x0407
        L_0x03d3:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x03e2
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r10)
        L_0x03e2:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x03f1
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r10)
        L_0x03f1:
            int r4 = r7 + 1
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r10 = "T"
            r9[r5] = r10
            java.lang.String r10 = "D"
            r9[r6] = r10
            boolean r9 = a(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x01e4
            int r4 = r7 + 2
            goto L_0x01e4
        L_0x0407:
            int r7 = r7 + 2
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r11 = "OM"
            r9[r5] = r11
            java.lang.String r11 = "AM"
            r9[r6] = r11
            boolean r9 = a(r1, r7, r14, r9)
            if (r9 != 0) goto L_0x0459
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r11 = "VAN "
            r9[r5] = r11
            java.lang.String r11 = "VON "
            r9[r6] = r11
            r11 = 4
            boolean r9 = a(r1, r5, r11, r9)
            if (r9 != 0) goto L_0x0459
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "SCH"
            r9[r5] = r11
            boolean r4 = a(r1, r5, r4, r9)
            if (r4 == 0) goto L_0x0437
            goto L_0x0459
        L_0x0437:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0448
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 48
            r4.append(r9)
        L_0x0448:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0092
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r10)
            goto L_0x0092
        L_0x0459:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0468
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r10)
        L_0x0468:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0092
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r10)
            goto L_0x0092
        L_0x0479:
            int r9 = r7 + -1
            java.lang.String[] r10 = new java.lang.String[r14]
            java.lang.String r15 = "ISL"
            r10[r5] = r15
            java.lang.String r15 = "YSL"
            r10[r6] = r15
            boolean r9 = a(r1, r9, r4, r10)
            if (r9 == 0) goto L_0x048d
        L_0x048b:
            goto L_0x0090
        L_0x048d:
            if (r7 != 0) goto L_0x04bd
            r9 = 5
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r15 = "SUGAR"
            r10[r5] = r15
            boolean r9 = a(r1, r7, r9, r10)
            if (r9 == 0) goto L_0x04bd
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x04ad
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 88
            r4.append(r9)
        L_0x04ad:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x048b
        L_0x04bd:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "SH"
            r9[r5] = r10
            boolean r9 = a(r1, r7, r14, r9)
            if (r9 == 0) goto L_0x0529
            int r9 = r7 + 1
            r10 = 4
            java.lang.String[] r12 = new java.lang.String[r10]
            java.lang.String r13 = "HEIM"
            r12[r5] = r13
            java.lang.String r13 = "HOEK"
            r12[r6] = r13
            java.lang.String r13 = "HOLM"
            r12[r14] = r13
            java.lang.String r13 = "HOLZ"
            r12[r4] = r13
            boolean r4 = a(r1, r9, r10, r12)
            if (r4 == 0) goto L_0x0504
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x04f3
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x04f3:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x00bb
        L_0x0504:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0516
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 88
            r4.append(r9)
            goto L_0x0518
        L_0x0516:
            r9 = 88
        L_0x0518:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x00bb
        L_0x0529:
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r10 = "SIO"
            r9[r5] = r10
            java.lang.String r10 = "SIA"
            r9[r6] = r10
            boolean r9 = a(r1, r7, r4, r9)
            if (r9 != 0) goto L_0x0783
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "SIAN"
            r9[r5] = r10
            r10 = 4
            boolean r9 = a(r1, r7, r10, r9)
            if (r9 == 0) goto L_0x0548
            goto L_0x0783
        L_0x0548:
            if (r7 != 0) goto L_0x0564
            int r9 = r7 + 1
            java.lang.String[] r15 = new java.lang.String[r10]
            java.lang.String r10 = "M"
            r15[r5] = r10
            java.lang.String r10 = "N"
            r15[r6] = r10
            java.lang.String r10 = "L"
            r15[r14] = r10
            java.lang.String r10 = "W"
            r15[r4] = r10
            boolean r9 = a(r1, r9, r6, r15)
            if (r9 != 0) goto L_0x0572
        L_0x0564:
            int r9 = r7 + 1
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r15 = "Z"
            r10[r5] = r15
            boolean r10 = a(r1, r9, r6, r10)
            if (r10 == 0) goto L_0x05a4
        L_0x0572:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0581
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x0581:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0592
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 88
            r4.append(r9)
        L_0x0592:
            int r4 = r7 + 1
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "Z"
            r9[r5] = r10
            boolean r9 = a(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x01e4
            int r4 = r7 + 2
            goto L_0x01e4
        L_0x05a4:
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r15 = "SC"
            r10[r5] = r15
            boolean r10 = a(r1, r7, r14, r10)
            if (r10 == 0) goto L_0x0728
            int r9 = r7 + 2
            char r10 = a(r1, r9)
            java.lang.String r15 = "SK"
            if (r10 != r13) goto L_0x06bd
            int r9 = r7 + 3
            r10 = 6
            java.lang.String[] r10 = new java.lang.String[r10]
            java.lang.String r13 = "OO"
            r10[r5] = r13
            java.lang.String r13 = "ER"
            r10[r6] = r13
            java.lang.String r13 = "EN"
            r10[r14] = r13
            java.lang.String r13 = "UY"
            r10[r4] = r13
            java.lang.String r13 = "ED"
            r16 = 4
            r10[r16] = r13
            r13 = 5
            java.lang.String r16 = "EM"
            r10[r13] = r16
            boolean r10 = a(r1, r9, r14, r10)
            if (r10 == 0) goto L_0x0662
            java.lang.String[] r4 = new java.lang.String[r14]
            java.lang.String r10 = "ER"
            r4[r5] = r10
            java.lang.String r10 = "EN"
            r4[r6] = r10
            boolean r4 = a(r1, r9, r14, r4)
            if (r4 == 0) goto L_0x062b
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r6 > r4) goto L_0x0603
            java.lang.StringBuilder r4 = r8.f4072a
            java.lang.String r9 = "X"
            r4.append(r9)
            goto L_0x060e
        L_0x0603:
            java.lang.StringBuilder r9 = r8.f4072a
            java.lang.String r10 = "X"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x060e:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x0620
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r15)
            goto L_0x0382
        L_0x0620:
            java.lang.StringBuilder r9 = r8.f4073b
            java.lang.String r4 = r15.substring(r5, r4)
            r9.append(r4)
            goto L_0x0382
        L_0x062b:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x063c
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
            goto L_0x0645
        L_0x063c:
            java.lang.StringBuilder r9 = r8.f4072a
            java.lang.String r4 = r15.substring(r5, r4)
            r9.append(r4)
        L_0x0645:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x0657
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r15)
            goto L_0x0382
        L_0x0657:
            java.lang.StringBuilder r9 = r8.f4073b
            java.lang.String r4 = r15.substring(r5, r4)
            r9.append(r4)
            goto L_0x0382
        L_0x0662:
            if (r7 != 0) goto L_0x0698
            char r9 = a(r1, r4)
            int r9 = r12.indexOf(r9)
            if (r9 != r3) goto L_0x0698
            char r4 = a(r1, r4)
            r9 = 87
            if (r4 == r9) goto L_0x0698
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0687
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 88
            r4.append(r9)
        L_0x0687:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0382
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x0382
        L_0x0698:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x06aa
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 88
            r4.append(r9)
            goto L_0x06ac
        L_0x06aa:
            r9 = 88
        L_0x06ac:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x0382
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x0382
        L_0x06bd:
            java.lang.String[] r4 = new java.lang.String[r4]
            java.lang.String r10 = "I"
            r4[r5] = r10
            java.lang.String r10 = "E"
            r4[r6] = r10
            java.lang.String r10 = "Y"
            r4[r14] = r10
            boolean r4 = a(r1, r9, r6, r4)
            if (r4 == 0) goto L_0x06f1
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x06e0
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x06e0:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0382
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x0382
        L_0x06f1:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x0702
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
            goto L_0x070b
        L_0x0702:
            java.lang.StringBuilder r9 = r8.f4072a
            java.lang.String r4 = r15.substring(r5, r4)
            r9.append(r4)
        L_0x070b:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x071d
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r15)
            goto L_0x0382
        L_0x071d:
            java.lang.StringBuilder r9 = r8.f4073b
            java.lang.String r4 = r15.substring(r5, r4)
            r9.append(r4)
            goto L_0x0382
        L_0x0728:
            int r4 = r1.length()
            int r4 = r4 - r6
            if (r7 != r4) goto L_0x0751
            int r4 = r7 + -2
            java.lang.String[] r10 = new java.lang.String[r14]
            java.lang.String r12 = "AI"
            r10[r5] = r12
            java.lang.String r12 = "OI"
            r10[r6] = r12
            boolean r4 = a(r1, r4, r14, r10)
            if (r4 == 0) goto L_0x0751
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x076f
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x076f
        L_0x0751:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x0760
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x0760:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x076f
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
        L_0x076f:
            java.lang.String[] r4 = new java.lang.String[r14]
            java.lang.String r10 = "S"
            r4[r5] = r10
            java.lang.String r10 = "Z"
            r4[r6] = r10
            boolean r4 = a(r1, r9, r6, r4)
            if (r4 == 0) goto L_0x0144
            int r9 = r7 + 2
            goto L_0x0144
        L_0x0783:
            if (r2 == 0) goto L_0x07a5
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0794
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x0794:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0382
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x0382
        L_0x07a5:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x07b4
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x07b4:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0382
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 88
            r4.append(r9)
            goto L_0x0382
        L_0x07c7:
            int r4 = r1.length()
            int r4 = r4 - r6
            if (r7 != r4) goto L_0x0802
            if (r2 != 0) goto L_0x0802
            int r4 = r7 + -2
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "IE"
            r9[r5] = r10
            boolean r4 = a(r1, r4, r14, r9)
            if (r4 == 0) goto L_0x0802
            int r4 = r7 + -4
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r10 = "ME"
            r9[r5] = r10
            java.lang.String r10 = "MA"
            r9[r6] = r10
            boolean r4 = a(r1, r4, r14, r9)
            if (r4 != 0) goto L_0x0802
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0824
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 82
            r4.append(r9)
            goto L_0x0824
        L_0x0802:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0813
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 82
            r4.append(r9)
        L_0x0813:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0824
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 82
            r4.append(r9)
        L_0x0824:
            int r4 = r7 + 1
            char r9 = a(r1, r4)
            r10 = 82
            if (r9 != r10) goto L_0x01e4
            goto L_0x00bb
        L_0x0830:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0842
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
            goto L_0x0844
        L_0x0842:
            r9 = 75
        L_0x0844:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x0853
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
        L_0x0853:
            int r4 = r7 + 1
            char r9 = a(r1, r4)
            r10 = 81
            if (r9 != r10) goto L_0x01e4
            goto L_0x00bb
        L_0x085f:
            int r4 = r7 + 1
            char r9 = a(r1, r4)
            if (r9 != r13) goto L_0x088c
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0879
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 70
            r4.append(r9)
            goto L_0x087b
        L_0x0879:
            r9 = 70
        L_0x087b:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x00bb
        L_0x088c:
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r10 = r8.c
            if (r9 >= r10) goto L_0x089d
            java.lang.StringBuilder r9 = r8.f4072a
            r10 = 80
            r9.append(r10)
        L_0x089d:
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r10 = r8.c
            if (r9 >= r10) goto L_0x08ae
            java.lang.StringBuilder r9 = r8.f4073b
            r10 = 80
            r9.append(r10)
        L_0x08ae:
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r10 = "P"
            r9[r5] = r10
            java.lang.String r10 = "B"
            r9[r6] = r10
            boolean r9 = a(r1, r4, r6, r9)
            if (r9 == 0) goto L_0x01e4
            int r4 = r7 + 2
            goto L_0x01e4
        L_0x08c2:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x08d3
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 78
            r4.append(r9)
        L_0x08d3:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x08e4
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 78
            r4.append(r9)
        L_0x08e4:
            int r4 = r7 + 1
            char r9 = a(r1, r4)
            r10 = 78
            if (r9 != r10) goto L_0x01e4
            goto L_0x00bb
        L_0x08f0:
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r10 = r8.c
            if (r9 >= r10) goto L_0x0901
            java.lang.StringBuilder r9 = r8.f4072a
            r10 = 77
            r9.append(r10)
        L_0x0901:
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r10 = r8.c
            if (r9 >= r10) goto L_0x0912
            java.lang.StringBuilder r9 = r8.f4073b
            r10 = 77
            r9.append(r10)
        L_0x0912:
            int r9 = r7 + 1
            char r10 = a(r1, r9)
            r11 = 77
            if (r10 != r11) goto L_0x091e
        L_0x091c:
            r4 = 1
            goto L_0x0943
        L_0x091e:
            int r10 = r7 + -1
            java.lang.String[] r11 = new java.lang.String[r6]
            java.lang.String r12 = "UMB"
            r11[r5] = r12
            boolean r4 = a(r1, r10, r4, r11)
            if (r4 == 0) goto L_0x0942
            int r4 = r1.length()
            int r4 = r4 - r6
            if (r9 == r4) goto L_0x091c
            int r4 = r7 + 2
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "ER"
            r10[r5] = r11
            boolean r4 = a(r1, r4, r14, r10)
            if (r4 == 0) goto L_0x0942
            goto L_0x091c
        L_0x0942:
            r4 = 0
        L_0x0943:
            if (r4 == 0) goto L_0x0144
            goto L_0x00bb
        L_0x0947:
            int r9 = r7 + 1
            char r10 = a(r1, r9)
            r11 = 76
            if (r10 != r11) goto L_0x09e5
            int r9 = r1.length()
            int r9 = r9 - r4
            if (r7 != r9) goto L_0x0971
            int r9 = r7 + -1
            java.lang.String[] r4 = new java.lang.String[r4]
            java.lang.String r10 = "ILLO"
            r4[r5] = r10
            java.lang.String r10 = "ILLA"
            r4[r6] = r10
            java.lang.String r10 = "ALLE"
            r4[r14] = r10
            r10 = 4
            boolean r4 = a(r1, r9, r10, r4)
            if (r4 == 0) goto L_0x0971
        L_0x096f:
            r4 = 1
            goto L_0x09ac
        L_0x0971:
            int r4 = r1.length()
            int r4 = r4 - r14
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r10 = "AS"
            r9[r5] = r10
            java.lang.String r10 = "OS"
            r9[r6] = r10
            boolean r4 = a(r1, r4, r14, r9)
            if (r4 != 0) goto L_0x099b
            int r4 = r1.length()
            int r4 = r4 - r6
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r10 = "A"
            r9[r5] = r10
            java.lang.String r10 = "O"
            r9[r6] = r10
            boolean r4 = a(r1, r4, r6, r9)
            if (r4 == 0) goto L_0x09ab
        L_0x099b:
            int r4 = r7 + -1
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "ALLE"
            r9[r5] = r10
            r10 = 4
            boolean r4 = a(r1, r4, r10, r9)
            if (r4 == 0) goto L_0x09ab
            goto L_0x096f
        L_0x09ab:
            r4 = 0
        L_0x09ac:
            if (r4 == 0) goto L_0x09c1
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 76
            r4.append(r9)
            goto L_0x00bb
        L_0x09c1:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x09d2
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 76
            r4.append(r9)
        L_0x09d2:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 76
            r4.append(r9)
            goto L_0x00bb
        L_0x09e5:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x09f6
            java.lang.StringBuilder r4 = r8.f4072a
            r7 = 76
            r4.append(r7)
        L_0x09f6:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x0144
            java.lang.StringBuilder r4 = r8.f4073b
            r7 = 76
            r4.append(r7)
            goto L_0x0144
        L_0x0a09:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0a1b
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
            goto L_0x0a1d
        L_0x0a1b:
            r9 = 75
        L_0x0a1d:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x0a2c
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
        L_0x0a2c:
            int r4 = r7 + 1
            char r10 = a(r1, r4)
            if (r10 != r9) goto L_0x01e4
            goto L_0x00bb
        L_0x0a36:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "JOSE"
            r9[r5] = r10
            r10 = 4
            boolean r9 = a(r1, r7, r10, r9)
            if (r9 != 0) goto L_0x0b2e
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "SAN "
            r9[r5] = r11
            boolean r9 = a(r1, r5, r10, r9)
            if (r9 == 0) goto L_0x0a51
            goto L_0x0b2e
        L_0x0a51:
            if (r7 != 0) goto L_0x0a81
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "JOSE"
            r9[r5] = r11
            boolean r9 = a(r1, r7, r10, r9)
            if (r9 != 0) goto L_0x0a81
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0a6e
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
        L_0x0a6e:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0b24
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 65
            r4.append(r9)
            goto L_0x0b24
        L_0x0a81:
            int r9 = r7 + -1
            char r10 = a(r1, r9)
            int r10 = r12.indexOf(r10)
            if (r10 == r3) goto L_0x0ac0
            if (r2 != 0) goto L_0x0ac0
            int r10 = r7 + 1
            char r11 = a(r1, r10)
            r12 = 65
            if (r11 == r12) goto L_0x0aa1
            char r10 = a(r1, r10)
            r11 = 79
            if (r10 != r11) goto L_0x0ac0
        L_0x0aa1:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0ab0
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
        L_0x0ab0:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0b24
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r13)
            goto L_0x0b24
        L_0x0ac0:
            int r10 = r1.length()
            int r10 = r10 - r6
            if (r7 != r10) goto L_0x0ae8
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0ad6
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
        L_0x0ad6:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0b24
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 32
            r4.append(r9)
            goto L_0x0b24
        L_0x0ae8:
            int r10 = r7 + 1
            java.lang.String[] r11 = com.helpshift.support.g.a.e
            boolean r10 = a(r1, r10, r6, r11)
            if (r10 != 0) goto L_0x0b24
            java.lang.String[] r4 = new java.lang.String[r4]
            java.lang.String r10 = "S"
            r4[r5] = r10
            java.lang.String r10 = "K"
            r4[r6] = r10
            java.lang.String r10 = "L"
            r4[r14] = r10
            boolean r4 = a(r1, r9, r6, r4)
            if (r4 != 0) goto L_0x0b24
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0b15
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
        L_0x0b15:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0b24
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r15)
        L_0x0b24:
            int r4 = r7 + 1
            char r9 = a(r1, r4)
            if (r9 != r15) goto L_0x01e4
            goto L_0x00bb
        L_0x0b2e:
            if (r7 != 0) goto L_0x0b3a
            int r4 = r7 + 4
            char r4 = a(r1, r4)
            r9 = 32
            if (r4 == r9) goto L_0x0b6e
        L_0x0b3a:
            int r4 = r1.length()
            r9 = 4
            if (r4 == r9) goto L_0x0b6e
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r10 = "SAN "
            r4[r5] = r10
            boolean r4 = a(r1, r5, r9, r4)
            if (r4 == 0) goto L_0x0b4e
            goto L_0x0b6e
        L_0x0b4e:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0b5d
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
        L_0x0b5d:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r13)
            goto L_0x0090
        L_0x0b6e:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0b7d
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r13)
        L_0x0b7d:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r13)
            goto L_0x0090
        L_0x0b8e:
            if (r7 == 0) goto L_0x0b9c
            int r4 = r7 + -1
            char r4 = a(r1, r4)
            int r4 = r12.indexOf(r4)
            if (r4 == r3) goto L_0x0090
        L_0x0b9c:
            int r4 = r7 + 1
            char r4 = a(r1, r4)
            int r4 = r12.indexOf(r4)
            if (r4 == r3) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0bb7
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r13)
        L_0x0bb7:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r13)
            goto L_0x00bb
        L_0x0bc8:
            int r9 = r7 + 1
            char r10 = a(r1, r9)
            if (r10 != r13) goto L_0x0d1c
            if (r7 <= 0) goto L_0x0c03
            int r9 = r7 + -1
            char r9 = a(r1, r9)
            int r9 = r12.indexOf(r9)
            if (r9 != r3) goto L_0x0c03
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0bf0
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
            goto L_0x0bf2
        L_0x0bf0:
            r9 = 75
        L_0x0bf2:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x00bb
        L_0x0c03:
            if (r7 != 0) goto L_0x0c54
            int r7 = r7 + 2
            char r4 = a(r1, r7)
            r9 = 73
            if (r4 != r9) goto L_0x0c2f
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0c1e
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
        L_0x0c1e:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0092
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r15)
            goto L_0x0092
        L_0x0c2f:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0c41
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
            goto L_0x0c43
        L_0x0c41:
            r9 = 75
        L_0x0c43:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x0092
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x0092
        L_0x0c54:
            if (r7 <= r6) goto L_0x0c6c
            int r9 = r7 + -2
            java.lang.String[] r10 = new java.lang.String[r4]
            java.lang.String r11 = "B"
            r10[r5] = r11
            java.lang.String r11 = "H"
            r10[r6] = r11
            java.lang.String r11 = "D"
            r10[r14] = r11
            boolean r9 = a(r1, r9, r6, r10)
            if (r9 != 0) goto L_0x00bb
        L_0x0c6c:
            if (r7 <= r14) goto L_0x0c84
            int r9 = r7 + -3
            java.lang.String[] r10 = new java.lang.String[r4]
            java.lang.String r11 = "B"
            r10[r5] = r11
            java.lang.String r11 = "H"
            r10[r6] = r11
            java.lang.String r11 = "D"
            r10[r14] = r11
            boolean r9 = a(r1, r9, r6, r10)
            if (r9 != 0) goto L_0x00bb
        L_0x0c84:
            if (r7 <= r4) goto L_0x0c9a
            int r9 = r7 + -4
            java.lang.String[] r10 = new java.lang.String[r14]
            java.lang.String r11 = "B"
            r10[r5] = r11
            java.lang.String r11 = "H"
            r10[r6] = r11
            boolean r9 = a(r1, r9, r6, r10)
            if (r9 == 0) goto L_0x0c9a
            goto L_0x00bb
        L_0x0c9a:
            if (r7 <= r14) goto L_0x0ceb
            int r9 = r7 + -1
            char r9 = a(r1, r9)
            r10 = 85
            if (r9 != r10) goto L_0x0ceb
            int r9 = r7 + -3
            r10 = 5
            java.lang.String[] r10 = new java.lang.String[r10]
            java.lang.String r11 = "C"
            r10[r5] = r11
            java.lang.String r11 = "G"
            r10[r6] = r11
            java.lang.String r11 = "L"
            r10[r14] = r11
            java.lang.String r11 = "R"
            r10[r4] = r11
            java.lang.String r4 = "T"
            r11 = 4
            r10[r11] = r4
            boolean r4 = a(r1, r9, r6, r10)
            if (r4 == 0) goto L_0x0ceb
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0cd8
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 70
            r4.append(r9)
            goto L_0x0cda
        L_0x0cd8:
            r9 = 70
        L_0x0cda:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x00bb
        L_0x0ceb:
            if (r7 <= 0) goto L_0x00bb
            int r4 = r7 + -1
            char r4 = a(r1, r4)
            r9 = 73
            if (r4 == r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0d09
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
            goto L_0x0d0b
        L_0x0d09:
            r9 = 75
        L_0x0d0b:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x00bb
        L_0x0d1c:
            char r10 = a(r1, r9)
            r11 = 78
            if (r10 != r11) goto L_0x0e07
            if (r7 != r6) goto L_0x0d71
            char r4 = a(r1, r5)
            int r4 = r12.indexOf(r4)
            if (r4 == r3) goto L_0x0d71
            if (r2 != 0) goto L_0x0d71
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x0d45
            java.lang.StringBuilder r4 = r8.f4072a
            java.lang.String r9 = "KN"
            r4.append(r9)
            goto L_0x0d50
        L_0x0d45:
            java.lang.StringBuilder r9 = r8.f4072a
            java.lang.String r10 = "KN"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0d50:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r6 > r4) goto L_0x0d64
            java.lang.StringBuilder r4 = r8.f4073b
            java.lang.String r9 = "N"
            r4.append(r9)
            goto L_0x00bb
        L_0x0d64:
            java.lang.StringBuilder r9 = r8.f4073b
            java.lang.String r10 = "N"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
            goto L_0x00bb
        L_0x0d71:
            int r4 = r7 + 2
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "EY"
            r10[r5] = r11
            boolean r4 = a(r1, r4, r14, r10)
            if (r4 != 0) goto L_0x0dc8
            char r4 = a(r1, r9)
            r9 = 89
            if (r4 == r9) goto L_0x0dc8
            if (r2 != 0) goto L_0x0dc8
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r6 > r4) goto L_0x0d9c
            java.lang.StringBuilder r4 = r8.f4072a
            java.lang.String r9 = "N"
            r4.append(r9)
            goto L_0x0da7
        L_0x0d9c:
            java.lang.StringBuilder r9 = r8.f4072a
            java.lang.String r10 = "N"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0da7:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x0dbb
            java.lang.StringBuilder r4 = r8.f4073b
            java.lang.String r9 = "KN"
            r4.append(r9)
            goto L_0x00bb
        L_0x0dbb:
            java.lang.StringBuilder r9 = r8.f4073b
            java.lang.String r10 = "KN"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
            goto L_0x00bb
        L_0x0dc8:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x0ddb
            java.lang.StringBuilder r4 = r8.f4072a
            java.lang.String r9 = "KN"
            r4.append(r9)
            goto L_0x0de6
        L_0x0ddb:
            java.lang.StringBuilder r9 = r8.f4072a
            java.lang.String r10 = "KN"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0de6:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x0dfa
            java.lang.StringBuilder r4 = r8.f4073b
            java.lang.String r9 = "KN"
            r4.append(r9)
            goto L_0x00bb
        L_0x0dfa:
            java.lang.StringBuilder r9 = r8.f4073b
            java.lang.String r10 = "KN"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
            goto L_0x00bb
        L_0x0e07:
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "LI"
            r10[r5] = r11
            boolean r10 = a(r1, r9, r14, r10)
            if (r10 == 0) goto L_0x0e54
            if (r2 != 0) goto L_0x0e54
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x0e28
            java.lang.StringBuilder r4 = r8.f4072a
            java.lang.String r9 = "KL"
            r4.append(r9)
            goto L_0x0e33
        L_0x0e28:
            java.lang.StringBuilder r9 = r8.f4072a
            java.lang.String r10 = "KL"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x0e33:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r6 > r4) goto L_0x0e47
            java.lang.StringBuilder r4 = r8.f4073b
            java.lang.String r9 = "L"
            r4.append(r9)
            goto L_0x00bb
        L_0x0e47:
            java.lang.StringBuilder r9 = r8.f4073b
            java.lang.String r10 = "L"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
            goto L_0x00bb
        L_0x0e54:
            if (r7 != 0) goto L_0x0e88
            char r10 = a(r1, r9)
            r11 = 89
            if (r10 == r11) goto L_0x0e66
            java.lang.String[] r10 = com.helpshift.support.g.a.d
            boolean r10 = a(r1, r9, r14, r10)
            if (r10 == 0) goto L_0x0e88
        L_0x0e66:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0e77
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
        L_0x0e77:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r15)
            goto L_0x00bb
        L_0x0e88:
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "ER"
            r10[r5] = r11
            boolean r10 = a(r1, r9, r14, r10)
            if (r10 != 0) goto L_0x0e9c
            char r10 = a(r1, r9)
            r11 = 89
            if (r10 != r11) goto L_0x0ef5
        L_0x0e9c:
            r10 = 6
            java.lang.String[] r11 = new java.lang.String[r4]
            java.lang.String r12 = "DANGER"
            r11[r5] = r12
            java.lang.String r12 = "RANGER"
            r11[r6] = r12
            java.lang.String r12 = "MANGER"
            r11[r14] = r12
            boolean r10 = a(r1, r5, r10, r11)
            if (r10 != 0) goto L_0x0ef5
            int r10 = r7 + -1
            java.lang.String[] r11 = new java.lang.String[r14]
            java.lang.String r12 = "E"
            r11[r5] = r12
            java.lang.String r12 = "I"
            r11[r6] = r12
            boolean r11 = a(r1, r10, r6, r11)
            if (r11 != 0) goto L_0x0ef5
            java.lang.String[] r11 = new java.lang.String[r14]
            java.lang.String r12 = "RGY"
            r11[r5] = r12
            java.lang.String r12 = "OGY"
            r11[r6] = r12
            boolean r10 = a(r1, r10, r4, r11)
            if (r10 != 0) goto L_0x0ef5
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0ee4
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
        L_0x0ee4:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r15)
            goto L_0x00bb
        L_0x0ef5:
            java.lang.String[] r10 = new java.lang.String[r4]
            java.lang.String r11 = "E"
            r10[r5] = r11
            java.lang.String r11 = "I"
            r10[r6] = r11
            java.lang.String r11 = "Y"
            r10[r14] = r11
            boolean r10 = a(r1, r9, r6, r10)
            if (r10 != 0) goto L_0x0f6e
            int r10 = r7 + -1
            java.lang.String[] r11 = new java.lang.String[r14]
            java.lang.String r12 = "AGGI"
            r11[r5] = r12
            java.lang.String r12 = "OGGI"
            r11[r6] = r12
            r12 = 4
            boolean r10 = a(r1, r10, r12, r11)
            if (r10 == 0) goto L_0x0f1d
            goto L_0x0f6e
        L_0x0f1d:
            char r4 = a(r1, r9)
            r10 = 71
            if (r4 != r10) goto L_0x0f4c
            int r7 = r7 + 2
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0f39
            java.lang.StringBuilder r4 = r8.f4072a
            r10 = 75
            r4.append(r10)
            goto L_0x0f3b
        L_0x0f39:
            r10 = 75
        L_0x0f3b:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0092
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r10)
            goto L_0x0092
        L_0x0f4c:
            r10 = 75
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x0f5d
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r10)
        L_0x0f5d:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x0144
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r10)
            goto L_0x0144
        L_0x0f6e:
            java.lang.String[] r10 = new java.lang.String[r14]
            java.lang.String r11 = "VAN "
            r10[r5] = r11
            java.lang.String r11 = "VON "
            r10[r6] = r11
            r11 = 4
            boolean r10 = a(r1, r5, r11, r10)
            if (r10 != 0) goto L_0x0fe6
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "SCH"
            r10[r5] = r11
            boolean r10 = a(r1, r5, r4, r10)
            if (r10 != 0) goto L_0x0fe6
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "ET"
            r10[r5] = r11
            boolean r10 = a(r1, r9, r14, r10)
            if (r10 == 0) goto L_0x0f98
            goto L_0x0fe6
        L_0x0f98:
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "IER"
            r10[r5] = r11
            boolean r4 = a(r1, r9, r4, r10)
            if (r4 == 0) goto L_0x0fc4
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0fb3
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
        L_0x0fb3:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r15)
            goto L_0x00bb
        L_0x0fc4:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0fd3
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
        L_0x0fd3:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 75
            r4.append(r9)
            goto L_0x00bb
        L_0x0fe6:
            r9 = 75
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x0ff7
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r9)
        L_0x0ff7:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x00bb
        L_0x1008:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x101a
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 70
            r4.append(r9)
            goto L_0x101c
        L_0x101a:
            r9 = 70
        L_0x101c:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x102b
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
        L_0x102b:
            int r4 = r7 + 1
            char r10 = a(r1, r4)
            if (r10 != r9) goto L_0x01e4
            goto L_0x00bb
        L_0x1035:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r11 = "DG"
            r9[r5] = r11
            boolean r9 = a(r1, r7, r14, r9)
            if (r9 == 0) goto L_0x10b6
            int r9 = r7 + 2
            java.lang.String[] r4 = new java.lang.String[r4]
            java.lang.String r10 = "I"
            r4[r5] = r10
            java.lang.String r10 = "E"
            r4[r6] = r10
            java.lang.String r10 = "Y"
            r4[r14] = r10
            boolean r4 = a(r1, r9, r6, r4)
            if (r4 == 0) goto L_0x1077
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x1066
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r15)
        L_0x1066:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0382
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r15)
            goto L_0x0382
        L_0x1077:
            int r4 = r8.c
            java.lang.StringBuilder r7 = r8.f4072a
            int r7 = r7.length()
            int r4 = r4 - r7
            if (r14 > r4) goto L_0x108a
            java.lang.StringBuilder r4 = r8.f4072a
            java.lang.String r7 = "TK"
            r4.append(r7)
            goto L_0x1095
        L_0x108a:
            java.lang.StringBuilder r7 = r8.f4072a
            java.lang.String r10 = "TK"
            java.lang.String r4 = r10.substring(r5, r4)
            r7.append(r4)
        L_0x1095:
            int r4 = r8.c
            java.lang.StringBuilder r7 = r8.f4073b
            int r7 = r7.length()
            int r4 = r4 - r7
            if (r14 > r4) goto L_0x10a9
            java.lang.StringBuilder r4 = r8.f4073b
            java.lang.String r7 = "TK"
            r4.append(r7)
            goto L_0x0144
        L_0x10a9:
            java.lang.StringBuilder r7 = r8.f4073b
            java.lang.String r10 = "TK"
            java.lang.String r4 = r10.substring(r5, r4)
            r7.append(r4)
            goto L_0x0144
        L_0x10b6:
            java.lang.String[] r4 = new java.lang.String[r14]
            java.lang.String r9 = "DT"
            r4[r5] = r9
            java.lang.String r9 = "DD"
            r4[r6] = r9
            boolean r4 = a(r1, r7, r14, r4)
            if (r4 == 0) goto L_0x10e6
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x10d5
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r10)
        L_0x10d5:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x00bb
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r10)
            goto L_0x00bb
        L_0x10e6:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x10f5
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r10)
        L_0x10f5:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r10)
            goto L_0x0090
        L_0x1106:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "CHIA"
            r9[r5] = r10
            r10 = 4
            boolean r9 = a(r1, r7, r10, r9)
            if (r9 == 0) goto L_0x1115
        L_0x1113:
            r9 = 1
            goto L_0x1155
        L_0x1115:
            if (r7 > r6) goto L_0x1119
        L_0x1117:
            r9 = 0
            goto L_0x1155
        L_0x1119:
            int r9 = r7 + -2
            char r10 = a(r1, r9)
            int r10 = r12.indexOf(r10)
            if (r10 == r3) goto L_0x1126
            goto L_0x1117
        L_0x1126:
            int r10 = r7 + -1
            java.lang.String[] r12 = new java.lang.String[r6]
            java.lang.String r13 = "ACH"
            r12[r5] = r13
            boolean r10 = a(r1, r10, r4, r12)
            if (r10 != 0) goto L_0x1135
            goto L_0x1117
        L_0x1135:
            int r10 = r7 + 2
            char r10 = a(r1, r10)
            r12 = 73
            if (r10 == r12) goto L_0x1143
            r12 = 69
            if (r10 != r12) goto L_0x1113
        L_0x1143:
            r10 = 6
            java.lang.String[] r12 = new java.lang.String[r14]
            java.lang.String r13 = "BACHER"
            r12[r5] = r13
            java.lang.String r13 = "MACHER"
            r12[r6] = r13
            boolean r9 = a(r1, r9, r10, r12)
            if (r9 == 0) goto L_0x1117
            goto L_0x1113
        L_0x1155:
            if (r9 == 0) goto L_0x1180
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x1169
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
            goto L_0x116b
        L_0x1169:
            r9 = 75
        L_0x116b:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x117a
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
        L_0x117a:
            int r7 = r7 + 2
        L_0x117c:
            r12 = 75
            goto L_0x0092
        L_0x1180:
            if (r7 != 0) goto L_0x11ae
            r9 = 6
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r12 = "CAESAR"
            r10[r5] = r12
            boolean r9 = a(r1, r7, r9, r10)
            if (r9 == 0) goto L_0x11ae
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x119e
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x119e:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x117a
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x117a
        L_0x11ae:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "CH"
            r9[r5] = r10
            boolean r9 = a(r1, r7, r14, r9)
            if (r9 == 0) goto L_0x136b
            if (r7 == 0) goto L_0x11be
        L_0x11bc:
            r9 = 0
            goto L_0x11fa
        L_0x11be:
            int r9 = r7 + 1
            r10 = 5
            java.lang.String[] r11 = new java.lang.String[r14]
            java.lang.String r12 = "HARAC"
            r11[r5] = r12
            java.lang.String r12 = "HARIS"
            r11[r6] = r12
            boolean r10 = a(r1, r9, r10, r11)
            if (r10 != 0) goto L_0x11eb
            r10 = 4
            java.lang.String[] r11 = new java.lang.String[r10]
            java.lang.String r10 = "HOR"
            r11[r5] = r10
            java.lang.String r10 = "HYM"
            r11[r6] = r10
            java.lang.String r10 = "HIA"
            r11[r14] = r10
            java.lang.String r10 = "HEM"
            r11[r4] = r10
            boolean r9 = a(r1, r9, r4, r11)
            if (r9 != 0) goto L_0x11eb
            goto L_0x11bc
        L_0x11eb:
            r9 = 5
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "CHORE"
            r10[r5] = r11
            boolean r9 = a(r1, r5, r9, r10)
            if (r9 == 0) goto L_0x11f9
            goto L_0x11bc
        L_0x11f9:
            r9 = 1
        L_0x11fa:
            if (r7 <= 0) goto L_0x122d
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r11 = "CHAE"
            r10[r5] = r11
            r11 = 4
            boolean r10 = a(r1, r7, r11, r10)
            if (r10 == 0) goto L_0x122d
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x121a
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
        L_0x121a:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x117a
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 88
            r4.append(r9)
            goto L_0x117a
        L_0x122d:
            if (r9 == 0) goto L_0x1254
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x1241
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
            goto L_0x1243
        L_0x1241:
            r9 = 75
        L_0x1243:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x117a
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x117a
        L_0x1254:
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r10 = "VAN "
            r9[r5] = r10
            java.lang.String r10 = "VON "
            r9[r6] = r10
            r10 = 4
            boolean r9 = a(r1, r5, r10, r9)
            if (r9 != 0) goto L_0x1346
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "SCH"
            r9[r5] = r10
            boolean r9 = a(r1, r5, r4, r9)
            if (r9 != 0) goto L_0x1346
            int r9 = r7 + -2
            r10 = 6
            java.lang.String[] r11 = new java.lang.String[r4]
            java.lang.String r12 = "ORCHES"
            r11[r5] = r12
            java.lang.String r12 = "ARCHIT"
            r11[r6] = r12
            java.lang.String r12 = "ORCHID"
            r11[r14] = r12
            boolean r9 = a(r1, r9, r10, r11)
            if (r9 != 0) goto L_0x1346
            int r9 = r7 + 2
            java.lang.String[] r10 = new java.lang.String[r14]
            java.lang.String r11 = "T"
            r10[r5] = r11
            java.lang.String r11 = "S"
            r10[r6] = r11
            boolean r10 = a(r1, r9, r6, r10)
            if (r10 != 0) goto L_0x1346
            int r10 = r7 + -1
            r11 = 4
            java.lang.String[] r11 = new java.lang.String[r11]
            java.lang.String r12 = "A"
            r11[r5] = r12
            java.lang.String r12 = "O"
            r11[r6] = r12
            java.lang.String r12 = "U"
            r11[r14] = r12
            java.lang.String r12 = "E"
            r11[r4] = r12
            boolean r4 = a(r1, r10, r6, r11)
            if (r4 != 0) goto L_0x12b7
            if (r7 != 0) goto L_0x12ca
        L_0x12b7:
            java.lang.String[] r4 = com.helpshift.support.g.a.c
            boolean r4 = a(r1, r9, r6, r4)
            if (r4 != 0) goto L_0x1346
            int r4 = r7 + 1
            int r10 = r1.length()
            int r10 = r10 - r6
            if (r4 != r10) goto L_0x12ca
            goto L_0x1346
        L_0x12ca:
            if (r7 <= 0) goto L_0x1321
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r7 = "MC"
            r4[r5] = r7
            boolean r4 = a(r1, r5, r14, r4)
            if (r4 == 0) goto L_0x12fd
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x12ea
            java.lang.StringBuilder r4 = r8.f4072a
            r7 = 75
            r4.append(r7)
            goto L_0x12ec
        L_0x12ea:
            r7 = 75
        L_0x12ec:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x14ba
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r7)
            goto L_0x14ba
        L_0x12fd:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x130e
            java.lang.StringBuilder r4 = r8.f4072a
            r7 = 88
            r4.append(r7)
        L_0x130e:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x14ba
            java.lang.StringBuilder r4 = r8.f4073b
            r7 = 75
            r4.append(r7)
            goto L_0x14ba
        L_0x1321:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x1333
            java.lang.StringBuilder r4 = r8.f4072a
            r7 = 88
            r4.append(r7)
            goto L_0x1335
        L_0x1333:
            r7 = 88
        L_0x1335:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x14ba
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r7)
            goto L_0x14ba
        L_0x1346:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x1358
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
            goto L_0x135a
        L_0x1358:
            r9 = 75
        L_0x135a:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x117a
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x117a
        L_0x136b:
            java.lang.String[] r9 = new java.lang.String[r6]
            java.lang.String r10 = "CZ"
            r9[r5] = r10
            boolean r9 = a(r1, r7, r14, r9)
            if (r9 == 0) goto L_0x13a8
            int r9 = r7 + -2
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r12 = "WICZ"
            r10[r5] = r12
            r12 = 4
            boolean r9 = a(r1, r9, r12, r10)
            if (r9 != 0) goto L_0x13a8
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x1395
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x1395:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x117a
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 88
            r4.append(r9)
            goto L_0x117a
        L_0x13a8:
            int r9 = r7 + 1
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r12 = "CIA"
            r10[r5] = r12
            boolean r10 = a(r1, r9, r4, r10)
            if (r10 == 0) goto L_0x13dd
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x13c8
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 88
            r4.append(r9)
            goto L_0x13ca
        L_0x13c8:
            r9 = 88
        L_0x13ca:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x13d9
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
        L_0x13d9:
            int r7 = r7 + 3
            goto L_0x117c
        L_0x13dd:
            java.lang.String[] r10 = new java.lang.String[r6]
            java.lang.String r12 = "CC"
            r10[r5] = r12
            boolean r10 = a(r1, r7, r14, r10)
            if (r10 == 0) goto L_0x14bd
            if (r7 != r6) goto L_0x13f3
            char r10 = a(r1, r5)
            r12 = 77
            if (r10 == r12) goto L_0x14bd
        L_0x13f3:
            int r9 = r7 + 2
            java.lang.String[] r4 = new java.lang.String[r4]
            java.lang.String r10 = "I"
            r4[r5] = r10
            java.lang.String r10 = "E"
            r4[r6] = r10
            java.lang.String r10 = "H"
            r4[r14] = r10
            boolean r4 = a(r1, r9, r6, r4)
            if (r4 == 0) goto L_0x1497
            java.lang.String[] r4 = new java.lang.String[r6]
            java.lang.String r10 = "HU"
            r4[r5] = r10
            boolean r4 = a(r1, r9, r14, r4)
            if (r4 != 0) goto L_0x1497
            if (r7 != r6) goto L_0x1421
            int r4 = r7 + -1
            char r4 = a(r1, r4)
            r9 = 65
            if (r4 == r9) goto L_0x1434
        L_0x1421:
            int r4 = r7 + -1
            r9 = 5
            java.lang.String[] r10 = new java.lang.String[r14]
            java.lang.String r11 = "UCCEE"
            r10[r5] = r11
            java.lang.String r11 = "UCCES"
            r10[r6] = r11
            boolean r4 = a(r1, r4, r9, r10)
            if (r4 == 0) goto L_0x1471
        L_0x1434:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4072a
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x1447
            java.lang.StringBuilder r4 = r8.f4072a
            java.lang.String r9 = "KS"
            r4.append(r9)
            goto L_0x1452
        L_0x1447:
            java.lang.StringBuilder r9 = r8.f4072a
            java.lang.String r10 = "KS"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
        L_0x1452:
            int r4 = r8.c
            java.lang.StringBuilder r9 = r8.f4073b
            int r9 = r9.length()
            int r4 = r4 - r9
            if (r14 > r4) goto L_0x1465
            java.lang.StringBuilder r4 = r8.f4073b
            java.lang.String r9 = "KS"
            r4.append(r9)
            goto L_0x1494
        L_0x1465:
            java.lang.StringBuilder r9 = r8.f4073b
            java.lang.String r10 = "KS"
            java.lang.String r4 = r10.substring(r5, r4)
            r9.append(r4)
            goto L_0x1494
        L_0x1471:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x1483
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 88
            r4.append(r9)
            goto L_0x1485
        L_0x1483:
            r9 = 88
        L_0x1485:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x1494
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
        L_0x1494:
            int r9 = r7 + 3
            goto L_0x14ba
        L_0x1497:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r7 = r8.c
            if (r4 >= r7) goto L_0x14a9
            java.lang.StringBuilder r4 = r8.f4072a
            r7 = 75
            r4.append(r7)
            goto L_0x14ab
        L_0x14a9:
            r7 = 75
        L_0x14ab:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x14ba
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r7)
        L_0x14ba:
            r7 = r9
            goto L_0x117c
        L_0x14bd:
            java.lang.String[] r10 = new java.lang.String[r4]
            java.lang.String r12 = "CK"
            r10[r5] = r12
            java.lang.String r12 = "CG"
            r10[r6] = r12
            java.lang.String r12 = "CQ"
            r10[r14] = r12
            boolean r10 = a(r1, r7, r14, r10)
            if (r10 == 0) goto L_0x14f6
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x14e3
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 75
            r4.append(r9)
            goto L_0x14e5
        L_0x14e3:
            r9 = 75
        L_0x14e5:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r10 = r8.c
            if (r4 >= r10) goto L_0x117a
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r9)
            goto L_0x117a
        L_0x14f6:
            java.lang.String[] r10 = new java.lang.String[r4]
            java.lang.String r12 = "CI"
            r10[r5] = r12
            java.lang.String r12 = "CE"
            r10[r6] = r12
            java.lang.String r12 = "CY"
            r10[r14] = r12
            boolean r10 = a(r1, r7, r14, r10)
            if (r10 == 0) goto L_0x1560
            java.lang.String[] r9 = new java.lang.String[r4]
            java.lang.String r10 = "CIO"
            r9[r5] = r10
            java.lang.String r10 = "CIE"
            r9[r6] = r10
            java.lang.String r10 = "CIA"
            r9[r14] = r10
            boolean r4 = a(r1, r7, r4, r9)
            if (r4 == 0) goto L_0x1540
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x152d
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x152d:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x117a
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 88
            r4.append(r9)
            goto L_0x117a
        L_0x1540:
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x154f
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x154f:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x117a
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x117a
        L_0x1560:
            java.lang.StringBuilder r10 = r8.f4072a
            int r10 = r10.length()
            int r11 = r8.c
            if (r10 >= r11) goto L_0x1572
            java.lang.StringBuilder r10 = r8.f4072a
            r12 = 75
            r10.append(r12)
            goto L_0x1574
        L_0x1572:
            r12 = 75
        L_0x1574:
            java.lang.StringBuilder r10 = r8.f4073b
            int r10 = r10.length()
            int r11 = r8.c
            if (r10 >= r11) goto L_0x1583
            java.lang.StringBuilder r10 = r8.f4073b
            r10.append(r12)
        L_0x1583:
            java.lang.String[] r10 = new java.lang.String[r4]
            java.lang.String r11 = " C"
            r10[r5] = r11
            java.lang.String r11 = " Q"
            r10[r6] = r11
            java.lang.String r11 = " G"
            r10[r14] = r11
            boolean r10 = a(r1, r9, r14, r10)
            if (r10 == 0) goto L_0x159b
            int r9 = r7 + 3
            goto L_0x0144
        L_0x159b:
            java.lang.String[] r4 = new java.lang.String[r4]
            java.lang.String r10 = "C"
            r4[r5] = r10
            java.lang.String r10 = "K"
            r4[r6] = r10
            java.lang.String r10 = "Q"
            r4[r14] = r10
            boolean r4 = a(r1, r9, r6, r4)
            if (r4 == 0) goto L_0x0144
            java.lang.String[] r4 = new java.lang.String[r14]
            java.lang.String r10 = "CE"
            r4[r5] = r10
            java.lang.String r10 = "CI"
            r4[r6] = r10
            boolean r4 = a(r1, r9, r14, r4)
            if (r4 != 0) goto L_0x0144
            int r9 = r7 + 2
            goto L_0x0144
        L_0x15c3:
            r12 = 75
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x15d6
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 80
            r4.append(r9)
        L_0x15d6:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x15e7
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 80
            r4.append(r9)
        L_0x15e7:
            int r4 = r7 + 1
            char r9 = a(r1, r4)
            r10 = 66
            if (r9 != r10) goto L_0x01e4
            goto L_0x00bb
        L_0x15f3:
            r12 = 75
            if (r7 != 0) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x1608
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 65
            r4.append(r9)
        L_0x1608:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 65
            r4.append(r9)
            goto L_0x0090
        L_0x161b:
            r12 = 75
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x162e
            java.lang.StringBuilder r4 = r8.f4072a
            r9 = 78
            r4.append(r9)
        L_0x162e:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4073b
            r9 = 78
            r4.append(r9)
            goto L_0x0090
        L_0x1641:
            r12 = 75
            java.lang.StringBuilder r4 = r8.f4072a
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x1652
            java.lang.StringBuilder r4 = r8.f4072a
            r4.append(r11)
        L_0x1652:
            java.lang.StringBuilder r4 = r8.f4073b
            int r4 = r4.length()
            int r9 = r8.c
            if (r4 >= r9) goto L_0x0090
            java.lang.StringBuilder r4 = r8.f4073b
            r4.append(r11)
            goto L_0x0090
        L_0x1663:
            java.lang.StringBuilder r1 = r8.f4072a
            java.lang.String r1 = r1.toString()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.g.a.a(java.lang.String, boolean):java.lang.String");
    }

    private static char a(String str, int i) {
        if (i < 0 || i >= str.length()) {
            return 0;
        }
        return str.charAt(i);
    }

    /* renamed from: com.helpshift.support.g.a$a  reason: collision with other inner class name */
    /* compiled from: DoubleMetaphone */
    public class C0145a {

        /* renamed from: a  reason: collision with root package name */
        final StringBuilder f4072a = new StringBuilder(a.this.f4071a);

        /* renamed from: b  reason: collision with root package name */
        final StringBuilder f4073b = new StringBuilder(a.this.f4071a);
        final int c;

        public C0145a(int i) {
            this.c = i;
        }
    }
}
