package com.helpshift.support.k.b;

import android.util.Pair;
import android.util.SparseArray;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* compiled from: PageIndexTrieNode */
public class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public final char f4153a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f4154b;
    SparseArray<Pair<Integer, Integer>> c = new SparseArray<>();
    List<b> d = new ArrayList();

    public b(char c2) {
        this.f4153a = c2;
    }

    public final b a(char c2) {
        List<b> list = this.d;
        if (list == null) {
            return null;
        }
        for (b next : list) {
            if (next.f4153a == c2) {
                return next;
            }
        }
        return null;
    }

    public final void a(int i, int i2, int i3) {
        Pair pair;
        Pair pair2 = this.c.get(i);
        if (pair2 == null) {
            pair = new Pair(Integer.valueOf(i2), Integer.valueOf(i3));
        } else {
            pair = new Pair(Integer.valueOf(((Integer) pair2.first).intValue() + i2), Integer.valueOf(((Integer) pair2.second).intValue()));
        }
        this.c.put(i, pair);
    }

    public final void a(b bVar) {
        this.d.add(bVar);
    }
}
