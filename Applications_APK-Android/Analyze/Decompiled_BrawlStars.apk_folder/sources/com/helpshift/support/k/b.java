package com.helpshift.support.k;

import java.util.Map;

/* compiled from: SearchTokenDto */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public final String f4149a;

    /* renamed from: b  reason: collision with root package name */
    public final int f4150b;
    public final Map<Integer, Double> c;

    public b(String str, int i, Map<Integer, Double> map) {
        this.f4149a = str;
        this.f4150b = i;
        this.c = map;
    }
}
