package com.helpshift.support.k.b;

import android.util.Pair;
import android.util.SparseArray;
import com.helpshift.support.k.a.b;
import com.helpshift.support.k.b;
import com.helpshift.support.s;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: PageIndexTrie */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private final int f4151a;

    /* renamed from: b  reason: collision with root package name */
    private b f4152b = new b(0);
    private List<b> c = new ArrayList();
    private com.helpshift.support.k.a d;

    public a(int i) {
        this.f4151a = i;
    }

    public final void a(String str, int i, int i2) {
        if (str != null && 50 >= str.length()) {
            int length = str.length();
            b bVar = this.f4152b;
            for (int i3 = 0; i3 < length; i3++) {
                char charAt = str.charAt(i3);
                b a2 = bVar.a(charAt);
                if (a2 == null) {
                    a2 = new b(charAt);
                    bVar.a(a2);
                }
                bVar = a2;
                if (i != 50 && i3 > 1 && i3 < 10 && i3 + 1 != length) {
                    bVar.f4154b = true;
                    bVar.a(i2, (s.a(i3, i) * i3) / length, i);
                }
            }
            bVar.f4154b = true;
            bVar.a(i2, s.a(length, i), i);
        }
    }

    private void a(b bVar, char[] cArr, int i) {
        if (bVar != null) {
            cArr[i] = bVar.f4153a;
            if (bVar.f4154b) {
                this.c.add(a(new String(cArr, 0, i + 1), bVar));
                if (this.c.size() > 1000) {
                    this.d.a(this.c);
                    this.c.clear();
                }
            }
            for (b a2 : bVar.d) {
                a(a2, cArr, i + 1);
            }
            bVar.d = null;
        }
    }

    public final void a() {
        this.d = b.a.f4148a;
        char[] cArr = new char[50];
        for (b a2 : this.f4152b.d) {
            a(a2, cArr, 0);
        }
        if (this.c.size() > 0) {
            this.d.a(this.c);
        }
    }

    private com.helpshift.support.k.b a(String str, b bVar) {
        b bVar2 = bVar;
        int i = 0;
        for (int i2 = 0; i2 < bVar2.c.size(); i2++) {
            i = Math.max(i, ((Integer) bVar2.c.valueAt(i2).first).intValue());
        }
        int size = bVar2.c.size();
        HashMap hashMap = new HashMap();
        SparseArray<Pair<Integer, Integer>> sparseArray = bVar2.c;
        int i3 = -1;
        for (int i4 = 0; i4 < sparseArray.size(); i4++) {
            int keyAt = sparseArray.keyAt(i4);
            Pair valueAt = sparseArray.valueAt(i4);
            double intValue = (double) ((Integer) valueAt.first).intValue();
            double d2 = (double) i;
            Double.isNaN(intValue);
            Double.isNaN(d2);
            double d3 = (double) this.f4151a;
            double d4 = (double) size;
            Double.isNaN(d3);
            Double.isNaN(d4);
            double log10 = (intValue / d2) * Math.log10(d3 / d4);
            double a2 = (double) s.a(((Integer) valueAt.second).intValue());
            Double.isNaN(a2);
            hashMap.put(Integer.valueOf(keyAt), Double.valueOf(log10 * a2));
            i3 = Math.max(i3, ((Integer) valueAt.second).intValue());
        }
        bVar2.c = null;
        return new com.helpshift.support.k.b(str, i3, hashMap);
    }
}
