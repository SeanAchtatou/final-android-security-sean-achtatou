package com.helpshift.support.k.a;

import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.util.Map;

/* compiled from: SearchTokenDaoImpl */
public class b implements com.helpshift.support.k.a {

    /* renamed from: a  reason: collision with root package name */
    private final SQLiteOpenHelper f4146a = new a(q.a());

    /* renamed from: b  reason: collision with root package name */
    private final char f4147b = '$';
    private final char c = ':';

    /* compiled from: SearchTokenDaoImpl */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public static final com.helpshift.support.k.a f4148a = new b();
    }

    b() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x008b A[SYNTHETIC, Splitter:B:36:0x008b] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x009f A[SYNTHETIC, Splitter:B:46:0x009f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.util.List<com.helpshift.support.k.b> r7) {
        /*
            r6 = this;
            if (r7 != 0) goto L_0x0003
            return
        L_0x0003:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r7 = r7.iterator()
        L_0x000c:
            boolean r1 = r7.hasNext()
            if (r1 == 0) goto L_0x003e
            java.lang.Object r1 = r7.next()
            com.helpshift.support.k.b r1 = (com.helpshift.support.k.b) r1
            java.util.Map<java.lang.Integer, java.lang.Double> r2 = r1.c
            java.lang.String r2 = a(r2)
            android.content.ContentValues r3 = new android.content.ContentValues
            r3.<init>()
            java.lang.String r4 = r1.f4149a
            java.lang.String r5 = "token"
            r3.put(r5, r4)
            int r1 = r1.f4150b
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.String r4 = "type"
            r3.put(r4, r1)
            java.lang.String r1 = "score"
            r3.put(r1, r2)
            r0.add(r3)
            goto L_0x000c
        L_0x003e:
            android.database.sqlite.SQLiteOpenHelper r7 = r6.f4146a
            monitor-enter(r7)
            r1 = 0
            android.database.sqlite.SQLiteOpenHelper r2 = r6.f4146a     // Catch:{ Exception -> 0x0081 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Exception -> 0x0081 }
            r2.beginTransaction()     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
        L_0x004f:
            boolean r3 = r0.hasNext()     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            if (r3 == 0) goto L_0x0061
            java.lang.Object r3 = r0.next()     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            android.content.ContentValues r3 = (android.content.ContentValues) r3     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            java.lang.String r4 = "search_token_table"
            r2.insert(r4, r1, r3)     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            goto L_0x004f
        L_0x0061:
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x007b, all -> 0x0079 }
            if (r2 == 0) goto L_0x009b
            boolean r0 = r2.inTransaction()     // Catch:{ Exception -> 0x0070 }
            if (r0 == 0) goto L_0x009b
            r2.endTransaction()     // Catch:{ Exception -> 0x0070 }
            goto L_0x009b
        L_0x0070:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_SearchToknDao"
            java.lang.String r2 = "Error occurred when calling save method inside finally block"
        L_0x0075:
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x00a9 }
            goto L_0x009b
        L_0x0079:
            r0 = move-exception
            goto L_0x009d
        L_0x007b:
            r0 = move-exception
            r1 = r2
            goto L_0x0082
        L_0x007e:
            r0 = move-exception
            r2 = r1
            goto L_0x009d
        L_0x0081:
            r0 = move-exception
        L_0x0082:
            java.lang.String r2 = "Helpshift_SearchToknDao"
            java.lang.String r3 = "Error occurred when calling save method"
            com.helpshift.util.n.c(r2, r3, r0)     // Catch:{ all -> 0x007e }
            if (r1 == 0) goto L_0x009b
            boolean r0 = r1.inTransaction()     // Catch:{ Exception -> 0x0095 }
            if (r0 == 0) goto L_0x009b
            r1.endTransaction()     // Catch:{ Exception -> 0x0095 }
            goto L_0x009b
        L_0x0095:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_SearchToknDao"
            java.lang.String r2 = "Error occurred when calling save method inside finally block"
            goto L_0x0075
        L_0x009b:
            monitor-exit(r7)     // Catch:{ all -> 0x00a9 }
            return
        L_0x009d:
            if (r2 == 0) goto L_0x00b3
            boolean r1 = r2.inTransaction()     // Catch:{ Exception -> 0x00ab }
            if (r1 == 0) goto L_0x00b3
            r2.endTransaction()     // Catch:{ Exception -> 0x00ab }
            goto L_0x00b3
        L_0x00a9:
            r0 = move-exception
            goto L_0x00b4
        L_0x00ab:
            r1 = move-exception
            java.lang.String r2 = "Helpshift_SearchToknDao"
            java.lang.String r3 = "Error occurred when calling save method inside finally block"
            com.helpshift.util.n.c(r2, r3, r1)     // Catch:{ all -> 0x00a9 }
        L_0x00b3:
            throw r0     // Catch:{ all -> 0x00a9 }
        L_0x00b4:
            monitor-exit(r7)     // Catch:{ all -> 0x00a9 }
            goto L_0x00b7
        L_0x00b6:
            throw r0
        L_0x00b7:
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.k.a.b.a(java.util.List):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a3, code lost:
        if (r4 != null) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b7, code lost:
        if (r4 != null) goto L_0x00a5;
     */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00bf A[Catch:{ all -> 0x00bc }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.helpshift.support.k.b a(java.lang.String r18) {
        /*
            r17 = this;
            r1 = r17
            android.database.sqlite.SQLiteOpenHelper r2 = r1.f4146a
            monitor-enter(r2)
            r3 = 0
            android.database.sqlite.SQLiteOpenHelper r0 = r1.f4146a     // Catch:{ Exception -> 0x00ae, all -> 0x00ab }
            android.database.sqlite.SQLiteDatabase r4 = r0.getWritableDatabase()     // Catch:{ Exception -> 0x00ae, all -> 0x00ab }
            r0 = 3
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00ae, all -> 0x00ab }
            java.lang.String r0 = "token"
            r12 = 0
            r6[r12] = r0     // Catch:{ Exception -> 0x00ae, all -> 0x00ab }
            java.lang.String r0 = "type"
            r13 = 1
            r6[r13] = r0     // Catch:{ Exception -> 0x00ae, all -> 0x00ab }
            java.lang.String r0 = "score"
            r14 = 2
            r6[r14] = r0     // Catch:{ Exception -> 0x00ae, all -> 0x00ab }
            java.lang.String r5 = "search_token_table"
            java.lang.String r7 = "token=?"
            java.lang.String[] r8 = new java.lang.String[r13]     // Catch:{ Exception -> 0x00ae, all -> 0x00ab }
            r8[r12] = r18     // Catch:{ Exception -> 0x00ae, all -> 0x00ab }
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r4 = r4.query(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x00ae, all -> 0x00ab }
            int r0 = r4.getCount()     // Catch:{ Exception -> 0x00a9 }
            if (r0 <= 0) goto L_0x00a3
            r4.moveToFirst()     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r0 = "token"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r5 = "type"
            int r5 = r4.getColumnIndexOrThrow(r5)     // Catch:{ Exception -> 0x00a9 }
            int r5 = r4.getInt(r5)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r6 = "score"
            int r6 = r4.getColumnIndexOrThrow(r6)     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r6 = r4.getString(r6)     // Catch:{ Exception -> 0x00a9 }
            java.util.HashMap r7 = new java.util.HashMap     // Catch:{ Exception -> 0x00a9 }
            r7.<init>()     // Catch:{ Exception -> 0x00a9 }
            if (r6 != 0) goto L_0x005c
            goto L_0x009d
        L_0x005c:
            java.lang.String r8 = "[$]"
            java.lang.String[] r6 = r6.split(r8)     // Catch:{ Exception -> 0x00a9 }
            r8 = 0
        L_0x0063:
            int r9 = r6.length     // Catch:{ Exception -> 0x00a9 }
            if (r8 >= r9) goto L_0x009d
            r9 = r6[r8]     // Catch:{ Exception -> 0x00a9 }
            if (r9 == 0) goto L_0x009a
            int r10 = r9.length()     // Catch:{ Exception -> 0x00a9 }
            if (r10 <= 0) goto L_0x009a
            java.lang.String r10 = "[:]"
            java.lang.String[] r9 = r9.split(r10)     // Catch:{ Exception -> 0x00a9 }
            if (r9 == 0) goto L_0x009a
            int r10 = r9.length     // Catch:{ Exception -> 0x00a9 }
            if (r10 != r14) goto L_0x009a
            r10 = r9[r12]     // Catch:{ Exception -> 0x00a9 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00a9 }
            int r10 = r10.intValue()     // Catch:{ Exception -> 0x00a9 }
            r9 = r9[r13]     // Catch:{ Exception -> 0x00a9 }
            java.lang.Double r9 = java.lang.Double.valueOf(r9)     // Catch:{ Exception -> 0x00a9 }
            double r15 = r9.doubleValue()     // Catch:{ Exception -> 0x00a9 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00a9 }
            java.lang.Double r10 = java.lang.Double.valueOf(r15)     // Catch:{ Exception -> 0x00a9 }
            r7.put(r9, r10)     // Catch:{ Exception -> 0x00a9 }
        L_0x009a:
            int r8 = r8 + 1
            goto L_0x0063
        L_0x009d:
            com.helpshift.support.k.b r6 = new com.helpshift.support.k.b     // Catch:{ Exception -> 0x00a9 }
            r6.<init>(r0, r5, r7)     // Catch:{ Exception -> 0x00a9 }
            r3 = r6
        L_0x00a3:
            if (r4 == 0) goto L_0x00ba
        L_0x00a5:
            r4.close()     // Catch:{ all -> 0x00c3 }
            goto L_0x00ba
        L_0x00a9:
            r0 = move-exception
            goto L_0x00b0
        L_0x00ab:
            r0 = move-exception
            r4 = r3
            goto L_0x00bd
        L_0x00ae:
            r0 = move-exception
            r4 = r3
        L_0x00b0:
            java.lang.String r5 = "Helpshift_SearchToknDao"
            java.lang.String r6 = "Error occurred when calling get method"
            com.helpshift.util.n.c(r5, r6, r0)     // Catch:{ all -> 0x00bc }
            if (r4 == 0) goto L_0x00ba
            goto L_0x00a5
        L_0x00ba:
            monitor-exit(r2)     // Catch:{ all -> 0x00c3 }
            return r3
        L_0x00bc:
            r0 = move-exception
        L_0x00bd:
            if (r4 == 0) goto L_0x00c2
            r4.close()     // Catch:{ all -> 0x00c3 }
        L_0x00c2:
            throw r0     // Catch:{ all -> 0x00c3 }
        L_0x00c3:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00c3 }
            goto L_0x00c7
        L_0x00c6:
            throw r0
        L_0x00c7:
            goto L_0x00c6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.k.a.b.a(java.lang.String):com.helpshift.support.k.b");
    }

    public final void a() {
        synchronized (this.f4146a) {
            try {
                this.f4146a.getWritableDatabase().delete("search_token_table", null, null);
            } catch (Exception e) {
                n.c("Helpshift_SearchToknDao", "Error occurred when calling clear method", e);
            }
        }
    }

    private static String a(Map<Integer, Double> map) {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Map.Entry next : map.entrySet()) {
            if (z) {
                z = false;
            } else {
                sb.append('$');
            }
            sb.append(next.getKey());
            sb.append(':');
            sb.append(next.getValue());
        }
        return sb.toString();
    }
}
