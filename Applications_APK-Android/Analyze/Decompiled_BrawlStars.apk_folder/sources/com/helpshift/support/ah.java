package com.helpshift.support;

import android.app.Activity;
import java.util.Map;

/* compiled from: Support */
public final class ah implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f3871a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f3872b;
    final /* synthetic */ Map c;

    public ah(Activity activity, String str, Map map) {
        this.f3871a = activity;
        this.f3872b = str;
        this.c = map;
    }

    public final void run() {
        al.a(this.f3871a, this.f3872b, this.c);
    }
}
