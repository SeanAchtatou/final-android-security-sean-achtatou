package com.helpshift.support.l;

import android.content.Context;
import com.helpshift.common.e.aa;
import com.helpshift.y.b;
import com.helpshift.y.e;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

/* compiled from: SupportKeyValueDBStorage */
public class j implements aa {

    /* renamed from: a  reason: collision with root package name */
    private e f4171a;

    public j(Context context) {
        this.f4171a = new b(new l(context), new HashSet(Arrays.asList("sdkLanguage", "disableInAppConversation", "debugLogLimit", "showAgentName", "enableTypingIndicatorAgent", "enableTypingIndicator", "fullPrivacy", "showConversationInfoScreen", "runtimeVersion", "sdkType", "disableAppLaunchEvent", "pluginVersion", "profileFormEnable", "requireNameAndEmail", "requireEmail", "hideNameAndEmail", "gotoConversationAfterContactUs", "showSearchOnNewConversation", "supportNotificationChannelId", "notificationIconId", "notificationLargeIconId", "app_reviewed", "defaultFallbackLanguageEnable", "conversationGreetingMessage", "conversationalIssueFiling", "showConversationResolutionQuestion", "showConversationResolutionQuestionAgent", "allowUserAttachments", "server_time_delta", "disableHelpshiftBrandingAgent", "disableHelpshiftBranding", "periodicReviewEnabled", "periodicReviewInterval", "periodicReviewType", "customerSatisfactionSurvey", "showConversationHistoryAgent", "enableDefaultConversationalFiling")));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.l.j.b(java.lang.String, java.io.Serializable):void
     arg types: [java.lang.String, java.lang.Boolean]
     candidates:
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.support.l.j.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.support.l.j.b(java.lang.String, java.io.Serializable):void */
    public final void a(String str, Boolean bool) {
        b(str, (Serializable) bool);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.l.j.b(java.lang.String, java.io.Serializable):void
     arg types: [java.lang.String, java.lang.Float]
     candidates:
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.support.l.j.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.support.l.j.b(java.lang.String, java.io.Serializable):void */
    public final void a(String str, Float f) {
        b(str, (Serializable) f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.l.j.b(java.lang.String, java.io.Serializable):void
     arg types: [java.lang.String, java.lang.Long]
     candidates:
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.support.l.j.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.support.l.j.b(java.lang.String, java.io.Serializable):void */
    public final void a(String str, Long l) {
        b(str, (Serializable) l);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.l.j.b(java.lang.String, java.io.Serializable):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.support.l.j.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.support.l.j.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.support.l.j.b(java.lang.String, java.io.Serializable):void */
    public final void a(String str, String str2) {
        b(str, (Serializable) str2);
    }

    public final Boolean b(String str, Boolean bool) {
        Object a2 = this.f4171a.a(str);
        if (a2 == null) {
            return bool;
        }
        return (Boolean) a2;
    }

    public final Integer a(String str, Integer num) {
        Object a2 = this.f4171a.a(str);
        if (a2 == null) {
            return num;
        }
        return (Integer) a2;
    }

    public final Float b(String str, Float f) {
        Object a2 = this.f4171a.a(str);
        if (a2 == null) {
            return f;
        }
        return (Float) a2;
    }

    public final Long b(String str, Long l) {
        Object a2 = this.f4171a.a(str);
        if (a2 == null) {
            return l;
        }
        return (Long) a2;
    }

    public final String a(String str) {
        Object a2 = this.f4171a.a(str);
        if (a2 == null) {
            return null;
        }
        return (String) a2;
    }

    public final String b(String str, String str2) {
        Object a2 = this.f4171a.a(str);
        if (a2 == null) {
            return str2;
        }
        return (String) a2;
    }

    public final void a(String str, Serializable serializable) {
        b(str, serializable);
    }

    public final Object b(String str) {
        return this.f4171a.a(str);
    }

    public final void a() {
        this.f4171a.a();
    }

    public final void a(Map<String, Serializable> map) {
        this.f4171a.a(map);
    }

    private void b(String str, Serializable serializable) {
        if (serializable == null) {
            this.f4171a.b(str);
        } else {
            this.f4171a.a(str, serializable);
        }
    }
}
