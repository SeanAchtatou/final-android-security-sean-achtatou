package com.helpshift.support.l;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: SupportKeyValueDBStorageHelper */
class k extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private static final Integer f4172a = 1;

    k(Context context) {
        super(context, "__hs__db_support_key_values", (SQLiteDatabase.CursorFactory) null, f4172a.intValue());
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE key_value_store(key text primary key,value blob not null);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS key_value_store;");
        onCreate(sQLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS key_value_store;");
        onCreate(sQLiteDatabase);
    }
}
