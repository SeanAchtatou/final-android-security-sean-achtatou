package com.helpshift.support.l;

import android.content.Context;
import com.helpshift.util.n;
import com.helpshift.y.a;
import com.helpshift.y.c;

/* compiled from: SupportRetryKeyValueDBStorage */
class l extends a {

    /* renamed from: b  reason: collision with root package name */
    private final Context f4173b;
    private k c;

    l(Context context) {
        this.f4173b = context;
        this.c = new k(context);
        this.f4351a = new c(this.c, null);
    }

    public final void b() {
        try {
            if (this.c != null) {
                this.c.close();
            }
        } catch (Exception e) {
            n.c("Helpshift_RetryKeyValue", "Error in closing DB", e);
        }
        this.c = new k(this.f4173b);
        this.f4351a = new c(this.c, null);
    }
}
