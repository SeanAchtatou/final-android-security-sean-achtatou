package com.helpshift.support.l;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.facebook.internal.ServerProtocol;
import com.helpshift.support.Faq;
import com.helpshift.support.g;
import com.helpshift.util.e;
import com.helpshift.util.j;
import com.helpshift.util.n;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: FaqsDataSource */
public class d implements b {

    /* renamed from: a  reason: collision with root package name */
    private final c f4159a;

    /* compiled from: FaqsDataSource */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public static final d f4160a = new d((byte) 0);
    }

    /* synthetic */ d(byte b2) {
        this();
    }

    private d() {
        this.f4159a = c.a();
    }

    public static void a(SQLiteDatabase sQLiteDatabase, String str, JSONArray jSONArray) {
        int i = 0;
        int i2 = 0;
        while (i2 < jSONArray.length()) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i2);
                ContentValues contentValues = new ContentValues();
                contentValues.put("question_id", jSONObject.getString("id"));
                contentValues.put("publish_id", jSONObject.getString("publish_id"));
                contentValues.put("language", jSONObject.getString("language"));
                contentValues.put("section_id", str);
                contentValues.put("title", jSONObject.getString("title"));
                contentValues.put("body", jSONObject.getString("body"));
                contentValues.put("helpful", Integer.valueOf(i));
                contentValues.put("rtl", Boolean.valueOf(jSONObject.getString("is_rtl").equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)));
                contentValues.put("tags", (jSONObject.has("stags") ? jSONObject.optJSONArray("stags") : new JSONArray()).toString());
                contentValues.put("c_tags", (jSONObject.has("issue_tags") ? jSONObject.optJSONArray("issue_tags") : new JSONArray()).toString());
                sQLiteDatabase.insert("faqs", null, contentValues);
                i2++;
                i = 0;
            } catch (JSONException e) {
                n.a("HelpShiftDebug", "addFaqsUnsafe", e);
                return;
            }
        }
    }

    private static Faq a(Cursor cursor) {
        long j = cursor.getLong(cursor.getColumnIndex("_id"));
        String string = cursor.getString(cursor.getColumnIndex("question_id"));
        String string2 = cursor.getString(cursor.getColumnIndex("publish_id"));
        String string3 = cursor.getString(cursor.getColumnIndex("language"));
        String string4 = cursor.getString(cursor.getColumnIndex("section_id"));
        String string5 = cursor.getString(cursor.getColumnIndex("title"));
        String string6 = cursor.getString(cursor.getColumnIndex("body"));
        int i = cursor.getInt(cursor.getColumnIndex("helpful"));
        boolean z = true;
        if (cursor.getInt(cursor.getColumnIndex("rtl")) != 1) {
            z = false;
        }
        return new Faq(j, string, string2, string3, string4, string5, string6, i, Boolean.valueOf(z), j.a(cursor.getString(cursor.getColumnIndex("tags"))), j.a(cursor.getString(cursor.getColumnIndex("c_tags"))));
    }

    public final synchronized void b() {
        try {
            this.f4159a.a(this.f4159a.getWritableDatabase());
        } catch (Exception e) {
            n.c("HelpShiftDebug", "Error in clearDB", e);
        }
        return;
    }

    public final synchronized void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                this.f4159a.getWritableDatabase().delete("faqs", "publish_id=?", new String[]{str});
            } catch (Exception e) {
                n.c("HelpShiftDebug", "Error in removeFaq", e);
            }
        }
        return;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:13:0x002c */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r1v2, types: [com.helpshift.support.Faq] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARN: Type inference failed for: r1v8 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        if (r11 != null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r11.close();
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003f, code lost:
        if (r11 != null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0043, code lost:
        return r1;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0048 A[SYNTHETIC, Splitter:B:29:0x0048] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.helpshift.support.Faq b(java.lang.String r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            boolean r0 = android.text.TextUtils.isEmpty(r11)     // Catch:{ all -> 0x004c }
            r1 = 0
            if (r0 == 0) goto L_0x000a
            monitor-exit(r10)
            return r1
        L_0x000a:
            com.helpshift.support.l.c r0 = r10.f4159a     // Catch:{ Exception -> 0x0036, all -> 0x0034 }
            android.database.sqlite.SQLiteDatabase r2 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x0036, all -> 0x0034 }
            java.lang.String r3 = "faqs"
            r4 = 0
            java.lang.String r5 = "publish_id = ?"
            r0 = 1
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0036, all -> 0x0034 }
            r0 = 0
            r6[r0] = r11     // Catch:{ Exception -> 0x0036, all -> 0x0034 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0036, all -> 0x0034 }
            boolean r0 = r11.moveToFirst()     // Catch:{ Exception -> 0x0032 }
            if (r0 == 0) goto L_0x002c
            com.helpshift.support.Faq r1 = a(r11)     // Catch:{ Exception -> 0x0032 }
        L_0x002c:
            if (r11 == 0) goto L_0x0042
        L_0x002e:
            r11.close()     // Catch:{ all -> 0x004c }
            goto L_0x0042
        L_0x0032:
            r0 = move-exception
            goto L_0x0038
        L_0x0034:
            r0 = move-exception
            goto L_0x0046
        L_0x0036:
            r0 = move-exception
            r11 = r1
        L_0x0038:
            java.lang.String r2 = "HelpShiftDebug"
            java.lang.String r3 = "Error in getFaq"
            com.helpshift.util.n.c(r2, r3, r0)     // Catch:{ all -> 0x0044 }
            if (r11 == 0) goto L_0x0042
            goto L_0x002e
        L_0x0042:
            monitor-exit(r10)
            return r1
        L_0x0044:
            r0 = move-exception
            r1 = r11
        L_0x0046:
            if (r1 == 0) goto L_0x004b
            r1.close()     // Catch:{ all -> 0x004c }
        L_0x004b:
            throw r0     // Catch:{ all -> 0x004c }
        L_0x004c:
            r11 = move-exception
            monitor-exit(r10)
            goto L_0x0050
        L_0x004f:
            throw r11
        L_0x0050:
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.l.d.b(java.lang.String):com.helpshift.support.Faq");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:12:0x0034 */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r1v2, types: [com.helpshift.support.Faq] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARN: Type inference failed for: r1v8 */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        if (r11 != null) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r11.close();
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0047, code lost:
        if (r11 != null) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004b, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0055, code lost:
        return null;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0050 A[SYNTHETIC, Splitter:B:28:0x0050] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.helpshift.support.Faq a(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            boolean r0 = android.text.TextUtils.isEmpty(r11)     // Catch:{ all -> 0x0056 }
            r1 = 0
            if (r0 != 0) goto L_0x0054
            boolean r0 = android.text.TextUtils.isEmpty(r12)     // Catch:{ all -> 0x0056 }
            if (r0 == 0) goto L_0x000f
            goto L_0x0054
        L_0x000f:
            com.helpshift.support.l.c r0 = r10.f4159a     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            android.database.sqlite.SQLiteDatabase r2 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            java.lang.String r3 = "faqs"
            r4 = 0
            java.lang.String r5 = "publish_id = ? AND language = ?"
            r0 = 2
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            r0 = 0
            r6[r0] = r11     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            r11 = 1
            r6[r11] = r12     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            boolean r12 = r11.moveToFirst()     // Catch:{ Exception -> 0x003a }
            if (r12 == 0) goto L_0x0034
            com.helpshift.support.Faq r1 = a(r11)     // Catch:{ Exception -> 0x003a }
        L_0x0034:
            if (r11 == 0) goto L_0x004a
        L_0x0036:
            r11.close()     // Catch:{ all -> 0x0056 }
            goto L_0x004a
        L_0x003a:
            r12 = move-exception
            goto L_0x0040
        L_0x003c:
            r12 = move-exception
            goto L_0x004e
        L_0x003e:
            r12 = move-exception
            r11 = r1
        L_0x0040:
            java.lang.String r0 = "HelpShiftDebug"
            java.lang.String r2 = "Error in getFaq"
            com.helpshift.util.n.c(r0, r2, r12)     // Catch:{ all -> 0x004c }
            if (r11 == 0) goto L_0x004a
            goto L_0x0036
        L_0x004a:
            monitor-exit(r10)
            return r1
        L_0x004c:
            r12 = move-exception
            r1 = r11
        L_0x004e:
            if (r1 == 0) goto L_0x0053
            r1.close()     // Catch:{ all -> 0x0056 }
        L_0x0053:
            throw r12     // Catch:{ all -> 0x0056 }
        L_0x0054:
            monitor-exit(r10)
            return r1
        L_0x0056:
            r11 = move-exception
            monitor-exit(r10)
            goto L_0x005a
        L_0x0059:
            throw r11
        L_0x005a:
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.l.d.a(java.lang.String, java.lang.String):com.helpshift.support.Faq");
    }

    public final List<Faq> a(String str, g gVar) {
        return a(c(str), gVar);
    }

    public final synchronized int a(String str, Boolean bool) {
        int i;
        i = 0;
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("helpful", Integer.valueOf(bool.booleanValue() ? 1 : -1));
        try {
            i = this.f4159a.getWritableDatabase().update("faqs", contentValues, "question_id = ?", new String[]{str});
        } catch (Exception e) {
            n.c("HelpShiftDebug", "Error in setIsHelpful", e);
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0043, code lost:
        if (r1 != null) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0053, code lost:
        if (r1 == null) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0057, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.helpshift.support.Faq> c(java.lang.String r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            boolean r0 = android.text.TextUtils.isEmpty(r12)     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x000e
            java.util.ArrayList r12 = new java.util.ArrayList     // Catch:{ all -> 0x005e }
            r12.<init>()     // Catch:{ all -> 0x005e }
            monitor-exit(r11)
            return r12
        L_0x000e:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x005e }
            r0.<init>()     // Catch:{ all -> 0x005e }
            r1 = 0
            com.helpshift.support.l.c r2 = r11.f4159a     // Catch:{ Exception -> 0x004b }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x004b }
            java.lang.String r4 = "faqs"
            r5 = 0
            java.lang.String r6 = "section_id = ?"
            r2 = 1
            java.lang.String[] r7 = new java.lang.String[r2]     // Catch:{ Exception -> 0x004b }
            r2 = 0
            r7[r2] = r12     // Catch:{ Exception -> 0x004b }
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r1 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x004b }
            boolean r12 = r1.moveToFirst()     // Catch:{ Exception -> 0x004b }
            if (r12 == 0) goto L_0x0043
        L_0x0032:
            boolean r12 = r1.isAfterLast()     // Catch:{ Exception -> 0x004b }
            if (r12 != 0) goto L_0x0043
            com.helpshift.support.Faq r12 = a(r1)     // Catch:{ Exception -> 0x004b }
            r0.add(r12)     // Catch:{ Exception -> 0x004b }
            r1.moveToNext()     // Catch:{ Exception -> 0x004b }
            goto L_0x0032
        L_0x0043:
            if (r1 == 0) goto L_0x0056
        L_0x0045:
            r1.close()     // Catch:{ all -> 0x005e }
            goto L_0x0056
        L_0x0049:
            r12 = move-exception
            goto L_0x0058
        L_0x004b:
            r12 = move-exception
            java.lang.String r2 = "HelpShiftDebug"
            java.lang.String r3 = "Error in getFaqsDataForSection"
            com.helpshift.util.n.c(r2, r3, r12)     // Catch:{ all -> 0x0049 }
            if (r1 == 0) goto L_0x0056
            goto L_0x0045
        L_0x0056:
            monitor-exit(r11)
            return r0
        L_0x0058:
            if (r1 == 0) goto L_0x005d
            r1.close()     // Catch:{ all -> 0x005e }
        L_0x005d:
            throw r12     // Catch:{ all -> 0x005e }
        L_0x005e:
            r12 = move-exception
            monitor-exit(r11)
            goto L_0x0062
        L_0x0061:
            throw r12
        L_0x0062:
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.l.d.c(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003d, code lost:
        if (r1 != null) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004d, code lost:
        if (r1 == null) goto L_0x0050;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<java.lang.String> a() {
        /*
            r11 = this;
            monitor-enter(r11)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0058 }
            r0.<init>()     // Catch:{ all -> 0x0058 }
            r1 = 0
            r2 = 1
            java.lang.String[] r5 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0045 }
            r2 = 0
            java.lang.String r3 = "publish_id"
            r5[r2] = r3     // Catch:{ Exception -> 0x0045 }
            com.helpshift.support.l.c r2 = r11.f4159a     // Catch:{ Exception -> 0x0045 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0045 }
            java.lang.String r4 = "faqs"
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r1 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0045 }
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x0045 }
            if (r2 == 0) goto L_0x003d
        L_0x0026:
            boolean r2 = r1.isAfterLast()     // Catch:{ Exception -> 0x0045 }
            if (r2 != 0) goto L_0x003d
            java.lang.String r2 = "publish_id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0045 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0045 }
            r0.add(r2)     // Catch:{ Exception -> 0x0045 }
            r1.moveToNext()     // Catch:{ Exception -> 0x0045 }
            goto L_0x0026
        L_0x003d:
            if (r1 == 0) goto L_0x0050
        L_0x003f:
            r1.close()     // Catch:{ all -> 0x0058 }
            goto L_0x0050
        L_0x0043:
            r0 = move-exception
            goto L_0x0052
        L_0x0045:
            r2 = move-exception
            java.lang.String r3 = "HelpShiftDebug"
            java.lang.String r4 = "Error in getFaqsDataForSection"
            com.helpshift.util.n.c(r3, r4, r2)     // Catch:{ all -> 0x0043 }
            if (r1 == 0) goto L_0x0050
            goto L_0x003f
        L_0x0050:
            monitor-exit(r11)
            return r0
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            r1.close()     // Catch:{ all -> 0x0058 }
        L_0x0057:
            throw r0     // Catch:{ all -> 0x0058 }
        L_0x0058:
            r0 = move-exception
            monitor-exit(r11)
            goto L_0x005c
        L_0x005b:
            throw r0
        L_0x005c:
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.l.d.a():java.util.List");
    }

    private static List<Faq> b(List<Faq> list, g gVar) {
        ArrayList arrayList = new ArrayList();
        for (Faq next : list) {
            if (new ArrayList(Arrays.asList(gVar.f4068b)).removeAll(next.b())) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private static List<Faq> c(List<Faq> list, g gVar) {
        ArrayList arrayList = new ArrayList();
        for (Faq next : list) {
            if (!new ArrayList(Arrays.asList(gVar.f4068b)).removeAll(next.b())) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public final synchronized void a(Faq faq) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("question_id", faq.j);
        contentValues.put("publish_id", faq.f3837b);
        contentValues.put("language", faq.c);
        contentValues.put("section_id", faq.d);
        contentValues.put("title", faq.f3836a);
        contentValues.put("body", faq.e);
        contentValues.put("helpful", Integer.valueOf(faq.f));
        contentValues.put("rtl", faq.g);
        contentValues.put("tags", String.valueOf(new JSONArray((Collection) faq.a())));
        contentValues.put("c_tags", String.valueOf(new JSONArray((Collection) faq.b())));
        String[] strArr = {faq.j};
        try {
            SQLiteDatabase writableDatabase = this.f4159a.getWritableDatabase();
            if (!e.a(writableDatabase, "faqs", "question_id=?", strArr)) {
                writableDatabase.insert("faqs", null, contentValues);
            } else {
                writableDatabase.update("faqs", contentValues, "question_id=?", strArr);
            }
        } catch (Exception e) {
            n.c("HelpShiftDebug", "Error in addFaq", e);
        }
        return;
    }

    public final List<Faq> a(List<Faq> list, g gVar) {
        if (gVar == null) {
            return list;
        }
        String str = gVar.f4067a;
        char c = 65535;
        int hashCode = str.hashCode();
        if (hashCode != -1038130864) {
            if (hashCode != 3555) {
                if (hashCode != 96727) {
                    if (hashCode == 109267 && str.equals("not")) {
                        c = 2;
                    }
                } else if (str.equals("and")) {
                    c = 0;
                }
            } else if (str.equals("or")) {
                c = 1;
            }
        } else if (str.equals("undefined")) {
            c = 3;
        }
        if (c == 0) {
            ArrayList arrayList = new ArrayList();
            for (Faq next : list) {
                ArrayList arrayList2 = new ArrayList(Arrays.asList(gVar.f4068b));
                arrayList2.removeAll(next.b());
                if (arrayList2.isEmpty()) {
                    arrayList.add(next);
                }
            }
            return arrayList;
        } else if (c == 1) {
            return b(list, gVar);
        } else {
            if (c == 2) {
                return c(list, gVar);
            }
            if (c != 3) {
            }
            return list;
        }
    }
}
