package com.helpshift.support.l;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.helpshift.support.Section;
import com.helpshift.support.g;
import com.helpshift.support.l.d;
import com.helpshift.util.n;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SectionsDataSource */
public class h implements g {

    /* renamed from: a  reason: collision with root package name */
    private final c f4166a;

    /* renamed from: b  reason: collision with root package name */
    private b f4167b;

    /* compiled from: SectionsDataSource */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public static final h f4168a = new h((byte) 0);
    }

    /* synthetic */ h(byte b2) {
        this();
    }

    private h() {
        this.f4166a = c.a();
        this.f4167b = d.a.f4160a;
    }

    private static Section a(Cursor cursor) {
        return new Section(cursor.getLong(0), cursor.getString(1), cursor.getString(3), cursor.getString(2));
    }

    public final synchronized void a(JSONArray jSONArray) {
        String str;
        String str2;
        SQLiteDatabase writableDatabase = this.f4166a.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                ContentValues contentValues = new ContentValues();
                contentValues.put("title", jSONObject.getString("title"));
                contentValues.put("publish_id", jSONObject.getString("publish_id"));
                contentValues.put("section_id", jSONObject.getString("id"));
                writableDatabase.insert("sections", null, contentValues);
                JSONArray optJSONArray = jSONObject.optJSONArray("faqs");
                if (optJSONArray != null) {
                    d.a(writableDatabase, jSONObject.getString("publish_id"), optJSONArray);
                }
            }
            writableDatabase.setTransactionSuccessful();
            if (writableDatabase != null) {
                try {
                    if (writableDatabase.inTransaction()) {
                        writableDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    e = e;
                    str = "HelpShiftDebug";
                    str2 = "Error in storeSections inside finally block";
                    n.c(str, str2, e);
                }
            }
        } catch (JSONException e2) {
            try {
                n.c("HelpShiftDebug", "Error in storeSections", e2);
                if (writableDatabase != null) {
                    try {
                    } catch (Exception e3) {
                        e = e3;
                        str = "HelpShiftDebug";
                        str2 = "Error in storeSections inside finally block";
                        n.c(str, str2, e);
                    }
                }
            } finally {
                if (writableDatabase != null) {
                    try {
                        if (writableDatabase.inTransaction()) {
                            writableDatabase.endTransaction();
                        }
                    } catch (Exception e4) {
                        n.c("HelpShiftDebug", "Error in storeSections inside finally block", e4);
                    }
                }
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:13:0x0032 */
    /* JADX WARN: Type inference failed for: r0v2 */
    /* JADX WARN: Type inference failed for: r0v3, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r0v4, types: [com.helpshift.support.Section] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARN: Type inference failed for: r0v8 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARN: Type inference failed for: r0v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        if (r12 != null) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r12.close();
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0045, code lost:
        if (r12 != null) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0049, code lost:
        return r0;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0050 A[SYNTHETIC, Splitter:B:29:0x0050] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.helpshift.support.Section a(java.lang.String r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            if (r12 == 0) goto L_0x0054
            java.lang.String r0 = ""
            boolean r0 = r12.equals(r0)     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x000c
            goto L_0x0054
        L_0x000c:
            r0 = 0
            com.helpshift.support.l.c r1 = r11.f4166a     // Catch:{ Exception -> 0x003c, all -> 0x003a }
            android.database.sqlite.SQLiteDatabase r2 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x003c, all -> 0x003a }
            java.lang.String r3 = "sections"
            r4 = 0
            java.lang.String r5 = "publish_id = ?"
            r1 = 1
            java.lang.String[] r6 = new java.lang.String[r1]     // Catch:{ Exception -> 0x003c, all -> 0x003a }
            r1 = 0
            r6[r1] = r12     // Catch:{ Exception -> 0x003c, all -> 0x003a }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r12 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x003c, all -> 0x003a }
            r12.moveToFirst()     // Catch:{ Exception -> 0x0038 }
            boolean r1 = r12.isAfterLast()     // Catch:{ Exception -> 0x0038 }
            if (r1 != 0) goto L_0x0032
            com.helpshift.support.Section r0 = a(r12)     // Catch:{ Exception -> 0x0038 }
        L_0x0032:
            if (r12 == 0) goto L_0x0048
        L_0x0034:
            r12.close()     // Catch:{ all -> 0x005b }
            goto L_0x0048
        L_0x0038:
            r1 = move-exception
            goto L_0x003e
        L_0x003a:
            r12 = move-exception
            goto L_0x004e
        L_0x003c:
            r1 = move-exception
            r12 = r0
        L_0x003e:
            java.lang.String r2 = "HelpShiftDebug"
            java.lang.String r3 = "Error in getSection"
            com.helpshift.util.n.c(r2, r3, r1)     // Catch:{ all -> 0x004a }
            if (r12 == 0) goto L_0x0048
            goto L_0x0034
        L_0x0048:
            monitor-exit(r11)
            return r0
        L_0x004a:
            r0 = move-exception
            r10 = r0
            r0 = r12
            r12 = r10
        L_0x004e:
            if (r0 == 0) goto L_0x0053
            r0.close()     // Catch:{ all -> 0x005b }
        L_0x0053:
            throw r12     // Catch:{ all -> 0x005b }
        L_0x0054:
            com.helpshift.support.Section r12 = new com.helpshift.support.Section     // Catch:{ all -> 0x005b }
            r12.<init>()     // Catch:{ all -> 0x005b }
            monitor-exit(r11)
            return r12
        L_0x005b:
            r12 = move-exception
            monitor-exit(r11)
            goto L_0x005f
        L_0x005e:
            throw r12
        L_0x005f:
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.l.h.a(java.lang.String):com.helpshift.support.Section");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003d, code lost:
        if (r1 == null) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002d, code lost:
        if (r1 != null) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.helpshift.support.Section> a() {
        /*
            r11 = this;
            monitor-enter(r11)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0048 }
            r0.<init>()     // Catch:{ all -> 0x0048 }
            r1 = 0
            com.helpshift.support.l.c r2 = r11.f4166a     // Catch:{ Exception -> 0x0035 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0035 }
            java.lang.String r4 = "sections"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r1 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0035 }
            r1.moveToFirst()     // Catch:{ Exception -> 0x0035 }
        L_0x001c:
            boolean r2 = r1.isAfterLast()     // Catch:{ Exception -> 0x0035 }
            if (r2 != 0) goto L_0x002d
            com.helpshift.support.Section r2 = a(r1)     // Catch:{ Exception -> 0x0035 }
            r0.add(r2)     // Catch:{ Exception -> 0x0035 }
            r1.moveToNext()     // Catch:{ Exception -> 0x0035 }
            goto L_0x001c
        L_0x002d:
            if (r1 == 0) goto L_0x0040
        L_0x002f:
            r1.close()     // Catch:{ all -> 0x0048 }
            goto L_0x0040
        L_0x0033:
            r0 = move-exception
            goto L_0x0042
        L_0x0035:
            r2 = move-exception
            java.lang.String r3 = "HelpShiftDebug"
            java.lang.String r4 = "Error in getAllSections"
            com.helpshift.util.n.c(r3, r4, r2)     // Catch:{ all -> 0x0033 }
            if (r1 == 0) goto L_0x0040
            goto L_0x002f
        L_0x0040:
            monitor-exit(r11)
            return r0
        L_0x0042:
            if (r1 == 0) goto L_0x0047
            r1.close()     // Catch:{ all -> 0x0048 }
        L_0x0047:
            throw r0     // Catch:{ all -> 0x0048 }
        L_0x0048:
            r0 = move-exception
            monitor-exit(r11)
            goto L_0x004c
        L_0x004b:
            throw r0
        L_0x004c:
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.l.h.a():java.util.List");
    }

    public final List<Section> a(g gVar) {
        List<Section> a2 = a();
        if (gVar == null) {
            return a2;
        }
        ArrayList arrayList = new ArrayList();
        for (Section next : a2) {
            if (!this.f4167b.a(next.c, gVar).isEmpty()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public final synchronized void b() {
        try {
            this.f4166a.a(this.f4166a.getWritableDatabase());
        } catch (Exception e) {
            n.c("HelpShiftDebug", "Error in clearSectionsData", e);
        }
        return;
    }
}
