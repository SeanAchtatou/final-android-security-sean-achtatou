package com.helpshift.support.l;

import com.helpshift.common.e.aa;
import com.helpshift.common.k;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* compiled from: AndroidAnalyticsEventDAO */
public class a implements com.helpshift.b.a {

    /* renamed from: a  reason: collision with root package name */
    private aa f4157a;

    public a(aa aaVar) {
        this.f4157a = aaVar;
    }

    public final void a(String str, HashMap<String, String> hashMap) {
        HashMap<String, HashMap<String, String>> b2 = b();
        b2.put(str, hashMap);
        this.f4157a.a("unsent_analytics_events", b2);
    }

    public final void a(String str) {
        if (!k.a(str)) {
            HashMap<String, HashMap<String, String>> b2 = b();
            b2.remove(str);
            if (b2.size() == 0) {
                this.f4157a.a("unsent_analytics_events", (Serializable) null);
            } else {
                this.f4157a.a("unsent_analytics_events", b2);
            }
        }
    }

    public final Map<String, HashMap<String, String>> a() {
        return b();
    }

    private HashMap<String, HashMap<String, String>> b() {
        Object b2 = this.f4157a.b("unsent_analytics_events");
        if (b2 == null) {
            return new HashMap<>();
        }
        return (HashMap) b2;
    }
}
