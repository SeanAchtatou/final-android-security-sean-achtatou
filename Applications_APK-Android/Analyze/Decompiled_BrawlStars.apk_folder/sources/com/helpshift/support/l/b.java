package com.helpshift.support.l;

import com.helpshift.support.Faq;
import com.helpshift.support.g;
import java.util.List;

/* compiled from: FaqDAO */
public interface b {
    int a(String str, Boolean bool);

    Faq a(String str, String str2);

    List<String> a();

    List<Faq> a(String str, g gVar);

    List<Faq> a(List<Faq> list, g gVar);

    void a(Faq faq);

    void a(String str);

    Faq b(String str);

    List<Faq> c(String str);
}
