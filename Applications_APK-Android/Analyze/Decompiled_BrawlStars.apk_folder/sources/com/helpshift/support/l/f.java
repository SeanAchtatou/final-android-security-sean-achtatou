package com.helpshift.support.l;

import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import com.helpshift.a.a.i;
import com.helpshift.a.b.c;
import com.helpshift.common.c.j;
import com.helpshift.common.e.aa;
import com.helpshift.common.k;
import com.helpshift.k;
import com.helpshift.r.a;
import com.helpshift.r.a.b;
import com.helpshift.support.u;
import com.helpshift.util.ab;
import com.helpshift.util.q;
import java.util.ArrayList;
import java.util.List;

/* compiled from: LegacyUserDataMigrator */
public class f {

    /* renamed from: a  reason: collision with root package name */
    public b f4164a;

    /* renamed from: b  reason: collision with root package name */
    private com.helpshift.b f4165b;
    private j c;
    private aa d;
    private u e;
    private com.helpshift.r.b f;
    private a g;
    private com.helpshift.common.b.a h;
    private String i;
    private String j;
    private i k;
    private List<i> l;
    private ab m;

    public f(com.helpshift.b bVar, u uVar, aa aaVar, b bVar2, com.helpshift.common.b.a aVar, com.helpshift.r.b bVar3, a aVar2, ab abVar) {
        this.f4165b = bVar;
        this.c = bVar.a();
        this.e = uVar;
        this.d = aaVar;
        this.f4164a = bVar2;
        this.h = aVar;
        this.f = bVar3;
        this.g = aVar2;
        this.m = abVar;
    }

    public final void a(ab abVar) {
        if (!abVar.a(new ab("7.0.0"))) {
            if (abVar.b(new ab("4.9.1"))) {
                this.i = this.e.d("loginIdentifier");
                String d2 = this.e.d("identity");
                this.j = this.e.d("uuid");
                if (k.a(this.j)) {
                    this.j = Settings.Secure.getString(q.a().getContentResolver(), "android_id");
                }
                this.k = new i(null, this.j, d2, this.e.d("username"), this.e.d(NotificationCompat.CATEGORY_EMAIL), null, null, null, true);
                List<i> a2 = this.f4164a.a();
                if (!com.helpshift.common.j.a(a2)) {
                    this.l = new ArrayList();
                    for (i next : a2) {
                        this.l.add(new i(next.f3167a, next.c, next.f3168b, next.d, next.e, next.c + "_" + next.f, next.g, next.h, next.i));
                    }
                    return;
                }
                return;
            }
            this.i = this.d.a("loginIdentifier");
            this.j = this.d.a("default_user_login");
            if (!k.a(this.j)) {
                Object b2 = this.d.b("default_user_profile");
                if (b2 instanceof i) {
                    this.k = (i) b2;
                }
            }
            this.l = this.f4164a.a();
        }
    }

    public final void a() {
        if (!this.m.a(new ab("7.0.0"))) {
            String str = this.j;
            if (str != null) {
                this.d.a("key_support_device_id", str);
                this.h.a("key_support_device_id", this.j);
            }
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            i iVar = this.k;
            if (iVar != null && !k.a(iVar.f3168b)) {
                c d2 = this.c.a().d();
                if (d2 == null) {
                    d2 = this.c.a().c();
                }
                arrayList2.add(new com.helpshift.r.a.a(d2.b(), this.k.e, this.k.d, this.k.f3168b, com.helpshift.r.c.NOT_STARTED));
            }
            if (!com.helpshift.common.j.a(this.l)) {
                for (i next : this.l) {
                    if (!k.a(next.f3168b)) {
                        arrayList2.add(new com.helpshift.r.a.a(next.c, next.e, next.d, next.f3168b, com.helpshift.r.c.NOT_STARTED));
                    }
                    arrayList.add(new com.helpshift.common.e.a.c(next.c, next.f));
                }
            }
            if (!com.helpshift.common.j.a(arrayList2)) {
                this.f.a(arrayList2);
            }
            if (!com.helpshift.common.j.a(arrayList)) {
                this.g.a(arrayList);
            }
            if (k.a(this.i)) {
                this.f4165b.e();
                return;
            }
            List<i> list = this.l;
            if (list != null) {
                for (i next2 : list) {
                    if (this.i.equals(next2.c)) {
                        this.f4165b.a(new k.a(next2.c, next2.e).a(next2.e).a());
                        return;
                    }
                }
            }
        }
    }
}
