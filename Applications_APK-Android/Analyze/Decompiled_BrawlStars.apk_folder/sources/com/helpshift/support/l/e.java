package com.helpshift.support.l;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* compiled from: IMAppSessionStorage */
public class e implements com.helpshift.y.e {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, Serializable> f4161a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    private Lock f4162b;
    private Lock c;

    /* compiled from: IMAppSessionStorage */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public static final e f4163a = new e();
    }

    e() {
        ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
        this.f4162b = reentrantReadWriteLock.readLock();
        this.c = reentrantReadWriteLock.writeLock();
    }

    public final boolean a(String str, Serializable serializable) {
        if (str == null) {
            return false;
        }
        this.c.lock();
        this.f4161a.put(str, serializable);
        this.c.unlock();
        return true;
    }

    public final boolean a(Map<String, Serializable> map) {
        if (map == null || map.size() == 0) {
            return false;
        }
        this.c.lock();
        this.f4161a.putAll(map);
        this.c.unlock();
        return true;
    }

    /* renamed from: c */
    public final Serializable a(String str) {
        if (str == null) {
            return null;
        }
        this.f4162b.lock();
        Serializable serializable = this.f4161a.get(str);
        this.f4162b.unlock();
        return serializable;
    }

    public final void b(String str) {
        if (str != null) {
            this.c.lock();
            this.f4161a.remove(str);
            this.c.unlock();
        }
    }

    public final void a() {
        this.c.lock();
        this.f4161a.clear();
        this.c.unlock();
    }
}
