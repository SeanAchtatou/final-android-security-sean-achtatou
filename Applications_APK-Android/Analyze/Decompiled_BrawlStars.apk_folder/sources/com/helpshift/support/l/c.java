package com.helpshift.support.l;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.util.q;

/* compiled from: FaqsDBHelper */
public class c extends SQLiteOpenHelper {

    /* compiled from: FaqsDBHelper */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        static final c f4158a = new c(q.a());
    }

    c(Context context) {
        super(context, "__hs__db_faq", (SQLiteDatabase.CursorFactory) null, 3);
    }

    public static c a() {
        return a.f4158a;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        b(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        q.b().t().a("/faqs/", null);
        a(sQLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        q.b().t().a("/faqs/", null);
        a(sQLiteDatabase);
    }

    private static void b(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE faqs (_id INTEGER PRIMARY KEY AUTOINCREMENT,question_id TEXT NOT NULL,publish_id TEXT NOT NULL,language TEXT NOT NULL,section_id TEXT NOT NULL,title TEXT NOT NULL,body TEXT NOT NULL,helpful INTEGER,rtl INTEGER,tags TEXT,c_tags TEXT,FOREIGN KEY(section_id) REFERENCES sections (_id));");
        sQLiteDatabase.execSQL("CREATE TABLE sections (_id INTEGER PRIMARY KEY AUTOINCREMENT,section_id TEXT NOT NULL,publish_id INTEGER NOT NULL,title TEXT NOT NULL);");
    }

    private static void c(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS faqs");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS sections");
    }

    public final void a(SQLiteDatabase sQLiteDatabase) {
        c(sQLiteDatabase);
        b(sQLiteDatabase);
    }
}
