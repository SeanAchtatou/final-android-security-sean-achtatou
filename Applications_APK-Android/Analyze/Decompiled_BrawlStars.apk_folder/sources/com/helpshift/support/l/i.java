package com.helpshift.support.l;

import com.helpshift.common.e.a.e;
import com.helpshift.common.e.aa;
import com.helpshift.common.e.ab;
import com.helpshift.common.k;
import com.helpshift.q.a.a;
import com.helpshift.support.u;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONObject;

/* compiled from: SupportKVStoreMigrator */
public class i {

    /* renamed from: a  reason: collision with root package name */
    public e f4169a;

    /* renamed from: b  reason: collision with root package name */
    public a f4170b;
    public aa c;
    public Boolean d;
    public Boolean e;
    public Boolean f;
    public Boolean g;
    public Boolean h;
    public Boolean i;
    public Boolean j;
    public Boolean k;
    public float l;
    public HashMap<String, Serializable> m;
    public String n;
    private u o;
    private com.helpshift.i.a.a p = q.c().r();

    public i(u uVar) {
        this.o = uVar;
        ab b2 = q.b();
        this.f4169a = b2.t();
        this.f4170b = b2.h();
        this.c = q.b().o();
    }

    public final void a() {
        if (this.o.b("requireEmail")) {
            this.d = this.o.c("requireEmail");
        } else {
            this.d = Boolean.valueOf(this.p.a("requireEmail"));
        }
        if (this.o.b("fullPrivacy")) {
            this.e = this.o.c("fullPrivacy");
        } else {
            this.e = Boolean.valueOf(this.p.a("fullPrivacy"));
        }
        if (this.o.b("hideNameAndEmail")) {
            this.f = this.o.c("hideNameAndEmail");
        } else {
            this.f = Boolean.valueOf(this.p.a("hideNameAndEmail"));
        }
        if (this.o.b("showSearchOnNewConversation")) {
            this.g = this.o.c("showSearchOnNewConversation");
        } else {
            this.g = Boolean.valueOf(this.p.a("showSearchOnNewConversation"));
        }
        if (this.o.b("gotoConversationAfterContactUs")) {
            this.h = this.o.c("gotoConversationAfterContactUs");
        } else {
            this.h = Boolean.valueOf(this.p.a("gotoConversationAfterContactUs"));
        }
        if (this.o.b("showConversationResolutionQuestion")) {
            this.i = this.o.c("showConversationResolutionQuestion");
        } else {
            this.i = Boolean.valueOf(this.p.a("showConversationResolutionQuestion"));
        }
        if (this.o.b("showConversationInfoScreen")) {
            this.j = this.o.c("showConversationInfoScreen");
        } else {
            this.j = Boolean.valueOf(this.p.a("showConversationInfoScreen"));
        }
        if (this.o.b("enableTypingIndicator")) {
            this.k = this.o.c("enableTypingIndicator");
        } else {
            this.k = Boolean.valueOf(this.p.a("enableTypingIndicator"));
        }
        this.n = this.c.a("key_support_device_id");
        if (this.o.b("serverTimeDelta")) {
            this.l = Float.valueOf(this.o.c.getFloat("serverTimeDelta", 0.0f)).floatValue();
        } else {
            this.l = this.f4169a.a();
        }
        if (this.o.b("customMetaData")) {
            String d2 = this.o.d("customMetaData");
            try {
                if (!k.a(d2)) {
                    JSONObject jSONObject = new JSONObject(d2);
                    Iterator<String> keys = jSONObject.keys();
                    this.m = new HashMap<>();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        Object obj = jSONObject.get(next);
                        if (obj instanceof Serializable) {
                            this.m.put(next, (Serializable) obj);
                        }
                    }
                }
            } catch (Exception e2) {
                n.a("Helpshift_KVStoreMigratorr", "Exception converting meta from storage", e2);
            }
        } else {
            this.m = this.f4170b.b();
        }
    }
}
