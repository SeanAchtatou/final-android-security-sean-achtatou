package com.helpshift.support;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.helpshift.R;
import com.helpshift.s.b;
import com.helpshift.support.h.g;
import com.helpshift.util.c;
import java.util.List;

public final class HSReview extends FragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    private List<g> f3838a;

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Integer a2 = b.a.f3833a.f3832b.a();
        setTheme(c.a(this, a2) ? a2.intValue() : R.style.Helpshift_Theme_Base);
        setContentView(new View(this));
        this.f3838a = com.helpshift.support.h.b.a();
        com.helpshift.support.h.b.a(null);
        new o().show(getSupportFragmentManager(), "hs__review_dialog");
    }

    public final void onDestroy() {
        super.onDestroy();
        com.helpshift.support.h.b.a(this.f3838a);
        com.helpshift.util.b.a();
    }

    /* access modifiers changed from: protected */
    public final void attachBaseContext(Context context) {
        super.attachBaseContext(com.helpshift.util.b.f(context));
    }
}
