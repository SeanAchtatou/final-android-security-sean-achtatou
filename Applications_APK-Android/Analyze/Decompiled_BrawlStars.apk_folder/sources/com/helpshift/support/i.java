package com.helpshift.support;

import android.os.Handler;
import android.os.Message;
import com.helpshift.common.exception.a;
import com.helpshift.common.exception.b;
import com.helpshift.common.h;
import com.helpshift.n.f;
import com.helpshift.util.n;
import org.json.JSONArray;

/* compiled from: HSApiData */
class i implements h<f, a> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Handler f4088a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ g f4089b;
    final /* synthetic */ Handler c;
    final /* synthetic */ h d;

    i(h hVar, Handler handler, g gVar, Handler handler2) {
        this.d = hVar;
        this.f4088a = handler;
        this.f4089b = gVar;
        this.c = handler2;
    }

    public final /* synthetic */ void a(Object obj) {
        f fVar = (f) obj;
        Handler handler = this.f4088a;
        if (handler != null) {
            Message obtainMessage = handler.obtainMessage();
            if (1 == fVar.f3772b) {
                obtainMessage.what = com.helpshift.support.c.a.d;
            } else if (2 == fVar.f3772b) {
                obtainMessage.what = com.helpshift.support.c.a.c;
            }
            Object obj2 = fVar.f3771a;
            if (obj2 != null) {
                JSONArray jSONArray = (JSONArray) obj2;
                h hVar = this.d;
                StringBuilder sb = new StringBuilder();
                sb.append("Updating ");
                sb.append(jSONArray == null ? 0 : jSONArray.length());
                sb.append(" FAQ sections in DB");
                n.a("Helpshift_ApiData", sb.toString());
                hVar.c.b();
                hVar.c.a(jSONArray);
                obtainMessage.obj = this.d.c.a(this.f4089b);
                Thread thread = new Thread(new j(this.d), "HS-search-index");
                thread.setDaemon(true);
                thread.start();
            }
            this.f4088a.sendMessage(obtainMessage);
            h.a();
        }
    }

    public final /* synthetic */ void b(Object obj) {
        int i;
        a aVar = (a) obj;
        Handler handler = this.c;
        if (handler != null) {
            Message obtainMessage = handler.obtainMessage();
            obtainMessage.obj = aVar;
            if (aVar == b.CONTENT_UNCHANGED) {
                i = com.helpshift.support.c.a.f;
            } else {
                i = com.helpshift.support.c.a.e;
            }
            obtainMessage.what = i;
            this.c.sendMessage(obtainMessage);
        }
    }
}
