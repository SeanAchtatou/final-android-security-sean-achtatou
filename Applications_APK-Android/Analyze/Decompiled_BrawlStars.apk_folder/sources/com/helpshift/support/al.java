package com.helpshift.support;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.helpshift.R;
import com.helpshift.common.g.f;
import com.helpshift.f.c;
import com.helpshift.i.b.a;
import com.helpshift.i.b.b;
import com.helpshift.k;
import com.helpshift.p.b.d;
import com.helpshift.s.b;
import com.helpshift.support.activities.ParentActivity;
import com.helpshift.support.h.b;
import com.helpshift.util.n;
import com.helpshift.util.o;
import com.helpshift.util.q;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SupportInternal */
public final class al {

    /* renamed from: a  reason: collision with root package name */
    private static h f3879a;

    /* renamed from: b  reason: collision with root package name */
    private static u f3880b;
    private static Context c;

    private static void a(Context context) {
        if (c == null) {
            f3879a = new h(context);
            f3880b = f3879a.f4075b;
            d.a(context);
            c = context;
        }
    }

    public static void a(Application application, String str, String str2, String str3, Map map) {
        q.a(application.getApplicationContext());
        q.a(str, str2, str3);
        boolean booleanValue = (map == null || !map.containsKey("manualLifecycleTracking")) ? false : ((Boolean) map.get("manualLifecycleTracking")).booleanValue();
        ak akVar = new ak();
        c a2 = c.a();
        a2.a(application, booleanValue);
        synchronized (c.f3396b) {
            a2.f3397a.add(akVar);
        }
    }

    public static void a(Handler handler, Handler handler2) {
        if (handler != null) {
            if (f3879a == null || f3880b == null) {
                if (q.a() != null) {
                    a(q.a().getApplicationContext());
                } else {
                    return;
                }
            }
            q.c().a(new am(handler, handler2));
        }
    }

    public static void a(String str, String str2) {
        String str3 = "";
        String trim = (str == null || o.b(str)) ? str3 : str.trim();
        if (o.a(str2)) {
            str3 = str2.trim();
        }
        q.c().a(trim, str3);
    }

    public static void a(String str) {
        if (str != null) {
            q.c().o().a(str.trim());
        }
    }

    public static void b(String str) {
        if (str != null && !TextUtils.isEmpty(str.trim())) {
            q.c().p().a(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Activity activity, Map<String, Object> map) {
        HashMap hashMap = new HashMap(map);
        n.a("Helpshift_SupportInter", "Show conversation : ", d.a("Config", hashMap));
        Intent intent = new Intent(activity, ParentActivity.class);
        intent.putExtra("support_mode", 1);
        intent.putExtra("decomp", true);
        intent.putExtras(c(b(hashMap)));
        intent.putExtra("showInFullScreen", com.helpshift.util.a.a(activity));
        intent.putExtra("isRoot", true);
        intent.putExtra("search_performed", false);
        activity.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Activity activity, String str, Map<String, Object> map) {
        if (!d(str)) {
            str = null;
        }
        HashMap hashMap = new HashMap(map);
        n.a("Helpshift_SupportInter", "Show FAQ section : Publish Id : " + str, d.a("Config", hashMap));
        Intent intent = new Intent(activity, ParentActivity.class);
        intent.putExtra("support_mode", 2);
        intent.putExtras(c(a((HashMap<String, Object>) hashMap)));
        intent.putExtra("sectionPublishId", str);
        intent.putExtra("showInFullScreen", com.helpshift.util.a.a(activity));
        intent.putExtra("decomp", true);
        intent.putExtra("isRoot", true);
        activity.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void b(Activity activity, String str, Map<String, Object> map) {
        if (!d(str)) {
            str = null;
        }
        HashMap hashMap = new HashMap(map);
        n.a("Helpshift_SupportInter", "Show single FAQ : Publish Id : " + str, d.a("Config", hashMap));
        Intent intent = new Intent(activity, ParentActivity.class);
        intent.putExtra("support_mode", 3);
        intent.putExtras(c(a((HashMap<String, Object>) hashMap)));
        intent.putExtra("questionPublishId", str);
        intent.putExtra("showInFullScreen", com.helpshift.util.a.a(activity));
        intent.putExtra("decomp", true);
        intent.putExtra("isRoot", true);
        activity.startActivity(intent);
    }

    private static void a(c cVar) {
        q.c().p().a(cVar);
    }

    public static void a(w wVar) {
        a((c) new an(wVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void b(Activity activity, Map<String, Object> map) {
        HashMap hashMap = new HashMap(map);
        n.a("Helpshift_SupportInter", "Show FAQs : ", d.a("Config", hashMap));
        Intent intent = new Intent(activity, ParentActivity.class);
        intent.putExtras(c(a((HashMap<String, Object>) hashMap)));
        intent.putExtra("showInFullScreen", com.helpshift.util.a.a(activity));
        intent.putExtra("decomp", false);
        intent.putExtra("isRoot", true);
        activity.startActivity(intent);
    }

    public static HashMap<String, Object> a(HashMap<String, Object> hashMap) {
        HashMap<String, Object> hashMap2 = new HashMap<>(hashMap);
        hashMap2.remove("conversationPrefillText");
        return hashMap2;
    }

    public static HashMap<String, Object> b(HashMap<String, Object> hashMap) {
        HashMap<String, Object> hashMap2 = new HashMap<>(hashMap);
        hashMap2.remove("enableContactUs");
        hashMap2.remove("customContactUsFlows");
        return hashMap2;
    }

    private static boolean d(String str) {
        return str != null && str.trim().length() > 0 && str.matches("\\d+");
    }

    public static Bundle c(HashMap<String, Object> hashMap) {
        HashMap hashMap2 = new HashMap(com.helpshift.support.m.d.a());
        hashMap2.putAll(hashMap);
        Object obj = hashMap2.get("enableContactUs");
        if (obj instanceof Integer) {
            d.f3901a = (Integer) hashMap2.get("enableContactUs");
        } else if (obj instanceof Boolean) {
            if (((Boolean) obj).booleanValue()) {
                d.f3901a = a.f3881a;
            } else {
                d.f3901a = a.f3882b;
            }
        }
        Bundle bundle = new Bundle();
        if (hashMap2.containsKey("hs-custom-metadata")) {
            a((c) new ao(hashMap2));
        }
        JSONObject jSONObject = new JSONObject(hashMap2);
        q.c().a(new a.b().a(hashMap2).a());
        Map map = null;
        if (hashMap2.containsKey("hs-custom-issue-field")) {
            Object obj2 = hashMap2.get("hs-custom-issue-field");
            if (obj2 instanceof Map) {
                try {
                    map = (Map) obj2;
                } catch (Exception e) {
                    n.c("Helpshift_SupportInter", "Exception while parsing CIF data : ", e);
                }
            }
        }
        q.c().q().a(map);
        try {
            if (jSONObject.has("conversationPrefillText") && !jSONObject.getString("conversationPrefillText").equals("null") && jSONObject.has("hs-custom-metadata")) {
                bundle.putBoolean("dropMeta", true);
            }
            if (jSONObject.has("toolbarId")) {
                bundle.putInt("toolbarId", jSONObject.getInt("toolbarId"));
            }
        } catch (JSONException e2) {
            n.a("Helpshift_SupportInter", "JSON exception while parsing config : ", e2);
        }
        bundle.putBoolean("showSearchOnNewConversation", jSONObject.optBoolean("showSearchOnNewConversation", false));
        bundle.putSerializable("withTagsMatching", a(hashMap2.get("withTagsMatching")));
        b.a((List) hashMap2.get("customContactUsFlows"));
        return bundle;
    }

    private static g a(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            Map map = (Map) obj;
            String str = (String) map.get("operator");
            if (!TextUtils.isEmpty(str)) {
                String lowerCase = str.trim().toLowerCase(Locale.US);
                String[] strArr = (String[]) map.get("tags");
                if (strArr != null && strArr.length > 0) {
                    if (lowerCase.equals("and")) {
                        return new g("and", strArr);
                    }
                    if (lowerCase.equals("or")) {
                        return new g("or", strArr);
                    }
                    if (lowerCase.equals("not")) {
                        return new g("not", strArr);
                    }
                }
            }
        } catch (ClassCastException e) {
            n.c("Helpshift_SupportInter", "Invalid FaqTagFilter object in config", e);
        }
        return null;
    }

    public static void a(com.helpshift.l.b bVar) {
        q.c().a(bVar);
    }

    public static boolean a(k kVar) {
        return q.c().a(kVar);
    }

    public static boolean a() {
        return q.c().e();
    }

    public static void c(String str) {
        q.c().r().d(str);
    }

    /* compiled from: SupportInternal */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public static final Integer f3881a = 0;

        /* renamed from: b  reason: collision with root package name */
        public static final Integer f3882b = 1;
        public static final Integer c = 2;
        public static final Integer d = 3;
        public static final HashSet e;

        static {
            HashSet hashSet = new HashSet();
            hashSet.add(f3881a);
            hashSet.add(f3882b);
            hashSet.add(c);
            hashSet.add(d);
            e = hashSet;
        }
    }

    public static void a(Application application, Map<String, Object> map) {
        int i;
        boolean z;
        NotificationManager d;
        NotificationChannel notificationChannel;
        a(application.getApplicationContext());
        com.helpshift.v.a.f4262b = new com.helpshift.support.providers.a();
        HashMap hashMap = (HashMap) com.helpshift.support.m.d.b();
        if (map != null) {
            hashMap.putAll(map);
        }
        String packageName = application.getPackageName();
        com.helpshift.s.a aVar = b.a.f3833a.f3831a;
        Object obj = hashMap.get("notificationIcon");
        if (obj instanceof String) {
            hashMap.put("notificationIcon", Integer.valueOf(com.helpshift.util.b.a(application, (String) obj, "drawable", packageName)));
        }
        Object obj2 = hashMap.get("notificationSound");
        if (obj2 instanceof String) {
            hashMap.put("notificationSound", Integer.valueOf(com.helpshift.util.b.a(application, (String) obj2, "raw", packageName)));
        }
        com.helpshift.i.b.b a2 = new b.a().a(hashMap).a();
        ap.a(c, q.b(), q.c().a(), f3879a, f3880b);
        String str = a2.l;
        aVar.l = str;
        aVar.m.a("font", str);
        Integer num = (Integer) f.a(hashMap, "screenOrientation", Integer.class, null);
        if (num == null) {
            i = -1;
        } else {
            i = num.intValue();
        }
        aVar.k = Integer.valueOf(i);
        aVar.m.a("screenOrientation", aVar.k);
        Boolean bool = a2.f;
        if (bool == null) {
            z = false;
        } else {
            z = bool.booleanValue();
        }
        Boolean valueOf = Boolean.valueOf(z);
        aVar.j = valueOf;
        aVar.m.a("disableAnimations", valueOf);
        String a3 = com.helpshift.util.b.a(c);
        if (!f3880b.a("applicationVersion").equals(a3)) {
            f3879a.d();
            q.c().r().a(false);
            f3880b.a("applicationVersion", a3);
        }
        q.c().a(a2);
        application.deleteDatabase("__hs__db_error_reports");
        com.helpshift.t.a aVar2 = new com.helpshift.t.a(application);
        if (Build.VERSION.SDK_INT >= 26 && com.helpshift.util.b.b(aVar2.f4226a) >= 26 && (d = com.helpshift.util.b.d(aVar2.f4226a)) != null && (notificationChannel = d.getNotificationChannel("helpshift_default_channel_id")) != null) {
            CharSequence name = notificationChannel.getName();
            String description = notificationChannel.getDescription();
            String string = aVar2.f4226a.getResources().getString(R.string.hs__default_notification_channel_name);
            String string2 = aVar2.f4226a.getResources().getString(R.string.hs__default_notification_channel_desc);
            if (!string.equals(name) || !string2.equals(description)) {
                NotificationChannel notificationChannel2 = new NotificationChannel("helpshift_default_channel_id", string, notificationChannel.getImportance());
                notificationChannel2.setDescription(string2);
                d.createNotificationChannel(notificationChannel2);
            }
        }
    }

    public static void a(Context context, String str) {
        a(context.getApplicationContext());
        if (str != null) {
            q.c().a(str);
        } else {
            n.b("Helpshift_SupportInter", "Device Token is null");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r4, android.content.Intent r5) {
        /*
            android.content.Context r4 = r4.getApplicationContext()
            a(r4)
            r4 = 0
            if (r5 == 0) goto L_0x001c
            android.os.Bundle r0 = r5.getExtras()
            if (r0 != 0) goto L_0x0011
            goto L_0x001c
        L_0x0011:
            android.os.Bundle r0 = r5.getExtras()
            java.lang.String r1 = "issue_type"
            java.lang.String r0 = r0.getString(r1)
            goto L_0x001d
        L_0x001c:
            r0 = r4
        L_0x001d:
            if (r5 == 0) goto L_0x0048
            android.os.Bundle r1 = r5.getExtras()
            if (r1 != 0) goto L_0x0026
            goto L_0x0048
        L_0x0026:
            android.os.Bundle r1 = r5.getExtras()
            java.lang.String r2 = "issue"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0039
            java.lang.String r2 = "issue_id"
            java.lang.String r1 = r1.getString(r2)
            goto L_0x0049
        L_0x0039:
            java.lang.String r2 = "preissue"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0048
            java.lang.String r2 = "preissue_id"
            java.lang.String r1 = r1.getString(r2)
            goto L_0x0049
        L_0x0048:
            r1 = r4
        L_0x0049:
            if (r1 != 0) goto L_0x0053
            java.lang.String r4 = "Helpshift_SupportInter"
            java.lang.String r5 = "Unknown issuetype/issueId in push payload"
            com.helpshift.util.n.b(r4, r5)
            return
        L_0x0053:
            android.os.Bundle r5 = r5.getExtras()
            if (r5 == 0) goto L_0x0065
            java.lang.String r2 = "app_name"
            boolean r3 = r5.containsKey(r2)
            if (r3 == 0) goto L_0x0065
            java.lang.String r4 = r5.getString(r2)
        L_0x0065:
            com.helpshift.b r5 = com.helpshift.util.q.c()
            r5.a(r0, r1, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.al.a(android.content.Context, android.content.Intent):void");
    }
}
