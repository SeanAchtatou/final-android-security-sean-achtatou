package com.helpshift.support;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import com.helpshift.support.j.b;
import com.helpshift.support.j.c;
import com.helpshift.support.j.d;
import com.helpshift.support.k.a.b;
import com.helpshift.support.m.g;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: HSSearch */
public final class s {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f4195a = Pattern.compile("[a-zA-Z0-9]+");

    /* renamed from: b  reason: collision with root package name */
    private static com.helpshift.support.g.a f4196b = new com.helpshift.support.g.a();
    private static boolean c = false;
    private static boolean d = false;
    private static HashMap<String, String[]> e;

    /* compiled from: HSSearch */
    public enum a {
        FULL_SEARCH,
        METAPHONE_SEARCH,
        KEYWORD_SEARCH
    }

    public static int a(int i) {
        if (i == 20) {
            return 5;
        }
        return i == 30 ? 3 : 1;
    }

    public static int a(int i, int i2) {
        if (i == 1) {
            return 5;
        }
        if (40 == i2) {
            return i;
        }
        if (10 == i2) {
            return 30;
        }
        if (50 == i2) {
            return 1;
        }
        if (20 == i2) {
            return 300;
        }
        return 30 == i2 ? 150 : 1;
    }

    public static void a() {
        if (!c) {
            Thread thread = new Thread(new t(), "HS-trnsltrtr");
            thread.setDaemon(true);
            thread.start();
        }
    }

    public static void b() {
        if (!c) {
            g.c();
        } else {
            d = true;
        }
        e = null;
    }

    private static ArrayList<String> a(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        Matcher matcher = f4195a.matcher(str);
        while (matcher.find()) {
            if (matcher.group(0).length() > 2) {
                arrayList.add(matcher.group(0));
            }
        }
        return arrayList;
    }

    private static ArrayList<String> b(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        Matcher matcher = f4195a.matcher(str);
        while (matcher.find()) {
            if (matcher.group(0).length() > 2 || str.length() > 2) {
                arrayList.add(matcher.group(0));
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.g.a.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.support.g.a.a(java.lang.String, int):char
      com.helpshift.support.g.a.a(java.lang.String, boolean):java.lang.String */
    private static ArrayList<d> a(String str, int i) {
        HashSet hashSet = new HashSet();
        hashSet.add(new d(str, i));
        String a2 = f4196b.a(str, false);
        if (a2 != null) {
            hashSet.add(new d(a2.toLowerCase(), 50));
        }
        return new ArrayList<>(hashSet);
    }

    private static void a(com.helpshift.support.k.b.a aVar, List<String> list, int i, int i2) {
        for (String a2 : list) {
            Iterator<d> it = a(a2, i).iterator();
            while (it.hasNext()) {
                d next = it.next();
                aVar.a(next.f4143a, next.f4144b, i2);
            }
        }
    }

    public static b a(ArrayList<Faq> arrayList) {
        if (c) {
            return null;
        }
        if (!g.a()) {
            g.b();
            d = true;
        }
        c = true;
        HashMap hashMap = new HashMap();
        Iterator<Faq> it = arrayList.iterator();
        int i = 0;
        while (it.hasNext()) {
            Iterator<String> it2 = a(c(it.next().f3836a)).iterator();
            while (it2.hasNext()) {
                String lowerCase = it2.next().toLowerCase();
                if (lowerCase.length() > 3) {
                    c cVar = new c(lowerCase, i + "");
                    String substring = lowerCase.substring(0, 1);
                    List list = (List) hashMap.get(substring);
                    if (list == null) {
                        list = new ArrayList();
                    }
                    list.add(cVar);
                    hashMap.put(substring, list);
                    String substring2 = lowerCase.substring(1, 2);
                    List list2 = (List) hashMap.get(substring2);
                    if (list2 == null) {
                        list2 = new ArrayList();
                    }
                    list2.add(cVar);
                    hashMap.put(substring2, list2);
                }
            }
            i++;
        }
        b(arrayList);
        b bVar = new b(hashMap);
        c = false;
        if (d) {
            b();
            d = false;
        }
        return bVar;
    }

    private static ArrayList<d> a(ArrayList<d> arrayList, a aVar) {
        ArrayList<d> arrayList2 = new ArrayList<>();
        Iterator<d> it = arrayList.iterator();
        while (it.hasNext()) {
            d next = it.next();
            int i = next.f4144b;
            if (aVar == a.FULL_SEARCH) {
                arrayList2.add(next);
            } else if (aVar == a.METAPHONE_SEARCH && 50 == i) {
                arrayList2.add(next);
            } else if (aVar == a.KEYWORD_SEARCH && (10 == i || 40 == i)) {
                arrayList2.add(next);
            }
        }
        return arrayList2;
    }

    public static ArrayList<HashMap> a(String str, a aVar) {
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        ArrayList arrayList = new ArrayList();
        Iterator<String> it = b(c(str)).iterator();
        while (it.hasNext()) {
            arrayList.addAll(a(a(it.next(), 10), aVar));
        }
        com.helpshift.support.k.a aVar2 = b.a.f4148a;
        HashSet hashSet = null;
        if (aVar2 != null) {
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                d dVar = (d) it2.next();
                String str2 = dVar.f4143a;
                int i = dVar.f4144b;
                com.helpshift.support.k.b a2 = aVar2.a(str2);
                if (a2 != null) {
                    int i2 = a2.f4150b;
                    int i3 = dVar.f4144b;
                    if (!(50 == i2 || 50 == i3) || i2 == i3) {
                        Map<Integer, Double> map = a2.c;
                        HashMap hashMap3 = new HashMap();
                        for (Map.Entry next : map.entrySet()) {
                            hashMap3.put(String.valueOf(next.getKey()), next.getValue());
                        }
                        if (!hashMap3.isEmpty()) {
                            for (Map.Entry entry : hashMap3.entrySet()) {
                                String str3 = (String) entry.getKey();
                                ArrayList arrayList2 = (ArrayList) hashMap2.get(str3);
                                if (arrayList2 == null) {
                                    arrayList2 = new ArrayList();
                                }
                                if (str2.length() > 0) {
                                    arrayList2.add(str2);
                                }
                                hashMap2.put(str3, arrayList2);
                                Double d2 = (Double) hashMap.get(str3);
                                double doubleValue = ((Double) entry.getValue()).doubleValue();
                                HashSet hashSet2 = hashSet;
                                double a3 = (double) a(str2.length(), i);
                                Double.isNaN(a3);
                                Double valueOf = Double.valueOf(doubleValue * a3);
                                if (d2 != null) {
                                    hashMap.put(str3, Double.valueOf(d2.doubleValue() + valueOf.doubleValue()));
                                } else {
                                    hashMap.put(str3, valueOf);
                                }
                                hashSet = hashSet2;
                            }
                            HashSet hashSet3 = hashSet;
                            HashSet hashSet4 = new HashSet(hashMap3.keySet());
                            if (hashSet3 == null || hashSet3.isEmpty()) {
                                hashSet = new HashSet(hashSet4);
                            } else {
                                hashSet = hashSet3;
                                hashSet.addAll(hashSet4);
                            }
                        }
                    }
                }
            }
        }
        if (hashSet == null || hashSet.isEmpty()) {
            TreeMap treeMap = new TreeMap(new x(hashMap));
            treeMap.putAll(hashMap);
            return a(treeMap, hashMap2);
        } else if (hashSet.size() == 1) {
            HashMap hashMap4 = new HashMap();
            ArrayList<HashMap> arrayList3 = new ArrayList<>();
            String str4 = (String) hashSet.iterator().next();
            hashMap4.put("f", str4);
            hashMap4.put("t", hashMap2.get(str4));
            arrayList3.add(hashMap4);
            return arrayList3;
        } else {
            HashMap hashMap5 = new HashMap();
            Iterator it3 = hashSet.iterator();
            while (it3.hasNext()) {
                String str5 = (String) it3.next();
                hashMap5.put(str5, hashMap.get(str5));
            }
            TreeMap treeMap2 = new TreeMap(new x(hashMap5));
            treeMap2.putAll(hashMap5);
            return a(treeMap2, hashMap2);
        }
    }

    private static ArrayList<HashMap> a(TreeMap treeMap, HashMap hashMap) {
        ArrayList<HashMap> arrayList = new ArrayList<>();
        for (String str : treeMap.keySet()) {
            HashMap hashMap2 = new HashMap();
            hashMap2.put("f", str);
            hashMap2.put("t", hashMap.get(str));
            arrayList.add(hashMap2);
        }
        return arrayList;
    }

    private static void b(ArrayList<Faq> arrayList) {
        int size = arrayList.size();
        com.helpshift.support.k.b.a aVar = new com.helpshift.support.k.b.a(size);
        for (int i = 0; i < size; i++) {
            Faq faq = arrayList.get(i);
            String str = faq.f3836a;
            String str2 = faq.e;
            List<String> a2 = faq.a();
            a(aVar, a(c(str)), 20, i);
            ArrayList arrayList2 = new ArrayList();
            for (String a3 : a2) {
                arrayList2.addAll(a(a3));
            }
            a(aVar, arrayList2, 30, i);
            a(aVar, a(c(str2)), 10, i);
        }
        aVar.a();
    }

    public static ArrayList<HashMap> a(String str, Map<String, List<c>> map) {
        String str2;
        ArrayList<HashMap> arrayList;
        HashMap hashMap;
        Iterator<String> it;
        Collection collection;
        HashMap hashMap2;
        Map<String, List<c>> map2 = map;
        ArrayList<HashMap> arrayList2 = new ArrayList<>();
        if (map2 == null) {
            return arrayList2;
        }
        HashMap hashMap3 = new HashMap();
        Iterator<String> it2 = a(c(str)).iterator();
        while (it2.hasNext()) {
            String next = it2.next();
            String substring = next.substring(0, 1);
            if (e == null) {
                e = new HashMap<>();
                e.put("a", new String[]{"q", "w", "s", "z"});
                e.put("b", new String[]{"v", "h", "n"});
                it = it2;
                arrayList = arrayList2;
                e.put("c", new String[]{"x", "f", "v"});
                e.put("d", new String[]{"s", "z", "x"});
                e.put("e", new String[]{"w", "s", "d", "r"});
                e.put("f", new String[]{"d", "g", "c", "x"});
                e.put("g", new String[]{"h", "f", "v", "b"});
                hashMap = hashMap3;
                e.put("h", new String[]{"g", "j", "b", "n"});
                str2 = next;
                e.put("i", new String[]{"u", "o", "k", "j"});
                e.put("j", new String[]{"m", "n", "h", "k"});
                e.put("k", new String[]{"j", "l", "m"});
                e.put("l", new String[]{"k", "p", "m"});
                e.put("m", new String[]{"n", "b", "l"});
                e.put("n", new String[]{"b", "j", "m"});
                e.put("o", new String[]{"l", "k", "p"});
                e.put("p", new String[]{"l", "o"});
                e.put("q", new String[]{"w", "a"});
                e.put("r", new String[]{"s", "d", "e", "f"});
                e.put("s", new String[]{"a", "z", "d"});
                e.put("t", new String[]{"r", "f", "g", "y"});
                e.put("u", new String[]{"j", "h", "i", "y"});
                e.put("v", new String[]{"c", "g", "b"});
                e.put("w", new String[]{"q", "a", "s"});
                e.put("x", new String[]{"z", "s", "c"});
                e.put("y", new String[]{"g", "h", "t", "u"});
                e.put("z", new String[]{"a", "s", "x"});
            } else {
                arrayList = arrayList2;
                hashMap = hashMap3;
                it = it2;
                str2 = next;
            }
            HashMap<String, String[]> hashMap4 = e;
            if (hashMap4 == null || !hashMap4.containsKey(substring)) {
                collection = new ArrayList();
            } else {
                collection = Arrays.asList((Object[]) hashMap4.get(substring));
            }
            ArrayList<String> arrayList3 = new ArrayList<>(collection);
            arrayList3.add(substring);
            for (String str3 : arrayList3) {
                ArrayList arrayList4 = (ArrayList) map2.get(str3);
                if (arrayList4 != null) {
                    Iterator it3 = arrayList4.iterator();
                    while (it3.hasNext()) {
                        c cVar = (c) it3.next();
                        String str4 = cVar.f4141a;
                        String str5 = str2;
                        if (((double) a(str4, str5)) > 0.7d) {
                            String str6 = cVar.f4142b;
                            hashMap2 = hashMap;
                            ArrayList arrayList5 = (ArrayList) hashMap2.get(str6);
                            if (arrayList5 == null) {
                                arrayList5 = new ArrayList();
                            }
                            arrayList5.add(str4);
                            hashMap2.put(str6, arrayList5);
                        } else {
                            hashMap2 = hashMap;
                        }
                        str2 = str5;
                        hashMap = hashMap2;
                    }
                }
                str2 = str2;
                hashMap = hashMap;
            }
            it2 = it;
            hashMap3 = hashMap;
            arrayList2 = arrayList;
        }
        ArrayList<HashMap> arrayList6 = arrayList2;
        for (Map.Entry entry : hashMap3.entrySet()) {
            HashMap hashMap5 = new HashMap();
            hashMap5.put("f", entry.getKey());
            hashMap5.put("t", entry.getValue());
            arrayList6.add(hashMap5);
        }
        return arrayList6;
    }

    private static float a(String str, String str2) {
        String trim = str.trim();
        String trim2 = str2.trim();
        String lowerCase = trim.toLowerCase();
        String lowerCase2 = trim2.toLowerCase();
        int length = lowerCase.length();
        int length2 = lowerCase2.length();
        int i = length + 1;
        if (length == 0) {
            return 0.0f;
        }
        int i2 = length2 + 1;
        if (length2 == 0) {
            return 0.0f;
        }
        int i3 = i * i2;
        int[] iArr = new int[i3];
        for (int i4 = 0; i4 < i; i4++) {
            iArr[i4] = i4;
        }
        for (int i5 = 0; i5 < i2; i5++) {
            iArr[i5 * i] = i5;
        }
        for (int i6 = 1; i6 < i; i6++) {
            for (int i7 = 1; i7 < i2; i7++) {
                int i8 = i6 - 1;
                int i9 = i7 - 1;
                int i10 = lowerCase.charAt(i8) == lowerCase2.charAt(i9) ? 0 : 1;
                int i11 = (i7 * i) + i6;
                int i12 = (i9 * i) + i6;
                int i13 = iArr[i12] + 1;
                int i14 = iArr[i11 - 1] + 1;
                int i15 = iArr[i12 - 1] + i10;
                if (i14 < i13) {
                    i13 = i14;
                }
                if (i15 >= i13) {
                    i15 = i13;
                }
                iArr[i11] = i15;
                if (i6 > 1 && i7 > 1) {
                    char charAt = lowerCase.charAt(i8);
                    int i16 = i7 - 2;
                    if (charAt == lowerCase2.charAt(i16) && lowerCase.charAt(i6 - 2) == lowerCase2.charAt(i9)) {
                        int i17 = iArr[i11];
                        int i18 = iArr[((i16 * i) + i6) - 2] + i10;
                        if (i18 < i17) {
                            i17 = i18;
                        }
                        iArr[i11] = i17;
                    }
                }
            }
        }
        int i19 = iArr[i3 - 1];
        if (i > i2) {
            i2 = i;
        }
        return 1.0f - (((float) i19) / ((float) i2));
    }

    private static String c(String str) {
        Spanned spanned;
        String replaceAll = str.replaceAll("<[^<>]+>", "");
        if (Build.VERSION.SDK_INT >= 24) {
            spanned = Html.fromHtml(replaceAll, 0);
        } else {
            spanned = Html.fromHtml(replaceAll);
        }
        return g.a(spanned.toString().toLowerCase());
    }
}
