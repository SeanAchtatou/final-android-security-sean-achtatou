package com.helpshift.support.webkit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.helpshift.util.g;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/* compiled from: CustomWebViewClient */
public class b extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    public static final String f4214a = b.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final a f4215b;
    private Context c;

    /* compiled from: CustomWebViewClient */
    public interface a {
        void a();

        void c();
    }

    public b(Context context, a aVar) {
        this.f4215b = aVar;
        this.c = context;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        if (Build.VERSION.SDK_INT < 24 || !a(webView, webResourceRequest.getUrl().toString())) {
            return super.shouldOverrideUrlLoading(webView, webResourceRequest);
        }
        return true;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (Build.VERSION.SDK_INT >= 24 || !a(webView, str)) {
            return super.shouldOverrideUrlLoading(webView, str);
        }
        return true;
    }

    private boolean a(WebView webView, String str) {
        Context context = webView.getContext();
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String trim = str.trim();
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri parse = Uri.parse(trim);
        intent.setData(parse);
        if (intent.resolveActivity(context.getPackageManager()) == null) {
            return false;
        }
        a(trim, parse.getScheme());
        context.startActivity(intent);
        return true;
    }

    private static void a(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("p", str2);
        hashMap.put("u", str);
        q.c().k().a(com.helpshift.b.b.LINK_VIA_FAQ, hashMap);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        this.f4215b.a();
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.f4215b.c();
    }

    private WebResourceResponse a(String str) {
        URL url;
        File externalCacheDir = this.c.getExternalCacheDir();
        try {
            url = new URL(str);
        } catch (MalformedURLException e) {
            n.a(f4214a, "MalformedURLException", e);
            url = null;
        }
        if (url != null) {
            File file = new File(externalCacheDir, str.replace("/", "_"));
            if (file.exists()) {
                try {
                    return new WebResourceResponse("", "", new FileInputStream(file));
                } catch (FileNotFoundException e2) {
                    n.b(f4214a, "FileNotFoundException", e2);
                }
            } else if (g.a(g.a(url))) {
                g.a(url, file);
            }
        }
        return null;
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
        WebResourceResponse a2;
        if (Build.VERSION.SDK_INT < 21 || (a2 = a(webResourceRequest.getUrl().toString())) == null) {
            return super.shouldInterceptRequest(webView, webResourceRequest);
        }
        return a2;
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        WebResourceResponse a2;
        if (Build.VERSION.SDK_INT >= 21 || (a2 = a(str)) == null) {
            return super.shouldInterceptRequest(webView, str);
        }
        return a2;
    }
}
