package com.helpshift.support.webkit;

import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;

/* compiled from: CustomWebChromeClient */
public class a extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    private final View f4212a;

    /* renamed from: b  reason: collision with root package name */
    private final View f4213b;
    private final ViewGroup c;
    private WebChromeClient.CustomViewCallback d;
    private View e;

    public a(View view, View view2) {
        this.f4212a = view;
        this.f4213b = view2;
        this.c = (ViewGroup) view.findViewById(16908290);
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        if (this.e != null) {
            customViewCallback.onCustomViewHidden();
            return;
        }
        this.c.addView(view);
        this.e = view;
        this.e.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.d = customViewCallback;
        this.f4213b.setVisibility(8);
        int systemUiVisibility = this.f4212a.getSystemUiVisibility();
        if (Build.VERSION.SDK_INT >= 14) {
            systemUiVisibility |= 2;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            systemUiVisibility |= 4;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            systemUiVisibility |= 4096;
        }
        this.f4212a.setSystemUiVisibility(systemUiVisibility);
    }

    public void onHideCustomView() {
        View view = this.e;
        if (view != null) {
            view.setVisibility(8);
            this.c.removeView(this.e);
            this.e = null;
            WebChromeClient.CustomViewCallback customViewCallback = this.d;
            if (customViewCallback != null) {
                customViewCallback.onCustomViewHidden();
            }
            this.f4213b.setVisibility(0);
            int systemUiVisibility = this.f4212a.getSystemUiVisibility();
            if (Build.VERSION.SDK_INT >= 14) {
                systemUiVisibility &= -3;
            }
            if (Build.VERSION.SDK_INT >= 16) {
                systemUiVisibility &= -5;
            }
            if (Build.VERSION.SDK_INT >= 18) {
                systemUiVisibility &= -4097;
            }
            this.f4212a.setSystemUiVisibility(systemUiVisibility);
        }
    }
}
