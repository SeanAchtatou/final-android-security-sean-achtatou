package com.helpshift.support;

import java.util.Comparator;
import java.util.HashMap;

/* compiled from: HSSearch */
class x implements Comparator<String> {

    /* renamed from: a  reason: collision with root package name */
    HashMap<String, Double> f4223a;

    public /* synthetic */ int compare(Object obj, Object obj2) {
        return this.f4223a.get((String) obj).doubleValue() >= this.f4223a.get((String) obj2).doubleValue() ? -1 : 1;
    }

    public x(HashMap<String, Double> hashMap) {
        this.f4223a = hashMap;
    }
}
