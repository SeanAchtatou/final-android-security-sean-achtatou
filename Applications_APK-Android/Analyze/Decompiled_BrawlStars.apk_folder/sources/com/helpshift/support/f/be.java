package com.helpshift.support.f;

import com.helpshift.R;
import com.helpshift.z.e;
import com.helpshift.z.s;

/* compiled from: NewConversationFragment */
class be implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ar f4028a;

    be(ar arVar) {
        this.f4028a = arVar;
    }

    public final void a(Object obj) {
        s sVar = (s) obj;
        bh a2 = this.f4028a.f4008b;
        a2.f.setText(sVar.b());
        a2.f.setSelection(a2.f.getText().length());
        bh a3 = this.f4028a.f4008b;
        s.a a4 = sVar.a();
        boolean e = sVar.e();
        if (s.a.INVALID_EMAIL.equals(a4)) {
            bh.a(a3.e, a3.a(R.string.hs__invalid_email_error));
        } else if (s.a.EMPTY.equals(a4)) {
            bh.a(a3.e, a3.a(R.string.hs__invalid_email_error));
        } else {
            bh.a(a3.e, (CharSequence) null);
        }
        if (e) {
            a3.f.setHint(a3.a(R.string.hs__email_required_hint));
        }
    }
}
