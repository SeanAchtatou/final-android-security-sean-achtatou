package com.helpshift.support.f;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.MenuItem;
import android.view.View;
import com.helpshift.R;
import com.helpshift.support.i.d;
import com.helpshift.support.i.f;
import com.helpshift.support.i.x;
import com.helpshift.support.l.e;
import com.helpshift.support.m.a;
import com.helpshift.support.m.h;
import com.helpshift.support.m.i;
import com.helpshift.support.m.k;
import com.helpshift.util.n;
import com.helpshift.views.c;
import java.lang.ref.WeakReference;

/* compiled from: BaseConversationFragment */
public abstract class b extends f implements MenuItem.OnMenuItemClickListener, d {

    /* renamed from: a  reason: collision with root package name */
    private Snackbar f4018a;

    /* renamed from: b  reason: collision with root package name */
    private Snackbar f4019b;

    /* access modifiers changed from: protected */
    public abstract a.C0146a a();

    /* access modifiers changed from: protected */
    public abstract void a(int i);

    /* access modifiers changed from: protected */
    public abstract String d();

    public final boolean h_() {
        return true;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        return false;
    }

    public void onStart() {
        super.onStart();
        e.a.f4163a.a("current_open_screen", a());
    }

    public void onPause() {
        Snackbar snackbar = this.f4018a;
        if (snackbar != null && snackbar.isShown()) {
            this.f4018a.dismiss();
        }
        Snackbar snackbar2 = this.f4019b;
        if (snackbar2 != null && snackbar2.isShown()) {
            this.f4019b.dismiss();
        }
        super.onPause();
    }

    public final void a(boolean z, int i) {
        String str = i != 2 ? i != 3 ? null : "android.permission.WRITE_EXTERNAL_STORAGE" : "android.permission.READ_EXTERNAL_STORAGE";
        if (z && str != null) {
            h.a(getContext(), getView());
            this.f4018a = i.a(getParentFragment(), new String[]{str}, i, getView());
        } else if (!isDetached()) {
            k.a(getView(), R.string.hs__permission_not_granted, -1);
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        boolean z = false;
        if (iArr.length == 1 && iArr[0] == 0) {
            z = true;
        }
        n.a("Helpshift_BaseConvFrag", "onRequestPermissionsResult : request: " + i + ", result: " + z);
        if (z) {
            a(i);
            return;
        }
        this.f4019b = c.a(getView(), R.string.hs__permission_denied_message, -1).setAction(R.string.hs__permission_denied_snackbar_action, new c(this));
        this.f4019b.show();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((x) getParentFragment()).a((Integer) 0);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        ((x) getParentFragment()).r = new WeakReference<>(this);
    }

    public void onResume() {
        super.onResume();
        b(d());
    }

    public void onDestroyView() {
        k.a(getView());
        x xVar = (x) getParentFragment();
        if (xVar.r != null && xVar.r.get() == this) {
            xVar.r = null;
        }
        super.onDestroyView();
    }

    public void onStop() {
        a.C0146a aVar = (a.C0146a) e.a.f4163a.a("current_open_screen");
        if (aVar != null && aVar.equals(a())) {
            e.a.f4163a.b("current_open_screen");
        }
        b(getString(R.string.hs__help_header));
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public final com.helpshift.support.e.b e() {
        return ((x) getParentFragment()).d;
    }
}
