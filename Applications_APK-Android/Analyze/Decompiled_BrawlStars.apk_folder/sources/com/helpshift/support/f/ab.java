package com.helpshift.support.f;

import android.view.KeyEvent;
import android.widget.TextView;

/* compiled from: ConversationalFragmentRenderer */
class ab implements TextView.OnEditorActionListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f3990a;

    ab(s sVar) {
        this.f3990a = sVar;
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        this.f3990a.h.performClick();
        return false;
    }
}
