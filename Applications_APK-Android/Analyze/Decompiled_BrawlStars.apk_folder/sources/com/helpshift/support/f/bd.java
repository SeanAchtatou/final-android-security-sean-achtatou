package com.helpshift.support.f;

import com.helpshift.R;
import com.helpshift.z.e;
import com.helpshift.z.s;

/* compiled from: NewConversationFragment */
class bd implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ar f4027a;

    bd(ar arVar) {
        this.f4027a = arVar;
    }

    public final void a(Object obj) {
        s sVar = (s) obj;
        bh a2 = this.f4027a.f4008b;
        a2.d.setText(sVar.b());
        a2.d.setSelection(a2.d.getText().length());
        bh a3 = this.f4027a.f4008b;
        s.a a4 = sVar.a();
        if (s.a.EMPTY.equals(a4)) {
            bh.a(a3.c, a3.a(R.string.hs__username_blank_error));
        } else if (s.a.ONLY_SPECIAL_CHARACTERS.equals(a4)) {
            bh.a(a3.c, a3.a(R.string.hs__username_blank_error));
        } else {
            bh.a(a3.c, (CharSequence) null);
        }
    }
}
