package com.helpshift.support.f;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import com.helpshift.R;
import com.helpshift.common.b;
import com.helpshift.common.c.j;
import com.helpshift.common.e.y;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.aa;
import com.helpshift.j.a.a.ab;
import com.helpshift.j.a.a.k;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.y;
import com.helpshift.j.a.b.a;
import com.helpshift.j.i.b;
import com.helpshift.j.i.bn;
import com.helpshift.network.a.e;
import com.helpshift.network.a.h;
import com.helpshift.support.activities.ParentActivity;
import com.helpshift.support.f.a.x;
import com.helpshift.support.f.am;
import com.helpshift.support.i.c;
import com.helpshift.support.i.u;
import com.helpshift.support.m.a;
import com.helpshift.support.m.l;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ConversationalFragment */
public class d extends b implements h, x, al, am.a, com.helpshift.support.i.d {

    /* renamed from: a  reason: collision with root package name */
    protected s f4039a;

    /* renamed from: b  reason: collision with root package name */
    protected boolean f4040b;
    protected Long c;
    public b d;
    public boolean e = false;
    public com.helpshift.j.d.d f;
    public String g;
    public boolean h;
    private boolean l;
    private final String m = "should_show_unread_message_indicator";
    private String n;
    private k o;
    private int p;
    private int q;
    private RecyclerView r;
    private am s;

    public static d a(Bundle bundle) {
        d dVar = new d();
        dVar.setArguments(bundle);
        return dVar;
    }

    public void onAttach(Context context) {
        s sVar;
        super.onAttach(context);
        if (this.j && (sVar = this.f4039a) != null) {
            this.f4040b = sVar.d.getVisibility() == 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.q = getActivity().getWindow().getAttributes().flags;
        getActivity().getWindow().addFlags(2048);
        getActivity().getWindow().clearFlags(1024);
        return layoutInflater.inflate(R.layout.hs__conversation_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        boolean z;
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.c = Long.valueOf(arguments.getLong("issueId"));
            this.l = arguments.getBoolean("show_conv_history");
            z = arguments.getBoolean("create_new_pre_issue");
        } else {
            z = false;
        }
        this.r = (RecyclerView) view.findViewById(R.id.hs__messagesList);
        View findViewById = view.findViewById(R.id.hs__confirmation);
        View findViewById2 = view.findViewById(R.id.scroll_indicator);
        View findViewById3 = view.findViewById(R.id.unread_indicator_red_dot);
        View findViewById4 = view.findViewById(R.id.unread_indicator_red_dot_image_view);
        if (Build.VERSION.SDK_INT < 21) {
            Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.hs__ring);
            findViewById2.setBackgroundDrawable(drawable);
            findViewById3.setBackgroundDrawable(drawable);
        }
        com.helpshift.util.x.a(getContext(), findViewById4, R.drawable.hs__circle, R.attr.colorAccent);
        this.f4039a = new s(getContext(), s(), this.r, getView(), findViewById, findViewById2, findViewById3, (com.helpshift.support.i.x) getParentFragment(), this);
        this.d = q.c().a(this.l, this.c, this.f4039a, this.f4040b);
        s sVar = this.f4039a;
        sVar.f4058a.addTextChangedListener(new aa(sVar));
        sVar.f4058a.setOnEditorActionListener(new ab(sVar));
        sVar.h.setOnClickListener(new ac(sVar));
        this.f4040b = false;
        this.d.C();
        this.e = true;
        if (this.h) {
            this.d.a(this.f, this.g);
            this.h = false;
        }
        view.findViewById(R.id.resolution_accepted_button).setOnClickListener(new e(this));
        view.findViewById(R.id.resolution_rejected_button).setOnClickListener(new k(this));
        ImageButton imageButton = (ImageButton) view.findViewById(R.id.scroll_jump_button);
        com.helpshift.util.x.a(getContext(), imageButton, R.drawable.hs__circle_shape_scroll_jump, R.attr.hs__composeBackgroundColor);
        com.helpshift.util.x.a(getContext(), imageButton.getDrawable(), R.attr.hs__selectableOptionColor);
        imageButton.setOnClickListener(new l(this));
        this.s = new am(new Handler(), this);
        this.r.addOnScrollListener(this.s);
        super.onViewCreated(view, bundle);
        if (bundle != null) {
            this.d.e(bundle.getBoolean("should_show_unread_message_indicator"));
        }
        if (z && bundle == null) {
            this.d.B();
        }
        n.a("Helpshift_ConvalFrag", "Now showing conversation screen");
    }

    private Window s() {
        Dialog dialog;
        Fragment parentFragment = getParentFragment();
        int i = 5;
        while (true) {
            int i2 = i - 1;
            if (i > 0 && parentFragment != null) {
                if ((parentFragment instanceof DialogFragment) && (dialog = ((DialogFragment) parentFragment).getDialog()) != null) {
                    return dialog.getWindow();
                }
                parentFragment = parentFragment.getParentFragment();
                i = i2;
            }
        }
        return getActivity().getWindow();
    }

    public void onResume() {
        super.onResume();
        j a2 = q.c().a();
        this.d.M().a(a2, new m(this));
        this.d.N().a(a2, new n(this));
        this.d.O().a(a2, new o(this));
        this.d.P().a(a2, new p(this));
        this.d.R().a(a2, new q(this));
        this.d.T().a(a2, new r(this));
        this.d.Q().a(a2, new f(this));
        this.d.S().a(a2, new g(this));
        this.d.j();
        this.p = getActivity().getWindow().getAttributes().softInputMode;
        getActivity().getWindow().setSoftInputMode(16);
        if (!this.j) {
            a h2 = this.d.f.h();
            String str = h2.c;
            String str2 = h2.d;
            HashMap hashMap = new HashMap();
            if (!com.helpshift.common.k.a(str)) {
                hashMap.put("id", str);
                this.d.a(com.helpshift.b.b.OPEN_ISSUE, hashMap);
            } else if (!com.helpshift.common.k.a(str2)) {
                hashMap.put("preissue_id", str2);
                this.d.a(com.helpshift.b.b.REPORTED_ISSUE, hashMap);
            }
        }
        e.a().a(this);
        q.c().y().a();
        q.c().y().a(b.a.CONVERSATION);
    }

    public void onPause() {
        e.a().b(this);
        getActivity().getWindow().setSoftInputMode(this.p);
        this.f4039a.f();
        this.d.M().d();
        this.d.N().d();
        this.d.O().d();
        this.d.P().d();
        this.d.Q().d();
        this.d.R().d();
        this.d.T().d();
        this.d.S().d();
        this.d.k();
        super.onPause();
    }

    public final void a(com.helpshift.j.a.a.x xVar, b.a aVar, boolean z) {
        this.d.a(xVar, aVar, z);
    }

    public final void a(v vVar, String str, String str2) {
        com.helpshift.support.e.b e2 = e();
        h hVar = new h(this, vVar, str);
        boolean a2 = l.a(e2.f3908a);
        e2.f3909b.putString("questionPublishId", str);
        e2.f3909b.putString("questionLanguage", str2);
        com.helpshift.support.m.e.a(e2.c, R.id.flow_fragment_container, u.a(e2.f3909b, 3, a2, hVar), null, false);
    }

    public final void a(CharSequence charSequence) {
        this.f4039a.j();
        this.d.f(charSequence != null && !com.helpshift.common.k.a(charSequence.toString()));
    }

    public final void f() {
        this.d.v();
    }

    public final void a(String str) {
        this.d.a(str);
    }

    public final void a(bn bnVar, boolean z) {
        this.d.a(bnVar, z);
    }

    public final boolean h() {
        s sVar = this.f4039a;
        if (sVar.s == null || sVar.t.getState() != 3) {
            return false;
        }
        sVar.t.setState(4);
        return true;
    }

    public final void i_() {
        this.d.t();
    }

    public final void c() {
        this.d.u();
    }

    /* access modifiers changed from: protected */
    public final a.C0146a a() {
        return a.C0146a.CONVERSATION;
    }

    /* access modifiers changed from: protected */
    public final String d() {
        return getString(R.string.hs__conversation_header);
    }

    public final void a(String str, v vVar) {
        this.d.a(str, vVar);
    }

    public final void a(ContextMenu contextMenu, String str) {
        if (!com.helpshift.common.k.a(str)) {
            contextMenu.add(0, 0, 0, R.string.hs__copy).setOnMenuItemClickListener(new i(this, str));
        }
    }

    public final void a(v vVar) {
        this.d.a(vVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.i.x.a(boolean, android.os.Bundle):void
     arg types: [int, android.os.Bundle]
     candidates:
      com.helpshift.support.i.x.a(int, java.lang.Long):void
      com.helpshift.support.i.x.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.i.x.a(com.helpshift.support.i.c, boolean):void
      com.helpshift.support.i.e.a(com.helpshift.support.i.c, boolean):void
      com.helpshift.support.widget.b.a.a(int, java.lang.Long):void
      com.helpshift.support.widget.b.a.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.i.x.a(boolean, android.os.Bundle):void */
    public final void a(aa aaVar) {
        this.n = aaVar.m;
        this.d.A();
        Bundle bundle = new Bundle();
        bundle.putInt("key_screenshot_mode", 3);
        bundle.putString("key_refers_id", this.n);
        ((com.helpshift.support.i.x) getParentFragment()).a(true, bundle);
    }

    public final void a(y yVar) {
        this.d.a(yVar);
    }

    public final void a(ab abVar) {
        this.d.a(abVar);
    }

    public final void a(com.helpshift.j.a.a.b bVar) {
        a(bVar.e(), bVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.f.d.a(boolean, com.helpshift.j.a.a.k):void
     arg types: [int, com.helpshift.j.a.a.e]
     candidates:
      com.helpshift.support.f.d.a(com.helpshift.support.f.d, java.lang.String):void
      com.helpshift.support.f.d.a(int, java.lang.String):void
      com.helpshift.support.f.d.a(android.view.ContextMenu, java.lang.String):void
      com.helpshift.support.f.d.a(com.helpshift.j.i.bn, boolean):void
      com.helpshift.support.f.d.a(java.lang.String, com.helpshift.j.a.a.v):void
      com.helpshift.support.f.b.a(boolean, int):void
      com.helpshift.support.f.a.x.a(int, java.lang.String):void
      com.helpshift.support.f.a.x.a(android.view.ContextMenu, java.lang.String):void
      com.helpshift.support.f.a.x.a(java.lang.String, com.helpshift.j.a.a.v):void
      com.helpshift.support.f.al.a(com.helpshift.j.i.bn, boolean):void
      com.helpshift.support.f.a.x.a(int, java.lang.String):void
      com.helpshift.support.f.a.x.a(android.view.ContextMenu, java.lang.String):void
      com.helpshift.support.f.a.x.a(java.lang.String, com.helpshift.j.a.a.v):void
      com.helpshift.support.f.d.a(boolean, com.helpshift.j.a.a.k):void */
    public final void a(com.helpshift.j.a.a.e eVar) {
        a(true, (k) eVar);
    }

    public final void a(int i, String str) {
        this.d.a(i, str);
    }

    public final void i() {
        this.d.L();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.f.b.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.helpshift.support.f.d.a(com.helpshift.support.f.d, java.lang.String):void
      com.helpshift.support.f.d.a(boolean, com.helpshift.j.a.a.k):void
      com.helpshift.support.f.d.a(int, java.lang.String):void
      com.helpshift.support.f.d.a(android.view.ContextMenu, java.lang.String):void
      com.helpshift.support.f.d.a(com.helpshift.j.i.bn, boolean):void
      com.helpshift.support.f.d.a(java.lang.String, com.helpshift.j.a.a.v):void
      com.helpshift.support.f.a.x.a(int, java.lang.String):void
      com.helpshift.support.f.a.x.a(android.view.ContextMenu, java.lang.String):void
      com.helpshift.support.f.a.x.a(java.lang.String, com.helpshift.j.a.a.v):void
      com.helpshift.support.f.al.a(com.helpshift.j.i.bn, boolean):void
      com.helpshift.support.f.a.x.a(int, java.lang.String):void
      com.helpshift.support.f.a.x.a(android.view.ContextMenu, java.lang.String):void
      com.helpshift.support.f.a.x.a(java.lang.String, com.helpshift.j.a.a.v):void
      com.helpshift.support.f.b.a(boolean, int):void */
    private void a(boolean z, k kVar) {
        this.o = null;
        if (z) {
            int i = j.f4049b[q.b().d().a(y.b.WRITE_STORAGE).ordinal()];
            if (i == 1) {
                this.d.a(kVar);
            } else if (i == 2) {
                c(kVar.e);
            } else if (i == 3) {
                this.o = kVar;
                a(true, 3);
            }
        } else {
            this.d.a(kVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.i.x.a(boolean, android.os.Bundle):void
     arg types: [int, android.os.Bundle]
     candidates:
      com.helpshift.support.i.x.a(int, java.lang.Long):void
      com.helpshift.support.i.x.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.i.x.a(com.helpshift.support.i.c, boolean):void
      com.helpshift.support.i.e.a(com.helpshift.support.i.c, boolean):void
      com.helpshift.support.widget.b.a.a(int, java.lang.Long):void
      com.helpshift.support.widget.b.a.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.i.x.a(boolean, android.os.Bundle):void */
    /* access modifiers changed from: protected */
    public final void a(int i) {
        k kVar;
        if (i == 2) {
            Bundle bundle = new Bundle();
            bundle.putInt("key_screenshot_mode", 3);
            bundle.putString("key_refers_id", this.n);
            ((com.helpshift.support.i.x) getParentFragment()).a(false, bundle);
        } else if (i == 3 && (kVar = this.o) != null) {
            this.d.a(kVar);
            this.o = null;
        }
    }

    private void c(String str) {
        DownloadManager downloadManager = (DownloadManager) getContext().getSystemService("download");
        if (downloadManager != null) {
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(str));
            request.setNotificationVisibility(1);
            downloadManager.enqueue(request);
            if (!isDetached()) {
                com.helpshift.support.m.k.a(getView(), R.string.hs__starting_download, -1);
            }
        }
    }

    public void onDestroyView() {
        if (getActivity() != null) {
            getActivity().getWindow().clearFlags(2048);
            Window window = getActivity().getWindow();
            int i = this.q;
            window.setFlags(i, i);
        }
        this.e = false;
        this.d.a(-1);
        s sVar = this.f4039a;
        if (sVar.f != null) {
            sVar.f.f4005a = null;
        }
        this.d.m();
        s sVar2 = this.f4039a;
        sVar2.a(true);
        sVar2.c = null;
        this.r.removeOnScrollListener(this.s);
        this.r = null;
        super.onDestroyView();
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("should_show_unread_message_indicator", this.d.o());
        super.onSaveInstanceState(bundle);
    }

    public final void j() {
        com.helpshift.j.i.b bVar = this.d;
        if (bVar != null) {
            bVar.D();
        }
    }

    public final void k() {
        this.d.l();
        this.f4039a.b(this.d.Q().a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.i.x.a(boolean, android.os.Bundle):void
     arg types: [int, android.os.Bundle]
     candidates:
      com.helpshift.support.i.x.a(int, java.lang.Long):void
      com.helpshift.support.i.x.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.i.x.a(com.helpshift.support.i.c, boolean):void
      com.helpshift.support.i.e.a(com.helpshift.support.i.c, boolean):void
      com.helpshift.support.widget.b.a.a(int, java.lang.Long):void
      com.helpshift.support.widget.b.a.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.i.x.a(boolean, android.os.Bundle):void */
    public final void a(c cVar) {
        if (j.c[cVar.ordinal()] == 1) {
            this.n = null;
            this.d.A();
            Bundle bundle = new Bundle();
            bundle.putInt("key_screenshot_mode", 3);
            bundle.putString("key_refers_id", null);
            ((com.helpshift.support.i.x) getParentFragment()).a(true, bundle);
        }
    }

    public final void l() {
        this.d.r();
    }

    public final void m() {
        this.d.s();
    }

    public final void n() {
        e().e();
    }

    public final void o() {
        this.d.K();
    }

    public final void p() {
        this.d.I();
    }

    public final void q() {
        this.d.J();
    }

    public void onDestroy() {
        this.d.U();
        super.onDestroy();
    }

    public final void b(int i) {
        com.helpshift.support.i.x xVar = (com.helpshift.support.i.x) getParentFragment();
        if (xVar != null && Build.VERSION.SDK_INT >= 19) {
            if (xVar.n != null) {
                xVar.o = xVar.n.getImportantForAccessibility();
                xVar.n.setImportantForAccessibility(4);
                return;
            }
            Activity a2 = com.helpshift.support.i.x.a((Fragment) xVar);
            if (a2 instanceof ParentActivity) {
                ((ParentActivity) a2).a(4);
            }
        }
    }

    public final void g() {
        com.helpshift.support.i.x xVar = (com.helpshift.support.i.x) getParentFragment();
        if (xVar != null && Build.VERSION.SDK_INT >= 19) {
            if (xVar.n != null) {
                xVar.n.setImportantForAccessibility(xVar.o);
                return;
            }
            Activity a2 = com.helpshift.support.i.x.a((Fragment) xVar);
            if (a2 instanceof ParentActivity) {
                ((ParentActivity) a2).a(0);
            }
        }
    }

    public final void a(Map<String, Boolean> map) {
        ((com.helpshift.support.i.x) getParentFragment()).d.a(map);
    }

    public void onDetach() {
        if (!this.j) {
            q.c().s().a(true);
        }
        super.onDetach();
    }

    static /* synthetic */ void a(d dVar, String str) {
        ((ClipboardManager) dVar.getContext().getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("Copy Text", str));
        com.helpshift.views.d.a(dVar.getContext(), dVar.getString(R.string.hs__copied_to_clipboard), 0).show();
    }
}
