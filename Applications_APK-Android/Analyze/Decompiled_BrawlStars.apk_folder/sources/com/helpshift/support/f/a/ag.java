package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.ad;
import com.helpshift.j.a.a.v;
import com.helpshift.util.x;

/* compiled from: SystemDateMessageDataBinder */
public class ag extends u<a, ad> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.util.x.a(android.content.Context, float):float
     arg types: [android.content.Context, int]
     candidates:
      com.helpshift.util.x.a(android.content.Context, int):int
      com.helpshift.util.x.a(android.content.Context, android.graphics.drawable.Drawable):void
      com.helpshift.util.x.a(android.content.Context, float):float */
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        a aVar = (a) viewHolder;
        ad adVar = (ad) vVar;
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) aVar.itemView.getLayoutParams();
        if (adVar.f3450a) {
            layoutParams.topMargin = (int) x.a(this.f3979a, 18.0f);
        } else {
            layoutParams.topMargin = (int) x.a(this.f3979a, 2.0f);
        }
        aVar.itemView.setLayoutParams(layoutParams);
        aVar.f3925b.setText(adVar.b());
    }

    public ag(Context context) {
        super(context);
    }

    /* compiled from: SystemDateMessageDataBinder */
    public class a extends RecyclerView.ViewHolder {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final TextView f3925b;

        public a(View view) {
            super(view);
            this.f3925b = (TextView) view.findViewById(R.id.system_message);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_system_layout, viewGroup, false));
    }
}
