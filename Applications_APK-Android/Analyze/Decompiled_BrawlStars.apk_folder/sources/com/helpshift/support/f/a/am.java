package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.ai;
import com.helpshift.j.a.a.v;
import com.helpshift.util.k;

/* compiled from: UserRedactedMessageDataBinder */
public class am extends u<a, v> {
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        a aVar = (a) viewHolder;
        aVar.f3935a.setText(b(a(vVar.n)));
        a(aVar.f3935a);
        aVar.d.setContentDescription(this.f3979a.getString(R.string.hs__user_sent_message_voice_over, vVar.i()));
        a(aVar.f3935a, (k.a) null);
        ai l = vVar.l();
        b(aVar.c, l);
        b(aVar.f3936b, l, vVar.h());
    }

    public am(Context context) {
        super(context);
    }

    /* compiled from: UserRedactedMessageDataBinder */
    protected final class a extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        /* renamed from: a  reason: collision with root package name */
        final TextView f3935a;

        /* renamed from: b  reason: collision with root package name */
        final TextView f3936b;
        final FrameLayout c;
        final View d;

        a(View view) {
            super(view);
            this.f3935a = (TextView) view.findViewById(R.id.user_message_text);
            this.f3936b = (TextView) view.findViewById(R.id.user_date_text);
            this.c = (FrameLayout) view.findViewById(R.id.user_message_container);
            this.d = view.findViewById(R.id.user_text_message_layout);
        }

        public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            if (am.this.f3980b != null) {
                am.this.f3980b.a(contextMenu, ((TextView) view).getText().toString());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        a aVar = new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_txt_user, viewGroup, false));
        a(aVar.c.getLayoutParams());
        aVar.f3935a.setOnCreateContextMenuListener(aVar);
        return aVar;
    }
}
