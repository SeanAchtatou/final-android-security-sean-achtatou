package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.ab;
import com.helpshift.support.m.l;
import com.helpshift.support.views.HSRoundedImageView;

/* compiled from: ScreenshotMessageViewDataBinder */
public class ad extends u<a, ab> {
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0119  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void a(android.support.v7.widget.RecyclerView.ViewHolder r17, com.helpshift.j.a.a.v r18) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            com.helpshift.support.f.a.ad$a r1 = (com.helpshift.support.f.a.ad.a) r1
            r2 = r18
            com.helpshift.j.a.a.ab r2 = (com.helpshift.j.a.a.ab) r2
            java.lang.String r3 = r2.b()
            android.content.Context r4 = r0.f3979a
            r5 = 16842808(0x1010038, float:2.3693715E-38)
            int r4 = com.helpshift.support.m.l.a(r4, r5)
            boolean r5 = com.helpshift.util.y.a(r3)
            r6 = 1
            r5 = r5 ^ r6
            r7 = 1056964608(0x3f000000, float:0.5)
            int[] r8 = com.helpshift.support.f.a.af.f3923a
            com.helpshift.j.a.a.am r9 = r2.D
            int r9 = r9.ordinal()
            r8 = r8[r9]
            r10 = 0
            if (r8 == r6) goto L_0x009e
            r11 = 2
            if (r8 == r11) goto L_0x007d
            r11 = 3
            if (r8 == r11) goto L_0x0063
            r11 = 4
            if (r8 == r11) goto L_0x0040
            java.lang.String r6 = ""
            r14 = r6
            r8 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r6 = r4
            r4 = 0
            goto L_0x00c0
        L_0x0040:
            r7 = 1065353216(0x3f800000, float:1.0)
            java.lang.String r8 = r2.h()
            boolean r11 = com.helpshift.util.y.a(r3)
            r12 = r11 ^ 1
            android.content.Context r13 = r0.f3979a
            int r14 = com.helpshift.R.string.hs__user_sent_message_voice_over
            java.lang.Object[] r6 = new java.lang.Object[r6]
            java.lang.String r15 = r2.i()
            r6[r10] = r15
            java.lang.String r6 = r13.getString(r14, r6)
            r14 = r6
            r13 = r12
            r12 = 0
            r6 = r4
            r4 = r8
            r8 = 0
            goto L_0x00c0
        L_0x0063:
            android.content.Context r8 = r0.f3979a
            android.content.res.Resources r8 = r8.getResources()
            int r11 = com.helpshift.R.string.hs__sending_msg
            java.lang.String r8 = r8.getString(r11)
            android.content.Context r11 = r0.f3979a
            int r12 = com.helpshift.R.string.hs__user_sending_message_voice_over
            java.lang.String r11 = r11.getString(r12)
            r6 = r4
            r4 = r8
            r14 = r11
            r8 = 0
            r11 = 1
            goto L_0x009c
        L_0x007d:
            android.content.Context r4 = r0.f3979a
            android.content.res.Resources r4 = r4.getResources()
            int r6 = com.helpshift.R.string.hs__sending_fail_msg
            java.lang.String r4 = r4.getString(r6)
            android.content.Context r6 = r0.f3979a
            int r8 = com.helpshift.R.attr.hs__errorTextColor
            int r6 = com.helpshift.support.m.l.a(r6, r8)
            android.content.Context r8 = r0.f3979a
            int r11 = com.helpshift.R.string.hs__user_failed_message_voice_over
            java.lang.String r8 = r8.getString(r11)
            r14 = r8
            r8 = 0
            r11 = 0
        L_0x009c:
            r12 = 0
            goto L_0x00bf
        L_0x009e:
            android.content.Context r4 = r0.f3979a
            android.content.res.Resources r4 = r4.getResources()
            int r8 = com.helpshift.R.string.hs__sending_fail_msg
            java.lang.String r4 = r4.getString(r8)
            android.content.Context r8 = r0.f3979a
            int r11 = com.helpshift.R.attr.hs__errorTextColor
            int r8 = com.helpshift.support.m.l.a(r8, r11)
            android.content.Context r11 = r0.f3979a
            int r12 = com.helpshift.R.string.hs__user_failed_message_voice_over
            java.lang.String r11 = r11.getString(r12)
            r12 = r1
            r6 = r8
            r14 = r11
            r8 = 1
            r11 = 0
        L_0x00bf:
            r13 = 0
        L_0x00c0:
            com.helpshift.j.a.a.ai r15 = r2.l()
            com.helpshift.support.views.HSRoundedImageView r9 = r1.f3920b
            r9.a(r3)
            com.helpshift.support.views.HSRoundedImageView r3 = r1.f3920b
            r3.setAlpha(r7)
            com.helpshift.support.views.HSRoundedImageView r3 = r1.f3920b
            a(r3, r5)
            android.widget.TextView r3 = r1.c
            r3.setVisibility(r10)
            boolean r3 = r15.a()
            if (r3 == 0) goto L_0x00e8
            android.widget.TextView r3 = r1.c
            r3.setText(r4)
            android.widget.TextView r3 = r1.c
            r3.setTextColor(r6)
        L_0x00e8:
            android.widget.TextView r3 = r1.c
            boolean r4 = r15.a()
            a(r3, r4)
            android.widget.ProgressBar r3 = r1.f
            a(r3, r11)
            android.widget.ImageView r3 = r1.d
            a(r3, r8)
            if (r8 == 0) goto L_0x0106
            android.widget.ImageView r3 = r1.d
            r3.setOnClickListener(r12)
            r4 = 0
            goto L_0x010c
        L_0x0106:
            android.widget.ImageView r3 = r1.d
            r4 = 0
            r3.setOnClickListener(r4)
        L_0x010c:
            if (r13 == 0) goto L_0x0119
            com.helpshift.support.views.HSRoundedImageView r3 = r1.f3920b
            com.helpshift.support.f.a.ae r4 = new com.helpshift.support.f.a.ae
            r4.<init>(r0, r2)
            r3.setOnClickListener(r4)
            goto L_0x011e
        L_0x0119:
            com.helpshift.support.views.HSRoundedImageView r2 = r1.f3920b
            r2.setOnClickListener(r4)
        L_0x011e:
            android.view.View r1 = r1.f3919a
            r1.setContentDescription(r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.f.a.ad.a(android.support.v7.widget.RecyclerView$ViewHolder, com.helpshift.j.a.a.v):void");
    }

    public ad(Context context) {
        super(context);
    }

    /* compiled from: ScreenshotMessageViewDataBinder */
    protected final class a extends RecyclerView.ViewHolder implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final View f3919a;

        /* renamed from: b  reason: collision with root package name */
        final HSRoundedImageView f3920b;
        final TextView c;
        final ImageView d;
        /* access modifiers changed from: private */
        public final ProgressBar f;

        a(View view) {
            super(view);
            this.f3919a = view.findViewById(R.id.user_image_message_layout);
            this.f = (ProgressBar) view.findViewById(R.id.upload_attachment_progressbar);
            this.f3920b = (HSRoundedImageView) view.findViewById(R.id.user_attachment_imageview);
            this.c = (TextView) view.findViewById(R.id.date);
            this.d = (ImageView) view.findViewById(R.id.user_message_retry_button);
            l.b(ad.this.f3979a, this.f.getIndeterminateDrawable());
        }

        public final void onClick(View view) {
            if (ad.this.f3980b != null) {
                ad.this.f3980b.a(getAdapterPosition());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new a(LayoutInflater.from(this.f3979a).inflate(R.layout.hs__msg_screenshot_status, viewGroup, false));
    }
}
