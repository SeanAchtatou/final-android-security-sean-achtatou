package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.ai;
import com.helpshift.j.a.a.n;
import com.helpshift.j.a.a.v;

/* compiled from: ConfirmationRejectedMessageDataBinder */
class p extends u<a, n> {
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        a aVar = (a) viewHolder;
        n nVar = (n) vVar;
        aVar.f3968b.setText(R.string.hs__cr_msg);
        ai l = nVar.l();
        a(aVar.d, l.b() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_admin, R.attr.hs__chatBubbleAdminBackgroundColor);
        if (l.a()) {
            aVar.c.setText(nVar.h());
        }
        aVar.f3967a.setContentDescription(a(nVar));
        a(aVar.c, l.a());
    }

    p(Context context) {
        super(context);
    }

    /* compiled from: ConfirmationRejectedMessageDataBinder */
    protected final class a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        final View f3967a;

        /* renamed from: b  reason: collision with root package name */
        final TextView f3968b;
        final TextView c;
        final View d;

        a(View view) {
            super(view);
            this.f3967a = view.findViewById(R.id.admin_text_message_layout);
            this.f3968b = (TextView) view.findViewById(R.id.admin_message_text);
            this.c = (TextView) view.findViewById(R.id.admin_date_text);
            this.d = view.findViewById(R.id.admin_message_container);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_txt_admin, viewGroup, false));
    }
}
