package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.ah;
import com.helpshift.j.a.a.v;

/* compiled from: SystemRedactedConversationDataBinder */
public class aj extends u<a, ah> {
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        String str;
        a aVar = (a) viewHolder;
        ah ahVar = (ah) vVar;
        int i = ahVar.f3453a;
        if (ahVar.f3453a > 1) {
            str = this.f3979a.getString(R.string.hs__conversation_redacted_status_multiple, Integer.valueOf(i));
        } else {
            str = this.f3979a.getString(R.string.hs__conversation_redacted_status);
        }
        aVar.f3931b.setText(str);
    }

    public aj(Context context) {
        super(context);
    }

    /* compiled from: SystemRedactedConversationDataBinder */
    public class a extends RecyclerView.ViewHolder {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public TextView f3931b;

        public a(View view) {
            super(view);
            this.f3931b = (TextView) view.findViewById(R.id.conversation_redacted_view);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_system_conversation_redacted_layout, viewGroup, false));
    }
}
