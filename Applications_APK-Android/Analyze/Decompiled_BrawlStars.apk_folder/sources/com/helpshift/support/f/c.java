package com.helpshift.support.f;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

/* compiled from: BaseConversationFragment */
class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f4033a;

    c(b bVar) {
        this.f4033a = bVar;
    }

    public void onClick(View view) {
        Context context = this.f4033a.getContext();
        try {
            Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.addCategory("android.intent.category.DEFAULT");
            String packageName = context.getPackageName();
            intent.setData(Uri.parse("package:" + packageName));
            context.startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            Intent intent2 = new Intent("android.settings.MANAGE_APPLICATIONS_SETTINGS");
            intent2.addCategory("android.intent.category.DEFAULT");
            context.startActivity(intent2);
        }
    }
}
