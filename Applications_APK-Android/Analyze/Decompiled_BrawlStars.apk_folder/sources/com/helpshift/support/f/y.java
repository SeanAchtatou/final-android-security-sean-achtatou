package com.helpshift.support.f;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import com.helpshift.common.g.b;
import com.helpshift.util.q;
import java.util.Calendar;

/* compiled from: ConversationalFragmentRenderer */
class y implements DatePickerDialog.OnDateSetListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f4065a;

    y(s sVar) {
        this.f4065a = sVar;
    }

    public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
        Calendar instance = Calendar.getInstance();
        instance.set(i, i2, i3);
        this.f4065a.f4058a.setText(b.a("EEEE, MMMM dd, yyyy", q.c().w().c()).a(instance.getTime()));
    }
}
