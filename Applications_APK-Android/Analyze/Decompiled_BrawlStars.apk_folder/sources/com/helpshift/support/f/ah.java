package com.helpshift.support.f;

import android.view.View;
import com.helpshift.support.m.h;

/* compiled from: ConversationalFragmentRenderer */
class ah implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f3996a;

    ah(s sVar) {
        this.f3996a = sVar;
    }

    public void onClick(View view) {
        this.f3996a.G.setVisibility(0);
        this.f3996a.u.setVisibility(8);
        this.f3996a.C.setVisibility(8);
        this.f3996a.G.requestFocus();
        this.f3996a.s.a(false);
        this.f3996a.B.setVisibility(8);
        this.f3996a.E.setVisibility(0);
        h.b(this.f3996a.g, this.f3996a.G);
        this.f3996a.s.a(false);
    }
}
