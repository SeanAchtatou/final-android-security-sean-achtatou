package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.b;
import com.helpshift.support.m.l;
import com.helpshift.util.x;

/* compiled from: AdminAttachmentMessageDataBinder */
class a extends u<C0143a, b> {
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0099  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void a(android.support.v7.widget.RecyclerView.ViewHolder r10, com.helpshift.j.a.a.v r11) {
        /*
            r9 = this;
            com.helpshift.support.f.a.a$a r10 = (com.helpshift.support.f.a.a.C0143a) r10
            com.helpshift.j.a.a.b r11 = (com.helpshift.j.a.a.b) r11
            android.content.Context r0 = r9.f3979a
            r1 = 16842806(0x1010036, float:2.369371E-38)
            int r0 = com.helpshift.support.m.l.a(r0, r1)
            java.lang.String r1 = r11.g()
            int[] r2 = com.helpshift.support.f.a.c.f3945a
            com.helpshift.j.a.a.b$a r3 = r11.f3466a
            int r3 = r3.ordinal()
            r2 = r2[r3]
            r3 = 2
            r4 = 0
            r5 = 1
            if (r2 == r5) goto L_0x0069
            r6 = 3
            if (r2 == r3) goto L_0x0045
            if (r2 == r6) goto L_0x002d
            java.lang.String r2 = ""
            r3 = r2
        L_0x0028:
            r5 = 0
        L_0x0029:
            r2 = r1
            r1 = r0
            r0 = 0
            goto L_0x0080
        L_0x002d:
            android.content.Context r0 = r9.f3979a
            int r2 = com.helpshift.R.attr.colorAccent
            int r0 = com.helpshift.support.m.l.a(r0, r2)
            android.content.Context r2 = r9.f3979a
            int r3 = com.helpshift.R.string.hs__attachment_downloaded__voice_over
            java.lang.Object[] r6 = new java.lang.Object[r5]
            java.lang.String r7 = r11.d
            r6[r4] = r7
            java.lang.String r2 = r2.getString(r3, r6)
            r3 = r2
            goto L_0x0029
        L_0x0045:
            java.lang.String r1 = r11.c()
            android.content.Context r2 = r9.f3979a
            int r7 = com.helpshift.R.string.hs__attachment_downloading_voice_over
            java.lang.Object[] r6 = new java.lang.Object[r6]
            java.lang.String r8 = r11.d
            r6[r4] = r8
            java.lang.String r8 = r11.d()
            r6[r5] = r8
            java.lang.String r8 = r11.g()
            r6[r3] = r8
            java.lang.String r2 = r2.getString(r7, r6)
            r3 = r2
            r5 = 0
            r2 = r1
            r1 = r0
            r0 = 1
            goto L_0x0080
        L_0x0069:
            android.content.Context r2 = r9.f3979a
            int r6 = com.helpshift.R.string.hs__attachment_not_downloaded_voice_over
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.String r7 = r11.d
            r3[r4] = r7
            java.lang.String r7 = r11.g()
            r3[r5] = r7
            java.lang.String r2 = r2.getString(r6, r3)
            r3 = r2
            r4 = 1
            goto L_0x0028
        L_0x0080:
            android.view.View r6 = r10.f
            a(r6, r4)
            android.widget.ImageView r4 = r10.g
            a(r4, r5)
            android.widget.ProgressBar r4 = r10.e
            a(r4, r0)
            com.helpshift.j.a.a.ai r0 = r11.l()
            boolean r4 = r0.a()
            if (r4 == 0) goto L_0x00a2
            android.widget.TextView r4 = r10.h
            java.lang.String r5 = r11.h()
            r4.setText(r5)
        L_0x00a2:
            android.widget.TextView r4 = r10.h
            boolean r0 = r0.a()
            a(r4, r0)
            android.widget.TextView r0 = r10.f3912b
            java.lang.String r4 = r11.d
            r0.setText(r4)
            android.widget.TextView r0 = r10.c
            r0.setText(r2)
            android.widget.TextView r0 = r10.f3912b
            r0.setTextColor(r1)
            android.view.View r0 = r10.d
            com.helpshift.support.f.a.b r1 = new com.helpshift.support.f.a.b
            r1.<init>(r9, r11)
            r0.setOnClickListener(r1)
            android.view.View r0 = r10.d
            r0.setContentDescription(r3)
            android.view.View r10 = r10.f3911a
            java.lang.String r11 = r9.a(r11)
            r10.setContentDescription(r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.f.a.a.a(android.support.v7.widget.RecyclerView$ViewHolder, com.helpshift.j.a.a.v):void");
    }

    a(Context context) {
        super(context);
    }

    /* renamed from: com.helpshift.support.f.a.a$a  reason: collision with other inner class name */
    /* compiled from: AdminAttachmentMessageDataBinder */
    protected final class C0143a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        final View f3911a;

        /* renamed from: b  reason: collision with root package name */
        final TextView f3912b;
        final TextView c;
        final View d;
        final ProgressBar e;
        final View f;
        final ImageView g;
        final TextView h;

        C0143a(View view) {
            super(view);
            this.f3911a = view.findViewById(R.id.admin_attachment_message_layout);
            this.f3912b = (TextView) view.findViewById(R.id.attachment_file_name);
            this.c = (TextView) view.findViewById(R.id.attachment_file_size);
            this.d = view.findViewById(R.id.admin_message);
            this.f = view.findViewById(R.id.download_button);
            this.e = (ProgressBar) view.findViewById(R.id.progress);
            this.g = (ImageView) view.findViewById(R.id.attachment_icon);
            this.h = (TextView) view.findViewById(R.id.attachment_date);
            x.a(a.this.f3979a, ((ImageView) view.findViewById(R.id.hs_download_foreground_view)).getDrawable(), R.attr.hs__chatBubbleMediaBackgroundColor);
            x.a(a.this.f3979a, this.d.getBackground(), R.attr.hs__chatBubbleMediaBackgroundColor);
            l.b(a.this.f3979a, this.e.getIndeterminateDrawable());
            l.b(a.this.f3979a, this.g.getDrawable());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new C0143a(LayoutInflater.from(this.f3979a).inflate(R.layout.hs__msg_attachment_generic, viewGroup, false));
    }
}
