package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.ai;
import com.helpshift.j.a.a.v;

/* compiled from: RequestAppReviewMessageDataBinder */
public class y extends u<a, com.helpshift.j.a.a.y> {
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        a aVar = (a) viewHolder;
        com.helpshift.j.a.a.y yVar = (com.helpshift.j.a.a.y) vVar;
        aVar.f3986b.setText(R.string.hs__review_request_message);
        if (yVar.f3498a) {
            aVar.c.setVisibility(8);
        } else {
            aVar.c.setVisibility(0);
        }
        ai l = yVar.l();
        a(aVar.e, l.b() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_admin, R.attr.hs__chatBubbleAdminBackgroundColor);
        if (l.a()) {
            aVar.d.setText(yVar.h());
        }
        a(aVar.d, l.a());
        if (yVar.f3499b) {
            aVar.c.setOnClickListener(new z(this, yVar));
        } else {
            aVar.c.setOnClickListener(null);
        }
        aVar.f3985a.setContentDescription(a(yVar));
    }

    public y(Context context) {
        super(context);
    }

    /* compiled from: RequestAppReviewMessageDataBinder */
    protected final class a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        final View f3985a;

        /* renamed from: b  reason: collision with root package name */
        final TextView f3986b;
        final Button c;
        final TextView d;
        final View e;

        a(View view) {
            super(view);
            this.f3985a = view.findViewById(R.id.admin_review_message_layout);
            this.f3986b = (TextView) view.findViewById(R.id.review_request_message);
            this.c = (Button) view.findViewById(R.id.review_request_button);
            this.d = (TextView) view.findViewById(R.id.review_request_date);
            this.e = view.findViewById(R.id.review_request_message_container);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_review_request, viewGroup, false));
    }
}
