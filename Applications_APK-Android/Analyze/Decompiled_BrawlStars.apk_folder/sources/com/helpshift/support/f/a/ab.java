package com.helpshift.support.f.a;

import android.view.View;
import com.helpshift.j.a.a.aa;

/* compiled from: RequestScreenshotMessageDataBinder */
class ab implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aa f3915a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ aa f3916b;

    ab(aa aaVar, aa aaVar2) {
        this.f3916b = aaVar;
        this.f3915a = aaVar2;
    }

    public void onClick(View view) {
        if (this.f3915a.b() && this.f3916b.f3980b != null) {
            this.f3916b.f3980b.a(this.f3915a);
        }
    }
}
