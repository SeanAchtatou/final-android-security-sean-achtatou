package com.helpshift.support.f;

import android.content.Context;
import android.content.res.TypedArray;
import com.helpshift.R;
import com.helpshift.support.m.l;
import com.helpshift.z.a;
import com.helpshift.z.e;

/* compiled from: ConversationalFragment */
class r implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f4057a;

    r(d dVar) {
        this.f4057a = dVar;
    }

    public final void a(Object obj) {
        s sVar = this.f4057a.f4039a;
        if (((a) obj).b()) {
            sVar.h.setEnabled(true);
            l.a(sVar.h, 255);
            l.a(sVar.g, sVar.h.getDrawable(), true);
            return;
        }
        sVar.h.setEnabled(false);
        Context context = sVar.g;
        int i = R.attr.hs__reply_button_disabled_alpha;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{i});
        int i2 = obtainStyledAttributes.getInt(0, 0);
        obtainStyledAttributes.recycle();
        l.a(sVar.h, i2);
        l.a(sVar.g, sVar.h.getDrawable(), false);
    }
}
