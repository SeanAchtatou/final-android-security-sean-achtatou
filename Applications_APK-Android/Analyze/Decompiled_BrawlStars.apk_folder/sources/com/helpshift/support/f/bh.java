package com.helpshift.support.f;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.helpshift.R;
import com.helpshift.common.exception.a;
import com.helpshift.j.d.d;
import com.helpshift.j.i.av;
import com.helpshift.support.i.c;
import com.helpshift.support.i.e;
import com.helpshift.support.m.k;
import java.util.ArrayList;

/* compiled from: NewConversationFragmentRenderer */
public class bh implements av {

    /* renamed from: a  reason: collision with root package name */
    final TextInputLayout f4031a;

    /* renamed from: b  reason: collision with root package name */
    final TextInputEditText f4032b;
    final TextInputLayout c;
    final TextInputEditText d;
    final TextInputLayout e;
    final TextInputEditText f;
    final ProgressBar g;
    final ImageView h;
    final TextView i;
    final TextView j;
    final CardView k;
    final ImageButton l;
    final View m;
    private final Context n;
    private final bi o;
    private final e p;

    bh(Context context, TextInputLayout textInputLayout, TextInputEditText textInputEditText, TextInputLayout textInputLayout2, TextInputEditText textInputEditText2, TextInputLayout textInputLayout3, TextInputEditText textInputEditText3, ProgressBar progressBar, ImageView imageView, TextView textView, TextView textView2, CardView cardView, ImageButton imageButton, View view, bi biVar, e eVar) {
        this.n = context;
        this.f4031a = textInputLayout;
        this.f4032b = textInputEditText;
        this.c = textInputLayout2;
        this.d = textInputEditText2;
        this.e = textInputLayout3;
        this.f = textInputEditText3;
        this.g = progressBar;
        this.h = imageView;
        this.i = textView;
        this.j = textView2;
        this.k = cardView;
        this.l = imageButton;
        this.m = view;
        this.o = biVar;
        this.p = eVar;
    }

    /* access modifiers changed from: package-private */
    public String a(int i2) {
        return this.n.getText(i2).toString();
    }

    public final void a(boolean z) {
        a(c.SCREENSHOT_ATTACHMENT, z);
    }

    public final void b(boolean z) {
        a(c.START_NEW_CONVERSATION, z);
    }

    public final void a() {
        this.o.f();
    }

    public final void b() {
        this.o.c();
    }

    public final void a(d dVar) {
        this.o.a(dVar);
    }

    public final void c() {
        Context context = this.n;
        Toast a2 = com.helpshift.views.d.a(context, context.getResources().getText(R.string.hs__conversation_started_message), 0);
        a2.setGravity(16, 0, 0);
        a2.show();
    }

    public final void a(ArrayList arrayList) {
        this.o.a(arrayList);
    }

    public final void a(a aVar) {
        k.a(aVar, this.m);
    }

    public final void d() {
        this.o.g();
    }

    static void a(TextInputLayout textInputLayout, CharSequence charSequence) {
        textInputLayout.setErrorEnabled(!TextUtils.isEmpty(charSequence));
        textInputLayout.setError(charSequence);
    }

    private void a(c cVar, boolean z) {
        e eVar = this.p;
        if (eVar != null) {
            eVar.a(cVar, z);
        }
    }
}
