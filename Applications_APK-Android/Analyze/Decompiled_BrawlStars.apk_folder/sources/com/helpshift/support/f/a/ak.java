package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.al;

/* compiled from: UserMessageViewDataBinder */
public class ak extends u<a, al> {
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00d2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void a(android.support.v7.widget.RecyclerView.ViewHolder r9, com.helpshift.j.a.a.v r10) {
        /*
            r8 = this;
            com.helpshift.support.f.a.ak$a r9 = (com.helpshift.support.f.a.ak.a) r9
            com.helpshift.j.a.a.al r10 = (com.helpshift.j.a.a.al) r10
            com.helpshift.j.a.a.am r0 = r10.e()
            android.widget.TextView r1 = r9.f3932a
            java.lang.String r2 = r10.n
            java.lang.String r2 = a(r2)
            r1.setText(r2)
            android.content.Context r1 = r8.f3979a
            r2 = 16842808(0x1010038, float:2.3693715E-38)
            int r1 = com.helpshift.util.x.a(r1, r2)
            int[] r2 = com.helpshift.support.f.a.al.f3934a
            int r0 = r0.ordinal()
            r0 = r2[r0]
            java.lang.String r2 = ""
            r3 = 1
            r4 = 0
            r5 = 1056964608(0x3f000000, float:0.5)
            if (r0 == r3) goto L_0x0081
            r6 = 2
            if (r0 == r6) goto L_0x0064
            r6 = 3
            if (r0 == r6) goto L_0x0050
            r6 = 4
            if (r0 == r6) goto L_0x0039
            r0 = r2
            r6 = r0
        L_0x0037:
            r2 = 1
            goto L_0x009b
        L_0x0039:
            java.lang.String r2 = r10.h()
            android.content.Context r0 = r8.f3979a
            int r5 = com.helpshift.R.string.hs__user_sent_message_voice_over
            java.lang.Object[] r6 = new java.lang.Object[r3]
            java.lang.String r7 = r10.i()
            r6[r4] = r7
            java.lang.String r0 = r0.getString(r5, r6)
            r5 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0099
        L_0x0050:
            android.content.Context r0 = r8.f3979a
            int r2 = com.helpshift.R.string.hs__sending_msg
            java.lang.String r2 = r0.getString(r2)
            android.content.Context r0 = r8.f3979a
            int r3 = com.helpshift.R.string.hs__user_sending_message_voice_over
            java.lang.String r0 = r0.getString(r3)
            r6 = r2
            r2 = 0
            r3 = 0
            goto L_0x009b
        L_0x0064:
            android.content.Context r0 = r8.f3979a
            int r1 = com.helpshift.R.string.hs__sending_fail_msg
            java.lang.String r2 = r0.getString(r1)
            android.content.Context r0 = r8.f3979a
            int r1 = com.helpshift.R.string.hs__user_failed_message_voice_over
            java.lang.String r0 = r0.getString(r1)
            android.content.Context r1 = r8.f3979a
            int r6 = com.helpshift.R.attr.hs__errorTextColor
            int r1 = com.helpshift.util.x.a(r1, r6)
            r6 = r2
            r2 = 1
            r3 = 0
            r4 = 1
            goto L_0x009b
        L_0x0081:
            android.content.Context r0 = r8.f3979a
            int r1 = com.helpshift.R.string.hs__sending_fail_msg
            java.lang.String r2 = r0.getString(r1)
            android.content.Context r0 = r8.f3979a
            int r1 = com.helpshift.R.string.hs__user_failed_message_voice_over
            java.lang.String r0 = r0.getString(r1)
            android.content.Context r1 = r8.f3979a
            int r6 = com.helpshift.R.attr.hs__errorTextColor
            int r1 = com.helpshift.util.x.a(r1, r6)
        L_0x0099:
            r6 = r2
            goto L_0x0037
        L_0x009b:
            android.view.View r7 = r9.e
            r7.setContentDescription(r0)
            android.widget.TextView r0 = r9.f3933b
            r0.setTextColor(r1)
            android.widget.FrameLayout r0 = r9.d
            r0.setAlpha(r5)
            r0 = 0
            if (r3 == 0) goto L_0x00b2
            android.widget.TextView r1 = r9.f3932a
            a(r1, r0)
        L_0x00b2:
            android.widget.TextView r1 = r9.f3932a
            r1.setEnabled(r2)
            android.widget.ImageView r1 = r9.c
            a(r1, r4)
            com.helpshift.j.a.a.ai r10 = r10.l()
            android.widget.FrameLayout r1 = r9.d
            r8.b(r1, r10)
            android.widget.TextView r1 = r9.f3933b
            r8.b(r1, r10, r6)
            if (r4 == 0) goto L_0x00d2
            android.widget.ImageView r10 = r9.c
            r10.setOnClickListener(r9)
            goto L_0x00d7
        L_0x00d2:
            android.widget.ImageView r9 = r9.c
            r9.setOnClickListener(r0)
        L_0x00d7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.f.a.ak.a(android.support.v7.widget.RecyclerView$ViewHolder, com.helpshift.j.a.a.v):void");
    }

    public ak(Context context) {
        super(context);
    }

    /* compiled from: UserMessageViewDataBinder */
    protected final class a extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        /* renamed from: a  reason: collision with root package name */
        final TextView f3932a;

        /* renamed from: b  reason: collision with root package name */
        final TextView f3933b;
        final ImageView c;
        final FrameLayout d;
        final View e;

        a(View view) {
            super(view);
            this.f3932a = (TextView) view.findViewById(R.id.user_message_text);
            this.f3933b = (TextView) view.findViewById(R.id.user_date_text);
            this.d = (FrameLayout) view.findViewById(R.id.user_message_container);
            this.c = (ImageView) view.findViewById(R.id.user_message_retry_button);
            this.e = view.findViewById(R.id.user_text_message_layout);
        }

        public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            if (ak.this.f3980b != null) {
                ak.this.f3980b.a(contextMenu, ((TextView) view).getText().toString());
            }
        }

        public final void onClick(View view) {
            if (ak.this.f3980b != null) {
                ak.this.f3980b.a(getAdapterPosition());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        a aVar = new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_txt_user, viewGroup, false));
        a(aVar.d.getLayoutParams());
        aVar.f3932a.setOnCreateContextMenuListener(aVar);
        return aVar;
    }
}
