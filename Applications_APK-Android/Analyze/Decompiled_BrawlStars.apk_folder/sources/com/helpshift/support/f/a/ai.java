package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.ag;
import com.helpshift.j.a.a.v;
import com.helpshift.util.x;

/* compiled from: SystemPublishIdMessageDataBinder */
public class ai extends u<a, ag> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.util.x.a(android.content.Context, float):float
     arg types: [android.content.Context, int]
     candidates:
      com.helpshift.util.x.a(android.content.Context, int):int
      com.helpshift.util.x.a(android.content.Context, android.graphics.drawable.Drawable):void
      com.helpshift.util.x.a(android.content.Context, float):float */
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        a aVar = (a) viewHolder;
        ag agVar = (ag) vVar;
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) aVar.itemView.getLayoutParams();
        if (agVar.f3452a) {
            layoutParams.topMargin = (int) x.a(this.f3979a, 18.0f);
        } else {
            layoutParams.topMargin = 0;
        }
        aVar.itemView.setLayoutParams(layoutParams);
        aVar.f3928a.setText(this.f3979a.getString(R.string.hs__conversation_issue_id_header, agVar.n));
        aVar.f3928a.setContentDescription(this.f3979a.getString(R.string.hs__conversation_publish_id_voice_over, agVar.n));
    }

    public ai(Context context) {
        super(context);
    }

    /* compiled from: SystemPublishIdMessageDataBinder */
    public class a extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        /* access modifiers changed from: package-private */

        /* renamed from: a  reason: collision with root package name */
        public final TextView f3928a;

        public a(View view) {
            super(view);
            this.f3928a = (TextView) view.findViewById(R.id.issue_publish_id_label);
        }

        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            if (ai.this.f3980b != null) {
                String[] split = ((TextView) view).getText().toString().split("#");
                if (split.length > 1) {
                    ai.this.f3980b.a(contextMenu, split[1]);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        a aVar = new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_publish_id_layout, viewGroup, false));
        aVar.f3928a.setOnCreateContextMenuListener(aVar);
        return aVar;
    }
}
