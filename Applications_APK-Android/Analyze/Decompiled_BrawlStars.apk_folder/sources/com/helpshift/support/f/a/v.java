package com.helpshift.support.f.a;

import android.support.v7.widget.helper.ItemTouchHelper;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.view.AvatarView;

/* compiled from: MessageViewType */
public enum v {
    USER_TEXT_MESSAGE(10),
    ADMIN_TEXT_MESSAGE(20),
    USER_SCREENSHOT_ATTACHMENT(30),
    ADMIN_ATTACHMENT_IMAGE(40),
    ADMIN_ATTACHMENT_GENERIC(50),
    ADMIN_REQUEST_ATTACHMENT(60),
    REQUESTED_APP_REVIEW(70),
    REQUEST_FOR_REOPEN(80),
    CONFIRMATION_REJECTED(90),
    CONVERSATION_FOOTER(100),
    AGENT_TYPING_FOOTER(110),
    SYSTEM_DATE(120),
    SYSTEM_DIVIDER(130),
    USER_SELECTABLE_OPTION(140),
    ADMIN_SUGGESTIONS_LIST(150),
    SYSTEM_PUBLISH_ID(AvatarView.INTRINSIC_POINT_SIZE),
    SYSTEM_CONVERSATION_REDACTED_MESSAGE(170),
    HISTORY_LOADING_VIEW(180),
    ADMIN_REDACTED_MESSAGE(FacebookRequestErrorClassification.EC_INVALID_TOKEN),
    USER_REDACTED_MESSAGE(ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
    
    public final int u;

    private v(int i) {
        this.u = i;
    }
}
