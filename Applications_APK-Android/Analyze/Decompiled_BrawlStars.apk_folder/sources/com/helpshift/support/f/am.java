package com.helpshift.support.f;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/* compiled from: HSRecyclerViewScrollListener */
public class am extends RecyclerView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private final Handler f4000a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final a f4001b;
    private boolean c = false;

    /* compiled from: HSRecyclerViewScrollListener */
    interface a {
        void o();

        void p();

        void q();
    }

    public am(Handler handler, a aVar) {
        this.f4000a = handler;
        this.f4001b = aVar;
    }

    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        this.c = false;
        if (i == 0) {
            a(recyclerView);
        }
    }

    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        if (!this.c || recyclerView.getScrollState() == 0) {
            this.c = true;
            a(recyclerView);
        }
    }

    private void a(RecyclerView recyclerView) {
        View childAt;
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        boolean z = true;
        if (layoutManager != null) {
            int itemCount = layoutManager.getItemCount();
            int childCount = layoutManager.getChildCount();
            if (childCount > 0 && (childAt = layoutManager.getChildAt(childCount - 1)) != null) {
                int position = layoutManager.getPosition(childAt);
                int i = position + 1;
                if (!(position == -1 || itemCount == i)) {
                    z = false;
                }
            }
        }
        if (!recyclerView.canScrollVertically(-1)) {
            this.f4000a.post(new an(this));
        }
        if (z) {
            this.f4000a.post(new ao(this));
        }
        if (!z) {
            this.f4000a.post(new ap(this));
        }
    }
}
