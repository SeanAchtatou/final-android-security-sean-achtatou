package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import com.helpshift.R;
import com.helpshift.support.m.l;

/* compiled from: HistoryLoadingViewBinder */
public class s {

    /* renamed from: a  reason: collision with root package name */
    Context f3974a;

    /* renamed from: b  reason: collision with root package name */
    public a f3975b;

    /* compiled from: HistoryLoadingViewBinder */
    public interface a {
        void d();
    }

    public s(Context context) {
        this.f3974a = context;
    }

    /* compiled from: HistoryLoadingViewBinder */
    public class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final View f3977b = this.itemView.findViewById(R.id.history_loading_layout_view);
        /* access modifiers changed from: private */
        public final View c = this.itemView.findViewById(R.id.loading_state_view);
        /* access modifiers changed from: private */
        public final View d = this.itemView.findViewById(R.id.loading_error_state_view);
        private final View e = this.itemView.findViewById(R.id.loading_error_tap_to_retry);
        private final ProgressBar f;

        public b(View view) {
            super(view);
            this.e.setOnClickListener(this);
            this.f = (ProgressBar) this.itemView.findViewById(R.id.loading_progressbar);
            l.b(s.this.f3974a, this.f.getIndeterminateDrawable());
        }

        public void onClick(View view) {
            if (s.this.f3975b != null) {
                s.this.f3975b.d();
            }
        }
    }
}
