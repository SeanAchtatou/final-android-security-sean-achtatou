package com.helpshift.support.f.a;

import com.helpshift.j.a.a.o;

/* compiled from: ConversationFooterViewBinder */
public /* synthetic */ class r {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ int[] f3973a = new int[o.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
    static {
        /*
            com.helpshift.j.a.a.o[] r0 = com.helpshift.j.a.a.o.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.helpshift.support.f.a.r.f3973a = r0
            int[] r0 = com.helpshift.support.f.a.r.f3973a     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.helpshift.j.a.a.o r1 = com.helpshift.j.a.a.o.NONE     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r2 = 1
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            int[] r0 = com.helpshift.support.f.a.r.f3973a     // Catch:{ NoSuchFieldError -> 0x001f }
            com.helpshift.j.a.a.o r1 = com.helpshift.j.a.a.o.CONVERSATION_ENDED_MESSAGE     // Catch:{ NoSuchFieldError -> 0x001f }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2 = 2
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            int[] r0 = com.helpshift.support.f.a.r.f3973a     // Catch:{ NoSuchFieldError -> 0x002a }
            com.helpshift.j.a.a.o r1 = com.helpshift.j.a.a.o.START_NEW_CONVERSATION     // Catch:{ NoSuchFieldError -> 0x002a }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r2 = 3
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            int[] r0 = com.helpshift.support.f.a.r.f3973a     // Catch:{ NoSuchFieldError -> 0x0035 }
            com.helpshift.j.a.a.o r1 = com.helpshift.j.a.a.o.CSAT_RATING     // Catch:{ NoSuchFieldError -> 0x0035 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
            r2 = 4
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
        L_0x0035:
            int[] r0 = com.helpshift.support.f.a.r.f3973a     // Catch:{ NoSuchFieldError -> 0x0040 }
            com.helpshift.j.a.a.o r1 = com.helpshift.j.a.a.o.ARCHIVAL_MESSAGE     // Catch:{ NoSuchFieldError -> 0x0040 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
            r2 = 5
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
        L_0x0040:
            int[] r0 = com.helpshift.support.f.a.r.f3973a     // Catch:{ NoSuchFieldError -> 0x004b }
            com.helpshift.j.a.a.o r1 = com.helpshift.j.a.a.o.AUTHOR_MISMATCH     // Catch:{ NoSuchFieldError -> 0x004b }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
            r2 = 6
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
        L_0x004b:
            int[] r0 = com.helpshift.support.f.a.r.f3973a     // Catch:{ NoSuchFieldError -> 0x0056 }
            com.helpshift.j.a.a.o r1 = com.helpshift.j.a.a.o.REJECTED_MESSAGE     // Catch:{ NoSuchFieldError -> 0x0056 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
            r2 = 7
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
        L_0x0056:
            int[] r0 = com.helpshift.support.f.a.r.f3973a     // Catch:{ NoSuchFieldError -> 0x0062 }
            com.helpshift.j.a.a.o r1 = com.helpshift.j.a.a.o.REDACTED_STATE     // Catch:{ NoSuchFieldError -> 0x0062 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
            r2 = 8
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
        L_0x0062:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.f.a.r.<clinit>():void");
    }
}
