package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Html;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.aa;
import com.helpshift.j.a.a.ab;
import com.helpshift.j.a.a.ai;
import com.helpshift.j.a.a.b;
import com.helpshift.j.a.a.e;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.x;
import com.helpshift.j.a.a.y;
import com.helpshift.util.k;
import com.helpshift.util.o;

/* compiled from: MessageViewDataBinder */
public abstract class u<VH extends RecyclerView.ViewHolder, M extends v> {

    /* renamed from: a  reason: collision with root package name */
    protected Context f3979a;

    /* renamed from: b  reason: collision with root package name */
    protected a f3980b;

    /* compiled from: MessageViewDataBinder */
    public interface a {
        void a(int i);

        void a(ContextMenu contextMenu, String str);

        void a(aa aaVar);

        void a(ab abVar);

        void a(b bVar);

        void a(e eVar);

        void a(v vVar, String str, String str2);

        void a(x xVar, b.a aVar, boolean z);

        void a(y yVar);

        void a(String str, v vVar);
    }

    public abstract VH a(ViewGroup viewGroup);

    public abstract void a(RecyclerView.ViewHolder viewHolder, v vVar);

    public u(Context context) {
        this.f3979a = context;
    }

    public final void a(a aVar) {
        this.f3980b = aVar;
    }

    protected static void a(TextView textView, k.a aVar) {
        k.a(textView, 14, aVar);
        k.a(textView, o.b(), null, null, null, aVar);
    }

    static String a(String str) {
        return Html.fromHtml(str.replace("\n", "<br/>")).toString();
    }

    static String b(String str) {
        return str + " ";
    }

    /* access modifiers changed from: protected */
    public final void a(View view, ai aiVar) {
        a(view, aiVar.b() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_admin, R.attr.hs__chatBubbleAdminBackgroundColor);
    }

    /* access modifiers changed from: protected */
    public final void a(TextView textView, ai aiVar, String str) {
        textView.setText(str);
        a(textView, aiVar.a());
    }

    /* access modifiers changed from: protected */
    public final void b(TextView textView, ai aiVar, String str) {
        textView.setText(str);
        a(textView, aiVar.a());
    }

    /* access modifiers changed from: protected */
    public final void b(View view, ai aiVar) {
        a(view, aiVar.b() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_user, R.attr.hs__chatBubbleUserBackgroundColor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* access modifiers changed from: protected */
    public final void a(ViewGroup.LayoutParams layoutParams) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        TypedValue typedValue = new TypedValue();
        this.f3979a.getResources().getValue(R.dimen.hs__screen_to_conversation_view_ratio, typedValue, true);
        marginLayoutParams.setMargins((int) (((float) this.f3979a.getResources().getDisplayMetrics().widthPixels) * typedValue.getFloat() * 0.2f), marginLayoutParams.topMargin, marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
    }

    protected static void a(View view, boolean z) {
        if (z) {
            view.setVisibility(0);
        } else {
            view.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(View view, int i, int i2) {
        com.helpshift.util.x.a(this.f3979a, view, i, i2);
    }

    /* access modifiers changed from: protected */
    public final String a(v vVar) {
        String j = vVar.j();
        String i = vVar.i();
        if (com.helpshift.common.k.a(j)) {
            return this.f3979a.getString(R.string.hs__agent_message_voice_over, i);
        }
        return this.f3979a.getString(R.string.hs__agent_message_with_name_voice_over, j, i);
    }

    static void a(TextView textView) {
        textView.setTypeface(textView.getTypeface(), 2);
        textView.setAlpha(0.55f);
    }
}
