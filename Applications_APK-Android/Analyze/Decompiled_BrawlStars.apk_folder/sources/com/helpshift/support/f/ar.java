package com.helpshift.support.f;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.b.b;
import com.helpshift.common.c.j;
import com.helpshift.j.d.d;
import com.helpshift.j.i.aw;
import com.helpshift.support.Faq;
import com.helpshift.support.i.c;
import com.helpshift.support.i.i;
import com.helpshift.support.i.p;
import com.helpshift.support.i.x;
import com.helpshift.support.m.a;
import com.helpshift.support.m.e;
import com.helpshift.support.m.h;
import com.helpshift.util.q;
import java.util.ArrayList;

/* compiled from: NewConversationFragment */
public class ar extends b implements bi {

    /* renamed from: a  reason: collision with root package name */
    public aw f4007a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public bh f4008b;
    private TextInputEditText c;
    private d d;
    private boolean e;

    public static ar a(Bundle bundle) {
        ar arVar = new ar();
        arVar.setArguments(bundle);
        return arVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__new_conversation_fragment, viewGroup, false);
    }

    public void onStart() {
        super.onStart();
        if (!this.j) {
            q.c().u().r();
        }
    }

    public void onResume() {
        super.onResume();
        j a2 = q.c().a();
        this.f4007a.g().a(a2, new as(this));
        this.f4007a.h().a(a2, new az(this));
        this.f4007a.i().a(a2, new ba(this));
        this.f4007a.j().a(a2, new bb(this));
        this.f4007a.k().a(a2, new bc(this));
        this.f4007a.l().a(a2, new bd(this));
        this.f4007a.m().a(a2, new be(this));
        this.f4007a.n().a(a2, new bf(this));
        if (!this.j) {
            q.c().k().a(b.REPORTED_ISSUE);
        }
        this.c.requestFocus();
        h.b(getContext(), this.c);
        this.f4007a.a(1);
    }

    /* access modifiers changed from: protected */
    public final a.C0146a a() {
        return a.C0146a.NEW_CONVERSATION;
    }

    /* access modifiers changed from: protected */
    public final String d() {
        return getString(R.string.hs__new_conversation_header);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.i.x.a(boolean, android.os.Bundle):void
     arg types: [int, android.os.Bundle]
     candidates:
      com.helpshift.support.i.x.a(int, java.lang.Long):void
      com.helpshift.support.i.x.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.i.x.a(com.helpshift.support.i.c, boolean):void
      com.helpshift.support.i.e.a(com.helpshift.support.i.c, boolean):void
      com.helpshift.support.widget.b.a.a(int, java.lang.Long):void
      com.helpshift.support.widget.b.a.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.i.x.a(boolean, android.os.Bundle):void */
    /* access modifiers changed from: protected */
    public final void a(int i) {
        if (i == 2) {
            Bundle bundle = new Bundle();
            bundle.putInt("key_screenshot_mode", 1);
            ((x) getParentFragment()).a(false, bundle);
        }
    }

    public void onDestroyView() {
        this.f4007a.a(this.f4008b);
        this.f4007a.a(-1);
        super.onDestroyView();
    }

    public final boolean a(i.b bVar, d dVar) {
        int i = ay.f4015a[bVar.ordinal()];
        if (i == 1) {
            aw awVar = this.f4007a;
            if (awVar == null) {
                this.d = dVar;
                this.e = true;
            } else {
                awVar.a(dVar);
            }
            return true;
        } else if (i != 2) {
            return false;
        } else {
            aw awVar2 = this.f4007a;
            if (awVar2 == null) {
                this.d = null;
                this.e = true;
            } else {
                awVar2.a((d) null);
            }
            return true;
        }
    }

    public final void a(ArrayList<Faq> arrayList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("search_fragment_results", arrayList);
        com.helpshift.support.e.b e2 = e();
        e.a(e2.c, R.id.flow_fragment_container, p.a(bundle, e2), "HSSearchResultFragment", false);
    }

    public final void f() {
        if (isResumed()) {
            e().d();
        }
    }

    public final void a(d dVar) {
        Bundle bundle = new Bundle();
        bundle.putInt("key_screenshot_mode", 2);
        e().a(dVar, bundle, i.a.ATTACHMENT_DRAFT);
    }

    public final void g() {
        e().e();
    }

    public final void k() {
        this.f4008b.b(this.f4007a.i().a());
        this.f4008b.a(this.f4007a.j().a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.i.x.a(boolean, android.os.Bundle):void
     arg types: [int, android.os.Bundle]
     candidates:
      com.helpshift.support.i.x.a(int, java.lang.Long):void
      com.helpshift.support.i.x.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.i.x.a(com.helpshift.support.i.c, boolean):void
      com.helpshift.support.i.e.a(com.helpshift.support.i.c, boolean):void
      com.helpshift.support.widget.b.a.a(int, java.lang.Long):void
      com.helpshift.support.widget.b.a.a(com.helpshift.j.d.d, android.os.Bundle):void
      com.helpshift.support.i.x.a(boolean, android.os.Bundle):void */
    public final void a(c cVar) {
        int i = ay.f4016b[cVar.ordinal()];
        if (i == 1) {
            this.f4007a.b();
        } else if (i == 2) {
            Bundle bundle = new Bundle();
            bundle.putInt("key_screenshot_mode", 1);
            bundle.putString("key_refers_id", null);
            ((x) getParentFragment()).a(true, bundle);
        }
    }

    public void onViewCreated(View view, Bundle bundle) {
        boolean z;
        View view2 = view;
        TextInputLayout textInputLayout = (TextInputLayout) view2.findViewById(R.id.hs__conversationDetailWrapper);
        TextInputLayout textInputLayout2 = textInputLayout;
        textInputLayout.setHintEnabled(false);
        textInputLayout.setHintAnimationEnabled(false);
        this.c = (TextInputEditText) view2.findViewById(R.id.hs__conversationDetail);
        TextInputLayout textInputLayout3 = (TextInputLayout) view2.findViewById(R.id.hs__usernameWrapper);
        TextInputLayout textInputLayout4 = textInputLayout3;
        textInputLayout3.setHintEnabled(false);
        textInputLayout3.setHintAnimationEnabled(false);
        TextInputEditText textInputEditText = (TextInputEditText) view2.findViewById(R.id.hs__username);
        TextInputLayout textInputLayout5 = (TextInputLayout) view2.findViewById(R.id.hs__emailWrapper);
        textInputLayout5.setHintEnabled(false);
        textInputLayout5.setHintAnimationEnabled(false);
        TextInputEditText textInputEditText2 = (TextInputEditText) view2.findViewById(R.id.hs__email);
        TextInputEditText textInputEditText3 = textInputEditText2;
        ImageButton imageButton = (ImageButton) view2.findViewById(16908314);
        TextInputEditText textInputEditText4 = textInputEditText;
        Context context = getContext();
        TextInputEditText textInputEditText5 = this.c;
        TextInputLayout textInputLayout6 = textInputLayout2;
        this.f4008b = new bh(context, textInputLayout6, textInputEditText5, textInputLayout4, textInputEditText, textInputLayout5, textInputEditText2, (ProgressBar) view2.findViewById(R.id.progress_bar), (ImageView) view2.findViewById(R.id.hs__screenshot), (TextView) view2.findViewById(R.id.attachment_file_name), (TextView) view2.findViewById(R.id.attachment_file_size), (CardView) view2.findViewById(R.id.screenshot_view_container), imageButton, getView(), this, (x) getParentFragment());
        this.f4007a = q.c().a(this.f4008b);
        if (this.e) {
            this.f4007a.a(this.d);
            z = false;
            this.e = false;
        } else {
            z = false;
        }
        this.c.addTextChangedListener(new bg(this));
        textInputEditText4.addTextChangedListener(new at(this));
        textInputEditText3.addTextChangedListener(new au(this));
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f4007a.d(arguments.getString("source_search_query"));
            this.f4007a.b(arguments.getBoolean("dropMeta"));
            this.f4007a.a(getArguments().getBoolean("search_performed", z));
        }
        super.onViewCreated(view, bundle);
        View view3 = view;
        this.c = (TextInputEditText) view3.findViewById(R.id.hs__conversationDetail);
        this.c.setOnTouchListener(new av(this));
        ImageButton imageButton2 = (ImageButton) view3.findViewById(16908314);
        imageButton2.setVisibility(8);
        imageButton2.setOnClickListener(new aw(this));
        ((ImageView) view3.findViewById(R.id.hs__screenshot)).setOnClickListener(new ax(this));
    }

    public void onPause() {
        this.f4007a.g().d();
        this.f4007a.h().d();
        this.f4007a.i().d();
        this.f4007a.j().d();
        this.f4007a.k().d();
        this.f4007a.l().d();
        this.f4007a.m().d();
        this.f4007a.n().d();
        super.onPause();
        h.a(getContext(), this.c);
    }

    public final void c() {
        ((x) getParentFragment()).a();
    }
}
