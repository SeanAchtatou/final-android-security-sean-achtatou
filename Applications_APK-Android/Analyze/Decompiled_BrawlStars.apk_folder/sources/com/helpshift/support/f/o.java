package com.helpshift.support.f;

import com.helpshift.R;
import com.helpshift.z.e;
import com.helpshift.z.r;

/* compiled from: ConversationalFragment */
class o implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f4054a;

    o(d dVar) {
        this.f4054a = dVar;
    }

    public final void a(Object obj) {
        String str;
        r rVar = (r) obj;
        s sVar = this.f4054a.f4039a;
        boolean a2 = rVar.a();
        boolean e = rVar.e();
        if (a2) {
            sVar.k.setVisibility(0);
            if (e) {
                sVar.l.setVisibility(0);
                str = sVar.g.getString(R.string.hs__jump_button_with_new_message_voice_over);
            } else {
                sVar.l.setVisibility(8);
                str = sVar.g.getString(R.string.hs__jump_button_voice_over);
            }
            sVar.m.setContentDescription(str);
            return;
        }
        sVar.k.setVisibility(8);
        sVar.l.setVisibility(8);
    }
}
