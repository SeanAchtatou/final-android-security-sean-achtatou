package com.helpshift.support.f.b;

import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.common.j;
import com.helpshift.j.i.af;
import com.helpshift.j.i.bn;
import com.helpshift.support.f.al;
import com.helpshift.util.x;
import java.util.List;

/* compiled from: PickerAdapter */
public class a extends RecyclerView.Adapter<C0144a> {

    /* renamed from: a  reason: collision with root package name */
    public List<bn> f4020a;

    /* renamed from: b  reason: collision with root package name */
    al f4021b;

    public /* synthetic */ void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        C0144a aVar = (C0144a) viewHolder;
        bn bnVar = this.f4020a.get(i);
        String str = bnVar.f3687a.f3441a;
        if (j.a(bnVar.f3688b)) {
            aVar.c.setText(str);
        } else {
            int a2 = x.a(aVar.c.getContext(), R.attr.hs__searchHighlightColor);
            SpannableString spannableString = new SpannableString(str);
            for (af next : bnVar.f3688b) {
                spannableString.setSpan(new BackgroundColorSpan(a2), next.f3625a, next.f3625a + next.f3626b, 33);
            }
            aVar.c.setText(spannableString);
        }
        aVar.f4023b.setContentDescription(aVar.c.getContext().getString(R.string.hs__picker_option_list_item_voice_over, str));
    }

    public a(List<bn> list, al alVar) {
        this.f4020a = list;
        this.f4021b = alVar;
    }

    public int getItemCount() {
        return this.f4020a.size();
    }

    /* renamed from: com.helpshift.support.f.b.a$a  reason: collision with other inner class name */
    /* compiled from: PickerAdapter */
    class C0144a extends RecyclerView.ViewHolder implements View.OnClickListener {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final View f4023b = this.itemView.findViewById(R.id.option_list_item_layout);
        /* access modifiers changed from: private */
        public final TextView c = ((TextView) this.itemView.findViewById(R.id.hs__option));

        public C0144a(View view) {
            super(view);
            this.f4023b.setOnClickListener(this);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.helpshift.support.f.al.a(com.helpshift.j.i.bn, boolean):void
         arg types: [com.helpshift.j.i.bn, int]
         candidates:
          com.helpshift.support.f.a.x.a(int, java.lang.String):void
          com.helpshift.support.f.a.x.a(android.view.ContextMenu, java.lang.String):void
          com.helpshift.support.f.a.x.a(java.lang.String, com.helpshift.j.a.a.v):void
          com.helpshift.support.f.al.a(com.helpshift.j.i.bn, boolean):void */
        public void onClick(View view) {
            if (a.this.f4021b != null) {
                a.this.f4021b.a((bn) a.this.f4020a.get(getAdapterPosition()), false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public /* synthetic */ RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new C0144a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__picker_option, viewGroup, false));
    }
}
