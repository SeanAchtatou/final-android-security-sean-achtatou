package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.e;
import com.helpshift.support.m.l;
import com.helpshift.support.views.HSRoundedImageView;
import com.helpshift.util.x;

/* compiled from: AdminImageAttachmentMessageDataBinder */
class d extends u<a, e> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.f.a.u.a(android.view.View, boolean):void
     arg types: [android.widget.TextView, int]
     candidates:
      com.helpshift.support.f.a.d.a(android.support.v7.widget.RecyclerView$ViewHolder, com.helpshift.j.a.a.v):void
      com.helpshift.support.f.a.u.a(android.widget.TextView, com.helpshift.util.k$a):void
      com.helpshift.support.f.a.u.a(android.support.v7.widget.RecyclerView$ViewHolder, com.helpshift.j.a.a.v):void
      com.helpshift.support.f.a.u.a(android.view.View, com.helpshift.j.a.a.ai):void
      com.helpshift.support.f.a.u.a(android.view.View, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x010d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void a(android.support.v7.widget.RecyclerView.ViewHolder r12, com.helpshift.j.a.a.v r13) {
        /*
            r11 = this;
            com.helpshift.support.f.a.d$a r12 = (com.helpshift.support.f.a.d.a) r12
            com.helpshift.j.a.a.e r13 = (com.helpshift.j.a.a.e) r13
            java.lang.String r0 = r13.g()
            int[] r1 = com.helpshift.support.f.a.f.f3950a
            com.helpshift.j.a.a.e$a r2 = r13.f3473a
            int r2 = r2.ordinal()
            r1 = r1[r2]
            r2 = 0
            r3 = 0
            r4 = 1
            if (r1 == r4) goto L_0x0092
            r5 = 2
            if (r1 == r5) goto L_0x0081
            r6 = 3
            if (r1 == r6) goto L_0x0069
            r6 = 4
            if (r1 == r6) goto L_0x0042
            r5 = 5
            if (r1 == r5) goto L_0x002f
            java.lang.String r1 = ""
            r7 = r0
            r9 = r1
            r6 = r2
            r0 = 1
            r1 = 0
        L_0x002a:
            r3 = 1
            r5 = 1
        L_0x002c:
            r8 = 1
            goto L_0x00a8
        L_0x002f:
            java.lang.String r1 = r13.f()
            android.content.Context r5 = r11.f3979a
            int r6 = com.helpshift.R.string.hs__image_downloaded_voice_over
            java.lang.String r5 = r5.getString(r6)
            r7 = r0
            r6 = r1
            r9 = r5
            r0 = 0
            r1 = 0
            r5 = 0
            goto L_0x002c
        L_0x0042:
            java.lang.String r0 = r13.e()
            java.lang.String r1 = r13.c()
            android.content.Context r6 = r11.f3979a
            int r7 = com.helpshift.R.string.hs__image_downloading_voice_over
            java.lang.Object[] r5 = new java.lang.Object[r5]
            java.lang.String r8 = r13.d()
            r5[r3] = r8
            java.lang.String r8 = r13.g()
            r5[r4] = r8
            java.lang.String r5 = r6.getString(r7, r5)
            r6 = r0
            r7 = r1
            r9 = r5
            r0 = 1
            r1 = 0
            r3 = 1
            r5 = 1
            r8 = 0
            goto L_0x00a8
        L_0x0069:
            java.lang.String r1 = r13.e()
            android.content.Context r5 = r11.f3979a
            int r6 = com.helpshift.R.string.hs__image_not_downloaded_voice_over
            java.lang.Object[] r7 = new java.lang.Object[r4]
            java.lang.String r8 = r13.g()
            r7[r3] = r8
            java.lang.String r5 = r5.getString(r6, r7)
            r7 = r0
            r6 = r1
            r9 = r5
            goto L_0x00a5
        L_0x0081:
            android.content.Context r1 = r11.f3979a
            int r5 = com.helpshift.R.string.hs__image_not_downloaded_voice_over
            java.lang.Object[] r6 = new java.lang.Object[r4]
            java.lang.String r7 = r13.g()
            r6[r3] = r7
            java.lang.String r1 = r1.getString(r5, r6)
            goto L_0x00a2
        L_0x0092:
            android.content.Context r1 = r11.f3979a
            int r5 = com.helpshift.R.string.hs__image_not_downloaded_voice_over
            java.lang.Object[] r6 = new java.lang.Object[r4]
            java.lang.String r7 = r13.g()
            r6[r3] = r7
            java.lang.String r1 = r1.getString(r5, r6)
        L_0x00a2:
            r7 = r0
            r9 = r1
            r6 = r2
        L_0x00a5:
            r0 = 0
            r1 = 1
            goto L_0x002a
        L_0x00a8:
            android.view.View r10 = r12.e
            a(r10, r3)
            android.widget.ProgressBar r3 = r12.c
            a(r3, r0)
            android.view.View r0 = r12.d
            a(r0, r1)
            if (r5 == 0) goto L_0x00c1
            com.helpshift.support.views.HSRoundedImageView r0 = r12.f3947b
            r3 = 1048576000(0x3e800000, float:0.25)
            r0.setAlpha(r3)
            goto L_0x00c8
        L_0x00c1:
            com.helpshift.support.views.HSRoundedImageView r0 = r12.f3947b
            r3 = 1065353216(0x3f800000, float:1.0)
            r0.setAlpha(r3)
        L_0x00c8:
            android.widget.TextView r0 = r12.f
            a(r0, r4)
            com.helpshift.support.views.HSRoundedImageView r0 = r12.f3947b
            r0.a(r6)
            com.helpshift.j.a.a.ai r0 = r13.l()
            boolean r3 = r0.a()
            if (r3 == 0) goto L_0x00e5
            android.widget.TextView r3 = r12.g
            java.lang.String r4 = r13.h()
            r3.setText(r4)
        L_0x00e5:
            android.widget.TextView r3 = r12.g
            boolean r0 = r0.a()
            a(r3, r0)
            android.widget.TextView r0 = r12.f
            r0.setText(r7)
            com.helpshift.support.f.a.e r0 = new com.helpshift.support.f.a.e
            r0.<init>(r11, r13)
            if (r1 == 0) goto L_0x0100
            android.view.View r1 = r12.d
            r1.setOnClickListener(r0)
            goto L_0x0105
        L_0x0100:
            android.view.View r1 = r12.d
            r1.setOnClickListener(r2)
        L_0x0105:
            if (r8 == 0) goto L_0x010d
            com.helpshift.support.views.HSRoundedImageView r1 = r12.f3947b
            r1.setOnClickListener(r0)
            goto L_0x0112
        L_0x010d:
            com.helpshift.support.views.HSRoundedImageView r0 = r12.f3947b
            r0.setOnClickListener(r2)
        L_0x0112:
            com.helpshift.support.views.HSRoundedImageView r0 = r12.f3947b
            r0.setContentDescription(r9)
            android.view.View r12 = r12.f3946a
            java.lang.String r13 = r11.a(r13)
            r12.setContentDescription(r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.f.a.d.a(android.support.v7.widget.RecyclerView$ViewHolder, com.helpshift.j.a.a.v):void");
    }

    d(Context context) {
        super(context);
    }

    /* compiled from: AdminImageAttachmentMessageDataBinder */
    protected final class a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        final View f3946a;

        /* renamed from: b  reason: collision with root package name */
        final HSRoundedImageView f3947b;
        final ProgressBar c;
        final View d;
        final View e;
        final TextView f;
        final TextView g;

        a(View view) {
            super(view);
            this.f3946a = view.findViewById(R.id.admin_image_message_layout);
            this.f3947b = (HSRoundedImageView) view.findViewById(R.id.admin_attachment_imageview);
            this.d = view.findViewById(R.id.download_button);
            this.e = view.findViewById(R.id.download_progressbar_container);
            this.c = (ProgressBar) view.findViewById(R.id.download_attachment_progressbar);
            this.f = (TextView) view.findViewById(R.id.attachment_file_size);
            this.g = (TextView) view.findViewById(R.id.date);
            x.a(d.this.f3979a, ((ImageView) view.findViewById(R.id.hs_download_foreground_view)).getDrawable(), R.attr.hs__chatBubbleMediaBackgroundColor);
            l.b(d.this.f3979a, this.c.getIndeterminateDrawable());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new a(LayoutInflater.from(this.f3979a).inflate(R.layout.hs__msg_attachment_image, viewGroup, false));
    }
}
