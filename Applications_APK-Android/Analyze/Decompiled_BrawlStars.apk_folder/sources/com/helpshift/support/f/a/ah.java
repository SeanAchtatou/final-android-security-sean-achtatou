package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.ae;
import com.helpshift.j.a.a.v;

/* compiled from: SystemDividerMessageDataBinder */
public class ah extends u<a, ae> {
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        String str;
        a aVar = (a) viewHolder;
        if (((ae) vVar).f3451a) {
            aVar.f3927b.setVisibility(0);
            str = "";
        } else {
            aVar.f3927b.setVisibility(8);
            str = this.f3979a.getString(R.string.hs__conversations_divider_voice_over);
        }
        aVar.c.setContentDescription(str);
    }

    public ah(Context context) {
        super(context);
    }

    /* compiled from: SystemDividerMessageDataBinder */
    public class a extends RecyclerView.ViewHolder {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public TextView f3927b;
        /* access modifiers changed from: private */
        public View c;

        public a(View view) {
            super(view);
            this.c = view.findViewById(R.id.conversations_divider);
            this.f3927b = (TextView) view.findViewById(R.id.conversation_closed_view);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_system_divider_layout, viewGroup, false));
    }
}
