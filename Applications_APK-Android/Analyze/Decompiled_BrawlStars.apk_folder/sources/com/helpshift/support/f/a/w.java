package com.helpshift.support.f.a;

import android.content.Context;
import android.util.SparseArray;

/* compiled from: MessageViewTypeConverter */
public class w {

    /* renamed from: a  reason: collision with root package name */
    public q f3983a;

    /* renamed from: b  reason: collision with root package name */
    public n f3984b;
    public s c;
    private SparseArray<u> d = new SparseArray<>();

    public w(Context context) {
        this.d.put(v.ADMIN_TEXT_MESSAGE.u, new g(context));
        this.d.put(v.USER_TEXT_MESSAGE.u, new ak(context));
        this.d.put(v.USER_SCREENSHOT_ATTACHMENT.u, new ad(context));
        this.d.put(v.ADMIN_ATTACHMENT_IMAGE.u, new d(context));
        this.d.put(v.ADMIN_ATTACHMENT_GENERIC.u, new a(context));
        this.d.put(v.REQUESTED_APP_REVIEW.u, new y(context));
        this.d.put(v.CONFIRMATION_REJECTED.u, new p(context));
        this.d.put(v.ADMIN_REQUEST_ATTACHMENT.u, new aa(context));
        this.d.put(v.REQUEST_FOR_REOPEN.u, new g(context));
        this.d.put(v.ADMIN_SUGGESTIONS_LIST.u, new k(context));
        this.d.put(v.USER_SELECTABLE_OPTION.u, new an(context));
        this.d.put(v.SYSTEM_DATE.u, new ag(context));
        this.d.put(v.SYSTEM_DIVIDER.u, new ah(context));
        this.d.put(v.SYSTEM_PUBLISH_ID.u, new ai(context));
        this.d.put(v.ADMIN_REDACTED_MESSAGE.u, new i(context));
        this.d.put(v.USER_REDACTED_MESSAGE.u, new am(context));
        this.d.put(v.SYSTEM_CONVERSATION_REDACTED_MESSAGE.u, new aj(context));
        this.f3983a = new q(context);
        this.f3984b = new n(context);
        this.c = new s(context);
    }

    public final u a(int i) {
        return this.d.get(i);
    }
}
