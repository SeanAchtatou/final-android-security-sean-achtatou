package com.helpshift.support.f.c;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.helpshift.R;
import com.helpshift.common.c.j;
import com.helpshift.network.a.e;
import com.helpshift.network.a.h;
import com.helpshift.support.e.b;
import com.helpshift.support.i.f;
import com.helpshift.support.i.x;
import com.helpshift.support.m.l;
import com.helpshift.util.q;
import java.util.HashMap;

/* compiled from: UserSetupFragment */
public class a extends f implements com.helpshift.j.a.c.a, h {

    /* renamed from: a  reason: collision with root package name */
    ProgressBar f4034a;

    /* renamed from: b  reason: collision with root package name */
    View f4035b;
    View c;
    private com.helpshift.j.g.a d;

    public final boolean h_() {
        return true;
    }

    public static a d() {
        return new a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__user_setup_fragment, viewGroup, false);
    }

    public void onResume() {
        super.onResume();
        j a2 = q.c().a();
        this.d.f().a(a2, new b(this));
        this.d.g().a(a2, new c(this));
        this.d.h().a(a2, new d(this));
        b(getString(R.string.hs__conversation_header));
        e.a().a(this);
        this.d.b();
    }

    public void onDestroyView() {
        this.d.c();
        super.onDestroyView();
    }

    public final void a() {
        e().b(new HashMap());
    }

    public final void b() {
        e().e();
    }

    private b e() {
        return ((x) getParentFragment()).d;
    }

    public final void i_() {
        this.d.d();
    }

    public final void c() {
        this.d.e();
    }

    public void onViewCreated(View view, Bundle bundle) {
        this.f4034a = (ProgressBar) view.findViewById(R.id.progressbar);
        l.b(getContext(), this.f4034a.getIndeterminateDrawable());
        this.f4035b = view.findViewById(R.id.progress_description_text_view);
        this.c = view.findViewById(R.id.offline_error_view);
        com.helpshift.util.x.a(getContext(), ((ImageView) view.findViewById(R.id.info_icon)).getDrawable(), 16842806);
        this.d = q.c().a(this);
        super.onViewCreated(view, bundle);
    }

    public void onPause() {
        this.d.f().d();
        this.d.g().d();
        this.d.h().d();
        e.a().b(this);
        super.onPause();
    }
}
