package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.common.k;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.x;
import com.helpshift.support.f.a.u;
import com.helpshift.support.m.l;
import java.util.ArrayList;

/* compiled from: UserSelectableOptionViewDataBinder */
public class an extends u<b, x> {
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        b bVar;
        b bVar2 = (b) viewHolder;
        x xVar = (x) vVar;
        bVar2.f3940b.removeAllViews();
        boolean z = false;
        if (!k.a(xVar.f3496a.c)) {
            bVar2.c.setVisibility(0);
            bVar2.c.setText(xVar.f3496a.c);
        } else {
            bVar2.c.setVisibility(8);
        }
        a aVar = new a(bVar2, this.f3980b, xVar, false);
        com.helpshift.support.views.a aVar2 = new com.helpshift.support.views.a(this.f3979a, l.a(this.f3979a) ? 0.6000000000000001d : 0.8d, (int) this.f3979a.getResources().getDimension(R.dimen.activity_horizontal_margin_medium), bVar2.f3940b, R.layout.hs__msg_user_selectable_option, R.id.selectable_option_text, R.drawable.hs__pill, R.attr.hs__selectableOptionColor, xVar.f3496a.e, aVar);
        ArrayList<LinearLayout> arrayList = new ArrayList<>();
        DisplayMetrics displayMetrics = aVar2.f4209a.getResources().getDisplayMetrics();
        double d = (double) displayMetrics.widthPixels;
        double d2 = aVar2.i;
        Double.isNaN(d);
        int i = ((int) (d * d2)) - ((int) (((float) aVar2.j) * displayMetrics.density));
        int size = aVar2.h.size();
        int i2 = 0;
        while (i2 < size) {
            LinearLayout linearLayout = new LinearLayout(aVar2.f4209a);
            linearLayout.setOrientation(z ? 1 : 0);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            linearLayout.setGravity(GravityCompat.END);
            while (true) {
                View inflate = LayoutInflater.from(aVar2.f4209a).inflate(aVar2.c, (ViewGroup) null, z);
                TextView textView = (TextView) inflate.findViewById(aVar2.d);
                int paddingLeft = textView.getPaddingLeft();
                int paddingTop = textView.getPaddingTop();
                int paddingRight = textView.getPaddingRight();
                int paddingBottom = textView.getPaddingBottom();
                bVar = bVar2;
                com.helpshift.util.x.a(aVar2.f4209a, textView, aVar2.e, aVar2.f);
                textView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
                textView.setMaxWidth(i);
                b.a aVar3 = aVar2.h.get(i2);
                textView.setTag(aVar3);
                textView.setText(aVar3.f3441a);
                textView.setOnClickListener(aVar2.g);
                linearLayout.addView(inflate);
                linearLayout.measure(0, 0);
                if (linearLayout.getMeasuredWidth() > i) {
                    if (linearLayout.getChildCount() == 1) {
                        i2++;
                    } else {
                        linearLayout.removeView(inflate);
                    }
                    arrayList.add(linearLayout);
                } else {
                    if (i2 == size - 1) {
                        arrayList.add(linearLayout);
                    }
                    i2++;
                    if (i2 >= size) {
                        break;
                    }
                    z = false;
                    bVar2 = bVar;
                }
            }
            z = false;
            bVar2 = bVar;
        }
        b bVar3 = bVar2;
        for (LinearLayout addView : arrayList) {
            aVar2.f4210b.addView(addView);
        }
        if (xVar.f3496a.f3440b || k.a(xVar.f3496a.d)) {
            bVar3.d.setVisibility(8);
            return;
        }
        b bVar4 = bVar3;
        int paddingLeft2 = bVar4.d.getPaddingLeft();
        int paddingTop2 = bVar4.d.getPaddingTop();
        int paddingRight2 = bVar4.d.getPaddingRight();
        int paddingBottom2 = bVar4.d.getPaddingBottom();
        a(bVar4.d, R.drawable.hs__pill_small, R.attr.hs__selectableOptionColor);
        bVar4.d.setPadding(paddingLeft2, paddingTop2, paddingRight2, paddingBottom2);
        bVar4.d.setText(xVar.f3496a.d);
        bVar4.d.setVisibility(0);
        bVar4.d.setOnClickListener(new a(bVar4, this.f3980b, xVar, true));
    }

    an(Context context) {
        super(context);
    }

    /* compiled from: UserSelectableOptionViewDataBinder */
    protected final class b extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        final LinearLayout f3939a;

        /* renamed from: b  reason: collision with root package name */
        final LinearLayout f3940b;
        final TextView c;
        final TextView d;

        b(View view) {
            super(view);
            this.f3939a = (LinearLayout) view.findViewById(R.id.options_message_view);
            this.f3940b = (LinearLayout) view.findViewById(R.id.selectable_options_container);
            this.c = (TextView) view.findViewById(R.id.options_header);
            this.d = (TextView) view.findViewById(R.id.selectable_option_skip);
        }
    }

    /* compiled from: UserSelectableOptionViewDataBinder */
    final class a implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final b f3937a;

        /* renamed from: b  reason: collision with root package name */
        final u.a f3938b;
        final x c;
        final boolean d;

        a(b bVar, u.a aVar, x xVar, boolean z) {
            this.f3937a = bVar;
            this.f3938b = aVar;
            this.c = xVar;
            this.d = z;
        }

        public final void onClick(View view) {
            com.helpshift.d.a aVar = new com.helpshift.d.a(this.f3937a.f3939a);
            aVar.setDuration(250);
            aVar.setFillAfter(true);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(250);
            alphaAnimation.setFillAfter(true);
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(alphaAnimation);
            animationSet.addAnimation(aVar);
            animationSet.setAnimationListener(new ao(this, (TextView) view));
            this.f3937a.f3939a.startAnimation(animationSet);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        b bVar = new b(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_user_selectable_options_container, viewGroup, false));
        bVar.setIsRecyclable(false);
        return bVar;
    }
}
