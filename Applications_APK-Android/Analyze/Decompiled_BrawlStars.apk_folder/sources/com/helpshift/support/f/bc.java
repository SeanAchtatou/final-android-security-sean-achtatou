package com.helpshift.support.f;

import android.graphics.Bitmap;
import android.widget.TextView;
import com.helpshift.common.k;
import com.helpshift.j.d.d;
import com.helpshift.support.j.a;
import com.helpshift.support.m.b;
import com.helpshift.z.e;
import com.helpshift.z.g;
import java.util.Locale;

/* compiled from: NewConversationFragment */
class bc implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ar f4026a;

    bc(ar arVar) {
        this.f4026a = arVar;
    }

    public final void a(Object obj) {
        String str;
        d a2 = ((g) obj).a();
        bh a3 = this.f4026a.f4008b;
        if (a2 == null || k.a(a2.d)) {
            a3.k.setVisibility(8);
            a3.h.setVisibility(8);
            a3.l.setVisibility(8);
        } else {
            String str2 = a2.d;
            String str3 = a2.f3590a;
            Long l = a2.f3591b;
            Bitmap a4 = b.a(str2, -1, a3.m.isHardwareAccelerated());
            if (a4 != null) {
                a3.h.setImageBitmap(a4);
                TextView textView = a3.i;
                String str4 = "";
                if (str3 == null) {
                    str3 = str4;
                }
                textView.setText(str3);
                if (l != null) {
                    a aVar = new a((double) l.longValue());
                    if (aVar.f4139b.equals(" MB")) {
                        str = String.format(Locale.US, "%.1f", Double.valueOf(aVar.f4138a)) + aVar.f4139b;
                    } else {
                        str = String.format(Locale.US, "%.0f", Double.valueOf(aVar.f4138a)) + aVar.f4139b;
                    }
                    str4 = str;
                }
                a3.j.setText(str4);
                a3.h.setVisibility(0);
                a3.l.setVisibility(0);
                a3.k.setVisibility(0);
            }
        }
        bh unused = this.f4026a.f4008b;
    }
}
