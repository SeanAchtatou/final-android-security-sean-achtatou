package com.helpshift.support.f;

import android.view.KeyEvent;
import android.widget.TextView;

/* compiled from: ConversationalFragmentRenderer */
class ag implements TextView.OnEditorActionListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f3995a;

    ag(s sVar) {
        this.f3995a = sVar;
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 3) {
            return false;
        }
        this.f3995a.f();
        return true;
    }
}
