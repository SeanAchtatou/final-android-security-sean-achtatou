package com.helpshift.support.f;

import android.view.MenuItem;

/* compiled from: ConversationalFragment */
class i implements MenuItem.OnMenuItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f4046a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ d f4047b;

    i(d dVar, String str) {
        this.f4047b = dVar;
        this.f4046a = str;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        d.a(this.f4047b, this.f4046a);
        return true;
    }
}
