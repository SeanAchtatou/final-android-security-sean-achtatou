package com.helpshift.support.f;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.c.a.a.g;
import com.helpshift.common.exception.d;
import com.helpshift.common.k;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.q;
import com.helpshift.j.i.bn;
import com.helpshift.support.f.b.a;
import com.helpshift.support.i.e;
import com.helpshift.support.m.h;
import com.helpshift.support.m.l;
import com.helpshift.support.z;
import com.helpshift.util.t;
import com.helpshift.util.x;
import com.helpshift.views.bottomsheet.a;
import com.helpshift.views.bottomsheet.c;
import java.io.File;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/* compiled from: ConversationalFragmentRenderer */
public class s implements q {
    RecyclerView A;
    ImageView B;
    ImageView C;
    ImageView D;
    ImageView E;
    ImageView F;
    EditText G;
    a H;
    View I;
    Window J;

    /* renamed from: a  reason: collision with root package name */
    EditText f4058a;

    /* renamed from: b  reason: collision with root package name */
    View f4059b;
    al c;
    View d;
    RecyclerView e;
    aq f;
    Context g;
    ImageButton h;
    View i;
    e j;
    View k;
    View l;
    View m;
    TextView n;
    LinearLayout o;
    TextView p;
    RecyclerView.ItemDecoration q;
    LinearLayout r;
    com.helpshift.views.bottomsheet.a s;
    BottomSheetBehavior t;
    TextView u;
    TextView v;
    View w;
    View x;
    View y;
    View z;

    s(Context context, Window window, RecyclerView recyclerView, View view, View view2, View view3, View view4, e eVar, al alVar) {
        this.g = context;
        this.J = window;
        this.e = recyclerView;
        RecyclerView.ItemAnimator itemAnimator = this.e.getItemAnimator();
        if (itemAnimator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) itemAnimator).setSupportsChangeAnimations(false);
        }
        this.f4059b = view;
        this.d = view.findViewById(R.id.replyBoxLayout);
        this.f4058a = (EditText) this.d.findViewById(R.id.hs__messageText);
        this.h = (ImageButton) this.d.findViewById(R.id.hs__sendMessageBtn);
        int i2 = R.attr.hs__messageSendIcon;
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(i2, typedValue, true);
        this.h.setImageDrawable(context.getResources().getDrawable(typedValue.resourceId).mutate());
        this.m = view.findViewById(R.id.scroll_jump_button);
        this.i = view2;
        this.c = alVar;
        this.j = eVar;
        this.k = view3;
        this.l = view4;
        this.n = (TextView) view.findViewById(R.id.skipBubbleTextView);
        this.o = (LinearLayout) view.findViewById(R.id.skipOuterBubble);
        this.p = (TextView) view.findViewById(R.id.errorReplyTextView);
        this.r = (LinearLayout) view.findViewById(R.id.networkErrorFooter);
        this.c = alVar;
    }

    public final void a(b bVar) {
        if (bVar == null) {
            v();
            return;
        }
        q();
        f();
        w();
    }

    private void s() {
        x.a(this.f4059b.getContext(), this.n.getBackground(), R.attr.hs__selectableOptionColor);
        x.a(this.f4059b.getContext(), this.o.getBackground(), 16842836);
        this.o.setVisibility(0);
        this.e.removeItemDecoration(this.q);
        if (this.q == null) {
            this.q = new x(this);
        }
        this.e.addItemDecoration(this.q);
    }

    public final void i() {
        this.o.setVisibility(8);
        this.e.removeItemDecoration(this.q);
    }

    public final void a(int i2) {
        String str;
        boolean z2 = this.f4059b.getResources().getConfiguration().orientation == 2;
        Resources resources = this.g.getResources();
        if (i2 == 1) {
            str = resources.getString(R.string.hs__conversation_detail_error);
        } else if (i2 != 2) {
            str = i2 != 3 ? i2 != 4 ? "" : z2 ? resources.getString(R.string.hs__landscape_date_input_validation_error) : resources.getString(R.string.hs__date_input_validation_error) : z2 ? resources.getString(R.string.hs__landscape_number_input_validation_error) : resources.getString(R.string.hs__number_input_validation_error);
        } else {
            str = z2 ? resources.getString(R.string.hs__landscape_email_input_validation_error) : resources.getString(R.string.hs__email_input_validation_error);
        }
        if (z2) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.f4059b.getContext());
            builder.setTitle(resources.getString(R.string.hs__landscape_input_validation_dialog_title));
            builder.setCancelable(true);
            builder.setMessage(str);
            builder.setPositiveButton(17039370, new t(this));
            builder.create().show();
            return;
        }
        this.p.setText(str);
        this.p.setVisibility(0);
    }

    public final void j() {
        this.p.setVisibility(8);
    }

    public final void b(int i2) {
        this.r.setVisibility(0);
        TextView textView = (TextView) this.r.findViewById(R.id.networkErrorFooterText);
        ProgressBar progressBar = (ProgressBar) this.r.findViewById(R.id.networkErrorProgressBar);
        ImageView imageView = (ImageView) this.r.findViewById(R.id.networkErrorIcon);
        imageView.setVisibility(0);
        x.a(this.g, imageView, R.drawable.hs__network_error, R.attr.hs__errorTextColor);
        progressBar.setVisibility(8);
        Resources resources = this.g.getResources();
        if (i2 == 1) {
            textView.setText(resources.getString(R.string.hs__no_internet_error));
        } else if (i2 == 2) {
            textView.setText(resources.getString(R.string.hs__network_reconnecting_error));
            imageView.setVisibility(8);
            progressBar.setVisibility(0);
        }
    }

    public final void k() {
        this.r.setVisibility(8);
    }

    public final void b(List<bn> list) {
        if (this.H != null) {
            t();
            a aVar = this.H;
            aVar.f4020a.clear();
            aVar.f4020a.addAll(list);
            aVar.notifyDataSetChanged();
        }
    }

    public final void a(boolean z2) {
        BottomSheetBehavior bottomSheetBehavior = this.t;
        if (bottomSheetBehavior != null && this.s != null) {
            if (z2) {
                bottomSheetBehavior.setHideable(true);
                this.s.b();
                this.s.a(new ad(this));
                this.t.setState(5);
            } else {
                o();
            }
            u();
            f();
            a(this.f4059b, 0);
            i();
        }
    }

    /* access modifiers changed from: package-private */
    public final void o() {
        this.s.a();
        this.s = null;
    }

    private void a(View view, int i2) {
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), (int) x.a(this.g, (float) i2));
    }

    private void t() {
        if (this.I.isShown()) {
            this.I.setVisibility(8);
        }
        if (!this.A.isShown()) {
            this.A.setVisibility(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.util.x.a(android.content.Context, float):float
     arg types: [android.content.Context, int]
     candidates:
      com.helpshift.util.x.a(android.content.Context, int):int
      com.helpshift.util.x.a(android.content.Context, android.graphics.drawable.Drawable):void
      com.helpshift.util.x.a(android.content.Context, float):float */
    public final void a(List<bn> list, String str, boolean z2, String str2) {
        View view;
        String str3 = str;
        if (this.s == null) {
            boolean a2 = l.a(this.f4059b.getContext());
            float f2 = a2 ? 0.8f : 1.0f;
            a.C0149a aVar = new a.C0149a(this.J);
            aVar.f4270b = R.layout.hs__picker_layout;
            aVar.c = this.e;
            aVar.e = true;
            aVar.f = f2;
            if (aVar.f4269a != null) {
                if (aVar.e) {
                    View view2 = new View(aVar.c.getContext());
                    aVar.f4269a.addContentView(view2, aVar.f4269a.getAttributes());
                    view = view2;
                } else {
                    view = null;
                }
                LayoutInflater from = LayoutInflater.from(aVar.f4269a.getContext());
                aVar.d = from.inflate(aVar.f4270b, (ViewGroup) null);
                CoordinatorLayout coordinatorLayout = (CoordinatorLayout) from.inflate(R.layout.hs__bottomsheet_wrapper, (ViewGroup) null);
                this.s = new com.helpshift.views.bottomsheet.a(aVar.d, aVar.f4269a, aVar.c, view, aVar.e, aVar.f, coordinatorLayout, (FrameLayout) coordinatorLayout.findViewById(R.id.hs__bottom_sheet));
                this.t = BottomSheetBehavior.from(this.s.d);
                View view3 = this.s.f4267a;
                this.w = view3.findViewById(R.id.hs__picker_collapsed_shadow);
                this.x = view3.findViewById(R.id.hs__picker_expanded_shadow);
                this.A = (RecyclerView) view3.findViewById(R.id.hs__optionsList);
                this.A.setLayoutManager(new LinearLayoutManager(view3.getContext(), 1, false));
                this.C = (ImageView) view3.findViewById(R.id.hs__picker_action_search);
                this.D = (ImageView) view3.findViewById(R.id.hs__picker_action_clear);
                this.B = (ImageView) view3.findViewById(R.id.hs__picker_action_collapse);
                this.E = (ImageView) view3.findViewById(R.id.hs__picker_action_back);
                this.G = (EditText) view3.findViewById(R.id.hs__picker_header_search);
                this.u = (TextView) view3.findViewById(R.id.hs__expanded_picker_header_text);
                this.y = view3.findViewById(R.id.hs__picker_expanded_header);
                this.z = view3.findViewById(R.id.hs__picker_collapsed_header);
                this.v = (TextView) view3.findViewById(R.id.hs__collapsed_picker_header_text);
                this.I = view3.findViewById(R.id.hs__empty_picker_view);
                this.F = (ImageView) view3.findViewById(R.id.hs__picker_action_expand);
                this.u.setText(str3);
                this.v.setText(str3);
                String string = this.f4059b.getResources().getString(R.string.hs__picker_options_expand_header_voice_over, str3);
                this.z.setContentDescription(string);
                this.v.setContentDescription(string);
                x.a(this.g, this.C.getDrawable(), R.attr.hs__expandedPickerIconColor);
                x.a(this.g, this.E.getDrawable(), R.attr.hs__expandedPickerIconColor);
                x.a(this.g, this.B.getDrawable(), R.attr.hs__expandedPickerIconColor);
                x.a(this.g, this.D.getDrawable(), R.attr.hs__expandedPickerIconColor);
                x.a(this.g, this.F.getDrawable(), R.attr.hs__collapsedPickerIconColor);
                this.t.setPeekHeight((int) x.a(this.g, 142.0f));
                this.H = new com.helpshift.support.f.b.a(list, this.c);
                this.A.setAdapter(this.H);
                x.a(this.w, ContextCompat.getColor(this.g, R.color.hs__color_40000000), 0, GradientDrawable.Orientation.BOTTOM_TOP);
                q();
                if (z2 || k.a(str2)) {
                    i();
                } else {
                    this.n.setOnClickListener(new w(this));
                    this.n.setText(str2);
                    s();
                }
                f();
                int i2 = 14;
                if (a2) {
                    i2 = (int) (((float) (((int) this.f4059b.getResources().getDimension(R.dimen.activity_horizontal_margin_large)) + 14 + 4)) + ((CardView) this.f4059b.findViewById(R.id.hs__conversation_cardview_container)).getCardElevation());
                }
                a(this.f4059b, 142 - i2);
                this.G.addTextChangedListener(new af(this));
                this.G.setOnEditorActionListener(new ag(this));
                this.C.setOnClickListener(new ah(this));
                this.E.setOnClickListener(new ai(this));
                this.D.setOnClickListener(new aj(this));
                this.B.setOnClickListener(new ak(this));
                this.z.setOnClickListener(new u(this));
                this.s.a(new ae(this));
                t();
                com.helpshift.views.bottomsheet.a aVar2 = this.s;
                aVar2.d.addView(aVar2.f4267a);
                BottomSheetBehavior.from(aVar2.d).setBottomSheetCallback(new c(aVar2));
                if (aVar2.f4268b == null) {
                    Window window = aVar2.e;
                    View view4 = aVar2.c;
                    ViewGroup.LayoutParams layoutParams = aVar2.f4267a.getLayoutParams();
                    layoutParams.height = -1;
                    layoutParams.width = -1;
                    window.addContentView(view4, layoutParams);
                } else if (ViewCompat.isLaidOut(aVar2.f4268b)) {
                    aVar2.c();
                } else {
                    aVar2.f4268b.post(new com.helpshift.views.bottomsheet.b(aVar2));
                }
            } else {
                throw new IllegalArgumentException("Bottomsheet layout window can not be null");
            }
        }
    }

    public final void m() {
        if (this.D.isShown()) {
            this.D.setVisibility(8);
        }
    }

    public final void n() {
        if (!this.D.isShown()) {
            this.D.setVisibility(0);
        }
    }

    private void u() {
        View view;
        if (Build.VERSION.SDK_INT >= 19 && (view = this.f4059b) != null) {
            view.setImportantForAccessibility(0);
            this.c.g();
        }
    }

    /* access modifiers changed from: package-private */
    public final void p() {
        this.G.setVisibility(8);
        this.u.setVisibility(0);
        this.G.setText("");
        this.E.setVisibility(8);
        this.B.setVisibility(0);
        this.D.setVisibility(8);
        this.C.setVisibility(0);
        f();
        this.s.a(true);
    }

    private void v() {
        this.f4058a.setInputType(147457);
        this.f4058a.setHint(R.string.hs__chat_hint);
    }

    /* access modifiers changed from: package-private */
    public void a(com.helpshift.j.a.a.a.a aVar) {
        if (aVar == null) {
            w();
            this.d.setVisibility(0);
            ((LinearLayout) this.f4059b.findViewById(R.id.replyBoxLabelLayout)).setVisibility(8);
            this.f4058a.setFocusableInTouchMode(true);
            this.f4058a.setOnClickListener(null);
            v();
            i();
            return;
        }
        if (aVar instanceof com.helpshift.j.a.a.a.c) {
            com.helpshift.j.a.a.a.c cVar = (com.helpshift.j.a.a.a.c) aVar;
            this.f4058a.setFocusableInTouchMode(true);
            this.f4058a.setOnClickListener(null);
            if (!TextUtils.isEmpty(cVar.c)) {
                ((LinearLayout) this.f4059b.findViewById(R.id.replyBoxLabelLayout)).setVisibility(0);
                ((TextView) this.d.findViewById(R.id.replyFieldLabel)).setText(cVar.c);
            }
            this.f4058a.setHint(TextUtils.isEmpty(cVar.e) ? "" : cVar.e);
            int i2 = 131072;
            int i3 = cVar.f;
            if (i3 == 1) {
                i2 = 147457;
            } else if (i3 == 2) {
                i2 = 131105;
            } else if (i3 == 3) {
                i2 = 139266;
            } else if (i3 != 4) {
                v();
            } else {
                f();
                this.f4058a.setFocusableInTouchMode(false);
                this.f4058a.setOnClickListener(new z(this));
                i2 = 0;
            }
            this.f4058a.setInputType(i2);
            if (cVar.f3440b || TextUtils.isEmpty(cVar.d)) {
                i();
            } else {
                this.n.setOnClickListener(new v(this));
                this.n.setText(cVar.d);
                s();
            }
            this.d.setVisibility(0);
        }
        w();
    }

    public final void q() {
        this.e.setPadding(0, 0, 0, 0);
        this.d.setVisibility(8);
        i();
    }

    /* access modifiers changed from: package-private */
    public final DatePickerDialog r() {
        y yVar = new y(this);
        Calendar instance = Calendar.getInstance();
        try {
            String obj = this.f4058a.getText().toString();
            if (!k.a(obj)) {
                instance.setTime(com.helpshift.common.g.b.a("EEEE, MMMM dd, yyyy", com.helpshift.util.q.c().w().c()).a(obj));
            }
        } catch (ParseException unused) {
        }
        return new DatePickerDialog(this.f4059b.getContext(), yVar, instance.get(1), instance.get(2), instance.get(5));
    }

    public final void a(List<v> list) {
        this.f = new aq(this.g, list, this.c);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.g);
        linearLayoutManager.setStackFromEnd(true);
        this.e.setLayoutManager(linearLayoutManager);
        this.e.setAdapter(this.f);
    }

    public final void a(int i2, int i3) {
        aq aqVar = this.f;
        if (aqVar != null) {
            aqVar.notifyItemRangeInserted(i2 + aqVar.a(), i3);
        }
    }

    public final void b(int i2, int i3) {
        aq aqVar = this.f;
        if (aqVar != null) {
            if (i2 == 0 && i3 == aqVar.c()) {
                this.f.notifyDataSetChanged();
                return;
            }
            aq aqVar2 = this.f;
            aqVar2.notifyItemRangeChanged(i2 + aqVar2.a(), i3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.f.s.a(com.helpshift.support.i.c, boolean):void
     arg types: [com.helpshift.support.i.c, int]
     candidates:
      com.helpshift.support.f.s.a(android.content.Intent, java.io.File):void
      com.helpshift.support.f.s.a(android.view.View, int):void
      com.helpshift.support.f.s.a(int, int):void
      com.helpshift.support.f.s.a(java.lang.String, java.lang.String):void
      com.helpshift.j.a.q.a(int, int):void
      com.helpshift.j.a.q.a(java.lang.String, java.lang.String):void
      com.helpshift.support.f.s.a(com.helpshift.support.i.c, boolean):void */
    public final void b(boolean z2) {
        if (z2) {
            a(com.helpshift.support.i.c.SCREENSHOT_ATTACHMENT, true);
        } else {
            a(com.helpshift.support.i.c.SCREENSHOT_ATTACHMENT, false);
        }
    }

    public final String a() {
        return this.f4058a.getText().toString();
    }

    public final void a(String str) {
        this.f4058a.setText(str);
        EditText editText = this.f4058a;
        editText.setSelection(editText.getText().length());
    }

    private void a(Intent intent, File file) {
        if (intent.resolveActivity(this.g.getPackageManager()) != null) {
            this.g.startActivity(intent);
        } else if (com.helpshift.util.q.c().m().a()) {
            com.helpshift.util.q.c().m().a(file);
        } else {
            a(d.NO_APPS_FOR_OPENING_ATTACHMENT);
        }
    }

    public final void a(String str, String str2) {
        File a2 = com.helpshift.common.g.a.a(str);
        if (a2 != null) {
            a(t.a(this.g, a2, str2), a2);
        } else {
            a(d.FILE_NOT_FOUND);
        }
    }

    public final void b(String str, String str2) {
        Intent intent;
        if (g.a(str)) {
            Uri parse = Uri.parse(str);
            Intent intent2 = new Intent("android.intent.action.VIEW");
            intent2.setFlags(1);
            intent2.setDataAndType(parse, str2);
            if (intent2.resolveActivity(this.g.getPackageManager()) != null) {
                this.g.startActivity(intent2);
            } else if (!com.helpshift.util.q.c().m().a()) {
                a(d.NO_APPS_FOR_OPENING_ATTACHMENT);
            } else if (!(com.helpshift.util.q.c().m().b() instanceof z.a)) {
                a(d.NO_APPS_FOR_OPENING_ATTACHMENT);
            }
        } else {
            File a2 = com.helpshift.common.g.a.a(str);
            if (a2 != null) {
                if (Build.VERSION.SDK_INT >= 24) {
                    intent = t.a(this.g, a2, str2);
                } else {
                    Intent intent3 = new Intent("android.intent.action.VIEW");
                    intent3.setDataAndType(Uri.fromFile(a2), str2);
                    intent = intent3;
                }
                a(intent, a2);
                return;
            }
            a(d.FILE_NOT_FOUND);
        }
    }

    private void a(com.helpshift.common.exception.a aVar) {
        com.helpshift.support.m.k.a(aVar, this.f4059b);
    }

    public final void b(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        if (intent.resolveActivity(this.g.getPackageManager()) != null) {
            this.g.startActivity(intent);
        } else {
            a(d.NO_APPS_FOR_OPENING_ATTACHMENT);
        }
    }

    public final void b() {
        com.helpshift.support.m.k.a(this.f4059b, this.g.getResources().getString(R.string.hs__csat_submit_toast), 0);
    }

    public final void a(Map<String, Boolean> map) {
        this.c.a(map);
    }

    public final void c() {
        aq aqVar = this.f;
        if (aqVar != null) {
            aqVar.a(true);
        }
    }

    public final void d() {
        aq aqVar = this.f;
        if (aqVar != null) {
            aqVar.a(false);
        }
    }

    public final void e() {
        al alVar = this.c;
        if (alVar != null) {
            alVar.n();
        }
    }

    public final void f() {
        h.a(this.g, this.f4058a);
    }

    public final void g() {
        aq aqVar = this.f;
        if (aqVar != null) {
            aqVar.notifyDataSetChanged();
        }
    }

    public final void h() {
        int itemCount;
        aq aqVar = this.f;
        if (aqVar != null && (itemCount = aqVar.getItemCount()) > 0) {
            this.e.scrollToPosition(itemCount - 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.util.x.a(android.content.Context, float):float
     arg types: [android.content.Context, int]
     candidates:
      com.helpshift.util.x.a(android.content.Context, int):int
      com.helpshift.util.x.a(android.content.Context, android.graphics.drawable.Drawable):void
      com.helpshift.util.x.a(android.content.Context, float):float */
    private void w() {
        this.e.setPadding(0, 0, 0, (int) x.a(this.g, 12.0f));
    }

    private void a(com.helpshift.support.i.c cVar, boolean z2) {
        e eVar = this.j;
        if (eVar != null) {
            eVar.a(cVar, z2);
        }
    }

    public final void l() {
        if (!this.I.isShown()) {
            this.I.setVisibility(0);
        }
        if (this.A.isShown()) {
            this.A.setVisibility(8);
        }
    }

    static /* synthetic */ void a(s sVar) {
        sVar.w.setVisibility(0);
        x.a(sVar.w, ContextCompat.getColor(sVar.g, R.color.hs__color_40000000), 0, GradientDrawable.Orientation.BOTTOM_TOP);
        sVar.t();
        sVar.p();
        sVar.z.setVisibility(0);
        sVar.y.setVisibility(8);
        sVar.A.scrollToPosition(0);
        sVar.u();
    }

    static /* synthetic */ void b(s sVar) {
        View view;
        sVar.w.setVisibility(8);
        x.a(sVar.x, ContextCompat.getColor(sVar.g, R.color.hs__color_40000000), 0, GradientDrawable.Orientation.TOP_BOTTOM);
        sVar.y.setVisibility(0);
        sVar.z.setVisibility(8);
        if (Build.VERSION.SDK_INT >= 19 && (view = sVar.f4059b) != null) {
            view.setImportantForAccessibility(4);
            sVar.c.b(4);
        }
    }
}
