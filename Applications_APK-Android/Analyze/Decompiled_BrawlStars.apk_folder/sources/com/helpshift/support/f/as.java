package com.helpshift.support.f;

import com.helpshift.R;
import com.helpshift.z.e;
import com.helpshift.z.s;

/* compiled from: NewConversationFragment */
class as implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ar f4009a;

    as(ar arVar) {
        this.f4009a = arVar;
    }

    public final void a(Object obj) {
        s sVar = (s) obj;
        bh a2 = this.f4009a.f4008b;
        a2.f4032b.setText(sVar.b());
        a2.f4032b.setSelection(a2.f4032b.getText().length());
        bh a3 = this.f4009a.f4008b;
        s.a a4 = sVar.a();
        if (s.a.EMPTY.equals(a4)) {
            bh.a(a3.f4031a, a3.a(R.string.hs__conversation_detail_error));
        } else if (s.a.ONLY_SPECIAL_CHARACTERS.equals(a4)) {
            bh.a(a3.f4031a, a3.a(R.string.hs__invalid_description_error));
        } else if (s.a.LESS_THAN_MINIMUM_LENGTH.equals(a4)) {
            bh.a(a3.f4031a, a3.a(R.string.hs__description_invalid_length_error));
        } else {
            bh.a(a3.f4031a, (CharSequence) null);
        }
    }
}
