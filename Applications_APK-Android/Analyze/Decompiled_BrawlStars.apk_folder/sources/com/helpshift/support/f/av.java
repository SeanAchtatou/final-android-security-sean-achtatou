package com.helpshift.support.f;

import android.view.MotionEvent;
import android.view.View;
import com.helpshift.R;

/* compiled from: NewConversationFragment */
class av implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ar f4012a;

    av(ar arVar) {
        this.f4012a = arVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view.getId() == R.id.hs__conversationDetail) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            if ((motionEvent.getAction() & 255) == 1) {
                view.getParent().requestDisallowInterceptTouchEvent(false);
            }
        }
        return false;
    }
}
