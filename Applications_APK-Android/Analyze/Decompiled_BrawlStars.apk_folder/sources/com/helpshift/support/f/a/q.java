package com.helpshift.support.f.a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.support.widget.CSATView;
import com.helpshift.util.x;

/* compiled from: ConversationFooterViewBinder */
public class q {

    /* renamed from: a  reason: collision with root package name */
    public a f3969a;

    /* renamed from: b  reason: collision with root package name */
    public Context f3970b;

    /* compiled from: ConversationFooterViewBinder */
    public interface a {
        void a(int i, String str);

        void b();
    }

    public q(Context context) {
        this.f3970b = context;
    }

    /* compiled from: ConversationFooterViewBinder */
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener, CSATView.a {

        /* renamed from: a  reason: collision with root package name */
        public final View f3971a;

        /* renamed from: b  reason: collision with root package name */
        public final TextView f3972b;
        public final LinearLayout c;
        public final Button d;
        public final CSATView e;
        public final TextView f;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.helpshift.util.x.a(android.content.Context, float):float
         arg types: [android.content.Context, int]
         candidates:
          com.helpshift.util.x.a(android.content.Context, int):int
          com.helpshift.util.x.a(android.content.Context, android.graphics.drawable.Drawable):void
          com.helpshift.util.x.a(android.content.Context, float):float */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, int, int, int, int):void}
         arg types: [android.graphics.drawable.GradientDrawable, int, int, int, int]
         candidates:
          ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, float, float, float, float):void}
          ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, int, int, int, int):void} */
        public b(View view) {
            super(view);
            this.f3971a = view;
            this.f3972b = (TextView) view.findViewById(R.id.footer_message);
            this.c = (LinearLayout) view.findViewById(R.id.hs__new_conversation);
            this.d = (Button) view.findViewById(R.id.hs__new_conversation_btn);
            this.e = (CSATView) view.findViewById(R.id.csat_view_layout);
            this.f = (TextView) view.findViewById(R.id.hs__new_conversation_footer_reason);
            GradientDrawable gradientDrawable = (GradientDrawable) ContextCompat.getDrawable(q.this.f3970b, R.drawable.hs__button_with_border);
            gradientDrawable.setStroke((int) x.a(q.this.f3970b, 1.0f), x.a(q.this.f3970b, R.attr.colorAccent));
            gradientDrawable.setColor(x.a(q.this.f3970b, R.attr.hs__footerPromptBackground));
            int a2 = (int) x.a(q.this.f3970b, 4.0f);
            int a3 = (int) x.a(q.this.f3970b, 6.0f);
            InsetDrawable insetDrawable = new InsetDrawable((Drawable) gradientDrawable, a2, a3, a2, a3);
            if (Build.VERSION.SDK_INT >= 16) {
                this.d.setBackground(insetDrawable);
            } else {
                this.d.setBackgroundDrawable(insetDrawable);
            }
        }

        public final void onClick(View view) {
            if (q.this.f3969a != null) {
                q.this.f3969a.b();
            }
        }

        public final void a(int i, String str) {
            if (q.this.f3969a != null) {
                q.this.f3969a.a(i, str);
            }
        }
    }
}
