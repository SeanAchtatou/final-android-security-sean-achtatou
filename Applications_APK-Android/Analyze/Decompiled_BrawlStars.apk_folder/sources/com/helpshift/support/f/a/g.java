package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.common.k;
import com.helpshift.j.a.a.ai;
import com.helpshift.j.a.a.v;

/* compiled from: AdminMessageViewDataBinder */
class g extends u<a, v> {
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        a aVar = (a) viewHolder;
        if (k.a(vVar.n)) {
            aVar.f3951a.setVisibility(8);
            return;
        }
        aVar.f3951a.setVisibility(0);
        aVar.f3952b.setText(a(vVar.n));
        ai l = vVar.l();
        a(aVar.d, l);
        a(aVar.c, l, vVar.h());
        aVar.f3951a.setContentDescription(a(vVar));
        a(aVar.f3952b, new h(this, vVar));
    }

    g(Context context) {
        super(context);
    }

    /* compiled from: AdminMessageViewDataBinder */
    protected final class a extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        /* renamed from: a  reason: collision with root package name */
        final View f3951a;

        /* renamed from: b  reason: collision with root package name */
        final TextView f3952b;
        final TextView c;
        final View d;

        a(View view) {
            super(view);
            this.f3951a = view.findViewById(R.id.admin_text_message_layout);
            this.f3952b = (TextView) view.findViewById(R.id.admin_message_text);
            this.c = (TextView) view.findViewById(R.id.admin_date_text);
            this.d = view.findViewById(R.id.admin_message_container);
        }

        public final void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            if (g.this.f3980b != null) {
                g.this.f3980b.a(contextMenu, ((TextView) view).getText().toString());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        a aVar = new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_txt_admin, viewGroup, false));
        aVar.f3952b.setOnCreateContextMenuListener(aVar);
        return aVar;
    }
}
