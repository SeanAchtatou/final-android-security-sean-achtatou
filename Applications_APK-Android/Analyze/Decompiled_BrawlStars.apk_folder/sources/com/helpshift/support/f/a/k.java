package com.helpshift.support.f.a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.ai;
import com.helpshift.j.a.a.p;
import com.helpshift.j.a.a.v;
import com.helpshift.util.x;

/* compiled from: AdminSuggestionsMessageViewDataBinder */
public class k extends u<a, p> {
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        Drawable[] drawableArr;
        a aVar = (a) viewHolder;
        p pVar = (p) vVar;
        if (com.helpshift.common.k.a(pVar.n)) {
            aVar.d.setVisibility(8);
        } else {
            aVar.d.setVisibility(0);
            aVar.f3960b.setText(a(pVar.n));
            a(aVar.d, pVar.l().b() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_admin, R.attr.hs__chatBubbleAdminBackgroundColor);
            aVar.d.setContentDescription(a(pVar));
            a(aVar.f3960b, new m(this, pVar));
        }
        aVar.f3959a.removeAllViews();
        TableRow tableRow = null;
        for (p.a next : pVar.f3485a) {
            View inflate = LayoutInflater.from(this.f3979a).inflate(R.layout.hs__msg_admin_suggesstion_item, (ViewGroup) null);
            TextView textView = (TextView) inflate.findViewById(R.id.admin_suggestion_message);
            textView.setText(next.f3487a);
            if (Build.VERSION.SDK_INT >= 17) {
                drawableArr = textView.getCompoundDrawablesRelative();
            } else {
                drawableArr = textView.getCompoundDrawables();
            }
            x.a(this.f3979a, drawableArr[2], R.attr.hs__adminFaqSuggestionArrowColor);
            TableRow tableRow2 = new TableRow(this.f3979a);
            tableRow2.addView(inflate);
            View inflate2 = LayoutInflater.from(this.f3979a).inflate(R.layout.hs__section_divider, (ViewGroup) null);
            inflate2.findViewById(R.id.divider).setBackgroundColor(x.a(this.f3979a, R.attr.hs__contentSeparatorColor));
            TableRow tableRow3 = new TableRow(this.f3979a);
            tableRow3.addView(inflate2);
            aVar.f3959a.addView(tableRow2);
            aVar.f3959a.addView(tableRow3);
            inflate.setOnClickListener(new l(this, pVar, next));
            tableRow = tableRow3;
        }
        aVar.f3959a.removeView(tableRow);
        ai l = pVar.l();
        a(aVar.c, l.a());
        if (l.a()) {
            aVar.c.setText(pVar.h());
        }
        aVar.e.setContentDescription(a(pVar));
    }

    k(Context context) {
        super(context);
    }

    /* compiled from: AdminSuggestionsMessageViewDataBinder */
    protected final class a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        final TableLayout f3959a;

        /* renamed from: b  reason: collision with root package name */
        final TextView f3960b;
        final TextView c;
        final View d;
        final View e;

        a(View view) {
            super(view);
            this.e = view.findViewById(R.id.admin_suggestion_message_layout);
            this.f3959a = (TableLayout) view.findViewById(R.id.suggestionsListStub);
            this.f3960b = (TextView) view.findViewById(R.id.admin_message_text);
            this.d = view.findViewById(R.id.admin_message_container);
            this.c = (TextView) view.findViewById(R.id.admin_date_text);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new a(LayoutInflater.from(this.f3979a).inflate(R.layout.hs__msg_admin_suggesstions_container, viewGroup, false));
    }
}
