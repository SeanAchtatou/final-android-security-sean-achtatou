package com.helpshift.support.f.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.j.a.a.ai;
import com.helpshift.j.a.a.v;
import com.helpshift.support.m.l;

/* compiled from: RequestScreenshotMessageDataBinder */
public class aa extends u<a, com.helpshift.j.a.a.aa> {
    public final /* synthetic */ void a(RecyclerView.ViewHolder viewHolder, v vVar) {
        a aVar = (a) viewHolder;
        com.helpshift.j.a.a.aa aaVar = (com.helpshift.j.a.a.aa) vVar;
        aVar.f3914b.setText(a(aaVar.n));
        a(aVar.c, !aaVar.f3445a);
        ai l = aaVar.l();
        a(aVar.f, l.b() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_admin, R.attr.hs__chatBubbleAdminBackgroundColor);
        if (l.a()) {
            aVar.d.setText(aaVar.h());
        }
        a(aVar.d, l.a());
        aVar.c.setOnClickListener(new ab(this, aaVar));
        aVar.f3913a.setContentDescription(a(aaVar));
        a(aVar.f3914b, new ac(this, aaVar));
    }

    public aa(Context context) {
        super(context);
    }

    /* compiled from: RequestScreenshotMessageDataBinder */
    protected final class a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        final View f3913a;

        /* renamed from: b  reason: collision with root package name */
        final TextView f3914b;
        final Button c;
        final TextView d;
        /* access modifiers changed from: private */
        public final LinearLayout f;

        a(View view) {
            super(view);
            this.f3913a = view.findViewById(R.id.agent_screenshot_request_message_layout);
            this.f3914b = (TextView) view.findViewById(R.id.admin_attachment_request_text);
            this.c = (Button) view.findViewById(R.id.admin_attach_screenshot_button);
            this.f = (LinearLayout) view.findViewById(R.id.admin_message);
            this.d = (TextView) view.findViewById(R.id.admin_date_text);
            l.a(aa.this.f3979a, this.f.getBackground());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_request_screenshot, viewGroup, false));
    }
}
