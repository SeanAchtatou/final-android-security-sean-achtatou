package com.helpshift.support.f.a;

import android.view.animation.Animation;
import android.widget.TextView;
import com.helpshift.j.a.a.a.b;
import com.helpshift.support.f.a.an;

/* compiled from: UserSelectableOptionViewDataBinder */
class ao implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TextView f3941a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ an.a f3942b;

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    ao(an.a aVar, TextView textView) {
        this.f3942b = aVar;
        this.f3941a = textView;
    }

    public void onAnimationEnd(Animation animation) {
        this.f3942b.f3937a.f3939a.setVisibility(8);
        if (this.f3942b.f3938b != null) {
            this.f3942b.f3938b.a(this.f3942b.c, (b.a) this.f3941a.getTag(), this.f3942b.d);
        }
    }
}
