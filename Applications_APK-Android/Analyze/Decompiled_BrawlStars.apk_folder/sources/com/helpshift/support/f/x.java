package com.helpshift.support.f;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

/* compiled from: ConversationalFragmentRenderer */
class x extends RecyclerView.ItemDecoration {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f4064a;

    x(s sVar) {
        this.f4064a = sVar;
    }

    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        RecyclerView.Adapter adapter;
        int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
        if (childAdapterPosition != -1 && (adapter = recyclerView.getAdapter()) != null && this.f4064a.o.getVisibility() == 0 && childAdapterPosition == adapter.getItemCount() - 1) {
            rect.set(rect.left, rect.top, rect.right, (int) TypedValue.applyDimension(1, 80.0f, recyclerView.getContext().getResources().getDisplayMetrics()));
        }
    }
}
