package com.helpshift.support.f;

import android.support.design.widget.BottomSheetBehavior;
import android.view.View;

/* compiled from: ConversationalFragmentRenderer */
class ae extends BottomSheetBehavior.BottomSheetCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f3993a;

    ae(s sVar) {
        this.f3993a = sVar;
    }

    public void onStateChanged(View view, int i) {
        if (4 == i) {
            s.a(this.f3993a);
        } else if (3 == i) {
            s.b(this.f3993a);
        }
    }

    public void onSlide(View view, float f) {
        if (((double) f) > 0.5d && this.f3993a.t.getState() == 2) {
            s.b(this.f3993a);
        } else if (this.f3993a.t.getState() == 2) {
            s.a(this.f3993a);
        }
    }
}
