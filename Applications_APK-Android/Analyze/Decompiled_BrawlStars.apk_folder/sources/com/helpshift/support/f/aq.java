package com.helpshift.support.f;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.aa;
import com.helpshift.j.a.a.ab;
import com.helpshift.j.a.a.ad;
import com.helpshift.j.a.a.ae;
import com.helpshift.j.a.a.ag;
import com.helpshift.j.a.a.ah;
import com.helpshift.j.a.a.al;
import com.helpshift.j.a.a.b;
import com.helpshift.j.a.a.e;
import com.helpshift.j.a.a.h;
import com.helpshift.j.a.a.o;
import com.helpshift.j.a.a.p;
import com.helpshift.j.a.a.t;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.y;
import com.helpshift.j.a.a.z;
import com.helpshift.support.f.a.n;
import com.helpshift.support.f.a.q;
import com.helpshift.support.f.a.r;
import com.helpshift.support.f.a.s;
import com.helpshift.support.f.a.u;
import com.helpshift.support.f.a.w;
import com.helpshift.support.f.a.x;
import com.helpshift.support.m.l;
import java.util.List;

/* compiled from: MessagesAdapter */
public class aq extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements q.a, s.a, u.a {

    /* renamed from: a  reason: collision with root package name */
    x f4005a;

    /* renamed from: b  reason: collision with root package name */
    o f4006b = o.NONE;
    t c = t.NONE;
    private w d;
    private List<v> e;
    private boolean f = false;

    public aq(Context context, List<v> list, x xVar) {
        this.d = new w(context);
        this.e = list;
        this.f4005a = xVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == com.helpshift.support.f.a.v.HISTORY_LOADING_VIEW.u) {
            s sVar = this.d.c;
            sVar.f3975b = this;
            return new s.b(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__history_loading_view_layout, viewGroup, false));
        } else if (i == com.helpshift.support.f.a.v.CONVERSATION_FOOTER.u) {
            q qVar = this.d.f3983a;
            qVar.f3969a = this;
            return new q.b(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__messages_list_footer, viewGroup, false));
        } else if (i == com.helpshift.support.f.a.v.AGENT_TYPING_FOOTER.u) {
            n nVar = this.d.f3984b;
            View inflate = LayoutInflater.from(nVar.f3965a).inflate(R.layout.hs__msg_agent_typing, viewGroup, false);
            l.a(nVar.f3965a, inflate.findViewById(R.id.agent_typing_container).getBackground());
            return new com.helpshift.support.f.a.o(nVar, inflate);
        } else {
            u a2 = this.d.a(i);
            a2.a(this);
            return a2.a(viewGroup);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        int itemViewType = viewHolder.getItemViewType();
        int i2 = 8;
        boolean z8 = true;
        if (itemViewType == com.helpshift.support.f.a.v.HISTORY_LOADING_VIEW.u) {
            s.b bVar = (s.b) viewHolder;
            int i3 = com.helpshift.support.f.a.t.f3978a[this.c.ordinal()];
            if (i3 != 1) {
                if (i3 == 2) {
                    z6 = true;
                } else if (i3 != 3) {
                    z6 = false;
                } else {
                    z6 = false;
                    z7 = true;
                }
                z7 = false;
            } else {
                z6 = false;
                z7 = false;
                z8 = false;
            }
            bVar.f3977b.setVisibility(z8 ? 0 : 8);
            bVar.c.setVisibility(z6 ? 0 : 8);
            View c2 = bVar.d;
            if (z7) {
                i2 = 0;
            }
            c2.setVisibility(i2);
        } else if (itemViewType == com.helpshift.support.f.a.v.CONVERSATION_FOOTER.u) {
            q qVar = this.d.f3983a;
            q.b bVar2 = (q.b) viewHolder;
            o oVar = this.f4006b;
            String string = qVar.f3970b.getResources().getString(R.string.hs__conversation_end_msg);
            switch (r.f3973a[oVar.ordinal()]) {
                case 1:
                    z = false;
                    z5 = false;
                    z8 = false;
                    z4 = false;
                    z3 = false;
                    z2 = false;
                    break;
                case 2:
                    string = qVar.f3970b.getResources().getString(R.string.hs__confirmation_footer_msg);
                    z = true;
                    z5 = false;
                    z4 = false;
                    z3 = false;
                    z2 = false;
                    break;
                case 3:
                    z = true;
                    z5 = true;
                    z4 = false;
                    z3 = false;
                    z2 = false;
                    break;
                case 4:
                    string = qVar.f3970b.getResources().getString(R.string.hs__confirmation_footer_msg);
                    z = true;
                    z5 = true;
                    z4 = true;
                    z3 = false;
                    z2 = false;
                    break;
                case 5:
                    z = false;
                    z5 = true;
                    z4 = false;
                    z3 = true;
                    z2 = false;
                    break;
                case 6:
                    z = false;
                    z5 = true;
                    z4 = false;
                    z3 = false;
                    z2 = true;
                    break;
                case 7:
                    string = qVar.f3970b.getResources().getString(R.string.hs__conversation_rejected_status);
                    z = true;
                    z5 = true;
                    z4 = false;
                    z3 = false;
                    z2 = false;
                    break;
                case 8:
                    z = false;
                    z5 = true;
                    z4 = false;
                    z3 = false;
                    z2 = false;
                    break;
                default:
                    z = true;
                    z5 = false;
                    z4 = false;
                    z3 = false;
                    z2 = false;
                    break;
            }
            if (z8) {
                bVar2.f3971a.setVisibility(0);
                if (z) {
                    bVar2.f3972b.setText(string);
                    bVar2.f3972b.setVisibility(0);
                } else {
                    bVar2.f3972b.setVisibility(8);
                }
                if (z5) {
                    bVar2.c.setVisibility(0);
                    bVar2.d.setOnClickListener(bVar2);
                } else {
                    bVar2.c.setVisibility(8);
                    bVar2.c.setOnClickListener(null);
                }
                if (z4) {
                    bVar2.e.setVisibility(0);
                    bVar2.e.setCSATListener(bVar2);
                } else {
                    bVar2.e.setVisibility(8);
                    bVar2.e.setCSATListener(null);
                }
                if (z3) {
                    bVar2.f.setVisibility(0);
                    bVar2.f.setText(R.string.hs__issue_archival_message);
                } else if (z2) {
                    bVar2.f.setVisibility(0);
                    bVar2.f.setText(R.string.hs__new_conversation_footer_generic_reason);
                } else {
                    bVar2.f.setVisibility(8);
                }
            } else {
                bVar2.f3971a.setVisibility(8);
            }
        } else if (itemViewType != com.helpshift.support.f.a.v.AGENT_TYPING_FOOTER.u) {
            this.d.a(itemViewType).a(viewHolder, b(i));
        }
    }

    public int getItemViewType(int i) {
        if (i < a()) {
            return com.helpshift.support.f.a.v.HISTORY_LOADING_VIEW.u;
        }
        if (i < a() + c()) {
            v b2 = b(i);
            if (b2.y) {
                if (b2.j) {
                    return com.helpshift.support.f.a.v.ADMIN_REDACTED_MESSAGE.u;
                }
                return com.helpshift.support.f.a.v.USER_REDACTED_MESSAGE.u;
            } else if (b2 instanceof p) {
                return com.helpshift.support.f.a.v.ADMIN_SUGGESTIONS_LIST.u;
            } else {
                if (b2 instanceof com.helpshift.j.a.a.x) {
                    return com.helpshift.support.f.a.v.USER_SELECTABLE_OPTION.u;
                }
                if (b2 instanceof h) {
                    return com.helpshift.support.f.a.v.ADMIN_TEXT_MESSAGE.u;
                }
                if (b2 instanceof al) {
                    return com.helpshift.support.f.a.v.USER_TEXT_MESSAGE.u;
                }
                if (b2 instanceof ab) {
                    return com.helpshift.support.f.a.v.USER_SCREENSHOT_ATTACHMENT.u;
                }
                if (b2 instanceof e) {
                    return com.helpshift.support.f.a.v.ADMIN_ATTACHMENT_IMAGE.u;
                }
                if (b2 instanceof b) {
                    return com.helpshift.support.f.a.v.ADMIN_ATTACHMENT_GENERIC.u;
                }
                if (b2 instanceof y) {
                    return com.helpshift.support.f.a.v.REQUESTED_APP_REVIEW.u;
                }
                if (b2 instanceof com.helpshift.j.a.a.n) {
                    return com.helpshift.support.f.a.v.CONFIRMATION_REJECTED.u;
                }
                if (b2 instanceof aa) {
                    return com.helpshift.support.f.a.v.ADMIN_REQUEST_ATTACHMENT.u;
                }
                if (b2 instanceof z) {
                    return com.helpshift.support.f.a.v.REQUEST_FOR_REOPEN.u;
                }
                if (b2 instanceof ad) {
                    return com.helpshift.support.f.a.v.SYSTEM_DATE.u;
                }
                if (b2 instanceof ae) {
                    return com.helpshift.support.f.a.v.SYSTEM_DIVIDER.u;
                }
                if (b2 instanceof ag) {
                    return com.helpshift.support.f.a.v.SYSTEM_PUBLISH_ID.u;
                }
                if (b2 instanceof ah) {
                    return com.helpshift.support.f.a.v.SYSTEM_CONVERSATION_REDACTED_MESSAGE.u;
                }
                return -1;
            }
        } else {
            int a2 = i - (a() + c());
            boolean z = this.f4006b != o.NONE;
            if (a2 != 0) {
                if (a2 == 1 && z) {
                    return com.helpshift.support.f.a.v.CONVERSATION_FOOTER.u;
                }
                return -1;
            } else if (this.f) {
                return com.helpshift.support.f.a.v.AGENT_TYPING_FOOTER.u;
            } else {
                if (z) {
                    return com.helpshift.support.f.a.v.CONVERSATION_FOOTER.u;
                }
                return -1;
            }
        }
    }

    public int getItemCount() {
        int a2 = a() + c();
        int i = this.f ? 1 : 0;
        if (this.f4006b != o.NONE) {
            i++;
        }
        return a2 + i;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.c != t.NONE ? 1 : 0;
    }

    private v b(int i) {
        return this.e.get(i - a());
    }

    public final void a(String str, v vVar) {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.a(str, vVar);
        }
    }

    public final void a(ContextMenu contextMenu, String str) {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.a(contextMenu, str);
        }
    }

    public final void a(int i) {
        if (this.f4005a != null) {
            this.f4005a.a(b(i));
        }
    }

    public final void a(aa aaVar) {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.a(aaVar);
        }
    }

    public final void a(y yVar) {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.a(yVar);
        }
    }

    public final void a(ab abVar) {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.a(abVar);
        }
    }

    public final void a(b bVar) {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.a(bVar);
        }
    }

    public final void a(e eVar) {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.a(eVar);
        }
    }

    public final void a(com.helpshift.j.a.a.x xVar, b.a aVar, boolean z) {
        x xVar2 = this.f4005a;
        if (xVar2 != null) {
            xVar2.a(xVar, aVar, z);
        }
    }

    public final void a(v vVar, String str, String str2) {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.a(vVar, str, str2);
        }
    }

    public final void a(boolean z) {
        if (this.f != z) {
            this.f = z;
            if (z) {
                notifyItemRangeInserted(this.e.size(), 1);
            } else {
                notifyItemRangeRemoved(this.e.size(), 1);
            }
        }
    }

    public final void b() {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.f();
        }
    }

    public final void a(int i, String str) {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.a(i, str);
        }
    }

    public final int c() {
        return this.e.size();
    }

    public final void d() {
        x xVar = this.f4005a;
        if (xVar != null) {
            xVar.i();
        }
    }
}
