package com.helpshift.support;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.helpshift.s.b;
import com.helpshift.support.c.a;
import com.helpshift.support.j.c;
import com.helpshift.support.l.b;
import com.helpshift.support.l.d;
import com.helpshift.support.l.g;
import com.helpshift.support.l.h;
import com.helpshift.support.s;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: HSApiData */
public final class h {

    /* renamed from: a  reason: collision with root package name */
    public static ArrayList<Object> f4074a = null;
    private static final Object e = new Object();

    /* renamed from: b  reason: collision with root package name */
    public u f4075b;
    public g c;
    public b d;
    private Iterator f = null;
    private ArrayList<Faq> g = null;

    public h(Context context) {
        this.f4075b = new u(context);
        this.c = h.a.f4168a;
        this.d = d.a.f4160a;
    }

    protected static void a() {
        if (f4074a != null) {
            for (int i = 0; i < f4074a.size(); i++) {
                f4074a.get(i);
            }
        }
    }

    private static void h() {
        if (f4074a != null) {
            for (int i = 0; i < f4074a.size(); i++) {
                f4074a.get(i);
            }
        }
    }

    private void i() {
        ArrayList<Section> j = j();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < j.size(); i++) {
            arrayList.addAll(d(j.get(i).c));
        }
        synchronized (e) {
            this.g = new ArrayList<>(arrayList);
        }
    }

    public void a(Handler handler, Handler handler2, g gVar) {
        q.c().v().a(new i(this, handler, gVar, handler2));
    }

    public final void b(Handler handler, Handler handler2, g gVar) {
        ArrayList arrayList;
        try {
            arrayList = (ArrayList) this.c.a(gVar);
        } catch (SQLException e2) {
            n.c("Helpshift_ApiData", "Database exception in getting sections data ", e2);
            arrayList = null;
        }
        if (arrayList != null) {
            Message obtainMessage = handler.obtainMessage();
            obtainMessage.what = a.f3899a;
            obtainMessage.obj = arrayList;
            handler.sendMessage(obtainMessage);
        } else {
            Message obtainMessage2 = handler2.obtainMessage();
            obtainMessage2.what = a.f3900b;
            handler2.sendMessage(obtainMessage2);
        }
        a(handler, handler2, gVar);
    }

    private ArrayList<Section> j() {
        ArrayList<Section> arrayList = new ArrayList<>();
        try {
            return (ArrayList) this.c.a();
        } catch (SQLException e2) {
            n.c("Helpshift_ApiData", "Database exception in getting sections data ", e2);
            return arrayList;
        }
    }

    public final ArrayList<Section> a(ArrayList<Section> arrayList, g gVar) {
        ArrayList<Section> arrayList2 = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            if (!a(arrayList.get(i).c, gVar).isEmpty()) {
                arrayList2.add(arrayList.get(i));
            }
        }
        return arrayList2;
    }

    public final ArrayList<Faq> a(String str, g gVar) {
        ArrayList<Faq> arrayList = new ArrayList<>();
        try {
            return (ArrayList) this.d.a(str, gVar);
        } catch (SQLException e2) {
            n.c("Helpshift_ApiData", "Database exception in getting faqs for section", e2);
            return arrayList;
        }
    }

    private ArrayList<Faq> d(String str) {
        ArrayList<Faq> arrayList = new ArrayList<>();
        try {
            return (ArrayList) this.d.c(str);
        } catch (SQLException e2) {
            n.c("Helpshift_ApiData", "Database exception in getting faqs for section", e2);
            return arrayList;
        }
    }

    public final ArrayList<Faq> a(String str, s.a aVar, g gVar) {
        ArrayList<Faq> arrayList = this.g;
        Map<String, List<c>> map = null;
        if (arrayList == null) {
            i();
        } else {
            Iterator<Faq> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().i = null;
            }
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        String lowerCase = str.toLowerCase();
        if (u.e() || !this.f4075b.c("dbFlag").booleanValue()) {
            for (int i = 0; i < this.g.size(); i++) {
                Faq faq = this.g.get(i);
                if (!faq.f3836a.toLowerCase().contains(lowerCase)) {
                    linkedHashSet.add(faq);
                }
            }
        } else {
            com.helpshift.support.j.b bVar = u.f4199a;
            if (bVar != null) {
                map = bVar.f4140a;
            }
            ArrayList<HashMap> a2 = s.a(str, aVar);
            ArrayList<HashMap> a3 = s.a(str, map);
            Iterator<HashMap> it2 = a2.iterator();
            while (it2.hasNext()) {
                HashMap next = it2.next();
                int intValue = Integer.decode((String) next.get("f")).intValue();
                if (intValue < this.g.size()) {
                    Faq faq2 = this.g.get(intValue);
                    faq2.a((ArrayList) next.get("t"));
                    linkedHashSet.add(faq2);
                }
            }
            Iterator<HashMap> it3 = a3.iterator();
            while (it3.hasNext()) {
                HashMap next2 = it3.next();
                int intValue2 = Integer.decode((String) next2.get("f")).intValue();
                if (intValue2 < this.g.size()) {
                    Faq faq3 = this.g.get(intValue2);
                    faq3.a((ArrayList) next2.get("t"));
                    linkedHashSet.add(faq3);
                }
            }
        }
        if (gVar != null) {
            return new ArrayList<>(this.d.a(new ArrayList(linkedHashSet), gVar));
        }
        return new ArrayList<>(linkedHashSet);
    }

    public final List<Faq> a(g gVar) {
        ArrayList<Faq> arrayList = this.g;
        if (arrayList == null) {
            i();
        } else {
            Iterator<Faq> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().i = null;
            }
        }
        if (gVar != null) {
            return new ArrayList(this.d.a(new ArrayList(this.g), gVar));
        }
        return this.g;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        com.helpshift.i.a.a r = q.c().r();
        if (r.a("app_reviewed") || TextUtils.isEmpty(r.c("reviewUrl"))) {
            return false;
        }
        com.helpshift.i.c.a b2 = r.b();
        if (b2.f3431a && b2.f3432b > 0) {
            int a2 = this.f4075b.a();
            String str = b2.c;
            int i = b2.f3432b;
            if ("l".equals(str) && a2 >= i) {
                return true;
            }
            if (!"s".equals(str) || a2 == 0 || (new Date().getTime() / 1000) - ((long) a2) < ((long) i)) {
                return false;
            }
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        int i;
        int a2 = this.f4075b.a();
        int b2 = this.f4075b.b();
        if (a2 == 0) {
            i = (int) (new Date().getTime() / 1000);
        } else {
            i = a2;
            a2 = b2;
        }
        this.f4075b.b(a2 + 1);
        if ("l".equals(q.c().r().b().c)) {
            i = this.f4075b.b();
        }
        this.f4075b.a(i);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        int a2 = this.f4075b.a();
        String str = q.c().r().b().c;
        if (str.equals("s")) {
            a2 = (int) (new Date().getTime() / 1000);
        } else if (str.equals("l")) {
            a2 = 0;
        }
        this.f4075b.a(a2);
        this.f4075b.b(0);
    }

    public final void a(String str) {
        try {
            JSONArray jSONArray = new JSONArray(this.f4075b.c.getString("cachedImages", "[]"));
            jSONArray.put(str);
            SharedPreferences.Editor edit = this.f4075b.c.edit();
            edit.putString("cachedImages", jSONArray.toString());
            edit.apply();
        } catch (JSONException e2) {
            n.a("Helpshift_ApiData", "storeFile", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        n.a("Helpshift_ApiData", "Updating search indexes.");
        this.f4075b.d();
        i();
        com.helpshift.support.j.b a2 = s.a(new ArrayList(this.g));
        if (a2 != null) {
            this.f4075b.a(a2);
        }
        h();
        n.a("Helpshift_ApiData", "Search index update finished.");
    }

    public final void f() {
        Thread thread = new Thread(new k(this), "HS-load-index");
        thread.setDaemon(true);
        thread.start();
    }

    public final Section b(String str) {
        return this.c.a(str);
    }

    /* access modifiers changed from: package-private */
    public final String c(String str) {
        ArrayList<Section> j = j();
        String str2 = "";
        for (int i = 0; i < j.size(); i++) {
            Section section = j.get(i);
            if (section.f3839a.equals(str)) {
                str2 = section.c;
            }
        }
        return str2;
    }

    /* access modifiers changed from: package-private */
    public final void a(List<com.helpshift.p.c.a> list) {
        if (list != null && !list.isEmpty()) {
            List<com.helpshift.p.c.a> list2 = list;
            q.c().B().a(new n(this), list2, q.c().o().a(), this.f4075b.a("domain"), "3", "7.6.3", Build.MODEL, com.helpshift.v.a.f4261a.b(), Build.VERSION.RELEASE);
        }
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        for (String str : this.d.a()) {
            String str2 = "/faqs/" + str + "/";
            q.b().t().a(str2, "");
            b.a.f3833a.f3832b.a(str2);
        }
        q.b().t().a("/faqs/", null);
    }
}
