package com.helpshift.support;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: Section */
final class y implements Parcelable.Creator<Section> {
    y() {
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new Section[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new Section(parcel);
    }
}
