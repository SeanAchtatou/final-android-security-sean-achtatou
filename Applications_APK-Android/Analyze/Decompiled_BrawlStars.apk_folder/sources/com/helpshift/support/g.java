package com.helpshift.support;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;

/* compiled from: FaqTagFilter */
public class g implements Serializable {
    private static final long serialVersionUID = 7526472295622776147L;

    /* renamed from: a  reason: collision with root package name */
    public String f4067a = "undefined";

    /* renamed from: b  reason: collision with root package name */
    public String[] f4068b;

    public g(String str, String[] strArr) {
        if (a.f4069a.contains(str)) {
            this.f4067a = str;
        }
        this.f4068b = strArr;
    }

    public boolean equals(Object obj) {
        if (obj instanceof g) {
            g gVar = (g) obj;
            return this.f4067a.equals(gVar.f4067a) && Arrays.equals(this.f4068b, gVar.f4068b);
        }
    }

    /* compiled from: FaqTagFilter */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public static final HashSet<String> f4069a;

        static {
            HashSet<String> hashSet = new HashSet<>();
            hashSet.add("and");
            hashSet.add("or");
            hashSet.add("not");
            f4069a = hashSet;
        }
    }
}
