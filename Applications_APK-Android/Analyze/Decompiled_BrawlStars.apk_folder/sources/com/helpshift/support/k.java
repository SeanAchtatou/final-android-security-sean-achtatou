package com.helpshift.support;

import com.helpshift.util.n;
import java.io.IOException;

/* compiled from: HSApiData */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f4145a;

    k(h hVar) {
        this.f4145a = hVar;
    }

    public void run() {
        try {
            this.f4145a.f4075b.c();
        } catch (IOException | ClassCastException | ClassNotFoundException e) {
            n.c("Helpshift_ApiData", "Exception while loading index: trying to re-create the index", e);
            this.f4145a.e();
            try {
                this.f4145a.f4075b.c();
            } catch (Exception e2) {
                n.c("Helpshift_ApiData", "Exception caught again, while loading index: ", e2);
            }
        }
    }
}
