package com.helpshift.support;

import android.app.Activity;
import java.util.Map;

/* compiled from: Support */
public final class ag implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f3869a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Map f3870b;

    public ag(Activity activity, Map map) {
        this.f3869a = activity;
        this.f3870b = map;
    }

    public final void run() {
        al.a(this.f3869a, this.f3870b);
    }
}
