package com.helpshift.support;

import android.os.Handler;
import android.os.Message;
import com.helpshift.common.c.b.p;
import com.helpshift.common.h;
import com.helpshift.n.a;
import com.helpshift.s.b;
import com.helpshift.s.c;
import com.helpshift.util.q;
import java.util.HashMap;

/* compiled from: HSApiData */
public class m implements h<a, Integer> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Handler f4174a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ boolean f4175b;
    final /* synthetic */ Handler c;
    final /* synthetic */ String d;
    final /* synthetic */ h e;

    public m(h hVar, Handler handler, boolean z, Handler handler2, String str) {
        this.e = hVar;
        this.f4174a = handler;
        this.f4175b = z;
        this.c = handler2;
        this.d = str;
    }

    public final /* synthetic */ void a(Object obj) {
        a aVar = (a) obj;
        Message obtainMessage = this.f4174a.obtainMessage();
        Faq faq = new Faq(aVar, this.e.c(aVar.d));
        obtainMessage.obj = faq;
        this.f4174a.sendMessage(obtainMessage);
        if (this.f4175b) {
            q.b().g().a(faq);
        } else {
            this.e.d.a(faq);
        }
    }

    public final /* synthetic */ void b(Object obj) {
        Integer num = (Integer) obj;
        Message obtainMessage = this.c.obtainMessage();
        if (p.m.equals(num) || p.n.equals(num)) {
            if (!this.f4175b) {
                this.e.d.a(this.d);
            }
            c cVar = b.a.f3833a.f3832b;
            cVar.a("/faqs/" + this.d + "/");
        }
        HashMap hashMap = new HashMap();
        hashMap.put("status", num);
        obtainMessage.obj = hashMap;
        this.c.sendMessage(obtainMessage);
    }
}
