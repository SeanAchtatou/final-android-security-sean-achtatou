package com.helpshift.support;

import android.content.Context;
import android.content.SharedPreferences;
import com.helpshift.support.j.b;
import com.helpshift.support.k.a.b;
import com.helpshift.util.n;
import com.helpshift.util.r;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/* compiled from: HSStorage */
public final class u {

    /* renamed from: a  reason: collision with root package name */
    static b f4199a;

    /* renamed from: b  reason: collision with root package name */
    Context f4200b;
    public SharedPreferences c;
    private final String d = "fullIndex.db";

    public u(Context context) {
        this.f4200b = context;
        this.c = context.getSharedPreferences("HSJsonData", 0);
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        return this.c.getString(str, "");
    }

    public final boolean b(String str) {
        return this.c.contains(str);
    }

    public final Boolean c(String str) {
        return Boolean.valueOf(this.c.getBoolean(str, false));
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        SharedPreferences.Editor edit = this.c.edit();
        edit.putString(str, str2);
        edit.apply();
    }

    private void a(String str, Integer num) {
        SharedPreferences.Editor edit = this.c.edit();
        edit.putInt(str, num.intValue());
        edit.apply();
    }

    private void a(String str, Boolean bool) {
        SharedPreferences.Editor edit = this.c.edit();
        edit.putBoolean(str, bool.booleanValue());
        edit.apply();
    }

    private void a(String str, Long l) {
        SharedPreferences.Editor edit = this.c.edit();
        edit.putLong(str, l.longValue());
        edit.apply();
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return e("reviewCounter").intValue();
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        a("reviewCounter", Integer.valueOf(i));
    }

    /* access modifiers changed from: protected */
    public final int b() {
        return e("launchReviewCounter").intValue();
    }

    /* access modifiers changed from: protected */
    public final void b(int i) {
        a("launchReviewCounter", Integer.valueOf(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.u.a(java.lang.String, java.lang.Boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.support.u.a(java.lang.String, java.lang.Integer):void
      com.helpshift.support.u.a(java.lang.String, java.lang.Long):void
      com.helpshift.support.u.a(java.lang.String, java.lang.String):void
      com.helpshift.support.u.a(java.lang.String, java.lang.Boolean):void */
    /* access modifiers changed from: protected */
    public final void a(b bVar) {
        ObjectOutputStream objectOutputStream;
        FileOutputStream fileOutputStream;
        f4199a = bVar;
        FileOutputStream fileOutputStream2 = null;
        try {
            fileOutputStream = this.f4200b.openFileOutput("fullIndex.db", 0);
            try {
                objectOutputStream = new ObjectOutputStream(fileOutputStream);
            } catch (Exception e) {
                e = e;
                objectOutputStream = null;
                fileOutputStream2 = fileOutputStream;
                try {
                    n.a("HelpShiftDebug", "store index", e);
                    r.a(fileOutputStream2);
                    r.a(objectOutputStream);
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream = fileOutputStream2;
                    r.a(fileOutputStream);
                    r.a(objectOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                objectOutputStream = null;
                r.a(fileOutputStream);
                r.a(objectOutputStream);
                throw th;
            }
            try {
                objectOutputStream.writeObject(bVar);
                objectOutputStream.flush();
                a("dbFlag", (Boolean) true);
                r.a(fileOutputStream);
            } catch (Exception e2) {
                e = e2;
                fileOutputStream2 = fileOutputStream;
                n.a("HelpShiftDebug", "store index", e);
                r.a(fileOutputStream2);
                r.a(objectOutputStream);
            } catch (Throwable th3) {
                th = th3;
                r.a(fileOutputStream);
                r.a(objectOutputStream);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            objectOutputStream = null;
            n.a("HelpShiftDebug", "store index", e);
            r.a(fileOutputStream2);
            r.a(objectOutputStream);
        } catch (Throwable th4) {
            th = th4;
            fileOutputStream = null;
            objectOutputStream = null;
            r.a(fileOutputStream);
            r.a(objectOutputStream);
            throw th;
        }
        r.a(objectOutputStream);
    }

    /* access modifiers changed from: protected */
    public final void c() throws IOException, ClassCastException, ClassNotFoundException {
        ObjectInputStream objectInputStream;
        FileInputStream fileInputStream;
        Throwable th;
        if (f4199a == null) {
            try {
                fileInputStream = this.f4200b.openFileInput("fullIndex.db");
                try {
                    objectInputStream = new ObjectInputStream(fileInputStream);
                    try {
                        f4199a = (b) objectInputStream.readObject();
                        r.a(fileInputStream);
                        r.a(objectInputStream);
                    } catch (Throwable th2) {
                        th = th2;
                        r.a(fileInputStream);
                        r.a(objectInputStream);
                        throw th;
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    objectInputStream = null;
                    th = th4;
                    r.a(fileInputStream);
                    r.a(objectInputStream);
                    throw th;
                }
            } catch (Throwable th5) {
                objectInputStream = null;
                th = th5;
                fileInputStream = null;
                r.a(fileInputStream);
                r.a(objectInputStream);
                throw th;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.u.a(java.lang.String, java.lang.Boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.support.u.a(java.lang.String, java.lang.Integer):void
      com.helpshift.support.u.a(java.lang.String, java.lang.Long):void
      com.helpshift.support.u.a(java.lang.String, java.lang.String):void
      com.helpshift.support.u.a(java.lang.String, java.lang.Boolean):void */
    /* access modifiers changed from: protected */
    public final void d() {
        f4199a = null;
        b.a.f4148a.a();
        this.f4200b.deleteFile("fullIndex.db");
        a("dbFlag", (Boolean) false);
    }

    /* access modifiers changed from: package-private */
    public final void a(long j) {
        a("lastErrorReportedTime", Long.valueOf(j));
    }

    static boolean e() {
        return f4199a == null;
    }

    public final String d(String str) {
        return this.c.getString(str, "");
    }

    private Integer e(String str) {
        return Integer.valueOf(this.c.getInt(str, 0));
    }
}
