package com.helpshift.support;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: Faq */
final class f implements Parcelable.Creator<Faq> {
    f() {
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new Faq[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new Faq(parcel);
    }
}
