package com.helpshift.support.b;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.helpshift.R;
import com.helpshift.support.Section;
import com.helpshift.support.d.b;
import com.helpshift.support.g;
import com.helpshift.support.i.f;
import com.helpshift.support.m.e;
import com.helpshift.util.x;
import java.util.ArrayList;

/* compiled from: SectionPagerFragment */
public class c extends f implements b {

    /* renamed from: a  reason: collision with root package name */
    private TabLayout f3897a;

    /* renamed from: b  reason: collision with root package name */
    private FrameLayout f3898b;
    private int c = 0;

    public final boolean h_() {
        return true;
    }

    public static c a(Bundle bundle) {
        c cVar = new c();
        cVar.setArguments(bundle);
        return cVar;
    }

    public final com.helpshift.support.d.c a() {
        return ((b) getParentFragment()).a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__section_pager_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        ArrayList parcelableArrayList = getArguments().getParcelableArrayList("sections");
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.section_pager);
        viewPager.setAdapter(new b(getChildFragmentManager(), parcelableArrayList, (g) getArguments().getSerializable("withTagsMatching")));
        this.f3897a = (TabLayout) view.findViewById(R.id.pager_tabs);
        int i = 0;
        View childAt = this.f3897a.getChildAt(0);
        int i2 = this.c;
        childAt.setPadding(i2, 0, i2, 0);
        this.f3897a.setupWithViewPager(viewPager);
        String string = getArguments().getString("sectionPublishId");
        int i3 = 0;
        while (true) {
            if (i3 >= parcelableArrayList.size()) {
                break;
            } else if (((Section) parcelableArrayList.get(i3)).c.equals(string)) {
                i = i3;
                break;
            } else {
                i3++;
            }
        }
        viewPager.setCurrentItem(i);
        this.f3898b = (FrameLayout) view.findViewById(R.id.view_pager_container);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.util.x.a(android.content.Context, float):float
     arg types: [android.content.Context, int]
     candidates:
      com.helpshift.util.x.a(android.content.Context, int):int
      com.helpshift.util.x.a(android.content.Context, android.graphics.drawable.Drawable):void
      com.helpshift.util.x.a(android.content.Context, float):float */
    public void onResume() {
        super.onResume();
        a(false);
        if (Build.VERSION.SDK_INT >= 21) {
            this.f3897a.setElevation(x.a(getContext(), 4.0f));
        } else {
            this.f3898b.setForeground(getResources().getDrawable(R.drawable.hs__actionbar_compat_shadow));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.util.x.a(android.content.Context, float):float
     arg types: [android.content.Context, int]
     candidates:
      com.helpshift.util.x.a(android.content.Context, int):int
      com.helpshift.util.x.a(android.content.Context, android.graphics.drawable.Drawable):void
      com.helpshift.util.x.a(android.content.Context, float):float */
    public void onAttach(Context context) {
        super.onAttach(context);
        this.c = (int) x.a(context, 48.0f);
    }

    public void onStop() {
        a(true);
        super.onStop();
    }

    private void a(boolean z) {
        com.helpshift.support.i.x a2 = e.a(this);
        if (a2 != null) {
            a2.a(z);
        }
    }

    public void onDestroyView() {
        this.f3897a = null;
        this.f3898b = null;
        super.onDestroyView();
    }
}
