package com.helpshift.support.b;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.helpshift.support.Section;
import com.helpshift.support.g;
import com.helpshift.util.n;
import java.util.List;

/* compiled from: SectionPagerAdapter */
public class b extends FragmentStatePagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private g f3895a;

    /* renamed from: b  reason: collision with root package name */
    private List<Section> f3896b;

    public b(FragmentManager fragmentManager, List<Section> list, g gVar) {
        super(fragmentManager);
        this.f3896b = list;
        this.f3895a = gVar;
    }

    public Fragment getItem(int i) {
        Bundle bundle = new Bundle();
        bundle.putString("sectionPublishId", this.f3896b.get(i).c);
        bundle.putSerializable("withTagsMatching", this.f3895a);
        return com.helpshift.support.i.g.a(bundle);
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
        try {
            super.restoreState(parcelable, classLoader);
        } catch (Exception e) {
            n.c("Helpshift_SectionPager", "Exception in restoreState: ", e);
        }
    }

    public int getCount() {
        return this.f3896b.size();
    }

    public CharSequence getPageTitle(int i) {
        return this.f3896b.get(i).f3840b;
    }
}
