package com.helpshift.support.b;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.support.Faq;
import com.helpshift.support.Section;
import com.helpshift.support.d.c;
import com.helpshift.support.g;
import com.helpshift.support.h;
import com.helpshift.support.i.f;
import com.helpshift.support.i.s;
import com.helpshift.support.i.x;
import com.helpshift.support.m.e;
import com.helpshift.support.m.k;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: FaqFragment */
public class a extends f implements com.helpshift.support.d.b {

    /* renamed from: a  reason: collision with root package name */
    public int f3891a = 0;

    /* renamed from: b  reason: collision with root package name */
    boolean f3892b;
    public g c;
    public h d;

    public final boolean h_() {
        return true;
    }

    public static a a(Bundle bundle) {
        a aVar = new a();
        aVar.setArguments(bundle);
        return aVar;
    }

    public final c a() {
        return ((com.helpshift.support.d.b) getParentFragment()).a();
    }

    public final void a(int i) {
        com.helpshift.support.i.b bVar = (com.helpshift.support.i.b) getParentFragment();
        x xVar = bVar != null ? (x) bVar.getParentFragment() : null;
        if (xVar != null) {
            if (i == 1) {
                bVar.b(true);
                bVar.d();
            } else {
                bVar.b(false);
                bVar.a(false);
            }
            xVar.e.setVisibility(8);
            xVar.f.setVisibility(8);
            xVar.g.setVisibility(8);
            if (i == 0) {
                xVar.f.setVisibility(0);
            } else if (i == 1) {
            } else {
                if (i == 2) {
                    xVar.e.setVisibility(0);
                } else if (i == 3) {
                    xVar.g.setVisibility(0);
                }
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.c = (g) arguments.getSerializable("withTagsMatching");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__faq_fragment, viewGroup, false);
    }

    public void onResume() {
        super.onResume();
        b(getString(R.string.hs__help_header));
        if (this.f3891a == 0) {
            a(0);
        }
        this.d.b(new b(this), new C0142a(this), this.c);
        if (!this.j) {
            q.c().k().a(com.helpshift.b.b.SUPPORT_LAUNCH);
        }
    }

    public void onDestroyView() {
        k.a(getView());
        super.onDestroyView();
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.d = new h(context);
    }

    public void onStop() {
        super.onStop();
        a(1);
    }

    /* compiled from: FaqFragment */
    public static class b extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<a> f3894a;

        public b(a aVar) {
            this.f3894a = new WeakReference<>(aVar);
        }

        public void handleMessage(Message message) {
            a aVar = this.f3894a.get();
            if (aVar != null && aVar.getHost() != null && !aVar.isDetached()) {
                ArrayList arrayList = (ArrayList) message.obj;
                int i = message.what;
                if (arrayList != null) {
                    ArrayList arrayList2 = new ArrayList();
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        Section section = (Section) it.next();
                        ArrayList<Faq> a2 = aVar.d.a(section.c, aVar.c);
                        if (a2 != null && !a2.isEmpty()) {
                            arrayList2.add(section);
                        }
                    }
                    aVar.f3891a = arrayList2.size();
                    arrayList = arrayList2;
                }
                if (i == com.helpshift.support.c.a.f3899a) {
                    if (aVar.f3891a != 0) {
                        aVar.a(1);
                        aVar.a(aVar, arrayList);
                    }
                } else if (i == com.helpshift.support.c.a.d) {
                    if (aVar.f3891a == 0) {
                        aVar.a(2);
                    } else {
                        aVar.f3892b = true;
                        aVar.a(1);
                        aVar.a(aVar, arrayList);
                    }
                } else if (i == com.helpshift.support.c.a.c && aVar.f3891a == 0) {
                    aVar.a(2);
                }
                n.a("Helpshift_FaqFragment", "Faq loaded with " + aVar.f3891a + " sections");
            }
        }
    }

    /* renamed from: com.helpshift.support.b.a$a  reason: collision with other inner class name */
    /* compiled from: FaqFragment */
    public static class C0142a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<a> f3893a;

        public C0142a(a aVar) {
            this.f3893a = new WeakReference<>(aVar);
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v7, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: com.helpshift.common.exception.a} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void handleMessage(android.os.Message r5) {
            /*
                r4 = this;
                java.lang.ref.WeakReference<com.helpshift.support.b.a> r0 = r4.f3893a
                java.lang.Object r0 = r0.get()
                com.helpshift.support.b.a r0 = (com.helpshift.support.b.a) r0
                if (r0 == 0) goto L_0x0042
                java.lang.Object r1 = r0.getHost()
                if (r1 == 0) goto L_0x0042
                boolean r1 = r0.isDetached()
                if (r1 == 0) goto L_0x0017
                goto L_0x0042
            L_0x0017:
                r1 = 0
                int r2 = r5.what
                java.lang.Object r3 = r5.obj
                boolean r3 = r3 instanceof com.helpshift.common.exception.a
                if (r3 == 0) goto L_0x0025
                java.lang.Object r5 = r5.obj
                r1 = r5
                com.helpshift.common.exception.a r1 = (com.helpshift.common.exception.a) r1
            L_0x0025:
                int r5 = r0.f3891a
                if (r5 != 0) goto L_0x003e
                int r5 = com.helpshift.support.c.a.f
                if (r2 != r5) goto L_0x0032
                r5 = 2
                r0.a(r5)
                goto L_0x0042
            L_0x0032:
                r5 = 3
                r0.a(r5)
                android.view.View r5 = r0.getView()
                com.helpshift.support.m.k.a(r1, r5)
                goto L_0x0042
            L_0x003e:
                r5 = 1
                r0.a(r5)
            L_0x0042:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.b.a.C0142a.handleMessage(android.os.Message):void");
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(a aVar, ArrayList<Section> arrayList) {
        x a2 = e.a(this);
        if (a2 != null) {
            a2.c();
        }
        if (aVar.r().findFragmentById(R.id.faq_fragment_container) == null || this.f3892b) {
            ArrayList<Section> a3 = aVar.d.a(arrayList, aVar.c);
            if (a3.size() == 1) {
                Bundle bundle = new Bundle();
                bundle.putString("sectionPublishId", a3.get(0).c);
                bundle.putSerializable("withTagsMatching", getArguments().getSerializable("withTagsMatching"));
                try {
                    e.a(aVar.r(), R.id.faq_fragment_container, com.helpshift.support.i.g.a(bundle), null, null, false, this.f3892b);
                    this.f3892b = false;
                } catch (IllegalStateException unused) {
                }
            } else {
                Bundle bundle2 = new Bundle();
                bundle2.putParcelableArrayList("sections", a3);
                bundle2.putSerializable("withTagsMatching", getArguments().getSerializable("withTagsMatching"));
                s sVar = new s();
                sVar.setArguments(bundle2);
                e.a(aVar.r(), R.id.faq_fragment_container, sVar, null, null, false, this.f3892b);
                this.f3892b = false;
            }
        }
    }
}
