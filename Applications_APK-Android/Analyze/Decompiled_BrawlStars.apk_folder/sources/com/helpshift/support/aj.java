package com.helpshift.support;

import android.app.Activity;
import java.util.Map;

/* compiled from: Support */
public final class aj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f3875a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Map f3876b;

    public aj(Activity activity, Map map) {
        this.f3875a = activity;
        this.f3876b = map;
    }

    public final void run() {
        al.b(this.f3875a, this.f3876b);
    }
}
