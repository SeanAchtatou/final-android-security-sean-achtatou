package com.helpshift.support;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import com.helpshift.a;
import com.helpshift.k;
import com.helpshift.support.al;
import java.util.Map;

/* compiled from: Support */
public class z implements a.C0131a {

    /* compiled from: Support */
    public interface a extends com.helpshift.l.b {
    }

    /* compiled from: Support */
    public static class b extends al.a {
    }

    /* compiled from: Support */
    public static class c {

        /* renamed from: a  reason: collision with root package name */
        public static final z f4224a = new z((byte) 0);
    }

    /* synthetic */ z(byte b2) {
        this();
    }

    private z() {
    }

    public final void a(Application application, String str, String str2, String str3, Map<String, Object> map) {
        al.a(application, str, str2, str3, map);
    }

    public final void b(Application application, String str, String str2, String str3, Map<String, Object> map) {
        al.a(application, map);
    }

    public final void a(String str, String str2) {
        al.a(str, str2);
    }

    public final void a(Context context, String str) {
        al.a(context, str);
    }

    public final void a(Context context, Intent intent) {
        al.a(context, intent);
    }

    public final boolean a(k kVar) {
        return al.a(kVar);
    }

    public final boolean a() {
        return al.a();
    }
}
