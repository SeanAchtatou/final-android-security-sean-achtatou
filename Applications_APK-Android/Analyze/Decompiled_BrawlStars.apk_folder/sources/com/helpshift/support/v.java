package com.helpshift.support;

import java.util.HashMap;
import java.util.Map;

/* compiled from: Metadata */
public class v {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, Object> f4201a;

    /* renamed from: b  reason: collision with root package name */
    private String[] f4202b;

    public v(Map<String, Object> map) {
        this(map, null);
    }

    public v(Map<String, Object> map, String[] strArr) {
        if (map != null) {
            this.f4201a = map;
        }
        if (strArr != null && strArr.length > 0) {
            this.f4202b = strArr;
        }
    }

    public final Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        Map<String, Object> map = this.f4201a;
        if (map != null) {
            hashMap.putAll(map);
        }
        String[] strArr = this.f4202b;
        if (strArr != null) {
            hashMap.put("hs-tags", strArr);
        }
        return hashMap;
    }
}
