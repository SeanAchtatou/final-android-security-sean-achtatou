package com.helpshift.support;

import android.content.Context;
import com.helpshift.support.al;
import com.helpshift.util.q;

/* compiled from: ContactUsFilter */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    static Integer f3901a;

    /* renamed from: b  reason: collision with root package name */
    private static h f3902b;

    /* compiled from: ContactUsFilter */
    public enum a {
        ACTION_BAR,
        SEARCH_FOOTER,
        QUESTION_FOOTER,
        QUESTION_ACTION_BAR,
        SEARCH_RESULT_ACTIVITY_HEADER
    }

    public static void a(Context context) {
        if (f3902b == null) {
            f3902b = new h(context);
            f3901a = Integer.valueOf(q.c().r().e().a());
        }
    }

    public static boolean a(a aVar) {
        if (aVar == a.SEARCH_RESULT_ACTIVITY_HEADER || al.a.f3882b.equals(f3901a)) {
            return false;
        }
        if (!al.a.f3881a.equals(f3901a) && aVar != a.QUESTION_FOOTER) {
            if (aVar == a.ACTION_BAR) {
                if (q.c().b() != null) {
                    return true;
                }
                return false;
            } else if (!al.a.c.equals(f3901a) && al.a.d.equals(f3901a)) {
                int i = e.f3905a[aVar.ordinal()];
                if (i == 1) {
                    return false;
                }
                if (i == 2 && q.c().b() == null) {
                    return false;
                }
                return true;
            }
        }
        return true;
    }
}
