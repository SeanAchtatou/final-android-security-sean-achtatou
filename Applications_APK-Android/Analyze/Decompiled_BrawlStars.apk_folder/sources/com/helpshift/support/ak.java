package com.helpshift.support;

import android.content.Context;
import android.content.Intent;
import com.helpshift.f.f;
import com.helpshift.p.c.a;
import com.helpshift.util.b;
import com.helpshift.util.n;
import com.helpshift.util.p;
import com.helpshift.util.q;
import com.helpshift.util.z;
import java.util.List;

/* compiled from: SupportAppLifeCycleListener */
public class ak implements f {

    /* renamed from: a  reason: collision with root package name */
    h f3877a = null;

    /* renamed from: b  reason: collision with root package name */
    u f3878b = null;

    public final void a(Context context) {
        List<a> a2;
        boolean z = true;
        com.helpshift.e.a.a(true);
        if (this.f3877a == null) {
            this.f3877a = new h(context);
            this.f3878b = this.f3877a.f4075b;
        }
        this.f3877a.c();
        if (this.f3877a.b()) {
            Intent intent = new Intent(context, HSReview.class);
            intent.setFlags(268435456);
            context.startActivity(intent);
        }
        boolean g = b.g(context);
        com.helpshift.i.a.a c = q.c().a().c();
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        if (g || Math.abs(currentTimeMillis - c.a().longValue()) >= c.j()) {
            q.c().h();
        }
        q.c().i();
        q.c().n();
        q.c().l();
        q.c().A();
        boolean a3 = p.a(context);
        synchronized (this) {
            if (a3) {
                if (com.helpshift.x.a.f4350a) {
                    long longValue = Long.valueOf(this.f3878b.c.getLong("lastErrorReportedTime", 0)).longValue();
                    long a4 = z.a(Float.valueOf(q.b().t().a()));
                    if (a4 - longValue <= 86400000) {
                        z = false;
                    }
                    if (z && n.c() > 0 && (a2 = n.a()) != null && !a2.isEmpty()) {
                        this.f3878b.a(a4);
                        this.f3877a.a(a2);
                    }
                }
            }
        }
    }

    public final void b(Context context) {
        com.helpshift.e.a.a(false);
        q.c().s().b();
        q.c().z();
    }
}
