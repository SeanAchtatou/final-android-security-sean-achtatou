package com.helpshift.support;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.helpshift.util.q;

/* compiled from: HSReviewFragment */
class p implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f4192a;

    p(o oVar) {
        this.f4192a = oVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (TextUtils.isEmpty(this.f4192a.f4191a)) {
            this.f4192a.f4191a = q.c().r().c("reviewUrl");
        }
        o oVar = this.f4192a;
        oVar.f4191a = oVar.f4191a.trim();
        if (!TextUtils.isEmpty(this.f4192a.f4191a)) {
            o oVar2 = this.f4192a;
            String str = oVar2.f4191a;
            if (!TextUtils.isEmpty(str)) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(str.trim()));
                if (intent.resolveActivity(oVar2.getContext().getPackageManager()) != null) {
                    oVar2.getContext().startActivity(intent);
                }
            }
        }
        o.a("reviewed");
        o.a(0);
    }
}
