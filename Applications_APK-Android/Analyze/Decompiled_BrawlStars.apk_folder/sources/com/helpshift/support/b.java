package com.helpshift.support;

import com.helpshift.support.g;
import com.helpshift.support.h.g;
import com.helpshift.support.z;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ApiConfig */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private final Integer f3887a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f3888b;
    private final boolean c;
    private final boolean d;
    private final String e;
    private final boolean f;
    private final boolean g;
    private final boolean h;
    private final List<g> i;
    private final g j;
    private final v k;
    private final int l;
    private final boolean m;
    private final boolean n;
    private final Map<String, String[]> o;
    private final Map<String, Object> p;

    b(Integer num, boolean z, boolean z2, boolean z3, String str, boolean z4, boolean z5, boolean z6, List<g> list, g gVar, v vVar, int i2, boolean z7, boolean z8, Map<String, String[]> map, Map<String, Object> map2) {
        this.f3887a = num;
        this.f3888b = z;
        this.c = z2;
        this.d = z3;
        this.e = str;
        this.f = z4;
        this.g = z5;
        this.h = z6;
        this.i = list;
        this.j = gVar;
        this.k = vVar;
        this.l = i2;
        this.m = z7;
        this.n = z8;
        this.o = map;
        this.p = map2;
    }

    public final Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("enableContactUs", this.f3887a);
        hashMap.put("gotoConversationAfterContactUs", Boolean.valueOf(this.f3888b));
        hashMap.put("requireEmail", Boolean.valueOf(this.c));
        hashMap.put("hideNameAndEmail", Boolean.valueOf(this.d));
        hashMap.put("enableFullPrivacy", Boolean.valueOf(this.f));
        hashMap.put("showSearchOnNewConversation", Boolean.valueOf(this.g));
        hashMap.put("showConversationResolutionQuestion", Boolean.valueOf(this.h));
        hashMap.put("showConversationInfoScreen", Boolean.valueOf(this.m));
        hashMap.put("enableTypingIndicator", Boolean.valueOf(this.n));
        String str = this.e;
        if (str != null && str.length() > 0) {
            hashMap.put("conversationPrefillText", this.e);
        }
        List<g> list = this.i;
        if (list != null) {
            hashMap.put("customContactUsFlows", list);
        }
        g gVar = this.j;
        if (gVar != null) {
            HashMap hashMap2 = null;
            if (gVar.f4067a != null && g.a.f4069a.contains(gVar.f4067a) && gVar.f4068b != null && gVar.f4068b.length > 0) {
                hashMap2 = new HashMap();
                hashMap2.put("operator", gVar.f4067a);
                hashMap2.put("tags", gVar.f4068b);
            }
            if (hashMap2 != null) {
                hashMap.put("withTagsMatching", hashMap2);
            }
        }
        v vVar = this.k;
        if (vVar != null) {
            Map<String, Object> a2 = vVar.a();
            if (a2.size() > 0) {
                hashMap.put("hs-custom-metadata", a2);
            }
        }
        Map<String, String[]> map = this.o;
        if (map != null) {
            hashMap.put("hs-custom-issue-field", map);
        }
        int i2 = this.l;
        if (i2 != 0) {
            hashMap.put("toolbarId", Integer.valueOf(i2));
        }
        Map<String, Object> map2 = this.p;
        if (map2 != null) {
            for (String next : map2.keySet()) {
                if (this.p.get(next) != null) {
                    hashMap.put(next, this.p.get(next));
                }
            }
        }
        return hashMap;
    }

    /* compiled from: ApiConfig */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public boolean f3889a = false;

        /* renamed from: b  reason: collision with root package name */
        public boolean f3890b = false;
        public g c;
        public v d;
        public Map<String, Object> e;
        public Map<String, String[]> f;
        private Integer g = z.b.f3881a;
        private boolean h = false;
        private boolean i = false;
        private boolean j = false;
        private String k;
        private boolean l = false;
        private List<com.helpshift.support.h.g> m;
        private int n;
        private boolean o = false;
        private boolean p = false;
        private boolean q = false;

        public final a a(Integer num) {
            if (num != null && z.b.e.contains(num)) {
                this.g = num;
            }
            return this;
        }

        @Deprecated
        public final a a(boolean z) {
            this.h = false;
            this.p = true;
            return this;
        }

        public final b a() {
            return new b(this.g, this.h, this.i, this.j, this.k, this.l, this.f3889a, this.f3890b, this.m, this.c, this.d, this.n, this.o, this.q, this.f, this.e);
        }
    }
}
