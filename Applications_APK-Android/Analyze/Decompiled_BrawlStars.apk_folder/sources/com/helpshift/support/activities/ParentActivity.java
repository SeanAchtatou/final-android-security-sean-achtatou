package com.helpshift.support.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.helpshift.R;
import com.helpshift.activities.MainActivity;
import com.helpshift.support.i.x;
import com.helpshift.util.b;
import com.helpshift.util.q;
import java.util.List;

public class ParentActivity extends MainActivity {

    /* renamed from: a  reason: collision with root package name */
    FragmentManager f3864a;

    /* renamed from: b  reason: collision with root package name */
    private Toolbar f3865b;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!q.f4255a.get()) {
            Intent b2 = b.b(getApplicationContext(), getPackageName());
            if (b2 != null) {
                finish();
                startActivity(b2);
                return;
            }
            return;
        }
        setContentView(R.layout.hs__parent_activity);
        this.f3865b = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.f3865b);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
        this.f3864a = getSupportFragmentManager();
        if (bundle == null) {
            FragmentTransaction beginTransaction = this.f3864a.beginTransaction();
            beginTransaction.add(R.id.support_fragment_container, x.a(getIntent().getExtras()));
            beginTransaction.commit();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007a, code lost:
        r4 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onBackPressed() {
        /*
            r8 = this;
            android.support.v4.app.FragmentManager r0 = r8.f3864a
            java.util.List r0 = r0.getFragments()
            if (r0 == 0) goto L_0x009d
            java.util.Iterator r0 = r0.iterator()
        L_0x000c:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x009d
            java.lang.Object r1 = r0.next()
            android.support.v4.app.Fragment r1 = (android.support.v4.app.Fragment) r1
            if (r1 == 0) goto L_0x000c
            boolean r2 = r1.isVisible()
            if (r2 == 0) goto L_0x000c
            boolean r2 = r1 instanceof com.helpshift.support.i.x
            if (r2 == 0) goto L_0x000c
            r2 = r1
            com.helpshift.support.i.x r2 = (com.helpshift.support.i.x) r2
            android.support.v4.app.FragmentManager r2 = r2.r()
            java.util.List r2 = r2.getFragments()
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x008c
            java.util.Iterator r2 = r2.iterator()
        L_0x0037:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x008c
            java.lang.Object r5 = r2.next()
            android.support.v4.app.Fragment r5 = (android.support.v4.app.Fragment) r5
            if (r5 == 0) goto L_0x0037
            boolean r6 = r5.isVisible()
            if (r6 == 0) goto L_0x0037
            boolean r6 = r5 instanceof com.helpshift.support.i.b
            if (r6 != 0) goto L_0x006d
            boolean r6 = r5 instanceof com.helpshift.support.f.b
            if (r6 == 0) goto L_0x0054
            goto L_0x006d
        L_0x0054:
            boolean r6 = r5 instanceof com.helpshift.support.i.i
            if (r6 == 0) goto L_0x0037
            com.helpshift.support.i.i r5 = (com.helpshift.support.i.i) r5
            com.helpshift.support.i.i$a r2 = r5.c
            com.helpshift.support.i.i$a r3 = com.helpshift.support.i.i.a.GALLERY_APP
            if (r2 != r3) goto L_0x008c
            com.helpshift.b r2 = com.helpshift.util.q.c()
            r2.x()
            com.helpshift.j.d.d r2 = r5.f4103a
            com.helpshift.common.c.a.a(r2)
            goto L_0x008c
        L_0x006d:
            android.support.v4.app.FragmentManager r6 = r5.getChildFragmentManager()
            int r7 = r6.getBackStackEntryCount()
            if (r7 <= 0) goto L_0x007c
            r6.popBackStack()
        L_0x007a:
            r4 = 1
            goto L_0x008c
        L_0x007c:
            boolean r6 = r5 instanceof com.helpshift.support.f.d
            if (r6 == 0) goto L_0x0037
            com.helpshift.support.f.d r5 = (com.helpshift.support.f.d) r5
            boolean r2 = r5.h()
            if (r2 == 0) goto L_0x0089
            goto L_0x007a
        L_0x0089:
            r5.j()
        L_0x008c:
            if (r4 == 0) goto L_0x008f
            return
        L_0x008f:
            android.support.v4.app.FragmentManager r1 = r1.getChildFragmentManager()
            int r2 = r1.getBackStackEntryCount()
            if (r2 <= 0) goto L_0x000c
            r1.popBackStack()
            return
        L_0x009d:
            super.onBackPressed()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.activities.ParentActivity.onBackPressed():void");
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        List<Fragment> fragments = this.f3864a.getFragments();
        if (fragments != null) {
            for (Fragment next : fragments) {
                if (next instanceof x) {
                    x xVar = (x) next;
                    Bundle extras = intent.getExtras();
                    if (xVar.f4133a) {
                        xVar.d.b(extras);
                    } else {
                        xVar.q = extras;
                    }
                    xVar.p = !xVar.f4133a;
                }
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }

    public final void a(int i) {
        if (this.f3865b != null && Build.VERSION.SDK_INT >= 19) {
            this.f3865b.setImportantForAccessibility(i);
        }
    }
}
