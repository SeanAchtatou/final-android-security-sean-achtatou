package com.helpshift.support.m;

import com.helpshift.util.c;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: HSTransliterator */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f4184a = false;

    /* renamed from: b  reason: collision with root package name */
    private static f f4185b;

    public static boolean a() {
        return f4184a;
    }

    public static void b() {
        if (!f4184a) {
            try {
                JSONObject jSONObject = new JSONObject(c.a(q.a(), "hs__data")).getJSONObject("HSCharacters");
                if (jSONObject != null) {
                    f4185b = new f(jSONObject);
                    f4184a = true;
                }
            } catch (JSONException e) {
                n.b("Helpshift_Transliteratr", "Error reading json : ", e);
            }
        }
    }

    public static void c() {
        f4185b = null;
        f4184a = false;
    }

    public static String a(String str) {
        String str2;
        if (!f4184a) {
            b();
        }
        if (str == null || str.length() == 0) {
            return "";
        }
        int i = 0;
        while (i < str.length() && str.charAt(i) <= 128) {
            if (i >= str.length()) {
                return str;
            }
            i++;
        }
        char[] charArray = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char c : charArray) {
            if (c < 128) {
                sb.append(c);
            } else {
                int i2 = c >> 8;
                char c2 = c & 255;
                f fVar = f4185b;
                if (fVar != null) {
                    List list = fVar.f4183a.get(String.valueOf(i2));
                    if (list != null && c2 < list.size() && ((String) list.get(c2)).length() > 0) {
                        List list2 = f4185b.f4183a.get(String.valueOf(i2));
                        if (list2 == null) {
                            str2 = "";
                        } else {
                            str2 = (String) list2.get(c2);
                        }
                        sb.append(str2);
                    }
                }
                sb.append("");
            }
        }
        return sb.toString();
    }
}
