package com.helpshift.support.m;

import android.graphics.ImageDecoder;
import android.util.Size;
import com.helpshift.util.s;

/* compiled from: AttachmentUtil */
final class c implements ImageDecoder.OnHeaderDecodedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f4178a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ boolean f4179b;

    c(int i, boolean z) {
        this.f4178a = i;
        this.f4179b = z;
    }

    public final void onHeaderDecoded(ImageDecoder imageDecoder, ImageDecoder.ImageInfo imageInfo, ImageDecoder.Source source) {
        Size size = imageInfo.getSize();
        int width = size.getWidth();
        int height = size.getHeight();
        int i = this.f4178a;
        int i2 = 4;
        if (i > 0 && width > 0 && height > 0) {
            int a2 = s.a(width, height, this.f4178a, s.a(width, height, i));
            if (a2 < 4) {
                a2++;
            }
            i2 = a2;
        }
        if (!this.f4179b) {
            imageDecoder.setAllocator(1);
        }
        imageDecoder.setTargetSampleSize(i2);
    }
}
