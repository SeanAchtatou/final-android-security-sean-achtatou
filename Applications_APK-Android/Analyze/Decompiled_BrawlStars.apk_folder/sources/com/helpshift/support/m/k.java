package com.helpshift.support.m;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import com.helpshift.R;
import com.helpshift.common.exception.a;
import com.helpshift.common.exception.b;
import com.helpshift.util.q;
import com.helpshift.views.c;
import com.helpshift.views.d;
import java.util.WeakHashMap;

/* compiled from: SnackbarUtil */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private static WeakHashMap<View, Snackbar> f4188a = new WeakHashMap<>();

    public static void a(View view, CharSequence charSequence, int i) {
        if (charSequence != null && charSequence.length() != 0) {
            if (view != null) {
                Snackbar a2 = c.a(view, charSequence, i);
                a2.show();
                f4188a.put(view, a2);
                return;
            }
            d.a(q.a(), charSequence, i == -1 ? 0 : 1).show();
        }
    }

    public static void a(View view, int i, int i2) {
        CharSequence charSequence;
        if (view != null) {
            charSequence = view.getResources().getText(i);
        } else {
            charSequence = q.a().getResources().getText(i);
        }
        a(view, charSequence, i2);
    }

    public static void a(int i, View view) {
        Context context;
        int i2;
        if (i != -1) {
            if (view == null) {
                context = q.a();
            } else {
                context = view.getContext();
            }
            if (i == 102) {
                i2 = R.string.hs__invalid_faq_publish_id_error;
            } else if (i == 103) {
                i2 = R.string.hs__invalid_section_publish_id_error;
            } else {
                i2 = R.string.hs__network_error_msg;
            }
            a(view, context.getResources().getString(i2), -1);
        }
    }

    public static void a(a aVar, View view) {
        Context context;
        int i;
        if (view == null) {
            context = q.a();
        } else {
            context = view.getContext();
        }
        if (aVar == b.NO_CONNECTION) {
            i = R.string.hs__network_unavailable_msg;
        } else if (aVar == b.UNKNOWN_HOST) {
            i = R.string.hs__could_not_reach_support_msg;
        } else if (aVar == b.SSL_PEER_UNVERIFIED) {
            i = R.string.hs__ssl_peer_unverified_error;
        } else if (aVar == b.SSL_HANDSHAKE) {
            i = R.string.hs__ssl_handshake_error;
        } else if (aVar == b.CONTENT_NOT_FOUND) {
            i = R.string.hs__data_not_found_msg;
        } else if (aVar == b.SCREENSHOT_UPLOAD_ERROR) {
            i = R.string.hs__screenshot_upload_error_msg;
        } else if (aVar == com.helpshift.common.exception.d.NO_APPS_FOR_OPENING_ATTACHMENT) {
            i = R.string.hs__could_not_open_attachment_msg;
        } else if (aVar == com.helpshift.common.exception.d.FILE_NOT_FOUND) {
            i = R.string.hs__file_not_found_msg;
        } else {
            i = R.string.hs__network_error_msg;
        }
        a(view, context.getResources().getString(i), -1);
    }

    public static void a(View view) {
        if (view != null) {
            Snackbar snackbar = f4188a.get(view);
            if (snackbar != null && snackbar.isShown()) {
                snackbar.dismiss();
            }
            f4188a.remove(view);
        }
    }
}
