package com.helpshift.support.m;

import android.support.v4.app.Fragment;
import android.view.View;

/* compiled from: PermissionUtil */
final class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Fragment f4186a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String[] f4187b;
    final /* synthetic */ int c;

    j(Fragment fragment, String[] strArr, int i) {
        this.f4186a = fragment;
        this.f4187b = strArr;
        this.c = i;
    }

    public final void onClick(View view) {
        this.f4186a.requestPermissions(this.f4187b, this.c);
    }
}
