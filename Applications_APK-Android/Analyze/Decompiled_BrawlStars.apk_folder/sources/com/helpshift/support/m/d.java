package com.helpshift.support.m;

import com.helpshift.support.b;
import com.helpshift.support.z;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ConfigUtil */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<String, Object> f4180a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    private static final Map<String, Object> f4181b = new HashMap();

    public static Map<String, Object> a() {
        if (f4180a.size() == 0) {
            f4180a.put("enableContactUs", z.b.f3881a);
            f4180a.put("gotoConversationAfterContactUs", false);
            f4180a.put("showSearchOnNewConversation", false);
            f4180a.put("requireEmail", false);
            f4180a.put("hideNameAndEmail", false);
            f4180a.put("enableFullPrivacy", false);
            f4180a.put("showConversationResolutionQuestion", false);
            f4180a.put("showConversationInfoScreen", false);
            f4180a.put("enableTypingIndicator", false);
        }
        return f4180a;
    }

    public static Map<String, Object> b() {
        if (f4181b.size() == 0) {
            f4181b.put("enableLogging", false);
            f4181b.put("disableHelpshiftBranding", false);
            f4181b.put("disableAppLaunchEvent", false);
            f4181b.put("enableInAppNotification", true);
            f4181b.put("enableDefaultFallbackLanguage", true);
            f4181b.put("disableAnimations", false);
            f4181b.put("font", null);
            f4181b.put("supportNotificationChannelId", null);
            f4181b.put("screenOrientation", -1);
            f4181b.put("manualLifecycleTracking", false);
        }
        return f4181b;
    }

    public static Map<String, Object> a(b bVar) {
        HashMap hashMap = new HashMap();
        if (bVar != null) {
            hashMap.putAll(bVar.a());
        }
        return hashMap;
    }
}
