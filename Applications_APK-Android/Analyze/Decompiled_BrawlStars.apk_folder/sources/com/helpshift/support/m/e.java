package com.helpshift.support.m;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import com.helpshift.R;
import com.helpshift.s.b;
import com.helpshift.support.i.b;
import com.helpshift.support.i.l;
import com.helpshift.support.i.x;
import java.util.Iterator;
import java.util.List;

/* compiled from: FragmentUtil */
public class e {
    public static void a(FragmentManager fragmentManager, int i, Fragment fragment, String str, String str2, boolean z, boolean z2) {
        b(fragmentManager, i, fragment, str, str2, false, z2);
    }

    public static void a(FragmentManager fragmentManager, int i, Fragment fragment, String str, boolean z) {
        b(fragmentManager, i, fragment, str, fragment.getClass().getName(), false, false);
    }

    public static void b(FragmentManager fragmentManager, int i, Fragment fragment, String str, boolean z) {
        b(fragmentManager, i, fragment, null, null, z, false);
    }

    public static void a(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss();
    }

    public static <T extends Fragment> T a(FragmentManager fragmentManager, Class cls) {
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments == null) {
            return null;
        }
        Iterator<Fragment> it = fragments.iterator();
        while (it.hasNext()) {
            T t = (Fragment) it.next();
            if (cls.isInstance(t)) {
                return t;
            }
        }
        return null;
    }

    public static b a(FragmentManager fragmentManager) {
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments == null) {
            return null;
        }
        for (int size = fragments.size() - 1; size >= 0; size--) {
            Fragment fragment = fragments.get(size);
            if (fragment != null && (fragment instanceof b)) {
                return (b) fragment;
            }
        }
        return null;
    }

    public static l b(FragmentManager fragmentManager) {
        return (l) a(fragmentManager, l.class);
    }

    public static x a(Fragment fragment) {
        while (!(fragment instanceof x)) {
            fragment = fragment.getParentFragment();
            if (fragment == null) {
                return null;
            }
            if (fragment instanceof x) {
                return (x) fragment;
            }
        }
        return (x) fragment;
    }

    private static void b(FragmentManager fragmentManager, int i, Fragment fragment, String str, String str2, boolean z, boolean z2) {
        FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        Fragment findFragmentById = fragmentManager.findFragmentById(i);
        if (!b.a.f3833a.f3831a.j.booleanValue()) {
            if (findFragmentById == null || z2) {
                beginTransaction.setCustomAnimations(0, 0, 0, 0);
            } else {
                beginTransaction.setCustomAnimations(R.anim.hs__slide_in_from_right, R.anim.hs__slide_out_to_left, R.anim.hs__slide_in_from_left, R.anim.hs__slide_out_to_right);
            }
        }
        beginTransaction.replace(i, fragment, str);
        if (!TextUtils.isEmpty(str2)) {
            beginTransaction.addToBackStack(str2);
        }
        beginTransaction.commitAllowingStateLoss();
        if (z) {
            fragmentManager.executePendingTransactions();
        }
    }

    public static Fragment c(FragmentManager fragmentManager) {
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments == null || fragments.size() <= 0) {
            return null;
        }
        return fragments.get(fragments.size() - 1);
    }
}
