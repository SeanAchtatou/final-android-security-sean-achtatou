package com.helpshift.support.m;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.media.ExifInterface;
import android.net.Uri;
import com.helpshift.support.h;
import com.helpshift.util.g;
import com.helpshift.util.n;
import com.helpshift.util.q;
import com.helpshift.util.r;
import com.helpshift.util.s;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

/* compiled from: AttachmentUtil */
public final class b {
    private static Bitmap a(Uri uri, int i, boolean z) {
        try {
            return ImageDecoder.decodeBitmap(ImageDecoder.createSource(q.a().getContentResolver(), uri), new c(i, z));
        } catch (IOException e) {
            n.c("Helpshift_AttachUtil", "Error while building bitmap from uri", e);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(java.lang.String r10, int r11, boolean r12) {
        /*
            boolean r0 = com.helpshift.c.a.a.g.a(r10)
            r1 = 0
            if (r0 == 0) goto L_0x0023
            android.net.Uri r10 = android.net.Uri.parse(r10)     // Catch:{ Exception -> 0x000c }
            goto L_0x0015
        L_0x000c:
            r10 = move-exception
            java.lang.String r0 = "Helpshift_AttachUtil"
            java.lang.String r2 = "Error while converting to uri from file path"
            com.helpshift.util.n.c(r0, r2, r10)
            r10 = r1
        L_0x0015:
            if (r10 == 0) goto L_0x0022
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 28
            if (r0 < r2) goto L_0x0022
            android.graphics.Bitmap r10 = a(r10, r11, r12)
            return r10
        L_0x0022:
            return r1
        L_0x0023:
            boolean r12 = android.text.TextUtils.isEmpty(r10)
            r0 = 1
            r2 = 0
            if (r12 != 0) goto L_0x003e
            java.io.File r12 = new java.io.File
            r12.<init>(r10)
            boolean r3 = r12.exists()
            if (r3 == 0) goto L_0x003e
            boolean r12 = r12.canRead()
            if (r12 == 0) goto L_0x003e
            r12 = 1
            goto L_0x003f
        L_0x003e:
            r12 = 0
        L_0x003f:
            if (r12 != 0) goto L_0x0042
            goto L_0x0092
        L_0x0042:
            android.graphics.BitmapFactory$Options r12 = new android.graphics.BitmapFactory$Options
            r12.<init>()
            r1 = 4
            if (r11 <= 0) goto L_0x0067
            int r3 = r12.outWidth
            int r4 = r12.outHeight
            int r3 = com.helpshift.util.s.a(r3, r4, r11)
            r12.inJustDecodeBounds = r0
            android.graphics.BitmapFactory.decodeFile(r10, r12)
            int r11 = com.helpshift.util.s.a(r12, r11, r3)
            r12.inSampleSize = r11
            int r11 = r12.inSampleSize
            if (r11 >= r1) goto L_0x0069
            int r11 = r12.inSampleSize
            int r11 = r11 + r0
            r12.inSampleSize = r11
            goto L_0x0069
        L_0x0067:
            r12.inSampleSize = r1
        L_0x0069:
            r12.inJustDecodeBounds = r2
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeFile(r10, r12)
            if (r3 == 0) goto L_0x0091
            int r10 = a(r10)
            if (r10 == 0) goto L_0x0091
            android.graphics.Matrix r8 = new android.graphics.Matrix
            r8.<init>()
            float r10 = (float) r10
            r8.preRotate(r10)
            r4 = 0
            r5 = 0
            int r6 = r3.getWidth()
            int r7 = r3.getHeight()
            r9 = 0
            android.graphics.Bitmap r10 = android.graphics.Bitmap.createBitmap(r3, r4, r5, r6, r7, r8, r9)
            r1 = r10
            goto L_0x0092
        L_0x0091:
            r1 = r3
        L_0x0092:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.m.b.a(java.lang.String, int, boolean):android.graphics.Bitmap");
    }

    public static String a(String str, String str2) throws IOException {
        FileInputStream fileInputStream;
        FileOutputStream fileOutputStream;
        Context a2 = q.a();
        h hVar = new h(a2);
        FileOutputStream fileOutputStream2 = null;
        try {
            String b2 = b(str2, g.c(str));
            File file = new File(a2.getFilesDir(), b2);
            String absolutePath = file.getAbsolutePath();
            if (!file.exists()) {
                hVar.a(b2);
                fileInputStream = new FileInputStream(new File(str));
                try {
                    fileOutputStream = a2.openFileOutput(b2, 0);
                } catch (NullPointerException e) {
                    e = e;
                    fileOutputStream = null;
                    try {
                        n.a("Helpshift_AttachUtil", "NPE", e);
                        r.a(fileOutputStream);
                        r.a(fileInputStream);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        fileOutputStream2 = fileOutputStream;
                        r.a(fileOutputStream2);
                        r.a(fileInputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    r.a(fileOutputStream2);
                    r.a(fileInputStream);
                    throw th;
                }
                try {
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                    if (s.a(absolutePath)) {
                        s.a(absolutePath, 1024);
                    }
                } catch (NullPointerException e2) {
                    e = e2;
                    n.a("Helpshift_AttachUtil", "NPE", e);
                    r.a(fileOutputStream);
                    r.a(fileInputStream);
                    return null;
                }
            } else {
                fileOutputStream = null;
                fileInputStream = null;
            }
            r.a(fileOutputStream);
            r.a(fileInputStream);
            return absolutePath;
        } catch (NullPointerException e3) {
            e = e3;
            fileOutputStream = null;
            fileInputStream = null;
            n.a("Helpshift_AttachUtil", "NPE", e);
            r.a(fileOutputStream);
            r.a(fileInputStream);
            return null;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            r.a(fileOutputStream2);
            r.a(fileInputStream);
            throw th;
        }
    }

    public static String b(String str, String str2) {
        if (str == null) {
            str = "localRscMessage_" + UUID.randomUUID().toString();
        }
        return str + "0-thumbnail" + str2;
    }

    private static int a(String str) {
        try {
            String b2 = g.b(str);
            if (b2 != null && b2.contains("jpeg")) {
                int attributeInt = new ExifInterface(str).getAttributeInt("Orientation", 1);
                if (attributeInt == 6) {
                    return 90;
                }
                if (attributeInt == 3) {
                    return 180;
                }
                if (attributeInt == 8) {
                    return 270;
                }
                return 0;
            }
        } catch (Exception e) {
            n.c("Helpshift_AttachUtil", "Exception in getting exif rotation", e);
        }
        return 0;
    }
}
