package com.helpshift.support.m;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageButton;
import com.helpshift.R;
import com.helpshift.util.x;

/* compiled from: Styles */
public class l {
    public static int a(Context context, int i) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{i});
        int color = obtainStyledAttributes.getColor(0, -1);
        obtainStyledAttributes.recycle();
        return color;
    }

    public static void a(ImageButton imageButton, int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            imageButton.setImageAlpha(i);
        } else {
            imageButton.setAlpha(i);
        }
    }

    public static void a(Context context, Drawable drawable, boolean z) {
        x.a(context, drawable, z ? R.attr.colorAccent : 16842906);
    }

    public static void a(Context context, Drawable drawable) {
        x.a(context, drawable, R.attr.hs__chatBubbleAdminBackgroundColor);
    }

    public static void b(Context context, Drawable drawable) {
        x.a(context, drawable, R.attr.colorAccent);
    }

    public static boolean a(Context context) {
        return context.getResources().getBoolean(R.bool.is_screen_large);
    }
}
