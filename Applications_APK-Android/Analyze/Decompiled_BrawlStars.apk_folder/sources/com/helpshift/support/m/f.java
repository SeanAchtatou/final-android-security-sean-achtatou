package com.helpshift.support.m;

import com.helpshift.util.j;
import com.helpshift.util.n;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: HSCharacters */
public class f {

    /* renamed from: b  reason: collision with root package name */
    private static final String f4182b = f.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    Map<String, List<String>> f4183a;

    public f(JSONObject jSONObject) {
        try {
            this.f4183a = new HashMap();
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                this.f4183a.put(next, j.a(jSONObject.getJSONArray(next)));
            }
        } catch (JSONException e) {
            String str = f4182b;
            n.b(str, "HSCharacters constructor error : " + e.getMessage());
        }
    }
}
