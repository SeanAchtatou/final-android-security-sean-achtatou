package com.helpshift.support.m;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import com.helpshift.R;
import com.helpshift.util.n;
import com.helpshift.views.c;

/* compiled from: PermissionUtil */
public class i {
    public static Snackbar a(Fragment fragment, String[] strArr, int i, View view) {
        n.a("Helpshift_Permissions", "Requesting permission : " + strArr[0]);
        if (fragment.shouldShowRequestPermissionRationale(strArr[0])) {
            Snackbar action = c.a(view, R.string.hs__permission_denied_message, -2).setAction(R.string.hs__permission_rationale_snackbar_action_label, new j(fragment, strArr, i));
            action.show();
            return action;
        }
        fragment.requestPermissions(strArr, i);
        return null;
    }

    public static boolean a(Context context, String str) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
            if (packageInfo.requestedPermissions != null) {
                for (String equals : packageInfo.requestedPermissions) {
                    if (equals.equals(str)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            n.a("Helpshift_Permissions", "Error checking permission in Manifest : ", e);
        }
        return false;
    }
}
