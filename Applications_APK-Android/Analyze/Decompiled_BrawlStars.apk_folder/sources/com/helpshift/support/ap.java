package com.helpshift.support;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import com.facebook.appevents.AppEventsConstants;
import com.helpshift.a.b.c;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.k;
import com.helpshift.i.b.a;
import com.helpshift.j.b.a;
import com.helpshift.j.d.e;
import com.helpshift.support.l.d;
import com.helpshift.support.l.f;
import com.helpshift.support.l.i;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/* compiled from: SupportMigrator */
public class ap {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, boolean):void
     arg types: [com.helpshift.j.a.b.a, int, int]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.ab, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, boolean):void */
    private static void a(ab abVar, j jVar, com.helpshift.util.ab abVar2) {
        if (abVar2.a(new com.helpshift.util.ab("7.0.0")) && abVar2.b(new com.helpshift.util.ab("7.1.0"))) {
            a f = abVar.f();
            List<c> i = jVar.a().i();
            if (!com.helpshift.common.j.a(i)) {
                for (c next : i) {
                    List<com.helpshift.j.a.b.a> b2 = f.b(next.a().longValue());
                    if (!com.helpshift.common.j.a(b2)) {
                        for (com.helpshift.j.a.b.a next2 : b2) {
                            if (next2.g == e.REJECTED && !next2.s) {
                                next2.t = next.a().longValue();
                                jVar.b().a(next).c.a(next2, true, true);
                            }
                        }
                    }
                }
            }
        }
    }

    private static void b(ab abVar, j jVar, com.helpshift.util.ab abVar2) {
        if (abVar2.a(new com.helpshift.util.ab("7.0.0"))) {
            List<c> i = jVar.a().i();
            a f = abVar.f();
            HashSet hashSet = new HashSet();
            HashSet hashSet2 = new HashSet();
            for (c next : i) {
                if (jVar.b().a(next).k() != null) {
                    List<com.helpshift.j.a.b.a> b2 = f.b(next.a().longValue());
                    if (com.helpshift.common.j.a(b2)) {
                        continue;
                    } else {
                        for (com.helpshift.j.a.b.a next2 : b2) {
                            boolean z = true;
                            boolean z2 = !k.a(next2.d) && hashSet2.contains(next2.d);
                            if (k.a(next2.c) || !hashSet.contains(next2.c)) {
                                z = false;
                            }
                            if (z2 || z) {
                                f.a();
                                q.c().C();
                                return;
                            }
                            if (!k.a(next2.d)) {
                                hashSet2.add(next2.d);
                            }
                            if (!k.a(next2.c)) {
                                hashSet.add(next2.c);
                            }
                        }
                        continue;
                    }
                }
            }
        }
    }

    public static void a(Context context, ab abVar, j jVar, h hVar, u uVar) {
        String str;
        String str2;
        String str3;
        com.helpshift.util.ab abVar2;
        ab abVar3 = abVar;
        j jVar2 = jVar;
        u uVar2 = uVar;
        String a2 = uVar2.a("libraryVersion");
        if (a2.length() <= 0 || a2.equals("7.6.3")) {
            str = "libraryVersion";
            str2 = a2;
            str3 = "7.6.3";
        } else {
            com.helpshift.util.ab abVar4 = new com.helpshift.util.ab(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            try {
                abVar2 = new com.helpshift.util.ab(a2);
            } catch (NumberFormatException e) {
                n.b("Helpshift_SupportMigr", "Error in creating SemVer: " + e);
                abVar2 = abVar4;
            }
            if (!abVar2.a(new com.helpshift.util.ab("7.0.0"))) {
                str = "libraryVersion";
                f fVar = r3;
                str2 = a2;
                str3 = "7.6.3";
                f fVar2 = new f(q.c(), uVar, abVar.o(), com.helpshift.a.a.a.a.a(context), abVar.k(), abVar.B(), abVar.C(), abVar2);
                i iVar = new i(uVar2);
                fVar.a(abVar2);
                iVar.a();
                hVar.g();
                d.a.f4160a.b();
                SharedPreferences.Editor edit = uVar2.c.edit();
                edit.clear();
                edit.apply();
                fVar.f4164a.b();
                abVar.f().a();
                q.c().C();
                abVar.o().a();
                fVar.a();
                HashMap hashMap = new HashMap();
                hashMap.put("requireEmail", iVar.d);
                hashMap.put("fullPrivacy", iVar.e);
                hashMap.put("hideNameAndEmail", iVar.f);
                hashMap.put("showSearchOnNewConversation", iVar.g);
                hashMap.put("gotoConversationAfterContactUs", iVar.h);
                hashMap.put("showConversationResolutionQuestion", iVar.i);
                hashMap.put("showConversationInfoScreen", iVar.j);
                hashMap.put("enableTypingIndicator", iVar.k);
                HashMap hashMap2 = new HashMap(com.helpshift.support.m.d.a());
                hashMap2.putAll(hashMap);
                q.c().a(new a.b().a(hashMap2).a());
                iVar.f4169a.a(iVar.l);
                iVar.f4170b.a(iVar.m);
                if (!k.a(iVar.n)) {
                    iVar.c.a("key_support_device_id", iVar.n);
                }
                jVar.a().k().b();
            } else {
                str = "libraryVersion";
                str2 = a2;
                str3 = "7.6.3";
                b(abVar3, jVar2, abVar2);
                a(abVar3, jVar2, abVar2);
                if (abVar2.b(new com.helpshift.util.ab("7.5.0"))) {
                    abVar.t().a(com.helpshift.common.c.b.n.d);
                }
            }
        }
        uVar2.f4200b.deleteFile("tfidf.db");
        try {
            File file = new File(context.getFilesDir() + File.separator + "__hs_supportkvdb_lock");
            if (file.exists()) {
                file.delete();
            }
            File file2 = new File(context.getFilesDir() + File.separator + "__hs_kvdb_lock");
            if (file2.exists()) {
                file2.delete();
            }
            File externalStoragePublicDirectory = Environment.getExternalStoragePublicDirectory(".backups/" + context.getPackageName() + "/helpshift/databases/");
            if (externalStoragePublicDirectory != null && externalStoragePublicDirectory.canWrite()) {
                File file3 = new File(externalStoragePublicDirectory, "__hs__db_profiles");
                if (file3.canWrite()) {
                    file3.delete();
                }
                File file4 = new File(externalStoragePublicDirectory, "__hs__kv_backup");
                if (file4.canWrite()) {
                    file4.delete();
                }
            }
        } catch (Exception e2) {
            n.b("Helpshift_SupportMigr", "Error on deleting lock file: " + e2);
        }
        if (!str3.equals(str2)) {
            uVar2.a(str, str3);
        }
    }
}
