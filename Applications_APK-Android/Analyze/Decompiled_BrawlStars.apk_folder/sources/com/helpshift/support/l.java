package com.helpshift.support;

import android.os.Handler;
import android.os.Message;

/* compiled from: HSApiData */
public class l extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f4155a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Handler f4156b;
    final /* synthetic */ h c;

    public l(h hVar, String str, Handler handler) {
        this.c = hVar;
        this.f4155a = str;
        this.f4156b = handler;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        Section a2 = this.c.c.a(this.f4155a);
        Message obtainMessage = this.f4156b.obtainMessage();
        obtainMessage.obj = a2;
        this.f4156b.sendMessage(obtainMessage);
    }
}
