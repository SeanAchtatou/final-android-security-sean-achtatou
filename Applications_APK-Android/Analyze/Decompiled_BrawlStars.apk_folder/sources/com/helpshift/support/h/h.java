package com.helpshift.support.h;

import android.os.Bundle;
import com.helpshift.support.al;
import com.helpshift.support.e.b;
import java.util.HashMap;
import java.util.List;

/* compiled from: SingleFAQFlow */
public class h implements g {

    /* renamed from: a  reason: collision with root package name */
    public b f4086a;

    /* renamed from: b  reason: collision with root package name */
    private final int f4087b;
    private final String c;
    private final String d;
    private final HashMap e;

    public final int a() {
        return this.f4087b;
    }

    public final String b() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(android.os.Bundle, boolean, java.util.List<com.helpshift.support.h.g>):void
     arg types: [android.os.Bundle, int, java.util.List]
     candidates:
      com.helpshift.support.e.b.a(boolean, java.lang.Long, java.util.Map<java.lang.String, java.lang.Boolean>):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, android.os.Bundle, com.helpshift.support.i.i$a):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.List<com.helpshift.support.h.g>, boolean):void
      com.helpshift.support.e.b.a(android.os.Bundle, boolean, java.util.List<com.helpshift.support.h.g>):void */
    public final void c() {
        Bundle c2 = al.c(al.a((HashMap<String, Object>) this.e));
        c2.putString("questionPublishId", this.d);
        c2.putInt("support_mode", 3);
        c2.putBoolean("decomp", true);
        this.f4086a.a(c2, true, (List<g>) ((List) this.e.get("customContactUsFlows")));
    }
}
