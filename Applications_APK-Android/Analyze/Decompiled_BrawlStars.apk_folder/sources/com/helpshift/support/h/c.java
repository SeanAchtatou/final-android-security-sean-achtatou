package com.helpshift.support.h;

import com.helpshift.support.e.b;
import java.util.List;

/* compiled from: DynamicFormFlow */
public class c implements g {

    /* renamed from: a  reason: collision with root package name */
    public b f4079a;

    /* renamed from: b  reason: collision with root package name */
    private final int f4080b;
    private final String c;
    private final List<g> d;

    public final int a() {
        return this.f4080b;
    }

    public final String b() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void
     arg types: [java.util.List<com.helpshift.support.h.g>, int]
     candidates:
      com.helpshift.support.e.b.a(android.os.Bundle, boolean):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.d.d.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.d.e.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(java.lang.String, java.util.List<com.helpshift.support.h.g>, boolean):void
     arg types: [java.lang.String, java.util.List<com.helpshift.support.h.g>, int]
     candidates:
      com.helpshift.support.e.b.a(boolean, java.lang.Long, java.util.Map<java.lang.String, java.lang.Boolean>):void
      com.helpshift.support.e.b.a(android.os.Bundle, boolean, java.util.List<com.helpshift.support.h.g>):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, android.os.Bundle, com.helpshift.support.i.i$a):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.List<com.helpshift.support.h.g>, boolean):void */
    public final void c() {
        int i = this.f4080b;
        if (i != 0) {
            b bVar = this.f4079a;
            List<g> list = this.d;
            if (!(bVar.f3909b == null || i == 0)) {
                bVar.f3909b.putString("flow_title", bVar.f3908a.getResources().getString(i));
            }
            bVar.a(list, true);
            return;
        }
        this.f4079a.a(this.c, this.d, true);
    }
}
