package com.helpshift.support.e;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import com.helpshift.R;
import com.helpshift.j.a.b.a;
import com.helpshift.support.d.d;
import com.helpshift.support.d.e;
import com.helpshift.support.d.f;
import com.helpshift.support.f.ar;
import com.helpshift.support.f.j;
import com.helpshift.support.h.g;
import com.helpshift.support.i.i;
import com.helpshift.support.i.p;
import com.helpshift.support.i.u;
import com.helpshift.support.m.l;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: SupportController */
public class b implements d, e {

    /* renamed from: a  reason: collision with root package name */
    public final Context f3908a;

    /* renamed from: b  reason: collision with root package name */
    public final Bundle f3909b;
    public FragmentManager c;
    public Bundle d;
    public boolean e;
    public int f;
    public boolean g = false;
    public boolean h;
    private final String i = "key_support_controller_started";
    private final String j = "key_conversation_bundle";
    private final String k = "key_conversation_add_to_back_stack";
    private final f l;
    private String m;

    public b(Context context, f fVar, FragmentManager fragmentManager, Bundle bundle) {
        this.f3908a = context;
        this.l = fVar;
        this.c = fragmentManager;
        this.f3909b = bundle;
    }

    private void c(Bundle bundle) {
        Long valueOf = Long.valueOf(bundle.getLong("conversationIdInPush"));
        Bundle bundle2 = this.d;
        boolean equals = valueOf.equals(bundle2 != null ? Long.valueOf(bundle2.getLong("issueId")) : null);
        boolean z = true;
        boolean z2 = !equals;
        List<Fragment> fragments = this.c.getFragments();
        if (z2) {
            j();
        } else if (fragments.size() > 0) {
            Fragment fragment = fragments.get(fragments.size() - 1);
            if (!(fragment instanceof i)) {
                z = true ^ (fragment instanceof com.helpshift.support.f.b);
            } else {
                return;
            }
        }
        if (z) {
            this.d = bundle;
            d();
        }
    }

    public final void a(Bundle bundle, boolean z) {
        this.h = z;
        this.d = bundle;
        d();
    }

    public final void d() {
        a(new HashMap());
    }

    public final void a(Map<String, Boolean> map) {
        int i2 = c.f3910a[q.c().o().k().a().ordinal()];
        if (i2 == 1 || i2 == 2 || i2 == 3) {
            g();
        } else if (i2 == 4) {
            b(map);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(boolean, java.lang.Long, java.util.Map<java.lang.String, java.lang.Boolean>):void
     arg types: [int, ?[OBJECT, ARRAY], java.util.Map<java.lang.String, java.lang.Boolean>]
     candidates:
      com.helpshift.support.e.b.a(android.os.Bundle, boolean, java.util.List<com.helpshift.support.h.g>):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, android.os.Bundle, com.helpshift.support.i.i$a):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.List<com.helpshift.support.h.g>, boolean):void
      com.helpshift.support.e.b.a(boolean, java.lang.Long, java.util.Map<java.lang.String, java.lang.Boolean>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(boolean, java.lang.Long, java.util.Map<java.lang.String, java.lang.Boolean>):void
     arg types: [int, java.lang.Long, java.util.Map<java.lang.String, java.lang.Boolean>]
     candidates:
      com.helpshift.support.e.b.a(android.os.Bundle, boolean, java.util.List<com.helpshift.support.h.g>):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, android.os.Bundle, com.helpshift.support.i.i$a):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.List<com.helpshift.support.h.g>, boolean):void
      com.helpshift.support.e.b.a(boolean, java.lang.Long, java.util.Map<java.lang.String, java.lang.Boolean>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void
     arg types: [java.util.List<com.helpshift.support.h.g>, int]
     candidates:
      com.helpshift.support.e.b.a(android.os.Bundle, boolean):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.d.d.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.d.e.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void */
    public void b(Map<String, Boolean> map) {
        String name;
        a c2;
        if (this.d == null) {
            this.d = this.f3909b;
        }
        boolean a2 = q.c().r().a("disableInAppConversation");
        Long l2 = null;
        if (!q.c().r().i() || a2) {
            long j2 = this.d.getLong("conversationIdInPush", 0);
            if (j2 != 0) {
                this.d.remove("conversationIdInPush");
                if (q.c().u().b(j2)) {
                    a(false, Long.valueOf(j2), map);
                    return;
                }
            }
            if (!a2 && (c2 = q.c().c()) != null) {
                l2 = c2.f3504b;
            }
            if (l2 == null) {
                List<g> a3 = com.helpshift.support.h.b.a();
                if (a3 == null || a3.isEmpty()) {
                    h();
                    return;
                }
                FragmentManager.BackStackEntry backStackEntryAt = this.c.getBackStackEntryAt(this.c.getBackStackEntryCount() - 1);
                if (!(backStackEntryAt == null || (name = backStackEntryAt.getName()) == null || !name.equals(com.helpshift.support.f.d.class.getName()))) {
                    this.c.popBackStackImmediate(name, 1);
                }
                a(a3, true);
                return;
            }
            a(false, l2, map);
            return;
        }
        a(true, (Long) null, map);
    }

    private void g() {
        String str;
        com.helpshift.support.f.c.a d2 = com.helpshift.support.f.c.a.d();
        if (this.h) {
            str = d2.getClass().getName();
            j();
        } else {
            str = null;
        }
        com.helpshift.support.m.e.a(this.c, R.id.flow_fragment_container, d2, "HSUserSetupFragment", str, false, false);
    }

    public final void a(Bundle bundle, boolean z, List<g> list) {
        if (!d(bundle)) {
            com.helpshift.support.i.b a2 = com.helpshift.support.i.b.a(bundle, list);
            String str = null;
            if (z) {
                str = com.helpshift.support.i.b.class.getName();
            }
            com.helpshift.support.m.e.a(this.c, R.id.flow_fragment_container, a2, "Helpshift_FaqFlowFrag", str, false, false);
        }
    }

    private boolean d(Bundle bundle) {
        a aVar;
        Fragment c2 = com.helpshift.support.m.e.c(this.c);
        if (!(c2 instanceof com.helpshift.support.i.b) || (aVar = ((com.helpshift.support.i.b) c2).f4092a) == null) {
            return false;
        }
        Fragment c3 = com.helpshift.support.m.e.c(aVar.d);
        if (!(c3 instanceof u)) {
            return true;
        }
        String string = bundle.getString("questionPublishId");
        String str = ((u) c3).c;
        if (string == null || !string.equals(str)) {
            return false;
        }
        return true;
    }

    private void h() {
        String str;
        n.a("Helpshift_SupportContr", "Starting new conversation fragment");
        this.d.putBoolean("search_performed", this.g);
        this.d.putString("source_search_query", this.m);
        ar a2 = ar.a(this.d);
        if (this.h) {
            str = a2.getClass().getName();
            j();
        } else {
            str = null;
        }
        com.helpshift.support.m.e.a(this.c, R.id.flow_fragment_container, a2, "HSNewConversationFragment", str, false, false);
    }

    private void a(boolean z, Long l2, Map<String, Boolean> map) {
        n.a("Helpshift_SupportContr", "Starting conversation fragment: " + l2);
        if (!z) {
            if (l2 != null) {
                this.d.putLong("issueId", l2.longValue());
            } else {
                return;
            }
        }
        this.d.putBoolean("show_conv_history", z);
        for (String next : map.keySet()) {
            this.d.putBoolean(next, map.get(next).booleanValue());
        }
        com.helpshift.support.f.d a2 = com.helpshift.support.f.d.a(this.d);
        String str = null;
        if (this.h) {
            str = a2.getClass().getName();
            j();
        }
        com.helpshift.support.m.e.a(this.c, R.id.flow_fragment_container, a2, "HSConversationFragment", str, false, false);
    }

    public final void a(List<g> list, boolean z) {
        com.helpshift.support.m.e.a(this.c, R.id.flow_fragment_container, com.helpshift.support.i.a.a(this.f3909b, list, this), "HSDynamicFormFragment", z ? com.helpshift.support.i.a.class.getName() : null, false, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void
     arg types: [java.util.List<com.helpshift.support.h.g>, int]
     candidates:
      com.helpshift.support.e.b.a(android.os.Bundle, boolean):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.d.d.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.d.e.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void */
    public final void a(String str, List<g> list, boolean z) {
        Bundle bundle = this.f3909b;
        if (bundle != null) {
            bundle.putString("flow_title", str);
        }
        a(list, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(android.os.Bundle, boolean):void
     arg types: [android.os.Bundle, int]
     candidates:
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void
      com.helpshift.support.d.d.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.d.e.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.e.b.a(android.os.Bundle, boolean):void */
    public final void a(String str) {
        if (!i()) {
            if (!TextUtils.isEmpty(str)) {
                this.m = str;
            }
            a(this.f3909b, true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void
     arg types: [java.util.List<com.helpshift.support.h.g>, int]
     candidates:
      com.helpshift.support.e.b.a(android.os.Bundle, boolean):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.d.d.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.support.d.e.a(java.lang.String, java.util.ArrayList<java.lang.String>):void
      com.helpshift.support.e.b.a(java.util.List<com.helpshift.support.h.g>, boolean):void */
    private boolean i() {
        com.helpshift.support.i.b a2;
        List<g> list;
        if (q.c().b() != null || (a2 = com.helpshift.support.m.e.a(this.c)) == null || (list = a2.f4093b) == null || list.isEmpty()) {
            return false;
        }
        a(list, true);
        return true;
    }

    public final void a(String str, ArrayList<String> arrayList) {
        boolean a2 = l.a(this.f3908a);
        this.f3909b.putString("questionPublishId", str);
        if (arrayList != null) {
            this.f3909b.putStringArrayList("searchTerms", arrayList);
        }
        com.helpshift.support.m.e.a(this.c, R.id.flow_fragment_container, u.a(this.f3909b, 2, a2, null), null, false);
    }

    public final void c() {
        q.c().k().a(com.helpshift.b.b.TICKET_AVOIDANCE_FAILED);
        this.c.popBackStackImmediate(p.class.getName(), 1);
        ar arVar = (ar) this.c.findFragmentByTag("HSNewConversationFragment");
        if (arVar != null) {
            arVar.f4007a.c();
        }
    }

    public final void a(com.helpshift.j.d.d dVar) {
        this.c.popBackStack(i.class.getName(), 1);
        ar arVar = (ar) this.c.findFragmentByTag("HSNewConversationFragment");
        if (arVar != null) {
            arVar.a(i.b.ADD, dVar);
        }
    }

    public final void a(com.helpshift.j.d.d dVar, String str) {
        this.c.popBackStack(i.class.getName(), 1);
        com.helpshift.support.f.d dVar2 = (com.helpshift.support.f.d) this.c.findFragmentByTag("HSConversationFragment");
        if (dVar2 != null) {
            if (j.f4048a[i.b.SEND.ordinal()] == 1) {
                if (!dVar2.e || dVar2.d == null) {
                    dVar2.f = dVar;
                    dVar2.g = str;
                    dVar2.h = true;
                    return;
                }
                dVar2.d.a(dVar, str);
            }
        }
    }

    public final void a() {
        this.c.popBackStack(i.class.getName(), 1);
        ar arVar = (ar) this.c.findFragmentByTag("HSNewConversationFragment");
        if (arVar != null) {
            arVar.a(i.b.REMOVE, (com.helpshift.j.d.d) null);
        }
    }

    public final void a(Bundle bundle) {
        this.l.a(true, bundle);
    }

    public final void b() {
        this.c.popBackStack(i.class.getName(), 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(android.os.Bundle, boolean, java.util.List<com.helpshift.support.h.g>):void
     arg types: [android.os.Bundle, int, java.util.List<com.helpshift.support.h.g>]
     candidates:
      com.helpshift.support.e.b.a(boolean, java.lang.Long, java.util.Map<java.lang.String, java.lang.Boolean>):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, android.os.Bundle, com.helpshift.support.i.i$a):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.List<com.helpshift.support.h.g>, boolean):void
      com.helpshift.support.e.b.a(android.os.Bundle, boolean, java.util.List<com.helpshift.support.h.g>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.e.b.a(java.lang.String, java.util.List<com.helpshift.support.h.g>, boolean):void
     arg types: [java.lang.String, java.util.List<com.helpshift.support.h.g>, int]
     candidates:
      com.helpshift.support.e.b.a(boolean, java.lang.Long, java.util.Map<java.lang.String, java.lang.Boolean>):void
      com.helpshift.support.e.b.a(android.os.Bundle, boolean, java.util.List<com.helpshift.support.h.g>):void
      com.helpshift.support.e.b.a(com.helpshift.j.d.d, android.os.Bundle, com.helpshift.support.i.i$a):void
      com.helpshift.support.e.b.a(java.lang.String, java.util.List<com.helpshift.support.h.g>, boolean):void */
    public final void b(Bundle bundle) {
        int i2 = bundle.getInt("support_mode");
        if (i2 == 1) {
            c(bundle);
        } else if (i2 != 4) {
            a(bundle, true, com.helpshift.support.h.b.a());
        } else {
            a(bundle.getString("flow_title"), com.helpshift.support.h.d.a(), true);
        }
    }

    private void j() {
        boolean z;
        List<Fragment> fragments = this.c.getFragments();
        for (int size = fragments.size() - 1; size >= 0; size--) {
            Fragment fragment = fragments.get(size);
            if ((fragment instanceof i) || (fragment instanceof com.helpshift.support.f.b) || (fragment instanceof com.helpshift.support.f.c.a) || (fragment instanceof com.helpshift.support.f.a)) {
                if (size == 0) {
                    com.helpshift.support.m.e.a(this.c, fragment);
                    List<Fragment> fragments2 = this.c.getFragments();
                    if (fragments2 != null && fragments2.size() > 0) {
                        this.c.popBackStack(fragment.getClass().getName(), 1);
                    }
                } else {
                    this.c.popBackStack(fragment.getClass().getName(), 1);
                }
            }
        }
        Fragment findFragmentByTag = this.c.findFragmentByTag("HSConversationFragment");
        if (findFragmentByTag != null) {
            this.c.popBackStackImmediate(findFragmentByTag.getClass().getName(), 1);
            z = true;
        } else {
            z = false;
        }
        if (!z) {
            this.h = true;
        }
    }

    public final void e() {
        n.a("Helpshift_SupportContr", "Starting authentication failure fragment");
        com.helpshift.support.f.a a2 = com.helpshift.support.f.a.a();
        String name = this.h ? a2.getClass().getName() : null;
        j();
        com.helpshift.support.m.e.a(this.c, R.id.flow_fragment_container, a2, "HSAuthenticationFailureFragment", name, false, false);
    }

    public final void a(com.helpshift.j.d.d dVar, Bundle bundle, i.a aVar) {
        i iVar = (i) com.helpshift.support.m.e.a(this.c, i.class);
        if (iVar == null) {
            iVar = i.a(this);
            com.helpshift.support.m.e.a(this.c, R.id.flow_fragment_container, iVar, "ScreenshotPreviewFragment", false);
        }
        iVar.e = bundle.getInt("key_screenshot_mode");
        iVar.f = bundle.getString("key_refers_id");
        iVar.f4103a = dVar;
        iVar.c = aVar;
        iVar.c();
    }

    public final void f() {
        String str;
        u uVar = (u) com.helpshift.support.m.e.a(this.c, u.class);
        if (uVar != null) {
            if (uVar.f4127b != null) {
                str = uVar.f4127b.j;
            } else {
                str = "";
            }
            if (!TextUtils.isEmpty(str)) {
                HashMap hashMap = new HashMap();
                hashMap.put("id", str);
                com.helpshift.j.d.a a2 = q.b().e().a(q.c().o().a().a().longValue());
                if (a2 != null) {
                    hashMap.put("str", a2.f3580a);
                }
                q.c().k().a(com.helpshift.b.b.TICKET_AVOIDED, hashMap);
            }
        }
        Long a3 = q.c().o().a().a();
        q.b().e().a(a3.longValue(), new com.helpshift.j.d.a("", System.nanoTime(), 0));
        q.b().e().a(a3.longValue(), (com.helpshift.j.d.d) null);
        if (this.f == 1) {
            this.l.a();
        } else {
            this.c.popBackStackImmediate(ar.class.getName(), 1);
        }
    }
}
