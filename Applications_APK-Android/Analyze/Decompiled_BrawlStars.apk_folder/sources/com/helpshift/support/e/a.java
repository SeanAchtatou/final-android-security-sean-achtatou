package com.helpshift.support.e;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.MenuItem;
import com.helpshift.R;
import com.helpshift.b.b;
import com.helpshift.support.d.c;
import com.helpshift.support.i.g;
import com.helpshift.support.i.u;
import com.helpshift.support.m.e;
import com.helpshift.support.m.l;
import com.helpshift.util.p;
import com.helpshift.util.q;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: FaqFlowController */
public class a implements MenuItemCompat.OnActionExpandListener, SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener, c {

    /* renamed from: a  reason: collision with root package name */
    public final com.helpshift.support.d.a f3906a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3907b;
    public final Bundle c;
    public FragmentManager d;
    public boolean e;
    public boolean f;
    private final String g = "key_faq_controller_state";
    private String h = "";
    private String i = "";

    public boolean onQueryTextSubmit(String str) {
        return false;
    }

    public a(com.helpshift.support.d.a aVar, Context context, FragmentManager fragmentManager, Bundle bundle) {
        this.f3906a = aVar;
        this.f3907b = l.a(context);
        this.d = fragmentManager;
        this.c = bundle;
    }

    public final void a(Bundle bundle) {
        if (this.f3907b) {
            e.a(this.d, R.id.list_fragment_container, g.a(bundle), null, false);
            return;
        }
        e.a(this.d, R.id.list_fragment_container, com.helpshift.support.b.c.a(bundle), null, false);
    }

    public final void a(String str, ArrayList<String> arrayList) {
        a();
        this.f3906a.c().d.g = true;
        Bundle bundle = new Bundle();
        bundle.putString("questionPublishId", str);
        bundle.putStringArrayList("searchTerms", arrayList);
        if (this.f3907b) {
            e.b(this.d, R.id.details_fragment_container, u.a(bundle, 1, false, null), null, false);
            return;
        }
        e.a(this.d, R.id.list_fragment_container, u.a(bundle, 1, false, null), null, false);
    }

    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        if (((com.helpshift.support.i.l) this.d.findFragmentByTag("Helpshift_SearchFrag")) != null) {
            return true;
        }
        e.a(this.d, R.id.list_fragment_container, com.helpshift.support.i.l.a(this.c), "Helpshift_SearchFrag", false);
        return true;
    }

    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        a();
        if (!this.f) {
            this.i = "";
            this.h = "";
            this.d.popBackStack(com.helpshift.support.i.l.class.getName(), 1);
        }
        return true;
    }

    public boolean onQueryTextChange(String str) {
        com.helpshift.support.i.l lVar;
        if (TextUtils.isEmpty(str) && this.h.length() > 2) {
            a();
        }
        this.h = str;
        if (this.f || (lVar = (com.helpshift.support.i.l) this.d.findFragmentByTag("Helpshift_SearchFrag")) == null) {
            return false;
        }
        lVar.a(str, this.c.getString("sectionPublishId"));
        return true;
    }

    private void a() {
        int c2;
        if (!TextUtils.isEmpty(this.h.trim()) && !this.i.equals(this.h)) {
            this.f3906a.c().d.g = true;
            this.c.putBoolean("search_performed", true);
            com.helpshift.support.i.l lVar = (com.helpshift.support.i.l) this.d.findFragmentByTag("Helpshift_SearchFrag");
            if (lVar != null && (c2 = lVar.c()) >= 0) {
                HashMap hashMap = new HashMap();
                hashMap.put("s", this.h);
                hashMap.put("n", Integer.valueOf(c2));
                hashMap.put("nt", Boolean.valueOf(p.a(q.a())));
                q.c().k().a(b.PERFORMED_SEARCH, hashMap);
                this.i = this.h;
            }
        }
    }

    public final void a(String str) {
        this.f = true;
        a();
        this.f3906a.c().d.a(str);
    }
}
