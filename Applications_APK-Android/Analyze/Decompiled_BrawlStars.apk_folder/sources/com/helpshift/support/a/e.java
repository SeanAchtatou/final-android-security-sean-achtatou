package com.helpshift.support.a;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.support.Section;
import java.util.List;

/* compiled from: SectionListAdapter */
public class e extends RecyclerView.Adapter<a> {

    /* renamed from: a  reason: collision with root package name */
    private List<Section> f3857a;

    /* renamed from: b  reason: collision with root package name */
    private View.OnClickListener f3858b;

    public /* synthetic */ void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        a aVar = (a) viewHolder;
        Section section = this.f3857a.get(i);
        aVar.f3859a.setText(section.f3840b);
        aVar.f3859a.setTag(section.c);
    }

    public e(List<Section> list, View.OnClickListener onClickListener) {
        this.f3857a = list;
        this.f3858b = onClickListener;
    }

    public int getItemCount() {
        return this.f3857a.size();
    }

    /* compiled from: SectionListAdapter */
    protected static class a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        TextView f3859a;

        public a(TextView textView) {
            super(textView);
            this.f3859a = textView;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public /* synthetic */ RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        TextView textView = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs_simple_recycler_view_item, viewGroup, false);
        textView.setOnClickListener(this.f3858b);
        return new a(textView);
    }
}
