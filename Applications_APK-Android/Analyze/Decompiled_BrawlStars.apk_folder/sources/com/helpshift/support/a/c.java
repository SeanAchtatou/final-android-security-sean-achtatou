package com.helpshift.support.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.support.Faq;
import com.helpshift.support.d;
import com.helpshift.support.m.g;
import com.helpshift.util.x;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SearchListAdapter */
public class c extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    public List<Faq> f3847a;

    /* renamed from: b  reason: collision with root package name */
    private final String f3848b;
    private final int c = 1;
    private View.OnClickListener d;
    private View.OnClickListener e;

    public c(String str, List<Faq> list, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        this.f3848b = str;
        this.f3847a = list;
        this.d = onClickListener;
        this.e = onClickListener2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i != 0) {
            return new b((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs_simple_recycler_view_item, viewGroup, false));
        }
        return new a((LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__search_list_footer, viewGroup, false));
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (a(i)) {
            a aVar = (a) viewHolder;
            Context context = aVar.c.getContext();
            String string = context.getResources().getString(R.string.hs__search_footer);
            String string2 = context.getResources().getString(R.string.hs__no_search_results_message);
            if (d.a(d.a.SEARCH_FOOTER)) {
                if (getItemCount() == 1) {
                    aVar.f3850b.setText(string2.replaceFirst("query", " \"" + this.f3848b + "\""));
                    aVar.e.setVisibility(8);
                } else {
                    aVar.e.setVisibility(0);
                    aVar.f3850b.setText(string);
                }
                aVar.f3849a.setVisibility(0);
                aVar.d.setVisibility(8);
                aVar.c.setOnClickListener(this.e);
                return;
            }
            aVar.f3849a.setVisibility(8);
            if (getItemCount() == 1) {
                aVar.d.setVisibility(0);
            } else {
                aVar.d.setVisibility(8);
            }
        } else {
            a((b) viewHolder, i);
        }
    }

    public int getItemViewType(int i) {
        return a(i) ? 0 : 1;
    }

    public long getItemId(int i) {
        if (a(i)) {
            return 0;
        }
        return Long.valueOf(this.f3847a.get(i).f3837b).longValue();
    }

    public int getItemCount() {
        return this.f3847a.size() + 1;
    }

    private void a(b bVar, int i) {
        b bVar2 = bVar;
        Faq faq = this.f3847a.get(i);
        ArrayList<String> arrayList = faq.i;
        String str = faq.f3836a;
        if (arrayList == null || arrayList.size() <= 0) {
            bVar2.f3851a.setText(str);
        } else {
            int a2 = x.a(bVar2.f3851a.getContext(), R.attr.hs__searchHighlightColor);
            SpannableString spannableString = new SpannableString(str);
            if (str.equals(g.a(str))) {
                String lowerCase = str.toLowerCase();
                for (String next : arrayList) {
                    if (next.length() >= 3) {
                        for (int indexOf = TextUtils.indexOf(lowerCase, next, 0); indexOf >= 0; indexOf = TextUtils.indexOf(lowerCase, next, indexOf + next.length())) {
                            spannableString.setSpan(new BackgroundColorSpan(a2), indexOf, next.length() + indexOf, 33);
                        }
                    }
                }
            } else {
                int length = str.length();
                StringBuilder sb = new StringBuilder();
                ArrayList arrayList2 = new ArrayList();
                for (int i2 = 0; i2 < length; i2++) {
                    char charAt = str.charAt(i2);
                    String a3 = g.a(charAt + "");
                    for (int i3 = 0; i3 < a3.length(); i3++) {
                        sb.append(a3.charAt(i3));
                        arrayList2.add(Integer.valueOf(i2));
                    }
                }
                String lowerCase2 = sb.toString().toLowerCase();
                for (String lowerCase3 : arrayList) {
                    String lowerCase4 = lowerCase3.toLowerCase();
                    if (lowerCase4.length() >= 3) {
                        for (int indexOf2 = TextUtils.indexOf(lowerCase2, lowerCase4, 0); indexOf2 >= 0; indexOf2 = TextUtils.indexOf(lowerCase2, lowerCase4, indexOf2 + lowerCase4.length())) {
                            spannableString.setSpan(new BackgroundColorSpan(a2), ((Integer) arrayList2.get(indexOf2)).intValue(), ((Integer) arrayList2.get((lowerCase4.length() + indexOf2) - 1)).intValue() + 1, 33);
                        }
                    }
                }
            }
            bVar2.f3851a.setText(spannableString);
        }
        bVar2.f3851a.setOnClickListener(this.d);
        bVar2.f3851a.setTag(faq.f3837b);
    }

    private boolean a(int i) {
        return i == getItemCount() - 1;
    }

    /* compiled from: SearchListAdapter */
    static class b extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        TextView f3851a;

        public b(TextView textView) {
            super(textView);
            this.f3851a = textView;
        }
    }

    /* compiled from: SearchListAdapter */
    static class a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        LinearLayout f3849a;

        /* renamed from: b  reason: collision with root package name */
        TextView f3850b;
        Button c;
        TextView d;
        View e;

        a(LinearLayout linearLayout) {
            super(linearLayout);
            this.f3849a = (LinearLayout) linearLayout.findViewById(R.id.contact_us_view);
            this.f3850b = (TextView) linearLayout.findViewById(R.id.contact_us_hint_text);
            this.c = (Button) linearLayout.findViewById(R.id.report_issue);
            this.d = (TextView) linearLayout.findViewById(R.id.no_faqs_view);
            this.e = linearLayout.findViewById(R.id.search_list_footer_divider);
        }
    }
}
