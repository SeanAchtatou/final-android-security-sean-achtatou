package com.helpshift.support.a;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.support.Faq;
import java.util.List;

/* compiled from: QuestionListAdapter */
public class b extends RecyclerView.Adapter<a> {

    /* renamed from: a  reason: collision with root package name */
    private List<Faq> f3844a;

    /* renamed from: b  reason: collision with root package name */
    private View.OnClickListener f3845b;

    public /* synthetic */ void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        a aVar = (a) viewHolder;
        Faq faq = this.f3844a.get(i);
        aVar.f3846a.setText(faq.f3836a);
        aVar.f3846a.setTag(faq.f3837b);
    }

    public b(List<Faq> list, View.OnClickListener onClickListener) {
        this.f3844a = list;
        this.f3845b = onClickListener;
    }

    public int getItemCount() {
        return this.f3844a.size();
    }

    /* compiled from: QuestionListAdapter */
    protected static class a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        TextView f3846a;

        public a(TextView textView) {
            super(textView);
            this.f3846a = textView;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public /* synthetic */ RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        TextView textView = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs_simple_recycler_view_item, viewGroup, false);
        textView.setOnClickListener(this.f3845b);
        return new a(textView);
    }
}
