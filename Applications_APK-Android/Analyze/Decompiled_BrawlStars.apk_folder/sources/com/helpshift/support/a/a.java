package com.helpshift.support.a;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.support.h.g;
import java.util.List;

/* compiled from: FlowListAdapter */
public class a extends RecyclerView.Adapter<C0141a> {

    /* renamed from: a  reason: collision with root package name */
    private List<g> f3841a;

    /* renamed from: b  reason: collision with root package name */
    private View.OnClickListener f3842b;

    public /* synthetic */ void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        String str;
        C0141a aVar = (C0141a) viewHolder;
        g gVar = this.f3841a.get(i);
        if (gVar.a() != 0) {
            str = aVar.f3843a.getResources().getString(gVar.a());
        } else {
            str = gVar.b();
        }
        aVar.f3843a.setText(str);
        aVar.f3843a.setTag(Integer.valueOf(i));
    }

    public a(List<g> list, View.OnClickListener onClickListener) {
        this.f3841a = list;
        this.f3842b = onClickListener;
    }

    public int getItemCount() {
        return this.f3841a.size();
    }

    /* renamed from: com.helpshift.support.a.a$a  reason: collision with other inner class name */
    /* compiled from: FlowListAdapter */
    protected static class C0141a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        TextView f3843a;

        public C0141a(TextView textView) {
            super(textView);
            this.f3843a = textView;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public /* synthetic */ RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        TextView textView = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__simple_list_item_1, viewGroup, false);
        textView.setOnClickListener(this.f3842b);
        return new C0141a(textView);
    }
}
