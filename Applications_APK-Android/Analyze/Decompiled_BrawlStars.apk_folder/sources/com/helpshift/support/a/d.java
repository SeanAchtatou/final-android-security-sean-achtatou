package com.helpshift.support.a;

import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.support.Faq;
import com.helpshift.support.d;
import com.helpshift.support.m.g;
import com.helpshift.util.x;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SearchResultAdapter */
public class d extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    public List<Faq> f3852a;

    /* renamed from: b  reason: collision with root package name */
    private View.OnClickListener f3853b;
    private View.OnClickListener c;

    public d(List<Faq> list, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        this.f3852a = list;
        this.f3853b = onClickListener;
        this.c = onClickListener2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 1) {
            return new b((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__search_result_header, viewGroup, false));
        }
        if (i != 2) {
            return new c((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs_simple_recycler_view_item, viewGroup, false));
        }
        return new a((LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__search_result_footer, viewGroup, false));
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        if (viewHolder2 instanceof a) {
            a aVar = (a) viewHolder2;
            if (com.helpshift.support.d.a(d.a.SEARCH_FOOTER)) {
                aVar.f3854a.setVisibility(0);
                aVar.f3855b.setOnClickListener(this.c);
                return;
            }
            aVar.f3854a.setVisibility(8);
        } else if (viewHolder2 instanceof c) {
            c cVar = (c) viewHolder2;
            Faq faq = this.f3852a.get(i - 1);
            ArrayList<String> arrayList = faq.i;
            String str = faq.f3836a;
            if (arrayList == null || arrayList.size() <= 0) {
                cVar.f3856a.setText(str);
            } else {
                int a2 = x.a(cVar.f3856a.getContext(), R.attr.hs__searchHighlightColor);
                SpannableString spannableString = new SpannableString(str);
                if (str.equals(g.a(str))) {
                    String lowerCase = str.toLowerCase();
                    for (String next : arrayList) {
                        if (next.length() >= 3) {
                            for (int indexOf = TextUtils.indexOf(lowerCase, next, 0); indexOf >= 0; indexOf = TextUtils.indexOf(lowerCase, next, indexOf + next.length())) {
                                spannableString.setSpan(new BackgroundColorSpan(a2), indexOf, next.length() + indexOf, 33);
                            }
                        }
                    }
                } else {
                    int length = str.length();
                    StringBuilder sb = new StringBuilder();
                    ArrayList arrayList2 = new ArrayList();
                    for (int i2 = 0; i2 < length; i2++) {
                        char charAt = str.charAt(i2);
                        String a3 = g.a(charAt + "");
                        for (int i3 = 0; i3 < a3.length(); i3++) {
                            sb.append(a3.charAt(i3));
                            arrayList2.add(Integer.valueOf(i2));
                        }
                    }
                    String lowerCase2 = sb.toString().toLowerCase();
                    for (String lowerCase3 : arrayList) {
                        String lowerCase4 = lowerCase3.toLowerCase();
                        if (lowerCase4.length() >= 3) {
                            for (int indexOf2 = TextUtils.indexOf(lowerCase2, lowerCase4, 0); indexOf2 >= 0; indexOf2 = TextUtils.indexOf(lowerCase2, lowerCase4, indexOf2 + lowerCase4.length())) {
                                spannableString.setSpan(new BackgroundColorSpan(a2), ((Integer) arrayList2.get(indexOf2)).intValue(), ((Integer) arrayList2.get((lowerCase4.length() + indexOf2) - 1)).intValue() + 1, 33);
                            }
                        }
                    }
                }
                cVar.f3856a.setText(spannableString);
            }
            cVar.f3856a.setOnClickListener(this.f3853b);
            cVar.f3856a.setTag(faq.f3837b);
        }
    }

    public int getItemViewType(int i) {
        if (i == 0) {
            return 1;
        }
        return a(i) ? 2 : 3;
    }

    public long getItemId(int i) {
        if (i == 0) {
            return 1;
        }
        if (a(i)) {
            return 2;
        }
        return Long.valueOf(this.f3852a.get(i - 1).f3837b).longValue();
    }

    public int getItemCount() {
        return this.f3852a.size() + 2;
    }

    private boolean a(int i) {
        return i == getItemCount() - 1;
    }

    /* compiled from: SearchResultAdapter */
    protected static class c extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        TextView f3856a;

        public c(TextView textView) {
            super(textView);
            this.f3856a = textView;
        }
    }

    /* compiled from: SearchResultAdapter */
    protected static class a extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        LinearLayout f3854a;

        /* renamed from: b  reason: collision with root package name */
        Button f3855b;

        public a(LinearLayout linearLayout) {
            super(linearLayout);
            this.f3854a = linearLayout;
            this.f3855b = (Button) linearLayout.findViewById(R.id.send_anyway_button);
        }
    }

    /* compiled from: SearchResultAdapter */
    protected static class b extends RecyclerView.ViewHolder {
        public b(TextView textView) {
            super(textView);
        }
    }
}
