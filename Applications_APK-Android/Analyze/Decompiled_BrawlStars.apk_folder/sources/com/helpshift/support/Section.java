package com.helpshift.support;

import android.os.Parcel;
import android.os.Parcelable;

public class Section implements Parcelable {
    public static final Parcelable.Creator<Section> CREATOR = new y();

    /* renamed from: a  reason: collision with root package name */
    public String f3839a;

    /* renamed from: b  reason: collision with root package name */
    public String f3840b;
    public String c;
    private long d;

    public int describeContents() {
        return 0;
    }

    public Section() {
        this.d = -1;
        this.f3839a = "";
        this.c = "";
        this.f3840b = "";
    }

    public Section(long j, String str, String str2, String str3) {
        this.d = j;
        this.f3839a = str;
        this.f3840b = str2;
        this.c = str3;
    }

    Section(Parcel parcel) {
        this.f3839a = parcel.readString();
        this.f3840b = parcel.readString();
        this.c = parcel.readString();
    }

    public boolean equals(Object obj) {
        Section section = (Section) obj;
        if (section != null && this.f3840b.equals(section.f3840b) && this.c.equals(section.c) && this.f3839a.equals(section.f3839a)) {
            return true;
        }
        return false;
    }

    public String toString() {
        return this.f3840b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f3839a);
        parcel.writeString(this.f3840b);
        parcel.writeString(this.c);
    }
}
