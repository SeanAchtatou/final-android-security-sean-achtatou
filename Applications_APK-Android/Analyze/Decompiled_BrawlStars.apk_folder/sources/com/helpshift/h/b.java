package com.helpshift.h;

import com.helpshift.common.c.l;
import com.helpshift.common.k;
import com.helpshift.h.b.a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/* compiled from: CustomIssueFieldDM */
class b extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Map f3413a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f3414b;

    b(a aVar, Map map) {
        this.f3414b = aVar;
        this.f3413a = map;
    }

    public final void a() {
        ArrayList arrayList;
        String[] strArr;
        a aVar = this.f3414b;
        Map map = this.f3413a;
        if (map == null) {
            arrayList = null;
        } else {
            ArrayList arrayList2 = new ArrayList();
            for (String str : map.keySet()) {
                if (!k.a(str) && (strArr = (String[]) map.get(str)) != null && strArr.length >= 2) {
                    String str2 = strArr[0];
                    if (!k.a(str2)) {
                        arrayList2.add(new a(str, str2, (String[]) Arrays.copyOfRange(strArr, 1, strArr.length)));
                    }
                }
            }
            arrayList = arrayList2;
        }
        this.f3414b.f3411a.a(arrayList);
    }
}
