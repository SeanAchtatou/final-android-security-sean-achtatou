package com.helpshift.h;

import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.e.z;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.util.n;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: CustomIssueFieldDM */
public class a {

    /* renamed from: a  reason: collision with root package name */
    com.helpshift.h.a.a f3411a;

    /* renamed from: b  reason: collision with root package name */
    private j f3412b;
    private z c;

    public a(j jVar, ab abVar) {
        this.f3412b = jVar;
        this.f3411a = abVar.j();
        this.c = abVar.p();
    }

    public final Object a() {
        ArrayList<com.helpshift.h.b.a> a2 = this.f3411a.a();
        if (a2 == null || a2.size() == 0) {
            return null;
        }
        try {
            return this.c.e(a2);
        } catch (RootAPIException e) {
            n.a("Helpshift_CIF_DM", "Exception when jsonify data : " + e.getMessage(), (Throwable[]) null, (com.helpshift.p.b.a[]) null);
            return null;
        }
    }

    public final void a(Map<String, String[]> map) {
        this.f3412b.b(new b(this, map));
    }
}
