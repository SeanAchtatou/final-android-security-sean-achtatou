package com.helpshift.q;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.e.y;
import com.helpshift.common.e.z;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.k;
import com.helpshift.q.b.b;
import com.helpshift.q.b.c;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.NoSuchElementException;

/* compiled from: MetaDataDM */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public com.helpshift.q.a.a f3811a;

    /* renamed from: b  reason: collision with root package name */
    public b f3812b;
    private final com.helpshift.i.a.a c;
    private final z d;
    private final y e;
    private j f;
    private LinkedList<b> g = new LinkedList<>();

    public a(j jVar, ab abVar, com.helpshift.i.a.a aVar) {
        this.f = jVar;
        this.c = aVar;
        this.f3811a = abVar.h();
        this.d = abVar.p();
        this.e = abVar.d();
    }

    private static String[] a(String[] strArr) {
        HashSet hashSet = new HashSet();
        if (strArr != null) {
            for (String str : strArr) {
                if (!k.a(str) && str.trim().length() > 0) {
                    hashSet.add(str.trim());
                }
            }
        }
        return (String[]) hashSet.toArray(new String[0]);
    }

    public final synchronized void a(String str) {
        String format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH).format(new Date());
        ArrayList<com.helpshift.q.b.a> a2 = this.f3811a.a();
        if (a2 == null) {
            a2 = new ArrayList<>();
        }
        a2.add(new com.helpshift.q.b.a(str, format));
        int intValue = this.c.b("breadcrumbLimit").intValue();
        int size = a2.size();
        if (intValue > 0) {
            if (size > intValue) {
                a2 = new ArrayList<>(a2.subList(size - intValue, size));
            }
            this.f3811a.a(a2);
        }
    }

    private Object b() {
        return this.d.c(this.f3811a.a());
    }

    private Object c() {
        HashMap hashMap = new HashMap();
        hashMap.put("platform", this.e.b());
        hashMap.put("library-version", this.e.a());
        hashMap.put("device-model", this.e.i());
        hashMap.put("os-version", this.e.c());
        try {
            String c2 = this.c.c("sdkLanguage");
            if (k.a(c2)) {
                c2 = this.e.h();
            }
            if (!k.a(c2)) {
                hashMap.put("language-code", c2);
            }
        } catch (MissingResourceException unused) {
        }
        hashMap.put("timestamp", this.e.l());
        hashMap.put("application-identifier", this.e.g());
        String f2 = this.e.f();
        if (k.a(f2)) {
            f2 = "(unknown)";
        }
        hashMap.put("application-name", f2);
        hashMap.put("application-version", this.e.e());
        c q = this.e.q();
        HashMap hashMap2 = new HashMap();
        hashMap2.put("total-space-phone", q.f3817a);
        hashMap2.put("free-space-phone", q.f3818b);
        hashMap.put("disk-space", this.d.b(hashMap2));
        if (!this.c.a("fullPrivacy")) {
            hashMap.put("country-code", this.e.k());
            hashMap.put("carrier-name", this.e.m());
        }
        hashMap.put("network-type", this.e.n());
        hashMap.put("battery-level", this.e.p());
        hashMap.put("battery-status", this.e.o());
        return this.d.b(hashMap);
    }

    private Object b(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("api-version", this.e.r());
        hashMap.put("library-version", this.e.a());
        if (!k.a(str)) {
            hashMap.put("user-id", str);
        }
        return this.d.b(hashMap);
    }

    private synchronized Object d() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        int size = this.g.size();
        int intValue = this.c.b("debugLogLimit").intValue();
        int i = 0;
        while (i < size && i < intValue) {
            try {
                arrayList.add(this.g.removeFirst());
                i++;
            } catch (NoSuchElementException e2) {
                throw RootAPIException.a(e2);
            }
        }
        this.g.clear();
        return this.d.d(arrayList);
    }

    public final void a(b bVar) {
        this.f3812b = bVar;
    }

    private Object e() {
        Map map;
        b bVar = this.f3812b;
        if (bVar != null) {
            if (bVar != null) {
                map = bVar.a();
                if (map != null) {
                    HashMap hashMap = new HashMap(map);
                    for (Map.Entry entry : map.entrySet()) {
                        String str = (String) entry.getKey();
                        Serializable serializable = (Serializable) entry.getValue();
                        if (k.a(str) || ((serializable instanceof String) && k.a((String) serializable))) {
                            hashMap.remove(str);
                        }
                    }
                    String[] strArr = new String[0];
                    Object remove = hashMap.remove("hs-tags");
                    if (remove instanceof String[]) {
                        strArr = a((String[]) remove);
                    }
                    if (strArr.length > 0) {
                        hashMap.put("hs-tags", strArr);
                    }
                    map = hashMap;
                }
            } else {
                map = null;
            }
            this.f3811a.a(map != null ? new HashMap(map) : null);
        } else {
            map = this.f3811a.b();
        }
        if (map == null) {
            return null;
        }
        if (this.c.a("fullPrivacy")) {
            map.remove("private-data");
        }
        return this.d.c(map);
    }

    public final Object a() {
        HashMap hashMap = new HashMap();
        hashMap.put("breadcrumbs", b());
        hashMap.put(DeviceRequestsHelper.DEVICE_INFO_PARAM, c());
        hashMap.put("logs", d());
        Object e2 = e();
        if (e2 != null) {
            hashMap.put("custom_meta", e2);
        }
        hashMap.put("extra", b(this.f.h.h()));
        HashMap hashMap2 = new HashMap();
        hashMap2.put("fp_status", Boolean.valueOf(this.c.a("fullPrivacy")));
        hashMap.put("user_info", this.d.b(hashMap2));
        return this.d.b(hashMap);
    }
}
