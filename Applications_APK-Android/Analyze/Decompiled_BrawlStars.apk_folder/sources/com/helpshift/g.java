package com.helpshift;

import android.app.Application;
import android.os.Build;
import com.helpshift.f.c;
import com.helpshift.util.n;
import com.helpshift.util.q;
import java.util.Map;

/* compiled from: CoreInternal */
final class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Application f3403a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Map f3404b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ String e;
    final /* synthetic */ String f;

    g(Application application, Map map, String str, String str2, String str3, String str4) {
        this.f3403a = application;
        this.f3404b = map;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
    }

    public final void run() {
        if (!q.f4255a.get()) {
            CoreInternal.b(this.f3403a.getApplicationContext(), this.f3404b);
            n.a("Helpshift_CoreInternal", "Helpshift install :\n Flavor : " + CoreInternal.f3155a.getClass().getSimpleName() + "\n Domain : " + this.c + "\n Config : " + this.f3404b.toString() + "\n Package Id : " + this.f3403a.getPackageName() + "\n SDK version : " + "7.6.3" + "\n OS version : " + Build.VERSION.SDK_INT + "\n Device : " + Build.DEVICE);
            CoreInternal.f3155a.b(this.f3403a, this.d, this.e, this.f, this.f3404b);
            q.f4255a.compareAndSet(false, true);
            if (t.a()) {
                c.a().a(this.f3403a.getApplicationContext());
            }
        }
    }
}
