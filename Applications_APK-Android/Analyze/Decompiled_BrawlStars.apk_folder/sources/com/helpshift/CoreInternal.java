package com.helpshift;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.helpshift.a;
import com.helpshift.exceptions.HelpshiftInitializationException;
import com.helpshift.exceptions.InstallException;
import com.helpshift.p.b.c;
import com.helpshift.p.b.d;
import com.helpshift.p.g;
import com.helpshift.util.a.b;
import com.helpshift.util.n;
import com.helpshift.util.q;
import com.helpshift.util.w;
import com.helpshift.util.y;
import java.util.Map;

public class CoreInternal {

    /* renamed from: a  reason: collision with root package name */
    static a.C0131a f3155a;

    public static void a(a.C0131a aVar) {
        f3155a = aVar;
    }

    public static void a(String str, String str2) {
        if (q.d()) {
            b.a.f4235a.a(new c(str, str2));
        }
    }

    public static void a(Context context, String str) {
        if (q.d()) {
            b.a.f4235a.a(new h(str, context));
        }
    }

    public static void a(Context context, Intent intent) {
        if (q.d()) {
            b.a.f4235a.a(new i(context, intent));
        }
    }

    public static void a(Context context, Map<String, String> map) {
        if (map != null && map.size() != 0) {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            for (Map.Entry next : map.entrySet()) {
                bundle.putString((String) next.getKey(), (String) next.getValue());
            }
            intent.putExtras(bundle);
            a(context, intent);
        }
    }

    public static void a(k kVar) {
        if (q.d()) {
            b.a.f4235a.a(new j(kVar));
        }
    }

    public static void onAppForeground() {
        if (q.d()) {
            b.a.f4235a.a(new d());
        }
    }

    public static void onAppBackground() {
        if (q.d()) {
            b.a.f4235a.a(new e());
        }
    }

    static void b(Context context, Map map) {
        Object obj = map.get("enableLogging");
        boolean z = false;
        boolean z2 = (obj instanceof Boolean) && ((Boolean) obj).booleanValue();
        boolean z3 = map.get("disableErrorLogging");
        if (z3 == null) {
            z3 = map.get("disableErrorReporting");
        }
        if (z3 == null) {
            z3 = true;
        }
        if ((z3 instanceof Boolean) && ((Boolean) z3).booleanValue()) {
            z = true;
        }
        float a2 = q.b().t().a();
        n.a(g.a(context, "__hs_log_store", "7.6.3"), com.helpshift.util.b.g(context) ? 2 : 4);
        d.a(new c());
        n.a(a2);
        n.a(z2, !z);
        com.helpshift.x.a.f4350a = !z;
        if (!z) {
            com.helpshift.exceptions.a.a.a(context);
        }
        if (n.c() == 0) {
            n.b();
        }
    }

    public static void a(Application application, String str, String str2, String str3, Map map) throws InstallException {
        if (f3155a != null) {
            if (!y.a(str)) {
                str = str.trim();
            }
            String trim = !y.a(str2) ? str2.trim() : str2;
            if (!y.a(str3)) {
                str3 = str3.trim();
            }
            if (!(!y.a(str))) {
                throw new InstallException("The api key used in the Core.install(application, apiKey, domain, appId) is not valid!");
            } else if (!w.b(trim)) {
                throw new InstallException("The domain name used in the Core.install(application, apiKey, domain, appId) is not valid!");
            } else if (w.a(str3)) {
                q.f4256b.compareAndSet(false, true);
                com.helpshift.util.a.a aVar = b.a.f4235a;
                Application application2 = application;
                aVar.b(new f(application2, str, trim, str3, map));
                aVar.a(new g(application2, map, str2, str, trim, str3));
            } else {
                throw new InstallException("The app id used in the Core.install(application, apiKey, domain, appId) is not valid!");
            }
        } else {
            throw new HelpshiftInitializationException("com.helpshift.Core.init() method not called with valid arguments");
        }
    }
}
