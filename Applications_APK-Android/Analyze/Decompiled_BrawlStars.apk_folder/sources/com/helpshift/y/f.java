package com.helpshift.y;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.util.n;

/* compiled from: RetryKeyValueDbStorage */
public class f extends a {

    /* renamed from: b  reason: collision with root package name */
    private final Context f4357b;
    private SQLiteOpenHelper c;

    f(Context context) {
        this.f4357b = context;
        this.c = new d(context);
        this.f4351a = new c(this.c, "__hs__kv_backup");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            if (this.c != null) {
                this.c.close();
            }
        } catch (Exception e) {
            n.c("Helpshift_RetryKeyValue", "Error in closing DB", e);
        }
        this.c = new d(this.f4357b);
        this.f4351a = new c(this.c, "__hs__kv_backup");
    }
}
