package com.helpshift.y;

import com.helpshift.util.q;
import java.util.Arrays;
import java.util.HashSet;

/* compiled from: StorageFactory */
public class g {

    /* renamed from: b  reason: collision with root package name */
    private static g f4358b;

    /* renamed from: a  reason: collision with root package name */
    public final e f4359a = new b(new f(q.a()), new HashSet(Arrays.asList("firstDeviceSyncComplete", "hs__campaigns_icon_image_retry_counts", "sdk-language", "sdk-theme", "disableHelpshiftBranding", "screenOrientation", "data_type_device", "data_type_user", "data_type_session", "data_type_switch_user", "data_type_analytics_event", "__hs_switch_current_user", "__hs_switch_prev_user")));

    g() {
    }

    public static synchronized g a() {
        g gVar;
        synchronized (g.class) {
            if (f4358b == null) {
                f4358b = new g();
            }
            gVar = f4358b;
        }
        return gVar;
    }
}
