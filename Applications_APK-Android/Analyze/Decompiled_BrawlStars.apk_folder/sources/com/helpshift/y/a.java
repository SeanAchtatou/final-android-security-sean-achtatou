package com.helpshift.y;

import com.helpshift.util.n;
import java.io.Serializable;
import java.util.Map;

/* compiled from: BaseRetryKeyValueStorage */
public abstract class a implements e {

    /* renamed from: a  reason: collision with root package name */
    protected e f4351a;

    /* access modifiers changed from: protected */
    public abstract void b();

    public final synchronized boolean a(String str, Serializable serializable) {
        int i = 0;
        do {
            try {
            } catch (Exception e) {
                if (i == 0) {
                    n.c("Helpshift_RetryKeyValue", "Exception in setting value for key : " + str + ", retry count : " + i, e);
                } else {
                    n.c("Helpshift_RetryKeyValue", "Exception in setting value for key : " + str + ", retry count : " + i, e, new com.helpshift.p.b.a[0]);
                }
                b();
                i++;
                if (i > 1) {
                    return false;
                }
            }
        } while (i > 1);
        return false;
        return this.f4351a.a(str, serializable);
    }

    public final synchronized boolean a(Map<String, Serializable> map) {
        int i = 0;
        do {
            try {
            } catch (Exception e) {
                if (i == 0) {
                    n.c("Helpshift_RetryKeyValue", "Exception in bulk insert, retry count : " + i, e);
                } else {
                    n.c("Helpshift_RetryKeyValue", "Exception in bulk insert, retry count : " + i, e, new com.helpshift.p.b.a[0]);
                }
                b();
                i++;
                if (i > 1) {
                    return false;
                }
            }
        } while (i > 1);
        return false;
        return this.f4351a.a(map);
    }

    public final synchronized Object a(String str) {
        int i = 0;
        do {
            try {
            } catch (Exception e) {
                if (i == 0) {
                    n.c("Helpshift_RetryKeyValue", "Exception getting value for : " + str + ", retry count : " + i, e);
                } else {
                    n.c("Helpshift_RetryKeyValue", "Exception getting value for : " + str + ", retry count : " + i, e, new com.helpshift.p.b.a[0]);
                }
                b();
                i++;
                if (i > 1) {
                    return null;
                }
            }
        } while (i > 1);
        return null;
        return this.f4351a.a(str);
    }

    public final synchronized void b(String str) {
        int i = 0;
        do {
            try {
                this.f4351a.b(str);
            } catch (Exception e) {
                if (i == 0) {
                    n.c("Helpshift_RetryKeyValue", "Exception removing key : " + str + ", retry count : " + i, e);
                } else {
                    n.c("Helpshift_RetryKeyValue", "Exception removing key : " + str + ", retry count : " + i, e, new com.helpshift.p.b.a[0]);
                }
                b();
                i++;
                if (i > 1) {
                }
            }
        } while (i > 1);
    }

    public final synchronized void a() {
        int i = 0;
        do {
            try {
                this.f4351a.a();
            } catch (Exception e) {
                if (i == 0) {
                    n.c("Helpshift_RetryKeyValue", "Exception removing all keys, retry count : " + i, e);
                } else {
                    n.c("Helpshift_RetryKeyValue", "Exception removing all keys, retry count : " + i, e, new com.helpshift.p.b.a[0]);
                }
                b();
                i++;
                if (i > 1) {
                }
            }
        } while (i > 1);
    }
}
