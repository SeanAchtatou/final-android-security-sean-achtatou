package com.helpshift.y;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: KeyValueDbStorageHelper */
class d extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private static final Integer f4356a = 1;

    d(Context context) {
        super(context, "__hs__db_key_values", (SQLiteDatabase.CursorFactory) null, f4356a.intValue());
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE key_value_store(key text primary key,value blob not null);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS key_value_store;");
        onCreate(sQLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS key_value_store;");
        onCreate(sQLiteDatabase);
    }
}
