package com.helpshift.y;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* compiled from: CachedKeyValueStorage */
public class b implements e {

    /* renamed from: a  reason: collision with root package name */
    private HashMap<String, Object> f4352a = new HashMap<>();

    /* renamed from: b  reason: collision with root package name */
    private Set<String> f4353b;
    private e c;

    public b(e eVar, Set<String> set) {
        this.c = eVar;
        this.f4353b = new HashSet(set);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.y.b.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.io.Serializable]
     candidates:
      com.helpshift.y.b.a(java.lang.String, java.io.Serializable):boolean
      com.helpshift.y.e.a(java.lang.String, java.io.Serializable):boolean
      com.helpshift.y.b.a(java.lang.String, java.lang.Object):void */
    public final synchronized boolean a(String str, Serializable serializable) {
        boolean a2;
        this.f4352a.remove(str);
        a2 = this.c.a(str, serializable);
        if (a2) {
            a(str, (Object) serializable);
        }
        return a2;
    }

    public final boolean a(Map<String, Serializable> map) {
        Set<String> keySet = map.keySet();
        if (keySet != null) {
            for (String remove : keySet) {
                this.f4352a.remove(remove);
            }
        }
        boolean a2 = this.c.a(map);
        if (a2 && map != null) {
            for (Map.Entry next : map.entrySet()) {
                if (this.f4353b.contains(next.getKey())) {
                    this.f4352a.put(next.getKey(), next.getValue());
                }
            }
        }
        return a2;
    }

    public final synchronized Object a(String str) {
        if (this.f4352a.containsKey(str)) {
            return this.f4352a.get(str);
        }
        Object a2 = this.c.a(str);
        a(str, a2);
        return a2;
    }

    public final synchronized void b(String str) {
        this.c.b(str);
        this.f4352a.remove(str);
    }

    public final synchronized void a() {
        this.c.a();
        this.f4352a.clear();
    }

    private void a(String str, Object obj) {
        if (this.f4353b.contains(str)) {
            this.f4352a.put(str, obj);
        }
    }
}
