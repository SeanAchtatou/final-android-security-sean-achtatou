package com.helpshift.y;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import com.helpshift.util.d;
import com.helpshift.util.e;
import com.helpshift.util.n;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/* compiled from: KeyValueDbStorage */
public class c implements e {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteOpenHelper f4354a;

    /* renamed from: b  reason: collision with root package name */
    private String f4355b;

    public c(SQLiteOpenHelper sQLiteOpenHelper, String str) {
        this.f4354a = sQLiteOpenHelper;
        this.f4355b = str;
    }

    public final boolean a(String str, Serializable serializable) {
        if (!TextUtils.isEmpty(str) && serializable != null) {
            SQLiteDatabase writableDatabase = this.f4354a.getWritableDatabase();
            String[] strArr = {str};
            ContentValues contentValues = new ContentValues();
            contentValues.put("key", str);
            try {
                contentValues.put("value", d.a(serializable));
                if (e.a(writableDatabase, "key_value_store", "key=?", strArr)) {
                    writableDatabase.update("key_value_store", contentValues, "key=?", strArr);
                } else if (writableDatabase.insert("key_value_store", null, contentValues) == -1) {
                    throw new SQLiteException("DB insert failed and return -1");
                }
                return true;
            } catch (IOException e) {
                n.c("HS_KeyValueDB", "Error in serializing value", e);
            }
        }
        return false;
    }

    public final boolean a(Map<String, Serializable> map) {
        if (map == null || map.size() == 0) {
            return false;
        }
        SQLiteDatabase writableDatabase = this.f4354a.getWritableDatabase();
        SQLiteStatement compileStatement = writableDatabase.compileStatement("INSERT INTO key_value_store VALUES (?,?)");
        SQLiteStatement compileStatement2 = writableDatabase.compileStatement("UPDATE key_value_store SET value = ? WHERE key = ?");
        writableDatabase.beginTransaction();
        for (Map.Entry next : map.entrySet()) {
            if (!TextUtils.isEmpty((CharSequence) next.getKey()) && next.getValue() != null) {
                try {
                    String[] strArr = {(String) next.getKey()};
                    byte[] a2 = d.a(next.getValue());
                    if (e.a(writableDatabase, "key_value_store", "key=?", strArr)) {
                        compileStatement2.bindString(2, (String) next.getKey());
                        compileStatement2.bindBlob(1, a2);
                        compileStatement2.execute();
                        compileStatement2.clearBindings();
                    } else {
                        compileStatement.bindString(1, (String) next.getKey());
                        compileStatement.bindBlob(2, a2);
                        compileStatement.execute();
                        compileStatement.clearBindings();
                    }
                } catch (IOException e) {
                    n.c("HS_KeyValueDB", "Error in serializing value", e);
                }
            }
        }
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        return true;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: android.database.Cursor} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r11) {
        /*
            r10 = this;
            boolean r0 = android.text.TextUtils.isEmpty(r11)
            r1 = 0
            if (r0 == 0) goto L_0x0008
            return r1
        L_0x0008:
            android.database.sqlite.SQLiteOpenHelper r0 = r10.f4354a     // Catch:{ all -> 0x0039 }
            android.database.sqlite.SQLiteDatabase r2 = r0.getReadableDatabase()     // Catch:{ all -> 0x0039 }
            java.lang.String r5 = "key=?"
            r0 = 1
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch:{ all -> 0x0039 }
            r3 = 0
            r6[r3] = r11     // Catch:{ all -> 0x0039 }
            java.lang.String r3 = "key_value_store"
            r4 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0039 }
            boolean r2 = r11.moveToFirst()     // Catch:{ all -> 0x0036 }
            if (r2 == 0) goto L_0x0030
            byte[] r0 = r11.getBlob(r0)     // Catch:{ Exception -> 0x002f }
            java.lang.Object r1 = com.helpshift.util.d.a(r0)     // Catch:{ Exception -> 0x002f }
            goto L_0x0030
        L_0x002f:
        L_0x0030:
            if (r11 == 0) goto L_0x0035
            r11.close()
        L_0x0035:
            return r1
        L_0x0036:
            r0 = move-exception
            r1 = r11
            goto L_0x003a
        L_0x0039:
            r0 = move-exception
        L_0x003a:
            if (r1 == 0) goto L_0x003f
            r1.close()
        L_0x003f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.y.c.a(java.lang.String):java.lang.Object");
    }

    public final void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f4354a.getWritableDatabase().delete("key_value_store", "key=?", new String[]{str});
        }
    }

    public final void a() {
        this.f4354a.getWritableDatabase().delete("key_value_store", null, null);
    }
}
