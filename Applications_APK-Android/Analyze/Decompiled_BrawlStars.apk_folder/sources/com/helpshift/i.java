package com.helpshift;

import android.content.Context;
import android.content.Intent;
import com.helpshift.util.n;

/* compiled from: CoreInternal */
final class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3417a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Intent f3418b;

    i(Context context, Intent intent) {
        this.f3417a = context;
        this.f3418b = intent;
    }

    public final void run() {
        n.a("Helpshift_CoreInternal", "Handling push");
        CoreInternal.f3155a.a(this.f3417a, this.f3418b);
    }
}
