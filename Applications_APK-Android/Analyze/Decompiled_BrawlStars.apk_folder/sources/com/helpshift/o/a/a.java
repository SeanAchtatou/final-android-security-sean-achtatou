package com.helpshift.o.a;

import com.helpshift.common.e.ab;
import com.helpshift.common.e.y;
import com.helpshift.common.k;
import java.util.Locale;

/* compiled from: LocaleProviderDM */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private com.helpshift.i.a.a f3784a;

    /* renamed from: b  reason: collision with root package name */
    private y f3785b;
    private Locale c;

    public a(com.helpshift.i.a.a aVar, ab abVar) {
        this.f3784a = aVar;
        this.f3785b = abVar.d();
    }

    public final void a() {
        if (this.c == null) {
            this.c = this.f3785b.s();
        }
    }

    public final void b() {
        Locale locale = this.c;
        if (locale != null) {
            this.f3785b.a(locale);
            this.c = null;
        }
    }

    public final Locale c() {
        String c2 = this.f3784a.c("sdkLanguage");
        if (k.a(c2)) {
            return Locale.getDefault();
        }
        if (!c2.contains("_")) {
            return new Locale(c2);
        }
        String[] split = c2.split("_");
        return new Locale(split[0], split[1]);
    }

    public final Locale d() {
        String c2 = this.f3784a.c("sdkLanguage");
        if (k.a(c2)) {
            return null;
        }
        if (!c2.contains("_")) {
            return new Locale(c2);
        }
        String[] split = c2.split("_");
        return new Locale(split[0], split[1]);
    }

    public final String e() {
        String c2 = this.f3784a.c("sdkLanguage");
        return k.a(c2) ? "" : c2;
    }
}
