package com.helpshift.b.a;

import com.facebook.appevents.UserDataStore;
import com.helpshift.a.b.c;
import com.helpshift.common.b;
import com.helpshift.common.c.b.g;
import com.helpshift.common.c.b.m;
import com.helpshift.common.c.b.o;
import com.helpshift.common.c.b.q;
import com.helpshift.common.c.j;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.ab;
import com.helpshift.common.e.y;
import com.helpshift.common.e.z;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.k;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

/* compiled from: AnalyticsEventDM */
public class a implements com.helpshift.common.a {

    /* renamed from: a  reason: collision with root package name */
    public static final DecimalFormat f3201a = new DecimalFormat("0.000", new DecimalFormatSymbols(Locale.US));

    /* renamed from: b  reason: collision with root package name */
    public com.helpshift.i.a.a f3202b;
    private final j c;
    private final ab d;
    private final z e;
    private final com.helpshift.b.a f;
    private List<com.helpshift.b.b.a> g;

    public a(j jVar, ab abVar) {
        this.c = jVar;
        this.d = abVar;
        this.e = abVar.p();
        this.f = abVar.i();
        this.f3202b = jVar.f3285b;
        this.c.g.a(b.a.ANALYTICS, this);
    }

    public final synchronized void a(com.helpshift.b.b bVar, Map<String, Object> map) {
        DecimalFormat decimalFormat = f3201a;
        double currentTimeMillis = (double) System.currentTimeMillis();
        Double.isNaN(currentTimeMillis);
        com.helpshift.b.b.a aVar = new com.helpshift.b.b.a(UUID.randomUUID().toString(), bVar, map, decimalFormat.format(currentTimeMillis / 1000.0d));
        if (this.g == null) {
            this.g = new ArrayList();
        }
        this.g.add(aVar);
    }

    public final synchronized void a(com.helpshift.b.b bVar, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("id", str);
        a(bVar, hashMap);
    }

    public final synchronized void a(com.helpshift.b.b bVar) {
        a(bVar, (Map<String, Object>) null);
    }

    public final void a(b.a aVar) {
        Map<String, HashMap<String, String>> a2;
        if (aVar == b.a.ANALYTICS && (a2 = this.f.a()) != null && a2.size() > 0) {
            m c2 = c();
            for (String next : a2.keySet()) {
                try {
                    c2.a(new i(a2.get(next)));
                    this.f.a(next);
                } catch (RootAPIException e2) {
                    if (e2.c == com.helpshift.common.exception.b.NON_RETRIABLE) {
                        this.f.a(next);
                    } else {
                        throw e2;
                    }
                }
            }
        }
    }

    public void a(List<com.helpshift.b.b.a> list, c cVar) {
        if (!com.helpshift.common.j.a(list)) {
            HashMap<String, String> a2 = a(this.e.b(list), cVar);
            try {
                c().a(new i(a2));
            } catch (RootAPIException e2) {
                if (e2.c != com.helpshift.common.exception.b.NON_RETRIABLE) {
                    this.f.a(UUID.randomUUID().toString(), a2);
                    this.c.g.a(b.a.ANALYTICS, e2.a());
                    throw e2;
                }
            }
        }
    }

    private m c() {
        return new com.helpshift.common.c.b.j(new g(new q("/events/", this.c, this.d)));
    }

    private HashMap<String, String> a(String str, c cVar) {
        HashMap<String, String> a2 = o.a(cVar);
        a2.put("id", a(cVar));
        a2.put("e", str);
        y d2 = this.d.d();
        a2.put("v", d2.a());
        a2.put("os", d2.c());
        a2.put("av", d2.e());
        a2.put("dm", d2.i());
        a2.put("s", this.f3202b.c("sdkType"));
        String c2 = this.f3202b.c("pluginVersion");
        String c3 = this.f3202b.c("runtimeVersion");
        if (!k.a(c2)) {
            a2.put("pv", c2);
        }
        if (!k.a(c3)) {
            a2.put("rv", c3);
        }
        a2.put("rs", d2.j());
        String k = d2.k();
        if (!k.a(k)) {
            a2.put("cc", k);
        }
        a2.put(UserDataStore.LAST_NAME, d2.h());
        String e2 = this.c.f.e();
        if (!k.a(e2)) {
            a2.put("dln", e2);
        }
        a2.put("and_id", d2.y());
        return a2;
    }

    private String a(c cVar) {
        String a2 = new b(this.d).a(cVar);
        return k.a(a2) ? cVar.e : a2;
    }

    public final synchronized void a() {
        if (this.g != null) {
            this.g.clear();
        }
    }

    public final synchronized List<com.helpshift.b.b.a> b() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        if (this.g != null) {
            arrayList.addAll(this.g);
        }
        return arrayList;
    }
}
