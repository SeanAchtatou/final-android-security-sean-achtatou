package com.helpshift.b;

/* compiled from: AnalyticsEventType */
public enum b {
    APP_START("a"),
    LIBRARY_OPENED("o"),
    LIBRARY_OPENED_DECOMP("d"),
    SUPPORT_LAUNCH("l"),
    PERFORMED_SEARCH("s"),
    BROWSED_FAQ_LIST("b"),
    READ_FAQ("f"),
    MARKED_HELPFUL("h"),
    MARKED_UNHELPFUL("u"),
    REPORTED_ISSUE("i"),
    CONVERSATION_POSTED("p"),
    REVIEWED_APP("r"),
    OPEN_ISSUE("c"),
    OPEN_INBOX("x"),
    LIBRARY_QUIT("q"),
    MESSAGE_ADDED("m"),
    RESOLUTION_ACCEPTED("y"),
    RESOLUTION_REJECTED("n"),
    START_CSAT_RATING("sr"),
    CANCEL_CSAT_RATING("cr"),
    LINK_VIA_FAQ("fl"),
    TICKET_AVOIDED("ta"),
    TICKET_AVOIDANCE_FAILED("taf"),
    DYNAMIC_FORM_OPEN("dfo"),
    ADMIN_MESSAGE_DEEPLINK_CLICKED("ml"),
    DYNAMIC_FORM_CLOSE("dfc");
    
    public final String A;

    private b(String str) {
        this.A = str;
    }
}
