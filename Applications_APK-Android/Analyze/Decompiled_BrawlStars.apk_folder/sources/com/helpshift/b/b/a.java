package com.helpshift.b.b;

import com.helpshift.b.b;
import java.util.Map;

/* compiled from: AnalyticsEventDTO */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public final String f3206a;

    /* renamed from: b  reason: collision with root package name */
    public final b f3207b;
    public final Map<String, Object> c;
    public final String d;

    public a(String str, b bVar, Map<String, Object> map, String str2) {
        this.f3206a = str;
        this.f3207b = bVar;
        this.c = map;
        this.d = str2;
    }
}
