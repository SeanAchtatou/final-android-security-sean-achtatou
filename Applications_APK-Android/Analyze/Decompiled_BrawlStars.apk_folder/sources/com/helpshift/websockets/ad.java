package com.helpshift.websockets;

import java.io.IOException;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: SocketConnector */
public class ad {

    /* renamed from: a  reason: collision with root package name */
    final a f4297a;

    /* renamed from: b  reason: collision with root package name */
    final int f4298b;
    final z c;
    public Socket d;
    private final SSLSocketFactory e;
    private final String f;
    private final int g;

    public ad(Socket socket, a aVar, int i) {
        this(socket, aVar, i, null, null, null, 0);
    }

    public ad(Socket socket, a aVar, int i, z zVar, SSLSocketFactory sSLSocketFactory, String str, int i2) {
        this.d = socket;
        this.f4297a = aVar;
        this.f4298b = i;
        this.c = zVar;
        this.e = sSLSocketFactory;
        this.f = str;
        this.g = i2;
    }

    static void a(SSLSocket sSLSocket, String str) throws HostnameUnverifiedException {
        if (!s.f4343a.verify(str, sSLSocket.getSession())) {
            throw new HostnameUnverifiedException(sSLSocket, str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    /* access modifiers changed from: package-private */
    public void a() throws WebSocketException {
        try {
            this.c.a();
            SSLSocketFactory sSLSocketFactory = this.e;
            if (sSLSocketFactory != null) {
                try {
                    this.d = sSLSocketFactory.createSocket(this.d, this.f, this.g, true);
                    try {
                        ((SSLSocket) this.d).startHandshake();
                        if (this.d instanceof SSLSocket) {
                            a((SSLSocket) this.d, this.c.f4348a);
                        }
                    } catch (IOException e2) {
                        throw new WebSocketException(al.SSL_HANDSHAKE_ERROR, String.format("SSL handshake with the WebSocket endpoint (%s) failed: %s", this.f4297a, e2.getMessage()), e2);
                    }
                } catch (IOException e3) {
                    throw new WebSocketException(al.SOCKET_OVERLAY_ERROR, "Failed to overlay an existing socket: " + e3.getMessage(), e3);
                }
            }
        } catch (IOException e4) {
            throw new WebSocketException(al.PROXY_HANDSHAKE_ERROR, String.format("Handshake with the proxy server (%s) failed: %s", this.f4297a, e4.getMessage()), e4);
        }
    }
}
