package com.helpshift.websockets;

import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/* compiled from: HandshakeBuilder */
public class l {

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f4334b = {"Connection", "Upgrade"};
    private static final String[] c = {"Upgrade", "websocket"};
    private static final String[] d = {"Sec-WebSocket-Version", "13"};

    /* renamed from: a  reason: collision with root package name */
    String f4335a;
    private final String e;
    private final String f;
    private final URI g;
    private boolean h;
    private String i;
    private Set<String> j;
    private List<am> k;
    private List<String[]> l;

    public l(boolean z, String str, String str2, String str3) {
        this.h = z;
        this.i = str;
        this.e = str2;
        this.f = str3;
        Object[] objArr = new Object[3];
        objArr[0] = z ? "wss" : "ws";
        objArr[1] = str2;
        objArr[2] = str3;
        this.g = URI.create(String.format("%s://%s%s", objArr));
    }

    public static String a(String str, List<String[]> list) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("\r\n");
        for (String[] next : list) {
            sb.append(next[0]);
            sb.append(": ");
            sb.append(next[1]);
            sb.append("\r\n");
        }
        sb.append("\r\n");
        return sb.toString();
    }

    public final boolean b(String str) {
        synchronized (this) {
            if (this.j == null) {
                return false;
            }
            boolean contains = this.j.contains(str);
            return contains;
        }
    }

    public final void a(am amVar) {
        if (amVar != null) {
            synchronized (this) {
                if (this.k == null) {
                    this.k = new ArrayList();
                }
                this.k.add(amVar);
            }
        }
    }

    public final boolean c(String str) {
        if (str == null) {
            return false;
        }
        synchronized (this) {
            if (this.k == null) {
                return false;
            }
            for (am amVar : this.k) {
                if (amVar.f4314a.equals(str)) {
                    return true;
                }
            }
            return false;
        }
    }

    public final void a(String str, String str2) {
        if (str != null && str.length() != 0) {
            if (str2 == null) {
                str2 = "";
            }
            synchronized (this) {
                if (this.l == null) {
                    this.l = new ArrayList();
                }
                this.l.add(new String[]{str, str2});
            }
        }
    }

    public final String a() {
        return String.format("GET %s HTTP/1.1", this.f);
    }

    public final List<String[]> b() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new String[]{"Host", this.e});
        arrayList.add(f4334b);
        arrayList.add(c);
        arrayList.add(d);
        arrayList.add(new String[]{"Sec-WebSocket-Key", this.f4335a});
        Set<String> set = this.j;
        if (!(set == null || set.size() == 0)) {
            arrayList.add(new String[]{"Sec-WebSocket-Protocol", q.a(this.j, ", ")});
        }
        List<am> list = this.k;
        if (!(list == null || list.size() == 0)) {
            arrayList.add(new String[]{"Sec-WebSocket-Extensions", q.a(this.k, ", ")});
        }
        String str = this.i;
        if (!(str == null || str.length() == 0)) {
            arrayList.add(new String[]{"Authorization", "Basic " + b.a(this.i)});
        }
        List<String[]> list2 = this.l;
        if (!(list2 == null || list2.size() == 0)) {
            arrayList.addAll(this.l);
        }
        return arrayList;
    }

    public final void a(String str) {
        boolean z = false;
        if (str != null && str.length() != 0) {
            int length = str.length();
            int i2 = 0;
            while (true) {
                if (i2 < length) {
                    char charAt = str.charAt(i2);
                    if (charAt < '!' || '~' < charAt || ai.a(charAt)) {
                        break;
                    }
                    i2++;
                } else {
                    z = true;
                    break;
                }
            }
        }
        if (z) {
            synchronized (this) {
                if (this.j == null) {
                    this.j = new LinkedHashSet();
                }
                this.j.add(str);
            }
            return;
        }
        throw new IllegalArgumentException("'protocol' must be a non-empty string with characters in the range U+0021 to U+007E not including separator characters.");
    }
}
