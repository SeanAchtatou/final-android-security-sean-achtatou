package com.helpshift.websockets;

import com.helpshift.websockets.af;
import java.io.IOException;
import java.util.LinkedList;

/* compiled from: WritingThread */
class au extends at {

    /* renamed from: b  reason: collision with root package name */
    private final LinkedList<ao> f4324b = new LinkedList<>();
    private final u c;
    private boolean d;
    private ao e;
    private boolean f;
    private boolean g;

    public au(aj ajVar) {
        super("WritingThread", ajVar, ah.WRITING_THREAD);
        this.c = ajVar.s;
    }

    private static boolean b(ao aoVar) {
        return aoVar.f() || aoVar.g();
    }

    public final void c() {
        synchronized (this) {
            this.d = true;
            notifyAll();
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:1:0x0001 */
    /* JADX WARNING: Removed duplicated region for block: B:1:0x0001 A[LOOP:0: B:1:0x0001->B:39:0x0001, LOOP_START, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(com.helpshift.websockets.ao r4) {
        /*
            r3 = this;
            monitor-enter(r3)
        L_0x0001:
            boolean r0 = r3.g     // Catch:{ all -> 0x005f }
            r1 = 0
            if (r0 == 0) goto L_0x0008
            monitor-exit(r3)     // Catch:{ all -> 0x005f }
            return r1
        L_0x0008:
            boolean r0 = r3.d     // Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x002c
            com.helpshift.websockets.ao r0 = r3.e     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x0011
            goto L_0x002c
        L_0x0011:
            boolean r0 = r4.h()     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x0018
            goto L_0x002c
        L_0x0018:
            com.helpshift.websockets.aj r0 = r3.f4322a     // Catch:{ all -> 0x005f }
            int r0 = r0.m     // Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x001f
            goto L_0x002c
        L_0x001f:
            java.util.LinkedList<com.helpshift.websockets.ao> r2 = r3.f4324b     // Catch:{ all -> 0x005f }
            int r2 = r2.size()     // Catch:{ all -> 0x005f }
            if (r2 >= r0) goto L_0x0028
            goto L_0x002c
        L_0x0028:
            r3.wait()     // Catch:{ InterruptedException -> 0x0001 }
            goto L_0x0001
        L_0x002c:
            boolean r0 = b(r4)     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x0054
            java.util.LinkedList<com.helpshift.websockets.ao> r0 = r3.f4324b     // Catch:{ all -> 0x005f }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x005f }
        L_0x0038:
            boolean r2 = r0.hasNext()     // Catch:{ all -> 0x005f }
            if (r2 == 0) goto L_0x004e
            java.lang.Object r2 = r0.next()     // Catch:{ all -> 0x005f }
            com.helpshift.websockets.ao r2 = (com.helpshift.websockets.ao) r2     // Catch:{ all -> 0x005f }
            boolean r2 = b(r2)     // Catch:{ all -> 0x005f }
            if (r2 != 0) goto L_0x004b
            goto L_0x004e
        L_0x004b:
            int r1 = r1 + 1
            goto L_0x0038
        L_0x004e:
            java.util.LinkedList<com.helpshift.websockets.ao> r0 = r3.f4324b     // Catch:{ all -> 0x005f }
            r0.add(r1, r4)     // Catch:{ all -> 0x005f }
            goto L_0x0059
        L_0x0054:
            java.util.LinkedList<com.helpshift.websockets.ao> r0 = r3.f4324b     // Catch:{ all -> 0x005f }
            r0.addLast(r4)     // Catch:{ all -> 0x005f }
        L_0x0059:
            r3.notifyAll()     // Catch:{ all -> 0x005f }
            monitor-exit(r3)     // Catch:{ all -> 0x005f }
            r4 = 1
            return r4
        L_0x005f:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x005f }
            goto L_0x0063
        L_0x0062:
            throw r4
        L_0x0063:
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.au.a(com.helpshift.websockets.ao):boolean");
    }

    private void d() {
        try {
            e();
        } catch (IOException unused) {
        }
    }

    private void e() throws IOException {
        this.f4322a.g.flush();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:10|(2:12|(3:14|15|16)(2:17|18))|19|20|(2:22|23)(2:24|(2:26|(3:28|29|30)(3:31|32|33))(2:34|35))) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0023 */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0027 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int f() {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = r4.d     // Catch:{ all -> 0x003e }
            r1 = 1
            if (r0 == 0) goto L_0x0008
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            return r1
        L_0x0008:
            com.helpshift.websockets.ao r0 = r4.e     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x000e
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            return r1
        L_0x000e:
            java.util.LinkedList<com.helpshift.websockets.ao> r0 = r4.f4324b     // Catch:{ all -> 0x003e }
            int r0 = r0.size()     // Catch:{ all -> 0x003e }
            r2 = 3
            r3 = 0
            if (r0 != 0) goto L_0x0023
            boolean r0 = r4.f     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x0020
            r4.f = r3     // Catch:{ all -> 0x003e }
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            return r2
        L_0x0020:
            r4.wait()     // Catch:{ InterruptedException -> 0x0023 }
        L_0x0023:
            boolean r0 = r4.d     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x0029
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            return r1
        L_0x0029:
            java.util.LinkedList<com.helpshift.websockets.ao> r0 = r4.f4324b     // Catch:{ all -> 0x003e }
            int r0 = r0.size()     // Catch:{ all -> 0x003e }
            if (r0 != 0) goto L_0x003c
            boolean r0 = r4.f     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x0039
            r4.f = r3     // Catch:{ all -> 0x003e }
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            return r2
        L_0x0039:
            r0 = 2
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            return r0
        L_0x003c:
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            return r3
        L_0x003e:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.au.f():int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        r3 = com.helpshift.websockets.ao.a(r2, r11.c);
        r4 = r11.f4322a.c.a().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0034, code lost:
        if (r4.hasNext() == false) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        r4.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003a, code lost:
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003d, code lost:
        if (r11.e == null) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0045, code lost:
        if (r3.e() == false) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0047, code lost:
        r11.e = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0049, code lost:
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004a, code lost:
        if (r4 == false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004c, code lost:
        r3 = r11.f4322a.c.a().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005c, code lost:
        if (r3.hasNext() == false) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005e, code lost:
        r3.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        if (r3.e() == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0068, code lost:
        h();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r4 = r11.f4322a.g;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0071, code lost:
        if (r3.f4318a == false) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0073, code lost:
        r6 = 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0076, code lost:
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0079, code lost:
        if (r3.f4319b == false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x007b, code lost:
        r7 = 64;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x007e, code lost:
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x007f, code lost:
        r6 = r6 | r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0082, code lost:
        if (r3.c == false) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0084, code lost:
        r7 = 32;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0087, code lost:
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0088, code lost:
        r6 = r6 | r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x008b, code lost:
        if (r3.d == false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x008d, code lost:
        r7 = 16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0090, code lost:
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0091, code lost:
        r4.write((r6 | r7) | (r3.e & 15));
        r6 = r3.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00a5, code lost:
        if (r6 > 125) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a7, code lost:
        r6 = r6 | 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00aa, code lost:
        if (r6 > 65535) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ac, code lost:
        r6 = 254;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00af, code lost:
        r6 = 255;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00b1, code lost:
        r4.write(r6);
        r6 = r3.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00b8, code lost:
        if (r6 > 125) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00bb, code lost:
        if (r6 > 65535) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00bd, code lost:
        r4.write((r6 >> 8) & 255);
        r4.write(r6 & 255);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00c9, code lost:
        r4.write(0);
        r4.write(0);
        r4.write(0);
        r4.write(0);
        r4.write((r6 >> 24) & 255);
        r4.write((r6 >> 16) & 255);
        r4.write((r6 >> 8) & 255);
        r4.write(r6 & 255);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00ec, code lost:
        r6 = com.helpshift.websockets.q.a(4);
        r4.write(r6);
        r7 = r3.g;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00f6, code lost:
        if (r7 != null) goto L_0x00f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00fa, code lost:
        if (r5 >= r7.length) goto L_0x010a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00fc, code lost:
        r4.write((r7[r5] ^ r6[r5 % 4]) & 255);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0107, code lost:
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x010a, code lost:
        r11.f4322a.c.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0115, code lost:
        if (r2.f() != false) goto L_0x0138;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x011b, code lost:
        if (r2.g() == false) goto L_0x011e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0122, code lost:
        if (b(r12) != false) goto L_0x0126;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0126, code lost:
        r2 = java.lang.System.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0130, code lost:
        if (1000 >= (r2 - r0)) goto L_0x0004;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0132, code lost:
        g();
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0138, code lost:
        g();
        r0 = java.lang.System.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0141, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0142, code lost:
        r1 = com.helpshift.websockets.al.IO_ERROR_IN_WRITING;
        r0 = new com.helpshift.websockets.WebSocketException(r1, "An I/O error occurred when a frame was tried to be sent: " + r12.getMessage(), r12);
        r12 = r11.f4322a.c;
        r12.a(r0);
        r12.b(r0, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0168, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (b(r12) == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0019, code lost:
        g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r12) throws com.helpshift.websockets.WebSocketException {
        /*
            r11 = this;
            long r0 = java.lang.System.currentTimeMillis()
        L_0x0004:
            monitor-enter(r11)
            java.util.LinkedList<com.helpshift.websockets.ao> r2 = r11.f4324b     // Catch:{ all -> 0x0169 }
            java.lang.Object r2 = r2.poll()     // Catch:{ all -> 0x0169 }
            com.helpshift.websockets.ao r2 = (com.helpshift.websockets.ao) r2     // Catch:{ all -> 0x0169 }
            r11.notifyAll()     // Catch:{ all -> 0x0169 }
            if (r2 != 0) goto L_0x001d
            monitor-exit(r11)     // Catch:{ all -> 0x0169 }
            boolean r12 = r11.b(r12)
            if (r12 == 0) goto L_0x001c
            r11.g()
        L_0x001c:
            return
        L_0x001d:
            monitor-exit(r11)     // Catch:{ all -> 0x0169 }
            com.helpshift.websockets.u r3 = r11.c
            com.helpshift.websockets.ao r3 = com.helpshift.websockets.ao.a(r2, r3)
            com.helpshift.websockets.aj r4 = r11.f4322a
            com.helpshift.websockets.p r4 = r4.c
            java.util.List r4 = r4.a()
            java.util.Iterator r4 = r4.iterator()
        L_0x0030:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x003a
            r4.next()
            goto L_0x0030
        L_0x003a:
            com.helpshift.websockets.ao r4 = r11.e
            r5 = 0
            if (r4 == 0) goto L_0x0041
            r4 = 1
            goto L_0x004a
        L_0x0041:
            boolean r4 = r3.e()
            if (r4 == 0) goto L_0x0049
            r11.e = r3
        L_0x0049:
            r4 = 0
        L_0x004a:
            if (r4 == 0) goto L_0x0062
            com.helpshift.websockets.aj r3 = r11.f4322a
            com.helpshift.websockets.p r3 = r3.c
            java.util.List r3 = r3.a()
            java.util.Iterator r3 = r3.iterator()
        L_0x0058:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0111
            r3.next()
            goto L_0x0058
        L_0x0062:
            boolean r4 = r3.e()
            if (r4 == 0) goto L_0x006b
            r11.h()
        L_0x006b:
            com.helpshift.websockets.aj r4 = r11.f4322a     // Catch:{ IOException -> 0x0141 }
            com.helpshift.websockets.ar r4 = r4.g     // Catch:{ IOException -> 0x0141 }
            boolean r6 = r3.f4318a     // Catch:{ IOException -> 0x0141 }
            if (r6 == 0) goto L_0x0076
            r6 = 128(0x80, float:1.794E-43)
            goto L_0x0077
        L_0x0076:
            r6 = 0
        L_0x0077:
            boolean r7 = r3.f4319b     // Catch:{ IOException -> 0x0141 }
            if (r7 == 0) goto L_0x007e
            r7 = 64
            goto L_0x007f
        L_0x007e:
            r7 = 0
        L_0x007f:
            r6 = r6 | r7
            boolean r7 = r3.c     // Catch:{ IOException -> 0x0141 }
            if (r7 == 0) goto L_0x0087
            r7 = 32
            goto L_0x0088
        L_0x0087:
            r7 = 0
        L_0x0088:
            r6 = r6 | r7
            boolean r7 = r3.d     // Catch:{ IOException -> 0x0141 }
            if (r7 == 0) goto L_0x0090
            r7 = 16
            goto L_0x0091
        L_0x0090:
            r7 = 0
        L_0x0091:
            r6 = r6 | r7
            int r7 = r3.e     // Catch:{ IOException -> 0x0141 }
            r7 = r7 & 15
            r6 = r6 | r7
            r4.write(r6)     // Catch:{ IOException -> 0x0141 }
            int r6 = r3.i()     // Catch:{ IOException -> 0x0141 }
            r7 = 65535(0xffff, float:9.1834E-41)
            r8 = 125(0x7d, float:1.75E-43)
            r9 = 255(0xff, float:3.57E-43)
            if (r6 > r8) goto L_0x00aa
            r6 = r6 | 128(0x80, float:1.794E-43)
            goto L_0x00b1
        L_0x00aa:
            if (r6 > r7) goto L_0x00af
            r6 = 254(0xfe, float:3.56E-43)
            goto L_0x00b1
        L_0x00af:
            r6 = 255(0xff, float:3.57E-43)
        L_0x00b1:
            r4.write(r6)     // Catch:{ IOException -> 0x0141 }
            int r6 = r3.i()     // Catch:{ IOException -> 0x0141 }
            if (r6 > r8) goto L_0x00bb
            goto L_0x00ec
        L_0x00bb:
            if (r6 > r7) goto L_0x00c9
            int r7 = r6 >> 8
            r7 = r7 & r9
            r4.write(r7)     // Catch:{ IOException -> 0x0141 }
            r6 = r6 & 255(0xff, float:3.57E-43)
            r4.write(r6)     // Catch:{ IOException -> 0x0141 }
            goto L_0x00ec
        L_0x00c9:
            r4.write(r5)     // Catch:{ IOException -> 0x0141 }
            r4.write(r5)     // Catch:{ IOException -> 0x0141 }
            r4.write(r5)     // Catch:{ IOException -> 0x0141 }
            r4.write(r5)     // Catch:{ IOException -> 0x0141 }
            int r7 = r6 >> 24
            r7 = r7 & r9
            r4.write(r7)     // Catch:{ IOException -> 0x0141 }
            int r7 = r6 >> 16
            r7 = r7 & r9
            r4.write(r7)     // Catch:{ IOException -> 0x0141 }
            int r7 = r6 >> 8
            r7 = r7 & r9
            r4.write(r7)     // Catch:{ IOException -> 0x0141 }
            r6 = r6 & 255(0xff, float:3.57E-43)
            r4.write(r6)     // Catch:{ IOException -> 0x0141 }
        L_0x00ec:
            r6 = 4
            byte[] r6 = com.helpshift.websockets.q.a(r6)     // Catch:{ IOException -> 0x0141 }
            r4.write(r6)     // Catch:{ IOException -> 0x0141 }
            byte[] r7 = r3.g     // Catch:{ IOException -> 0x0141 }
            if (r7 != 0) goto L_0x00f9
            goto L_0x010a
        L_0x00f9:
            int r8 = r7.length     // Catch:{ IOException -> 0x0141 }
            if (r5 >= r8) goto L_0x010a
            byte r8 = r7[r5]     // Catch:{ IOException -> 0x0141 }
            int r10 = r5 % 4
            byte r10 = r6[r10]     // Catch:{ IOException -> 0x0141 }
            r8 = r8 ^ r10
            r8 = r8 & r9
            r4.write(r8)     // Catch:{ IOException -> 0x0141 }
            int r5 = r5 + 1
            goto L_0x00f9
        L_0x010a:
            com.helpshift.websockets.aj r4 = r11.f4322a
            com.helpshift.websockets.p r4 = r4.c
            r4.a(r3)
        L_0x0111:
            boolean r3 = r2.f()
            if (r3 != 0) goto L_0x0138
            boolean r2 = r2.g()
            if (r2 == 0) goto L_0x011e
            goto L_0x0138
        L_0x011e:
            boolean r2 = r11.b(r12)
            if (r2 != 0) goto L_0x0126
            goto L_0x0004
        L_0x0126:
            long r2 = java.lang.System.currentTimeMillis()
            r4 = 1000(0x3e8, double:4.94E-321)
            long r6 = r2 - r0
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 >= 0) goto L_0x0004
            r11.g()
            r0 = r2
            goto L_0x0004
        L_0x0138:
            r11.g()
            long r0 = java.lang.System.currentTimeMillis()
            goto L_0x0004
        L_0x0141:
            r12 = move-exception
            com.helpshift.websockets.WebSocketException r0 = new com.helpshift.websockets.WebSocketException
            com.helpshift.websockets.al r1 = com.helpshift.websockets.al.IO_ERROR_IN_WRITING
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "An I/O error occurred when a frame was tried to be sent: "
            r2.append(r4)
            java.lang.String r4 = r12.getMessage()
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.<init>(r1, r2, r12)
            com.helpshift.websockets.aj r12 = r11.f4322a
            com.helpshift.websockets.p r12 = r12.c
            r12.a(r0)
            r12.b(r0, r3)
            throw r0
        L_0x0169:
            r12 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0169 }
            goto L_0x016d
        L_0x016c:
            throw r12
        L_0x016d:
            goto L_0x016c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.au.a(boolean):void");
    }

    private boolean b(boolean z) {
        return z || this.f4322a.k || this.f || this.e != null;
    }

    private void g() throws WebSocketException {
        try {
            e();
            synchronized (this) {
                this.f = false;
            }
        } catch (IOException e2) {
            al alVar = al.FLUSH_ERROR;
            WebSocketException webSocketException = new WebSocketException(alVar, "Flushing frames to the server failed: " + e2.getMessage(), e2);
            p pVar = this.f4322a.c;
            pVar.a(webSocketException);
            pVar.b(webSocketException, null);
            throw webSocketException;
        }
    }

    private void h() {
        boolean z;
        af afVar = this.f4322a.f4310b;
        synchronized (afVar) {
            as asVar = afVar.f4301a;
            if (asVar == as.CLOSING || asVar == as.CLOSED) {
                z = false;
            } else {
                afVar.a(af.a.CLIENT);
                z = true;
            }
        }
        if (z) {
            this.f4322a.c.a(as.CLOSING);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:14|15) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        a(true);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x001d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r6 = this;
            r0 = 1
            com.helpshift.websockets.aj r1 = r6.f4322a     // Catch:{ all -> 0x0021 }
            r1.b()     // Catch:{ all -> 0x0021 }
        L_0x0006:
            int r1 = r6.f()     // Catch:{ all -> 0x0021 }
            if (r1 != r0) goto L_0x000d
            goto L_0x001d
        L_0x000d:
            r2 = 3
            if (r1 != r2) goto L_0x0014
            r6.d()     // Catch:{ all -> 0x0021 }
            goto L_0x0006
        L_0x0014:
            r2 = 2
            if (r1 != r2) goto L_0x0018
            goto L_0x0006
        L_0x0018:
            r1 = 0
            r6.a(r1)     // Catch:{ WebSocketException -> 0x001d }
            goto L_0x0006
        L_0x001d:
            r6.a(r0)     // Catch:{ WebSocketException -> 0x0048 }
            goto L_0x0048
        L_0x0021:
            r1 = move-exception
            com.helpshift.websockets.WebSocketException r2 = new com.helpshift.websockets.WebSocketException
            com.helpshift.websockets.al r3 = com.helpshift.websockets.al.UNEXPECTED_ERROR_IN_WRITING_THREAD
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "An uncaught throwable was detected in the writing thread: "
            r4.append(r5)
            java.lang.String r5 = r1.getMessage()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.<init>(r3, r4, r1)
            com.helpshift.websockets.aj r1 = r6.f4322a
            com.helpshift.websockets.p r1 = r1.c
            r1.a(r2)
            r1.b(r2)
        L_0x0048:
            monitor-enter(r6)
            r6.g = r0     // Catch:{ all -> 0x0057 }
            r6.notifyAll()     // Catch:{ all -> 0x0057 }
            monitor-exit(r6)     // Catch:{ all -> 0x0057 }
            com.helpshift.websockets.aj r0 = r6.f4322a
            com.helpshift.websockets.ao r1 = r6.e
            r0.b(r1)
            return
        L_0x0057:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0057 }
            goto L_0x005b
        L_0x005a:
            throw r0
        L_0x005b:
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.au.a():void");
    }
}
