package com.helpshift.websockets;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: SocketFactorySettings */
public class ae {

    /* renamed from: a  reason: collision with root package name */
    private SocketFactory f4299a;

    /* renamed from: b  reason: collision with root package name */
    private SSLSocketFactory f4300b;
    private SSLContext c;

    ae() {
    }

    public final SocketFactory a(boolean z) {
        if (z) {
            SSLContext sSLContext = this.c;
            if (sSLContext != null) {
                return sSLContext.getSocketFactory();
            }
            SSLSocketFactory sSLSocketFactory = this.f4300b;
            if (sSLSocketFactory != null) {
                return sSLSocketFactory;
            }
            return SSLSocketFactory.getDefault();
        }
        SocketFactory socketFactory = this.f4299a;
        if (socketFactory != null) {
            return socketFactory;
        }
        return SocketFactory.getDefault();
    }
}
