package com.helpshift.websockets;

/* compiled from: Huffman */
class n {

    /* renamed from: a  reason: collision with root package name */
    private final int f4337a;

    /* renamed from: b  reason: collision with root package name */
    private final int f4338b;
    private final int[] c;
    private final int[] d;

    public n(int[] iArr) {
        this.f4337a = Math.max(q.a(iArr), 1);
        this.f4338b = q.b(iArr);
        Object[] objArr = new Object[2];
        this.c = a(a(iArr, this.f4338b), this.f4338b, objArr);
        this.d = a(iArr, (int[]) objArr[0], ((Integer) objArr[1]).intValue());
    }

    private static int[] a(int[] iArr, int i) {
        int[] iArr2 = new int[(i + 1)];
        for (int i2 : iArr) {
            iArr2[i2] = iArr2[i2] + 1;
        }
        return iArr2;
    }

    private static int[] a(int[] iArr, int[] iArr2, int i) {
        int[] iArr3 = new int[(i + 1)];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            int i3 = iArr[i2];
            if (i3 != 0) {
                int i4 = iArr2[i3];
                iArr2[i3] = i4 + 1;
                iArr3[i4] = i2;
            }
        }
        return iArr3;
    }

    public final int a(c cVar, int[] iArr) throws k {
        int b2;
        for (int i = this.f4337a; i <= this.f4338b; i++) {
            int i2 = this.c[i];
            if (i2 >= 0 && i2 >= (b2 = cVar.b(iArr[0], i))) {
                int i3 = this.d[b2];
                iArr[0] = iArr[0] + i;
                return i3;
            }
        }
        throw new k(String.format("[%s] Bad code at the bit index '%d'.", getClass().getSimpleName(), Integer.valueOf(iArr[0])));
    }

    /* JADX WARN: Type inference failed for: r9v0, types: [java.lang.Object[]] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int[] a(int[] r7, int r8, java.lang.Object[] r9) {
        /*
            r0 = 1
            int r8 = r8 + r0
            int[] r1 = new int[r8]
            r2 = 0
            r3 = 0
        L_0x0006:
            if (r3 >= r8) goto L_0x000e
            r4 = -1
            r1[r3] = r4
            int r3 = r3 + 1
            goto L_0x0006
        L_0x000e:
            r7[r2] = r2
            int[] r8 = new int[r8]
            r3 = 1
            r4 = 0
            r5 = 0
        L_0x0015:
            int r6 = r7.length
            if (r3 >= r6) goto L_0x0029
            int r4 = r3 + -1
            r4 = r7[r4]
            int r5 = r5 + r4
            int r5 = r5 << r0
            r8[r3] = r5
            r4 = r7[r3]
            int r4 = r4 + r5
            int r4 = r4 - r0
            r1[r3] = r4
            int r3 = r3 + 1
            goto L_0x0015
        L_0x0029:
            r9[r2] = r8
            java.lang.Integer r7 = java.lang.Integer.valueOf(r4)
            r9[r0] = r7
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.n.a(int[], int, java.lang.Object[]):int[]");
    }
}
