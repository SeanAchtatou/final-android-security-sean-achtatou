package com.helpshift.websockets;

/* compiled from: FixedLiteralLengthHuffman */
class j extends n {

    /* renamed from: a  reason: collision with root package name */
    private static final j f4333a = new j();

    private j() {
        super(b());
    }

    private static int[] b() {
        int[] iArr = new int[288];
        int i = 0;
        while (i < 144) {
            iArr[i] = 8;
            i++;
        }
        while (i < 256) {
            iArr[i] = 9;
            i++;
        }
        while (i < 280) {
            iArr[i] = 7;
            i++;
        }
        while (i < 288) {
            iArr[i] = 8;
            i++;
        }
        return iArr;
    }

    public static j a() {
        return f4333a;
    }
}
