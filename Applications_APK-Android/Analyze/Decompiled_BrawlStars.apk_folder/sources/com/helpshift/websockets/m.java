package com.helpshift.websockets;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: HandshakeReader */
class m {

    /* renamed from: a  reason: collision with root package name */
    private final aj f4336a;

    public m(aj ajVar) {
        this.f4336a = ajVar;
    }

    public final Map<String, List<String>> a(ap apVar, String str) throws WebSocketException {
        String str2;
        ag a2 = a(apVar);
        Map<String, List<String>> b2 = b(apVar);
        if (a2.f4305a == 101) {
            a(a2, b2);
            b(a2, b2);
            List list = b2.get("Sec-WebSocket-Accept");
            if (list != null) {
                String str3 = (String) list.get(0);
                try {
                    if (!b.a(MessageDigest.getInstance("SHA-1").digest(q.a(str + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"))).equals(str3)) {
                        throw new OpeningHandshakeException(al.UNEXPECTED_SEC_WEBSOCKET_ACCEPT_HEADER, "The value of 'Sec-WebSocket-Accept' header is different from the expected one.", a2, b2);
                    }
                } catch (Exception unused) {
                }
                c(a2, b2);
                List list2 = b2.get("Sec-WebSocket-Protocol");
                if (!(list2 == null || (str2 = (String) list2.get(0)) == null || str2.length() == 0)) {
                    if (this.f4336a.e.b(str2)) {
                        this.f4336a.i = str2;
                    } else {
                        al alVar = al.UNSUPPORTED_PROTOCOL;
                        throw new OpeningHandshakeException(alVar, "The protocol contained in the Sec-WebSocket-Protocol header is not supported: " + str2, a2, b2);
                    }
                }
                return b2;
            }
            throw new OpeningHandshakeException(al.NO_SEC_WEBSOCKET_ACCEPT_HEADER, "The opening handshake response does not contain 'Sec-WebSocket-Accept' header.", a2, b2);
        }
        byte[] a3 = a(b2, apVar);
        al alVar2 = al.NOT_SWITCHING_PROTOCOLS;
        throw new OpeningHandshakeException(alVar2, "The status code of the opening handshake response is not '101 Switching Protocols'. The status line is: " + a2, a2, b2, a3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Map<java.lang.String, java.util.List<java.lang.String>> b(com.helpshift.websockets.ap r6) throws com.helpshift.websockets.WebSocketException {
        /*
            r5 = this;
            java.util.TreeMap r0 = new java.util.TreeMap
            java.util.Comparator r1 = java.lang.String.CASE_INSENSITIVE_ORDER
            r0.<init>(r1)
            r1 = 0
        L_0x0008:
            java.lang.String r2 = "UTF-8"
            java.lang.String r2 = com.helpshift.websockets.q.a(r6, r2)     // Catch:{ IOException -> 0x004d }
            if (r2 == 0) goto L_0x0043
            int r3 = r2.length()
            if (r3 != 0) goto L_0x0017
            goto L_0x0043
        L_0x0017:
            r3 = 0
            char r3 = r2.charAt(r3)
            r4 = 32
            if (r3 == r4) goto L_0x0034
            r4 = 9
            if (r3 != r4) goto L_0x0025
            goto L_0x0034
        L_0x0025:
            if (r1 == 0) goto L_0x002e
            java.lang.String r1 = r1.toString()
            a(r0, r1)
        L_0x002e:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r2)
            goto L_0x0008
        L_0x0034:
            if (r1 != 0) goto L_0x0037
            goto L_0x0008
        L_0x0037:
            java.lang.String r3 = "^[ \t]+"
            java.lang.String r4 = " "
            java.lang.String r2 = r2.replaceAll(r3, r4)
            r1.append(r2)
            goto L_0x0008
        L_0x0043:
            if (r1 == 0) goto L_0x004c
            java.lang.String r6 = r1.toString()
            a(r0, r6)
        L_0x004c:
            return r0
        L_0x004d:
            r6 = move-exception
            com.helpshift.websockets.WebSocketException r0 = new com.helpshift.websockets.WebSocketException
            com.helpshift.websockets.al r1 = com.helpshift.websockets.al.HTTP_HEADER_FAILURE
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "An error occurred while HTTP header section was being read: "
            r2.append(r3)
            java.lang.String r3 = r6.getMessage()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.<init>(r1, r2, r6)
            goto L_0x006c
        L_0x006b:
            throw r0
        L_0x006c:
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.m.b(com.helpshift.websockets.ap):java.util.Map");
    }

    private static void a(Map<String, List<String>> map, String str) {
        String[] split = str.split(":", 2);
        if (split.length >= 2) {
            String trim = split[0].trim();
            String trim2 = split[1].trim();
            List list = map.get(trim);
            if (list == null) {
                list = new ArrayList();
                map.put(trim, list);
            }
            list.add(trim2);
        }
    }

    private byte[] a(Map<String, List<String>> map, ap apVar) {
        int a2 = a(map);
        if (a2 <= 0) {
            return null;
        }
        try {
            byte[] bArr = new byte[a2];
            apVar.a(bArr, a2);
            return bArr;
        } catch (Throwable unused) {
            return null;
        }
    }

    private static int a(Map<String, List<String>> map) {
        try {
            return Integer.parseInt((String) map.get("Content-Length").get(0));
        } catch (Exception unused) {
            return -1;
        }
    }

    private static void a(ag agVar, Map<String, List<String>> map) throws WebSocketException {
        List<String> list = map.get("Upgrade");
        if (list == null || list.size() == 0) {
            throw new OpeningHandshakeException(al.NO_UPGRADE_HEADER, "The opening handshake response does not contain 'Upgrade' header.", agVar, map);
        }
        for (String split : list) {
            String[] split2 = split.split("\\s*,\\s*");
            int length = split2.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    if (!"websocket".equalsIgnoreCase(split2[i])) {
                        i++;
                    } else {
                        return;
                    }
                }
            }
        }
        throw new OpeningHandshakeException(al.NO_WEBSOCKET_IN_UPGRADE_HEADER, "'websocket' was not found in 'Upgrade' header.", agVar, map);
    }

    private static void b(ag agVar, Map<String, List<String>> map) throws WebSocketException {
        List<String> list = map.get("Connection");
        if (list == null || list.size() == 0) {
            throw new OpeningHandshakeException(al.NO_CONNECTION_HEADER, "The opening handshake response does not contain 'Connection' header.", agVar, map);
        }
        for (String split : list) {
            String[] split2 = split.split("\\s*,\\s*");
            int length = split2.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    if (!"Upgrade".equalsIgnoreCase(split2[i])) {
                        i++;
                    } else {
                        return;
                    }
                }
            }
        }
        throw new OpeningHandshakeException(al.NO_UPGRADE_IN_CONNECTION_HEADER, "'Upgrade' was not found in 'Connection' header.", agVar, map);
    }

    private void c(ag agVar, Map<String, List<String>> map) throws WebSocketException {
        List<String> list = map.get("Sec-WebSocket-Extensions");
        if (list != null && list.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (String split : list) {
                String[] split2 = split.split("\\s*,\\s*");
                int length = split2.length;
                int i = 0;
                while (true) {
                    if (i < length) {
                        String str = split2[i];
                        am a2 = am.a(str);
                        if (a2 != null) {
                            String str2 = a2.f4314a;
                            if (this.f4336a.e.c(str2)) {
                                a2.a();
                                arrayList.add(a2);
                                i++;
                            } else {
                                al alVar = al.UNSUPPORTED_EXTENSION;
                                throw new OpeningHandshakeException(alVar, "The extension contained in the Sec-WebSocket-Extensions header is not supported: " + str2, agVar, map);
                            }
                        } else {
                            al alVar2 = al.EXTENSION_PARSE_ERROR;
                            throw new OpeningHandshakeException(alVar2, "The value in 'Sec-WebSocket-Extensions' failed to be parsed: " + str, agVar, map);
                        }
                    }
                }
            }
            a(agVar, map, arrayList);
            this.f4336a.h = arrayList;
        }
    }

    private static void a(ag agVar, Map<String, List<String>> map, List<am> list) throws WebSocketException {
        am amVar = null;
        for (am next : list) {
            if (next instanceof u) {
                if (amVar == null) {
                    amVar = next;
                } else {
                    throw new OpeningHandshakeException(al.EXTENSIONS_CONFLICT, String.format("'%s' extension and '%s' extension conflict with each other.", amVar.f4314a, next.f4314a), agVar, map);
                }
            }
        }
    }

    private static ag a(ap apVar) throws WebSocketException {
        try {
            String a2 = q.a(apVar, "UTF-8");
            if (a2 == null || a2.length() == 0) {
                throw new WebSocketException(al.STATUS_LINE_EMPTY, "The status line of the opening handshake response is empty.");
            }
            try {
                return new ag(a2);
            } catch (Exception unused) {
                al alVar = al.STATUS_LINE_BAD_FORMAT;
                throw new WebSocketException(alVar, "The status line of the opening handshake response is badly formatted. The status line is: " + a2);
            }
        } catch (IOException e) {
            al alVar2 = al.OPENING_HANDSHAKE_RESPONSE_FAILURE;
            throw new WebSocketException(alVar2, "Failed to read an opening handshake response from the server: " + e.getMessage(), e);
        }
    }
}
