package com.helpshift.websockets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.Map;

/* compiled from: WebSocket */
public class aj {
    private boolean A;
    private Object B = new Object();
    private ao C;

    /* renamed from: a  reason: collision with root package name */
    public final ad f4309a;

    /* renamed from: b  reason: collision with root package name */
    final af f4310b;
    public final p c;
    final Object d = new Object();
    public l e;
    ap f;
    ar g;
    List<am> h;
    String i;
    boolean j;
    boolean k = true;
    boolean l = true;
    int m;
    boolean n;
    boolean o;
    boolean p;
    boolean q;
    ao r;
    u s;
    private final an t;
    private final x u;
    private final y v;
    private ab w;
    private au x;
    private Map<String, List<String>> y;
    private int z;

    public aj(an anVar, boolean z2, String str, String str2, String str3, ad adVar) {
        this.t = anVar;
        this.f4309a = adVar;
        this.f4310b = new af();
        this.e = new l(z2, str, str2, str3);
        this.c = new p(this);
        this.u = new x(this, new d());
        this.v = new y(this, new d());
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        if (a(as.CREATED)) {
            e();
        }
        super.finalize();
    }

    /* access modifiers changed from: package-private */
    public boolean a(as asVar) {
        boolean z2;
        synchronized (this.f4310b) {
            z2 = this.f4310b.f4301a == asVar;
        }
        return z2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        r2.c.a(com.helpshift.websockets.as.d);
        r6 = r2.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0031, code lost:
        monitor-enter(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r5 = r2.w;
        r0 = r2.x;
        r2.w = null;
        r2.x = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        monitor-exit(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        if (r5 == null) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003e, code lost:
        r5.a(10000);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0041, code lost:
        if (r0 == null) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0043, code lost:
        r0.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0046, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.helpshift.websockets.aj a(int r3, java.lang.String r4, long r5) {
        /*
            r2 = this;
            com.helpshift.websockets.af r5 = r2.f4310b
            monitor-enter(r5)
            int[] r6 = com.helpshift.websockets.ak.f4311a     // Catch:{ all -> 0x0057 }
            com.helpshift.websockets.af r0 = r2.f4310b     // Catch:{ all -> 0x0057 }
            com.helpshift.websockets.as r0 = r0.f4301a     // Catch:{ all -> 0x0057 }
            int r0 = r0.ordinal()     // Catch:{ all -> 0x0057 }
            r6 = r6[r0]     // Catch:{ all -> 0x0057 }
            r0 = 1
            if (r6 == r0) goto L_0x004a
            r0 = 2
            if (r6 == r0) goto L_0x0017
            monitor-exit(r5)     // Catch:{ all -> 0x0057 }
            return r2
        L_0x0017:
            com.helpshift.websockets.af r6 = r2.f4310b     // Catch:{ all -> 0x0057 }
            com.helpshift.websockets.af$a r0 = com.helpshift.websockets.af.a.CLIENT     // Catch:{ all -> 0x0057 }
            r6.a(r0)     // Catch:{ all -> 0x0057 }
            com.helpshift.websockets.ao r3 = com.helpshift.websockets.ao.a(r3, r4)     // Catch:{ all -> 0x0057 }
            r2.a(r3)     // Catch:{ all -> 0x0057 }
            monitor-exit(r5)     // Catch:{ all -> 0x0057 }
            com.helpshift.websockets.p r3 = r2.c
            com.helpshift.websockets.as r4 = com.helpshift.websockets.as.CLOSING
            r3.a(r4)
            r3 = 10000(0x2710, double:4.9407E-320)
            java.lang.Object r6 = r2.d
            monitor-enter(r6)
            com.helpshift.websockets.ab r5 = r2.w     // Catch:{ all -> 0x0047 }
            com.helpshift.websockets.au r0 = r2.x     // Catch:{ all -> 0x0047 }
            r1 = 0
            r2.w = r1     // Catch:{ all -> 0x0047 }
            r2.x = r1     // Catch:{ all -> 0x0047 }
            monitor-exit(r6)     // Catch:{ all -> 0x0047 }
            if (r5 == 0) goto L_0x0041
            r5.a(r3)
        L_0x0041:
            if (r0 == 0) goto L_0x0046
            r0.c()
        L_0x0046:
            return r2
        L_0x0047:
            r3 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0047 }
            throw r3
        L_0x004a:
            com.helpshift.websockets.h r3 = new com.helpshift.websockets.h     // Catch:{ all -> 0x0057 }
            r3.<init>(r2)     // Catch:{ all -> 0x0057 }
            r3.b()     // Catch:{ all -> 0x0057 }
            r3.start()     // Catch:{ all -> 0x0057 }
            monitor-exit(r5)     // Catch:{ all -> 0x0057 }
            return r2
        L_0x0057:
            r3 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0057 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.aj.a(int, java.lang.String, long):com.helpshift.websockets.aj");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0015, code lost:
        r0 = r9.x;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
        if (r0 != null) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0019, code lost:
        return r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001a, code lost:
        r1 = r9.z;
        r2 = r9.s;
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001f, code lost:
        if (r1 != 0) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0027, code lost:
        if (r10.i() > r1) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002f, code lost:
        if (r10.d() != false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0035, code lost:
        if (r10.c() == false) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003c, code lost:
        if (r10.b() != false) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003f, code lost:
        r2 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0041, code lost:
        r2 = com.helpshift.websockets.ao.a(r10, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0049, code lost:
        if (r2.i() > r1) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x004c, code lost:
        r3 = r2.g;
        r4 = r2.f4318a;
        r5 = new java.util.ArrayList();
        r6 = java.util.Arrays.copyOf(r3, r1);
        r2.f4318a = false;
        r2.a(r6);
        r5.add(r2);
        r2 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0064, code lost:
        if (r2 >= r3.length) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0066, code lost:
        r6 = r2 + r1;
        r2 = java.util.Arrays.copyOfRange(r3, r2, java.lang.Math.min(r6, r3.length));
        r8 = new com.helpshift.websockets.ao();
        r8.e = 0;
        r5.add(r8.a(r2));
        r2 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0081, code lost:
        if (r4 == false) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0083, code lost:
        ((com.helpshift.websockets.ao) r5.get(r5.size() - 1)).f4318a = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0091, code lost:
        r3 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0092, code lost:
        if (r3 != null) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0094, code lost:
        r0.a(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0098, code lost:
        r10 = r3.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a0, code lost:
        if (r10.hasNext() == false) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a2, code lost:
        r0.a((com.helpshift.websockets.ao) r10.next());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ac, code lost:
        return r9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.helpshift.websockets.aj a(com.helpshift.websockets.ao r10) {
        /*
            r9 = this;
            if (r10 != 0) goto L_0x0003
            return r9
        L_0x0003:
            com.helpshift.websockets.af r0 = r9.f4310b
            monitor-enter(r0)
            com.helpshift.websockets.af r1 = r9.f4310b     // Catch:{ all -> 0x00ad }
            com.helpshift.websockets.as r1 = r1.f4301a     // Catch:{ all -> 0x00ad }
            com.helpshift.websockets.as r2 = com.helpshift.websockets.as.OPEN     // Catch:{ all -> 0x00ad }
            if (r1 == r2) goto L_0x0014
            com.helpshift.websockets.as r2 = com.helpshift.websockets.as.CLOSING     // Catch:{ all -> 0x00ad }
            if (r1 == r2) goto L_0x0014
            monitor-exit(r0)     // Catch:{ all -> 0x00ad }
            return r9
        L_0x0014:
            monitor-exit(r0)     // Catch:{ all -> 0x00ad }
            com.helpshift.websockets.au r0 = r9.x
            if (r0 != 0) goto L_0x001a
            return r9
        L_0x001a:
            int r1 = r9.z
            com.helpshift.websockets.u r2 = r9.s
            r3 = 0
            if (r1 != 0) goto L_0x0023
            goto L_0x0092
        L_0x0023:
            int r4 = r10.i()
            if (r4 > r1) goto L_0x002b
            goto L_0x0092
        L_0x002b:
            boolean r4 = r10.d()
            if (r4 != 0) goto L_0x0041
            boolean r4 = r10.c()
            if (r4 == 0) goto L_0x0038
            goto L_0x0041
        L_0x0038:
            boolean r2 = r10.b()
            if (r2 != 0) goto L_0x003f
            goto L_0x0092
        L_0x003f:
            r2 = r10
            goto L_0x004c
        L_0x0041:
            com.helpshift.websockets.ao r2 = com.helpshift.websockets.ao.a(r10, r2)
            int r4 = r2.i()
            if (r4 > r1) goto L_0x004c
            goto L_0x0092
        L_0x004c:
            byte[] r3 = r2.g
            boolean r4 = r2.f4318a
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            byte[] r6 = java.util.Arrays.copyOf(r3, r1)
            r7 = 0
            r2.f4318a = r7
            r2.a(r6)
            r5.add(r2)
            r2 = r1
        L_0x0063:
            int r6 = r3.length
            if (r2 >= r6) goto L_0x0081
            int r6 = r2 + r1
            int r8 = r3.length
            int r8 = java.lang.Math.min(r6, r8)
            byte[] r2 = java.util.Arrays.copyOfRange(r3, r2, r8)
            com.helpshift.websockets.ao r8 = new com.helpshift.websockets.ao
            r8.<init>()
            r8.e = r7
            com.helpshift.websockets.ao r2 = r8.a(r2)
            r5.add(r2)
            r2 = r6
            goto L_0x0063
        L_0x0081:
            if (r4 == 0) goto L_0x0091
            int r1 = r5.size()
            r2 = 1
            int r1 = r1 - r2
            java.lang.Object r1 = r5.get(r1)
            com.helpshift.websockets.ao r1 = (com.helpshift.websockets.ao) r1
            r1.f4318a = r2
        L_0x0091:
            r3 = r5
        L_0x0092:
            if (r3 != 0) goto L_0x0098
            r0.a(r10)
            goto L_0x00ac
        L_0x0098:
            java.util.Iterator r10 = r3.iterator()
        L_0x009c:
            boolean r1 = r10.hasNext()
            if (r1 == 0) goto L_0x00ac
            java.lang.Object r1 = r10.next()
            com.helpshift.websockets.ao r1 = (com.helpshift.websockets.ao) r1
            r0.a(r1)
            goto L_0x009c
        L_0x00ac:
            return r9
        L_0x00ad:
            r10 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00ad }
            goto L_0x00b1
        L_0x00b0:
            throw r10
        L_0x00b1:
            goto L_0x00b0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.aj.a(com.helpshift.websockets.ao):com.helpshift.websockets.aj");
    }

    private static ap a(Socket socket) throws WebSocketException {
        try {
            return new ap(new BufferedInputStream(socket.getInputStream()));
        } catch (IOException e2) {
            al alVar = al.SOCKET_INPUT_STREAM_FAILURE;
            throw new WebSocketException(alVar, "Failed to get the input stream of the raw socket: " + e2.getMessage(), e2);
        }
    }

    private static ar b(Socket socket) throws WebSocketException {
        try {
            return new ar(new BufferedOutputStream(socket.getOutputStream()));
        } catch (IOException e2) {
            al alVar = al.SOCKET_OUTPUT_STREAM_FAILURE;
            throw new WebSocketException(alVar, "Failed to get the output stream from the raw socket: " + e2.getMessage(), e2);
        }
    }

    private void a(ar arVar, String str) throws WebSocketException {
        l lVar = this.e;
        lVar.f4335a = str;
        String a2 = lVar.a();
        List<String[]> b2 = this.e.b();
        String a3 = l.a(a2, b2);
        this.c.a(a2, b2);
        try {
            arVar.a(a3);
            arVar.flush();
        } catch (IOException e2) {
            al alVar = al.OPENING_HAHDSHAKE_REQUEST_FAILURE;
            throw new WebSocketException(alVar, "Failed to send an opening handshake request to the server: " + e2.getMessage(), e2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        boolean z2;
        synchronized (this.d) {
            this.o = true;
            z2 = this.n;
        }
        c();
        if (z2) {
            d();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        synchronized (this.B) {
            if (!this.A) {
                this.A = true;
                this.c.a(this.y);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.u.a();
        this.v.a();
    }

    /* access modifiers changed from: package-private */
    public final void b(ao aoVar) {
        synchronized (this.d) {
            this.q = true;
            this.C = aoVar;
            if (this.p) {
                e();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        this.u.b();
        this.v.b();
        try {
            this.f4309a.d.close();
        } catch (Throwable unused) {
        }
        synchronized (this.f4310b) {
            this.f4310b.f4301a = as.CLOSED;
        }
        this.c.a(as.CLOSED);
        this.c.a(this.r, this.C, this.f4310b.a());
    }

    private u f() {
        List<am> list = this.h;
        if (list == null) {
            return null;
        }
        for (am next : list) {
            if (next instanceof u) {
                return (u) next;
            }
        }
        return null;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:45|46|47|48|49) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:48:0x00d6 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:48:0x00d6=Splitter:B:48:0x00d6, B:23:0x004c=Splitter:B:23:0x004c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.helpshift.websockets.aj a() throws com.helpshift.websockets.WebSocketException {
        /*
            r8 = this;
            com.helpshift.websockets.af r0 = r8.f4310b
            monitor-enter(r0)
            com.helpshift.websockets.af r1 = r8.f4310b     // Catch:{ all -> 0x00f7 }
            com.helpshift.websockets.as r1 = r1.f4301a     // Catch:{ all -> 0x00f7 }
            com.helpshift.websockets.as r2 = com.helpshift.websockets.as.CREATED     // Catch:{ all -> 0x00f7 }
            if (r1 != r2) goto L_0x00ed
            com.helpshift.websockets.af r1 = r8.f4310b     // Catch:{ all -> 0x00f7 }
            com.helpshift.websockets.as r2 = com.helpshift.websockets.as.CONNECTING     // Catch:{ all -> 0x00f7 }
            r1.f4301a = r2     // Catch:{ all -> 0x00f7 }
            monitor-exit(r0)     // Catch:{ all -> 0x00f7 }
            com.helpshift.websockets.p r0 = r8.c
            com.helpshift.websockets.as r1 = com.helpshift.websockets.as.CONNECTING
            r0.a(r1)
            com.helpshift.websockets.ad r0 = r8.f4309a     // Catch:{ WebSocketException -> 0x00d7 }
            com.helpshift.websockets.z r1 = r0.c     // Catch:{ WebSocketException -> 0x00d0 }
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0023
            r1 = 1
            goto L_0x0024
        L_0x0023:
            r1 = 0
        L_0x0024:
            java.net.Socket r4 = r0.d     // Catch:{ IOException -> 0x00aa }
            com.helpshift.websockets.a r5 = r0.f4297a     // Catch:{ IOException -> 0x00aa }
            java.net.InetSocketAddress r6 = new java.net.InetSocketAddress     // Catch:{ IOException -> 0x00aa }
            java.lang.String r7 = r5.f4290a     // Catch:{ IOException -> 0x00aa }
            int r5 = r5.f4291b     // Catch:{ IOException -> 0x00aa }
            r6.<init>(r7, r5)     // Catch:{ IOException -> 0x00aa }
            int r5 = r0.f4298b     // Catch:{ IOException -> 0x00aa }
            r4.connect(r6, r5)     // Catch:{ IOException -> 0x00aa }
            java.net.Socket r4 = r0.d     // Catch:{ IOException -> 0x00aa }
            boolean r4 = r4 instanceof javax.net.ssl.SSLSocket     // Catch:{ IOException -> 0x00aa }
            if (r4 == 0) goto L_0x0047
            java.net.Socket r4 = r0.d     // Catch:{ IOException -> 0x00aa }
            javax.net.ssl.SSLSocket r4 = (javax.net.ssl.SSLSocket) r4     // Catch:{ IOException -> 0x00aa }
            com.helpshift.websockets.a r5 = r0.f4297a     // Catch:{ IOException -> 0x00aa }
            java.lang.String r5 = r5.f4290a     // Catch:{ IOException -> 0x00aa }
            com.helpshift.websockets.ad.a(r4, r5)     // Catch:{ IOException -> 0x00aa }
        L_0x0047:
            if (r1 == 0) goto L_0x004c
            r0.a()     // Catch:{ WebSocketException -> 0x00d0 }
        L_0x004c:
            com.helpshift.websockets.ad r0 = r8.f4309a     // Catch:{ WebSocketException -> 0x00d7 }
            java.net.Socket r0 = r0.d     // Catch:{ WebSocketException -> 0x00d7 }
            com.helpshift.websockets.ap r1 = a(r0)     // Catch:{ WebSocketException -> 0x00d7 }
            com.helpshift.websockets.ar r0 = b(r0)     // Catch:{ WebSocketException -> 0x00d7 }
            r2 = 16
            byte[] r2 = new byte[r2]     // Catch:{ WebSocketException -> 0x00d7 }
            com.helpshift.websockets.q.b(r2)     // Catch:{ WebSocketException -> 0x00d7 }
            java.lang.String r2 = com.helpshift.websockets.b.a(r2)     // Catch:{ WebSocketException -> 0x00d7 }
            r8.a(r0, r2)     // Catch:{ WebSocketException -> 0x00d7 }
            com.helpshift.websockets.m r3 = new com.helpshift.websockets.m     // Catch:{ WebSocketException -> 0x00d7 }
            r3.<init>(r8)     // Catch:{ WebSocketException -> 0x00d7 }
            java.util.Map r2 = r3.a(r1, r2)     // Catch:{ WebSocketException -> 0x00d7 }
            r8.f = r1     // Catch:{ WebSocketException -> 0x00d7 }
            r8.g = r0     // Catch:{ WebSocketException -> 0x00d7 }
            r8.y = r2
            com.helpshift.websockets.u r0 = r8.f()
            r8.s = r0
            com.helpshift.websockets.af r0 = r8.f4310b
            com.helpshift.websockets.as r1 = com.helpshift.websockets.as.OPEN
            r0.f4301a = r1
            com.helpshift.websockets.p r0 = r8.c
            com.helpshift.websockets.as r1 = com.helpshift.websockets.as.OPEN
            r0.a(r1)
            com.helpshift.websockets.ab r0 = new com.helpshift.websockets.ab
            r0.<init>(r8)
            com.helpshift.websockets.au r1 = new com.helpshift.websockets.au
            r1.<init>(r8)
            java.lang.Object r2 = r8.d
            monitor-enter(r2)
            r8.w = r0     // Catch:{ all -> 0x00a7 }
            r8.x = r1     // Catch:{ all -> 0x00a7 }
            monitor-exit(r2)     // Catch:{ all -> 0x00a7 }
            r0.b()
            r1.b()
            r0.start()
            r1.start()
            return r8
        L_0x00a7:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00a7 }
            throw r0
        L_0x00aa:
            r4 = move-exception
            java.lang.String r5 = "Failed to connect to %s'%s': %s"
            r6 = 3
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ WebSocketException -> 0x00d0 }
            if (r1 == 0) goto L_0x00b5
            java.lang.String r1 = "the proxy "
            goto L_0x00b7
        L_0x00b5:
            java.lang.String r1 = ""
        L_0x00b7:
            r6[r3] = r1     // Catch:{ WebSocketException -> 0x00d0 }
            com.helpshift.websockets.a r1 = r0.f4297a     // Catch:{ WebSocketException -> 0x00d0 }
            r6[r2] = r1     // Catch:{ WebSocketException -> 0x00d0 }
            r1 = 2
            java.lang.String r2 = r4.getMessage()     // Catch:{ WebSocketException -> 0x00d0 }
            r6[r1] = r2     // Catch:{ WebSocketException -> 0x00d0 }
            java.lang.String r1 = java.lang.String.format(r5, r6)     // Catch:{ WebSocketException -> 0x00d0 }
            com.helpshift.websockets.WebSocketException r2 = new com.helpshift.websockets.WebSocketException     // Catch:{ WebSocketException -> 0x00d0 }
            com.helpshift.websockets.al r3 = com.helpshift.websockets.al.SOCKET_CONNECT_ERROR     // Catch:{ WebSocketException -> 0x00d0 }
            r2.<init>(r3, r1, r4)     // Catch:{ WebSocketException -> 0x00d0 }
            throw r2     // Catch:{ WebSocketException -> 0x00d0 }
        L_0x00d0:
            r1 = move-exception
            java.net.Socket r0 = r0.d     // Catch:{ IOException -> 0x00d6 }
            r0.close()     // Catch:{ IOException -> 0x00d6 }
        L_0x00d6:
            throw r1     // Catch:{ WebSocketException -> 0x00d7 }
        L_0x00d7:
            r0 = move-exception
            com.helpshift.websockets.ad r1 = r8.f4309a
            java.net.Socket r1 = r1.d     // Catch:{ all -> 0x00df }
            r1.close()     // Catch:{ all -> 0x00df }
        L_0x00df:
            com.helpshift.websockets.af r1 = r8.f4310b
            com.helpshift.websockets.as r2 = com.helpshift.websockets.as.CLOSED
            r1.f4301a = r2
            com.helpshift.websockets.p r1 = r8.c
            com.helpshift.websockets.as r2 = com.helpshift.websockets.as.CLOSED
            r1.a(r2)
            throw r0
        L_0x00ed:
            com.helpshift.websockets.WebSocketException r1 = new com.helpshift.websockets.WebSocketException     // Catch:{ all -> 0x00f7 }
            com.helpshift.websockets.al r2 = com.helpshift.websockets.al.NOT_IN_CREATED_STATE     // Catch:{ all -> 0x00f7 }
            java.lang.String r3 = "The current state of the WebSocket is not CREATED."
            r1.<init>(r2, r3)     // Catch:{ all -> 0x00f7 }
            throw r1     // Catch:{ all -> 0x00f7 }
        L_0x00f7:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00f7 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.aj.a():com.helpshift.websockets.aj");
    }
}
