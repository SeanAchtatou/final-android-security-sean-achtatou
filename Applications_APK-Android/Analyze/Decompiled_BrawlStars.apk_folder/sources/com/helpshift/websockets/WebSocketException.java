package com.helpshift.websockets;

public class WebSocketException extends Exception {
    private static final long serialVersionUID = 1;

    /* renamed from: b  reason: collision with root package name */
    final al f4289b;

    public WebSocketException(al alVar, String str) {
        super(str);
        this.f4289b = alVar;
    }

    public WebSocketException(al alVar, String str, Throwable th) {
        super(str, th);
        this.f4289b = alVar;
    }
}
