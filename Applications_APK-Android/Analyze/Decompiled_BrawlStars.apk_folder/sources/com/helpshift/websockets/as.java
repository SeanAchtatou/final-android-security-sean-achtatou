package com.helpshift.websockets;

/* compiled from: WebSocketState */
public enum as {
    CREATED,
    CONNECTING,
    OPEN,
    CLOSING,
    CLOSED
}
