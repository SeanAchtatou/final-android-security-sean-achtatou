package com.helpshift.websockets;

/* compiled from: DeflateDecompressor */
class e {
    e() {
    }

    private static void a(c cVar, int[] iArr, c cVar2, n nVar, n nVar2) throws k {
        while (true) {
            int a2 = nVar.a(cVar, iArr);
            if (a2 != 256) {
                if (a2 < 0 || a2 > 255) {
                    a(f.a(cVar, iArr, a2), f.a(cVar, iArr, nVar2), cVar2);
                } else {
                    cVar2.c(a2);
                }
            } else {
                return;
            }
        }
    }

    public static void a(c cVar, c cVar2) throws k {
        boolean a2;
        int[] iArr = {0};
        do {
            a2 = cVar.a(iArr);
            int a3 = cVar.a(iArr, 2);
            if (a3 == 0) {
                int i = ((iArr[0] + 7) & -8) / 8;
                int a4 = (cVar.a(i) & 255) + ((cVar.a(i + 1) & 255) * 256);
                int i2 = i + 4;
                byte[] array = cVar.f4326a.array();
                if (cVar2.f4326a.capacity() < cVar2.f4327b + a4) {
                    cVar2.b(cVar2.f4327b + a4 + 1024);
                }
                cVar2.f4326a.put(array, i2, a4);
                cVar2.f4327b += a4;
                iArr[0] = (i2 + a4) * 8;
            } else if (a3 == 1) {
                a(cVar, iArr, cVar2, j.a(), i.a());
            } else if (a3 == 2) {
                n[] nVarArr = new n[2];
                f.a(cVar, iArr, nVarArr);
                a(cVar, iArr, cVar2, nVarArr[0], nVarArr[1]);
            } else {
                throw new k(String.format("[%s] Bad compression type '11' at the bit index '%d'.", e.class.getSimpleName(), Integer.valueOf(iArr[0])));
            }
            if (cVar.f4327b <= iArr[0] / 8) {
                a2 = true;
            }
        } while (!a2);
    }

    private static void a(int i, int i2, c cVar) {
        int i3 = cVar.f4327b;
        byte[] bArr = new byte[i];
        int i4 = i3 - i2;
        int i5 = 0;
        int i6 = i4;
        while (i5 < i) {
            if (i3 <= i6) {
                i6 = i4;
            }
            bArr[i5] = cVar.a(i6);
            i5++;
            i6++;
        }
        cVar.a(bArr);
    }
}
