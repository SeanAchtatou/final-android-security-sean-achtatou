package com.helpshift.websockets;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.InputDeviceCompat;

/* compiled from: DeflateUtil */
class f {

    /* renamed from: a  reason: collision with root package name */
    private static int[] f4329a = {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};

    f() {
    }

    public static void a(c cVar, int[] iArr, n[] nVarArr) throws k {
        int a2 = cVar.a(iArr, 5) + InputDeviceCompat.SOURCE_KEYBOARD;
        int a3 = cVar.a(iArr, 5) + 1;
        int a4 = cVar.a(iArr, 4) + 4;
        int[] iArr2 = new int[19];
        for (int i = 0; i < a4; i++) {
            iArr2[f4329a[i]] = (byte) cVar.a(iArr, 3);
        }
        n nVar = new n(iArr2);
        int[] iArr3 = new int[a2];
        a(cVar, iArr, iArr3, nVar);
        n nVar2 = new n(iArr3);
        int[] iArr4 = new int[a3];
        a(cVar, iArr, iArr4, nVar);
        n nVar3 = new n(iArr4);
        nVarArr[0] = nVar2;
        nVarArr[1] = nVar3;
    }

    private static void a(c cVar, int[] iArr, int[] iArr2, n nVar) throws k {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        while (i4 < iArr2.length) {
            int a2 = nVar.a(cVar, iArr);
            if (a2 < 0 || a2 > 15) {
                switch (a2) {
                    case 16:
                        i2 = iArr2[i4 - 1];
                        i = cVar.a(iArr, 2) + 3;
                        break;
                    case 17:
                        i3 = cVar.a(iArr, 3) + 3;
                        i = i3;
                        i2 = 0;
                        break;
                    case 18:
                        i3 = cVar.a(iArr, 7) + 11;
                        i = i3;
                        i2 = 0;
                        break;
                    default:
                        throw new k(String.format("[%s] Bad code length '%d' at the bit index '%d'.", f.class.getSimpleName(), Integer.valueOf(a2), iArr));
                }
                for (int i5 = 0; i5 < i; i5++) {
                    iArr2[i4 + i5] = i2;
                }
                i4 += i - 1;
            } else {
                iArr2[i4] = a2;
            }
            i4++;
        }
    }

    public static int a(c cVar, int[] iArr, int i) throws k {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 5;
        switch (i) {
            case InputDeviceCompat.SOURCE_KEYBOARD:
            case 258:
            case 259:
            case 260:
            case 261:
            case 262:
            case 263:
            case 264:
                return i - 254;
            case 265:
                i3 = 11;
                i6 = 1;
                break;
            case 266:
                i3 = 13;
                i6 = 1;
                break;
            case 267:
                i3 = 15;
                i6 = 1;
                break;
            case 268:
                i3 = 17;
                i6 = 1;
                break;
            case 269:
                i4 = 19;
                i6 = 2;
                break;
            case 270:
                i4 = 23;
                i6 = 2;
                break;
            case 271:
                i4 = 27;
                i6 = 2;
                break;
            case 272:
                i4 = 31;
                i6 = 2;
                break;
            case 273:
                i5 = 35;
                i6 = 3;
                break;
            case 274:
                i5 = 43;
                i6 = 3;
                break;
            case 275:
                i5 = 51;
                i6 = 3;
                break;
            case 276:
                i5 = 59;
                i6 = 3;
                break;
            case 277:
                i2 = 67;
                i6 = 4;
                break;
            case 278:
                i2 = 83;
                i6 = 4;
                break;
            case 279:
                i2 = 99;
                i6 = 4;
                break;
            case 280:
                i2 = 115;
                i6 = 4;
                break;
            case 281:
                i2 = 131;
                break;
            case 282:
                i2 = 163;
                break;
            case 283:
                i2 = 195;
                break;
            case 284:
                i2 = 227;
                break;
            case 285:
                return 258;
            default:
                throw new k(String.format("[%s] Bad literal/length code '%d' at the bit index '%d'.", f.class.getSimpleName(), Integer.valueOf(i), Integer.valueOf(iArr[0])));
        }
        return i2 + cVar.a(iArr, i6);
    }

    public static int a(c cVar, int[] iArr, n nVar) throws k {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int a2 = nVar.a(cVar, iArr);
        int i9 = 12;
        int i10 = 13;
        switch (a2) {
            case 0:
            case 1:
            case 2:
            case 3:
                return a2 + 1;
            case 4:
                i9 = 1;
                i10 = 5;
                break;
            case 5:
                i9 = 1;
                i10 = 7;
                break;
            case 6:
                i9 = 2;
                i10 = 9;
                break;
            case 7:
                i9 = 2;
                break;
            case 8:
                i = 17;
                i9 = 3;
                break;
            case 9:
                i = 25;
                i9 = 3;
                break;
            case 10:
                i2 = 33;
                i9 = 4;
                break;
            case 11:
                i2 = 49;
                i9 = 4;
                break;
            case 12:
                i3 = 65;
                i9 = 5;
                break;
            case 13:
                i3 = 97;
                i9 = 5;
                break;
            case 14:
                i4 = 129;
                i9 = 6;
                break;
            case 15:
                i4 = 193;
                i9 = 6;
                break;
            case 16:
                i5 = InputDeviceCompat.SOURCE_KEYBOARD;
                i9 = 7;
                break;
            case 17:
                i5 = 385;
                i9 = 7;
                break;
            case 18:
                i6 = InputDeviceCompat.SOURCE_DPAD;
                i9 = 8;
                break;
            case 19:
                i6 = 769;
                i9 = 8;
                break;
            case 20:
                i7 = InputDeviceCompat.SOURCE_GAMEPAD;
                i9 = 9;
                break;
            case 21:
                i7 = 1537;
                i9 = 9;
                break;
            case 22:
                i8 = 2049;
                i9 = 10;
                break;
            case 23:
                i8 = 3073;
                i9 = 10;
                break;
            case 24:
                i10 = FragmentTransaction.TRANSIT_FRAGMENT_OPEN;
                i9 = 11;
                break;
            case 25:
                i10 = 6145;
                i9 = 11;
                break;
            case 26:
                i10 = 8193;
                break;
            case 27:
                i10 = 12289;
                break;
            case 28:
                i9 = 13;
                i10 = 16385;
                break;
            case 29:
                i9 = 13;
                i10 = 24577;
                break;
            default:
                throw new k(String.format("[%s] Bad distance code '%d' at the bit index '%d'.", f.class.getSimpleName(), Integer.valueOf(a2), Integer.valueOf(iArr[0])));
        }
        return i10 + cVar.a(iArr, i9);
    }
}
