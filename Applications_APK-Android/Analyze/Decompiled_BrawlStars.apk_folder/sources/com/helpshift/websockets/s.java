package com.helpshift.websockets;

import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;

/* compiled from: OkHostnameVerifier */
final class s implements HostnameVerifier {

    /* renamed from: a  reason: collision with root package name */
    public static final s f4343a = new s();

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f4344b = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");

    private s() {
    }

    private static List<String> a(X509Certificate x509Certificate, int i) {
        String str;
        ArrayList arrayList = new ArrayList();
        try {
            Collection<List<?>> subjectAlternativeNames = x509Certificate.getSubjectAlternativeNames();
            if (subjectAlternativeNames == null) {
                return Collections.emptyList();
            }
            for (List next : subjectAlternativeNames) {
                if (next != null) {
                    if (next.size() >= 2) {
                        Integer num = (Integer) next.get(0);
                        if (num != null) {
                            if (num.intValue() == i && (str = (String) next.get(1)) != null) {
                                arrayList.add(str);
                            }
                        }
                    }
                }
            }
            return arrayList;
        } catch (CertificateParsingException unused) {
            return Collections.emptyList();
        }
    }

    public final boolean verify(String str, SSLSession sSLSession) {
        try {
            X509Certificate x509Certificate = (X509Certificate) sSLSession.getPeerCertificates()[0];
            if (f4344b.matcher(str).matches()) {
                List<String> a2 = a(x509Certificate, 7);
                int size = a2.size();
                int i = 0;
                while (i < size) {
                    if (!str.equalsIgnoreCase(a2.get(i))) {
                        i++;
                    }
                }
                return false;
            }
            String lowerCase = str.toLowerCase(Locale.US);
            List<String> a3 = a(x509Certificate, 2);
            int size2 = a3.size();
            int i2 = 0;
            boolean z = false;
            while (i2 < size2) {
                if (!a(lowerCase, a3.get(i2))) {
                    i2++;
                    z = true;
                }
            }
            if (z) {
                return false;
            }
            g gVar = new g(x509Certificate.getSubjectX500Principal());
            gVar.c = 0;
            gVar.d = 0;
            gVar.e = 0;
            gVar.f = 0;
            gVar.g = gVar.f4330a.toCharArray();
            String a4 = gVar.a();
            String str2 = null;
            if (a4 != null) {
                while (true) {
                    String str3 = "";
                    if (gVar.c == gVar.f4331b) {
                        break;
                    }
                    char c = gVar.g[gVar.c];
                    if (c == '\"') {
                        gVar.c++;
                        gVar.d = gVar.c;
                        gVar.e = gVar.d;
                        while (gVar.c != gVar.f4331b) {
                            if (gVar.g[gVar.c] == '\"') {
                                gVar.c++;
                                while (gVar.c < gVar.f4331b && gVar.g[gVar.c] == ' ') {
                                    gVar.c++;
                                }
                                str3 = new String(gVar.g, gVar.d, gVar.e - gVar.d);
                            } else {
                                if (gVar.g[gVar.c] == '\\') {
                                    gVar.g[gVar.e] = gVar.d();
                                } else {
                                    gVar.g[gVar.e] = gVar.g[gVar.c];
                                }
                                gVar.c++;
                                gVar.e++;
                            }
                        }
                        throw new IllegalStateException("Unexpected end of DN: " + gVar.f4330a);
                    } else if (c == '#') {
                        str3 = gVar.b();
                    } else if (!(c == '+' || c == ',' || c == ';')) {
                        str3 = gVar.c();
                    }
                    if ("cn".equalsIgnoreCase(a4)) {
                        str2 = str3;
                        break;
                    } else if (gVar.c >= gVar.f4331b) {
                        break;
                    } else {
                        if (gVar.g[gVar.c] != ',') {
                            if (gVar.g[gVar.c] != ';') {
                                if (gVar.g[gVar.c] != '+') {
                                    throw new IllegalStateException("Malformed DN: " + gVar.f4330a);
                                }
                            }
                        }
                        gVar.c++;
                        a4 = gVar.a();
                        if (a4 == null) {
                            throw new IllegalStateException("Malformed DN: " + gVar.f4330a);
                        }
                    }
                }
            }
            if (str2 != null) {
                return a(lowerCase, str2);
            }
            return false;
            return true;
        } catch (SSLException unused) {
            return false;
        }
    }

    private static boolean a(String str, String str2) {
        if (str != null && str.length() != 0 && !str.startsWith(".") && !str.endsWith("..") && str2 != null && str2.length() != 0 && !str2.startsWith(".") && !str2.endsWith("..")) {
            if (!str.endsWith(".")) {
                str = str + '.';
            }
            if (!str2.endsWith(".")) {
                str2 = str2 + '.';
            }
            String lowerCase = str2.toLowerCase(Locale.US);
            if (!lowerCase.contains("*")) {
                return str.equals(lowerCase);
            }
            if (!lowerCase.startsWith("*.") || lowerCase.indexOf(42, 1) != -1 || str.length() < lowerCase.length() || "*.".equals(lowerCase)) {
                return false;
            }
            String substring = lowerCase.substring(1);
            if (!str.endsWith(substring)) {
                return false;
            }
            int length = str.length() - substring.length();
            if (length <= 0 || str.lastIndexOf(46, length - 1) == -1) {
                return true;
            }
            return false;
        }
        return false;
    }
}
