package com.helpshift.websockets;

import java.util.Iterator;

/* compiled from: WebSocketThread */
abstract class at extends Thread {

    /* renamed from: a  reason: collision with root package name */
    protected final aj f4322a;

    /* renamed from: b  reason: collision with root package name */
    private final ah f4323b;

    /* access modifiers changed from: protected */
    public abstract void a();

    at(String str, aj ajVar, ah ahVar) {
        super(str);
        this.f4322a = ajVar;
        this.f4323b = ahVar;
    }

    public void run() {
        p pVar = this.f4322a.c;
        if (pVar != null) {
            ah ahVar = this.f4323b;
            Iterator<aq> it = pVar.a().iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
        a();
        if (pVar != null) {
            ah ahVar2 = this.f4323b;
            Iterator<aq> it2 = pVar.a().iterator();
            while (it2.hasNext()) {
                it2.next();
            }
        }
    }

    public final void b() {
        p pVar = this.f4322a.c;
        if (pVar != null) {
            ah ahVar = this.f4323b;
            Iterator<aq> it = pVar.a().iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }
}
