package com.helpshift.websockets;

import java.util.List;
import java.util.Map;

public class OpeningHandshakeException extends WebSocketException {
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    private final ag f4288a;
    private final Map<String, List<String>> c;
    private final byte[] d;

    OpeningHandshakeException(al alVar, String str, ag agVar, Map<String, List<String>> map) {
        this(alVar, str, agVar, map, null);
    }

    OpeningHandshakeException(al alVar, String str, ag agVar, Map<String, List<String>> map, byte[] bArr) {
        super(alVar, str);
        this.f4288a = agVar;
        this.c = map;
        this.d = bArr;
    }
}
