package com.helpshift.websockets;

import javax.security.auth.x500.X500Principal;

/* compiled from: DistinguishedNameParser */
final class g {

    /* renamed from: a  reason: collision with root package name */
    final String f4330a;

    /* renamed from: b  reason: collision with root package name */
    final int f4331b = this.f4330a.length();
    int c;
    int d;
    int e;
    int f;
    char[] g;

    public g(X500Principal x500Principal) {
        this.f4330a = x500Principal.getName("RFC2253");
    }

    /* access modifiers changed from: package-private */
    public String a() {
        while (true) {
            int i = this.c;
            if (i >= this.f4331b || this.g[i] != ' ') {
                int i2 = this.c;
            } else {
                this.c = i + 1;
            }
        }
        int i22 = this.c;
        if (i22 == this.f4331b) {
            return null;
        }
        this.d = i22;
        this.c = i22 + 1;
        while (true) {
            int i3 = this.c;
            if (i3 >= this.f4331b) {
                break;
            }
            char[] cArr = this.g;
            if (cArr[i3] == '=' || cArr[i3] == ' ') {
                break;
            }
            this.c = i3 + 1;
        }
        int i4 = this.c;
        if (i4 < this.f4331b) {
            this.e = i4;
            if (this.g[i4] == ' ') {
                while (true) {
                    int i5 = this.c;
                    if (i5 >= this.f4331b) {
                        break;
                    }
                    char[] cArr2 = this.g;
                    if (cArr2[i5] == '=' || cArr2[i5] != ' ') {
                        break;
                    }
                    this.c = i5 + 1;
                }
                char[] cArr3 = this.g;
                int i6 = this.c;
                if (cArr3[i6] != '=' || i6 == this.f4331b) {
                    throw new IllegalStateException("Unexpected end of DN: " + this.f4330a);
                }
            }
            this.c++;
            while (true) {
                int i7 = this.c;
                if (i7 >= this.f4331b || this.g[i7] != ' ') {
                    int i8 = this.e;
                    int i9 = this.d;
                } else {
                    this.c = i7 + 1;
                }
            }
            int i82 = this.e;
            int i92 = this.d;
            if (i82 - i92 > 4) {
                char[] cArr4 = this.g;
                if (cArr4[i92 + 3] == '.' && (cArr4[i92] == 'O' || cArr4[i92] == 'o')) {
                    char[] cArr5 = this.g;
                    int i10 = this.d;
                    if (cArr5[i10 + 1] == 'I' || cArr5[i10 + 1] == 'i') {
                        char[] cArr6 = this.g;
                        int i11 = this.d;
                        if (cArr6[i11 + 2] == 'D' || cArr6[i11 + 2] == 'd') {
                            this.d += 4;
                        }
                    }
                }
            }
            char[] cArr7 = this.g;
            int i12 = this.d;
            return new String(cArr7, i12, this.e - i12);
        }
        throw new IllegalStateException("Unexpected end of DN: " + this.f4330a);
    }

    /* access modifiers changed from: package-private */
    public String b() {
        int i = this.c;
        if (i + 4 < this.f4331b) {
            this.d = i;
            this.c = i + 1;
            while (true) {
                int i2 = this.c;
                if (i2 == this.f4331b) {
                    break;
                }
                char[] cArr = this.g;
                if (cArr[i2] == '+' || cArr[i2] == ',' || cArr[i2] == ';') {
                    break;
                } else if (cArr[i2] == ' ') {
                    this.e = i2;
                    this.c = i2 + 1;
                    while (true) {
                        int i3 = this.c;
                        if (i3 >= this.f4331b || this.g[i3] != ' ') {
                            break;
                        }
                        this.c = i3 + 1;
                    }
                } else {
                    if (cArr[i2] >= 'A' && cArr[i2] <= 'F') {
                        cArr[i2] = (char) (cArr[i2] + ' ');
                    }
                    this.c++;
                }
            }
            this.e = this.c;
            int i4 = this.e;
            int i5 = this.d;
            int i6 = i4 - i5;
            if (i6 < 5 || (i6 & 1) == 0) {
                throw new IllegalStateException("Unexpected end of DN: " + this.f4330a);
            }
            byte[] bArr = new byte[(i6 / 2)];
            int i7 = i5 + 1;
            for (int i8 = 0; i8 < bArr.length; i8++) {
                bArr[i8] = (byte) a(i7);
                i7 += 2;
            }
            return new String(this.g, this.d, i6);
        }
        throw new IllegalStateException("Unexpected end of DN: " + this.f4330a);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009b, code lost:
        r1 = r8.g;
        r2 = r8.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a7, code lost:
        return new java.lang.String(r1, r2, r8.f - r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String c() {
        /*
            r8 = this;
            int r0 = r8.c
            r8.d = r0
            r8.e = r0
        L_0x0006:
            int r0 = r8.c
            int r1 = r8.f4331b
            if (r0 < r1) goto L_0x0019
            java.lang.String r0 = new java.lang.String
            char[] r1 = r8.g
            int r2 = r8.d
            int r3 = r8.e
            int r3 = r3 - r2
            r0.<init>(r1, r2, r3)
            return r0
        L_0x0019:
            char[] r1 = r8.g
            char r2 = r1[r0]
            r3 = 44
            r4 = 43
            r5 = 59
            r6 = 32
            if (r2 == r6) goto L_0x0060
            if (r2 == r5) goto L_0x0053
            r5 = 92
            if (r2 == r5) goto L_0x0040
            if (r2 == r4) goto L_0x0053
            if (r2 == r3) goto L_0x0053
            int r2 = r8.e
            int r3 = r2 + 1
            r8.e = r3
            char r3 = r1[r0]
            r1[r2] = r3
            int r0 = r0 + 1
            r8.c = r0
            goto L_0x0006
        L_0x0040:
            int r0 = r8.e
            int r2 = r0 + 1
            r8.e = r2
            char r2 = r8.d()
            r1[r0] = r2
            int r0 = r8.c
            int r0 = r0 + 1
            r8.c = r0
            goto L_0x0006
        L_0x0053:
            java.lang.String r0 = new java.lang.String
            char[] r1 = r8.g
            int r2 = r8.d
            int r3 = r8.e
            int r3 = r3 - r2
            r0.<init>(r1, r2, r3)
            return r0
        L_0x0060:
            int r2 = r8.e
            r8.f = r2
            int r0 = r0 + 1
            r8.c = r0
            int r0 = r2 + 1
            r8.e = r0
            r1[r2] = r6
        L_0x006e:
            int r0 = r8.c
            int r1 = r8.f4331b
            if (r0 >= r1) goto L_0x0087
            char[] r1 = r8.g
            char r2 = r1[r0]
            if (r2 != r6) goto L_0x0087
            int r2 = r8.e
            int r7 = r2 + 1
            r8.e = r7
            r1[r2] = r6
            int r0 = r0 + 1
            r8.c = r0
            goto L_0x006e
        L_0x0087:
            int r0 = r8.c
            int r1 = r8.f4331b
            if (r0 == r1) goto L_0x009b
            char[] r1 = r8.g
            char r2 = r1[r0]
            if (r2 == r3) goto L_0x009b
            char r2 = r1[r0]
            if (r2 == r4) goto L_0x009b
            char r0 = r1[r0]
            if (r0 != r5) goto L_0x0006
        L_0x009b:
            java.lang.String r0 = new java.lang.String
            char[] r1 = r8.g
            int r2 = r8.d
            int r3 = r8.f
            int r3 = r3 - r2
            r0.<init>(r1, r2, r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.g.c():java.lang.String");
    }

    /* access modifiers changed from: package-private */
    public char d() {
        this.c++;
        int i = this.c;
        if (i != this.f4331b) {
            char c2 = this.g[i];
            if (!(c2 == ' ' || c2 == '%' || c2 == '\\' || c2 == '_' || c2 == '\"' || c2 == '#')) {
                switch (c2) {
                    case '*':
                    case '+':
                    case ',':
                        break;
                    default:
                        switch (c2) {
                            case ';':
                            case '<':
                            case '=':
                            case '>':
                                break;
                            default:
                                return e();
                        }
                }
            }
            return this.g[this.c];
        }
        throw new IllegalStateException("Unexpected end of DN: " + this.f4330a);
    }

    private char e() {
        int i;
        int i2;
        int a2 = a(this.c);
        this.c++;
        if (a2 < 128) {
            return (char) a2;
        }
        if (a2 < 192 || a2 > 247) {
            return '?';
        }
        if (a2 <= 223) {
            i2 = a2 & 31;
            i = 1;
        } else if (a2 <= 239) {
            i = 2;
            i2 = a2 & 15;
        } else {
            i = 3;
            i2 = a2 & 7;
        }
        for (int i3 = 0; i3 < i; i3++) {
            this.c++;
            int i4 = this.c;
            if (i4 == this.f4331b || this.g[i4] != '\\') {
                return '?';
            }
            this.c = i4 + 1;
            int a3 = a(this.c);
            this.c++;
            if ((a3 & 192) != 128) {
                return '?';
            }
            i2 = (i2 << 6) + (a3 & 63);
        }
        return (char) i2;
    }

    private int a(int i) {
        int i2;
        int i3;
        int i4 = i + 1;
        if (i4 < this.f4331b) {
            char c2 = this.g[i];
            if (c2 >= '0' && c2 <= '9') {
                i2 = c2 - '0';
            } else if (c2 >= 'a' && c2 <= 'f') {
                i2 = c2 - 'W';
            } else if (c2 < 'A' || c2 > 'F') {
                throw new IllegalStateException("Malformed DN: " + this.f4330a);
            } else {
                i2 = c2 - '7';
            }
            char c3 = this.g[i4];
            if (c3 >= '0' && c3 <= '9') {
                i3 = c3 - '0';
            } else if (c3 >= 'a' && c3 <= 'f') {
                i3 = c3 - 'W';
            } else if (c3 < 'A' || c3 > 'F') {
                throw new IllegalStateException("Malformed DN: " + this.f4330a);
            } else {
                i3 = c3 - '7';
            }
            return (i2 << 4) + i3;
        }
        throw new IllegalStateException("Malformed DN: " + this.f4330a);
    }
}
