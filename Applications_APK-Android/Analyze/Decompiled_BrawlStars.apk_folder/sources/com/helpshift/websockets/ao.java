package com.helpshift.websockets;

import com.facebook.appevents.AppEventsConstants;

/* compiled from: WebSocketFrame */
public class ao {

    /* renamed from: a  reason: collision with root package name */
    public boolean f4318a;

    /* renamed from: b  reason: collision with root package name */
    boolean f4319b;
    boolean c;
    boolean d;
    public int e;
    boolean f;
    byte[] g;

    private static ao j() {
        ao aoVar = new ao();
        aoVar.f4318a = true;
        aoVar.e = 8;
        return aoVar;
    }

    public static ao a(int i, String str) {
        return j().b(i, str);
    }

    public static ao a() {
        ao aoVar = new ao();
        aoVar.f4318a = true;
        aoVar.e = 10;
        return aoVar;
    }

    static byte[] a(byte[] bArr, byte[] bArr2) {
        if (bArr != null && bArr.length >= 4) {
            for (int i = 0; i < bArr2.length; i++) {
                bArr2[i] = (byte) (bArr2[i] ^ bArr[i % 4]);
            }
        }
        return bArr2;
    }

    static ao a(ao aoVar, u uVar) {
        byte[] bArr;
        if (uVar == null) {
            return aoVar;
        }
        if ((aoVar.c() || aoVar.d()) && aoVar.f4318a && !aoVar.f4319b && (bArr = aoVar.g) != null && bArr.length != 0) {
            aoVar.a(a(bArr, uVar));
            aoVar.f4319b = true;
        }
        return aoVar;
    }

    private static byte[] a(byte[] bArr, u uVar) {
        try {
            return uVar.b(bArr);
        } catch (WebSocketException unused) {
            return bArr;
        }
    }

    public final boolean b() {
        return this.e == 0;
    }

    public final boolean c() {
        return this.e == 1;
    }

    public final boolean d() {
        return this.e == 2;
    }

    public final boolean e() {
        return this.e == 8;
    }

    public final boolean f() {
        return this.e == 9;
    }

    public final boolean g() {
        return this.e == 10;
    }

    public final boolean h() {
        int i = this.e;
        return 8 <= i && i <= 15;
    }

    public final int i() {
        byte[] bArr = this.g;
        if (bArr == null) {
            return 0;
        }
        return bArr.length;
    }

    public final ao a(byte[] bArr) {
        if (bArr != null && bArr.length == 0) {
            bArr = null;
        }
        this.g = bArr;
        return this;
    }

    private ao b(int i, String str) {
        byte[] bArr = {(byte) ((i >> 8) & 255), (byte) (i & 255)};
        if (str == null || str.length() == 0) {
            return a(bArr);
        }
        byte[] a2 = q.a(str);
        byte[] bArr2 = new byte[(a2.length + 2)];
        System.arraycopy(bArr, 0, bArr2, 0, 2);
        System.arraycopy(a2, 0, bArr2, 2, a2.length);
        return a(bArr2);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("WebSocketFrame(FIN=");
        boolean z = this.f4318a;
        String str = AppEventsConstants.EVENT_PARAM_VALUE_YES;
        sb.append(z ? str : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        sb.append(",RSV1=");
        sb.append(this.f4319b ? str : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        sb.append(",RSV2=");
        sb.append(this.c ? str : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        sb.append(",RSV3=");
        if (!this.d) {
            str = AppEventsConstants.EVENT_PARAM_VALUE_NO;
        }
        sb.append(str);
        sb.append(",Opcode=");
        sb.append(q.b(this.e));
        sb.append(",Length=");
        sb.append(i());
        int i = this.e;
        String str2 = null;
        if (i != 1) {
            if (i == 2) {
                b(sb);
            } else if (i == 8) {
                sb.append(",CloseCode=");
                byte[] bArr = this.g;
                sb.append((int) ((bArr == null || bArr.length < 2) ? 1005 : (bArr[1] & 255) | ((bArr[0] & 255) << 8)));
                sb.append(",Reason=");
                byte[] bArr2 = this.g;
                if (bArr2 != null && bArr2.length >= 3) {
                    str2 = q.a(bArr2, 2, bArr2.length - 2);
                }
                if (str2 == null) {
                    sb.append("null");
                } else {
                    sb.append("\"");
                    sb.append(str2);
                    sb.append("\"");
                }
            }
        } else if (!a(sb)) {
            sb.append("\"");
            byte[] bArr3 = this.g;
            if (bArr3 != null) {
                str2 = q.a(bArr3);
            }
            sb.append(str2);
            sb.append("\"");
        }
        sb.append(")");
        return sb.toString();
    }

    private boolean a(StringBuilder sb) {
        sb.append(",Payload=");
        if (this.g == null) {
            sb.append("null");
            return true;
        } else if (!this.f4319b) {
            return false;
        } else {
            sb.append("compressed");
            return true;
        }
    }

    private void b(StringBuilder sb) {
        byte[] bArr;
        if (!a(sb)) {
            int i = 0;
            while (true) {
                bArr = this.g;
                if (i >= bArr.length) {
                    break;
                }
                sb.append(String.format("%02X ", Integer.valueOf(bArr[i] & 255)));
                i++;
            }
            if (bArr.length != 0) {
                sb.setLength(sb.length() - 1);
            }
        }
    }
}
