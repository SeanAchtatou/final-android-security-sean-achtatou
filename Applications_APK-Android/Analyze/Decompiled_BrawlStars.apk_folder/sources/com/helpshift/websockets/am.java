package com.helpshift.websockets;

import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: WebSocketExtension */
public class am {

    /* renamed from: a  reason: collision with root package name */
    final String f4314a;

    /* renamed from: b  reason: collision with root package name */
    final Map<String, String> f4315b;

    /* access modifiers changed from: package-private */
    public void a() throws WebSocketException {
    }

    public am(String str) {
        if (ai.a(str)) {
            this.f4314a = str;
            this.f4315b = new LinkedHashMap();
            return;
        }
        throw new IllegalArgumentException("'name' is not a valid token.");
    }

    public static am a(String str) {
        String a2;
        if (str == null) {
            return null;
        }
        String[] split = str.trim().split("\\s*;\\s*");
        if (split.length == 0) {
            return null;
        }
        String str2 = split[0];
        if (!ai.a(str2)) {
            return null;
        }
        am b2 = b(str2);
        for (int i = 1; i < split.length; i++) {
            String[] split2 = split[i].split("\\s*=\\s*", 2);
            if (!(split2.length == 0 || split2[0].length() == 0)) {
                String str3 = split2[0];
                if (ai.a(str3) && ((a2 = a(split2)) == null || ai.a(a2))) {
                    b2.a(str3, a2);
                }
            }
        }
        return b2;
    }

    private static String a(String[] strArr) {
        if (strArr.length != 2) {
            return null;
        }
        return ai.b(strArr[1]);
    }

    private static am b(String str) {
        if ("permessage-deflate".equals(str)) {
            return new v(str);
        }
        return new am(str);
    }

    private am a(String str, String str2) {
        if (!ai.a(str)) {
            throw new IllegalArgumentException("'key' is not a valid token.");
        } else if (str2 == null || ai.a(str2)) {
            this.f4315b.put(str, str2);
            return this;
        } else {
            throw new IllegalArgumentException("'value' is not a valid token.");
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(this.f4314a);
        for (Map.Entry next : this.f4315b.entrySet()) {
            sb.append("; ");
            sb.append((String) next.getKey());
            String str = (String) next.getValue();
            if (!(str == null || str.length() == 0)) {
                sb.append("=");
                sb.append(str);
            }
        }
        return sb.toString();
    }
}
