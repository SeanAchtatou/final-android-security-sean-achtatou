package com.helpshift.websockets;

import javax.net.ssl.SSLSocket;

public class HostnameUnverifiedException extends WebSocketException {
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    private final SSLSocket f4287a;
    private final String c;

    public HostnameUnverifiedException(SSLSocket sSLSocket, String str) {
        super(al.HOSTNAME_UNVERIFIED, String.format("The certificate of the peer%s does not match the expected hostname (%s)", a(sSLSocket), str));
        this.f4287a = sSLSocket;
        this.c = str;
    }

    private static String a(SSLSocket sSLSocket) {
        try {
            return String.format(" (%s)", sSLSocket.getSession().getPeerPrincipal().toString());
        } catch (Exception unused) {
            return "";
        }
    }
}
