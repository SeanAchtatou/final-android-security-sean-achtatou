package com.helpshift.websockets;

/* compiled from: StateManager */
class af {

    /* renamed from: a  reason: collision with root package name */
    as f4301a = as.CREATED;

    /* renamed from: b  reason: collision with root package name */
    private a f4302b = a.NONE;

    /* compiled from: StateManager */
    enum a {
        NONE,
        SERVER,
        CLIENT
    }

    public final void a(a aVar) {
        this.f4301a = as.CLOSING;
        if (this.f4302b == a.NONE) {
            this.f4302b = aVar;
        }
    }

    public final boolean a() {
        return this.f4302b == a.SERVER;
    }
}
