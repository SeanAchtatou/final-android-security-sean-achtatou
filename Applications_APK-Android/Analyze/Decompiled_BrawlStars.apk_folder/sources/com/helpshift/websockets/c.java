package com.helpshift.websockets;

import java.nio.ByteBuffer;

/* compiled from: ByteArray */
class c {

    /* renamed from: a  reason: collision with root package name */
    ByteBuffer f4326a;

    /* renamed from: b  reason: collision with root package name */
    int f4327b = 0;

    public c(int i) {
        this.f4326a = ByteBuffer.allocate(i);
    }

    public final byte a(int i) throws IndexOutOfBoundsException {
        if (i >= 0 && this.f4327b > i) {
            return this.f4326a.get(i);
        }
        throw new IndexOutOfBoundsException(String.format("Bad index: index=%d, length=%d", Integer.valueOf(i), Integer.valueOf(this.f4327b)));
    }

    /* access modifiers changed from: package-private */
    public void b(int i) {
        ByteBuffer allocate = ByteBuffer.allocate(i);
        int position = this.f4326a.position();
        this.f4326a.position(0);
        allocate.put(this.f4326a);
        allocate.position(position);
        this.f4326a = allocate;
    }

    public final void c(int i) {
        int capacity = this.f4326a.capacity();
        int i2 = this.f4327b;
        if (capacity < i2 + 1) {
            b(i2 + 1024);
        }
        this.f4326a.put((byte) i);
        this.f4327b++;
    }

    public final void a(byte[] bArr) {
        int capacity = this.f4326a.capacity();
        int i = this.f4327b;
        if (capacity < bArr.length + i) {
            b(i + bArr.length + 1024);
        }
        this.f4326a.put(bArr);
        this.f4327b += bArr.length;
    }

    public final byte[] a(int i, int i2) {
        int i3 = i2 - i;
        if (i3 < 0 || i < 0 || this.f4327b < i2) {
            throw new IllegalArgumentException(String.format("Bad range: beginIndex=%d, endIndex=%d, length=%d", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(this.f4327b)));
        }
        byte[] bArr = new byte[i3];
        if (i3 != 0) {
            System.arraycopy(this.f4326a.array(), i, bArr, 0, i3);
        }
        return bArr;
    }

    private boolean d(int i) {
        return ((1 << (i % 8)) & a(i / 8)) != 0;
    }

    public final int b(int i, int i2) {
        int i3 = 1;
        int i4 = i2 - 1;
        int i5 = 0;
        while (i4 >= 0) {
            if (d(i + i4)) {
                i5 += i3;
            }
            i4--;
            i3 *= 2;
        }
        return i5;
    }

    public final boolean a(int[] iArr) {
        boolean d = d(iArr[0]);
        iArr[0] = iArr[0] + 1;
        return d;
    }

    public final int a(int[] iArr, int i) {
        int i2 = iArr[0];
        int i3 = 0;
        int i4 = 0;
        int i5 = 1;
        while (i3 < i) {
            if (d(i2 + i3)) {
                i4 += i5;
            }
            i3++;
            i5 *= 2;
        }
        iArr[0] = iArr[0] + i;
        return i4;
    }
}
