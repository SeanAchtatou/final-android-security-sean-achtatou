package com.helpshift.websockets;

import java.util.Timer;
import java.util.TimerTask;

/* compiled from: PeriodicalFrameSender */
abstract class w {

    /* renamed from: a  reason: collision with root package name */
    private final aj f4345a;

    /* renamed from: b  reason: collision with root package name */
    private final String f4346b;
    private Timer c;
    private boolean d;
    private long e;
    private t f;

    /* access modifiers changed from: protected */
    public abstract ao a(byte[] bArr);

    public w(aj ajVar, String str, t tVar) {
        this.f4345a = ajVar;
        this.f4346b = str;
        this.f = tVar;
    }

    private static boolean a(Timer timer, a aVar, long j) {
        try {
            timer.schedule(aVar, j);
            return true;
        } catch (RuntimeException unused) {
            return false;
        }
    }

    public final void a() {
        long d2 = d();
        if (d2 < 0) {
            d2 = 0;
        }
        synchronized (this) {
            this.e = d2;
        }
        if (d2 != 0 && this.f4345a.a(as.OPEN)) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = new Timer(this.f4346b);
                }
                if (!this.d) {
                    this.d = a(this.c, new a(), d2);
                }
            }
        }
    }

    public final void b() {
        synchronized (this) {
            if (this.c != null) {
                this.d = false;
                this.c.cancel();
            }
        }
    }

    private long d() {
        long j;
        synchronized (this) {
            j = this.e;
        }
        return j;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        synchronized (this) {
            if (this.e != 0) {
                if (this.f4345a.a(as.OPEN)) {
                    this.f4345a.a(a(e()));
                    this.d = a(this.c, new a(), this.e);
                    return;
                }
            }
            this.d = false;
        }
    }

    private byte[] e() {
        t tVar = this.f;
        if (tVar == null) {
            return null;
        }
        try {
            return tVar.a();
        } catch (Throwable unused) {
            return null;
        }
    }

    /* compiled from: PeriodicalFrameSender */
    final class a extends TimerTask {
        a() {
        }

        public final void run() {
            w.this.c();
        }
    }
}
