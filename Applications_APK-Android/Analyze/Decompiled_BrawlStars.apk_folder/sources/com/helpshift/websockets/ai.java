package com.helpshift.websockets;

/* compiled from: Token */
class ai {
    public static boolean a(char c) {
        if (c == 9 || c == ' ' || c == '\"' || c == ',' || c == '/' || c == '{' || c == '}' || c == '(' || c == ')') {
            return true;
        }
        switch (c) {
            case ':':
            case ';':
            case '<':
            case '=':
            case '>':
            case '?':
            case '@':
                return true;
            default:
                switch (c) {
                    case '[':
                    case '\\':
                    case ']':
                        return true;
                    default:
                        return false;
                }
        }
    }

    public static boolean a(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (a(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String b(String str) {
        if (str == null) {
            return null;
        }
        int length = str.length();
        if (length >= 2 && str.charAt(0) == '\"') {
            int i = length - 1;
            if (str.charAt(i) == '\"') {
                String substring = str.substring(1, i);
                if (substring == null) {
                    return null;
                }
                if (substring.indexOf(92) < 0) {
                    return substring;
                }
                int length2 = substring.length();
                StringBuilder sb = new StringBuilder();
                boolean z = false;
                for (int i2 = 0; i2 < length2; i2++) {
                    char charAt = substring.charAt(i2);
                    if (charAt != '\\' || z) {
                        sb.append(charAt);
                        z = false;
                    } else {
                        z = true;
                    }
                }
                return sb.toString();
            }
        }
        return str;
    }
}
