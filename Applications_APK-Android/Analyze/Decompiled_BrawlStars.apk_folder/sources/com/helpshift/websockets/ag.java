package com.helpshift.websockets;

/* compiled from: StatusLine */
public class ag {

    /* renamed from: a  reason: collision with root package name */
    final int f4305a;

    /* renamed from: b  reason: collision with root package name */
    private final String f4306b;
    private final String c;
    private final String d;

    ag(String str) {
        String[] split = str.split(" +", 3);
        if (split.length >= 2) {
            this.f4306b = split[0];
            this.f4305a = Integer.parseInt(split[1]);
            this.c = split.length == 3 ? split[2] : null;
            this.d = str;
            return;
        }
        throw new IllegalArgumentException();
    }

    public String toString() {
        return this.d;
    }
}
