package com.helpshift.websockets;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ListenerManager */
public class p {

    /* renamed from: a  reason: collision with root package name */
    private final aj f4340a;

    /* renamed from: b  reason: collision with root package name */
    private final List<aq> f4341b = new ArrayList();
    private boolean c = true;
    private List<aq> d;

    public p(aj ajVar) {
        this.f4340a = ajVar;
    }

    public final void a(aq aqVar) {
        if (aqVar != null) {
            synchronized (this.f4341b) {
                this.f4341b.add(aqVar);
                this.c = true;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public List<aq> a() {
        synchronized (this.f4341b) {
            if (!this.c) {
                List<aq> list = this.d;
                return list;
            }
            ArrayList arrayList = new ArrayList(this.f4341b.size());
            for (aq add : this.f4341b) {
                arrayList.add(add);
            }
            this.d = arrayList;
            this.c = false;
            return arrayList;
        }
    }

    public final void a(as asVar) {
        Iterator<aq> it = a().iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public final void a(Map<String, List<String>> map) {
        for (aq a2 : a()) {
            try {
                a2.a();
            } catch (Throwable unused) {
            }
        }
    }

    public final void a(ao aoVar, ao aoVar2, boolean z) {
        for (aq b2 : a()) {
            try {
                b2.b();
            } catch (Throwable unused) {
            }
        }
    }

    public final void a(String str) {
        for (aq a2 : a()) {
            try {
                a2.a(str);
            } catch (Throwable unused) {
            }
        }
    }

    public final void a(byte[] bArr) {
        Iterator<aq> it = a().iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public final void a(ao aoVar) {
        Iterator<aq> it = a().iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public final void a(WebSocketException webSocketException) {
        for (aq a2 : a()) {
            try {
                a2.a(webSocketException);
            } catch (Throwable unused) {
            }
        }
    }

    public final void a(WebSocketException webSocketException, ao aoVar) {
        Iterator<aq> it = a().iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public final void a(WebSocketException webSocketException, List<ao> list) {
        Iterator<aq> it = a().iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public final void a(WebSocketException webSocketException, byte[] bArr) {
        for (aq c2 : a()) {
            try {
                c2.c(webSocketException);
            } catch (Throwable unused) {
            }
        }
    }

    public final void b(WebSocketException webSocketException, ao aoVar) {
        for (aq d2 : a()) {
            try {
                d2.d(webSocketException);
            } catch (Throwable unused) {
            }
        }
    }

    public final void b(WebSocketException webSocketException) {
        for (aq e : a()) {
            try {
                e.e(webSocketException);
            } catch (Throwable unused) {
            }
        }
    }

    public final void a(String str, List<String[]> list) {
        Iterator<aq> it = a().iterator();
        while (it.hasNext()) {
            it.next();
        }
    }
}
