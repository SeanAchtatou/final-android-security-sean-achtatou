package com.helpshift.websockets;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Map;

/* compiled from: ProxyHandshaker */
public class z {

    /* renamed from: a  reason: collision with root package name */
    final String f4348a;

    /* renamed from: b  reason: collision with root package name */
    private final Socket f4349b;
    private final int c;
    private final aa d;

    public z(Socket socket, String str, int i, aa aaVar) {
        this.f4349b = socket;
        this.f4348a = str;
        this.c = i;
        this.d = aaVar;
    }

    private void a(StringBuilder sb) {
        for (Map.Entry next : this.d.f4292a.entrySet()) {
            String str = (String) next.getKey();
            for (String str2 : (List) next.getValue()) {
                if (str2 == null) {
                    str2 = "";
                }
                sb.append(str);
                sb.append(": ");
                sb.append(str2);
                sb.append("\r\n");
            }
        }
    }

    private static void a(InputStream inputStream) throws IOException {
        while (true) {
            int i = 0;
            while (true) {
                int read = inputStream.read();
                if (read == -1) {
                    throw new EOFException("The end of the stream from the proxy server was reached unexpectedly.");
                } else if (read == 10) {
                    if (i == 0) {
                        return;
                    }
                } else if (read != 13) {
                    i++;
                } else {
                    int read2 = inputStream.read();
                    if (read2 == -1) {
                        throw new EOFException("The end of the stream from the proxy server was reached unexpectedly after a carriage return.");
                    } else if (read2 != 10) {
                        i += 2;
                    } else if (i == 0) {
                        return;
                    }
                }
            }
        }
    }

    public final void a() throws IOException {
        String format = String.format("%s:%d", this.f4348a, Integer.valueOf(this.c));
        StringBuilder sb = new StringBuilder();
        sb.append("CONNECT ");
        sb.append(format);
        sb.append(" HTTP/1.1");
        sb.append("\r\n");
        sb.append("Host: ");
        sb.append(format);
        sb.append("\r\n");
        a(sb);
        String str = this.d.f;
        if (!(str == null || str.length() == 0)) {
            String str2 = this.d.g;
            if (str2 == null) {
                str2 = "";
            }
            String format2 = String.format("%s:%s", str, str2);
            sb.append("Proxy-Authorization: Basic ");
            sb.append(b.a(format2));
            sb.append("\r\n");
        }
        sb.append("\r\n");
        byte[] a2 = q.a(sb.toString());
        OutputStream outputStream = this.f4349b.getOutputStream();
        outputStream.write(a2);
        outputStream.flush();
        InputStream inputStream = this.f4349b.getInputStream();
        String a3 = q.a(inputStream, "UTF-8");
        if (a3 == null || a3.length() == 0) {
            throw new IOException("The response from the proxy server does not contain a status line.");
        }
        String[] split = a3.split(" +", 3);
        if (split.length < 2) {
            throw new IOException("The status line in the response from the proxy server is badly formatted. The status line is: " + a3);
        } else if ("200".equals(split[1])) {
            a(inputStream);
        } else {
            throw new IOException("The status code in the response from the proxy server is not '200 Connection established'. The status line is: " + a3);
        }
    }
}
