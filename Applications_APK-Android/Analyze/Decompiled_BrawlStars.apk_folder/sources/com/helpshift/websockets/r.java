package com.helpshift.websockets;

/* compiled from: NoMoreFrameException */
class r extends WebSocketException {
    private static final long serialVersionUID = 1;

    public r() {
        super(al.NO_MORE_FRAME, "No more WebSocket frame from the server.");
    }
}
