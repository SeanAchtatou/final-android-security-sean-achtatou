package com.helpshift.websockets;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

/* compiled from: PerMessageDeflateExtension */
class v extends u {
    private static final byte[] c = {0, 0, -1, -1};
    private boolean d;
    private boolean e;
    private int f = 32768;
    private int g = 32768;
    private int h;
    private c i;

    public v() {
        super("permessage-deflate");
    }

    public v(String str) {
        super(str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x009a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] c(byte[] r9) throws com.helpshift.websockets.k {
        /*
            com.helpshift.websockets.c r0 = new com.helpshift.websockets.c
            int r1 = r9.length
            r2 = 1
            int r1 = r1 + r2
            r0.<init>(r1)
            r0.a(r9)
            int[] r9 = new int[r2]
            boolean[] r1 = new boolean[r2]
        L_0x000f:
            boolean r3 = r0.a(r9)
            r4 = 0
            if (r3 == 0) goto L_0x002c
            r5 = r9[r4]
            int r5 = r5 - r2
            int r6 = r5 / 8
            int r5 = r5 % 8
            byte r7 = r0.a(r6)
            int r5 = r2 << r5
            r5 = r5 ^ -1
            r5 = r5 & r7
            java.nio.ByteBuffer r7 = r0.f4326a
            byte r5 = (byte) r5
            r7.put(r6, r5)
        L_0x002c:
            r5 = 2
            int r6 = r0.a(r9, r5)
            r7 = 7
            if (r6 == 0) goto L_0x0070
            if (r6 == r2) goto L_0x0063
            if (r6 != r5) goto L_0x0045
            com.helpshift.websockets.n[] r5 = new com.helpshift.websockets.n[r5]
            com.helpshift.websockets.f.a(r0, r9, r5)
            r6 = r5[r4]
            r5 = r5[r2]
            a(r0, r9, r6, r5)
            goto L_0x006e
        L_0x0045:
            java.lang.Object[] r0 = new java.lang.Object[r5]
            java.lang.Class<com.helpshift.websockets.v> r1 = com.helpshift.websockets.v.class
            java.lang.String r1 = r1.getSimpleName()
            r0[r4] = r1
            r9 = r9[r4]
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r0[r2] = r9
            java.lang.String r9 = "[%s] Bad compression type '11' at the bit index '%d'."
            java.lang.String r9 = java.lang.String.format(r9, r0)
            com.helpshift.websockets.k r0 = new com.helpshift.websockets.k
            r0.<init>(r9)
            throw r0
        L_0x0063:
            com.helpshift.websockets.j r5 = com.helpshift.websockets.j.a()
            com.helpshift.websockets.i r6 = com.helpshift.websockets.i.a()
            a(r0, r9, r5, r6)
        L_0x006e:
            r5 = 0
            goto L_0x0092
        L_0x0070:
            r5 = r9[r4]
            int r5 = r5 + r7
            r5 = r5 & -8
            int r5 = r5 / 8
            byte r6 = r0.a(r5)
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r8 = r5 + 1
            byte r8 = r0.a(r8)
            r8 = r8 & 255(0xff, float:3.57E-43)
            int r8 = r8 * 256
            int r6 = r6 + r8
            int r5 = r5 + 4
            int r5 = r5 + r6
            int r5 = r5 * 8
            r9[r4] = r5
            if (r6 != 0) goto L_0x006e
            r5 = 1
        L_0x0092:
            int r6 = r0.f4327b
            r8 = r9[r4]
            int r8 = r8 / 8
            if (r6 > r8) goto L_0x009b
            r3 = 1
        L_0x009b:
            if (r3 == 0) goto L_0x00a1
            if (r5 == 0) goto L_0x00a1
            r1[r4] = r2
        L_0x00a1:
            r3 = r3 ^ 1
            if (r3 == 0) goto L_0x00a7
            goto L_0x000f
        L_0x00a7:
            boolean r1 = r1[r4]
            if (r1 == 0) goto L_0x00b8
            r9 = r9[r4]
            int r9 = r9 - r2
            int r9 = r9 / 8
            int r9 = r9 + r2
            int r9 = r9 + -4
            byte[] r9 = r0.a(r4, r9)
            return r9
        L_0x00b8:
            r1 = r9[r4]
            int r1 = r1 % 8
            if (r1 == 0) goto L_0x00c4
            r3 = 6
            if (r1 == r3) goto L_0x00c4
            if (r1 == r7) goto L_0x00c4
            goto L_0x00c7
        L_0x00c4:
            r0.c(r4)
        L_0x00c7:
            r1 = r9[r4]
            int r1 = r1 + 3
            r9[r4] = r1
            r9 = r9[r4]
            int r9 = r9 - r2
            int r9 = r9 / 8
            int r9 = r9 + r2
            byte[] r9 = r0.a(r4, r9)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.v.c(byte[]):byte[]");
    }

    private static void a(c cVar, int[] iArr, n nVar, n nVar2) throws k {
        while (true) {
            int a2 = nVar.a(cVar, iArr);
            if (a2 != 256) {
                if (a2 < 0 || a2 > 255) {
                    f.a(cVar, iArr, a2);
                    f.a(cVar, iArr, nVar2);
                }
            } else {
                return;
            }
        }
    }

    private int a(String str, String str2) throws WebSocketException {
        int b2 = b(str, str2);
        int i2 = 256;
        for (int i3 = 8; i3 < b2; i3++) {
            i2 *= 2;
        }
        return i2;
    }

    private int b(String str, String str2) throws WebSocketException {
        int b2 = b(str2);
        if (b2 >= 0) {
            return b2;
        }
        throw new WebSocketException(al.PERMESSAGE_DEFLATE_INVALID_MAX_WINDOW_BITS, String.format("The value of %s parameter of permessage-deflate extension is invalid: %s", str, str2));
    }

    private static int b(String str) {
        if (str == null) {
            return -1;
        }
        try {
            int parseInt = Integer.parseInt(str);
            if (parseInt < 8 || 15 < parseInt) {
                return -1;
            }
            return parseInt;
        } catch (NumberFormatException unused) {
        }
    }

    /* access modifiers changed from: protected */
    public final byte[] a(byte[] bArr) throws WebSocketException {
        c cVar = new c(bArr.length + c.length);
        cVar.a(bArr);
        cVar.a(c);
        if (this.i == null) {
            this.i = new c(this.h);
        }
        int i2 = this.i.f4327b;
        try {
            e.a(cVar, this.i);
            c cVar2 = this.i;
            byte[] a2 = cVar2.a(i2, cVar2.f4327b);
            c cVar3 = this.i;
            int i3 = this.h;
            if (cVar3.f4326a.capacity() > i3) {
                byte[] a3 = cVar3.a(cVar3.f4327b - i3, cVar3.f4327b);
                cVar3.f4326a = ByteBuffer.wrap(a3);
                cVar3.f4326a.position(a3.length);
                cVar3.f4327b = a3.length;
            }
            if (this.d) {
                c cVar4 = this.i;
                cVar4.f4326a.clear();
                cVar4.f4326a.position(0);
                cVar4.f4327b = 0;
            }
            return a2;
        } catch (Exception e2) {
            throw new WebSocketException(al.DECOMPRESSION_ERROR, String.format("Failed to decompress the message: %s", e2.getMessage()), e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.websockets.v.a(java.lang.String, java.lang.String):int
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.helpshift.websockets.am.a(java.lang.String, java.lang.String):com.helpshift.websockets.am
      com.helpshift.websockets.v.a(java.lang.String, java.lang.String):int */
    /* access modifiers changed from: package-private */
    public final void a() throws WebSocketException {
        for (Map.Entry next : this.f4315b.entrySet()) {
            String str = (String) next.getKey();
            String str2 = (String) next.getValue();
            if ("server_no_context_takeover".equals(str)) {
                this.d = true;
            } else if ("client_no_context_takeover".equals(str)) {
                this.e = true;
            } else if ("server_max_window_bits".equals(str)) {
                this.f = a(str, str2);
            } else if ("client_max_window_bits".equals(str)) {
                this.g = a(str, str2);
            } else {
                al alVar = al.PERMESSAGE_DEFLATE_UNSUPPORTED_PARAMETER;
                throw new WebSocketException(alVar, "permessage-deflate extension contains an unsupported parameter: " + str);
            }
        }
        this.h = this.f + 1024;
    }

    /* access modifiers changed from: protected */
    public final byte[] b(byte[] bArr) throws WebSocketException {
        boolean z;
        int i2 = this.g;
        if (i2 != 32768 && bArr.length >= i2) {
            z = false;
        } else {
            z = true;
        }
        if (!z) {
            return bArr;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Deflater deflater = new Deflater(-1, true);
            DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(byteArrayOutputStream, deflater);
            deflaterOutputStream.write(bArr, 0, bArr.length);
            deflaterOutputStream.close();
            deflater.end();
            return c(byteArrayOutputStream.toByteArray());
        } catch (Exception e2) {
            throw new WebSocketException(al.COMPRESSION_ERROR, String.format("Failed to compress the message: %s", e2.getMessage()), e2);
        }
    }
}
