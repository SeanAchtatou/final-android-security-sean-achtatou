package com.helpshift.websockets;

import android.support.v4.view.PointerIconCompat;
import com.helpshift.websockets.af;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: ReadingThread */
class ab extends at {

    /* renamed from: b  reason: collision with root package name */
    private final u f4294b;
    private boolean c;
    private ao d;
    private List<ao> e = new ArrayList();
    private Object f = new Object();
    private Timer g;
    private a h;
    private long i;
    private boolean j;

    public ab(aj ajVar) {
        super("ReadingThread", ajVar, ah.READING_THREAD);
        this.f4294b = ajVar.s;
    }

    public final void a() {
        try {
            c();
        } catch (Throwable th) {
            al alVar = al.UNEXPECTED_ERROR_IN_READING_THREAD;
            WebSocketException webSocketException = new WebSocketException(alVar, "An uncaught throwable was detected in the reading thread: " + th.getMessage(), th);
            p pVar = this.f4322a.c;
            pVar.a(webSocketException);
            pVar.b(webSocketException);
        }
        aj ajVar = this.f4322a;
        ao aoVar = this.d;
        synchronized (ajVar.d) {
            ajVar.p = true;
            ajVar.r = aoVar;
            if (ajVar.q) {
                ajVar.e();
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private void c() {
        boolean z;
        boolean z2;
        byte[] bArr;
        aj ajVar = this.f4322a;
        synchronized (ajVar.d) {
            ajVar.n = true;
            z = ajVar.o;
        }
        ajVar.c();
        if (z) {
            ajVar.d();
        }
        while (true) {
            synchronized (this) {
                if (!this.c) {
                    ao d2 = d();
                    if (d2 != null) {
                        Iterator<aq> it = this.f4322a.c.a().iterator();
                        while (it.hasNext()) {
                            it.next();
                        }
                        int i2 = d2.e;
                        if (i2 != 0) {
                            if (i2 == 1) {
                                Iterator<aq> it2 = this.f4322a.c.a().iterator();
                                while (it2.hasNext()) {
                                    it2.next();
                                }
                                if (!d2.f4318a) {
                                    this.e.add(d2);
                                } else {
                                    a(a(d2));
                                }
                            } else if (i2 != 2) {
                                switch (i2) {
                                    case 8:
                                        z2 = b(d2);
                                        continue;
                                    case 9:
                                        Iterator<aq> it3 = this.f4322a.c.a().iterator();
                                        while (it3.hasNext()) {
                                            it3.next();
                                        }
                                        this.f4322a.a(ao.a().a(d2.g));
                                        break;
                                    case 10:
                                        Iterator<aq> it4 = this.f4322a.c.a().iterator();
                                        while (it4.hasNext()) {
                                            it4.next();
                                        }
                                        break;
                                }
                            } else {
                                Iterator<aq> it5 = this.f4322a.c.a().iterator();
                                while (it5.hasNext()) {
                                    it5.next();
                                }
                                if (!d2.f4318a) {
                                    this.e.add(d2);
                                } else {
                                    b(a(d2));
                                }
                            }
                            z2 = true;
                            continue;
                        } else {
                            Iterator<aq> it6 = this.f4322a.c.a().iterator();
                            while (it6.hasNext()) {
                                it6.next();
                            }
                            this.e.add(d2);
                            boolean z3 = false;
                            if (d2.f4318a) {
                                List<ao> list = this.e;
                                byte[] a2 = a(list);
                                if (a2 == null) {
                                    bArr = null;
                                } else {
                                    if (this.f4294b != null && list.get(0).f4319b) {
                                        a2 = c(a2);
                                    }
                                    bArr = a2;
                                }
                                if (bArr != null) {
                                    if (this.e.get(0).c()) {
                                        a(bArr);
                                    } else {
                                        b(bArr);
                                    }
                                    this.e.clear();
                                }
                                z2 = z3;
                                continue;
                            }
                            z3 = true;
                            z2 = z3;
                            continue;
                        }
                        if (!z2) {
                        }
                    }
                }
            }
        }
        if (!this.j && this.d == null) {
            e();
            while (true) {
                try {
                    ao a3 = this.f4322a.f.a();
                    if (a3.e()) {
                        this.d = a3;
                    } else if (isInterrupted()) {
                    }
                } catch (Throwable unused) {
                }
            }
        }
        synchronized (this.f) {
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(long j2) {
        synchronized (this) {
            if (!this.c) {
                this.c = true;
                interrupt();
                this.i = j2;
                e();
            }
        }
    }

    private void a(byte[] bArr) {
        try {
            this.f4322a.c.a(q.a(bArr));
        } catch (Throwable th) {
            al alVar = al.TEXT_MESSAGE_CONSTRUCTION_ERROR;
            WebSocketException webSocketException = new WebSocketException(alVar, "Failed to convert payload data into a string: " + th.getMessage(), th);
            a(webSocketException);
            this.f4322a.c.a(webSocketException, bArr);
        }
    }

    private void b(byte[] bArr) {
        this.f4322a.c.a(bArr);
    }

    private void a(WebSocketException webSocketException) {
        this.f4322a.c.a(webSocketException);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:99:0x015f, code lost:
        if (r9.f4322a.l != false) goto L_0x0163;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0165  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0183  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0028 A[Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0031 A[Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00ea A[Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0138 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0159  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.helpshift.websockets.ao d() {
        /*
            r9 = this;
            r0 = 0
            r1 = 0
            r2 = 1
            com.helpshift.websockets.aj r3 = r9.f4322a     // Catch:{ InterruptedIOException -> 0x0132, IOException -> 0x0108, WebSocketException -> 0x0104 }
            com.helpshift.websockets.ap r3 = r3.f     // Catch:{ InterruptedIOException -> 0x0132, IOException -> 0x0108, WebSocketException -> 0x0104 }
            com.helpshift.websockets.ao r3 = r3.a()     // Catch:{ InterruptedIOException -> 0x0132, IOException -> 0x0108, WebSocketException -> 0x0104 }
            com.helpshift.websockets.aj r4 = r9.f4322a     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            boolean r4 = r4.j     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 == 0) goto L_0x0012
            goto L_0x0035
        L_0x0012:
            com.helpshift.websockets.u r4 = r9.f4294b     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 == 0) goto L_0x0029
            boolean r4 = r3.c()     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 != 0) goto L_0x0025
            boolean r4 = r3.d()     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 == 0) goto L_0x0023
            goto L_0x0025
        L_0x0023:
            r4 = 0
            goto L_0x0026
        L_0x0025:
            r4 = 1
        L_0x0026:
            if (r4 == 0) goto L_0x0029
            goto L_0x002d
        L_0x0029:
            boolean r4 = r3.f4319b     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 != 0) goto L_0x00f4
        L_0x002d:
            boolean r4 = r3.c     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 != 0) goto L_0x00ea
            boolean r4 = r3.d     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 != 0) goto L_0x00e0
        L_0x0035:
            int r4 = r3.e     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 == 0) goto L_0x0067
            if (r4 == r2) goto L_0x0067
            r5 = 2
            if (r4 == r5) goto L_0x0067
            switch(r4) {
                case 8: goto L_0x0067;
                case 9: goto L_0x0067;
                case 10: goto L_0x0067;
                default: goto L_0x0041;
            }     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
        L_0x0041:
            com.helpshift.websockets.aj r4 = r9.f4322a     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            boolean r4 = r4.j     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 == 0) goto L_0x0048
            goto L_0x0067
        L_0x0048:
            com.helpshift.websockets.WebSocketException r4 = new com.helpshift.websockets.WebSocketException     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            com.helpshift.websockets.al r5 = com.helpshift.websockets.al.UNKNOWN_OPCODE     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            r6.<init>()     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r7 = "A frame has an unknown opcode: 0x"
            r6.append(r7)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            int r7 = r3.e     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r7 = java.lang.Integer.toHexString(r7)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            r6.append(r7)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r6 = r6.toString()     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            r4.<init>(r5, r6)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            throw r4     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
        L_0x0067:
            boolean r4 = r3.f     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 != 0) goto L_0x00d6
            boolean r4 = r3.h()     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 == 0) goto L_0x0080
            boolean r4 = r3.f4318a     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 == 0) goto L_0x0076
            goto L_0x00a0
        L_0x0076:
            com.helpshift.websockets.WebSocketException r4 = new com.helpshift.websockets.WebSocketException     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            com.helpshift.websockets.al r5 = com.helpshift.websockets.al.FRAGMENTED_CONTROL_FRAME     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r6 = "A control frame is fragmented."
            r4.<init>(r5, r6)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            throw r4     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
        L_0x0080:
            java.util.List<com.helpshift.websockets.ao> r4 = r9.e     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            int r4 = r4.size()     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 == 0) goto L_0x008a
            r4 = 1
            goto L_0x008b
        L_0x008a:
            r4 = 0
        L_0x008b:
            boolean r5 = r3.b()     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r5 == 0) goto L_0x009e
            if (r4 == 0) goto L_0x0094
            goto L_0x00a0
        L_0x0094:
            com.helpshift.websockets.WebSocketException r4 = new com.helpshift.websockets.WebSocketException     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            com.helpshift.websockets.al r5 = com.helpshift.websockets.al.UNEXPECTED_CONTINUATION_FRAME     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r6 = "A continuation frame was detected although a continuation had not started."
            r4.<init>(r5, r6)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            throw r4     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
        L_0x009e:
            if (r4 != 0) goto L_0x00cc
        L_0x00a0:
            boolean r4 = r3.h()     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 != 0) goto L_0x00a7
            goto L_0x00b1
        L_0x00a7:
            byte[] r4 = r3.g     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r4 != 0) goto L_0x00ac
            goto L_0x00b1
        L_0x00ac:
            r5 = 125(0x7d, float:1.75E-43)
            int r6 = r4.length     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            if (r5 < r6) goto L_0x00b2
        L_0x00b1:
            return r3
        L_0x00b2:
            com.helpshift.websockets.WebSocketException r5 = new com.helpshift.websockets.WebSocketException     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            com.helpshift.websockets.al r6 = com.helpshift.websockets.al.TOO_LONG_CONTROL_FRAME_PAYLOAD     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            r7.<init>()     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r8 = "The payload size of a control frame exceeds the maximum size (125 bytes): "
            r7.append(r8)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            int r4 = r4.length     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            r7.append(r4)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r4 = r7.toString()     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            r5.<init>(r6, r4)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            throw r5     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
        L_0x00cc:
            com.helpshift.websockets.WebSocketException r4 = new com.helpshift.websockets.WebSocketException     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            com.helpshift.websockets.al r5 = com.helpshift.websockets.al.CONTINUATION_NOT_CLOSED     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r6 = "A non-control frame was detected although the existing continuation had not been closed."
            r4.<init>(r5, r6)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            throw r4     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
        L_0x00d6:
            com.helpshift.websockets.WebSocketException r4 = new com.helpshift.websockets.WebSocketException     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            com.helpshift.websockets.al r5 = com.helpshift.websockets.al.FRAME_MASKED     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r6 = "A frame from the server is masked."
            r4.<init>(r5, r6)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            throw r4     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
        L_0x00e0:
            com.helpshift.websockets.WebSocketException r4 = new com.helpshift.websockets.WebSocketException     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            com.helpshift.websockets.al r5 = com.helpshift.websockets.al.UNEXPECTED_RESERVED_BIT     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r6 = "The RSV3 bit of a frame is set unexpectedly."
            r4.<init>(r5, r6)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            throw r4     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
        L_0x00ea:
            com.helpshift.websockets.WebSocketException r4 = new com.helpshift.websockets.WebSocketException     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            com.helpshift.websockets.al r5 = com.helpshift.websockets.al.UNEXPECTED_RESERVED_BIT     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r6 = "The RSV2 bit of a frame is set unexpectedly."
            r4.<init>(r5, r6)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            throw r4     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
        L_0x00f4:
            com.helpshift.websockets.WebSocketException r4 = new com.helpshift.websockets.WebSocketException     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            com.helpshift.websockets.al r5 = com.helpshift.websockets.al.UNEXPECTED_RESERVED_BIT     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            java.lang.String r6 = "The RSV1 bit of a frame is set unexpectedly."
            r4.<init>(r5, r6)     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
            throw r4     // Catch:{ InterruptedIOException -> 0x0102, IOException -> 0x0100, WebSocketException -> 0x00fe }
        L_0x00fe:
            r4 = move-exception
            goto L_0x0106
        L_0x0100:
            r4 = move-exception
            goto L_0x010a
        L_0x0102:
            r4 = move-exception
            goto L_0x0134
        L_0x0104:
            r4 = move-exception
            r3 = r1
        L_0x0106:
            r5 = r4
            goto L_0x0155
        L_0x0108:
            r4 = move-exception
            r3 = r1
        L_0x010a:
            boolean r5 = r9.c
            if (r5 == 0) goto L_0x0115
            boolean r5 = r9.isInterrupted()
            if (r5 == 0) goto L_0x0115
            return r1
        L_0x0115:
            com.helpshift.websockets.WebSocketException r5 = new com.helpshift.websockets.WebSocketException
            com.helpshift.websockets.al r6 = com.helpshift.websockets.al.IO_ERROR_IN_READING
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "An I/O error occurred while a frame was being read from the web socket: "
            r7.append(r8)
            java.lang.String r8 = r4.getMessage()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r5.<init>(r6, r7, r4)
            goto L_0x0155
        L_0x0132:
            r4 = move-exception
            r3 = r1
        L_0x0134:
            boolean r5 = r9.c
            if (r5 == 0) goto L_0x0139
            return r1
        L_0x0139:
            com.helpshift.websockets.WebSocketException r5 = new com.helpshift.websockets.WebSocketException
            com.helpshift.websockets.al r6 = com.helpshift.websockets.al.INTERRUPTED_IN_READING
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Interruption occurred while a frame was being read from the web socket: "
            r7.append(r8)
            java.lang.String r8 = r4.getMessage()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r5.<init>(r6, r7, r4)
        L_0x0155:
            boolean r4 = r5 instanceof com.helpshift.websockets.r
            if (r4 == 0) goto L_0x0162
            r9.j = r2
            com.helpshift.websockets.aj r4 = r9.f4322a
            boolean r4 = r4.l
            if (r4 == 0) goto L_0x0162
            goto L_0x0163
        L_0x0162:
            r0 = 1
        L_0x0163:
            if (r0 == 0) goto L_0x016f
            r9.a(r5)
            com.helpshift.websockets.aj r0 = r9.f4322a
            com.helpshift.websockets.p r0 = r0.c
            r0.a(r5, r3)
        L_0x016f:
            int[] r0 = com.helpshift.websockets.ac.f4296a
            com.helpshift.websockets.al r2 = r5.f4289b
            int r2 = r2.ordinal()
            r0 = r0[r2]
            r2 = 1002(0x3ea, float:1.404E-42)
            r3 = 1008(0x3f0, float:1.413E-42)
            switch(r0) {
                case 1: goto L_0x0185;
                case 2: goto L_0x0185;
                case 3: goto L_0x0185;
                case 4: goto L_0x0183;
                case 5: goto L_0x0183;
                case 6: goto L_0x0185;
                case 7: goto L_0x0185;
                case 8: goto L_0x0185;
                case 9: goto L_0x0185;
                case 10: goto L_0x0185;
                case 11: goto L_0x0185;
                case 12: goto L_0x0185;
                case 13: goto L_0x0185;
                case 14: goto L_0x0180;
                case 15: goto L_0x0180;
                default: goto L_0x0180;
            }
        L_0x0180:
            r2 = 1008(0x3f0, float:1.413E-42)
            goto L_0x0185
        L_0x0183:
            r2 = 1009(0x3f1, float:1.414E-42)
        L_0x0185:
            java.lang.String r0 = r5.getMessage()
            com.helpshift.websockets.ao r0 = com.helpshift.websockets.ao.a(r2, r0)
            com.helpshift.websockets.aj r2 = r9.f4322a
            r2.a(r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.websockets.ab.d():com.helpshift.websockets.ao");
    }

    private byte[] a(List<ao> list) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (ao aoVar : list) {
                byte[] bArr = aoVar.g;
                if (bArr != null) {
                    if (bArr.length != 0) {
                        byteArrayOutputStream.write(bArr);
                    }
                }
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException | OutOfMemoryError e2) {
            al alVar = al.MESSAGE_CONSTRUCTION_ERROR;
            WebSocketException webSocketException = new WebSocketException(alVar, "Failed to concatenate payloads of multiple frames to construct a message: " + e2.getMessage(), e2);
            a(webSocketException);
            this.f4322a.c.a(webSocketException, list);
            this.f4322a.a(ao.a((int) PointerIconCompat.TYPE_VERTICAL_TEXT, webSocketException.getMessage()));
            return null;
        }
    }

    private byte[] c(byte[] bArr) {
        try {
            return this.f4294b.a(bArr);
        } catch (WebSocketException e2) {
            a(e2);
            for (aq b2 : this.f4322a.c.a()) {
                b2.b(e2);
            }
            this.f4322a.a(ao.a((int) PointerIconCompat.TYPE_HELP, e2.getMessage()));
            return null;
        } catch (Throwable unused) {
        }
    }

    private boolean b(ao aoVar) {
        boolean z;
        af afVar = this.f4322a.f4310b;
        this.d = aoVar;
        synchronized (afVar) {
            as asVar = afVar.f4301a;
            if (asVar == as.CLOSING || asVar == as.CLOSED) {
                z = false;
            } else {
                afVar.a(af.a.SERVER);
                this.f4322a.a(aoVar);
                z = true;
            }
        }
        if (z) {
            this.f4322a.c.a(as.CLOSING);
        }
        Iterator<aq> it = this.f4322a.c.a().iterator();
        while (it.hasNext()) {
            it.next();
        }
        return false;
    }

    private void e() {
        synchronized (this.f) {
            g();
            f();
        }
    }

    private void f() {
        this.h = new a();
        this.g = new Timer("ReadingThreadCloseTimer");
        this.g.schedule(this.h, this.i);
    }

    private void g() {
        Timer timer = this.g;
        if (timer != null) {
            timer.cancel();
            this.g = null;
        }
        a aVar = this.h;
        if (aVar != null) {
            aVar.cancel();
            this.h = null;
        }
    }

    /* compiled from: ReadingThread */
    class a extends TimerTask {
        a() {
        }

        public void run() {
            try {
                ab.this.f4322a.f4309a.d.close();
            } catch (Throwable unused) {
            }
        }
    }

    private byte[] a(ao aoVar) {
        byte[] bArr = aoVar.g;
        return (this.f4294b == null || !aoVar.f4319b) ? bArr : c(bArr);
    }
}
