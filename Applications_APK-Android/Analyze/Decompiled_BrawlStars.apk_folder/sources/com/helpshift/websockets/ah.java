package com.helpshift.websockets;

/* compiled from: ThreadType */
public enum ah {
    READING_THREAD,
    WRITING_THREAD,
    CONNECT_THREAD,
    FINISH_THREAD
}
