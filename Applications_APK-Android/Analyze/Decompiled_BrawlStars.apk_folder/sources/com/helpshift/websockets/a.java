package com.helpshift.websockets;

/* compiled from: Address */
public class a {

    /* renamed from: a  reason: collision with root package name */
    final String f4290a;

    /* renamed from: b  reason: collision with root package name */
    final int f4291b;
    private transient String c;

    public a(String str, int i) {
        this.f4290a = str;
        this.f4291b = i;
    }

    public String toString() {
        if (this.c == null) {
            this.c = String.format("%s:%d", this.f4290a, Integer.valueOf(this.f4291b));
        }
        return this.c;
    }
}
