package com.helpshift.websockets;

/* compiled from: InsufficientDataException */
class o extends WebSocketException {
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    final int f4339a;
    private final int c;

    public o(int i, int i2) {
        super(al.INSUFFICENT_DATA, "The end of the stream has been reached unexpectedly.");
        this.c = i;
        this.f4339a = i2;
    }
}
