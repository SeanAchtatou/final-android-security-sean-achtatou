package com.helpshift.websockets;

import android.support.v7.widget.ActivityChooserView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: Misc */
public class q {

    /* renamed from: a  reason: collision with root package name */
    private static final SecureRandom f4342a = new SecureRandom();

    public static byte[] a(String str) {
        if (str == null) {
            return null;
        }
        try {
            return str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException unused) {
            return null;
        }
    }

    public static String a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        return a(bArr, 0, bArr.length);
    }

    public static String a(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            return null;
        }
        try {
            return new String(bArr, i, i2, "UTF-8");
        } catch (UnsupportedEncodingException | IndexOutOfBoundsException unused) {
            return null;
        }
    }

    public static byte[] b(byte[] bArr) {
        f4342a.nextBytes(bArr);
        return bArr;
    }

    public static byte[] a(int i) {
        return b(new byte[4]);
    }

    public static String b(int i) {
        if (i == 0) {
            return "CONTINUATION";
        }
        if (i == 1) {
            return "TEXT";
        }
        if (i == 2) {
            return "BINARY";
        }
        switch (i) {
            case 8:
                return "CLOSE";
            case 9:
                return "PING";
            case 10:
                return "PONG";
            default:
                if (1 <= i && i <= 7) {
                    return String.format("DATA(0x%X)", Integer.valueOf(i));
                } else if (8 > i || i > 15) {
                    return String.format("0x%X", Integer.valueOf(i));
                } else {
                    return String.format("CONTROL(0x%X)", Integer.valueOf(i));
                }
        }
    }

    public static String a(InputStream inputStream, String str) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read();
            if (read != -1) {
                if (read == 10) {
                    break;
                } else if (read != 13) {
                    byteArrayOutputStream.write(read);
                } else {
                    int read2 = inputStream.read();
                    if (read2 == -1) {
                        byteArrayOutputStream.write(read);
                        break;
                    } else if (read2 == 10) {
                        break;
                    } else {
                        byteArrayOutputStream.write(read);
                        byteArrayOutputStream.write(read2);
                    }
                }
            } else if (byteArrayOutputStream.size() == 0) {
                return null;
            }
        }
        return byteArrayOutputStream.toString(str);
    }

    public static int a(int[] iArr) {
        int i = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            if (iArr[i2] < i) {
                i = iArr[i2];
            }
        }
        return i;
    }

    public static int b(int[] iArr) {
        int i = Integer.MIN_VALUE;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            if (i < iArr[i2]) {
                i = iArr[i2];
            }
        }
        return i;
    }

    public static String a(Collection<?> collection, String str) {
        StringBuilder sb = new StringBuilder();
        a(sb, collection, str);
        return sb.toString();
    }

    private static void a(StringBuilder sb, Collection<?> collection, String str) {
        boolean z = true;
        for (Object next : collection) {
            if (z) {
                z = false;
            } else {
                sb.append(str);
            }
            sb.append(next.toString());
        }
    }

    public static String a(URI uri) {
        String str;
        Matcher matcher;
        Matcher matcher2;
        String host = uri.getHost();
        if (host != null) {
            return host;
        }
        String rawAuthority = uri.getRawAuthority();
        if (rawAuthority == null || (matcher2 = Pattern.compile("^(.*@)?([^:]+)(:\\d+)?$").matcher(rawAuthority)) == null || !matcher2.matches()) {
            str = null;
        } else {
            str = matcher2.group(2);
        }
        if (str != null) {
            return str;
        }
        String uri2 = uri.toString();
        if (uri2 == null || (matcher = Pattern.compile("^\\w+://([^@/]*@)?([^:/]+)(:\\d+)?(/.*)?$").matcher(uri2)) == null || !matcher.matches()) {
            return null;
        }
        return matcher.group(2);
    }
}
