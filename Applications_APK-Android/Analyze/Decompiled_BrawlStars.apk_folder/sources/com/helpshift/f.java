package com.helpshift;

import android.app.Application;
import com.helpshift.util.q;
import java.util.Map;

/* compiled from: CoreInternal */
final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Application f3391a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f3392b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ Map e;

    f(Application application, String str, String str2, String str3, Map map) {
        this.f3391a = application;
        this.f3392b = str;
        this.c = str2;
        this.d = str3;
        this.e = map;
    }

    public final void run() {
        if (!q.f4255a.get()) {
            CoreInternal.f3155a.a(this.f3391a, this.f3392b, this.c, this.d, this.e);
        }
    }
}
