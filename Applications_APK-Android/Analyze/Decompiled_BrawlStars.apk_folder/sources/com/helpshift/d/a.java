package com.helpshift.d;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/* compiled from: CollapseViewAnimation */
public class a extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private LinearLayout f3385a;

    /* renamed from: b  reason: collision with root package name */
    private int f3386b;

    public boolean willChangeBounds() {
        return true;
    }

    public a(LinearLayout linearLayout) {
        this.f3385a = linearLayout;
        this.f3386b = linearLayout.getHeight();
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        this.f3385a.getLayoutParams().height = (int) (((float) this.f3386b) * (1.0f - f));
        this.f3385a.requestLayout();
    }
}
