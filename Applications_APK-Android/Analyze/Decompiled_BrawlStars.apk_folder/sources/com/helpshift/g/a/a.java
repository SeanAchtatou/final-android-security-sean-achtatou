package com.helpshift.g.a;

import com.helpshift.common.c.b.h;
import com.helpshift.common.c.j;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.k;
import com.helpshift.common.e.aa;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.util.n;
import java.util.HashMap;

/* compiled from: WebSocketAuthDM */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public com.helpshift.g.b.a f3405a;

    /* renamed from: b  reason: collision with root package name */
    public aa f3406b;
    private final Object c = new Object();
    private j d;
    private ab e;
    private k f;

    public a(j jVar, ab abVar) {
        this.d = jVar;
        this.e = abVar;
        this.f = abVar.l();
        this.f3406b = abVar.o();
    }

    public com.helpshift.g.b.a a() {
        com.helpshift.g.b.a aVar;
        synchronized (this.c) {
            n.a("Helpshift_WebSocketAuthDM", "Fetching auth token", (Throwable) null, (com.helpshift.p.b.a[]) null);
            com.helpshift.common.c.b.j jVar = new com.helpshift.common.c.b.j(new h("/ws-config/", this.d, this.e));
            try {
                HashMap hashMap = new HashMap();
                hashMap.put("platform-id", this.e.c());
                aVar = this.f.k(jVar.a(new i(hashMap)).f3323b);
                try {
                    n.a("Helpshift_WebSocketAuthDM", "Auth token fetch successful", (Throwable) null, (com.helpshift.p.b.a[]) null);
                } catch (RootAPIException e2) {
                    e = e2;
                }
            } catch (RootAPIException e3) {
                e = e3;
                aVar = null;
                n.c("Helpshift_WebSocketAuthDM", "Exception in fetching auth token", e);
                return aVar;
            }
        }
        return aVar;
    }
}
