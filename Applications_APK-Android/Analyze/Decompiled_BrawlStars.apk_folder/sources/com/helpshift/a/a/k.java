package com.helpshift.a.a;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NotificationCompat;
import com.helpshift.a.b.c;
import com.helpshift.a.b.p;
import com.helpshift.util.n;
import com.helpshift.w.g;

/* compiled from: UserDB */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private static k f3169a;

    /* renamed from: b  reason: collision with root package name */
    private final m f3170b = new m();
    private final l c;

    private k(Context context) {
        this.c = new l(context, this.f3170b);
    }

    public static synchronized k a(Context context) {
        k kVar;
        synchronized (k.class) {
            if (f3169a == null) {
                f3169a = new k(context);
            }
            kVar = f3169a;
        }
        return kVar;
    }

    /* access modifiers changed from: package-private */
    public final c a(c cVar) {
        Long l;
        try {
            l = Long.valueOf(this.c.getWritableDatabase().insert("user_table", null, c(cVar)));
        } catch (Exception e) {
            n.c("Helpshift_UserDB", "Error in creating user", e);
            l = null;
        }
        if (l == null) {
            return null;
        }
        return new c(Long.valueOf(l.longValue()), cVar.b(), cVar.c(), cVar.d(), cVar.e(), cVar.f(), cVar.g(), cVar.h(), cVar.i(), cVar.j(), cVar.k());
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean b(c cVar) {
        boolean z;
        if (cVar.a() == null) {
            return false;
        }
        z = true;
        try {
            this.c.getWritableDatabase().update("user_table", c(cVar), "_id = ?", new String[]{String.valueOf(cVar.a())});
        } catch (Exception e) {
            n.c("Helpshift_UserDB", "Error in updating user", e);
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0032, code lost:
        if (r11 != null) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001e, code lost:
        if (r11 != null) goto L_0x0020;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003a A[SYNTHETIC, Splitter:B:24:0x003a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.helpshift.a.b.c a(java.lang.String r11, java.lang.String[] r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 0
            com.helpshift.a.a.l r1 = r10.c     // Catch:{ Exception -> 0x0029, all -> 0x0026 }
            android.database.sqlite.SQLiteDatabase r2 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x0029, all -> 0x0026 }
            java.lang.String r3 = "user_table"
            r4 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r5 = r11
            r6 = r12
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0029, all -> 0x0026 }
            boolean r12 = r11.moveToFirst()     // Catch:{ Exception -> 0x0024 }
            if (r12 == 0) goto L_0x001e
            com.helpshift.a.b.c r0 = r10.a(r11)     // Catch:{ Exception -> 0x0024 }
        L_0x001e:
            if (r11 == 0) goto L_0x0035
        L_0x0020:
            r11.close()     // Catch:{ all -> 0x003e }
            goto L_0x0035
        L_0x0024:
            r12 = move-exception
            goto L_0x002b
        L_0x0026:
            r12 = move-exception
            r11 = r0
            goto L_0x0038
        L_0x0029:
            r12 = move-exception
            r11 = r0
        L_0x002b:
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in reading user"
            com.helpshift.util.n.c(r1, r2, r12)     // Catch:{ all -> 0x0037 }
            if (r11 == 0) goto L_0x0035
            goto L_0x0020
        L_0x0035:
            monitor-exit(r10)
            return r0
        L_0x0037:
            r12 = move-exception
        L_0x0038:
            if (r11 == 0) goto L_0x003d
            r11.close()     // Catch:{ all -> 0x003e }
        L_0x003d:
            throw r12     // Catch:{ all -> 0x003e }
        L_0x003e:
            r11 = move-exception
            monitor-exit(r10)
            goto L_0x0042
        L_0x0041:
            throw r11
        L_0x0042:
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.k.a(java.lang.String, java.lang.String[]):com.helpshift.a.b.c");
    }

    /* access modifiers changed from: package-private */
    public final synchronized c a(Long l) {
        if (l == null) {
            return null;
        }
        return a("_id = ?", new String[]{l.toString()});
    }

    /* access modifiers changed from: package-private */
    public final synchronized c a(String str, String str2) {
        if (str == null && str2 == null) {
            return null;
        }
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        return a("identifier = ? AND email = ?", new String[]{str, str2});
    }

    /* access modifiers changed from: package-private */
    public final synchronized c a() {
        return a("active = ?", new String[]{m.f3173b.toString()});
    }

    /* access modifiers changed from: package-private */
    public final synchronized c b() {
        return a("anonymous = ?", new String[]{m.f3173b.toString()});
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
        if (r1 == null) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        if (r1 != null) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.helpshift.a.b.c> c() {
        /*
            r11 = this;
            monitor-enter(r11)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0047 }
            r0.<init>()     // Catch:{ all -> 0x0047 }
            r1 = 0
            com.helpshift.a.a.l r2 = r11.c     // Catch:{ Exception -> 0x0034 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0034 }
            java.lang.String r4 = "user_table"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r1 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0034 }
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x0034 }
            if (r2 == 0) goto L_0x002c
        L_0x001f:
            com.helpshift.a.b.c r2 = r11.a(r1)     // Catch:{ Exception -> 0x0034 }
            r0.add(r2)     // Catch:{ Exception -> 0x0034 }
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x0034 }
            if (r2 != 0) goto L_0x001f
        L_0x002c:
            if (r1 == 0) goto L_0x003f
        L_0x002e:
            r1.close()     // Catch:{ all -> 0x0047 }
            goto L_0x003f
        L_0x0032:
            r0 = move-exception
            goto L_0x0041
        L_0x0034:
            r2 = move-exception
            java.lang.String r3 = "Helpshift_UserDB"
            java.lang.String r4 = "Error in reading all users"
            com.helpshift.util.n.c(r3, r4, r2)     // Catch:{ all -> 0x0032 }
            if (r1 == 0) goto L_0x003f
            goto L_0x002e
        L_0x003f:
            monitor-exit(r11)
            return r0
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ all -> 0x0047 }
        L_0x0046:
            throw r0     // Catch:{ all -> 0x0047 }
        L_0x0047:
            r0 = move-exception
            monitor-exit(r11)
            goto L_0x004b
        L_0x004a:
            throw r0
        L_0x004b:
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.k.c():java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0057, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        com.helpshift.util.n.c("Helpshift_UserDB", "Error in activating user in finally block", r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0061, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007b, code lost:
        if (r1 != null) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r1.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0083, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        com.helpshift.util.n.c("Helpshift_UserDB", "Error in activating user in finally block", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008b, code lost:
        throw r10;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:12:0x0053, B:22:0x0068] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean b(java.lang.Long r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            r0 = 0
            if (r10 != 0) goto L_0x0006
            monitor-exit(r9)
            return r0
        L_0x0006:
            r1 = 0
            r2 = 1
            com.helpshift.a.a.l r3 = r9.c     // Catch:{ Exception -> 0x0063 }
            android.database.sqlite.SQLiteDatabase r1 = r3.getWritableDatabase()     // Catch:{ Exception -> 0x0063 }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Exception -> 0x0063 }
            r3.<init>()     // Catch:{ Exception -> 0x0063 }
            java.lang.String r4 = "active"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x0063 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0063 }
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ Exception -> 0x0063 }
            r4.<init>()     // Catch:{ Exception -> 0x0063 }
            java.lang.String r5 = "active"
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r0)     // Catch:{ Exception -> 0x0063 }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x0063 }
            r1.beginTransaction()     // Catch:{ Exception -> 0x0063 }
            java.lang.String r5 = "user_table"
            java.lang.String r6 = "_id = ?"
            java.lang.String[] r7 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0063 }
            java.lang.String r8 = r10.toString()     // Catch:{ Exception -> 0x0063 }
            r7[r0] = r8     // Catch:{ Exception -> 0x0063 }
            int r3 = r1.update(r5, r3, r6, r7)     // Catch:{ Exception -> 0x0063 }
            if (r3 <= 0) goto L_0x004e
            java.lang.String r3 = "user_table"
            java.lang.String r5 = "_id != ?"
            java.lang.String[] r6 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0063 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0063 }
            r6[r0] = r10     // Catch:{ Exception -> 0x0063 }
            r1.update(r3, r4, r5, r6)     // Catch:{ Exception -> 0x0063 }
        L_0x004e:
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x0063 }
            if (r1 == 0) goto L_0x005f
            r1.endTransaction()     // Catch:{ Exception -> 0x0057 }
            goto L_0x005f
        L_0x0057:
            r10 = move-exception
            java.lang.String r0 = "Helpshift_UserDB"
            java.lang.String r1 = "Error in activating user in finally block"
            com.helpshift.util.n.c(r0, r1, r10)     // Catch:{ all -> 0x0081 }
        L_0x005f:
            r0 = 1
            goto L_0x0079
        L_0x0061:
            r10 = move-exception
            goto L_0x007b
        L_0x0063:
            r10 = move-exception
            java.lang.String r2 = "Helpshift_UserDB"
            java.lang.String r3 = "Error in activating user"
            com.helpshift.util.n.c(r2, r3, r10)     // Catch:{ all -> 0x0061 }
            if (r1 == 0) goto L_0x0079
            r1.endTransaction()     // Catch:{ Exception -> 0x0071 }
            goto L_0x0079
        L_0x0071:
            r10 = move-exception
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in activating user in finally block"
            com.helpshift.util.n.c(r1, r2, r10)     // Catch:{ all -> 0x0081 }
        L_0x0079:
            monitor-exit(r9)
            return r0
        L_0x007b:
            if (r1 == 0) goto L_0x008b
            r1.endTransaction()     // Catch:{ Exception -> 0x0083 }
            goto L_0x008b
        L_0x0081:
            r10 = move-exception
            goto L_0x008c
        L_0x0083:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in activating user in finally block"
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x0081 }
        L_0x008b:
            throw r10     // Catch:{ all -> 0x0081 }
        L_0x008c:
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.k.b(java.lang.Long):boolean");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0032, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean c(java.lang.Long r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            r0 = 0
            if (r9 != 0) goto L_0x0006
            monitor-exit(r8)
            return r0
        L_0x0006:
            r1 = 1
            r2 = 0
            com.helpshift.a.a.l r4 = r8.c     // Catch:{ Exception -> 0x0023 }
            android.database.sqlite.SQLiteDatabase r4 = r4.getWritableDatabase()     // Catch:{ Exception -> 0x0023 }
            java.lang.String r5 = "user_table"
            java.lang.String r6 = "_id = ?"
            java.lang.String[] r7 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0023 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x0023 }
            r7[r0] = r9     // Catch:{ Exception -> 0x0023 }
            int r9 = r4.delete(r5, r6, r7)     // Catch:{ Exception -> 0x0023 }
            long r4 = (long) r9
            goto L_0x002c
        L_0x0021:
            r9 = move-exception
            goto L_0x0033
        L_0x0023:
            r9 = move-exception
            java.lang.String r4 = "Helpshift_UserDB"
            java.lang.String r5 = "Error in deleting user"
            com.helpshift.util.n.c(r4, r5, r9)     // Catch:{ all -> 0x0021 }
            r4 = r2
        L_0x002c:
            int r9 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r9 <= 0) goto L_0x0031
            r0 = 1
        L_0x0031:
            monitor-exit(r8)
            return r0
        L_0x0033:
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.k.c(java.lang.Long):boolean");
    }

    private c a(Cursor cursor) {
        Long valueOf = Long.valueOf(cursor.getLong(cursor.getColumnIndex("_id")));
        String string = cursor.getString(cursor.getColumnIndex("identifier"));
        String string2 = cursor.getString(cursor.getColumnIndex("name"));
        String string3 = cursor.getString(cursor.getColumnIndex(NotificationCompat.CATEGORY_EMAIL));
        String string4 = cursor.getString(cursor.getColumnIndex("deviceid"));
        String string5 = cursor.getString(cursor.getColumnIndex("auth_token"));
        return new c(valueOf, string, string3, string2, string4, cursor.getInt(cursor.getColumnIndex("active")) == m.f3173b.intValue(), cursor.getInt(cursor.getColumnIndex("anonymous")) == m.f3173b.intValue(), cursor.getInt(cursor.getColumnIndex("push_token_synced")) == m.f3173b.intValue(), string5, cursor.getInt(cursor.getColumnIndex("issue_exists")) == m.f3173b.intValue(), a(cursor.getInt(cursor.getColumnIndex("initial_state_synced"))));
    }

    private static ContentValues c(c cVar) {
        ContentValues contentValues = new ContentValues();
        if (cVar.a() != null) {
            contentValues.put("_id", cVar.a());
        }
        if (cVar.b() != null) {
            contentValues.put("identifier", cVar.b());
        } else {
            contentValues.put("identifier", "");
        }
        if (cVar.d() != null) {
            contentValues.put("name", cVar.d());
        } else {
            contentValues.put("name", "");
        }
        if (cVar.c() != null) {
            contentValues.put(NotificationCompat.CATEGORY_EMAIL, cVar.c());
        } else {
            contentValues.put(NotificationCompat.CATEGORY_EMAIL, "");
        }
        if (cVar.e() != null) {
            contentValues.put("deviceid", cVar.e());
        } else {
            contentValues.put("deviceid", "");
        }
        if (cVar.i() != null) {
            contentValues.put("auth_token", cVar.i());
        } else {
            contentValues.put("auth_token", "");
        }
        contentValues.put("active", Boolean.valueOf(cVar.f()));
        contentValues.put("anonymous", Boolean.valueOf(cVar.g()));
        contentValues.put("issue_exists", Boolean.valueOf(cVar.j()));
        contentValues.put("push_token_synced", Boolean.valueOf(cVar.h()));
        contentValues.put("initial_state_synced", Integer.valueOf(cVar.k().ordinal()));
        return contentValues;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x007a, code lost:
        if (r1 != null) goto L_0x007c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008a, code lost:
        if (r1 == null) goto L_0x008d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.helpshift.a.b.a> d() {
        /*
            r11 = this;
            monitor-enter(r11)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0095 }
            r0.<init>()     // Catch:{ all -> 0x0095 }
            r1 = 0
            com.helpshift.a.a.l r2 = r11.c     // Catch:{ Exception -> 0x0082 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0082 }
            java.lang.String r4 = "cleared_user_table"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r1 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0082 }
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x0082 }
            if (r2 == 0) goto L_0x007a
        L_0x001f:
            java.lang.String r2 = "_id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0082 }
            long r2 = r1.getLong(r2)     // Catch:{ Exception -> 0x0082 }
            java.lang.Long r5 = java.lang.Long.valueOf(r2)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "identifier"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r6 = r1.getString(r2)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "email"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r7 = r1.getString(r2)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "deviceid"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r9 = r1.getString(r2)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "auth_token"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r8 = r1.getString(r2)     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "sync_state"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0082 }
            int r2 = r1.getInt(r2)     // Catch:{ Exception -> 0x0082 }
            if (r2 < 0) goto L_0x0064
            r3 = 3
            if (r2 <= r3) goto L_0x0065
        L_0x0064:
            r2 = 0
        L_0x0065:
            com.helpshift.a.a.h[] r3 = com.helpshift.a.a.h.values()     // Catch:{ Exception -> 0x0082 }
            r10 = r3[r2]     // Catch:{ Exception -> 0x0082 }
            com.helpshift.a.b.a r2 = new com.helpshift.a.b.a     // Catch:{ Exception -> 0x0082 }
            r4 = r2
            r4.<init>(r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0082 }
            r0.add(r2)     // Catch:{ Exception -> 0x0082 }
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x0082 }
            if (r2 != 0) goto L_0x001f
        L_0x007a:
            if (r1 == 0) goto L_0x008d
        L_0x007c:
            r1.close()     // Catch:{ all -> 0x0095 }
            goto L_0x008d
        L_0x0080:
            r0 = move-exception
            goto L_0x008f
        L_0x0082:
            r2 = move-exception
            java.lang.String r3 = "Helpshift_UserDB"
            java.lang.String r4 = "Error in reading all cleared users"
            com.helpshift.util.n.c(r3, r4, r2)     // Catch:{ all -> 0x0080 }
            if (r1 == 0) goto L_0x008d
            goto L_0x007c
        L_0x008d:
            monitor-exit(r11)
            return r0
        L_0x008f:
            if (r1 == 0) goto L_0x0094
            r1.close()     // Catch:{ all -> 0x0095 }
        L_0x0094:
            throw r0     // Catch:{ all -> 0x0095 }
        L_0x0095:
            r0 = move-exception
            monitor-exit(r11)
            goto L_0x0099
        L_0x0098:
            throw r0
        L_0x0099:
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.k.d():java.util.List");
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean a(Long l, h hVar) {
        boolean z;
        z = true;
        try {
            SQLiteDatabase writableDatabase = this.c.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("sync_state", Integer.valueOf(hVar.ordinal()));
            writableDatabase.update("cleared_user_table", contentValues, "_id = ?", new String[]{l.toString()});
        } catch (Exception e) {
            n.c("Helpshift_UserDB", "Error in updating cleared user sync status", e);
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean d(Long l) {
        boolean z;
        long j;
        z = false;
        try {
            j = (long) this.c.getWritableDatabase().delete("user_table", "_id = ?", new String[]{String.valueOf(l)});
        } catch (Exception e) {
            n.c("Helpshift_UserDB", "Error in deleting cleared user", e);
            j = 0;
        }
        if (j > 0) {
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0075 A[SYNTHETIC, Splitter:B:27:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0083 A[SYNTHETIC, Splitter:B:34:0x0083] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.util.List<com.helpshift.r.a.a> r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = 0
            com.helpshift.a.a.l r1 = r6.c     // Catch:{ Exception -> 0x006b }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ Exception -> 0x006b }
            r1.beginTransaction()     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
        L_0x000f:
            boolean r2 = r7.hasNext()     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            if (r2 == 0) goto L_0x0051
            java.lang.Object r2 = r7.next()     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            com.helpshift.r.a.a r2 = (com.helpshift.r.a.a) r2     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            r3.<init>()     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            java.lang.String r4 = "identifier"
            java.lang.String r5 = r2.f3819a     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            java.lang.String r4 = "name"
            java.lang.String r5 = r2.c     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            java.lang.String r4 = "email"
            java.lang.String r5 = r2.f3820b     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            java.lang.String r4 = "serverid"
            java.lang.String r5 = r2.d     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            java.lang.String r4 = "migration_state"
            com.helpshift.r.c r2 = r2.e     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            int r2 = r2.ordinal()     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            r3.put(r4, r2)     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            java.lang.String r2 = "legacy_profile_table"
            r1.insert(r2, r0, r3)     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            goto L_0x000f
        L_0x0051:
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x0065, all -> 0x0063 }
            if (r1 == 0) goto L_0x007f
            r1.endTransaction()     // Catch:{ Exception -> 0x005a }
            goto L_0x007f
        L_0x005a:
            r7 = move-exception
            java.lang.String r0 = "Helpshift_UserDB"
            java.lang.String r1 = "Error in storing legacy profiles in finally block"
        L_0x005f:
            com.helpshift.util.n.c(r0, r1, r7)     // Catch:{ all -> 0x0087 }
            goto L_0x007f
        L_0x0063:
            r7 = move-exception
            goto L_0x0081
        L_0x0065:
            r7 = move-exception
            r0 = r1
            goto L_0x006c
        L_0x0068:
            r7 = move-exception
            r1 = r0
            goto L_0x0081
        L_0x006b:
            r7 = move-exception
        L_0x006c:
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in storing legacy profiles"
            com.helpshift.util.n.c(r1, r2, r7)     // Catch:{ all -> 0x0068 }
            if (r0 == 0) goto L_0x007f
            r0.endTransaction()     // Catch:{ Exception -> 0x0079 }
            goto L_0x007f
        L_0x0079:
            r7 = move-exception
            java.lang.String r0 = "Helpshift_UserDB"
            java.lang.String r1 = "Error in storing legacy profiles in finally block"
            goto L_0x005f
        L_0x007f:
            monitor-exit(r6)
            return
        L_0x0081:
            if (r1 == 0) goto L_0x0091
            r1.endTransaction()     // Catch:{ Exception -> 0x0089 }
            goto L_0x0091
        L_0x0087:
            r7 = move-exception
            goto L_0x0092
        L_0x0089:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in storing legacy profiles in finally block"
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x0087 }
        L_0x0091:
            throw r7     // Catch:{ all -> 0x0087 }
        L_0x0092:
            monitor-exit(r6)
            goto L_0x0095
        L_0x0094:
            throw r7
        L_0x0095:
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.k.a(java.util.List):void");
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        try {
            this.c.getWritableDatabase().delete("legacy_profile_table", "identifier = ?", new String[]{str});
        } catch (Exception e) {
            n.c("Helpshift_UserDB", "Error in deleting legacy profile", e);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0066, code lost:
        if (r1 != null) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007b, code lost:
        if (r1 != null) goto L_0x0068;
     */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0083 A[SYNTHETIC, Splitter:B:34:0x0083] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.helpshift.r.a.a b(java.lang.String r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            r0 = 1
            java.lang.String[] r5 = new java.lang.String[r0]     // Catch:{ all -> 0x0087 }
            r0 = 0
            r5[r0] = r10     // Catch:{ all -> 0x0087 }
            r10 = 0
            com.helpshift.a.a.l r1 = r9.c     // Catch:{ Exception -> 0x0072, all -> 0x006e }
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x0072, all -> 0x006e }
            java.lang.String r2 = "legacy_profile_table"
            r3 = 0
            java.lang.String r4 = "identifier = ?"
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0072, all -> 0x006e }
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x006c }
            if (r2 == 0) goto L_0x0066
            java.lang.String r2 = "identifier"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x006c }
            java.lang.String r4 = r1.getString(r2)     // Catch:{ Exception -> 0x006c }
            java.lang.String r2 = "email"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x006c }
            java.lang.String r5 = r1.getString(r2)     // Catch:{ Exception -> 0x006c }
            java.lang.String r2 = "name"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x006c }
            java.lang.String r6 = r1.getString(r2)     // Catch:{ Exception -> 0x006c }
            java.lang.String r2 = "serverid"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x006c }
            java.lang.String r7 = r1.getString(r2)     // Catch:{ Exception -> 0x006c }
            java.lang.String r2 = "migration_state"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x006c }
            int r2 = r1.getInt(r2)     // Catch:{ Exception -> 0x006c }
            if (r2 < 0) goto L_0x0059
            r3 = 3
            if (r2 <= r3) goto L_0x0058
            goto L_0x0059
        L_0x0058:
            r0 = r2
        L_0x0059:
            com.helpshift.r.c[] r2 = com.helpshift.r.c.values()     // Catch:{ Exception -> 0x006c }
            r8 = r2[r0]     // Catch:{ Exception -> 0x006c }
            com.helpshift.r.a.a r0 = new com.helpshift.r.a.a     // Catch:{ Exception -> 0x006c }
            r3 = r0
            r3.<init>(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x006c }
            r10 = r0
        L_0x0066:
            if (r1 == 0) goto L_0x007e
        L_0x0068:
            r1.close()     // Catch:{ all -> 0x0087 }
            goto L_0x007e
        L_0x006c:
            r0 = move-exception
            goto L_0x0074
        L_0x006e:
            r0 = move-exception
            r1 = r10
            r10 = r0
            goto L_0x0081
        L_0x0072:
            r0 = move-exception
            r1 = r10
        L_0x0074:
            java.lang.String r2 = "Helpshift_UserDB"
            java.lang.String r3 = "Error in reading legacy profile with identifier"
            com.helpshift.util.n.c(r2, r3, r0)     // Catch:{ all -> 0x0080 }
            if (r1 == 0) goto L_0x007e
            goto L_0x0068
        L_0x007e:
            monitor-exit(r9)
            return r10
        L_0x0080:
            r10 = move-exception
        L_0x0081:
            if (r1 == 0) goto L_0x0086
            r1.close()     // Catch:{ all -> 0x0087 }
        L_0x0086:
            throw r10     // Catch:{ all -> 0x0087 }
        L_0x0087:
            r10 = move-exception
            monitor-exit(r9)
            goto L_0x008b
        L_0x008a:
            throw r10
        L_0x008b:
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.k.b(java.lang.String):com.helpshift.r.a.a");
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean a(String str, com.helpshift.r.c cVar) {
        boolean z;
        z = true;
        try {
            SQLiteDatabase writableDatabase = this.c.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("migration_state", Integer.valueOf(cVar.ordinal()));
            writableDatabase.update("legacy_profile_table", contentValues, "identifier = ?", new String[]{str});
        } catch (Exception e) {
            n.c("Helpshift_UserDB", "Error in updating user migration sync status", e);
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0058 A[SYNTHETIC, Splitter:B:27:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0066 A[SYNTHETIC, Splitter:B:34:0x0066] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(java.util.List<com.helpshift.common.e.a.c> r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = 0
            com.helpshift.a.a.l r1 = r6.c     // Catch:{ Exception -> 0x004e }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ Exception -> 0x004e }
            r1.beginTransaction()     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
        L_0x000f:
            boolean r2 = r7.hasNext()     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            if (r2 == 0) goto L_0x0034
            java.lang.Object r2 = r7.next()     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            com.helpshift.common.e.a.c r2 = (com.helpshift.common.e.a.c) r2     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            r3.<init>()     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            java.lang.String r4 = "identifier"
            java.lang.String r5 = r2.f3313a     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            java.lang.String r4 = "analytics_event_id"
            java.lang.String r2 = r2.f3314b     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            r3.put(r4, r2)     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            java.lang.String r2 = "legacy_analytics_event_id_table"
            r1.insert(r2, r0, r3)     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            goto L_0x000f
        L_0x0034:
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x0048, all -> 0x0046 }
            if (r1 == 0) goto L_0x0062
            r1.endTransaction()     // Catch:{ Exception -> 0x003d }
            goto L_0x0062
        L_0x003d:
            r7 = move-exception
            java.lang.String r0 = "Helpshift_UserDB"
            java.lang.String r1 = "Error in storing legacy analytics events in finally block"
        L_0x0042:
            com.helpshift.util.n.c(r0, r1, r7)     // Catch:{ all -> 0x006a }
            goto L_0x0062
        L_0x0046:
            r7 = move-exception
            goto L_0x0064
        L_0x0048:
            r7 = move-exception
            r0 = r1
            goto L_0x004f
        L_0x004b:
            r7 = move-exception
            r1 = r0
            goto L_0x0064
        L_0x004e:
            r7 = move-exception
        L_0x004f:
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in storing legacy analytics events"
            com.helpshift.util.n.c(r1, r2, r7)     // Catch:{ all -> 0x004b }
            if (r0 == 0) goto L_0x0062
            r0.endTransaction()     // Catch:{ Exception -> 0x005c }
            goto L_0x0062
        L_0x005c:
            r7 = move-exception
            java.lang.String r0 = "Helpshift_UserDB"
            java.lang.String r1 = "Error in storing legacy analytics events in finally block"
            goto L_0x0042
        L_0x0062:
            monitor-exit(r6)
            return
        L_0x0064:
            if (r1 == 0) goto L_0x0074
            r1.endTransaction()     // Catch:{ Exception -> 0x006c }
            goto L_0x0074
        L_0x006a:
            r7 = move-exception
            goto L_0x0075
        L_0x006c:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in storing legacy analytics events in finally block"
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x006a }
        L_0x0074:
            throw r7     // Catch:{ all -> 0x006a }
        L_0x0075:
            monitor-exit(r6)
            goto L_0x0078
        L_0x0077:
            throw r7
        L_0x0078:
            goto L_0x0077
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.k.b(java.util.List):void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002a, code lost:
        if (r0 != null) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
        if (r0 != null) goto L_0x002c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0048 A[SYNTHETIC, Splitter:B:27:0x0048] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.String c(java.lang.String r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 1
            java.lang.String[] r5 = new java.lang.String[r0]     // Catch:{ all -> 0x004c }
            r0 = 0
            r5[r0] = r11     // Catch:{ all -> 0x004c }
            r11 = 0
            com.helpshift.a.a.l r0 = r10.c     // Catch:{ Exception -> 0x0037, all -> 0x0032 }
            android.database.sqlite.SQLiteDatabase r1 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x0037, all -> 0x0032 }
            java.lang.String r2 = "legacy_analytics_event_id_table"
            r3 = 0
            java.lang.String r4 = "identifier = ?"
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0037, all -> 0x0032 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0030 }
            if (r1 == 0) goto L_0x002a
            java.lang.String r1 = "analytics_event_id"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0030 }
            java.lang.String r11 = r0.getString(r1)     // Catch:{ Exception -> 0x0030 }
        L_0x002a:
            if (r0 == 0) goto L_0x0043
        L_0x002c:
            r0.close()     // Catch:{ all -> 0x004c }
            goto L_0x0043
        L_0x0030:
            r1 = move-exception
            goto L_0x0039
        L_0x0032:
            r0 = move-exception
            r9 = r0
            r0 = r11
            r11 = r9
            goto L_0x0046
        L_0x0037:
            r1 = move-exception
            r0 = r11
        L_0x0039:
            java.lang.String r2 = "Helpshift_UserDB"
            java.lang.String r3 = "Error in reading legacy analytics eventID with identifier"
            com.helpshift.util.n.c(r2, r3, r1)     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x0043
            goto L_0x002c
        L_0x0043:
            monitor-exit(r10)
            return r11
        L_0x0045:
            r11 = move-exception
        L_0x0046:
            if (r0 == 0) goto L_0x004b
            r0.close()     // Catch:{ all -> 0x004c }
        L_0x004b:
            throw r11     // Catch:{ all -> 0x004c }
        L_0x004c:
            r11 = move-exception
            monitor-exit(r10)
            goto L_0x0050
        L_0x004f:
            throw r11
        L_0x0050:
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.k.c(java.lang.String):java.lang.String");
    }

    private static p a(int i) {
        if (i < 0 || i > 3) {
            i = 0;
        }
        return p.values()[i];
    }

    private static ContentValues c(com.helpshift.w.c cVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("user_local_id", Long.valueOf(cVar.f4276a));
        contentValues.put("redaction_state", Integer.valueOf(cVar.f4277b.ordinal()));
        contentValues.put("redaction_type", Integer.valueOf(cVar.c.ordinal()));
        return contentValues;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(com.helpshift.w.c cVar) {
        try {
            this.c.getWritableDatabase().insert("redaction_info_table", null, c(cVar));
        } catch (Exception e) {
            n.c("Helpshift_UserDB", "Error in inserting redaction info of the user", e);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b(com.helpshift.w.c cVar) {
        try {
            this.c.getWritableDatabase().update("redaction_info_table", c(cVar), "user_local_id = ?", new String[]{String.valueOf(cVar.f4276a)});
        } catch (Exception e) {
            n.c("Helpshift_UserDB", "Error in updating redaction detail", e);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0061, code lost:
        if (r0 != null) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0076, code lost:
        if (r0 != null) goto L_0x0063;
     */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x007e A[SYNTHETIC, Splitter:B:39:0x007e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.helpshift.w.c a(long r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            r0 = 1
            java.lang.String[] r5 = new java.lang.String[r0]     // Catch:{ all -> 0x0082 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ all -> 0x0082 }
            r11 = 0
            r5[r11] = r10     // Catch:{ all -> 0x0082 }
            r10 = 0
            com.helpshift.a.a.l r0 = r9.c     // Catch:{ Exception -> 0x006d, all -> 0x0069 }
            android.database.sqlite.SQLiteDatabase r1 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x006d, all -> 0x0069 }
            java.lang.String r2 = "redaction_info_table"
            r3 = 0
            java.lang.String r4 = "user_local_id = ?"
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x006d, all -> 0x0069 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0067 }
            if (r1 == 0) goto L_0x0061
            java.lang.String r1 = "user_local_id"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0067 }
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x0067 }
            java.lang.String r3 = "redaction_state"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x0067 }
            int r3 = r0.getInt(r3)     // Catch:{ Exception -> 0x0067 }
            com.helpshift.w.g[] r4 = com.helpshift.w.g.values()     // Catch:{ Exception -> 0x0067 }
            if (r3 < 0) goto L_0x0041
            int r5 = r4.length     // Catch:{ Exception -> 0x0067 }
            if (r3 <= r5) goto L_0x0042
        L_0x0041:
            r3 = 0
        L_0x0042:
            r3 = r4[r3]     // Catch:{ Exception -> 0x0067 }
            java.lang.String r4 = "redaction_type"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x0067 }
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x0067 }
            com.helpshift.w.h[] r5 = com.helpshift.w.h.values()     // Catch:{ Exception -> 0x0067 }
            if (r4 < 0) goto L_0x0059
            int r6 = r5.length     // Catch:{ Exception -> 0x0067 }
            if (r4 <= r6) goto L_0x0058
            goto L_0x0059
        L_0x0058:
            r11 = r4
        L_0x0059:
            r11 = r5[r11]     // Catch:{ Exception -> 0x0067 }
            com.helpshift.w.c r4 = new com.helpshift.w.c     // Catch:{ Exception -> 0x0067 }
            r4.<init>(r1, r3, r11)     // Catch:{ Exception -> 0x0067 }
            r10 = r4
        L_0x0061:
            if (r0 == 0) goto L_0x0079
        L_0x0063:
            r0.close()     // Catch:{ all -> 0x0082 }
            goto L_0x0079
        L_0x0067:
            r11 = move-exception
            goto L_0x006f
        L_0x0069:
            r11 = move-exception
            r0 = r10
            r10 = r11
            goto L_0x007c
        L_0x006d:
            r11 = move-exception
            r0 = r10
        L_0x006f:
            java.lang.String r1 = "Helpshift_UserDB"
            java.lang.String r2 = "Error in reading redaction detail of the user"
            com.helpshift.util.n.c(r1, r2, r11)     // Catch:{ all -> 0x007b }
            if (r0 == 0) goto L_0x0079
            goto L_0x0063
        L_0x0079:
            monitor-exit(r9)
            return r10
        L_0x007b:
            r10 = move-exception
        L_0x007c:
            if (r0 == 0) goto L_0x0081
            r0.close()     // Catch:{ all -> 0x0082 }
        L_0x0081:
            throw r10     // Catch:{ all -> 0x0082 }
        L_0x0082:
            r10 = move-exception
            monitor-exit(r9)
            goto L_0x0086
        L_0x0085:
            throw r10
        L_0x0086:
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.k.a(long):com.helpshift.w.c");
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(long j, g gVar) {
        try {
            SQLiteDatabase writableDatabase = this.c.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("redaction_state", Integer.valueOf(gVar.ordinal()));
            writableDatabase.update("redaction_info_table", contentValues, "user_local_id = ?", new String[]{String.valueOf(j)});
        } catch (Exception e) {
            n.c("Helpshift_UserDB", "Error in updating redaction status", e);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b(long j) {
        try {
            this.c.getWritableDatabase().delete("redaction_info_table", "user_local_id = ?", new String[]{String.valueOf(j)});
        } catch (Exception e) {
            n.c("Helpshift_UserDB", "Error in deleting redaction detail", e);
        }
        return;
    }
}
