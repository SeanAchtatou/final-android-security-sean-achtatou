package com.helpshift.a.a;

import com.helpshift.w.b;
import com.helpshift.w.c;
import com.helpshift.w.g;

/* compiled from: AndroidRedactionDAO */
public class d implements b {

    /* renamed from: a  reason: collision with root package name */
    private final k f3162a;

    public d(k kVar) {
        this.f3162a = kVar;
    }

    public final c a(long j) {
        if (j < 0) {
            return null;
        }
        return this.f3162a.a(j);
    }

    public final void a(c cVar) {
        this.f3162a.a(cVar);
    }

    public final void b(c cVar) {
        this.f3162a.b(cVar);
    }

    public final void a(long j, g gVar) {
        if (j >= 0 && gVar != null) {
            this.f3162a.a(j, gVar);
        }
    }

    public final void b(long j) {
        if (j > 0) {
            this.f3162a.b(j);
        }
    }
}
