package com.helpshift.a.a.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.r.a.b;

/* compiled from: AndroidLegacyProfileDAO */
public class a extends SQLiteOpenHelper implements b {

    /* renamed from: a  reason: collision with root package name */
    private static a f3159a;

    private a(Context context) {
        super(context, "__hs__db_profiles", (SQLiteDatabase.CursorFactory) null, 3);
    }

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (f3159a == null) {
                f3159a = new a(context);
            }
            aVar = f3159a;
        }
        return aVar;
    }

    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4, types: [java.util.List<com.helpshift.a.a.i>] */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x009c, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a4, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00bf, code lost:
        r2.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a4 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x0013] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00bf  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.helpshift.a.a.i> a() {
        /*
            r14 = this;
            java.lang.String r0 = "IDENTIFIER"
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r14.getReadableDatabase()     // Catch:{ Exception -> 0x00ad }
            java.lang.String r3 = "profiles"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x00ad }
            boolean r3 = r2.moveToFirst()     // Catch:{ Exception -> 0x00a6, all -> 0x00a4 }
            if (r3 == 0) goto L_0x009e
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x00a6, all -> 0x00a4 }
            r3.<init>()     // Catch:{ Exception -> 0x00a6, all -> 0x00a4 }
        L_0x001e:
            com.helpshift.a.a.i r1 = new com.helpshift.a.a.i     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r4 = "_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            long r4 = r2.getLong(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.Long r5 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            int r4 = r2.getColumnIndex(r0)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            r6 = -1
            if (r4 != r6) goto L_0x003d
            java.lang.String r4 = r0.toLowerCase()     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
        L_0x003d:
            java.lang.String r6 = r2.getString(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r4 = "profile_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r7 = r2.getString(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r4 = "name"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r8 = r2.getString(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r4 = "email"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r9 = r2.getString(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r4 = "salt"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r10 = r2.getString(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r4 = "uid"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r11 = r2.getString(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r4 = "did"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r12 = r2.getString(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            java.lang.String r4 = "push_token_sync"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            int r4 = r2.getInt(r4)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            r13 = 1
            if (r4 != r13) goto L_0x008b
            goto L_0x008d
        L_0x008b:
            r4 = 0
            r13 = 0
        L_0x008d:
            r4 = r1
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            r3.add(r1)     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            boolean r1 = r2.moveToNext()     // Catch:{ Exception -> 0x009c, all -> 0x00a4 }
            if (r1 != 0) goto L_0x001e
            r1 = r3
            goto L_0x009e
        L_0x009c:
            r0 = move-exception
            goto L_0x00a8
        L_0x009e:
            if (r2 == 0) goto L_0x00bc
            r2.close()
            goto L_0x00bc
        L_0x00a4:
            r0 = move-exception
            goto L_0x00bd
        L_0x00a6:
            r0 = move-exception
            r3 = r1
        L_0x00a8:
            r1 = r2
            goto L_0x00af
        L_0x00aa:
            r0 = move-exception
            r2 = r1
            goto L_0x00bd
        L_0x00ad:
            r0 = move-exception
            r3 = r1
        L_0x00af:
            java.lang.String r2 = "Helpshift_ALProfileDAO"
            java.lang.String r4 = "Error in fetchProfiles"
            com.helpshift.util.n.c(r2, r4, r0)     // Catch:{ all -> 0x00aa }
            if (r1 == 0) goto L_0x00bb
            r1.close()
        L_0x00bb:
            r1 = r3
        L_0x00bc:
            return r1
        L_0x00bd:
            if (r2 == 0) goto L_0x00c2
            r2.close()
        L_0x00c2:
            goto L_0x00c4
        L_0x00c3:
            throw r0
        L_0x00c4:
            goto L_0x00c3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.a.a.a.a():java.util.List");
    }

    public final void b() {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        if (writableDatabase != null) {
            writableDatabase.execSQL("DROP TABLE IF EXISTS profiles");
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE profiles(_id INTEGER PRIMARY KEY AUTOINCREMENT, IDENTIFIER TEXT NOT NULL UNIQUE, profile_id TEXT UNIQUE, name TEXT, email TEXT, salt TEXT, uid TEXT, did TEXT, push_token_sync INTEGER );");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase.isOpen()) {
            if (i < 2) {
                sQLiteDatabase.execSQL("ALTER TABLE profiles ADD uid TEXT");
                sQLiteDatabase.execSQL("ALTER TABLE profiles ADD did TEXT");
            }
            if (i < 3) {
                sQLiteDatabase.execSQL("ALTER TABLE profiles ADD push_token_sync INTEGER");
            }
        }
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase.isOpen()) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS profiles");
            onCreate(sQLiteDatabase);
        }
    }
}
