package com.helpshift.a.a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: UserDBInfo */
public class m {

    /* renamed from: a  reason: collision with root package name */
    static final Integer f3172a = 2;

    /* renamed from: b  reason: collision with root package name */
    static final Integer f3173b = 1;

    public static List<String> a() {
        return new ArrayList(Arrays.asList("CREATE TABLE user_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, identifier TEXT, name TEXT, email TEXT, deviceid TEXT, auth_token TEXT, active INTEGER DEFAULT 0, anonymous INTEGER DEFAULT 0, issue_exists INTEGER DEFAULT 1, initial_state_synced INTEGER DEFAULT 0, push_token_synced INTEGER DEFAULT 0 );", "CREATE TABLE cleared_user_table ( _id INTEGER PRIMARY KEY AUTOINCREMENT, identifier TEXT, name TEXT, email TEXT, deviceid TEXT, auth_token TEXT, sync_state INTEGER );", "CREATE TABLE legacy_profile_table ( identifier TEXT PRIMARY KEY, name TEXT, email TEXT, serverid TEXT, migration_state INTEGER );", "CREATE TABLE legacy_analytics_event_id_table ( identifier TEXT, analytics_event_id TEXT );", "CREATE TABLE redaction_info_table ( user_local_id INTEGER PRIMARY KEY, redaction_state INTEGER , redaction_type INTEGER );"));
    }

    public static List<String> a(int i) {
        ArrayList arrayList = new ArrayList();
        if (i == 1) {
            arrayList.add("CREATE TABLE redaction_info_table ( user_local_id INTEGER PRIMARY KEY, redaction_state INTEGER , redaction_type INTEGER );");
        }
        return arrayList;
    }
}
