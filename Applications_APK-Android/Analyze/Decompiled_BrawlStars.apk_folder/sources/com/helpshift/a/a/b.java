package com.helpshift.a.a;

import com.helpshift.common.e.a.c;
import com.helpshift.common.j;
import com.helpshift.common.k;
import com.helpshift.r.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AndroidLegacyAnalyticsEventIDDAO */
public class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private k f3160a;

    public b(k kVar) {
        this.f3160a = kVar;
    }

    public final String a(String str) {
        if (k.a(str)) {
            return null;
        }
        return this.f3160a.c(str);
    }

    public final void a(List<c> list) {
        if (!j.a(list)) {
            ArrayList arrayList = new ArrayList();
            for (c next : list) {
                if (next != null && !k.a(next.f3313a) && !k.a(next.f3314b)) {
                    arrayList.add(next);
                }
            }
            this.f3160a.b(arrayList);
        }
    }
}
