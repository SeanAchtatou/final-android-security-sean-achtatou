package com.helpshift.a.a;

import com.helpshift.a.b.c;
import java.util.List;

/* compiled from: AndroidUserDAO */
public class e implements j {

    /* renamed from: a  reason: collision with root package name */
    private final k f3163a;

    public e(k kVar) {
        this.f3163a = kVar;
    }

    public final c a(c cVar) {
        if (cVar == null) {
            return null;
        }
        return this.f3163a.a(cVar);
    }

    public final boolean b(c cVar) {
        if (cVar == null) {
            return false;
        }
        return this.f3163a.b(cVar);
    }

    public final c a(String str, String str2) {
        if (str == null && str2 == null) {
            return null;
        }
        return this.f3163a.a(str, str2);
    }

    public final c a() {
        return this.f3163a.a();
    }

    public final c b() {
        return this.f3163a.b();
    }

    public final List<c> c() {
        return this.f3163a.c();
    }

    public final boolean a(Long l) {
        if (l == null || this.f3163a.a(l) == null) {
            return false;
        }
        return this.f3163a.b(l);
    }

    public final boolean b(Long l) {
        if (l == null) {
            return false;
        }
        return this.f3163a.c(l);
    }
}
