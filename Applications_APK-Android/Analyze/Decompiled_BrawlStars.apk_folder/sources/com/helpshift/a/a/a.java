package com.helpshift.a.a;

import java.util.List;

/* compiled from: AndroidClearedUserDAO */
public class a implements g {

    /* renamed from: a  reason: collision with root package name */
    private final k f3158a;

    public a(k kVar) {
        this.f3158a = kVar;
    }

    public final List<com.helpshift.a.b.a> a() {
        return this.f3158a.d();
    }

    public final boolean a(Long l, h hVar) {
        if (l == null || hVar == null) {
            return false;
        }
        return this.f3158a.a(l, hVar);
    }

    public final boolean a(Long l) {
        if (l == null) {
            return false;
        }
        return this.f3158a.d(l);
    }
}
