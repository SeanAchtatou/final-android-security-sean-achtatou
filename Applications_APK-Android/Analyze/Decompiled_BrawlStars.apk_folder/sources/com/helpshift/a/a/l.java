package com.helpshift.a.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.helpshift.p.b.a;
import com.helpshift.util.n;
import java.util.ArrayList;
import java.util.List;

/* compiled from: UserDBHelper */
public class l extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private m f3171a;

    l(Context context, m mVar) {
        super(context, "__hs_db_helpshift_users", (SQLiteDatabase.CursorFactory) null, m.f3172a.intValue());
        this.f3171a = mVar;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase.isOpen()) {
            List<String> a2 = m.a();
            try {
                sQLiteDatabase.beginTransaction();
                for (String execSQL : a2) {
                    sQLiteDatabase.execSQL(execSQL);
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    n.c("Helpshift_UserDBDB", "Error in onCreate inside finally block, ", e, new a[0]);
                }
            } catch (Exception e2) {
                n.c("Helpshift_UserDBDB", "Exception while creating userDB, version: " + m.f3172a, e2, new a[0]);
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    n.c("Helpshift_UserDBDB", "Error in onCreate inside finally block, ", e3, new a[0]);
                }
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e4) {
                    n.c("Helpshift_UserDBDB", "Error in onCreate inside finally block, ", e4, new a[0]);
                }
                throw th;
            }
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase.isOpen()) {
            List<String> a2 = m.a(i);
            try {
                sQLiteDatabase.beginTransaction();
                for (String execSQL : a2) {
                    sQLiteDatabase.execSQL(execSQL);
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    n.c("Helpshift_UserDBDB", "Exception while migrating userDB inside finally block, ", e, new a[0]);
                }
            } catch (Exception e2) {
                n.c("Helpshift_UserDBDB", "Exception while migrating userDB, version: " + m.f3172a, e2, new a[0]);
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    n.c("Helpshift_UserDBDB", "Exception while migrating userDB inside finally block, ", e3, new a[0]);
                }
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e4) {
                    n.c("Helpshift_UserDBDB", "Exception while migrating userDB inside finally block, ", e4, new a[0]);
                }
                throw th;
            }
        }
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (sQLiteDatabase.isOpen()) {
            m mVar = this.f3171a;
            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add("DROP TABLE IF EXISTS user_table");
            arrayList.add("DROP TABLE IF EXISTS legacy_profile_table");
            arrayList.add("DROP TABLE IF EXISTS cleared_user_table");
            arrayList.add("DROP TABLE IF EXISTS legacy_analytics_event_id_table");
            arrayList.add("DROP TABLE IF EXISTS redaction_info_table");
            arrayList.addAll(m.a());
            try {
                sQLiteDatabase.beginTransaction();
                for (String execSQL : arrayList) {
                    sQLiteDatabase.execSQL(execSQL);
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e) {
                    n.c("Helpshift_UserDBDB", "Exception while downgrading userDB inside finally block, ", e, new a[0]);
                }
            } catch (Exception e2) {
                n.c("Helpshift_UserDBDB", "Exception while downgrading userDB, version: " + i2, e2, new a[0]);
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e3) {
                    n.c("Helpshift_UserDBDB", "Exception while downgrading userDB inside finally block, ", e3, new a[0]);
                }
            } catch (Throwable th) {
                try {
                    if (sQLiteDatabase.inTransaction()) {
                        sQLiteDatabase.endTransaction();
                    }
                } catch (Exception e4) {
                    n.c("Helpshift_UserDBDB", "Exception while downgrading userDB inside finally block, ", e4, new a[0]);
                }
                throw th;
            }
        }
    }
}
