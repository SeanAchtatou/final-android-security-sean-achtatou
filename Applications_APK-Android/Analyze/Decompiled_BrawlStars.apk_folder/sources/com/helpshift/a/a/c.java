package com.helpshift.a.a;

import com.helpshift.common.j;
import com.helpshift.r.a.a;
import com.helpshift.r.b;
import java.util.List;

/* compiled from: AndroidLegacyProfileMigrationDAO */
public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private final k f3161a;

    public c(k kVar) {
        this.f3161a = kVar;
    }

    public final void a(List<a> list) {
        if (!j.a(list)) {
            this.f3161a.a(list);
        }
    }

    public final void a(String str) {
        if (str != null) {
            this.f3161a.a(str);
        }
    }

    public final a b(String str) {
        if (str == null) {
            return null;
        }
        return this.f3161a.b(str);
    }

    public final boolean a(String str, com.helpshift.r.c cVar) {
        if (str == null || cVar == null) {
            return false;
        }
        return this.f3161a.a(str, cVar);
    }
}
