package com.helpshift.a.a;

import java.io.Serializable;

/* compiled from: ProfileDTO */
public class i implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public final Long f3167a;

    /* renamed from: b  reason: collision with root package name */
    public final String f3168b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;
    public final String h;
    public final boolean i;

    public i(Long l, String str, String str2, String str3, String str4, String str5, String str6, String str7, boolean z) {
        this.f3167a = l;
        this.f3168b = str2;
        this.c = str;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = z;
    }
}
