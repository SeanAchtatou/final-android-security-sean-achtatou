package com.helpshift.a;

import com.helpshift.a.b.c;
import com.helpshift.common.c.j;
import com.helpshift.common.exception.b;
import com.helpshift.k;
import com.helpshift.l.l;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AuthenticationFailureDM */
public class a {

    /* renamed from: a  reason: collision with root package name */
    List<C0132a> f3156a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    j f3157b;

    /* renamed from: com.helpshift.a.a$a  reason: collision with other inner class name */
    /* compiled from: AuthenticationFailureDM */
    public interface C0132a {
        void a();
    }

    public a(j jVar) {
        this.f3157b = jVar;
    }

    public final void a(C0132a aVar) {
        this.f3156a.add(aVar);
    }

    public final void b(C0132a aVar) {
        this.f3156a.remove(aVar);
    }

    public final void a(c cVar, com.helpshift.common.exception.a aVar) {
        if (cVar.f) {
            com.helpshift.l.a aVar2 = null;
            if (aVar == b.AUTH_TOKEN_NOT_PROVIDED) {
                aVar2 = com.helpshift.l.a.AUTH_TOKEN_NOT_PROVIDED;
            } else if (aVar == b.INVALID_AUTH_TOKEN) {
                aVar2 = com.helpshift.l.a.INVALID_AUTH_TOKEN;
            }
            if (aVar2 != null) {
                for (C0132a next : this.f3156a) {
                    if (next != null) {
                        next.a();
                    }
                }
                com.helpshift.l.c cVar2 = this.f3157b.e;
                if (cVar2.f3738b != null && cVar.f) {
                    String str = cVar.f3176a + "_" + cVar.i;
                    if (!cVar2.c.containsKey(str) || !cVar2.c.get(str).booleanValue()) {
                        cVar2.c.put(str, true);
                        k.a aVar3 = new k.a(cVar.f3177b, cVar.c);
                        aVar3.c = cVar.d;
                        aVar3.d = cVar.i;
                        cVar2.f3737a.c(new l(cVar2, aVar3.a(), aVar2));
                    }
                }
            }
        }
    }
}
