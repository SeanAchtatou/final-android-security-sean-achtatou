package com.helpshift.a.b;

import android.support.v4.app.NotificationCompat;
import com.helpshift.a.a.g;
import com.helpshift.a.a.h;
import com.helpshift.a.a.j;
import com.helpshift.a.a.n;
import com.helpshift.a.b;
import com.helpshift.a.b.c;
import com.helpshift.common.a;
import com.helpshift.common.b;
import com.helpshift.common.c.b.o;
import com.helpshift.common.c.b.q;
import com.helpshift.common.c.b.r;
import com.helpshift.common.c.b.s;
import com.helpshift.common.c.b.u;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.ab;
import com.helpshift.common.e.y;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.k;
import com.helpshift.w.d;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: UserManagerDM */
public class e implements a {

    /* renamed from: a  reason: collision with root package name */
    public j f3182a;

    /* renamed from: b  reason: collision with root package name */
    public n f3183b;
    public com.helpshift.common.b.a c;
    public g d;
    public y e;
    public c f;
    public ab g;
    public com.helpshift.common.c.j h;
    private h i;
    private Set<b> j;

    public e(ab abVar, com.helpshift.common.c.j jVar) {
        this.g = abVar;
        this.h = jVar;
    }

    public final c a() {
        c cVar = this.f;
        if (cVar != null) {
            return cVar;
        }
        this.f = this.f3182a.a();
        c cVar2 = this.f;
        if (cVar2 == null) {
            b();
        } else {
            a((b) cVar2);
            this.i = null;
        }
        return this.f;
    }

    public final synchronized void a(k kVar) {
        c a2 = this.f3182a.a(kVar.f3727a, kVar.f3728b);
        if (a2 == null) {
            a2 = this.f3182a.a(b(kVar));
        }
        if (a2 != null) {
            a((b) a2);
            c(a2);
        }
    }

    public final synchronized boolean b() {
        c d2 = d();
        if (d2 == null) {
            d2 = c();
        }
        c(d2);
        return true;
    }

    public final synchronized c c() {
        return this.f3182a.a(new c(null, l(), null, null, this.e.w(), false, true, false, null, true, p.NOT_STARTED));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void c(com.helpshift.a.b.c r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            if (r3 != 0) goto L_0x0005
            monitor-exit(r2)
            return
        L_0x0005:
            com.helpshift.a.b.c r0 = r2.f     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x0017
            com.helpshift.a.b.c r0 = r2.f     // Catch:{ all -> 0x0050 }
            java.lang.Long r0 = r0.f3176a     // Catch:{ all -> 0x0050 }
            java.lang.Long r1 = r3.f3176a     // Catch:{ all -> 0x0050 }
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x0017
            monitor-exit(r2)
            return
        L_0x0017:
            com.helpshift.a.a.j r0 = r2.f3182a     // Catch:{ all -> 0x0050 }
            java.lang.Long r1 = r3.f3176a     // Catch:{ all -> 0x0050 }
            boolean r0 = r0.a(r1)     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x004e
            com.helpshift.a.b.c r0 = r2.f     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x0038
            com.helpshift.a.b.c$a r0 = new com.helpshift.a.b.c$a     // Catch:{ all -> 0x0050 }
            com.helpshift.a.b.c r1 = r2.f     // Catch:{ all -> 0x0050 }
            r0.<init>(r1)     // Catch:{ all -> 0x0050 }
            r1 = 0
            r0.c = r1     // Catch:{ all -> 0x0050 }
            com.helpshift.a.b.c r0 = r0.a()     // Catch:{ all -> 0x0050 }
            com.helpshift.a.b.c r1 = r2.f     // Catch:{ all -> 0x0050 }
            r2.a(r1, r0)     // Catch:{ all -> 0x0050 }
        L_0x0038:
            com.helpshift.a.b.c$a r0 = new com.helpshift.a.b.c$a     // Catch:{ all -> 0x0050 }
            r0.<init>(r3)     // Catch:{ all -> 0x0050 }
            r3 = 1
            r0.c = r3     // Catch:{ all -> 0x0050 }
            com.helpshift.a.b.c r3 = r0.a()     // Catch:{ all -> 0x0050 }
            r2.f = r3     // Catch:{ all -> 0x0050 }
            r3 = 0
            r2.i = r3     // Catch:{ all -> 0x0050 }
            com.helpshift.a.b.c r3 = r2.f     // Catch:{ all -> 0x0050 }
            r2.a(r3)     // Catch:{ all -> 0x0050 }
        L_0x004e:
            monitor-exit(r2)
            return
        L_0x0050:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.b.e.c(com.helpshift.a.b.c):void");
    }

    private synchronized c b(k kVar) {
        return new c(null, kVar.f3727a, kVar.f3728b, kVar.c, this.e.w(), false, false, false, kVar.d, true, p.NOT_STARTED);
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [java.io.Serializable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.lang.String l() {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            com.helpshift.common.b.a r1 = r5.c     // Catch:{ all -> 0x0054 }
            java.lang.String r2 = "anonymous_user_id_backup_key"
            java.io.Serializable r1 = r1.a(r2)     // Catch:{ all -> 0x0054 }
            boolean r2 = r1 instanceof java.lang.String     // Catch:{ all -> 0x0054 }
            if (r2 == 0) goto L_0x0011
            r0 = r1
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0054 }
        L_0x0011:
            boolean r1 = com.helpshift.common.k.a(r0)     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x0052
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0054 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0054 }
            r2.<init>()     // Catch:{ all -> 0x0054 }
            java.lang.String r3 = "hsft_anon_"
            r2.append(r3)     // Catch:{ all -> 0x0054 }
            com.helpshift.common.g.e r3 = com.helpshift.util.i.c     // Catch:{ all -> 0x0054 }
            java.util.Date r4 = new java.util.Date     // Catch:{ all -> 0x0054 }
            r4.<init>(r0)     // Catch:{ all -> 0x0054 }
            java.lang.String r0 = r3.a(r4)     // Catch:{ all -> 0x0054 }
            r2.append(r0)     // Catch:{ all -> 0x0054 }
            java.lang.String r0 = "-"
            r2.append(r0)     // Catch:{ all -> 0x0054 }
            java.lang.String r0 = "abcdefghijklmnopqrstuvwxyz0123456789"
            char[] r0 = r0.toCharArray()     // Catch:{ all -> 0x0054 }
            r1 = 15
            java.lang.String r0 = com.helpshift.common.k.a(r0, r1)     // Catch:{ all -> 0x0054 }
            r2.append(r0)     // Catch:{ all -> 0x0054 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0054 }
            com.helpshift.common.b.a r1 = r5.c     // Catch:{ all -> 0x0054 }
            java.lang.String r2 = "anonymous_user_id_backup_key"
            r1.a(r2, r0)     // Catch:{ all -> 0x0054 }
        L_0x0052:
            monitor-exit(r5)
            return r0
        L_0x0054:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.b.e.l():java.lang.String");
    }

    public final c d() {
        c cVar = this.f;
        if (cVar == null || !cVar.g) {
            return this.f3182a.b();
        }
        return this.f;
    }

    public final boolean a(c cVar) {
        Long l;
        if (cVar == null) {
            return false;
        }
        boolean b2 = this.f3182a.b(cVar.f3176a);
        if (b2) {
            if (cVar.g) {
                this.c.b("anonymous_user_id_backup_key");
            }
            c cVar2 = this.f;
            if (!(cVar2 == null || (l = cVar2.f3176a) == null || !l.equals(cVar.f3176a))) {
                Set<b> set = this.j;
                if (set != null) {
                    set.remove(this.f);
                }
                this.f = null;
                this.i = null;
            }
        }
        return b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.a.b.e.b(com.helpshift.a.b.c, boolean):void
     arg types: [com.helpshift.a.b.c, int]
     candidates:
      com.helpshift.a.b.e.b(com.helpshift.a.b.c, java.lang.String):void
      com.helpshift.a.b.e.b(com.helpshift.a.b.c, boolean):void */
    public final synchronized void e() {
        for (c next : this.f3182a.c()) {
            if (this.f == null || !next.f3176a.equals(this.f.f3176a)) {
                b(next, false);
            } else {
                b(this.f, false);
            }
        }
    }

    public final synchronized void f() {
        try {
            m();
        } catch (RootAPIException e2) {
            this.h.g.a(b.a.PUSH_TOKEN, e2.a());
            throw e2;
        }
    }

    public final synchronized void g() {
        if (k().a() == l.COMPLETED) {
            this.h.b(new f(this));
        }
    }

    public final String h() {
        c a2 = a();
        return a2.g ? this.f3183b.a() : a2.f3177b;
    }

    public final void a(String str) {
        this.f3183b.a(str);
    }

    public final List<c> i() {
        return this.f3182a.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.a.b.e.b(com.helpshift.a.b.c, boolean):void
     arg types: [com.helpshift.a.b.c, int]
     candidates:
      com.helpshift.a.b.e.b(com.helpshift.a.b.c, java.lang.String):void
      com.helpshift.a.b.e.b(com.helpshift.a.b.c, boolean):void */
    private void m() {
        String x = this.e.x();
        c a2 = a();
        if (!com.helpshift.common.k.a(x) && !a2.h && a2.j && k().a() == l.COMPLETED) {
            HashMap<String, String> a3 = o.a(a2);
            a3.put("token", x);
            try {
                new com.helpshift.common.c.b.j(new com.helpshift.common.c.b.g(new u(new com.helpshift.common.c.b.b(new s(new q("/update-push-token/", this.h, this.g), this.g))))).a(new i(a3));
                b(a2, true);
            } catch (RootAPIException e2) {
                if (e2.c != com.helpshift.common.exception.b.USER_NOT_FOUND) {
                    if (e2.c == com.helpshift.common.exception.b.INVALID_AUTH_TOKEN || e2.c == com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED) {
                        this.h.j.a(a2, e2.c);
                        throw e2;
                    } else if (e2.c == com.helpshift.common.exception.b.NON_RETRIABLE) {
                        b(a2, true);
                    } else {
                        throw e2;
                    }
                }
            }
        }
    }

    public final synchronized void a(c cVar, String str) {
        c.a aVar = new c.a(cVar);
        aVar.e = str;
        c a2 = aVar.a();
        if (this.f3182a.b(a2)) {
            a(cVar, a2);
        }
    }

    public final synchronized void b(c cVar, String str) {
        c.a aVar = new c.a(cVar);
        aVar.f3179b = str;
        c a2 = aVar.a();
        if (this.f3182a.b(a2)) {
            a(cVar, a2);
        }
    }

    public final synchronized void j() {
        for (c b2 : i()) {
            b(b2);
        }
        n();
    }

    public final synchronized void b(c cVar) {
        a(cVar, p.NOT_STARTED);
    }

    private synchronized void a(com.helpshift.a.b bVar) {
        if (bVar != null) {
            if (this.j == null) {
                this.j = new HashSet();
            }
            this.j.add(bVar);
        }
    }

    public synchronized void a(c cVar, c cVar2) {
        if (this.j != null) {
            for (com.helpshift.a.b a2 : this.j) {
                a2.a(cVar, cVar2);
            }
        }
    }

    public final void a(b.a aVar) {
        int i2 = g.f3185a[aVar.ordinal()];
        if (i2 == 1) {
            m();
        } else if (i2 == 2) {
            List<a> a2 = this.d.a();
            if (!com.helpshift.common.j.a(a2)) {
                for (a next : a2) {
                    if (next.f == h.COMPLETED) {
                        this.d.a(next.f3174a);
                    } else if (!(next == null || next.f3174a == null || next.f == h.COMPLETED || next.f == h.IN_PROGRESS)) {
                        com.helpshift.common.c.b.j jVar = new com.helpshift.common.c.b.j(new com.helpshift.common.c.b.g(new u(new s(new r("/clear-profile/", this.h, this.g), this.g))));
                        HashMap hashMap = new HashMap();
                        if (next != null) {
                            if (!com.helpshift.common.k.a(next.e)) {
                                hashMap.put("did", next.e);
                            }
                            if (!com.helpshift.common.k.a(next.f3175b)) {
                                hashMap.put("uid", next.f3175b);
                            }
                            if (!com.helpshift.common.k.a(next.c)) {
                                hashMap.put(NotificationCompat.CATEGORY_EMAIL, next.c);
                            }
                            if (!com.helpshift.common.k.a(next.d)) {
                                hashMap.put("user_auth_token", next.d);
                            }
                        }
                        this.d.a(next.f3174a, h.IN_PROGRESS);
                        try {
                            jVar.a(new i(hashMap));
                            this.d.a(next.f3174a, h.COMPLETED);
                            this.d.a(next.f3174a);
                        } catch (RootAPIException e2) {
                            if (e2.c == com.helpshift.common.exception.b.USER_NOT_FOUND || e2.c == com.helpshift.common.exception.b.NON_RETRIABLE) {
                                this.d.a(next.f3174a, h.COMPLETED);
                                this.d.a(next.f3174a);
                            } else {
                                this.d.a(next.f3174a, h.FAILED);
                                throw e2;
                            }
                        }
                    }
                }
            }
        }
    }

    public final synchronized h k() {
        if (this.i == null) {
            h hVar = new h(this.g, this.h, a(), this, this.h.i.a());
            d dVar = hVar.f;
            com.helpshift.w.g b2 = dVar.b();
            if (b2 == com.helpshift.w.g.IN_PROGRESS) {
                dVar.a(b2, com.helpshift.w.g.PENDING);
            }
            com.helpshift.r.d dVar2 = hVar.e;
            if (dVar2.a() == com.helpshift.r.c.IN_PROGRESS) {
                dVar2.a(com.helpshift.r.c.IN_PROGRESS, com.helpshift.r.c.NOT_STARTED);
            }
            m mVar = hVar.d;
            if (mVar.f3195b.k == p.IN_PROGRESS) {
                mVar.a(p.IN_PROGRESS, p.NOT_STARTED);
            }
            hVar.f3186a.g.a(b.a.MIGRATION, hVar);
            hVar.f3186a.g.a(b.a.SYNC_USER, hVar);
            this.i = hVar;
        }
        return this.i;
    }

    private synchronized void n() {
        this.i = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.helpshift.a.b.c r2, boolean r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            boolean r0 = r2.j     // Catch:{ all -> 0x001f }
            if (r0 != r3) goto L_0x0007
            monitor-exit(r1)
            return
        L_0x0007:
            com.helpshift.a.b.c$a r0 = new com.helpshift.a.b.c$a     // Catch:{ all -> 0x001f }
            r0.<init>(r2)     // Catch:{ all -> 0x001f }
            r0.f = r3     // Catch:{ all -> 0x001f }
            com.helpshift.a.b.c r3 = r0.a()     // Catch:{ all -> 0x001f }
            com.helpshift.a.a.j r0 = r1.f3182a     // Catch:{ all -> 0x001f }
            boolean r0 = r0.b(r3)     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x001d
            r1.a(r2, r3)     // Catch:{ all -> 0x001f }
        L_0x001d:
            monitor-exit(r1)
            return
        L_0x001f:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.b.e.a(com.helpshift.a.b.c, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.helpshift.a.b.c r2, com.helpshift.a.b.p r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.helpshift.a.b.p r0 = r2.k     // Catch:{ all -> 0x001f }
            if (r0 != r3) goto L_0x0007
            monitor-exit(r1)
            return
        L_0x0007:
            com.helpshift.a.b.c$a r0 = new com.helpshift.a.b.c$a     // Catch:{ all -> 0x001f }
            r0.<init>(r2)     // Catch:{ all -> 0x001f }
            r0.g = r3     // Catch:{ all -> 0x001f }
            com.helpshift.a.b.c r3 = r0.a()     // Catch:{ all -> 0x001f }
            com.helpshift.a.a.j r0 = r1.f3182a     // Catch:{ all -> 0x001f }
            boolean r0 = r0.b(r3)     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x001d
            r1.a(r2, r3)     // Catch:{ all -> 0x001f }
        L_0x001d:
            monitor-exit(r1)
            return
        L_0x001f:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.b.e.a(com.helpshift.a.b.c, com.helpshift.a.b.p):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void b(com.helpshift.a.b.c r2, boolean r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            boolean r0 = r2.h     // Catch:{ all -> 0x001f }
            if (r0 != r3) goto L_0x0007
            monitor-exit(r1)
            return
        L_0x0007:
            com.helpshift.a.b.c$a r0 = new com.helpshift.a.b.c$a     // Catch:{ all -> 0x001f }
            r0.<init>(r2)     // Catch:{ all -> 0x001f }
            r0.d = r3     // Catch:{ all -> 0x001f }
            com.helpshift.a.b.c r3 = r0.a()     // Catch:{ all -> 0x001f }
            com.helpshift.a.a.j r0 = r1.f3182a     // Catch:{ all -> 0x001f }
            boolean r0 = r0.b(r3)     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x001d
            r1.a(r2, r3)     // Catch:{ all -> 0x001f }
        L_0x001d:
            monitor-exit(r1)
            return
        L_0x001f:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.b.e.b(com.helpshift.a.b.c, boolean):void");
    }
}
