package com.helpshift.a.b;

import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import java.lang.ref.WeakReference;

/* compiled from: UserSyncDM */
public class m {

    /* renamed from: a  reason: collision with root package name */
    j f3194a;

    /* renamed from: b  reason: collision with root package name */
    c f3195b;
    private ab c;
    private e d;
    private b e;
    private WeakReference<a> f;

    /* compiled from: UserSyncDM */
    public interface a {
        void a(p pVar);
    }

    public m(ab abVar, j jVar, c cVar, e eVar, b bVar, a aVar) {
        this.c = abVar;
        this.f3194a = jVar;
        this.f3195b = cVar;
        this.d = eVar;
        this.e = bVar;
        this.f = new WeakReference<>(aVar);
    }

    /* access modifiers changed from: package-private */
    public void a(p pVar, p pVar2) {
        WeakReference<a> weakReference = this.f;
        a aVar = weakReference != null ? weakReference.get() : null;
        this.d.a(this.f3195b, pVar2);
        if (aVar != null) {
            this.f3194a.a(new n(this, aVar, pVar, pVar2));
        }
    }

    public final p a() {
        return this.f3195b.k;
    }

    public final void b() {
        p a2 = a();
        if (a2 != p.COMPLETED && a2 != p.IN_PROGRESS) {
            this.f3194a.b(new o(this));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.a.b.e.a(com.helpshift.a.b.c, boolean):void
     arg types: [com.helpshift.a.b.c, int]
     candidates:
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, com.helpshift.a.b.c):void
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, com.helpshift.a.b.p):void
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, java.lang.String):void
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.b.b.b(long, boolean):void
     arg types: [long, int]
     candidates:
      com.helpshift.j.b.b.b(long, java.lang.String):void
      com.helpshift.j.b.b.b(long, boolean):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0057, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void c() {
        /*
            r5 = this;
            monitor-enter(r5)
            com.helpshift.a.b.p r0 = r5.a()     // Catch:{ all -> 0x005e }
            com.helpshift.a.b.p r1 = com.helpshift.a.b.p.NOT_STARTED     // Catch:{ all -> 0x005e }
            if (r0 == r1) goto L_0x000f
            com.helpshift.a.b.p r1 = com.helpshift.a.b.p.FAILED     // Catch:{ all -> 0x005e }
            if (r0 == r1) goto L_0x000f
            monitor-exit(r5)
            return
        L_0x000f:
            com.helpshift.a.b.p r1 = com.helpshift.a.b.p.IN_PROGRESS     // Catch:{ all -> 0x005e }
            r5.a(r0, r1)     // Catch:{ all -> 0x005e }
            com.helpshift.a.b.b r1 = r5.e     // Catch:{ RootAPIException -> 0x001f }
            r1.a()     // Catch:{ RootAPIException -> 0x001f }
            com.helpshift.a.b.p r1 = com.helpshift.a.b.p.COMPLETED     // Catch:{ RootAPIException -> 0x001f }
            r5.a(r0, r1)     // Catch:{ RootAPIException -> 0x001f }
            goto L_0x0056
        L_0x001f:
            r1 = move-exception
            int r2 = r1.a()     // Catch:{ all -> 0x005e }
            java.lang.Integer r3 = com.helpshift.common.c.b.p.n     // Catch:{ all -> 0x005e }
            int r3 = r3.intValue()     // Catch:{ all -> 0x005e }
            if (r2 != r3) goto L_0x004b
            com.helpshift.a.b.p r1 = com.helpshift.a.b.p.COMPLETED     // Catch:{ all -> 0x005e }
            r5.a(r0, r1)     // Catch:{ all -> 0x005e }
            com.helpshift.a.b.e r0 = r5.d     // Catch:{ all -> 0x005e }
            com.helpshift.a.b.c r1 = r5.f3195b     // Catch:{ all -> 0x005e }
            r2 = 0
            r0.a(r1, r2)     // Catch:{ all -> 0x005e }
            com.helpshift.common.e.ab r0 = r5.c     // Catch:{ all -> 0x005e }
            com.helpshift.j.b.b r0 = r0.e()     // Catch:{ all -> 0x005e }
            com.helpshift.a.b.c r1 = r5.f3195b     // Catch:{ all -> 0x005e }
            java.lang.Long r1 = r1.f3176a     // Catch:{ all -> 0x005e }
            long r3 = r1.longValue()     // Catch:{ all -> 0x005e }
            r0.b(r3, r2)     // Catch:{ all -> 0x005e }
            goto L_0x0056
        L_0x004b:
            com.helpshift.common.exception.a r2 = r1.c     // Catch:{ all -> 0x005e }
            com.helpshift.common.exception.b r3 = com.helpshift.common.exception.b.NON_RETRIABLE     // Catch:{ all -> 0x005e }
            if (r2 != r3) goto L_0x0058
            com.helpshift.a.b.p r1 = com.helpshift.a.b.p.FAILED     // Catch:{ all -> 0x005e }
            r5.a(r0, r1)     // Catch:{ all -> 0x005e }
        L_0x0056:
            monitor-exit(r5)
            return
        L_0x0058:
            com.helpshift.a.b.p r2 = com.helpshift.a.b.p.FAILED     // Catch:{ all -> 0x005e }
            r5.a(r0, r2)     // Catch:{ all -> 0x005e }
            throw r1     // Catch:{ all -> 0x005e }
        L_0x005e:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.a.b.m.c():void");
    }
}
