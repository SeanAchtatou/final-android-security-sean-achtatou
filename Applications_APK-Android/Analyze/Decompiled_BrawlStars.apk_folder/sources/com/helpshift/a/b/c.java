package com.helpshift.a.b;

import com.helpshift.a.b;
import java.util.Observable;

/* compiled from: UserDM */
public class c extends Observable implements b {

    /* renamed from: a  reason: collision with root package name */
    public Long f3176a;

    /* renamed from: b  reason: collision with root package name */
    public String f3177b;
    public String c;
    public String d;
    public String e;
    public boolean f;
    public boolean g;
    public boolean h;
    public String i;
    public boolean j;
    public p k;

    public c(Long l, String str, String str2, String str3, String str4, boolean z, boolean z2, boolean z3, String str5, boolean z4, p pVar) {
        this.f3176a = l;
        this.f3177b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = z;
        this.g = z2;
        this.h = z3;
        this.i = str5;
        this.j = z4;
        this.k = pVar;
    }

    public final Long a() {
        return this.f3176a;
    }

    public final String b() {
        return this.f3177b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final boolean f() {
        return this.f;
    }

    public final boolean g() {
        return this.g;
    }

    public final boolean h() {
        return this.h;
    }

    public final String i() {
        return this.i;
    }

    public final boolean j() {
        return this.j;
    }

    public final p k() {
        return this.k;
    }

    public final void a(c cVar, c cVar2) {
        if (equals(cVar)) {
            this.j = cVar2.j;
            this.i = cVar2.i;
            this.d = cVar2.d;
            this.c = cVar2.c;
            this.k = cVar2.k;
            this.f = cVar2.f;
            this.h = cVar2.h;
            setChanged();
            notifyObservers();
        }
    }

    /* compiled from: UserDM */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public String f3178a;

        /* renamed from: b  reason: collision with root package name */
        public String f3179b;
        boolean c;
        boolean d;
        String e;
        boolean f;
        p g;
        private Long h;
        private String i;
        private String j;
        private boolean k;

        public a(c cVar) {
            this.h = cVar.f3176a;
            this.i = cVar.f3177b;
            this.f3178a = cVar.c;
            this.f3179b = cVar.d;
            this.j = cVar.e;
            this.c = cVar.f;
            this.k = cVar.g;
            this.d = cVar.h;
            this.e = cVar.i;
            this.f = cVar.j;
            this.g = cVar.k;
        }

        public final c a() {
            return new c(this.h, this.i, this.f3178a, this.f3179b, this.j, this.c, this.k, this.d, this.e, this.f, this.g);
        }
    }
}
