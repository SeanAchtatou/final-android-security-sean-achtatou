package com.helpshift.a.b;

/* compiled from: UserSyncStatus */
public enum p {
    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED,
    FAILED
}
