package com.helpshift.a.b;

import com.helpshift.b;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.j.c.a;
import com.helpshift.util.n;

/* compiled from: UserLoginManager */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public b f3180a;

    /* renamed from: b  reason: collision with root package name */
    public j f3181b;
    private ab c;

    public d(b bVar, j jVar, ab abVar) {
        this.f3180a = bVar;
        this.f3181b = jVar;
        this.c = abVar;
    }

    public void a() {
        a a2 = this.f3181b.i.a();
        a2.n();
        a2.l.b();
    }

    public void b() {
        a a2 = this.f3181b.i.a();
        a2.o();
        h k = this.f3180a.o().k();
        if (l.COMPLETED == k.a()) {
            a2.l.a(false);
        } else {
            k.b();
        }
    }

    public boolean a(c cVar) {
        boolean a2 = this.f3180a.o().a(cVar);
        if (a2) {
            this.c.D().b(cVar.f3176a.longValue());
            this.f3181b.i.b(cVar);
        }
        return a2;
    }

    public final boolean c() {
        if (this.f3180a.d()) {
            n.a("Helpshift_ULoginM", "Logout should be called before starting a Helpshift session", (Throwable) null, (com.helpshift.p.b.a[]) null);
            return false;
        }
        e o = this.f3180a.o();
        c a2 = o.a();
        if (a2 != null && a2.g) {
            return true;
        }
        a();
        o.b();
        b();
        d();
        this.f3181b.g.b();
        return true;
    }

    public void d() {
        this.c.t().a(com.helpshift.common.c.b.n.d);
    }
}
