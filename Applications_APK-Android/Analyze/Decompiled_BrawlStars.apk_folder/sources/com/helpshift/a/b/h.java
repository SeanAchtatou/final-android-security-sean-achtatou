package com.helpshift.a.b;

import com.helpshift.a.b.m;
import com.helpshift.common.b;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.r.c;
import com.helpshift.r.d;
import com.helpshift.w.d;
import com.helpshift.w.g;
import java.lang.ref.WeakReference;

/* compiled from: UserSetupDM */
public class h implements m.a, com.helpshift.common.a, d.a, d.a {

    /* renamed from: a  reason: collision with root package name */
    j f3186a;

    /* renamed from: b  reason: collision with root package name */
    c f3187b;
    public WeakReference<a> c;
    m d;
    com.helpshift.r.d e;
    com.helpshift.w.d f;

    /* compiled from: UserSetupDM */
    public interface a {
        void a(l lVar);
    }

    public h(ab abVar, j jVar, c cVar, e eVar, b bVar) {
        this.f3186a = jVar;
        this.f3187b = cVar;
        this.d = new m(abVar, jVar, cVar, eVar, bVar, this);
        this.e = new com.helpshift.r.d(abVar, jVar, cVar, this);
        this.f = new com.helpshift.w.d(abVar, jVar, cVar, this);
    }

    public final l a() {
        g b2 = this.f.b();
        if (b2 == g.PENDING) {
            return l.NON_STARTED;
        }
        if (b2 == g.IN_PROGRESS) {
            return l.IN_PROGRESS;
        }
        c a2 = this.e.a();
        if (a2 == c.NOT_STARTED) {
            return l.NON_STARTED;
        }
        if (a2 == c.FAILED) {
            return l.FAILED;
        }
        if (a2 == c.IN_PROGRESS) {
            return l.IN_PROGRESS;
        }
        p pVar = this.d.f3195b.k;
        if (pVar == p.NOT_STARTED) {
            return l.NON_STARTED;
        }
        if (pVar == p.FAILED) {
            return l.FAILED;
        }
        if (pVar == p.IN_PROGRESS) {
            return l.IN_PROGRESS;
        }
        return l.COMPLETED;
    }

    public final void b() {
        l a2 = a();
        if (a2 != l.IN_PROGRESS && a2 != l.COMPLETED) {
            g b2 = this.f.b();
            b(b2);
            if (b2 == g.PENDING) {
                this.f.a();
            }
        }
    }

    public final void a(g gVar) {
        b(gVar);
    }

    public final void a(p pVar) {
        b(pVar);
    }

    public final void a(c cVar) {
        b(cVar);
    }

    private void b(g gVar) {
        if (gVar == g.COMPLETED) {
            c a2 = this.e.a();
            if (a2 == c.COMPLETED || a2 == c.IN_PROGRESS) {
                b(a2);
            } else {
                this.e.b();
            }
        } else if (gVar == g.IN_PROGRESS) {
            a(l.IN_PROGRESS);
        } else if (gVar == g.PENDING) {
            a(l.NON_STARTED);
        }
    }

    private void b(c cVar) {
        if (cVar == c.COMPLETED) {
            p pVar = this.d.f3195b.k;
            if (pVar == p.COMPLETED || pVar == p.IN_PROGRESS) {
                b(pVar);
            } else {
                this.d.b();
            }
        } else if (cVar == c.IN_PROGRESS) {
            a(l.IN_PROGRESS);
        } else if (cVar == c.FAILED) {
            a(l.FAILED);
        } else if (cVar == c.NOT_STARTED) {
            a(l.NON_STARTED);
        }
    }

    private void b(p pVar) {
        if (pVar == p.COMPLETED) {
            a(l.COMPLETED);
        } else if (pVar == p.IN_PROGRESS) {
            a(l.IN_PROGRESS);
        } else if (pVar == p.FAILED) {
            a(l.FAILED);
        } else if (pVar == p.NOT_STARTED) {
            a(l.NON_STARTED);
        }
    }

    private void a(l lVar) {
        WeakReference<a> weakReference = this.c;
        a aVar = weakReference == null ? null : weakReference.get();
        if (aVar != null) {
            this.f3186a.a(new i(this, aVar, lVar));
        }
        if (lVar == l.COMPLETED) {
            this.f3186a.b(new j(this));
        }
    }

    public final void a(b.a aVar) {
        if (this.f.b() == g.COMPLETED) {
            int i = k.f3191a[aVar.ordinal()];
            if (i == 1) {
                this.e.c();
                if (this.e.a() == c.COMPLETED) {
                    this.d.b();
                }
            } else if (i == 2 && this.e.a() == c.COMPLETED) {
                this.d.c();
            }
        }
    }
}
