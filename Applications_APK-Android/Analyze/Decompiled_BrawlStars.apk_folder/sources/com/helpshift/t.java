package com.helpshift;

import android.app.PendingIntent;

/* compiled from: PluginEventBridge */
public final class t {

    /* renamed from: a  reason: collision with root package name */
    public static a f4225a;

    /* compiled from: PluginEventBridge */
    public interface a {
        PendingIntent a();

        boolean b();
    }

    public static boolean a() {
        a aVar = f4225a;
        if (aVar != null) {
            return aVar.b();
        }
        return false;
    }
}
