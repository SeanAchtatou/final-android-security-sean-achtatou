package com.helpshift;

import com.helpshift.common.i;
import com.helpshift.util.n;

/* compiled from: CoreInternal */
final class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f3435a;

    j(k kVar) {
        this.f3435a = kVar;
    }

    public final void run() {
        if (i.a(this.f3435a)) {
            n.a("Helpshift_CoreInternal", "Login state changed : name : " + this.f3435a.a());
            CoreInternal.f3155a.a(this.f3435a);
            return;
        }
        n.a("Helpshift_CoreInternal", "Login called with invalid helpshift user,So calling Logout");
        CoreInternal.f3155a.a();
    }
}
