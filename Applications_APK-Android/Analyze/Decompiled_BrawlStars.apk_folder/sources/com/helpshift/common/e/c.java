package com.helpshift.common.e;

import android.content.Context;
import com.helpshift.common.a.a;
import com.helpshift.common.k;
import com.helpshift.j.b.b;
import com.helpshift.j.d.a.a;
import com.helpshift.j.d.d;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AndroidConversationInboxDAO */
public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private a f3328a;

    /* renamed from: b  reason: collision with root package name */
    private aa f3329b;

    public c(Context context, aa aaVar) {
        this.f3328a = a.a(context);
        this.f3329b = aaVar;
    }

    private synchronized a.C0140a m(long j) {
        a.C0140a aVar;
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        if (c == null) {
            aVar = new a.C0140a(j);
        } else {
            aVar = new a.C0140a(c);
        }
        return aVar;
    }

    public final synchronized void a(long j, com.helpshift.j.d.a aVar) {
        a.C0140a m = m(j);
        m.c(aVar.f3580a);
        m.a(aVar.f3581b);
        m.a(aVar.c);
        this.f3328a.a(m.a());
    }

    public final synchronized com.helpshift.j.d.a a(long j) {
        com.helpshift.j.d.a aVar;
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        aVar = null;
        if (c != null) {
            String str = c.d;
            long j2 = c.e;
            int i = c.g;
            if (!k.a(str)) {
                aVar = new com.helpshift.j.d.a(str, j2, i);
            }
        }
        return aVar;
    }

    public final synchronized void a(long j, String str) {
        a.C0140a m = m(j);
        m.a(str);
        this.f3328a.a(m.a());
    }

    public final synchronized String b(long j) {
        String str;
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        str = null;
        if (c != null) {
            str = c.f3583b;
        }
        return str;
    }

    public final synchronized void b(long j, String str) {
        a.C0140a m = m(j);
        m.b(str);
        this.f3328a.a(m.a());
    }

    public final synchronized String c(long j) {
        String str;
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        str = null;
        if (c != null) {
            str = c.c;
        }
        return str;
    }

    public final synchronized void a(long j, d dVar) {
        a.C0140a m = m(j);
        m.a(dVar);
        this.f3328a.a(m.a());
    }

    public final synchronized d d(long j) {
        d dVar;
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        dVar = null;
        if (c != null) {
            dVar = c.f;
        }
        return dVar;
    }

    public final synchronized void c(long j, String str) {
        a.C0140a m = m(j);
        m.f(str);
        this.f3328a.a(m.a());
    }

    public final synchronized String e(long j) {
        String str;
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        str = null;
        if (c != null) {
            str = c.k;
        }
        return str;
    }

    public final synchronized void d(long j, String str) {
        a.C0140a m = m(j);
        m.d(str);
        this.f3328a.a(m.a());
    }

    public final synchronized String f(long j) {
        String str;
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        str = null;
        if (c != null) {
            str = c.h;
        }
        return str;
    }

    public final synchronized void e(long j, String str) {
        if (str == null) {
            str = "";
        }
        a.C0140a m = m(j);
        m.e(str);
        this.f3328a.a(m.a());
    }

    public final synchronized String g(long j) {
        String str;
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        str = "";
        if (c != null) {
            str = c.i;
        }
        return str;
    }

    public final synchronized boolean h(long j) {
        boolean z;
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        z = false;
        if (c != null) {
            z = c.j;
        }
        return z;
    }

    public final synchronized void a(long j, boolean z) {
        a.C0140a m = m(j);
        m.a(z);
        this.f3328a.a(m.a());
    }

    public final com.helpshift.j.b.d a(String str) {
        String a2 = this.f3329b.a("push_notification_data");
        if (k.a(a2)) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(a2);
            if (!jSONObject.has(str)) {
                return null;
            }
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            return new com.helpshift.j.b.d(jSONObject2.getInt("notification_count"), jSONObject2.getString("notification_title"));
        } catch (JSONException unused) {
            return null;
        }
    }

    public final void a(String str, com.helpshift.j.b.d dVar) {
        String a2 = this.f3329b.a("push_notification_data");
        if (k.a(a2)) {
            a2 = "{}";
        }
        try {
            JSONObject jSONObject = new JSONObject(a2);
            if (dVar == null) {
                jSONObject.remove(str);
            } else {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("notification_count", dVar.f3553a);
                jSONObject2.put("notification_title", dVar.f3554b);
                jSONObject.put(str, jSONObject2);
            }
            this.f3329b.a("push_notification_data", jSONObject.toString());
        } catch (JSONException unused) {
        }
    }

    public final void i(long j) {
        if (j > 0) {
            this.f3328a.f(j);
        }
    }

    public final void b(long j, boolean z) {
        a.C0140a m = m(j);
        m.b(z);
        this.f3328a.a(m.a());
    }

    public final boolean j(long j) {
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        if (c == null || c.l == null) {
            return true;
        }
        return c.l.booleanValue();
    }

    public final void a(long j, long j2) {
        a.C0140a m = m(j);
        m.a(Long.valueOf(j2));
        this.f3328a.a(m.a());
    }

    public final Long k(long j) {
        com.helpshift.j.d.a.a c = this.f3328a.c(j);
        if (c != null) {
            return c.m;
        }
        return null;
    }

    public final void l(long j) {
        a.C0140a m = m(j);
        m.b(true);
        m.f(null);
        this.f3328a.a(m.a());
    }
}
