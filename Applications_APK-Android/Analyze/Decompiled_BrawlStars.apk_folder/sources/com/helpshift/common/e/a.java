package com.helpshift.common.e;

import android.os.Environment;
import android.text.TextUtils;
import com.helpshift.util.q;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;

/* compiled from: AndroidBackupDAO */
public class a implements com.helpshift.common.b.a {

    /* renamed from: a  reason: collision with root package name */
    private final String f3305a = "__hs__backup_dao_storage";

    /* renamed from: b  reason: collision with root package name */
    private String f3306b;

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r3, java.io.Serializable r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = android.text.TextUtils.isEmpty(r3)     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x0029
            if (r4 != 0) goto L_0x000a
            goto L_0x0029
        L_0x000a:
            java.util.HashMap r0 = r2.a()     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x0015
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x002b }
            r0.<init>()     // Catch:{ all -> 0x002b }
        L_0x0015:
            java.lang.Object r1 = r0.get(r3)     // Catch:{ all -> 0x002b }
            boolean r1 = r4.equals(r1)     // Catch:{ all -> 0x002b }
            if (r1 == 0) goto L_0x0021
            monitor-exit(r2)
            return
        L_0x0021:
            r0.put(r3, r4)     // Catch:{ all -> 0x002b }
            r2.a(r0)     // Catch:{ all -> 0x002b }
            monitor-exit(r2)
            return
        L_0x0029:
            monitor-exit(r2)
            return
        L_0x002b:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.e.a.a(java.lang.String, java.io.Serializable):void");
    }

    public final synchronized Serializable a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        HashMap<String, Serializable> a2 = a();
        if (a2 == null) {
            return null;
        }
        return a2.get(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001c, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(java.lang.String r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = android.text.TextUtils.isEmpty(r3)     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r2)
            return
        L_0x0009:
            java.util.HashMap r0 = r2.a()     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001b
            boolean r1 = r0.containsKey(r3)     // Catch:{ all -> 0x001d }
            if (r1 == 0) goto L_0x001b
            r0.remove(r3)     // Catch:{ all -> 0x001d }
            r2.a(r0)     // Catch:{ all -> 0x001d }
        L_0x001b:
            monitor-exit(r2)
            return
        L_0x001d:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.e.a.b(java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003f A[SYNTHETIC, Splitter:B:22:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.util.HashMap<java.lang.String, java.io.Serializable> r5) {
        /*
            r4 = this;
            if (r5 != 0) goto L_0x0003
            return
        L_0x0003:
            r0 = 0
            java.lang.String r1 = r4.c()     // Catch:{ Exception -> 0x0043, all -> 0x003c }
            java.io.File r1 = android.os.Environment.getExternalStoragePublicDirectory(r1)     // Catch:{ Exception -> 0x0043, all -> 0x003c }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x0043, all -> 0x003c }
            if (r2 != 0) goto L_0x0015
            r1.mkdirs()     // Catch:{ Exception -> 0x0043, all -> 0x003c }
        L_0x0015:
            boolean r2 = r1.canWrite()     // Catch:{ Exception -> 0x0043, all -> 0x003c }
            if (r2 == 0) goto L_0x0036
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0043, all -> 0x003c }
            java.lang.String r3 = "__hs__backup_dao_storage"
            r2.<init>(r1, r3)     // Catch:{ Exception -> 0x0043, all -> 0x003c }
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0043, all -> 0x003c }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0043, all -> 0x003c }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0043, all -> 0x003c }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0043, all -> 0x003c }
            r1.writeObject(r5)     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            r0 = r1
            goto L_0x0036
        L_0x0031:
            r5 = move-exception
            r0 = r1
            goto L_0x003d
        L_0x0034:
            r0 = r1
            goto L_0x0043
        L_0x0036:
            if (r0 == 0) goto L_0x0046
        L_0x0038:
            r0.close()     // Catch:{ Exception -> 0x0046 }
            goto L_0x0046
        L_0x003c:
            r5 = move-exception
        L_0x003d:
            if (r0 == 0) goto L_0x0042
            r0.close()     // Catch:{ Exception -> 0x0042 }
        L_0x0042:
            throw r5
        L_0x0043:
            if (r0 == 0) goto L_0x0046
            goto L_0x0038
        L_0x0046:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.e.a.a(java.util.HashMap):void");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:27:? */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:13:0x0035 */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.util.HashMap<java.lang.String, java.io.Serializable>] */
    /* JADX WARN: Type inference failed for: r1v2, types: [java.io.ObjectInputStream] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0035, code lost:
        if (r0 != null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0043, code lost:
        if (r0 != null) goto L_0x0037;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003e A[SYNTHETIC, Splitter:B:18:0x003e] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.HashMap<java.lang.String, java.io.Serializable> a() {
        /*
            r5 = this;
            boolean r0 = r5.b()
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            java.lang.String r0 = r5.c()     // Catch:{ Exception -> 0x0042, all -> 0x003b }
            java.io.File r0 = android.os.Environment.getExternalStoragePublicDirectory(r0)     // Catch:{ Exception -> 0x0042, all -> 0x003b }
            boolean r2 = r0.canRead()     // Catch:{ Exception -> 0x0042, all -> 0x003b }
            if (r2 == 0) goto L_0x0034
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0042, all -> 0x003b }
            java.lang.String r3 = "__hs__backup_dao_storage"
            r2.<init>(r0, r3)     // Catch:{ Exception -> 0x0042, all -> 0x003b }
            java.io.ObjectInputStream r0 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x0042, all -> 0x003b }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0042, all -> 0x003b }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0042, all -> 0x003b }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0042, all -> 0x003b }
            java.lang.Object r2 = r0.readObject()     // Catch:{ Exception -> 0x0043, all -> 0x002f }
            java.util.HashMap r2 = (java.util.HashMap) r2     // Catch:{ Exception -> 0x0043, all -> 0x002f }
            r1 = r2
            goto L_0x0035
        L_0x002f:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x003c
        L_0x0034:
            r0 = r1
        L_0x0035:
            if (r0 == 0) goto L_0x0046
        L_0x0037:
            r0.close()     // Catch:{ IOException -> 0x0046 }
            goto L_0x0046
        L_0x003b:
            r0 = move-exception
        L_0x003c:
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ IOException -> 0x0041 }
        L_0x0041:
            throw r0
        L_0x0042:
            r0 = r1
        L_0x0043:
            if (r0 == 0) goto L_0x0046
            goto L_0x0037
        L_0x0046:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.e.a.a():java.util.HashMap");
    }

    private boolean b() {
        try {
            return new File(Environment.getExternalStoragePublicDirectory(c()), "__hs__backup_dao_storage").exists();
        } catch (Exception unused) {
            return false;
        }
    }

    private String c() {
        if (this.f3306b == null) {
            this.f3306b = ".backups/" + q.a().getPackageName() + "/helpshift/databases/";
        }
        return this.f3306b;
    }
}
