package com.helpshift.common.e;

import com.helpshift.c.a.a.a.c;
import java.io.Serializable;

/* compiled from: SupportDownloaderKVStorage */
public class ac implements c {

    /* renamed from: a  reason: collision with root package name */
    private final aa f3325a;

    public ac(aa aaVar) {
        this.f3325a = aaVar;
    }

    public final boolean a(String str, Serializable serializable) {
        this.f3325a.a(str, serializable);
        return true;
    }

    public final Object a(String str) {
        return this.f3325a.b(str);
    }
}
