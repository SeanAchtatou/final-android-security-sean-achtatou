package com.helpshift.common.e;

import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.helpshift.common.e.a.k;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.c;
import com.helpshift.common.g.b;
import com.helpshift.j.a.a.a;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.aa;
import com.helpshift.j.a.a.ab;
import com.helpshift.j.a.a.aj;
import com.helpshift.j.a.a.ak;
import com.helpshift.j.a.a.al;
import com.helpshift.j.a.a.an;
import com.helpshift.j.a.a.ap;
import com.helpshift.j.a.a.d;
import com.helpshift.j.a.a.e;
import com.helpshift.j.a.a.i;
import com.helpshift.j.a.a.j;
import com.helpshift.j.a.a.m;
import com.helpshift.j.a.a.n;
import com.helpshift.j.a.a.p;
import com.helpshift.j.a.a.q;
import com.helpshift.j.a.a.r;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.w;
import com.helpshift.j.a.a.y;
import com.helpshift.j.a.a.z;
import com.helpshift.j.d.f;
import com.helpshift.j.d.g;
import com.helpshift.j.d.h;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AndroidResponseParser */
class s implements k {
    s() {
    }

    public final al a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("created_at");
            al alVar = new al(jSONObject.getString("body"), string, b.b(string), jSONObject.getJSONObject("author").getString("name"));
            alVar.m = jSONObject.getString("id");
            alVar.u = r(jSONObject.optString("md_state", ""));
            alVar.y = jSONObject.optBoolean("redacted", false);
            a(alVar, jSONObject);
            return alVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading user text message");
        }
    }

    public final ap b(String str) {
        boolean z;
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("type");
            char c = 65535;
            int i = 3;
            switch (string.hashCode()) {
                case -831290677:
                    if (string.equals("rsp_txt_msg_with_email_input")) {
                        c = 2;
                        break;
                    }
                    break;
                case -94670724:
                    if (string.equals("rsp_txt_msg_with_numeric_input")) {
                        c = 3;
                        break;
                    }
                    break;
                case 493654943:
                    if (string.equals("rsp_txt_msg_with_txt_input")) {
                        c = 1;
                        break;
                    }
                    break;
                case 919037346:
                    if (string.equals("rsp_empty_msg_with_txt_input")) {
                        c = 0;
                        break;
                    }
                    break;
                case 2071762039:
                    if (string.equals("rsp_txt_msg_with_dt_input")) {
                        c = 4;
                        break;
                    }
                    break;
            }
            if (c != 0) {
                if (c == 1) {
                    i = 1;
                } else if (c == 2) {
                    i = 2;
                } else if (c != 3) {
                    if (c != 4) {
                        return null;
                    }
                    i = 4;
                }
                z = false;
            } else {
                i = 1;
                z = true;
            }
            boolean z2 = !z && jSONObject.getBoolean("skipped");
            JSONObject jSONObject2 = jSONObject.getJSONObject("meta");
            String string2 = jSONObject.getString("created_at");
            ap apVar = new ap(jSONObject.getString("body"), string2, b.b(string2), jSONObject.getJSONObject("author").getString("name"), i, jSONObject.getJSONObject("chatbot_info").toString(), z2, jSONObject2.getString("refers"), z);
            if (i == 4 && !z2) {
                apVar.f = jSONObject2.getLong("dt");
                apVar.g = jSONObject2.optString("timezone");
            }
            apVar.m = jSONObject.getString("id");
            apVar.y = jSONObject.optBoolean("redacted", false);
            a(apVar, jSONObject);
            return apVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading user response for text input");
        }
    }

    public final ab c(String str) {
        try {
            return l(new JSONObject(str));
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading reopen message");
        }
    }

    public final a d(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("created_at");
            a aVar = new a(jSONObject.getString("body"), string, b.b(string), jSONObject.getJSONObject("author").getString("name"), jSONObject.getJSONObject("meta").getString("refers"), 2);
            aVar.m = jSONObject.getString("id");
            aVar.u = r(jSONObject.optString("md_state", ""));
            aVar.y = jSONObject.optBoolean("redacted", false);
            a(aVar, jSONObject);
            return aVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading accepted review message");
        }
    }

    public final m e(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("created_at");
            m mVar = new m(jSONObject.getString("body"), string, b.b(string), jSONObject.getJSONObject("author").getString("name"), 2);
            mVar.m = jSONObject.getString("id");
            mVar.u = r(jSONObject.optString("md_state", ""));
            mVar.y = jSONObject.optBoolean("redacted", false);
            a(mVar, jSONObject);
            return mVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading confirmation accepted message");
        }
    }

    public final n f(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("created_at");
            n nVar = new n(jSONObject.getString("body"), string, b.b(string), jSONObject.getJSONObject("author").getString("name"), 2);
            nVar.m = jSONObject.getString("id");
            nVar.u = r(jSONObject.optString("md_state", ""));
            nVar.y = jSONObject.optBoolean("redacted", false);
            a(nVar, jSONObject);
            return nVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading confirmation rejected message");
        }
    }

    public final com.helpshift.j.d.c g(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            ArrayList arrayList = new ArrayList();
            JSONArray jSONArray = jSONObject.getJSONArray("issues");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(o(jSONArray.getJSONObject(i).toString()));
            }
            Boolean bool = null;
            if (jSONObject.has("has_older_messages")) {
                bool = Boolean.valueOf(jSONObject.getBoolean("has_older_messages"));
            }
            return new com.helpshift.j.d.c(jSONObject.getString("cursor"), arrayList, jSONObject.getBoolean("issue_exists"), bool);
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading conversation inbox");
        }
    }

    public final com.helpshift.j.d.b h(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            ArrayList arrayList = new ArrayList();
            JSONArray jSONArray = jSONObject.getJSONArray("issues");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(o(jSONArray.getJSONObject(i).toString()));
            }
            return new com.helpshift.j.d.b(arrayList, jSONObject.getBoolean("has_older_messages"));
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading conversation history");
        }
    }

    public final com.helpshift.j.a.a.s i(String str) {
        try {
            return c(new JSONObject(str));
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading follow-up rejected message");
        }
    }

    public final r j(String str) {
        try {
            return d(new JSONObject(str));
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading follow-up accepted message");
        }
    }

    public final com.helpshift.g.b.a k(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            return new com.helpshift.g.b.a(jSONObject.getString("token"), jSONObject.getString("endpoint"));
        } catch (JSONException e) {
            com.helpshift.util.n.c("Helpshift_AResponseParser", "Exception in parsing auth token", e);
            return null;
        }
    }

    public final h l(String str) {
        g gVar;
        g gVar2;
        try {
            JSONArray jSONArray = new JSONArray(str);
            int i = jSONArray.getInt(0);
            if (i == 100) {
                JSONArray jSONArray2 = jSONArray.getJSONArray(2);
                gVar = null;
                int i2 = 0;
                while (i2 < jSONArray2.length()) {
                    try {
                        JSONObject jSONObject = new JSONObject(jSONArray2.getJSONObject(i2).getString("m"));
                        if ("agent_type_activity".equals(jSONObject.getString("stream"))) {
                            String string = jSONObject.getString(NativeProtocol.WEB_DIALOG_ACTION);
                            if ("start".equals(string)) {
                                gVar2 = new g(true, TimeUnit.SECONDS.toMillis(jSONObject.getLong("ttl")));
                            } else if ("stop".equals(string)) {
                                gVar2 = new g(false, 0);
                            }
                            gVar = gVar2;
                        }
                        i2++;
                    } catch (JSONException e) {
                        e = e;
                        com.helpshift.util.n.c("Helpshift_AResponseParser", "Exception in parsing web-socket message", e);
                        return gVar;
                    }
                }
                return gVar;
            } else if (i != 107) {
                return null;
            } else {
                return new f(TimeUnit.SECONDS.toMillis(jSONArray.getLong(1)));
            }
        } catch (JSONException e2) {
            e = e2;
            gVar = null;
            com.helpshift.util.n.c("Helpshift_AResponseParser", "Exception in parsing web-socket message", e);
            return gVar;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.e.s.a(java.lang.String, boolean):com.helpshift.j.a.a.v
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.common.e.s.a(org.json.JSONObject, int):com.helpshift.j.a.a.j
      com.helpshift.common.e.s.a(org.json.JSONObject, org.json.JSONArray):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.common.e.s.a(com.helpshift.j.a.a.v, org.json.JSONObject):void
      com.helpshift.common.e.s.a(java.lang.String, boolean):com.helpshift.j.a.a.v */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x02b2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x02b4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0169, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        com.helpshift.util.n.c("Helpshift_AResponseParser", "Exception while parsing messages: ", r0);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x02b2 A[ExcHandler: JSONException (e org.json.JSONException), Splitter:B:3:0x0014] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.helpshift.j.a.a.v> a(org.json.JSONArray r20) {
        /*
            r19 = this;
            r1 = r19
            java.lang.String r2 = "Exception while parsing messages: "
            java.lang.String r3 = "Helpshift_AResponseParser"
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            int r5 = r20.length()
            r7 = 0
        L_0x0010:
            if (r7 >= r5) goto L_0x02bd
            r8 = r20
            org.json.JSONObject r0 = r8.getJSONObject(r7)     // Catch:{ RootAPIException -> 0x02b4, JSONException -> 0x02b2 }
            java.lang.String r9 = "type"
            java.lang.String r9 = r0.getString(r9)     // Catch:{ RootAPIException -> 0x02b4, JSONException -> 0x02b2 }
            java.lang.String r10 = "origin"
            java.lang.String r10 = r0.getString(r10)     // Catch:{ RootAPIException -> 0x02b4, JSONException -> 0x02b2 }
            java.lang.String r11 = "admin"
            boolean r11 = r11.equals(r10)     // Catch:{ RootAPIException -> 0x02b4, JSONException -> 0x02b2 }
            r16 = 7
            r17 = 11
            java.lang.String r12 = "txt"
            r18 = -1
            r13 = 4
            r14 = 3
            r15 = 2
            r6 = 1
            if (r11 == 0) goto L_0x016f
            int r10 = r9.hashCode()     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            switch(r10) {
                case -1052767485: goto L_0x00ca;
                case -185416997: goto L_0x00c0;
                case 112675: goto L_0x00b6;
                case 112830: goto L_0x00ab;
                case 113218: goto L_0x00a0;
                case 115312: goto L_0x0098;
                case 133418599: goto L_0x008e;
                case 373335180: goto L_0x0084;
                case 534550447: goto L_0x007a;
                case 892689447: goto L_0x006f;
                case 903982601: goto L_0x0063;
                case 1564911026: goto L_0x0058;
                case 1829173826: goto L_0x004c;
                case 2114645132: goto L_0x0041;
                default: goto L_0x003f;
            }     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
        L_0x003f:
            goto L_0x00d5
        L_0x0041:
            java.lang.String r10 = "txt_msg_with_numeric_input"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 3
            goto L_0x00d6
        L_0x004c:
            java.lang.String r10 = "bot_ended"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 13
            goto L_0x00d6
        L_0x0058:
            java.lang.String r10 = "empty_msg_with_txt_input"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 5
            goto L_0x00d6
        L_0x0063:
            java.lang.String r10 = "bot_started"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 12
            goto L_0x00d6
        L_0x006f:
            java.lang.String r10 = "faq_list"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 10
            goto L_0x00d6
        L_0x007a:
            java.lang.String r10 = "txt_msg_with_txt_input"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 1
            goto L_0x00d6
        L_0x0084:
            java.lang.String r10 = "txt_msg_with_option_input"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 6
            goto L_0x00d6
        L_0x008e:
            java.lang.String r10 = "txt_msg_with_dt_input"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 4
            goto L_0x00d6
        L_0x0098:
            boolean r9 = r9.equals(r12)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 0
            goto L_0x00d6
        L_0x00a0:
            java.lang.String r10 = "rsc"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 8
            goto L_0x00d6
        L_0x00ab:
            java.lang.String r10 = "rfr"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 9
            goto L_0x00d6
        L_0x00b6:
            java.lang.String r10 = "rar"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 7
            goto L_0x00d6
        L_0x00c0:
            java.lang.String r10 = "txt_msg_with_email_input"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 2
            goto L_0x00d6
        L_0x00ca:
            java.lang.String r10 = "faq_list_msg_with_option_input"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x00d5
            r9 = 11
            goto L_0x00d6
        L_0x00d5:
            r9 = -1
        L_0x00d6:
            switch(r9) {
                case 0: goto L_0x014d;
                case 1: goto L_0x0144;
                case 2: goto L_0x013b;
                case 3: goto L_0x0132;
                case 4: goto L_0x0129;
                case 5: goto L_0x0120;
                case 6: goto L_0x0117;
                case 7: goto L_0x010e;
                case 8: goto L_0x0105;
                case 9: goto L_0x00fc;
                case 10: goto L_0x00f3;
                case 11: goto L_0x00ea;
                case 12: goto L_0x00dd;
                case 13: goto L_0x00dd;
                default: goto L_0x00d9;
            }     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
        L_0x00d9:
            java.lang.String r6 = "input"
            goto L_0x0156
        L_0x00dd:
            java.lang.String r0 = r0.toString()     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            com.helpshift.j.a.a.v r0 = r1.a(r0, r6)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x00ea:
            com.helpshift.j.a.a.v r0 = r1.a(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x00f3:
            com.helpshift.j.a.a.p r0 = r1.b(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x00fc:
            com.helpshift.j.a.a.z r0 = r1.e(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x0105:
            java.util.List r0 = r1.k(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.addAll(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x010e:
            com.helpshift.j.a.a.y r0 = r1.j(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x0117:
            com.helpshift.j.a.a.v r0 = r1.g(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x0120:
            com.helpshift.j.a.a.j r0 = r1.h(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x0129:
            com.helpshift.j.a.a.j r0 = r1.a(r0, r13)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x0132:
            com.helpshift.j.a.a.j r0 = r1.a(r0, r14)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x013b:
            com.helpshift.j.a.a.j r0 = r1.a(r0, r15)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x0144:
            com.helpshift.j.a.a.j r0 = r1.a(r0, r6)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x014d:
            java.util.List r0 = r1.i(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.addAll(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x0156:
            boolean r6 = r0.has(r6)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x0223
            java.lang.String r0 = r0.toString()     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            com.helpshift.j.a.a.aj r0 = q(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x0169, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x0169:
            r0 = move-exception
            com.helpshift.util.n.c(r3, r2, r0)     // Catch:{ RootAPIException -> 0x02b4, JSONException -> 0x02b2 }
            goto L_0x0223
        L_0x016f:
            java.lang.String r11 = "mobile"
            boolean r10 = r11.equals(r10)     // Catch:{ RootAPIException -> 0x02b4, JSONException -> 0x02b2 }
            if (r10 == 0) goto L_0x02a7
            int r10 = r9.hashCode()     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            switch(r10) {
                case -831290677: goto L_0x0214;
                case -657647885: goto L_0x0209;
                case -545696551: goto L_0x01fe;
                case -94670724: goto L_0x01f3;
                case 3121: goto L_0x01ea;
                case 3166: goto L_0x01e0;
                case 3631: goto L_0x01d6;
                case 3640: goto L_0x01cc;
                case 3664: goto L_0x01c2;
                case 108893: goto L_0x01b8;
                case 115312: goto L_0x01af;
                case 493654943: goto L_0x01a3;
                case 919037346: goto L_0x0198;
                case 1826087580: goto L_0x018c;
                case 2071762039: goto L_0x0180;
                default: goto L_0x017e;
            }     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
        L_0x017e:
            goto L_0x021f
        L_0x0180:
            java.lang.String r6 = "rsp_txt_msg_with_dt_input"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 11
            goto L_0x0220
        L_0x018c:
            java.lang.String r6 = "rsp_txt_msg_with_option_input"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 12
            goto L_0x0220
        L_0x0198:
            java.lang.String r6 = "rsp_empty_msg_with_txt_input"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 7
            goto L_0x0220
        L_0x01a3:
            java.lang.String r6 = "rsp_txt_msg_with_txt_input"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 8
            goto L_0x0220
        L_0x01af:
            boolean r6 = r9.equals(r12)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 0
            goto L_0x0220
        L_0x01b8:
            java.lang.String r6 = "ncr"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 2
            goto L_0x0220
        L_0x01c2:
            java.lang.String r6 = "sc"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 4
            goto L_0x0220
        L_0x01cc:
            java.lang.String r6 = "rj"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 6
            goto L_0x0220
        L_0x01d6:
            java.lang.String r6 = "ra"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 5
            goto L_0x0220
        L_0x01e0:
            java.lang.String r6 = "ca"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 3
            goto L_0x0220
        L_0x01ea:
            java.lang.String r10 = "ar"
            boolean r9 = r9.equals(r10)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r9 == 0) goto L_0x021f
            goto L_0x0220
        L_0x01f3:
            java.lang.String r6 = "rsp_txt_msg_with_numeric_input"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 10
            goto L_0x0220
        L_0x01fe:
            java.lang.String r6 = "bot_cancelled"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 14
            goto L_0x0220
        L_0x0209:
            java.lang.String r6 = "rsp_faq_list_msg_with_option_input"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 13
            goto L_0x0220
        L_0x0214:
            java.lang.String r6 = "rsp_txt_msg_with_email_input"
            boolean r6 = r9.equals(r6)     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            if (r6 == 0) goto L_0x021f
            r6 = 9
            goto L_0x0220
        L_0x021f:
            r6 = -1
        L_0x0220:
            switch(r6) {
                case 0: goto L_0x0292;
                case 1: goto L_0x0285;
                case 2: goto L_0x0278;
                case 3: goto L_0x026b;
                case 4: goto L_0x0262;
                case 5: goto L_0x0259;
                case 6: goto L_0x0250;
                case 7: goto L_0x0242;
                case 8: goto L_0x0242;
                case 9: goto L_0x0242;
                case 10: goto L_0x0242;
                case 11: goto L_0x0242;
                case 12: goto L_0x0234;
                case 13: goto L_0x0234;
                case 14: goto L_0x0226;
                default: goto L_0x0223;
            }     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
        L_0x0223:
            r6 = 0
            goto L_0x02b9
        L_0x0226:
            java.lang.String r0 = r0.toString()     // Catch:{ RootAPIException -> 0x02a1, JSONException -> 0x02b2 }
            r6 = 0
            com.helpshift.j.a.a.v r0 = r1.a(r0, r6)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x0234:
            r6 = 0
            java.lang.String r0 = r0.toString()     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            com.helpshift.j.a.a.an r0 = r1.m(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x0242:
            r6 = 0
            java.lang.String r0 = r0.toString()     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            com.helpshift.j.a.a.ap r0 = r1.b(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x0250:
            r6 = 0
            com.helpshift.j.a.a.s r0 = r1.c(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x0259:
            r6 = 0
            com.helpshift.j.a.a.r r0 = r1.d(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x0262:
            r6 = 0
            com.helpshift.j.a.a.ab r0 = r1.l(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x026b:
            r6 = 0
            java.lang.String r0 = r0.toString()     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            com.helpshift.j.a.a.m r0 = r1.e(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x0278:
            r6 = 0
            java.lang.String r0 = r0.toString()     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            com.helpshift.j.a.a.n r0 = r1.f(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x0285:
            r6 = 0
            java.lang.String r0 = r0.toString()     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            com.helpshift.j.a.a.a r0 = r1.d(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x0292:
            r6 = 0
            java.lang.String r0 = r0.toString()     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            com.helpshift.j.a.a.al r0 = r1.a(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            r4.add(r0)     // Catch:{ RootAPIException -> 0x029f, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x029f:
            r0 = move-exception
            goto L_0x02a3
        L_0x02a1:
            r0 = move-exception
            r6 = 0
        L_0x02a3:
            com.helpshift.util.n.c(r3, r2, r0)     // Catch:{ RootAPIException -> 0x02b0, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x02a7:
            r6 = 0
            java.lang.String r0 = "Unknown message type received."
            com.helpshift.util.n.b(r3, r0)     // Catch:{ RootAPIException -> 0x02b0, JSONException -> 0x02ae }
            goto L_0x02b9
        L_0x02ae:
            r0 = move-exception
            goto L_0x02b6
        L_0x02b0:
            r0 = move-exception
            goto L_0x02b6
        L_0x02b2:
            r0 = move-exception
            goto L_0x02b5
        L_0x02b4:
            r0 = move-exception
        L_0x02b5:
            r6 = 0
        L_0x02b6:
            com.helpshift.util.n.c(r3, r2, r0)
        L_0x02b9:
            int r7 = r7 + 1
            goto L_0x0010
        L_0x02bd:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.e.s.a(org.json.JSONArray):java.util.List");
    }

    public final v a(String str, boolean z) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("type");
            String string2 = jSONObject.getString("id");
            String jSONObject2 = jSONObject.getJSONObject("chatbot_info").toString();
            boolean optBoolean = jSONObject.optBoolean("redacted", false);
            String string3 = jSONObject.getString("created_at");
            long b2 = b.b(string3);
            if (z) {
                d dVar = new d(string2, jSONObject.getString("body"), string3, b2, jSONObject.getJSONObject("author").getString("name"), string, jSONObject2);
                dVar.c = jSONObject.optBoolean("has_next_bot", false);
                dVar.y = optBoolean;
                return dVar;
            }
            JSONObject jSONObject3 = jSONObject.getJSONObject("meta");
            String string4 = jSONObject3.getString("chatbot_cancelled_reason");
            String string5 = jSONObject3.getString("refers");
            ak akVar = new ak(jSONObject.getString("body"), string3, b2, jSONObject.getJSONObject("author").getString("name"), string, string4, jSONObject2, string5, 2);
            akVar.m = string2;
            akVar.y = optBoolean;
            a(akVar, jSONObject);
            return akVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading bot control messages.");
        }
    }

    private static List<p.a> b(JSONArray jSONArray) throws JSONException {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            JSONObject jSONObject2 = jSONObject.getJSONObject(ShareConstants.WEB_DIALOG_PARAM_DATA);
            arrayList.add(new p.a(jSONObject.getString("title"), jSONObject2.getString("publish_id"), jSONObject2.getString("language")));
        }
        return arrayList;
    }

    private v a(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        try {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("input");
            String string = jSONObject2.getString("created_at");
            q qVar = new q(jSONObject2.getString("id"), jSONObject2.getString("body"), string, b.b(string), jSONObject2.getJSONObject("author").getString("name"), b(jSONObject2.getJSONArray("faqs")), jSONObject2.getJSONObject("chatbot_info").toString(), jSONObject3.getBoolean("required"), jSONObject3.getString("label"), jSONObject3.optString("skip_label"), f(jSONObject3));
            qVar.u = r(jSONObject2.optString("md_state", ""));
            qVar.y = jSONObject2.optBoolean("redacted", false);
            return qVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading list message with option input");
        }
    }

    private p b(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("created_at");
            p pVar = new p(jSONObject.getString("id"), jSONObject.getString("body"), string, b.b(string), jSONObject.getJSONObject("author").getString("name"), b(jSONObject.getJSONArray("faqs")));
            pVar.u = r(jSONObject.optString("md_state", ""));
            pVar.y = jSONObject.optBoolean("redacted", false);
            return pVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading faq list message");
        }
    }

    private com.helpshift.j.a.a.s c(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("created_at");
            com.helpshift.j.a.a.s sVar = new com.helpshift.j.a.a.s(jSONObject.getString("body"), string, b.b(string), jSONObject.getJSONObject("author").getString("name"), jSONObject.getJSONObject("meta").getString("refers"), 2);
            sVar.m = jSONObject.getString("id");
            sVar.u = r(jSONObject.optString("md_state", ""));
            sVar.y = jSONObject.optBoolean("redacted", false);
            a(sVar, jSONObject);
            return sVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading follow-up rejected message");
        }
    }

    private r d(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("created_at");
            r rVar = new r(jSONObject.getString("body"), string, b.b(string), jSONObject.getJSONObject("author").getString("name"), jSONObject.getJSONObject("meta").getString("refers"), 2);
            rVar.m = jSONObject.getString("id");
            rVar.u = r(jSONObject.optString("md_state", ""));
            rVar.y = jSONObject.optBoolean("redacted", false);
            a(rVar, jSONObject);
            return rVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading follow-up accepted message");
        }
    }

    private z e(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("created_at");
            z zVar = new z(jSONObject.getString("id"), jSONObject.getString("body"), string, b.b(string), jSONObject.getJSONObject("author").getString("name"));
            zVar.u = r(jSONObject.optString("md_state", ""));
            zVar.y = jSONObject.optBoolean("redacted", false);
            return zVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading reopen message");
        }
    }

    private static List<b.a> f(JSONObject jSONObject) throws JSONException {
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("options");
        int min = Math.min(jSONArray.length(), 500);
        for (int i = 0; i < min; i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            arrayList.add(new b.a(jSONObject2.getString("title"), jSONObject2.getJSONObject(ShareConstants.WEB_DIALOG_PARAM_DATA).toString()));
        }
        return arrayList;
    }

    private v g(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        try {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("input");
            String string = jSONObject2.getString("created_at");
            i iVar = new i(jSONObject2.getString("id"), jSONObject2.getString("body"), string, com.helpshift.common.g.b.b(string), jSONObject2.getJSONObject("author").getString("name"), jSONObject2.getJSONObject("chatbot_info").toString(), jSONObject3.getBoolean("required"), jSONObject3.getString("label"), jSONObject3.optString("skip_label"), f(jSONObject3), b.C0138b.a(jSONObject3.optString("type"), jSONObject3.getJSONArray("options").length()));
            iVar.u = r(jSONObject2.optString("md_state", ""));
            iVar.y = jSONObject2.optBoolean("redacted", false);
            return iVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading admin text message with option input");
        }
    }

    public final an m(String str) {
        w wVar;
        String jSONObject;
        try {
            JSONObject jSONObject2 = new JSONObject(str);
            String string = jSONObject2.getString("type");
            char c = 65535;
            int hashCode = string.hashCode();
            if (hashCode != -657647885) {
                if (hashCode == 1826087580) {
                    if (string.equals("rsp_txt_msg_with_option_input")) {
                        c = 0;
                    }
                }
            } else if (string.equals("rsp_faq_list_msg_with_option_input")) {
                c = 1;
            }
            if (c == 0) {
                wVar = w.ADMIN_TEXT_WITH_OPTION_INPUT;
            } else if (c != 1) {
                return null;
            } else {
                wVar = w.FAQ_LIST_WITH_OPTION_INPUT;
            }
            w wVar2 = wVar;
            boolean z = jSONObject2.getBoolean("skipped");
            if (z) {
                jSONObject = "{}";
            } else {
                jSONObject = jSONObject2.getJSONObject("option_data").toString();
            }
            String str2 = jSONObject;
            String string2 = jSONObject2.getString("created_at");
            an anVar = new an(jSONObject2.getString("body"), string2, com.helpshift.common.g.b.b(string2), jSONObject2.getJSONObject("author").getString("name"), jSONObject2.getJSONObject("chatbot_info").toString(), z, str2, jSONObject2.getJSONObject("meta").getString("refers"), wVar2);
            anVar.m = jSONObject2.getString("id");
            anVar.y = jSONObject2.optBoolean("redacted", false);
            a(anVar, jSONObject2);
            return anVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading user response for option input");
        }
    }

    private j h(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        try {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("input");
            String string = jSONObject2.getString("created_at");
            j jVar = new j(jSONObject2.getString("id"), "", string, com.helpshift.common.g.b.b(string), jSONObject2.getJSONObject("author").getString("name"), jSONObject2.getJSONObject("chatbot_info").toString(), jSONObject3.getString("placeholder"), jSONObject3.getBoolean("required"), jSONObject3.getString("label"), jSONObject3.optString("skip_label"), 1, true);
            jVar.u = r(jSONObject2.optString("md_state", ""));
            jVar.y = jSONObject2.optBoolean("redacted", false);
            return jVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading admin empty message with text input");
        }
    }

    private j a(JSONObject jSONObject, int i) {
        JSONObject jSONObject2 = jSONObject;
        try {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("input");
            String string = jSONObject2.getString("created_at");
            j jVar = new j(jSONObject2.getString("id"), jSONObject2.getString("body"), string, com.helpshift.common.g.b.b(string), jSONObject2.getJSONObject("author").getString("name"), jSONObject2.getJSONObject("chatbot_info").toString(), jSONObject3.getString("placeholder"), jSONObject3.getBoolean("required"), jSONObject3.getString("label"), jSONObject3.optString("skip_label"), i, false);
            jVar.u = r(jSONObject2.optString("md_state", ""));
            jVar.y = jSONObject2.optBoolean("redacted", false);
            return jVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading admin message with text input");
        }
    }

    private List<v> i(JSONObject jSONObject) {
        try {
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray = jSONObject.getJSONObject("meta").optJSONArray("attachments");
            String string = jSONObject.getString("created_at");
            com.helpshift.j.a.a.h hVar = new com.helpshift.j.a.a.h(jSONObject.getString("id"), jSONObject.getString("body"), string, com.helpshift.common.g.b.b(string), jSONObject.getJSONObject("author").getString("name"));
            hVar.u = r(jSONObject.optString("md_state", ""));
            hVar.y = jSONObject.optBoolean("redacted", false);
            arrayList.add(hVar);
            if (optJSONArray != null) {
                arrayList.addAll(a(jSONObject, optJSONArray));
            }
            return arrayList;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading admin text message");
        }
    }

    private static aj q(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("created_at");
            return new aj(jSONObject.getString("id"), jSONObject.getString("body"), string, com.helpshift.common.g.b.b(string), jSONObject.getJSONObject("author").getString("name"), jSONObject.getString("type"), jSONObject.getJSONObject("chatbot_info").toString(), jSONObject.getJSONObject("input").toString());
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading unsupported admin message with input");
        }
    }

    public final com.helpshift.i.c.b n(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Long l = null;
            Long valueOf = jSONObject.has("last_redaction_at") ? Long.valueOf(jSONObject.getLong("last_redaction_at")) : null;
            if (jSONObject.has("profile_created_at")) {
                l = Long.valueOf(jSONObject.getLong("profile_created_at"));
            }
            Long l2 = l;
            long optLong = jSONObject.optLong("pfi", 0) / 1000;
            long optLong2 = jSONObject.optLong("pri", 0) / 1000;
            boolean optBoolean = jSONObject.optBoolean("afp", false);
            boolean optBoolean2 = jSONObject.optBoolean("rne", false);
            boolean optBoolean3 = jSONObject.optBoolean("pfe", true);
            boolean optBoolean4 = jSONObject.optBoolean("san", true);
            boolean optBoolean5 = jSONObject.optBoolean("csat", false);
            boolean optBoolean6 = jSONObject.optBoolean("dia", false);
            JSONObject optJSONObject = jSONObject.optJSONObject("t");
            boolean z = optJSONObject != null ? !optJSONObject.optString("hl", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE).equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE) : false;
            boolean optBoolean7 = jSONObject.optBoolean("issue_exists", true);
            int optInt = jSONObject.optInt("dbgl", 100);
            int optInt2 = jSONObject.optInt("bcl", 100);
            String optString = jSONObject.optString("rurl", "");
            JSONObject jSONObject2 = jSONObject.getJSONObject("pr");
            return new com.helpshift.i.c.b(optBoolean2, optBoolean3, optBoolean4, optBoolean5, optBoolean6, z, optBoolean7, optInt, optInt2, optString, new com.helpshift.i.c.a(jSONObject2.optBoolean("s"), jSONObject2.optInt("i"), jSONObject2.optString("t", "")), jSONObject.optBoolean("ic", false), jSONObject.optString("gm", ""), jSONObject.optBoolean("tyi", true), jSONObject.optBoolean("rq", true), jSONObject.optBoolean("conversation_history_enabled", false), valueOf, l2, jSONObject.optBoolean("allow_user_attachments", true), optLong, optLong2, optBoolean);
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while fetching config");
        }
    }

    public final com.helpshift.j.a.b.a o(String str) {
        String str2;
        try {
            JSONObject jSONObject = new JSONObject(str);
            try {
                List<v> a2 = a(jSONObject.getJSONArray("messages"));
                int size = a2.size() - 1;
                while (true) {
                    if (size < 0) {
                        str2 = null;
                        break;
                    }
                    v vVar = a2.get(size);
                    if (!(vVar instanceof com.helpshift.j.a.a.b) && !(vVar instanceof e)) {
                        str2 = vVar.m();
                        break;
                    }
                    size--;
                }
                com.helpshift.j.d.e a3 = com.helpshift.j.d.e.a(jSONObject.getInt(ServerProtocol.DIALOG_PARAM_STATE));
                String string = jSONObject.getString("created_at");
                long b2 = com.helpshift.common.g.b.b(string);
                String string2 = jSONObject.getString("type");
                com.helpshift.j.a.b.a aVar = r8;
                com.helpshift.j.a.b.a aVar2 = new com.helpshift.j.a.b.a(jSONObject.optString("title", ""), a3, string, b2, jSONObject.getString("updated_at"), jSONObject.getString("publish_id"), str2, jSONObject.optBoolean("show-agent-name", true), string2);
                aVar.x = jSONObject.optBoolean("redacted", false);
                aVar.c = jSONObject.isNull("issue_id") ? null : jSONObject.getString("issue_id");
                aVar.d = jSONObject.isNull("preissue_id") ? null : jSONObject.getString("preissue_id");
                aVar.h = string2;
                aVar.v = jSONObject.optString("request_id");
                if ("issue".equals(string2)) {
                    aVar.p = jSONObject.optBoolean("csat_received") ? com.helpshift.j.f.a.SUBMITTED_SYNCED : com.helpshift.j.f.a.NONE;
                }
                aVar.a(a2);
                return aVar;
            } catch (JSONException e) {
                e = e;
                throw RootAPIException.a(e, c.GENERIC, "Parsing exception in reading conversation");
            }
        } catch (JSONException e2) {
            e = e2;
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception in reading conversation");
        }
    }

    public final com.helpshift.n.a p(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            return new com.helpshift.n.a(jSONObject.getString("id"), jSONObject.getString("publish_id"), jSONObject.getString("language"), jSONObject.getString("section_id"), jSONObject.getString("title"), jSONObject.getString("body"), 0, Boolean.valueOf(jSONObject.getString("is_rtl").equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)), jSONObject.has("stags") ? com.helpshift.util.j.a(jSONObject.getString("stags")) : new ArrayList<>(), jSONObject.has("issue_tags") ? com.helpshift.util.j.a(jSONObject.getString("issue_tags")) : new ArrayList<>());
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading single faq");
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: com.helpshift.j.a.a.b} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v4, resolved type: com.helpshift.j.a.a.b} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v5, resolved type: com.helpshift.j.a.a.e} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: com.helpshift.j.a.a.b} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a5 A[Catch:{ JSONException -> 0x00d8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00be A[Catch:{ JSONException -> 0x00d8 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.helpshift.j.a.a.v> a(org.json.JSONObject r25, org.json.JSONArray r26) {
        /*
            r24 = this;
            r0 = r25
            java.lang.String r1 = "redacted"
            java.lang.String r2 = "body"
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ JSONException -> 0x00d8 }
            r3.<init>()     // Catch:{ JSONException -> 0x00d8 }
            r4 = 0
            r5 = 0
        L_0x000d:
            int r6 = r26.length()     // Catch:{ JSONException -> 0x00d8 }
            if (r5 >= r6) goto L_0x00d7
            r6 = r26
            org.json.JSONObject r7 = r6.getJSONObject(r5)     // Catch:{ JSONException -> 0x00d8 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00d8 }
            r8.<init>()     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r9 = "id"
            java.lang.String r9 = r0.getString(r9)     // Catch:{ JSONException -> 0x00d8 }
            r8.append(r9)     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r9 = "_"
            r8.append(r9)     // Catch:{ JSONException -> 0x00d8 }
            r8.append(r5)     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r11 = r8.toString()     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r8 = "created_at"
            java.lang.String r8 = r0.getString(r8)     // Catch:{ JSONException -> 0x00d8 }
            com.helpshift.common.g.e r9 = com.helpshift.common.g.b.f3381a     // Catch:{ JSONException -> 0x00d8 }
            int r5 = r5 + 1
            java.lang.String r13 = com.helpshift.common.g.b.a(r9, r8, r5)     // Catch:{ JSONException -> 0x00d8 }
            long r14 = com.helpshift.common.g.b.b(r13)     // Catch:{ JSONException -> 0x00d8 }
            boolean r8 = r7.has(r2)     // Catch:{ JSONException -> 0x00d8 }
            if (r8 == 0) goto L_0x0050
            java.lang.String r8 = r7.getString(r2)     // Catch:{ JSONException -> 0x00d8 }
            goto L_0x0054
        L_0x0050:
            java.lang.String r8 = r0.getString(r2)     // Catch:{ JSONException -> 0x00d8 }
        L_0x0054:
            r12 = r8
            java.lang.String r8 = "author"
            org.json.JSONObject r8 = r0.getJSONObject(r8)     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r9 = "name"
            java.lang.String r16 = r8.getString(r9)     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r8 = "url"
            java.lang.String r19 = r7.getString(r8)     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r8 = "content-type"
            java.lang.String r8 = r7.getString(r8)     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r9 = "size"
            int r9 = r7.getInt(r9)     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r10 = "file-name"
            java.lang.String r20 = r7.getString(r10)     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r10 = "secure?"
            boolean r21 = r7.optBoolean(r10, r4)     // Catch:{ JSONException -> 0x00d8 }
            boolean r10 = r0.optBoolean(r1, r4)     // Catch:{ JSONException -> 0x00d8 }
            if (r10 != 0) goto L_0x008e
            boolean r10 = r7.optBoolean(r1, r4)     // Catch:{ JSONException -> 0x00d8 }
            if (r10 == 0) goto L_0x008c
            goto L_0x008e
        L_0x008c:
            r10 = 0
            goto L_0x008f
        L_0x008e:
            r10 = 1
        L_0x008f:
            java.lang.String r4 = "md_state"
            r23 = r1
            java.lang.String r1 = ""
            java.lang.String r1 = r0.optString(r4, r1)     // Catch:{ JSONException -> 0x00d8 }
            int r1 = r(r1)     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r4 = "image"
            boolean r4 = r7.optBoolean(r4)     // Catch:{ JSONException -> 0x00d8 }
            if (r4 == 0) goto L_0x00be
            com.helpshift.j.a.a.e r4 = new com.helpshift.j.a.a.e     // Catch:{ JSONException -> 0x00d8 }
            java.lang.String r0 = "thumbnail"
            java.lang.String r0 = r7.getString(r0)     // Catch:{ JSONException -> 0x00d8 }
            r7 = r10
            r10 = r4
            r17 = r19
            r18 = r20
            r19 = r0
            r20 = r8
            r22 = r9
            r10.<init>(r11, r12, r13, r14, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ JSONException -> 0x00d8 }
            r0 = r4
            goto L_0x00c9
        L_0x00be:
            r7 = r10
            com.helpshift.j.a.a.b r0 = new com.helpshift.j.a.a.b     // Catch:{ JSONException -> 0x00d8 }
            r10 = r0
            r17 = r9
            r18 = r8
            r10.<init>(r11, r12, r13, r14, r16, r17, r18, r19, r20, r21)     // Catch:{ JSONException -> 0x00d8 }
        L_0x00c9:
            r0.u = r1     // Catch:{ JSONException -> 0x00d8 }
            r0.y = r7     // Catch:{ JSONException -> 0x00d8 }
            r3.add(r0)     // Catch:{ JSONException -> 0x00d8 }
            r0 = r25
            r1 = r23
            r4 = 0
            goto L_0x000d
        L_0x00d7:
            return r3
        L_0x00d8:
            r0 = move-exception
            com.helpshift.common.exception.c r1 = com.helpshift.common.exception.c.GENERIC
            java.lang.String r2 = "Parsing exception while reading admin attachment message"
            com.helpshift.common.exception.RootAPIException r0 = com.helpshift.common.exception.RootAPIException.a(r0, r1, r2)
            goto L_0x00e3
        L_0x00e2:
            throw r0
        L_0x00e3:
            goto L_0x00e2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.e.s.a(org.json.JSONObject, org.json.JSONArray):java.util.List");
    }

    private y j(JSONObject jSONObject) {
        boolean z;
        try {
            JSONObject optJSONObject = jSONObject.getJSONObject("meta").optJSONObject("response");
            boolean optBoolean = optJSONObject != null ? optJSONObject.optBoolean(ServerProtocol.DIALOG_PARAM_STATE) : false;
            if (!jSONObject.optBoolean("invisible")) {
                if (!optBoolean) {
                    z = false;
                    String string = jSONObject.getString("created_at");
                    y yVar = new y(jSONObject.getString("id"), jSONObject.getString("body"), string, com.helpshift.common.g.b.b(string), jSONObject.getJSONObject("author").getString("name"), z);
                    yVar.u = r(jSONObject.optString("md_state", ""));
                    yVar.y = jSONObject.optBoolean("redacted", false);
                    return yVar;
                }
            }
            z = true;
            String string2 = jSONObject.getString("created_at");
            y yVar2 = new y(jSONObject.getString("id"), jSONObject.getString("body"), string2, com.helpshift.common.g.b.b(string2), jSONObject.getJSONObject("author").getString("name"), z);
            yVar2.u = r(jSONObject.optString("md_state", ""));
            yVar2.y = jSONObject.optBoolean("redacted", false);
            return yVar2;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading request review message");
        }
    }

    private List<v> k(JSONObject jSONObject) {
        try {
            ArrayList arrayList = new ArrayList();
            JSONObject jSONObject2 = jSONObject.getJSONObject("meta");
            JSONObject optJSONObject = jSONObject2.optJSONObject("response");
            JSONArray optJSONArray = jSONObject2.optJSONArray("attachments");
            boolean z = optJSONObject != null ? optJSONObject.getBoolean(ServerProtocol.DIALOG_PARAM_STATE) : false;
            String string = jSONObject.getString("created_at");
            aa aaVar = new aa(jSONObject.getString("id"), jSONObject.getString("body"), string, com.helpshift.common.g.b.b(string), jSONObject.getJSONObject("author").getString("name"), z);
            aaVar.u = r(jSONObject.optString("md_state", ""));
            aaVar.y = jSONObject.optBoolean("redacted", false);
            arrayList.add(aaVar);
            if (optJSONArray != null) {
                arrayList.addAll(a(jSONObject, optJSONArray));
            }
            return arrayList;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading request screenshot message");
        }
    }

    private ab l(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        try {
            boolean z = false;
            JSONObject jSONObject3 = jSONObject2.getJSONObject("meta").getJSONArray("attachments").getJSONObject(0);
            String string = jSONObject2.getString("created_at");
            ab abVar = new ab(jSONObject3.has("body") ? jSONObject3.getString("body") : jSONObject2.getString("body"), string, com.helpshift.common.g.b.b(string), jSONObject2.getJSONObject("author").getString("name"), jSONObject3.getString("content-type"), jSONObject3.optString("thumbnail", ""), jSONObject3.getString("file-name"), jSONObject3.getString("url"), jSONObject3.getInt("size"), jSONObject3.optBoolean("secure?", false));
            abVar.m = jSONObject2.getString("id");
            abVar.u = r(jSONObject2.optString("md_state", ""));
            if (jSONObject2.optBoolean("redacted", false) || jSONObject3.optBoolean("redacted", false)) {
                z = true;
            }
            abVar.y = z;
            a(abVar, jSONObject2);
            return abVar;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Parsing exception while reading screenshot message");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003f A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int r(java.lang.String r5) {
        /*
            int r0 = r5.hashCode()
            r1 = -840272977(0xffffffffcdea73af, float:-4.91681248E8)
            r2 = 0
            r3 = 2
            r4 = 1
            if (r0 == r1) goto L_0x002b
            r1 = 3496342(0x355996, float:4.899419E-39)
            if (r0 == r1) goto L_0x0021
            r1 = 3526552(0x35cf98, float:4.941752E-39)
            if (r0 == r1) goto L_0x0017
            goto L_0x0035
        L_0x0017:
            java.lang.String r0 = "sent"
            boolean r5 = r5.equals(r0)
            if (r5 == 0) goto L_0x0035
            r5 = 2
            goto L_0x0036
        L_0x0021:
            java.lang.String r0 = "read"
            boolean r5 = r5.equals(r0)
            if (r5 == 0) goto L_0x0035
            r5 = 1
            goto L_0x0036
        L_0x002b:
            java.lang.String r0 = "unread"
            boolean r5 = r5.equals(r0)
            if (r5 == 0) goto L_0x0035
            r5 = 0
            goto L_0x0036
        L_0x0035:
            r5 = -1
        L_0x0036:
            if (r5 == 0) goto L_0x003f
            if (r5 == r4) goto L_0x003e
            if (r5 == r3) goto L_0x003d
            return r2
        L_0x003d:
            return r3
        L_0x003e:
            return r4
        L_0x003f:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.e.s.r(java.lang.String):int");
    }

    private static void a(v vVar, JSONObject jSONObject) throws JSONException {
        vVar.o = jSONObject.getJSONObject("author").optString("id");
        vVar.x = jSONObject.optString("request_id");
    }
}
