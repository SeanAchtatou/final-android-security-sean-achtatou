package com.helpshift.common.e;

import java.io.Serializable;
import java.util.Map;

/* compiled from: KVStore */
public interface aa {
    Integer a(String str, Integer num);

    String a(String str);

    void a();

    void a(String str, Serializable serializable);

    void a(String str, Boolean bool);

    void a(String str, Float f);

    void a(String str, Long l);

    void a(String str, String str2);

    void a(Map<String, Serializable> map);

    Boolean b(String str, Boolean bool);

    Float b(String str, Float f);

    Long b(String str, Long l);

    Object b(String str);

    String b(String str, String str2);
}
