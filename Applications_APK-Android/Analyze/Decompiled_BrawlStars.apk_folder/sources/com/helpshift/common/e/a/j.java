package com.helpshift.common.e.a;

import java.util.List;

/* compiled from: Response */
public class j {

    /* renamed from: a  reason: collision with root package name */
    public final int f3322a;

    /* renamed from: b  reason: collision with root package name */
    public final String f3323b;
    public final List<c> c;

    public j(int i, String str, List<c> list) {
        this.f3322a = i;
        this.f3323b = str;
        this.c = list;
    }
}
