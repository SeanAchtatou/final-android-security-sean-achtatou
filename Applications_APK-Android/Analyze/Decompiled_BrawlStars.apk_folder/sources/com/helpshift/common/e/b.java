package com.helpshift.common.e;

import android.content.Context;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.w;
import com.helpshift.j.a.p;
import com.helpshift.j.b.a;
import com.helpshift.j.b.c;
import com.helpshift.support.Faq;
import com.helpshift.util.n;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/* compiled from: AndroidConversationDAO */
class b implements a, c {

    /* renamed from: a  reason: collision with root package name */
    private final com.helpshift.common.a.a f3326a;

    /* renamed from: b  reason: collision with root package name */
    private final String f3327b = "Helpshift_CnDAO";

    b(Context context) {
        this.f3326a = com.helpshift.common.a.a.a(context);
    }

    public final void a() {
        this.f3326a.a();
    }

    public final synchronized com.helpshift.j.a.b.a a(String str) {
        return this.f3326a.a(str);
    }

    public final synchronized com.helpshift.j.a.b.a b(String str) {
        return this.f3326a.b(str);
    }

    public final com.helpshift.j.a.b.a a(Long l) {
        return this.f3326a.a(l);
    }

    public final synchronized void a(long j) {
        if (j != 0) {
            this.f3326a.b(j);
        }
    }

    public final synchronized List<com.helpshift.j.a.b.a> b(long j) {
        return this.f3326a.a(j);
    }

    public final synchronized void a(com.helpshift.j.a.b.a aVar) {
        if (aVar.e == null) {
            aVar.e = UUID.randomUUID().toString();
        }
        long a2 = this.f3326a.a(aVar);
        if (a2 != -1) {
            aVar.a(a2);
        }
    }

    public final synchronized Map<Long, Integer> a(List<Long> list) {
        return this.f3326a.a(list, (String[]) null);
    }

    public final synchronized Map<Long, Integer> a(List<Long> list, String[] strArr) {
        return this.f3326a.a(list, strArr);
    }

    public final synchronized List<v> c(long j) {
        return this.f3326a.d(j);
    }

    public final List<v> a(long j, w wVar) {
        return this.f3326a.a(j, wVar);
    }

    public final List<v> b(List<Long> list) {
        return this.f3326a.a(list);
    }

    public final v c(String str) {
        return this.f3326a.c(str);
    }

    public final synchronized void a(v vVar) {
        Long l = vVar.r;
        String str = vVar.m;
        if (l == null && str == null) {
            long a2 = this.f3326a.a(vVar);
            if (a2 != -1) {
                vVar.r = Long.valueOf(a2);
            }
        } else if (l == null && str != null) {
            v c = this.f3326a.c(str);
            if (c == null) {
                long a3 = this.f3326a.a(vVar);
                if (a3 != -1) {
                    vVar.r = Long.valueOf(a3);
                }
            } else {
                vVar.r = c.r;
                this.f3326a.b(vVar);
            }
        } else if (this.f3326a.b(l) == null) {
            long a4 = this.f3326a.a(vVar);
            if (a4 != -1) {
                vVar.r = Long.valueOf(a4);
            }
        } else {
            this.f3326a.b(vVar);
        }
    }

    public final synchronized void c(List<v> list) {
        if (list.size() != 0) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (v next : list) {
                Long l = next.r;
                String str = next.m;
                if (l == null && str == null) {
                    arrayList.add(next);
                } else if (l == null && str != null) {
                    v c = this.f3326a.c(str);
                    if (c == null) {
                        arrayList.add(next);
                    } else {
                        next.r = c.r;
                        arrayList2.add(next);
                    }
                } else if (this.f3326a.b(l) == null) {
                    arrayList.add(next);
                } else {
                    arrayList2.add(next);
                }
            }
            List<Long> c2 = this.f3326a.c(arrayList);
            for (int i = 0; i < arrayList.size(); i++) {
                long longValue = c2.get(i).longValue();
                if (longValue != -1) {
                    ((v) arrayList.get(i)).r = Long.valueOf(longValue);
                }
            }
            this.f3326a.d(arrayList2);
        }
    }

    public final void b(com.helpshift.j.a.b.a aVar) {
        String str = aVar.c;
        String str2 = aVar.d;
        if (str != null || str2 != null) {
            if (aVar.e == null) {
                aVar.e = UUID.randomUUID().toString();
            }
            long a2 = this.f3326a.a(aVar);
            if (a2 != -1) {
                aVar.a(a2);
            }
            c(aVar.j);
        }
    }

    public final void c(com.helpshift.j.a.b.a aVar) {
        String str = aVar.c;
        String str2 = aVar.d;
        if (str != null || str2 != null) {
            this.f3326a.b(aVar);
            c(aVar.j);
        }
    }

    public final void a(Long l, long j) {
        if (l == null) {
            n.b("Helpshift_CnDAO", "Trying to update last user activity time but localId is null");
        } else {
            this.f3326a.a(l, j);
        }
    }

    public final void d(com.helpshift.j.a.b.a aVar) {
        this.f3326a.b(aVar);
    }

    public final void d(List<com.helpshift.j.a.b.a> list) {
        if (list.size() != 0) {
            for (com.helpshift.j.a.b.a next : list) {
                if (next.e == null) {
                    next.e = UUID.randomUUID().toString();
                }
            }
            List<Long> a2 = this.f3326a.a(list);
            HashSet hashSet = new HashSet();
            for (int i = 0; i < list.size(); i++) {
                long longValue = a2.get(i).longValue();
                com.helpshift.j.a.b.a aVar = list.get(i);
                if (longValue == -1) {
                    hashSet.add(aVar);
                } else {
                    aVar.a(longValue);
                }
            }
            ArrayList arrayList = new ArrayList();
            for (com.helpshift.j.a.b.a next2 : list) {
                if (!hashSet.contains(next2)) {
                    arrayList.addAll(next2.j);
                }
            }
            c(arrayList);
        }
    }

    public final void a(List<com.helpshift.j.a.b.a> list, Map<Long, p> map) {
        if (list.size() != 0) {
            this.f3326a.b(list);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (com.helpshift.j.a.b.a next : list) {
                if (map.containsKey(next.f3504b)) {
                    p pVar = map.get(next.f3504b);
                    arrayList.addAll(pVar.f3530b);
                    arrayList2.addAll(pVar.f3529a);
                }
            }
            List<Long> c = this.f3326a.c(arrayList);
            for (int i = 0; i < arrayList.size(); i++) {
                long longValue = c.get(i).longValue();
                if (longValue != -1) {
                    ((v) arrayList.get(i)).r = Long.valueOf(longValue);
                }
            }
            this.f3326a.d(arrayList2);
        }
    }

    public final void d(long j) {
        if (j > 0) {
            this.f3326a.g(j);
        }
    }

    public final String e(long j) {
        return this.f3326a.h(j);
    }

    public final Long f(long j) {
        return this.f3326a.i(j);
    }

    public final boolean g(long j) {
        return this.f3326a.e(j);
    }

    public final Object a(String str, String str2) {
        return this.f3326a.a(str, str2);
    }

    public final void a(Object obj) {
        this.f3326a.a((Faq) obj);
    }

    public final void b(String str, String str2) {
        this.f3326a.b(str, str2);
    }
}
