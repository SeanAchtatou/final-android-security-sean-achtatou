package com.helpshift.common.e;

import com.helpshift.h.a.a;
import java.util.ArrayList;

/* compiled from: AndroidCustomIssueFieldDAO */
public class d implements a {

    /* renamed from: a  reason: collision with root package name */
    private aa f3330a;

    public d(aa aaVar) {
        this.f3330a = aaVar;
    }

    public final ArrayList<com.helpshift.h.b.a> a() {
        Object b2 = this.f3330a.b("key_custom_issue_field_storage");
        if (b2 instanceof ArrayList) {
            return (ArrayList) b2;
        }
        return null;
    }

    public final void a(ArrayList<com.helpshift.h.b.a> arrayList) {
        if (arrayList == null || arrayList.size() <= 0) {
            arrayList = null;
        }
        this.f3330a.a("key_custom_issue_field_storage", arrayList);
    }
}
