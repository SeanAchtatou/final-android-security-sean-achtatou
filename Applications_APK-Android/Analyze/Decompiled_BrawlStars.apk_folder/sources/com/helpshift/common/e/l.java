package com.helpshift.common.e;

import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.facebook.appevents.UserDataStore;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.ShareConstants;
import com.helpshift.b.b.a;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.c;
import com.helpshift.q.b.b;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AndroidJsonifier */
public class l implements z {
    public final String a(Map<String, Object> map) {
        try {
            JSONObject jSONObject = new JSONObject();
            for (Map.Entry next : map.entrySet()) {
                jSONObject.put((String) next.getKey(), next.getValue());
            }
            return jSONObject.toString();
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Exception while calling jsonify on map");
        }
    }

    public final Object b(Map<String, Object> map) {
        try {
            JSONObject jSONObject = new JSONObject();
            for (Map.Entry next : map.entrySet()) {
                jSONObject.put((String) next.getKey(), next.getValue());
            }
            return jSONObject;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Exception while calling jsonify on map");
        }
    }

    public final String a(Collection collection) {
        return new JSONArray(collection).toString();
    }

    public final Object a(List<String> list) {
        JSONArray jSONArray = new JSONArray();
        for (String put : list) {
            jSONArray.put(put);
        }
        return jSONArray;
    }

    public final String b(List<a> list) {
        if (list == null) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        try {
            for (a next : list) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("ts", next.d);
                jSONObject.put("t", next.f3207b.A);
                if (next.c != null) {
                    jSONObject.put("d", b(new HashMap(next.c)));
                }
                jSONArray.put(jSONObject);
            }
            return jSONArray.toString();
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Exception while forming analytics string");
        }
    }

    public final Object c(List<com.helpshift.q.b.a> list) {
        JSONArray jSONArray = new JSONArray();
        if (list != null) {
            try {
                for (com.helpshift.q.b.a next : list) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put(NativeProtocol.WEB_DIALOG_ACTION, next.f3813a);
                    jSONObject.put("datetime", next.f3814b);
                    jSONArray.put(jSONObject);
                }
            } catch (JSONException e) {
                throw RootAPIException.a(e, c.GENERIC, "Exception while forming breadcrumb string");
            }
        }
        return jSONArray;
    }

    public final Object d(List<b> list) {
        JSONArray jSONArray = new JSONArray();
        try {
            for (b next : list) {
                JSONObject jSONObject = new JSONObject();
                if (next.c != null) {
                    jSONObject.put(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, next.c);
                }
                jSONObject.put("level", next.f3815a);
                jSONObject.put("tag", next.f3816b);
                if (!TextUtils.isEmpty(next.d)) {
                    jSONObject.put("exception", next.d);
                }
                jSONArray.put(jSONObject);
            }
            return jSONArray;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Exception while forming debugLog string");
        }
    }

    public final String a(String str, String str2) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            jSONObject.remove(str2);
            return jSONObject.toString();
        } catch (JSONException unused) {
            return str;
        }
    }

    public final Object c(Map<String, Serializable> map) {
        try {
            JSONObject jSONObject = new JSONObject();
            for (Map.Entry next : map.entrySet()) {
                Object value = next.getValue();
                if (value instanceof String[]) {
                    value = a((List<String>) new ArrayList(Arrays.asList((String[]) value)));
                }
                jSONObject.put((String) next.getKey(), value);
            }
            return jSONObject;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Exception while forming custom meta string");
        }
    }

    public final Object e(List<com.helpshift.h.b.a> list) {
        JSONObject jSONObject = new JSONObject();
        for (com.helpshift.h.b.a next : list) {
            try {
                JSONArray jSONArray = new JSONArray();
                jSONArray.put(next.f3416b);
                for (String put : next.c) {
                    jSONArray.put(put);
                }
                jSONObject.put(next.f3415a, jSONArray);
            } catch (JSONException e) {
                throw RootAPIException.a(e, c.GENERIC, "Exception while forming custom issue field string");
            }
        }
        return jSONObject;
    }

    public final Object a(String str) {
        try {
            return new JSONArray(str);
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Exception while jsonifying string to array");
        }
    }

    public final Object f(List<com.helpshift.p.c.a> list) {
        try {
            JSONArray jSONArray = new JSONArray();
            for (com.helpshift.p.c.a next : list) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("dt", next.f3798a);
                jSONObject.put("l", next.d);
                jSONObject.put(UserDataStore.CITY, next.e);
                jSONObject.put(NotificationCompat.CATEGORY_MESSAGE, next.f3799b);
                jSONObject.put(UserDataStore.STATE, next.c);
                if (!TextUtils.isEmpty(next.f)) {
                    jSONObject.put("src", "sdk.android." + next.f);
                }
                jSONArray.put(jSONObject);
            }
            return jSONArray;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Exception while jsonifying LogModelList");
        }
    }

    public final Object b(String str, String str2) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(str, str2);
            return jSONObject;
        } catch (JSONException e) {
            throw RootAPIException.a(e, c.GENERIC, "Exception while jsonifying single object.");
        }
    }
}
