package com.helpshift.common.e;

import com.helpshift.b.b.a;
import com.helpshift.q.b.b;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/* compiled from: Jsonifier */
public interface z {
    Object a(String str);

    Object a(List<String> list);

    String a(String str, String str2);

    String a(Collection collection);

    String a(Map<String, Object> map);

    Object b(String str, String str2);

    Object b(Map<String, Object> map);

    String b(List<a> list);

    Object c(List<com.helpshift.q.b.a> list);

    Object c(Map<String, Serializable> map);

    Object d(List<b> list);

    Object e(List<com.helpshift.h.b.a> list);

    Object f(List<com.helpshift.p.c.a> list);
}
