package com.helpshift.common.e;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.places.model.PlaceFields;
import com.helpshift.common.b.a;
import com.helpshift.common.e.y;
import com.helpshift.common.k;
import com.helpshift.q.b.c;
import com.helpshift.support.m.i;
import com.helpshift.util.b;
import com.helpshift.util.n;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

/* compiled from: AndroidDevice */
public class e implements y {

    /* renamed from: a  reason: collision with root package name */
    aa f3331a;

    /* renamed from: b  reason: collision with root package name */
    a f3332b;
    private final Context c;
    private String d;
    private String e;

    public final String a() {
        return "7.6.3";
    }

    public final String b() {
        return "Android";
    }

    public final String r() {
        return "3";
    }

    e(Context context, aa aaVar, a aVar) {
        this.c = context;
        this.f3331a = aaVar;
        this.f3332b = aVar;
    }

    public final String c() {
        return Build.VERSION.RELEASE;
    }

    public final int d() {
        return Build.VERSION.SDK_INT;
    }

    public final String e() {
        return b.a(this.c);
    }

    public final String f() {
        return b.c(this.c);
    }

    public final String g() {
        return this.c.getPackageName();
    }

    public final String h() {
        return Locale.getDefault().toString();
    }

    public final String i() {
        return Build.MODEL;
    }

    public final String j() {
        return System.getProperty("os.version") + ":" + Build.FINGERPRINT;
    }

    public final String k() {
        TelephonyManager telephonyManager = (TelephonyManager) this.c.getSystemService(PlaceFields.PHONE);
        if (telephonyManager == null) {
            return "";
        }
        return telephonyManager.getSimCountryIso();
    }

    public final String l() {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH).format(new Date());
    }

    public final String m() {
        TelephonyManager telephonyManager = (TelephonyManager) this.c.getSystemService(PlaceFields.PHONE);
        if (telephonyManager == null) {
            return "";
        }
        return telephonyManager.getNetworkOperatorName();
    }

    public final String n() {
        NetworkInfo activeNetworkInfo;
        String str = null;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.c.getSystemService("connectivity");
            if (!(connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null)) {
                str = activeNetworkInfo.getTypeName();
            }
        } catch (SecurityException unused) {
        }
        return str == null ? AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN : str;
    }

    public final String o() {
        Intent registerReceiver = this.c.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return "Not charging";
        }
        int intExtra = registerReceiver.getIntExtra("status", -1);
        if (intExtra == 2 || intExtra == 5) {
            return "Charging";
        }
        return "Not charging";
    }

    public final String p() {
        Intent registerReceiver = this.c.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return "";
        }
        int intExtra = (int) ((((float) registerReceiver.getIntExtra("level", -1)) / ((float) registerReceiver.getIntExtra("scale", -1))) * 100.0f);
        return intExtra + "%";
    }

    public final c q() {
        double d2;
        double d3;
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        if (Build.VERSION.SDK_INT >= 18) {
            double availableBlocksLong = (double) statFs.getAvailableBlocksLong();
            double blockSizeLong = (double) statFs.getBlockSizeLong();
            Double.isNaN(availableBlocksLong);
            Double.isNaN(blockSizeLong);
            double round = (double) Math.round(((availableBlocksLong * blockSizeLong) / 1.073741824E9d) * 100.0d);
            Double.isNaN(round);
            d2 = round / 100.0d;
            double blockCountLong = (double) statFs.getBlockCountLong();
            double blockSizeLong2 = (double) statFs.getBlockSizeLong();
            Double.isNaN(blockCountLong);
            Double.isNaN(blockSizeLong2);
            d3 = (double) Math.round(((blockCountLong * blockSizeLong2) / 1.073741824E9d) * 100.0d);
            Double.isNaN(d3);
        } else {
            double availableBlocks = (double) statFs.getAvailableBlocks();
            double blockSize = (double) statFs.getBlockSize();
            Double.isNaN(availableBlocks);
            Double.isNaN(blockSize);
            double round2 = (double) Math.round(((availableBlocks * blockSize) / 1.073741824E9d) * 100.0d);
            Double.isNaN(round2);
            d2 = round2 / 100.0d;
            double blockCount = (double) statFs.getBlockCount();
            double blockSize2 = (double) statFs.getBlockSize();
            Double.isNaN(blockCount);
            Double.isNaN(blockSize2);
            d3 = (double) Math.round(((blockCount * blockSize2) / 1.073741824E9d) * 100.0d);
            Double.isNaN(d3);
        }
        return new c((d3 / 100.0d) + " GB", d2 + " GB");
    }

    public final y.a a(y.b bVar) {
        int i = f.f3333a[bVar.ordinal()];
        if (i == 1) {
            return b("android.permission.READ_EXTERNAL_STORAGE");
        }
        if (i != 2) {
            return null;
        }
        return b("android.permission.WRITE_EXTERNAL_STORAGE");
    }

    public final Locale s() {
        Configuration configuration = this.c.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= 24) {
            return configuration.getLocales().get(0);
        }
        return configuration.locale;
    }

    public final void a(Locale locale) {
        Resources resources = this.c.getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, displayMetrics);
    }

    public final boolean t() {
        return DateFormat.is24HourFormat(this.c);
    }

    public final String u() {
        return TimeZone.getDefault().getID();
    }

    public final long v() {
        TimeZone timeZone = new GregorianCalendar().getTimeZone();
        return (long) (timeZone.getRawOffset() + timeZone.getDSTSavings());
    }

    public final String w() {
        String str = this.d;
        if (str != null) {
            return str;
        }
        this.d = this.f3331a.a("key_support_device_id");
        if (k.a(this.d)) {
            this.d = (String) this.f3332b.a("key_support_device_id");
            if (!k.a(this.d)) {
                this.f3331a.a("key_support_device_id", this.d);
            }
        } else {
            this.f3332b.a("key_support_device_id", this.d);
        }
        if (k.a(this.d)) {
            this.d = UUID.randomUUID().toString();
            this.f3331a.a("key_support_device_id", this.d);
            this.f3332b.a("key_support_device_id", this.d);
        }
        return this.d;
    }

    public final String x() {
        if (this.e == null) {
            this.e = this.f3331a.a("key_push_token");
        }
        return this.e;
    }

    public final void a(String str) {
        this.f3331a.a("key_push_token", str);
        this.e = str;
    }

    public final String y() {
        try {
            return Settings.Secure.getString(this.c.getContentResolver(), "android_id");
        } catch (Exception e2) {
            n.c("AndroidDevice", "Exception while getting android_id", e2);
            return null;
        }
    }

    private y.a b(String str) {
        int i = Build.VERSION.SDK_INT;
        if (i < 19) {
            return y.a.AVAILABLE;
        }
        if (b.a(this.c, str)) {
            return y.a.AVAILABLE;
        }
        if (i < 23) {
            return y.a.UNAVAILABLE;
        }
        if (i.a(this.c, str)) {
            return y.a.REQUESTABLE;
        }
        return y.a.UNAVAILABLE;
    }
}
