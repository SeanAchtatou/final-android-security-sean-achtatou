package com.helpshift.common.e;

import com.helpshift.q.b.c;
import java.util.Locale;

/* compiled from: Device */
public interface y {

    /* compiled from: Device */
    public enum a {
        AVAILABLE,
        UNAVAILABLE,
        REQUESTABLE
    }

    /* compiled from: Device */
    public enum b {
        READ_STORAGE,
        WRITE_STORAGE
    }

    a a(b bVar);

    String a();

    void a(String str);

    void a(Locale locale);

    String b();

    String c();

    int d();

    String e();

    String f();

    String g();

    String h();

    String i();

    String j();

    String k();

    String l();

    String m();

    String n();

    String o();

    String p();

    c q();

    String r();

    Locale s();

    boolean t();

    String u();

    long v();

    String w();

    String x();

    String y();
}
