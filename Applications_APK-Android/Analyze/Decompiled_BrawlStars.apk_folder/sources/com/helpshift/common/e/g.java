package com.helpshift.common.e;

import android.os.Handler;
import android.os.HandlerThread;
import com.helpshift.n.b.a;
import com.helpshift.support.h;
import com.helpshift.support.s;
import java.util.ArrayList;

/* compiled from: AndroidFAQSearchDM */
public class g implements a {

    /* renamed from: a  reason: collision with root package name */
    h f3334a;

    public g(h hVar) {
        this.f3334a = hVar;
    }

    public final ArrayList a(String str) {
        return this.f3334a.a(str, s.a.KEYWORD_SEARCH, (com.helpshift.support.g) null);
    }

    public final void a() {
        HandlerThread handlerThread = new HandlerThread("HS-faqdm-index");
        handlerThread.start();
        new Handler(handlerThread.getLooper()).post(new h(this));
    }
}
