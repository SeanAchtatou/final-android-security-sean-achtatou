package com.helpshift.common.e.a.a;

import com.helpshift.websockets.WebSocketException;
import com.helpshift.websockets.aq;

/* compiled from: NVWebSocketListenerImpl */
class c implements aq {

    /* renamed from: a  reason: collision with root package name */
    private final b f3311a;

    /* renamed from: b  reason: collision with root package name */
    private final a f3312b;

    c(a aVar, b bVar) {
        this.f3311a = bVar;
        this.f3312b = aVar;
    }

    public final void a() throws Exception {
        this.f3311a.a(this.f3312b);
    }

    public final void b() throws Exception {
        this.f3311a.a();
    }

    public final void a(String str) throws Exception {
        this.f3311a.a(str);
    }

    public final void a(WebSocketException webSocketException) throws Exception {
        this.f3311a.b(webSocketException.getMessage());
    }

    public final void b(WebSocketException webSocketException) throws Exception {
        this.f3311a.b(webSocketException.getMessage());
    }

    public final void c(WebSocketException webSocketException) throws Exception {
        this.f3311a.b(webSocketException.getMessage());
    }

    public final void d(WebSocketException webSocketException) throws Exception {
        this.f3311a.b(webSocketException.getMessage());
    }

    public final void e(WebSocketException webSocketException) throws Exception {
        this.f3311a.b(webSocketException.getMessage());
    }
}
