package com.helpshift.common.e.a.a;

import com.helpshift.websockets.aj;
import com.helpshift.websockets.ao;
import com.helpshift.websockets.q;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: HSWebSocket */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public final aj f3307a;

    /* renamed from: b  reason: collision with root package name */
    public final b f3308b;

    public a(aj ajVar, b bVar) {
        this.f3307a = ajVar;
        this.f3308b = bVar;
        ajVar.c.a(new c(this, bVar));
    }

    public final void a(String str) {
        ao aoVar;
        try {
            aj ajVar = this.f3307a;
            ao aoVar2 = new ao();
            aoVar2.f4318a = true;
            aoVar2.e = 1;
            if (str != null) {
                if (str.length() != 0) {
                    aoVar = aoVar2.a(q.a(str));
                    ajVar.a(aoVar);
                }
            }
            aoVar = aoVar2.a((byte[]) null);
            ajVar.a(aoVar);
        } catch (Exception e) {
            this.f3308b.b(e.getMessage());
        }
    }

    /* renamed from: com.helpshift.common.e.a.a.a$a  reason: collision with other inner class name */
    /* compiled from: HSWebSocket */
    public static class C0136a {

        /* renamed from: a  reason: collision with root package name */
        public String f3309a;

        /* renamed from: b  reason: collision with root package name */
        public int f3310b;
        public int c;
        public List<String> d = new ArrayList();
        public List<String> e = new ArrayList();
        public Map<String, String> f = new HashMap();
        public b g;

        public C0136a(String str) {
            this.f3309a = str;
        }

        public final C0136a a(String str) {
            this.d.add(str);
            return this;
        }
    }
}
