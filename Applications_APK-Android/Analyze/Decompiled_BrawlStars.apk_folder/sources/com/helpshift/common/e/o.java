package com.helpshift.common.e;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import com.helpshift.R;
import com.helpshift.a.a.d;
import com.helpshift.a.a.f;
import com.helpshift.a.a.g;
import com.helpshift.a.a.j;
import com.helpshift.a.a.n;
import com.helpshift.common.c.t;
import com.helpshift.common.e.a.e;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.k;
import com.helpshift.j.b.b;
import com.helpshift.m.c;
import com.helpshift.q.a.a;
import com.helpshift.support.activities.ParentActivity;
import com.helpshift.support.h;
import com.helpshift.t.a;
import com.helpshift.util.p;
import com.helpshift.util.q;
import com.helpshift.util.r;
import com.helpshift.util.s;
import com.helpshift.util.y;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: AndroidPlatform */
public class o implements ab {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3341a;

    /* renamed from: b  reason: collision with root package name */
    private String f3342b;
    private String c;
    private String d;
    private h e;
    private aa f;
    private y g;
    private e h;
    private a i;
    private com.helpshift.j.b.a j;
    private b k;
    private com.helpshift.b.a l;
    private com.helpshift.h.a.a m;
    private com.helpshift.common.b.a n = new a();
    private com.helpshift.n.a.a o;
    private com.helpshift.n.b.a p;
    private t q;
    private c r;
    private Context s;
    private z t;
    private f u;
    private j v;
    private g w;
    private com.helpshift.r.b x;
    private com.helpshift.r.a y;
    private com.helpshift.w.b z;

    public o(Context context, String str, String str2, String str3) {
        this.f3341a = context;
        this.f3342b = str;
        this.c = str2;
        this.d = str3;
        this.f = new com.helpshift.support.l.j(context);
        e eVar = new e(context, this.f, this.n);
        String a2 = eVar.f3331a.a("key_support_device_id");
        if (!k.a(a2)) {
            eVar.f3332b.a("key_support_device_id", a2);
        }
        this.g = eVar;
        this.v = new com.helpshift.a.a.e(com.helpshift.a.a.k.a(context));
        this.u = new f(this.f);
        this.w = new com.helpshift.a.a.a(com.helpshift.a.a.k.a(context));
        this.t = new l();
        this.l = new com.helpshift.support.l.a(this.f);
        this.i = new m(this.f);
    }

    public final String a() {
        return this.f3342b;
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public final y d() {
        return this.g;
    }

    public final synchronized b e() {
        if (this.k == null) {
            this.k = new c(this.f3341a, this.f);
        }
        return this.k;
    }

    public final synchronized com.helpshift.j.b.a f() {
        if (this.j == null) {
            this.j = new b(this.f3341a);
        }
        return this.j;
    }

    public final synchronized com.helpshift.j.b.c g() {
        if (this.j == null) {
            this.j = new b(this.f3341a);
        }
        return (com.helpshift.j.b.c) this.j;
    }

    public final a h() {
        return this.i;
    }

    public final com.helpshift.b.a i() {
        return this.l;
    }

    public final synchronized com.helpshift.h.a.a j() {
        if (this.m == null) {
            this.m = new d(this.f);
        }
        return this.m;
    }

    public final com.helpshift.common.b.a k() {
        return this.n;
    }

    public final com.helpshift.common.e.a.k l() {
        return new s();
    }

    public final com.helpshift.common.e.a.b m() {
        return new k();
    }

    public final synchronized com.helpshift.n.b.a n() {
        if (this.p == null) {
            this.p = new g(E());
        }
        return this.p;
    }

    public final aa o() {
        return this.f;
    }

    public final z p() {
        return this.t;
    }

    public final n q() {
        return this.u;
    }

    public final j r() {
        return this.v;
    }

    public final g s() {
        return this.w;
    }

    public final synchronized e t() {
        if (this.h == null) {
            this.h = new n(this.f);
        }
        return this.h;
    }

    public final synchronized com.helpshift.n.a.a u() {
        if (this.o == null) {
            this.o = new j(this.f);
        }
        return this.o;
    }

    public final boolean v() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public final synchronized t w() {
        if (this.q == null) {
            this.q = new p(this);
        }
        return this.q;
    }

    public final synchronized c x() {
        if (this.r == null) {
            this.r = new t(this.f3341a, this.f);
        }
        return this.r;
    }

    public final boolean a(String str) {
        return com.helpshift.util.g.a(str);
    }

    public final String b(String str) {
        return com.helpshift.util.g.b(str);
    }

    public final String a(String str, String str2) {
        try {
            String a2 = com.helpshift.support.m.b.a(str, str2);
            if (a2 == null) {
                return str;
            }
            return a2;
        } catch (IOException e2) {
            com.helpshift.util.n.a("AndroidPlatform", "Saving attachment", e2);
            return str;
        }
    }

    public final int y() {
        Context context = this.s;
        if (context == null) {
            context = this.f3341a;
        }
        return context.getResources().getInteger(R.integer.hs__issue_description_min_chars);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void a(Long l2, String str, int i2, String str2, boolean z2) {
        Context context = this.s;
        if (context == null) {
            context = com.helpshift.util.b.e(this.f3341a);
        }
        com.helpshift.util.n.a("Helpshift_SupportNotif", "Creating Support notification : \n Id : " + str + "\n Title : " + str2 + "\n Message count : " + i2);
        q.c().m().a(i2);
        String quantityString = context.getResources().getQuantityString(R.plurals.hs__notification_content_title, i2, Integer.valueOf(i2));
        int i3 = context.getApplicationInfo().logo;
        if (i3 == 0) {
            i3 = context.getApplicationInfo().icon;
        }
        Bitmap bitmap = null;
        Integer b2 = q.c().r().b("notificationIconId");
        if (com.helpshift.util.c.a(context, b2)) {
            i3 = b2.intValue();
        }
        Integer b3 = q.c().r().b("notificationLargeIconId");
        if (com.helpshift.util.c.a(context, b3)) {
            bitmap = BitmapFactory.decodeResource(context.getResources(), b3.intValue());
        }
        int abs = str != null ? Math.abs(str.hashCode()) : 0;
        Intent intent = new Intent(context, ParentActivity.class);
        intent.setFlags(268435456);
        intent.putExtra("support_mode", 1);
        intent.putExtra("conversationIdInPush", l2);
        intent.putExtra("isRoot", true);
        PendingIntent activity = PendingIntent.getActivity(context, abs, intent, 0);
        if (com.helpshift.t.f4225a != null) {
            activity = com.helpshift.t.f4225a.a();
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(i3);
        builder.setContentTitle(str2);
        builder.setContentText(quantityString);
        builder.setContentIntent(activity);
        builder.setAutoCancel(true);
        if (bitmap != null) {
            builder.setLargeIcon(bitmap);
        }
        Uri a2 = com.helpshift.util.c.a();
        if (a2 != null) {
            builder.setSound(a2);
            if (com.helpshift.util.b.a(context, "android.permission.VIBRATE")) {
                builder.setDefaults(6);
            } else {
                builder.setDefaults(4);
            }
        } else if (com.helpshift.util.b.a(context, "android.permission.VIBRATE")) {
            builder.setDefaults(-1);
        } else {
            builder.setDefaults(5);
        }
        Notification build = builder.build();
        com.helpshift.t.a aVar = new com.helpshift.t.a(this.f3341a);
        a.C0147a aVar2 = a.C0147a.SUPPORT;
        if (Build.VERSION.SDK_INT >= 26 && com.helpshift.util.b.b(aVar.f4226a) >= 26) {
            Notification.Builder recoverBuilder = Notification.Builder.recoverBuilder(aVar.f4226a, build);
            if (com.helpshift.t.b.f4229a[aVar2.ordinal()] == 1) {
                String c2 = q.c().r().c("supportNotificationChannelId");
                if (y.a(c2)) {
                    NotificationManager d2 = com.helpshift.util.b.d(aVar.f4226a);
                    if (d2 != null && d2.getNotificationChannel("helpshift_default_channel_id") == null) {
                        String string = aVar.f4226a.getResources().getString(R.string.hs__default_notification_channel_name);
                        String string2 = aVar.f4226a.getResources().getString(R.string.hs__default_notification_channel_desc);
                        NotificationChannel notificationChannel = new NotificationChannel("helpshift_default_channel_id", string, 3);
                        notificationChannel.setDescription(string2);
                        Uri a3 = com.helpshift.util.c.a();
                        if (a3 != null) {
                            notificationChannel.setSound(a3, new AudioAttributes.Builder().build());
                        }
                        d2.createNotificationChannel(notificationChannel);
                    }
                    c2 = "helpshift_default_channel_id";
                } else {
                    NotificationManager d3 = com.helpshift.util.b.d(aVar.f4226a);
                    if (!(d3 == null || d3.getNotificationChannel("helpshift_default_channel_id") == null)) {
                        d3.deleteNotificationChannel("helpshift_default_channel_id");
                    }
                }
                recoverBuilder.setChannelId(c2);
                build = recoverBuilder.build();
            } else {
                throw new IllegalStateException();
            }
        }
        Context context2 = this.f3341a;
        if (build != null) {
            com.helpshift.util.n.a("Helpshift_AppUtil", "Showing notification : Tag : " + str);
            NotificationManager d4 = com.helpshift.util.b.d(context2);
            if (d4 != null) {
                d4.notify(str, 1, build);
            }
        }
        if (z2) {
            "" + i2;
        }
    }

    public final void c(String str) {
        Context context = this.f3341a;
        com.helpshift.util.n.a("Helpshift_AppUtil", "Cancelling notification : Tag : " + str + ", id : " + 1);
        NotificationManager d2 = com.helpshift.util.b.d(context);
        if (d2 != null) {
            d2.cancel(str, 1);
        }
    }

    public final boolean A() {
        return p.a(this.f3341a);
    }

    public final void a(Object obj) {
        if (obj == null) {
            this.s = null;
        } else if (obj instanceof Context) {
            this.s = (Context) obj;
        }
    }

    public final synchronized com.helpshift.r.b B() {
        if (this.x == null) {
            this.x = new com.helpshift.a.a.c(com.helpshift.a.a.k.a(this.f3341a));
        }
        return this.x;
    }

    public final synchronized com.helpshift.r.a C() {
        if (this.y == null) {
            this.y = new com.helpshift.a.a.b(com.helpshift.a.a.k.a(this.f3341a));
        }
        return this.y;
    }

    public final synchronized com.helpshift.w.b D() {
        if (this.z == null) {
            this.z = new d(com.helpshift.a.a.k.a(this.f3341a));
        }
        return this.z;
    }

    public final boolean d(String str) {
        return com.helpshift.c.a.a.g.a(this.f3341a, str);
    }

    private synchronized h E() {
        if (this.e == null) {
            this.e = new h(this.f3341a);
        }
        return this.e;
    }

    public final void a(com.helpshift.j.d.d dVar, String str) throws RootAPIException {
        InputStream inputStream;
        try {
            Uri uri = (Uri) dVar.c;
            if (uri == null) {
                com.helpshift.util.n.a("Helpshift_AttachUtil", "Can't proceed if uri is null");
                return;
            }
            Context a2 = q.a();
            h hVar = new h(a2);
            FileOutputStream fileOutputStream = null;
            try {
                String b2 = com.helpshift.support.m.b.b(str, "." + com.helpshift.util.g.a(a2, uri));
                File file = new File(a2.getFilesDir(), b2);
                String absolutePath = file.getAbsolutePath();
                if (!file.exists()) {
                    hVar.a(b2);
                    inputStream = a2.getContentResolver().openInputStream(uri);
                    try {
                        fileOutputStream = a2.openFileOutput(b2, 0);
                        byte[] bArr = new byte[8192];
                        while (true) {
                            int read = inputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                        }
                        dVar.d = absolutePath;
                        dVar.e = true;
                        if (s.a(absolutePath)) {
                            s.a(absolutePath, 1024);
                        }
                    } catch (Throwable th) {
                        th = th;
                        r.a(null);
                        r.a(inputStream);
                        throw th;
                    }
                } else {
                    dVar.d = absolutePath;
                    dVar.e = true;
                    inputStream = null;
                }
                r.a(fileOutputStream);
                r.a(inputStream);
            } catch (Throwable th2) {
                th = th2;
                inputStream = null;
                r.a(null);
                r.a(inputStream);
                throw th;
            }
        } catch (Exception e2) {
            throw RootAPIException.a(e2);
        }
    }

    public final com.helpshift.v.b z() {
        return com.helpshift.v.a.f4261a;
    }
}
