package com.helpshift.common.e;

import com.helpshift.q.a.a;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: AndroidMetadataDAO */
public class m implements a {

    /* renamed from: a  reason: collision with root package name */
    private aa f3338a;

    public m(aa aaVar) {
        this.f3338a = aaVar;
    }

    public final ArrayList<com.helpshift.q.b.a> a() {
        Object b2 = this.f3338a.b("key_bread_crumb_storage");
        if (b2 != null) {
            return (ArrayList) b2;
        }
        return null;
    }

    public final void a(ArrayList<com.helpshift.q.b.a> arrayList) {
        this.f3338a.a("key_bread_crumb_storage", arrayList);
    }

    public final void a(HashMap<String, Serializable> hashMap) {
        this.f3338a.a("key_custom_meta_storage", hashMap);
    }

    public final HashMap<String, Serializable> b() {
        Object b2 = this.f3338a.b("key_custom_meta_storage");
        if (b2 != null) {
            return (HashMap) b2;
        }
        return null;
    }
}
