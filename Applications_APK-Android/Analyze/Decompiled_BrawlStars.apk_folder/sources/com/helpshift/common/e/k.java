package com.helpshift.common.e;

import android.os.Build;
import com.helpshift.c.a.a.f;
import com.helpshift.common.e.a.b;
import com.helpshift.common.e.a.h;
import com.helpshift.common.e.a.j;
import com.helpshift.common.e.a.l;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: AndroidHTTPTransport */
public class k implements b {
    public final j a(h hVar) {
        if (hVar instanceof l) {
            return a((l) hVar);
        }
        return b(hVar);
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:1:0x000b */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:99:0x01c9 */
    /* JADX INFO: additional move instructions added (4) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:69:0x0152 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:38:0x009f */
    /* JADX WARN: Type inference failed for: r8v11 */
    /* JADX WARN: Type inference failed for: r8v12 */
    /* JADX WARN: Type inference failed for: r8v13, types: [java.io.Closeable] */
    /* JADX WARN: Type inference failed for: r8v16, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r8v18 */
    /* JADX WARN: Type inference failed for: r6v15, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r6v21 */
    /* JADX WARN: Type inference failed for: r6v24 */
    /* JADX WARN: Type inference failed for: r6v25 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x0223 A[Catch:{ Exception -> 0x022f }] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x022b A[Catch:{ Exception -> 0x022f }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d3 A[Catch:{ UnknownHostException -> 0x01bb, SecurityException | SocketException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0197 A[Catch:{ Exception -> 0x01a3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x019f A[Catch:{ Exception -> 0x01a3 }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.helpshift.common.e.a.j b(com.helpshift.common.e.a.h r17) {
        /*
            r16 = this;
            r1 = r17
            java.lang.String r2 = "Error in finally closing resources"
            java.lang.String r3 = "Helpshift_HTTPTrnsport"
            java.lang.String r4 = "Network error"
            r5 = 0
            java.lang.String r6 = "https://"
            java.lang.String r7 = com.helpshift.common.c.b.n.f3268a     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            boolean r6 = r6.equals(r7)     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            if (r6 == 0) goto L_0x0042
            java.net.URL r6 = new java.net.URL     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            java.lang.String r7 = r1.c     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            r6.<init>(r7)     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            java.net.URLConnection r6 = r6.openConnection()     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            javax.net.ssl.HttpsURLConnection r6 = (javax.net.ssl.HttpsURLConnection) r6     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            r7 = r6
            javax.net.ssl.HttpsURLConnection r7 = (javax.net.ssl.HttpsURLConnection) r7     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            a(r7)     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            goto L_0x004f
        L_0x0027:
            r0 = move-exception
            r1 = r0
            r8 = r5
            goto L_0x0219
        L_0x002c:
            r0 = move-exception
            r7 = r5
            goto L_0x01c7
        L_0x0030:
            r0 = move-exception
            r7 = r5
            goto L_0x01d7
        L_0x0034:
            r0 = move-exception
            r7 = r5
            goto L_0x01e7
        L_0x0038:
            r0 = move-exception
            goto L_0x003b
        L_0x003a:
            r0 = move-exception
        L_0x003b:
            r7 = r5
            goto L_0x01f9
        L_0x003e:
            r0 = move-exception
            r7 = r5
            goto L_0x0209
        L_0x0042:
            java.net.URL r6 = new java.net.URL     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            java.lang.String r7 = r1.c     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            r6.<init>(r7)     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            java.net.URLConnection r6 = r6.openConnection()     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
            java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ UnknownHostException -> 0x0206, SocketException -> 0x01f6, SecurityException -> 0x01f4, SSLPeerUnverifiedException -> 0x01e4, SSLHandshakeException -> 0x01d4, IOException -> 0x01c4, all -> 0x01be }
        L_0x004f:
            com.helpshift.common.e.a.d r7 = r1.f3319b     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            java.lang.String r7 = r7.name()     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            r6.setRequestMethod(r7)     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            int r7 = r1.e     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            r6.setConnectTimeout(r7)     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            java.util.List<com.helpshift.common.e.a.c> r7 = r1.d     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
        L_0x0063:
            boolean r8 = r7.hasNext()     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            if (r8 == 0) goto L_0x0077
            java.lang.Object r8 = r7.next()     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            com.helpshift.common.e.a.c r8 = (com.helpshift.common.e.a.c) r8     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            java.lang.String r9 = r8.f3313a     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            java.lang.String r8 = r8.f3314b     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            r6.setRequestProperty(r9, r8)     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            goto L_0x0063
        L_0x0077:
            com.helpshift.common.e.a.d r7 = r1.f3319b     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            com.helpshift.common.e.a.d r8 = com.helpshift.common.e.a.d.POST     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            if (r7 == r8) goto L_0x0086
            com.helpshift.common.e.a.d r7 = r1.f3319b     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            com.helpshift.common.e.a.d r8 = com.helpshift.common.e.a.d.PUT     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            if (r7 != r8) goto L_0x0084
            goto L_0x0086
        L_0x0084:
            r8 = r5
            goto L_0x00b7
        L_0x0086:
            com.helpshift.common.e.a.d r7 = r1.f3319b     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            com.helpshift.common.e.a.d r8 = com.helpshift.common.e.a.d.PUT     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            if (r7 != r8) goto L_0x0092
            r7 = r1
            com.helpshift.common.e.a.g r7 = (com.helpshift.common.e.a.g) r7     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            java.lang.String r7 = r7.f3318a     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            goto L_0x0097
        L_0x0092:
            r7 = r1
            com.helpshift.common.e.a.f r7 = (com.helpshift.common.e.a.f) r7     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            java.lang.String r7 = r7.f3317a     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
        L_0x0097:
            r8 = 1
            r6.setDoOutput(r8)     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            java.io.OutputStream r8 = r6.getOutputStream()     // Catch:{ UnknownHostException -> 0x003e, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0034, SSLHandshakeException -> 0x0030, IOException -> 0x002c, all -> 0x0027 }
            java.io.BufferedWriter r9 = new java.io.BufferedWriter     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.io.OutputStreamWriter r10 = new java.io.OutputStreamWriter     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r11 = "UTF-8"
            r10.<init>(r8, r11)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r9.<init>(r10)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r9.write(r7)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r9.flush()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r9.close()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r8.flush()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
        L_0x00b7:
            int r7 = r6.getResponseCode()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r9.<init>()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.util.Map r10 = r6.getHeaderFields()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.util.Set r11 = r10.keySet()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.util.Iterator r11 = r11.iterator()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
        L_0x00cc:
            boolean r12 = r11.hasNext()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r13 = 0
            if (r12 == 0) goto L_0x00f4
            java.lang.Object r12 = r11.next()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            boolean r14 = com.helpshift.common.k.a(r12)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            if (r14 != 0) goto L_0x00cc
            com.helpshift.common.e.a.c r14 = new com.helpshift.common.e.a.c     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.Object r15 = r10.get(r12)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.util.List r15 = (java.util.List) r15     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.Object r13 = r15.get(r13)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r14.<init>(r12, r13)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r9.add(r14)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            goto L_0x00cc
        L_0x00f4:
            r11 = 200(0xc8, float:2.8E-43)
            if (r7 < r11) goto L_0x0152
            r11 = 300(0x12c, float:4.2E-43)
            if (r7 >= r11) goto L_0x0152
            java.io.BufferedInputStream r11 = new java.io.BufferedInputStream     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.io.InputStream r12 = r6.getInputStream()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r11.<init>(r12)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r12 = "Content-Encoding"
            java.lang.Object r10 = r10.get(r12)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.util.List r10 = (java.util.List) r10     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            if (r10 == 0) goto L_0x0129
            int r12 = r10.size()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            if (r12 <= 0) goto L_0x0129
            java.lang.Object r10 = r10.get(r13)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r12 = "gzip"
            boolean r10 = r10.equals(r12)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            if (r10 == 0) goto L_0x0129
            java.util.zip.GZIPInputStream r10 = new java.util.zip.GZIPInputStream     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r10.<init>(r11)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            goto L_0x012a
        L_0x0129:
            r10 = r11
        L_0x012a:
            java.lang.String r11 = a(r10)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r10.close()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            com.helpshift.common.e.a.j r10 = new com.helpshift.common.e.a.j     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r10.<init>(r7, r11, r9)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            com.helpshift.util.r.a(r5)
            com.helpshift.util.r.a(r8)
            boolean r1 = r6 instanceof javax.net.ssl.HttpsURLConnection     // Catch:{ Exception -> 0x014c }
            if (r1 == 0) goto L_0x0146
            r1 = r6
            javax.net.ssl.HttpsURLConnection r1 = (javax.net.ssl.HttpsURLConnection) r1     // Catch:{ Exception -> 0x014c }
            b(r1)     // Catch:{ Exception -> 0x014c }
        L_0x0146:
            if (r6 == 0) goto L_0x0151
            r6.disconnect()     // Catch:{ Exception -> 0x014c }
            goto L_0x0151
        L_0x014c:
            r0 = move-exception
            r1 = r0
            com.helpshift.util.n.c(r3, r2, r1)
        L_0x0151:
            return r10
        L_0x0152:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r10.<init>()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r11 = "Api : "
            r10.append(r11)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r11 = r1.c     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r10.append(r11)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r11 = " \t Status : "
            r10.append(r11)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r10.append(r7)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r11 = "\t Thread : "
            r10.append(r11)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.Thread r11 = java.lang.Thread.currentThread()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r11 = r11.getName()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r10.append(r11)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r10 = r10.toString()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            com.helpshift.util.n.a(r3, r10)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.io.InputStream r5 = r6.getErrorStream()     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            java.lang.String r10 = a(r5)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            com.helpshift.common.e.a.j r11 = new com.helpshift.common.e.a.j     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            r11.<init>(r7, r10, r9)     // Catch:{ UnknownHostException -> 0x01bb, SocketException -> 0x01b8, SecurityException -> 0x01b6, SSLPeerUnverifiedException -> 0x01b3, SSLHandshakeException -> 0x01b0, IOException -> 0x01ad, all -> 0x01a9 }
            com.helpshift.util.r.a(r5)
            com.helpshift.util.r.a(r8)
            boolean r1 = r6 instanceof javax.net.ssl.HttpsURLConnection     // Catch:{ Exception -> 0x01a3 }
            if (r1 == 0) goto L_0x019d
            r1 = r6
            javax.net.ssl.HttpsURLConnection r1 = (javax.net.ssl.HttpsURLConnection) r1     // Catch:{ Exception -> 0x01a3 }
            b(r1)     // Catch:{ Exception -> 0x01a3 }
        L_0x019d:
            if (r6 == 0) goto L_0x01a8
            r6.disconnect()     // Catch:{ Exception -> 0x01a3 }
            goto L_0x01a8
        L_0x01a3:
            r0 = move-exception
            r1 = r0
            com.helpshift.util.n.c(r3, r2, r1)
        L_0x01a8:
            return r11
        L_0x01a9:
            r0 = move-exception
            r1 = r0
            goto L_0x0219
        L_0x01ad:
            r0 = move-exception
            r7 = r5
            goto L_0x01c8
        L_0x01b0:
            r0 = move-exception
            r7 = r5
            goto L_0x01d8
        L_0x01b3:
            r0 = move-exception
            r7 = r5
            goto L_0x01e8
        L_0x01b6:
            r0 = move-exception
            goto L_0x01b9
        L_0x01b8:
            r0 = move-exception
        L_0x01b9:
            r7 = r5
            goto L_0x01fa
        L_0x01bb:
            r0 = move-exception
            r7 = r5
            goto L_0x020a
        L_0x01be:
            r0 = move-exception
            r1 = r0
            r6 = r5
            r8 = r6
            goto L_0x0219
        L_0x01c4:
            r0 = move-exception
            r6 = r5
            r7 = r6
        L_0x01c7:
            r8 = r7
        L_0x01c8:
            r5 = r0
            com.helpshift.common.exception.b r9 = com.helpshift.common.exception.b.GENERIC     // Catch:{ all -> 0x0216 }
            java.lang.String r1 = r1.c     // Catch:{ all -> 0x0216 }
            r9.v = r1     // Catch:{ all -> 0x0216 }
            com.helpshift.common.exception.RootAPIException r1 = com.helpshift.common.exception.RootAPIException.a(r5, r9, r4)     // Catch:{ all -> 0x0216 }
            throw r1     // Catch:{ all -> 0x0216 }
        L_0x01d4:
            r0 = move-exception
            r6 = r5
            r7 = r6
        L_0x01d7:
            r8 = r7
        L_0x01d8:
            r5 = r0
            com.helpshift.common.exception.b r9 = com.helpshift.common.exception.b.SSL_HANDSHAKE     // Catch:{ all -> 0x0216 }
            java.lang.String r1 = r1.c     // Catch:{ all -> 0x0216 }
            r9.v = r1     // Catch:{ all -> 0x0216 }
            com.helpshift.common.exception.RootAPIException r1 = com.helpshift.common.exception.RootAPIException.a(r5, r9, r4)     // Catch:{ all -> 0x0216 }
            throw r1     // Catch:{ all -> 0x0216 }
        L_0x01e4:
            r0 = move-exception
            r6 = r5
            r7 = r6
        L_0x01e7:
            r8 = r7
        L_0x01e8:
            r5 = r0
            com.helpshift.common.exception.b r9 = com.helpshift.common.exception.b.SSL_PEER_UNVERIFIED     // Catch:{ all -> 0x0216 }
            java.lang.String r1 = r1.c     // Catch:{ all -> 0x0216 }
            r9.v = r1     // Catch:{ all -> 0x0216 }
            com.helpshift.common.exception.RootAPIException r1 = com.helpshift.common.exception.RootAPIException.a(r5, r9, r4)     // Catch:{ all -> 0x0216 }
            throw r1     // Catch:{ all -> 0x0216 }
        L_0x01f4:
            r0 = move-exception
            goto L_0x01f7
        L_0x01f6:
            r0 = move-exception
        L_0x01f7:
            r6 = r5
            r7 = r6
        L_0x01f9:
            r8 = r7
        L_0x01fa:
            r5 = r0
            com.helpshift.common.exception.b r9 = com.helpshift.common.exception.b.NO_CONNECTION     // Catch:{ all -> 0x0216 }
            java.lang.String r1 = r1.c     // Catch:{ all -> 0x0216 }
            r9.v = r1     // Catch:{ all -> 0x0216 }
            com.helpshift.common.exception.RootAPIException r1 = com.helpshift.common.exception.RootAPIException.a(r5, r9, r4)     // Catch:{ all -> 0x0216 }
            throw r1     // Catch:{ all -> 0x0216 }
        L_0x0206:
            r0 = move-exception
            r6 = r5
            r7 = r6
        L_0x0209:
            r8 = r7
        L_0x020a:
            r5 = r0
            com.helpshift.common.exception.b r9 = com.helpshift.common.exception.b.UNKNOWN_HOST     // Catch:{ all -> 0x0216 }
            java.lang.String r1 = r1.c     // Catch:{ all -> 0x0216 }
            r9.v = r1     // Catch:{ all -> 0x0216 }
            com.helpshift.common.exception.RootAPIException r1 = com.helpshift.common.exception.RootAPIException.a(r5, r9, r4)     // Catch:{ all -> 0x0216 }
            throw r1     // Catch:{ all -> 0x0216 }
        L_0x0216:
            r0 = move-exception
            r1 = r0
            r5 = r7
        L_0x0219:
            com.helpshift.util.r.a(r5)
            com.helpshift.util.r.a(r8)
            boolean r4 = r6 instanceof javax.net.ssl.HttpsURLConnection     // Catch:{ Exception -> 0x022f }
            if (r4 == 0) goto L_0x0229
            r4 = r6
            javax.net.ssl.HttpsURLConnection r4 = (javax.net.ssl.HttpsURLConnection) r4     // Catch:{ Exception -> 0x022f }
            b(r4)     // Catch:{ Exception -> 0x022f }
        L_0x0229:
            if (r6 == 0) goto L_0x0234
            r6.disconnect()     // Catch:{ Exception -> 0x022f }
            goto L_0x0234
        L_0x022f:
            r0 = move-exception
            r4 = r0
            com.helpshift.util.n.c(r3, r2, r4)
        L_0x0234:
            goto L_0x0236
        L_0x0235:
            throw r1
        L_0x0236:
            goto L_0x0235
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.e.k.b(com.helpshift.common.e.a.h):com.helpshift.common.e.a.j");
    }

    private static String a(InputStream inputStream) throws IOException {
        if (inputStream == null) {
            return null;
        }
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder sb = new StringBuilder();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                sb.append(readLine);
            } else {
                inputStreamReader.close();
                return sb.toString();
            }
        }
    }

    private static void a(HttpsURLConnection httpsURLConnection) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT <= 19) {
            ArrayList arrayList = new ArrayList();
            arrayList.add("TLSv1.2");
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add("SSLv3");
            httpsURLConnection.setSSLSocketFactory(new f(httpsURLConnection.getSSLSocketFactory(), arrayList, arrayList2));
        }
    }

    private static void b(HttpsURLConnection httpsURLConnection) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT <= 19 && httpsURLConnection != null) {
            SSLSocketFactory sSLSocketFactory = httpsURLConnection.getSSLSocketFactory();
            if (sSLSocketFactory instanceof f) {
                ((f) sSLSocketFactory).a();
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (6) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v0, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v1, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v11, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v28, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v29, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v33, resolved type: java.io.FileInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v38, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v39, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v51, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v52, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v53, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v55, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v56, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v57, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v58, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v59, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v60, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v61, resolved type: java.io.DataOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v62, resolved type: java.io.DataOutputStream} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x03da A[SYNTHETIC, Splitter:B:227:0x03da] */
    /* JADX WARNING: Removed duplicated region for block: B:233:0x017f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0106 A[Catch:{ UnknownHostException -> 0x01a5, SecurityException | SocketException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.helpshift.common.e.a.j a(com.helpshift.common.e.a.l r19) {
        /*
            r18 = this;
            r1 = r19
            java.lang.String r2 = "Error in finally closing resources"
            java.lang.String r3 = "Helpshift_HTTPTrnsport"
            java.lang.String r4 = "Upload error"
            java.net.URL r6 = new java.net.URL     // Catch:{ UnknownHostException -> 0x03bc, SocketException -> 0x03ab, SecurityException -> 0x03a9, SSLPeerUnverifiedException -> 0x0398, SSLHandshakeException -> 0x0387, Exception -> 0x0376, all -> 0x036e }
            java.lang.String r7 = r1.c     // Catch:{ UnknownHostException -> 0x03bc, SocketException -> 0x03ab, SecurityException -> 0x03a9, SSLPeerUnverifiedException -> 0x0398, SSLHandshakeException -> 0x0387, Exception -> 0x0376, all -> 0x036e }
            r6.<init>(r7)     // Catch:{ UnknownHostException -> 0x03bc, SocketException -> 0x03ab, SecurityException -> 0x03a9, SSLPeerUnverifiedException -> 0x0398, SSLHandshakeException -> 0x0387, Exception -> 0x0376, all -> 0x036e }
            java.lang.String r7 = "--"
            java.lang.String r8 = "*****"
            java.lang.String r9 = "\r\n"
            java.lang.String r10 = "https://"
            java.lang.String r11 = com.helpshift.common.c.b.n.f3268a     // Catch:{ UnknownHostException -> 0x03bc, SocketException -> 0x03ab, SecurityException -> 0x03a9, SSLPeerUnverifiedException -> 0x0398, SSLHandshakeException -> 0x0387, Exception -> 0x0376, all -> 0x036e }
            boolean r10 = r10.equals(r11)     // Catch:{ UnknownHostException -> 0x03bc, SocketException -> 0x03ab, SecurityException -> 0x03a9, SSLPeerUnverifiedException -> 0x0398, SSLHandshakeException -> 0x0387, Exception -> 0x0376, all -> 0x036e }
            if (r10 == 0) goto L_0x0072
            java.net.URLConnection r6 = r6.openConnection()     // Catch:{ UnknownHostException -> 0x006a, SocketException -> 0x0062, SecurityException -> 0x0060, SSLPeerUnverifiedException -> 0x0058, SSLHandshakeException -> 0x0050, Exception -> 0x0048, all -> 0x0040 }
            javax.net.ssl.HttpsURLConnection r6 = (javax.net.ssl.HttpsURLConnection) r6     // Catch:{ UnknownHostException -> 0x006a, SocketException -> 0x0062, SecurityException -> 0x0060, SSLPeerUnverifiedException -> 0x0058, SSLHandshakeException -> 0x0050, Exception -> 0x0048, all -> 0x0040 }
            r10 = r6
            javax.net.ssl.HttpsURLConnection r10 = (javax.net.ssl.HttpsURLConnection) r10     // Catch:{ UnknownHostException -> 0x003d, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0035, SSLHandshakeException -> 0x0032, Exception -> 0x002f, all -> 0x002c }
            a(r10)     // Catch:{ UnknownHostException -> 0x003d, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0035, SSLHandshakeException -> 0x0032, Exception -> 0x002f, all -> 0x002c }
            goto L_0x0078
        L_0x002c:
            r0 = move-exception
            r1 = r0
            goto L_0x0043
        L_0x002f:
            r0 = move-exception
            r5 = r0
            goto L_0x004b
        L_0x0032:
            r0 = move-exception
            r5 = r0
            goto L_0x0053
        L_0x0035:
            r0 = move-exception
            r5 = r0
            goto L_0x005b
        L_0x0038:
            r0 = move-exception
            goto L_0x003b
        L_0x003a:
            r0 = move-exception
        L_0x003b:
            r5 = r0
            goto L_0x0065
        L_0x003d:
            r0 = move-exception
            r5 = r0
            goto L_0x006d
        L_0x0040:
            r0 = move-exception
            r1 = r0
            r6 = 0
        L_0x0043:
            r8 = 0
            r11 = 0
            r12 = 0
            goto L_0x03cf
        L_0x0048:
            r0 = move-exception
            r5 = r0
            r6 = 0
        L_0x004b:
            r8 = 0
            r11 = 0
            r12 = 0
            goto L_0x037c
        L_0x0050:
            r0 = move-exception
            r5 = r0
            r6 = 0
        L_0x0053:
            r8 = 0
            r11 = 0
            r12 = 0
            goto L_0x038d
        L_0x0058:
            r0 = move-exception
            r5 = r0
            r6 = 0
        L_0x005b:
            r8 = 0
            r11 = 0
            r12 = 0
            goto L_0x039e
        L_0x0060:
            r0 = move-exception
            goto L_0x0063
        L_0x0062:
            r0 = move-exception
        L_0x0063:
            r5 = r0
            r6 = 0
        L_0x0065:
            r8 = 0
            r11 = 0
            r12 = 0
            goto L_0x03b1
        L_0x006a:
            r0 = move-exception
            r5 = r0
            r6 = 0
        L_0x006d:
            r8 = 0
            r11 = 0
            r12 = 0
            goto L_0x03c2
        L_0x0072:
            java.net.URLConnection r6 = r6.openConnection()     // Catch:{ UnknownHostException -> 0x03bc, SocketException -> 0x03ab, SecurityException -> 0x03a9, SSLPeerUnverifiedException -> 0x0398, SSLHandshakeException -> 0x0387, Exception -> 0x0376, all -> 0x036e }
            java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ UnknownHostException -> 0x03bc, SocketException -> 0x03ab, SecurityException -> 0x03a9, SSLPeerUnverifiedException -> 0x0398, SSLHandshakeException -> 0x0387, Exception -> 0x0376, all -> 0x036e }
        L_0x0078:
            r10 = 1
            r6.setDoInput(r10)     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            r6.setDoOutput(r10)     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            r11 = 0
            r6.setUseCaches(r11)     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            com.helpshift.common.e.a.d r12 = r1.f3319b     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            java.lang.String r12 = r12.name()     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            r6.setRequestMethod(r12)     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            int r12 = r1.e     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            r6.setConnectTimeout(r12)     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            int r12 = r1.e     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            r6.setReadTimeout(r12)     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            java.util.List r12 = r1.d     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            java.util.Iterator r12 = r12.iterator()     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
        L_0x009c:
            boolean r13 = r12.hasNext()     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            if (r13 == 0) goto L_0x00b0
            java.lang.Object r13 = r12.next()     // Catch:{ UnknownHostException -> 0x003d, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0035, SSLHandshakeException -> 0x0032, Exception -> 0x002f, all -> 0x002c }
            com.helpshift.common.e.a.c r13 = (com.helpshift.common.e.a.c) r13     // Catch:{ UnknownHostException -> 0x003d, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0035, SSLHandshakeException -> 0x0032, Exception -> 0x002f, all -> 0x002c }
            java.lang.String r14 = r13.f3313a     // Catch:{ UnknownHostException -> 0x003d, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0035, SSLHandshakeException -> 0x0032, Exception -> 0x002f, all -> 0x002c }
            java.lang.String r13 = r13.f3314b     // Catch:{ UnknownHostException -> 0x003d, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0035, SSLHandshakeException -> 0x0032, Exception -> 0x002f, all -> 0x002c }
            r6.setRequestProperty(r14, r13)     // Catch:{ UnknownHostException -> 0x003d, SocketException -> 0x003a, SecurityException -> 0x0038, SSLPeerUnverifiedException -> 0x0035, SSLHandshakeException -> 0x0032, Exception -> 0x002f, all -> 0x002c }
            goto L_0x009c
        L_0x00b0:
            java.io.DataOutputStream r12 = new java.io.DataOutputStream     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            java.io.OutputStream r13 = r6.getOutputStream()     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            r12.<init>(r13)     // Catch:{ UnknownHostException -> 0x0368, SocketException -> 0x0363, SecurityException -> 0x0361, SSLPeerUnverifiedException -> 0x035c, SSLHandshakeException -> 0x0357, Exception -> 0x0352, all -> 0x034d }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            r13.<init>()     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            r13.append(r7)     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            r13.append(r8)     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            r13.append(r9)     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            java.lang.String r13 = r13.toString()     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            r12.writeBytes(r13)     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            java.util.Map<java.lang.String, java.lang.String> r13 = r1.f3324a     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            java.util.Set r14 = r13.entrySet()     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            java.util.Iterator r14 = r14.iterator()     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
        L_0x00d8:
            boolean r15 = r14.hasNext()     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            java.lang.String r10 = "Content-Length: "
            java.lang.String r5 = "originalFileName"
            java.lang.String r11 = "screenshot"
            if (r15 == 0) goto L_0x01ab
            java.lang.Object r15 = r14.next()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.util.Map$Entry r15 = (java.util.Map.Entry) r15     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.Object r16 = r15.getKey()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r17 = r14
            r14 = r16
            java.lang.String r14 = (java.lang.String) r14     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            boolean r11 = r11.equals(r14)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            if (r11 != 0) goto L_0x0103
            boolean r5 = r5.equals(r14)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            if (r5 == 0) goto L_0x0101
            goto L_0x0103
        L_0x0101:
            r5 = 0
            goto L_0x0104
        L_0x0103:
            r5 = 1
        L_0x0104:
            if (r5 != 0) goto L_0x017f
            java.lang.Object r5 = r15.getValue()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r11.<init>()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.String r15 = "Content-Disposition: form-data; name=\""
            r11.append(r15)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r11.append(r14)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.String r14 = "\"; "
            r11.append(r14)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r11.append(r9)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.String r11 = r11.toString()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r12.writeBytes(r11)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r11.<init>()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.String r14 = "Content-Type: text/plain;charset=UTF-8"
            r11.append(r14)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r11.append(r9)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.String r11 = r11.toString()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r12.writeBytes(r11)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r11.<init>()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r11.append(r10)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            int r10 = r5.length()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r11.append(r10)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r11.append(r9)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.String r10 = r11.toString()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r12.writeBytes(r10)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r12.writeBytes(r9)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r10.<init>()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r10.append(r5)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r10.append(r9)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.String r5 = r10.toString()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r12.writeBytes(r5)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r5.<init>()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r5.append(r7)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r5.append(r8)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r5.append(r9)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            java.lang.String r5 = r5.toString()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
            r12.writeBytes(r5)     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
        L_0x017f:
            r14 = r17
            r10 = 1
            r11 = 0
            goto L_0x00d8
        L_0x0185:
            r0 = move-exception
            r1 = r0
            r8 = 0
            r11 = 0
            goto L_0x03cf
        L_0x018b:
            r0 = move-exception
            r5 = r0
            r8 = 0
            r11 = 0
            goto L_0x037c
        L_0x0191:
            r0 = move-exception
            r5 = r0
            r8 = 0
            r11 = 0
            goto L_0x038d
        L_0x0197:
            r0 = move-exception
            r5 = r0
            r8 = 0
            r11 = 0
            goto L_0x039e
        L_0x019d:
            r0 = move-exception
            goto L_0x01a0
        L_0x019f:
            r0 = move-exception
        L_0x01a0:
            r5 = r0
            r8 = 0
            r11 = 0
            goto L_0x03b1
        L_0x01a5:
            r0 = move-exception
            r5 = r0
            r8 = 0
            r11 = 0
            goto L_0x03c2
        L_0x01ab:
            java.io.File r14 = new java.io.File     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            java.lang.Object r11 = r13.get(r11)     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            r14.<init>(r11)     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            java.lang.Object r5 = r13.get(r5)     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            if (r5 != 0) goto L_0x01c2
            java.lang.String r5 = r14.getName()     // Catch:{ UnknownHostException -> 0x01a5, SocketException -> 0x019f, SecurityException -> 0x019d, SSLPeerUnverifiedException -> 0x0197, SSLHandshakeException -> 0x0191, Exception -> 0x018b, all -> 0x0185 }
        L_0x01c2:
            java.io.FileInputStream r11 = new java.io.FileInputStream     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            r11.<init>(r14)     // Catch:{ UnknownHostException -> 0x0347, SocketException -> 0x0341, SecurityException -> 0x033f, SSLPeerUnverifiedException -> 0x0339, SSLHandshakeException -> 0x0333, Exception -> 0x032d, all -> 0x0327 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r13.<init>()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r13.append(r7)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r13.append(r8)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r13.append(r9)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.String r13 = r13.toString()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r12.writeBytes(r13)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r13.<init>()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.String r15 = "Content-Disposition: form-data; name=\"screenshot\"; filename=\""
            r13.append(r15)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r13.append(r5)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.String r5 = "\""
            r13.append(r5)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r13.append(r9)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.String r5 = r13.toString()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r12.writeBytes(r5)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.<init>()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.String r13 = "Content-Type: "
            r5.append(r13)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.String r13 = r1.f     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.append(r13)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.append(r9)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.String r5 = r5.toString()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r12.writeBytes(r5)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.<init>()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.append(r10)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            long r13 = r14.length()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.append(r13)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.append(r9)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.String r5 = r5.toString()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r12.writeBytes(r5)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r12.writeBytes(r9)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            int r5 = r11.available()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r10 = 1048576(0x100000, float:1.469368E-39)
            int r5 = java.lang.Math.min(r5, r10)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            byte[] r13 = new byte[r5]     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r14 = 0
            int r15 = r11.read(r13, r14, r5)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
        L_0x023e:
            if (r15 <= 0) goto L_0x0270
            r12.write(r13, r14, r5)     // Catch:{ UnknownHostException -> 0x026b, SocketException -> 0x0266, SecurityException -> 0x0264, SSLPeerUnverifiedException -> 0x025f, SSLHandshakeException -> 0x025a, Exception -> 0x0255, all -> 0x0250 }
            int r5 = r11.available()     // Catch:{ UnknownHostException -> 0x026b, SocketException -> 0x0266, SecurityException -> 0x0264, SSLPeerUnverifiedException -> 0x025f, SSLHandshakeException -> 0x025a, Exception -> 0x0255, all -> 0x0250 }
            int r5 = java.lang.Math.min(r5, r10)     // Catch:{ UnknownHostException -> 0x026b, SocketException -> 0x0266, SecurityException -> 0x0264, SSLPeerUnverifiedException -> 0x025f, SSLHandshakeException -> 0x025a, Exception -> 0x0255, all -> 0x0250 }
            int r15 = r11.read(r13, r14, r5)     // Catch:{ UnknownHostException -> 0x026b, SocketException -> 0x0266, SecurityException -> 0x0264, SSLPeerUnverifiedException -> 0x025f, SSLHandshakeException -> 0x025a, Exception -> 0x0255, all -> 0x0250 }
            goto L_0x023e
        L_0x0250:
            r0 = move-exception
            r1 = r0
            r8 = 0
            goto L_0x03cf
        L_0x0255:
            r0 = move-exception
            r5 = r0
            r8 = 0
            goto L_0x037c
        L_0x025a:
            r0 = move-exception
            r5 = r0
            r8 = 0
            goto L_0x038d
        L_0x025f:
            r0 = move-exception
            r5 = r0
            r8 = 0
            goto L_0x039e
        L_0x0264:
            r0 = move-exception
            goto L_0x0267
        L_0x0266:
            r0 = move-exception
        L_0x0267:
            r5 = r0
            r8 = 0
            goto L_0x03b1
        L_0x026b:
            r0 = move-exception
            r5 = r0
            r8 = 0
            goto L_0x03c2
        L_0x0270:
            r12.writeBytes(r9)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.<init>()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.append(r7)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.append(r8)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.append(r7)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r5.append(r9)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            java.lang.String r5 = r5.toString()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r12.writeBytes(r5)     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r12.flush()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            int r5 = r6.getResponseCode()     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r7 = 200(0xc8, float:2.8E-43)
            if (r5 < r7) goto L_0x02e1
            r7 = 300(0x12c, float:4.2E-43)
            if (r5 >= r7) goto L_0x02e1
            java.io.InputStream r7 = r6.getInputStream()     // Catch:{ UnknownHostException -> 0x026b, SocketException -> 0x0266, SecurityException -> 0x0264, SSLPeerUnverifiedException -> 0x025f, SSLHandshakeException -> 0x025a, Exception -> 0x0255, all -> 0x0250 }
            if (r7 == 0) goto L_0x02a5
            java.lang.String r8 = a(r7)     // Catch:{ UnknownHostException -> 0x02dc, SocketException -> 0x02d7, SecurityException -> 0x02d5, SSLPeerUnverifiedException -> 0x02d0, SSLHandshakeException -> 0x02cb, Exception -> 0x02c6, all -> 0x02c1 }
            goto L_0x02a6
        L_0x02a5:
            r8 = 0
        L_0x02a6:
            com.helpshift.common.e.a.j r9 = new com.helpshift.common.e.a.j     // Catch:{ UnknownHostException -> 0x02dc, SocketException -> 0x02d7, SecurityException -> 0x02d5, SSLPeerUnverifiedException -> 0x02d0, SSLHandshakeException -> 0x02cb, Exception -> 0x02c6, all -> 0x02c1 }
            r10 = 0
            r9.<init>(r5, r8, r10)     // Catch:{ UnknownHostException -> 0x02dc, SocketException -> 0x02d7, SecurityException -> 0x02d5, SSLPeerUnverifiedException -> 0x02d0, SSLHandshakeException -> 0x02cb, Exception -> 0x02c6, all -> 0x02c1 }
            com.helpshift.util.r.a(r11)
            com.helpshift.util.r.a(r12)
            com.helpshift.util.r.a(r7)
            if (r6 == 0) goto L_0x02c0
            r6.disconnect()     // Catch:{ Exception -> 0x02bb }
            goto L_0x02c0
        L_0x02bb:
            r0 = move-exception
            r1 = r0
            com.helpshift.util.n.c(r3, r2, r1)
        L_0x02c0:
            return r9
        L_0x02c1:
            r0 = move-exception
            r1 = r0
            r8 = r7
            goto L_0x03cf
        L_0x02c6:
            r0 = move-exception
            r5 = r0
            r8 = r7
            goto L_0x037c
        L_0x02cb:
            r0 = move-exception
            r5 = r0
            r8 = r7
            goto L_0x038d
        L_0x02d0:
            r0 = move-exception
            r5 = r0
            r8 = r7
            goto L_0x039e
        L_0x02d5:
            r0 = move-exception
            goto L_0x02d8
        L_0x02d7:
            r0 = move-exception
        L_0x02d8:
            r5 = r0
            r8 = r7
            goto L_0x03b1
        L_0x02dc:
            r0 = move-exception
            r5 = r0
            r8 = r7
            goto L_0x03c2
        L_0x02e1:
            com.helpshift.common.e.a.j r7 = new com.helpshift.common.e.a.j     // Catch:{ UnknownHostException -> 0x0322, SocketException -> 0x031d, SecurityException -> 0x031b, SSLPeerUnverifiedException -> 0x0316, SSLHandshakeException -> 0x0311, Exception -> 0x030c, all -> 0x0308 }
            r8 = 0
            r7.<init>(r5, r8, r8)     // Catch:{ UnknownHostException -> 0x0306, SocketException -> 0x0304, SecurityException -> 0x0302, SSLPeerUnverifiedException -> 0x0300, SSLHandshakeException -> 0x02fe, Exception -> 0x02fc }
            com.helpshift.util.r.a(r11)
            com.helpshift.util.r.a(r12)
            com.helpshift.util.r.a(r8)
            if (r6 == 0) goto L_0x02fb
            r6.disconnect()     // Catch:{ Exception -> 0x02f6 }
            goto L_0x02fb
        L_0x02f6:
            r0 = move-exception
            r1 = r0
            com.helpshift.util.n.c(r3, r2, r1)
        L_0x02fb:
            return r7
        L_0x02fc:
            r0 = move-exception
            goto L_0x030e
        L_0x02fe:
            r0 = move-exception
            goto L_0x0313
        L_0x0300:
            r0 = move-exception
            goto L_0x0318
        L_0x0302:
            r0 = move-exception
            goto L_0x031f
        L_0x0304:
            r0 = move-exception
            goto L_0x031f
        L_0x0306:
            r0 = move-exception
            goto L_0x0324
        L_0x0308:
            r0 = move-exception
            r8 = 0
            goto L_0x03ce
        L_0x030c:
            r0 = move-exception
            r8 = 0
        L_0x030e:
            r5 = r0
            goto L_0x037c
        L_0x0311:
            r0 = move-exception
            r8 = 0
        L_0x0313:
            r5 = r0
            goto L_0x038d
        L_0x0316:
            r0 = move-exception
            r8 = 0
        L_0x0318:
            r5 = r0
            goto L_0x039e
        L_0x031b:
            r0 = move-exception
            goto L_0x031e
        L_0x031d:
            r0 = move-exception
        L_0x031e:
            r8 = 0
        L_0x031f:
            r5 = r0
            goto L_0x03b1
        L_0x0322:
            r0 = move-exception
            r8 = 0
        L_0x0324:
            r5 = r0
            goto L_0x03c2
        L_0x0327:
            r0 = move-exception
            r8 = 0
            r1 = r0
            r11 = r8
            goto L_0x03cf
        L_0x032d:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r11 = r8
            goto L_0x037c
        L_0x0333:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r11 = r8
            goto L_0x038d
        L_0x0339:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r11 = r8
            goto L_0x039e
        L_0x033f:
            r0 = move-exception
            goto L_0x0342
        L_0x0341:
            r0 = move-exception
        L_0x0342:
            r8 = 0
            r5 = r0
            r11 = r8
            goto L_0x03b1
        L_0x0347:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r11 = r8
            goto L_0x03c2
        L_0x034d:
            r0 = move-exception
            r8 = 0
            r1 = r0
            r11 = r8
            goto L_0x0373
        L_0x0352:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r11 = r8
            goto L_0x037b
        L_0x0357:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r11 = r8
            goto L_0x038c
        L_0x035c:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r11 = r8
            goto L_0x039d
        L_0x0361:
            r0 = move-exception
            goto L_0x0364
        L_0x0363:
            r0 = move-exception
        L_0x0364:
            r8 = 0
            r5 = r0
            r11 = r8
            goto L_0x03b0
        L_0x0368:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r11 = r8
            goto L_0x03c1
        L_0x036e:
            r0 = move-exception
            r8 = 0
            r1 = r0
            r6 = r8
            r11 = r6
        L_0x0373:
            r12 = r11
            goto L_0x03cf
        L_0x0376:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r6 = r8
            r11 = r6
        L_0x037b:
            r12 = r11
        L_0x037c:
            com.helpshift.common.exception.b r7 = com.helpshift.common.exception.b.GENERIC     // Catch:{ all -> 0x03cd }
            java.lang.String r1 = r1.c     // Catch:{ all -> 0x03cd }
            r7.v = r1     // Catch:{ all -> 0x03cd }
            com.helpshift.common.exception.RootAPIException r1 = com.helpshift.common.exception.RootAPIException.a(r5, r7, r4)     // Catch:{ all -> 0x03cd }
            throw r1     // Catch:{ all -> 0x03cd }
        L_0x0387:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r6 = r8
            r11 = r6
        L_0x038c:
            r12 = r11
        L_0x038d:
            com.helpshift.common.exception.b r7 = com.helpshift.common.exception.b.SSL_HANDSHAKE     // Catch:{ all -> 0x03cd }
            java.lang.String r1 = r1.c     // Catch:{ all -> 0x03cd }
            r7.v = r1     // Catch:{ all -> 0x03cd }
            com.helpshift.common.exception.RootAPIException r1 = com.helpshift.common.exception.RootAPIException.a(r5, r7, r4)     // Catch:{ all -> 0x03cd }
            throw r1     // Catch:{ all -> 0x03cd }
        L_0x0398:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r6 = r8
            r11 = r6
        L_0x039d:
            r12 = r11
        L_0x039e:
            com.helpshift.common.exception.b r7 = com.helpshift.common.exception.b.SSL_PEER_UNVERIFIED     // Catch:{ all -> 0x03cd }
            java.lang.String r1 = r1.c     // Catch:{ all -> 0x03cd }
            r7.v = r1     // Catch:{ all -> 0x03cd }
            com.helpshift.common.exception.RootAPIException r1 = com.helpshift.common.exception.RootAPIException.a(r5, r7, r4)     // Catch:{ all -> 0x03cd }
            throw r1     // Catch:{ all -> 0x03cd }
        L_0x03a9:
            r0 = move-exception
            goto L_0x03ac
        L_0x03ab:
            r0 = move-exception
        L_0x03ac:
            r8 = 0
            r5 = r0
            r6 = r8
            r11 = r6
        L_0x03b0:
            r12 = r11
        L_0x03b1:
            com.helpshift.common.exception.b r7 = com.helpshift.common.exception.b.NO_CONNECTION     // Catch:{ all -> 0x03cd }
            java.lang.String r1 = r1.c     // Catch:{ all -> 0x03cd }
            r7.v = r1     // Catch:{ all -> 0x03cd }
            com.helpshift.common.exception.RootAPIException r1 = com.helpshift.common.exception.RootAPIException.a(r5, r7, r4)     // Catch:{ all -> 0x03cd }
            throw r1     // Catch:{ all -> 0x03cd }
        L_0x03bc:
            r0 = move-exception
            r8 = 0
            r5 = r0
            r6 = r8
            r11 = r6
        L_0x03c1:
            r12 = r11
        L_0x03c2:
            com.helpshift.common.exception.b r7 = com.helpshift.common.exception.b.UNKNOWN_HOST     // Catch:{ all -> 0x03cd }
            java.lang.String r1 = r1.c     // Catch:{ all -> 0x03cd }
            r7.v = r1     // Catch:{ all -> 0x03cd }
            com.helpshift.common.exception.RootAPIException r1 = com.helpshift.common.exception.RootAPIException.a(r5, r7, r4)     // Catch:{ all -> 0x03cd }
            throw r1     // Catch:{ all -> 0x03cd }
        L_0x03cd:
            r0 = move-exception
        L_0x03ce:
            r1 = r0
        L_0x03cf:
            com.helpshift.util.r.a(r11)
            com.helpshift.util.r.a(r12)
            com.helpshift.util.r.a(r8)
            if (r6 == 0) goto L_0x03e3
            r6.disconnect()     // Catch:{ Exception -> 0x03de }
            goto L_0x03e3
        L_0x03de:
            r0 = move-exception
            r4 = r0
            com.helpshift.util.n.c(r3, r2, r4)
        L_0x03e3:
            goto L_0x03e5
        L_0x03e4:
            throw r1
        L_0x03e5:
            goto L_0x03e4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.e.k.a(com.helpshift.common.e.a.l):com.helpshift.common.e.a.j");
    }
}
