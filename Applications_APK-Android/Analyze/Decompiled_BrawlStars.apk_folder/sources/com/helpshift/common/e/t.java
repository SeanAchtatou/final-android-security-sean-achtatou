package com.helpshift.common.e;

import android.content.Context;
import com.helpshift.c.a.a.b;
import com.helpshift.common.c.m;
import com.helpshift.m.c;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: AndroidSupportDownloader */
public class t implements c {

    /* renamed from: a  reason: collision with root package name */
    private static final TimeUnit f3347a = TimeUnit.SECONDS;

    /* renamed from: b  reason: collision with root package name */
    private final b f3348b;
    private Context c;
    private Map<String, Set<com.helpshift.m.b>> d = new HashMap();

    public t(Context context, aa aaVar) {
        this.c = context;
        this.f3348b = new b(context, new ac(aaVar), new ThreadPoolExecutor(5, 5, 1, f3347a, new LinkedBlockingQueue(), new m("sp-dwnld")));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r13v5, types: [java.lang.Runnable] */
    /* JADX WARN: Type inference failed for: r13v7 */
    /* JADX WARN: Type inference failed for: r1v7, types: [com.helpshift.c.a.a.b.d] */
    /* JADX WARN: Type inference failed for: r1v8, types: [com.helpshift.c.a.a.b.e] */
    /* JADX WARN: Type inference failed for: r1v9, types: [com.helpshift.c.a.a.b.d] */
    /* JADX WARN: Type inference failed for: r1v10, types: [com.helpshift.c.a.a.b.c] */
    /* JADX WARN: Type inference failed for: r13v16 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.helpshift.m.a r12, com.helpshift.m.c.a r13, com.helpshift.common.c.b.a r14, com.helpshift.m.b r15) {
        /*
            r11 = this;
            java.lang.String r0 = r12.f3756a
            r11.a(r0, r15)
            com.helpshift.c.a.a.a.b r3 = new com.helpshift.c.a.a.a.b
            java.lang.String r15 = r12.f3756a
            boolean r0 = r12.d
            java.lang.String r12 = r12.c
            r3.<init>(r15, r0, r12)
            com.helpshift.c.a.a.b r12 = r11.f3348b
            int[] r15 = com.helpshift.common.e.x.f3353a
            int r13 = r13.ordinal()
            r13 = r15[r13]
            java.lang.String r15 = "Unsupported download Dir type"
            r0 = 3
            r1 = 2
            r2 = 1
            r4 = 0
            if (r13 == r2) goto L_0x0032
            if (r13 == r1) goto L_0x002f
            if (r13 != r0) goto L_0x0029
            com.helpshift.c.a.a.a.a r13 = com.helpshift.c.a.a.a.a.EXTERNAL_OR_INTERNAL
            goto L_0x0035
        L_0x0029:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            r12.<init>(r15)
            throw r12
        L_0x002f:
            com.helpshift.c.a.a.a.a r13 = com.helpshift.c.a.a.a.a.EXTERNAL_ONLY
            goto L_0x0035
        L_0x0032:
            com.helpshift.c.a.a.a.a r13 = com.helpshift.c.a.a.a.a.INTERNAL_ONLY
            r4 = 1
        L_0x0035:
            com.helpshift.c.a.a.a$a r5 = new com.helpshift.c.a.a.a$a
            r5.<init>()
            r5.f3212a = r2
            r5.c = r4
            r5.f3213b = r2
            r5.e = r13
            com.helpshift.c.a.a.a r13 = new com.helpshift.c.a.a.a
            r13.<init>()
            boolean r4 = r5.f3212a
            r13.f3210a = r4
            boolean r4 = r5.c
            r13.c = r4
            boolean r4 = r5.f3213b
            r13.f3211b = r4
            java.lang.String r4 = r5.d
            r13.d = r4
            com.helpshift.c.a.a.a.a r4 = r5.e
            r13.e = r4
            com.helpshift.common.e.u r7 = new com.helpshift.common.e.u
            r7.<init>(r11, r14)
            com.helpshift.common.e.v r14 = new com.helpshift.common.e.v
            r14.<init>(r11)
            com.helpshift.common.e.w r4 = new com.helpshift.common.e.w
            r4.<init>(r11)
            boolean r5 = r13.f3210a
            if (r5 == 0) goto L_0x00b8
            java.lang.String r5 = r3.f3216a
            com.helpshift.c.a.a.c.c r6 = r12.e
            java.lang.String r6 = r6.a(r5)
            boolean r8 = android.text.TextUtils.isEmpty(r6)
            r9 = 0
            if (r8 == 0) goto L_0x007e
            goto L_0x00ab
        L_0x007e:
            boolean r8 = com.helpshift.c.a.a.g.a(r6)
            if (r8 == 0) goto L_0x0092
            android.content.Context r8 = r12.f
            boolean r8 = com.helpshift.c.a.a.g.a(r8, r6)
            if (r8 != 0) goto L_0x00a4
            com.helpshift.c.a.a.c.c r6 = r12.e
            r6.b(r5)
            goto L_0x00ab
        L_0x0092:
            java.io.File r8 = new java.io.File
            r8.<init>(r6)
            boolean r10 = r8.exists()
            if (r10 == 0) goto L_0x00a6
            boolean r8 = r8.canRead()
            if (r8 != 0) goto L_0x00a4
            goto L_0x00a6
        L_0x00a4:
            r9 = r6
            goto L_0x00ab
        L_0x00a6:
            com.helpshift.c.a.a.c.c r6 = r12.e
            r6.b(r5)
        L_0x00ab:
            boolean r5 = android.text.TextUtils.isEmpty(r9)
            if (r5 != 0) goto L_0x00b8
            java.lang.String r12 = r3.f3216a
            r4.a(r2, r12, r9)
            goto L_0x019e
        L_0x00b8:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.concurrent.ConcurrentLinkedQueue<com.helpshift.c.a.a.a.e>> r5 = r12.f3218a
            java.lang.String r6 = r3.f3216a
            java.lang.Object r5 = r5.get(r6)
            java.util.concurrent.ConcurrentLinkedQueue r5 = (java.util.concurrent.ConcurrentLinkedQueue) r5
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.concurrent.ConcurrentLinkedQueue<com.helpshift.c.a.a.a.f>> r6 = r12.f3219b
            java.lang.String r8 = r3.f3216a
            java.lang.Object r6 = r6.get(r8)
            java.util.concurrent.ConcurrentLinkedQueue r6 = (java.util.concurrent.ConcurrentLinkedQueue) r6
            if (r5 == 0) goto L_0x00d8
            if (r6 == 0) goto L_0x00d8
            r5.add(r4)
            r6.add(r14)
            goto L_0x019e
        L_0x00d8:
            java.util.concurrent.ConcurrentLinkedQueue r5 = new java.util.concurrent.ConcurrentLinkedQueue
            r5.<init>()
            java.util.concurrent.ConcurrentLinkedQueue r6 = new java.util.concurrent.ConcurrentLinkedQueue
            r6.<init>()
            r5.add(r4)
            r6.add(r14)
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.concurrent.ConcurrentLinkedQueue<com.helpshift.c.a.a.a.e>> r14 = r12.f3218a
            java.lang.String r4 = r3.f3216a
            r14.put(r4, r5)
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.concurrent.ConcurrentLinkedQueue<com.helpshift.c.a.a.a.f>> r14 = r12.f3219b
            java.lang.String r4 = r3.f3216a
            r14.put(r4, r6)
            com.helpshift.c.a.a.d r14 = new com.helpshift.c.a.a.d
            r14.<init>(r12)
            com.helpshift.c.a.a.c r9 = new com.helpshift.c.a.a.c
            r9.<init>(r12, r13)
            boolean r4 = r13.f3211b
            if (r4 != 0) goto L_0x010b
            com.helpshift.c.a.a.b.f r13 = new com.helpshift.c.a.a.b.f
            r13.<init>(r3, r7, r14, r9)
            goto L_0x0199
        L_0x010b:
            com.helpshift.c.a.a.c.b r6 = new com.helpshift.c.a.a.c.b
            com.helpshift.c.a.a.a.c r4 = r12.d
            r6.<init>(r3, r4)
            int[] r4 = com.helpshift.c.a.a.e.f3232a
            com.helpshift.c.a.a.a.a r5 = r13.e
            int r5 = r5.ordinal()
            r4 = r4[r5]
            if (r4 == r2) goto L_0x018d
            if (r4 == r1) goto L_0x015d
            if (r4 != r0) goto L_0x0157
            boolean r15 = com.helpshift.c.a.a.b.b()
            if (r15 == 0) goto L_0x0136
            com.helpshift.c.a.a.b.c r13 = new com.helpshift.c.a.a.b.c
            android.content.Context r2 = r12.f
            r1 = r13
            r4 = r6
            r5 = r7
            r6 = r14
            r7 = r9
            r1.<init>(r2, r3, r4, r5, r6, r7)
            goto L_0x0199
        L_0x0136:
            boolean r15 = r12.a()
            if (r15 == 0) goto L_0x014a
            com.helpshift.c.a.a.b.b r15 = new com.helpshift.c.a.a.b.b
            android.content.Context r2 = r12.f
            java.lang.String r4 = r13.d
            boolean r5 = r13.c
            r1 = r15
            r8 = r14
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x0183
        L_0x014a:
            com.helpshift.c.a.a.b.d r13 = new com.helpshift.c.a.a.b.d
            android.content.Context r2 = r12.f
            r1 = r13
            r4 = r6
            r5 = r7
            r6 = r14
            r7 = r9
            r1.<init>(r2, r3, r4, r5, r6, r7)
            goto L_0x0199
        L_0x0157:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            r12.<init>(r15)
            throw r12
        L_0x015d:
            boolean r15 = com.helpshift.c.a.a.b.b()
            if (r15 == 0) goto L_0x0170
            com.helpshift.c.a.a.b.e r13 = new com.helpshift.c.a.a.b.e
            android.content.Context r2 = r12.f
            r1 = r13
            r4 = r6
            r5 = r7
            r6 = r14
            r7 = r9
            r1.<init>(r2, r3, r4, r5, r6, r7)
            goto L_0x0199
        L_0x0170:
            boolean r15 = r12.a()
            if (r15 == 0) goto L_0x0185
            com.helpshift.c.a.a.b.b r15 = new com.helpshift.c.a.a.b.b
            android.content.Context r2 = r12.f
            java.lang.String r4 = r13.d
            boolean r5 = r13.c
            r1 = r15
            r8 = r14
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
        L_0x0183:
            r13 = r15
            goto L_0x0199
        L_0x0185:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "External storage permission is not granted on below Android-Q device"
            r12.<init>(r13)
            throw r12
        L_0x018d:
            com.helpshift.c.a.a.b.d r13 = new com.helpshift.c.a.a.b.d
            android.content.Context r2 = r12.f
            r1 = r13
            r4 = r6
            r5 = r7
            r6 = r14
            r7 = r9
            r1.<init>(r2, r3, r4, r5, r6, r7)
        L_0x0199:
            java.util.concurrent.ThreadPoolExecutor r12 = r12.c
            r12.execute(r13)
        L_0x019e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.e.t.a(com.helpshift.m.a, com.helpshift.m.c$a, com.helpshift.common.c.b.a, com.helpshift.m.b):void");
    }

    private synchronized void a(String str, com.helpshift.m.b bVar) {
        if (bVar != null) {
            Set set = this.d.get(str);
            if (set == null) {
                set = new HashSet();
            }
            set.add(bVar);
            this.d.put(str, set);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(String str) {
        this.d.remove(str);
    }

    /* access modifiers changed from: package-private */
    public synchronized Set<com.helpshift.m.b> b(String str) {
        HashSet hashSet;
        Set set = this.d.get(str);
        if (set == null) {
            hashSet = new HashSet();
        } else {
            hashSet = new HashSet(set);
        }
        return hashSet;
    }
}
