package com.helpshift.common.e;

import com.helpshift.common.e.a.e;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* compiled from: AndroidNetworkRequestDAO */
public class n implements e {

    /* renamed from: a  reason: collision with root package name */
    private Set<String> f3339a = new HashSet();

    /* renamed from: b  reason: collision with root package name */
    private aa f3340b;

    public n(aa aaVar) {
        this.f3340b = aaVar;
    }

    public final void a(float f) {
        this.f3340b.a("server_time_delta", Float.valueOf(f));
        com.helpshift.util.n.a(f);
    }

    public final float a() {
        return this.f3340b.b("server_time_delta", Float.valueOf(0.0f)).floatValue();
    }

    public final void a(String str, String str2) {
        HashMap hashMap;
        Object b2 = this.f3340b.b("route_etag_map");
        if (b2 == null) {
            hashMap = new HashMap();
        } else {
            hashMap = (HashMap) b2;
        }
        hashMap.put(str, str2);
        this.f3340b.a("route_etag_map", hashMap);
    }

    public final void a(String str) {
        Object b2;
        if (str != null && (b2 = this.f3340b.b("route_etag_map")) != null) {
            HashMap hashMap = (HashMap) b2;
            if (hashMap.containsKey(str)) {
                hashMap.remove(str);
                this.f3340b.a("route_etag_map", hashMap);
            }
        }
    }

    public final String b(String str) {
        Object b2 = this.f3340b.b("route_etag_map");
        if (b2 == null) {
            return null;
        }
        return (String) ((HashMap) b2).get(str);
    }

    public final void a(String str, String str2, String str3) {
        HashMap hashMap;
        String str4 = "idempotent_" + str;
        Object b2 = this.f3340b.b(str4);
        if (b2 == null) {
            hashMap = new HashMap();
        } else {
            hashMap = (HashMap) b2;
        }
        hashMap.put(str2, str3);
        this.f3340b.a(str4, hashMap);
    }

    public final void b(String str, String str2) {
        String str3 = "idempotent_" + str;
        Object b2 = this.f3340b.b(str3);
        if (b2 instanceof HashMap) {
            HashMap hashMap = (HashMap) b2;
            hashMap.remove(str2);
            this.f3340b.a(str3, hashMap);
        }
    }

    public final String c(String str, String str2) {
        aa aaVar = this.f3340b;
        Object b2 = aaVar.b("idempotent_" + str);
        if (b2 == null) {
            return null;
        }
        return (String) ((HashMap) b2).get(str2);
    }

    public final Map<String, String> c(String str) {
        aa aaVar = this.f3340b;
        Object b2 = aaVar.b("idempotent_" + str);
        if (b2 == null) {
            return null;
        }
        return (HashMap) b2;
    }

    public final void d(String str) {
        this.f3339a.add(str);
    }

    public final Set<String> b() {
        return this.f3339a;
    }

    public final void c() {
        this.f3339a.clear();
    }
}
