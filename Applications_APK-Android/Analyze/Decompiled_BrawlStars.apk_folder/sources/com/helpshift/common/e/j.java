package com.helpshift.common.e;

import com.helpshift.n.a.a;
import java.util.HashMap;
import java.util.Map;

/* compiled from: AndroidFaqEventDAO */
public class j implements a {

    /* renamed from: a  reason: collision with root package name */
    private aa f3337a;

    public j(aa aaVar) {
        this.f3337a = aaVar;
    }

    public final void a(String str, boolean z) {
        HashMap<String, Boolean> b2 = b();
        b2.put(str, Boolean.valueOf(z));
        this.f3337a.a("key_faq_mark_event", b2);
    }

    public final void a(String str) {
        HashMap<String, Boolean> b2 = b();
        if (b2.containsKey(str)) {
            b2.remove(str);
            this.f3337a.a("key_faq_mark_event", b2);
        }
    }

    public final Map<String, Boolean> a() {
        return b();
    }

    private synchronized HashMap<String, Boolean> b() {
        HashMap<String, Boolean> hashMap;
        Object b2 = this.f3337a.b("key_faq_mark_event");
        if (b2 instanceof HashMap) {
            hashMap = (HashMap) b2;
        } else {
            hashMap = new HashMap<>();
        }
        return hashMap;
    }
}
