package com.helpshift.common.e.a;

import java.util.Map;
import java.util.UUID;

/* compiled from: RequestData */
public class i {

    /* renamed from: a  reason: collision with root package name */
    public final Map<String, String> f3320a;

    /* renamed from: b  reason: collision with root package name */
    public String f3321b = UUID.randomUUID().toString();
    public Map<String, String> c;

    public i(Map<String, String> map) {
        this.f3320a = map;
    }

    public i(i iVar) {
        this.f3320a = iVar.f3320a;
        this.c = iVar.c;
    }
}
