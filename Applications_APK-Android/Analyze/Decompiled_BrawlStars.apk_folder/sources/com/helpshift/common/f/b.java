package com.helpshift.common.f;

import android.support.v7.widget.ActivityChooserView;
import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

/* compiled from: ExponentialBackoff */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private final long f3371a;

    /* renamed from: b  reason: collision with root package name */
    private final long f3372b;
    private final float c;
    private final float d;
    private final int e;
    private final SecureRandom f = new SecureRandom();
    private long g;
    private int h;

    b(a aVar) {
        this.f3371a = aVar.f3373a;
        this.f3372b = aVar.f3374b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        a();
    }

    public final void a() {
        this.g = this.f3371a;
        this.h = 0;
    }

    public final long b() {
        int i = this.h;
        if (i >= this.e) {
            return -100;
        }
        this.h = i + 1;
        long j = this.g;
        float f2 = this.c;
        float f3 = ((float) j) * (1.0f - f2);
        float f4 = ((float) j) * (f2 + 1.0f);
        long j2 = this.f3372b;
        if (j <= j2) {
            this.g = Math.min((long) (((float) j) * this.d), j2);
        }
        return (long) (f3 + (this.f.nextFloat() * (f4 - f3)));
    }

    /* compiled from: ExponentialBackoff */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        long f3373a = TimeUnit.SECONDS.toMillis(10);

        /* renamed from: b  reason: collision with root package name */
        long f3374b = TimeUnit.SECONDS.toMillis(60);
        float c = 0.5f;
        float d = 2.0f;
        public int e = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;

        public final a a(a aVar) {
            this.f3373a = aVar.f3370b.toMillis(aVar.f3369a);
            return this;
        }

        public final a b(a aVar) {
            this.f3374b = aVar.f3370b.toMillis(aVar.f3369a);
            return this;
        }

        /* access modifiers changed from: package-private */
        public final void a() {
            long j = this.f3373a;
            if (j > 0) {
                long j2 = this.f3374b;
                if (j2 <= 0) {
                    throw new IllegalArgumentException("Max interval can't be negative or zero");
                } else if (j2 >= j) {
                    float f = this.c;
                    if (f < 0.0f || f > 1.0f) {
                        throw new IllegalArgumentException("Randomness must be between 0 and 1 (both inclusive)");
                    } else if (this.d < 1.0f) {
                        throw new IllegalArgumentException("Multiplier can't be less than 1");
                    } else if (this.e <= 0) {
                        throw new IllegalArgumentException("Max attempts can't be negative or zero");
                    }
                } else {
                    throw new IllegalArgumentException("Max interval can't be less than base interval");
                }
            } else {
                throw new IllegalArgumentException("Base interval can't be negative or zero");
            }
        }
    }
}
