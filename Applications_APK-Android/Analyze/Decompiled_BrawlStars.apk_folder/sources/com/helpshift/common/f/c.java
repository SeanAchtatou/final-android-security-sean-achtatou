package com.helpshift.common.f;

import com.helpshift.common.f.b;

/* compiled from: HttpBackoff */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public final b f3375a;

    /* renamed from: b  reason: collision with root package name */
    private final b f3376b;

    /* compiled from: HttpBackoff */
    public interface b {

        /* renamed from: b  reason: collision with root package name */
        public static final b f3379b = new d();
        public static final b c = new e();
        public static final b d = new f();

        boolean a(int i);
    }

    c(a aVar) {
        this.f3375a = new b(aVar.f3377a);
        this.f3376b = aVar.f3378b;
    }

    public final long a(int i) {
        long b2 = this.f3375a.b();
        if (this.f3376b.a(i)) {
            return b2;
        }
        return -100;
    }

    /* compiled from: HttpBackoff */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public final b.a f3377a = new b.a();

        /* renamed from: b  reason: collision with root package name */
        public b f3378b = b.c;

        public final a a(a aVar) {
            this.f3377a.a(aVar);
            return this;
        }

        public final a b(a aVar) {
            this.f3377a.b(aVar);
            return this;
        }

        public final a a(float f) {
            this.f3377a.c = f;
            return this;
        }

        public final a b(float f) {
            this.f3377a.d = f;
            return this;
        }

        public final c a() throws IllegalArgumentException {
            this.f3377a.a();
            return new c(this);
        }
    }
}
