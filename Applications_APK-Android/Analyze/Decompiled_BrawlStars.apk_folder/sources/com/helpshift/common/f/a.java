package com.helpshift.common.f;

import java.util.concurrent.TimeUnit;

/* compiled from: Delay */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public final long f3369a;

    /* renamed from: b  reason: collision with root package name */
    public final TimeUnit f3370b;

    private a(long j, TimeUnit timeUnit) {
        this.f3369a = j;
        this.f3370b = timeUnit;
    }

    public static a a(long j, TimeUnit timeUnit) {
        return new a(j, timeUnit);
    }

    public String toString() {
        return this.f3369a + " " + this.f3370b;
    }
}
