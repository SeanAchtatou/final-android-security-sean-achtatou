package com.helpshift.common.c.b;

import android.support.v4.app.NotificationCompat;
import com.helpshift.a.b.c;
import com.helpshift.common.k;
import java.util.HashMap;
import java.util.Map;

/* compiled from: NetworkDataRequestUtil */
public class o {
    public static HashMap<String, String> a(c cVar) {
        HashMap<String, String> hashMap = new HashMap<>();
        if (cVar != null) {
            if (!k.a(cVar.e)) {
                hashMap.put("did", cVar.e);
            }
            if (!k.a(cVar.f3177b)) {
                hashMap.put("uid", cVar.f3177b);
            }
            if (!k.a(cVar.c)) {
                hashMap.put(NotificationCompat.CATEGORY_EMAIL, cVar.c);
            }
            if (!k.a(cVar.i)) {
                hashMap.put("user_auth_token", cVar.i);
            }
        }
        return hashMap;
    }

    public static Map<String, String> a(Map<String, String> map) {
        String str;
        HashMap hashMap = new HashMap();
        for (String next : map.keySet()) {
            if (!(next == null || (str = map.get(next)) == null)) {
                hashMap.put(next, str);
            }
        }
        return hashMap;
    }

    public static Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("ia", true);
        hashMap.put("rs", true);
        hashMap.put("clc", true);
        hashMap.put("atai_v2", true);
        hashMap.put("fp", true);
        hashMap.put("cb", true);
        return hashMap;
    }
}
