package com.helpshift.common.c.b;

import com.helpshift.common.e.a.j;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;

/* compiled from: GuardAgainstConversationArchivalNetwork */
public class i implements m {

    /* renamed from: a  reason: collision with root package name */
    private final m f3262a;

    public i(m mVar) {
        this.f3262a = mVar;
    }

    public final j a(com.helpshift.common.e.a.i iVar) {
        j a2 = this.f3262a.a(iVar);
        if (a2.f3322a != 410) {
            return a2;
        }
        throw RootAPIException.a(null, b.CONVERSATION_ARCHIVED);
    }
}
