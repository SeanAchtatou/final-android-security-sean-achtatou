package com.helpshift.common.c;

import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.p.b.a;
import com.helpshift.p.b.d;
import com.helpshift.util.n;

/* compiled from: BackgroundThreader */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f3283a;

    h(g gVar) {
        this.f3283a = gVar;
    }

    public void run() {
        try {
            this.f3283a.f3281a.a();
        } catch (RootAPIException e) {
            if (e.b()) {
                a aVar = null;
                String str = e.f3358a == null ? "" : e.f3358a;
                if (e.c instanceof b) {
                    aVar = d.a("route", ((b) e.c).v);
                }
                n.a("Helpshift_CoreBgTh", str, new Throwable[]{e.f3359b, this.f3283a.f3281a.e}, aVar);
            }
        } catch (Exception e2) {
            n.b("Helpshift_CoreBgTh", "Caught unhandled exception inside BackgroundThreader", new Throwable[]{e2, this.f3283a.f3281a.e}, new a[0]);
        }
    }
}
