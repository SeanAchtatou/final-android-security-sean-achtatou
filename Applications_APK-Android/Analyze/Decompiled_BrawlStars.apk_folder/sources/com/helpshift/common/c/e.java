package com.helpshift.common.c;

import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.p.b.a;
import com.helpshift.p.b.d;
import com.helpshift.util.n;

/* compiled from: BackgroundDelayedThreader */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f3279a;

    e(d dVar) {
        this.f3279a = dVar;
    }

    public void run() {
        try {
            this.f3279a.f3277a.a();
        } catch (RootAPIException e) {
            if (e.b()) {
                a aVar = null;
                String str = e.f3358a == null ? "" : e.f3358a;
                if (e.c instanceof b) {
                    aVar = d.a("route", ((b) e.c).v);
                }
                n.a("Helpshift_CoreDelayTh", str, new Throwable[]{e.f3359b, this.f3279a.f3277a.e}, aVar);
            }
        } catch (Exception e2) {
            n.b("Helpshift_CoreDelayTh", "Caught unhandled exception inside BackgroundThreader", new Throwable[]{e2, this.f3279a.f3277a.e}, new a[0]);
        }
    }
}
