package com.helpshift.common.c.b;

import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.k;

/* compiled from: AuthenticationFailureNetwork */
public class b implements m {

    /* renamed from: a  reason: collision with root package name */
    private final m f3255a;

    public b(m mVar) {
        this.f3255a = mVar;
    }

    public final j a(i iVar) {
        j a2 = this.f3255a.a(iVar);
        if (a2.f3322a == p.k.intValue() && !k.a(a2.f3323b)) {
            if ("missing user auth token".equalsIgnoreCase(a2.f3323b)) {
                com.helpshift.common.exception.b bVar = com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED;
                bVar.u = p.G.intValue();
                throw RootAPIException.a(null, bVar);
            } else if ("invalid user auth token".equalsIgnoreCase(a2.f3323b)) {
                com.helpshift.common.exception.b bVar2 = com.helpshift.common.exception.b.INVALID_AUTH_TOKEN;
                bVar2.u = p.H.intValue();
                throw RootAPIException.a(null, bVar2);
            }
        }
        return a2;
    }
}
