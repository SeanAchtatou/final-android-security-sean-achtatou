package com.helpshift.common.c.b;

import com.helpshift.common.e.a.c;
import com.helpshift.common.e.a.e;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.g.b;
import java.util.List;

/* compiled from: TSCorrectedNetwork */
public class s implements m {

    /* renamed from: a  reason: collision with root package name */
    private final m f3272a;

    /* renamed from: b  reason: collision with root package name */
    private final e f3273b;

    public s(m mVar, ab abVar) {
        this.f3272a = mVar;
        this.f3273b = abVar.t();
    }

    public final j a(i iVar) {
        return a(iVar, 3);
    }

    private j a(i iVar, int i) {
        while (true) {
            j a2 = this.f3272a.a(iVar);
            if (a2.f3322a != 422) {
                return a2;
            }
            if (i != 0) {
                i--;
                String a3 = a(a2.c, "HS-UEpoch");
                if (a3 != null) {
                    this.f3273b.a(b.a(a3));
                }
                iVar = new i(iVar);
            } else {
                throw RootAPIException.a(null, com.helpshift.common.exception.b.TIMESTAMP_CORRECTION_RETRIES_EXHAUSTED);
            }
        }
    }

    private static String a(List<c> list, String str) {
        for (c next : list) {
            if (next.f3313a != null && next.f3313a.equals(str)) {
                return next.f3314b;
            }
        }
        return null;
    }
}
