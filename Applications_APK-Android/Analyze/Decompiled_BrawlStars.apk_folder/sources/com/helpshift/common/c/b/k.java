package com.helpshift.common.c.b;

import com.helpshift.common.c.a.b;
import com.helpshift.common.e.a.e;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.e.ab;

/* compiled from: IdempotentNetwork */
public class k implements m {

    /* renamed from: a  reason: collision with root package name */
    private m f3264a;

    /* renamed from: b  reason: collision with root package name */
    private String f3265b;
    private String c;
    private e d;
    private b e;

    public k(m mVar, ab abVar, b bVar, String str, String str2) {
        this.f3264a = mVar;
        this.e = bVar;
        this.d = abVar.t();
        this.f3265b = str;
        this.c = str2;
    }

    public final j a(i iVar) {
        String c2 = this.d.c(this.f3265b, this.c);
        if (com.helpshift.common.k.a(c2)) {
            this.d.a(this.f3265b, this.c, iVar.f3321b);
        } else {
            iVar.f3321b = c2;
        }
        j a2 = this.f3264a.a(iVar);
        if (a2 == null || this.e.a(a2.f3322a)) {
            this.d.b(this.f3265b, this.c);
            this.d.d(iVar.f3321b);
        }
        return a2;
    }
}
