package com.helpshift.common.c.b;

import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;

/* compiled from: UserPreConditionsFailedNetwork */
public class v implements m {

    /* renamed from: a  reason: collision with root package name */
    private final m f3275a;

    public v(m mVar) {
        this.f3275a = mVar;
    }

    public final j a(i iVar) {
        j a2 = this.f3275a.a(iVar);
        if (a2.f3322a != p.u.intValue()) {
            return a2;
        }
        throw RootAPIException.a(null, b.USER_PRE_CONDITION_FAILED);
    }
}
