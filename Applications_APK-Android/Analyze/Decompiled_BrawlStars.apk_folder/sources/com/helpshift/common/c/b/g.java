package com.helpshift.common.c.b;

import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;

/* compiled from: FailedAPICallNetworkDecorator */
public class g implements m {

    /* renamed from: a  reason: collision with root package name */
    private final m f3261a;

    public g(m mVar) {
        this.f3261a = mVar;
    }

    public final j a(i iVar) {
        j a2 = this.f3261a.a(iVar);
        int i = a2.f3322a;
        if (!p.K.contains(Integer.valueOf(i))) {
            return a2;
        }
        b bVar = b.NON_RETRIABLE;
        bVar.u = i;
        throw RootAPIException.a(null, bVar);
    }
}
