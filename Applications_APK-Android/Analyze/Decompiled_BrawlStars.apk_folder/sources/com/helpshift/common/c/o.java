package com.helpshift.common.c;

import com.helpshift.common.c.b.p;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.f.c;
import com.helpshift.util.n;

/* compiled from: PollFunction */
public class o extends l {

    /* renamed from: a  reason: collision with root package name */
    c f3292a;

    /* renamed from: b  reason: collision with root package name */
    boolean f3293b;
    final s c;
    private final l d;
    private final j f;
    private final a g;

    /* compiled from: PollFunction */
    public interface a {
        void a();
    }

    public o(j jVar, c cVar, l lVar, s sVar, a aVar) {
        this.f3292a = cVar;
        this.d = lVar;
        this.f = jVar;
        this.c = sVar;
        this.g = aVar;
    }

    public final void a() {
        int i;
        if (this.f3293b) {
            try {
                n.a("Helpshift_PollFunc", "Running:" + this.c.name(), (Throwable) null, (com.helpshift.p.b.a[]) null);
                this.d.a();
                i = p.h.intValue();
            } catch (RootAPIException e) {
                if (e.c instanceof b) {
                    i = e.a();
                } else {
                    throw e;
                }
            }
            long a2 = this.f3292a.a(i);
            if (a2 == -100) {
                a aVar = this.g;
                if (aVar != null) {
                    aVar.a();
                    return;
                }
                return;
            }
            a(a2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(long j) {
        this.f.a(this, j);
    }
}
