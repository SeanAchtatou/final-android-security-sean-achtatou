package com.helpshift.common.c.b;

import com.helpshift.common.e.a.c;
import com.helpshift.common.e.a.d;
import com.helpshift.common.e.a.h;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.e.a.l;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import java.io.File;
import java.util.List;

/* compiled from: UploadNetwork */
public class t extends c {
    ab c;

    public final /* bridge */ /* synthetic */ j a(i iVar) {
        return super.a(iVar);
    }

    public t(String str, com.helpshift.common.c.j jVar, ab abVar) {
        super(str, jVar, abVar);
        this.c = abVar;
    }

    /* access modifiers changed from: package-private */
    public final h b(i iVar) {
        String b2 = this.c.b(new File(iVar.f3320a.get("screenshot")).getPath());
        if (this.c.a(b2)) {
            return new l(d.POST, a(), a(d.POST, o.a(iVar.f3320a)), b2, a(iVar.f3321b, iVar), 30000);
        }
        throw RootAPIException.a(null, b.UNSUPPORTED_MIME_TYPE);
    }

    /* access modifiers changed from: package-private */
    public final List<c> a(String str, i iVar) {
        List<c> a2 = super.a(str, iVar);
        a2.add(new c("Connection", "Keep-Alive"));
        a2.add(new c("Content-Type", "multipart/form-data;boundary=*****"));
        return a2;
    }
}
