package com.helpshift.common.c.b;

import com.facebook.share.internal.ShareConstants;
import com.helpshift.common.c.j;
import com.helpshift.common.e.a.b;
import com.helpshift.common.e.a.d;
import com.helpshift.common.e.a.e;
import com.helpshift.common.e.a.h;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.ab;
import com.helpshift.common.e.y;
import com.helpshift.common.e.z;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.k;
import com.helpshift.o.a.a;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* compiled from: BaseNetwork */
abstract class c implements m {

    /* renamed from: a  reason: collision with root package name */
    final String f3256a;

    /* renamed from: b  reason: collision with root package name */
    final e f3257b;
    private final b c;
    private final String d;
    private final a e;
    private final com.helpshift.k.a f;
    private final String g;
    private final String h;
    private final y i;
    private final z j;
    private final j k;
    private final ab l;

    /* access modifiers changed from: package-private */
    public abstract h b(i iVar);

    c(String str, j jVar, ab abVar) {
        this.f3256a = str;
        this.l = abVar;
        this.k = jVar;
        this.e = jVar.f;
        this.f = jVar.e();
        this.f3257b = abVar.t();
        this.c = abVar.m();
        this.d = abVar.a();
        this.g = abVar.b();
        this.h = abVar.c();
        this.i = abVar.d();
        this.j = abVar.p();
    }

    /* access modifiers changed from: protected */
    public final String a() {
        return n.f3268a + this.g + b();
    }

    private String b() {
        return "/api/lib/3" + this.f3256a;
    }

    /* access modifiers changed from: package-private */
    public final Map<String, String> a(d dVar, Map<String, String> map) {
        a aVar = new a(this.k, this.l, this.f3256a);
        map.put(ShareConstants.MEDIA_URI, b());
        try {
            return aVar.a(dVar, map);
        } catch (GeneralSecurityException e2) {
            com.helpshift.common.exception.b bVar = com.helpshift.common.exception.b.UNABLE_TO_GENERATE_SIGNATURE;
            bVar.v = this.f3256a;
            throw RootAPIException.a(e2, bVar, "Network error");
        }
    }

    private static List<com.helpshift.common.e.a.c> c(i iVar) {
        ArrayList arrayList = new ArrayList();
        Map<String, String> map = iVar.c;
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                arrayList.add(new com.helpshift.common.e.a.c((String) next.getKey(), (String) next.getValue()));
            }
        }
        return arrayList;
    }

    public com.helpshift.common.e.a.j a(i iVar) {
        return this.c.a(b(iVar));
    }

    /* access modifiers changed from: package-private */
    public List<com.helpshift.common.e.a.c> a(String str, i iVar) {
        String str2;
        String format = String.format(Locale.ENGLISH, "Helpshift-%s/%s/%s", this.i.b(), this.i.a(), this.i.c());
        String e2 = this.e.e();
        String locale = Locale.getDefault().toString();
        if (!k.a(e2)) {
            str2 = String.format(Locale.ENGLISH, "%s;q=1.0, %s;q=0.5", e2, locale);
        } else {
            str2 = String.format(Locale.ENGLISH, "%s;q=1.0", locale);
        }
        String format2 = String.format(Locale.ENGLISH, "Helpshift-%s/%s", this.i.b(), this.i.a());
        ArrayList arrayList = new ArrayList();
        arrayList.add(new com.helpshift.common.e.a.c("User-Agent", format));
        arrayList.add(new com.helpshift.common.e.a.c("Accept-Language", str2));
        arrayList.add(new com.helpshift.common.e.a.c("Accept-Encoding", "gzip"));
        arrayList.add(new com.helpshift.common.e.a.c("X-HS-V", format2));
        arrayList.add(new com.helpshift.common.e.a.c("X-HS-Request-ID", str));
        arrayList.addAll(c(iVar));
        return arrayList;
    }
}
