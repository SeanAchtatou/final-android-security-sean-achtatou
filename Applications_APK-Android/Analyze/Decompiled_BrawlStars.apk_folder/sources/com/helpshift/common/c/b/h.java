package com.helpshift.common.c.b;

import com.helpshift.common.e.a.a;
import com.helpshift.common.e.a.c;
import com.helpshift.common.e.a.d;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.k;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: GETNetwork */
public class h extends c {
    public final /* bridge */ /* synthetic */ j a(i iVar) {
        return super.a(iVar);
    }

    public h(String str, com.helpshift.common.c.j jVar, ab abVar) {
        super(str, jVar, abVar);
    }

    /* access modifiers changed from: package-private */
    public final com.helpshift.common.e.a.h b(i iVar) {
        return new a(a() + "?" + a(o.a(iVar.f3320a)), a(iVar.f3321b, iVar), 5000);
    }

    private String a(Map<String, String> map) {
        Map<String, String> a2 = a(d.GET, map);
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : a2.entrySet()) {
            try {
                arrayList.add(((String) next.getKey()) + "=" + URLEncoder.encode((String) next.getValue(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                throw RootAPIException.a(e, b.UNSUPPORTED_ENCODING_EXCEPTION);
            }
        }
        return k.a("&", arrayList);
    }

    /* access modifiers changed from: package-private */
    public final List<c> a(String str, i iVar) {
        List<c> a2 = super.a(str, iVar);
        String b2 = this.f3257b.b(this.f3256a);
        if (b2 != null) {
            a2.add(new c("If-None-Match", b2));
        }
        return a2;
    }
}
