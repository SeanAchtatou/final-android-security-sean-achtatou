package com.helpshift.common.c;

import com.helpshift.a.b.e;
import com.helpshift.common.b;
import com.helpshift.common.e.ab;
import com.helpshift.common.f.c;
import com.helpshift.i.a.a;
import com.helpshift.j.c.l;
import com.helpshift.l.c;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/* compiled from: Domain */
public class j {

    /* renamed from: a  reason: collision with root package name */
    public t f3284a;

    /* renamed from: b  reason: collision with root package name */
    public a f3285b;
    public com.helpshift.b.a.a c;
    public com.helpshift.q.a d;
    public c e = new c(this);
    public com.helpshift.o.a.a f;
    public b g;
    public e h;
    public l i;
    public com.helpshift.a.a j;
    private final ab k;
    private t l;
    private i m;
    private com.helpshift.n.b n;
    private com.helpshift.k.a o;
    private com.helpshift.g.a.a p;
    private a q;
    private com.helpshift.h.a r;
    private com.helpshift.p.a s;

    public j(ab abVar) {
        this.k = abVar;
        c.a b2 = new c.a().a(com.helpshift.common.f.a.a(5, TimeUnit.SECONDS)).b(com.helpshift.common.f.a.a(60, TimeUnit.SECONDS));
        b2.f3377a.e = 10;
        c.a b3 = b2.a(0.1f).b(2.0f);
        b3.f3378b = c.b.f3379b;
        this.g = new b(this, abVar, b3.a());
        this.h = new e(abVar, this);
        e eVar = this.h;
        eVar.e = eVar.g.d();
        eVar.f3182a = eVar.g.r();
        eVar.f3183b = eVar.g.q();
        eVar.c = eVar.g.k();
        eVar.d = eVar.g.s();
        eVar.h.g.a(b.a.PUSH_TOKEN, eVar);
        eVar.h.g.a(b.a.CLEAR_USER, eVar);
        com.helpshift.a.b.c d2 = eVar.d();
        if (d2 != null) {
            eVar.c.a("anonymous_user_id_backup_key", d2.f3177b);
        }
        this.l = new f(Executors.newSingleThreadExecutor(new m("core-s")));
        this.f3284a = new f(Executors.newCachedThreadPool(new m("core-p")));
        this.f3285b = new a(this, abVar);
        this.d = new com.helpshift.q.a(this, abVar, this.f3285b);
        this.c = new com.helpshift.b.a.a(this, abVar);
        this.i = new l(abVar, this, this.h);
        this.f = new com.helpshift.o.a.a(this.f3285b, abVar);
        this.j = new com.helpshift.a.a(this);
    }

    private synchronized i j() {
        if (this.m == null) {
            this.m = new c(Executors.newScheduledThreadPool(1, new m("core-d")));
        }
        return this.m;
    }

    public final e a() {
        return this.h;
    }

    public final l b() {
        return this.i;
    }

    public final a c() {
        return this.f3285b;
    }

    public final synchronized com.helpshift.h.a d() {
        if (this.r == null) {
            this.r = new com.helpshift.h.a(this, this.k);
        }
        return this.r;
    }

    public final synchronized com.helpshift.k.a e() {
        if (this.o == null) {
            this.o = new com.helpshift.k.a();
        }
        return this.o;
    }

    public final synchronized com.helpshift.n.b f() {
        if (this.n == null) {
            this.n = new com.helpshift.n.b(this, this.k);
        }
        return this.n;
    }

    public final synchronized com.helpshift.g.a.a g() {
        if (this.p == null) {
            this.p = new com.helpshift.g.a.a(this, this.k);
        }
        return this.p;
    }

    public final synchronized a h() {
        if (this.q == null) {
            this.q = new a(this, this.k);
        }
        return this.q;
    }

    public final void c(l lVar) {
        if (this.k.v()) {
            lVar.a();
        } else {
            this.k.w().a(lVar).a();
        }
    }

    private void b(l lVar, long j2) {
        j().a(lVar, j2).a();
    }

    public final void a(l lVar, long j2) {
        b(new k(this, lVar), j2);
    }

    public final synchronized com.helpshift.p.a i() {
        if (this.s == null) {
            this.s = new com.helpshift.p.a(this.k, this);
        }
        return this.s;
    }

    public final void a(l lVar) {
        this.l.a(lVar).a();
    }

    public final void b(l lVar) {
        this.f3284a.a(lVar).a();
    }
}
