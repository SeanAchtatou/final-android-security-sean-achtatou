package com.helpshift.common.c.b;

import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;

/* compiled from: UserNotFoundNetwork */
public class u implements m {

    /* renamed from: a  reason: collision with root package name */
    private final m f3274a;

    public u(m mVar) {
        this.f3274a = mVar;
    }

    public final j a(i iVar) {
        j a2 = this.f3274a.a(iVar);
        if (a2.f3322a != p.n.intValue()) {
            return a2;
        }
        throw RootAPIException.a(null, b.USER_NOT_FOUND);
    }
}
