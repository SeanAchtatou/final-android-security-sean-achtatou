package com.helpshift.common.c;

import com.helpshift.util.n;
import java.util.concurrent.RejectedExecutionException;

/* compiled from: BackgroundThreader */
class g extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3281a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ f f3282b;

    g(f fVar, l lVar) {
        this.f3282b = fVar;
        this.f3281a = lVar;
    }

    public final void a() {
        this.f3281a.e = new Throwable();
        try {
            this.f3282b.f3280a.submit(new h(this));
        } catch (RejectedExecutionException e) {
            n.c("Helpshift_CoreBgTh", "Rejected execution of task in BackgroundThreader", e);
        }
    }
}
