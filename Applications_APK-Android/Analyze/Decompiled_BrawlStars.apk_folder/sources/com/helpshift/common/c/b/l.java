package com.helpshift.common.c.b;

import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.e.z;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import java.util.HashMap;
import java.util.Map;

/* compiled from: MetaCorrectedNetwork */
public class l implements m {

    /* renamed from: a  reason: collision with root package name */
    private final m f3266a;

    /* renamed from: b  reason: collision with root package name */
    private final z f3267b;

    public l(m mVar, ab abVar) {
        this.f3266a = mVar;
        this.f3267b = abVar.p();
    }

    public final j a(i iVar) {
        return a(iVar, 1);
    }

    private j a(i iVar, int i) {
        while (true) {
            j a2 = this.f3266a.a(iVar);
            if (a2.f3322a != 413) {
                return a2;
            }
            if (i > 0) {
                i--;
                HashMap hashMap = new HashMap(iVar.f3320a);
                a(hashMap);
                iVar = new i(hashMap);
            } else {
                throw RootAPIException.a(null, b.ENTITY_TOO_LARGE_RETRIES_EXHAUSTED);
            }
        }
    }

    private void a(Map<String, String> map) {
        map.put("meta", this.f3267b.a(map.get("meta"), "custom_meta"));
        map.remove("custom_fields");
    }
}
