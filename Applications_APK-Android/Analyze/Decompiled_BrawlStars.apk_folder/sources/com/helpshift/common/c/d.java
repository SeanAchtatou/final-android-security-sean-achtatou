package com.helpshift.common.c;

import com.helpshift.util.n;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

/* compiled from: BackgroundDelayedThreader */
class d extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3277a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ long f3278b;
    final /* synthetic */ c c;

    d(c cVar, l lVar, long j) {
        this.c = cVar;
        this.f3277a = lVar;
        this.f3278b = j;
    }

    public final void a() {
        this.f3277a.e = new Throwable();
        try {
            this.c.f3276a.schedule(new e(this), this.f3278b, TimeUnit.MILLISECONDS);
        } catch (RejectedExecutionException e) {
            n.c("Helpshift_CoreDelayTh", "Rejected execution of task in BackgroundDelayedThreader", e);
        }
    }
}
