package com.helpshift.common.c;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: HSThreadFactory */
public class m implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final String f3288a;

    /* renamed from: b  reason: collision with root package name */
    private final AtomicInteger f3289b = new AtomicInteger(1);

    public m(String str) {
        this.f3288a = str;
    }

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, "HS-" + this.f3288a + "-t-" + this.f3289b.getAndIncrement());
    }
}
