package com.helpshift.common.c.b;

import com.helpshift.common.c.j;
import com.helpshift.common.e.a.f;
import com.helpshift.common.e.a.h;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.k;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: CustomAuthDataPOSTNetwork */
public class e extends q {
    private Map<String, String> c;

    public e(String str, j jVar, ab abVar, Map<String, String> map) {
        super(str, jVar, abVar);
        this.c = map;
    }

    /* access modifiers changed from: package-private */
    public final h b(i iVar) {
        return new f(a(), a(o.a(iVar.f3320a)), a(iVar.f3321b, iVar), 5000);
    }

    /* access modifiers changed from: protected */
    public final String a(Map<String, String> map) {
        map.putAll(this.c);
        Map<String, String> a2 = o.a(map);
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : a2.entrySet()) {
            try {
                arrayList.add(URLEncoder.encode((String) next.getKey(), "UTF-8") + "=" + URLEncoder.encode((String) next.getValue(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                throw RootAPIException.a(e, b.UNSUPPORTED_ENCODING_EXCEPTION);
            }
        }
        return k.a("&", arrayList);
    }
}
