package com.helpshift.common.c.b;

import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;

/* compiled from: ContentUnchangedNetwork */
public class d implements m {

    /* renamed from: a  reason: collision with root package name */
    private final m f3258a;

    public d(m mVar) {
        this.f3258a = mVar;
    }

    public final j a(i iVar) {
        j a2 = this.f3258a.a(iVar);
        if (a2.f3322a != p.i.intValue()) {
            return a2;
        }
        throw RootAPIException.a(null, b.CONTENT_UNCHANGED);
    }
}
