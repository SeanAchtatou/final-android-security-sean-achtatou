package com.helpshift.common.c.b;

import com.helpshift.common.e.a.i;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;

/* compiled from: GuardOKNetwork */
public class j implements m {

    /* renamed from: a  reason: collision with root package name */
    private final m f3263a;

    public j(m mVar) {
        this.f3263a = mVar;
    }

    public final com.helpshift.common.e.a.j a(i iVar) {
        com.helpshift.common.e.a.j a2 = this.f3263a.a(iVar);
        int i = a2.f3322a;
        if (i >= 200 && i < 300) {
            return a2;
        }
        b bVar = b.UNHANDLED_STATUS_CODE;
        bVar.u = a2.f3322a;
        throw RootAPIException.a(null, bVar);
    }
}
