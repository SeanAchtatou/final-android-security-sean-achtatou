package com.helpshift.common.c.b;

import android.support.v7.widget.helper.ItemTouchHelper;
import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/* compiled from: NetworkErrorCodes */
public interface p {
    public static final Integer A = 422;
    public static final Integer B = 423;
    public static final Integer C = 424;
    public static final Integer D = 426;
    public static final Integer E = 428;
    public static final Integer F = 431;
    public static final Integer G = 441;
    public static final Integer H = 443;
    public static final Integer I = 451;
    public static final Integer J = 500;
    public static final Set<Integer> K = new HashSet(Arrays.asList(j, l, m, n, o, p, r, s, t, u, v, w, x, y, z, B, C, D, E, F, I));

    /* renamed from: a  reason: collision with root package name */
    public static final Integer f3270a = 0;

    /* renamed from: b  reason: collision with root package name */
    public static final Integer f3271b = 1;
    public static final Integer c = 2;
    public static final Integer d = 3;
    public static final Integer e = 4;
    public static final Integer f = 5;
    public static final Integer g = 102;
    public static final Integer h = Integer.valueOf((int) ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
    public static final Integer i = 304;
    public static final Integer j = 400;
    public static final Integer k = 401;
    public static final Integer l = 402;
    public static final Integer m = 403;
    public static final Integer n = 404;
    public static final Integer o = 405;
    public static final Integer p = 406;
    public static final Integer q = 408;
    public static final Integer r = 409;
    public static final Integer s = 410;
    public static final Integer t = 411;
    public static final Integer u = Integer.valueOf((int) FacebookRequestErrorClassification.EC_APP_NOT_INSTALLED);
    public static final Integer v = 413;
    public static final Integer w = 414;
    public static final Integer x = 415;
    public static final Integer y = 416;
    public static final Integer z = 417;
}
