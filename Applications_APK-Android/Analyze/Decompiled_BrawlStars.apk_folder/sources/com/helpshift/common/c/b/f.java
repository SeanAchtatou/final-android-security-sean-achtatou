package com.helpshift.common.c.b;

import com.helpshift.common.e.a.c;
import com.helpshift.common.e.a.e;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.e.ab;
import java.util.Iterator;

/* compiled from: ETagNetwork */
public class f implements m {

    /* renamed from: a  reason: collision with root package name */
    private final m f3259a;

    /* renamed from: b  reason: collision with root package name */
    private final e f3260b;
    private final String c;

    public f(m mVar, ab abVar, String str) {
        this.f3259a = mVar;
        this.f3260b = abVar.t();
        this.c = str;
    }

    public final j a(i iVar) {
        j a2 = this.f3259a.a(iVar);
        int i = a2.f3322a;
        if (i >= 200 && i < 300) {
            String str = null;
            Iterator<c> it = a2.c.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                c next = it.next();
                if (next.f3313a != null && next.f3313a.equals("ETag")) {
                    str = next.f3314b;
                    break;
                }
            }
            if (str != null) {
                this.f3260b.a(this.c, str);
            }
        }
        return a2;
    }
}
