package com.helpshift.common.c;

import com.helpshift.common.f.a;
import com.helpshift.common.f.c;
import com.helpshift.util.n;
import java.util.concurrent.TimeUnit;

/* compiled from: Poller */
public class p {

    /* renamed from: a  reason: collision with root package name */
    private final j f3294a;

    /* renamed from: b  reason: collision with root package name */
    private final l f3295b;
    private o c;
    private c d;
    private c e;
    private c f;

    public p(j jVar, l lVar) {
        c.a b2 = new c.a().a(a.a(5, TimeUnit.SECONDS)).b(a.a(1, TimeUnit.MINUTES)).a(0.1f).b(2.0f);
        b2.f3378b = b();
        this.d = b2.a();
        c.a b3 = new c.a().a(a.a(3, TimeUnit.SECONDS)).b(a.a(3, TimeUnit.SECONDS)).a(0.0f).b(1.0f);
        b3.f3378b = b();
        this.e = b3.a();
        c.a b4 = new c.a().a(a.a(30, TimeUnit.SECONDS)).b(a.a(5, TimeUnit.MINUTES)).a(0.1f).b(4.0f);
        b4.f3378b = b();
        this.f = b4.a();
        this.f3294a = jVar;
        this.f3295b = lVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0078, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.helpshift.common.c.s r9, long r10, com.helpshift.common.c.o.a r12) {
        /*
            r8 = this;
            monitor-enter(r8)
            r8.a()     // Catch:{ all -> 0x0079 }
            if (r9 != 0) goto L_0x0008
            monitor-exit(r8)
            return
        L_0x0008:
            int[] r0 = com.helpshift.common.c.r.f3297a     // Catch:{ all -> 0x0079 }
            int r9 = r9.ordinal()     // Catch:{ all -> 0x0079 }
            r9 = r0[r9]     // Catch:{ all -> 0x0079 }
            r0 = 1
            if (r9 == r0) goto L_0x003e
            r1 = 2
            if (r9 == r1) goto L_0x002c
            r1 = 3
            if (r9 == r1) goto L_0x001a
            goto L_0x004f
        L_0x001a:
            com.helpshift.common.c.o r9 = new com.helpshift.common.c.o     // Catch:{ all -> 0x0079 }
            com.helpshift.common.c.j r3 = r8.f3294a     // Catch:{ all -> 0x0079 }
            com.helpshift.common.f.c r4 = r8.d     // Catch:{ all -> 0x0079 }
            com.helpshift.common.c.l r5 = r8.f3295b     // Catch:{ all -> 0x0079 }
            com.helpshift.common.c.s r6 = com.helpshift.common.c.s.CONSERVATIVE     // Catch:{ all -> 0x0079 }
            r2 = r9
            r7 = r12
            r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0079 }
            r8.c = r9     // Catch:{ all -> 0x0079 }
            goto L_0x004f
        L_0x002c:
            com.helpshift.common.c.o r9 = new com.helpshift.common.c.o     // Catch:{ all -> 0x0079 }
            com.helpshift.common.c.j r2 = r8.f3294a     // Catch:{ all -> 0x0079 }
            com.helpshift.common.f.c r3 = r8.f     // Catch:{ all -> 0x0079 }
            com.helpshift.common.c.l r4 = r8.f3295b     // Catch:{ all -> 0x0079 }
            com.helpshift.common.c.s r5 = com.helpshift.common.c.s.PASSIVE     // Catch:{ all -> 0x0079 }
            r1 = r9
            r6 = r12
            r1.<init>(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0079 }
            r8.c = r9     // Catch:{ all -> 0x0079 }
            goto L_0x004f
        L_0x003e:
            com.helpshift.common.c.o r9 = new com.helpshift.common.c.o     // Catch:{ all -> 0x0079 }
            com.helpshift.common.c.j r2 = r8.f3294a     // Catch:{ all -> 0x0079 }
            com.helpshift.common.f.c r3 = r8.e     // Catch:{ all -> 0x0079 }
            com.helpshift.common.c.l r4 = r8.f3295b     // Catch:{ all -> 0x0079 }
            com.helpshift.common.c.s r5 = com.helpshift.common.c.s.AGGRESSIVE     // Catch:{ all -> 0x0079 }
            r1 = r9
            r6 = r12
            r1.<init>(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0079 }
            r8.c = r9     // Catch:{ all -> 0x0079 }
        L_0x004f:
            com.helpshift.common.c.o r9 = r8.c     // Catch:{ all -> 0x0079 }
            java.lang.String r12 = "Helpshift_PollFunc"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0079 }
            r1.<init>()     // Catch:{ all -> 0x0079 }
            java.lang.String r2 = "Start: "
            r1.append(r2)     // Catch:{ all -> 0x0079 }
            com.helpshift.common.c.s r2 = r9.c     // Catch:{ all -> 0x0079 }
            java.lang.String r2 = r2.name()     // Catch:{ all -> 0x0079 }
            r1.append(r2)     // Catch:{ all -> 0x0079 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0079 }
            r2 = 0
            com.helpshift.util.n.a(r12, r1, r2, r2)     // Catch:{ all -> 0x0079 }
            boolean r12 = r9.f3293b     // Catch:{ all -> 0x0079 }
            if (r12 != 0) goto L_0x0077
            r9.f3293b = r0     // Catch:{ all -> 0x0079 }
            r9.a(r10)     // Catch:{ all -> 0x0079 }
        L_0x0077:
            monitor-exit(r8)
            return
        L_0x0079:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.c.p.a(com.helpshift.common.c.s, long, com.helpshift.common.c.o$a):void");
    }

    public final synchronized void a() {
        if (this.c != null) {
            o oVar = this.c;
            n.a("Helpshift_PollFunc", "Stop: " + oVar.c.name(), (Throwable) null, (com.helpshift.p.b.a[]) null);
            oVar.f3293b = false;
            oVar.f3292a.f3375a.a();
            this.c = null;
        }
    }

    private c.b b() {
        return new q(this);
    }
}
