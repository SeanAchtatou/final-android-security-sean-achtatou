package com.helpshift.common.c.a;

import com.helpshift.common.c.b.p;

/* compiled from: SuccessOrNonRetriableStatusCodeIdempotentPolicy */
public class c extends a {
    /* access modifiers changed from: package-private */
    public final boolean b(int i) {
        if (i < 200 || i >= 300) {
            return p.K.contains(Integer.valueOf(i));
        }
        return true;
    }
}
