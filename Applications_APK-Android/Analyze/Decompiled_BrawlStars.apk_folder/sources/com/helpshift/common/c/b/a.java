package com.helpshift.common.c.b;

import com.facebook.share.internal.ShareConstants;
import com.helpshift.common.c.j;
import com.helpshift.common.e.a.d;
import com.helpshift.common.e.a.e;
import com.helpshift.common.e.ab;
import com.helpshift.common.e.z;
import com.helpshift.common.k;
import java.security.GeneralSecurityException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;

/* compiled from: AuthDataProvider */
public class a {

    /* renamed from: a  reason: collision with root package name */
    final String f3253a;

    /* renamed from: b  reason: collision with root package name */
    final String f3254b;
    final String c;
    final com.helpshift.k.a d;
    final e e;
    final z f;

    public a(j jVar, ab abVar, String str) {
        this.f3254b = abVar.a();
        this.f3253a = abVar.c();
        this.c = str;
        this.d = jVar.e();
        this.e = abVar.t();
        this.f = abVar.p();
    }

    public final Map<String, String> a(d dVar, Map<String, String> map) throws GeneralSecurityException {
        if (map == null || k.a(map.get(ShareConstants.MEDIA_URI))) {
            throw new IllegalArgumentException("No value for uri in auth data.");
        }
        map.put("platform-id", this.f3253a);
        map.put("method", dVar.name());
        float a2 = this.e.a();
        DecimalFormat decimalFormat = new DecimalFormat("0.000", new DecimalFormatSymbols(Locale.US));
        double currentTimeMillis = (double) System.currentTimeMillis();
        Double.isNaN(currentTimeMillis);
        double d2 = (double) a2;
        Double.isNaN(d2);
        map.put("timestamp", decimalFormat.format((currentTimeMillis / 1000.0d) + d2));
        map.put("sm", this.f.a(o.a()));
        ArrayList<String> arrayList = new ArrayList<>(map.keySet());
        Collections.sort(arrayList);
        ArrayList arrayList2 = new ArrayList();
        for (String str : arrayList) {
            if (!str.equals("screenshot") && !str.equals("meta") && !str.equals("originalFileName")) {
                arrayList2.add(str + "=" + map.get(str));
            }
        }
        map.put("signature", this.d.a(k.a("&", arrayList2), this.f3254b));
        map.remove("method");
        map.remove(ShareConstants.MEDIA_URI);
        return map;
    }
}
