package com.helpshift.common.c.a;

import com.helpshift.common.c.b.p;

/* compiled from: BaseIdempotentPolicy */
public abstract class a implements b {
    /* access modifiers changed from: package-private */
    public abstract boolean b(int i);

    public final boolean a(int i) {
        if (i == p.g.intValue()) {
            return false;
        }
        return b(i);
    }
}
