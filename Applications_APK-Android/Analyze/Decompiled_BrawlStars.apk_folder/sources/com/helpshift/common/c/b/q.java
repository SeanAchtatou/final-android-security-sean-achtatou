package com.helpshift.common.c.b;

import com.helpshift.common.e.a.c;
import com.helpshift.common.e.a.d;
import com.helpshift.common.e.a.f;
import com.helpshift.common.e.a.h;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.k;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: POSTNetwork */
public class q extends c implements m {
    public final /* bridge */ /* synthetic */ j a(i iVar) {
        return super.a(iVar);
    }

    public q(String str, com.helpshift.common.c.j jVar, ab abVar) {
        super(str, jVar, abVar);
    }

    /* access modifiers changed from: package-private */
    public h b(i iVar) {
        return new f(a(), a(o.a(iVar.f3320a)), a(iVar.f3321b, iVar), 5000);
    }

    /* access modifiers changed from: protected */
    public final List<c> a(String str, i iVar) {
        List<c> a2 = super.a(str, iVar);
        a2.add(new c("Content-type", "application/x-www-form-urlencoded"));
        return a2;
    }

    /* access modifiers changed from: protected */
    public String a(Map<String, String> map) {
        Map<String, String> a2 = a(d.POST, map);
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : a2.entrySet()) {
            try {
                arrayList.add(URLEncoder.encode((String) next.getKey(), "UTF-8") + "=" + URLEncoder.encode((String) next.getValue(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                throw RootAPIException.a(e, b.UNSUPPORTED_ENCODING_EXCEPTION);
            }
        }
        return k.a("&", arrayList);
    }
}
