package com.helpshift.common.c;

import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: One */
public class n extends l {

    /* renamed from: a  reason: collision with root package name */
    public final l f3290a;

    /* renamed from: b  reason: collision with root package name */
    private final AtomicBoolean f3291b = new AtomicBoolean(false);

    public n(l lVar) {
        this.f3290a = lVar;
    }

    public final void a() {
        if (this.f3291b.compareAndSet(false, true)) {
            try {
                this.f3290a.a();
            } finally {
                this.f3291b.set(false);
            }
        }
    }
}
