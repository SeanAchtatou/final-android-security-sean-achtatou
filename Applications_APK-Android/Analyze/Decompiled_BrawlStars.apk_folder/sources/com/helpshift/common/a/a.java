package com.helpshift.common.a;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.facebook.internal.ServerProtocol;
import com.helpshift.common.j;
import com.helpshift.common.k;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.l;
import com.helpshift.j.a.a.p;
import com.helpshift.j.a.a.s;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.w;
import com.helpshift.j.d.d;
import com.helpshift.support.Faq;
import com.helpshift.util.e;
import com.helpshift.util.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ConversationDB */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f3235a;
    private final String A = "input_required";
    private final String B = "input_skip_label";
    private final String C = "input_placeholder";
    private final String D = "input_label";
    private final String E = "input_options";
    private final String F = "option_type";
    private final String G = "option_title";
    private final String H = "option_data";
    private final String I = "chatbot_info";
    private final String J = "has_next_bot";
    private final String K = "faqs";
    private final String L = "faq_title";
    private final String M = "faq_publish_id";
    private final String N = "faq_language";
    private final String O = "is_response_skipped";
    private final String P = "selected_option_data";
    private final String Q = "referred_message_type";
    private final String R = "bot_action_type";
    private final String S = "bot_ended_reason";
    private final String T = "message_sync_status";
    private final String U = "is_secure";
    private final String V = "is_message_empty";
    private final String W = "is_suggestion_read_event_sent";
    private final String X = "suggestion_read_faq_publish_id";
    private final String Y = "dt";
    private final String Z = "timezone_id";
    private final c aa = new c();
    private final com.helpshift.u.a.a ab;

    /* renamed from: b  reason: collision with root package name */
    private final String f3236b = "csat_rating";
    private final String c = "csat_state";
    private final String d = "csat_feedback";
    private final String e = "increment_message_count";
    private final String f = "ended_delegate_sent";
    private final String g = "image_draft_orig_name";
    private final String h = "image_draft_orig_size";
    private final String i = "image_draft_file_path";
    private final String j = "image_copy_done";
    private final String k = "is_autofilled_preissue";
    private final String l = "referredMessageId";
    private final String m = "rejected_reason";
    private final String n = "rejected_conv_id";
    private final String o = "is_answered";
    private final String p = "content_type";
    private final String q = "file_name";
    private final String r = "url";
    private final String s = "size";
    private final String t = "thumbnail_url";
    private final String u = "thumbnailFilePath";
    private final String v = "filePath";
    private final String w = "seen_cursor";
    private final String x = "seen_sync_status";
    private final String y = "read_at";
    private final String z = "input_keyboard";

    private a(Context context) {
        this.ab = new com.helpshift.u.a.a(context, this.aa);
    }

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (f3235a == null) {
                f3235a = new a(context);
            }
            aVar = f3235a;
        }
        return aVar;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:8:0x0023 */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r0v2, types: [com.helpshift.j.a.b.a] */
    /* JADX WARN: Type inference failed for: r0v3 */
    /* JADX WARN: Type inference failed for: r0v4 */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARN: Type inference failed for: r0v8 */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r11.close();
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        if (r11 != null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0023, code lost:
        if (r11 != null) goto L_0x0025;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003f A[SYNTHETIC, Splitter:B:24:0x003f] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.helpshift.j.a.b.a a(java.lang.String r11, java.lang.String[] r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 0
            com.helpshift.u.a.a r1 = r10.ab     // Catch:{ Exception -> 0x002d, all -> 0x002b }
            android.database.sqlite.SQLiteDatabase r2 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x002d, all -> 0x002b }
            com.helpshift.common.a.c r1 = r10.aa     // Catch:{ Exception -> 0x002d, all -> 0x002b }
            r1.getClass()     // Catch:{ Exception -> 0x002d, all -> 0x002b }
            java.lang.String r3 = "issues"
            r4 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r5 = r11
            r6 = r12
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x002d, all -> 0x002b }
            boolean r12 = r11.moveToFirst()     // Catch:{ Exception -> 0x0029 }
            if (r12 == 0) goto L_0x0023
            com.helpshift.j.a.b.a r0 = r10.a(r11)     // Catch:{ Exception -> 0x0029 }
        L_0x0023:
            if (r11 == 0) goto L_0x0039
        L_0x0025:
            r11.close()     // Catch:{ all -> 0x0043 }
            goto L_0x0039
        L_0x0029:
            r12 = move-exception
            goto L_0x002f
        L_0x002b:
            r12 = move-exception
            goto L_0x003d
        L_0x002d:
            r12 = move-exception
            r11 = r0
        L_0x002f:
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in read conversations with localId"
            com.helpshift.util.n.c(r1, r2, r12)     // Catch:{ all -> 0x003b }
            if (r11 == 0) goto L_0x0039
            goto L_0x0025
        L_0x0039:
            monitor-exit(r10)
            return r0
        L_0x003b:
            r12 = move-exception
            r0 = r11
        L_0x003d:
            if (r0 == 0) goto L_0x0042
            r0.close()     // Catch:{ all -> 0x0043 }
        L_0x0042:
            throw r12     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r11 = move-exception
            monitor-exit(r10)
            goto L_0x0047
        L_0x0046:
            throw r11
        L_0x0047:
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.a(java.lang.String, java.lang.String[]):com.helpshift.j.a.b.a");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0061, code lost:
        if (r1 == null) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0051, code lost:
        if (r1 != null) goto L_0x0053;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.helpshift.j.a.b.a> a(long r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x006c }
            r0.<init>()     // Catch:{ all -> 0x006c }
            r1 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x006c }
            r2.<init>()     // Catch:{ all -> 0x006c }
            com.helpshift.common.a.c r3 = r12.aa     // Catch:{ all -> 0x006c }
            r3.getClass()     // Catch:{ all -> 0x006c }
            java.lang.String r3 = "user_local_id"
            r2.append(r3)     // Catch:{ all -> 0x006c }
            java.lang.String r3 = " = ?"
            r2.append(r3)     // Catch:{ all -> 0x006c }
            java.lang.String r7 = r2.toString()     // Catch:{ all -> 0x006c }
            r2 = 1
            java.lang.String[] r8 = new java.lang.String[r2]     // Catch:{ all -> 0x006c }
            r2 = 0
            java.lang.String r13 = java.lang.String.valueOf(r13)     // Catch:{ all -> 0x006c }
            r8[r2] = r13     // Catch:{ all -> 0x006c }
            com.helpshift.u.a.a r13 = r12.ab     // Catch:{ Exception -> 0x0059 }
            android.database.sqlite.SQLiteDatabase r4 = r13.getReadableDatabase()     // Catch:{ Exception -> 0x0059 }
            com.helpshift.common.a.c r13 = r12.aa     // Catch:{ Exception -> 0x0059 }
            r13.getClass()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r5 = "issues"
            r6 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r1 = r4.query(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x0059 }
            boolean r13 = r1.moveToFirst()     // Catch:{ Exception -> 0x0059 }
            if (r13 == 0) goto L_0x0051
        L_0x0044:
            com.helpshift.j.a.b.a r13 = r12.a(r1)     // Catch:{ Exception -> 0x0059 }
            r0.add(r13)     // Catch:{ Exception -> 0x0059 }
            boolean r13 = r1.moveToNext()     // Catch:{ Exception -> 0x0059 }
            if (r13 != 0) goto L_0x0044
        L_0x0051:
            if (r1 == 0) goto L_0x0064
        L_0x0053:
            r1.close()     // Catch:{ all -> 0x006c }
            goto L_0x0064
        L_0x0057:
            r13 = move-exception
            goto L_0x0066
        L_0x0059:
            r13 = move-exception
            java.lang.String r14 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in read conversations with localId"
            com.helpshift.util.n.c(r14, r2, r13)     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x0064
            goto L_0x0053
        L_0x0064:
            monitor-exit(r12)
            return r0
        L_0x0066:
            if (r1 == 0) goto L_0x006b
            r1.close()     // Catch:{ all -> 0x006c }
        L_0x006b:
            throw r13     // Catch:{ all -> 0x006c }
        L_0x006c:
            r13 = move-exception
            monitor-exit(r12)
            goto L_0x0070
        L_0x006f:
            throw r13
        L_0x0070:
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.a(long):java.util.List");
    }

    public final synchronized com.helpshift.j.a.b.a a(Long l2) {
        StringBuilder sb;
        sb = new StringBuilder();
        this.aa.getClass();
        sb.append("_id");
        sb.append(" = ?");
        return a(sb.toString(), new String[]{String.valueOf(l2)});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0063, code lost:
        r1 = "Helpshift_ConverDB";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r6 = "Exception in ending transaction deleteConversationWithLocalId : " + r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x007a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a1, code lost:
        if (r3 != null) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r3.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a7, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        com.helpshift.util.n.c("Helpshift_ConverDB", "Exception in ending transaction deleteConversationWithLocalId : " + r6, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00be, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0062, code lost:
        r0 = e;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:7:0x005e, B:17:0x0081] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(long r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bf }
            r0.<init>()     // Catch:{ all -> 0x00bf }
            com.helpshift.common.a.c r1 = r5.aa     // Catch:{ all -> 0x00bf }
            r1.getClass()     // Catch:{ all -> 0x00bf }
            java.lang.String r1 = "_id"
            r0.append(r1)     // Catch:{ all -> 0x00bf }
            java.lang.String r1 = " = ?"
            r0.append(r1)     // Catch:{ all -> 0x00bf }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00bf }
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ all -> 0x00bf }
            r2 = 0
            java.lang.String r3 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x00bf }
            r1[r2] = r3     // Catch:{ all -> 0x00bf }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bf }
            r2.<init>()     // Catch:{ all -> 0x00bf }
            com.helpshift.common.a.c r3 = r5.aa     // Catch:{ all -> 0x00bf }
            r3.getClass()     // Catch:{ all -> 0x00bf }
            java.lang.String r3 = "conversation_id"
            r2.append(r3)     // Catch:{ all -> 0x00bf }
            java.lang.String r3 = " = ?"
            r2.append(r3)     // Catch:{ all -> 0x00bf }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00bf }
            r3 = 0
            com.helpshift.u.a.a r4 = r5.ab     // Catch:{ Exception -> 0x007c }
            android.database.sqlite.SQLiteDatabase r3 = r4.getWritableDatabase()     // Catch:{ Exception -> 0x007c }
            r3.beginTransaction()     // Catch:{ Exception -> 0x007c }
            com.helpshift.common.a.c r4 = r5.aa     // Catch:{ Exception -> 0x007c }
            r4.getClass()     // Catch:{ Exception -> 0x007c }
            java.lang.String r4 = "issues"
            r3.delete(r4, r0, r1)     // Catch:{ Exception -> 0x007c }
            com.helpshift.common.a.c r0 = r5.aa     // Catch:{ Exception -> 0x007c }
            r0.getClass()     // Catch:{ Exception -> 0x007c }
            java.lang.String r0 = "messages"
            r3.delete(r0, r2, r1)     // Catch:{ Exception -> 0x007c }
            r3.setTransactionSuccessful()     // Catch:{ Exception -> 0x007c }
            if (r3 == 0) goto L_0x009f
            r3.endTransaction()     // Catch:{ Exception -> 0x0062 }
            goto L_0x009f
        L_0x0062:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bf }
            r2.<init>()     // Catch:{ all -> 0x00bf }
            java.lang.String r3 = "Exception in ending transaction deleteConversationWithLocalId : "
            r2.append(r3)     // Catch:{ all -> 0x00bf }
            r2.append(r6)     // Catch:{ all -> 0x00bf }
            java.lang.String r6 = r2.toString()     // Catch:{ all -> 0x00bf }
        L_0x0076:
            com.helpshift.util.n.c(r1, r6, r0)     // Catch:{ all -> 0x00bf }
            goto L_0x009f
        L_0x007a:
            r0 = move-exception
            goto L_0x00a1
        L_0x007c:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in delete conversation with localId"
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x007a }
            if (r3 == 0) goto L_0x009f
            r3.endTransaction()     // Catch:{ Exception -> 0x008a }
            goto L_0x009f
        L_0x008a:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bf }
            r2.<init>()     // Catch:{ all -> 0x00bf }
            java.lang.String r3 = "Exception in ending transaction deleteConversationWithLocalId : "
            r2.append(r3)     // Catch:{ all -> 0x00bf }
            r2.append(r6)     // Catch:{ all -> 0x00bf }
            java.lang.String r6 = r2.toString()     // Catch:{ all -> 0x00bf }
            goto L_0x0076
        L_0x009f:
            monitor-exit(r5)
            return
        L_0x00a1:
            if (r3 == 0) goto L_0x00be
            r3.endTransaction()     // Catch:{ Exception -> 0x00a7 }
            goto L_0x00be
        L_0x00a7:
            r1 = move-exception
            java.lang.String r2 = "Helpshift_ConverDB"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bf }
            r3.<init>()     // Catch:{ all -> 0x00bf }
            java.lang.String r4 = "Exception in ending transaction deleteConversationWithLocalId : "
            r3.append(r4)     // Catch:{ all -> 0x00bf }
            r3.append(r6)     // Catch:{ all -> 0x00bf }
            java.lang.String r6 = r3.toString()     // Catch:{ all -> 0x00bf }
            com.helpshift.util.n.c(r2, r6, r1)     // Catch:{ all -> 0x00bf }
        L_0x00be:
            throw r0     // Catch:{ all -> 0x00bf }
        L_0x00bf:
            r6 = move-exception
            monitor-exit(r5)
            goto L_0x00c3
        L_0x00c2:
            throw r6
        L_0x00c3:
            goto L_0x00c2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.b(long):void");
    }

    public final synchronized com.helpshift.j.a.b.a a(String str) {
        StringBuilder sb;
        sb = new StringBuilder();
        this.aa.getClass();
        sb.append("server_id");
        sb.append(" = ?");
        return a(sb.toString(), new String[]{String.valueOf(str)});
    }

    public final synchronized com.helpshift.j.a.b.a b(String str) {
        StringBuilder sb;
        sb = new StringBuilder();
        this.aa.getClass();
        sb.append("pre_conv_server_id");
        sb.append(" = ?");
        return a(sb.toString(), new String[]{String.valueOf(str)});
    }

    public final synchronized long a(com.helpshift.j.a.b.a aVar) {
        long j2;
        ContentValues c2 = c(aVar);
        j2 = -1;
        try {
            SQLiteDatabase writableDatabase = this.ab.getWritableDatabase();
            this.aa.getClass();
            j2 = writableDatabase.insert("issues", null, c2);
        } catch (Exception e2) {
            n.c("Helpshift_ConverDB", "Error in insert conversation", e2);
        }
        return j2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x007c A[SYNTHETIC, Splitter:B:38:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008a A[SYNTHETIC, Splitter:B:45:0x008a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<java.lang.Long> a(java.util.List<com.helpshift.j.a.b.a> r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            int r0 = r6.size()     // Catch:{ all -> 0x0097 }
            r1 = 0
            if (r0 != 0) goto L_0x000a
            monitor-exit(r5)
            return r1
        L_0x000a:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0097 }
            r0.<init>()     // Catch:{ all -> 0x0097 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x0097 }
        L_0x0013:
            boolean r2 = r6.hasNext()     // Catch:{ all -> 0x0097 }
            if (r2 == 0) goto L_0x0027
            java.lang.Object r2 = r6.next()     // Catch:{ all -> 0x0097 }
            com.helpshift.j.a.b.a r2 = (com.helpshift.j.a.b.a) r2     // Catch:{ all -> 0x0097 }
            android.content.ContentValues r2 = r5.c(r2)     // Catch:{ all -> 0x0097 }
            r0.add(r2)     // Catch:{ all -> 0x0097 }
            goto L_0x0013
        L_0x0027:
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x0097 }
            r6.<init>()     // Catch:{ all -> 0x0097 }
            com.helpshift.u.a.a r2 = r5.ab     // Catch:{ Exception -> 0x0072 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Exception -> 0x0072 }
            r2.beginTransaction()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
        L_0x0039:
            boolean r3 = r0.hasNext()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            if (r3 == 0) goto L_0x0058
            java.lang.Object r3 = r0.next()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            android.content.ContentValues r3 = (android.content.ContentValues) r3     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            com.helpshift.common.a.c r4 = r5.aa     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            r4.getClass()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.lang.String r4 = "issues"
            long r3 = r2.insert(r4, r1, r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            r6.add(r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            goto L_0x0039
        L_0x0058:
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            if (r2 == 0) goto L_0x0086
            r2.endTransaction()     // Catch:{ Exception -> 0x0061 }
            goto L_0x0086
        L_0x0061:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert conversations inside finally block"
        L_0x0066:
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x0097 }
            goto L_0x0086
        L_0x006a:
            r6 = move-exception
            goto L_0x0088
        L_0x006c:
            r0 = move-exception
            r1 = r2
            goto L_0x0073
        L_0x006f:
            r6 = move-exception
            r2 = r1
            goto L_0x0088
        L_0x0072:
            r0 = move-exception
        L_0x0073:
            java.lang.String r2 = "Helpshift_ConverDB"
            java.lang.String r3 = "Error in insert conversations"
            com.helpshift.util.n.c(r2, r3, r0)     // Catch:{ all -> 0x006f }
            if (r1 == 0) goto L_0x0086
            r1.endTransaction()     // Catch:{ Exception -> 0x0080 }
            goto L_0x0086
        L_0x0080:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert conversations inside finally block"
            goto L_0x0066
        L_0x0086:
            monitor-exit(r5)
            return r6
        L_0x0088:
            if (r2 == 0) goto L_0x0096
            r2.endTransaction()     // Catch:{ Exception -> 0x008e }
            goto L_0x0096
        L_0x008e:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert conversations inside finally block"
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x0097 }
        L_0x0096:
            throw r6     // Catch:{ all -> 0x0097 }
        L_0x0097:
            r6 = move-exception
            monitor-exit(r5)
            goto L_0x009b
        L_0x009a:
            throw r6
        L_0x009b:
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.a(java.util.List):java.util.List");
    }

    public final synchronized void b(com.helpshift.j.a.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(aVar);
        b(arrayList);
    }

    public final synchronized void a(Long l2, long j2) {
        ContentValues contentValues = new ContentValues();
        this.aa.getClass();
        contentValues.put("last_user_activity_time", Long.valueOf(j2));
        StringBuilder sb = new StringBuilder();
        this.aa.getClass();
        sb.append("_id");
        sb.append(" = ?");
        String sb2 = sb.toString();
        String[] strArr = {String.valueOf(l2)};
        try {
            SQLiteDatabase writableDatabase = this.ab.getWritableDatabase();
            this.aa.getClass();
            writableDatabase.update("issues", contentValues, sb2, strArr);
        } catch (Exception e2) {
            n.c("Helpshift_ConverDB", "Error in updateLastUserActivityTimeInConversation", e2);
        }
        return;
    }

    public final synchronized void b(List<com.helpshift.j.a.b.a> list) {
        int i2;
        String str;
        String str2;
        if (list.size() != 0) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Iterator<com.helpshift.j.a.b.a> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                com.helpshift.j.a.b.a next = it.next();
                arrayList.add(c(next));
                arrayList2.add(new String[]{String.valueOf(next.f3504b)});
            }
            SQLiteDatabase sQLiteDatabase = null;
            StringBuilder sb = new StringBuilder();
            this.aa.getClass();
            sb.append("_id");
            sb.append(" = ?");
            String sb2 = sb.toString();
            try {
                SQLiteDatabase writableDatabase = this.ab.getWritableDatabase();
                writableDatabase.beginTransaction();
                for (i2 = 0; i2 < list.size(); i2++) {
                    this.aa.getClass();
                    writableDatabase.update("issues", (ContentValues) arrayList.get(i2), sb2, (String[]) arrayList2.get(i2));
                }
                writableDatabase.setTransactionSuccessful();
                if (writableDatabase != null) {
                    try {
                        writableDatabase.endTransaction();
                    } catch (Exception e2) {
                        e = e2;
                        String str3 = "Helpshift_ConverDB";
                        String str4 = "Error in update conversations inside finally block";
                    }
                }
            } catch (Exception e3) {
                try {
                    n.c("Helpshift_ConverDB", "Error in update conversations", e3);
                } finally {
                    if (sQLiteDatabase != null) {
                        try {
                            sQLiteDatabase.endTransaction();
                        } catch (Exception e4) {
                            str = "Helpshift_ConverDB";
                            str2 = "Error in update conversations inside finally block";
                            n.c(str, str2, e4);
                        }
                    }
                }
            }
        }
    }

    public final synchronized com.helpshift.j.d.a.a a(com.helpshift.j.d.a.a aVar) {
        StringBuilder sb = new StringBuilder();
        this.aa.getClass();
        sb.append("user_local_id");
        sb.append(" = ?");
        String sb2 = sb.toString();
        String[] strArr = {String.valueOf(aVar.f3582a)};
        ContentValues b2 = b(aVar);
        try {
            SQLiteDatabase writableDatabase = this.ab.getWritableDatabase();
            this.aa.getClass();
            if (a(writableDatabase, "conversation_inbox", sb2, strArr)) {
                this.aa.getClass();
                writableDatabase.update("conversation_inbox", b2, sb2, strArr);
            } else {
                this.aa.getClass();
                writableDatabase.insert("conversation_inbox", null, b2);
            }
        } catch (Exception e2) {
            n.c("Helpshift_ConverDB", "Error in store conversation inbox record", e2);
        }
        return aVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0114, code lost:
        if (r3 != null) goto L_0x0116;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0128, code lost:
        if (r3 != null) goto L_0x0116;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0130 A[SYNTHETIC, Splitter:B:32:0x0130] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.helpshift.j.d.a.a c(long r29) {
        /*
            r28 = this;
            r1 = r28
            monitor-enter(r28)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0134 }
            r0.<init>()     // Catch:{ all -> 0x0134 }
            com.helpshift.common.a.c r2 = r1.aa     // Catch:{ all -> 0x0134 }
            r2.getClass()     // Catch:{ all -> 0x0134 }
            java.lang.String r2 = "user_local_id"
            r0.append(r2)     // Catch:{ all -> 0x0134 }
            java.lang.String r2 = " = ?"
            r0.append(r2)     // Catch:{ all -> 0x0134 }
            java.lang.String r6 = r0.toString()     // Catch:{ all -> 0x0134 }
            r0 = 1
            java.lang.String[] r7 = new java.lang.String[r0]     // Catch:{ all -> 0x0134 }
            java.lang.String r2 = java.lang.String.valueOf(r29)     // Catch:{ all -> 0x0134 }
            r11 = 0
            r7[r11] = r2     // Catch:{ all -> 0x0134 }
            r2 = 0
            com.helpshift.u.a.a r3 = r1.ab     // Catch:{ Exception -> 0x011f, all -> 0x011c }
            android.database.sqlite.SQLiteDatabase r3 = r3.getReadableDatabase()     // Catch:{ Exception -> 0x011f, all -> 0x011c }
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011f, all -> 0x011c }
            r4.getClass()     // Catch:{ Exception -> 0x011f, all -> 0x011c }
            java.lang.String r4 = "conversation_inbox"
            r5 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r3 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x011f, all -> 0x011c }
            boolean r4 = r3.moveToFirst()     // Catch:{ Exception -> 0x011a }
            if (r4 == 0) goto L_0x0114
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011a }
            r4.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = "user_local_id"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x011a }
            long r13 = r3.getLong(r4)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011a }
            r4.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = "form_name"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x011a }
            java.lang.String r15 = r3.getString(r4)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011a }
            r4.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = "form_email"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x011a }
            java.lang.String r16 = r3.getString(r4)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011a }
            r4.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = "description_draft"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x011a }
            java.lang.String r17 = r3.getString(r4)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011a }
            r4.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = "description_draft_timestamp"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x011a }
            long r18 = r3.getLong(r4)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011a }
            r4.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = "attachment_draft"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x011a }
            com.helpshift.j.d.d r20 = d(r4)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011a }
            r4.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = "description_type"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x011a }
            int r21 = r3.getInt(r4)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011a }
            r4.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = "archival_text"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x011a }
            java.lang.String r22 = r3.getString(r4)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011a }
            r4.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = "reply_text"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x011a }
            java.lang.String r23 = r3.getString(r4)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r4 = r1.aa     // Catch:{ Exception -> 0x011a }
            r4.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r4 = "persist_message_box"
            int r4 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x011a }
            int r4 = r3.getInt(r4)     // Catch:{ Exception -> 0x011a }
            if (r4 != r0) goto L_0x00e0
            r24 = 1
            goto L_0x00e2
        L_0x00e0:
            r24 = 0
        L_0x00e2:
            com.helpshift.common.a.c r0 = r1.aa     // Catch:{ Exception -> 0x011a }
            r0.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r0 = "since"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x011a }
            java.lang.String r25 = r3.getString(r0)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r0 = r1.aa     // Catch:{ Exception -> 0x011a }
            r0.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r0 = "has_older_messages"
            java.lang.Boolean r26 = com.helpshift.util.e.a(r3, r0)     // Catch:{ Exception -> 0x011a }
            com.helpshift.common.a.c r0 = r1.aa     // Catch:{ Exception -> 0x011a }
            r0.getClass()     // Catch:{ Exception -> 0x011a }
            java.lang.String r0 = "last_conv_redaction_time"
            java.lang.Class<java.lang.Long> r4 = java.lang.Long.class
            java.lang.Object r0 = com.helpshift.util.e.a(r3, r0, r4)     // Catch:{ Exception -> 0x011a }
            r27 = r0
            java.lang.Long r27 = (java.lang.Long) r27     // Catch:{ Exception -> 0x011a }
            com.helpshift.j.d.a.a r0 = new com.helpshift.j.d.a.a     // Catch:{ Exception -> 0x011a }
            r12 = r0
            r12.<init>(r13, r15, r16, r17, r18, r20, r21, r22, r23, r24, r25, r26, r27)     // Catch:{ Exception -> 0x011a }
            r2 = r0
        L_0x0114:
            if (r3 == 0) goto L_0x012b
        L_0x0116:
            r3.close()     // Catch:{ all -> 0x0134 }
            goto L_0x012b
        L_0x011a:
            r0 = move-exception
            goto L_0x0121
        L_0x011c:
            r0 = move-exception
            r3 = r2
            goto L_0x012e
        L_0x011f:
            r0 = move-exception
            r3 = r2
        L_0x0121:
            java.lang.String r4 = "Helpshift_ConverDB"
            java.lang.String r5 = "Error in read conversation inbox record"
            com.helpshift.util.n.c(r4, r5, r0)     // Catch:{ all -> 0x012d }
            if (r3 == 0) goto L_0x012b
            goto L_0x0116
        L_0x012b:
            monitor-exit(r28)
            return r2
        L_0x012d:
            r0 = move-exception
        L_0x012e:
            if (r3 == 0) goto L_0x0133
            r3.close()     // Catch:{ all -> 0x0134 }
        L_0x0133:
            throw r0     // Catch:{ all -> 0x0134 }
        L_0x0134:
            r0 = move-exception
            monitor-exit(r28)
            goto L_0x0138
        L_0x0137:
            throw r0
        L_0x0138:
            goto L_0x0137
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.c(long):com.helpshift.j.d.a.a");
    }

    public final synchronized long a(v vVar) {
        long j2;
        ContentValues c2 = c(vVar);
        j2 = -1;
        try {
            SQLiteDatabase writableDatabase = this.ab.getWritableDatabase();
            this.aa.getClass();
            j2 = writableDatabase.insert("messages", null, c2);
        } catch (Exception e2) {
            n.c("Helpshift_ConverDB", "Error in insert message", e2);
        }
        return j2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x007c A[SYNTHETIC, Splitter:B:38:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008a A[SYNTHETIC, Splitter:B:45:0x008a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<java.lang.Long> c(java.util.List<com.helpshift.j.a.a.v> r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            int r0 = r6.size()     // Catch:{ all -> 0x0097 }
            r1 = 0
            if (r0 != 0) goto L_0x000a
            monitor-exit(r5)
            return r1
        L_0x000a:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0097 }
            r0.<init>()     // Catch:{ all -> 0x0097 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x0097 }
        L_0x0013:
            boolean r2 = r6.hasNext()     // Catch:{ all -> 0x0097 }
            if (r2 == 0) goto L_0x0027
            java.lang.Object r2 = r6.next()     // Catch:{ all -> 0x0097 }
            com.helpshift.j.a.a.v r2 = (com.helpshift.j.a.a.v) r2     // Catch:{ all -> 0x0097 }
            android.content.ContentValues r2 = r5.c(r2)     // Catch:{ all -> 0x0097 }
            r0.add(r2)     // Catch:{ all -> 0x0097 }
            goto L_0x0013
        L_0x0027:
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x0097 }
            r6.<init>()     // Catch:{ all -> 0x0097 }
            com.helpshift.u.a.a r2 = r5.ab     // Catch:{ Exception -> 0x0072 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Exception -> 0x0072 }
            r2.beginTransaction()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
        L_0x0039:
            boolean r3 = r0.hasNext()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            if (r3 == 0) goto L_0x0058
            java.lang.Object r3 = r0.next()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            android.content.ContentValues r3 = (android.content.ContentValues) r3     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            com.helpshift.common.a.c r4 = r5.aa     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            r4.getClass()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.lang.String r4 = "messages"
            long r3 = r2.insert(r4, r1, r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            r6.add(r3)     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            goto L_0x0039
        L_0x0058:
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x006c, all -> 0x006a }
            if (r2 == 0) goto L_0x0086
            r2.endTransaction()     // Catch:{ Exception -> 0x0061 }
            goto L_0x0086
        L_0x0061:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert messages inside finally block"
        L_0x0066:
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x0097 }
            goto L_0x0086
        L_0x006a:
            r6 = move-exception
            goto L_0x0088
        L_0x006c:
            r0 = move-exception
            r1 = r2
            goto L_0x0073
        L_0x006f:
            r6 = move-exception
            r2 = r1
            goto L_0x0088
        L_0x0072:
            r0 = move-exception
        L_0x0073:
            java.lang.String r2 = "Helpshift_ConverDB"
            java.lang.String r3 = "Error in insert messages"
            com.helpshift.util.n.c(r2, r3, r0)     // Catch:{ all -> 0x006f }
            if (r1 == 0) goto L_0x0086
            r1.endTransaction()     // Catch:{ Exception -> 0x0080 }
            goto L_0x0086
        L_0x0080:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert messages inside finally block"
            goto L_0x0066
        L_0x0086:
            monitor-exit(r5)
            return r6
        L_0x0088:
            if (r2 == 0) goto L_0x0096
            r2.endTransaction()     // Catch:{ Exception -> 0x008e }
            goto L_0x0096
        L_0x008e:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in insert messages inside finally block"
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x0097 }
        L_0x0096:
            throw r6     // Catch:{ all -> 0x0097 }
        L_0x0097:
            r6 = move-exception
            monitor-exit(r5)
            goto L_0x009b
        L_0x009a:
            throw r6
        L_0x009b:
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.c(java.util.List):java.util.List");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:93:0x001f */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v2, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v7, types: [android.database.Cursor] */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a2, code lost:
        r13 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ac, code lost:
        if (r2 == 0) goto L_0x00f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b2, code lost:
        if (r2 != 0) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b4, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b7, code lost:
        throw r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b8, code lost:
        if (r2 != 0) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00da, code lost:
        r13 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e4, code lost:
        if (r1 == null) goto L_0x00f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00ea, code lost:
        if (r1 != null) goto L_0x00ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00ec, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00ef, code lost:
        throw r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00f0, code lost:
        if (r1 == null) goto L_0x00f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0105, code lost:
        r13 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x010f, code lost:
        if (r2 == 0) goto L_0x011e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0115, code lost:
        if (r2 != 0) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0117, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x011a, code lost:
        throw r13;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:31:0x00a9, B:58:0x00e1, B:79:0x010c] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00d0 A[SYNTHETIC, Splitter:B:51:0x00d0] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00fb A[SYNTHETIC, Splitter:B:72:0x00fb] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x011d A[Catch:{ all -> 0x0105, all -> 0x00da, all -> 0x00a2 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:82:0x0111=Splitter:B:82:0x0111, B:61:0x00e6=Splitter:B:61:0x00e6, B:34:0x00ae=Splitter:B:34:0x00ae} */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.helpshift.j.a.a.v> a(java.util.Collection<java.lang.Long> r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x011f }
            r0.<init>()     // Catch:{ all -> 0x011f }
            r1 = 900(0x384, float:1.261E-42)
            r2 = 0
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x00c5, all -> 0x00c2 }
            r3.<init>(r13)     // Catch:{ Exception -> 0x00c5, all -> 0x00c2 }
            java.util.List r13 = com.helpshift.util.e.a(r1, r3)     // Catch:{ Exception -> 0x00c5, all -> 0x00c2 }
            com.helpshift.u.a.a r1 = r12.ab     // Catch:{ Exception -> 0x00c5, all -> 0x00c2 }
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x00c5, all -> 0x00c2 }
            r1.beginTransaction()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.util.Iterator r13 = r13.iterator()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
        L_0x001f:
            boolean r3 = r13.hasNext()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            if (r3 == 0) goto L_0x0093
            java.lang.Object r3 = r13.next()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.util.List r3 = (java.util.List) r3     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            int r4 = r3.size()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.lang.String r4 = com.helpshift.util.e.a(r4)     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            r5.<init>()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            com.helpshift.common.a.c r6 = r12.aa     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            r6.getClass()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.lang.String r6 = "conversation_id"
            r5.append(r6)     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.lang.String r6 = " IN ("
            r5.append(r6)     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            r5.append(r4)     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.lang.String r4 = ")"
            r5.append(r4)     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.lang.String r6 = r5.toString()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            int r4 = r3.size()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.lang.String[] r7 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            r4 = 0
        L_0x005a:
            int r5 = r3.size()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            if (r4 >= r5) goto L_0x006d
            java.lang.Object r5 = r3.get(r4)     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            r7[r4] = r5     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            int r4 = r4 + 1
            goto L_0x005a
        L_0x006d:
            com.helpshift.common.a.c r3 = r12.aa     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            r3.getClass()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            java.lang.String r4 = "messages"
            r5 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r3 = r1
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            boolean r3 = r2.moveToFirst()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            if (r3 == 0) goto L_0x001f
        L_0x0083:
            com.helpshift.j.a.a.v r3 = r12.b(r2)     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            if (r3 == 0) goto L_0x008c
            r0.add(r3)     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
        L_0x008c:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            if (r3 != 0) goto L_0x0083
            goto L_0x001f
        L_0x0093:
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x00bd, all -> 0x00bb }
            if (r1 == 0) goto L_0x00b8
            boolean r13 = r1.inTransaction()     // Catch:{ Exception -> 0x00a4 }
            if (r13 == 0) goto L_0x00b8
            r1.endTransaction()     // Catch:{ Exception -> 0x00a4 }
            goto L_0x00b8
        L_0x00a2:
            r13 = move-exception
            goto L_0x00b2
        L_0x00a4:
            r13 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r3 = "Error in read messages inside finally block, "
            com.helpshift.util.n.c(r1, r3, r13)     // Catch:{ all -> 0x00a2 }
            if (r2 == 0) goto L_0x00f3
        L_0x00ae:
            r2.close()     // Catch:{ all -> 0x011f }
            goto L_0x00f3
        L_0x00b2:
            if (r2 == 0) goto L_0x00b7
            r2.close()     // Catch:{ all -> 0x011f }
        L_0x00b7:
            throw r13     // Catch:{ all -> 0x011f }
        L_0x00b8:
            if (r2 == 0) goto L_0x00f3
            goto L_0x00ae
        L_0x00bb:
            r13 = move-exception
            goto L_0x00f9
        L_0x00bd:
            r13 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
            goto L_0x00c7
        L_0x00c2:
            r13 = move-exception
            r1 = r2
            goto L_0x00f9
        L_0x00c5:
            r13 = move-exception
            r1 = r2
        L_0x00c7:
            java.lang.String r3 = "Helpshift_ConverDB"
            java.lang.String r4 = "Error in read messages"
            com.helpshift.util.n.c(r3, r4, r13)     // Catch:{ all -> 0x00f5 }
            if (r2 == 0) goto L_0x00f0
            boolean r13 = r2.inTransaction()     // Catch:{ Exception -> 0x00dc }
            if (r13 == 0) goto L_0x00f0
            r2.endTransaction()     // Catch:{ Exception -> 0x00dc }
            goto L_0x00f0
        L_0x00da:
            r13 = move-exception
            goto L_0x00ea
        L_0x00dc:
            r13 = move-exception
            java.lang.String r2 = "Helpshift_ConverDB"
            java.lang.String r3 = "Error in read messages inside finally block, "
            com.helpshift.util.n.c(r2, r3, r13)     // Catch:{ all -> 0x00da }
            if (r1 == 0) goto L_0x00f3
        L_0x00e6:
            r1.close()     // Catch:{ all -> 0x011f }
            goto L_0x00f3
        L_0x00ea:
            if (r1 == 0) goto L_0x00ef
            r1.close()     // Catch:{ all -> 0x011f }
        L_0x00ef:
            throw r13     // Catch:{ all -> 0x011f }
        L_0x00f0:
            if (r1 == 0) goto L_0x00f3
            goto L_0x00e6
        L_0x00f3:
            monitor-exit(r12)
            return r0
        L_0x00f5:
            r13 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
        L_0x00f9:
            if (r1 == 0) goto L_0x011b
            boolean r0 = r1.inTransaction()     // Catch:{ Exception -> 0x0107 }
            if (r0 == 0) goto L_0x011b
            r1.endTransaction()     // Catch:{ Exception -> 0x0107 }
            goto L_0x011b
        L_0x0105:
            r13 = move-exception
            goto L_0x0115
        L_0x0107:
            r0 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r3 = "Error in read messages inside finally block, "
            com.helpshift.util.n.c(r1, r3, r0)     // Catch:{ all -> 0x0105 }
            if (r2 == 0) goto L_0x011e
        L_0x0111:
            r2.close()     // Catch:{ all -> 0x011f }
            goto L_0x011e
        L_0x0115:
            if (r2 == 0) goto L_0x011a
            r2.close()     // Catch:{ all -> 0x011f }
        L_0x011a:
            throw r13     // Catch:{ all -> 0x011f }
        L_0x011b:
            if (r2 == 0) goto L_0x011e
            goto L_0x0111
        L_0x011e:
            throw r13     // Catch:{ all -> 0x011f }
        L_0x011f:
            r13 = move-exception
            monitor-exit(r12)
            goto L_0x0123
        L_0x0122:
            throw r13
        L_0x0123:
            goto L_0x0122
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.a(java.util.Collection):java.util.List");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:96:0x0038 */
    /* JADX WARN: Type inference failed for: r2v1 */
    /* JADX WARN: Type inference failed for: r2v2, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v3, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v8, types: [android.database.Cursor] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:33:0x0147, B:60:0x017e, B:81:0x01a8] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x016d A[SYNTHETIC, Splitter:B:53:0x016d] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0197 A[SYNTHETIC, Splitter:B:74:0x0197] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01b9 A[Catch:{ all -> 0x01a1, all -> 0x0177, all -> 0x0140 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:63:0x0183=Splitter:B:63:0x0183, B:36:0x014c=Splitter:B:36:0x014c, B:84:0x01ad=Splitter:B:84:0x01ad} */
    public final synchronized java.util.Map<java.lang.Long, java.lang.Integer> a(java.util.List<java.lang.Long> r14, java.lang.String[] r15) {
        /*
            r13 = this;
            monitor-enter(r13)
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x01bb }
            r0.<init>()     // Catch:{ all -> 0x01bb }
            java.util.Iterator r1 = r14.iterator()     // Catch:{ all -> 0x01bb }
        L_0x000a:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x01bb }
            r3 = 0
            if (r2 == 0) goto L_0x001f
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x01bb }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x01bb }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x01bb }
            r0.put(r2, r3)     // Catch:{ all -> 0x01bb }
            goto L_0x000a
        L_0x001f:
            r1 = 900(0x384, float:1.261E-42)
            r2 = 0
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x0162, all -> 0x015f }
            r4.<init>(r14)     // Catch:{ Exception -> 0x0162, all -> 0x015f }
            java.util.List r14 = com.helpshift.util.e.a(r1, r4)     // Catch:{ Exception -> 0x0162, all -> 0x015f }
            com.helpshift.u.a.a r1 = r13.ab     // Catch:{ Exception -> 0x0162, all -> 0x015f }
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x0162, all -> 0x015f }
            r1.beginTransaction()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.util.Iterator r14 = r14.iterator()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
        L_0x0038:
            boolean r4 = r14.hasNext()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            if (r4 == 0) goto L_0x0131
            java.lang.Object r4 = r14.next()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.util.List r4 = (java.util.List) r4     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            int r5 = r4.size()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r5 = com.helpshift.util.e.a(r5)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r6.<init>()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r7.<init>()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            com.helpshift.common.a.c r8 = r13.aa     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r8.getClass()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r8 = "conversation_id"
            r7.append(r8)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r8 = " IN ("
            r7.append(r8)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r7.append(r5)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r5 = ")"
            r7.append(r5)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r5 = r7.toString()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r6.append(r5)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r5.<init>()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
        L_0x007d:
            boolean r7 = r4.hasNext()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            if (r7 == 0) goto L_0x0091
            java.lang.Object r7 = r4.next()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r5.add(r7)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            goto L_0x007d
        L_0x0091:
            if (r15 == 0) goto L_0x00c7
            int r4 = r15.length     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r4 = com.helpshift.util.e.a(r4)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r7.<init>()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            com.helpshift.common.a.c r8 = r13.aa     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r8.getClass()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r8 = "type"
            r7.append(r8)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r8 = " IN ("
            r7.append(r8)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r7.append(r4)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r4 = ")"
            r7.append(r4)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r4 = r7.toString()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r7 = " AND "
            r6.append(r7)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r6.append(r4)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.util.List r4 = java.util.Arrays.asList(r15)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r5.addAll(r4)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
        L_0x00c7:
            int r4 = r5.size()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String[] r8 = new java.lang.String[r4]     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r5.toArray(r8)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            com.helpshift.common.a.c r4 = r13.aa     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r4.getClass()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r5 = "messages"
            r4 = 2
            java.lang.String[] r7 = new java.lang.String[r4]     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r4 = "COUNT(*) AS COUNT"
            r7[r3] = r4     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r4 = 1
            com.helpshift.common.a.c r9 = r13.aa     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r9.getClass()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r9 = "conversation_id"
            r7[r4] = r9     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r9 = r6.toString()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            com.helpshift.common.a.c r4 = r13.aa     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r4.getClass()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r10 = "conversation_id"
            r11 = 0
            r12 = 0
            r4 = r1
            r6 = r7
            r7 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            android.database.Cursor r2 = r4.query(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            boolean r4 = r2.moveToFirst()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            if (r4 == 0) goto L_0x0038
        L_0x0105:
            com.helpshift.common.a.c r4 = r13.aa     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r4.getClass()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r4 = "conversation_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            long r4 = r2.getLong(r4)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.String r6 = "COUNT"
            int r6 = r2.getColumnIndex(r6)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            int r6 = r2.getInt(r6)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            r0.put(r4, r5)     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            boolean r4 = r2.moveToNext()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            if (r4 != 0) goto L_0x0105
            goto L_0x0038
        L_0x0131:
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x015b, all -> 0x0159 }
            if (r1 == 0) goto L_0x0156
            boolean r14 = r1.inTransaction()     // Catch:{ Exception -> 0x0142 }
            if (r14 == 0) goto L_0x0156
            r1.endTransaction()     // Catch:{ Exception -> 0x0142 }
            goto L_0x0156
        L_0x0140:
            r14 = move-exception
            goto L_0x0150
        L_0x0142:
            r14 = move-exception
            java.lang.String r15 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in get messages count inside finally block, "
            com.helpshift.util.n.c(r15, r1, r14)     // Catch:{ all -> 0x0140 }
            if (r2 == 0) goto L_0x0190
        L_0x014c:
            r2.close()     // Catch:{ all -> 0x01bb }
            goto L_0x0190
        L_0x0150:
            if (r2 == 0) goto L_0x0155
            r2.close()     // Catch:{ all -> 0x01bb }
        L_0x0155:
            throw r14     // Catch:{ all -> 0x01bb }
        L_0x0156:
            if (r2 == 0) goto L_0x0190
            goto L_0x014c
        L_0x0159:
            r14 = move-exception
            goto L_0x0195
        L_0x015b:
            r14 = move-exception
            r15 = r2
            r2 = r1
            goto L_0x0164
        L_0x015f:
            r14 = move-exception
            r1 = r2
            goto L_0x0195
        L_0x0162:
            r14 = move-exception
            r15 = r2
        L_0x0164:
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r3 = "Error in get messages count"
            com.helpshift.util.n.c(r1, r3, r14)     // Catch:{ all -> 0x0192 }
            if (r2 == 0) goto L_0x018d
            boolean r14 = r2.inTransaction()     // Catch:{ Exception -> 0x0179 }
            if (r14 == 0) goto L_0x018d
            r2.endTransaction()     // Catch:{ Exception -> 0x0179 }
            goto L_0x018d
        L_0x0177:
            r14 = move-exception
            goto L_0x0187
        L_0x0179:
            r14 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in get messages count inside finally block, "
            com.helpshift.util.n.c(r1, r2, r14)     // Catch:{ all -> 0x0177 }
            if (r15 == 0) goto L_0x0190
        L_0x0183:
            r15.close()     // Catch:{ all -> 0x01bb }
            goto L_0x0190
        L_0x0187:
            if (r15 == 0) goto L_0x018c
            r15.close()     // Catch:{ all -> 0x01bb }
        L_0x018c:
            throw r14     // Catch:{ all -> 0x01bb }
        L_0x018d:
            if (r15 == 0) goto L_0x0190
            goto L_0x0183
        L_0x0190:
            monitor-exit(r13)
            return r0
        L_0x0192:
            r14 = move-exception
            r1 = r2
            r2 = r15
        L_0x0195:
            if (r1 == 0) goto L_0x01b7
            boolean r15 = r1.inTransaction()     // Catch:{ Exception -> 0x01a3 }
            if (r15 == 0) goto L_0x01b7
            r1.endTransaction()     // Catch:{ Exception -> 0x01a3 }
            goto L_0x01b7
        L_0x01a1:
            r14 = move-exception
            goto L_0x01b1
        L_0x01a3:
            r15 = move-exception
            java.lang.String r0 = "Helpshift_ConverDB"
            java.lang.String r1 = "Error in get messages count inside finally block, "
            com.helpshift.util.n.c(r0, r1, r15)     // Catch:{ all -> 0x01a1 }
            if (r2 == 0) goto L_0x01ba
        L_0x01ad:
            r2.close()     // Catch:{ all -> 0x01bb }
            goto L_0x01ba
        L_0x01b1:
            if (r2 == 0) goto L_0x01b6
            r2.close()     // Catch:{ all -> 0x01bb }
        L_0x01b6:
            throw r14     // Catch:{ all -> 0x01bb }
        L_0x01b7:
            if (r2 == 0) goto L_0x01ba
            goto L_0x01ad
        L_0x01ba:
            throw r14     // Catch:{ all -> 0x01bb }
        L_0x01bb:
            r14 = move-exception
            monitor-exit(r13)
            goto L_0x01bf
        L_0x01be:
            throw r14
        L_0x01bf:
            goto L_0x01be
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.a(java.util.List, java.lang.String[]):java.util.Map");
    }

    public final synchronized List<v> d(long j2) {
        StringBuilder sb;
        sb = new StringBuilder();
        this.aa.getClass();
        sb.append("conversation_id");
        sb.append(" = ?");
        return b(sb.toString(), new String[]{String.valueOf(j2)});
    }

    public final synchronized List<v> a(long j2, w wVar) {
        StringBuilder sb;
        sb = new StringBuilder();
        this.aa.getClass();
        sb.append("conversation_id");
        sb.append(" = ? AND ");
        this.aa.getClass();
        sb.append("type");
        sb.append(" = ?");
        return b(sb.toString(), new String[]{String.valueOf(j2), wVar.a()});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003f, code lost:
        if (r1 == null) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0041, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0044, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0032, code lost:
        if (r1 != null) goto L_0x0041;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.helpshift.j.a.a.v> b(java.lang.String r12, java.lang.String[] r13) {
        /*
            r11 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            com.helpshift.u.a.a r2 = r11.ab     // Catch:{ Exception -> 0x0037 }
            android.database.sqlite.SQLiteDatabase r3 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0037 }
            com.helpshift.common.a.c r2 = r11.aa     // Catch:{ Exception -> 0x0037 }
            r2.getClass()     // Catch:{ Exception -> 0x0037 }
            java.lang.String r4 = "messages"
            r5 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r6 = r12
            r7 = r13
            android.database.Cursor r1 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0037 }
            boolean r12 = r1.moveToFirst()     // Catch:{ Exception -> 0x0037 }
            if (r12 == 0) goto L_0x0032
        L_0x0023:
            com.helpshift.j.a.a.v r12 = r11.b(r1)     // Catch:{ Exception -> 0x0037 }
            if (r12 == 0) goto L_0x002c
            r0.add(r12)     // Catch:{ Exception -> 0x0037 }
        L_0x002c:
            boolean r12 = r1.moveToNext()     // Catch:{ Exception -> 0x0037 }
            if (r12 != 0) goto L_0x0023
        L_0x0032:
            if (r1 == 0) goto L_0x0044
            goto L_0x0041
        L_0x0035:
            r12 = move-exception
            goto L_0x0045
        L_0x0037:
            r12 = move-exception
            java.lang.String r13 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in read messages"
            com.helpshift.util.n.c(r13, r2, r12)     // Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0044
        L_0x0041:
            r1.close()
        L_0x0044:
            return r0
        L_0x0045:
            if (r1 == 0) goto L_0x004a
            r1.close()
        L_0x004a:
            goto L_0x004c
        L_0x004b:
            throw r12
        L_0x004c:
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.b(java.lang.String, java.lang.String[]):java.util.List");
    }

    public final synchronized void b(v vVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(vVar);
        d(arrayList);
    }

    public final synchronized void d(List<v> list) {
        int i2;
        String str;
        String str2;
        if (list.size() != 0) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Iterator<v> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                v next = it.next();
                arrayList.add(c(next));
                arrayList2.add(new String[]{String.valueOf(next.r)});
            }
            SQLiteDatabase sQLiteDatabase = null;
            StringBuilder sb = new StringBuilder();
            this.aa.getClass();
            sb.append("_id");
            sb.append(" = ?");
            String sb2 = sb.toString();
            try {
                SQLiteDatabase writableDatabase = this.ab.getWritableDatabase();
                writableDatabase.beginTransaction();
                for (i2 = 0; i2 < list.size(); i2++) {
                    this.aa.getClass();
                    writableDatabase.update("messages", (ContentValues) arrayList.get(i2), sb2, (String[]) arrayList2.get(i2));
                }
                writableDatabase.setTransactionSuccessful();
                if (writableDatabase != null) {
                    try {
                        writableDatabase.endTransaction();
                    } catch (Exception e2) {
                        e = e2;
                        String str3 = "Helpshift_ConverDB";
                        String str4 = "Error in update messages";
                    }
                }
            } catch (Exception e3) {
                try {
                    n.c("Helpshift_ConverDB", "Error in update messages", e3);
                } finally {
                    if (sQLiteDatabase != null) {
                        try {
                            sQLiteDatabase.endTransaction();
                        } catch (Exception e4) {
                            str = "Helpshift_ConverDB";
                            str2 = "Error in update messages";
                            n.c(str, str2, e4);
                        }
                    }
                }
            }
        }
    }

    public final synchronized boolean e(long j2) {
        StringBuilder sb = new StringBuilder();
        this.aa.getClass();
        sb.append("conversation_id");
        sb.append("= ? ");
        String sb2 = sb.toString();
        String[] strArr = {String.valueOf(j2)};
        try {
            SQLiteDatabase writableDatabase = this.ab.getWritableDatabase();
            this.aa.getClass();
            writableDatabase.delete("messages", sb2, strArr);
        } catch (Exception e2) {
            n.c("Helpshift_ConverDB", "Error deleting messages for : " + j2, e2);
            return false;
        }
        return true;
    }

    private static boolean a(SQLiteDatabase sQLiteDatabase, String str, String str2, String[] strArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(*) FROM ");
        sb.append(str);
        sb.append(" WHERE ");
        sb.append(str2);
        sb.append(" LIMIT 1");
        return DatabaseUtils.longForQuery(sQLiteDatabase, sb.toString(), strArr) > 0;
    }

    private ContentValues b(com.helpshift.j.d.a.a aVar) {
        String str;
        ContentValues contentValues = new ContentValues();
        this.aa.getClass();
        contentValues.put("user_local_id", Long.valueOf(aVar.f3582a));
        this.aa.getClass();
        contentValues.put("form_name", aVar.f3583b);
        this.aa.getClass();
        contentValues.put("form_email", aVar.c);
        this.aa.getClass();
        contentValues.put("description_draft", aVar.d);
        this.aa.getClass();
        contentValues.put("description_draft_timestamp", Long.valueOf(aVar.e));
        this.aa.getClass();
        contentValues.put("description_type", Integer.valueOf(aVar.g));
        this.aa.getClass();
        contentValues.put("archival_text", aVar.h);
        this.aa.getClass();
        contentValues.put("reply_text", aVar.i);
        this.aa.getClass();
        contentValues.put("persist_message_box", Integer.valueOf(aVar.j ? 1 : 0));
        this.aa.getClass();
        contentValues.put("since", aVar.k);
        if (aVar.l != null) {
            this.aa.getClass();
            contentValues.put("has_older_messages", Integer.valueOf(aVar.l.booleanValue() ? 1 : 0));
        }
        this.aa.getClass();
        contentValues.put("last_conv_redaction_time", aVar.m);
        try {
            d dVar = aVar.f;
            if (dVar == null) {
                str = null;
            } else {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("image_draft_orig_name", dVar.f3590a);
                jSONObject.put("image_draft_orig_size", dVar.f3591b);
                jSONObject.put("image_draft_file_path", dVar.d);
                jSONObject.put("image_copy_done", dVar.e);
                str = jSONObject.toString();
            }
            this.aa.getClass();
            contentValues.put("attachment_draft", str);
        } catch (JSONException e2) {
            n.c("Helpshift_ConverDB", "Error in generating meta string for image attachment", e2);
        }
        return contentValues;
    }

    private ContentValues c(com.helpshift.j.a.b.a aVar) {
        ContentValues contentValues = new ContentValues();
        this.aa.getClass();
        contentValues.put("user_local_id", Long.valueOf(aVar.t));
        this.aa.getClass();
        contentValues.put("server_id", aVar.c);
        this.aa.getClass();
        contentValues.put("pre_conv_server_id", aVar.d);
        this.aa.getClass();
        contentValues.put("publish_id", aVar.k);
        this.aa.getClass();
        contentValues.put("uuid", aVar.e);
        this.aa.getClass();
        contentValues.put("title", aVar.f);
        this.aa.getClass();
        contentValues.put("message_cursor", aVar.m);
        this.aa.getClass();
        contentValues.put("show_agent_name", Integer.valueOf(aVar.l ? 1 : 0));
        this.aa.getClass();
        contentValues.put("start_new_conversation_action", Integer.valueOf(aVar.s ? 1 : 0));
        this.aa.getClass();
        contentValues.put("created_at", aVar.e());
        this.aa.getClass();
        contentValues.put("updated_at", aVar.i);
        this.aa.getClass();
        contentValues.put("epoch_time_created_at", Long.valueOf(aVar.d()));
        this.aa.getClass();
        contentValues.put("last_user_activity_time", Long.valueOf(aVar.u));
        this.aa.getClass();
        contentValues.put("issue_type", aVar.h);
        this.aa.getClass();
        contentValues.put("full_privacy_enabled", Integer.valueOf(aVar.w ? 1 : 0));
        this.aa.getClass();
        contentValues.put(ServerProtocol.DIALOG_PARAM_STATE, Integer.valueOf(aVar.g == null ? -1 : aVar.g.a()));
        this.aa.getClass();
        contentValues.put("is_redacted", Integer.valueOf(aVar.x ? 1 : 0));
        try {
            com.helpshift.j.f.a aVar2 = aVar.p;
            JSONObject jSONObject = new JSONObject();
            String str = aVar.r;
            int i2 = aVar.q;
            jSONObject.put("csat_feedback", str);
            jSONObject.put("csat_rating", i2);
            jSONObject.put("csat_state", aVar2.a());
            jSONObject.put("increment_message_count", aVar.n);
            jSONObject.put("ended_delegate_sent", aVar.o);
            jSONObject.put("is_autofilled_preissue", aVar.D);
            String jSONObject2 = jSONObject.toString();
            this.aa.getClass();
            contentValues.put("meta", jSONObject2);
        } catch (JSONException e2) {
            n.c("Helpshift_ConverDB", "Error in generating meta string for conversation", e2);
        }
        return contentValues;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.util.e.a(android.database.Cursor, java.lang.String, boolean):boolean
     arg types: [android.database.Cursor, java.lang.String, int]
     candidates:
      com.helpshift.util.e.a(android.database.Cursor, java.lang.String, java.lang.Class):T
      com.helpshift.util.e.a(android.database.Cursor, java.lang.String, boolean):boolean */
    private com.helpshift.j.a.b.a a(Cursor cursor) {
        Cursor cursor2 = cursor;
        this.aa.getClass();
        Long valueOf = Long.valueOf(cursor2.getLong(cursor2.getColumnIndex("_id")));
        this.aa.getClass();
        long j2 = cursor2.getLong(cursor2.getColumnIndex("user_local_id"));
        this.aa.getClass();
        String string = cursor2.getString(cursor2.getColumnIndex("server_id"));
        this.aa.getClass();
        String string2 = cursor2.getString(cursor2.getColumnIndex("publish_id"));
        this.aa.getClass();
        String string3 = cursor2.getString(cursor2.getColumnIndex("uuid"));
        this.aa.getClass();
        String string4 = cursor2.getString(cursor2.getColumnIndex("title"));
        this.aa.getClass();
        boolean z2 = cursor2.getInt(cursor2.getColumnIndex("show_agent_name")) == 1;
        this.aa.getClass();
        String string5 = cursor2.getString(cursor2.getColumnIndex("message_cursor"));
        this.aa.getClass();
        boolean z3 = cursor2.getInt(cursor2.getColumnIndex("start_new_conversation_action")) == 1;
        this.aa.getClass();
        String string6 = cursor2.getString(cursor2.getColumnIndex("meta"));
        this.aa.getClass();
        String string7 = cursor2.getString(cursor2.getColumnIndex("created_at"));
        this.aa.getClass();
        long j3 = cursor2.getLong(cursor2.getColumnIndex("epoch_time_created_at"));
        this.aa.getClass();
        String string8 = cursor2.getString(cursor2.getColumnIndex("updated_at"));
        this.aa.getClass();
        String string9 = cursor2.getString(cursor2.getColumnIndex("pre_conv_server_id"));
        this.aa.getClass();
        String str = string7;
        long j4 = cursor2.getLong(cursor2.getColumnIndex("last_user_activity_time"));
        this.aa.getClass();
        String string10 = cursor2.getString(cursor2.getColumnIndex("issue_type"));
        this.aa.getClass();
        long j5 = j4;
        boolean a2 = e.a(cursor2, "full_privacy_enabled", false);
        this.aa.getClass();
        com.helpshift.j.d.e a3 = com.helpshift.j.d.e.a(cursor2.getInt(cursor2.getColumnIndex(ServerProtocol.DIALOG_PARAM_STATE)));
        this.aa.getClass();
        com.helpshift.j.d.e eVar = a3;
        boolean a4 = e.a(cursor2, "is_redacted", false);
        com.helpshift.j.a.b.a aVar = new com.helpshift.j.a.b.a(string4, eVar, str, j3, string8, string2, string5, z2, string10);
        aVar.c = string;
        aVar.d = string9;
        aVar.a(valueOf.longValue());
        aVar.e = string3;
        aVar.g = eVar;
        aVar.t = j2;
        aVar.s = z3;
        aVar.u = j5;
        aVar.w = a2;
        aVar.x = a4;
        a(aVar, string6);
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void
     arg types: [org.json.JSONObject, com.helpshift.j.a.a.ak]
     candidates:
      com.helpshift.common.a.a.a(java.lang.String, org.json.JSONObject):int
      com.helpshift.common.a.a.a(org.json.JSONObject, int):com.helpshift.j.a.a.a.b$b
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String[]):com.helpshift.j.a.b.a
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.s, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.v, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.a.b):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.p):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.v):void
      com.helpshift.common.a.a.a(org.json.JSONObject, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, boolean):void
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String):com.helpshift.support.Faq
      com.helpshift.common.a.a.a(long, com.helpshift.j.a.a.w):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.common.a.a.a(java.util.List<java.lang.Long>, java.lang.String[]):java.util.Map<java.lang.Long, java.lang.Integer>
      com.helpshift.common.a.a.a(java.lang.Long, long):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void
     arg types: [org.json.JSONObject, com.helpshift.j.a.a.u]
     candidates:
      com.helpshift.common.a.a.a(java.lang.String, org.json.JSONObject):int
      com.helpshift.common.a.a.a(org.json.JSONObject, int):com.helpshift.j.a.a.a.b$b
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String[]):com.helpshift.j.a.b.a
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.s, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.v, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.a.b):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.p):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.v):void
      com.helpshift.common.a.a.a(org.json.JSONObject, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, boolean):void
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String):com.helpshift.support.Faq
      com.helpshift.common.a.a.a(long, com.helpshift.j.a.a.w):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.common.a.a.a(java.util.List<java.lang.Long>, java.lang.String[]):java.util.Map<java.lang.Long, java.lang.Integer>
      com.helpshift.common.a.a.a(java.lang.Long, long):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void
     arg types: [org.json.JSONObject, com.helpshift.j.a.a.ab]
     candidates:
      com.helpshift.common.a.a.a(java.lang.String, org.json.JSONObject):int
      com.helpshift.common.a.a.a(org.json.JSONObject, int):com.helpshift.j.a.a.a.b$b
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String[]):com.helpshift.j.a.b.a
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.s, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.v, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.a.b):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.p):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.v):void
      com.helpshift.common.a.a.a(org.json.JSONObject, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, boolean):void
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String):com.helpshift.support.Faq
      com.helpshift.common.a.a.a(long, com.helpshift.j.a.a.w):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.common.a.a.a(java.util.List<java.lang.Long>, java.lang.String[]):java.util.Map<java.lang.Long, java.lang.Integer>
      com.helpshift.common.a.a.a(java.lang.Long, long):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void
     arg types: [org.json.JSONObject, com.helpshift.j.a.a.n]
     candidates:
      com.helpshift.common.a.a.a(java.lang.String, org.json.JSONObject):int
      com.helpshift.common.a.a.a(org.json.JSONObject, int):com.helpshift.j.a.a.a.b$b
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String[]):com.helpshift.j.a.b.a
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.s, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.v, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.a.b):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.p):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.v):void
      com.helpshift.common.a.a.a(org.json.JSONObject, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, boolean):void
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String):com.helpshift.support.Faq
      com.helpshift.common.a.a.a(long, com.helpshift.j.a.a.w):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.common.a.a.a(java.util.List<java.lang.Long>, java.lang.String[]):java.util.Map<java.lang.Long, java.lang.Integer>
      com.helpshift.common.a.a.a(java.lang.Long, long):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void
     arg types: [org.json.JSONObject, com.helpshift.j.a.a.m]
     candidates:
      com.helpshift.common.a.a.a(java.lang.String, org.json.JSONObject):int
      com.helpshift.common.a.a.a(org.json.JSONObject, int):com.helpshift.j.a.a.a.b$b
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String[]):com.helpshift.j.a.b.a
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.s, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.v, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.a.b):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.p):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.v):void
      com.helpshift.common.a.a.a(org.json.JSONObject, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, boolean):void
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String):com.helpshift.support.Faq
      com.helpshift.common.a.a.a(long, com.helpshift.j.a.a.w):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.common.a.a.a(java.util.List<java.lang.Long>, java.lang.String[]):java.util.Map<java.lang.Long, java.lang.Integer>
      com.helpshift.common.a.a.a(java.lang.Long, long):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void
     arg types: [org.json.JSONObject, com.helpshift.j.a.a.s]
     candidates:
      com.helpshift.common.a.a.a(java.lang.String, org.json.JSONObject):int
      com.helpshift.common.a.a.a(org.json.JSONObject, int):com.helpshift.j.a.a.a.b$b
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String[]):com.helpshift.j.a.b.a
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.s, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.v, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.a.b):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.p):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.v):void
      com.helpshift.common.a.a.a(org.json.JSONObject, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, boolean):void
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String):com.helpshift.support.Faq
      com.helpshift.common.a.a.a(long, com.helpshift.j.a.a.w):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.common.a.a.a(java.util.List<java.lang.Long>, java.lang.String[]):java.util.Map<java.lang.Long, java.lang.Integer>
      com.helpshift.common.a.a.a(java.lang.Long, long):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void
     arg types: [org.json.JSONObject, com.helpshift.j.a.a.r]
     candidates:
      com.helpshift.common.a.a.a(java.lang.String, org.json.JSONObject):int
      com.helpshift.common.a.a.a(org.json.JSONObject, int):com.helpshift.j.a.a.a.b$b
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String[]):com.helpshift.j.a.b.a
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.s, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.v, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.a.b):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.p):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.v):void
      com.helpshift.common.a.a.a(org.json.JSONObject, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, boolean):void
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String):com.helpshift.support.Faq
      com.helpshift.common.a.a.a(long, com.helpshift.j.a.a.w):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.common.a.a.a(java.util.List<java.lang.Long>, java.lang.String[]):java.util.Map<java.lang.Long, java.lang.Integer>
      com.helpshift.common.a.a.a(java.lang.Long, long):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void
     arg types: [org.json.JSONObject, com.helpshift.j.a.a.a]
     candidates:
      com.helpshift.common.a.a.a(java.lang.String, org.json.JSONObject):int
      com.helpshift.common.a.a.a(org.json.JSONObject, int):com.helpshift.j.a.a.a.b$b
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String[]):com.helpshift.j.a.b.a
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.s, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.v, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.a.b):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.p):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.v):void
      com.helpshift.common.a.a.a(org.json.JSONObject, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, boolean):void
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String):com.helpshift.support.Faq
      com.helpshift.common.a.a.a(long, com.helpshift.j.a.a.w):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.common.a.a.a(java.util.List<java.lang.Long>, java.lang.String[]):java.util.Map<java.lang.Long, java.lang.Integer>
      com.helpshift.common.a.a.a(java.lang.Long, long):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.p):void
     arg types: [org.json.JSONObject, com.helpshift.j.a.a.q]
     candidates:
      com.helpshift.common.a.a.a(java.lang.String, org.json.JSONObject):int
      com.helpshift.common.a.a.a(org.json.JSONObject, int):com.helpshift.j.a.a.a.b$b
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String[]):com.helpshift.j.a.b.a
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.s, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.a.v, org.json.JSONObject):void
      com.helpshift.common.a.a.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.a.b):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.k):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.l):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.v):void
      com.helpshift.common.a.a.a(org.json.JSONObject, java.lang.String):void
      com.helpshift.common.a.a.a(org.json.JSONObject, boolean):void
      com.helpshift.common.a.a.a(java.lang.String, java.lang.String):com.helpshift.support.Faq
      com.helpshift.common.a.a.a(long, com.helpshift.j.a.a.w):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.common.a.a.a(java.util.List<java.lang.Long>, java.lang.String[]):java.util.Map<java.lang.Long, java.lang.Integer>
      com.helpshift.common.a.a.a(java.lang.Long, long):void
      com.helpshift.common.a.a.a(org.json.JSONObject, com.helpshift.j.a.a.p):void */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x02a2 A[Catch:{ JSONException -> 0x02ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x02a3 A[Catch:{ JSONException -> 0x02ab }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.content.ContentValues c(com.helpshift.j.a.a.v r9) {
        /*
            r8 = this;
            android.content.ContentValues r0 = new android.content.ContentValues
            r0.<init>()
            com.helpshift.common.a.c r1 = r8.aa
            r1.getClass()
            java.lang.String r1 = r9.m
            java.lang.String r2 = "server_id"
            r0.put(r2, r1)
            com.helpshift.common.a.c r1 = r8.aa
            r1.getClass()
            java.lang.Long r1 = r9.q
            java.lang.String r2 = "conversation_id"
            r0.put(r2, r1)
            com.helpshift.common.a.c r1 = r8.aa
            r1.getClass()
            java.lang.String r1 = r9.n
            java.lang.String r2 = "body"
            r0.put(r2, r1)
            com.helpshift.common.a.c r1 = r8.aa
            r1.getClass()
            java.lang.String r1 = r9.p
            java.lang.String r2 = "author_name"
            r0.put(r2, r1)
            com.helpshift.common.a.c r1 = r8.aa
            r1.getClass()
            java.lang.String r1 = r9.m()
            java.lang.String r2 = "created_at"
            r0.put(r2, r1)
            com.helpshift.common.a.c r1 = r8.aa
            r1.getClass()
            long r1 = r9.n()
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            java.lang.String r2 = "epoch_time_created_at"
            r0.put(r2, r1)
            com.helpshift.common.a.c r1 = r8.aa
            r1.getClass()
            com.helpshift.j.a.a.w r1 = r9.k
            java.lang.String r1 = r1.a()
            java.lang.String r2 = "type"
            r0.put(r2, r1)
            com.helpshift.common.a.c r1 = r8.aa
            r1.getClass()
            int r1 = r9.u
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.String r2 = "md_state"
            r0.put(r2, r1)
            com.helpshift.common.a.c r1 = r8.aa
            r1.getClass()
            boolean r1 = r9.y
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.String r2 = "is_redacted"
            r0.put(r2, r1)
            com.helpshift.common.a.c r1 = r8.aa     // Catch:{ JSONException -> 0x02ab }
            r1.getClass()     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r1 = "meta"
            com.helpshift.j.a.a.w r2 = r9.k     // Catch:{ JSONException -> 0x02ab }
            int[] r3 = com.helpshift.common.a.b.f3241a     // Catch:{ JSONException -> 0x02ab }
            int r2 = r2.ordinal()     // Catch:{ JSONException -> 0x02ab }
            r2 = r3[r2]     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = "is_secure"
            java.lang.String r4 = "thumbnail_url"
            java.lang.String r5 = "bot_action_type"
            r6 = 0
            java.lang.String r7 = "referredMessageId"
            switch(r2) {
                case 2: goto L_0x026b;
                case 3: goto L_0x0240;
                case 4: goto L_0x0237;
                case 5: goto L_0x01fe;
                case 6: goto L_0x01ed;
                case 7: goto L_0x01d8;
                case 8: goto L_0x01bb;
                case 9: goto L_0x01aa;
                case 10: goto L_0x0198;
                case 11: goto L_0x0187;
                case 12: goto L_0x0168;
                case 13: goto L_0x015c;
                case 14: goto L_0x0150;
                case 15: goto L_0x0135;
                case 16: goto L_0x0123;
                case 17: goto L_0x0113;
                case 18: goto L_0x00f2;
                case 19: goto L_0x00de;
                case 20: goto L_0x00c9;
                case 21: goto L_0x00a5;
                default: goto L_0x00a2;
            }
        L_0x00a2:
            r2 = r6
            goto L_0x02a0
        L_0x00a5:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.ak r9 = (com.helpshift.j.a.a.ak) r9     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = r9.f3458a     // Catch:{ JSONException -> 0x02ab }
            r2.put(r5, r3)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = "chatbot_info"
            java.lang.String r4 = r9.d     // Catch:{ JSONException -> 0x02ab }
            r2.put(r3, r4)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = "bot_ended_reason"
            java.lang.String r4 = r9.c     // Catch:{ JSONException -> 0x02ab }
            r2.put(r3, r4)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = r9.e     // Catch:{ JSONException -> 0x02ab }
            r2.put(r7, r3)     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x00c9:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.d r9 = (com.helpshift.j.a.a.d) r9     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = r9.f3471a     // Catch:{ JSONException -> 0x02ab }
            r2.put(r5, r3)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = "has_next_bot"
            boolean r9 = r9.c     // Catch:{ JSONException -> 0x02ab }
            r2.put(r3, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x00de:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            r3 = r9
            com.helpshift.j.a.a.z r3 = (com.helpshift.j.a.a.z) r3     // Catch:{ JSONException -> 0x02ab }
            boolean r3 = r3.b()     // Catch:{ JSONException -> 0x02ab }
            c(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x00f2:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            r5 = r9
            com.helpshift.j.a.a.u r5 = (com.helpshift.j.a.a.u) r5     // Catch:{ JSONException -> 0x02ab }
            a(r2, r5)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r7 = r5.f3493b     // Catch:{ JSONException -> 0x02ab }
            r2.put(r4, r7)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r4 = "thumbnailFilePath"
            java.lang.String r7 = r5.i     // Catch:{ JSONException -> 0x02ab }
            r2.put(r4, r7)     // Catch:{ JSONException -> 0x02ab }
            boolean r4 = r5.h     // Catch:{ JSONException -> 0x02ab }
            r2.put(r3, r4)     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x0113:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            r3 = r9
            com.helpshift.j.a.a.k r3 = (com.helpshift.j.a.a.k) r3     // Catch:{ JSONException -> 0x02ab }
            a(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x0123:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            r3 = r9
            com.helpshift.j.a.a.aa r3 = (com.helpshift.j.a.a.aa) r3     // Catch:{ JSONException -> 0x02ab }
            boolean r3 = r3.f3445a     // Catch:{ JSONException -> 0x02ab }
            c(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x0135:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.ab r9 = (com.helpshift.j.a.a.ab) r9     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r5 = r9.f3493b     // Catch:{ JSONException -> 0x02ab }
            r2.put(r4, r5)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r4 = r9.f3447a     // Catch:{ JSONException -> 0x02ab }
            r2.put(r7, r4)     // Catch:{ JSONException -> 0x02ab }
            boolean r9 = r9.h     // Catch:{ JSONException -> 0x02ab }
            r2.put(r3, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x0150:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.n r9 = (com.helpshift.j.a.a.n) r9     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x015c:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.m r9 = (com.helpshift.j.a.a.m) r9     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x0168:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.s r9 = (com.helpshift.j.a.a.s) r9     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = r9.f3490a     // Catch:{ JSONException -> 0x02ab }
            r2.put(r7, r3)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = "rejected_reason"
            int r4 = r9.c     // Catch:{ JSONException -> 0x02ab }
            r2.put(r3, r4)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = "rejected_conv_id"
            java.lang.String r4 = r9.d     // Catch:{ JSONException -> 0x02ab }
            r2.put(r3, r4)     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x0187:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.r r9 = (com.helpshift.j.a.a.r) r9     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = r9.f3489a     // Catch:{ JSONException -> 0x02ab }
            b(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x0198:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            r3 = r9
            com.helpshift.j.a.a.y r3 = (com.helpshift.j.a.a.y) r3     // Catch:{ JSONException -> 0x02ab }
            boolean r3 = r3.f3498a     // Catch:{ JSONException -> 0x02ab }
            c(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x01aa:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.a r9 = (com.helpshift.j.a.a.a) r9     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = r9.f3438a     // Catch:{ JSONException -> 0x02ab }
            b(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x01bb:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            r3 = r9
            com.helpshift.j.a.a.p r3 = (com.helpshift.j.a.a.p) r3     // Catch:{ JSONException -> 0x02ab }
            b(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            r3 = r9
            com.helpshift.j.a.a.q r3 = (com.helpshift.j.a.a.q) r3     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.a.b r3 = r3.d     // Catch:{ JSONException -> 0x02ab }
            r8.a(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.q r9 = (com.helpshift.j.a.a.q) r9     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x01d8:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            r3 = r9
            com.helpshift.j.a.a.p r3 = (com.helpshift.j.a.a.p) r3     // Catch:{ JSONException -> 0x02ab }
            b(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.p r9 = (com.helpshift.j.a.a.p) r9     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x01ed:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.i r9 = (com.helpshift.j.a.a.i) r9     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.a.b r9 = r9.f3479a     // Catch:{ JSONException -> 0x02ab }
            r8.a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x01fe:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            r3 = r9
            com.helpshift.j.a.a.j r3 = (com.helpshift.j.a.a.j) r3     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.a.c r9 = r3.f3481b     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r4 = r9.f3439a     // Catch:{ JSONException -> 0x02ab }
            a(r2, r4)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r4 = "input_required"
            boolean r5 = r9.f3440b     // Catch:{ JSONException -> 0x02ab }
            r2.put(r4, r5)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r4 = "input_skip_label"
            java.lang.String r5 = r9.d     // Catch:{ JSONException -> 0x02ab }
            r2.put(r4, r5)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r4 = "input_label"
            java.lang.String r5 = r9.c     // Catch:{ JSONException -> 0x02ab }
            r2.put(r4, r5)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r4 = "input_placeholder"
            java.lang.String r5 = r9.e     // Catch:{ JSONException -> 0x02ab }
            r2.put(r4, r5)     // Catch:{ JSONException -> 0x02ab }
            int r9 = r9.f     // Catch:{ JSONException -> 0x02ab }
            b(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            boolean r9 = r3.f3480a     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x0237:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            a(r2, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x0240:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.an r9 = (com.helpshift.j.a.a.an) r9     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = r9.f3461a     // Catch:{ JSONException -> 0x02ab }
            a(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            boolean r3 = r9.f3462b     // Catch:{ JSONException -> 0x02ab }
            b(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = r9.d()     // Catch:{ JSONException -> 0x02ab }
            b(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.w r3 = r9.e     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r4 = "referred_message_type"
            java.lang.String r3 = r3.a()     // Catch:{ JSONException -> 0x02ab }
            r2.put(r4, r3)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r9 = r9.d     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = "selected_option_data"
            r2.put(r3, r9)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02a0
        L_0x026b:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x02ab }
            r2.<init>()     // Catch:{ JSONException -> 0x02ab }
            com.helpshift.j.a.a.ap r9 = (com.helpshift.j.a.a.ap) r9     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = r9.d     // Catch:{ JSONException -> 0x02ab }
            a(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            int r3 = r9.f3465b     // Catch:{ JSONException -> 0x02ab }
            b(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            boolean r3 = r9.e     // Catch:{ JSONException -> 0x02ab }
            b(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = r9.d()     // Catch:{ JSONException -> 0x02ab }
            b(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            boolean r3 = r9.f3464a     // Catch:{ JSONException -> 0x02ab }
            a(r2, r3)     // Catch:{ JSONException -> 0x02ab }
            int r3 = r9.f3465b     // Catch:{ JSONException -> 0x02ab }
            r4 = 4
            if (r3 != r4) goto L_0x02a0
            java.lang.String r3 = "dt"
            long r4 = r9.f     // Catch:{ JSONException -> 0x02ab }
            r2.put(r3, r4)     // Catch:{ JSONException -> 0x02ab }
            java.lang.String r3 = "timezone_id"
            java.lang.String r9 = r9.g     // Catch:{ JSONException -> 0x02ab }
            r2.put(r3, r9)     // Catch:{ JSONException -> 0x02ab }
        L_0x02a0:
            if (r2 != 0) goto L_0x02a3
            goto L_0x02a7
        L_0x02a3:
            java.lang.String r6 = r2.toString()     // Catch:{ JSONException -> 0x02ab }
        L_0x02a7:
            r0.put(r1, r6)     // Catch:{ JSONException -> 0x02ab }
            goto L_0x02b3
        L_0x02ab:
            r9 = move-exception
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in generating meta string for message"
            com.helpshift.util.n.c(r1, r2, r9)
        L_0x02b3:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.c(com.helpshift.j.a.a.v):android.content.ContentValues");
    }

    private static void a(com.helpshift.j.a.b.a aVar, String str) {
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt("csat_rating", 0);
                int optInt2 = jSONObject.optInt("csat_state", com.helpshift.j.f.a.NONE.a());
                String optString = jSONObject.optString("csat_feedback", null);
                aVar.q = optInt;
                aVar.p = com.helpshift.j.f.a.a(optInt2);
                aVar.r = optString;
                aVar.n = jSONObject.optBoolean("increment_message_count", false);
                aVar.o = jSONObject.optBoolean("ended_delegate_sent", false);
                aVar.D = jSONObject.optBoolean("is_autofilled_preissue", false);
            } catch (JSONException e2) {
                n.c("Helpshift_ConverDB", "Error in parseAndSetMetaData", e2);
            }
        }
    }

    private static d d(String str) {
        d dVar;
        if (str == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("image_draft_orig_name", null);
            Long valueOf = Long.valueOf(jSONObject.optLong("image_draft_orig_size", -1));
            String optString2 = jSONObject.optString("image_draft_file_path", null);
            boolean optBoolean = jSONObject.optBoolean("image_copy_done", false);
            if (valueOf.longValue() == -1) {
                valueOf = null;
            }
            dVar = new d(optString2, optString, valueOf);
            try {
                dVar.e = optBoolean;
            } catch (JSONException e2) {
                e = e2;
            }
        } catch (JSONException e3) {
            e = e3;
            dVar = null;
            n.c("Helpshift_ConverDB", "Error in parseAndGetImageAttachmentDraft", e);
            return dVar;
        }
        return dVar;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v5, types: [com.helpshift.j.a.a.v] */
    /* JADX WARN: Type inference failed for: r2v12 */
    /* JADX WARN: Type inference failed for: r2v15 */
    /* JADX WARN: Type inference failed for: r2v18 */
    /* JADX WARN: Type inference failed for: r0v1 */
    /* JADX WARN: Type inference failed for: r2v19 */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r2v22 */
    /* JADX WARN: Type inference failed for: r2v24 */
    /* JADX WARN: Type inference failed for: r2v26 */
    /* JADX WARN: Type inference failed for: r1v28 */
    /* JADX WARN: Type inference failed for: r2v29 */
    /* JADX WARN: Type inference failed for: r2v32 */
    /* JADX WARN: Type inference failed for: r9v33, types: [com.helpshift.j.a.a.al] */
    /* JADX WARN: Type inference failed for: r7v22, types: [com.helpshift.j.a.a.h] */
    /* JADX WARN: Type inference failed for: r7v23, types: [com.helpshift.j.a.a.j] */
    /* JADX WARN: Type inference failed for: r2v34 */
    /* JADX WARN: Type inference failed for: r7v24, types: [com.helpshift.j.a.a.i] */
    /* JADX WARN: Type inference failed for: r7v25, types: [com.helpshift.j.a.a.p] */
    /* JADX WARN: Type inference failed for: r7v26, types: [com.helpshift.j.a.a.q] */
    /* JADX WARN: Type inference failed for: r9v34, types: [com.helpshift.j.a.a.a] */
    /* JADX WARN: Type inference failed for: r7v27, types: [com.helpshift.j.a.a.y] */
    /* JADX WARN: Type inference failed for: r9v35, types: [com.helpshift.j.a.a.r] */
    /* JADX WARN: Type inference failed for: r9v36, types: [com.helpshift.j.a.a.s] */
    /* JADX WARN: Type inference failed for: r9v37, types: [com.helpshift.j.a.a.m] */
    /* JADX WARN: Type inference failed for: r9v38, types: [com.helpshift.j.a.a.n] */
    /* JADX WARN: Type inference failed for: r7v28, types: [com.helpshift.j.a.a.b] */
    /* JADX WARN: Type inference failed for: r7v29, types: [com.helpshift.j.a.a.e] */
    /* JADX WARN: Type inference failed for: r7v30, types: [com.helpshift.j.a.a.z] */
    /* JADX WARN: Type inference failed for: r9v39, types: [com.helpshift.j.a.a.ak] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.util.e.a(android.database.Cursor, java.lang.String, boolean):boolean
     arg types: [android.database.Cursor, java.lang.String, int]
     candidates:
      com.helpshift.util.e.a(android.database.Cursor, java.lang.String, java.lang.Class):T
      com.helpshift.util.e.a(android.database.Cursor, java.lang.String, boolean):boolean */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 5 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.helpshift.j.a.a.v b(android.database.Cursor r28) {
        /*
            r27 = this;
            r0 = r27
            r1 = r28
            com.helpshift.common.a.c r2 = r0.aa
            r2.getClass()
            java.lang.String r2 = "_id"
            int r2 = r1.getColumnIndex(r2)
            long r2 = r1.getLong(r2)
            com.helpshift.common.a.c r4 = r0.aa
            r4.getClass()
            java.lang.String r4 = "conversation_id"
            int r4 = r1.getColumnIndex(r4)
            long r4 = r1.getLong(r4)
            com.helpshift.common.a.c r6 = r0.aa
            r6.getClass()
            java.lang.String r6 = "server_id"
            int r6 = r1.getColumnIndex(r6)
            java.lang.String r8 = r1.getString(r6)
            com.helpshift.common.a.c r6 = r0.aa
            r6.getClass()
            java.lang.String r6 = "body"
            int r6 = r1.getColumnIndex(r6)
            java.lang.String r10 = r1.getString(r6)
            com.helpshift.common.a.c r6 = r0.aa
            r6.getClass()
            java.lang.String r6 = "author_name"
            int r6 = r1.getColumnIndex(r6)
            java.lang.String r14 = r1.getString(r6)
            com.helpshift.common.a.c r6 = r0.aa
            r6.getClass()
            java.lang.String r6 = "meta"
            int r6 = r1.getColumnIndex(r6)
            java.lang.String r6 = r1.getString(r6)
            com.helpshift.common.a.c r7 = r0.aa
            r7.getClass()
            java.lang.String r7 = "type"
            int r7 = r1.getColumnIndex(r7)
            java.lang.String r7 = r1.getString(r7)
            com.helpshift.common.a.c r9 = r0.aa
            r9.getClass()
            java.lang.String r9 = "created_at"
            int r9 = r1.getColumnIndex(r9)
            java.lang.String r11 = r1.getString(r9)
            long r12 = com.helpshift.common.g.b.b(r11)
            com.helpshift.common.a.c r9 = r0.aa
            r9.getClass()
            java.lang.String r9 = "md_state"
            int r9 = r1.getColumnIndex(r9)
            int r15 = r1.getInt(r9)
            com.helpshift.common.a.c r9 = r0.aa
            r9.getClass()
            r9 = 0
            r16 = r15
            java.lang.String r15 = "is_redacted"
            boolean r1 = com.helpshift.util.e.a(r1, r15, r9)
            com.helpshift.j.a.a.w r7 = com.helpshift.j.a.a.w.a(r7)
            org.json.JSONObject r6 = e(r6)
            int[] r15 = com.helpshift.common.a.b.f3241a
            int r7 = r7.ordinal()
            r7 = r15[r7]
            java.lang.String r15 = "is_suggestion_read_event_sent"
            java.lang.String r9 = "is_message_empty"
            r28 = r1
            java.lang.String r1 = "is_response_skipped"
            r22 = r2
            java.lang.String r3 = "input_keyboard"
            java.lang.String r2 = "is_answered"
            r24 = r4
            java.lang.String r4 = "input_required"
            java.lang.String r5 = "referredMessageId"
            r19 = r1
            r1 = 0
            switch(r7) {
                case 1: goto L_0x0362;
                case 2: goto L_0x032b;
                case 3: goto L_0x0307;
                case 4: goto L_0x02fa;
                case 5: goto L_0x02c5;
                case 6: goto L_0x0294;
                case 7: goto L_0x0278;
                case 8: goto L_0x0246;
                case 9: goto L_0x022c;
                case 10: goto L_0x0219;
                case 11: goto L_0x0205;
                case 12: goto L_0x01eb;
                case 13: goto L_0x01dc;
                case 14: goto L_0x01cc;
                case 15: goto L_0x0197;
                case 16: goto L_0x0182;
                case 17: goto L_0x0158;
                case 18: goto L_0x0124;
                case 19: goto L_0x010c;
                case 20: goto L_0x00e9;
                case 21: goto L_0x00c8;
                default: goto L_0x00c7;
            }
        L_0x00c7:
            return r1
        L_0x00c8:
            java.lang.String r15 = a(r6)
            java.lang.String r17 = k(r6)
            java.lang.String r2 = b(r6)
            java.lang.String r18 = r6.optString(r5, r1)
            int r19 = a(r8, r6)
            com.helpshift.j.a.a.ak r1 = new com.helpshift.j.a.a.ak
            r9 = r1
            r3 = r16
            r16 = r2
            r9.<init>(r10, r11, r12, r14, r15, r16, r17, r18, r19)
            r1.m = r8
            goto L_0x0120
        L_0x00e9:
            r3 = r16
            java.lang.String r1 = a(r6)
            java.lang.String r15 = k(r6)
            java.lang.Boolean r2 = l(r6)
            com.helpshift.j.a.a.d r4 = new com.helpshift.j.a.a.d
            r7 = r4
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r1
            r7.<init>(r8, r9, r10, r11, r13, r14, r15)
            boolean r1 = r2.booleanValue()
            r4.c = r1
            r1 = r3
            r2 = r4
            goto L_0x036c
        L_0x010c:
            r3 = r16
            com.helpshift.j.a.a.z r1 = new com.helpshift.j.a.a.z
            r7 = r1
            r5 = 0
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r7.<init>(r8, r9, r10, r11, r13)
            boolean r2 = r6.optBoolean(r2, r5)
            r1.a(r2)
        L_0x0120:
            r2 = r1
            r1 = r3
            goto L_0x036c
        L_0x0124:
            r3 = r16
            com.helpshift.common.a.a$b r1 = r0.n(r6)
            com.helpshift.j.a.a.e r2 = new com.helpshift.j.a.a.e
            java.lang.String r4 = r1.c
            java.lang.String r15 = r1.f3238b
            java.lang.String r5 = r1.h
            java.lang.String r9 = r1.f3237a
            boolean r7 = r1.f
            r26 = r3
            int r3 = r1.e
            r18 = r7
            r7 = r2
            r17 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r4
            r16 = r5
            r19 = r3
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19)
            java.lang.String r3 = r1.d
            r2.g = r3
            java.lang.String r1 = r1.i
            r2.i = r1
            r2.b()
            goto L_0x02c1
        L_0x0158:
            r26 = r16
            com.helpshift.common.a.a$a r1 = r0.m(r6)
            com.helpshift.j.a.a.b r2 = new com.helpshift.j.a.a.b
            int r3 = r1.e
            java.lang.String r15 = r1.f3237a
            java.lang.String r4 = r1.c
            java.lang.String r5 = r1.f3238b
            boolean r9 = r1.f
            r7 = r2
            r18 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r3
            r16 = r4
            r17 = r5
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18)
            java.lang.String r1 = r1.d
            r2.g = r1
            r2.b()
            goto L_0x02c1
        L_0x0182:
            r26 = r16
            r5 = 0
            com.helpshift.j.a.a.aa r1 = new com.helpshift.j.a.a.aa
            boolean r2 = r6.optBoolean(r2, r5)
            r7 = r1
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r2
            r7.<init>(r8, r9, r10, r11, r13, r14)
            r2 = r1
            goto L_0x02c1
        L_0x0197:
            r26 = r16
            com.helpshift.common.a.a$b r2 = r0.n(r6)
            com.helpshift.j.a.a.ab r3 = new com.helpshift.j.a.a.ab
            java.lang.String r15 = r2.f3237a
            java.lang.String r4 = r2.h
            java.lang.String r7 = r2.f3238b
            java.lang.String r9 = r2.c
            int r1 = r2.e
            boolean r0 = r2.f
            r18 = r9
            r9 = r3
            r16 = r4
            r17 = r7
            r19 = r1
            r20 = r0
            r9.<init>(r10, r11, r12, r14, r15, r16, r17, r18, r19, r20)
            java.lang.String r0 = r2.d
            r3.g = r0
            r3.m = r8
            r0 = 0
            java.lang.String r0 = r6.optString(r5, r0)
            r3.d(r0)
            r0 = r27
            r2 = r3
            goto L_0x02c1
        L_0x01cc:
            r26 = r16
            com.helpshift.j.a.a.n r0 = new com.helpshift.j.a.a.n
            int r15 = a(r8, r6)
            r9 = r0
            r9.<init>(r10, r11, r12, r14, r15)
            r0.m = r8
            goto L_0x023f
        L_0x01dc:
            r26 = r16
            com.helpshift.j.a.a.m r0 = new com.helpshift.j.a.a.m
            int r15 = a(r8, r6)
            r9 = r0
            r9.<init>(r10, r11, r12, r14, r15)
            r0.m = r8
            goto L_0x023f
        L_0x01eb:
            r26 = r16
            com.helpshift.j.a.a.s r0 = new com.helpshift.j.a.a.s
            r1 = 0
            java.lang.String r15 = r6.optString(r5, r1)
            int r16 = a(r8, r6)
            r9 = r0
            r9.<init>(r10, r11, r12, r14, r15, r16)
            r0.m = r8
            r1 = r0
            com.helpshift.j.a.a.s r1 = (com.helpshift.j.a.a.s) r1
            a(r1, r6)
            goto L_0x023f
        L_0x0205:
            r26 = r16
            com.helpshift.j.a.a.r r0 = new com.helpshift.j.a.a.r
            r1 = 0
            java.lang.String r15 = r6.optString(r5, r1)
            int r16 = a(r8, r6)
            r9 = r0
            r9.<init>(r10, r11, r12, r14, r15, r16)
            r0.m = r8
            goto L_0x023f
        L_0x0219:
            r26 = r16
            r5 = 0
            com.helpshift.j.a.a.y r0 = new com.helpshift.j.a.a.y
            boolean r1 = r6.optBoolean(r2, r5)
            r7 = r0
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r1
            r7.<init>(r8, r9, r10, r11, r13, r14)
            goto L_0x023f
        L_0x022c:
            r26 = r16
            com.helpshift.j.a.a.a r0 = new com.helpshift.j.a.a.a
            r1 = 0
            java.lang.String r15 = r6.optString(r5, r1)
            int r16 = a(r8, r6)
            r9 = r0
            r9.<init>(r10, r11, r12, r14, r15, r16)
            r0.m = r8
        L_0x023f:
            r2 = r0
            r1 = r26
            r0 = r27
            goto L_0x036c
        L_0x0246:
            r26 = r16
            r5 = 0
            com.helpshift.j.a.a.q r0 = new com.helpshift.j.a.a.q
            java.util.List r1 = d(r6)
            java.lang.String r2 = k(r6)
            boolean r16 = r6.optBoolean(r4, r5)
            r3 = r27
            java.lang.String r17 = r3.i(r6)
            java.lang.String r18 = r3.h(r6)
            java.util.List r19 = e(r6)
            boolean r20 = r6.optBoolean(r15, r5)
            java.lang.String r21 = r3.c(r6)
            r7 = r0
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r1
            r15 = r2
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            goto L_0x02bf
        L_0x0278:
            r3 = r0
            r26 = r16
            r5 = 0
            com.helpshift.j.a.a.p r0 = new com.helpshift.j.a.a.p
            java.util.List r1 = d(r6)
            boolean r15 = r6.optBoolean(r15, r5)
            java.lang.String r16 = r3.c(r6)
            r7 = r0
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r1
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16)
            goto L_0x02bf
        L_0x0294:
            r3 = r0
            r26 = r16
            r5 = 0
            java.util.List r18 = e(r6)
            com.helpshift.j.a.a.i r0 = new com.helpshift.j.a.a.i
            java.lang.String r1 = k(r6)
            boolean r15 = r6.optBoolean(r4, r5)
            java.lang.String r16 = r3.i(r6)
            java.lang.String r17 = r3.h(r6)
            int r2 = r18.size()
            com.helpshift.j.a.a.a.b$b r19 = r3.a(r6, r2)
            r7 = r0
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r1
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19)
        L_0x02bf:
            r2 = r0
            r0 = r3
        L_0x02c1:
            r1 = r26
            goto L_0x036c
        L_0x02c5:
            r1 = r16
            r5 = 0
            com.helpshift.j.a.a.j r2 = new com.helpshift.j.a.a.j
            java.lang.String r15 = k(r6)
            java.lang.String r16 = r0.j(r6)
            boolean r4 = r6.optBoolean(r4, r5)
            java.lang.String r17 = r0.i(r6)
            java.lang.String r19 = r0.h(r6)
            r7 = 1
            int r3 = r6.optInt(r3, r7)
            boolean r20 = r6.optBoolean(r9, r5)
            r7 = r2
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r14 = r15
            r15 = r16
            r16 = r4
            r18 = r19
            r19 = r3
            r7.<init>(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19, r20)
            goto L_0x036c
        L_0x02fa:
            r1 = r16
            com.helpshift.j.a.a.h r2 = new com.helpshift.j.a.a.h
            r7 = r2
            r9 = r10
            r10 = r11
            r11 = r12
            r13 = r14
            r7.<init>(r8, r9, r10, r11, r13)
            goto L_0x036c
        L_0x0307:
            r1 = r16
            r2 = 0
            com.helpshift.j.a.a.an r3 = new com.helpshift.j.a.a.an
            java.lang.String r15 = k(r6)
            r4 = r19
            boolean r16 = r6.optBoolean(r4, r2)
            java.lang.String r17 = r0.g(r6)
            r2 = 0
            java.lang.String r18 = r6.optString(r5, r2)
            com.helpshift.j.a.a.w r19 = r0.f(r6)
            r9 = r3
            r9.<init>(r10, r11, r12, r14, r15, r16, r17, r18, r19)
            r3.m = r8
            r2 = r3
            goto L_0x036c
        L_0x032b:
            r1 = r16
            r4 = r19
            r2 = 0
            com.helpshift.j.a.a.ap r7 = new com.helpshift.j.a.a.ap
            r15 = 1
            int r15 = r6.optInt(r3, r15)
            java.lang.String r16 = k(r6)
            boolean r17 = r6.optBoolean(r4, r2)
            r3 = 0
            java.lang.String r18 = r6.optString(r5, r3)
            boolean r19 = r6.optBoolean(r9, r2)
            r9 = r7
            r9.<init>(r10, r11, r12, r14, r15, r16, r17, r18, r19)
            r7.m = r8
            r2 = 0
            java.lang.String r4 = "dt"
            long r2 = r6.optLong(r4, r2)
            r7.f = r2
            java.lang.String r2 = "timezone_id"
            java.lang.String r2 = r6.optString(r2)
            r7.g = r2
            r2 = r7
            goto L_0x036c
        L_0x0362:
            r1 = r16
            com.helpshift.j.a.a.al r2 = new com.helpshift.j.a.a.al
            r9 = r2
            r9.<init>(r10, r11, r12, r14)
            r2.m = r8
        L_0x036c:
            java.lang.Long r3 = java.lang.Long.valueOf(r24)
            r2.q = r3
            java.lang.Long r3 = java.lang.Long.valueOf(r22)
            r2.r = r3
            r2.u = r1
            r1 = r28
            r2.y = r1
            a(r2, r6)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.b(android.database.Cursor):com.helpshift.j.a.a.v");
    }

    private static String a(JSONObject jSONObject) {
        return jSONObject.optString("bot_action_type", "");
    }

    private static String b(JSONObject jSONObject) {
        return jSONObject.optString("bot_ended_reason", "");
    }

    private static List<p.a> d(JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("faqs");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                arrayList.add(new p.a(jSONObject2.getString("faq_title"), jSONObject2.getString("faq_publish_id"), jSONObject2.getString("faq_language")));
            }
        } catch (JSONException unused) {
        }
        return arrayList;
    }

    private static List<b.a> e(JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("input_options");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                arrayList.add(new b.a(jSONObject2.getString("option_title"), jSONObject2.getString("option_data")));
            }
        } catch (JSONException unused) {
        }
        return arrayList;
    }

    private static String k(JSONObject jSONObject) {
        return jSONObject.optString("chatbot_info", "{}");
    }

    private static Boolean l(JSONObject jSONObject) {
        return Boolean.valueOf(jSONObject.optBoolean("has_next_bot", false));
    }

    private static int a(String str, JSONObject jSONObject) {
        if (!k.a(str)) {
            return 2;
        }
        return jSONObject.optInt("message_sync_status", 1);
    }

    private static void a(v vVar, JSONObject jSONObject) {
        String optString = jSONObject.optString("read_at", "");
        String optString2 = jSONObject.optString("seen_cursor", null);
        boolean optBoolean = jSONObject.optBoolean("seen_sync_status", false);
        vVar.t = optString2;
        vVar.v = optBoolean;
        vVar.s = optString;
    }

    private static JSONObject e(String str) {
        JSONObject jSONObject = new JSONObject();
        if (k.a(str)) {
            return jSONObject;
        }
        try {
            return new JSONObject(str);
        } catch (JSONException e2) {
            n.c("Helpshift_ConverDB", "Exception in jsonify", e2);
            return jSONObject;
        }
    }

    private C0134a m(JSONObject jSONObject) {
        return new C0134a(jSONObject);
    }

    private b n(JSONObject jSONObject) {
        return new b(jSONObject);
    }

    private static void a(s sVar, JSONObject jSONObject) {
        int optInt = jSONObject.optInt("rejected_reason");
        String optString = jSONObject.optString("rejected_conv_id", null);
        sVar.c = optInt;
        sVar.d = optString;
    }

    private static void a(JSONObject jSONObject, p pVar) throws JSONException {
        jSONObject.put("is_suggestion_read_event_sent", pVar.f3486b);
        jSONObject.put("suggestion_read_faq_publish_id", pVar.c);
    }

    private static void a(JSONObject jSONObject, boolean z2) throws JSONException {
        jSONObject.put("is_message_empty", z2);
    }

    private static void b(JSONObject jSONObject, p pVar) throws JSONException {
        if (pVar.f3485a != null) {
            JSONArray jSONArray = new JSONArray();
            for (p.a next : pVar.f3485a) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("faq_title", next.f3487a);
                jSONObject2.put("faq_publish_id", next.f3488b);
                jSONObject2.put("faq_language", next.c);
                jSONArray.put(jSONObject2);
            }
            jSONObject.put("faqs", jSONArray);
        }
    }

    private static void b(JSONObject jSONObject, boolean z2) throws JSONException {
        jSONObject.put("is_response_skipped", z2);
    }

    private static void b(JSONObject jSONObject, int i2) throws JSONException {
        jSONObject.put("input_keyboard", i2);
    }

    private static void a(JSONObject jSONObject, String str) throws JSONException {
        jSONObject.put("chatbot_info", str);
    }

    private void a(JSONObject jSONObject, com.helpshift.j.a.a.a.b bVar) throws JSONException {
        a(jSONObject, bVar.f3439a);
        jSONObject.put("input_required", bVar.f3440b);
        jSONObject.put("input_label", bVar.c);
        jSONObject.put("input_skip_label", bVar.d);
        if (bVar.e != null) {
            JSONArray jSONArray = new JSONArray();
            for (b.a next : bVar.e) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("option_title", next.f3441a);
                jSONObject2.put("option_data", next.f3442b);
                jSONArray.put(jSONObject2);
            }
            jSONObject.put("input_options", jSONArray);
        }
        jSONObject.put("option_type", bVar.f.toString());
    }

    private static void a(JSONObject jSONObject, v vVar) throws JSONException {
        jSONObject.put("seen_cursor", vVar.t);
        jSONObject.put("seen_sync_status", vVar.v);
        jSONObject.put("read_at", vVar.s);
    }

    private static void b(JSONObject jSONObject, String str) throws JSONException {
        jSONObject.put("referredMessageId", str);
    }

    private static void c(JSONObject jSONObject, boolean z2) throws JSONException {
        jSONObject.put("is_answered", z2);
    }

    private static void a(JSONObject jSONObject, com.helpshift.j.a.a.k kVar) throws JSONException {
        jSONObject.put("content_type", kVar.c);
        jSONObject.put("file_name", kVar.d);
        jSONObject.put("filePath", kVar.g);
        jSONObject.put("url", kVar.e);
        jSONObject.put("size", kVar.f);
        jSONObject.put("is_secure", kVar.h);
    }

    private static void a(JSONObject jSONObject, l lVar) throws JSONException {
        jSONObject.put("message_sync_status", lVar.b());
    }

    public final synchronized v c(String str) {
        List<v> b2;
        StringBuilder sb = new StringBuilder();
        this.aa.getClass();
        sb.append("server_id");
        sb.append(" = ?");
        b2 = b(sb.toString(), new String[]{String.valueOf(str)});
        return j.a(b2) ? null : b2.get(0);
    }

    public final synchronized v b(Long l2) {
        List<v> b2;
        StringBuilder sb = new StringBuilder();
        this.aa.getClass();
        sb.append("_id");
        sb.append(" = ?");
        b2 = b(sb.toString(), new String[]{String.valueOf(l2)});
        return j.a(b2) ? null : b2.get(0);
    }

    public final synchronized void a() {
        this.ab.a(this.ab.getWritableDatabase());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b7, code lost:
        if (r3 != null) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00cb, code lost:
        if (r3 != null) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00cf, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00d8, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d3 A[SYNTHETIC, Splitter:B:33:0x00d3] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.helpshift.support.Faq a(java.lang.String r26, java.lang.String r27) {
        /*
            r25 = this;
            r1 = r25
            monitor-enter(r25)
            boolean r0 = com.helpshift.common.k.a(r26)     // Catch:{ all -> 0x00d9 }
            r2 = 0
            if (r0 != 0) goto L_0x00d7
            boolean r0 = com.helpshift.common.k.a(r27)     // Catch:{ all -> 0x00d9 }
            if (r0 == 0) goto L_0x0012
            goto L_0x00d7
        L_0x0012:
            com.helpshift.u.a.a r0 = r1.ab     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            android.database.sqlite.SQLiteDatabase r3 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            java.lang.String r4 = "faq_suggestions"
            r5 = 0
            java.lang.String r6 = "publish_id = ? AND language = ?"
            r0 = 2
            java.lang.String[] r7 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            r0 = 0
            r7[r0] = r26     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            r11 = 1
            r7[r11] = r27     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r3 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            boolean r4 = r3.moveToFirst()     // Catch:{ Exception -> 0x00bd }
            if (r4 == 0) goto L_0x00b7
            com.helpshift.support.Faq r4 = new com.helpshift.support.Faq     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "_id"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ Exception -> 0x00bd }
            long r13 = r3.getLong(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "question_id"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r15 = r3.getString(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "publish_id"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r16 = r3.getString(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "language"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r17 = r3.getString(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "section_id"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r18 = r3.getString(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "title"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r19 = r3.getString(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "body"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r20 = r3.getString(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "helpful"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ Exception -> 0x00bd }
            int r21 = r3.getInt(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "rtl"
            int r5 = r3.getColumnIndex(r5)     // Catch:{ Exception -> 0x00bd }
            int r5 = r3.getInt(r5)     // Catch:{ Exception -> 0x00bd }
            if (r5 != r11) goto L_0x0092
            r0 = 1
        L_0x0092:
            java.lang.Boolean r22 = java.lang.Boolean.valueOf(r0)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r0 = "tags"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r0 = r3.getString(r0)     // Catch:{ Exception -> 0x00bd }
            java.util.ArrayList r23 = com.helpshift.util.j.a(r0)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r0 = "c_tags"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r0 = r3.getString(r0)     // Catch:{ Exception -> 0x00bd }
            java.util.ArrayList r24 = com.helpshift.util.j.a(r0)     // Catch:{ Exception -> 0x00bd }
            r12 = r4
            r12.<init>(r13, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)     // Catch:{ Exception -> 0x00bd }
            r2 = r4
        L_0x00b7:
            if (r3 == 0) goto L_0x00ce
        L_0x00b9:
            r3.close()     // Catch:{ all -> 0x00d9 }
            goto L_0x00ce
        L_0x00bd:
            r0 = move-exception
            goto L_0x00c4
        L_0x00bf:
            r0 = move-exception
            r3 = r2
            goto L_0x00d1
        L_0x00c2:
            r0 = move-exception
            r3 = r2
        L_0x00c4:
            java.lang.String r4 = "Helpshift_ConverDB"
            java.lang.String r5 = "Error in getAdminFAQSuggestion"
            com.helpshift.util.n.c(r4, r5, r0)     // Catch:{ all -> 0x00d0 }
            if (r3 == 0) goto L_0x00ce
            goto L_0x00b9
        L_0x00ce:
            monitor-exit(r25)
            return r2
        L_0x00d0:
            r0 = move-exception
        L_0x00d1:
            if (r3 == 0) goto L_0x00d6
            r3.close()     // Catch:{ all -> 0x00d9 }
        L_0x00d6:
            throw r0     // Catch:{ all -> 0x00d9 }
        L_0x00d7:
            monitor-exit(r25)
            return r2
        L_0x00d9:
            r0 = move-exception
            monitor-exit(r25)
            goto L_0x00dd
        L_0x00dc:
            throw r0
        L_0x00dd:
            goto L_0x00dc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.a(java.lang.String, java.lang.String):com.helpshift.support.Faq");
    }

    public final synchronized void b(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                this.ab.getWritableDatabase().delete("faq_suggestions", "publish_id = ? AND language = ?", new String[]{str, str2});
            } catch (Exception e2) {
                n.c("Helpshift_ConverDB", "Error in removeAdminFAQSuggestion", e2);
            }
        }
        return;
    }

    public final synchronized void f(long j2) {
        StringBuilder sb = new StringBuilder();
        sb.append("delete from ");
        this.aa.getClass();
        sb.append("conversation_inbox");
        sb.append(" where ");
        this.aa.getClass();
        sb.append("user_local_id");
        sb.append(" = ?");
        String sb2 = sb.toString();
        try {
            this.ab.getWritableDatabase().execSQL(sb2, new String[]{String.valueOf(j2)});
        } catch (Exception e2) {
            n.c("Helpshift_ConverDB", "Error in delete conversationInboxData with UserLocalId", e2);
        }
        return;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x011b, code lost:
        if (r2 == null) goto L_0x011e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x010b, code lost:
        if (r2 != null) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        r2.endTransaction();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void g(long r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            com.helpshift.common.a.c r0 = r7.aa     // Catch:{ all -> 0x0126 }
            r0.getClass()     // Catch:{ all -> 0x0126 }
            java.lang.String r0 = "issues"
            com.helpshift.common.a.c r1 = r7.aa     // Catch:{ all -> 0x0126 }
            r1.getClass()     // Catch:{ all -> 0x0126 }
            java.lang.String r1 = "messages"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0126 }
            r2.<init>()     // Catch:{ all -> 0x0126 }
            r2.append(r0)     // Catch:{ all -> 0x0126 }
            java.lang.String r3 = "."
            r2.append(r3)     // Catch:{ all -> 0x0126 }
            com.helpshift.common.a.c r3 = r7.aa     // Catch:{ all -> 0x0126 }
            r3.getClass()     // Catch:{ all -> 0x0126 }
            java.lang.String r3 = "_id"
            r2.append(r3)     // Catch:{ all -> 0x0126 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0126 }
            r3.<init>()     // Catch:{ all -> 0x0126 }
            r3.append(r0)     // Catch:{ all -> 0x0126 }
            java.lang.String r0 = "."
            r3.append(r0)     // Catch:{ all -> 0x0126 }
            com.helpshift.common.a.c r0 = r7.aa     // Catch:{ all -> 0x0126 }
            r0.getClass()     // Catch:{ all -> 0x0126 }
            java.lang.String r0 = "user_local_id"
            r3.append(r0)     // Catch:{ all -> 0x0126 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0126 }
            r3.<init>()     // Catch:{ all -> 0x0126 }
            r3.append(r1)     // Catch:{ all -> 0x0126 }
            java.lang.String r1 = "."
            r3.append(r1)     // Catch:{ all -> 0x0126 }
            com.helpshift.common.a.c r1 = r7.aa     // Catch:{ all -> 0x0126 }
            r1.getClass()     // Catch:{ all -> 0x0126 }
            java.lang.String r1 = "conversation_id"
            r3.append(r1)     // Catch:{ all -> 0x0126 }
            java.lang.String r1 = r3.toString()     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0126 }
            r3.<init>()     // Catch:{ all -> 0x0126 }
            java.lang.String r4 = "select "
            r3.append(r4)     // Catch:{ all -> 0x0126 }
            r3.append(r2)     // Catch:{ all -> 0x0126 }
            java.lang.String r2 = " from  "
            r3.append(r2)     // Catch:{ all -> 0x0126 }
            com.helpshift.common.a.c r2 = r7.aa     // Catch:{ all -> 0x0126 }
            r2.getClass()     // Catch:{ all -> 0x0126 }
            java.lang.String r2 = "issues"
            r3.append(r2)     // Catch:{ all -> 0x0126 }
            java.lang.String r2 = "  where "
            r3.append(r2)     // Catch:{ all -> 0x0126 }
            r3.append(r0)     // Catch:{ all -> 0x0126 }
            java.lang.String r0 = " = ?"
            r3.append(r0)     // Catch:{ all -> 0x0126 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0126 }
            r2.<init>()     // Catch:{ all -> 0x0126 }
            java.lang.String r3 = "delete from "
            r2.append(r3)     // Catch:{ all -> 0x0126 }
            com.helpshift.common.a.c r3 = r7.aa     // Catch:{ all -> 0x0126 }
            r3.getClass()     // Catch:{ all -> 0x0126 }
            java.lang.String r3 = "messages"
            r2.append(r3)     // Catch:{ all -> 0x0126 }
            java.lang.String r3 = " where "
            r2.append(r3)     // Catch:{ all -> 0x0126 }
            r2.append(r1)     // Catch:{ all -> 0x0126 }
            java.lang.String r1 = " IN  ( "
            r2.append(r1)     // Catch:{ all -> 0x0126 }
            r2.append(r0)     // Catch:{ all -> 0x0126 }
            java.lang.String r0 = " )"
            r2.append(r0)     // Catch:{ all -> 0x0126 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0126 }
            r1.<init>()     // Catch:{ all -> 0x0126 }
            java.lang.String r2 = "delete from "
            r1.append(r2)     // Catch:{ all -> 0x0126 }
            com.helpshift.common.a.c r2 = r7.aa     // Catch:{ all -> 0x0126 }
            r2.getClass()     // Catch:{ all -> 0x0126 }
            java.lang.String r2 = "issues"
            r1.append(r2)     // Catch:{ all -> 0x0126 }
            java.lang.String r2 = " where "
            r1.append(r2)     // Catch:{ all -> 0x0126 }
            com.helpshift.common.a.c r2 = r7.aa     // Catch:{ all -> 0x0126 }
            r2.getClass()     // Catch:{ all -> 0x0126 }
            java.lang.String r2 = "user_local_id"
            r1.append(r2)     // Catch:{ all -> 0x0126 }
            java.lang.String r2 = " = ?"
            r1.append(r2)     // Catch:{ all -> 0x0126 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0126 }
            r2 = 0
            com.helpshift.u.a.a r3 = r7.ab     // Catch:{ Exception -> 0x0113 }
            android.database.sqlite.SQLiteDatabase r2 = r3.getWritableDatabase()     // Catch:{ Exception -> 0x0113 }
            r2.beginTransaction()     // Catch:{ Exception -> 0x0113 }
            r3 = 1
            java.lang.String[] r4 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0113 }
            java.lang.String r5 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0113 }
            r6 = 0
            r4[r6] = r5     // Catch:{ Exception -> 0x0113 }
            r2.execSQL(r0, r4)     // Catch:{ Exception -> 0x0113 }
            java.lang.String[] r0 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0113 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0113 }
            r0[r6] = r8     // Catch:{ Exception -> 0x0113 }
            r2.execSQL(r1, r0)     // Catch:{ Exception -> 0x0113 }
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x0113 }
            if (r2 == 0) goto L_0x011e
        L_0x010d:
            r2.endTransaction()     // Catch:{ all -> 0x0126 }
            goto L_0x011e
        L_0x0111:
            r8 = move-exception
            goto L_0x0120
        L_0x0113:
            r8 = move-exception
            java.lang.String r9 = "Helpshift_ConverDB"
            java.lang.String r0 = "Error in delete conversations with UserLocalId"
            com.helpshift.util.n.c(r9, r0, r8)     // Catch:{ all -> 0x0111 }
            if (r2 == 0) goto L_0x011e
            goto L_0x010d
        L_0x011e:
            monitor-exit(r7)
            return
        L_0x0120:
            if (r2 == 0) goto L_0x0125
            r2.endTransaction()     // Catch:{ all -> 0x0126 }
        L_0x0125:
            throw r8     // Catch:{ all -> 0x0126 }
        L_0x0126:
            r8 = move-exception
            monitor-exit(r7)
            goto L_0x012a
        L_0x0129:
            throw r8
        L_0x012a:
            goto L_0x0129
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.g(long):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x012c, code lost:
        if (r11 != null) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0142, code lost:
        if (r11 != null) goto L_0x012e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x014a A[SYNTHETIC, Splitter:B:27:0x014a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.String h(long r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            java.lang.String r0 = "message_create_at"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x014e }
            r1.<init>()     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r2 = r9.aa     // Catch:{ all -> 0x014e }
            r2.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r2 = "issues"
            r1.append(r2)     // Catch:{ all -> 0x014e }
            java.lang.String r2 = "."
            r1.append(r2)     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r2 = r9.aa     // Catch:{ all -> 0x014e }
            r2.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r2 = "user_local_id"
            r1.append(r2)     // Catch:{ all -> 0x014e }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x014e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x014e }
            r2.<init>()     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r3 = r9.aa     // Catch:{ all -> 0x014e }
            r3.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r3 = "issues"
            r2.append(r3)     // Catch:{ all -> 0x014e }
            java.lang.String r3 = "."
            r2.append(r3)     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r3 = r9.aa     // Catch:{ all -> 0x014e }
            r3.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r3 = "_id"
            r2.append(r3)     // Catch:{ all -> 0x014e }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x014e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x014e }
            r3.<init>()     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r4 = r9.aa     // Catch:{ all -> 0x014e }
            r4.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r4 = "messages"
            r3.append(r4)     // Catch:{ all -> 0x014e }
            java.lang.String r4 = "."
            r3.append(r4)     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r4 = r9.aa     // Catch:{ all -> 0x014e }
            r4.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r4 = "conversation_id"
            r3.append(r4)     // Catch:{ all -> 0x014e }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x014e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x014e }
            r4.<init>()     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r5 = r9.aa     // Catch:{ all -> 0x014e }
            r5.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r5 = "messages"
            r4.append(r5)     // Catch:{ all -> 0x014e }
            java.lang.String r5 = "."
            r4.append(r5)     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r5 = r9.aa     // Catch:{ all -> 0x014e }
            r5.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r5 = "created_at"
            r4.append(r5)     // Catch:{ all -> 0x014e }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x014e }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x014e }
            r5.<init>()     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r6 = r9.aa     // Catch:{ all -> 0x014e }
            r6.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r6 = "messages"
            r5.append(r6)     // Catch:{ all -> 0x014e }
            java.lang.String r6 = "."
            r5.append(r6)     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r6 = r9.aa     // Catch:{ all -> 0x014e }
            r6.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r6 = "epoch_time_created_at"
            r5.append(r6)     // Catch:{ all -> 0x014e }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x014e }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x014e }
            r6.<init>()     // Catch:{ all -> 0x014e }
            java.lang.String r7 = "SELECT "
            r6.append(r7)     // Catch:{ all -> 0x014e }
            r6.append(r4)     // Catch:{ all -> 0x014e }
            java.lang.String r4 = " AS "
            r6.append(r4)     // Catch:{ all -> 0x014e }
            r6.append(r0)     // Catch:{ all -> 0x014e }
            java.lang.String r4 = " FROM "
            r6.append(r4)     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r4 = r9.aa     // Catch:{ all -> 0x014e }
            r4.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r4 = "issues"
            r6.append(r4)     // Catch:{ all -> 0x014e }
            java.lang.String r4 = " INNER JOIN "
            r6.append(r4)     // Catch:{ all -> 0x014e }
            com.helpshift.common.a.c r4 = r9.aa     // Catch:{ all -> 0x014e }
            r4.getClass()     // Catch:{ all -> 0x014e }
            java.lang.String r4 = "messages"
            r6.append(r4)     // Catch:{ all -> 0x014e }
            java.lang.String r4 = " ON "
            r6.append(r4)     // Catch:{ all -> 0x014e }
            r6.append(r2)     // Catch:{ all -> 0x014e }
            java.lang.String r2 = " = "
            r6.append(r2)     // Catch:{ all -> 0x014e }
            r6.append(r3)     // Catch:{ all -> 0x014e }
            java.lang.String r2 = " WHERE "
            r6.append(r2)     // Catch:{ all -> 0x014e }
            r6.append(r1)     // Catch:{ all -> 0x014e }
            java.lang.String r1 = " = ? ORDER BY "
            r6.append(r1)     // Catch:{ all -> 0x014e }
            r6.append(r5)     // Catch:{ all -> 0x014e }
            java.lang.String r1 = "  ASC LIMIT 1"
            r6.append(r1)     // Catch:{ all -> 0x014e }
            java.lang.String r1 = r6.toString()     // Catch:{ all -> 0x014e }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ all -> 0x014e }
            r3 = 0
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ all -> 0x014e }
            r2[r3] = r10     // Catch:{ all -> 0x014e }
            r10 = 0
            com.helpshift.u.a.a r11 = r9.ab     // Catch:{ Exception -> 0x0139, all -> 0x0134 }
            android.database.sqlite.SQLiteDatabase r11 = r11.getReadableDatabase()     // Catch:{ Exception -> 0x0139, all -> 0x0134 }
            android.database.Cursor r11 = r11.rawQuery(r1, r2)     // Catch:{ Exception -> 0x0139, all -> 0x0134 }
            boolean r1 = r11.moveToFirst()     // Catch:{ Exception -> 0x0132 }
            if (r1 == 0) goto L_0x012c
            int r0 = r11.getColumnIndex(r0)     // Catch:{ Exception -> 0x0132 }
            java.lang.String r10 = r11.getString(r0)     // Catch:{ Exception -> 0x0132 }
        L_0x012c:
            if (r11 == 0) goto L_0x0145
        L_0x012e:
            r11.close()     // Catch:{ all -> 0x014e }
            goto L_0x0145
        L_0x0132:
            r0 = move-exception
            goto L_0x013b
        L_0x0134:
            r11 = move-exception
            r8 = r11
            r11 = r10
            r10 = r8
            goto L_0x0148
        L_0x0139:
            r0 = move-exception
            r11 = r10
        L_0x013b:
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in read messages"
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x0147 }
            if (r11 == 0) goto L_0x0145
            goto L_0x012e
        L_0x0145:
            monitor-exit(r9)
            return r10
        L_0x0147:
            r10 = move-exception
        L_0x0148:
            if (r11 == 0) goto L_0x014d
            r11.close()     // Catch:{ all -> 0x014e }
        L_0x014d:
            throw r10     // Catch:{ all -> 0x014e }
        L_0x014e:
            r10 = move-exception
            monitor-exit(r9)
            goto L_0x0152
        L_0x0151:
            throw r10
        L_0x0152:
            goto L_0x0151
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.h(long):java.lang.String");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:11:0x0072 */
    /* JADX WARN: Type inference failed for: r13v3 */
    /* JADX WARN: Type inference failed for: r13v4, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r13v5, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r13v7 */
    /* JADX WARN: Type inference failed for: r13v8 */
    /* JADX WARN: Type inference failed for: r13v9 */
    /* JADX WARN: Type inference failed for: r13v10 */
    /* JADX WARN: Type inference failed for: r13v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0072, code lost:
        if (r14 != null) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r14.close();
        r13 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0085, code lost:
        if (r14 != null) goto L_0x0074;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0090 A[SYNTHETIC, Splitter:B:27:0x0090] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.Long i(long r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            r0.<init>()     // Catch:{ all -> 0x0094 }
            com.helpshift.common.a.c r1 = r12.aa     // Catch:{ all -> 0x0094 }
            r1.getClass()     // Catch:{ all -> 0x0094 }
            java.lang.String r1 = "user_local_id"
            r0.append(r1)     // Catch:{ all -> 0x0094 }
            java.lang.String r1 = " = ?"
            r0.append(r1)     // Catch:{ all -> 0x0094 }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x0094 }
            r0 = 1
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch:{ all -> 0x0094 }
            java.lang.String r13 = java.lang.String.valueOf(r13)     // Catch:{ all -> 0x0094 }
            r14 = 0
            r6[r14] = r13     // Catch:{ all -> 0x0094 }
            r13 = 0
            com.helpshift.u.a.a r1 = r12.ab     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            android.database.sqlite.SQLiteDatabase r2 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            com.helpshift.common.a.c r1 = r12.aa     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r1.getClass()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r3 = "issues"
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            com.helpshift.common.a.c r0 = r12.aa     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r0.getClass()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r0 = "epoch_time_created_at"
            r4[r14] = r0     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r7 = 0
            r8 = 0
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r14.<init>()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            com.helpshift.common.a.c r0 = r12.aa     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r0.getClass()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r0 = "epoch_time_created_at"
            r14.append(r0)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r0 = " ASC"
            r14.append(r0)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r9 = r14.toString()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r10 = "1"
            android.database.Cursor r14 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            boolean r0 = r14.moveToFirst()     // Catch:{ Exception -> 0x0078 }
            if (r0 == 0) goto L_0x0072
            com.helpshift.common.a.c r0 = r12.aa     // Catch:{ Exception -> 0x0078 }
            r0.getClass()     // Catch:{ Exception -> 0x0078 }
            java.lang.String r0 = "epoch_time_created_at"
            java.lang.Class<java.lang.Long> r1 = java.lang.Long.class
            java.lang.Object r0 = com.helpshift.util.e.a(r14, r0, r1)     // Catch:{ Exception -> 0x0078 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Exception -> 0x0078 }
            r13 = r0
        L_0x0072:
            if (r14 == 0) goto L_0x0088
        L_0x0074:
            r14.close()     // Catch:{ all -> 0x0094 }
            goto L_0x0088
        L_0x0078:
            r0 = move-exception
            goto L_0x007e
        L_0x007a:
            r14 = move-exception
            goto L_0x008e
        L_0x007c:
            r0 = move-exception
            r14 = r13
        L_0x007e:
            java.lang.String r1 = "Helpshift_ConverDB"
            java.lang.String r2 = "Error in getting latest conversation created_at time"
            com.helpshift.util.n.c(r1, r2, r0)     // Catch:{ all -> 0x008a }
            if (r14 == 0) goto L_0x0088
            goto L_0x0074
        L_0x0088:
            monitor-exit(r12)
            return r13
        L_0x008a:
            r13 = move-exception
            r11 = r14
            r14 = r13
            r13 = r11
        L_0x008e:
            if (r13 == 0) goto L_0x0093
            r13.close()     // Catch:{ all -> 0x0094 }
        L_0x0093:
            throw r14     // Catch:{ all -> 0x0094 }
        L_0x0094:
            r13 = move-exception
            monitor-exit(r12)
            goto L_0x0098
        L_0x0097:
            throw r13
        L_0x0098:
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.a.a.i(long):java.lang.Long");
    }

    /* compiled from: ConversationDB */
    class b extends C0134a {
        final String h;
        final String i;

        b(JSONObject jSONObject) {
            super(jSONObject);
            this.h = jSONObject.optString("thumbnail_url", null);
            this.i = jSONObject.optString("thumbnailFilePath", null);
        }
    }

    /* renamed from: com.helpshift.common.a.a$a  reason: collision with other inner class name */
    /* compiled from: ConversationDB */
    class C0134a {

        /* renamed from: a  reason: collision with root package name */
        final String f3237a;

        /* renamed from: b  reason: collision with root package name */
        final String f3238b;
        final String c;
        final String d;
        final int e;
        final boolean f;

        C0134a(JSONObject jSONObject) {
            this.f3238b = jSONObject.optString("file_name", null);
            this.f3237a = jSONObject.optString("content_type", null);
            this.c = jSONObject.optString("url", null);
            this.e = jSONObject.optInt("size", 0);
            this.d = jSONObject.optString("filePath", null);
            this.f = jSONObject.optBoolean("is_secure", false);
        }
    }

    private b.C0138b a(JSONObject jSONObject, int i2) {
        return b.C0138b.a(jSONObject.optString("option_type", ""), i2);
    }

    private String c(JSONObject jSONObject) {
        return jSONObject.optString("suggestion_read_faq_publish_id", "");
    }

    private w f(JSONObject jSONObject) {
        return w.a(jSONObject.optString("referred_message_type", ""));
    }

    private String g(JSONObject jSONObject) {
        return jSONObject.optString("selected_option_data", "{}");
    }

    private String h(JSONObject jSONObject) {
        return jSONObject.optString("input_skip_label", "");
    }

    private String i(JSONObject jSONObject) {
        return jSONObject.optString("input_label", "");
    }

    private String j(JSONObject jSONObject) {
        return jSONObject.optString("input_placeholder", "");
    }

    public final synchronized void a(Faq faq) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("question_id", faq.j);
        contentValues.put("publish_id", faq.f3837b);
        contentValues.put("language", faq.c);
        contentValues.put("section_id", faq.d);
        contentValues.put("title", faq.f3836a);
        contentValues.put("body", faq.e);
        contentValues.put("helpful", Integer.valueOf(faq.f));
        contentValues.put("rtl", faq.g);
        contentValues.put("tags", String.valueOf(new JSONArray((Collection) faq.a())));
        contentValues.put("c_tags", String.valueOf(new JSONArray((Collection) faq.b())));
        String[] strArr = {faq.f3837b, faq.c};
        try {
            SQLiteDatabase writableDatabase = this.ab.getWritableDatabase();
            if (!a(writableDatabase, "faq_suggestions", "publish_id = ? AND language = ?", strArr)) {
                writableDatabase.insert("faq_suggestions", null, contentValues);
            } else {
                writableDatabase.update("faq_suggestions", contentValues, "publish_id = ? AND language = ?", strArr);
            }
        } catch (Exception e2) {
            n.c("Helpshift_ConverDB", "Error in insertOrUpdateAdminFAQSuggestion", e2);
        }
        return;
    }
}
