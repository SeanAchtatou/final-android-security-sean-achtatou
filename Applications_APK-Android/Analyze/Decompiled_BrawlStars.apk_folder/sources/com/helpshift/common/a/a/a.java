package com.helpshift.common.a.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.helpshift.common.a.c;
import com.helpshift.common.g.b;
import java.util.HashMap;
import java.util.Map;

/* compiled from: MigrationFromDb_6_to_7 */
public class a extends com.helpshift.common.d.a {

    /* renamed from: b  reason: collision with root package name */
    private c f3239b = new c();
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;

    public a(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase);
        StringBuilder sb = new StringBuilder();
        sb.append("ALTER TABLE ");
        this.f3239b.getClass();
        sb.append("conversation_inbox");
        sb.append(" ADD COLUMN ");
        this.f3239b.getClass();
        sb.append("has_older_messages");
        sb.append(" INT ;");
        this.c = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("ALTER TABLE ");
        this.f3239b.getClass();
        sb2.append("conversation_inbox");
        sb2.append(" ADD COLUMN ");
        this.f3239b.getClass();
        sb2.append("last_conv_redaction_time");
        sb2.append(" INT ;");
        this.d = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("ALTER TABLE ");
        this.f3239b.getClass();
        sb3.append("issues");
        sb3.append(" ADD COLUMN ");
        this.f3239b.getClass();
        sb3.append("full_privacy_enabled");
        sb3.append(" INTEGER ;");
        this.e = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("ALTER TABLE ");
        this.f3239b.getClass();
        sb4.append("issues");
        sb4.append(" ADD COLUMN ");
        this.f3239b.getClass();
        sb4.append("is_redacted");
        sb4.append(" INTEGER ;");
        this.f = sb4.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append("ALTER TABLE ");
        this.f3239b.getClass();
        sb5.append("issues");
        sb5.append(" ADD COLUMN ");
        this.f3239b.getClass();
        sb5.append("epoch_time_created_at");
        sb5.append(" INTEGER NOT NULL DEFAULT 0 ;");
        this.g = sb5.toString();
        StringBuilder sb6 = new StringBuilder();
        sb6.append("ALTER TABLE ");
        this.f3239b.getClass();
        sb6.append("messages");
        sb6.append(" ADD COLUMN ");
        this.f3239b.getClass();
        sb6.append("is_redacted");
        sb6.append(" INTEGER ;");
        this.h = sb6.toString();
        StringBuilder sb7 = new StringBuilder();
        sb7.append("ALTER TABLE ");
        this.f3239b.getClass();
        sb7.append("messages");
        sb7.append(" ADD COLUMN ");
        this.f3239b.getClass();
        sb7.append("epoch_time_created_at");
        sb7.append(" INTEGER NOT NULL DEFAULT 0 ;");
        this.i = sb7.toString();
    }

    public final void a() {
        this.f3302a.execSQL(this.c);
        this.f3302a.execSQL(this.d);
        this.f3302a.execSQL(this.e);
        this.f3302a.execSQL(this.f);
        this.f3302a.execSQL(this.i);
        this.f3302a.execSQL(this.h);
        this.f3302a.execSQL(this.g);
        HashMap hashMap = new HashMap();
        this.f3239b.getClass();
        this.f3239b.getClass();
        String[] strArr = {"_id", "created_at"};
        SQLiteDatabase sQLiteDatabase = this.f3302a;
        this.f3239b.getClass();
        Cursor query = sQLiteDatabase.query("issues", strArr, null, null, null, null, null);
        if (query.moveToFirst()) {
            do {
                this.f3239b.getClass();
                long j = query.getLong(query.getColumnIndex("_id"));
                this.f3239b.getClass();
                hashMap.put(Long.valueOf(j), query.getString(query.getColumnIndex("created_at")));
            } while (query.moveToNext());
        }
        query.close();
        HashMap hashMap2 = new HashMap();
        SQLiteDatabase sQLiteDatabase2 = this.f3302a;
        this.f3239b.getClass();
        Cursor query2 = sQLiteDatabase2.query("messages", strArr, null, null, null, null, null);
        if (query2.moveToFirst()) {
            do {
                this.f3239b.getClass();
                long j2 = query2.getLong(query2.getColumnIndex("_id"));
                this.f3239b.getClass();
                hashMap2.put(Long.valueOf(j2), query2.getString(query2.getColumnIndex("created_at")));
            } while (query2.moveToNext());
        }
        query2.close();
        HashMap hashMap3 = new HashMap();
        for (Map.Entry entry : hashMap.entrySet()) {
            hashMap3.put(entry.getKey(), Long.valueOf(b.b((String) entry.getValue())));
        }
        HashMap hashMap4 = new HashMap();
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            hashMap4.put(entry2.getKey(), Long.valueOf(b.b((String) entry2.getValue())));
        }
        for (Map.Entry entry3 : hashMap3.entrySet()) {
            ContentValues contentValues = new ContentValues();
            this.f3239b.getClass();
            contentValues.put("epoch_time_created_at", (Long) entry3.getValue());
            StringBuilder sb = new StringBuilder();
            this.f3239b.getClass();
            sb.append("_id");
            sb.append(" = ?");
            String sb2 = sb.toString();
            String[] strArr2 = {String.valueOf((Long) entry3.getKey())};
            SQLiteDatabase sQLiteDatabase3 = this.f3302a;
            this.f3239b.getClass();
            sQLiteDatabase3.update("issues", contentValues, sb2, strArr2);
        }
        for (Map.Entry entry4 : hashMap4.entrySet()) {
            ContentValues contentValues2 = new ContentValues();
            this.f3239b.getClass();
            contentValues2.put("epoch_time_created_at", (Long) entry4.getValue());
            StringBuilder sb3 = new StringBuilder();
            this.f3239b.getClass();
            sb3.append("_id");
            sb3.append(" = ?");
            String sb4 = sb3.toString();
            String[] strArr3 = {String.valueOf((Long) entry4.getKey())};
            SQLiteDatabase sQLiteDatabase4 = this.f3302a;
            this.f3239b.getClass();
            sQLiteDatabase4.update("messages", contentValues2, sb4, strArr3);
        }
    }
}
