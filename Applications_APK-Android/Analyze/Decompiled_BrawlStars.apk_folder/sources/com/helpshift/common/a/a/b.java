package com.helpshift.common.a.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.internal.ServerProtocol;
import com.helpshift.common.a.c;
import com.helpshift.common.d.a;
import com.helpshift.util.q;

/* compiled from: MigrationFromDb_7_to_8 */
public class b extends a {

    /* renamed from: b  reason: collision with root package name */
    private c f3240b = new c();
    private String c;
    private String d;

    public b(SQLiteDatabase sQLiteDatabase) {
        super(sQLiteDatabase);
        StringBuilder sb = new StringBuilder();
        this.f3240b.getClass();
        sb.append("issues");
        sb.append("_old");
        this.c = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("ALTER TABLE ");
        this.f3240b.getClass();
        sb2.append("issues");
        sb2.append(" RENAME TO ");
        sb2.append(this.c);
        this.d = sb2.toString();
    }

    private int a(String str) {
        Cursor rawQuery = this.f3302a.rawQuery(str, null);
        try {
            int i = 0;
            if (rawQuery.moveToFirst()) {
                i = rawQuery.getInt(0) - rawQuery.getInt(1);
            }
            return i;
        } finally {
            if (rawQuery != null) {
                rawQuery.close();
            }
        }
    }

    public final void a() {
        this.f3302a.execSQL(this.d);
        this.f3302a.execSQL(this.f3240b.Q);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT Count(");
        this.f3240b.getClass();
        sb.append("server_id");
        sb.append(") , Count(DISTINCT ");
        this.f3240b.getClass();
        sb.append("server_id");
        sb.append(") FROM ");
        sb.append(this.c);
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("SELECT Count(");
        this.f3240b.getClass();
        sb3.append("pre_conv_server_id");
        sb3.append(") , Count(DISTINCT ");
        this.f3240b.getClass();
        sb3.append("pre_conv_server_id");
        sb3.append(") FROM ");
        sb3.append(this.c);
        if (a(sb2) + a(sb3.toString()) == 0) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("INSERT INTO ");
            this.f3240b.getClass();
            sb4.append("issues");
            sb4.append(" (");
            this.f3240b.getClass();
            sb4.append("_id");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("server_id");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("pre_conv_server_id");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("publish_id");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("uuid");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("user_local_id");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("title");
            sb4.append(",");
            this.f3240b.getClass();
            sb4.append("issue_type");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append(ServerProtocol.DIALOG_PARAM_STATE);
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("show_agent_name");
            sb4.append(",");
            this.f3240b.getClass();
            sb4.append("message_cursor");
            sb4.append(",");
            this.f3240b.getClass();
            sb4.append("start_new_conversation_action");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("is_redacted");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("meta");
            sb4.append(",");
            this.f3240b.getClass();
            sb4.append("last_user_activity_time");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("full_privacy_enabled");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("epoch_time_created_at");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("created_at");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("updated_at");
            sb4.append(" ) SELECT ");
            this.f3240b.getClass();
            sb4.append("_id");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("server_id");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("pre_conv_server_id");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("publish_id");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("uuid");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("user_local_id");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("title");
            sb4.append(",");
            this.f3240b.getClass();
            sb4.append("issue_type");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append(ServerProtocol.DIALOG_PARAM_STATE);
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("show_agent_name");
            sb4.append(",");
            this.f3240b.getClass();
            sb4.append("message_cursor");
            sb4.append(",");
            this.f3240b.getClass();
            sb4.append("start_new_conversation_action");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("is_redacted");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("meta");
            sb4.append(",");
            this.f3240b.getClass();
            sb4.append("last_user_activity_time");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("full_privacy_enabled");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("epoch_time_created_at");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("created_at");
            sb4.append(", ");
            this.f3240b.getClass();
            sb4.append("updated_at");
            sb4.append(" FROM ");
            sb4.append(this.c);
            this.f3302a.execSQL(sb4.toString());
        } else {
            q.c().C();
        }
        SQLiteDatabase sQLiteDatabase = this.f3302a;
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + this.c);
    }
}
