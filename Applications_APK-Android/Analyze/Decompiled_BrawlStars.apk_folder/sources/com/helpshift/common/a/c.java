package com.helpshift.common.a;

import com.facebook.internal.ServerProtocol;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: ConversationDBInfo */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public static final Integer f3242a = 8;
    public final String A = "type";
    public final String B = "meta";
    public final String C = "md_state";
    public final String D = "is_redacted";
    public final String E = "has_older_messages";
    public final String F = "last_conv_redaction_time";
    final String G = "form_name";
    final String H = "form_email";
    final String I = "description_draft";
    final String J = "description_draft_timestamp";
    final String K = "attachment_draft";
    final String L = "description_type";
    final String M = "archival_text";
    final String N = "reply_text";
    final String O = "persist_message_box";
    final String P = "since";
    public String Q = "CREATE TABLE issues ( _id INTEGER PRIMARY KEY AUTOINCREMENT,server_id TEXT UNIQUE, pre_conv_server_id TEXT UNIQUE, publish_id TEXT, uuid TEXT NOT NULL, user_local_id TEXT NOT NULL, title TEXT NOT NULL,issue_type TEXT NOT NULL, state INTEGER NOT NULL, show_agent_name INTEGER,message_cursor TEXT,start_new_conversation_action INTEGER, is_redacted INTEGER, meta TEXT,last_user_activity_time INTEGER, full_privacy_enabled INTEGER, epoch_time_created_at INTEGER NOT NULL, created_at TEXT NOT NULL,updated_at TEXT NOT NULL  );";
    public String R = "CREATE TABLE conversation_inbox ( user_local_id TEXT PRIMARY KEY NOT NULL, form_name TEXT,form_email TEXT,description_draft TEXT,description_draft_timestamp TEXT,attachment_draft TEXT,description_type TEXT,archival_text TEXT, reply_text TEXT, persist_message_box INT, since TEXT, has_older_messages INT, last_conv_redaction_time INT );";
    public String S = "CREATE TABLE messages ( _id INTEGER PRIMARY KEY AUTOINCREMENT, server_id TEXT, conversation_id TEXT, body TEXT, author_name TEXT, type TEXT, meta TEXT, is_redacted INTEGER, created_at TEXT, epoch_time_created_at INTEGER NOT NULL, md_state INTEGER  );";
    private final String T = "CREATE INDEX SERVER_IDX ON messages(server_id)";
    private String U = "CREATE TABLE faq_suggestions ( _id INTEGER PRIMARY KEY AUTOINCREMENT,question_id TEXT NOT NULL,publish_id TEXT NOT NULL,language TEXT NOT NULL,section_id TEXT NOT NULL,title TEXT NOT NULL,body TEXT NOT NULL,helpful INTEGER,rtl INTEGER,tags TEXT,c_tags TEXT );";

    /* renamed from: b  reason: collision with root package name */
    public final String f3243b = "_id";
    public final String c = "created_at";
    public final String d = "epoch_time_created_at";
    public final String e = "issues";
    public final String f = "messages";
    public final String g = "conversation_inbox";
    public final String h = "server_id";
    public final String i = "publish_id";
    public final String j = "uuid";
    public final String k = "user_local_id";
    public final String l = "title";
    public final String m = "updated_at";
    public final String n = ServerProtocol.DIALOG_PARAM_STATE;
    public final String o = "show_agent_name";
    public final String p = "message_cursor";
    public final String q = "start_new_conversation_action";
    public final String r = "is_redacted";
    public final String s = "meta";
    public final String t = "pre_conv_server_id";
    public final String u = "last_user_activity_time";
    public final String v = "issue_type";
    public final String w = "full_privacy_enabled";
    public final String x = "conversation_id";
    public final String y = "body";
    public final String z = "author_name";

    public final List<String> a() {
        return new ArrayList(Arrays.asList(this.Q, this.R, this.S, "CREATE INDEX SERVER_IDX ON messages(server_id)", this.U));
    }

    public final List<String> b() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("DROP TABLE IF EXISTS issues");
        arrayList.add("DROP TABLE IF EXISTS conversation_inbox");
        arrayList.add("DROP TABLE IF EXISTS messages");
        arrayList.add("DROP TABLE IF EXISTS faq_suggestions");
        arrayList.add("DROP TABLE IF EXISTS issues_old");
        arrayList.addAll(a());
        return arrayList;
    }
}
