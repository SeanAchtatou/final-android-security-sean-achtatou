package com.helpshift.common.exception;

public class RootAPIException extends RuntimeException {

    /* renamed from: a  reason: collision with root package name */
    public final String f3358a;

    /* renamed from: b  reason: collision with root package name */
    public final Exception f3359b;
    public final a c;

    private RootAPIException(Exception exc, a aVar, String str) {
        super(str, exc);
        this.f3359b = exc;
        this.c = aVar;
        this.f3358a = str;
    }

    public static RootAPIException a(Exception exc, a aVar) {
        return a(exc, aVar, null);
    }

    public static RootAPIException a(Exception exc, a aVar, String str) {
        if (exc instanceof RootAPIException) {
            RootAPIException rootAPIException = (RootAPIException) exc;
            Exception exc2 = rootAPIException.f3359b;
            if (aVar == null) {
                aVar = rootAPIException.c;
            }
            if (str == null) {
                str = rootAPIException.f3358a;
            }
            exc = exc2;
        } else if (aVar == null) {
            aVar = e.GENERIC;
        }
        return new RootAPIException(exc, aVar, str);
    }

    public final int a() {
        a aVar = this.c;
        if (aVar instanceof b) {
            return ((b) aVar).u;
        }
        return 0;
    }

    public final boolean b() {
        return this.f3359b != null;
    }

    public static RootAPIException a(Exception exc) {
        return a(exc, null, null);
    }
}
