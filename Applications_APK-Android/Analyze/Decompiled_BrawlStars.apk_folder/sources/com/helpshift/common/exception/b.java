package com.helpshift.common.exception;

/* compiled from: NetworkException */
public enum b implements a {
    GENERIC,
    NO_CONNECTION,
    UNKNOWN_HOST,
    SSL_PEER_UNVERIFIED,
    SSL_HANDSHAKE,
    UNHANDLED_STATUS_CODE,
    TIMESTAMP_CORRECTION_RETRIES_EXHAUSTED,
    ENTITY_TOO_LARGE_RETRIES_EXHAUSTED,
    CONTENT_NOT_FOUND,
    UNSUPPORTED_ENCODING_EXCEPTION,
    UNABLE_TO_GENERATE_SIGNATURE,
    UNSUPPORTED_MIME_TYPE,
    NON_RETRIABLE,
    CONVERSATION_ARCHIVED,
    SCREENSHOT_UPLOAD_ERROR,
    INVALID_AUTH_TOKEN,
    AUTH_TOKEN_NOT_PROVIDED,
    USER_PRE_CONDITION_FAILED,
    USER_NOT_FOUND,
    CONTENT_UNCHANGED;
    
    public int u;
    public String v;
}
