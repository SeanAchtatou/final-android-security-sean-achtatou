package com.helpshift.common;

import java.security.SecureRandom;

/* compiled from: StringUtils */
public class k {
    public static boolean a(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static String a(CharSequence charSequence, Iterable iterable) {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Object next : iterable) {
            if (z) {
                z = false;
            } else {
                sb.append(charSequence);
            }
            sb.append(next);
        }
        return sb.toString();
    }

    public static boolean b(String str) {
        return str == null || str.length() == 0;
    }

    public static String a(char[] cArr, int i) {
        if (cArr == null || cArr.length == 0) {
            return null;
        }
        SecureRandom secureRandom = new SecureRandom();
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < 15; i2++) {
            sb.append(cArr[secureRandom.nextInt(cArr.length)]);
        }
        return sb.toString();
    }

    public static boolean a(String str, String str2) {
        return (str == null && str2 == null) || (str != null && str.equals(str2));
    }
}
