package com.helpshift.common.g;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/* compiled from: HSSimpleDateFormat */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private SimpleDateFormat f3384a;

    public e(String str, String str2) {
        this.f3384a = new SimpleDateFormat(str);
        this.f3384a.setTimeZone(TimeZone.getTimeZone(str2));
    }

    public e(String str, Locale locale) {
        this.f3384a = new SimpleDateFormat(str, locale);
    }

    public e(String str, Locale locale, String str2) {
        this.f3384a = new SimpleDateFormat(str, locale);
        this.f3384a.setTimeZone(TimeZone.getTimeZone(str2));
    }

    public final synchronized Date a(String str) throws ParseException {
        return this.f3384a.parse(str);
    }

    public final synchronized String a(Date date) {
        return this.f3384a.format(date);
    }
}
