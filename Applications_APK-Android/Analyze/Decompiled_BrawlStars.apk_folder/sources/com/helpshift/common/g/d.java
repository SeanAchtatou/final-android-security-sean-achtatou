package com.helpshift.common.g;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* compiled from: HSObservableList */
public class d<T> extends ArrayList<T> {

    /* renamed from: a  reason: collision with root package name */
    public c<T> f3383a;

    public d() {
    }

    public d(List<T> list) {
        super(list);
    }

    public boolean add(T t) {
        c<T> cVar;
        boolean add = super.add(t);
        if (add && (cVar = this.f3383a) != null) {
            cVar.a((Object) t);
        }
        return add;
    }

    public boolean addAll(Collection<? extends T> collection) {
        c<T> cVar;
        boolean addAll = super.addAll(collection);
        if (addAll && (cVar = this.f3383a) != null) {
            cVar.a((Collection) collection);
        }
        return addAll;
    }

    public final void a(Collection<? extends T> collection) {
        super.addAll(0, collection);
    }

    public final T a(int i, T t) {
        c<T> cVar;
        T t2 = super.set(i, t);
        if (!(t2 == null || (cVar = this.f3383a) == null)) {
            cVar.b(t);
        }
        return t2;
    }
}
