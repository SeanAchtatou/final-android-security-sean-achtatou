package com.helpshift.common.g;

import com.helpshift.common.e.ab;
import com.helpshift.util.aa;
import com.helpshift.util.n;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* compiled from: HSDateFormatSpec */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public static final e f3381a = new e("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "GMT");

    /* renamed from: b  reason: collision with root package name */
    private static final Map<String, e> f3382b = new HashMap();

    public static e a(String str, Locale locale, String str2) {
        String str3 = str + "_" + locale.getLanguage() + "_" + str2;
        e eVar = f3382b.get(str3);
        if (eVar != null) {
            return eVar;
        }
        e eVar2 = new e(str, locale, str2);
        f3382b.put(str3, eVar2);
        return eVar2;
    }

    public static e a(String str, Locale locale) {
        String str2 = str + "_" + locale.getLanguage();
        e eVar = f3382b.get(str2);
        if (eVar != null) {
            return eVar;
        }
        e eVar2 = new e(str, locale);
        f3382b.put(str2, eVar2);
        return eVar2;
    }

    private static long c(ab abVar) {
        float a2 = abVar.t().a();
        return System.currentTimeMillis() + ((a2 <= -0.001f || a2 >= 0.001f) ? (long) (a2 * 1000.0f) : 0);
    }

    public static Date a(ab abVar) {
        return new Date(c(abVar));
    }

    public static aa<String, Long> b(ab abVar) {
        Long valueOf = Long.valueOf(c(abVar));
        return new aa<>(f3381a.a(new Date(valueOf.longValue())), valueOf);
    }

    public static String a(e eVar, String str, int i) {
        try {
            Date a2 = eVar.a(str);
            Calendar instance = Calendar.getInstance();
            instance.setTime(a2);
            return eVar.a(new Date(instance.getTimeInMillis() + ((long) i)));
        } catch (ParseException e) {
            n.c("Helpshift_DFSpec", "Parsing exception on adding millisecond", e);
            return str;
        }
    }

    public static float a(String str) {
        Double valueOf = Double.valueOf(Double.parseDouble(str));
        double currentTimeMillis = (double) System.currentTimeMillis();
        Double.isNaN(currentTimeMillis);
        return (float) (valueOf.doubleValue() - Double.valueOf(currentTimeMillis / 1000.0d).doubleValue());
    }

    public static long b(String str) {
        try {
            return f3381a.a(str).getTime();
        } catch (ParseException e) {
            n.c("Helpshift_DFSpec", "Parsing exception on converting storageTimeFormat to epochTime", e);
            return -1;
        }
    }
}
