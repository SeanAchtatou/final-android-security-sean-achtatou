package com.helpshift.common.g;

import java.util.Map;

/* compiled from: MapUtil */
public class f {
    public static <T> T a(Map<String, Object> map, String str, Class<T> cls, T t) {
        Object obj = map.get(str);
        return cls.isInstance(obj) ? cls.cast(obj) : t;
    }
}
