package com.helpshift.common.g;

import com.helpshift.common.k;
import java.io.File;

/* compiled from: FileUtil */
public class a {
    public static File a(String str) {
        if (k.a(str)) {
            return null;
        }
        File file = new File(str);
        if (file.exists()) {
            return file;
        }
        return null;
    }

    public static boolean b(String str) {
        File a2 = a(str);
        return a2 != null && a2.canRead();
    }

    public static boolean c(String str) {
        File a2 = a(str);
        return a2 != null && a2.delete();
    }
}
