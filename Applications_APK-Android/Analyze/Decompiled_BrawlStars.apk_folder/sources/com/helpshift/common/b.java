package com.helpshift.common;

import com.helpshift.common.c.b.p;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.f.c;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: AutoRetryFailedEventDM */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public final j f3244a;

    /* renamed from: b  reason: collision with root package name */
    AtomicBoolean f3245b = new AtomicBoolean(false);
    public AtomicBoolean c = new AtomicBoolean(false);
    public Set<a> d = Collections.synchronizedSet(new LinkedHashSet());
    private final ab e;
    private final c f;
    private boolean g = true;
    private Map<a, a> h = new HashMap();

    /* compiled from: AutoRetryFailedEventDM */
    public enum a {
        MIGRATION,
        SYNC_USER,
        PUSH_TOKEN,
        CLEAR_USER,
        CONVERSATION,
        FAQ,
        ANALYTICS
    }

    public b(j jVar, ab abVar, c cVar) {
        this.f3244a = jVar;
        this.e = abVar;
        this.f = cVar;
    }

    public final void a(a aVar, a aVar2) {
        this.h.put(aVar, aVar2);
    }

    public final void a(a aVar, int i) {
        this.d.add(aVar);
        if (!b(aVar)) {
            a(i, this.d);
        } else if (i == p.H.intValue() || i == p.G.intValue()) {
            this.g = false;
        } else {
            a(i, this.d);
        }
    }

    public final void a(a aVar) {
        this.f3244a.b(new d(this, aVar));
    }

    public final void a() {
        this.f.f3375a.a();
    }

    private void a(int i, Set<a> set) {
        if (this.f3245b.compareAndSet(false, true)) {
            long a2 = this.f.a(i);
            if (a2 != -100) {
                this.f3244a.a(new e(this, set), a2);
            } else {
                this.f3245b.compareAndSet(true, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0032 A[Catch:{ RootAPIException -> 0x0052, RootAPIException -> 0x006c }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0033 A[Catch:{ RootAPIException -> 0x0052, RootAPIException -> 0x006c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.util.Set<com.helpshift.common.b.a> r6) {
        /*
            r5 = this;
            com.helpshift.common.e.ab r0 = r5.e
            boolean r0 = r0.A()
            r1 = 0
            if (r0 != 0) goto L_0x000d
            r5.a(r1, r6)
            return
        L_0x000d:
            java.util.LinkedList r0 = new java.util.LinkedList
            r0.<init>(r6)
            java.util.Iterator r0 = r0.iterator()     // Catch:{ RootAPIException -> 0x006c }
        L_0x0016:
            boolean r2 = r0.hasNext()     // Catch:{ RootAPIException -> 0x006c }
            if (r2 == 0) goto L_0x0064
            java.lang.Object r2 = r0.next()     // Catch:{ RootAPIException -> 0x006c }
            com.helpshift.common.b$a r2 = (com.helpshift.common.b.a) r2     // Catch:{ RootAPIException -> 0x006c }
            boolean r3 = b(r2)     // Catch:{ RootAPIException -> 0x006c }
            if (r3 == 0) goto L_0x002f
            boolean r3 = r5.g     // Catch:{ RootAPIException -> 0x006c }
            if (r3 == 0) goto L_0x002d
            goto L_0x002f
        L_0x002d:
            r3 = 0
            goto L_0x0030
        L_0x002f:
            r3 = 1
        L_0x0030:
            if (r3 != 0) goto L_0x0033
            goto L_0x0016
        L_0x0033:
            java.util.Map<com.helpshift.common.b$a, com.helpshift.common.a> r3 = r5.h     // Catch:{ RootAPIException -> 0x006c }
            java.lang.Object r3 = r3.get(r2)     // Catch:{ RootAPIException -> 0x006c }
            com.helpshift.common.a r3 = (com.helpshift.common.a) r3     // Catch:{ RootAPIException -> 0x006c }
            if (r3 != 0) goto L_0x0046
            java.util.Set<com.helpshift.common.b$a> r3 = r5.d     // Catch:{ RootAPIException -> 0x006c }
            r3.remove(r2)     // Catch:{ RootAPIException -> 0x006c }
            r6.remove(r2)     // Catch:{ RootAPIException -> 0x006c }
            goto L_0x0016
        L_0x0046:
            r3.a(r2)     // Catch:{ RootAPIException -> 0x0052 }
            java.util.Set<com.helpshift.common.b$a> r3 = r5.d     // Catch:{ RootAPIException -> 0x0052 }
            r3.remove(r2)     // Catch:{ RootAPIException -> 0x0052 }
            r6.remove(r2)     // Catch:{ RootAPIException -> 0x0052 }
            goto L_0x0016
        L_0x0052:
            r2 = move-exception
            com.helpshift.common.exception.a r3 = r2.c     // Catch:{ RootAPIException -> 0x006c }
            com.helpshift.common.exception.b r4 = com.helpshift.common.exception.b.INVALID_AUTH_TOKEN     // Catch:{ RootAPIException -> 0x006c }
            if (r3 == r4) goto L_0x0061
            com.helpshift.common.exception.a r3 = r2.c     // Catch:{ RootAPIException -> 0x006c }
            com.helpshift.common.exception.b r4 = com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED     // Catch:{ RootAPIException -> 0x006c }
            if (r3 != r4) goto L_0x0060
            goto L_0x0061
        L_0x0060:
            throw r2     // Catch:{ RootAPIException -> 0x006c }
        L_0x0061:
            r5.g = r1     // Catch:{ RootAPIException -> 0x006c }
            goto L_0x0016
        L_0x0064:
            com.helpshift.common.f.c r0 = r5.f     // Catch:{ RootAPIException -> 0x006c }
            com.helpshift.common.f.b r0 = r0.f3375a     // Catch:{ RootAPIException -> 0x006c }
            r0.a()     // Catch:{ RootAPIException -> 0x006c }
            goto L_0x0074
        L_0x006c:
            r0 = move-exception
            int r0 = r0.a()
            r5.a(r0, r6)
        L_0x0074:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.common.b.a(java.util.Set):void");
    }

    public final void b() {
        if (!this.g) {
            this.g = true;
            this.f3244a.b(new f(this));
        }
    }

    private static boolean b(a aVar) {
        int i = g.f3380a[aVar.ordinal()];
        return i == 1 || i == 2 || i == 3;
    }
}
