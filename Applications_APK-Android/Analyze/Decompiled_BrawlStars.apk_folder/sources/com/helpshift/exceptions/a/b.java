package com.helpshift.exceptions.a;

import android.content.Context;
import com.helpshift.p.b.a;
import com.helpshift.util.f;
import com.helpshift.util.n;
import java.lang.Thread;

/* compiled from: UncaughtExceptionHandler */
final class b implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3389a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Thread.UncaughtExceptionHandler f3390b;

    b(Context context, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.f3389a = context;
        this.f3390b = uncaughtExceptionHandler;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        if (a.a(th)) {
            n.c("UncaughtExceptionHandler", "UNCAUGHT EXCEPTION ", th, (a[]) f.a(this.f3389a, thread).toArray(new a[0]));
        }
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.f3390b;
        if (uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(thread, th);
        }
    }
}
