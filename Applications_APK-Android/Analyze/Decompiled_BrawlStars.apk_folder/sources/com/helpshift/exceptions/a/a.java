package com.helpshift.exceptions.a;

import android.content.Context;
import android.util.Log;

/* compiled from: UncaughtExceptionHandler */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final CharSequence f3388a = "com.helpshift";

    public static void a(Context context) {
        Thread.setDefaultUncaughtExceptionHandler(new b(context, Thread.getDefaultUncaughtExceptionHandler()));
    }

    static boolean a(Throwable th) {
        if (th == null) {
            return false;
        }
        return Log.getStackTraceString(th).contains(f3388a);
    }
}
