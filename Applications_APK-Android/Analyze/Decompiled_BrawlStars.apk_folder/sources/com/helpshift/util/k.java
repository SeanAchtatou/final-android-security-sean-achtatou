package com.helpshift.util;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.widget.TextView;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: HSLinkify */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private static final b f4251a = new l();

    /* compiled from: HSLinkify */
    public interface a {
        void a(String str);
    }

    /* compiled from: HSLinkify */
    public interface b {
        boolean a(CharSequence charSequence, int i);
    }

    /* compiled from: HSLinkify */
    public interface c {
        String a();
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x016c */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x016c A[LOOP:5: B:49:0x016c->B:109:0x016c, LOOP_START, PHI: r3 r5 
      PHI: (r3v19 java.lang.String) = (r3v18 java.lang.String), (r3v20 java.lang.String) binds: [B:48:0x0167, B:109:0x016c] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r5v12 int) = (r5v11 int), (r5v13 int) binds: [B:48:0x0167, B:109:0x016c] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:49:0x016c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(android.text.Spannable r19, int r20, com.helpshift.util.k.a r21) {
        /*
            r0 = r19
            r1 = 0
            if (r20 != 0) goto L_0x0006
            return r1
        L_0x0006:
            int r2 = r19.length()
            java.lang.Class<android.text.style.URLSpan> r3 = android.text.style.URLSpan.class
            java.lang.Object[] r2 = r0.getSpans(r1, r2, r3)
            android.text.style.URLSpan[] r2 = (android.text.style.URLSpan[]) r2
            int r3 = r2.length
            r4 = 1
            int r3 = r3 - r4
        L_0x0015:
            if (r3 < 0) goto L_0x001f
            r5 = r2[r3]
            r0.removeSpan(r5)
            int r3 = r3 + -1
            goto L_0x0015
        L_0x001f:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r3 = r20 & 1
            if (r3 == 0) goto L_0x00d2
            java.util.regex.Pattern r3 = android.util.Patterns.WEB_URL
            java.util.regex.Matcher r3 = r3.matcher(r0)
        L_0x002e:
            boolean r5 = r3.find()
            if (r5 == 0) goto L_0x00d2
            int r5 = r3.start()
            int r6 = r3.end()
            com.helpshift.util.k$b r7 = com.helpshift.util.k.f4251a
            if (r7 == 0) goto L_0x0046
            boolean r7 = r7.a(r0, r5)
            if (r7 == 0) goto L_0x002e
        L_0x0046:
            com.helpshift.util.u r7 = new com.helpshift.util.u
            r7.<init>()
            java.lang.String r14 = r3.group(r1)
            r15 = 3
            java.lang.String[] r13 = new java.lang.String[r15]
            java.lang.String r8 = "http://"
            r13[r1] = r8
            java.lang.String r8 = "https://"
            r13[r4] = r8
            r8 = 2
            java.lang.String r9 = "rtsp://"
            r13[r8] = r9
            r12 = 0
        L_0x0060:
            if (r12 >= r15) goto L_0x00b1
            r9 = 1
            r10 = 0
            r11 = r13[r12]
            r16 = 0
            r8 = r13[r12]
            int r17 = r8.length()
            r8 = r14
            r18 = r12
            r12 = r16
            r16 = r13
            r13 = r17
            boolean r8 = r8.regionMatches(r9, r10, r11, r12, r13)
            if (r8 == 0) goto L_0x00ac
            r9 = 0
            r10 = 0
            r11 = r16[r18]
            r12 = 0
            r8 = r16[r18]
            int r13 = r8.length()
            r8 = r14
            boolean r8 = r8.regionMatches(r9, r10, r11, r12, r13)
            if (r8 != 0) goto L_0x00aa
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r9 = r16[r18]
            r8.append(r9)
            r9 = r16[r18]
            int r9 = r9.length()
            java.lang.String r9 = r14.substring(r9)
            r8.append(r9)
            java.lang.String r14 = r8.toString()
        L_0x00aa:
            r8 = 1
            goto L_0x00b4
        L_0x00ac:
            int r12 = r18 + 1
            r13 = r16
            goto L_0x0060
        L_0x00b1:
            r16 = r13
            r8 = 0
        L_0x00b4:
            if (r8 != 0) goto L_0x00c7
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r9 = r16[r1]
            r8.append(r9)
            r8.append(r14)
            java.lang.String r14 = r8.toString()
        L_0x00c7:
            r7.f4259a = r14
            r7.f4260b = r5
            r7.c = r6
            r2.add(r7)
            goto L_0x002e
        L_0x00d2:
            r3 = r20 & 2
            if (r3 == 0) goto L_0x0163
            java.util.regex.Pattern r3 = android.util.Patterns.EMAIL_ADDRESS
            java.util.regex.Matcher r3 = r3.matcher(r0)
        L_0x00dc:
            boolean r5 = r3.find()
            if (r5 == 0) goto L_0x0163
            int r5 = r3.start()
            int r6 = r3.end()
            com.helpshift.util.u r7 = new com.helpshift.util.u
            r7.<init>()
            java.lang.String r14 = r3.group(r1)
            java.lang.String[] r15 = new java.lang.String[r4]
            java.lang.String r8 = "mailto:"
            r15[r1] = r8
            r13 = 0
        L_0x00fa:
            if (r13 >= r4) goto L_0x0144
            r9 = 1
            r10 = 0
            r11 = r15[r13]
            r12 = 0
            r8 = r15[r13]
            int r16 = r8.length()
            r8 = r14
            r17 = r13
            r13 = r16
            boolean r8 = r8.regionMatches(r9, r10, r11, r12, r13)
            if (r8 == 0) goto L_0x0141
            r9 = 0
            r10 = 0
            r11 = r15[r17]
            r12 = 0
            r8 = r15[r17]
            int r13 = r8.length()
            r8 = r14
            boolean r8 = r8.regionMatches(r9, r10, r11, r12, r13)
            if (r8 != 0) goto L_0x013f
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r9 = r15[r17]
            r8.append(r9)
            r9 = r15[r17]
            int r9 = r9.length()
            java.lang.String r9 = r14.substring(r9)
            r8.append(r9)
            java.lang.String r14 = r8.toString()
        L_0x013f:
            r8 = 1
            goto L_0x0145
        L_0x0141:
            int r13 = r17 + 1
            goto L_0x00fa
        L_0x0144:
            r8 = 0
        L_0x0145:
            if (r8 != 0) goto L_0x0158
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r9 = r15[r1]
            r8.append(r9)
            r8.append(r14)
            java.lang.String r14 = r8.toString()
        L_0x0158:
            r7.f4259a = r14
            r7.f4260b = r5
            r7.c = r6
            r2.add(r7)
            goto L_0x00dc
        L_0x0163:
            r3 = r20 & 8
            if (r3 == 0) goto L_0x01ab
            java.lang.String r3 = r19.toString()
            r5 = 0
        L_0x016c:
            java.lang.String r6 = android.webkit.WebView.findAddress(r3)     // Catch:{ UnsupportedOperationException -> 0x01aa }
            if (r6 == 0) goto L_0x01ab
            int r7 = r3.indexOf(r6)     // Catch:{ UnsupportedOperationException -> 0x01aa }
            if (r7 >= 0) goto L_0x0179
            goto L_0x01ab
        L_0x0179:
            com.helpshift.util.u r8 = new com.helpshift.util.u     // Catch:{ UnsupportedOperationException -> 0x01aa }
            r8.<init>()     // Catch:{ UnsupportedOperationException -> 0x01aa }
            int r9 = r6.length()     // Catch:{ UnsupportedOperationException -> 0x01aa }
            int r9 = r9 + r7
            int r7 = r7 + r5
            r8.f4260b = r7     // Catch:{ UnsupportedOperationException -> 0x01aa }
            int r5 = r5 + r9
            r8.c = r5     // Catch:{ UnsupportedOperationException -> 0x01aa }
            java.lang.String r3 = r3.substring(r9)     // Catch:{ UnsupportedOperationException -> 0x01aa }
            java.lang.String r7 = "UTF-8"
            java.lang.String r6 = java.net.URLEncoder.encode(r6, r7)     // Catch:{ UnsupportedEncodingException -> 0x016c }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ UnsupportedOperationException -> 0x01aa }
            r7.<init>()     // Catch:{ UnsupportedOperationException -> 0x01aa }
            java.lang.String r9 = "geo:0,0?q="
            r7.append(r9)     // Catch:{ UnsupportedOperationException -> 0x01aa }
            r7.append(r6)     // Catch:{ UnsupportedOperationException -> 0x01aa }
            java.lang.String r6 = r7.toString()     // Catch:{ UnsupportedOperationException -> 0x01aa }
            r8.f4259a = r6     // Catch:{ UnsupportedOperationException -> 0x01aa }
            r2.add(r8)     // Catch:{ UnsupportedOperationException -> 0x01aa }
            goto L_0x016c
        L_0x01aa:
        L_0x01ab:
            r3 = r20 & 4
            if (r3 == 0) goto L_0x01ee
            java.util.regex.Pattern r3 = android.util.Patterns.PHONE
            java.util.regex.Matcher r3 = r3.matcher(r0)
        L_0x01b5:
            boolean r5 = r3.find()
            if (r5 == 0) goto L_0x01ee
            java.lang.String r5 = r3.group()
            int r6 = r5.length()
            r7 = 6
            if (r6 < r7) goto L_0x01b5
            com.helpshift.util.u r6 = new com.helpshift.util.u
            r6.<init>()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "tel:"
            r7.append(r8)
            r7.append(r5)
            java.lang.String r5 = r7.toString()
            r6.f4259a = r5
            int r5 = r3.start()
            r6.f4260b = r5
            int r5 = r3.end()
            r6.c = r5
            r2.add(r6)
            goto L_0x01b5
        L_0x01ee:
            com.helpshift.util.m r3 = new com.helpshift.util.m
            r3.<init>()
            java.util.Collections.sort(r2, r3)
            int r3 = r2.size()
            r5 = r3
            r3 = 0
        L_0x01fc:
            int r6 = r5 + -1
            if (r3 >= r6) goto L_0x0249
            java.lang.Object r6 = r2.get(r3)
            com.helpshift.util.u r6 = (com.helpshift.util.u) r6
            int r7 = r3 + 1
            java.lang.Object r8 = r2.get(r7)
            com.helpshift.util.u r8 = (com.helpshift.util.u) r8
            int r9 = r6.f4260b
            int r10 = r8.f4260b
            if (r9 > r10) goto L_0x0247
            int r9 = r6.c
            int r10 = r8.f4260b
            if (r9 <= r10) goto L_0x0247
            int r9 = r8.c
            int r10 = r6.c
            r11 = -1
            if (r9 > r10) goto L_0x0223
        L_0x0221:
            r6 = r7
            goto L_0x023f
        L_0x0223:
            int r9 = r6.c
            int r10 = r6.f4260b
            int r9 = r9 - r10
            int r10 = r8.c
            int r12 = r8.f4260b
            int r10 = r10 - r12
            if (r9 <= r10) goto L_0x0230
            goto L_0x0221
        L_0x0230:
            int r9 = r6.c
            int r6 = r6.f4260b
            int r9 = r9 - r6
            int r6 = r8.c
            int r8 = r8.f4260b
            int r6 = r6 - r8
            if (r9 >= r6) goto L_0x023e
            r6 = r3
            goto L_0x023f
        L_0x023e:
            r6 = -1
        L_0x023f:
            if (r6 == r11) goto L_0x0247
            r2.remove(r6)
            int r5 = r5 + -1
            goto L_0x01fc
        L_0x0247:
            r3 = r7
            goto L_0x01fc
        L_0x0249:
            int r3 = r2.size()
            if (r3 != 0) goto L_0x0250
            return r1
        L_0x0250:
            java.util.Iterator r1 = r2.iterator()
        L_0x0254:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0273
            java.lang.Object r2 = r1.next()
            com.helpshift.util.u r2 = (com.helpshift.util.u) r2
            com.helpshift.util.HSLinkify$3 r3 = new com.helpshift.util.HSLinkify$3
            java.lang.String r5 = r2.f4259a
            r6 = r21
            r3.<init>(r5, r6, r2)
            int r5 = r2.f4260b
            int r2 = r2.c
            r7 = 33
            r0.setSpan(r3, r5, r2, r7)
            goto L_0x0254
        L_0x0273:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.util.k.a(android.text.Spannable, int, com.helpshift.util.k$a):boolean");
    }

    public static boolean a(TextView textView, int i, a aVar) {
        CharSequence text = textView.getText();
        if (!(text instanceof Spannable)) {
            SpannableString valueOf = SpannableString.valueOf(text);
            if (!a(valueOf, 14, aVar)) {
                return false;
            }
            MovementMethod movementMethod = textView.getMovementMethod();
            if ((movementMethod == null || !(movementMethod instanceof LinkMovementMethod)) && textView.getLinksClickable()) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
            }
            textView.setText(valueOf);
            return true;
        } else if (!a((Spannable) text, 14, aVar)) {
            return false;
        } else {
            MovementMethod movementMethod2 = textView.getMovementMethod();
            if ((movementMethod2 == null || !(movementMethod2 instanceof LinkMovementMethod)) && textView.getLinksClickable()) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
            }
            return true;
        }
    }

    public static void a(TextView textView, Pattern pattern, String str, b bVar, c cVar, a aVar) {
        boolean z;
        TextView textView2 = textView;
        SpannableString valueOf = SpannableString.valueOf(textView.getText());
        Matcher matcher = pattern.matcher(valueOf);
        boolean z2 = false;
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            String group = matcher.group(0);
            String[] strArr = {""};
            int i = 0;
            while (true) {
                if (i >= 1) {
                    z = false;
                    break;
                }
                if (group.regionMatches(true, 0, strArr[i], 0, strArr[i].length())) {
                    if (!group.regionMatches(false, 0, strArr[i], 0, strArr[i].length())) {
                        group = strArr[i] + group.substring(strArr[i].length());
                    }
                    z = true;
                } else {
                    i++;
                }
            }
            if (!z) {
                group = strArr[0] + group;
            }
            valueOf.setSpan(new HSLinkify$4(group, aVar, group), start, end, 33);
            z2 = true;
        }
        if (z2) {
            textView2.setText(valueOf);
            MovementMethod movementMethod = textView.getMovementMethod();
            if ((movementMethod == null || !(movementMethod instanceof LinkMovementMethod)) && textView.getLinksClickable()) {
                textView2.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }
}
