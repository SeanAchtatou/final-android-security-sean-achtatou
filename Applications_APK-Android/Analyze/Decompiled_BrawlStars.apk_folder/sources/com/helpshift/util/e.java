package com.helpshift.util;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

/* compiled from: DatabaseUtils */
public class e {
    public static boolean a(SQLiteDatabase sQLiteDatabase, String str, String str2, String[] strArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(*) FROM ");
        sb.append(str);
        sb.append(" WHERE ");
        sb.append(str2);
        sb.append(" LIMIT 1");
        return DatabaseUtils.longForQuery(sQLiteDatabase, sb.toString(), strArr) > 0;
    }

    public static String a(int i) {
        if (i < 1) {
            return null;
        }
        StringBuilder sb = new StringBuilder((i * 2) - 1);
        sb.append("?");
        for (int i2 = 1; i2 < i; i2++) {
            sb.append(",?");
        }
        return sb.toString();
    }

    public static <T> List<List<T>> a(int i, List list) {
        ArrayList arrayList = new ArrayList();
        int i2 = 900;
        if (900 > list.size()) {
            arrayList.add(list);
        } else {
            int i3 = 0;
            while (i2 <= list.size() && i3 <= i2) {
                List subList = list.subList(i3, i2);
                i3 += 900;
                i2 = Math.min(subList.size() + i2, list.size());
                if (subList.size() > 0) {
                    arrayList.add(subList);
                }
            }
        }
        return arrayList;
    }

    public static <T> T a(Cursor cursor, String str, Class cls) {
        T cast;
        try {
            int columnIndex = cursor.getColumnIndex(str);
            if (cls == Long.class) {
                if (cursor.isNull(columnIndex)) {
                    return null;
                }
                cast = cls.cast(Long.valueOf(cursor.getLong(columnIndex)));
            } else if (cls == Integer.class) {
                if (cursor.isNull(columnIndex)) {
                    return null;
                }
                cast = cls.cast(Integer.valueOf(cursor.getInt(columnIndex)));
            } else if (cls != String.class) {
                return null;
            } else {
                cast = cls.cast(cursor.getString(cursor.getColumnIndex(str)));
            }
            return cast;
        } catch (Exception e) {
            n.c("DatabaseUtils", "Error in parse long column : " + str, e);
            return null;
        }
    }

    public static boolean a(Cursor cursor, String str, boolean z) {
        Boolean a2 = a(cursor, str);
        if (a2 == null) {
            return false;
        }
        return a2.booleanValue();
    }

    public static Boolean a(Cursor cursor, String str) {
        Integer num = (Integer) a(cursor, str, Integer.class);
        if (num == null) {
            return null;
        }
        boolean z = true;
        if (num.intValue() != 1) {
            z = false;
        }
        return Boolean.valueOf(z);
    }
}
