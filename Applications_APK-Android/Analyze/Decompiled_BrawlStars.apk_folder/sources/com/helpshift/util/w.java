package com.helpshift.util;

/* compiled from: SchemaUtil */
public class w {
    private static boolean a(String str, String str2) {
        return !y.a(str) && o.c(str2).matcher(str).matches();
    }

    public static boolean a(String str) {
        return !y.a(str) && a(str, "platform");
    }

    public static boolean b(String str) {
        if (y.a(str)) {
            return false;
        }
        return o.a().matcher(str).matches();
    }
}
