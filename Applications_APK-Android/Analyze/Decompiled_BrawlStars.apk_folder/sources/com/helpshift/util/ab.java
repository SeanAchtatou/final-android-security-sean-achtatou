package com.helpshift.util;

/* compiled from: VersionName */
public class ab implements Comparable<ab> {

    /* renamed from: a  reason: collision with root package name */
    public final int[] f4244a;

    public ab(String str) {
        String[] split = str.split("-")[0].split("\\.");
        this.f4244a = new int[split.length];
        for (int i = 0; i < split.length; i++) {
            this.f4244a[i] = Integer.valueOf(split[i]).intValue();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public int compareTo(ab abVar) {
        int max = Math.max(this.f4244a.length, abVar.f4244a.length);
        int i = 0;
        while (i < max) {
            int[] iArr = this.f4244a;
            int i2 = i < iArr.length ? iArr[i] : 0;
            int[] iArr2 = abVar.f4244a;
            int i3 = i < iArr2.length ? iArr2[i] : 0;
            if (i2 != i3) {
                return i2 < i3 ? -1 : 1;
            }
            i++;
        }
        return 0;
    }

    public final boolean a(ab abVar) {
        int c = compareTo(abVar);
        return c == 1 || c == 0;
    }

    public final boolean b(ab abVar) {
        int c = compareTo(abVar);
        return c == -1 || c == 0;
    }
}
