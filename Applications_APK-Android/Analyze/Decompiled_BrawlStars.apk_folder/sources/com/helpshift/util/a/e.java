package com.helpshift.util.a;

/* compiled from: NotifyingRunnable */
public class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final Object f4240a = new Object();

    /* renamed from: b  reason: collision with root package name */
    boolean f4241b;
    private final Runnable c;

    e(Runnable runnable) {
        this.c = runnable;
    }

    public void run() {
        synchronized (this.f4240a) {
            try {
                this.c.run();
                this.f4241b = true;
                this.f4240a.notifyAll();
            } catch (Throwable th) {
                this.f4241b = true;
                this.f4240a.notifyAll();
                throw th;
            }
        }
    }
}
