package com.helpshift.util.a;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.helpshift.util.n;

/* compiled from: HandlerThreadExecutor */
public class c implements a {

    /* renamed from: a  reason: collision with root package name */
    Handler f4236a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f4237b = new Object();
    private Handler c;

    public c(String str) {
        HandlerThread handlerThread = new HandlerThread(str);
        handlerThread.start();
        this.c = new Handler(handlerThread.getLooper());
        this.f4236a = new Handler(Looper.getMainLooper());
    }

    public final void a(Runnable runnable) {
        this.c.post(runnable);
    }

    public final void b(Runnable runnable) {
        e eVar = new e(runnable);
        synchronized (this.f4237b) {
            this.c.post(eVar);
            synchronized (eVar.f4240a) {
                try {
                    if (!eVar.f4241b) {
                        eVar.f4240a.wait();
                    }
                } catch (InterruptedException e) {
                    n.a("Helpshift_NotiRunnable", "Exception in NotifyingRunnable", e);
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public final void c(Runnable runnable) {
        a(new d(this, runnable));
    }
}
