package com.helpshift.util;

/* compiled from: TimeUtil */
public class z {
    public static long a(Float f) {
        long currentTimeMillis = System.currentTimeMillis();
        return (f == null || f.floatValue() == 0.0f) ? currentTimeMillis : (long) (((float) currentTimeMillis) + (f.floatValue() * 1000.0f));
    }
}
