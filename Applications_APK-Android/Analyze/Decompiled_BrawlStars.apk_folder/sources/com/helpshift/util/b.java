package com.helpshift.util;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import java.util.Locale;

/* compiled from: ApplicationUtil */
public final class b {
    public static String a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            n.a("Helpshift_AppUtil", "Error getting app version", e);
            return null;
        }
    }

    public static int b(Context context) {
        try {
            return context.getApplicationInfo().targetSdkVersion;
        } catch (Exception e) {
            n.a("Helpshift_AppUtil", "Target SDK version not found", e);
            return 0;
        }
    }

    public static String c(Context context) {
        String str;
        try {
            str = context.getPackageManager().getApplicationLabel(context.getApplicationInfo()).toString();
        } catch (Exception e) {
            n.a("Helpshift_AppUtil", "Error getting application name", e);
            str = null;
        }
        return str == null ? "Support" : str;
    }

    public static boolean a(Context context, String str) {
        try {
            return ContextCompat.checkSelfPermission(context, str) == 0;
        } catch (Exception e) {
            n.a("Helpshift_AppUtil", "Error checking for permission : " + str, e);
            return false;
        }
    }

    public static NotificationManager d(Context context) {
        try {
            return (NotificationManager) context.getSystemService("notification");
        } catch (Exception e) {
            n.c("Helpshift_AppUtil", "Unable to get notification manager from System service", e);
            return null;
        }
    }

    public static Intent b(Context context, String str) {
        try {
            return context.getPackageManager().getLaunchIntentForPackage(str);
        } catch (Exception e) {
            n.a("Helpshift_AppUtil", "Error getting launch activity for package : " + str, e);
            return null;
        }
    }

    public static Context e(Context context) {
        Locale d;
        if (Build.VERSION.SDK_INT < 17 || (d = q.c().w().d()) == null) {
            return context;
        }
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(d);
        return context.createConfigurationContext(configuration);
    }

    public static Context f(Context context) {
        Locale d = q.c().w().d();
        if (d != null) {
            q.c().w().a();
            Resources resources = context.getResources();
            Configuration configuration = new Configuration(resources.getConfiguration());
            configuration.locale = d;
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }
        return context;
    }

    public static void a() {
        q.c().w().b();
    }

    private static String h(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo.metaData != null) {
                return applicationInfo.metaData.getString("android.support.VERSION");
            }
            return null;
        } catch (Exception e) {
            n.a("Helpshift_AppUtil", "Error getting SupportLib version : ", e);
            return null;
        }
    }

    public static boolean a(Context context, int i) {
        try {
            String h = h(context);
            if (h == null || Integer.parseInt(h.split("\\.")[0]) < 26) {
                return false;
            }
            return true;
        } catch (Exception e) {
            n.a("Helpshift_AppUtil", "Error in doing comparison check for supportLib version : ", e);
        }
        return false;
    }

    public static boolean g(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    public static int a(Context context, String str, String str2, String str3) {
        return context.getResources().getIdentifier(str, str2, str3);
    }
}
