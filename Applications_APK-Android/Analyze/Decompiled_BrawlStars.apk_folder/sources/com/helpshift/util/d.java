package com.helpshift.util;

/* compiled from: ByteArrayUtil */
public class d {
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.lang.Object r3) throws java.io.IOException {
        /*
            r0 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0021 }
            r1.<init>()     // Catch:{ all -> 0x0021 }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ all -> 0x001f }
            r2.<init>(r1)     // Catch:{ all -> 0x001f }
            r2.writeObject(r3)     // Catch:{ all -> 0x001c }
            r2.flush()     // Catch:{ all -> 0x001c }
            byte[] r3 = r1.toByteArray()     // Catch:{ all -> 0x001c }
            r2.close()
            r1.close()
            return r3
        L_0x001c:
            r3 = move-exception
            r0 = r2
            goto L_0x0023
        L_0x001f:
            r3 = move-exception
            goto L_0x0023
        L_0x0021:
            r3 = move-exception
            r1 = r0
        L_0x0023:
            if (r0 == 0) goto L_0x0028
            r0.close()
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.util.d.a(java.lang.Object):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object a(byte[] r3) throws java.io.IOException, java.lang.ClassNotFoundException {
        /*
            r0 = 0
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x001d }
            r1.<init>(r3)     // Catch:{ all -> 0x001d }
            java.io.ObjectInputStream r3 = new java.io.ObjectInputStream     // Catch:{ all -> 0x001b }
            r3.<init>(r1)     // Catch:{ all -> 0x001b }
            java.lang.Object r0 = r3.readObject()     // Catch:{ all -> 0x0016 }
            r1.close()
            r3.close()
            return r0
        L_0x0016:
            r0 = move-exception
            r2 = r0
            r0 = r3
            r3 = r2
            goto L_0x001f
        L_0x001b:
            r3 = move-exception
            goto L_0x001f
        L_0x001d:
            r3 = move-exception
            r1 = r0
        L_0x001f:
            if (r1 == 0) goto L_0x0024
            r1.close()
        L_0x0024:
            if (r0 == 0) goto L_0x0029
            r0.close()
        L_0x0029:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.util.d.a(byte[]):java.lang.Object");
    }
}
