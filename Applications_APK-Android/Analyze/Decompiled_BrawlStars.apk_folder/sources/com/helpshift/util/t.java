package com.helpshift.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import java.io.File;

/* compiled from: IntentUtil */
public class t {
    public static Intent a(Context context, File file, String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        String packageName = context.getApplicationContext().getPackageName();
        Uri uriForFile = FileProvider.getUriForFile(context, packageName + ".helpshift.fileprovider", file);
        intent.setFlags(1);
        intent.setDataAndType(uriForFile, str);
        return intent;
    }
}
