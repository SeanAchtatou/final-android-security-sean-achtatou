package com.helpshift.util;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/* compiled from: AssetsUtil */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4245a = c.class.getSimpleName();

    public static String a(Context context, String str) {
        BufferedReader bufferedReader;
        InputStream inputStream;
        try {
            inputStream = context.getAssets().open(str);
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                try {
                    StringBuilder sb = new StringBuilder();
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine != null) {
                            sb.append(readLine);
                        } else {
                            String sb2 = sb.toString();
                            r.a(inputStream);
                            r.a(bufferedReader);
                            return sb2;
                        }
                    }
                } catch (IOException e) {
                    e = e;
                    try {
                        n.b(f4245a, "Error reading the file : " + e.getMessage());
                        r.a(inputStream);
                        r.a(bufferedReader);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        r.a(inputStream);
                        r.a(bufferedReader);
                        throw th;
                    }
                }
            } catch (IOException e2) {
                e = e2;
                bufferedReader = null;
                n.b(f4245a, "Error reading the file : " + e.getMessage());
                r.a(inputStream);
                r.a(bufferedReader);
                return null;
            } catch (Throwable th2) {
                th = th2;
                bufferedReader = null;
                r.a(inputStream);
                r.a(bufferedReader);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            inputStream = null;
            bufferedReader = null;
            n.b(f4245a, "Error reading the file : " + e.getMessage());
            r.a(inputStream);
            r.a(bufferedReader);
            return null;
        } catch (Throwable th3) {
            bufferedReader = null;
            th = th3;
            inputStream = null;
            r.a(inputStream);
            r.a(bufferedReader);
            throw th;
        }
    }

    public static Uri a() {
        Integer b2 = q.c().r().b("notificationSoundId");
        if (b2 == null) {
            return null;
        }
        return Uri.parse("android.resource://" + q.a().getPackageName() + "/" + b2);
    }

    public static boolean a(Context context, Integer num) {
        if (context == null || num == null) {
            return false;
        }
        try {
            return context.getResources().getResourceName(num.intValue()) != null;
        } catch (Resources.NotFoundException unused) {
            return false;
        }
    }
}
