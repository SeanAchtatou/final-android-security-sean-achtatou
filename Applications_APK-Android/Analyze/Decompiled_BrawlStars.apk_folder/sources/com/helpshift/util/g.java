package com.helpshift.util;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/* compiled from: FileUtil */
public class g {

    /* renamed from: a  reason: collision with root package name */
    public static final String f4247a = g.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static final Set<String> f4248b = new HashSet(Arrays.asList("image/jpeg", "image/png", "image/gif", "image/x-png", "image/x-citrix-pjpeg", "image/x-citrix-gif", "image/pjpeg"));

    public static boolean a(String str) {
        return f4248b.contains(str);
    }

    public static void a(URL url, File file) {
        InputStream inputStream;
        FileOutputStream fileOutputStream;
        InputStream inputStream2 = null;
        try {
            inputStream = url.openStream();
            try {
                fileOutputStream = new FileOutputStream(file);
            } catch (Exception e) {
                e = e;
                fileOutputStream = null;
                inputStream2 = inputStream;
                try {
                    n.a(f4247a, "saveFile Exception :", e);
                    r.a(inputStream2);
                    r.a(fileOutputStream);
                } catch (Throwable th) {
                    th = th;
                    inputStream = inputStream2;
                    r.a(inputStream);
                    r.a(fileOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = null;
                r.a(inputStream);
                r.a(fileOutputStream);
                throw th;
            }
            try {
                byte[] bArr = new byte[500];
                while (true) {
                    int read = inputStream.read(bArr, 0, 500);
                    if (read < 0) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                r.a(inputStream);
            } catch (Exception e2) {
                e = e2;
                inputStream2 = inputStream;
                n.a(f4247a, "saveFile Exception :", e);
                r.a(inputStream2);
                r.a(fileOutputStream);
            } catch (Throwable th3) {
                th = th3;
                r.a(inputStream);
                r.a(fileOutputStream);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            fileOutputStream = null;
            n.a(f4247a, "saveFile Exception :", e);
            r.a(inputStream2);
            r.a(fileOutputStream);
        } catch (Throwable th4) {
            th = th4;
            inputStream = null;
            fileOutputStream = null;
            r.a(inputStream);
            r.a(fileOutputStream);
            throw th;
        }
        r.a(fileOutputStream);
    }

    public static String a(URL url) {
        try {
            return url.openConnection().getContentType();
        } catch (Exception e) {
            n.a(f4247a, "openConnection() Exception :", e);
            return null;
        }
    }

    public static String b(String str) {
        try {
            return a(new URL("file://" + str));
        } catch (MalformedURLException e) {
            n.a(f4247a, "error in getting mimeType :", e);
            return null;
        }
    }

    public static String c(String str) {
        int lastIndexOf = str.lastIndexOf(47);
        int lastIndexOf2 = str.lastIndexOf(46);
        return lastIndexOf < lastIndexOf2 ? str.substring(lastIndexOf2) : "";
    }

    public static boolean a(Uri uri, Context context) {
        boolean z = false;
        try {
            ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
            if (openFileDescriptor != null) {
                z = true;
            }
            if (openFileDescriptor != null) {
                try {
                    openFileDescriptor.close();
                } catch (IOException unused) {
                }
            }
            return z;
        } catch (Exception e) {
            String str = f4247a;
            n.a(str, "Unable to open input file descriptor for doesFileFromUriExistAndCanRead: " + uri, e);
            return false;
        }
    }

    public static String a(Context context, Uri uri) {
        if ("content".equals(uri.getScheme())) {
            return MimeTypeMap.getSingleton().getExtensionFromMimeType(context.getContentResolver().getType(uri));
        }
        return MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
    }
}
