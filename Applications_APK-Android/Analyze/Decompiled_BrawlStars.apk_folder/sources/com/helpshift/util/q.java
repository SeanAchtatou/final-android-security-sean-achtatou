package com.helpshift.util;

import android.content.Context;
import com.helpshift.b;
import com.helpshift.common.e.ab;
import com.helpshift.common.e.o;
import com.helpshift.exceptions.HelpshiftInitializationException;
import com.helpshift.m;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: HelpshiftContext */
public class q {

    /* renamed from: a  reason: collision with root package name */
    public static AtomicBoolean f4255a = new AtomicBoolean(false);

    /* renamed from: b  reason: collision with root package name */
    public static AtomicBoolean f4256b = new AtomicBoolean(false);
    private static final Object c = new Object();
    private static Context d;
    private static b e;
    private static ab f;

    public static Context a() {
        return d;
    }

    public static void a(Context context) {
        synchronized (c) {
            if (d == null) {
                d = context;
            }
        }
    }

    public static void a(String str, String str2, String str3) {
        if (f == null) {
            f = new o(d, str, str2, str3);
        }
        if (e == null) {
            e = new m(f);
        }
    }

    public static ab b() {
        return f;
    }

    public static b c() {
        return e;
    }

    public static boolean d() {
        if (f4256b.get()) {
            return true;
        }
        Context context = d;
        if (context == null || context.getApplicationInfo() == null || !b.g(d)) {
            return false;
        }
        throw new HelpshiftInitializationException("com.helpshift.Core.install() method not called with valid arguments");
    }
}
