package com.helpshift.util;

import com.helpshift.p.b.a;
import com.helpshift.p.c;
import java.util.List;

/* compiled from: HSLogger */
public class n {

    /* renamed from: a  reason: collision with root package name */
    private static c f4252a;

    public static void a(c cVar, int i) {
        f4252a = cVar;
        f4252a.a(i);
    }

    public static void a(float f) {
        f4252a.a(((long) f) * 1000);
    }

    public static void a(boolean z, boolean z2) {
        c cVar = f4252a;
        if (cVar != null) {
            cVar.a(z, z2);
        }
    }

    public static void a(String str, String str2) {
        a(str, str2, (Throwable) null, (a[]) null);
    }

    public static void b(String str, String str2) {
        a(str, str2, (Throwable[]) null, (a[]) null);
    }

    public static void a(String str, String str2, Throwable th) {
        a(str, str2, th, (a[]) null);
    }

    public static void c(String str, String str2, Throwable th) {
        a(str, str2, new Throwable[]{th}, (a[]) null);
    }

    public static void a(String str, String str2, a... aVarArr) {
        a(str, str2, (Throwable) null, aVarArr);
    }

    public static void a(String str, String str2, Throwable th, a... aVarArr) {
        a(2, str, str2, new Throwable[]{th}, aVarArr);
    }

    public static void b(String str, String str2, Throwable th, a... aVarArr) {
        a(8, str, str2, new Throwable[]{th}, aVarArr);
    }

    public static void a(String str, String str2, Throwable[] thArr, a... aVarArr) {
        a(8, str, str2, thArr, aVarArr);
    }

    public static void c(String str, String str2, Throwable th, a... aVarArr) {
        a(16, str, str2, new Throwable[]{th}, aVarArr);
    }

    public static void b(String str, String str2, Throwable[] thArr, a... aVarArr) {
        a(16, str, str2, thArr, aVarArr);
    }

    private static void a(int i, String str, String str2, Throwable[] thArr, a... aVarArr) {
        c cVar = f4252a;
        if (cVar != null) {
            if (i == 2) {
                cVar.a(str, str2, thArr, aVarArr);
            } else if (i == 4) {
                cVar.b(str, str2, thArr, aVarArr);
            } else if (i == 8) {
                cVar.c(str, str2, thArr, aVarArr);
            } else if (i == 16) {
                cVar.d(str, str2, thArr, aVarArr);
            }
        }
    }

    public static List<com.helpshift.p.c.a> a() {
        c cVar = f4252a;
        if (cVar == null) {
            return null;
        }
        return cVar.a();
    }

    public static void b() {
        c cVar = f4252a;
        if (cVar != null) {
            cVar.b();
        }
    }

    public static int c() {
        c cVar = f4252a;
        if (cVar == null) {
            return 0;
        }
        return cVar.b(16);
    }

    public static void b(String str, String str2, Throwable th) {
        a(4, str, str2, new Throwable[]{th}, null);
    }
}
