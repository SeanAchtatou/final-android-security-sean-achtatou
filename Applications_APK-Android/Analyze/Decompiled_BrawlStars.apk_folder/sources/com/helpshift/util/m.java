package com.helpshift.util;

import java.util.Comparator;

/* compiled from: HSLinkify */
final class m implements Comparator<u> {
    public final boolean equals(Object obj) {
        return false;
    }

    m() {
    }

    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        u uVar = (u) obj;
        u uVar2 = (u) obj2;
        if (uVar.f4260b >= uVar2.f4260b) {
            if (uVar.f4260b > uVar2.f4260b || uVar.c < uVar2.c) {
                return 1;
            }
            if (uVar.c > uVar2.c) {
                return -1;
            }
            return 0;
        }
        return -1;
    }
}
