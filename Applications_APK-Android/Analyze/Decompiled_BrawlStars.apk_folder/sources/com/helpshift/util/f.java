package com.helpshift.util;

import android.content.Context;
import com.facebook.internal.AnalyticsEvents;
import com.helpshift.p.b.a;
import com.helpshift.p.b.d;
import com.helpshift.v.c;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ErrorReportProvider */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private static String f4246a = "HS_ErrorReport";

    public static List<a> a(Context context, Thread thread) {
        ArrayList arrayList = new ArrayList();
        try {
            arrayList.add(d.a("appId", context.getPackageName()));
            arrayList.add(d.a("nt", p.b(context)));
            c cVar = com.helpshift.v.a.f4262b;
            String str = "";
            String b2 = cVar == null ? str : cVar.b();
            if (b2 != null) {
                arrayList.add(d.a("funnel", b2));
            }
            if (cVar != null) {
                str = cVar.a();
            }
            if (!y.a(str)) {
                arrayList.add(d.a("actconvid", str));
            }
            String str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            if (thread != null) {
                str2 = thread.toString();
            }
            arrayList.add(d.a("thread", str2));
        } catch (Exception e) {
            n.c(f4246a, "Error creating error report", e);
        }
        return arrayList;
    }
}
