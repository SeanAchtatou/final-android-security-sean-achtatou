package com.helpshift.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ImageUtil */
public class s {

    /* renamed from: a  reason: collision with root package name */
    private static final Set<String> f4257a = new HashSet(Arrays.asList("image/jpeg", "image/png", "image/x-png", "image/x-citrix-pjpeg", "image/pjpeg"));

    /* renamed from: b  reason: collision with root package name */
    private static Set<String> f4258b = new HashSet(Arrays.asList("image/jpeg", "image/png", "image/bmp"));

    public static int a(int i, int i2, int i3) {
        return (int) ((((float) i2) / ((float) i)) * ((float) i3));
    }

    public static boolean a(String str) {
        return f4257a.contains(g.b(str));
    }

    public static boolean a(Uri uri, Context context) {
        return f4257a.contains(context.getContentResolver().getType(uri));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    public static void a(String str, int i) {
        Bitmap.CompressFormat compressFormat;
        if (!TextUtils.isEmpty(str)) {
            File file = new File(str);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            int i2 = options.outHeight;
            int i3 = options.outWidth;
            float max = 1024.0f / ((float) Math.max(i2, i3));
            if (max < 1.0f) {
                options.inSampleSize = a(options, (int) (((float) i3) * max), (int) (((float) i2) * max));
                options.inJustDecodeBounds = false;
                Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
                if (decodeFile != null) {
                    int width = decodeFile.getWidth();
                    int height = decodeFile.getHeight();
                    float max2 = ((float) 1024) / ((float) Math.max(height, width));
                    if (max2 < 1.0f) {
                        decodeFile = Bitmap.createScaledBitmap(decodeFile, (int) (((float) width) * max2), (int) (((float) height) * max2), true);
                    }
                    String b2 = g.b(str);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    FileOutputStream fileOutputStream = null;
                    if (b2.contains("png")) {
                        compressFormat = Bitmap.CompressFormat.PNG;
                    } else {
                        compressFormat = Bitmap.CompressFormat.JPEG;
                    }
                    if (decodeFile.compress(compressFormat, 70, byteArrayOutputStream)) {
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        try {
                            FileOutputStream fileOutputStream2 = new FileOutputStream(str, false);
                            try {
                                fileOutputStream2.write(byteArray);
                                fileOutputStream2.flush();
                                r.a(byteArrayOutputStream);
                                r.a(fileOutputStream2);
                            } catch (IOException e) {
                                e = e;
                                fileOutputStream = fileOutputStream2;
                                try {
                                    n.a("Helpshift_ImageUtil", "saveBitmapToFile : ", e);
                                    r.a(byteArrayOutputStream);
                                    r.a(fileOutputStream);
                                } catch (Throwable th) {
                                    th = th;
                                    r.a(byteArrayOutputStream);
                                    r.a(fileOutputStream);
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                fileOutputStream = fileOutputStream2;
                                r.a(byteArrayOutputStream);
                                r.a(fileOutputStream);
                                throw th;
                            }
                        } catch (IOException e2) {
                            e = e2;
                            n.a("Helpshift_ImageUtil", "saveBitmapToFile : ", e);
                            r.a(byteArrayOutputStream);
                            r.a(fileOutputStream);
                        }
                    } else {
                        n.b("Helpshift_ImageUtil", "saveBitmapToFile : Compression Failed");
                    }
                }
            }
        }
    }

    public static int a(int i, int i2, int i3, int i4) {
        int i5 = 1;
        if (i2 > i4 || i > i3) {
            int i6 = i2 / 2;
            int i7 = i / 2;
            while (i6 / i5 > i4 && i7 / i5 > i3) {
                i5 *= 2;
            }
        }
        return i5;
    }

    public static int a(BitmapFactory.Options options, int i, int i2) {
        return a(options.outWidth, options.outHeight, i, i2);
    }
}
