package com.helpshift.util;

import android.text.style.URLSpan;
import android.view.View;
import com.helpshift.util.k;

final class HSLinkify$4 extends URLSpan {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k.a f4233a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f4234b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    HSLinkify$4(String str, k.a aVar, String str2) {
        super(str);
        this.f4233a = aVar;
        this.f4234b = str2;
    }

    public final void onClick(View view) {
        super.onClick(view);
        k.a aVar = this.f4233a;
        if (aVar != null) {
            aVar.a(this.f4234b);
        }
    }
}
