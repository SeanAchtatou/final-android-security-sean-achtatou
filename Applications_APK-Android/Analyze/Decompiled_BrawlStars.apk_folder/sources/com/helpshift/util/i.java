package com.helpshift.util;

import com.helpshift.common.g.e;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/* compiled from: HSFormat */
public final class i {

    /* renamed from: a  reason: collision with root package name */
    public static final e f4249a = new e("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());

    /* renamed from: b  reason: collision with root package name */
    public static final e f4250b = new e("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault(), "UTC");
    public static final e c = new e("yyyyMMddHHmmssSSS", Locale.getDefault());
    public static final DecimalFormat d = new DecimalFormat("0.000", new DecimalFormatSymbols(Locale.US));
    public static final e e = new e("dd/MM/yyyy HH:mm:ss.SSS ", Locale.getDefault());
}
