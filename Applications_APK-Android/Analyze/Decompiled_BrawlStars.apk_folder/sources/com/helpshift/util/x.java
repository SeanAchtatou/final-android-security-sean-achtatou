package com.helpshift.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.view.View;
import com.helpshift.R;

/* compiled from: Styles */
public class x {
    public static int a(Context context, int i) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{i});
        int color = obtainStyledAttributes.getColor(0, -1);
        obtainStyledAttributes.recycle();
        return color;
    }

    public static String b(Context context, int i) {
        return a(a(context, i));
    }

    private static String a(int i) {
        return String.format("#%06X", Integer.valueOf(i & ViewCompat.MEASURED_SIZE_MASK));
    }

    public static void a(Context context, Drawable drawable) {
        a(context, drawable, R.attr.hs__actionButtonIconColor);
    }

    public static void a(Context context, Drawable drawable, int i) {
        if (drawable != null) {
            drawable.setColorFilter(a(context, i), PorterDuff.Mode.SRC_ATOP);
        }
    }

    public static float a(Context context, float f) {
        return f * context.getResources().getDisplayMetrics().density;
    }

    public static void a(Context context, View view, int i, int i2) {
        Drawable drawable = ContextCompat.getDrawable(context, i);
        a(context, drawable, i2);
        if (Build.VERSION.SDK_INT >= 16) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    public static void a(View view, int i, int i2, GradientDrawable.Orientation orientation) {
        ViewCompat.setBackground(view, new GradientDrawable(orientation, new int[]{i, 0}));
    }
}
