package com.helpshift.util;

import java.util.regex.Pattern;

/* compiled from: HSPattern */
public final class o {

    /* renamed from: a  reason: collision with root package name */
    public static Pattern f4253a;

    /* renamed from: b  reason: collision with root package name */
    private static Pattern f4254b;
    private static Pattern c;
    private static Pattern d;
    private static Pattern e;

    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        return c().matcher(str.trim()).matches();
    }

    public static boolean b(String str) {
        if (f4254b == null) {
            f4254b = Pattern.compile("\\W+");
        }
        return f4254b.matcher(str.trim()).matches();
    }

    public static Pattern c(String str) {
        return Pattern.compile("^[\\p{L}\\p{N}-]+_" + str + "_\\d{17}-[0-9a-z]{15}$");
    }

    public static Pattern a() {
        if (d == null) {
            d = Pattern.compile("^[\\p{L}\\p{N}][\\p{L}\\p{N}-]*[\\p{L}\\p{N}]\\.helpshift\\.(com|mobi)$");
        }
        return d;
    }

    public static Pattern b() {
        if (e == null) {
            e = Pattern.compile("[^\\p{Z}\\n\\p{Ps}]+://[^\\p{Z}\\n\\p{Pe}.]+(\\.[^\\p{Z}\\n\\p{Pe}.]+)*");
        }
        return e;
    }

    public static Pattern c() {
        if (c == null) {
            c = Pattern.compile("(?i)^[\\p{L}\\p{N}\\p{M}\\p{S}\\p{Po}A-Z0-9._%'-]{1,64}(\\+.*)?@[\\p{L}\\p{M}\\p{N}\\p{S}A-Z0-9'.-]{1,246}\\.[\\p{L}\\p{M}\\p{N}\\p{S}A-Z]{2,8}[^\\s]*$");
        }
        return c;
    }
}
