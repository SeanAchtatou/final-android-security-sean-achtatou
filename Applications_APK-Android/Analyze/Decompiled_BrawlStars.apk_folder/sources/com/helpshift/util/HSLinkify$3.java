package com.helpshift.util;

import android.text.style.URLSpan;
import android.view.View;
import com.helpshift.util.k;

final class HSLinkify$3 extends URLSpan {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k.a f4231a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ u f4232b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    HSLinkify$3(String str, k.a aVar, u uVar) {
        super(str);
        this.f4231a = aVar;
        this.f4232b = uVar;
    }

    public final void onClick(View view) {
        super.onClick(view);
        k.a aVar = this.f4231a;
        if (aVar != null) {
            aVar.a(this.f4232b.f4259a);
        }
    }
}
