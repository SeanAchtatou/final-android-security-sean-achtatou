package com.helpshift.util;

import java.util.ArrayList;
import java.util.List;

/* compiled from: Filters */
public class h {
    public static <T> List<T> a(List<T> list, v<T> vVar) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (T next : list) {
            if (vVar.a(next)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
