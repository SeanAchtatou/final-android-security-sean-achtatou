package com.helpshift.k;

import com.helpshift.util.n;
import java.security.GeneralSecurityException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: CryptoDM */
public class a {
    public final String a(String str, String str2) throws GeneralSecurityException {
        String a2 = a(str, str2, 0);
        if (a2 != null) {
            return a2;
        }
        throw new GeneralSecurityException();
    }

    private String a(String str, String str2, int i) {
        while (i <= 1) {
            try {
                SecretKeySpec secretKeySpec = new SecretKeySpec(str2.getBytes("UTF-8"), "HmacSHA256");
                Mac instance = Mac.getInstance("HmacSHA256");
                instance.init(secretKeySpec);
                return a(instance.doFinal(str.getBytes("UTF-8")));
            } catch (Exception e) {
                if (i == 1) {
                    n.c("Helpshift_CryptoDM", "Could not generate mac signature: " + e.getLocalizedMessage() + ", retryCount: " + i, e, new com.helpshift.p.b.a[0]);
                } else {
                    n.c("Helpshift_CryptoDM", "Could not generate mac signature: " + e.getLocalizedMessage() + ", retryCount: " + i, e);
                }
                i++;
            }
        }
        return null;
    }

    private static String a(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        char[] cArr2 = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            byte b2 = bArr[i] & 255;
            int i2 = i * 2;
            cArr2[i2] = cArr[b2 >>> 4];
            cArr2[i2 + 1] = cArr[b2 & 15];
        }
        return new String(cArr2);
    }
}
