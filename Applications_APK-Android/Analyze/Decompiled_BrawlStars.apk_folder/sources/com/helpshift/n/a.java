package com.helpshift.n;

import java.util.List;

/* compiled from: FaqCore */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    public final String f3761a;

    /* renamed from: b  reason: collision with root package name */
    public final String f3762b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final int g = 0;
    public final Boolean h;
    public final List<String> i;
    public final List<String> j;

    public a(String str, String str2, String str3, String str4, String str5, String str6, int i2, Boolean bool, List<String> list, List<String> list2) {
        this.f3761a = str;
        this.f3762b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.h = bool;
        this.i = list;
        this.j = list2;
    }
}
