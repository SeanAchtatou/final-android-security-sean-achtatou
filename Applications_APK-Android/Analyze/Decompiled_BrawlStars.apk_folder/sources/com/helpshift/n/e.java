package com.helpshift.n;

import com.helpshift.common.c.b.m;
import com.helpshift.common.c.l;
import com.helpshift.common.e.a.i;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.h;
import com.helpshift.common.k;
import java.util.HashMap;
import java.util.Locale;

/* compiled from: FaqsDM */
class e extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f3769a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f3770b;

    e(b bVar, h hVar) {
        this.f3770b = bVar;
        this.f3769a = hVar;
    }

    public final void a() {
        try {
            m a2 = this.f3770b.a("/faqs/");
            HashMap hashMap = new HashMap();
            hashMap.put("edfl", String.valueOf(this.f3770b.f3763a.f3285b.a("defaultFallbackLanguageEnable")));
            i iVar = new i(hashMap);
            HashMap hashMap2 = new HashMap();
            String e = this.f3770b.f3763a.f.e();
            String locale = Locale.getDefault().toString();
            if (k.a(e)) {
                e = locale;
            }
            int i = 1;
            hashMap2.put("Accept-Language", String.format(Locale.ENGLISH, "%s;q=1.0", e));
            iVar.c = hashMap2;
            Object obj = null;
            this.f3770b.a(iVar, (String) null);
            String str = a2.a(iVar).f3323b;
            if (str != null) {
                obj = this.f3770b.f3764b.p().a(str);
            } else {
                i = 2;
            }
            this.f3769a.a(new f(obj, i));
        } catch (RootAPIException e2) {
            this.f3769a.b(e2.c);
        }
    }
}
