package com.helpshift.n;

import com.helpshift.common.a;
import com.helpshift.common.b;
import com.helpshift.common.c.b.d;
import com.helpshift.common.c.b.f;
import com.helpshift.common.c.b.g;
import com.helpshift.common.c.b.m;
import com.helpshift.common.c.b.q;
import com.helpshift.common.c.b.s;
import com.helpshift.common.c.j;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.h;
import com.helpshift.common.k;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* compiled from: FaqsDM */
public class b implements a {

    /* renamed from: a  reason: collision with root package name */
    final j f3763a;

    /* renamed from: b  reason: collision with root package name */
    final ab f3764b;
    final com.helpshift.n.a.a c;

    public b(j jVar, ab abVar) {
        this.f3763a = jVar;
        this.f3764b = abVar;
        this.c = abVar.u();
        this.f3763a.g.a(b.a.FAQ, this);
    }

    public final void a(String str, boolean z) {
        com.helpshift.b.b bVar;
        this.f3763a.b(new c(this, str, z));
        if (z) {
            bVar = com.helpshift.b.b.MARKED_HELPFUL;
        } else {
            bVar = com.helpshift.b.b.MARKED_UNHELPFUL;
        }
        this.f3763a.c.a(bVar, str);
    }

    public final void a(h<a, Integer> hVar, String str, String str2, boolean z) {
        this.f3763a.b(new d(this, str2, z, str, hVar));
    }

    public final void a(h<f, com.helpshift.common.exception.a> hVar) {
        this.f3763a.b(new e(this, hVar));
    }

    public final void a(b.a aVar) {
        Map<String, Boolean> a2;
        if (aVar == b.a.FAQ && (a2 = this.c.a()) != null) {
            for (String next : a2.keySet()) {
                try {
                    b(next, a2.get(next).booleanValue());
                    this.c.a(next);
                } catch (RootAPIException e) {
                    if (e.c == com.helpshift.common.exception.b.NON_RETRIABLE) {
                        this.c.a(next);
                    } else {
                        throw e;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(String str, boolean z) {
        String str2;
        if (z) {
            str2 = "/faqs/" + str + "/helpful/";
        } else {
            str2 = "/faqs/" + str + "/unhelpful/";
        }
        new com.helpshift.common.c.b.j(new s(new g(new q(str2, this.f3763a, this.f3764b)), this.f3764b)).a(new i(new HashMap()));
    }

    /* access modifiers changed from: package-private */
    public final void a(i iVar, String str) {
        HashMap hashMap = new HashMap();
        if (k.a(str)) {
            str = this.f3763a.f.e();
            String locale = Locale.getDefault().toString();
            if (k.a(str)) {
                str = locale;
            }
        }
        hashMap.put("Accept-Language", String.format(Locale.ENGLISH, "%s;q=1.0", str));
        iVar.c = hashMap;
    }

    /* access modifiers changed from: package-private */
    public final m a(String str) {
        return new f(new com.helpshift.common.c.b.j(new d(new s(new g(new com.helpshift.common.c.b.h(str, this.f3763a, this.f3764b)), this.f3764b))), this.f3764b, str);
    }
}
