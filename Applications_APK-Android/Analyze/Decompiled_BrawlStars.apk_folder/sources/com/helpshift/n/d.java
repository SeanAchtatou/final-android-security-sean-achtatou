package com.helpshift.n;

import com.helpshift.common.c.b.p;
import com.helpshift.common.c.l;
import com.helpshift.common.e.a.i;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.h;
import java.util.HashMap;

/* compiled from: FaqsDM */
class d extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3767a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ boolean f3768b;
    final /* synthetic */ String c;
    final /* synthetic */ h d;
    final /* synthetic */ b f;

    d(b bVar, String str, boolean z, String str2, h hVar) {
        this.f = bVar;
        this.f3767a = str;
        this.f3768b = z;
        this.c = str2;
        this.d = hVar;
    }

    public final void a() {
        boolean z;
        try {
            HashMap hashMap = new HashMap();
            String str = this.f3767a;
            if (this.f3768b) {
                z = true;
            } else {
                z = this.f.f3763a.f3285b.a("defaultFallbackLanguageEnable");
            }
            hashMap.put("edfl", String.valueOf(z));
            i iVar = new i(hashMap);
            this.f.a(iVar, str);
            this.d.a(this.f.f3764b.l().p(this.f.a("/faqs/" + this.c + "/").a(iVar).f3323b));
        } catch (RootAPIException e) {
            if (e.c != b.CONTENT_UNCHANGED) {
                int a2 = e.a();
                if (a2 == p.m.intValue() || a2 == p.n.intValue()) {
                    if (this.f3768b) {
                        this.f.f3764b.g().b(this.c, this.f3767a);
                    }
                    this.f.f3764b.t().a("/faqs/" + this.c + "/", "");
                }
                this.d.b(Integer.valueOf(a2));
            }
        }
    }
}
