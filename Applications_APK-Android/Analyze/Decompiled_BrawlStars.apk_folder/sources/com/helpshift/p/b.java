package com.helpshift.p;

import com.helpshift.a.b.c;
import com.helpshift.common.c.b.e;
import com.helpshift.common.c.b.f;
import com.helpshift.common.c.b.g;
import com.helpshift.common.c.b.j;
import com.helpshift.common.c.b.s;
import com.helpshift.common.c.l;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.h;
import com.helpshift.common.k;
import com.helpshift.util.i;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/* compiled from: ErrorReportsDM */
class b extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f3791a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f3792b;
    final /* synthetic */ String c;
    final /* synthetic */ c d;
    final /* synthetic */ String f;
    final /* synthetic */ String g;
    final /* synthetic */ String h;
    final /* synthetic */ String i;
    final /* synthetic */ h j;
    final /* synthetic */ a k;

    b(a aVar, List list, String str, String str2, c cVar, String str3, String str4, String str5, String str6, h hVar) {
        this.k = aVar;
        this.f3791a = list;
        this.f3792b = str;
        this.c = str2;
        this.d = cVar;
        this.f = str3;
        this.g = str4;
        this.h = str5;
        this.i = str6;
        this.j = hVar;
    }

    public final void a() {
        try {
            Object f2 = this.k.f3788b.p().f(this.f3791a);
            ArrayList arrayList = new ArrayList(5);
            arrayList.add(this.k.f3788b.p().b("domain", this.f3792b).toString());
            arrayList.add(this.k.f3788b.p().b("dm", this.c).toString());
            arrayList.add(this.k.f3788b.p().b("did", this.d.e).toString());
            if (!k.a(this.f)) {
                arrayList.add(this.k.f3788b.p().b("cdid", this.f).toString());
            }
            arrayList.add(this.k.f3788b.p().b("os", this.g).toString());
            Object a2 = this.k.f3788b.p().a((List<String>) arrayList);
            HashMap hashMap = new HashMap();
            hashMap.put("id", UUID.randomUUID().toString());
            hashMap.put("v", this.h);
            hashMap.put("ctime", i.e.a(com.helpshift.common.g.b.a(this.k.f3788b)));
            hashMap.put("src", "sdk.android." + this.i);
            hashMap.put("logs", f2.toString());
            hashMap.put("md", a2.toString());
            this.j.a(new f(new j(new s(new g(new e("/events/crash-log", this.k.f3787a, this.k.f3788b, this.k.a())), this.k.f3788b)), this.k.f3788b, "/faqs").a(new com.helpshift.common.e.a.i(hashMap)));
        } catch (RootAPIException unused) {
            this.j.b(Float.valueOf(this.k.f3788b.t().a()));
        }
    }
}
