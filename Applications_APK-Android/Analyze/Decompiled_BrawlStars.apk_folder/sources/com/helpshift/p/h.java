package com.helpshift.p;

import android.text.TextUtils;
import com.helpshift.p.a.b;
import com.helpshift.p.c.a;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: Logger */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private d f3807a;

    /* renamed from: b  reason: collision with root package name */
    private b f3808b;
    private SimpleDateFormat c;

    h(d dVar, b bVar, SimpleDateFormat simpleDateFormat) {
        this.f3807a = dVar;
        this.f3808b = bVar;
        this.c = simpleDateFormat;
    }

    public void run() {
        try {
            if (this.f3807a != null) {
                String format = this.c.format(new Date(this.f3807a.f3800a));
                if (!TextUtils.isEmpty(this.f3807a.f3801b) && this.f3807a.f3801b.length() > 5000) {
                    this.f3807a.f3801b = this.f3807a.f3801b.substring(0, 5000);
                }
                this.f3808b.a(new a(format, this.f3807a.d, this.f3807a.f3801b, this.f3807a.c, a(this.f3807a.e), this.f3807a.f));
            }
        } catch (Exception unused) {
        }
    }

    private static String a(com.helpshift.p.b.a[] aVarArr) {
        JSONArray jSONArray = new JSONArray();
        if (aVarArr == null || aVarArr.length == 0) {
            return jSONArray.toString();
        }
        for (com.helpshift.p.b.a aVar : aVarArr) {
            if (aVar != null) {
                if (jSONArray.length() > 20) {
                    break;
                }
                JSONObject jSONObject = (JSONObject) aVar.b();
                if (jSONObject.toString().length() <= 5000) {
                    jSONArray.put(jSONObject);
                }
            }
        }
        return jSONArray.toString();
    }
}
