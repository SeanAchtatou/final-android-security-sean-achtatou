package com.helpshift.p;

import com.facebook.internal.ServerProtocol;
import com.helpshift.a.b.c;
import com.helpshift.common.c.b.o;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.h;
import com.helpshift.common.k;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/* compiled from: ErrorReportsDM */
public class a {

    /* renamed from: a  reason: collision with root package name */
    j f3787a;

    /* renamed from: b  reason: collision with root package name */
    ab f3788b;

    public a(ab abVar, j jVar) {
        this.f3788b = abVar;
        this.f3787a = jVar;
    }

    public final void a(h<com.helpshift.common.e.a.j, Float> hVar, List<com.helpshift.p.c.a> list, c cVar, String str, String str2, String str3, String str4, String str5, String str6) {
        this.f3787a.b(new b(this, list, str, str4, cVar, str5, str6, str2, str3, hVar));
    }

    /* access modifiers changed from: package-private */
    public final Map<String, String> a() {
        try {
            HashMap hashMap = new HashMap();
            ArrayList arrayList = new ArrayList();
            arrayList.add("platform-id=sdk");
            String uuid = UUID.randomUUID().toString();
            arrayList.add("token=" + uuid);
            hashMap.put("token", uuid);
            hashMap.put("sm", this.f3788b.p().a(o.a()));
            arrayList.add("sm=" + this.f3788b.p().a(o.a()));
            hashMap.put("signature", this.f3787a.e().a(k.a("&", arrayList), ServerProtocol.DIALOG_PARAM_SDK_VERSION));
            return hashMap;
        } catch (GeneralSecurityException e) {
            throw RootAPIException.a(e, null, "SecurityException while creating signature");
        }
    }
}
