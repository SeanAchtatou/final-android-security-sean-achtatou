package com.helpshift.p;

import android.content.Context;
import java.util.HashMap;

/* compiled from: LoggerProvider */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private static HashMap<String, c> f3805a = new HashMap<>();

    /* renamed from: b  reason: collision with root package name */
    private static final Object f3806b = new Object();

    public static c a(Context context, String str, String str2) {
        c cVar;
        if (context == null) {
            return null;
        }
        synchronized (f3806b) {
            cVar = f3805a.get(str);
            if (cVar == null) {
                cVar = new e(context, str, str2);
                f3805a.put(str, cVar);
            }
        }
        return cVar;
    }
}
