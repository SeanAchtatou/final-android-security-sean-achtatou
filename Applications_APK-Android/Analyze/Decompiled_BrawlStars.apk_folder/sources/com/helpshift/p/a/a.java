package com.helpshift.p.a;

import android.content.Context;

/* compiled from: LogSQLiteStorage */
public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f3789a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private c f3790b;

    public a(Context context, String str) {
        this.f3790b = new c(context, str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0097, code lost:
        r10 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0099, code lost:
        r1 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009b, code lost:
        r10 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009c, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r2.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r2.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008c A[SYNTHETIC, Splitter:B:22:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0093 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0097 A[ExcHandler: all (th java.lang.Throwable), PHI: r3 
      PHI: (r3v4 android.database.Cursor) = (r3v6 android.database.Cursor), (r3v8 android.database.Cursor) binds: [B:18:0x003a, B:11:0x0015] A[DONT_GENERATE, DONT_INLINE], Splitter:B:11:0x0015] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009b A[ExcHandler: all (th java.lang.Throwable), Splitter:B:5:0x000a] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a3 A[SYNTHETIC, Splitter:B:36:0x00a3] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00aa A[SYNTHETIC, Splitter:B:40:0x00aa] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b1 A[SYNTHETIC, Splitter:B:47:0x00b1] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00ba A[SYNTHETIC, Splitter:B:52:0x00ba] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.helpshift.p.c.a r10) {
        /*
            r9 = this;
            java.lang.Object r0 = com.helpshift.p.a.a.f3789a
            monitor-enter(r0)
            r1 = 0
            com.helpshift.p.a.c r2 = r9.f3790b     // Catch:{ Exception -> 0x00ae, all -> 0x009e }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ Exception -> 0x00ae, all -> 0x009e }
            r2.beginTransaction()     // Catch:{ Exception -> 0x00af, all -> 0x009b }
            java.lang.String r3 = "SELECT rowid FROM LOG_MESSAGES"
            android.database.Cursor r3 = r2.rawQuery(r3, r1)     // Catch:{ Exception -> 0x0038, all -> 0x009b }
            if (r3 == 0) goto L_0x0053
            int r4 = r3.getCount()     // Catch:{ Exception -> 0x0036, all -> 0x0097 }
            r5 = 100
            if (r4 < r5) goto L_0x0053
            r3.moveToFirst()     // Catch:{ Exception -> 0x0036, all -> 0x0097 }
            r4 = 0
            int r5 = r3.getInt(r4)     // Catch:{ Exception -> 0x0036, all -> 0x0097 }
            java.lang.String r6 = "LOG_MESSAGES"
            java.lang.String r7 = "rowid = ?"
            r8 = 1
            java.lang.String[] r8 = new java.lang.String[r8]     // Catch:{ Exception -> 0x0036, all -> 0x0097 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0036, all -> 0x0097 }
            r8[r4] = r5     // Catch:{ Exception -> 0x0036, all -> 0x0097 }
            r2.delete(r6, r7, r8)     // Catch:{ Exception -> 0x0036, all -> 0x0097 }
            goto L_0x0053
        L_0x0036:
            r4 = move-exception
            goto L_0x003a
        L_0x0038:
            r4 = move-exception
            r3 = r1
        L_0x003a:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r5.<init>()     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            java.lang.String r6 = "Error in rotation of logs + "
            r5.append(r6)     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            java.lang.String r4 = r4.getMessage()     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r5.append(r4)     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r5.toString()     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            java.lang.String r4 = "LOG_MESSAGES"
            r2.delete(r4, r1, r1)     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
        L_0x0053:
            java.lang.String r4 = "LOG_MESSAGES"
            android.content.ContentValues r5 = new android.content.ContentValues     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r5.<init>()     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            java.lang.String r6 = "TIMESTAMP"
            java.lang.String r7 = r10.f3798a     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r5.put(r6, r7)     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            java.lang.String r6 = "MESSAGE"
            java.lang.String r7 = r10.f3799b     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r5.put(r6, r7)     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            java.lang.String r6 = "LEVEL"
            java.lang.String r7 = r10.d     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r5.put(r6, r7)     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            java.lang.String r6 = "EXTRAS"
            java.lang.String r7 = r10.e     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r5.put(r6, r7)     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            java.lang.String r6 = "STACKTRACE"
            java.lang.String r7 = r10.c     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r5.put(r6, r7)     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            java.lang.String r6 = "SDK_VERSION"
            java.lang.String r10 = r10.f     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r5.put(r6, r10)     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r2.insert(r4, r1, r5)     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            r2.setTransactionSuccessful()     // Catch:{ Exception -> 0x0099, all -> 0x0097 }
            if (r2 == 0) goto L_0x0091
            r2.endTransaction()     // Catch:{ Exception -> 0x0090 }
            goto L_0x0091
        L_0x0090:
        L_0x0091:
            if (r3 == 0) goto L_0x00bd
            r3.close()     // Catch:{ all -> 0x00b5 }
            goto L_0x00bd
        L_0x0097:
            r10 = move-exception
            goto L_0x00a1
        L_0x0099:
            r1 = r3
            goto L_0x00af
        L_0x009b:
            r10 = move-exception
            r3 = r1
            goto L_0x00a1
        L_0x009e:
            r10 = move-exception
            r2 = r1
            r3 = r2
        L_0x00a1:
            if (r2 == 0) goto L_0x00a8
            r2.endTransaction()     // Catch:{ Exception -> 0x00a7 }
            goto L_0x00a8
        L_0x00a7:
        L_0x00a8:
            if (r3 == 0) goto L_0x00ad
            r3.close()     // Catch:{ all -> 0x00b5 }
        L_0x00ad:
            throw r10     // Catch:{ all -> 0x00b5 }
        L_0x00ae:
            r2 = r1
        L_0x00af:
            if (r2 == 0) goto L_0x00b8
            r2.endTransaction()     // Catch:{ Exception -> 0x00b7 }
            goto L_0x00b8
        L_0x00b5:
            r10 = move-exception
            goto L_0x00bf
        L_0x00b7:
        L_0x00b8:
            if (r1 == 0) goto L_0x00bd
            r1.close()     // Catch:{ all -> 0x00b5 }
        L_0x00bd:
            monitor-exit(r0)     // Catch:{ all -> 0x00b5 }
            return
        L_0x00bf:
            monitor-exit(r0)     // Catch:{ all -> 0x00b5 }
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.p.a.a.a(com.helpshift.p.c.a):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x007d A[Catch:{ Exception -> 0x0075, all -> 0x0073 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0084 A[Catch:{ Exception -> 0x0075, all -> 0x0073 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.helpshift.p.c.a> a() {
        /*
            r13 = this;
            java.lang.Object r0 = com.helpshift.p.a.a.f3789a
            monitor-enter(r0)
            r1 = 0
            com.helpshift.p.a.c r2 = r13.f3790b     // Catch:{ Exception -> 0x0081, all -> 0x0077 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0081, all -> 0x0077 }
            java.lang.String r3 = "SELECT * FROM LOG_MESSAGES"
            android.database.Cursor r2 = r2.rawQuery(r3, r1)     // Catch:{ Exception -> 0x0081, all -> 0x0077 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            r3.<init>()     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            if (r2 == 0) goto L_0x006c
            boolean r4 = r2.moveToFirst()     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            if (r4 == 0) goto L_0x006c
        L_0x001d:
            boolean r4 = r2.isAfterLast()     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            if (r4 != 0) goto L_0x006c
            java.lang.String r4 = "TIMESTAMP"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r6 = r2.getString(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r4 = "MESSAGE"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r8 = r2.getString(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r4 = "LEVEL"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r7 = r2.getString(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r4 = "STACKTRACE"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r9 = r2.getString(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r4 = "EXTRAS"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r10 = r2.getString(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r4 = "SDK_VERSION"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            java.lang.String r11 = r2.getString(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            com.helpshift.p.c.a r4 = new com.helpshift.p.c.a     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            r5 = r4
            r5.<init>(r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            r3.add(r4)     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            r2.moveToNext()     // Catch:{ Exception -> 0x0075, all -> 0x0073 }
            goto L_0x001d
        L_0x006c:
            if (r2 == 0) goto L_0x0071
            r2.close()     // Catch:{ all -> 0x0089 }
        L_0x0071:
            r1 = r3
            goto L_0x0087
        L_0x0073:
            r1 = move-exception
            goto L_0x007b
        L_0x0075:
            goto L_0x0082
        L_0x0077:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
        L_0x007b:
            if (r2 == 0) goto L_0x0080
            r2.close()     // Catch:{ all -> 0x0089 }
        L_0x0080:
            throw r1     // Catch:{ all -> 0x0089 }
        L_0x0081:
            r2 = r1
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()     // Catch:{ all -> 0x0089 }
        L_0x0087:
            monitor-exit(r0)     // Catch:{ all -> 0x0089 }
            return r1
        L_0x0089:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0089 }
            goto L_0x008d
        L_0x008c:
            throw r1
        L_0x008d:
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.p.a.a.a():java.util.List");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0011 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b() {
        /*
            r3 = this;
            java.lang.Object r0 = com.helpshift.p.a.a.f3789a
            monitor-enter(r0)
            com.helpshift.p.a.c r1 = r3.f3790b     // Catch:{ Exception -> 0x0011 }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ Exception -> 0x0011 }
            java.lang.String r2 = "DELETE FROM LOG_MESSAGES"
            r1.execSQL(r2)     // Catch:{ Exception -> 0x0011 }
            goto L_0x0011
        L_0x000f:
            r1 = move-exception
            goto L_0x0013
        L_0x0011:
            monitor-exit(r0)     // Catch:{ all -> 0x000f }
            return
        L_0x0013:
            monitor-exit(r0)     // Catch:{ all -> 0x000f }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.p.a.a.b():void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v8, resolved type: java.lang.String} */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0061, code lost:
        if (r2 != null) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0063, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x007e, code lost:
        if (r2 == null) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0081, code lost:
        return r0;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(java.util.List<java.lang.String> r7) {
        /*
            r6 = this;
            r0 = 0
            if (r7 == 0) goto L_0x0088
            boolean r1 = r7.isEmpty()
            if (r1 == 0) goto L_0x000b
            goto L_0x0088
        L_0x000b:
            int r1 = r7.size()
            r2 = 0
            if (r1 != 0) goto L_0x0014
            r1 = r2
            goto L_0x0031
        L_0x0014:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "("
            r3.<init>(r4)
            r4 = 0
        L_0x001c:
            int r5 = r1 + -1
            if (r4 >= r5) goto L_0x0028
            java.lang.String r5 = "?,"
            r3.append(r5)
            int r4 = r4 + 1
            goto L_0x001c
        L_0x0028:
            java.lang.String r1 = "?)"
            r3.append(r1)
            java.lang.String r1 = r3.toString()
        L_0x0031:
            com.helpshift.p.a.c r3 = r6.f3790b     // Catch:{ Exception -> 0x0069 }
            android.database.sqlite.SQLiteDatabase r3 = r3.getReadableDatabase()     // Catch:{ Exception -> 0x0069 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0069 }
            r4.<init>()     // Catch:{ Exception -> 0x0069 }
            java.lang.String r5 = "SELECT COUNT(*) FROM LOG_MESSAGES WHERE LEVEL IN "
            r4.append(r5)     // Catch:{ Exception -> 0x0069 }
            r4.append(r1)     // Catch:{ Exception -> 0x0069 }
            java.lang.String r1 = r4.toString()     // Catch:{ Exception -> 0x0069 }
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0069 }
            java.lang.Object[] r7 = r7.toArray(r4)     // Catch:{ Exception -> 0x0069 }
            java.lang.String[] r7 = (java.lang.String[]) r7     // Catch:{ Exception -> 0x0069 }
            android.database.Cursor r2 = r3.rawQuery(r1, r7)     // Catch:{ Exception -> 0x0069 }
            if (r2 == 0) goto L_0x0061
            boolean r7 = r2.moveToFirst()     // Catch:{ Exception -> 0x0069 }
            if (r7 == 0) goto L_0x0061
            int r7 = r2.getInt(r0)     // Catch:{ Exception -> 0x0069 }
            r0 = r7
        L_0x0061:
            if (r2 == 0) goto L_0x0081
        L_0x0063:
            r2.close()
            goto L_0x0081
        L_0x0067:
            r7 = move-exception
            goto L_0x0082
        L_0x0069:
            r7 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0067 }
            r1.<init>()     // Catch:{ all -> 0x0067 }
            java.lang.String r3 = "Error getting logs count : "
            r1.append(r3)     // Catch:{ all -> 0x0067 }
            java.lang.String r7 = r7.getMessage()     // Catch:{ all -> 0x0067 }
            r1.append(r7)     // Catch:{ all -> 0x0067 }
            r1.toString()     // Catch:{ all -> 0x0067 }
            if (r2 == 0) goto L_0x0081
            goto L_0x0063
        L_0x0081:
            return r0
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()
        L_0x0087:
            throw r7
        L_0x0088:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.p.a.a.a(java.util.List):int");
    }
}
