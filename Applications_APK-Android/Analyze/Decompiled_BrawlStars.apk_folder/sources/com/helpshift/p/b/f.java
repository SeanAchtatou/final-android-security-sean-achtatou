package com.helpshift.p.b;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StringExtrasModel */
class f implements a {

    /* renamed from: a  reason: collision with root package name */
    private String f3796a;

    /* renamed from: b  reason: collision with root package name */
    private String f3797b;

    f(String str, String str2) {
        this.f3796a = str;
        this.f3797b = str2;
    }

    public final String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f3796a);
        sb.append(" : ");
        String str = this.f3797b;
        if (str == null) {
            str = "";
        }
        sb.append(str);
        return sb.toString();
    }

    public final Object b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(this.f3796a, this.f3797b == null ? "" : this.f3797b);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
