package com.helpshift.p.b;

import java.util.Map;

/* compiled from: LogExtrasModelProvider */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static b f3793a;

    public static void a(b bVar) {
        f3793a = bVar;
    }

    public static a a(String str, String str2) {
        b bVar = f3793a;
        if (bVar != null) {
            return bVar.a(str, str2);
        }
        return null;
    }

    public static a a(String str, Map map) {
        b bVar = f3793a;
        if (bVar != null) {
            return bVar.a(str, map);
        }
        return null;
    }
}
