package com.helpshift.p.b;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: MapExtrasModel */
class e implements a {

    /* renamed from: a  reason: collision with root package name */
    private String f3794a;

    /* renamed from: b  reason: collision with root package name */
    private Map f3795b;

    e(String str, Map map) {
        this.f3794a = str;
        this.f3795b = map;
    }

    public final String a() {
        Map map = this.f3795b;
        if (map == null) {
            return this.f3794a + " : " + this.f3795b;
        }
        JSONObject jSONObject = new JSONObject(map);
        return this.f3794a + " : " + jSONObject.toString();
    }

    public final Object b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(this.f3794a, this.f3795b == null ? "" : this.f3795b.toString());
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
