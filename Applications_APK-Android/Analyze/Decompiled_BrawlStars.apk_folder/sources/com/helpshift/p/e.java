package com.helpshift.p;

import android.content.Context;
import android.util.Log;
import com.helpshift.p.a.a;
import com.helpshift.p.a.b;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: Logger */
class e implements c {

    /* renamed from: a  reason: collision with root package name */
    private int f3802a = 4;

    /* renamed from: b  reason: collision with root package name */
    private final String f3803b = e.class.getSimpleName();
    private final String c;
    private boolean d;
    private boolean e;
    private long f;
    private b g;
    private ThreadPoolExecutor h;
    private final SimpleDateFormat i = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);

    e(Context context, String str, String str2) {
        this.g = new a(context, str);
        this.c = str2;
    }

    public final void a(boolean z, boolean z2) {
        this.d = z;
        if (this.e != z2) {
            this.e = z2;
            if (this.e) {
                this.h = new ThreadPoolExecutor(1, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new f(this));
                return;
            }
            ThreadPoolExecutor threadPoolExecutor = this.h;
            if (threadPoolExecutor != null) {
                threadPoolExecutor.shutdown();
            }
        }
    }

    public final void a(int i2) {
        this.f3802a = i2;
    }

    public final void a(long j) {
        this.f = j;
    }

    public final List<com.helpshift.p.c.a> a() {
        return this.g.a();
    }

    public final void b() {
        this.g.b();
    }

    public final int b(int i2) {
        b bVar = this.g;
        ArrayList arrayList = new ArrayList();
        arrayList.add("FATAL");
        return bVar.a(arrayList);
    }

    private Future a(String str, String str2, String str3, com.helpshift.p.b.a[] aVarArr) {
        d dVar = new d();
        dVar.d = str;
        dVar.e = aVarArr;
        dVar.f3801b = str2;
        dVar.f3800a = System.currentTimeMillis() + this.f;
        dVar.c = str3;
        dVar.f = this.c;
        try {
            return this.h.submit(new h(dVar, this.g, this.i));
        } catch (RejectedExecutionException unused) {
            "Rejected execution of log message : " + dVar.f3801b;
            return null;
        }
    }

    private static boolean a(Throwable[] thArr) {
        if (thArr == null) {
            return false;
        }
        for (Throwable th : thArr) {
            if (th instanceof UnknownHostException) {
                return true;
            }
        }
        return false;
    }

    private static String a(com.helpshift.p.b.a[] aVarArr) {
        if (aVarArr == null || aVarArr.length <= 0) {
            return " ";
        }
        StringBuilder sb = new StringBuilder(" ");
        for (com.helpshift.p.b.a aVar : aVarArr) {
            if (aVar != null) {
                sb.append(aVar.a());
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    private String b(Throwable[] thArr) {
        if (thArr == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (a(thArr)) {
            return "UnknownHostException";
        }
        for (Throwable stackTraceString : thArr) {
            sb.append(Log.getStackTraceString(stackTraceString));
        }
        return sb.toString();
    }

    public final void a(String str, String str2, Throwable[] thArr, com.helpshift.p.b.a... aVarArr) {
        if (this.d && this.f3802a <= 2) {
            str2 + a(aVarArr) + b(thArr);
        }
    }

    public final void b(String str, String str2, Throwable[] thArr, com.helpshift.p.b.a... aVarArr) {
        String str3;
        if (!this.d || this.f3802a > 4) {
            str3 = null;
        } else {
            str3 = b(thArr);
            str2 + a(aVarArr) + str3;
        }
        if (this.e) {
            if (str3 == null) {
                str3 = b(thArr);
            }
            a("WARN", str2, str3, aVarArr);
        }
    }

    public final void c(String str, String str2, Throwable[] thArr, com.helpshift.p.b.a... aVarArr) {
        String str3;
        if (!this.d || this.f3802a > 8) {
            str3 = null;
        } else {
            str3 = b(thArr);
            str2 + a(aVarArr) + str3;
        }
        if (this.e && !a(thArr)) {
            if (str3 == null) {
                str3 = b(thArr);
            }
            a("ERROR", str2, str3, aVarArr);
        }
    }

    public final void d(String str, String str2, Throwable[] thArr, com.helpshift.p.b.a... aVarArr) {
        String str3;
        if (!this.d || this.f3802a > 16) {
            str3 = null;
        } else {
            str3 = b(thArr);
            str2 + a(aVarArr) + str3;
        }
        if (this.e) {
            if (str3 == null) {
                str3 = b(thArr);
            }
            Future a2 = a("FATAL", str2, str3, aVarArr);
            if (a2 != null) {
                try {
                    a2.get();
                } catch (Exception e2) {
                    "Error logging fatal log : " + e2.getMessage();
                }
            }
        }
    }
}
