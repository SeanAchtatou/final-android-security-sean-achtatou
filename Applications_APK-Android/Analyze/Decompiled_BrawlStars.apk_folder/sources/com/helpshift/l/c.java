package com.helpshift.l;

import com.helpshift.common.c.j;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/* compiled from: UIThreadDelegateDecorator */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public j f3737a;

    /* renamed from: b  reason: collision with root package name */
    public b f3738b;
    public Map<String, Boolean> c = new HashMap();

    public c(j jVar) {
        this.f3737a = jVar;
    }

    public final void a(String str) {
        if (this.f3738b != null) {
            this.f3737a.c(new f(this, str));
        }
    }

    public final void b(String str) {
        if (this.f3738b != null) {
            this.f3737a.c(new h(this, str));
        }
    }

    public final void a(File file) {
        if (this.f3738b != null) {
            this.f3737a.c(new j(this, file));
        }
    }

    public final void a(int i) {
        if (this.f3738b != null) {
            this.f3737a.c(new k(this, i));
        }
    }

    public final boolean a() {
        return this.f3738b != null;
    }

    public final b b() {
        return this.f3738b;
    }
}
