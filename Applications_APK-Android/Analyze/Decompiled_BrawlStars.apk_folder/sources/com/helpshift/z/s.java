package com.helpshift.z;

import java.util.regex.Pattern;

/* compiled from: TextViewState */
public class s extends c {
    public static final Pattern e = Pattern.compile("\\W+");

    /* renamed from: a  reason: collision with root package name */
    protected String f4370a;

    /* renamed from: b  reason: collision with root package name */
    protected a f4371b;
    public boolean d;

    /* compiled from: TextViewState */
    public enum a {
        EMPTY,
        LESS_THAN_MINIMUM_LENGTH,
        ONLY_SPECIAL_CHARACTERS,
        INVALID_EMAIL
    }

    protected s(boolean z) {
        this.d = z;
    }

    public final a a() {
        return this.f4371b;
    }

    public final String b() {
        String str = this.f4370a;
        return str == null ? "" : str.trim();
    }

    public final boolean e() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        a(this);
    }
}
