package com.helpshift.z;

import com.helpshift.common.c.j;

/* compiled from: HSBaseObservable */
public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    private j f4363a;
    e c;

    /* access modifiers changed from: protected */
    public abstract void c();

    public final void a(j jVar, e eVar) {
        this.f4363a = jVar;
        this.c = eVar;
        c();
    }

    public final void d() {
        this.c = null;
    }

    public final void a(Object obj) {
        j jVar;
        if (this.c != null && (jVar = this.f4363a) != null) {
            jVar.c(new d(this, obj));
        }
    }
}
