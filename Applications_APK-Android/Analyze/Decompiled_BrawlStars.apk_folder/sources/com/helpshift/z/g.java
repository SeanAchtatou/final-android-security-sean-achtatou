package com.helpshift.z;

import com.helpshift.j.d.d;

/* compiled from: ImageAttachmentViewState */
public class g extends c {

    /* renamed from: a  reason: collision with root package name */
    protected d f4367a;

    /* renamed from: b  reason: collision with root package name */
    protected boolean f4368b = true;

    public final d a() {
        return this.f4367a;
    }

    public final String b() {
        d dVar = this.f4367a;
        if (dVar == null || dVar.d == null) {
            return "";
        }
        return this.f4367a.d;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        a(this);
    }
}
