package com.helpshift.z;

import com.helpshift.i.a.a;
import com.helpshift.j.a.a.o;
import com.helpshift.j.c;
import com.helpshift.j.d.e;

/* compiled from: WidgetGateway */
public class t {

    /* renamed from: a  reason: collision with root package name */
    public final a f4374a;

    /* renamed from: b  reason: collision with root package name */
    public final com.helpshift.j.c.a f4375b;

    public t(a aVar, com.helpshift.j.c.a aVar2) {
        this.f4374a = aVar;
        this.f4375b = aVar2;
    }

    public static void a(l lVar, com.helpshift.j.a.b.a aVar, boolean z) {
        boolean z2 = true;
        if (aVar.x || (!c.a(aVar.g) && (aVar.g != e.RESOLUTION_REJECTED || !z))) {
            z2 = false;
        }
        lVar.a(z2);
    }

    public final void a(i iVar, com.helpshift.j.a.b.a aVar, boolean z) {
        o oVar = o.NONE;
        if (aVar.x) {
            oVar = o.REDACTED_STATE;
        } else if (aVar.g == e.RESOLUTION_ACCEPTED) {
            if (this.f4375b.c.b(aVar)) {
                oVar = o.CSAT_RATING;
            } else {
                oVar = o.START_NEW_CONVERSATION;
            }
        } else if (aVar.g == e.REJECTED) {
            oVar = o.REJECTED_MESSAGE;
        } else if (aVar.g == e.ARCHIVED) {
            oVar = o.ARCHIVAL_MESSAGE;
        } else if (aVar.g == e.RESOLUTION_REQUESTED && this.f4374a.d()) {
            oVar = o.CONVERSATION_ENDED_MESSAGE;
        } else if (aVar.g == e.RESOLUTION_REJECTED) {
            if (z) {
                oVar = o.NONE;
            } else if (this.f4375b.c.b(aVar)) {
                oVar = o.CSAT_RATING;
            } else {
                oVar = o.START_NEW_CONVERSATION;
            }
        } else if (aVar.g == e.AUTHOR_MISMATCH) {
            oVar = o.AUTHOR_MISMATCH;
        }
        iVar.a(oVar);
    }

    public final void a(h hVar, com.helpshift.j.a.b.a aVar) {
        hVar.b(!aVar.x && aVar.g == e.RESOLUTION_REQUESTED && this.f4374a.d());
    }

    public final boolean a(com.helpshift.j.a.b.a aVar) {
        return !aVar.a() && a();
    }

    public final boolean a() {
        return !this.f4374a.a("fullPrivacy") && this.f4374a.a("allowUserAttachments");
    }

    public final void a(k kVar) {
        this.f4375b.a(kVar.a());
    }
}
