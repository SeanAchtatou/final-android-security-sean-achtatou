package com.helpshift.z;

import com.helpshift.z.s;

/* compiled from: MutableTextViewState */
public class o extends s {
    public o(boolean z) {
        super(z);
    }

    public final void a(s.a aVar) {
        this.f4371b = aVar;
        a(this);
    }

    public final void a(String str) {
        if (!b().equals(str)) {
            this.f4370a = str;
            if (a() != null) {
                a((s.a) null);
            }
        }
    }
}
