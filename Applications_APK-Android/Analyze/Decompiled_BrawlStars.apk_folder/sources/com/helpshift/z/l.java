package com.helpshift.z;

import com.helpshift.j.a.a.a.a;

/* compiled from: MutableReplyBoxViewState */
public class l extends p {
    public final void a(a aVar) {
        if (aVar == null || !aVar.equals(this.d)) {
            this.f4361b = true;
            this.d = aVar;
            a(this);
        }
    }

    public final void e() {
        if (this.d != null || !this.f4361b) {
            this.d = null;
            this.f4361b = true;
            a(this);
        }
    }

    public final void a(boolean z) {
        if (z != this.f4361b) {
            this.f4361b = z;
            this.d = null;
            a(this);
        }
    }
}
