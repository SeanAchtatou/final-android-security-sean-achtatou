package com.helpshift.network.a;

import android.content.Context;
import android.os.Build;
import com.helpshift.util.q;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/* compiled from: HSConnectivityManager */
public class e implements h {

    /* renamed from: a  reason: collision with root package name */
    private static e f3776a;

    /* renamed from: b  reason: collision with root package name */
    private Context f3777b = q.a();
    private b c = new b();
    private Set<h> d = Collections.synchronizedSet(new LinkedHashSet());
    private a e;

    private e() {
    }

    public static e a() {
        if (f3776a == null) {
            f3776a = new e();
        }
        return f3776a;
    }

    public final synchronized void a(h hVar) {
        a aVar;
        boolean isEmpty = this.d.isEmpty();
        this.d.add(hVar);
        if (isEmpty) {
            if (this.e == null) {
                Context context = this.f3777b;
                if (Build.VERSION.SDK_INT >= 24) {
                    aVar = new i(context);
                } else {
                    aVar = new c(context);
                }
                this.e = aVar;
            }
            this.e.a(this);
        } else {
            int i = f.f3778a[this.e.b().ordinal()];
            if (i == 1) {
                hVar.i_();
            } else if (i == 2) {
                hVar.c();
            }
        }
    }

    public final synchronized void b(h hVar) {
        this.d.remove(hVar);
        if (this.d.isEmpty()) {
            if (this.e != null) {
                this.e.a();
                this.e = null;
            }
        }
    }

    public final void i_() {
        if (!this.d.isEmpty()) {
            for (h i_ : this.d) {
                i_.i_();
            }
        }
    }

    public final void c() {
        if (!this.d.isEmpty()) {
            for (h c2 : this.d) {
                c2.c();
            }
        }
    }
}
