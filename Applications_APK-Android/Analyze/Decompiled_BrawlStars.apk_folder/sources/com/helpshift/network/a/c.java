package com.helpshift.network.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.helpshift.util.n;

/* compiled from: HSBelowNConnectivityManager */
class c extends BroadcastReceiver implements a {

    /* renamed from: a  reason: collision with root package name */
    private Context f3773a;

    /* renamed from: b  reason: collision with root package name */
    private h f3774b;

    c(Context context) {
        this.f3773a = context;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getExtras() != null && this.f3774b != null) {
            int i = d.f3775a[b().ordinal()];
            if (i == 1) {
                this.f3774b.i_();
            } else if (i == 2) {
                this.f3774b.c();
            }
        }
    }

    public final void a(h hVar) {
        this.f3774b = hVar;
        try {
            this.f3773a.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        } catch (Exception e) {
            n.c("Helpshift_BelowNConnMan", "Exception while registering network receiver", e);
        }
    }

    public final void a() {
        try {
            this.f3773a.unregisterReceiver(this);
        } catch (Exception e) {
            n.c("Helpshift_BelowNConnMan", "Exception while unregistering network receiver", e);
        }
    }

    public final g b() {
        g gVar = g.UNKNOWN;
        ConnectivityManager c = c();
        if (c == null) {
            return gVar;
        }
        NetworkInfo activeNetworkInfo = c.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnectedOrConnecting()) {
            return g.NOT_CONNECTED;
        }
        return g.CONNECTED;
    }

    private ConnectivityManager c() {
        try {
            return (ConnectivityManager) this.f3773a.getSystemService("connectivity");
        } catch (Exception e) {
            n.c("Helpshift_BelowNConnMan", "Exception while getting connectivity manager", e);
            return null;
        }
    }
}
