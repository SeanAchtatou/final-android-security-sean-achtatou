package com.helpshift.network.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import com.helpshift.util.n;

/* compiled from: HSOnAndAboveNConnectivityManager */
class i extends ConnectivityManager.NetworkCallback implements a {

    /* renamed from: a  reason: collision with root package name */
    private Context f3781a;

    /* renamed from: b  reason: collision with root package name */
    private h f3782b;

    i(Context context) {
        this.f3781a = context;
    }

    public final void a(h hVar) {
        this.f3782b = hVar;
        ConnectivityManager c = c();
        if (c != null) {
            try {
                c.registerDefaultNetworkCallback(this);
            } catch (Exception e) {
                n.c("Helpshift_AboveNConnMan", "Exception while registering network callback", e);
            }
        }
        if (b() == g.NOT_CONNECTED) {
            hVar.c();
        }
    }

    public final void a() {
        ConnectivityManager c = c();
        if (c != null) {
            try {
                c.unregisterNetworkCallback(this);
            } catch (Exception e) {
                n.c("Helpshift_AboveNConnMan", "Exception while unregistering network callback", e);
            }
        }
        this.f3782b = null;
    }

    public final g b() {
        g gVar = g.UNKNOWN;
        ConnectivityManager c = c();
        if (c == null) {
            return gVar;
        }
        if (c.getActiveNetwork() != null) {
            return g.CONNECTED;
        }
        return g.NOT_CONNECTED;
    }

    public void onAvailable(Network network) {
        h hVar = this.f3782b;
        if (hVar != null) {
            hVar.i_();
        }
    }

    public void onLost(Network network) {
        h hVar = this.f3782b;
        if (hVar != null) {
            hVar.c();
        }
    }

    public void onUnavailable() {
        h hVar = this.f3782b;
        if (hVar != null) {
            hVar.c();
        }
    }

    private ConnectivityManager c() {
        try {
            return (ConnectivityManager) this.f3781a.getSystemService("connectivity");
        } catch (Exception e) {
            n.c("Helpshift_AboveNConnMan", "Exception while getting connectivity manager", e);
            return null;
        }
    }
}
