package com.helpshift.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.helpshift.R;
import com.helpshift.s.b;
import com.helpshift.util.c;
import com.helpshift.util.n;
import com.helpshift.util.q;

public class MainActivity extends AppCompatActivity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (q.f4255a.get()) {
            if (getIntent().getBooleanExtra("showInFullScreen", false)) {
                getWindow().setFlags(1024, 1024);
            }
            try {
                Integer num = b.a.f3833a.f3831a.k;
                if (!(num == null || num.intValue() == -1)) {
                    setRequestedOrientation(num.intValue());
                }
            } catch (Exception e) {
                n.b("Helpshift_MainActvty", "Unable to set the requested orientation : " + e.getMessage());
            }
            Integer a2 = b.a.f3833a.f3832b.a();
            setTheme(c.a(this, a2) ? a2.intValue() : R.style.Helpshift_Theme_Base);
        }
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        if (q.f4255a.get()) {
            context = com.helpshift.util.b.e(context);
        }
        super.attachBaseContext(context);
    }
}
