package com.helpshift;

import com.helpshift.common.c.b.q;
import com.helpshift.common.c.l;
import com.helpshift.common.e.a.i;
import java.util.HashMap;
import java.util.Set;

/* compiled from: JavaCore */
class s extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f3828a;

    s(m mVar) {
        this.f3828a = mVar;
    }

    public final void a() {
        com.helpshift.common.c.b.s sVar = new com.helpshift.common.c.b.s(new q("/clear-idempotent-cache/", this.f3828a.d, this.f3828a.c), this.f3828a.c);
        Set<String> b2 = this.f3828a.c.t().b();
        if (!b2.isEmpty()) {
            String a2 = this.f3828a.c.p().a(b2);
            HashMap hashMap = new HashMap();
            hashMap.put("request_ids", a2);
            sVar.a(new i(hashMap));
            this.f3828a.c.t().c();
        }
    }
}
