package com.helpshift;

import com.helpshift.common.c.l;
import com.helpshift.common.h;

/* compiled from: JavaCore */
class q extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f3809a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ m f3810b;

    q(m mVar, h hVar) {
        this.f3810b = mVar;
        this.f3809a = hVar;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r10 = this;
            com.helpshift.m r0 = r10.f3810b     // Catch:{ all -> 0x00c1 }
            com.helpshift.j.c.a r0 = r0.u()     // Catch:{ all -> 0x00c1 }
            com.helpshift.a.b.c r1 = r0.e     // Catch:{ all -> 0x00c1 }
            r2 = 1
            if (r1 == 0) goto L_0x0099
            com.helpshift.a.b.c r1 = r0.e     // Catch:{ all -> 0x00c1 }
            boolean r1 = r1.j     // Catch:{ all -> 0x00c1 }
            if (r1 != 0) goto L_0x0013
            goto L_0x0099
        L_0x0013:
            boolean r1 = r0.r     // Catch:{ all -> 0x00c1 }
            r3 = 0
            if (r1 == 0) goto L_0x0027
            com.helpshift.util.aa r0 = new com.helpshift.util.aa     // Catch:{ all -> 0x00c1 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x00c1 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x00c1 }
            r0.<init>(r1, r2)     // Catch:{ all -> 0x00c1 }
            goto L_0x00a7
        L_0x0027:
            com.helpshift.j.b.a r1 = r0.g     // Catch:{ all -> 0x00c1 }
            com.helpshift.a.b.c r4 = r0.e     // Catch:{ all -> 0x00c1 }
            java.lang.Long r4 = r4.f3176a     // Catch:{ all -> 0x00c1 }
            long r4 = r4.longValue()     // Catch:{ all -> 0x00c1 }
            java.util.List r1 = r1.b(r4)     // Catch:{ all -> 0x00c1 }
            boolean r4 = com.helpshift.common.j.a(r1)     // Catch:{ all -> 0x00c1 }
            if (r4 == 0) goto L_0x0049
            com.helpshift.util.aa r0 = new com.helpshift.util.aa     // Catch:{ all -> 0x00c1 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x00c1 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x00c1 }
            r0.<init>(r1, r2)     // Catch:{ all -> 0x00c1 }
            goto L_0x00a7
        L_0x0049:
            boolean r1 = com.helpshift.j.c.c(r1)     // Catch:{ all -> 0x00c1 }
            if (r1 == 0) goto L_0x0053
            r4 = 60000(0xea60, double:2.9644E-319)
            goto L_0x0056
        L_0x0053:
            r4 = 300000(0x493e0, double:1.482197E-318)
        L_0x0056:
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00c1 }
            long r8 = r0.f3558a     // Catch:{ all -> 0x00c1 }
            long r6 = r6 - r8
            int r1 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r1 >= 0) goto L_0x0074
            int r0 = r0.p()     // Catch:{ all -> 0x00c1 }
            com.helpshift.util.aa r1 = new com.helpshift.util.aa     // Catch:{ all -> 0x00c1 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00c1 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x00c1 }
            r1.<init>(r0, r2)     // Catch:{ all -> 0x00c1 }
        L_0x0072:
            r0 = r1
            goto L_0x00a7
        L_0x0074:
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00c1 }
            r0.f3558a = r1     // Catch:{ all -> 0x00c1 }
            r0.h()     // Catch:{ all -> 0x00c1 }
            com.helpshift.j.a.b.a r1 = r0.q()     // Catch:{ all -> 0x00c1 }
            if (r1 == 0) goto L_0x008a
            com.helpshift.j.a.b r0 = r0.c     // Catch:{ all -> 0x00c1 }
            int r0 = r0.l(r1)     // Catch:{ all -> 0x00c1 }
            goto L_0x008b
        L_0x008a:
            r0 = 0
        L_0x008b:
            com.helpshift.util.aa r1 = new com.helpshift.util.aa     // Catch:{ all -> 0x00c1 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00c1 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x00c1 }
            r1.<init>(r0, r2)     // Catch:{ all -> 0x00c1 }
            goto L_0x0072
        L_0x0099:
            com.helpshift.util.aa r0 = new com.helpshift.util.aa     // Catch:{ all -> 0x00c1 }
            r1 = -1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x00c1 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x00c1 }
            r0.<init>(r1, r2)     // Catch:{ all -> 0x00c1 }
        L_0x00a7:
            com.helpshift.common.h r1 = r10.f3809a
            if (r1 == 0) goto L_0x00c0
            F r1 = r0.f4242a
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r1 = r1.intValue()
            if (r1 < 0) goto L_0x00bb
            com.helpshift.common.h r1 = r10.f3809a
            r1.a(r0)
            goto L_0x00c0
        L_0x00bb:
            com.helpshift.common.h r1 = r10.f3809a
            r1.b(r0)
        L_0x00c0:
            return
        L_0x00c1:
            r0 = move-exception
            com.helpshift.common.h r1 = r10.f3809a
            if (r1 == 0) goto L_0x00ca
            r2 = 0
            r1.b(r2)
        L_0x00ca:
            goto L_0x00cc
        L_0x00cb:
            throw r0
        L_0x00cc:
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.q.a():void");
    }
}
