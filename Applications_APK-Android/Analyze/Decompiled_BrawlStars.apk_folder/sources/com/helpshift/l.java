package com.helpshift;

import com.facebook.appevents.codeless.internal.Constants;
import java.util.HashMap;
import java.util.Map;

/* compiled from: InstallConfig */
public class l {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f3731a;

    /* renamed from: b  reason: collision with root package name */
    private final int f3732b;
    private final int c;
    private final int d;
    private final boolean e;
    private final boolean f;
    private final String g;
    private final boolean h;
    private final int i;
    private final String j;
    private final Map<String, Object> k;

    /* compiled from: InstallConfig */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public boolean f3733a = true;

        /* renamed from: b  reason: collision with root package name */
        public int f3734b;
        public int c;
        public int d;
        public boolean e = true;
        public boolean f = true;
        public String g = null;
        public boolean h = false;
        public int i = -1;
        public String j;
        public Map<String, Object> k;
    }

    public l(boolean z, int i2, int i3, int i4, boolean z2, boolean z3, String str, boolean z4, int i5, String str2, Map<String, Object> map) {
        this.f3731a = z;
        this.f3732b = i2;
        this.c = i3;
        this.d = i4;
        this.e = z2;
        this.f = z3;
        this.g = str;
        this.i = i5;
        this.k = map;
        this.h = z4;
        this.j = str2;
    }

    public final Map<String, Object> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("enableInAppNotification", Boolean.valueOf(this.f3731a));
        int i2 = this.f3732b;
        if (i2 != 0) {
            hashMap.put("notificationIcon", Integer.valueOf(i2));
        }
        int i3 = this.c;
        if (i3 != 0) {
            hashMap.put("largeNotificationIcon", Integer.valueOf(i3));
        }
        int i4 = this.d;
        if (i4 != 0) {
            hashMap.put("notificationSound", Integer.valueOf(i4));
        }
        hashMap.put("enableDefaultFallbackLanguage", Boolean.valueOf(this.e));
        hashMap.put("enableInboxPolling", Boolean.valueOf(this.f));
        hashMap.put("enableLogging", Boolean.valueOf(this.h));
        hashMap.put("font", this.g);
        hashMap.put("screenOrientation", Integer.valueOf(this.i));
        Map<String, Object> map = this.k;
        if (map != null) {
            Object remove = map.remove("disableErrorLogging");
            if (remove != null) {
                this.k.put("disableErrorReporting", remove);
            }
            for (String next : this.k.keySet()) {
                if (this.k.get(next) != null) {
                    hashMap.put(next, this.k.get(next));
                }
            }
        }
        hashMap.put("sdkType", Constants.PLATFORM);
        hashMap.put("supportNotificationChannelId", this.j);
        return hashMap;
    }
}
