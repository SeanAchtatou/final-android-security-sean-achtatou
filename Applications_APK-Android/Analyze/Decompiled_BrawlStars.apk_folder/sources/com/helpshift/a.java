package com.helpshift;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import com.helpshift.k;
import java.util.Map;

/* compiled from: Core */
public class a {

    /* renamed from: com.helpshift.a$a  reason: collision with other inner class name */
    /* compiled from: Core */
    public interface C0131a {
        void a(Application application, String str, String str2, String str3, Map<String, Object> map);

        void a(Context context, Intent intent);

        void a(Context context, String str);

        void a(String str, String str2);

        boolean a();

        boolean a(k kVar);

        void b(Application application, String str, String str2, String str3, Map<String, Object> map);
    }

    @Deprecated
    public static void a(String str, String str2, String str3) {
        CoreInternal.a(new k.a(str, str3).a(str2).a());
    }
}
