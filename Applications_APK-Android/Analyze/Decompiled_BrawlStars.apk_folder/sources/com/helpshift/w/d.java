package com.helpshift.w;

import com.helpshift.a.b.c;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import java.lang.ref.WeakReference;

/* compiled from: RedactionManager */
public class d {

    /* renamed from: a  reason: collision with root package name */
    j f4278a;

    /* renamed from: b  reason: collision with root package name */
    c f4279b;
    WeakReference<a> c;
    private b d;

    /* compiled from: RedactionManager */
    public interface a {
        void a(g gVar);
    }

    public d(ab abVar, j jVar, c cVar, a aVar) {
        this.f4278a = jVar;
        this.f4279b = cVar;
        this.c = new WeakReference<>(aVar);
        this.d = abVar.D();
    }

    public final synchronized void a() {
        g b2 = b();
        if (b2 == g.PENDING) {
            a(b2, g.IN_PROGRESS);
            this.f4278a.b(new e(this));
        }
    }

    public final g b() {
        c a2 = this.d.a(this.f4279b.f3176a.longValue());
        if (a2 == null) {
            return g.COMPLETED;
        }
        return a2.f4277b;
    }

    public void a(g gVar, g gVar2) {
        if (gVar2 == g.COMPLETED) {
            this.d.b(this.f4279b.f3176a.longValue());
        } else {
            this.d.a(this.f4279b.f3176a.longValue(), gVar2);
        }
        this.f4278a.a(new f(this, gVar, gVar2));
    }
}
