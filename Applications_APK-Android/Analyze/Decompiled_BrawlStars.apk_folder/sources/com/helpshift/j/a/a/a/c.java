package com.helpshift.j.a.a.a;

import com.helpshift.common.c.j;
import com.helpshift.common.g.b;
import com.helpshift.common.k;
import com.helpshift.util.o;
import java.text.ParseException;
import java.util.regex.Pattern;

/* compiled from: TextInput */
public class c extends a {
    public final String e;
    public final int f;
    public j g;

    public c(String str, boolean z, String str2, String str3, String str4, int i) {
        super(str, z, str2, str3);
        this.e = str4;
        this.f = i;
    }

    public final boolean a(String str) {
        int i = this.f;
        if (i == 2) {
            return o.a(str);
        }
        if (i != 3) {
            if (i != 4) {
                return true;
            }
            try {
                b.a("EEEE, MMMM dd, yyyy", this.g.f.c()).a(str.trim());
                return true;
            } catch (ParseException unused) {
                return false;
            }
        } else if (k.a(str)) {
            return false;
        } else {
            if (o.f4253a == null) {
                o.f4253a = Pattern.compile("^[+]?\\p{N}+(\\.\\p{N}+)?$");
            }
            return o.f4253a.matcher(str).matches();
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (cVar.f != this.f || !k.a(cVar.e, this.e) || !super.equals(obj)) {
            return false;
        }
        return true;
    }
}
