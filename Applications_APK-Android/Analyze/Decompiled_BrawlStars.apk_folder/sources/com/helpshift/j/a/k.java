package com.helpshift.j.a;

import com.helpshift.common.c.l;
import com.helpshift.common.g.a;
import com.helpshift.util.n;
import java.util.List;

/* compiled from: ConversationManager */
class k extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f3521a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f3522b;

    k(b bVar, List list) {
        this.f3522b = bVar;
        this.f3521a = list;
    }

    public final void a() {
        for (com.helpshift.j.a.a.k kVar : this.f3521a) {
            try {
                if (a.c(kVar.g)) {
                    kVar.g = null;
                }
            } catch (Exception e) {
                n.c("Helpshift_ConvManager", "Exception while deleting redacted AttachmentMessageDM file", e);
            }
        }
    }
}
