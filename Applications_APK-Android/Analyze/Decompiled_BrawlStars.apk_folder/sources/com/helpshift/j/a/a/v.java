package com.helpshift.j.a.a;

import com.helpshift.common.c.a.c;
import com.helpshift.common.c.b.i;
import com.helpshift.common.c.b.m;
import com.helpshift.common.c.b.q;
import com.helpshift.common.c.b.s;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.g.b;
import com.helpshift.common.k;
import com.helpshift.j.a.o;
import com.helpshift.p.b.a;
import com.helpshift.util.n;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;

/* compiled from: MessageDM */
public abstract class v extends Observable {
    protected ab A;
    public String B;
    public long C;
    public final boolean j;
    public final w k;
    public final ai l = new ai();
    public String m;
    public String n;
    public String o;
    public String p;
    public Long q;
    public Long r;
    public String s;
    public String t;
    public int u;
    public boolean v;
    public boolean w;
    public String x;
    public boolean y;
    protected j z;

    public abstract boolean a();

    v(String str, String str2, long j2, String str3, boolean z2, w wVar) {
        this.n = str;
        this.B = str2;
        this.C = j2;
        this.p = str3;
        this.j = z2;
        this.k = wVar;
    }

    public void a(j jVar, ab abVar) {
        this.z = jVar;
        this.A = abVar;
    }

    public final String h() {
        Date date;
        Locale c = this.z.f.c();
        try {
            date = b.a("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", c, "GMT").a(this.B);
        } catch (ParseException e) {
            Date date2 = new Date();
            n.a("Helpshift_MessageDM", "getSubText : ParseException", e, (a[]) null);
            date = date2;
        }
        String a2 = b.a(this.A.d().t() ? "H:mm" : "h:mm a", c).a(date);
        String j2 = j();
        if (k.a(j2)) {
            return a2;
        }
        return j2 + ", " + a2;
    }

    public final String i() {
        Locale c = this.z.f.c();
        Date date = new Date(this.C);
        String a2 = b.a(this.A.d().t() ? "H:mm" : "h:mm a", c).a(date);
        String a3 = b.a("EEEE, MMMM dd, yyyy", c).a(date);
        return a2 + " " + a3;
    }

    public final String j() {
        if (!this.j || !this.w || !this.z.f3285b.a("showAgentName") || k.a(this.p)) {
            return null;
        }
        return this.p.trim();
    }

    public final void k() {
        setChanged();
        notifyObservers();
    }

    public final void b(v vVar) {
        a(vVar);
        k();
    }

    public void a(v vVar) {
        this.n = vVar.n;
        this.B = vVar.B;
        this.C = vVar.C;
        this.p = vVar.p;
        if (k.a(this.m)) {
            this.m = vVar.m;
        }
        this.y = vVar.y;
    }

    /* access modifiers changed from: package-private */
    public final m b(String str) {
        return new com.helpshift.common.c.b.j(new i(new com.helpshift.common.c.b.b(new com.helpshift.common.c.b.v(new s(new com.helpshift.common.c.b.k(new q(str, this.z, this.A), this.A, new c(), str, String.valueOf(this.r)), this.A)))));
    }

    static String a(o oVar) {
        return "/preissues/" + oVar.c() + "/messages/";
    }

    static String b(o oVar) {
        return "/issues/" + oVar.b() + "/messages/";
    }

    public final ai l() {
        return this.l;
    }

    public final String m() {
        return this.B;
    }

    public final void c(String str) {
        if (!k.a(str)) {
            this.B = str;
        }
    }

    public final long n() {
        return this.C;
    }
}
