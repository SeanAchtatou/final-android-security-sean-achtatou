package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.k;
import com.helpshift.j.a.o;
import java.util.HashMap;

/* compiled from: AcceptedAppReviewMessageDM */
public class a extends l {

    /* renamed from: a  reason: collision with root package name */
    public String f3438a;

    public final boolean a() {
        return false;
    }

    public a(String str, String str2, long j, String str3, String str4, int i) {
        super(str, str2, j, str3, false, w.ACCEPTED_APP_REVIEW, i);
        this.f3438a = str4;
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof a) {
            this.f3438a = ((a) vVar).f3438a;
        }
    }

    public final void a(c cVar, o oVar) throws RootAPIException {
        if (!k.a(oVar.b())) {
            HashMap<String, String> a2 = com.helpshift.common.c.b.o.a(cVar);
            a2.put("body", this.n);
            a2.put("type", "ar");
            a2.put("refers", this.f3438a);
            try {
                a d = this.A.l().d(a(b(oVar), a2).f3323b);
                a(d);
                this.m = d.m;
                this.A.f().a(this);
            } catch (RootAPIException e) {
                if (e.c == b.INVALID_AUTH_TOKEN || e.c == b.AUTH_TOKEN_NOT_PROVIDED) {
                    this.z.j.a(cVar, e.c);
                }
                throw e;
            }
        } else {
            throw new UnsupportedOperationException("AcceptedAppReviewMessageDM send called with conversation in pre issue mode.");
        }
    }
}
