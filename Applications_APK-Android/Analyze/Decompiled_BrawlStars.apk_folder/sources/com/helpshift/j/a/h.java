package com.helpshift.j.a;

import com.helpshift.common.b;
import com.helpshift.common.c.l;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.a;
import com.helpshift.common.exception.b;

/* compiled from: ConversationManager */
class h extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3515a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f3516b;

    h(b bVar, l lVar) {
        this.f3516b = bVar;
        this.f3515a = lVar;
    }

    public final void a() {
        try {
            this.f3515a.a();
        } catch (RootAPIException e) {
            a aVar = e.c;
            if (aVar != b.NON_RETRIABLE && aVar != b.USER_PRE_CONDITION_FAILED) {
                this.f3516b.f3502b.g.a(b.a.CONVERSATION, e.a());
                throw e;
            }
        }
    }
}
