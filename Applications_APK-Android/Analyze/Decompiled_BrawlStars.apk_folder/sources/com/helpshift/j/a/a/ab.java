package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.common.c.b.b;
import com.helpshift.common.c.b.i;
import com.helpshift.common.c.b.j;
import com.helpshift.common.c.b.t;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.g.a;
import com.helpshift.common.k;
import com.helpshift.j.a.o;
import com.helpshift.m.c;
import java.util.HashMap;
import java.util.UUID;

/* compiled from: ScreenshotMessageDM */
public class ab extends u {
    public am D;

    /* renamed from: a  reason: collision with root package name */
    public String f3447a;

    public final boolean a() {
        return true;
    }

    public ab(String str, String str2, long j, String str3, String str4, String str5, String str6, String str7, int i, boolean z) {
        super(str, str2, j, str3, str7, str6, str5, str4, i, false, z, w.SCREENSHOT);
    }

    public final void d(String str) {
        if (str == null) {
            str = "localRscMessage_" + UUID.randomUUID().toString();
        }
        this.f3447a = str;
    }

    public final void a(c cVar, o oVar, boolean z) {
        String b2 = oVar.b();
        if (k.a(b2)) {
            throw new UnsupportedOperationException("ScreenshotMessageDM send called with conversation in pre issue mode.");
        } else if (b() != null) {
            if (z) {
                this.g = this.A.a(b(), this.f3447a);
                this.A.f().a(this);
            }
            a(am.SENDING);
            HashMap<String, String> a2 = com.helpshift.common.c.b.o.a(cVar);
            a2.put("body", "Screenshot sent");
            a2.put("type", "sc");
            a2.put("refers", this.f3447a);
            a2.put("screenshot", b());
            a2.put("originalFileName", this.d);
            try {
                String b3 = b(oVar);
                i iVar = new i(new b(new com.helpshift.common.c.b.k(new t(b3, this.z, this.A), this.A, new com.helpshift.common.c.a.c(), b3, String.valueOf(this.r))));
                ab c = this.A.l().c(new j(iVar).a(new com.helpshift.common.e.a.i(a2)).f3323b);
                this.m = c.m;
                this.o = c.o;
                a(c);
                a(am.SENT);
                this.A.f().a(this);
                k();
                HashMap hashMap = new HashMap();
                hashMap.put("id", b2);
                hashMap.put("body", c.e);
                hashMap.put("type", "url");
                this.z.c.a(com.helpshift.b.b.MESSAGE_ADDED, hashMap);
                this.z.e.b("User sent a screenshot");
            } catch (RootAPIException e) {
                if (e.c == com.helpshift.common.exception.b.INVALID_AUTH_TOKEN || e.c == com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED) {
                    this.z.j.a(cVar, e.c);
                }
                if (k.a(this.m)) {
                    a(am.UNSENT_RETRYABLE);
                }
                throw RootAPIException.a(e);
            }
        }
    }

    public final void a(am amVar) {
        this.D = amVar;
        k();
    }

    public final void a(boolean z) {
        if (this.m != null) {
            a(am.SENT);
        } else if (this.D != am.SENDING) {
            if (z) {
                a(am.UNSENT_RETRYABLE);
            } else {
                a(am.UNSENT_NOT_RETRYABLE);
            }
        }
    }

    public final String b() {
        if (!a.b(this.g)) {
            this.g = null;
        }
        return this.g;
    }

    public final void a(com.helpshift.common.e.ab abVar) {
        if (this.D == am.SENT && !a.b(b())) {
            abVar.x().a(new com.helpshift.m.a(this.e, this.d, this.c, this.h), c.a.INTERNAL_ONLY, new com.helpshift.common.c.b.a(this.z, abVar, this.e), new ac(this, abVar));
        }
    }
}
