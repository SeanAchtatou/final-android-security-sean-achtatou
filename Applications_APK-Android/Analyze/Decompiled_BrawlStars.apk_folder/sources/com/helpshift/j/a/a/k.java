package com.helpshift.j.a.a;

import java.util.Locale;

/* compiled from: AttachmentMessageDM */
public abstract class k extends v {
    public String c;
    public String d;
    public String e;
    public int f;
    public String g;
    public boolean h;

    k(String str, String str2, long j, String str3, int i, String str4, String str5, String str6, boolean z, boolean z2, w wVar) {
        super(str, str2, j, str3, z, wVar);
        this.f = i;
        this.c = str4;
        this.e = str5;
        this.d = str6;
        this.h = z2;
    }

    public final String g() {
        return a((double) this.f);
    }

    static String a(double d2) {
        String str;
        if (d2 < 1024.0d) {
            str = " B";
        } else if (d2 < 1048576.0d) {
            d2 /= 1024.0d;
            str = " KB";
        } else {
            d2 /= 1048576.0d;
            str = " MB";
        }
        if (" MB".equals(str)) {
            return String.format(Locale.US, "%.1f", Double.valueOf(d2)) + str;
        }
        return String.format(Locale.US, "%.0f", Double.valueOf(d2)) + str;
    }

    public void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof k) {
            k kVar = (k) vVar;
            this.c = kVar.c;
            this.d = kVar.d;
            this.e = kVar.e;
            this.f = kVar.f;
            this.h = kVar.h;
        }
    }

    static boolean a(String str) {
        return !com.helpshift.common.k.a(str) && str.startsWith("content://");
    }
}
