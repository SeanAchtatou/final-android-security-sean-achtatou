package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.k;
import com.helpshift.j.a.o;
import java.util.HashMap;

/* compiled from: FollowupRejectedMessageDM */
public class s extends l {

    /* renamed from: a  reason: collision with root package name */
    public String f3490a;
    public int c;
    public String d;

    public final boolean a() {
        return false;
    }

    public s(String str, String str2, long j, String str3, String str4, int i) {
        super(str, str2, j, str3, false, w.FOLLOWUP_REJECTED, i);
        this.f3490a = str4;
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof s) {
            this.f3490a = ((s) vVar).f3490a;
        }
    }

    public final void a(c cVar, o oVar) {
        if (!k.a(oVar.b())) {
            HashMap hashMap = new HashMap();
            hashMap.put("reason", Integer.valueOf(this.c));
            String str = this.d;
            if (str != null) {
                hashMap.put("open_issue_id", str);
            }
            String a2 = this.A.p().a(hashMap);
            HashMap<String, String> a3 = com.helpshift.common.c.b.o.a(cVar);
            a3.put("body", "Rejected the follow-up");
            a3.put("type", "rj");
            a3.put("refers", this.f3490a);
            a3.put("message_meta", a2);
            try {
                s i = this.A.l().i(a(b(oVar), a3).f3323b);
                a(i);
                this.o = i.o;
                this.m = i.m;
                this.A.f().a(this);
            } catch (RootAPIException e) {
                if (e.c == b.INVALID_AUTH_TOKEN || e.c == b.AUTH_TOKEN_NOT_PROVIDED) {
                    this.z.j.a(cVar, e.c);
                }
                throw e;
            }
        } else {
            throw new UnsupportedOperationException("FollowupRejectedMessageDM send called with conversation in pre issue mode.");
        }
    }
}
