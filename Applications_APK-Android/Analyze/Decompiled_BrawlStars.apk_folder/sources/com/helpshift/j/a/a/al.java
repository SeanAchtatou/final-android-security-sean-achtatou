package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.b.b;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.a.j;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.k;
import com.helpshift.j.a.o;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/* compiled from: UserMessageDM */
public class al extends v {
    public am c;

    public final boolean a() {
        return true;
    }

    /* access modifiers changed from: protected */
    public String c() {
        return "txt";
    }

    public String d() {
        return "";
    }

    public al(String str, String str2, long j, String str3) {
        super(str, str2, j, str3, false, w.USER_TEXT);
    }

    public al(String str, String str2, long j, String str3, w wVar) {
        super(str, str2, j, str3, false, wVar);
    }

    /* access modifiers changed from: protected */
    public Map<String, String> b() throws ParseException {
        return new HashMap();
    }

    public final void a(c cVar, o oVar) {
        String str;
        if (this.c != am.SENDING && this.c != am.SENT && this.c != am.UNSENT_NOT_RETRYABLE) {
            a(am.SENDING);
            if (oVar.a()) {
                str = a(oVar);
            } else {
                str = b(oVar);
            }
            try {
                Map<String, String> b2 = b();
                b2.putAll(com.helpshift.common.c.b.o.a(cVar));
                b2.put("body", this.n);
                b2.put("type", c());
                b2.put("refers", d());
                al a2 = a(b(str).a(new i(b2)));
                this.c = am.SENT;
                a(a2);
                this.m = a2.m;
                this.A.f().a(this);
                this.o = a2.o;
                k();
                this.z.e.b(this.n);
                if (!oVar.a()) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("id", oVar.b());
                    hashMap.put("body", this.n);
                    hashMap.put("type", "txt");
                    this.z.c.a(b.MESSAGE_ADDED, hashMap);
                }
            } catch (RootAPIException e) {
                if (e.c == com.helpshift.common.exception.b.INVALID_AUTH_TOKEN || e.c == com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED) {
                    f();
                    this.z.j.a(cVar, e.c);
                } else if (!(e.c == com.helpshift.common.exception.b.CONVERSATION_ARCHIVED || e.c == com.helpshift.common.exception.b.USER_PRE_CONDITION_FAILED)) {
                    f();
                }
                throw RootAPIException.a(e);
            } catch (ParseException e2) {
                f();
                throw RootAPIException.a(e2);
            } catch (Throwable th) {
                if (!oVar.a()) {
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put("id", oVar.b());
                    hashMap2.put("body", this.n);
                    hashMap2.put("type", "txt");
                    this.z.c.a(b.MESSAGE_ADDED, hashMap2);
                }
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    public al a(j jVar) {
        return this.A.l().a(jVar.f3323b);
    }

    private void f() {
        if (k.a(this.m)) {
            a(am.UNSENT_RETRYABLE);
        }
    }

    public final void a(boolean z) {
        if (!k.a(this.m)) {
            a(am.SENT);
        } else if (this.c != am.SENDING) {
            if (z) {
                a(am.UNSENT_RETRYABLE);
            } else {
                a(am.UNSENT_NOT_RETRYABLE);
            }
        }
    }

    public final am e() {
        return this.c;
    }

    public final void a(am amVar) {
        am amVar2 = this.c;
        this.c = amVar;
        if (amVar2 != this.c) {
            k();
        }
    }
}
