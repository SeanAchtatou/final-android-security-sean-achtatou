package com.helpshift.j.a;

import com.helpshift.common.c.l;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.j.a.a.m;
import com.helpshift.j.a.b.a;
import com.helpshift.j.d.e;

/* compiled from: ConversationManager */
class i extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f3517a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f3518b;
    final /* synthetic */ b c;

    i(b bVar, m mVar, a aVar) {
        this.c = bVar;
        this.f3517a = mVar;
        this.f3518b = aVar;
    }

    public final void a() {
        try {
            this.f3517a.a(this.c.c, this.f3518b);
        } catch (RootAPIException e) {
            if (e.c == b.CONVERSATION_ARCHIVED) {
                this.c.a(this.f3518b, e.ARCHIVED);
                return;
            }
            throw e;
        }
    }
}
