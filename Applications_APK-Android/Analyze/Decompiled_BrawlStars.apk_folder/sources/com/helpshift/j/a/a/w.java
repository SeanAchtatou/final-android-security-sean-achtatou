package com.helpshift.j.a.a;

/* compiled from: MessageType */
public enum w {
    USER_TEXT("mobile_text"),
    ADMIN_TEXT("admin_text"),
    ACCEPTED_APP_REVIEW("accepted_app_review"),
    REQUESTED_APP_REVIEW("request_app_review"),
    FOLLOWUP_ACCEPTED("followup_accepted"),
    FOLLOWUP_REJECTED("followup_rejected"),
    CONFIRMATION_ACCEPTED("confirmation_accepted"),
    CONFIRMATION_REJECTED("confirmation_rejected"),
    SCREENSHOT("screenshot"),
    REQUESTED_SCREENSHOT("request_screenshot"),
    ADMIN_ATTACHMENT("admin_attachment"),
    ADMIN_IMAGE_ATTACHMENT("admin_image_attachment"),
    REQUEST_FOR_REOPEN("request_for_reopen"),
    ADMIN_TEXT_WITH_TEXT_INPUT("admin_text_with_text_input"),
    ADMIN_TEXT_WITH_OPTION_INPUT("admin_text_with_option_input"),
    FAQ_LIST("faq_list"),
    FAQ_LIST_WITH_OPTION_INPUT("faq_list_with_option_input"),
    OPTION_INPUT("option_input"),
    UNSUPPORTED_ADMIN_MESSAGE_WITH_INPUT("unsupported_admin_message_with_input"),
    ADMIN_BOT_CONTROL("admin_bot_control"),
    USER_BOT_CONTROL("user_bot_control"),
    USER_RESP_FOR_TEXT_INPUT("mobile_response_for_text_input"),
    USER_RESP_FOR_OPTION_INPUT("mobile_response_for_option_input"),
    SYSTEM_DATE("system_date"),
    SYSTEM_DIVIDER("system_divider"),
    SYSTEM_PUBLISH_ID("system_publish_id"),
    SYSTEM_CONVERSATION_REDACTED("system_conv_redacted");
    
    public String B;

    private w(String str) {
        this.B = str;
    }

    public static w a(String str) {
        for (w wVar : values()) {
            if (wVar.B.equals(str)) {
                return wVar;
            }
        }
        return null;
    }

    public final String a() {
        return this.B;
    }
}
