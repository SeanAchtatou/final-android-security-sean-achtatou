package com.helpshift.j.a.a;

import com.helpshift.common.e.ab;
import com.helpshift.j.a.a.a.c;

/* compiled from: AdminMessageWithTextInputDM */
public class j extends h {

    /* renamed from: a  reason: collision with root package name */
    public final boolean f3480a;

    /* renamed from: b  reason: collision with root package name */
    public c f3481b;

    public j(String str, String str2, String str3, long j, String str4, String str5, String str6, boolean z, String str7, String str8, int i, boolean z2) {
        super(str, str2, str3, j, str4, w.ADMIN_TEXT_WITH_TEXT_INPUT);
        this.f3481b = new c(str5, z, str7, str8, str6, i);
        this.f3480a = z2;
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof j) {
            this.f3481b = ((j) vVar).f3481b;
        }
    }

    public final void a(com.helpshift.common.c.j jVar, ab abVar) {
        super.a(jVar, abVar);
        this.f3481b.g = jVar;
    }
}
