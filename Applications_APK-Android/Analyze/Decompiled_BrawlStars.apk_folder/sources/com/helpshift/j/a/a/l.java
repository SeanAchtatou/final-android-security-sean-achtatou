package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.common.c.b.b;
import com.helpshift.common.c.b.g;
import com.helpshift.common.c.b.i;
import com.helpshift.common.c.b.k;
import com.helpshift.common.c.b.q;
import com.helpshift.common.c.b.s;
import com.helpshift.common.c.b.v;
import com.helpshift.common.e.a.j;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.j.a.o;
import java.util.Map;

/* compiled from: AutoRetriableMessageDM */
public abstract class l extends v {

    /* renamed from: b  reason: collision with root package name */
    public int f3482b;

    public abstract void a(c cVar, o oVar);

    l(String str, String str2, long j, String str3, boolean z, w wVar, int i) {
        super(str, str2, j, str3, false, wVar);
        this.f3482b = i;
    }

    private void a(int i) {
        if (this.f3482b != i) {
            this.f3482b = i;
            this.A.f().a(this);
        }
    }

    public void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof l) {
            this.f3482b = ((l) vVar).f3482b;
        }
    }

    public final int b() {
        return this.f3482b;
    }

    /* access modifiers changed from: package-private */
    public final j a(String str, Map<String, String> map) {
        try {
            return new com.helpshift.common.c.b.j(new g(new i(new v(new b(new s(new k(new q(str, this.z, this.A), this.A, new com.helpshift.common.c.a.c(), str, String.valueOf(this.r)), this.A)))))).a(new com.helpshift.common.e.a.i(map));
        } catch (RootAPIException e) {
            if (e.c == com.helpshift.common.exception.b.NON_RETRIABLE || e.c == com.helpshift.common.exception.b.USER_PRE_CONDITION_FAILED) {
                a(3);
            } else {
                a(1);
            }
            throw e;
        }
    }
}
