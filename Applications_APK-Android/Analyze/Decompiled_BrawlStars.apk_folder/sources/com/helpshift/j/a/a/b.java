package com.helpshift.j.a.a;

import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.k;
import com.helpshift.m.c;

/* compiled from: AdminAttachmentMessageDM */
public class b extends k {

    /* renamed from: a  reason: collision with root package name */
    public a f3466a;

    /* renamed from: b  reason: collision with root package name */
    int f3467b = 0;

    /* compiled from: AdminAttachmentMessageDM */
    public enum a {
        DOWNLOAD_NOT_STARTED,
        DOWNLOADING,
        DOWNLOADED
    }

    public final boolean a() {
        return true;
    }

    public b(String str, String str2, String str3, long j, String str4, int i, String str5, String str6, String str7, boolean z) {
        super(str2, str3, j, str4, i, str5, str6, str7, true, z, w.ADMIN_ATTACHMENT);
        this.m = str;
        b();
    }

    public final void a(j jVar, ab abVar) {
        super.a(jVar, abVar);
        if (a(this.g)) {
            b();
        }
    }

    public final void b() {
        if (f() != null) {
            this.f3466a = a.DOWNLOADED;
        } else {
            this.f3466a = a.DOWNLOAD_NOT_STARTED;
        }
    }

    public final String c() {
        String d = d();
        if (k.a(d)) {
            return g();
        }
        return d + "/" + g();
    }

    public final String d() {
        if (this.f3466a == a.DOWNLOADING && this.f3467b > 0) {
            double d = (double) (this.f * this.f3467b);
            Double.isNaN(d);
            double d2 = d / 100.0d;
            if (d2 < ((double) this.f)) {
                return a(d2);
            }
        }
        return null;
    }

    public final boolean e() {
        return this.f3466a == a.DOWNLOAD_NOT_STARTED;
    }

    /* access modifiers changed from: package-private */
    public final void a(a aVar) {
        this.f3466a = aVar;
        k();
    }

    public final void a(com.helpshift.j.i.a aVar) {
        if (this.f3466a == a.DOWNLOADED) {
            if (aVar != null) {
                aVar.a(f(), this.c);
            }
        } else if (this.f3466a == a.DOWNLOAD_NOT_STARTED) {
            a(a.DOWNLOADING);
            this.A.x().a(new com.helpshift.m.a(this.e, this.d, this.c, this.h), c.a.EXTERNAL_ONLY, new com.helpshift.common.c.b.a(this.z, this.A, this.e), new c(this));
        }
    }

    private String f() {
        if (a(this.g)) {
            if (this.A != null && !this.A.d(this.g)) {
                this.g = null;
                this.f3466a = a.DOWNLOAD_NOT_STARTED;
            }
        } else if (!com.helpshift.common.g.a.b(this.g)) {
            this.g = null;
            this.f3466a = a.DOWNLOAD_NOT_STARTED;
        }
        return this.g;
    }
}
