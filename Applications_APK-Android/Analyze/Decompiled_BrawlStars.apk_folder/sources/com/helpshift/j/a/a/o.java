package com.helpshift.j.a.a;

/* compiled from: ConversationFooterState */
public enum o {
    NONE,
    CONVERSATION_ENDED_MESSAGE,
    START_NEW_CONVERSATION,
    CSAT_RATING,
    ARCHIVAL_MESSAGE,
    AUTHOR_MISMATCH,
    REJECTED_MESSAGE,
    REDACTED_STATE
}
