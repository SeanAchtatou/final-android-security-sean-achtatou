package com.helpshift.j.a.b;

import com.helpshift.common.g.d;
import com.helpshift.j.a.a.m;
import com.helpshift.j.a.a.n;
import com.helpshift.j.a.a.s;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.z;
import com.helpshift.j.a.o;
import com.helpshift.j.d.e;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/* compiled from: Conversation */
public class a implements o, Observer {
    public long A;
    public boolean B;
    public com.helpshift.j.a.a C;
    public boolean D;

    /* renamed from: a  reason: collision with root package name */
    public final Map<String, z> f3503a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    public Long f3504b;
    public String c;
    public String d;
    public String e;
    public String f;
    public e g;
    public String h;
    public String i;
    public d<v> j = new d<>();
    public String k;
    public boolean l;
    public String m;
    public boolean n;
    public boolean o;
    public com.helpshift.j.f.a p = com.helpshift.j.f.a.NONE;
    public int q;
    public String r;
    public boolean s;
    public long t;
    public long u;
    public String v;
    public boolean w;
    public boolean x;
    public boolean y;
    public String z;

    public a(String str, e eVar, String str2, long j2, String str3, String str4, String str5, boolean z2, String str6) {
        this.f = str;
        this.z = str2;
        this.A = j2;
        this.i = str3;
        this.k = str4;
        this.m = str5;
        this.l = z2;
        this.g = eVar;
        this.h = str6;
    }

    public final void a(long j2) {
        this.f3504b = Long.valueOf(j2);
        Iterator<E> it = this.j.iterator();
        while (it.hasNext()) {
            ((v) it.next()).q = this.f3504b;
        }
    }

    public final long d() {
        return this.A;
    }

    public final String e() {
        return this.z;
    }

    public final boolean a() {
        return "preissue".equals(this.h);
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public final void a(List<v> list) {
        this.j = new d<>(list);
        g();
    }

    public void update(Observable observable, Object obj) {
        if (observable instanceof v) {
            v vVar = (v) observable;
            this.j.a(this.j.indexOf(vVar), vVar);
        }
    }

    private void g() {
        d<v> dVar;
        if (this.g == e.RESOLUTION_REQUESTED && (dVar = this.j) != null && dVar.size() > 0) {
            v vVar = null;
            for (int size = this.j.size() - 1; size >= 0; size--) {
                vVar = (v) this.j.get(size);
                if (!(vVar instanceof s) && !(vVar instanceof z)) {
                    break;
                }
            }
            if (vVar instanceof m) {
                this.g = e.RESOLUTION_ACCEPTED;
            } else if (vVar instanceof n) {
                this.g = e.RESOLUTION_REJECTED;
            }
        }
    }

    public final void f() {
        Iterator<E> it = this.j.iterator();
        while (it.hasNext()) {
            ((v) it.next()).addObserver(this);
        }
    }
}
