package com.helpshift.j.a.a;

/* compiled from: UnsupportedAdminMessageWithInputDM */
public class aj extends h {

    /* renamed from: a  reason: collision with root package name */
    public String f3456a;

    /* renamed from: b  reason: collision with root package name */
    public String f3457b;
    public String c;

    public final boolean a() {
        return false;
    }

    public aj(String str, String str2, String str3, long j, String str4, String str5, String str6, String str7) {
        super(str, str2, str3, j, str4, w.UNSUPPORTED_ADMIN_MESSAGE_WITH_INPUT);
        this.f3456a = str5;
        this.f3457b = str6;
        this.c = str7;
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof aj) {
            aj ajVar = (aj) vVar;
            this.f3456a = ajVar.f3456a;
            this.f3457b = ajVar.f3457b;
            this.c = ajVar.c;
        }
    }
}
