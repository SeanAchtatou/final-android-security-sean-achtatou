package com.helpshift.j.a;

import com.helpshift.a.b.c;
import com.helpshift.common.c.b.g;
import com.helpshift.common.c.b.o;
import com.helpshift.common.c.b.q;
import com.helpshift.common.c.j;
import com.helpshift.common.c.l;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.j.a.a.aj;
import com.helpshift.j.a.a.ak;
import com.helpshift.j.a.a.al;
import com.helpshift.j.a.a.am;
import com.helpshift.j.a.a.ap;
import com.helpshift.j.a.a.d;
import com.helpshift.j.a.a.k;
import com.helpshift.j.a.a.m;
import com.helpshift.j.a.a.n;
import com.helpshift.j.a.a.p;
import com.helpshift.j.a.a.r;
import com.helpshift.j.a.a.s;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.w;
import com.helpshift.j.a.a.y;
import com.helpshift.j.a.a.z;
import com.helpshift.j.b.a;
import com.helpshift.j.d.e;
import com.helpshift.util.aa;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: ConversationManager */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public ab f3501a;

    /* renamed from: b  reason: collision with root package name */
    public j f3502b;
    public c c;
    public a d;
    public com.helpshift.i.a.a e;

    public b(ab abVar, j jVar, c cVar) {
        this.f3501a = abVar;
        this.f3502b = jVar;
        this.c = cVar;
        this.d = abVar.f();
        this.e = jVar.f3285b;
    }

    public final boolean a(com.helpshift.j.a.b.a aVar, int i, String str, boolean z) {
        com.helpshift.j.a.b.a aVar2 = aVar;
        int i2 = i;
        String str2 = str;
        if (aVar2.j == null || aVar2.j.size() <= 0) {
            return false;
        }
        v vVar = (v) aVar2.j.get(aVar2.j.size() - 1);
        if (!(vVar instanceof z)) {
            return false;
        }
        z zVar = (z) vVar;
        if (zVar.f3500a) {
            return false;
        }
        if (i2 == 1) {
            a(aVar2, 1, (String) null, vVar.m);
            return false;
        } else if (z) {
            a(aVar2, 4, (String) null, vVar.m);
            return false;
        } else if (i2 == 2) {
            a(aVar2, 3, (String) null, vVar.m);
            return false;
        } else if (str2 == null || str2.equals(aVar2.c)) {
            aVar2.g = e.WAITING_FOR_AGENT;
            aVar2.o = false;
            this.d.d(aVar2);
            aa<String, Long> b2 = com.helpshift.common.g.b.b(this.f3501a);
            r rVar = new r(null, (String) b2.f4242a, ((Long) b2.f4243b).longValue(), "mobile", vVar.m, 1);
            rVar.q = aVar2.f3504b;
            rVar.a(this.f3502b, this.f3501a);
            b(aVar2, rVar);
            zVar.b(true);
            this.d.a(zVar);
            a(new c(this, rVar, aVar2));
            return true;
        } else {
            a(aVar2, 2, str2, vVar.m);
            return false;
        }
    }

    private void a(com.helpshift.j.a.b.a aVar, int i, String str, String str2) {
        aa<String, Long> b2 = com.helpshift.common.g.b.b(this.f3501a);
        s sVar = new s(null, (String) b2.f4242a, ((Long) b2.f4243b).longValue(), "mobile", str2, 1);
        sVar.c = i;
        sVar.d = str;
        sVar.q = aVar.f3504b;
        sVar.a(this.f3502b, this.f3501a);
        b(aVar, sVar);
        a(new g(this, sVar, aVar));
    }

    private void b(com.helpshift.j.a.b.a aVar, v vVar) {
        this.d.a(vVar);
        vVar.a(this.f3502b, this.f3501a);
        vVar.addObserver(aVar);
        aVar.j.add(vVar);
    }

    public void a(l lVar) {
        this.f3502b.b(new h(this, lVar));
    }

    public final void a(com.helpshift.j.a.b.a aVar) {
        int i = f.f3511a[aVar.g.ordinal()];
        if (i == 1) {
            ArrayList<al> arrayList = new ArrayList<>();
            for (v next : this.d.c(aVar.f3504b.longValue())) {
                if ((next instanceof al) && next.m == null) {
                    arrayList.add((al) next);
                }
            }
            StringBuilder sb = new StringBuilder();
            for (al alVar : arrayList) {
                sb.append(alVar.n);
                sb.append("\n");
            }
            this.f3501a.e().d(this.c.f3176a.longValue(), sb.toString());
            c(aVar);
        } else if (i == 2 || i == 3) {
            c(aVar);
        }
        n(aVar);
    }

    private void n(com.helpshift.j.a.b.a aVar) {
        boolean m = m(aVar);
        Iterator<E> it = aVar.j.iterator();
        while (it.hasNext()) {
            a((v) it.next(), m);
        }
    }

    private void a(v vVar, boolean z) {
        b(vVar, z);
        if (vVar instanceof com.helpshift.j.a.a.ab) {
            ((com.helpshift.j.a.a.ab) vVar).a(this.f3501a);
        }
    }

    private static void b(v vVar, boolean z) {
        if (vVar instanceof al) {
            ((al) vVar).a(z);
        } else if (vVar instanceof com.helpshift.j.a.a.aa) {
            ((com.helpshift.j.a.a.aa) vVar).a(z);
        } else if (vVar instanceof com.helpshift.j.a.a.ab) {
            ((com.helpshift.j.a.a.ab) vVar).a(z);
        }
    }

    public final void a(com.helpshift.j.a.b.a aVar, boolean z) {
        aa<String, Long> b2 = com.helpshift.common.g.b.b(this.f3501a);
        String str = (String) b2.f4242a;
        long longValue = ((Long) b2.f4243b).longValue();
        if (z) {
            m mVar = new m("Accepted the solution", str, longValue, "mobile", 1);
            mVar.a(this.f3502b, this.f3501a);
            mVar.q = aVar.f3504b;
            this.d.a(mVar);
            a(new i(this, mVar, aVar));
            a(aVar, e.RESOLUTION_ACCEPTED);
            this.f3502b.c.a(com.helpshift.b.b.RESOLUTION_ACCEPTED, aVar.c);
            this.f3502b.e.b("User accepted the solution");
            return;
        }
        n nVar = new n("Did not accept the solution", str, longValue, "mobile", 1);
        nVar.q = aVar.f3504b;
        a(aVar, nVar);
        a(new j(this, nVar, aVar));
        a(aVar, e.RESOLUTION_REJECTED);
        this.f3502b.c.a(com.helpshift.b.b.RESOLUTION_REJECTED, aVar.c);
        this.f3502b.e.b("User rejected the solution");
    }

    public final void a(com.helpshift.j.a.b.a aVar, e eVar) {
        if (aVar.g != eVar) {
            com.helpshift.util.n.a("Helpshift_ConvManager", "Changing conversation status from: " + aVar.g + ", new status: " + eVar + ", for: " + aVar.c, (Throwable) null, (com.helpshift.p.b.a[]) null);
            aVar.g = eVar;
            a(aVar);
            this.d.d(aVar);
            if (aVar.C != null) {
                aVar.C.a(aVar.g);
            }
        }
    }

    public final void a(com.helpshift.j.a.b.a aVar, com.helpshift.j.a.b.a aVar2, boolean z, p pVar) {
        e eVar = aVar2.g;
        e eVar2 = aVar.g;
        if (f.f3511a[eVar.ordinal()] == 4 && (aVar.g == e.RESOLUTION_ACCEPTED || aVar.g == e.RESOLUTION_REJECTED)) {
            eVar = eVar2;
        }
        String str = aVar2.m;
        if (str != null) {
            aVar.m = str;
        }
        aVar.c = aVar2.c;
        aVar.d = aVar2.d;
        aVar.h = aVar2.h;
        aVar.f = aVar2.f;
        aVar.l = aVar2.l;
        aVar.k = aVar2.k;
        aVar.z = aVar2.z;
        aVar.A = aVar2.A;
        aVar.x = aVar2.x;
        aVar.i = aVar2.i;
        if (aVar2.p == com.helpshift.j.f.a.SUBMITTED_SYNCED) {
            aVar.p = aVar2.p;
        }
        aVar.g = eVar;
        a(aVar, z, aVar2.j, pVar);
    }

    public void a(com.helpshift.j.a.b.a aVar, v vVar) {
        this.d.a(vVar);
        c(aVar, vVar);
    }

    private void c(com.helpshift.j.a.b.a aVar, v vVar) {
        vVar.a(this.f3502b, this.f3501a);
        if (vVar.a()) {
            vVar.addObserver(aVar);
            aVar.j.add(vVar);
            com.helpshift.j.c.b(aVar.j);
        }
    }

    public final void a(com.helpshift.j.a.b.a aVar, boolean z, List<v> list, p pVar) {
        if (pVar == null) {
            pVar = new p();
        }
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        a(aVar, hashMap, hashMap2);
        ArrayList<v> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        for (v next : list) {
            v a2 = a(next, hashMap, hashMap2, pVar);
            if (a2 != null) {
                if (a2 instanceof al) {
                    a2.a(next);
                    ((al) a2).a(am.SENT);
                } else if (a2 instanceof com.helpshift.j.a.a.ab) {
                    a2.a(next);
                    ((com.helpshift.j.a.a.ab) a2).a(am.SENT);
                    if (a2.y) {
                        arrayList2.add((k) a2);
                    }
                } else if (a2 instanceof k) {
                    a2.b(next);
                    if (a2.y) {
                        arrayList2.add((k) a2);
                    }
                } else {
                    a2.b(next);
                }
                pVar.f3529a.add(a2);
            } else {
                arrayList.add(next);
            }
        }
        a((List<k>) arrayList2);
        if (!com.helpshift.common.j.a(arrayList)) {
            for (v vVar : arrayList) {
                vVar.a(this.f3502b, this.f3501a);
                vVar.q = aVar.f3504b;
                vVar.w = aVar.l;
                if (vVar instanceof al) {
                    ((al) vVar).a(am.SENT);
                } else if (vVar instanceof com.helpshift.j.a.a.ab) {
                    ((com.helpshift.j.a.a.ab) vVar).a(am.SENT);
                }
                vVar.addObserver(aVar);
            }
            if (z) {
                com.helpshift.j.c.b(arrayList);
                aVar.y = a(arrayList, aVar.y);
                aVar.j.addAll(arrayList);
                for (v vVar2 : arrayList) {
                    if (vVar2 instanceof com.helpshift.j.a.a.e) {
                        ((com.helpshift.j.a.a.e) vVar2).a(this.f3501a);
                    }
                    d(aVar, vVar2);
                }
            } else {
                aVar.j.addAll(arrayList);
            }
            pVar.f3530b.addAll(arrayList);
            a(aVar, (Collection<? extends v>) arrayList);
        }
    }

    private void a(List<k> list) {
        if (list.size() != 0) {
            this.f3502b.b(new k(this, list));
        }
    }

    private void a(com.helpshift.j.a.b.a aVar, Collection<? extends v> collection) {
        com.helpshift.j.a.b.a aVar2 = aVar;
        for (v vVar : collection) {
            if (f.f3512b[vVar.k.ordinal()] == 1) {
                aa<String, Long> b2 = com.helpshift.common.g.b.b(this.f3501a);
                aj ajVar = (aj) vVar;
                ak akVar = new ak("Unsupported bot input", (String) b2.f4242a, ((Long) b2.f4243b).longValue(), "mobile", "bot_cancelled", "unsupported_bot_input", ajVar.f3457b, ajVar.m, 1);
                akVar.q = aVar2.f3504b;
                a(aVar2, akVar);
                a(new l(this, akVar, aVar2));
            }
        }
    }

    private void d(com.helpshift.j.a.b.a aVar, v vVar) {
        if (vVar instanceof z) {
            z zVar = (z) vVar;
            if (!zVar.f3500a) {
                aVar.f3503a.put(vVar.m, zVar);
            }
        } else if (vVar instanceof r) {
            String str = ((r) vVar).f3489a;
            if (aVar.f3503a.containsKey(str)) {
                z remove = aVar.f3503a.remove(str);
                remove.a(this.f3502b, this.f3501a);
                remove.w = aVar.l;
                remove.b(true);
                this.d.a(remove);
            }
        }
    }

    private void a(com.helpshift.j.a.b.a aVar, Map<String, v> map, Map<String, v> map2) {
        ArrayList<v> arrayList = new ArrayList<>();
        List<v> c2 = this.d.c(aVar.f3504b.longValue());
        HashMap hashMap = new HashMap();
        Iterator<E> it = aVar.j.iterator();
        while (it.hasNext()) {
            v vVar = (v) it.next();
            if (vVar.r != null) {
                hashMap.put(vVar.r, vVar);
            }
        }
        for (v next : c2) {
            v vVar2 = (v) hashMap.get(next.r);
            if (vVar2 == null) {
                arrayList.add(next);
            } else {
                arrayList.add(vVar2);
            }
        }
        Map<String, String> c3 = this.f3501a.t().c(o(aVar));
        for (v vVar3 : arrayList) {
            if (!com.helpshift.common.k.a(vVar3.m)) {
                map.put(vVar3.m, vVar3);
            }
            if (vVar3.r != null) {
                String valueOf = String.valueOf(vVar3.r);
                if (c3 != null && c3.containsKey(valueOf)) {
                    map2.put(c3.get(valueOf), vVar3);
                }
            }
        }
    }

    private static String o(com.helpshift.j.a.b.a aVar) {
        if (aVar.a()) {
            return "/preissues/" + aVar.d + "/messages/";
        }
        return "/issues/" + aVar.c + "/messages/";
    }

    private static v a(v vVar, Map<String, v> map, Map<String, v> map2, p pVar) {
        if (map.containsKey(vVar.m)) {
            return map.get(vVar.m);
        }
        if (!map2.containsKey(vVar.x)) {
            return null;
        }
        v vVar2 = map2.get(vVar.x);
        pVar.c.add(String.valueOf(vVar2.r));
        return vVar2;
    }

    public static boolean a(List<v> list, boolean z) {
        if (!(list == null || list.size() == 0)) {
            for (int size = list.size() - 1; size >= 0; size--) {
                v vVar = list.get(size);
                if (w.ADMIN_BOT_CONTROL == vVar.k) {
                    d dVar = (d) vVar;
                    String str = dVar.f3471a;
                    if ("bot_started".equals(str)) {
                        return true;
                    }
                    if ("bot_ended".equals(str)) {
                        return dVar.c;
                    }
                }
            }
        }
        return z;
    }

    public final void b(com.helpshift.j.a.b.a aVar, com.helpshift.j.a.b.a aVar2, boolean z, p pVar) {
        e eVar = aVar2.g;
        int i = f.f3511a[eVar.ordinal()];
        if (i == 4) {
            eVar = e.RESOLUTION_ACCEPTED;
        } else if (i == 5) {
            eVar = e.COMPLETED_ISSUE_CREATED;
            aVar.c = aVar2.c;
        }
        String str = aVar2.m;
        if (str != null) {
            aVar.m = str;
        }
        aVar.d = aVar2.d;
        aVar.c = aVar2.c;
        aVar.h = aVar2.h;
        aVar.f = aVar2.f;
        aVar.l = aVar2.l;
        aVar.k = aVar2.k;
        aVar.z = aVar2.z;
        aVar.A = aVar2.A;
        aVar.i = aVar2.i;
        aVar.g = eVar;
        a(aVar, z, aVar2.j, pVar);
    }

    public final void b(com.helpshift.j.a.b.a aVar, boolean z) {
        aVar.B = z;
        if (aVar.g == e.RESOLUTION_REJECTED) {
            n(aVar);
        }
    }

    public final boolean b(com.helpshift.j.a.b.a aVar) {
        if (!aVar.a() && aVar.p == com.helpshift.j.f.a.NONE && this.e.a("customerSatisfactionSurvey")) {
            return true;
        }
        return false;
    }

    public final void c(com.helpshift.j.a.b.a aVar) {
        this.f3502b.b(new m(this, aVar));
    }

    public void a(com.helpshift.j.a.b.a aVar, al alVar) {
        try {
            alVar.a(this.c, aVar);
            if (aVar.g == e.RESOLUTION_REJECTED) {
                a(aVar, e.WAITING_FOR_AGENT);
            }
        } catch (RootAPIException e2) {
            if (e2.c == com.helpshift.common.exception.b.CONVERSATION_ARCHIVED) {
                a(aVar, e.ARCHIVED);
            } else if (e2.c == com.helpshift.common.exception.b.USER_PRE_CONDITION_FAILED) {
                a(aVar, e.AUTHOR_MISMATCH);
            } else {
                throw e2;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
     arg types: [com.helpshift.j.a.b.a, com.helpshift.j.a.a.ap]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Collection<? extends com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Set<java.lang.Long>):void
      com.helpshift.j.a.b.a(java.util.List<com.helpshift.j.a.a.v>, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, long):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.e):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.f.a):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void
     arg types: [com.helpshift.j.a.b.a, com.helpshift.j.a.a.ap]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Collection<? extends com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Set<java.lang.Long>):void
      com.helpshift.j.a.b.a(java.util.List<com.helpshift.j.a.a.v>, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, long):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.e):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.f.a):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void */
    public final void a(com.helpshift.j.a.b.a aVar, String str, com.helpshift.j.a.a.j jVar, boolean z) {
        aa<String, Long> b2 = com.helpshift.common.g.b.b(this.f3501a);
        ap apVar = new ap(str, (String) b2.f4242a, ((Long) b2.f4243b).longValue(), "mobile", jVar, z);
        apVar.q = aVar.f3504b;
        apVar.a(true);
        a(aVar, (v) apVar);
        a(aVar, (al) apVar);
    }

    public void a(com.helpshift.j.a.b.a aVar, com.helpshift.j.a.a.ab abVar, boolean z) {
        try {
            abVar.a(this.c, aVar, z);
            if (aVar.g == e.RESOLUTION_REJECTED) {
                a(aVar, e.WAITING_FOR_AGENT);
            }
        } catch (RootAPIException e2) {
            if (e2.c == com.helpshift.common.exception.b.CONVERSATION_ARCHIVED) {
                a(aVar, e.ARCHIVED);
                return;
            }
            throw e2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.p):void
     arg types: [com.helpshift.j.a.b.a, int, java.util.ArrayList, ?[OBJECT, ARRAY]]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, com.helpshift.j.a.p):com.helpshift.j.a.a.v
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, int, java.lang.String, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, boolean, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String, com.helpshift.j.a.a.j, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, int, java.lang.String, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.p):void */
    public final void c(com.helpshift.j.a.b.a aVar, boolean z) {
        List<v> c2 = this.d.c(aVar.f3504b.longValue());
        ArrayList<com.helpshift.j.a.a.l> arrayList = new ArrayList<>();
        ArrayList<v> arrayList2 = new ArrayList<>();
        HashMap hashMap = new HashMap();
        ArrayList<p> arrayList3 = new ArrayList<>();
        Iterator<v> it = c2.iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            v next = it.next();
            next.a(this.f3502b, this.f3501a);
            if (next instanceof com.helpshift.j.a.a.l) {
                com.helpshift.j.a.a.l lVar = (com.helpshift.j.a.a.l) next;
                if (lVar.f3482b != 1) {
                    z2 = false;
                }
                if (z2) {
                    arrayList.add(lVar);
                }
            }
            if (!com.helpshift.common.k.a(next.s) && !next.v) {
                arrayList2.add(next);
            }
            if (next instanceof y) {
                hashMap.put(next.m, (y) next);
            }
            if (next instanceof p) {
                p pVar = (p) next;
                if (pVar.b()) {
                    arrayList3.add(pVar);
                }
            }
        }
        for (com.helpshift.j.a.a.l lVar2 : arrayList) {
            if (aVar.g != e.ARCHIVED && aVar.g != e.AUTHOR_MISMATCH) {
                try {
                    lVar2.a(this.c, aVar);
                    if (lVar2 instanceof com.helpshift.j.a.a.a) {
                        ArrayList arrayList4 = new ArrayList();
                        com.helpshift.j.a.a.a aVar2 = (com.helpshift.j.a.a.a) lVar2;
                        String str = aVar2.f3438a;
                        if (hashMap.containsKey(str)) {
                            y yVar = (y) hashMap.get(str);
                            yVar.a(this.f3501a);
                            arrayList4.add(yVar);
                        }
                        if (z) {
                            arrayList4.add(lVar2);
                            c(aVar, aVar2);
                            a(aVar, true, (List<v>) arrayList4, (p) null);
                        }
                    }
                } catch (RootAPIException e2) {
                    if (e2.c == com.helpshift.common.exception.b.CONVERSATION_ARCHIVED) {
                        a(aVar, e.ARCHIVED);
                    } else if (e2.c == com.helpshift.common.exception.b.USER_PRE_CONDITION_FAILED) {
                        a(aVar, e.AUTHOR_MISMATCH);
                    } else if (e2.c != com.helpshift.common.exception.b.NON_RETRIABLE) {
                        throw e2;
                    }
                }
            } else {
                return;
            }
        }
        HashMap hashMap2 = new HashMap();
        for (v vVar : arrayList2) {
            String str2 = vVar.s;
            List list = (List) hashMap2.get(str2);
            if (list == null) {
                list = new ArrayList();
            }
            list.add(vVar);
            hashMap2.put(str2, list);
        }
        for (String str3 : hashMap2.keySet()) {
            try {
                a(aVar, (List<v>) ((List) hashMap2.get(str3)));
            } catch (RootAPIException e3) {
                if (e3.c != com.helpshift.common.exception.b.NON_RETRIABLE) {
                    throw e3;
                }
            }
        }
        for (p a2 : arrayList3) {
            a2.a(aVar, this.c);
        }
    }

    public final void d(com.helpshift.j.a.b.a aVar) {
        List<v> c2 = this.d.c(aVar.f3504b.longValue());
        HashSet hashSet = new HashSet();
        for (v next : c2) {
            if (next.u != 1) {
                switch (f.f3512b[next.k.ordinal()]) {
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                        hashSet.add(next.r);
                        continue;
                }
            }
        }
        if (hashSet.size() != 0) {
            a(aVar, (Set<Long>) hashSet);
        }
    }

    private void a(com.helpshift.j.a.b.a aVar, Set<Long> set) {
        String str = (String) com.helpshift.common.g.b.b(this.f3501a).f4242a;
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        Iterator<E> it = aVar.j.iterator();
        while (it.hasNext()) {
            v vVar = (v) it.next();
            if (vVar.r != null) {
                hashMap.put(vVar.r, vVar);
            }
        }
        for (Long l : set) {
            v vVar2 = (v) hashMap.get(l);
            if (vVar2 != null) {
                vVar2.s = str;
                vVar2.u = 1;
                vVar2.t = aVar.m;
                arrayList.add(vVar2);
            }
        }
        if (!com.helpshift.common.j.a(arrayList)) {
            this.d.c(arrayList);
            a(aVar, (List<v>) arrayList);
        }
    }

    private void a(com.helpshift.j.a.b.a aVar, List<v> list) {
        if (!com.helpshift.common.j.a(list)) {
            String str = list.get(0).s;
            String str2 = list.get(0).t;
            HashMap<String, String> a2 = o.a(this.c);
            a2.put("read_at", str);
            a2.put("mc", str2);
            a2.put("md_state", "read");
            try {
                new com.helpshift.common.c.b.j(new g(new com.helpshift.common.c.b.s(new com.helpshift.common.c.b.b(new com.helpshift.common.c.b.r(o(aVar), this.f3502b, this.f3501a)), this.f3501a))).a(new i(a2));
            } catch (RootAPIException e2) {
                if (e2.c == com.helpshift.common.exception.b.INVALID_AUTH_TOKEN || e2.c == com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED) {
                    this.f3502b.j.a(this.c, e2.c);
                } else if (e2.c != com.helpshift.common.exception.b.NON_RETRIABLE) {
                    throw e2;
                }
            }
            for (v vVar : list) {
                vVar.v = true;
            }
            this.d.c(list);
        }
    }

    public final void a(com.helpshift.j.a.b.a aVar, com.helpshift.j.d.d dVar, String str) {
        com.helpshift.j.a.b.a aVar2 = aVar;
        com.helpshift.j.d.d dVar2 = dVar;
        String str2 = str;
        aa<String, Long> b2 = com.helpshift.common.g.b.b(this.f3501a);
        com.helpshift.j.a.a.ab abVar = new com.helpshift.j.a.a.ab(null, (String) b2.f4242a, ((Long) b2.f4243b).longValue(), "mobile", null, null, null, null, 0, false);
        abVar.d = dVar2.f3590a;
        abVar.g = dVar2.d;
        abVar.d(str2);
        abVar.a(m(aVar));
        abVar.q = aVar2.f3504b;
        a(aVar2, abVar);
        if (str2 != null) {
            Iterator<E> it = aVar2.j.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                v vVar = (v) it.next();
                if (vVar.m != null && vVar.m.equals(str2) && vVar.k == w.REQUESTED_SCREENSHOT) {
                    com.helpshift.j.a.a.aa aaVar = (com.helpshift.j.a.a.aa) vVar;
                    ab abVar2 = this.f3501a;
                    aaVar.f3445a = true;
                    abVar2.f().a(aaVar);
                    aaVar.k();
                    break;
                }
            }
        }
        a(aVar2, abVar, !dVar2.e);
    }

    public final void e(com.helpshift.j.a.b.a aVar) {
        String str = "/issues/" + aVar.c + "/customer-survey/";
        HashMap<String, String> a2 = o.a(this.c);
        a2.put("rating", String.valueOf(aVar.q));
        a2.put("feedback", aVar.r);
        try {
            new com.helpshift.common.c.b.j(new g(new com.helpshift.common.c.b.s(new com.helpshift.common.c.b.k(new q(str, this.f3502b, this.f3501a), this.f3501a, new com.helpshift.common.c.a.c(), str, aVar.c), this.f3501a))).a(new i(a2));
            com.helpshift.j.f.a aVar2 = com.helpshift.j.f.a.SUBMITTED_SYNCED;
            if (aVar2 != null) {
                a(aVar, aVar2);
            }
        } catch (RootAPIException e2) {
            if (e2.c == com.helpshift.common.exception.b.INVALID_AUTH_TOKEN || e2.c == com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED) {
                this.f3502b.j.a(this.c, e2.c);
            } else if (e2.c == com.helpshift.common.exception.b.NON_RETRIABLE) {
                com.helpshift.j.f.a aVar3 = com.helpshift.j.f.a.SUBMITTED_SYNCED;
            }
            throw e2;
        } catch (Throwable th) {
            if (0 != 0) {
                a(aVar, (com.helpshift.j.f.a) null);
            }
            throw th;
        }
    }

    public void a(com.helpshift.j.a.b.a aVar, com.helpshift.j.f.a aVar2) {
        if (aVar.p != aVar2) {
            com.helpshift.util.n.a("Helpshift_ConvManager", "Update CSAT state : Conversation : " + aVar.c + ", state : " + aVar2.toString(), (Throwable) null, (com.helpshift.p.b.a[]) null);
        }
        aVar.p = aVar2;
        this.d.d(aVar);
    }

    public final void a(com.helpshift.j.a.b.a aVar, boolean z, boolean z2) {
        aVar.s = true;
        this.d.d(aVar);
    }

    public final void b(com.helpshift.j.a.b.a aVar, boolean z, boolean z2) {
        if (aVar.n != z) {
            aVar.n = z;
            this.d.d(aVar);
        }
    }

    public final void a(com.helpshift.j.a.b.a aVar, long j) {
        aVar.u = j;
        this.d.a(aVar.f3504b, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(java.util.List<com.helpshift.j.a.a.v>, boolean):boolean
     arg types: [com.helpshift.common.g.d<com.helpshift.j.a.a.v>, int]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Collection<? extends com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Set<java.lang.Long>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, long):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.e):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.f.a):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void
      com.helpshift.j.a.b.a(java.util.List<com.helpshift.j.a.a.v>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, boolean):void
     arg types: [com.helpshift.j.a.a.v, int]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Collection<? extends com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Set<java.lang.Long>):void
      com.helpshift.j.a.b.a(java.util.List<com.helpshift.j.a.a.v>, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, long):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.e):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.f.a):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, boolean):void */
    public final void d(com.helpshift.j.a.b.a aVar, boolean z) {
        com.helpshift.j.c.b(aVar.j);
        if (z) {
            aVar.y = a((List<v>) aVar.j, false);
            Iterator<E> it = aVar.j.iterator();
            while (it.hasNext()) {
                v vVar = (v) it.next();
                vVar.a(this.f3502b, this.f3501a);
                vVar.w = aVar.l;
                if (vVar instanceof com.helpshift.j.a.a.e) {
                    ((com.helpshift.j.a.a.e) vVar).a(this.f3501a);
                }
                a(vVar, m(aVar));
                d(aVar, vVar);
            }
            if (aVar.j.size() > 0) {
                v vVar2 = (v) aVar.j.get(aVar.j.size() - 1);
                if (vVar2.k == w.USER_RESP_FOR_OPTION_INPUT || vVar2.k == w.USER_RESP_FOR_TEXT_INPUT) {
                    v h = h(aVar);
                    if (aVar.y && h == null) {
                        ((al) vVar2).a(true);
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        Iterator<E> it2 = aVar.j.iterator();
        while (it2.hasNext()) {
            v vVar3 = (v) it2.next();
            vVar3.a(this.f3502b, this.f3501a);
            vVar3.w = aVar.l;
            if (vVar3 instanceof com.helpshift.j.a.a.e) {
                ((com.helpshift.j.a.a.e) vVar3).a(this.f3501a);
            }
            a(vVar3, false);
        }
    }

    public final void a(com.helpshift.j.a.b.a aVar, List<v> list, boolean z) {
        for (v next : list) {
            next.a(this.f3502b, this.f3501a);
            next.w = aVar.l;
            a(next, z);
            d(aVar, next);
        }
    }

    public static boolean f(com.helpshift.j.a.b.a aVar) {
        return !com.helpshift.common.k.a(aVar.c) || !com.helpshift.common.k.a(aVar.d);
    }

    public final boolean g(com.helpshift.j.a.b.a aVar) {
        int i;
        Integer num;
        if (!this.e.a("conversationalIssueFiling") && aVar.a() && com.helpshift.common.k.a(aVar.d)) {
            return false;
        }
        if (aVar.a() && com.helpshift.j.c.a(aVar.g)) {
            return true;
        }
        e eVar = aVar.g;
        if (aVar.x) {
            return false;
        }
        if (!com.helpshift.j.c.a(aVar.g) && eVar != e.RESOLUTION_REQUESTED) {
            if (eVar == e.RESOLUTION_ACCEPTED || eVar == e.RESOLUTION_REJECTED || eVar == e.ARCHIVED) {
                return !aVar.s;
            }
            if (eVar != e.REJECTED || aVar.s) {
                return false;
            }
            if (aVar.a()) {
                a aVar2 = this.d;
                Long l = aVar.f3504b;
                if (l == null || (num = com.helpshift.j.c.a(aVar2, Collections.singletonList(l)).get(l)) == null) {
                    i = 0;
                } else {
                    i = num.intValue();
                }
                if (i > 0) {
                    return true;
                }
                return false;
            }
        }
        return true;
    }

    public static v h(com.helpshift.j.a.b.a aVar) {
        boolean z = true;
        for (int size = aVar.j.size() - 1; size >= 0; size--) {
            v vVar = (v) aVar.j.get(size);
            if (vVar.k == w.ADMIN_BOT_CONTROL) {
                return null;
            }
            if (vVar.k == w.ADMIN_TEXT_WITH_TEXT_INPUT || vVar.k == w.ADMIN_TEXT_WITH_OPTION_INPUT || vVar.k == w.FAQ_LIST_WITH_OPTION_INPUT || vVar.k == w.OPTION_INPUT) {
                int i = size + 1;
                while (true) {
                    if (i >= aVar.j.size()) {
                        z = false;
                        break;
                    }
                    v vVar2 = (v) aVar.j.get(i);
                    if ((vVar2.k == w.USER_RESP_FOR_OPTION_INPUT || vVar2.k == w.USER_RESP_FOR_TEXT_INPUT) && vVar.m.equals(((al) vVar2).d())) {
                        break;
                    }
                    i++;
                }
                if (z) {
                    return null;
                }
                return vVar;
            }
        }
        return null;
    }

    public static boolean a(Collection<? extends v> collection) {
        if (!(collection == null || collection.size() == 0)) {
            ArrayList arrayList = new ArrayList(collection);
            boolean z = false;
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                v vVar = (v) arrayList.get(size);
                if (w.ADMIN_BOT_CONTROL == vVar.k) {
                    String str = ((d) vVar).f3471a;
                    if ("bot_ended".equals(str)) {
                        return z;
                    }
                    if ("bot_started".equals(str)) {
                        z = true;
                    }
                }
            }
        }
        return false;
    }

    public final void e(com.helpshift.j.a.b.a aVar, boolean z) {
        Iterator<E> it = aVar.j.iterator();
        while (it.hasNext()) {
            b((v) it.next(), z);
        }
    }

    public static boolean i(com.helpshift.j.a.b.a aVar) {
        if (!aVar.a()) {
            return true;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<E> it = aVar.j.iterator();
        while (it.hasNext()) {
            v vVar = (v) it.next();
            if (vVar.a()) {
                if (vVar instanceof al) {
                    return true;
                }
                arrayList.add(vVar);
                if (arrayList.size() > 3) {
                    return true;
                }
            }
        }
        return false;
    }

    public final void a(com.helpshift.j.a.b.a aVar, p pVar) {
        for (int i = 0; i < pVar.c.size(); i++) {
            this.f3501a.t().b(o(aVar), pVar.c.remove(i));
        }
        pVar.f3529a.clear();
        pVar.f3530b.clear();
    }

    public final void j(com.helpshift.j.a.b.a aVar) {
        this.f3502b.c.a(com.helpshift.b.b.CONVERSATION_POSTED, aVar.c);
    }

    public final void k(com.helpshift.j.a.b.a aVar) {
        List<v> c2 = this.d.c(aVar.f3504b.longValue());
        ArrayList arrayList = new ArrayList();
        for (v next : c2) {
            if (next instanceof com.helpshift.j.a.a.ab) {
                com.helpshift.j.a.a.ab abVar = (com.helpshift.j.a.a.ab) next;
                try {
                    if (com.helpshift.common.g.a.c(abVar.b())) {
                        abVar.g = null;
                        arrayList.add(abVar);
                    }
                } catch (Exception e2) {
                    com.helpshift.util.n.c("Helpshift_ConvManager", "Exception while deleting ScreenshotMessageDM file", e2);
                }
            }
        }
        this.d.c(arrayList);
    }

    public final int l(com.helpshift.j.a.b.a aVar) {
        int i = 0;
        if (!g(aVar)) {
            return 0;
        }
        List<v> c2 = this.d.c(aVar.f3504b.longValue());
        if (c2 != null) {
            for (v next : c2) {
                if (next.a() && next.u != 1) {
                    switch (f.f3512b[next.k.ordinal()]) {
                        case 3:
                            if (next instanceof com.helpshift.j.a.a.j) {
                                if (((com.helpshift.j.a.a.j) next).f3480a) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        case 2:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                            i++;
                            break;
                    }
                }
            }
        }
        return aVar.n ? i + 1 : i;
    }

    public final void b(com.helpshift.j.a.b.a aVar, e eVar) {
        if (com.helpshift.j.c.a(eVar) && (aVar.g == e.RESOLUTION_REQUESTED || aVar.g == e.RESOLUTION_ACCEPTED || aVar.g == e.RESOLUTION_REJECTED)) {
            b(aVar, true, true);
        } else if (com.helpshift.j.c.a(aVar.g)) {
            b(aVar, false, true);
        }
    }

    public static boolean m(com.helpshift.j.a.b.a aVar) {
        if (aVar.y) {
            return false;
        }
        if (com.helpshift.j.c.a(aVar.g)) {
            return true;
        }
        if (aVar.g == e.RESOLUTION_REQUESTED || aVar.g == e.RESOLUTION_ACCEPTED || aVar.g == e.ARCHIVED || aVar.g == e.REJECTED || aVar.g != e.RESOLUTION_REJECTED) {
            return false;
        }
        return aVar.B;
    }

    public final void f(com.helpshift.j.a.b.a aVar, boolean z) {
        aVar.D = true;
        this.d.d(aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
     arg types: [com.helpshift.j.a.b.a, com.helpshift.j.a.a.al]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Collection<? extends com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Set<java.lang.Long>):void
      com.helpshift.j.a.b.a(java.util.List<com.helpshift.j.a.a.v>, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, long):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.e):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.f.a):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void */
    public final void a(com.helpshift.j.a.b.a aVar, String str) {
        com.helpshift.util.n.a("Helpshift_ConvManager", "Adding first user message to DB and UI.", (Throwable) null, (com.helpshift.p.b.a[]) null);
        aa<String, Long> b2 = com.helpshift.common.g.b.b(this.f3501a);
        al alVar = new al(str, (String) b2.f4242a, ((Long) b2.f4243b).longValue(), "mobile");
        alVar.a(this.f3502b, this.f3501a);
        alVar.q = aVar.f3504b;
        alVar.a(am.SENDING);
        a(aVar, (v) alVar);
    }
}
