package com.helpshift.j.a;

import com.helpshift.common.c.l;
import com.helpshift.j.a.a.i;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.w;
import com.helpshift.j.a.b.a;
import com.helpshift.l.c;
import com.helpshift.l.g;
import java.util.Iterator;
import java.util.List;

/* compiled from: ConversationManager */
class m extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3525a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f3526b;

    m(b bVar, a aVar) {
        this.f3526b = bVar;
        this.f3525a = aVar;
    }

    public final void a() {
        b bVar = this.f3526b;
        List<v> a2 = bVar.d.a(this.f3525a.f3504b.longValue(), w.ADMIN_TEXT_WITH_OPTION_INPUT);
        Iterator<v> it = a2.iterator();
        while (it.hasNext()) {
            ((i) it.next()).f3479a.e.clear();
        }
        bVar.d.c(a2);
        b bVar2 = this.f3526b;
        a aVar = this.f3525a;
        if (!aVar.o) {
            c cVar = bVar2.f3502b.e;
            if (cVar.f3738b != null) {
                cVar.f3737a.c(new g(cVar));
            }
            aVar.o = true;
            bVar2.d.d(aVar);
        }
    }
}
