package com.helpshift.j.a.a;

import com.helpshift.common.e.a.j;
import com.helpshift.common.g.b;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/* compiled from: UserResponseMessageForTextInputDM */
public class ap extends al {

    /* renamed from: a  reason: collision with root package name */
    public final boolean f3464a;

    /* renamed from: b  reason: collision with root package name */
    public int f3465b;
    public String d;
    public boolean e;
    public long f;
    public String g;
    private String h;

    public ap(String str, String str2, long j, String str3, int i, String str4, boolean z, String str5, boolean z2) {
        super(str, str2, j, str3, w.USER_RESP_FOR_TEXT_INPUT);
        this.f3465b = i;
        this.d = str4;
        this.e = z;
        this.h = str5;
        this.f3464a = z2;
    }

    public ap(String str, String str2, long j, String str3, j jVar, boolean z) {
        super(str, str2, j, str3, w.USER_RESP_FOR_TEXT_INPUT);
        this.f3465b = jVar.f3481b.f;
        this.d = jVar.f3481b.f3439a;
        this.e = z;
        this.h = jVar.m;
        this.f3464a = jVar.f3480a;
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof ap) {
            ap apVar = (ap) vVar;
            this.f3465b = apVar.f3465b;
            this.d = apVar.d;
            this.e = apVar.e;
            this.h = apVar.h;
            this.f = apVar.f;
            this.g = apVar.g;
        }
    }

    /* access modifiers changed from: protected */
    public final String c() {
        int i = this.f3465b;
        if (i == 1) {
            return this.f3464a ? "rsp_empty_msg_with_txt_input" : "rsp_txt_msg_with_txt_input";
        }
        if (i == 2) {
            return "rsp_txt_msg_with_email_input";
        }
        if (i == 3) {
            return "rsp_txt_msg_with_numeric_input";
        }
        if (i != 4) {
            return super.c();
        }
        return "rsp_txt_msg_with_dt_input";
    }

    public final String d() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public final Map<String, String> b() throws ParseException {
        HashMap hashMap = new HashMap();
        hashMap.put("chatbot_info", this.d);
        hashMap.put("skipped", String.valueOf(this.e));
        if (this.f3465b == 4 && !this.e) {
            Date a2 = b.a("EEEE, MMMM dd, yyyy", this.z.f.c()).a(this.n.trim());
            HashMap hashMap2 = new HashMap();
            this.f = a2.getTime();
            this.g = this.A.d().u();
            hashMap2.put("dt", Long.valueOf(this.f));
            hashMap2.put("timezone", this.g);
            hashMap.put("message_meta", this.A.p().a(hashMap2));
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final al a(j jVar) {
        return this.A.l().b(jVar.f3323b);
    }
}
