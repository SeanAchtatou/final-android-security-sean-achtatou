package com.helpshift.j.a;

import com.helpshift.common.c.l;
import com.helpshift.p.b.a;
import com.helpshift.util.n;

/* compiled from: LiveUpdateDM */
class s extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f3541a;

    s(r rVar) {
        this.f3541a = rVar;
    }

    public final void a() {
        if (this.f3541a.d == null) {
            return;
        }
        if (this.f3541a.f) {
            this.f3541a.e = true;
            return;
        }
        try {
            n.a("Helpshift_LiveUpdateDM", "Disconnecting web-socket", (Throwable) null, (a[]) null);
            this.f3541a.d.f3307a.a(1000, null, 10000);
        } catch (Exception e) {
            n.c("Helpshift_LiveUpdateDM", "Exception in disconnecting web-socket", e);
        }
        this.f3541a.d = null;
    }
}
