package com.helpshift.j.a;

import com.helpshift.common.c.l;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.j.a.a.a;
import com.helpshift.j.a.a.y;
import com.helpshift.j.d.e;

/* compiled from: ConversationManager */
public class n extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3527a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ com.helpshift.j.a.b.a f3528b;
    final /* synthetic */ y c;
    final /* synthetic */ b d;

    public n(b bVar, a aVar, com.helpshift.j.a.b.a aVar2, y yVar) {
        this.d = bVar;
        this.f3527a = aVar;
        this.f3528b = aVar2;
        this.c = yVar;
    }

    public final void a() {
        try {
            this.f3527a.a(this.d.c, this.f3528b);
            this.c.a(this.d.f3501a);
        } catch (RootAPIException e) {
            if (e.c == b.CONVERSATION_ARCHIVED) {
                this.d.a(this.f3528b, e.ARCHIVED);
            } else {
                this.c.a(true);
                throw e;
            }
        }
    }
}
