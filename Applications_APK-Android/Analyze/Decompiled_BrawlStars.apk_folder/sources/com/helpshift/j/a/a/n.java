package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.k;
import com.helpshift.j.a.o;
import java.util.HashMap;

/* compiled from: ConfirmationRejectedMessageDM */
public class n extends l {
    public final boolean a() {
        return true;
    }

    public n(String str, String str2, long j, String str3, int i) {
        super(str, str2, j, str3, false, w.CONFIRMATION_REJECTED, i);
    }

    public final void a(c cVar, o oVar) {
        if (!k.a(oVar.b())) {
            HashMap<String, String> a2 = com.helpshift.common.c.b.o.a(cVar);
            a2.put("body", this.n);
            a2.put("type", "ncr");
            a2.put("refers", "");
            try {
                n f = this.A.l().f(a(b(oVar), a2).f3323b);
                a(f);
                this.m = f.m;
                this.o = f.o;
                k();
                this.A.f().a(this);
            } catch (RootAPIException e) {
                if (e.c == b.INVALID_AUTH_TOKEN || e.c == b.AUTH_TOKEN_NOT_PROVIDED) {
                    this.z.j.a(cVar, e.c);
                }
                throw e;
            }
        } else {
            throw new UnsupportedOperationException("ConfirmationRejectedMessageDM send called with conversation in pre issue mode.");
        }
    }
}
