package com.helpshift.j.a.a;

import com.helpshift.common.g.b;
import java.util.Date;
import java.util.Locale;

/* compiled from: SystemDateMessageDM */
public class ad extends af {

    /* renamed from: a  reason: collision with root package name */
    public boolean f3450a;

    public ad(String str, long j, boolean z) {
        super("", str, j, w.SYSTEM_DATE);
        this.f3450a = z;
    }

    public final String b() {
        Locale c = this.z.f.c();
        return b.a("EEEE, MMMM dd, yyyy", c).a(new Date(this.C));
    }

    public boolean equals(Object obj) {
        if (obj instanceof ad) {
            return ((ad) obj).B.equals(this.B);
        }
        return false;
    }
}
