package com.helpshift.j.a;

import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.v;
import com.helpshift.j.i.bn;
import java.util.List;
import java.util.Map;

/* compiled from: ConversationalRenderer */
public interface q {
    String a();

    void a(int i);

    void a(int i, int i2);

    void a(b bVar);

    void a(String str);

    void a(String str, String str2);

    void a(List<v> list);

    void a(List<bn> list, String str, boolean z, String str2);

    void a(Map<String, Boolean> map);

    void a(boolean z);

    void b();

    void b(int i);

    void b(int i, int i2);

    void b(String str);

    void b(String str, String str2);

    void b(List<bn> list);

    void c();

    void d();

    void e();

    void f();

    void g();

    void h();

    void i();

    void j();

    void k();

    void l();

    void m();

    void n();
}
