package com.helpshift.j.a.a.a;

import java.util.List;

/* compiled from: OptionInput */
public class b extends a {
    public final List<a> e;
    public final C0138b f;

    public b(String str, boolean z, String str2, String str3, List<a> list, C0138b bVar) {
        super(str, z, str2, str3);
        this.e = list;
        this.f = bVar;
    }

    /* compiled from: OptionInput */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public final String f3441a;

        /* renamed from: b  reason: collision with root package name */
        public final String f3442b;

        public a(String str, String str2) {
            this.f3441a = str;
            this.f3442b = str2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (!aVar.f3441a.equals(this.f3441a) || !aVar.f3442b.equals(this.f3442b)) {
                return false;
            }
            return true;
        }
    }

    /* renamed from: com.helpshift.j.a.a.a.b$b  reason: collision with other inner class name */
    /* compiled from: OptionInput */
    public enum C0138b {
        PILL("pill"),
        PICKER("picker");
        
        private final String c;

        private C0138b(String str) {
            this.c = str;
        }

        public final String toString() {
            return this.c;
        }

        public static C0138b a(String str, int i) {
            if ("pill".equals(str)) {
                return PILL;
            }
            if ("picker".equals(str)) {
                return PICKER;
            }
            if (i <= 5) {
                return PILL;
            }
            return PICKER;
        }
    }
}
