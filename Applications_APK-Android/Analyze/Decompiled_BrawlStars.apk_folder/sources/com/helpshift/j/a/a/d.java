package com.helpshift.j.a.a;

/* compiled from: AdminBotControlMessageDM */
public class d extends h {

    /* renamed from: a  reason: collision with root package name */
    public String f3471a;

    /* renamed from: b  reason: collision with root package name */
    public String f3472b;
    public boolean c;

    public final boolean a() {
        return false;
    }

    public d(String str, String str2, String str3, long j, String str4, String str5, String str6) {
        super(str, str2, str3, j, str4, w.ADMIN_BOT_CONTROL);
        this.f3471a = str5;
        this.f3472b = str6;
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof d) {
            d dVar = (d) vVar;
            this.f3471a = dVar.f3471a;
            this.f3472b = dVar.f3472b;
        }
    }
}
