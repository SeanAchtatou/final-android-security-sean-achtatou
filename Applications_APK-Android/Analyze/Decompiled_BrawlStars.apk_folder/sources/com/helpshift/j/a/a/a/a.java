package com.helpshift.j.a.a.a;

import com.helpshift.common.k;

/* compiled from: Input */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public final String f3439a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3440b;
    public final String c;
    public final String d;

    public a(String str, boolean z, String str2, String str3) {
        this.f3439a = str;
        this.f3440b = z;
        this.c = str2;
        this.d = str3;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        if (aVar.f3440b != this.f3440b || !k.a(aVar.c, this.c) || !k.a(aVar.d, this.d) || !k.a(aVar.f3439a, this.f3439a)) {
            return false;
        }
        return true;
    }
}
