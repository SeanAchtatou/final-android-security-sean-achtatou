package com.helpshift.j.a;

import com.helpshift.common.c.j;
import com.helpshift.common.c.l;
import com.helpshift.common.e.ab;
import com.helpshift.common.e.y;
import com.helpshift.common.k;
import com.helpshift.j.d.f;
import com.helpshift.j.d.g;
import com.helpshift.j.d.h;
import com.helpshift.util.n;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: LiveUpdateDM */
public class r implements com.helpshift.common.e.a.a.b {

    /* renamed from: a  reason: collision with root package name */
    final long f3531a = TimeUnit.SECONDS.toMillis(3);

    /* renamed from: b  reason: collision with root package name */
    final String f3532b;
    AtomicInteger c;
    com.helpshift.common.e.a.a.a d;
    boolean e;
    boolean f;
    boolean g;
    e h;
    j i;
    ab j;
    AtomicInteger k;
    boolean l;
    l m = new s(this);
    boolean n;
    private final String o = "[110]";
    private final String p = "hs-sdk-ver";
    private String q;
    private l r = new t(this);

    /* compiled from: LiveUpdateDM */
    interface e {
        void a(boolean z);
    }

    public r(j jVar, ab abVar) {
        this.i = jVar;
        this.j = abVar;
        this.c = new AtomicInteger(-1);
        this.k = new AtomicInteger(-1);
        y d2 = abVar.d();
        this.f3532b = d2.b().toLowerCase() + "-" + d2.a();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(e eVar, String str) {
        if (this.h == null) {
            this.h = eVar;
            this.q = str;
            this.g = false;
            this.e = false;
            this.i.b(new a(this.c.incrementAndGet()));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b() {
        if (this.h != null) {
            this.l = false;
            d();
            this.k.incrementAndGet();
            this.c.incrementAndGet();
            this.h = null;
        }
        this.i.b(this.m);
    }

    public final void a(String str) {
        this.i.b(new b(str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0052  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(java.lang.String r5) {
        /*
            r4 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Error in web-socket connection: "
            r0.append(r1)
            r0.append(r5)
            java.lang.String r0 = r0.toString()
            r1 = 0
            java.lang.String r2 = "Helpshift_LiveUpdateDM"
            com.helpshift.util.n.a(r2, r0, r1, r1)
            r0 = 0
            r4.f = r0
            com.helpshift.j.a.r$e r0 = r4.h
            if (r0 == 0) goto L_0x0055
            java.lang.String r0 = "The status line is: "
            java.lang.String[] r5 = r5.split(r0)
            int r0 = r5.length
            r1 = 403(0x193, float:5.65E-43)
            r2 = 2
            if (r2 != r0) goto L_0x0043
            r0 = 1
            r5 = r5[r0]
            java.lang.String r3 = " +"
            java.lang.String[] r5 = r5.split(r3)
            int r3 = r5.length
            if (r3 < r2) goto L_0x0043
            r5 = r5[r0]
            java.lang.String r0 = "403"
            boolean r5 = r0.equals(r5)
            if (r5 == 0) goto L_0x0043
            r5 = 403(0x193, float:5.65E-43)
            goto L_0x0044
        L_0x0043:
            r5 = -1
        L_0x0044:
            if (r5 != r1) goto L_0x0052
            boolean r5 = r4.g
            if (r5 != 0) goto L_0x0055
            com.helpshift.common.c.j r5 = r4.i
            com.helpshift.common.c.l r0 = r4.r
            r5.b(r0)
            goto L_0x0055
        L_0x0052:
            r4.c()
        L_0x0055:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.a.r.b(java.lang.String):void");
    }

    /* access modifiers changed from: package-private */
    public final String a(com.helpshift.g.b.a aVar) {
        String c2 = this.j.c();
        String[] split = this.j.b().split("\\.");
        String str = "";
        String str2 = split.length == 3 ? split[0] : str;
        try {
            str = URLEncoder.encode(aVar.f3407a, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            n.c("Helpshift_LiveUpdateDM", "Exception in encoding authToken", e2);
        }
        if (k.a(str) || k.a(aVar.f3408b)) {
            return null;
        }
        return aVar.f3408b + "/subscribe/websocket/?origin_v3=" + str + "&platform_id=" + c2 + "&domain=" + str2;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.i.a(new a(this.c.incrementAndGet()), TimeUnit.SECONDS.toMillis(10));
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        e eVar = this.h;
        if (eVar != null) {
            eVar.a(this.l);
        }
    }

    /* compiled from: LiveUpdateDM */
    class a extends l {

        /* renamed from: b  reason: collision with root package name */
        private final int f3534b;

        a(int i) {
            this.f3534b = i;
        }

        /* JADX WARN: Failed to insert an additional move for type inference into block B:89:0x01d3 */
        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Failed to insert an additional move for type inference into block B:84:0x01aa */
        /* JADX WARN: Type inference failed for: r1v3, types: [java.lang.String] */
        /* JADX WARN: Type inference failed for: r1v4 */
        /* JADX WARN: Type inference failed for: r1v13 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:102:0x023e A[Catch:{ Exception -> 0x0293 }, LOOP:1: B:100:0x0238->B:102:0x023e, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:106:0x025a A[Catch:{ Exception -> 0x0293 }, LOOP:2: B:104:0x0254->B:106:0x025a, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x015b  */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x015e A[SYNTHETIC, Splitter:B:74:0x015e] */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x01b5 A[Catch:{ Exception -> 0x0293 }] */
        /* JADX WARNING: Removed duplicated region for block: B:90:0x01d5 A[Catch:{ Exception -> 0x0293 }] */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x01ec A[Catch:{ Exception -> 0x0293 }] */
        /* JADX WARNING: Removed duplicated region for block: B:94:0x0202 A[Catch:{ Exception -> 0x0293 }] */
        /* JADX WARNING: Removed duplicated region for block: B:98:0x0222 A[Catch:{ Exception -> 0x0293 }, LOOP:0: B:96:0x021c->B:98:0x0222, LOOP_END] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a() {
            /*
                r21 = this;
                r1 = r21
                com.helpshift.j.a.r r0 = com.helpshift.j.a.r.this
                com.helpshift.j.a.r$e r0 = r0.h
                if (r0 == 0) goto L_0x02e7
                int r0 = r1.f3534b
                com.helpshift.j.a.r r2 = com.helpshift.j.a.r.this
                java.util.concurrent.atomic.AtomicInteger r2 = r2.c
                int r2 = r2.get()
                if (r0 != r2) goto L_0x02e7
                com.helpshift.j.a.r r0 = com.helpshift.j.a.r.this
                boolean r0 = r0.n
                if (r0 != 0) goto L_0x02e7
                com.helpshift.j.a.r r0 = com.helpshift.j.a.r.this
                boolean r0 = r0.f
                if (r0 == 0) goto L_0x0022
                goto L_0x02e7
            L_0x0022:
                com.helpshift.j.a.r r0 = com.helpshift.j.a.r.this
                com.helpshift.common.c.j r0 = r0.i
                com.helpshift.g.a.a r0 = r0.g()
                com.helpshift.g.b.a r2 = r0.f3405a
                java.lang.String r3 = "websocket_auth_data"
                if (r2 != 0) goto L_0x003e
                com.helpshift.common.e.aa r2 = r0.f3406b
                java.lang.Object r2 = r2.b(r3)
                boolean r4 = r2 instanceof com.helpshift.g.b.a
                if (r4 == 0) goto L_0x003e
                com.helpshift.g.b.a r2 = (com.helpshift.g.b.a) r2
                r0.f3405a = r2
            L_0x003e:
                com.helpshift.g.b.a r2 = r0.f3405a
                if (r2 != 0) goto L_0x004f
                com.helpshift.g.b.a r2 = r0.a()
                r0.f3405a = r2
                com.helpshift.common.e.aa r2 = r0.f3406b
                com.helpshift.g.b.a r4 = r0.f3405a
                r2.a(r3, r4)
            L_0x004f:
                com.helpshift.g.b.a r0 = r0.f3405a
                if (r0 != 0) goto L_0x0059
                com.helpshift.j.a.r r0 = com.helpshift.j.a.r.this
                r0.c()
                return
            L_0x0059:
                java.lang.String r2 = "Helpshift_LiveUpdateDM"
                r3 = 0
                java.lang.String r4 = "Connecting web-socket"
                com.helpshift.util.n.a(r2, r4, r3, r3)
                com.helpshift.j.a.r r4 = com.helpshift.j.a.r.this     // Catch:{ Exception -> 0x02d7 }
                com.helpshift.common.e.a.a.a$a r5 = new com.helpshift.common.e.a.a.a$a     // Catch:{ Exception -> 0x02d7 }
                com.helpshift.j.a.r r6 = com.helpshift.j.a.r.this     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r0 = r6.a(r0)     // Catch:{ Exception -> 0x02d7 }
                r5.<init>(r0)     // Catch:{ Exception -> 0x02d7 }
                java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ Exception -> 0x02d7 }
                r6 = 60
                long r6 = r0.toMillis(r6)     // Catch:{ Exception -> 0x02d7 }
                int r0 = (int) r6     // Catch:{ Exception -> 0x02d7 }
                r5.f3310b = r0     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r0 = "permessage-deflate"
                com.helpshift.common.e.a.a.a$a r0 = r5.a(r0)     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r5 = "client_no_context_takeover"
                com.helpshift.common.e.a.a.a$a r0 = r0.a(r5)     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r5 = "server_no_context_takeover"
                com.helpshift.common.e.a.a.a$a r0 = r0.a(r5)     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r5 = "dirigent-pubsub-v1"
                java.util.List<java.lang.String> r6 = r0.e     // Catch:{ Exception -> 0x02d7 }
                r6.add(r5)     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r5 = "hs-sdk-ver"
                com.helpshift.j.a.r r6 = com.helpshift.j.a.r.this     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r6 = r6.f3532b     // Catch:{ Exception -> 0x02d7 }
                if (r6 == 0) goto L_0x00a5
                boolean r7 = com.helpshift.common.k.a(r5)     // Catch:{ Exception -> 0x02d7 }
                if (r7 != 0) goto L_0x00a5
                java.util.Map<java.lang.String, java.lang.String> r7 = r0.f     // Catch:{ Exception -> 0x02d7 }
                r7.put(r5, r6)     // Catch:{ Exception -> 0x02d7 }
            L_0x00a5:
                com.helpshift.j.a.r r5 = com.helpshift.j.a.r.this     // Catch:{ Exception -> 0x02d7 }
                r0.g = r5     // Catch:{ Exception -> 0x02d7 }
                com.helpshift.websockets.an r7 = new com.helpshift.websockets.an     // Catch:{ Exception -> 0x02d7 }
                r7.<init>()     // Catch:{ Exception -> 0x02d7 }
                int r5 = r0.f3310b     // Catch:{ Exception -> 0x02d7 }
                if (r5 < 0) goto L_0x02cb
                r7.c = r5     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r5 = r0.f3309a     // Catch:{ Exception -> 0x02d7 }
                int r11 = r7.c     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r6 = "The given URI is null."
                if (r5 == 0) goto L_0x02c3
                java.lang.String r8 = "The given timeout value is negative."
                if (r11 < 0) goto L_0x02bb
                java.net.URI r5 = java.net.URI.create(r5)     // Catch:{ Exception -> 0x02d7 }
                if (r5 == 0) goto L_0x02b3
                if (r11 < 0) goto L_0x02ab
                java.lang.String r6 = r5.getScheme()     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r16 = r5.getUserInfo()     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r15 = com.helpshift.websockets.q.a(r5)     // Catch:{ Exception -> 0x02d7 }
                int r14 = r5.getPort()     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r8 = r5.getRawPath()     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r5 = r5.getRawQuery()     // Catch:{ Exception -> 0x02d7 }
                if (r6 == 0) goto L_0x02a1
                int r9 = r6.length()     // Catch:{ Exception -> 0x02d7 }
                if (r9 == 0) goto L_0x02a1
                java.lang.String r9 = "wss"
                boolean r9 = r9.equalsIgnoreCase(r6)     // Catch:{ Exception -> 0x02d7 }
                r10 = 0
                if (r9 != 0) goto L_0x0124
                java.lang.String r9 = "https"
                boolean r9 = r9.equalsIgnoreCase(r6)     // Catch:{ Exception -> 0x02d7 }
                if (r9 == 0) goto L_0x00fa
                goto L_0x0124
            L_0x00fa:
                java.lang.String r9 = "ws"
                boolean r9 = r9.equalsIgnoreCase(r6)     // Catch:{ Exception -> 0x02d7 }
                if (r9 != 0) goto L_0x0122
                java.lang.String r9 = "http"
                boolean r9 = r9.equalsIgnoreCase(r6)     // Catch:{ Exception -> 0x02d7 }
                if (r9 == 0) goto L_0x010b
                goto L_0x0122
            L_0x010b:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ Exception -> 0x02d7 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d7 }
                r3.<init>()     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r4 = "Bad scheme: "
                r3.append(r4)     // Catch:{ Exception -> 0x02d7 }
                r3.append(r6)     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x02d7 }
                r0.<init>(r3)     // Catch:{ Exception -> 0x02d7 }
                throw r0     // Catch:{ Exception -> 0x02d7 }
            L_0x0122:
                r6 = 0
                goto L_0x0125
            L_0x0124:
                r6 = 1
            L_0x0125:
                if (r15 == 0) goto L_0x0297
                int r9 = r15.length()     // Catch:{ Exception -> 0x02d7 }
                if (r9 == 0) goto L_0x0297
                java.lang.String r9 = "/"
                if (r8 == 0) goto L_0x0150
                int r12 = r8.length()     // Catch:{ Exception -> 0x02d7 }
                if (r12 != 0) goto L_0x0138
                goto L_0x0150
            L_0x0138:
                boolean r12 = r8.startsWith(r9)     // Catch:{ Exception -> 0x02d7 }
                if (r12 == 0) goto L_0x0140
            L_0x013e:
                r12 = r8
                goto L_0x0151
            L_0x0140:
                java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d7 }
                r12.<init>()     // Catch:{ Exception -> 0x02d7 }
                r12.append(r9)     // Catch:{ Exception -> 0x02d7 }
                r12.append(r8)     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r8 = r12.toString()     // Catch:{ Exception -> 0x02d7 }
                goto L_0x013e
            L_0x0150:
                r12 = r9
            L_0x0151:
                int r9 = com.helpshift.websockets.an.a(r14, r6)     // Catch:{ Exception -> 0x02d7 }
                com.helpshift.websockets.aa r8 = r7.f4317b     // Catch:{ Exception -> 0x02d7 }
                java.lang.String r8 = r8.d     // Catch:{ Exception -> 0x02d7 }
                if (r8 == 0) goto L_0x015c
                r10 = 1
            L_0x015c:
                if (r10 == 0) goto L_0x01b5
                com.helpshift.websockets.aa r8 = r7.f4317b     // Catch:{ Exception -> 0x01b0 }
                int r8 = r8.e     // Catch:{ Exception -> 0x01b0 }
                com.helpshift.websockets.aa r10 = r7.f4317b     // Catch:{ Exception -> 0x01b0 }
                boolean r10 = r10.c     // Catch:{ Exception -> 0x01b0 }
                int r8 = com.helpshift.websockets.an.a(r8, r10)     // Catch:{ Exception -> 0x01b0 }
                com.helpshift.websockets.aa r10 = r7.f4317b     // Catch:{ Exception -> 0x01b0 }
                com.helpshift.websockets.ae r3 = r10.f4293b     // Catch:{ Exception -> 0x01b0 }
                boolean r10 = r10.c     // Catch:{ Exception -> 0x01b0 }
                javax.net.SocketFactory r3 = r3.a(r10)     // Catch:{ Exception -> 0x01b0 }
                java.net.Socket r3 = r3.createSocket()     // Catch:{ Exception -> 0x01b0 }
                com.helpshift.websockets.a r10 = new com.helpshift.websockets.a     // Catch:{ Exception -> 0x01b0 }
                com.helpshift.websockets.aa r13 = r7.f4317b     // Catch:{ Exception -> 0x01b0 }
                java.lang.String r13 = r13.d     // Catch:{ Exception -> 0x01b0 }
                r10.<init>(r13, r8)     // Catch:{ Exception -> 0x01b0 }
                com.helpshift.websockets.z r13 = new com.helpshift.websockets.z     // Catch:{ Exception -> 0x01b0 }
                com.helpshift.websockets.aa r8 = r7.f4317b     // Catch:{ Exception -> 0x01b0 }
                r13.<init>(r3, r15, r9, r8)     // Catch:{ Exception -> 0x01b0 }
                if (r6 == 0) goto L_0x0195
                com.helpshift.websockets.ae r8 = r7.f4316a     // Catch:{ Exception -> 0x02d7 }
                javax.net.SocketFactory r8 = r8.a(r6)     // Catch:{ Exception -> 0x02d7 }
                javax.net.ssl.SSLSocketFactory r8 = (javax.net.ssl.SSLSocketFactory) r8     // Catch:{ Exception -> 0x02d7 }
                r17 = r8
                goto L_0x0197
            L_0x0195:
                r17 = 0
            L_0x0197:
                com.helpshift.websockets.ad r18 = new com.helpshift.websockets.ad     // Catch:{ Exception -> 0x01b0 }
                r8 = r18
                r19 = r9
                r9 = r3
                r3 = r12
                r12 = r13
                r20 = r2
                r2 = 1
                r13 = r17
                r2 = r14
                r14 = r15
                r1 = r15
                r15 = r19
                r8.<init>(r9, r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x0293 }
                r12 = r18
                goto L_0x01d3
            L_0x01b0:
                r0 = move-exception
                r20 = r2
                goto L_0x0294
            L_0x01b5:
                r20 = r2
                r19 = r9
                r3 = r12
                r2 = r14
                r1 = r15
                com.helpshift.websockets.ae r8 = r7.f4316a     // Catch:{ Exception -> 0x0293 }
                javax.net.SocketFactory r8 = r8.a(r6)     // Catch:{ Exception -> 0x0293 }
                java.net.Socket r8 = r8.createSocket()     // Catch:{ Exception -> 0x0293 }
                com.helpshift.websockets.a r9 = new com.helpshift.websockets.a     // Catch:{ Exception -> 0x0293 }
                r10 = r19
                r9.<init>(r1, r10)     // Catch:{ Exception -> 0x0293 }
                com.helpshift.websockets.ad r10 = new com.helpshift.websockets.ad     // Catch:{ Exception -> 0x0293 }
                r10.<init>(r8, r9, r11)     // Catch:{ Exception -> 0x0293 }
                r12 = r10
            L_0x01d3:
                if (r2 < 0) goto L_0x01e9
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0293 }
                r8.<init>()     // Catch:{ Exception -> 0x0293 }
                r8.append(r1)     // Catch:{ Exception -> 0x0293 }
                java.lang.String r1 = ":"
                r8.append(r1)     // Catch:{ Exception -> 0x0293 }
                r8.append(r2)     // Catch:{ Exception -> 0x0293 }
                java.lang.String r1 = r8.toString()     // Catch:{ Exception -> 0x0293 }
            L_0x01e9:
                r10 = r1
                if (r5 == 0) goto L_0x0202
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0293 }
                r1.<init>()     // Catch:{ Exception -> 0x0293 }
                r1.append(r3)     // Catch:{ Exception -> 0x0293 }
                java.lang.String r2 = "?"
                r1.append(r2)     // Catch:{ Exception -> 0x0293 }
                r1.append(r5)     // Catch:{ Exception -> 0x0293 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0293 }
                r11 = r1
                goto L_0x0203
            L_0x0202:
                r11 = r3
            L_0x0203:
                com.helpshift.websockets.aj r1 = new com.helpshift.websockets.aj     // Catch:{ Exception -> 0x0293 }
                r13 = r6
                r6 = r1
                r8 = r13
                r9 = r16
                r6.<init>(r7, r8, r9, r10, r11, r12)     // Catch:{ Exception -> 0x0293 }
                com.helpshift.websockets.ad r2 = r1.f4309a     // Catch:{ Exception -> 0x0293 }
                java.net.Socket r2 = r2.d     // Catch:{ Exception -> 0x0293 }
                int r3 = r0.c     // Catch:{ Exception -> 0x0293 }
                r2.setSoTimeout(r3)     // Catch:{ Exception -> 0x0293 }
                java.util.List<java.lang.String> r2 = r0.d     // Catch:{ Exception -> 0x0293 }
                java.util.Iterator r2 = r2.iterator()     // Catch:{ Exception -> 0x0293 }
            L_0x021c:
                boolean r3 = r2.hasNext()     // Catch:{ Exception -> 0x0293 }
                if (r3 == 0) goto L_0x0232
                java.lang.Object r3 = r2.next()     // Catch:{ Exception -> 0x0293 }
                java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0293 }
                com.helpshift.websockets.l r5 = r1.e     // Catch:{ Exception -> 0x0293 }
                com.helpshift.websockets.am r3 = com.helpshift.websockets.am.a(r3)     // Catch:{ Exception -> 0x0293 }
                r5.a(r3)     // Catch:{ Exception -> 0x0293 }
                goto L_0x021c
            L_0x0232:
                java.util.List<java.lang.String> r2 = r0.e     // Catch:{ Exception -> 0x0293 }
                java.util.Iterator r2 = r2.iterator()     // Catch:{ Exception -> 0x0293 }
            L_0x0238:
                boolean r3 = r2.hasNext()     // Catch:{ Exception -> 0x0293 }
                if (r3 == 0) goto L_0x024a
                java.lang.Object r3 = r2.next()     // Catch:{ Exception -> 0x0293 }
                java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0293 }
                com.helpshift.websockets.l r5 = r1.e     // Catch:{ Exception -> 0x0293 }
                r5.a(r3)     // Catch:{ Exception -> 0x0293 }
                goto L_0x0238
            L_0x024a:
                java.util.Map<java.lang.String, java.lang.String> r2 = r0.f     // Catch:{ Exception -> 0x0293 }
                java.util.Set r2 = r2.keySet()     // Catch:{ Exception -> 0x0293 }
                java.util.Iterator r2 = r2.iterator()     // Catch:{ Exception -> 0x0293 }
            L_0x0254:
                boolean r3 = r2.hasNext()     // Catch:{ Exception -> 0x0293 }
                if (r3 == 0) goto L_0x026e
                java.lang.Object r3 = r2.next()     // Catch:{ Exception -> 0x0293 }
                java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0293 }
                java.util.Map<java.lang.String, java.lang.String> r5 = r0.f     // Catch:{ Exception -> 0x0293 }
                java.lang.Object r5 = r5.get(r3)     // Catch:{ Exception -> 0x0293 }
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x0293 }
                com.helpshift.websockets.l r6 = r1.e     // Catch:{ Exception -> 0x0293 }
                r6.a(r3, r5)     // Catch:{ Exception -> 0x0293 }
                goto L_0x0254
            L_0x026e:
                com.helpshift.common.e.a.a.a r2 = new com.helpshift.common.e.a.a.a     // Catch:{ Exception -> 0x0293 }
                com.helpshift.common.e.a.a.b r0 = r0.g     // Catch:{ Exception -> 0x0293 }
                r2.<init>(r1, r0)     // Catch:{ Exception -> 0x0293 }
                r4.d = r2     // Catch:{ Exception -> 0x0293 }
                r1 = r21
                com.helpshift.j.a.r r0 = com.helpshift.j.a.r.this     // Catch:{ Exception -> 0x02d5 }
                r2 = 1
                r0.f = r2     // Catch:{ Exception -> 0x02d5 }
                com.helpshift.j.a.r r0 = com.helpshift.j.a.r.this     // Catch:{ Exception -> 0x02d5 }
                com.helpshift.common.e.a.a.a r2 = r0.d     // Catch:{ Exception -> 0x02d5 }
                com.helpshift.websockets.aj r0 = r2.f3307a     // Catch:{ WebSocketException -> 0x0288 }
                r0.a()     // Catch:{ WebSocketException -> 0x0288 }
                goto L_0x02e7
            L_0x0288:
                r0 = move-exception
                com.helpshift.common.e.a.a.b r2 = r2.f3308b     // Catch:{ Exception -> 0x02d5 }
                java.lang.String r0 = r0.getMessage()     // Catch:{ Exception -> 0x02d5 }
                r2.b(r0)     // Catch:{ Exception -> 0x02d5 }
                goto L_0x02e7
            L_0x0293:
                r0 = move-exception
            L_0x0294:
                r1 = r21
                goto L_0x02da
            L_0x0297:
                r20 = r2
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ Exception -> 0x02d5 }
                java.lang.String r2 = "The host part is empty."
                r0.<init>(r2)     // Catch:{ Exception -> 0x02d5 }
                throw r0     // Catch:{ Exception -> 0x02d5 }
            L_0x02a1:
                r20 = r2
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ Exception -> 0x02d5 }
                java.lang.String r2 = "The scheme part is empty."
                r0.<init>(r2)     // Catch:{ Exception -> 0x02d5 }
                throw r0     // Catch:{ Exception -> 0x02d5 }
            L_0x02ab:
                r20 = r2
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ Exception -> 0x02d5 }
                r0.<init>(r8)     // Catch:{ Exception -> 0x02d5 }
                throw r0     // Catch:{ Exception -> 0x02d5 }
            L_0x02b3:
                r20 = r2
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ Exception -> 0x02d5 }
                r0.<init>(r6)     // Catch:{ Exception -> 0x02d5 }
                throw r0     // Catch:{ Exception -> 0x02d5 }
            L_0x02bb:
                r20 = r2
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ Exception -> 0x02d5 }
                r0.<init>(r8)     // Catch:{ Exception -> 0x02d5 }
                throw r0     // Catch:{ Exception -> 0x02d5 }
            L_0x02c3:
                r20 = r2
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ Exception -> 0x02d5 }
                r0.<init>(r6)     // Catch:{ Exception -> 0x02d5 }
                throw r0     // Catch:{ Exception -> 0x02d5 }
            L_0x02cb:
                r20 = r2
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ Exception -> 0x02d5 }
                java.lang.String r2 = "timeout value cannot be negative."
                r0.<init>(r2)     // Catch:{ Exception -> 0x02d5 }
                throw r0     // Catch:{ Exception -> 0x02d5 }
            L_0x02d5:
                r0 = move-exception
                goto L_0x02da
            L_0x02d7:
                r0 = move-exception
                r20 = r2
            L_0x02da:
                java.lang.String r2 = "Exception in connecting web-socket"
                r3 = r20
                com.helpshift.util.n.c(r3, r2, r0)
                com.helpshift.j.a.r r0 = com.helpshift.j.a.r.this
                r0.c()
            L_0x02e7:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.a.r.a.a():void");
        }
    }

    /* compiled from: LiveUpdateDM */
    class d extends l {

        /* renamed from: a  reason: collision with root package name */
        int f3539a;

        d(int i) {
            this.f3539a = i;
        }

        public final void a() {
            if (this.f3539a == r.this.k.get() && r.this.h != null) {
                n.a("Helpshift_LiveUpdateDM", "Start Typing action timed out, disabling TAI", (Throwable) null, (com.helpshift.p.b.a[]) null);
                r rVar = r.this;
                rVar.l = false;
                rVar.d();
            }
        }
    }

    /* compiled from: LiveUpdateDM */
    class c extends l {

        /* renamed from: a  reason: collision with root package name */
        int f3537a;

        c(int i) {
            this.f3537a = i;
        }

        public final void a() {
            if (this.f3537a == r.this.c.get() && r.this.h != null) {
                n.a("Helpshift_LiveUpdateDM", "Ping timed out, resetting connection", (Throwable) null, (com.helpshift.p.b.a[]) null);
                r.this.m.a();
                r rVar = r.this;
                new a(rVar.c.incrementAndGet()).a();
            }
        }
    }

    /* compiled from: LiveUpdateDM */
    class b extends l {

        /* renamed from: b  reason: collision with root package name */
        private final String f3536b;

        b(String str) {
            this.f3536b = str;
        }

        public final void a() {
            h l = r.this.j.l().l(this.f3536b);
            if (l instanceof f) {
                long j = ((f) l).f3594a + r.this.f3531a;
                j jVar = r.this.i;
                r rVar = r.this;
                jVar.a(new c(rVar.c.incrementAndGet()), j);
                if (r.this.d != null) {
                    r.this.d.a("[110]");
                }
            } else if (r.this.h != null && (l instanceof g)) {
                g gVar = (g) l;
                if (gVar.f3595a) {
                    r.this.l = true;
                    long j2 = gVar.f3596b + r.this.f3531a;
                    j jVar2 = r.this.i;
                    r rVar2 = r.this;
                    jVar2.a(new d(rVar2.k.incrementAndGet()), j2);
                } else {
                    r.this.l = false;
                }
                r.this.d();
            }
        }
    }

    public final void a(com.helpshift.common.e.a.a.a aVar) {
        n.a("Helpshift_LiveUpdateDM", "web-socket connected", (Throwable) null, (com.helpshift.p.b.a[]) null);
        this.f = false;
        this.n = true;
        if (this.e) {
            this.m.a();
        } else if (this.h != null) {
            n.a("Helpshift_LiveUpdateDM", "Subscribing to conversation topic", (Throwable) null, (com.helpshift.p.b.a[]) null);
            aVar.a("[104, [\"" + "agent_type_act.issue." + this.q + "\"]]");
            this.i.a(new c(this.c.incrementAndGet()), TimeUnit.SECONDS.toMillis(60));
        } else {
            this.m.a();
        }
    }

    public final void a() {
        n.a("Helpshift_LiveUpdateDM", "web-socket disconnected", (Throwable) null, (com.helpshift.p.b.a[]) null);
        this.n = false;
        this.e = false;
    }
}
