package com.helpshift.j.a.a;

import com.helpshift.common.e.ab;

/* compiled from: RequestAppReviewMessageDM */
public class y extends v {

    /* renamed from: a  reason: collision with root package name */
    public boolean f3498a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f3499b = true;

    public final boolean a() {
        return true;
    }

    public y(String str, String str2, String str3, long j, String str4, boolean z) {
        super(str2, str3, j, str4, true, w.REQUESTED_APP_REVIEW);
        this.m = str;
        this.f3498a = z;
    }

    public final void a(boolean z) {
        this.f3499b = z;
        k();
    }

    public final void a(ab abVar) {
        this.f3499b = false;
        this.f3498a = true;
        k();
        abVar.f().a(this);
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof y) {
            this.f3498a = ((y) vVar).f3498a;
        }
    }
}
