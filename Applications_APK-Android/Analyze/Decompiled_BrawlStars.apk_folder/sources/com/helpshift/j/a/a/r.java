package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.k;
import com.helpshift.j.a.o;
import java.util.HashMap;

/* compiled from: FollowupAcceptedMessageDM */
public class r extends l {

    /* renamed from: a  reason: collision with root package name */
    public String f3489a;

    public final boolean a() {
        return false;
    }

    public r(String str, String str2, long j, String str3, String str4, int i) {
        super(str, str2, j, str3, false, w.FOLLOWUP_ACCEPTED, i);
        this.f3489a = str4;
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof r) {
            this.f3489a = ((r) vVar).f3489a;
        }
    }

    public final void a(c cVar, o oVar) {
        if (!k.a(oVar.b())) {
            HashMap<String, String> a2 = com.helpshift.common.c.b.o.a(cVar);
            a2.put("body", "Accepted the follow-up");
            a2.put("type", "ra");
            a2.put("refers", this.f3489a);
            try {
                r j = this.A.l().j(a(b(oVar), a2).f3323b);
                a(j);
                this.o = j.o;
                this.m = j.m;
                this.A.f().a(this);
            } catch (RootAPIException e) {
                if (e.c == b.INVALID_AUTH_TOKEN || e.c == b.AUTH_TOKEN_NOT_PROVIDED) {
                    this.z.j.a(cVar, e.c);
                }
                throw e;
            }
        } else {
            throw new UnsupportedOperationException("FollowupAcceptedMessageDM send called with conversation in pre issue mode.");
        }
    }
}
