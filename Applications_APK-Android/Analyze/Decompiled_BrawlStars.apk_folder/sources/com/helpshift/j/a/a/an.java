package com.helpshift.j.a.a;

import com.helpshift.common.e.a.j;
import com.helpshift.common.e.aa;
import com.helpshift.j.a.a.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: UserResponseMessageForOptionInput */
public class an extends al {

    /* renamed from: a  reason: collision with root package name */
    public String f3461a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f3462b;
    public String d;
    public w e;
    private String f;

    public an(String str, String str2, long j, String str3, String str4, boolean z, String str5, String str6, w wVar) {
        super(str, str2, j, str3, w.USER_RESP_FOR_OPTION_INPUT);
        this.f3461a = str4;
        this.f3462b = z;
        this.d = str5;
        this.f = str6;
        this.e = wVar;
    }

    public an(String str, String str2, long j, String str3, x xVar, boolean z) {
        super(str, str2, j, str3, w.USER_RESP_FOR_OPTION_INPUT);
        String str4;
        this.f3461a = xVar.f3496a.f3439a;
        this.f3462b = z;
        Iterator<b.a> it = xVar.f3496a.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                str4 = "{}";
                break;
            }
            b.a next = it.next();
            if (next.f3441a.equals(this.n)) {
                str4 = next.f3442b;
                break;
            }
        }
        this.d = str4;
        this.f = xVar.m;
        this.e = xVar.f3497b;
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof an) {
            an anVar = (an) vVar;
            this.f3461a = anVar.f3461a;
            this.f3462b = anVar.f3462b;
            this.d = anVar.d;
            this.f = anVar.f;
            this.e = anVar.e;
        }
    }

    /* access modifiers changed from: protected */
    public final Map<String, String> b() {
        HashMap hashMap = new HashMap();
        hashMap.put("chatbot_info", this.f3461a);
        hashMap.put("skipped", String.valueOf(this.f3462b));
        if (!this.f3462b) {
            hashMap.put("option_data", this.d);
        }
        if (this.e == w.FAQ_LIST_WITH_OPTION_INPUT) {
            List arrayList = new ArrayList();
            aa o = this.A.o();
            Object b2 = o.b("read_faq_" + this.f);
            if (b2 instanceof ArrayList) {
                arrayList = (List) b2;
            }
            hashMap.put("read_faqs", this.A.p().a((List<String>) arrayList).toString());
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final String c() {
        int i = ao.f3463a[this.e.ordinal()];
        if (i != 1) {
            return i != 2 ? super.c() : "rsp_faq_list_msg_with_option_input";
        }
        return "rsp_txt_msg_with_option_input";
    }

    public final String d() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final al a(j jVar) {
        return this.A.l().m(jVar.f3323b);
    }
}
