package com.helpshift.j.a.a;

/* compiled from: RequestScreenshotMessageDM */
public class aa extends v {

    /* renamed from: a  reason: collision with root package name */
    public boolean f3445a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f3446b = true;

    public final boolean a() {
        return true;
    }

    public aa(String str, String str2, String str3, long j, String str4, boolean z) {
        super(str2, str3, j, str4, true, w.REQUESTED_SCREENSHOT);
        this.m = str;
        this.f3445a = z;
    }

    public final boolean b() {
        return !this.f3445a && this.f3446b;
    }

    public final void a(boolean z) {
        this.f3446b = z;
        k();
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof aa) {
            this.f3445a = ((aa) vVar).f3445a;
        }
    }
}
