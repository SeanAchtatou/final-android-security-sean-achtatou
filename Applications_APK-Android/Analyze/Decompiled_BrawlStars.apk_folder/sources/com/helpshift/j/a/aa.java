package com.helpshift.j.a;

import com.helpshift.a.b.c;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.b.a;
import com.helpshift.j.a.x;
import com.helpshift.j.e.g;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: ViewableSingleConversation */
public class aa extends x {
    public a h;

    public final void b(a aVar) {
    }

    public aa(ab abVar, j jVar, c cVar, g gVar, b bVar) {
        super(abVar, jVar, cVar, gVar, bVar);
    }

    public final x.a a() {
        return x.a.SINGLE;
    }

    public final synchronized void f() {
        this.h = this.f3547a.b().get(0);
        this.h.t = this.d.f3176a.longValue();
        Iterator<E> it = this.h.j.iterator();
        while (it.hasNext()) {
            ((v) it.next()).a(this.c, this.f3548b);
        }
    }

    public final boolean g() {
        return this.f.g(this.h);
    }

    public final a h() {
        return this.h;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.d(com.helpshift.j.a.b.a, boolean):void
     arg types: [com.helpshift.j.a.b.a, int]
     candidates:
      com.helpshift.j.a.b.d(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
      com.helpshift.j.a.b.d(com.helpshift.j.a.b.a, boolean):void */
    public final void i() {
        this.f.d(this.h, true);
    }

    public final void a(com.helpshift.common.g.c<v> cVar) {
        this.h.j.f3383a = cVar;
        this.h.f();
    }

    public final List<a> j() {
        return Collections.singletonList(this.h);
    }

    public final void a(List<a> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a aVar = list.get(i);
            if (this.h.f3504b.equals(aVar.f3504b)) {
                this.h.j.a(aVar.j);
            }
        }
    }

    public final u k() {
        return c(this.h);
    }
}
