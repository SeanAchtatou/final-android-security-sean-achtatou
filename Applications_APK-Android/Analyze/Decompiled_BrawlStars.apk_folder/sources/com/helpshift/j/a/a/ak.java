package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.j.a.o;
import java.util.HashMap;

/* compiled from: UserBotControlMessageDM */
public class ak extends l {

    /* renamed from: a  reason: collision with root package name */
    public String f3458a;
    public String c;
    public String d;
    public String e;

    public final boolean a() {
        return false;
    }

    public ak(String str, String str2, long j, String str3, String str4, String str5, String str6, String str7, int i) {
        super(str, str2, j, str3, false, w.USER_BOT_CONTROL, i);
        this.f3458a = str4;
        this.c = str5;
        this.d = str6;
        this.e = str7;
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof ak) {
            ak akVar = (ak) vVar;
            this.f3458a = akVar.f3458a;
            this.c = akVar.c;
            this.d = akVar.d;
            this.e = akVar.e;
        }
    }

    public final void a(c cVar, o oVar) {
        String str;
        HashMap<String, String> a2 = com.helpshift.common.c.b.o.a(cVar);
        a2.put("origin", "mobile");
        a2.put("type", this.f3458a);
        a2.put("chatbot_cancelled_reason", this.c);
        a2.put("body", this.n);
        a2.put("chatbot_info", this.d);
        a2.put("refers", this.e);
        if (oVar.a()) {
            str = a(oVar);
        } else {
            str = b(oVar);
        }
        try {
            ak akVar = (ak) this.A.l().a(a(str, a2).f3323b, false);
            a(akVar);
            this.m = akVar.m;
            this.A.f().a(this);
        } catch (RootAPIException e2) {
            if (e2.c == b.AUTH_TOKEN_NOT_PROVIDED || e2.c == b.INVALID_AUTH_TOKEN) {
                this.z.j.a(cVar, e2.c);
            }
            throw e2;
        }
    }
}
