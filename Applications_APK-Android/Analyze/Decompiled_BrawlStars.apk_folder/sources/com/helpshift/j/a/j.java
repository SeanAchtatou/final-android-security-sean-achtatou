package com.helpshift.j.a;

import com.helpshift.common.c.l;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.j.a.a.n;
import com.helpshift.j.a.b.a;
import com.helpshift.j.d.e;

/* compiled from: ConversationManager */
class j extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f3519a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f3520b;
    final /* synthetic */ b c;

    j(b bVar, n nVar, a aVar) {
        this.c = bVar;
        this.f3519a = nVar;
        this.f3520b = aVar;
    }

    public final void a() {
        try {
            this.f3519a.a(this.c.c, this.f3520b);
        } catch (RootAPIException e) {
            if (e.c == b.CONVERSATION_ARCHIVED) {
                this.c.a(this.f3520b, e.ARCHIVED);
                return;
            }
            throw e;
        }
    }
}
