package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.common.c.j;
import com.helpshift.common.e.aa;
import com.helpshift.common.e.ab;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.p;
import com.helpshift.j.a.o;
import java.util.ArrayList;
import java.util.List;

/* compiled from: FAQListMessageWithOptionInputDM */
public class q extends p {
    public b d;
    private ArrayList<String> e;

    public q(String str, String str2, String str3, long j, String str4, List<p.a> list, String str5, boolean z, String str6, String str7, List<b.a> list2) {
        super(str, str2, str3, j, str4, list, w.FAQ_LIST_WITH_OPTION_INPUT);
        this.d = new b(str5, z, str6, str7, list2, b.C0138b.PILL);
    }

    public q(String str, String str2, String str3, long j, String str4, List<p.a> list, String str5, boolean z, String str6, String str7, List<b.a> list2, boolean z2, String str8) {
        super(str, str2, str3, j, str4, list, w.FAQ_LIST_WITH_OPTION_INPUT);
        this.d = new b(str5, z, str6, str7, list2, b.C0138b.PILL);
        this.f3486b = z2;
        this.c = str8;
    }

    public final void a(j jVar, ab abVar) {
        super.a(jVar, abVar);
        if (this.e == null) {
            this.e = new ArrayList<>();
            aa o = this.A.o();
            Object b2 = o.b("read_faq_" + this.m);
            if (b2 instanceof ArrayList) {
                this.e = (ArrayList) b2;
            }
        }
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof q) {
            this.d = ((q) vVar).d;
        }
    }

    public final void a(o oVar, c cVar, String str, String str2) {
        if (this.e.size() < 10) {
            this.e.add(str);
            aa o = this.A.o();
            o.a("read_faq_" + this.m, this.e);
        }
        super.a(oVar, cVar, str, str2);
    }
}
