package com.helpshift.j.a;

import com.helpshift.j.a.a.w;
import com.helpshift.j.d.e;

/* compiled from: ConversationManager */
/* synthetic */ class f {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f3511a = new int[e.values().length];

    /* renamed from: b  reason: collision with root package name */
    static final /* synthetic */ int[] f3512b = new int[w.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(33:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|36|37|38|39|40|(3:41|42|44)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(35:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|36|37|38|39|40|41|42|44) */
    /* JADX WARNING: Can't wrap try/catch for region: R(36:0|(2:1|2)|3|5|6|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|36|37|38|39|40|41|42|44) */
    /* JADX WARNING: Can't wrap try/catch for region: R(37:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|36|37|38|39|40|41|42|44) */
    /* JADX WARNING: Can't wrap try/catch for region: R(38:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|33|34|35|36|37|38|39|40|41|42|44) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0040 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x004b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0056 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0062 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x006e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x0099 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00a3 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x00ad */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00b7 */
    static {
        /*
            com.helpshift.j.a.a.w[] r0 = com.helpshift.j.a.a.w.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.helpshift.j.a.f.f3512b = r0
            r0 = 1
            int[] r1 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.helpshift.j.a.a.w r2 = com.helpshift.j.a.a.w.UNSUPPORTED_ADMIN_MESSAGE_WITH_INPUT     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            r1 = 2
            int[] r2 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x001f }
            com.helpshift.j.a.a.w r3 = com.helpshift.j.a.a.w.ADMIN_TEXT     // Catch:{ NoSuchFieldError -> 0x001f }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            r2 = 3
            int[] r3 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x002a }
            com.helpshift.j.a.a.w r4 = com.helpshift.j.a.a.w.ADMIN_TEXT_WITH_TEXT_INPUT     // Catch:{ NoSuchFieldError -> 0x002a }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            r3 = 4
            int[] r4 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x0035 }
            com.helpshift.j.a.a.w r5 = com.helpshift.j.a.a.w.ADMIN_TEXT_WITH_OPTION_INPUT     // Catch:{ NoSuchFieldError -> 0x0035 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
            r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
        L_0x0035:
            r4 = 5
            int[] r5 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x0040 }
            com.helpshift.j.a.a.w r6 = com.helpshift.j.a.a.w.FAQ_LIST     // Catch:{ NoSuchFieldError -> 0x0040 }
            int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
            r5[r6] = r4     // Catch:{ NoSuchFieldError -> 0x0040 }
        L_0x0040:
            int[] r5 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x004b }
            com.helpshift.j.a.a.w r6 = com.helpshift.j.a.a.w.FAQ_LIST_WITH_OPTION_INPUT     // Catch:{ NoSuchFieldError -> 0x004b }
            int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
            r7 = 6
            r5[r6] = r7     // Catch:{ NoSuchFieldError -> 0x004b }
        L_0x004b:
            int[] r5 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x0056 }
            com.helpshift.j.a.a.w r6 = com.helpshift.j.a.a.w.ADMIN_ATTACHMENT     // Catch:{ NoSuchFieldError -> 0x0056 }
            int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
            r7 = 7
            r5[r6] = r7     // Catch:{ NoSuchFieldError -> 0x0056 }
        L_0x0056:
            int[] r5 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x0062 }
            com.helpshift.j.a.a.w r6 = com.helpshift.j.a.a.w.ADMIN_IMAGE_ATTACHMENT     // Catch:{ NoSuchFieldError -> 0x0062 }
            int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
            r7 = 8
            r5[r6] = r7     // Catch:{ NoSuchFieldError -> 0x0062 }
        L_0x0062:
            int[] r5 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x006e }
            com.helpshift.j.a.a.w r6 = com.helpshift.j.a.a.w.REQUEST_FOR_REOPEN     // Catch:{ NoSuchFieldError -> 0x006e }
            int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
            r7 = 9
            r5[r6] = r7     // Catch:{ NoSuchFieldError -> 0x006e }
        L_0x006e:
            int[] r5 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x007a }
            com.helpshift.j.a.a.w r6 = com.helpshift.j.a.a.w.REQUESTED_SCREENSHOT     // Catch:{ NoSuchFieldError -> 0x007a }
            int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
            r7 = 10
            r5[r6] = r7     // Catch:{ NoSuchFieldError -> 0x007a }
        L_0x007a:
            int[] r5 = com.helpshift.j.a.f.f3512b     // Catch:{ NoSuchFieldError -> 0x0086 }
            com.helpshift.j.a.a.w r6 = com.helpshift.j.a.a.w.REQUESTED_APP_REVIEW     // Catch:{ NoSuchFieldError -> 0x0086 }
            int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0086 }
            r7 = 11
            r5[r6] = r7     // Catch:{ NoSuchFieldError -> 0x0086 }
        L_0x0086:
            com.helpshift.j.d.e[] r5 = com.helpshift.j.d.e.values()
            int r5 = r5.length
            int[] r5 = new int[r5]
            com.helpshift.j.a.f.f3511a = r5
            int[] r5 = com.helpshift.j.a.f.f3511a     // Catch:{ NoSuchFieldError -> 0x0099 }
            com.helpshift.j.d.e r6 = com.helpshift.j.d.e.ARCHIVED     // Catch:{ NoSuchFieldError -> 0x0099 }
            int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0099 }
            r5[r6] = r0     // Catch:{ NoSuchFieldError -> 0x0099 }
        L_0x0099:
            int[] r0 = com.helpshift.j.a.f.f3511a     // Catch:{ NoSuchFieldError -> 0x00a3 }
            com.helpshift.j.d.e r5 = com.helpshift.j.d.e.RESOLUTION_ACCEPTED     // Catch:{ NoSuchFieldError -> 0x00a3 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x00a3 }
            r0[r5] = r1     // Catch:{ NoSuchFieldError -> 0x00a3 }
        L_0x00a3:
            int[] r0 = com.helpshift.j.a.f.f3511a     // Catch:{ NoSuchFieldError -> 0x00ad }
            com.helpshift.j.d.e r1 = com.helpshift.j.d.e.REJECTED     // Catch:{ NoSuchFieldError -> 0x00ad }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ad }
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00ad }
        L_0x00ad:
            int[] r0 = com.helpshift.j.a.f.f3511a     // Catch:{ NoSuchFieldError -> 0x00b7 }
            com.helpshift.j.d.e r1 = com.helpshift.j.d.e.RESOLUTION_REQUESTED     // Catch:{ NoSuchFieldError -> 0x00b7 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b7 }
            r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x00b7 }
        L_0x00b7:
            int[] r0 = com.helpshift.j.a.f.f3511a     // Catch:{ NoSuchFieldError -> 0x00c1 }
            com.helpshift.j.d.e r1 = com.helpshift.j.d.e.COMPLETED_ISSUE_CREATED     // Catch:{ NoSuchFieldError -> 0x00c1 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00c1 }
            r0[r1] = r4     // Catch:{ NoSuchFieldError -> 0x00c1 }
        L_0x00c1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.a.f.<clinit>():void");
    }
}
