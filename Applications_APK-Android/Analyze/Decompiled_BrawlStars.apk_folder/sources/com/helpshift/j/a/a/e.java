package com.helpshift.j.a.a;

import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.k;
import com.helpshift.m.c;

/* compiled from: AdminImageAttachmentMessageDM */
public class e extends u {
    /* access modifiers changed from: private */
    public int D;

    /* renamed from: a  reason: collision with root package name */
    public a f3473a;

    /* compiled from: AdminImageAttachmentMessageDM */
    public enum a {
        DOWNLOAD_NOT_STARTED,
        THUMBNAIL_DOWNLOADING,
        THUMBNAIL_DOWNLOADED,
        IMAGE_DOWNLOADING,
        IMAGE_DOWNLOADED
    }

    public final boolean a() {
        return true;
    }

    public e(String str, String str2, String str3, long j, String str4, String str5, String str6, String str7, String str8, boolean z, int i) {
        super(str2, str3, j, str4, str5, str6, str7, str8, i, true, z, w.ADMIN_IMAGE_ATTACHMENT);
        this.m = str;
        b();
    }

    public final void a(j jVar, ab abVar) {
        super.a(jVar, abVar);
        if (a(this.g)) {
            b();
        }
    }

    public final void b() {
        if (f() != null) {
            this.f3473a = a.IMAGE_DOWNLOADED;
        } else if (e() != null) {
            this.f3473a = a.THUMBNAIL_DOWNLOADED;
        } else {
            this.f3473a = a.DOWNLOAD_NOT_STARTED;
        }
    }

    public final String c() {
        String d = d();
        if (k.a(d)) {
            return g();
        }
        return d + "/" + g();
    }

    public final String d() {
        if (this.f3473a == a.IMAGE_DOWNLOADING && this.D > 0) {
            double d = (double) (this.f * this.D);
            Double.isNaN(d);
            double d2 = d / 100.0d;
            if (d2 < ((double) this.f)) {
                return a(d2);
            }
        }
        return null;
    }

    public final String e() {
        if (!com.helpshift.common.g.a.b(this.i)) {
            this.f3473a = a.DOWNLOAD_NOT_STARTED;
            this.i = null;
        }
        return this.i;
    }

    public final String f() {
        if (a(this.g)) {
            if (this.A != null && !this.A.d(this.g)) {
                this.g = null;
            }
        } else if (!com.helpshift.common.g.a.b(this.g)) {
            this.g = null;
        }
        if (this.g == null && e() != null) {
            this.f3473a = a.THUMBNAIL_DOWNLOADED;
        }
        return this.g;
    }

    public final void a(a aVar) {
        this.f3473a = aVar;
        k();
    }

    public final void a(ab abVar) {
        if (this.f3473a == a.DOWNLOAD_NOT_STARTED) {
            a(a.THUMBNAIL_DOWNLOADING);
            abVar.x().a(new com.helpshift.m.a(this.f3493b, null, null, this.h), c.a.EXTERNAL_OR_INTERNAL, new com.helpshift.common.c.b.a(this.z, abVar, this.e), new f(this, abVar));
        }
    }

    public final void a(com.helpshift.j.i.a aVar) {
        if (this.f3473a == a.IMAGE_DOWNLOADED) {
            if (aVar != null) {
                aVar.a(f(), this.c);
            }
        } else if (this.f3473a == a.DOWNLOAD_NOT_STARTED || this.f3473a == a.THUMBNAIL_DOWNLOADING || this.f3473a == a.THUMBNAIL_DOWNLOADED) {
            a(a.IMAGE_DOWNLOADING);
            this.A.x().a(new com.helpshift.m.a(this.e, this.d, this.c, this.h), c.a.EXTERNAL_ONLY, new com.helpshift.common.c.b.a(this.z, this.A, this.e), new g(this));
        }
    }
}
