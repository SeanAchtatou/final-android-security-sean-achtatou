package com.helpshift.j.a;

import com.helpshift.a.b.c;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.k;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.r;
import com.helpshift.j.d.e;
import com.helpshift.j.e.d;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: ViewableConversation */
public abstract class x implements a, r.e, d.a {

    /* renamed from: a  reason: collision with root package name */
    protected d f3547a;

    /* renamed from: b  reason: collision with root package name */
    protected ab f3548b;
    protected j c;
    protected c d;
    protected r e;
    protected b f;
    public com.helpshift.j.i.a g;
    private com.helpshift.i.a.a h;
    private AtomicBoolean i = new AtomicBoolean(false);

    /* compiled from: ViewableConversation */
    public enum a {
        HISTORY,
        SINGLE
    }

    public abstract a a();

    public abstract void a(com.helpshift.common.g.c<v> cVar);

    public abstract void a(List<com.helpshift.j.a.b.a> list);

    public abstract void b(com.helpshift.j.a.b.a aVar);

    public abstract void f();

    public abstract boolean g();

    public abstract com.helpshift.j.a.b.a h();

    public abstract void i();

    public abstract List<com.helpshift.j.a.b.a> j();

    public abstract u k();

    public x(ab abVar, j jVar, c cVar, d dVar, b bVar) {
        this.f3548b = abVar;
        this.c = jVar;
        this.d = cVar;
        this.f3547a = dVar;
        this.h = jVar.f3285b;
        this.f = bVar;
    }

    public final void a(r rVar) {
        this.e = rVar;
    }

    public final boolean b() {
        com.helpshift.j.i.a aVar = this.g;
        return aVar != null && aVar.b();
    }

    public final void a(com.helpshift.j.a.b.a aVar, p pVar) {
        com.helpshift.j.a.b.a h2 = h();
        e eVar = h2.g;
        this.f.b(h2, aVar, true, pVar);
        com.helpshift.j.i.a aVar2 = this.g;
        if (aVar2 != null) {
            aVar2.d();
        }
        e eVar2 = h2.g;
        if (eVar2 != eVar) {
            this.f.a(h2);
            a(eVar2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, boolean, com.helpshift.j.a.p):void
     arg types: [com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, int, com.helpshift.j.a.p]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, com.helpshift.j.a.p):com.helpshift.j.a.a.v
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, int, java.lang.String, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String, com.helpshift.j.a.a.j, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, int, java.lang.String, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, boolean, com.helpshift.j.a.p):void */
    public final void b(com.helpshift.j.a.b.a aVar, p pVar) {
        com.helpshift.j.a.b.a h2 = h();
        e eVar = h2.g;
        String str = h2.h;
        this.f.a(h2, aVar, true, pVar);
        com.helpshift.j.i.a aVar2 = this.g;
        if (aVar2 != null) {
            aVar2.d();
        }
        if ("preissue".equals(str) && "issue".equals(h2.h)) {
            c();
        }
        e eVar2 = h2.g;
        if (eVar2 != eVar) {
            this.f.a(h2);
            boolean z = false;
            boolean z2 = com.helpshift.j.c.a(eVar2) && com.helpshift.j.c.a(eVar);
            if (eVar == e.COMPLETED_ISSUE_CREATED) {
                z = true;
            }
            if (z || !z2) {
                a(eVar2);
            }
        }
    }

    public final boolean a(int i2, String str, boolean z) {
        com.helpshift.j.a.b.a h2 = h();
        boolean a2 = this.f.a(h2, i2, str, z);
        if (a2) {
            this.f.a(h2);
            a(h2.g);
        }
        return a2;
    }

    public final boolean a(com.helpshift.j.a.b.a aVar) {
        com.helpshift.j.a.b.a h2;
        if (aVar == null || (h2 = h()) == null) {
            return false;
        }
        if (!k.a(h2.c)) {
            return h2.c.equals(aVar.c);
        }
        if (!k.a(h2.d)) {
            return h2.d.equals(aVar.d);
        }
        return false;
    }

    public final void a(e eVar) {
        com.helpshift.j.i.a aVar = this.g;
        if (aVar != null) {
            aVar.a(eVar);
        }
    }

    public final void c() {
        com.helpshift.j.a.b.a h2 = h();
        if (this.e != null && !h2.a() && this.h.f()) {
            this.e.a(this, h2.c);
        }
    }

    public final void d() {
        r rVar = this.e;
        if (rVar != null) {
            rVar.b();
        }
    }

    public final boolean e() {
        r rVar = this.e;
        return rVar != null && rVar.l && this.h.f();
    }

    public final void a(boolean z) {
        com.helpshift.j.i.a aVar = this.g;
        if (aVar != null) {
            aVar.a(z);
        }
    }

    public final void a(com.helpshift.j.i.a aVar) {
        this.g = aVar;
        h().C = this;
    }

    public final boolean l() {
        return this.f3547a.a();
    }

    public final List<w> m() {
        List<com.helpshift.j.a.b.a> j = j();
        ArrayList arrayList = new ArrayList();
        if (com.helpshift.common.j.a(j)) {
            return arrayList;
        }
        int size = j.size();
        for (int i2 = 0; i2 < size; i2++) {
            com.helpshift.j.a.b.a aVar = j.get(i2);
            arrayList.add(new w(aVar.f3504b.longValue(), i2, aVar.z, aVar.A, aVar.k, aVar.a(), aVar.g, aVar.x));
        }
        return arrayList;
    }

    public final void n() {
        if (this.i.compareAndSet(false, true)) {
            this.f3547a.a(k(), this);
        }
    }

    public final void a(List<com.helpshift.j.a.b.a> list, boolean z) {
        com.helpshift.j.i.a aVar = this.g;
        if (aVar != null) {
            aVar.g();
        }
        if (com.helpshift.common.j.a(list)) {
            this.i.set(false);
            com.helpshift.j.i.a aVar2 = this.g;
            if (aVar2 != null) {
                aVar2.a(new ArrayList(), z);
                return;
            }
            return;
        }
        ArrayList arrayList = new ArrayList();
        for (com.helpshift.j.a.b.a next : list) {
            next.t = this.d.f3176a.longValue();
            this.f.a(next, next.j, a(next) && b.m(h()));
            arrayList.add(next);
        }
        a(arrayList);
        com.helpshift.j.i.a aVar3 = this.g;
        if (aVar3 != null) {
            aVar3.a(arrayList, z);
        }
        this.i.set(false);
    }

    public final void o() {
        this.i.set(false);
        com.helpshift.j.i.a aVar = this.g;
        if (aVar != null) {
            aVar.h();
        }
    }

    public final void p() {
        this.i.set(false);
        com.helpshift.j.i.a aVar = this.g;
        if (aVar != null) {
            aVar.i();
        }
    }

    protected static u c(com.helpshift.j.a.b.a aVar) {
        String str;
        if (aVar == null) {
            return null;
        }
        String str2 = aVar.z;
        if (com.helpshift.common.j.a(aVar.j)) {
            str = str2;
        } else {
            str = ((v) aVar.j.get(0)).B;
        }
        return new u(str2, str);
    }
}
