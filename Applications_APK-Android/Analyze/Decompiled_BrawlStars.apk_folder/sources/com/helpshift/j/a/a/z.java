package com.helpshift.j.a.a;

/* compiled from: RequestForReopenMessageDM */
public class z extends v {

    /* renamed from: a  reason: collision with root package name */
    public boolean f3500a;

    public z(String str, String str2, String str3, long j, String str4) {
        super(str2, str3, j, str4, true, w.REQUEST_FOR_REOPEN);
        this.m = str;
    }

    public final boolean b() {
        return this.f3500a;
    }

    public final void a(boolean z) {
        this.f3500a = z;
    }

    public final void b(boolean z) {
        if (!this.f3500a) {
            this.f3500a = true;
            k();
        }
    }

    public final boolean a() {
        return this.f3500a;
    }
}
