package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.common.c.b.g;
import com.helpshift.common.c.b.j;
import com.helpshift.common.c.b.q;
import com.helpshift.common.c.b.s;
import com.helpshift.common.e.a.i;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.k;
import com.helpshift.j.a.o;
import java.util.HashMap;
import java.util.List;

/* compiled from: FAQListMessageDM */
public class p extends h {

    /* renamed from: a  reason: collision with root package name */
    public List<a> f3485a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f3486b = false;
    public String c = "";

    p(String str, String str2, String str3, long j, String str4, List<a> list, w wVar) {
        super(str, str2, str3, j, str4, wVar);
        this.f3485a = list;
    }

    public p(String str, String str2, String str3, long j, String str4, List<a> list) {
        super(str, str2, str3, j, str4, w.FAQ_LIST);
        this.f3485a = list;
    }

    public p(String str, String str2, String str3, long j, String str4, List<a> list, boolean z, String str5) {
        super(str, str2, str3, j, str4, w.FAQ_LIST);
        this.f3485a = list;
        this.f3486b = z;
        this.c = str5;
    }

    public void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof p) {
            this.f3485a = ((p) vVar).f3485a;
        }
    }

    public void a(o oVar, c cVar, String str, String str2) {
        if (k.a(this.c)) {
            if (k.a(this.c)) {
                this.c = str2;
                this.A.f().a(this);
            }
            if (b()) {
                a(oVar, cVar);
            }
        }
    }

    public final void a(o oVar, c cVar) {
        if (!k.a(this.c)) {
            HashMap<String, String> a2 = com.helpshift.common.c.b.o.a(cVar);
            if (oVar.a()) {
                a2.put("preissue_id", oVar.c());
            } else {
                a2.put("issue_id", oVar.b());
            }
            a2.put("message_id", this.m);
            a2.put("faq_publish_id", this.c);
            try {
                new j(new g(new s(new com.helpshift.common.c.b.k(new q("/faqs_suggestion_read/", this.z, this.A), this.A, new com.helpshift.common.c.a.c(), "/faqs_suggestion_read/", this.m), this.A))).a(new i(a2));
                c();
            } catch (RootAPIException e) {
                if (e.c == b.NON_RETRIABLE) {
                    c();
                    return;
                }
                throw e;
            }
        }
    }

    public final boolean b() {
        return !this.f3486b;
    }

    private void c() {
        this.f3486b = true;
        this.A.f().a(this);
    }

    /* compiled from: FAQListMessageDM */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public final String f3487a;

        /* renamed from: b  reason: collision with root package name */
        public final String f3488b;
        public final String c;

        public a(String str, String str2, String str3) {
            this.f3487a = str;
            this.f3488b = str2;
            this.c = str3;
        }
    }
}
