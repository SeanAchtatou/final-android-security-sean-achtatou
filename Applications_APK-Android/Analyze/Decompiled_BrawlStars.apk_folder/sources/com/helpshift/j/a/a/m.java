package com.helpshift.j.a.a;

import com.helpshift.a.b.c;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.exception.b;
import com.helpshift.common.k;
import com.helpshift.j.a.o;
import java.util.HashMap;

/* compiled from: ConfirmationAcceptedMessageDM */
public class m extends l {
    public final boolean a() {
        return false;
    }

    public m(String str, String str2, long j, String str3, int i) {
        super(str, str2, j, str3, false, w.CONFIRMATION_ACCEPTED, i);
    }

    public final void a(c cVar, o oVar) {
        if (!k.a(oVar.b())) {
            HashMap<String, String> a2 = com.helpshift.common.c.b.o.a(cVar);
            a2.put("body", this.n);
            a2.put("type", "ca");
            a2.put("refers", "");
            try {
                m e = this.A.l().e(a(b(oVar), a2).f3323b);
                a(e);
                this.m = e.m;
                this.o = e.o;
                this.A.f().a(this);
            } catch (RootAPIException e2) {
                if (e2.c == b.INVALID_AUTH_TOKEN || e2.c == b.AUTH_TOKEN_NOT_PROVIDED) {
                    this.z.j.a(cVar, e2.c);
                }
                throw e2;
            }
        } else {
            throw new UnsupportedOperationException("ConfirmationAcceptedMessageDM send called with conversation in pre issue mode.");
        }
    }
}
