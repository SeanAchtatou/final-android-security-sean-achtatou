package com.helpshift.j.a.a;

import com.helpshift.j.a.a.a.b;
import java.util.List;

/* compiled from: AdminMessageWithOptionInputDM */
public class i extends h {

    /* renamed from: a  reason: collision with root package name */
    public b f3479a;

    public i(String str, String str2, String str3, long j, String str4, String str5, boolean z, String str6, String str7, List<b.a> list, b.C0138b bVar) {
        super(str, str2, str3, j, str4, w.ADMIN_TEXT_WITH_OPTION_INPUT);
        this.f3479a = new b(str5, z, str6, str7, list, bVar);
    }

    public final void a(v vVar) {
        super.a(vVar);
        if (vVar instanceof i) {
            this.f3479a = ((i) vVar).f3479a;
        }
    }
}
