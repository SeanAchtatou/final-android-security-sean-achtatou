package com.helpshift.j.a;

import com.helpshift.a.b.c;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.j.a.a.m;
import com.helpshift.j.a.a.n;
import com.helpshift.j.a.a.s;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.b.a;
import com.helpshift.j.a.x;
import com.helpshift.j.d.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: ViewableConversationHistory */
public class z extends x {
    private List<a> h = new ArrayList();

    public final boolean g() {
        return true;
    }

    public z(ab abVar, j jVar, c cVar, com.helpshift.j.e.c cVar2, b bVar) {
        super(abVar, jVar, cVar, cVar2, bVar);
    }

    public final x.a a() {
        return x.a.HISTORY;
    }

    public final synchronized void f() {
        this.h = this.f3547a.b();
        for (a next : this.h) {
            next.t = this.d.f3176a.longValue();
            if (next.g == e.RESOLUTION_REQUESTED && next.j != null && next.j.size() > 0) {
                v vVar = null;
                for (int size = next.j.size() - 1; size >= 0; size--) {
                    vVar = (v) next.j.get(size);
                    if (!(vVar instanceof s) && !(vVar instanceof com.helpshift.j.a.a.z)) {
                        break;
                    }
                }
                if (vVar instanceof m) {
                    next.g = e.RESOLUTION_ACCEPTED;
                } else if (vVar instanceof n) {
                    next.g = e.RESOLUTION_REJECTED;
                }
            }
            Iterator<E> it = next.j.iterator();
            while (it.hasNext()) {
                ((v) it.next()).a(this.c, this.f3548b);
            }
        }
    }

    public final synchronized a h() {
        return this.h.get(this.h.size() - 1);
    }

    public final synchronized void i() {
        long longValue = h().f3504b.longValue();
        for (a next : this.h) {
            this.f.d(next, next.f3504b.equals(Long.valueOf(longValue)));
        }
    }

    public final synchronized void a(com.helpshift.common.g.c<v> cVar) {
        for (a next : this.h) {
            next.j.f3383a = cVar;
            next.f();
        }
    }

    public final synchronized List<a> j() {
        return new ArrayList(this.h);
    }

    public final synchronized void a(List<a> list) {
        HashMap hashMap = new HashMap();
        for (a next : this.h) {
            hashMap.put(next.f3504b, next);
        }
        int size = list.size();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < size; i++) {
            a aVar = list.get(i);
            a aVar2 = (a) hashMap.get(aVar.f3504b);
            if (aVar2 != null) {
                aVar2.j.a(aVar.j);
            } else {
                arrayList.add(aVar);
            }
        }
        if (!com.helpshift.common.j.a(arrayList)) {
            this.h.addAll(0, arrayList);
        }
    }

    public final synchronized u k() {
        if (com.helpshift.common.j.a(this.h)) {
            return null;
        }
        return c(this.h.get(0));
    }

    public final synchronized void b(a aVar) {
        aVar.C = this;
        this.h.add(aVar);
    }
}
