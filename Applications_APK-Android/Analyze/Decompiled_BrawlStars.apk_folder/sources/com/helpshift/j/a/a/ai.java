package com.helpshift.j.a.a;

/* compiled from: UIViewState */
public class ai {

    /* renamed from: a  reason: collision with root package name */
    public boolean f3454a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f3455b;

    public ai() {
        this(false, false);
    }

    public ai(boolean z, boolean z2) {
        this.f3454a = z;
        this.f3455b = z2;
    }

    public final boolean a() {
        return this.f3454a;
    }

    public final boolean b() {
        return this.f3455b;
    }

    public boolean equals(Object obj) {
        ai aiVar = (ai) obj;
        return aiVar != null && aiVar.f3454a == this.f3454a && aiVar.f3455b == this.f3455b;
    }
}
