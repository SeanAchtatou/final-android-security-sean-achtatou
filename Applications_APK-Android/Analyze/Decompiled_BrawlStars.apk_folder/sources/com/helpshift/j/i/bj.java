package com.helpshift.j.i;

import com.helpshift.common.c.l;

/* compiled from: NewConversationVM */
class bj extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f3680a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ aw f3681b;

    bj(aw awVar, long j) {
        this.f3681b = awVar;
        this.f3680a = j;
    }

    public final void a() {
        if (this.f3681b.n.get() != null) {
            av avVar = this.f3681b.n.get();
            if (!this.f3681b.c.a("gotoConversationAfterContactUs") || this.f3681b.c.a("disableInAppConversation")) {
                avVar.c();
                avVar.b();
                return;
            }
            avVar.a();
        }
    }
}
