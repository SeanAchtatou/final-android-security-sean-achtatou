package com.helpshift.j.i;

import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.g.b;
import com.helpshift.common.k;
import com.helpshift.j.a.a.ad;
import com.helpshift.j.a.a.ae;
import com.helpshift.j.a.a.af;
import com.helpshift.j.a.a.ag;
import com.helpshift.j.a.a.al;
import com.helpshift.j.a.a.am;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.w;
import com.helpshift.j.d.e;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: MessageListVM */
public class an {

    /* renamed from: a  reason: collision with root package name */
    protected final ab f3642a;

    /* renamed from: b  reason: collision with root package name */
    protected final j f3643b;
    au c;
    List<v> d;
    private Map<Long, w> e = new ConcurrentHashMap();
    private long f;

    public an(ab abVar, j jVar) {
        this.f3642a = abVar;
        this.f3643b = jVar;
        this.f = this.f3642a.d().v();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.i.an.a(java.util.Date, boolean, java.lang.Long):com.helpshift.j.a.a.ad
     arg types: [java.util.Date, int, java.lang.Long]
     candidates:
      com.helpshift.j.i.an.a(long, int, int):int
      com.helpshift.j.i.an.a(com.helpshift.j.a.a.v, boolean, boolean):boolean
      com.helpshift.j.i.an.a(java.util.List<com.helpshift.j.a.a.v>, int, int):com.helpshift.util.aa<java.lang.Integer, java.lang.Integer>
      com.helpshift.j.i.an.a(java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.a.v, boolean):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.j.i.an.a(java.util.Date, boolean, java.lang.Long):com.helpshift.j.a.a.ad */
    public final void a(List<w> list, List<v> list2, boolean z, au auVar) {
        a(list);
        this.d = a(c(c((Collection<? extends v>) list2)), (v) null, z);
        if (!com.helpshift.common.j.a(this.d)) {
            List<v> list3 = this.d;
            v vVar = list3.get(list3.size() - 1);
            Long l = vVar.q;
            w wVar = list.get(list.size() - 1);
            if (!Long.valueOf(wVar.f3545a).equals(l)) {
                w a2 = a(vVar.q.longValue());
                boolean z2 = !(a2 != null && a2.h) && c(vVar) == e.REJECTED;
                Date date = new Date(wVar.d);
                ae a3 = a(date, z2);
                ad a4 = a(date, false, Long.valueOf(wVar.f3545a));
                this.d.add(a3);
                this.d.add(a4);
            }
        }
        List<v> list4 = this.d;
        a(list4, 0, list4.size() - 1);
        this.c = auVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.List<com.helpshift.j.a.a.v> c(java.util.List<com.helpshift.j.a.a.v> r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0042 }
            r0.<init>()     // Catch:{ all -> 0x0042 }
            boolean r1 = com.helpshift.common.j.a(r8)     // Catch:{ all -> 0x0042 }
            if (r1 == 0) goto L_0x000e
            monitor-exit(r7)
            return r0
        L_0x000e:
            java.util.Iterator r8 = r8.iterator()     // Catch:{ all -> 0x0042 }
            r1 = 0
            r2 = 0
            r3 = r1
            r4 = 0
        L_0x0016:
            boolean r5 = r8.hasNext()     // Catch:{ all -> 0x0042 }
            if (r5 == 0) goto L_0x0039
            java.lang.Object r5 = r8.next()     // Catch:{ all -> 0x0042 }
            com.helpshift.j.a.a.v r5 = (com.helpshift.j.a.a.v) r5     // Catch:{ all -> 0x0042 }
            boolean r6 = r5 instanceof com.helpshift.j.a.a.ah     // Catch:{ all -> 0x0042 }
            if (r6 == 0) goto L_0x002c
            int r4 = r4 + 1
            com.helpshift.j.a.a.ah r5 = (com.helpshift.j.a.a.ah) r5     // Catch:{ all -> 0x0042 }
            r3 = r5
            goto L_0x0016
        L_0x002c:
            if (r3 == 0) goto L_0x0035
            r3.f3453a = r4     // Catch:{ all -> 0x0042 }
            r0.add(r3)     // Catch:{ all -> 0x0042 }
            r3 = r1
            r4 = 0
        L_0x0035:
            r0.add(r5)     // Catch:{ all -> 0x0042 }
            goto L_0x0016
        L_0x0039:
            if (r3 == 0) goto L_0x0040
            r3.f3453a = r4     // Catch:{ all -> 0x0042 }
            r0.add(r3)     // Catch:{ all -> 0x0042 }
        L_0x0040:
            monitor-exit(r7)
            return r0
        L_0x0042:
            r8 = move-exception
            monitor-exit(r7)
            goto L_0x0046
        L_0x0045:
            throw r8
        L_0x0046:
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.i.an.c(java.util.List):java.util.List");
    }

    private synchronized List<v> b(Collection<? extends v> collection) {
        ArrayList arrayList = new ArrayList();
        if (collection == null) {
            return arrayList;
        }
        for (v vVar : collection) {
            if (vVar.a()) {
                arrayList.add(vVar);
            }
        }
        return arrayList;
    }

    private static boolean a(v vVar, v vVar2) {
        if (vVar == null || vVar2 == null) {
            return false;
        }
        return !vVar.q.equals(vVar2.q);
    }

    private e c(v vVar) {
        if (vVar == null) {
            return e.UNKNOWN;
        }
        w a2 = a(vVar.q.longValue());
        if (a2 == null) {
            return e.UNKNOWN;
        }
        return a2.g;
    }

    /* access modifiers changed from: package-private */
    public synchronized List<v> a(List<v> list, v vVar, boolean z) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        boolean a2 = this.f3643b.f3285b.a("showConversationInfoScreen");
        boolean z2 = !z && vVar == null;
        for (v next : list) {
            arrayList.addAll(a(vVar, next, z2, a2));
            arrayList.add(next);
            vVar = next;
            z2 = false;
        }
        return arrayList;
    }

    private ae b(v vVar, v vVar2) {
        if (vVar == null || vVar2 == null || !a(vVar, vVar2)) {
            return null;
        }
        w a2 = a(vVar.q.longValue());
        boolean z = true;
        if ((a2 != null && a2.h) || c(vVar) != e.REJECTED) {
            z = false;
        }
        ae a3 = a(d(vVar2), z);
        a3.q = vVar2.q;
        return a3;
    }

    /* access modifiers changed from: private */
    public List<af> a(v vVar, v vVar2, boolean z, boolean z2) {
        if (vVar2 == null) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList();
        ae b2 = b(vVar, vVar2);
        if (b2 != null) {
            arrayList.add(b2);
        }
        boolean a2 = a(vVar, vVar2);
        Date d2 = d(vVar2);
        w a3 = a(vVar2.q.longValue());
        boolean z3 = true;
        if (!(a3 != null && a3.h)) {
            if (a2 || z) {
                if (a3 != null) {
                    if (!z2 || a3.f || k.a(a3.e)) {
                        z3 = false;
                    }
                    if (z3) {
                        ag a4 = a(a3.e, d2, z, vVar2.q);
                        a4.q = vVar2.q;
                        arrayList.add(a4);
                        z = false;
                    }
                }
                ad a5 = a(d2, z, vVar2.q);
                a5.q = vVar2.q;
                arrayList.add(a5);
            } else if (c(vVar, vVar2) && !(vVar2 instanceof ad)) {
                ad a6 = a(d2, z, vVar2.q);
                a6.q = vVar2.q;
                arrayList.add(a6);
            }
        }
        return arrayList;
    }

    private ad a(Date date, boolean z, Long l) {
        String a2 = b.f3381a.a(new Date(date.getTime()));
        ad adVar = new ad(a2, b.b(a2) - 1, z);
        adVar.a(this.f3643b, this.f3642a);
        adVar.q = l;
        return adVar;
    }

    private ae a(Date date, boolean z) {
        String a2 = b.f3381a.a(new Date(date.getTime()));
        ae aeVar = new ae(a2, b.b(a2) - 1, z);
        aeVar.a(this.f3643b, this.f3642a);
        return aeVar;
    }

    private ag a(String str, Date date, boolean z, Long l) {
        String a2 = b.f3381a.a(new Date(date.getTime()));
        ag agVar = new ag(str, a2, b.b(a2) - 1, z);
        agVar.a(this.f3643b, this.f3642a);
        agVar.q = l;
        return agVar;
    }

    private static Date d(v vVar) {
        return new Date(vVar.C);
    }

    private boolean a(long j, long j2) {
        long j3 = this.f;
        return (j + j3) / 86400000 != (j2 + j3) / 86400000;
    }

    private Comparator<v> c() {
        return new ao(this);
    }

    private List<v> c(Collection<? extends v> collection) {
        List<v> b2 = b(collection);
        Collections.sort(b2, c());
        return b2;
    }

    public final void a(Collection<? extends v> collection) {
        List<v> c2 = c(collection);
        if (c2.size() > 0) {
            this.f3643b.c(new ap(this, c2));
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(v vVar) {
        int e2 = e(vVar);
        this.d.add(e2, vVar);
        b(e2);
        a(this.d, e2 - 1, e2 + 1);
    }

    public final void b(v vVar) {
        if (vVar != null && vVar.a()) {
            this.f3643b.c(new aq(this, vVar));
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        au auVar = this.c;
        if (auVar != null) {
            auVar.G();
        }
    }

    /* access modifiers changed from: package-private */
    public v a(int i) {
        if (i < 0 || i >= this.d.size()) {
            return null;
        }
        return this.d.get(i);
    }

    private int e(v vVar) {
        int a2;
        int size = this.d.size();
        if (size != 0 && (a2 = a(vVar.C, 0, size - 1)) >= 0) {
            return a2 > size ? size : a2;
        }
        return 0;
    }

    private int a(long j, int i, int i2) {
        while (true) {
            int i3 = ((i2 - i) / 2) + i;
            if (i == i3) {
                break;
            } else if (this.d.get(i3).C <= j) {
                i = i3;
            } else {
                i2 = i3;
            }
        }
        long j2 = this.d.get(i).C;
        long j3 = this.d.get(i2).C;
        if (j < j2) {
            return i;
        }
        return j >= j3 ? i2 + 1 : i2;
    }

    private static boolean g(v vVar) {
        if (vVar == null) {
            return false;
        }
        if (vVar.k == com.helpshift.j.a.a.w.USER_TEXT || vVar.k == com.helpshift.j.a.a.w.USER_RESP_FOR_TEXT_INPUT || vVar.k == com.helpshift.j.a.a.w.USER_RESP_FOR_OPTION_INPUT) {
            if (((al) vVar).c == am.SENT) {
                return true;
            }
            return false;
        } else if (vVar.k == com.helpshift.j.a.a.w.SCREENSHOT && ((com.helpshift.j.a.a.ab) vVar).D == am.SENT) {
            return true;
        } else {
            return false;
        }
    }

    private boolean d(v vVar, v vVar2) {
        if (vVar == null || vVar2 == null || k.a(vVar2.n)) {
            return false;
        }
        if ((f(vVar) && f(vVar2)) || (vVar.j && vVar2.j)) {
            long j = vVar.C;
            long j2 = vVar2.C;
            long j3 = this.f;
            if ((j + j3) / 60000 != (j2 + j3) / 60000) {
                return false;
            }
            if (!f(vVar)) {
                String j4 = vVar.j();
                String j5 = vVar2.j();
                if (j4 != null) {
                    return j4.equals(j5);
                }
                if (j5 == null) {
                    return true;
                }
                return false;
            } else if (!g(vVar) || !g(vVar2)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.i.an.a(com.helpshift.j.a.a.v, boolean, boolean):boolean
     arg types: [com.helpshift.j.a.a.v, boolean, int]
     candidates:
      com.helpshift.j.i.an.a(long, int, int):int
      com.helpshift.j.i.an.a(java.util.Date, boolean, java.lang.Long):com.helpshift.j.a.a.ad
      com.helpshift.j.i.an.a(java.util.List<com.helpshift.j.a.a.v>, int, int):com.helpshift.util.aa<java.lang.Integer, java.lang.Integer>
      com.helpshift.j.i.an.a(java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.a.v, boolean):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.j.i.an.a(com.helpshift.j.a.a.v, boolean, boolean):boolean */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a1, code lost:
        return r3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0058  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.helpshift.util.aa<java.lang.Integer, java.lang.Integer> a(java.util.List<com.helpshift.j.a.a.v> r11, int r12, int r13) {
        /*
            r10 = this;
            monitor-enter(r10)
            int r0 = r11.size()     // Catch:{ all -> 0x00a2 }
            r1 = 0
            int r12 = java.lang.Math.max(r12, r1)     // Catch:{ all -> 0x00a2 }
            r2 = 1
            int r0 = r0 - r2
            int r13 = java.lang.Math.min(r13, r0)     // Catch:{ all -> 0x00a2 }
            r3 = 0
            if (r13 >= r12) goto L_0x0015
            monitor-exit(r10)
            return r3
        L_0x0015:
            r4 = -1
            if (r12 <= 0) goto L_0x005d
            int r5 = r12 + -1
            java.lang.Object r6 = r11.get(r5)     // Catch:{ all -> 0x00a2 }
            com.helpshift.j.a.a.v r6 = (com.helpshift.j.a.a.v) r6     // Catch:{ all -> 0x00a2 }
            if (r5 >= 0) goto L_0x0024
        L_0x0022:
            r7 = 0
            goto L_0x0041
        L_0x0024:
            if (r5 != 0) goto L_0x0028
        L_0x0026:
            r7 = 1
            goto L_0x0041
        L_0x0028:
            java.util.List<com.helpshift.j.a.a.v> r7 = r10.d     // Catch:{ all -> 0x00a2 }
            java.lang.Object r7 = r7.get(r5)     // Catch:{ all -> 0x00a2 }
            com.helpshift.j.a.a.v r7 = (com.helpshift.j.a.a.v) r7     // Catch:{ all -> 0x00a2 }
            java.util.List<com.helpshift.j.a.a.v> r8 = r10.d     // Catch:{ all -> 0x00a2 }
            int r9 = r5 + -1
            java.lang.Object r8 = r8.get(r9)     // Catch:{ all -> 0x00a2 }
            com.helpshift.j.a.a.v r8 = (com.helpshift.j.a.a.v) r8     // Catch:{ all -> 0x00a2 }
            boolean r7 = r10.d(r8, r7)     // Catch:{ all -> 0x00a2 }
            if (r7 != 0) goto L_0x0022
            goto L_0x0026
        L_0x0041:
            java.lang.Object r8 = r11.get(r12)     // Catch:{ all -> 0x00a2 }
            com.helpshift.j.a.a.v r8 = (com.helpshift.j.a.a.v) r8     // Catch:{ all -> 0x00a2 }
            boolean r8 = r10.d(r6, r8)     // Catch:{ all -> 0x00a2 }
            if (r8 != 0) goto L_0x004f
            r9 = 1
            goto L_0x0050
        L_0x004f:
            r9 = 0
        L_0x0050:
            boolean r6 = r10.a(r6, r7, r9)     // Catch:{ all -> 0x00a2 }
            if (r6 == 0) goto L_0x0058
            r6 = r5
            goto L_0x005a
        L_0x0058:
            r5 = -1
            r6 = -1
        L_0x005a:
            r7 = r8 ^ 1
            goto L_0x0060
        L_0x005d:
            r5 = -1
            r6 = -1
            r7 = 1
        L_0x0060:
            if (r12 > r13) goto L_0x0091
            java.lang.Object r8 = r11.get(r12)     // Catch:{ all -> 0x00a2 }
            com.helpshift.j.a.a.v r8 = (com.helpshift.j.a.a.v) r8     // Catch:{ all -> 0x00a2 }
            if (r12 != r0) goto L_0x006f
            boolean r8 = r10.a(r8, r7, r2)     // Catch:{ all -> 0x00a2 }
            goto L_0x0088
        L_0x006f:
            int r9 = r12 + 1
            java.lang.Object r9 = r11.get(r9)     // Catch:{ all -> 0x00a2 }
            com.helpshift.j.a.a.v r9 = (com.helpshift.j.a.a.v) r9     // Catch:{ all -> 0x00a2 }
            boolean r9 = r10.d(r8, r9)     // Catch:{ all -> 0x00a2 }
            if (r9 == 0) goto L_0x0083
            boolean r8 = r10.a(r8, r7, r1)     // Catch:{ all -> 0x00a2 }
            r7 = 0
            goto L_0x0088
        L_0x0083:
            boolean r8 = r10.a(r8, r7, r2)     // Catch:{ all -> 0x00a2 }
            r7 = 1
        L_0x0088:
            if (r8 == 0) goto L_0x008e
            if (r5 != r4) goto L_0x008d
            r5 = r12
        L_0x008d:
            r6 = r12
        L_0x008e:
            int r12 = r12 + 1
            goto L_0x0060
        L_0x0091:
            if (r5 == r4) goto L_0x00a0
            com.helpshift.util.aa r3 = new com.helpshift.util.aa     // Catch:{ all -> 0x00a2 }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x00a2 }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x00a2 }
            r3.<init>(r11, r12)     // Catch:{ all -> 0x00a2 }
        L_0x00a0:
            monitor-exit(r10)
            return r3
        L_0x00a2:
            r11 = move-exception
            monitor-exit(r10)
            goto L_0x00a6
        L_0x00a5:
            throw r11
        L_0x00a6:
            goto L_0x00a5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.i.an.a(java.util.List, int, int):com.helpshift.util.aa");
    }

    /* access modifiers changed from: package-private */
    public final boolean b(int i) {
        boolean z;
        v a2 = a(i);
        v a3 = a(i + 1);
        boolean z2 = false;
        if (!(a2 instanceof ad) || (a3 != null && !(a3 instanceof ad))) {
            z = false;
        } else {
            this.d.remove(i);
            i--;
            z = true;
        }
        v a4 = a(i);
        v a5 = a(i - 1);
        if (a5 == null || a4 == null || (a4 instanceof ad) || !c(a5, a4)) {
            return z;
        }
        Date date = new Date(a4.C);
        if (a4.C == -1) {
            z2 = true;
        }
        this.d.add(i, a(date, z2, a4.q));
        return true;
    }

    public final void a(List<v> list, boolean z) {
        if (!com.helpshift.common.j.a(list)) {
            List<v> b2 = b((Collection<? extends v>) list);
            Collections.sort(b2, c());
            List<v> a2 = a(c(b2), (v) null, z);
            a(a2, 0, a2.size() - 1);
            d(a2);
        } else if (!z) {
            d();
        }
    }

    private void d() {
        this.f3643b.c(new ar(this));
    }

    private void d(List<v> list) {
        if (!com.helpshift.common.j.a(list)) {
            this.f3643b.c(new as(this, list));
        }
    }

    public final synchronized w a(long j) {
        return this.e.get(Long.valueOf(j));
    }

    public final synchronized void a(List<w> list) {
        if (!com.helpshift.common.j.a(list)) {
            this.e.clear();
            for (w next : list) {
                this.e.put(Long.valueOf(next.f3545a), next);
            }
        }
    }

    public final void b(List<v> list) {
        List<v> b2 = b((Collection<? extends v>) list);
        if (!com.helpshift.common.j.a(b2)) {
            this.f3643b.c(new at(this, b2));
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        au auVar = this.c;
        if (auVar != null) {
            auVar.w();
        }
    }

    private boolean c(v vVar, v vVar2) {
        if (vVar == null || vVar2 == null) {
            return false;
        }
        return a(vVar.C, vVar2.C);
    }

    private boolean f(v vVar) {
        return !vVar.j && !(vVar instanceof af);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(com.helpshift.j.a.a.v r4, boolean r5, boolean r6) {
        /*
            r3 = this;
            com.helpshift.j.a.a.ai r0 = r4.l
            r1 = 1
            r2 = 0
            if (r5 == 0) goto L_0x0018
            if (r6 == 0) goto L_0x000e
            com.helpshift.j.a.a.ai r4 = new com.helpshift.j.a.a.ai
            r4.<init>(r1, r2)
            goto L_0x0028
        L_0x000e:
            com.helpshift.j.a.a.ai r5 = new com.helpshift.j.a.a.ai
            boolean r4 = r3.f(r4)
            r5.<init>(r2, r4)
            goto L_0x0021
        L_0x0018:
            if (r6 == 0) goto L_0x0023
            com.helpshift.j.a.a.ai r5 = new com.helpshift.j.a.a.ai
            boolean r4 = r4.j
            r5.<init>(r1, r4)
        L_0x0021:
            r4 = r5
            goto L_0x0028
        L_0x0023:
            com.helpshift.j.a.a.ai r4 = new com.helpshift.j.a.a.ai
            r4.<init>(r2, r1)
        L_0x0028:
            boolean r5 = r0.equals(r4)
            if (r5 == 0) goto L_0x002f
            return r2
        L_0x002f:
            boolean r5 = r4.f3454a
            r0.f3454a = r5
            boolean r4 = r4.f3455b
            r0.f3455b = r4
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.i.an.a(com.helpshift.j.a.a.v, boolean, boolean):boolean");
    }

    static /* synthetic */ void a(an anVar, List list) {
        boolean z;
        Iterator it = list.iterator();
        while (true) {
            if (it.hasNext()) {
                if (((v) it.next()).j) {
                    z = true;
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        au auVar = anVar.c;
        if (auVar == null) {
            return;
        }
        if (z) {
            auVar.E();
        } else {
            auVar.F();
        }
    }
}
