package com.helpshift.j.i;

import com.helpshift.common.c.l;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.al;
import com.helpshift.j.a.a.an;
import com.helpshift.j.a.a.i;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.w;
import com.helpshift.j.a.a.x;
import com.helpshift.j.a.b.a;
import com.helpshift.j.c;
import com.helpshift.util.aa;

/* compiled from: ConversationalVM */
class g extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x f3700a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b.a f3701b;
    final /* synthetic */ boolean c;
    final /* synthetic */ b d;

    g(b bVar, x xVar, b.a aVar, boolean z) {
        this.d = bVar;
        this.f3700a = xVar;
        this.f3701b = aVar;
        this.c = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
     arg types: [com.helpshift.j.a.b.a, com.helpshift.j.a.a.an]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Collection<? extends com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Set<java.lang.Long>):void
      com.helpshift.j.a.b.a(java.util.List<com.helpshift.j.a.a.v>, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, long):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.e):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.f.a):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void
     arg types: [com.helpshift.j.a.b.a, com.helpshift.j.a.a.an]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Collection<? extends com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Set<java.lang.Long>):void
      com.helpshift.j.a.b.a(java.util.List<com.helpshift.j.a.a.v>, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, long):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.e):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.f.a):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void */
    public final void a() {
        String str;
        try {
            com.helpshift.j.a.b bVar = this.d.n;
            a h = this.d.f.h();
            x xVar = this.f3700a;
            b.a aVar = this.f3701b;
            boolean z = this.c;
            aa<String, Long> b2 = com.helpshift.common.g.b.b(bVar.f3501a);
            String str2 = (String) b2.f4242a;
            long longValue = ((Long) b2.f4243b).longValue();
            if (z) {
                str = xVar.f3496a.d;
            } else {
                str = aVar.f3441a;
            }
            an anVar = new an(str, str2, longValue, "mobile", xVar, z);
            anVar.q = h.f3504b;
            anVar.a(true);
            bVar.a(h, (v) anVar);
            if (xVar.f3497b == w.ADMIN_TEXT_WITH_OPTION_INPUT) {
                i iVar = (i) bVar.d.c(xVar.m);
                iVar.f3479a.e.clear();
                bVar.d.a(iVar);
            }
            bVar.a(h, (al) anVar);
            if (c.a(this.d.f.h().g)) {
                b bVar2 = this.d;
                bVar2.b(!bVar2.d);
            }
        } catch (RootAPIException e) {
            b.a(this.d, e);
            throw e;
        }
    }
}
