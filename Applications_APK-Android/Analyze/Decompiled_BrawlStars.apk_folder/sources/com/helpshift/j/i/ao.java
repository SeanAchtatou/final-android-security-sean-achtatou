package com.helpshift.j.i;

import com.helpshift.j.a.a.v;
import com.helpshift.j.a.w;
import java.util.Comparator;

/* compiled from: MessageListVM */
class ao implements Comparator<v> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ an f3644a;

    ao(an anVar) {
        this.f3644a = anVar;
    }

    public /* synthetic */ int compare(Object obj, Object obj2) {
        v vVar = (v) obj;
        v vVar2 = (v) obj2;
        w a2 = this.f3644a.a(vVar.q.longValue());
        w a3 = this.f3644a.a(vVar2.q.longValue());
        if (!(a2 == null || a3 == null)) {
            Integer valueOf = Integer.valueOf(a2.f3546b);
            Integer valueOf2 = Integer.valueOf(a3.f3546b);
            if (valueOf.intValue() >= valueOf2.intValue()) {
                if (valueOf.intValue() > valueOf2.intValue()) {
                    return 1;
                }
                long j = vVar.C;
                long j2 = vVar2.C;
                if (j > j2) {
                    return 1;
                }
                if (j < j2) {
                    return -1;
                }
            }
            return -1;
        }
        return 0;
    }
}
