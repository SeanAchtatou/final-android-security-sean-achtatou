package com.helpshift.j.i;

import com.helpshift.common.c.l;
import com.helpshift.common.exception.RootAPIException;

/* compiled from: NewConversationVM */
class bk extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Exception f3682a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ aw f3683b;

    bk(aw awVar, Exception exc) {
        this.f3683b = awVar;
        this.f3682a = exc;
    }

    public final void a() {
        Exception exc = this.f3682a;
        if (exc instanceof RootAPIException) {
            RootAPIException rootAPIException = (RootAPIException) exc;
            if (this.f3683b.n.get() != null) {
                this.f3683b.n.get().a(rootAPIException.c);
            }
        }
    }
}
