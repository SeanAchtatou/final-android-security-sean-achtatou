package com.helpshift.j.i;

import com.helpshift.common.c.l;

/* compiled from: NewConversationVM */
class bh extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f3676a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ aw f3677b;

    bh(aw awVar, boolean z) {
        this.f3677b = awVar;
        this.f3676a = z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r8 = this;
            com.helpshift.j.i.aw r0 = r8.f3677b
            com.helpshift.z.o r1 = r0.f
            com.helpshift.z.o r2 = r0.f
            java.lang.String r2 = r2.b()
            int r3 = r2.length()
            r4 = 0
            if (r3 != 0) goto L_0x0014
            com.helpshift.z.s$a r2 = com.helpshift.z.s.a.EMPTY
            goto L_0x0033
        L_0x0014:
            java.util.regex.Pattern r3 = com.helpshift.z.s.e
            java.util.regex.Matcher r3 = r3.matcher(r2)
            boolean r3 = r3.matches()
            if (r3 == 0) goto L_0x0023
            com.helpshift.z.s$a r2 = com.helpshift.z.s.a.ONLY_SPECIAL_CHARACTERS
            goto L_0x0033
        L_0x0023:
            int r2 = r2.length()
            com.helpshift.i.a.a r3 = r0.c
            int r3 = r3.h()
            if (r2 >= r3) goto L_0x0032
            com.helpshift.z.s$a r2 = com.helpshift.z.s.a.LESS_THAN_MINIMUM_LENGTH
            goto L_0x0033
        L_0x0032:
            r2 = r4
        L_0x0033:
            r1.a(r2)
            com.helpshift.z.o r1 = r0.g
            com.helpshift.z.o r2 = r0.g
            java.lang.String r2 = r2.b()
            int r3 = r2.length()
            if (r3 != 0) goto L_0x0047
            com.helpshift.z.s$a r2 = com.helpshift.z.s.a.EMPTY
            goto L_0x0057
        L_0x0047:
            java.util.regex.Pattern r3 = com.helpshift.z.s.e
            java.util.regex.Matcher r2 = r3.matcher(r2)
            boolean r2 = r2.matches()
            if (r2 == 0) goto L_0x0056
            com.helpshift.z.s$a r2 = com.helpshift.z.s.a.ONLY_SPECIAL_CHARACTERS
            goto L_0x0057
        L_0x0056:
            r2 = r4
        L_0x0057:
            r1.a(r2)
            com.helpshift.z.o r1 = r0.h
            com.helpshift.z.o r2 = r0.h
            java.lang.String r2 = r2.b()
            int r3 = r2.length()
            if (r3 != 0) goto L_0x0071
            com.helpshift.z.o r2 = r0.h
            boolean r2 = r2.d
            if (r2 == 0) goto L_0x007a
            com.helpshift.z.s$a r2 = com.helpshift.z.s.a.EMPTY
            goto L_0x007b
        L_0x0071:
            boolean r2 = com.helpshift.util.o.a(r2)
            if (r2 != 0) goto L_0x007a
            com.helpshift.z.s$a r2 = com.helpshift.z.s.a.INVALID_EMAIL
            goto L_0x007b
        L_0x007a:
            r2 = r4
        L_0x007b:
            r1.a(r2)
            com.helpshift.z.o r1 = r0.f
            com.helpshift.z.s$a r1 = r1.a()
            r2 = 1
            r3 = 0
            if (r1 != 0) goto L_0x009a
            com.helpshift.z.o r1 = r0.g
            com.helpshift.z.s$a r1 = r1.a()
            if (r1 != 0) goto L_0x009a
            com.helpshift.z.o r0 = r0.h
            com.helpshift.z.s$a r0 = r0.a()
            if (r0 != 0) goto L_0x009a
            r0 = 1
            goto L_0x009b
        L_0x009a:
            r0 = 0
        L_0x009b:
            if (r0 == 0) goto L_0x0145
            boolean r0 = r8.f3676a
            if (r0 == 0) goto L_0x00d9
            com.helpshift.j.i.aw r0 = r8.f3677b
            boolean r0 = r0.d()
            if (r0 == 0) goto L_0x00d9
            com.helpshift.j.i.aw r0 = r8.f3677b
            com.helpshift.j.c.a r0 = r0.d
            com.helpshift.j.i.aw r1 = r8.f3677b
            com.helpshift.z.o r1 = r1.f
            java.lang.String r1 = r1.b()
            com.helpshift.n.b.a r0 = r0.i
            java.util.ArrayList r0 = r0.a(r1)
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x00d9
            com.helpshift.j.i.aw r1 = r8.f3677b
            java.lang.ref.WeakReference<com.helpshift.j.i.av> r1 = r1.n
            java.lang.Object r1 = r1.get()
            if (r1 == 0) goto L_0x00d8
            com.helpshift.j.i.aw r1 = r8.f3677b
            java.lang.ref.WeakReference<com.helpshift.j.i.av> r1 = r1.n
            java.lang.Object r1 = r1.get()
            com.helpshift.j.i.av r1 = (com.helpshift.j.i.av) r1
            r1.a(r0)
        L_0x00d8:
            return
        L_0x00d9:
            com.helpshift.j.i.aw r0 = r8.f3677b
            com.helpshift.j.c.a r0 = r0.d
            com.helpshift.j.a.b.a r0 = r0.k()
            if (r0 == 0) goto L_0x00f0
            com.helpshift.j.i.aw r1 = r8.f3677b
            com.helpshift.common.c.j r1 = r1.f3654a
            com.helpshift.j.i.bi r2 = new com.helpshift.j.i.bi
            r2.<init>(r8, r0)
            r1.c(r2)
            goto L_0x0145
        L_0x00f0:
            java.lang.String r0 = "Helpshift_NewConvVM"
            java.lang.String r1 = "Creating new conversation"
            com.helpshift.util.n.a(r0, r1, r4, r4)
            com.helpshift.j.i.aw r0 = r8.f3677b
            com.helpshift.z.h r0 = r0.j
            r0.b(r2)
            com.helpshift.j.i.aw r0 = r8.f3677b
            com.helpshift.z.h r0 = r0.k
            r0.b(r3)
            com.helpshift.j.i.aw r0 = r8.f3677b
            com.helpshift.z.h r0 = r0.l
            r0.b(r3)
            com.helpshift.j.i.aw r0 = r8.f3677b
            com.helpshift.z.k r0 = r0.i
            r0.a(r3)
            com.helpshift.j.i.aw r0 = r8.f3677b
            com.helpshift.j.c.a r0 = r0.d
            com.helpshift.j.i.aw r1 = r8.f3677b
            com.helpshift.z.o r1 = r1.f
            java.lang.String r3 = r1.b()
            com.helpshift.j.i.aw r1 = r8.f3677b
            com.helpshift.z.o r1 = r1.g
            java.lang.String r4 = r1.b()
            com.helpshift.j.i.aw r1 = r8.f3677b
            com.helpshift.z.o r1 = r1.h
            java.lang.String r5 = r1.b()
            com.helpshift.j.i.aw r1 = r8.f3677b
            com.helpshift.z.k r1 = r1.i
            com.helpshift.j.d.d r6 = r1.a()
            com.helpshift.j.c.a$a r7 = new com.helpshift.j.c.a$a
            r1 = r7
            r2 = r0
            r1.<init>(r3, r4, r5, r6)
            com.helpshift.common.c.j r0 = r0.f
            com.helpshift.common.c.l r1 = r7.e
            r0.b(r1)
        L_0x0145:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.i.bh.a():void");
    }
}
