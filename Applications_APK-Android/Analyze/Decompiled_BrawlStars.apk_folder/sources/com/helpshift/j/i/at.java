package com.helpshift.j.i;

import com.helpshift.common.c.l;
import com.helpshift.j.a.a.v;
import java.util.List;

/* compiled from: MessageListVM */
class at extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f3652a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ an f3653b;

    at(an anVar, List list) {
        this.f3653b = anVar;
        this.f3652a = list;
    }

    public final void a() {
        boolean z = false;
        for (v indexOf : this.f3652a) {
            int indexOf2 = this.f3653b.d.indexOf(indexOf);
            if (indexOf2 != -1) {
                this.f3653b.d.remove(indexOf2);
                int i = indexOf2 - 1;
                this.f3653b.b(i);
                an anVar = this.f3653b;
                anVar.a(anVar.d, i, indexOf2 + 1);
                z = true;
            }
        }
        if (z) {
            this.f3653b.a();
            this.f3653b.b();
        }
    }
}
