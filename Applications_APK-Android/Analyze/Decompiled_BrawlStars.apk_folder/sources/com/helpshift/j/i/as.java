package com.helpshift.j.i;

import com.helpshift.common.c.l;
import com.helpshift.common.j;
import com.helpshift.j.a.a.v;
import com.helpshift.util.aa;
import java.util.List;

/* compiled from: MessageListVM */
class as extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f3650a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ an f3651b;

    as(an anVar, List list) {
        this.f3651b = anVar;
        this.f3650a = list;
    }

    public final void a() {
        List list = this.f3650a;
        this.f3650a.addAll(this.f3651b.a((v) list.get(list.size() - 1), !j.a(this.f3651b.d) ? this.f3651b.d.get(0) : null, false, this.f3651b.f3643b.f3285b.a("showConversationInfoScreen")));
        int size = this.f3650a.size();
        this.f3651b.d.addAll(0, this.f3650a);
        if (this.f3651b.c != null) {
            this.f3651b.c.a(0, size);
        }
        int i = size - 1;
        boolean b2 = this.f3651b.b(i);
        an anVar = this.f3651b;
        aa<Integer, Integer> a2 = anVar.a(anVar.d, i, size + 1);
        if (b2) {
            this.f3651b.a();
        } else if (a2 != null) {
            an anVar2 = this.f3651b;
            if (a2 != null) {
                int intValue = ((Integer) a2.f4242a).intValue();
                int intValue2 = (((Integer) a2.f4243b).intValue() - intValue) + 1;
                if (anVar2.c != null && intValue > 0 && intValue2 > 0 && ((Integer) a2.f4243b).intValue() < anVar2.d.size()) {
                    anVar2.c.b(intValue, intValue2);
                }
            }
        }
    }
}
