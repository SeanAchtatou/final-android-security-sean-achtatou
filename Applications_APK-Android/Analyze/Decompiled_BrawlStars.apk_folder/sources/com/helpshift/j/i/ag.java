package com.helpshift.j.i;

import com.helpshift.common.c.j;
import com.helpshift.common.k;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.x;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ListPickerVM */
class ag {
    static final Object d = new Object();

    /* renamed from: a  reason: collision with root package name */
    j f3627a;
    /* access modifiers changed from: package-private */

    /* renamed from: b  reason: collision with root package name */
    public final x f3628b;
    /* access modifiers changed from: package-private */
    public final am c;

    /* compiled from: ListPickerVM */
    interface c {
        List<b.a> a(List<b.a> list, List<String> list2);
    }

    ag(j jVar, x xVar, am amVar) {
        this.f3627a = jVar;
        this.f3628b = xVar;
        this.c = amVar;
    }

    /* access modifiers changed from: package-private */
    public final List<bn> a() {
        return a(this.f3628b.f3496a.e, (List<String>) null);
    }

    /* access modifiers changed from: package-private */
    public void a(List<bn> list) {
        this.f3627a.c(new al(this, list));
    }

    private List<bn> a(List<b.a> list, List<String> list2) {
        ArrayList arrayList = new ArrayList();
        for (b.a a2 : list) {
            arrayList.add(a(a2, (List<String>) null));
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public bn a(b.a aVar, List<String> list) {
        return new bn(aVar, !com.helpshift.common.j.a(list) ? a(aVar.f3441a, list) : null);
    }

    private static List<af> a(String str, List<String> list) {
        if (k.a(str) || com.helpshift.common.j.a(list)) {
            return null;
        }
        String lowerCase = str.toLowerCase();
        ArrayList arrayList = new ArrayList();
        HashSet hashSet = new HashSet();
        for (String next : list) {
            if (!k.a(next)) {
                String lowerCase2 = next.toLowerCase();
                Matcher matcher = Pattern.compile("\\b" + lowerCase2).matcher(lowerCase);
                while (matcher.find()) {
                    int start = matcher.start();
                    if (!hashSet.contains(Integer.valueOf(start))) {
                        arrayList.add(new af(start, matcher.end() - start));
                        hashSet.add(Integer.valueOf(start));
                    }
                }
            }
        }
        if (com.helpshift.common.j.a(arrayList)) {
            return null;
        }
        return arrayList;
    }

    /* compiled from: ListPickerVM */
    class b implements c {

        /* renamed from: b  reason: collision with root package name */
        private List<c> f3631b;

        b(List<c> list) {
            this.f3631b = list;
        }

        public final List<b.a> a(List<b.a> list, List<String> list2) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (c a2 : this.f3631b) {
                linkedHashSet.addAll(a2.a(list, list2));
            }
            return new ArrayList(linkedHashSet);
        }
    }

    /* compiled from: ListPickerVM */
    abstract class a implements c {
        /* access modifiers changed from: package-private */
        public abstract String a(String str);

        private a() {
        }

        /* synthetic */ a(ag agVar, byte b2) {
            this();
        }

        public final List<b.a> a(List<b.a> list, List<String> list2) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (String a2 : list2) {
                linkedHashSet.addAll(a(list, a(a2)));
            }
            return new ArrayList(linkedHashSet);
        }

        private static List<b.a> a(List<b.a> list, String str) {
            ArrayList arrayList = new ArrayList();
            Pattern compile = Pattern.compile(str);
            for (b.a next : list) {
                if (compile.matcher(next.f3441a.toLowerCase()).find()) {
                    arrayList.add(next);
                }
            }
            return arrayList;
        }
    }

    /* compiled from: ListPickerVM */
    class d extends a {
        d() {
            super(ag.this, (byte) 0);
        }

        /* access modifiers changed from: package-private */
        public final String a(String str) {
            return "^" + str;
        }
    }

    /* compiled from: ListPickerVM */
    class e extends a {
        e() {
            super(ag.this, (byte) 0);
        }

        /* access modifiers changed from: package-private */
        public final String a(String str) {
            return "\\b" + str;
        }
    }

    /* compiled from: ListPickerVM */
    class f extends a {
        f() {
            super(ag.this, (byte) 0);
        }

        /* access modifiers changed from: package-private */
        public final String a(String str) {
            return "\\B" + str;
        }
    }
}
