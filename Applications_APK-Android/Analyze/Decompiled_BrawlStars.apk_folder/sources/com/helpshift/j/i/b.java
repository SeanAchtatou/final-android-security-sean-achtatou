package com.helpshift.j.i;

import com.helpshift.a.a;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.g.c;
import com.helpshift.common.k;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.a.a.ah;
import com.helpshift.j.a.a.al;
import com.helpshift.j.a.a.am;
import com.helpshift.j.a.a.an;
import com.helpshift.j.a.a.ap;
import com.helpshift.j.a.a.o;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.y;
import com.helpshift.j.a.q;
import com.helpshift.j.a.w;
import com.helpshift.j.a.x;
import com.helpshift.j.c.a;
import com.helpshift.j.d.d;
import com.helpshift.j.d.e;
import com.helpshift.util.aa;
import com.helpshift.z.f;
import com.helpshift.z.h;
import com.helpshift.z.i;
import com.helpshift.z.l;
import com.helpshift.z.m;
import com.helpshift.z.n;
import com.helpshift.z.p;
import com.helpshift.z.r;
import com.helpshift.z.t;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/* compiled from: ConversationalVM */
public class b implements a.C0132a, a.b, a, am, au, Observer {
    private boolean A;
    private boolean B;

    /* renamed from: a  reason: collision with root package name */
    boolean f3661a = true;

    /* renamed from: b  reason: collision with root package name */
    boolean f3662b;
    boolean c;
    boolean d;
    boolean e;
    public final x f;
    final com.helpshift.j.c.a g;
    final com.helpshift.i.a.a h;
    q i;
    j j;
    ab k;
    t l;
    an m;
    com.helpshift.j.a.b n;
    protected boolean o;
    m p;
    com.helpshift.z.j q;
    n r;
    h s;
    h t;
    i u;
    h v;
    l w;
    private boolean x;
    /* access modifiers changed from: private */
    public ag y;
    /* access modifiers changed from: private */
    public v z;

    public final /* synthetic */ void a(Object obj) {
        a((Collection<? extends v>) Collections.singletonList((v) obj));
    }

    public final /* synthetic */ void b(Object obj) {
        v vVar = (v) obj;
        com.helpshift.util.n.a("Helpshift_ConvsatnlVM", "update called : " + vVar, (Throwable) null, (com.helpshift.p.b.a[]) null);
        aa();
        an anVar = this.m;
        if (anVar != null) {
            anVar.b(vVar);
        }
    }

    public b(ab abVar, j jVar, com.helpshift.j.c.a aVar, x xVar, q qVar, boolean z2, boolean z3) {
        this.j = jVar;
        this.k = abVar;
        this.g = aVar;
        this.f = xVar;
        this.h = jVar.f3285b;
        this.A = z3;
        this.n = aVar.c;
        this.h.addObserver(this);
        jVar.j.a(this);
        this.l = new t(this.h, aVar);
        this.p = new m();
        this.q = new com.helpshift.z.j();
        this.r = new n(false, false);
        boolean ad = ad();
        com.helpshift.j.a.b.a h2 = xVar.h();
        this.n.b(h2, ad);
        t tVar = this.l;
        i iVar = new i();
        tVar.a(iVar, h2, ad);
        this.u = iVar;
        t tVar2 = this.l;
        com.helpshift.j.a.b.a h3 = xVar.h();
        h hVar = new h();
        hVar.b(tVar2.a(h3));
        this.v = hVar;
        this.t = new h();
        t tVar3 = this.l;
        l lVar = new l();
        t.a(lVar, h2, ad);
        this.w = lVar;
        t tVar4 = this.l;
        h hVar2 = new h();
        tVar4.a(hVar2, h2);
        this.s = hVar2;
        aVar.s = this.w.a() ? 2 : -1;
        if (!ad && h2.g == e.RESOLUTION_REJECTED) {
            this.n.c(h2);
        }
        xVar.a((a) this);
        this.i = qVar;
        p();
        this.x = z2;
    }

    public final void j() {
        q();
        l();
        this.B = true;
        g(true);
        com.helpshift.j.a.b.a h2 = this.f.h();
        if (com.helpshift.j.a.b.f(h2)) {
            this.j.b(new n(this, h2));
        }
        ae();
    }

    public final void l() {
        this.t.a(!k.a(this.p.b()));
        al();
    }

    private void g(boolean z2) {
        this.g.r = z2;
        a(this.f.e());
    }

    public final void m() {
        x xVar = this.f;
        xVar.g = null;
        xVar.h().C = null;
        an anVar = this.m;
        if (anVar != null) {
            anVar.c = null;
            this.m = null;
        }
        this.i = null;
        this.h.deleteObserver(this);
        this.j.j.b(this);
    }

    /* access modifiers changed from: protected */
    public final void n() {
        this.v.b(this.l.a(this.f.h()));
    }

    public final boolean o() {
        return this.r.e();
    }

    private void V() {
        this.g.c("");
        this.p.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void
     arg types: [com.helpshift.j.a.b.a, int]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Collection<? extends com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Set<java.lang.Long>):void
      com.helpshift.j.a.b.a(java.util.List<com.helpshift.j.a.a.v>, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, long):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.e):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.f.a):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.i.b.a(java.util.Collection<? extends com.helpshift.j.a.a.v>, boolean):java.util.List<com.helpshift.j.a.a.v>
     arg types: [com.helpshift.common.g.d<com.helpshift.j.a.a.v>, int]
     candidates:
      com.helpshift.j.i.b.a(com.helpshift.j.a.a.v, com.helpshift.j.a.a.v):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.b, com.helpshift.common.exception.RootAPIException):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.b, com.helpshift.j.a.a.x):void
      com.helpshift.j.i.b.a(int, int):void
      com.helpshift.j.i.b.a(int, java.lang.String):void
      com.helpshift.j.i.b.a(com.helpshift.b.b, java.util.Map<java.lang.String, java.lang.Object>):void
      com.helpshift.j.i.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.bn, boolean):void
      com.helpshift.j.i.b.a(java.lang.String, com.helpshift.j.a.a.v):void
      com.helpshift.j.i.b.a(java.lang.String, java.lang.String):void
      com.helpshift.j.i.b.a(java.lang.String, boolean):void
      com.helpshift.j.i.b.a(java.util.List<com.helpshift.j.a.b.a>, boolean):void
      com.helpshift.j.i.a.a(java.lang.String, java.lang.String):void
      com.helpshift.j.i.a.a(java.util.List<com.helpshift.j.a.b.a>, boolean):void
      com.helpshift.j.i.au.a(int, int):void
      com.helpshift.j.i.b.a(java.util.Collection<? extends com.helpshift.j.a.a.v>, boolean):java.util.List<com.helpshift.j.a.a.v> */
    /* access modifiers changed from: protected */
    public final void p() {
        List<v> list;
        an anVar = this.m;
        if (anVar != null) {
            anVar.c = null;
        }
        com.helpshift.j.a.b.a h2 = this.f.h();
        this.f.i();
        com.helpshift.j.a.b bVar = this.n;
        boolean z2 = true;
        if (h2.g == e.RESOLUTION_REQUESTED && !bVar.e.d()) {
            bVar.a(h2, true);
        }
        boolean l2 = this.f.l();
        this.m = new an(this.k, this.j);
        List<w> m2 = this.f.m();
        ArrayList arrayList = new ArrayList();
        for (com.helpshift.j.a.b.a next : this.f.j()) {
            ArrayList arrayList2 = new ArrayList();
            if (next.x) {
                arrayList2.add(a(next));
            } else {
                com.helpshift.j.a.b.a h3 = this.f.h();
                if (!h3.f3504b.equals(next.f3504b) || !this.n.g(h3)) {
                    list = new ArrayList<>(next.j);
                } else {
                    list = a((Collection<? extends v>) next.j, false);
                }
                arrayList2.addAll(list);
            }
            arrayList.addAll(arrayList2);
        }
        this.m.a(m2, arrayList, l2, this);
        this.i.a(this.m.d);
        this.f.a((c<v>) this);
        if (h2.g != e.REJECTED) {
            z2 = false;
        }
        this.o = z2;
        String d2 = this.g.d();
        com.helpshift.j.a.b.a h4 = this.f.h();
        if (k.a(d2) && !com.helpshift.j.a.b.i(h4)) {
            d2 = this.g.c();
            if (k.a(d2)) {
                d2 = this.h.c("conversationPrefillText");
            }
        }
        if (d2 != null) {
            this.p.a(d2);
        }
    }

    private ah a(com.helpshift.j.a.b.a aVar) {
        ah ahVar = new ah(aVar.z, aVar.A, 1);
        ahVar.a(this.j, this.k);
        ahVar.q = aVar.f3504b;
        return ahVar;
    }

    private List<v> a(Collection<? extends v> collection, boolean z2) {
        com.helpshift.j.a.a.x xVar;
        ArrayList arrayList = new ArrayList(collection);
        com.helpshift.j.a.b.a h2 = this.f.h();
        this.f3662b = com.helpshift.j.a.b.a(arrayList, z2);
        if (this.f3662b) {
            v h3 = com.helpshift.j.a.b.h(h2);
            v vVar = this.z;
            if (vVar == null || h3 == null || !vVar.m.equals(h3.m)) {
                if (h3 == null || !(h3.k == com.helpshift.j.a.a.w.ADMIN_TEXT_WITH_OPTION_INPUT || h3.k == com.helpshift.j.a.a.w.FAQ_LIST_WITH_OPTION_INPUT)) {
                    this.z = h3;
                } else {
                    int indexOf = arrayList.indexOf(h3);
                    if (indexOf != -1) {
                        if (h3.k == com.helpshift.j.a.a.w.ADMIN_TEXT_WITH_OPTION_INPUT) {
                            xVar = a((com.helpshift.j.a.a.i) h3);
                        } else {
                            xVar = a((com.helpshift.j.a.a.q) h3);
                        }
                        a(xVar, h3);
                        if (xVar.f3496a.f == b.C0138b.PILL) {
                            arrayList.add(indexOf + 1, xVar);
                        }
                        this.z = xVar;
                    }
                }
                if (h3 != null) {
                    W();
                    this.d = true;
                } else {
                    this.d = false;
                }
            } else {
                this.d = true;
                return arrayList;
            }
        } else {
            this.d = false;
        }
        return arrayList;
    }

    private void W() {
        ArrayList<v> arrayList;
        an anVar = this.m;
        if (anVar != null) {
            if (anVar.d != null) {
                arrayList = new ArrayList<>(anVar.d);
            } else {
                arrayList = new ArrayList<>();
            }
            ArrayList arrayList2 = new ArrayList();
            if (!com.helpshift.common.j.a(arrayList)) {
                for (v vVar : arrayList) {
                    if (vVar.k == com.helpshift.j.a.a.w.OPTION_INPUT) {
                        arrayList2.add(vVar);
                    }
                }
                this.m.b((List<v>) arrayList2);
            }
            h(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.i.b.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.j.i.b.a(java.util.Collection<? extends com.helpshift.j.a.a.v>, boolean):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.j.i.b.a(com.helpshift.j.a.a.v, com.helpshift.j.a.a.v):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.b, com.helpshift.common.exception.RootAPIException):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.b, com.helpshift.j.a.a.x):void
      com.helpshift.j.i.b.a(int, int):void
      com.helpshift.j.i.b.a(int, java.lang.String):void
      com.helpshift.j.i.b.a(com.helpshift.b.b, java.util.Map<java.lang.String, java.lang.Object>):void
      com.helpshift.j.i.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.bn, boolean):void
      com.helpshift.j.i.b.a(java.lang.String, com.helpshift.j.a.a.v):void
      com.helpshift.j.i.b.a(java.lang.String, java.lang.String):void
      com.helpshift.j.i.b.a(java.util.List<com.helpshift.j.a.b.a>, boolean):void
      com.helpshift.j.i.a.a(java.lang.String, java.lang.String):void
      com.helpshift.j.i.a.a(java.util.List<com.helpshift.j.a.b.a>, boolean):void
      com.helpshift.j.i.au.a(int, int):void
      com.helpshift.j.i.b.a(java.lang.String, boolean):void */
    public final void q() {
        boolean ad = ad();
        com.helpshift.j.a.b.a h2 = this.f.h();
        t.a(this.w, h2, ad);
        this.l.a(this.s, h2);
        this.l.a(this.u, h2, ad);
        this.g.s = this.w.a() ? 2 : -1;
        this.f.a((c<v>) this);
        this.f.a((a) this);
        if (!(h2.c == null && h2.d == null && this.f.j().size() <= 1)) {
            this.g.l.a();
        }
        if (com.helpshift.j.a.b.f(h2) || !com.helpshift.j.a.b.i(h2)) {
            if (!com.helpshift.j.a.b.f(h2) && this.h.k()) {
                String c2 = this.h.c("initialUserMessageToAutoSendInPreissue");
                if (!k.a(c2)) {
                    com.helpshift.util.n.a("Helpshift_ConvsatnlVM", "Auto-filing preissue with client set user message.", (Throwable) null, (com.helpshift.p.b.a[]) null);
                    this.n.f(h2, true);
                    a(c2, false);
                    return;
                }
            }
            if (com.helpshift.j.a.b.f(h2)) {
                b((Collection<? extends v>) h2.j);
            }
            ab();
            return;
        }
        v vVar = (v) h2.j.get(h2.j.size() - 1);
        if (vVar instanceof al) {
            al alVar = (al) vVar;
            if (alVar.c != am.SENT) {
                this.w.a(false);
            }
            if (this.g.a(h2.f3504b.longValue())) {
                alVar.a(am.SENDING);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, boolean z2) {
        com.helpshift.util.n.a("Helpshift_ConvsatnlVM", "Trigger preissue creation. Retrying ? " + z2, (Throwable) null, (com.helpshift.p.b.a[]) null);
        af();
        V();
        Y();
        String c2 = this.h.c("conversationGreetingMessage");
        if (!z2) {
            this.n.a(this.f.h(), str);
        }
        if (!this.f3661a) {
            a(new Exception("No internet connection."));
        } else {
            this.g.a(this.f, c2, str, this);
        }
    }

    public final void d() {
        if (this.e) {
            this.j.c(new z(this));
            this.e = false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(boolean z2) {
        this.j.c(new aa(this, z2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.i.b.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.j.i.b.a(java.util.Collection<? extends com.helpshift.j.a.a.v>, boolean):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.j.i.b.a(com.helpshift.j.a.a.v, com.helpshift.j.a.a.v):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.b, com.helpshift.common.exception.RootAPIException):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.b, com.helpshift.j.a.a.x):void
      com.helpshift.j.i.b.a(int, int):void
      com.helpshift.j.i.b.a(int, java.lang.String):void
      com.helpshift.j.i.b.a(com.helpshift.b.b, java.util.Map<java.lang.String, java.lang.Object>):void
      com.helpshift.j.i.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.bn, boolean):void
      com.helpshift.j.i.b.a(java.lang.String, com.helpshift.j.a.a.v):void
      com.helpshift.j.i.b.a(java.lang.String, java.lang.String):void
      com.helpshift.j.i.b.a(java.util.List<com.helpshift.j.a.b.a>, boolean):void
      com.helpshift.j.i.a.a(java.lang.String, java.lang.String):void
      com.helpshift.j.i.a.a(java.util.List<com.helpshift.j.a.b.a>, boolean):void
      com.helpshift.j.i.au.a(int, int):void
      com.helpshift.j.i.b.a(java.lang.String, boolean):void */
    public final void r() {
        String a2 = this.i.a();
        if (!k.a(a2)) {
            this.g.a(true);
            String trim = a2.trim();
            af();
            com.helpshift.j.a.b.a h2 = this.f.h();
            if (!com.helpshift.j.a.b.i(h2)) {
                int i2 = -1;
                if (trim != null) {
                    if (trim.length() == 0) {
                        i2 = 0;
                    } else {
                        BreakIterator characterInstance = BreakIterator.getCharacterInstance();
                        characterInstance.setText(trim);
                        int i3 = 0;
                        while (characterInstance.next() != -1) {
                            i3++;
                        }
                        i2 = i3;
                    }
                }
                if (i2 < this.h.h()) {
                    this.i.a(1);
                    return;
                } else if (k.a(h2.d)) {
                    X();
                    a(trim, false);
                    return;
                }
            }
            if (!this.f3662b) {
                b(trim);
                return;
            }
            v vVar = this.z;
            if (!(vVar instanceof com.helpshift.j.a.a.j)) {
                b(trim);
                return;
            }
            com.helpshift.j.a.a.j jVar = (com.helpshift.j.a.a.j) vVar;
            com.helpshift.j.a.a.a.c cVar = jVar.f3481b;
            if (!jVar.f3481b.a(trim)) {
                this.i.a(cVar.f);
                return;
            }
            this.i.j();
            Y();
            X();
            this.j.b(new ad(this, trim, jVar));
        }
    }

    private void X() {
        this.j.c(new ab(this));
    }

    private void b(String str) {
        X();
        this.j.b(new ac(this, str));
    }

    public final void a(v vVar) {
        this.j.b(new d(this, vVar));
    }

    public final void a(com.helpshift.j.a.a.ab abVar) {
        a aVar = this.f.g;
        if (abVar.D == am.SENT && aVar != null) {
            aVar.b(abVar.b(), abVar.c);
        }
    }

    public final void a(y yVar) {
        com.helpshift.j.a.a.a aVar;
        String trim = this.h.c("reviewUrl").trim();
        if (!k.a(trim)) {
            this.h.a(true);
            q qVar = this.i;
            if (qVar != null) {
                qVar.b(trim);
            }
        }
        com.helpshift.j.a.b bVar = this.n;
        com.helpshift.j.a.b.a h2 = this.f.h();
        j jVar = bVar.f3502b;
        ab abVar = bVar.f3501a;
        if (yVar.f3498a) {
            aVar = null;
        } else {
            yVar.a(false);
            aa<String, Long> b2 = com.helpshift.common.g.b.b(abVar);
            com.helpshift.j.a.a.a aVar2 = new com.helpshift.j.a.a.a("Accepted review request", (String) b2.f4242a, ((Long) b2.f4243b).longValue(), "mobile", yVar.m, 1);
            aVar2.q = yVar.q;
            aVar2.a(jVar, abVar);
            abVar.f().a(aVar2);
            HashMap hashMap = new HashMap();
            hashMap.put("type", "conversation");
            jVar.c.a(com.helpshift.b.b.REVIEWED_APP, hashMap);
            jVar.e.b("User reviewed the app");
            aVar = aVar2;
        }
        if (aVar != null) {
            bVar.a(new com.helpshift.j.a.n(bVar, aVar, h2, yVar));
        }
    }

    public final void b(String str, String str2) {
        this.i.a(str, str2);
    }

    public final boolean b() {
        return this.B;
    }

    public final void a(boolean z2) {
        this.j.c(new e(this, z2));
    }

    public final void s() {
        af();
        v vVar = this.z;
        if (vVar instanceof com.helpshift.j.a.a.j) {
            V();
            Y();
            this.j.b(new f(this, vVar));
        }
        this.i.i();
    }

    public final void a(Collection<? extends v> collection) {
        com.helpshift.util.n.a("Helpshift_ConvsatnlVM", "addAll called : " + collection.size(), (Throwable) null, (com.helpshift.p.b.a[]) null);
        com.helpshift.j.a.b.a h2 = this.f.h();
        if (com.helpshift.j.a.b.a(collection)) {
            this.n.e(h2, false);
        }
        List<v> b2 = b(collection);
        if (!this.f3662b) {
            this.c = false;
        } else if (!this.c && com.helpshift.j.a.b.i(h2)) {
            V();
            this.c = true;
        }
        an anVar = this.m;
        if (anVar != null) {
            anVar.a(b2);
        }
    }

    public final void a(com.helpshift.j.a.a.x xVar, b.a aVar, boolean z2) {
        if (this.m != null) {
            if (xVar.f3496a.f == b.C0138b.PILL) {
                int indexOf = this.m.d.indexOf(xVar);
                this.m.b((List<v>) Collections.singletonList(xVar));
                this.i.b(indexOf - 1, 1);
            }
            af();
            if (xVar.f3496a.f == b.C0138b.PILL) {
                Y();
            } else if (xVar.f3496a.f == b.C0138b.PICKER) {
                h(true);
            }
            this.j.b(new g(this, xVar, aVar, z2));
        }
    }

    private void h(boolean z2) {
        this.j.c(new h(this, z2));
    }

    private void Y() {
        q qVar = this.i;
        if (qVar != null) {
            qVar.f();
        }
        this.v.b(false);
        Z();
    }

    private List<v> b(Collection<? extends v> collection) {
        com.helpshift.j.a.b.a h2 = this.f.h();
        boolean z2 = this.f3662b;
        List<v> a2 = a(collection, z2);
        if (!h2.a()) {
            if (z2 && !this.f3662b) {
                this.n.e(h2, com.helpshift.j.a.b.m(h2));
                W();
                this.w.e();
                this.j.c(new i(this));
            } else if (this.f3662b && !z2) {
                this.n.e(h2, false);
            }
        }
        aa();
        return a2;
    }

    private void Z() {
        this.w.a(false);
    }

    /* access modifiers changed from: private */
    public void aa() {
        com.helpshift.j.a.b.a h2 = this.f.h();
        e eVar = h2.g;
        boolean z2 = true;
        boolean z3 = false;
        if (eVar == e.REJECTED) {
            Y();
        } else if (!(eVar == e.RESOLUTION_REQUESTED || eVar == e.RESOLUTION_ACCEPTED || eVar == e.COMPLETED_ISSUE_CREATED)) {
            if (this.f3662b) {
                this.v.b(false);
                if (!this.d) {
                    Y();
                    if (this.m != null) {
                        int size = h2.j.size();
                        if (size > 0) {
                            v vVar = (v) h2.j.get(size - 1);
                            if (((vVar instanceof ap) || (vVar instanceof an)) && ((al) vVar).c != am.SENT) {
                                z2 = false;
                            }
                        }
                        z3 = z2;
                    }
                }
            } else if (h2.a() && !k.a(h2.d)) {
                Y();
            }
            z3 = true;
        }
        b(z3);
    }

    public final void t() {
        this.j.c(new j(this));
    }

    public final void u() {
        this.j.c(new k(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.b(com.helpshift.j.a.b.a, boolean):void
     arg types: [com.helpshift.j.a.b.a, int]
     candidates:
      com.helpshift.j.a.b.b(com.helpshift.j.a.a.v, boolean):void
      com.helpshift.j.a.b.b(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
      com.helpshift.j.a.b.b(com.helpshift.j.a.b.a, com.helpshift.j.d.e):void
      com.helpshift.j.a.b.b(com.helpshift.j.a.b.a, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.helpshift.j.d.e r8) {
        /*
            r7 = this;
            com.helpshift.j.a.x r0 = r7.f
            com.helpshift.j.a.b.a r0 = r0.h()
            boolean r0 = r0.a()
            r1 = 2
            r2 = 1
            r3 = 0
            if (r0 != 0) goto L_0x00d7
            r0 = -1
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Changing conversation status to: "
            r4.append(r5)
            r4.append(r8)
            java.lang.String r4 = r4.toString()
            r5 = 0
            java.lang.String r6 = "Helpshift_ConvsatnlVM"
            com.helpshift.util.n.a(r6, r4, r5, r5)
            com.helpshift.j.a.x r4 = r7.f
            com.helpshift.j.a.b.a r4 = r4.h()
            boolean r5 = com.helpshift.j.c.a(r8)
            if (r5 == 0) goto L_0x003a
            r7.ak()
            r8 = 0
        L_0x0037:
            r0 = 0
            goto L_0x00bd
        L_0x003a:
            com.helpshift.j.d.e r5 = com.helpshift.j.d.e.RESOLUTION_REQUESTED
            if (r8 != r5) goto L_0x006a
            com.helpshift.i.a.a r8 = r7.h
            boolean r8 = r8.d()
            if (r8 == 0) goto L_0x005a
            com.helpshift.z.l r8 = r7.w
            r8.a(r3)
            r7.al()
            com.helpshift.z.h r8 = r7.s
            r8.b(r2)
            com.helpshift.z.i r8 = r7.u
            com.helpshift.j.a.a.o r1 = com.helpshift.j.a.a.o.NONE
            r8.a(r1)
        L_0x005a:
            com.helpshift.z.n r8 = r7.r
            boolean r8 = r8.a()
            if (r8 != 0) goto L_0x0065
            r7.aj()
        L_0x0065:
            r8 = 1
            r0 = 0
            r1 = -1
            r2 = 0
            goto L_0x00bd
        L_0x006a:
            com.helpshift.j.d.e r5 = com.helpshift.j.d.e.REJECTED
            if (r8 != r5) goto L_0x0075
            r7.ac()
            r8 = 1
            r0 = 1
        L_0x0073:
            r1 = -1
            goto L_0x00bd
        L_0x0075:
            com.helpshift.j.d.e r5 = com.helpshift.j.d.e.RESOLUTION_ACCEPTED
            if (r8 != r5) goto L_0x0094
            com.helpshift.j.c.a r8 = r7.g
            java.lang.String r1 = ""
            r8.c(r1)
            com.helpshift.j.a.b r8 = r7.n
            boolean r8 = r8.b(r4)
            if (r8 == 0) goto L_0x008e
            com.helpshift.j.a.a.o r8 = com.helpshift.j.a.a.o.CSAT_RATING
            r7.a(r8)
            goto L_0x00ba
        L_0x008e:
            com.helpshift.j.a.a.o r8 = com.helpshift.j.a.a.o.START_NEW_CONVERSATION
            r7.a(r8)
            goto L_0x00ba
        L_0x0094:
            com.helpshift.j.d.e r5 = com.helpshift.j.d.e.RESOLUTION_REJECTED
            if (r8 != r5) goto L_0x00a7
            com.helpshift.j.c.a r8 = r7.g
            r8.a(r3)
            r7.ak()
            com.helpshift.j.a.b r8 = r7.n
            r8.b(r4, r2)
            r8 = 1
            goto L_0x0037
        L_0x00a7:
            com.helpshift.j.d.e r1 = com.helpshift.j.d.e.ARCHIVED
            if (r8 != r1) goto L_0x00b1
            com.helpshift.j.a.a.o r8 = com.helpshift.j.a.a.o.ARCHIVAL_MESSAGE
            r7.a(r8)
            goto L_0x00ba
        L_0x00b1:
            com.helpshift.j.d.e r1 = com.helpshift.j.d.e.AUTHOR_MISMATCH
            if (r8 != r1) goto L_0x00ba
            com.helpshift.j.a.a.o r8 = com.helpshift.j.a.a.o.AUTHOR_MISMATCH
            r7.a(r8)
        L_0x00ba:
            r8 = 1
            r0 = 0
            goto L_0x0073
        L_0x00bd:
            if (r2 == 0) goto L_0x00c2
            r7.ah()
        L_0x00c2:
            if (r8 == 0) goto L_0x00c7
            r7.a(r3)
        L_0x00c7:
            com.helpshift.j.c.a r8 = r7.g
            r8.s = r1
            r7.o = r0
            boolean r8 = r7.f3662b
            if (r8 == 0) goto L_0x00d6
            com.helpshift.z.h r8 = r7.v
            r8.b(r3)
        L_0x00d6:
            return
        L_0x00d7:
            int[] r0 = com.helpshift.j.i.x.f3724a
            int r8 = r8.ordinal()
            r8 = r0[r8]
            if (r8 == r2) goto L_0x00f0
            if (r8 == r1) goto L_0x00e4
            goto L_0x00fa
        L_0x00e4:
            r7.d = r3
            r7.W()
            r7.ac()
            r7.ah()
            goto L_0x00fa
        L_0x00f0:
            r7.d = r3
            com.helpshift.j.a.a.o r8 = com.helpshift.j.a.a.o.START_NEW_CONVERSATION
            r7.a(r8)
            r7.ah()
        L_0x00fa:
            r7.aa()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.i.b.a(com.helpshift.j.d.e):void");
    }

    public final void a(long j2) {
        e();
    }

    public final void e() {
        this.j.c(new l(this));
    }

    public final void f() {
        this.j.c(new m(this));
    }

    public final void a(Exception exc) {
        com.helpshift.util.n.c("Helpshift_ConvsatnlVM", "Error filing a pre-issue", exc);
        this.j.c(new o(this));
    }

    public final void a(v vVar, String str, String str2) {
        if (!k.a(str2)) {
            this.j.b(new p(this, vVar.q, vVar.m, str, str2));
        }
    }

    private com.helpshift.j.a.a.x a(com.helpshift.j.a.a.q qVar) {
        if (qVar == null) {
            return null;
        }
        com.helpshift.j.a.a.x xVar = new com.helpshift.j.a.a.x(qVar);
        xVar.a(this.j, this.k);
        return xVar;
    }

    private com.helpshift.j.a.a.x a(com.helpshift.j.a.a.i iVar) {
        if (iVar == null) {
            return null;
        }
        com.helpshift.j.a.a.x xVar = new com.helpshift.j.a.a.x(iVar);
        xVar.a(this.j, this.k);
        return xVar;
    }

    private static void a(v vVar, v vVar2) {
        String a2 = com.helpshift.common.g.b.f3381a.a(new Date(vVar2.C + 1));
        long b2 = com.helpshift.common.g.b.b(a2);
        vVar.c(a2);
        vVar.C = b2;
    }

    public final void w() {
        ab();
    }

    private void ab() {
        if (this.f3662b && !this.o) {
            v vVar = this.z;
            if (vVar == null) {
                this.w.a(false);
            } else if (vVar.k == com.helpshift.j.a.a.w.ADMIN_TEXT_WITH_TEXT_INPUT) {
                this.w.a((com.helpshift.j.a.a.a.a) ((com.helpshift.j.a.a.j) this.z).f3481b);
            } else if (this.z.k == com.helpshift.j.a.a.w.OPTION_INPUT) {
                this.j.c(new q(this));
            }
        } else if (this.w.a()) {
            this.w.e();
        }
    }

    public final void a(List<bn> list) {
        this.i.b(list);
    }

    public final void b(com.helpshift.j.a.a.x xVar, b.a aVar, boolean z2) {
        this.y = null;
        a(xVar, aVar, z2);
    }

    public final void x() {
        this.i.l();
    }

    public final void a(String str) {
        ag agVar = this.y;
        if (agVar == null) {
            return;
        }
        if (k.a(str)) {
            agVar.a(agVar.a());
            agVar.f3627a.c(new aj(agVar));
            return;
        }
        agVar.f3627a.c(new ai(agVar));
        String trim = str.trim();
        if (trim.length() < 2) {
            agVar.a(agVar.a());
            return;
        }
        String[] split = trim.split("\\b");
        ArrayList arrayList = new ArrayList();
        for (String trim2 : split) {
            String lowerCase = trim2.trim().toLowerCase();
            if (lowerCase.length() >= 2) {
                arrayList.add(lowerCase);
            }
        }
        if (arrayList.size() == 0) {
            agVar.a(agVar.a());
            return;
        }
        synchronized (ag.d) {
            agVar.f3627a.b(new ah(agVar, arrayList));
        }
    }

    public final void a(bn bnVar, boolean z2) {
        ag agVar = this.y;
        if (agVar != null) {
            agVar.c.b(agVar.f3628b, z2 ? null : bnVar.f3687a, z2);
        }
    }

    public final void y() {
        this.i.n();
    }

    public final void z() {
        this.i.m();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.i.an.a(java.util.List<com.helpshift.j.a.a.v>, boolean):void
     arg types: [java.util.ArrayList, int]
     candidates:
      com.helpshift.j.i.an.a(java.util.Date, boolean):com.helpshift.j.a.a.ae
      com.helpshift.j.i.an.a(com.helpshift.j.i.an, java.util.List):void
      com.helpshift.j.i.an.a(long, long):boolean
      com.helpshift.j.i.an.a(com.helpshift.j.a.a.v, com.helpshift.j.a.a.v):boolean
      com.helpshift.j.i.an.a(java.util.List<com.helpshift.j.a.a.v>, boolean):void */
    public final void a(List<com.helpshift.j.a.b.a> list, boolean z2) {
        if (!com.helpshift.common.j.a(list)) {
            List<w> m2 = this.f.m();
            ArrayList arrayList = new ArrayList();
            for (com.helpshift.j.a.b.a next : list) {
                ArrayList arrayList2 = new ArrayList();
                if (next.x) {
                    arrayList2.add(a(next));
                } else {
                    arrayList2.addAll(next.j);
                }
                arrayList.addAll(arrayList2);
            }
            an anVar = this.m;
            if (anVar != null) {
                anVar.a(m2);
                this.m.a(arrayList, z2);
            }
        } else if (!z2) {
            this.m.a((List<v>) new ArrayList(), false);
        }
    }

    public final void g() {
        this.q.a(com.helpshift.j.a.a.t.NONE);
    }

    public final void h() {
        this.q.a(com.helpshift.j.a.a.t.ERROR);
    }

    public final void i() {
        this.q.a(com.helpshift.j.a.a.t.LOADING);
    }

    public final void a(d dVar, String str) {
        this.j.b(new s(this, dVar, str));
    }

    public final void a(com.helpshift.j.a.a.k kVar) {
        x xVar = this.f;
        int i2 = com.helpshift.j.a.y.f3551a[kVar.k.ordinal()];
        if (i2 == 1) {
            ((com.helpshift.j.a.a.e) kVar).a(xVar.g);
        } else if (i2 == 2) {
            ((com.helpshift.j.a.a.b) kVar).a(xVar.g);
        }
    }

    public final void a(String str, String str2) {
        this.i.b(str, str2);
    }

    public final void c(boolean z2) {
        com.helpshift.util.n.a("Helpshift_ConvsatnlVM", "Sending resolution event : Accepted? " + z2, (Throwable) null, (com.helpshift.p.b.a[]) null);
        com.helpshift.j.a.b.a h2 = this.f.h();
        if (h2.g == e.RESOLUTION_REQUESTED) {
            this.n.a(h2, z2);
        }
    }

    private void ac() {
        o oVar;
        com.helpshift.j.a.b.a h2 = this.f.h();
        this.g.c("");
        if (h2.x) {
            oVar = o.REDACTED_STATE;
        } else {
            oVar = o.REJECTED_MESSAGE;
        }
        a(oVar);
        this.o = true;
    }

    public final void A() {
        this.g.a(true);
    }

    private boolean ad() {
        return !k.a(this.g.d()) || this.g.m() || this.A;
    }

    public final void a(int i2, String str) {
        q qVar = this.i;
        if (qVar != null) {
            qVar.b();
        }
        com.helpshift.j.a.b.a h2 = this.f.h();
        if (!com.helpshift.j.c.a(h2.g)) {
            a(o.START_NEW_CONVERSATION);
        }
        com.helpshift.util.n.a("Helpshift_ConvsatnlVM", "Sending CSAT rating : " + i2 + ", feedback: " + str, (Throwable) null, (com.helpshift.p.b.a[]) null);
        com.helpshift.j.a.b bVar = this.n;
        if (i2 > 5) {
            i2 = 5;
        } else if (i2 < 0) {
            i2 = 0;
        }
        h2.q = i2;
        if (str != null) {
            str = str.trim();
        }
        h2.r = str;
        bVar.a(h2, com.helpshift.j.f.a.SUBMITTED_NOT_SYNCED);
        bVar.a(new com.helpshift.j.a.d(bVar, h2));
        com.helpshift.l.c cVar = bVar.f3502b.e;
        int i3 = h2.q;
        String str2 = h2.r;
        if (cVar.f3738b != null) {
            cVar.f3737a.c(new com.helpshift.l.i(cVar, i3, str2));
        }
    }

    public final void a(int i2) {
        this.g.s = -1;
    }

    public final void B() {
        if (this.f.h().s) {
            v();
        }
    }

    private void ae() {
        com.helpshift.j.a.b.a h2 = this.f.h();
        this.g.c(h2);
        this.g.b(h2);
    }

    public final void a(com.helpshift.b.b bVar, Map<String, Object> map) {
        this.j.c.a(bVar, map);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x002e A[EDGE_INSN: B:25:0x002e->B:13:0x002e ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r6, com.helpshift.j.a.a.v r7) {
        /*
            r5 = this;
            r0 = 0
            java.net.URI r1 = java.net.URI.create(r6)     // Catch:{ Exception -> 0x000c }
            if (r1 == 0) goto L_0x000c
            java.lang.String r1 = r1.getScheme()     // Catch:{ Exception -> 0x000c }
            goto L_0x000d
        L_0x000c:
            r1 = r0
        L_0x000d:
            java.lang.Long r7 = r7.q
            com.helpshift.j.a.x r2 = r5.f
            java.util.List r2 = r2.j()
            java.util.Iterator r2 = r2.iterator()
        L_0x0019:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x002e
            java.lang.Object r3 = r2.next()
            com.helpshift.j.a.b.a r3 = (com.helpshift.j.a.b.a) r3
            java.lang.Long r4 = r3.f3504b
            boolean r4 = r4.equals(r7)
            if (r4 == 0) goto L_0x0019
            r0 = r3
        L_0x002e:
            boolean r7 = com.helpshift.common.k.a(r1)
            if (r7 != 0) goto L_0x0068
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            if (r0 == 0) goto L_0x0059
            java.lang.String r2 = r0.d
            boolean r2 = com.helpshift.common.k.a(r2)
            if (r2 != 0) goto L_0x004a
            java.lang.String r2 = r0.d
            java.lang.String r3 = "preissue_id"
            r7.put(r3, r2)
        L_0x004a:
            java.lang.String r2 = r0.c
            boolean r2 = com.helpshift.common.k.a(r2)
            if (r2 != 0) goto L_0x0059
            java.lang.String r0 = r0.c
            java.lang.String r2 = "issue_id"
            r7.put(r2, r0)
        L_0x0059:
            java.lang.String r0 = "p"
            r7.put(r0, r1)
            java.lang.String r0 = "u"
            r7.put(r0, r6)
            com.helpshift.b.b r6 = com.helpshift.b.b.ADMIN_MESSAGE_DEEPLINK_CLICKED
            r5.a(r6, r7)
        L_0x0068:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.i.b.a(java.lang.String, com.helpshift.j.a.a.v):void");
    }

    public final void C() {
        this.f.c();
    }

    public final void D() {
        this.f.d();
    }

    public void update(Observable observable, Object obj) {
        this.j.c(new t(this, observable));
    }

    private void af() {
        this.n.a(this.f.h(), System.currentTimeMillis());
    }

    public final void a(int i2, int i3) {
        q qVar = this.i;
        if (qVar != null) {
            qVar.a(i2, i3);
        }
    }

    public final void E() {
        ah();
    }

    public final void F() {
        aj();
    }

    private void ag() {
        this.r.b(true);
    }

    private void ah() {
        if (this.r.a()) {
            ag();
        } else {
            aj();
        }
    }

    public final void b(int i2, int i3) {
        q qVar = this.i;
        if (qVar != null) {
            qVar.b(i2, i3);
        }
    }

    public final void G() {
        q qVar = this.i;
        if (qVar != null) {
            qVar.g();
        }
    }

    public final void a() {
        this.j.c(new u(this));
    }

    public final void H() {
        aj();
    }

    public final void I() {
        this.r.a(false);
        this.r.b(false);
    }

    public final void J() {
        this.r.a(true);
    }

    public final void K() {
        if (this.q.a() == com.helpshift.j.a.a.t.NONE) {
            ai();
        }
    }

    public final void L() {
        if (this.q.a() == com.helpshift.j.a.a.t.ERROR) {
            ai();
        }
    }

    private void ai() {
        if (this.q.a() != com.helpshift.j.a.a.t.LOADING) {
            this.j.b(new v(this));
        }
    }

    /* access modifiers changed from: protected */
    public final void d(boolean z2) {
        boolean z3;
        if (z2) {
            this.i.c();
            z3 = !this.r.a();
        } else {
            this.i.d();
            z3 = false;
        }
        if (z3) {
            aj();
        }
    }

    private void aj() {
        this.j.c(new w(this));
    }

    public final void e(boolean z2) {
        this.r.b(z2);
    }

    private void ak() {
        this.w.a(true);
        al();
        this.s.b(false);
        this.u.a(o.NONE);
    }

    private void al() {
        n();
        if (this.v.a()) {
            this.v.b(!this.o && this.w.a());
        }
    }

    private void a(o oVar) {
        this.w.a(false);
        al();
        this.s.b(false);
        this.u.a(oVar);
    }

    public final void f(boolean z2) {
        this.t.a(z2);
    }

    public final com.helpshift.z.q M() {
        return this.p;
    }

    public final f N() {
        return this.q;
    }

    public final r O() {
        return this.r;
    }

    public final com.helpshift.z.b P() {
        return this.u;
    }

    public final com.helpshift.z.a Q() {
        return this.v;
    }

    public final p R() {
        return this.w;
    }

    public final com.helpshift.z.a S() {
        return this.s;
    }

    public final com.helpshift.z.a T() {
        return this.t;
    }

    public final void U() {
        this.g.f3558a = 0;
    }

    public final void k() {
        this.B = false;
        g(false);
        ArrayList arrayList = new ArrayList(this.f.j());
        com.helpshift.j.a.b.a h2 = this.f.h();
        if (!com.helpshift.j.a.b.f(h2)) {
            arrayList.remove(h2);
        }
        this.j.b(new c(this, arrayList));
        ae();
        this.n.b(this.f.h(), false, true);
        String a2 = this.i.a();
        this.p.a(a2);
        this.g.c(a2);
    }

    public final void c() {
        com.helpshift.util.n.a("Helpshift_ConvsatnlVM", "On conversation inbox poll failure", (Throwable[]) null, (com.helpshift.p.b.a[]) null);
        b(false);
        if (this.k.A() && !this.d) {
            if ((this.f3662b || this.f.h().a()) && com.helpshift.j.c.a(this.f.h().g)) {
                this.j.c(new y(this));
                this.e = true;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, boolean):void
     arg types: [com.helpshift.j.a.b.a, int, int]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.ab, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, boolean):void */
    public final void v() {
        this.f.d();
        this.n.a(this.f.h(), true, true);
        boolean z2 = false;
        if (this.x) {
            this.w.a(false);
            al();
            this.s.b(false);
            this.u.a(o.NONE);
            com.helpshift.j.c.a aVar = this.g;
            List<com.helpshift.j.a.b.a> b2 = aVar.g.b(aVar.e.f3176a.longValue());
            ArrayList arrayList = new ArrayList();
            com.helpshift.j.a.b.a aVar2 = null;
            if (!b2.isEmpty()) {
                for (com.helpshift.j.a.b.a next : b2) {
                    next.t = aVar.e.f3176a.longValue();
                    if (com.helpshift.j.c.a(next.g)) {
                        arrayList.add(next);
                    }
                }
                if (!arrayList.isEmpty()) {
                    aVar2 = com.helpshift.j.c.a((Collection<com.helpshift.j.a.b.a>) arrayList);
                    aVar2.a(aVar.g.c(aVar2.f3504b.longValue()));
                }
            }
            if (aVar2 == null) {
                aVar2 = this.g.l();
            }
            this.f.b(aVar2);
            q();
            l();
            p();
            this.i.g();
            return;
        }
        HashMap hashMap = new HashMap();
        if (this.x != this.h.i()) {
            z2 = true;
        }
        hashMap.put("create_new_pre_issue", Boolean.valueOf(z2));
        this.i.a(hashMap);
    }

    static /* synthetic */ void a(b bVar, RootAPIException rootAPIException) {
        if ((rootAPIException.c instanceof com.helpshift.common.exception.b) && !bVar.k.A()) {
            bVar.j.c(new ae(bVar));
        }
    }

    static /* synthetic */ void a(b bVar, com.helpshift.j.a.a.x xVar) {
        if (xVar.f3496a.f == b.C0138b.PILL) {
            bVar.i.a(xVar.f3496a);
            return;
        }
        bVar.y = new ag(bVar.j, xVar, bVar);
        bVar.j.c(new r(bVar, xVar));
    }
}
