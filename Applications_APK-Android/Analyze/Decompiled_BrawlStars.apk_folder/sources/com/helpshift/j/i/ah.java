package com.helpshift.j.i;

import com.helpshift.common.c.l;
import com.helpshift.j.a.a.a.b;
import com.helpshift.j.i.ag;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: ListPickerVM */
class ah extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f3635a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ag f3636b;

    ah(ag agVar, List list) {
        this.f3636b = agVar;
        this.f3635a = list;
    }

    public final void a() {
        List<b.a> a2 = new ag.b(new ArrayList(Arrays.asList(new ag.d(), new ag.e(), new ag.f()))).a(this.f3636b.f3628b.f3496a.e, this.f3635a);
        if (a2.size() == 0) {
            this.f3636b.f3627a.c(new ak(this.f3636b));
            return;
        }
        ArrayList arrayList = new ArrayList();
        for (b.a a3 : a2) {
            arrayList.add(this.f3636b.a(a3, this.f3635a));
        }
        this.f3636b.a(arrayList);
    }
}
