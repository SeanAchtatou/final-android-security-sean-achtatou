package com.helpshift.j.i;

import com.helpshift.common.c.l;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.b;
import com.helpshift.j.a.b.a;
import com.helpshift.j.a.e;
import java.util.Iterator;

/* compiled from: ConversationalVM */
class p extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Long f3712a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f3713b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ b f;

    p(b bVar, Long l, String str, String str2, String str3) {
        this.f = bVar;
        this.f3712a = l;
        this.f3713b = str;
        this.c = str2;
        this.d = str3;
    }

    public final void a() {
        a aVar;
        Iterator<a> it = this.f.f.j().iterator();
        while (true) {
            if (!it.hasNext()) {
                aVar = null;
                break;
            }
            aVar = it.next();
            if (aVar.f3504b.equals(this.f3712a)) {
                break;
            }
        }
        a aVar2 = aVar;
        if (aVar2 != null) {
            b bVar = this.f.n;
            String str = this.f3713b;
            String str2 = this.c;
            String str3 = this.d;
            Iterator<E> it2 = aVar2.j.iterator();
            while (it2.hasNext()) {
                v vVar = (v) it2.next();
                if ((vVar instanceof com.helpshift.j.a.a.p) && str.equals(vVar.m)) {
                    bVar.a(new e(bVar, vVar, aVar2, str2, str3));
                    return;
                }
            }
        }
    }
}
