package com.helpshift.j.i;

import com.helpshift.common.c.l;
import com.helpshift.j.a.a.v;
import com.helpshift.util.aa;
import java.util.ArrayList;
import java.util.Collections;

/* compiled from: MessageListVM */
class aq extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f3647a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ an f3648b;

    aq(an anVar, v vVar) {
        this.f3648b = anVar;
        this.f3647a = vVar;
    }

    public final void a() {
        int i;
        v a2;
        v a3;
        if (this.f3648b.d.contains(this.f3647a)) {
            an anVar = this.f3648b;
            v vVar = this.f3647a;
            int indexOf = anVar.d.indexOf(vVar);
            if (indexOf != -1) {
                v a4 = anVar.a(indexOf);
                boolean z = false;
                if (a4 == null || (((a2 = anVar.a(indexOf - 1)) == null || a4.C >= a2.C) && ((a3 = anVar.a(indexOf + 1)) == null || a4.C <= a3.C))) {
                    z = true;
                }
                if (z) {
                    boolean b2 = anVar.b(indexOf);
                    aa<Integer, Integer> a5 = anVar.a(anVar.d, indexOf - 1, indexOf + 1);
                    if (b2) {
                        anVar.a();
                    } else {
                        if (a5 != null) {
                            i = Math.min(indexOf, ((Integer) a5.f4242a).intValue());
                            indexOf = Math.max(indexOf, ((Integer) a5.f4243b).intValue());
                        } else {
                            i = indexOf;
                        }
                        if (anVar.c != null && i <= indexOf && indexOf < anVar.d.size()) {
                            anVar.c.b(i, (indexOf - i) + 1);
                        }
                    }
                } else {
                    anVar.d.remove(indexOf);
                    int i2 = indexOf - 1;
                    anVar.b(i2);
                    anVar.a(anVar.d, i2, indexOf + 1);
                    anVar.a(vVar);
                    anVar.a();
                }
            }
        } else {
            this.f3648b.a(this.f3647a);
            an.a(this.f3648b, new ArrayList(Collections.singletonList(this.f3647a)));
            this.f3648b.a();
        }
        this.f3648b.b();
    }
}
