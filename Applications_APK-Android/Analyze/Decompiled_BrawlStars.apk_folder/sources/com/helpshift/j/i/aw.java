package com.helpshift.j.i;

import com.helpshift.a.a;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.j.c.a;
import com.helpshift.j.d.d;
import com.helpshift.z.g;
import com.helpshift.z.h;
import com.helpshift.z.k;
import com.helpshift.z.o;
import com.helpshift.z.s;
import com.helpshift.z.t;
import java.lang.ref.WeakReference;

/* compiled from: NewConversationVM */
public class aw implements a.C0132a, a.b {

    /* renamed from: a  reason: collision with root package name */
    final j f3654a;

    /* renamed from: b  reason: collision with root package name */
    final ab f3655b;
    final com.helpshift.i.a.a c;
    final com.helpshift.j.c.a d;
    final t e;
    final o f;
    final o g;
    final o h;
    final k i;
    final h j;
    final h k;
    final h l;
    final h m;
    WeakReference<av> n;
    boolean o = false;

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x017e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public aw(com.helpshift.common.e.ab r17, com.helpshift.common.c.j r18, com.helpshift.j.c.a r19, com.helpshift.j.i.av r20) {
        /*
            r16 = this;
            r0 = r16
            r1 = r18
            r2 = r19
            r16.<init>()
            r3 = 0
            r0.o = r3
            r4 = r17
            r0.f3655b = r4
            r0.f3654a = r1
            com.helpshift.i.a.a r4 = r1.f3285b
            r0.c = r4
            r0.d = r2
            com.helpshift.z.t r4 = new com.helpshift.z.t
            com.helpshift.i.a.a r5 = r0.c
            r4.<init>(r5, r2)
            r0.e = r4
            com.helpshift.z.t r4 = r0.e
            com.helpshift.z.o r5 = new com.helpshift.z.o
            r6 = 1
            r5.<init>(r6)
            com.helpshift.j.c.a r7 = r4.f4375b
            java.lang.String r7 = r7.c()
            com.helpshift.i.a.a r8 = r4.f4374a
            java.lang.String r9 = "conversationPrefillText"
            java.lang.String r8 = r8.c(r9)
            com.helpshift.j.c.a r9 = r4.f4375b
            com.helpshift.j.b.b r10 = r9.h
            com.helpshift.a.b.c r9 = r9.e
            java.lang.Long r9 = r9.f3176a
            long r11 = r9.longValue()
            com.helpshift.j.d.a r9 = r10.a(r11)
            java.lang.String r10 = ""
            if (r9 == 0) goto L_0x006f
            int r11 = r9.c
            if (r11 != r6) goto L_0x006f
            java.lang.String r11 = r9.f3580a
            long r12 = java.lang.System.nanoTime()
            long r14 = r9.f3581b
            long r12 = r12 - r14
            r14 = 0
            int r9 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r9 < 0) goto L_0x006a
            java.util.concurrent.TimeUnit r9 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r12 = r9.toSeconds(r12)
            r14 = 7200(0x1c20, double:3.5573E-320)
            int r9 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r9 <= 0) goto L_0x0070
        L_0x006a:
            com.helpshift.j.c.a r9 = r4.f4375b
            r9.a(r10, r3)
        L_0x006f:
            r11 = r10
        L_0x0070:
            boolean r9 = com.helpshift.common.k.a(r11)
            if (r9 != 0) goto L_0x0077
            goto L_0x0094
        L_0x0077:
            boolean r9 = com.helpshift.common.k.a(r7)
            if (r9 != 0) goto L_0x0085
            com.helpshift.j.c.a r4 = r4.f4375b
            r8 = 3
            r4.a(r7, r8)
            r11 = r7
            goto L_0x0094
        L_0x0085:
            boolean r7 = com.helpshift.common.k.a(r8)
            if (r7 != 0) goto L_0x0093
            com.helpshift.j.c.a r4 = r4.f4375b
            r7 = 2
            r4.a(r8, r7)
            r11 = r8
            goto L_0x0094
        L_0x0093:
            r11 = r10
        L_0x0094:
            r5.a(r11)
            r0.f = r5
            com.helpshift.z.t r4 = r0.e
            com.helpshift.z.o r5 = new com.helpshift.z.o
            r5.<init>(r6)
            com.helpshift.i.a.a r7 = r4.f4374a
            boolean r7 = r7.g()
            if (r7 != 0) goto L_0x00c3
            com.helpshift.j.c.a r4 = r4.f4375b
            com.helpshift.j.b.b r7 = r4.h
            com.helpshift.a.b.c r8 = r4.e
            java.lang.Long r8 = r8.f3176a
            long r8 = r8.longValue()
            java.lang.String r7 = r7.b(r8)
            boolean r8 = com.helpshift.common.k.a(r7)
            if (r8 == 0) goto L_0x00c5
            com.helpshift.a.b.c r4 = r4.e
            java.lang.String r7 = r4.d
            goto L_0x00c5
        L_0x00c3:
            java.lang.String r7 = "Anonymous"
        L_0x00c5:
            r5.a(r7)
            r0.g = r5
            com.helpshift.z.t r4 = r0.e
            com.helpshift.z.o r5 = new com.helpshift.z.o
            com.helpshift.i.a.a r7 = r4.f4374a
            java.lang.String r8 = "fullPrivacy"
            boolean r7 = r7.a(r8)
            java.lang.String r9 = "requireEmail"
            java.lang.String r10 = "profileFormEnable"
            java.lang.String r11 = "requireNameAndEmail"
            if (r7 == 0) goto L_0x00e0
        L_0x00de:
            r7 = 0
            goto L_0x00fb
        L_0x00e0:
            com.helpshift.i.a.a r7 = r4.f4374a
            boolean r7 = r7.a(r11)
            if (r7 == 0) goto L_0x00ea
        L_0x00e8:
            r7 = 1
            goto L_0x00fb
        L_0x00ea:
            com.helpshift.i.a.a r7 = r4.f4374a
            boolean r7 = r7.a(r10)
            if (r7 == 0) goto L_0x00de
            com.helpshift.i.a.a r7 = r4.f4374a
            boolean r7 = r7.a(r9)
            if (r7 == 0) goto L_0x00de
            goto L_0x00e8
        L_0x00fb:
            r5.<init>(r7)
            com.helpshift.i.a.a r7 = r4.f4374a
            boolean r7 = r7.g()
            if (r7 != 0) goto L_0x0123
            com.helpshift.j.c.a r4 = r4.f4375b
            com.helpshift.j.b.b r7 = r4.h
            com.helpshift.a.b.c r12 = r4.e
            java.lang.Long r12 = r12.f3176a
            long r12 = r12.longValue()
            java.lang.String r7 = r7.c(r12)
            boolean r12 = com.helpshift.common.k.a(r7)
            if (r12 == 0) goto L_0x0120
            com.helpshift.a.b.c r4 = r4.e
            java.lang.String r7 = r4.c
        L_0x0120:
            r5.a(r7)
        L_0x0123:
            r0.h = r5
            com.helpshift.z.t r4 = r0.e
            com.helpshift.z.k r5 = new com.helpshift.z.k
            r5.<init>()
            com.helpshift.i.a.a r7 = r4.f4374a
            boolean r7 = r7.a(r8)
            if (r7 == 0) goto L_0x013c
            r7 = 0
            r5.a(r7)
            r4.a(r5)
            goto L_0x0157
        L_0x013c:
            com.helpshift.j.c.a r7 = r4.f4375b
            com.helpshift.j.b.b r12 = r7.h
            com.helpshift.a.b.c r7 = r7.e
            java.lang.Long r7 = r7.f3176a
            long r13 = r7.longValue()
            com.helpshift.j.d.d r7 = r12.d(r13)
            r5.a(r7)
            com.helpshift.j.c.a r4 = r4.f4375b
            boolean r4 = r4.q
            r4 = r4 ^ r6
            r5.a(r4)
        L_0x0157:
            r0.i = r5
            com.helpshift.z.t r4 = r0.e
            com.helpshift.z.h r5 = new com.helpshift.z.h
            r5.<init>()
            com.helpshift.j.c.a r4 = r4.f4375b
            boolean r4 = r4.q
            r5.b(r4)
            r0.j = r5
            com.helpshift.z.t r4 = r0.e
            com.helpshift.z.o r5 = r0.g
            com.helpshift.z.o r7 = r0.h
            com.helpshift.z.h r12 = new com.helpshift.z.h
            r12.<init>()
            com.helpshift.i.a.a r13 = r4.f4374a
            boolean r8 = r13.a(r8)
            if (r8 == 0) goto L_0x017e
        L_0x017c:
            r4 = 0
            goto L_0x01c6
        L_0x017e:
            com.helpshift.i.a.a r8 = r4.f4374a
            boolean r8 = r8.a(r10)
            com.helpshift.i.a.a r10 = r4.f4374a
            java.lang.String r13 = "hideNameAndEmail"
            boolean r10 = r10.a(r13)
            java.lang.String r5 = r5.b()
            int r5 = r5.length()
            if (r5 <= 0) goto L_0x0198
            r5 = 1
            goto L_0x0199
        L_0x0198:
            r5 = 0
        L_0x0199:
            java.lang.String r7 = r7.b()
            int r7 = r7.length()
            if (r7 <= 0) goto L_0x01a5
            r7 = 1
            goto L_0x01a6
        L_0x01a5:
            r7 = 0
        L_0x01a6:
            com.helpshift.i.a.a r13 = r4.f4374a
            boolean r11 = r13.a(r11)
            if (r11 == 0) goto L_0x01b5
            if (r10 == 0) goto L_0x01b5
            if (r5 == 0) goto L_0x01c5
            if (r7 != 0) goto L_0x017c
            goto L_0x01c5
        L_0x01b5:
            if (r8 == 0) goto L_0x017c
            if (r10 == 0) goto L_0x01c5
            com.helpshift.i.a.a r4 = r4.f4374a
            boolean r4 = r4.a(r9)
            if (r4 == 0) goto L_0x01c3
            if (r7 == 0) goto L_0x01c5
        L_0x01c3:
            if (r5 != 0) goto L_0x017c
        L_0x01c5:
            r4 = 1
        L_0x01c6:
            r12.b(r4)
            r0.m = r12
            com.helpshift.z.t r4 = r0.e
            com.helpshift.z.k r5 = r0.i
            com.helpshift.z.h r7 = new com.helpshift.z.h
            r7.<init>()
            boolean r8 = r4.a()
            if (r8 == 0) goto L_0x01eb
            java.lang.String r5 = r5.b()
            boolean r5 = com.helpshift.common.k.a(r5)
            if (r5 == 0) goto L_0x01eb
            com.helpshift.j.c.a r4 = r4.f4375b
            boolean r4 = r4.q
            if (r4 != 0) goto L_0x01eb
            r3 = 1
        L_0x01eb:
            r7.b(r3)
            r0.l = r7
            com.helpshift.z.t r3 = r0.e
            com.helpshift.z.h r4 = new com.helpshift.z.h
            r4.<init>()
            com.helpshift.j.c.a r3 = r3.f4375b
            boolean r3 = r3.q
            r3 = r3 ^ r6
            r4.b(r3)
            r0.k = r4
            java.lang.ref.WeakReference r3 = new java.lang.ref.WeakReference
            r3.<init>(r0)
            r2.o = r3
            com.helpshift.a.a r1 = r1.j
            r1.a(r0)
            java.lang.ref.WeakReference r1 = new java.lang.ref.WeakReference
            r2 = r20
            r1.<init>(r2)
            r0.n = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.i.aw.<init>(com.helpshift.common.e.ab, com.helpshift.common.c.j, com.helpshift.j.c.a, com.helpshift.j.i.av):void");
    }

    public final void a(String str) {
        this.f3654a.a(new ax(this, str));
    }

    public final void b(String str) {
        this.f3654a.a(new be(this, str));
    }

    public final void c(String str) {
        this.f3654a.a(new bf(this, str));
    }

    public final void a(boolean z) {
        this.f3654a.a(new bg(this, z));
    }

    public final void b() {
        c(true);
    }

    public final void c() {
        c(false);
    }

    private void c(boolean z) {
        this.f3654a.a(new bh(this, z));
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return !this.o && this.c.a("showSearchOnNewConversation");
    }

    public final void a(long j2) {
        this.j.b(false);
        this.k.b(true);
        this.f.a((String) null);
        this.i.a((d) null);
        this.l.b(com.helpshift.common.k.a(this.i.b()));
        this.f3654a.c(new bj(this, j2));
    }

    public final void a(Exception exc) {
        this.j.b(false);
        this.k.b(true);
        this.i.a(true);
        this.l.b(com.helpshift.common.k.a(this.i.b()));
        this.f3654a.c(new bk(this, exc));
    }

    public final void a(av avVar) {
        WeakReference<av> weakReference = this.n;
        if (weakReference != null && weakReference.get() == avVar) {
            this.n = new WeakReference<>(null);
        }
        this.f3654a.j.b(this);
        com.helpshift.j.c.a aVar = this.d;
        if (aVar.o != null && aVar.o.get() == this) {
            aVar.o = new WeakReference<>(null);
        }
    }

    public final void d(String str) {
        this.f3654a.a(new bl(this, str));
    }

    public final void e() {
        if (!this.j.a()) {
            this.f3654a.a(new bm(this));
            a((d) null);
        }
    }

    public final void a(d dVar) {
        this.f3654a.a(new ay(this, dVar));
    }

    public final void f() {
        if (!this.j.a()) {
            this.f3654a.a(new az(this));
        }
    }

    public final void b(boolean z) {
        this.f3654a.a(new bb(this, z));
    }

    public final void a(int i2) {
        this.f3654a.a(new bc(this, i2));
    }

    public final void a() {
        this.f3654a.c(new bd(this));
    }

    public final s g() {
        return this.f;
    }

    public final com.helpshift.z.a h() {
        return this.j;
    }

    public final com.helpshift.z.a i() {
        return this.k;
    }

    public final com.helpshift.z.a j() {
        return this.l;
    }

    public final g k() {
        return this.i;
    }

    public final s l() {
        return this.g;
    }

    public final s m() {
        return this.h;
    }

    public final com.helpshift.z.a n() {
        return this.m;
    }
}
