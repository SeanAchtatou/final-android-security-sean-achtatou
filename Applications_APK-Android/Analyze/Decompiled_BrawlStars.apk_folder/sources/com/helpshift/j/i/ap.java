package com.helpshift.j.i;

import com.helpshift.common.c.l;
import com.helpshift.j.a.a.v;
import com.helpshift.util.aa;
import java.util.List;

/* compiled from: MessageListVM */
class ap extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f3645a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ an f3646b;

    ap(an anVar, List list) {
        this.f3646b = anVar;
        this.f3645a = list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.i.an.a(java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.a.v, boolean):java.util.List<com.helpshift.j.a.a.v>
     arg types: [java.util.List, com.helpshift.j.a.a.v, int]
     candidates:
      com.helpshift.j.i.an.a(long, int, int):int
      com.helpshift.j.i.an.a(java.util.Date, boolean, java.lang.Long):com.helpshift.j.a.a.ad
      com.helpshift.j.i.an.a(com.helpshift.j.a.a.v, boolean, boolean):boolean
      com.helpshift.j.i.an.a(java.util.List<com.helpshift.j.a.a.v>, int, int):com.helpshift.util.aa<java.lang.Integer, java.lang.Integer>
      com.helpshift.j.i.an.a(java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.a.v, boolean):java.util.List<com.helpshift.j.a.a.v> */
    public final void a() {
        int intValue;
        an anVar = this.f3646b;
        v a2 = anVar.a(anVar.d.size() - 1);
        if (a2 == null || a2.C <= ((v) this.f3645a.get(0)).C) {
            an anVar2 = this.f3646b;
            List list = this.f3645a;
            int size = anVar2.d.size();
            int i = size - 1;
            List<v> a3 = anVar2.a((List<v>) list, anVar2.a(i), true);
            anVar2.d.addAll(a3);
            aa<Integer, Integer> a4 = anVar2.a(anVar2.d, i, anVar2.d.size() - 1);
            if (anVar2.c != null) {
                anVar2.c.a(size, a3.size());
                if (a4 != null && (intValue = ((Integer) a4.f4242a).intValue()) < size) {
                    anVar2.c.b(intValue, size - intValue);
                }
            }
        } else {
            an anVar3 = this.f3646b;
            for (v a5 : this.f3645a) {
                anVar3.a(a5);
            }
            anVar3.a();
        }
        an.a(this.f3646b, this.f3645a);
        this.f3646b.b();
    }
}
