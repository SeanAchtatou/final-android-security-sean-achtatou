package com.helpshift.j.i;

import com.helpshift.common.c.l;
import com.helpshift.j.a.a.ab;
import com.helpshift.j.a.a.al;
import com.helpshift.j.a.a.am;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.b;
import com.helpshift.j.a.b.a;
import com.helpshift.util.n;

/* compiled from: ConversationalVM */
class d extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f3694a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f3695b;

    d(b bVar, v vVar) {
        this.f3695b = bVar;
        this.f3694a = vVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.i.b.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.helpshift.j.i.b.a(java.util.Collection<? extends com.helpshift.j.a.a.v>, boolean):java.util.List<com.helpshift.j.a.a.v>
      com.helpshift.j.i.b.a(com.helpshift.j.a.a.v, com.helpshift.j.a.a.v):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.b, com.helpshift.common.exception.RootAPIException):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.b, com.helpshift.j.a.a.x):void
      com.helpshift.j.i.b.a(int, int):void
      com.helpshift.j.i.b.a(int, java.lang.String):void
      com.helpshift.j.i.b.a(com.helpshift.b.b, java.util.Map<java.lang.String, java.lang.Object>):void
      com.helpshift.j.i.b.a(com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.j.i.b.a(com.helpshift.j.i.bn, boolean):void
      com.helpshift.j.i.b.a(java.lang.String, com.helpshift.j.a.a.v):void
      com.helpshift.j.i.b.a(java.lang.String, java.lang.String):void
      com.helpshift.j.i.b.a(java.util.List<com.helpshift.j.a.b.a>, boolean):void
      com.helpshift.j.i.a.a(java.lang.String, java.lang.String):void
      com.helpshift.j.i.a.a(java.util.List<com.helpshift.j.a.b.a>, boolean):void
      com.helpshift.j.i.au.a(int, int):void
      com.helpshift.j.i.b.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.ab, boolean):void
     arg types: [com.helpshift.j.a.b.a, com.helpshift.j.a.a.ab, int]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.d, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.ab, boolean):void */
    public final void a() {
        a h = this.f3695b.f.h();
        if ((this.f3694a instanceof al) && !b.f(h)) {
            n.a("Helpshift_ConvsatnlVM", "User retrying message to file preissue.", (Throwable) null, (com.helpshift.p.b.a[]) null);
            ((al) this.f3694a).a(am.SENDING);
            this.f3695b.a(this.f3694a.n, true);
        } else if (this.f3695b.f3661a) {
            b bVar = this.f3695b.n;
            v vVar = this.f3694a;
            if (vVar instanceof al) {
                bVar.a(h, (al) vVar);
            } else if (vVar instanceof ab) {
                bVar.a(h, (ab) vVar, false);
            }
            b bVar2 = this.f3695b;
            bVar2.b(bVar2.f3662b);
        }
    }
}
