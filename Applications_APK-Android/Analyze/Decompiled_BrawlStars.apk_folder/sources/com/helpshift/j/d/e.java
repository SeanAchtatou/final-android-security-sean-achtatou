package com.helpshift.j.d;

import java.util.HashMap;
import java.util.Map;

/* compiled from: IssueState */
public enum e {
    NEW(0),
    NEW_FOR_AGENT(1),
    AGENT_REPLIED(2),
    WAITING_FOR_AGENT(3),
    RESOLUTION_REQUESTED(4),
    REJECTED(5),
    PENDING_REASSIGNMENT(6),
    COMPLETED_ISSUE_CREATED(7),
    RESOLUTION_ACCEPTED(101),
    RESOLUTION_REJECTED(102),
    ARCHIVED(103),
    AUTHOR_MISMATCH(104),
    UNKNOWN(-1);
    
    private static final Map<Integer, e> o = new HashMap();
    public final int n;

    private e(int i) {
        this.n = i;
    }

    public static e a(int i) {
        if (o.size() == 0) {
            for (e eVar : values()) {
                o.put(Integer.valueOf(eVar.n), eVar);
            }
        }
        e eVar2 = o.get(Integer.valueOf(i));
        return eVar2 == null ? UNKNOWN : eVar2;
    }

    public final int a() {
        return this.n;
    }
}
