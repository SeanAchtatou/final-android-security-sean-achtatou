package com.helpshift.j.d.a;

import com.helpshift.j.d.d;

/* compiled from: ConversationInboxRecord */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public final long f3582a;

    /* renamed from: b  reason: collision with root package name */
    public final String f3583b;
    public final String c;
    public final String d;
    public final long e;
    public final d f;
    public final int g;
    public final String h;
    public final String i;
    public final boolean j;
    public final String k;
    public final Boolean l;
    public final Long m;

    public a(long j2, String str, String str2, String str3, long j3, d dVar, int i2, String str4, String str5, boolean z, String str6, Boolean bool, Long l2) {
        this.f3582a = j2;
        this.f3583b = str;
        this.c = str2;
        this.d = str3;
        this.e = j3;
        this.f = dVar;
        this.g = i2;
        this.h = str4;
        this.i = str5;
        this.j = z;
        this.k = str6;
        this.l = bool;
        this.m = l2;
    }

    /* renamed from: com.helpshift.j.d.a.a$a  reason: collision with other inner class name */
    /* compiled from: ConversationInboxRecord */
    public static final class C0140a {

        /* renamed from: a  reason: collision with root package name */
        private long f3584a;

        /* renamed from: b  reason: collision with root package name */
        private String f3585b;
        private String c;
        private String d;
        private long e;
        private d f;
        private int g;
        private String h;
        private String i;
        private boolean j;
        private String k;
        private Boolean l;
        private Long m;

        public C0140a(long j2) {
            this.f3584a = j2;
        }

        public C0140a(a aVar) {
            this.f3584a = aVar.f3582a;
            this.f3585b = aVar.f3583b;
            this.c = aVar.c;
            this.d = aVar.d;
            this.e = aVar.e;
            this.f = aVar.f;
            this.g = aVar.g;
            this.h = aVar.h;
            this.k = aVar.k;
            this.j = aVar.j;
            this.i = aVar.i;
            this.l = aVar.l;
            this.m = aVar.m;
        }

        public final C0140a a(String str) {
            this.f3585b = str;
            return this;
        }

        public final C0140a b(String str) {
            this.c = str;
            return this;
        }

        public final C0140a c(String str) {
            this.d = str;
            return this;
        }

        public final C0140a a(long j2) {
            this.e = j2;
            return this;
        }

        public final C0140a a(d dVar) {
            this.f = dVar;
            return this;
        }

        public final C0140a a(int i2) {
            this.g = i2;
            return this;
        }

        public final C0140a d(String str) {
            this.h = str;
            return this;
        }

        public final C0140a e(String str) {
            this.i = str;
            return this;
        }

        public final C0140a f(String str) {
            this.k = str;
            return this;
        }

        public final C0140a a(boolean z) {
            this.j = z;
            return this;
        }

        public final C0140a b(boolean z) {
            this.l = Boolean.valueOf(z);
            return this;
        }

        public final C0140a a(Long l2) {
            this.m = l2;
            return this;
        }

        public final a a() {
            return new a(this.f3584a, this.f3585b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m);
        }
    }
}
