package com.helpshift.j.d;

import com.helpshift.j.a.b.a;
import java.util.List;

/* compiled from: ConversationInbox */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public final boolean f3588a;

    /* renamed from: b  reason: collision with root package name */
    public final String f3589b;
    public final List<a> c;
    public final Boolean d;

    public c(String str, List<a> list, boolean z, Boolean bool) {
        this.f3589b = str;
        this.c = list;
        this.f3588a = z;
        this.d = bool;
    }
}
