package com.helpshift.j.e;

import com.helpshift.common.g.b;
import com.helpshift.common.j;
import com.helpshift.common.k;
import com.helpshift.j.a.a.v;
import com.helpshift.j.c;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ConversationDBLoader */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected com.helpshift.j.b.a f3597a;

    /* renamed from: b  reason: collision with root package name */
    boolean f3598b = true;

    public abstract List<com.helpshift.j.a.b.a> a(String str, String str2, long j);

    protected a(com.helpshift.j.b.a aVar) {
        this.f3597a = aVar;
    }

    static List<v> a(String str, long j, List<v> list) {
        List<v> list2;
        if (j.a(list) || j < 1) {
            return new ArrayList();
        }
        c.b(list);
        if (k.a(str)) {
            list2 = list;
        } else {
            long b2 = b.b(str);
            list2 = new ArrayList<>();
            for (v next : list) {
                if (b2 <= next.C) {
                    break;
                }
                list2.add(next);
            }
            if (j.a(list2)) {
                return new ArrayList();
            }
        }
        int size = list2.size();
        return list2.subList(Math.max(0, (int) (((long) size) - j)), size);
    }
}
