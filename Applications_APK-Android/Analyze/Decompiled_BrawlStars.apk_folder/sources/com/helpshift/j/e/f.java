package com.helpshift.j.e;

import com.helpshift.j.b.a;
import com.helpshift.j.c;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: SingleConversationDBLoader */
public class f extends a {
    private Long c;

    public f(a aVar, Long l) {
        super(aVar);
        this.c = l;
    }

    public final List<com.helpshift.j.a.b.a> a(String str, String str2, long j) {
        com.helpshift.j.a.b.a a2 = this.f3597a.a(this.c);
        if (a2 == null) {
            return new ArrayList();
        }
        a2.a(a(str2, j, this.f3597a.c(this.c.longValue())));
        c.b(a2.j);
        return Collections.singletonList(a2);
    }
}
