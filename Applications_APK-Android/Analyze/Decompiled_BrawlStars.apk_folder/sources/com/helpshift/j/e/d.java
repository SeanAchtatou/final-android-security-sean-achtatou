package com.helpshift.j.e;

import com.helpshift.common.j;
import java.util.List;

/* compiled from: ConversationsLoader */
public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    protected final a f3599a;

    /* renamed from: b  reason: collision with root package name */
    protected final e f3600b;
    private final long c;

    /* compiled from: ConversationsLoader */
    public interface a {
        void a(List<com.helpshift.j.a.b.a> list, boolean z);

        void o();

        void p();
    }

    public abstract boolean a();

    public d(a aVar, e eVar, long j) {
        this.f3599a = aVar;
        this.f3600b = eVar;
        this.c = j;
    }

    public final List<com.helpshift.j.a.b.a> b() {
        List<com.helpshift.j.a.b.a> a2 = this.f3599a.a((String) null, (String) null, this.c);
        a(a2);
        return a2;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:30|31) */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r8.o();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0090, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x008a */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:36:0x0091=Splitter:B:36:0x0091, B:19:0x0044=Splitter:B:19:0x0044, B:25:0x0056=Splitter:B:25:0x0056} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.helpshift.j.a.u r7, com.helpshift.j.e.d.a r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = 0
            if (r7 == 0) goto L_0x0091
            boolean r1 = r6.a()     // Catch:{ all -> 0x009b }
            if (r1 != 0) goto L_0x000c
            goto L_0x0091
        L_0x000c:
            java.lang.String r1 = r7.f3543a     // Catch:{ all -> 0x009b }
            boolean r1 = com.helpshift.common.k.a(r1)     // Catch:{ all -> 0x009b }
            if (r1 != 0) goto L_0x008f
            java.lang.String r1 = r7.f3544b     // Catch:{ all -> 0x009b }
            boolean r1 = com.helpshift.common.k.a(r1)     // Catch:{ all -> 0x009b }
            if (r1 == 0) goto L_0x001d
            goto L_0x008f
        L_0x001d:
            r8.p()     // Catch:{ all -> 0x009b }
            com.helpshift.j.e.a r1 = r6.f3599a     // Catch:{ all -> 0x009b }
            boolean r1 = r1.f3598b     // Catch:{ all -> 0x009b }
            if (r1 == 0) goto L_0x0044
            com.helpshift.j.e.a r1 = r6.f3599a     // Catch:{ all -> 0x009b }
            java.lang.String r2 = r7.f3543a     // Catch:{ all -> 0x009b }
            java.lang.String r3 = r7.f3544b     // Catch:{ all -> 0x009b }
            long r4 = r6.c     // Catch:{ all -> 0x009b }
            java.util.List r1 = r1.a(r2, r3, r4)     // Catch:{ all -> 0x009b }
            r6.a(r1)     // Catch:{ all -> 0x009b }
            boolean r2 = com.helpshift.common.j.a(r1)     // Catch:{ all -> 0x009b }
            if (r2 != 0) goto L_0x0044
            boolean r7 = r6.a()     // Catch:{ all -> 0x009b }
            r8.a(r1, r7)     // Catch:{ all -> 0x009b }
            monitor-exit(r6)
            return
        L_0x0044:
            com.helpshift.j.e.e r1 = r6.f3600b     // Catch:{ all -> 0x009b }
            boolean r1 = r1.a()     // Catch:{ all -> 0x009b }
            if (r1 != 0) goto L_0x0056
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x009b }
            r7.<init>()     // Catch:{ all -> 0x009b }
            r8.a(r7, r0)     // Catch:{ all -> 0x009b }
            monitor-exit(r6)
            return
        L_0x0056:
            r8.p()     // Catch:{ RootAPIException -> 0x008a }
            com.helpshift.j.e.e r0 = r6.f3600b     // Catch:{ RootAPIException -> 0x008a }
            boolean r0 = r0.b()     // Catch:{ RootAPIException -> 0x008a }
            if (r0 == 0) goto L_0x007d
            com.helpshift.j.e.a r0 = r6.f3599a     // Catch:{ RootAPIException -> 0x008a }
            r1 = 1
            r0.f3598b = r1     // Catch:{ RootAPIException -> 0x008a }
            com.helpshift.j.e.a r0 = r6.f3599a     // Catch:{ RootAPIException -> 0x008a }
            java.lang.String r1 = r7.f3543a     // Catch:{ RootAPIException -> 0x008a }
            java.lang.String r7 = r7.f3544b     // Catch:{ RootAPIException -> 0x008a }
            long r2 = r6.c     // Catch:{ RootAPIException -> 0x008a }
            java.util.List r7 = r0.a(r1, r7, r2)     // Catch:{ RootAPIException -> 0x008a }
            r6.a(r7)     // Catch:{ RootAPIException -> 0x008a }
            boolean r0 = r6.a()     // Catch:{ RootAPIException -> 0x008a }
            r8.a(r7, r0)     // Catch:{ RootAPIException -> 0x008a }
            goto L_0x008d
        L_0x007d:
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ RootAPIException -> 0x008a }
            r7.<init>()     // Catch:{ RootAPIException -> 0x008a }
            boolean r0 = r6.a()     // Catch:{ RootAPIException -> 0x008a }
            r8.a(r7, r0)     // Catch:{ RootAPIException -> 0x008a }
            goto L_0x008d
        L_0x008a:
            r8.o()     // Catch:{ all -> 0x009b }
        L_0x008d:
            monitor-exit(r6)
            return
        L_0x008f:
            monitor-exit(r6)
            return
        L_0x0091:
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x009b }
            r7.<init>()     // Catch:{ all -> 0x009b }
            r8.a(r7, r0)     // Catch:{ all -> 0x009b }
            monitor-exit(r6)
            return
        L_0x009b:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.e.d.a(com.helpshift.j.a.u, com.helpshift.j.e.d$a):void");
    }

    private void a(List<com.helpshift.j.a.b.a> list) {
        if (j.a(list)) {
            this.f3599a.f3598b = false;
        }
        int i = 0;
        for (com.helpshift.j.a.b.a aVar : list) {
            i += aVar.j.size();
        }
        if (i == 0) {
            this.f3599a.f3598b = false;
        }
    }
}
