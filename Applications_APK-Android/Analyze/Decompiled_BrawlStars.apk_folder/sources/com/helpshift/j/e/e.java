package com.helpshift.j.e;

import com.helpshift.a.b.c;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.j.b.a;
import com.helpshift.j.b.b;
import com.helpshift.j.c.k;

/* compiled from: RemoteConversationLoader */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private ab f3601a;

    /* renamed from: b  reason: collision with root package name */
    private j f3602b;
    private c c;
    private a d;
    private b e;
    private com.helpshift.j.c.j f;
    private k g;

    public e(ab abVar, j jVar, c cVar, com.helpshift.j.a.b bVar) {
        this.f3601a = abVar;
        this.f3602b = jVar;
        this.c = cVar;
        this.d = abVar.f();
        this.e = abVar.e();
        this.f = new com.helpshift.j.c.j(abVar, jVar, cVar);
        this.g = new k(abVar, jVar, cVar, bVar);
    }

    public final boolean a() {
        return this.e.j(this.c.f3176a.longValue());
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v27, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v4, resolved type: com.helpshift.j.a.b.a} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v28, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v6, resolved type: com.helpshift.j.a.b.a} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.p):void
     arg types: [com.helpshift.j.a.b.a, int, com.helpshift.common.g.d<com.helpshift.j.a.a.v>, com.helpshift.j.a.p]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, com.helpshift.j.a.p):com.helpshift.j.a.a.v
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, int, java.lang.String, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, boolean, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String, com.helpshift.j.a.a.j, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, int, java.lang.String, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.p):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, boolean, com.helpshift.j.a.p):void
     arg types: [com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, int, com.helpshift.j.a.p]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, com.helpshift.j.a.p):com.helpshift.j.a.a.v
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, int, java.lang.String, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String, com.helpshift.j.a.a.j, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, int, java.lang.String, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, boolean, com.helpshift.j.a.p):void */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0218, code lost:
        return true;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean b() throws com.helpshift.common.exception.RootAPIException {
        /*
            r16 = this;
            r1 = r16
            monitor-enter(r16)
            boolean r0 = r16.a()     // Catch:{ all -> 0x0232 }
            r2 = 0
            if (r0 != 0) goto L_0x000c
            monitor-exit(r16)
            return r2
        L_0x000c:
            com.helpshift.j.b.a r0 = r1.d     // Catch:{ all -> 0x0232 }
            com.helpshift.a.b.c r3 = r1.c     // Catch:{ all -> 0x0232 }
            java.lang.Long r3 = r3.f3176a     // Catch:{ all -> 0x0232 }
            long r3 = r3.longValue()     // Catch:{ all -> 0x0232 }
            java.lang.String r0 = r0.e(r3)     // Catch:{ all -> 0x0232 }
            boolean r3 = com.helpshift.common.k.a(r0)     // Catch:{ all -> 0x0232 }
            if (r3 == 0) goto L_0x0022
            monitor-exit(r16)
            return r2
        L_0x0022:
            com.helpshift.j.c.j r3 = r1.f     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.c.b.q r4 = new com.helpshift.common.c.b.q     // Catch:{ RootAPIException -> 0x0219 }
            java.lang.String r5 = "/conversations/history/"
            com.helpshift.common.c.j r6 = r3.f3575b     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.e.ab r7 = r3.f3574a     // Catch:{ RootAPIException -> 0x0219 }
            r4.<init>(r5, r6, r7)     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.c.b.b r5 = new com.helpshift.common.c.b.b     // Catch:{ RootAPIException -> 0x0219 }
            r5.<init>(r4)     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.c.b.g r4 = new com.helpshift.common.c.b.g     // Catch:{ RootAPIException -> 0x0219 }
            r4.<init>(r5)     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.c.b.s r5 = new com.helpshift.common.c.b.s     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.e.ab r6 = r3.f3574a     // Catch:{ RootAPIException -> 0x0219 }
            r5.<init>(r4, r6)     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.c.b.j r4 = new com.helpshift.common.c.b.j     // Catch:{ RootAPIException -> 0x0219 }
            r4.<init>(r5)     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.a.b.c r5 = r3.c     // Catch:{ RootAPIException -> 0x0219 }
            java.util.HashMap r5 = com.helpshift.common.c.b.o.a(r5)     // Catch:{ RootAPIException -> 0x0219 }
            java.lang.String r6 = "cursor"
            r5.put(r6, r0)     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.e.a.i r0 = new com.helpshift.common.e.a.i     // Catch:{ RootAPIException -> 0x0219 }
            r0.<init>(r5)     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.e.a.j r0 = r4.a(r0)     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.e.ab r3 = r3.f3574a     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.common.e.a.k r3 = r3.l()     // Catch:{ RootAPIException -> 0x0219 }
            java.lang.String r0 = r0.f3323b     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.j.d.b r0 = r3.h(r0)     // Catch:{ RootAPIException -> 0x0219 }
            com.helpshift.j.b.b r3 = r1.e     // Catch:{ all -> 0x0232 }
            com.helpshift.a.b.c r4 = r1.c     // Catch:{ all -> 0x0232 }
            java.lang.Long r4 = r4.f3176a     // Catch:{ all -> 0x0232 }
            long r4 = r4.longValue()     // Catch:{ all -> 0x0232 }
            boolean r6 = r0.f3587b     // Catch:{ all -> 0x0232 }
            r3.b(r4, r6)     // Catch:{ all -> 0x0232 }
            com.helpshift.j.c.k r3 = r1.g     // Catch:{ all -> 0x0232 }
            java.util.List<com.helpshift.j.a.b.a> r0 = r0.f3586a     // Catch:{ all -> 0x0232 }
            boolean r4 = com.helpshift.common.j.a(r0)     // Catch:{ all -> 0x0232 }
            r5 = 1
            if (r4 == 0) goto L_0x0081
            goto L_0x0217
        L_0x0081:
            java.util.HashSet r4 = new java.util.HashSet     // Catch:{ all -> 0x0232 }
            r4.<init>()     // Catch:{ all -> 0x0232 }
            java.util.HashMap r6 = new java.util.HashMap     // Catch:{ all -> 0x0232 }
            r6.<init>()     // Catch:{ all -> 0x0232 }
            java.util.HashSet r7 = new java.util.HashSet     // Catch:{ all -> 0x0232 }
            r7.<init>()     // Catch:{ all -> 0x0232 }
            int r8 = r0.size()     // Catch:{ all -> 0x0232 }
            if (r8 <= r5) goto L_0x0099
            com.helpshift.j.c.a(r0)     // Catch:{ all -> 0x0232 }
        L_0x0099:
            com.helpshift.common.e.ab r8 = r3.f3576a     // Catch:{ all -> 0x0232 }
            com.helpshift.j.b.a r8 = r8.f()     // Catch:{ all -> 0x0232 }
            com.helpshift.a.b.c r9 = r3.f3577b     // Catch:{ all -> 0x0232 }
            java.lang.Long r9 = r9.f3176a     // Catch:{ all -> 0x0232 }
            long r9 = r9.longValue()     // Catch:{ all -> 0x0232 }
            java.util.List r8 = r8.b(r9)     // Catch:{ all -> 0x0232 }
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ all -> 0x0232 }
            r9.<init>()     // Catch:{ all -> 0x0232 }
            java.util.HashMap r10 = new java.util.HashMap     // Catch:{ all -> 0x0232 }
            r10.<init>()     // Catch:{ all -> 0x0232 }
            java.util.HashMap r11 = new java.util.HashMap     // Catch:{ all -> 0x0232 }
            r11.<init>()     // Catch:{ all -> 0x0232 }
            java.util.Iterator r8 = r8.iterator()     // Catch:{ all -> 0x0232 }
        L_0x00be:
            boolean r12 = r8.hasNext()     // Catch:{ all -> 0x0232 }
            if (r12 == 0) goto L_0x00e6
            java.lang.Object r12 = r8.next()     // Catch:{ all -> 0x0232 }
            com.helpshift.j.a.b.a r12 = (com.helpshift.j.a.b.a) r12     // Catch:{ all -> 0x0232 }
            java.lang.String r13 = r12.c     // Catch:{ all -> 0x0232 }
            boolean r13 = com.helpshift.common.k.a(r13)     // Catch:{ all -> 0x0232 }
            if (r13 != 0) goto L_0x00d8
            java.lang.String r13 = r12.c     // Catch:{ all -> 0x0232 }
            r10.put(r13, r12)     // Catch:{ all -> 0x0232 }
            goto L_0x00be
        L_0x00d8:
            java.lang.String r13 = r12.d     // Catch:{ all -> 0x0232 }
            boolean r13 = com.helpshift.common.k.a(r13)     // Catch:{ all -> 0x0232 }
            if (r13 != 0) goto L_0x00be
            java.lang.String r13 = r12.d     // Catch:{ all -> 0x0232 }
            r11.put(r13, r12)     // Catch:{ all -> 0x0232 }
            goto L_0x00be
        L_0x00e6:
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0232 }
        L_0x00ea:
            boolean r8 = r0.hasNext()     // Catch:{ all -> 0x0232 }
            if (r8 == 0) goto L_0x01b5
            java.lang.Object r8 = r0.next()     // Catch:{ all -> 0x0232 }
            com.helpshift.j.a.b.a r8 = (com.helpshift.j.a.b.a) r8     // Catch:{ all -> 0x0232 }
            java.lang.String r12 = r8.c     // Catch:{ all -> 0x0232 }
            java.lang.String r13 = r8.d     // Catch:{ all -> 0x0232 }
            com.helpshift.a.b.c r15 = r3.f3577b     // Catch:{ all -> 0x0232 }
            java.lang.Long r15 = r15.f3176a     // Catch:{ all -> 0x0232 }
            long r14 = r15.longValue()     // Catch:{ all -> 0x0232 }
            r8.t = r14     // Catch:{ all -> 0x0232 }
            boolean r14 = r10.containsKey(r12)     // Catch:{ all -> 0x0232 }
            if (r14 == 0) goto L_0x0112
            java.lang.Object r12 = r10.get(r12)     // Catch:{ all -> 0x0232 }
            r14 = r12
            com.helpshift.j.a.b.a r14 = (com.helpshift.j.a.b.a) r14     // Catch:{ all -> 0x0232 }
            goto L_0x0121
        L_0x0112:
            boolean r12 = r11.containsKey(r13)     // Catch:{ all -> 0x0232 }
            if (r12 == 0) goto L_0x0120
            java.lang.Object r12 = r11.get(r13)     // Catch:{ all -> 0x0232 }
            r14 = r12
            com.helpshift.j.a.b.a r14 = (com.helpshift.j.a.b.a) r14     // Catch:{ all -> 0x0232 }
            goto L_0x0121
        L_0x0120:
            r14 = 0
        L_0x0121:
            if (r14 == 0) goto L_0x0174
            com.helpshift.a.b.c r12 = r3.f3577b     // Catch:{ all -> 0x0232 }
            java.lang.Long r12 = r12.f3176a     // Catch:{ all -> 0x0232 }
            long r12 = r12.longValue()     // Catch:{ all -> 0x0232 }
            r14.t = r12     // Catch:{ all -> 0x0232 }
            java.lang.Long r12 = r14.f3504b     // Catch:{ all -> 0x0232 }
            boolean r12 = r6.containsKey(r12)     // Catch:{ all -> 0x0232 }
            if (r12 == 0) goto L_0x013e
            java.lang.Long r12 = r14.f3504b     // Catch:{ all -> 0x0232 }
            java.lang.Object r12 = r6.get(r12)     // Catch:{ all -> 0x0232 }
            com.helpshift.j.a.p r12 = (com.helpshift.j.a.p) r12     // Catch:{ all -> 0x0232 }
            goto L_0x0143
        L_0x013e:
            com.helpshift.j.a.p r12 = new com.helpshift.j.a.p     // Catch:{ all -> 0x0232 }
            r12.<init>()     // Catch:{ all -> 0x0232 }
        L_0x0143:
            boolean r13 = r8.a()     // Catch:{ all -> 0x0232 }
            if (r13 == 0) goto L_0x0165
            boolean r13 = r14.a()     // Catch:{ all -> 0x0232 }
            if (r13 == 0) goto L_0x0155
            com.helpshift.j.a.b r13 = r3.c     // Catch:{ all -> 0x0232 }
            r13.b(r14, r8, r2, r12)     // Catch:{ all -> 0x0232 }
            goto L_0x016a
        L_0x0155:
            com.helpshift.common.g.d<com.helpshift.j.a.a.v> r13 = r8.j     // Catch:{ all -> 0x0232 }
            boolean r13 = com.helpshift.common.j.a(r13)     // Catch:{ all -> 0x0232 }
            if (r13 != 0) goto L_0x016a
            com.helpshift.j.a.b r13 = r3.c     // Catch:{ all -> 0x0232 }
            com.helpshift.common.g.d<com.helpshift.j.a.a.v> r8 = r8.j     // Catch:{ all -> 0x0232 }
            r13.a(r14, r2, r8, r12)     // Catch:{ all -> 0x0232 }
            goto L_0x016a
        L_0x0165:
            com.helpshift.j.a.b r13 = r3.c     // Catch:{ all -> 0x0232 }
            r13.a(r14, r8, r2, r12)     // Catch:{ all -> 0x0232 }
        L_0x016a:
            r4.add(r14)     // Catch:{ all -> 0x0232 }
            java.lang.Long r8 = r14.f3504b     // Catch:{ all -> 0x0232 }
            r6.put(r8, r12)     // Catch:{ all -> 0x0232 }
            goto L_0x00ea
        L_0x0174:
            boolean r12 = r8.a()     // Catch:{ all -> 0x0232 }
            if (r12 == 0) goto L_0x018a
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0232 }
            r8.u = r12     // Catch:{ all -> 0x0232 }
            com.helpshift.j.d.e r12 = r8.g     // Catch:{ all -> 0x0232 }
            com.helpshift.j.d.e r13 = com.helpshift.j.d.e.RESOLUTION_REQUESTED     // Catch:{ all -> 0x0232 }
            if (r12 != r13) goto L_0x018a
            com.helpshift.j.d.e r12 = com.helpshift.j.d.e.RESOLUTION_ACCEPTED     // Catch:{ all -> 0x0232 }
            r8.g = r12     // Catch:{ all -> 0x0232 }
        L_0x018a:
            com.helpshift.j.d.e r12 = r8.g     // Catch:{ all -> 0x0232 }
            com.helpshift.j.d.e r13 = com.helpshift.j.d.e.RESOLUTION_ACCEPTED     // Catch:{ all -> 0x0232 }
            if (r12 == r13) goto L_0x019c
            com.helpshift.j.d.e r13 = com.helpshift.j.d.e.RESOLUTION_REJECTED     // Catch:{ all -> 0x0232 }
            if (r12 == r13) goto L_0x019c
            com.helpshift.j.d.e r13 = com.helpshift.j.d.e.REJECTED     // Catch:{ all -> 0x0232 }
            if (r12 == r13) goto L_0x019c
            com.helpshift.j.d.e r13 = com.helpshift.j.d.e.ARCHIVED     // Catch:{ all -> 0x0232 }
            if (r12 != r13) goto L_0x019e
        L_0x019c:
            r8.s = r5     // Catch:{ all -> 0x0232 }
        L_0x019e:
            if (r12 == 0) goto L_0x01b0
            boolean r12 = r8.x     // Catch:{ all -> 0x0232 }
            if (r12 == 0) goto L_0x01b0
            com.helpshift.j.d.e r12 = r8.g     // Catch:{ all -> 0x0232 }
            com.helpshift.j.d.e r13 = com.helpshift.j.d.e.RESOLUTION_REQUESTED     // Catch:{ all -> 0x0232 }
            if (r12 != r13) goto L_0x01b0
            r8.s = r5     // Catch:{ all -> 0x0232 }
            com.helpshift.j.d.e r12 = com.helpshift.j.d.e.RESOLUTION_ACCEPTED     // Catch:{ all -> 0x0232 }
            r8.g = r12     // Catch:{ all -> 0x0232 }
        L_0x01b0:
            r9.add(r8)     // Catch:{ all -> 0x0232 }
            goto L_0x00ea
        L_0x01b5:
            int r0 = r9.size()     // Catch:{ all -> 0x0232 }
            if (r0 > r5) goto L_0x01c2
            r7.addAll(r9)     // Catch:{ all -> 0x0232 }
            r3.a(r4, r7, r6)     // Catch:{ all -> 0x0232 }
            goto L_0x0217
        L_0x01c2:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0232 }
            r0.<init>(r9)     // Catch:{ all -> 0x0232 }
            int r2 = r0.size()     // Catch:{ all -> 0x0232 }
            int r2 = r2 - r5
        L_0x01cc:
            if (r2 < 0) goto L_0x0211
            java.lang.Object r8 = r0.get(r2)     // Catch:{ all -> 0x0232 }
            com.helpshift.j.a.b.a r8 = (com.helpshift.j.a.b.a) r8     // Catch:{ all -> 0x0232 }
            boolean r10 = r8.a()     // Catch:{ all -> 0x0232 }
            if (r10 != 0) goto L_0x020e
            int r10 = r2 + -1
        L_0x01dc:
            if (r10 < 0) goto L_0x020e
            java.lang.Object r11 = r0.get(r10)     // Catch:{ all -> 0x0232 }
            com.helpshift.j.a.b.a r11 = (com.helpshift.j.a.b.a) r11     // Catch:{ all -> 0x0232 }
            java.lang.String r12 = r8.d     // Catch:{ all -> 0x0232 }
            boolean r12 = com.helpshift.common.k.a(r12)     // Catch:{ all -> 0x0232 }
            if (r12 != 0) goto L_0x020b
            java.lang.String r12 = r8.d     // Catch:{ all -> 0x0232 }
            java.lang.String r13 = r11.d     // Catch:{ all -> 0x0232 }
            boolean r12 = r12.equals(r13)     // Catch:{ all -> 0x0232 }
            if (r12 == 0) goto L_0x020b
            java.lang.String r12 = r8.c     // Catch:{ all -> 0x0232 }
            java.lang.String r13 = r11.c     // Catch:{ all -> 0x0232 }
            boolean r12 = r12.equals(r13)     // Catch:{ all -> 0x0232 }
            if (r12 == 0) goto L_0x020b
            com.helpshift.common.g.d<com.helpshift.j.a.a.v> r8 = r8.j     // Catch:{ all -> 0x0232 }
            com.helpshift.common.g.d<com.helpshift.j.a.a.v> r11 = r11.j     // Catch:{ all -> 0x0232 }
            r8.addAll(r11)     // Catch:{ all -> 0x0232 }
            r9.remove(r10)     // Catch:{ all -> 0x0232 }
            goto L_0x020e
        L_0x020b:
            int r10 = r10 + -1
            goto L_0x01dc
        L_0x020e:
            int r2 = r2 + -1
            goto L_0x01cc
        L_0x0211:
            r7.addAll(r9)     // Catch:{ all -> 0x0232 }
            r3.a(r4, r7, r6)     // Catch:{ all -> 0x0232 }
        L_0x0217:
            monitor-exit(r16)
            return r5
        L_0x0219:
            r0 = move-exception
            com.helpshift.common.exception.a r2 = r0.c     // Catch:{ all -> 0x0232 }
            com.helpshift.common.exception.b r3 = com.helpshift.common.exception.b.INVALID_AUTH_TOKEN     // Catch:{ all -> 0x0232 }
            if (r2 == r3) goto L_0x0226
            com.helpshift.common.exception.a r2 = r0.c     // Catch:{ all -> 0x0232 }
            com.helpshift.common.exception.b r3 = com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED     // Catch:{ all -> 0x0232 }
            if (r2 != r3) goto L_0x0231
        L_0x0226:
            com.helpshift.common.c.j r2 = r1.f3602b     // Catch:{ all -> 0x0232 }
            com.helpshift.a.a r2 = r2.j     // Catch:{ all -> 0x0232 }
            com.helpshift.a.b.c r3 = r1.c     // Catch:{ all -> 0x0232 }
            com.helpshift.common.exception.a r4 = r0.c     // Catch:{ all -> 0x0232 }
            r2.a(r3, r4)     // Catch:{ all -> 0x0232 }
        L_0x0231:
            throw r0     // Catch:{ all -> 0x0232 }
        L_0x0232:
            r0 = move-exception
            monitor-exit(r16)
            goto L_0x0236
        L_0x0235:
            throw r0
        L_0x0236:
            goto L_0x0235
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.e.e.b():boolean");
    }
}
