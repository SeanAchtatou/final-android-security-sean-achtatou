package com.helpshift.j.e;

import com.helpshift.a.b.c;
import com.helpshift.common.j;
import com.helpshift.common.k;
import com.helpshift.j.a.a.v;
import com.helpshift.j.b.a;
import com.helpshift.j.d.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ConversationHistoryDBLoader */
public class b extends a {
    private c c;

    public b(c cVar, a aVar) {
        super(aVar);
        this.c = cVar;
    }

    public final List<com.helpshift.j.a.b.a> a(String str, String str2, long j) {
        List<com.helpshift.j.a.b.a> list;
        List<com.helpshift.j.a.b.a> b2 = this.f3597a.b(this.c.f3176a.longValue());
        if (b2.isEmpty()) {
            return new ArrayList();
        }
        com.helpshift.j.c.a(b2);
        boolean a2 = k.a(str);
        ArrayList<com.helpshift.j.a.b.a> arrayList = new ArrayList<>();
        if (!a2) {
            b2 = a(str, b2);
            if (!j.a(b2)) {
                com.helpshift.j.a.b.a aVar = b2.get(b2.size() - 1);
                if (aVar.z.equals(str)) {
                    List<v> a3 = a(str2, j, this.f3597a.c(aVar.f3504b.longValue()));
                    if (!j.a(a3)) {
                        aVar.a(a3);
                        arrayList.add(aVar);
                        j -= (long) a3.size();
                    }
                    b2.remove(aVar);
                }
            }
        }
        if (j < 1) {
            return arrayList;
        }
        if (a2) {
            int size = b2.size();
            if (size > 1) {
                int i = size - 1;
                b2 = a(b2.subList(0, i));
                b2.add(b2.get(i));
            }
        } else {
            b2 = a(b2);
        }
        List<com.helpshift.j.a.b.a> b3 = b(b2);
        if (a2) {
            com.helpshift.j.a.b.a d = d(b3);
            list = c(b3);
            if (d != null) {
                list.add(d);
            }
        } else {
            list = c(b3);
        }
        List<com.helpshift.j.a.b.a> a4 = a(j, list);
        ArrayList arrayList2 = new ArrayList();
        HashMap hashMap = new HashMap();
        for (com.helpshift.j.a.b.a next : a4) {
            arrayList2.add(next.f3504b);
            hashMap.put(next.f3504b, next);
        }
        for (v next2 : this.f3597a.b(arrayList2)) {
            if (hashMap.containsKey(next2.q)) {
                ((com.helpshift.j.a.b.a) hashMap.get(next2.q)).j.add(next2);
            }
        }
        int i2 = 0;
        for (int size2 = a4.size() - 1; size2 >= 0; size2--) {
            com.helpshift.j.a.b.a aVar2 = a4.get(size2);
            if (((long) (aVar2.j.size() + i2)) > j) {
                com.helpshift.j.c.b(aVar2.j);
                ArrayList arrayList3 = new ArrayList(aVar2.j);
                aVar2.j.clear();
                aVar2.j.addAll(arrayList3.subList(arrayList3.size() - ((int) (j - ((long) i2))), arrayList3.size()));
            } else {
                i2 += aVar2.j.size();
            }
        }
        arrayList.addAll(0, a4);
        for (com.helpshift.j.a.b.a aVar3 : arrayList) {
            com.helpshift.j.c.b(aVar3.j);
        }
        return arrayList;
    }

    private List<com.helpshift.j.a.b.a> a(long j, List<com.helpshift.j.a.b.a> list) {
        ArrayList arrayList = new ArrayList();
        for (com.helpshift.j.a.b.a aVar : list) {
            arrayList.add(aVar.f3504b);
        }
        Map<Long, Integer> a2 = this.f3597a.a(arrayList);
        int i = 0;
        ArrayList arrayList2 = new ArrayList();
        for (int size = list.size() - 1; size >= 0; size--) {
            com.helpshift.j.a.b.a aVar2 = list.get(size);
            int intValue = a2.get(aVar2.f3504b).intValue();
            arrayList2.add(aVar2);
            i += intValue;
            if (((long) i) >= j) {
                break;
            }
        }
        Collections.reverse(arrayList2);
        return arrayList2;
    }

    private List<com.helpshift.j.a.b.a> a(String str, List<com.helpshift.j.a.b.a> list) {
        if (j.a(list) || k.a(str)) {
            return list;
        }
        long b2 = com.helpshift.common.g.b.b(str);
        ArrayList arrayList = new ArrayList();
        for (com.helpshift.j.a.b.a next : list) {
            long j = next.A;
            if ((j > b2 ? 1 : j < b2 ? (char) 65535 : 0) > 0) {
                break;
            }
            arrayList.add(next);
        }
        return arrayList;
    }

    private static List<com.helpshift.j.a.b.a> a(List<com.helpshift.j.a.b.a> list) {
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        int size = list.size();
        for (int i = 0; i < size; i++) {
            com.helpshift.j.a.b.a aVar = list.get(i);
            if (!aVar.w) {
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }

    private List<com.helpshift.j.a.b.a> b(List<com.helpshift.j.a.b.a> list) {
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList();
        for (com.helpshift.j.a.b.a next : list) {
            if (next.g == e.REJECTED && "preissue".equals(next.h)) {
                arrayList2.add(next.f3504b);
            }
        }
        if (arrayList2.isEmpty()) {
            arrayList.addAll(list);
            return arrayList;
        }
        Map<Long, Integer> a2 = com.helpshift.j.c.a(this.f3597a, arrayList2);
        for (com.helpshift.j.a.b.a next2 : list) {
            Integer num = a2.get(next2.f3504b);
            if (num == null || num.intValue() != 0) {
                arrayList.add(next2);
            }
        }
        return arrayList;
    }

    private static List<com.helpshift.j.a.b.a> c(List<com.helpshift.j.a.b.a> list) {
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        for (com.helpshift.j.a.b.a next : list) {
            if (!com.helpshift.j.c.a(next.g)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private static com.helpshift.j.a.b.a d(List<com.helpshift.j.a.b.a> list) {
        com.helpshift.j.a.b.a aVar = null;
        if (j.a(list)) {
            return null;
        }
        for (com.helpshift.j.a.b.a next : list) {
            if (com.helpshift.j.c.a(next.g)) {
                aVar = next;
            }
        }
        return aVar;
    }
}
