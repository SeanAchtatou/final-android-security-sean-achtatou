package com.helpshift.j.e;

import com.helpshift.a.b.c;
import com.helpshift.common.e.ab;
import com.helpshift.common.j;
import com.helpshift.j.a.b.a;
import java.util.Iterator;
import java.util.List;

/* compiled from: SingleConversationLoader */
public class g extends d {
    private ab c;
    private c d;
    private Long e;
    private boolean f = false;

    public g(ab abVar, c cVar, Long l, e eVar, long j) {
        super(new f(abVar.f(), l), eVar, 100);
        this.c = abVar;
        this.d = cVar;
        this.e = l;
    }

    public final boolean a() {
        if (this.f) {
            return false;
        }
        if (this.f3599a.f3598b) {
            return true;
        }
        List<a> b2 = this.c.f().b(this.d.f3176a.longValue());
        if (!j.a(b2)) {
            long j = 0;
            Iterator<a> it = b2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                a next = it.next();
                if (next.f3504b.equals(this.e)) {
                    j = next.A;
                    break;
                }
            }
            for (a next2 : b2) {
                if (!next2.f3504b.equals(this.e) && j > next2.A) {
                    this.f = true;
                    return false;
                }
            }
        }
        return this.f3600b.a();
    }
}
