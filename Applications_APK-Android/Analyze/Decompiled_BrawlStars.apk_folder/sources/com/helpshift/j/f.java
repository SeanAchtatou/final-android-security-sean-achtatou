package com.helpshift.j;

import android.support.v4.app.NotificationCompat;
import com.helpshift.common.c.a.c;
import com.helpshift.common.c.b.j;
import com.helpshift.common.c.b.o;
import com.helpshift.common.c.b.q;
import com.helpshift.common.c.b.s;
import com.helpshift.common.c.l;
import com.helpshift.common.e.a.i;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.k;
import com.helpshift.j.a.a.al;
import com.helpshift.j.a.a.h;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.b;
import com.helpshift.j.a.x;
import com.helpshift.j.c.a;
import com.helpshift.util.n;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

/* compiled from: CreatePreIssueDM */
public class f extends l {

    /* renamed from: a  reason: collision with root package name */
    public WeakReference<a.b> f3603a;

    /* renamed from: b  reason: collision with root package name */
    private final a f3604b;
    private final b c;
    private final x d;
    private final String f;
    private final String g;

    public f(a aVar, b bVar, x xVar, a.b bVar2, String str, String str2) {
        this.d = xVar;
        this.f3604b = aVar;
        this.c = bVar;
        this.f3603a = new WeakReference<>(bVar2);
        this.f = str;
        this.g = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.a.b.e.a(com.helpshift.a.b.c, boolean):void
     arg types: [com.helpshift.a.b.c, int]
     candidates:
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, com.helpshift.a.b.c):void
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, com.helpshift.a.b.p):void
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, java.lang.String):void
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, boolean):void */
    public final void a() {
        a aVar;
        com.helpshift.j.a.b.a h = this.d.h();
        try {
            if (!b.f(h)) {
                n.a("Helpshift_CrtePreIsue", "Filing preissue with backend.", (Throwable) null, (com.helpshift.p.b.a[]) null);
                aVar = this.f3604b;
                String str = this.f;
                String str2 = this.g;
                HashMap<String, String> a2 = o.a(aVar.e);
                String str3 = aVar.e.d;
                String str4 = aVar.e.c;
                if (!k.a(str3)) {
                    a2.put("name", str3);
                }
                if (!k.a(str4)) {
                    a2.put(NotificationCompat.CATEGORY_EMAIL, str4);
                }
                a2.put("cuid", aVar.f());
                a2.put("cdid", aVar.g());
                a2.put("device_language", Locale.getDefault().toString());
                String e = aVar.f.f.e();
                if (!k.a(e)) {
                    a2.put("developer_set_language", e);
                }
                a2.put("meta", aVar.f.d.a().toString());
                boolean a3 = aVar.j.a("fullPrivacy");
                Object a4 = aVar.f.d().a();
                if (a4 != null) {
                    a2.put("custom_fields", a4.toString());
                }
                if (!k.a(str)) {
                    a2.put("greeting", str);
                }
                if (!k.a(str2)) {
                    a2.put("user_message", str2);
                }
                a2.put("is_prefilled", String.valueOf(h.D));
                com.helpshift.j.a.b.a o = aVar.d.l().o(new j(new com.helpshift.common.c.b.l(new s(new com.helpshift.common.c.b.b(new com.helpshift.common.c.b.k(new q("/preissues/", aVar.f, aVar.d), aVar.d, new c(), "/preissues/", "preissue_default_unique_key")), aVar.d), aVar.d)).a(new i(a2)).f3323b);
                if (h.c == null) {
                    h.c = o.c;
                }
                h.h = o.h;
                h.f = o.f;
                String str5 = o.z;
                if (!k.a(str5)) {
                    h.z = str5;
                }
                h.A = o.A;
                h.i = o.i;
                h.k = o.k;
                h.l = o.l;
                h.g = o.g;
                h.w = a3;
                h.t = aVar.e.f3176a.longValue();
                aVar.g.g(h.f3504b.longValue());
                h.j = o.j;
                Iterator<E> it = h.j.iterator();
                while (it.hasNext()) {
                    v vVar = (v) it.next();
                    vVar.q = h.f3504b;
                    if (vVar instanceof h) {
                        vVar.u = 1;
                    } else if (vVar instanceof al) {
                        vVar.u = 2;
                    }
                }
                h.d = o.d;
                aVar.f.h.a(aVar.e, true);
                aVar.f.h.g();
                aVar.g.c(h);
                if (k.a(str2)) {
                    str2 = "";
                }
                aVar.f.e.a(str2);
                if ("issue".equals(o.h)) {
                    n.a("Helpshift_ConvInboxDM", "Preissue creation skipped, issue created directly.", (Throwable) null, (com.helpshift.p.b.a[]) null);
                    aVar.c.j(o);
                }
                this.f3604b.c.a(h, System.currentTimeMillis());
                if (this.f3603a.get() != null) {
                    this.f3603a.get().a(h.f3504b.longValue());
                }
            }
        } catch (RootAPIException e2) {
            if (e2.c == com.helpshift.common.exception.b.INVALID_AUTH_TOKEN || e2.c == com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED) {
                aVar.f.j.a(aVar.e, e2.c);
            }
            throw e2;
        } catch (RootAPIException e3) {
            n.c("Helpshift_CrtePreIsue", "Error filing a pre-issue", e3);
            if (this.f3603a.get() != null && k.a(h.d)) {
                this.f3603a.get().a(e3);
            }
        }
    }
}
