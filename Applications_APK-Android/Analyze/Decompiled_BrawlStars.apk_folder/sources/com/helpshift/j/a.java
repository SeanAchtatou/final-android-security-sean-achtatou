package com.helpshift.j;

import com.helpshift.a.b.c;
import com.helpshift.common.c.o;
import com.helpshift.common.c.p;
import com.helpshift.common.c.s;
import com.helpshift.common.j;
import com.helpshift.util.n;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/* compiled from: ConversationInboxPoller */
public class a implements Observer {

    /* renamed from: a  reason: collision with root package name */
    public final p f3436a;

    /* renamed from: b  reason: collision with root package name */
    private final c f3437b;
    private final com.helpshift.i.a.a c;
    private final com.helpshift.j.b.a d;
    private s e;
    private o.a f = new b(this);

    public a(c cVar, com.helpshift.i.a.a aVar, p pVar, com.helpshift.j.b.a aVar2) {
        this.f3437b = cVar;
        this.c = aVar;
        this.f3436a = pVar;
        this.d = aVar2;
    }

    public final void a(boolean z) {
        if (!c()) {
            b();
            return;
        }
        List<com.helpshift.j.a.b.a> b2 = this.d.b(this.f3437b.f3176a.longValue());
        s sVar = !j.a(b2) ? c.c(b2) ^ true : false ? s.PASSIVE : s.CONSERVATIVE;
        if (this.e != sVar) {
            b();
            this.e = sVar;
            n.a("Helpshift_ConvPoller", "Listening for conversation updates : " + this.e, (Throwable) null, (com.helpshift.p.b.a[]) null);
            this.f3436a.a(sVar, z ? 3000 : 0, this.f);
        }
    }

    public final void b() {
        n.a("Helpshift_ConvPoller", "Stopped listening for conversation updates : " + this.e, (Throwable) null, (com.helpshift.p.b.a[]) null);
        this.f3436a.a();
        this.e = null;
    }

    public void update(Observable observable, Object obj) {
        b(true);
    }

    private boolean c() {
        return com.helpshift.e.a.f3387a && this.f3437b.j && !this.f3437b.h && !this.c.a("disableInAppConversation");
    }

    public final void a() {
        if (!com.helpshift.e.a.f3387a) {
            b();
        } else if (this.e != s.AGGRESSIVE) {
            b();
            this.e = s.AGGRESSIVE;
            n.a("Helpshift_ConvPoller", "Listening for in-chat conversation updates", (Throwable) null, (com.helpshift.p.b.a[]) null);
            this.f3436a.a(s.AGGRESSIVE, 0, this.f);
        }
    }

    public final void b(boolean z) {
        if (!com.helpshift.e.a.f3387a || !this.f3437b.f) {
            b();
        } else if (this.e == s.AGGRESSIVE) {
            a();
        } else {
            a(z);
        }
    }
}
