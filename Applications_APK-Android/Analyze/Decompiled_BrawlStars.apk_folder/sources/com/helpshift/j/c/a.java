package com.helpshift.j.c;

import com.helpshift.a.b.c;
import com.helpshift.common.b;
import com.helpshift.common.c.b.g;
import com.helpshift.common.c.b.l;
import com.helpshift.common.c.b.o;
import com.helpshift.common.c.b.q;
import com.helpshift.common.c.b.s;
import com.helpshift.common.c.j;
import com.helpshift.common.c.n;
import com.helpshift.common.c.p;
import com.helpshift.common.e.a.i;
import com.helpshift.common.e.ab;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.h;
import com.helpshift.common.k;
import com.helpshift.j.a.r;
import com.helpshift.j.a.x;
import com.helpshift.j.d.d;
import com.helpshift.j.e.e;
import com.helpshift.j.f;
import com.helpshift.util.aa;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: ConversationController */
public class a implements com.helpshift.a.b.b, com.helpshift.common.a {

    /* renamed from: b  reason: collision with root package name */
    public static final Object f3557b = new Object();

    /* renamed from: a  reason: collision with root package name */
    public long f3558a = 0;
    public final com.helpshift.j.a.b c;
    public final ab d;
    public final c e;
    public final j f;
    public final com.helpshift.j.b.a g;
    public final com.helpshift.j.b.b h;
    public final com.helpshift.n.b.a i;
    public final com.helpshift.i.a.a j;
    public final r k;
    public final com.helpshift.j.a l;
    public AtomicReference<h<Integer, Integer>> m = null;
    HashMap<Long, n> n = new HashMap<>();
    public WeakReference<b> o;
    public boolean p;
    public boolean q;
    public boolean r;
    public int s = -1;
    public WeakReference<x> t;
    public e u;
    private Map<String, Integer> v = new ConcurrentHashMap();

    /* compiled from: ConversationController */
    public interface b {
        void a(long j);

        void a(Exception exc);
    }

    public a(ab abVar, j jVar, c cVar) {
        this.d = abVar;
        this.f = jVar;
        this.e = cVar;
        this.h = abVar.e();
        this.g = abVar.f();
        this.i = abVar.n();
        this.j = jVar.f3285b;
        this.l = new com.helpshift.j.a(cVar, this.j, new p(this.f, new b(this)), this.g);
        this.k = new r(jVar, abVar);
        this.c = new com.helpshift.j.a.b(abVar, jVar, cVar);
        this.u = new e(abVar, jVar, cVar, this.c);
    }

    public void b() {
        long longValue = this.e.f3176a.longValue();
        for (com.helpshift.j.a.b.a next : this.g.b(longValue)) {
            next.t = this.e.f3176a.longValue();
            this.c.k(next);
        }
        this.g.d(longValue);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.c.a.a(com.helpshift.j.a.b.a, boolean):void
     arg types: [com.helpshift.j.a.b.a, int]
     candidates:
      com.helpshift.j.c.a.a(com.helpshift.j.a.x, java.lang.String):com.helpshift.j.a.b.a
      com.helpshift.j.c.a.a(com.helpshift.j.c.a, java.lang.Long):com.helpshift.j.a.x
      com.helpshift.j.c.a.a(com.helpshift.j.a.b.a, int):void
      com.helpshift.j.c.a.a(java.lang.String, int):void
      com.helpshift.j.c.a.a(com.helpshift.j.a.b.a, boolean):void */
    public final void a(b.a aVar) {
        for (com.helpshift.j.a.b.a next : this.g.b(this.e.f3176a.longValue())) {
            x a2 = a(next.f3504b);
            if (a2 != null) {
                a(a2.h(), true);
            } else {
                a(next, false);
            }
        }
    }

    private void a(com.helpshift.j.a.b.a aVar, boolean z) {
        aVar.t = this.e.f3176a.longValue();
        this.c.c(aVar, z);
        if (aVar.p == com.helpshift.j.f.a.SUBMITTED_NOT_SYNCED) {
            try {
                this.c.e(aVar);
            } catch (RootAPIException e2) {
                if (e2.c != com.helpshift.common.exception.b.NON_RETRIABLE) {
                    throw e2;
                }
            }
        }
    }

    public final String c() {
        return this.h.f(this.e.f3176a.longValue());
    }

    public final void a(String str, int i2) {
        this.h.a(this.e.f3176a.longValue(), new com.helpshift.j.d.a(str, System.nanoTime(), i2));
    }

    public final void a(String str) {
        this.h.a(this.e.f3176a.longValue(), str);
    }

    public final void b(String str) {
        this.h.b(this.e.f3176a.longValue(), str);
    }

    public final void a(d dVar) {
        this.h.a(this.e.f3176a.longValue(), dVar);
    }

    public final void c(String str) {
        this.h.e(this.e.f3176a.longValue(), str);
    }

    public final String d() {
        return this.h.g(this.e.f3176a.longValue());
    }

    public synchronized void a(x xVar) {
        this.t = new WeakReference<>(xVar);
    }

    public synchronized void e() {
        this.t = null;
    }

    public final com.helpshift.j.a.b.a a(String str, String str2, String str3) {
        com.helpshift.j.a.b.a b2;
        try {
            synchronized (f3557b) {
                b2 = b(str, str2, str3);
            }
            a("", 0);
            if (!this.j.g()) {
                a(str2);
                b(str3);
            }
            this.h.d(this.e.f3176a.longValue(), null);
            if (this.p) {
                com.helpshift.j.a.b bVar = this.c;
                bVar.f3502b.d.f3812b = null;
                bVar.f3502b.d.f3811a.a((HashMap<String, Serializable>) null);
            }
            this.c.j(b2);
            this.f.e.a(str);
            return b2;
        } catch (Exception e2) {
            this.q = false;
            if (this.o.get() != null) {
                this.o.get().a(e2);
            }
            throw e2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.a.b.e.a(com.helpshift.a.b.c, boolean):void
     arg types: [com.helpshift.a.b.c, int]
     candidates:
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, com.helpshift.a.b.c):void
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, com.helpshift.a.b.p):void
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, java.lang.String):void
      com.helpshift.a.b.e.a(com.helpshift.a.b.c, boolean):void */
    private com.helpshift.j.a.b.a b(String str, String str2, String str3) {
        com.helpshift.a.b.e eVar = this.f.h;
        c cVar = this.e;
        HashMap<String, String> a2 = o.a(cVar);
        a2.put("name", cVar.d);
        try {
            new com.helpshift.common.c.b.j(new s(new com.helpshift.common.c.b.b(new q("/profiles/", eVar.h, eVar.g)), eVar.g)).a(new i(a2));
            HashMap<String, String> a3 = o.a(this.e);
            a3.put("user_provided_emails", this.d.p().a((Collection) Collections.singletonList(str3)).toString());
            a3.put("user_provided_name", str2);
            a3.put("body", str);
            a3.put("cuid", f());
            a3.put("cdid", g());
            a3.put("device_language", Locale.getDefault().toString());
            String e2 = this.f.f.e();
            if (!k.a(e2)) {
                a3.put("developer_set_language", e2);
            }
            a3.put("meta", this.f.d.a().toString());
            boolean a4 = this.j.a("fullPrivacy");
            Object a5 = this.f.d().a();
            if (a5 != null) {
                a3.put("custom_fields", a5.toString());
            }
            try {
                com.helpshift.j.a.b.a o2 = this.d.l().o(new com.helpshift.common.c.b.j(new l(new s(new com.helpshift.common.c.b.b(new com.helpshift.common.c.b.k(new q("/issues/", this.f, this.d), this.d, new com.helpshift.common.c.a.c(), "/issues/", "issue_default_unique_key")), this.d), this.d)).a(new i(a3)).f3323b);
                o2.w = a4;
                o2.t = this.e.f3176a.longValue();
                if (this.g.a(o2.c) == null) {
                    this.g.b(o2);
                }
                this.f.h.a(this.e, true);
                this.f.h.g();
                this.l.a(true);
                return o2;
            } catch (RootAPIException e3) {
                if (e3.c == com.helpshift.common.exception.b.INVALID_AUTH_TOKEN || e3.c == com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED) {
                    this.f.j.a(this.e, e3.c);
                }
                throw e3;
            }
        } catch (RootAPIException e4) {
            if (e4.c == com.helpshift.common.exception.b.INVALID_AUTH_TOKEN || e4.c == com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED) {
                eVar.h.j.a(cVar, e4.c);
            }
            throw e4;
        }
    }

    public final void a(x xVar, String str, String str2, b bVar) {
        com.helpshift.j.a.b.a h2 = xVar.h();
        n nVar = this.n.get(h2.f3504b);
        if (nVar != null) {
            com.helpshift.util.n.a("Helpshift_ConvInboxDM", "Pre issue creation already in progress: " + h2.f3504b, (Throwable) null, (com.helpshift.p.b.a[]) null);
            ((f) nVar.f3290a).f3603a = new WeakReference<>(bVar);
            return;
        }
        n nVar2 = new n(new f(this, this.c, xVar, bVar, str, str2));
        this.n.put(h2.f3504b, nVar2);
        this.f.b(new c(this, nVar2, h2));
    }

    public final boolean a(long j2) {
        return this.n.containsKey(Long.valueOf(j2));
    }

    public String f() {
        com.helpshift.v.b z = this.d.z();
        if (z == null) {
            return null;
        }
        return z.a();
    }

    public String g() {
        com.helpshift.v.b z = this.d.z();
        if (z == null) {
            return null;
        }
        return z.b();
    }

    public final com.helpshift.j.d.c h() {
        com.helpshift.j.d.c d2;
        synchronized (f3557b) {
            d2 = d(this.h.e(this.e.f3176a.longValue()));
        }
        return d2;
    }

    private com.helpshift.j.d.c s() {
        com.helpshift.j.d.c d2;
        synchronized (f3557b) {
            d2 = d((String) null);
        }
        return d2;
    }

    public void i() {
        h hVar;
        AtomicReference<h<Integer, Integer>> atomicReference = this.m;
        if (atomicReference != null && (hVar = atomicReference.get()) != null) {
            this.f.c(new d(this, hVar));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void
     arg types: [com.helpshift.j.a.b.a, int]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Collection<? extends com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.List<com.helpshift.j.a.a.v>):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.util.Set<java.lang.Long>):void
      com.helpshift.j.a.b.a(java.util.List<com.helpshift.j.a.a.v>, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, long):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.al):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.a.v):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.d.e):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.f.a):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean):void */
    private void a(Collection<com.helpshift.j.a.b.a> collection) {
        for (com.helpshift.j.a.b.a next : collection) {
            if (next.g == com.helpshift.j.d.e.RESOLUTION_REQUESTED && !next.a() && !this.j.d()) {
                this.c.a(next, true);
            }
        }
    }

    private void a(List<com.helpshift.j.a.b.a> list) {
        String c2 = this.d.t().c("/issues/", "issue_default_unique_key");
        String c3 = this.d.t().c("/preissues/", "preissue_default_unique_key");
        if (c2 != null || c3 != null) {
            for (com.helpshift.j.a.b.a next : list) {
                if (next.v != null) {
                    if (next.v.equals(c2)) {
                        this.d.t().b("/issues/", "issue_default_unique_key");
                    } else if (next.v.equals(c3)) {
                        this.d.t().b("/preissues/", "preissue_default_unique_key");
                    }
                }
            }
        }
    }

    public x j() {
        WeakReference<x> weakReference = this.t;
        if (weakReference == null || weakReference.get() == null) {
            return null;
        }
        return this.t.get();
    }

    private void b(List<com.helpshift.j.a.b.a> list) {
        for (com.helpshift.j.a.b.a next : list) {
            if (d(next)) {
                next.t = this.e.f3176a.longValue();
                a(next, e(next));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.c.a.a(java.lang.Long, java.lang.String, int, java.lang.String, boolean):void
     arg types: [java.lang.Long, java.lang.String, int, java.lang.String, int]
     candidates:
      com.helpshift.j.c.a.a(java.util.List<com.helpshift.j.a.b.a>, java.util.List<com.helpshift.j.a.b.a>, java.util.Set<com.helpshift.j.a.b.a>, java.util.Set<com.helpshift.j.a.b.a>, java.util.Map<java.lang.Long, com.helpshift.j.a.p>):void
      com.helpshift.j.c.a.a(java.lang.Long, java.lang.String, int, java.lang.String, boolean):void */
    private void a(com.helpshift.j.a.b.a aVar, int i2) {
        if (i2 > 0) {
            a(aVar.f3504b, aVar.e, i2, this.d.d().f(), true);
            b(aVar.e, i2);
        }
    }

    private int e(String str) {
        Integer num = this.v.get(str);
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    private void b(String str, int i2) {
        this.v.put(str, Integer.valueOf(i2));
    }

    private void t() {
        this.v.clear();
    }

    private boolean d(com.helpshift.j.a.b.a aVar) {
        if (this.j.a("enableInAppNotification")) {
            return a(aVar);
        }
        return false;
    }

    private int e(com.helpshift.j.a.b.a aVar) {
        int e2 = e(aVar.e);
        int l2 = this.c.l(aVar);
        if (l2 > 0 && l2 != e2) {
            return l2;
        }
        return 0;
    }

    public boolean a(com.helpshift.j.a.b.a aVar) {
        com.helpshift.j.a.b.a aVar2;
        if (aVar == null || this.e.f3176a.longValue() != aVar.t || k.a(aVar.e)) {
            return false;
        }
        x j2 = j();
        if (j2 != null && j2.b()) {
            return false;
        }
        if (j2 == null) {
            aVar2 = k();
        } else {
            aVar2 = j2.h();
        }
        if (aVar2 != null) {
            return aVar.e.equals(aVar2.e);
        }
        return true;
    }

    public void a(Long l2, String str, int i2, String str2, boolean z) {
        if (i2 > 0) {
            this.f.c(new e(this, l2, str, i2, str2, z));
        }
    }

    private void c(List<com.helpshift.j.a.b.a> list) {
        boolean z;
        com.helpshift.j.a.b.a q2 = q();
        String str = null;
        boolean z2 = false;
        if (q2 != null) {
            if (!q2.a()) {
                str = q2.c;
            } else {
                z2 = true;
            }
        }
        x j2 = j();
        for (com.helpshift.j.a.b.a next : list) {
            next.t = this.e.f3176a.longValue();
            if (j2 == null || !j2.a(next)) {
                z = this.c.a(next, this.s, str, z2);
            } else {
                z = j2.a(this.s, str, z2);
            }
            if (z && d(q2)) {
                a(q2, e(q2));
            }
        }
    }

    private void a(Set<com.helpshift.j.a.b.a> set, Set<com.helpshift.j.a.b.a> set2, Map<Long, com.helpshift.j.a.p> map) {
        for (com.helpshift.j.a.b.a aVar : set) {
            aVar.t = this.e.f3176a.longValue();
        }
        for (com.helpshift.j.a.b.a aVar2 : set2) {
            aVar2.t = this.e.f3176a.longValue();
        }
        this.g.a(new ArrayList(set), map);
        this.g.d(new ArrayList(set2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, boolean, com.helpshift.j.a.p):void
     arg types: [com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, int, com.helpshift.j.a.p]
     candidates:
      com.helpshift.j.a.b.a(com.helpshift.j.a.a.v, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, java.util.Map<java.lang.String, com.helpshift.j.a.a.v>, com.helpshift.j.a.p):com.helpshift.j.a.a.v
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, int, java.lang.String, java.lang.String):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, java.lang.String, com.helpshift.j.a.a.j, boolean):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, boolean, java.util.List<com.helpshift.j.a.a.v>, com.helpshift.j.a.p):void
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, int, java.lang.String, boolean):boolean
      com.helpshift.j.a.b.a(com.helpshift.j.a.b.a, com.helpshift.j.a.b.a, boolean, com.helpshift.j.a.p):void */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x018c  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01aa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.util.List<com.helpshift.j.a.b.a> r23, java.util.List<com.helpshift.j.a.b.a> r24, java.util.Set<com.helpshift.j.a.b.a> r25, java.util.Set<com.helpshift.j.a.b.a> r26, java.util.Map<java.lang.Long, com.helpshift.j.a.p> r27) {
        /*
            r22 = this;
            r0 = r22
            r1 = r24
            r2 = r25
            r3 = r26
            r4 = r27
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.HashMap r6 = new java.util.HashMap
            r6.<init>()
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            java.util.Iterator r9 = r23.iterator()
        L_0x0022:
            boolean r10 = r9.hasNext()
            if (r10 == 0) goto L_0x006e
            java.lang.Object r10 = r9.next()
            com.helpshift.j.a.b.a r10 = (com.helpshift.j.a.b.a) r10
            com.helpshift.a.b.c r11 = r0.e
            java.lang.Long r11 = r11.f3176a
            long r11 = r11.longValue()
            r10.t = r11
            java.lang.String r11 = r10.c
            boolean r11 = com.helpshift.common.k.a(r11)
            if (r11 != 0) goto L_0x0046
            java.lang.String r11 = r10.c
            r6.put(r11, r10)
            goto L_0x0022
        L_0x0046:
            java.lang.String r11 = r10.d
            boolean r11 = com.helpshift.common.k.a(r11)
            if (r11 != 0) goto L_0x0054
            java.lang.String r11 = r10.d
            r7.put(r11, r10)
            goto L_0x0022
        L_0x0054:
            boolean r11 = r10.a()
            if (r11 == 0) goto L_0x0022
            com.helpshift.common.e.ab r11 = r0.d
            com.helpshift.common.e.a.e r11 = r11.t()
            java.lang.String r12 = "/preissues/"
            java.lang.String r13 = "preissue_default_unique_key"
            java.lang.String r11 = r11.c(r12, r13)
            if (r11 == 0) goto L_0x0022
            r8.put(r11, r10)
            goto L_0x0022
        L_0x006e:
            r10 = 0
            r11 = 0
        L_0x0070:
            int r12 = r24.size()
            if (r10 >= r12) goto L_0x0285
            java.lang.Object r12 = r1.get(r10)
            com.helpshift.j.a.b.a r12 = (com.helpshift.j.a.b.a) r12
            com.helpshift.a.b.c r14 = r0.e
            java.lang.Long r14 = r14.f3176a
            long r14 = r14.longValue()
            r12.t = r14
            java.lang.String r14 = r12.c
            java.lang.String r15 = r12.d
            java.lang.String r13 = r12.v
            boolean r16 = r6.containsKey(r14)
            r9 = 0
            if (r16 == 0) goto L_0x009a
            java.lang.Object r9 = r6.get(r14)
            com.helpshift.j.a.b.a r9 = (com.helpshift.j.a.b.a) r9
            goto L_0x00de
        L_0x009a:
            boolean r14 = r7.containsKey(r15)
            if (r14 == 0) goto L_0x00a7
            java.lang.Object r9 = r7.get(r15)
            com.helpshift.j.a.b.a r9 = (com.helpshift.j.a.b.a) r9
            goto L_0x00de
        L_0x00a7:
            boolean r14 = com.helpshift.common.k.a(r13)
            if (r14 != 0) goto L_0x00de
            boolean r14 = r8.containsKey(r13)
            if (r14 == 0) goto L_0x00de
            java.lang.Object r11 = r8.get(r13)
            com.helpshift.j.a.b.a r11 = (com.helpshift.j.a.b.a) r11
            if (r11 == 0) goto L_0x00c6
            com.helpshift.j.b.a r13 = r0.g
            java.lang.Long r14 = r11.f3504b
            long r14 = r14.longValue()
            r13.g(r14)
        L_0x00c6:
            java.lang.String r13 = r12.h
            java.lang.String r14 = "issue"
            boolean r13 = r14.equals(r13)
            if (r13 == 0) goto L_0x00dc
            java.lang.String r13 = "Helpshift_ConvInboxDM"
            java.lang.String r14 = "Preissue creation was skipped, issue created directly. Handling idempotent case for it."
            com.helpshift.util.n.a(r13, r14, r9, r9)
            com.helpshift.j.a.b r9 = r0.c
            r9.j(r12)
        L_0x00dc:
            r9 = r11
            r11 = 1
        L_0x00de:
            if (r9 == 0) goto L_0x0222
            com.helpshift.a.b.c r13 = r0.e
            java.lang.Long r13 = r13.f3176a
            long r13 = r13.longValue()
            r9.t = r13
            java.lang.Long r13 = r9.f3504b
            boolean r13 = r4.containsKey(r13)
            if (r13 == 0) goto L_0x00fb
            java.lang.Long r13 = r9.f3504b
            java.lang.Object r13 = r4.get(r13)
            com.helpshift.j.a.p r13 = (com.helpshift.j.a.p) r13
            goto L_0x0100
        L_0x00fb:
            com.helpshift.j.a.p r13 = new com.helpshift.j.a.p
            r13.<init>()
        L_0x0100:
            boolean r14 = r12.a()
            if (r14 == 0) goto L_0x01b3
            java.lang.String r14 = r12.d
            com.helpshift.j.a.x r15 = r22.j()
            if (r15 == 0) goto L_0x0131
            com.helpshift.j.a.b.a r16 = a(r15, r14)
            r17 = r6
            if (r16 == 0) goto L_0x0117
            goto L_0x0119
        L_0x0117:
            r16 = r9
        L_0x0119:
            com.helpshift.j.a.b.a r6 = r15.h()
            java.lang.String r6 = r6.d
            boolean r6 = r14.equals(r6)
            boolean r18 = r15.b()
            r19 = r8
            r21 = r7
            r7 = r6
            r6 = r16
            r16 = r21
            goto L_0x013b
        L_0x0131:
            r17 = r6
            r16 = r7
            r19 = r8
            r6 = r9
            r7 = 0
            r18 = 0
        L_0x013b:
            com.helpshift.j.a.b r8 = r0.c
            r20 = r11
            com.helpshift.j.d.e r11 = r12.g
            com.helpshift.j.d.e r1 = com.helpshift.j.d.e.COMPLETED_ISSUE_CREATED
            if (r11 != r1) goto L_0x014e
            com.helpshift.j.d.e r1 = r12.g
            com.helpshift.j.d.e r11 = r6.g
            if (r1 == r11) goto L_0x014e
            r8.j(r12)
        L_0x014e:
            java.lang.String r1 = r6.d
            boolean r1 = com.helpshift.common.k.a(r1)
            if (r1 == 0) goto L_0x0176
            boolean r1 = r6.a()
            if (r1 == 0) goto L_0x0176
            boolean r1 = com.helpshift.common.k.a(r14)
            if (r1 != 0) goto L_0x0176
            if (r7 == 0) goto L_0x016e
            com.helpshift.j.i.a r1 = r15.g
            if (r1 == 0) goto L_0x0176
            com.helpshift.j.i.a r1 = r15.g
            r1.e()
            goto L_0x0176
        L_0x016e:
            r1 = r10
            long r10 = java.lang.System.currentTimeMillis()
            r6.u = r10
            goto L_0x0177
        L_0x0176:
            r1 = r10
        L_0x0177:
            com.helpshift.j.d.e r8 = r6.g
            boolean r10 = r6.a()
            if (r10 == 0) goto L_0x018c
            if (r7 == 0) goto L_0x0185
            r15.a(r12, r13)
            goto L_0x019b
        L_0x0185:
            com.helpshift.j.a.b r7 = r0.c
            r10 = 0
            r7.b(r6, r12, r10, r13)
            goto L_0x019b
        L_0x018c:
            com.helpshift.common.g.d<com.helpshift.j.a.a.v> r10 = r12.j
            boolean r10 = com.helpshift.common.j.a(r10)
            if (r10 != 0) goto L_0x019b
            com.helpshift.j.a.b r10 = r0.c
            com.helpshift.common.g.d<com.helpshift.j.a.a.v> r11 = r12.j
            r10.a(r6, r7, r11, r13)
        L_0x019b:
            if (r18 != 0) goto L_0x01a2
            com.helpshift.j.a.b r7 = r0.c
            r7.b(r6, r8)
        L_0x01a2:
            com.helpshift.j.a.b r7 = r0.c
            com.helpshift.j.d.e r8 = r6.g
            com.helpshift.j.d.e r10 = com.helpshift.j.d.e.RESOLUTION_ACCEPTED
            if (r8 != r10) goto L_0x01ad
            r7.c(r6)
        L_0x01ad:
            r2.add(r6)
            r14 = 0
            goto L_0x021c
        L_0x01b3:
            r17 = r6
            r16 = r7
            r19 = r8
            r1 = r10
            r20 = r11
            com.helpshift.j.a.x r6 = r22.j()
            if (r6 == 0) goto L_0x01dd
            java.lang.String r7 = r12.c
            com.helpshift.j.a.b.a r7 = b(r6, r7)
            if (r7 == 0) goto L_0x01cb
            goto L_0x01cc
        L_0x01cb:
            r7 = r9
        L_0x01cc:
            com.helpshift.j.a.b.a r8 = r6.h()
            java.lang.String r10 = r12.c
            java.lang.String r8 = r8.c
            boolean r8 = r10.equals(r8)
            boolean r10 = r6.b()
            goto L_0x01e0
        L_0x01dd:
            r7 = r9
            r8 = 0
            r10 = 0
        L_0x01e0:
            com.helpshift.j.d.e r11 = r7.g
            if (r8 == 0) goto L_0x01e9
            r6.b(r12, r13)
            r14 = 0
            goto L_0x01ef
        L_0x01e9:
            com.helpshift.j.a.b r8 = r0.c
            r14 = 0
            r8.a(r7, r12, r14, r13)
        L_0x01ef:
            if (r6 == 0) goto L_0x01f7
            boolean r6 = r6.b()
            if (r6 != 0) goto L_0x0212
        L_0x01f7:
            com.helpshift.j.d.e r6 = r7.g
            com.helpshift.j.d.e r8 = com.helpshift.j.d.e.REJECTED
            if (r6 != r8) goto L_0x0212
            com.helpshift.j.a.b.a r6 = r22.k()
            if (r6 == 0) goto L_0x0212
            java.lang.Long r6 = r6.f3504b
            java.lang.Long r8 = r7.f3504b
            boolean r6 = r6.equals(r8)
            if (r6 == 0) goto L_0x0212
            com.helpshift.j.a.b r6 = r0.c
            r6.c(r7)
        L_0x0212:
            if (r10 != 0) goto L_0x0219
            com.helpshift.j.a.b r6 = r0.c
            r6.b(r7, r11)
        L_0x0219:
            r2.add(r7)
        L_0x021c:
            java.lang.Long r6 = r9.f3504b
            r4.put(r6, r13)
            goto L_0x0277
        L_0x0222:
            r17 = r6
            r16 = r7
            r19 = r8
            r1 = r10
            r20 = r11
            r14 = 0
            boolean r6 = r12.a()
            if (r6 == 0) goto L_0x0242
            long r6 = java.lang.System.currentTimeMillis()
            r12.u = r6
            com.helpshift.j.d.e r6 = r12.g
            com.helpshift.j.d.e r7 = com.helpshift.j.d.e.RESOLUTION_REQUESTED
            if (r6 != r7) goto L_0x0242
            com.helpshift.j.d.e r6 = com.helpshift.j.d.e.RESOLUTION_ACCEPTED
            r12.g = r6
        L_0x0242:
            com.helpshift.j.d.e r6 = r12.g
            if (r6 == 0) goto L_0x0261
            com.helpshift.j.d.e r7 = com.helpshift.j.d.e.RESOLUTION_ACCEPTED
            if (r6 == r7) goto L_0x0256
            com.helpshift.j.d.e r7 = com.helpshift.j.d.e.RESOLUTION_REJECTED
            if (r6 == r7) goto L_0x0256
            com.helpshift.j.d.e r7 = com.helpshift.j.d.e.REJECTED
            if (r6 == r7) goto L_0x0256
            com.helpshift.j.d.e r7 = com.helpshift.j.d.e.ARCHIVED
            if (r6 != r7) goto L_0x0261
        L_0x0256:
            int r7 = r24.size()
            r8 = 1
            int r7 = r7 - r8
            if (r1 == r7) goto L_0x0262
            r12.s = r8
            goto L_0x0262
        L_0x0261:
            r8 = 1
        L_0x0262:
            if (r6 == 0) goto L_0x0274
            boolean r6 = r12.x
            if (r6 == 0) goto L_0x0274
            com.helpshift.j.d.e r6 = r12.g
            com.helpshift.j.d.e r7 = com.helpshift.j.d.e.RESOLUTION_REQUESTED
            if (r6 != r7) goto L_0x0274
            r12.s = r8
            com.helpshift.j.d.e r6 = com.helpshift.j.d.e.RESOLUTION_ACCEPTED
            r12.g = r6
        L_0x0274:
            r5.add(r12)
        L_0x0277:
            int r10 = r1 + 1
            r1 = r24
            r7 = r16
            r6 = r17
            r8 = r19
            r11 = r20
            goto L_0x0070
        L_0x0285:
            int r1 = r5.size()
            r6 = 1
            if (r1 > r6) goto L_0x0292
            r3.addAll(r5)
        L_0x028f:
            r1 = r24
            goto L_0x02e5
        L_0x0292:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>(r5)
            int r7 = r1.size()
            int r7 = r7 - r6
        L_0x029c:
            if (r7 < 0) goto L_0x02e1
            java.lang.Object r6 = r1.get(r7)
            com.helpshift.j.a.b.a r6 = (com.helpshift.j.a.b.a) r6
            boolean r8 = r6.a()
            if (r8 != 0) goto L_0x02de
            int r8 = r7 + -1
        L_0x02ac:
            if (r8 < 0) goto L_0x02de
            java.lang.Object r9 = r1.get(r8)
            com.helpshift.j.a.b.a r9 = (com.helpshift.j.a.b.a) r9
            java.lang.String r10 = r6.d
            boolean r10 = com.helpshift.common.k.a(r10)
            if (r10 != 0) goto L_0x02db
            java.lang.String r10 = r6.d
            java.lang.String r12 = r9.d
            boolean r10 = r10.equals(r12)
            if (r10 == 0) goto L_0x02db
            java.lang.String r10 = r6.c
            java.lang.String r12 = r9.c
            boolean r10 = r10.equals(r12)
            if (r10 == 0) goto L_0x02db
            com.helpshift.common.g.d<com.helpshift.j.a.a.v> r6 = r6.j
            com.helpshift.common.g.d<com.helpshift.j.a.a.v> r8 = r9.j
            r6.addAll(r8)
            r5.remove(r9)
            goto L_0x02de
        L_0x02db:
            int r8 = r8 + -1
            goto L_0x02ac
        L_0x02de:
            int r7 = r7 + -1
            goto L_0x029c
        L_0x02e1:
            r3.addAll(r5)
            goto L_0x028f
        L_0x02e5:
            r0.a(r1)
            r0.a(r2, r3, r4)
            if (r11 == 0) goto L_0x02ff
            com.helpshift.j.a.x r1 = r22.j()
            if (r1 == 0) goto L_0x02ff
            com.helpshift.j.i.a r2 = r1.g
            if (r2 == 0) goto L_0x02ff
            r1.f()
            com.helpshift.j.i.a r1 = r1.g
            r1.f()
        L_0x02ff:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.c.a.a(java.util.List, java.util.List, java.util.Set, java.util.Set, java.util.Map):void");
    }

    public x a(Long l2) {
        WeakReference<x> weakReference = this.t;
        if (!(weakReference == null || weakReference.get() == null)) {
            x xVar = this.t.get();
            if (l2.equals(xVar.h().f3504b)) {
                return xVar;
            }
        }
        return null;
    }

    public final boolean b(long j2) {
        com.helpshift.j.a.b.a a2;
        x a3 = a(Long.valueOf(j2));
        if ((a3 != null && a3.h() != null) || (a2 = this.g.a(Long.valueOf(j2))) == null) {
            return a3 != null && a3.g();
        }
        a2.t = this.e.f3176a.longValue();
        return this.c.g(a2);
    }

    public final com.helpshift.j.a.b.a k() {
        if (!this.j.a("disableInAppConversation")) {
            List<com.helpshift.j.a.b.a> b2 = this.g.b(this.e.f3176a.longValue());
            ArrayList arrayList = new ArrayList();
            for (com.helpshift.j.a.b.a next : b2) {
                next.t = this.e.f3176a.longValue();
                if (this.c.g(next)) {
                    arrayList.add(next);
                }
            }
            if (arrayList.size() > 0) {
                return com.helpshift.j.c.a((Collection<com.helpshift.j.a.b.a>) arrayList);
            }
        }
        return null;
    }

    public final com.helpshift.j.a.b.a l() {
        aa<String, Long> b2 = com.helpshift.common.g.b.b(this.d);
        String str = (String) b2.f4242a;
        long longValue = ((Long) b2.f4243b).longValue();
        com.helpshift.j.a.b.a aVar = new com.helpshift.j.a.b.a("Pre Issue Conversation", com.helpshift.j.d.e.NEW, str, longValue, str, null, null, false, "preissue");
        aVar.t = this.e.f3176a.longValue();
        aVar.u = System.currentTimeMillis();
        this.g.a(aVar);
        String c2 = this.j.c("conversationGreetingMessage");
        if (!k.a(c2)) {
            com.helpshift.j.a.a.h hVar = new com.helpshift.j.a.a.h(null, c2, str, longValue, "");
            hVar.q = aVar.f3504b;
            hVar.u = 1;
            hVar.a(this.f, this.d);
            this.g.a(hVar);
            aVar.j.add(hVar);
        }
        return aVar;
    }

    public final boolean m() {
        return this.h.h(this.e.f3176a.longValue());
    }

    public final void a(boolean z) {
        this.h.a(this.e.f3176a.longValue(), z);
    }

    public final void b(com.helpshift.j.a.b.a aVar) {
        this.h.a(aVar.e, (com.helpshift.j.b.d) null);
        this.f.e.a(0);
    }

    public final void n() {
        for (com.helpshift.j.a.b.a c2 : this.g.b(this.e.f3176a.longValue())) {
            c(c2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.j.c.a.a(java.lang.Long, java.lang.String, int, java.lang.String, boolean):void
     arg types: [java.lang.Long, java.lang.String, int, java.lang.String, int]
     candidates:
      com.helpshift.j.c.a.a(java.util.List<com.helpshift.j.a.b.a>, java.util.List<com.helpshift.j.a.b.a>, java.util.Set<com.helpshift.j.a.b.a>, java.util.Set<com.helpshift.j.a.b.a>, java.util.Map<java.lang.Long, com.helpshift.j.a.p>):void
      com.helpshift.j.c.a.a(java.lang.Long, java.lang.String, int, java.lang.String, boolean):void */
    public final void o() {
        for (com.helpshift.j.a.b.a next : this.g.b(this.e.f3176a.longValue())) {
            com.helpshift.j.b.d a2 = this.h.a(next.e);
            if (a2 != null && a2.f3553a > 0) {
                a(next.f3504b, next.e, a2.f3553a, a2.f3554b, false);
            }
        }
    }

    public final void c(com.helpshift.j.a.b.a aVar) {
        this.f.c(new f(this, aVar));
        t();
    }

    public final int p() {
        com.helpshift.j.a.b.a q2;
        int i2 = 0;
        if (this.r || (q2 = q()) == null) {
            return 0;
        }
        int l2 = this.c.l(q2);
        com.helpshift.j.b.d a2 = this.h.a(q2.e);
        if (a2 != null) {
            i2 = a2.f3553a;
        }
        return Math.max(l2, i2);
    }

    public com.helpshift.j.a.b.a q() {
        x j2 = j();
        if (j2 != null) {
            return j2.h();
        }
        com.helpshift.j.a.b.a k2 = k();
        if (k2 == null) {
            return null;
        }
        k2.t = this.e.f3176a.longValue();
        return k2;
    }

    private com.helpshift.j.a.b.a u() {
        List<com.helpshift.j.a.b.a> b2 = this.g.b(this.e.f3176a.longValue());
        if (b2.isEmpty()) {
            return null;
        }
        List a2 = com.helpshift.util.h.a(b2, com.helpshift.j.h.a.a.a(this.c));
        List a3 = com.helpshift.util.h.a(a2, new com.helpshift.j.h.a.c());
        if (com.helpshift.common.j.a(a2)) {
            return null;
        }
        if (a3.isEmpty()) {
            return com.helpshift.j.c.a((Collection<com.helpshift.j.a.b.a>) a2);
        }
        return com.helpshift.j.c.a((Collection<com.helpshift.j.a.b.a>) a3);
    }

    public final void r() {
        this.f.b(new g(this));
    }

    private static com.helpshift.j.a.b.a a(x xVar, String str) {
        for (com.helpshift.j.a.b.a next : xVar.j()) {
            if (!k.a(str) && str.equals(next.d)) {
                return next;
            }
        }
        return null;
    }

    private static com.helpshift.j.a.b.a b(x xVar, String str) {
        for (com.helpshift.j.a.b.a next : xVar.j()) {
            if (!k.a(str) && str.equals(next.c)) {
                return next;
            }
        }
        return null;
    }

    public final void a() {
        s();
        List<com.helpshift.j.a.b.a> b2 = this.g.b(this.e.f3176a.longValue());
        if (!d(b2)) {
            int i2 = 0;
            boolean a2 = this.u.a();
            while (!d(b2) && a2 && i2 < 3) {
                synchronized (f3557b) {
                    this.u.b();
                }
                b2 = this.g.b(this.e.f3176a.longValue());
                a2 = this.u.a();
                i2++;
            }
        }
    }

    private boolean d(List<com.helpshift.j.a.b.a> list) {
        if (com.helpshift.common.j.a(list)) {
            return false;
        }
        for (com.helpshift.j.a.b.a next : list) {
            next.t = this.e.f3176a.longValue();
            if (!com.helpshift.j.c.a(next.g)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: com.helpshift.j.c.a$a  reason: collision with other inner class name */
    /* compiled from: ConversationController */
    public class C0139a {

        /* renamed from: a  reason: collision with root package name */
        final String f3559a;

        /* renamed from: b  reason: collision with root package name */
        final String f3560b;
        final String c;
        final d d;
        public final com.helpshift.common.c.l e = new n(new i(this));

        public C0139a(String str, String str2, String str3, d dVar) {
            this.f3559a = str;
            this.f3560b = str2;
            this.c = str3;
            this.d = dVar;
        }
    }

    private com.helpshift.j.d.c d(String str) {
        com.helpshift.j.a.b.a aVar;
        x j2;
        com.helpshift.common.c.b.j jVar = new com.helpshift.common.c.b.j(new s(new g(new com.helpshift.common.c.b.b(new q("/conversations/updates/", this.f, this.d))), this.d));
        HashMap<String, String> a2 = o.a(this.e);
        if (!k.a(str)) {
            a2.put("cursor", str);
        }
        x j3 = j();
        if (j3 != null) {
            aVar = j3.h();
            if (!com.helpshift.j.a.b.f(aVar)) {
                aVar = u();
            }
        } else {
            aVar = u();
        }
        if (aVar != null) {
            if (!k.a(aVar.c)) {
                a2.put("issue_id", aVar.c);
            } else if (!k.a(aVar.d)) {
                a2.put("preissue_id", aVar.d);
            }
        }
        a2.put("ucrm", String.valueOf(this.r));
        i iVar = new i(a2);
        try {
            com.helpshift.j.d.c g2 = this.d.l().g(jVar.a(iVar).f3323b);
            this.f.h.a(this.e, g2.f3588a);
            if (!iVar.f3320a.containsKey("cursor") && g2.d != null) {
                this.h.b(this.e.f3176a.longValue(), g2.d.booleanValue());
            }
            List<com.helpshift.j.a.b.a> list = g2.c;
            if (!com.helpshift.common.j.a(list)) {
                List<com.helpshift.j.a.b.a> b2 = this.g.b(this.e.f3176a.longValue());
                HashSet<com.helpshift.j.a.b.a> hashSet = new HashSet<>();
                HashMap hashMap = new HashMap();
                HashSet hashSet2 = new HashSet();
                if (list.size() > 1) {
                    com.helpshift.j.c.a(list);
                }
                a(b2, com.helpshift.util.h.a(list, new com.helpshift.j.h.a.d(this.c)), hashSet, hashSet2, hashMap);
                for (com.helpshift.j.a.b.a aVar2 : hashSet) {
                    this.c.a(aVar2, (com.helpshift.j.a.p) hashMap.get(aVar2.f3504b));
                }
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(hashSet);
                arrayList.addAll(hashSet2);
                c(arrayList);
                a(hashSet);
                if (!this.e.h && this.j.a("enableInAppNotification")) {
                    b(arrayList);
                }
                i();
            }
            this.h.c(this.e.f3176a.longValue(), g2.f3589b);
            return g2;
        } catch (RootAPIException e2) {
            if (e2.c == com.helpshift.common.exception.b.INVALID_AUTH_TOKEN || e2.c == com.helpshift.common.exception.b.AUTH_TOKEN_NOT_PROVIDED) {
                this.f.j.a(this.e, e2.c);
            } else if ((e2.c instanceof com.helpshift.common.exception.b) && (j2 = j()) != null && j2.b()) {
                j2.g.c();
            }
            throw e2;
        }
    }
}
