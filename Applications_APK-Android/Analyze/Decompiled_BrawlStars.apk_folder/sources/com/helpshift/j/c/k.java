package com.helpshift.j.c;

import com.helpshift.a.b.c;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.j.a.b;
import com.helpshift.j.a.b.a;
import com.helpshift.j.a.p;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/* compiled from: ConversationHistoryRemoteDataMerger */
public class k {

    /* renamed from: a  reason: collision with root package name */
    public ab f3576a;

    /* renamed from: b  reason: collision with root package name */
    public c f3577b;
    public b c;
    private j d;

    public k(ab abVar, j jVar, c cVar, b bVar) {
        this.f3576a = abVar;
        this.d = jVar;
        this.f3577b = cVar;
        this.c = bVar;
    }

    public final void a(Set<a> set, Set<a> set2, Map<Long, p> map) {
        for (a aVar : set) {
            aVar.t = this.f3577b.f3176a.longValue();
        }
        for (a aVar2 : set2) {
            aVar2.t = this.f3577b.f3176a.longValue();
        }
        this.f3576a.f().a(new ArrayList(set), map);
        this.f3576a.f().d(new ArrayList(set2));
    }
}
