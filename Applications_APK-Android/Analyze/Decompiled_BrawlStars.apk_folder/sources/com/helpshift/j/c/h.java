package com.helpshift.j.c;

import com.facebook.internal.ServerProtocol;
import com.helpshift.a.b.c;
import com.helpshift.common.c.b.j;
import com.helpshift.common.c.b.o;
import com.helpshift.common.c.b.r;
import com.helpshift.common.c.b.s;
import com.helpshift.common.c.l;
import com.helpshift.common.e.a.i;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.j.a.b.a;
import com.helpshift.j.a.x;
import com.helpshift.j.d.e;
import com.helpshift.util.n;
import java.util.HashMap;

/* compiled from: ConversationController */
class h extends l {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3571a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ c f3572b;
    final /* synthetic */ a c;

    h(a aVar, a aVar2, c cVar) {
        this.c = aVar;
        this.f3571a = aVar2;
        this.f3572b = cVar;
    }

    public final void a() {
        a aVar;
        try {
            n.a("Helpshift_ConvInboxDM", "Reseting preissue on backend: " + this.f3571a.d, (Throwable) null, (com.helpshift.p.b.a[]) null);
            HashMap<String, String> a2 = o.a(this.f3572b);
            a2.put(ServerProtocol.DIALOG_PARAM_STATE, String.valueOf(e.REJECTED.n));
            new j(new s(new r("/preissues/" + this.f3571a.d + "/", this.c.f, this.c.d), this.c.d)).a(new i(a2));
            x a3 = this.c.a(this.f3571a.f3504b);
            if (a3 == null) {
                aVar = this.f3571a;
            } else {
                aVar = a3.h();
            }
            this.c.c.a(aVar, e.REJECTED);
        } catch (RootAPIException e) {
            n.c("Helpshift_ConvInboxDM", "Error resetting preissue : " + this.f3571a.d, e);
            throw e;
        }
    }
}
