package com.helpshift.j.c;

import com.helpshift.a.b.c;
import com.helpshift.a.b.e;
import com.helpshift.a.b.p;
import com.helpshift.common.b;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import com.helpshift.common.k;
import com.helpshift.p.b.a;
import com.helpshift.util.n;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ConversationInboxManagerDM */
public class l {

    /* renamed from: a  reason: collision with root package name */
    private final ab f3578a;

    /* renamed from: b  reason: collision with root package name */
    private final j f3579b;
    private final e c;
    private Map<Long, a> d = new HashMap();

    public l(ab abVar, j jVar, e eVar) {
        this.f3578a = abVar;
        this.f3579b = jVar;
        this.c = eVar;
    }

    private a c(c cVar) {
        return new a(this.f3578a, this.f3579b, cVar);
    }

    public final synchronized a a() {
        a aVar;
        c a2 = this.c.a();
        aVar = this.d.get(a2.f3176a);
        if (aVar == null) {
            aVar = c(a2);
            aVar.f.g.a(b.a.CONVERSATION, aVar);
            if (aVar.e.k == p.COMPLETED) {
                aVar.e.addObserver(aVar.l);
            }
            this.d.clear();
            this.d.put(a2.f3176a, aVar);
        }
        return aVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.helpshift.j.c.a a(com.helpshift.a.b.c r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            if (r3 != 0) goto L_0x0006
            r3 = 0
            monitor-exit(r2)
            return r3
        L_0x0006:
            java.util.Map<java.lang.Long, com.helpshift.j.c.a> r0 = r2.d     // Catch:{ all -> 0x0018 }
            java.lang.Long r1 = r3.f3176a     // Catch:{ all -> 0x0018 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0018 }
            com.helpshift.j.c.a r0 = (com.helpshift.j.c.a) r0     // Catch:{ all -> 0x0018 }
            if (r0 != 0) goto L_0x0016
            com.helpshift.j.c.a r0 = r2.c(r3)     // Catch:{ all -> 0x0018 }
        L_0x0016:
            monitor-exit(r2)
            return r0
        L_0x0018:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.j.c.l.a(com.helpshift.a.b.c):com.helpshift.j.c.a");
    }

    public final synchronized void b(c cVar) {
        a a2 = a(cVar);
        if (a2 != null) {
            a2.b();
            a2.h.i(a2.e.f3176a.longValue());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long
     arg types: [java.lang.String, long]
     candidates:
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Boolean):java.lang.Boolean
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Float):java.lang.Float
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.String):java.lang.String
      com.helpshift.common.e.aa.b(java.lang.String, java.lang.Long):java.lang.Long */
    public final synchronized void b() {
        List<c> i = this.f3579b.h.i();
        if (!com.helpshift.common.j.a(i)) {
            for (c next : i) {
                a a2 = a(next);
                if (a2 != null) {
                    n.a("Helpshift_ConvInboxDM", "Starting preissues reset.", (Throwable) null, (a[]) null);
                    List<com.helpshift.j.a.b.a> b2 = a2.g.b(next.f3176a.longValue());
                    if (b2 != null) {
                        if (b2.size() != 0) {
                            long max = Math.max(a2.j.f3420b.b("preissueResetInterval", (Long) 0L).longValue(), com.helpshift.i.a.a.f3419a.longValue()) * 1000;
                            for (com.helpshift.j.a.b.a next2 : b2) {
                                if (next2.a()) {
                                    if (System.currentTimeMillis() - next2.u >= max) {
                                        if (k.a(next2.d) && k.a(next2.c)) {
                                            n.a("Helpshift_ConvInboxDM", "Deleting offline preissue : " + next2.f3504b, (Throwable) null, (a[]) null);
                                            a2.g.a(next2.f3504b.longValue());
                                            a2.e();
                                        } else if (com.helpshift.j.c.a(next2.g) || next2.g == com.helpshift.j.d.e.UNKNOWN) {
                                            a2.c(next2);
                                            a2.f.b(new h(a2, next2, next));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
