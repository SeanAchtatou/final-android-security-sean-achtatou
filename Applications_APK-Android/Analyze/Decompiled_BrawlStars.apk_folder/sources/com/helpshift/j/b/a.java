package com.helpshift.j.b;

import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.w;
import com.helpshift.j.a.p;
import java.util.List;
import java.util.Map;

/* compiled from: ConversationDAO */
public interface a {
    com.helpshift.j.a.b.a a(Long l);

    com.helpshift.j.a.b.a a(String str);

    List<v> a(long j, w wVar);

    Map<Long, Integer> a(List<Long> list);

    Map<Long, Integer> a(List<Long> list, String[] strArr);

    void a();

    void a(long j);

    void a(v vVar);

    void a(com.helpshift.j.a.b.a aVar);

    void a(Long l, long j);

    void a(List<com.helpshift.j.a.b.a> list, Map<Long, p> map);

    com.helpshift.j.a.b.a b(String str);

    List<com.helpshift.j.a.b.a> b(long j);

    List<v> b(List<Long> list);

    void b(com.helpshift.j.a.b.a aVar);

    v c(String str);

    List<v> c(long j);

    void c(com.helpshift.j.a.b.a aVar);

    void c(List<v> list);

    void d(long j);

    void d(com.helpshift.j.a.b.a aVar);

    void d(List<com.helpshift.j.a.b.a> list);

    String e(long j);

    Long f(long j);

    boolean g(long j);
}
