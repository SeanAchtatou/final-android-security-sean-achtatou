package com.helpshift.j.h.a;

import com.helpshift.common.k;
import com.helpshift.j.a.b;
import com.helpshift.j.a.b.a;
import com.helpshift.util.h;
import com.helpshift.util.v;
import java.util.List;

/* compiled from: ConversationPredicates */
public final class d implements v<a> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3615a;

    public d(b bVar) {
        this.f3615a = bVar;
    }

    public final /* synthetic */ boolean a(Object obj) {
        a aVar = (a) obj;
        b bVar = this.f3615a;
        String e = bVar.d.e(bVar.c.f3176a.longValue());
        boolean z = false;
        if (!k.a(e)) {
            List a2 = h.a(aVar.j, new f(com.helpshift.common.g.b.b(e)));
            int size = aVar.j.size();
            int size2 = a2.size();
            if (size != 0 && size2 == 0) {
                z = true;
            }
            if (size != size2) {
                aVar.a(a2);
            }
        }
        return !z;
    }
}
