package com.helpshift.j;

import com.helpshift.common.j;
import com.helpshift.j.a.a.v;
import com.helpshift.j.a.a.w;
import com.helpshift.j.a.b.a;
import com.helpshift.j.d.e;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/* compiled from: ConversationUtil */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static Comparator<a> f3555a;

    /* renamed from: b  reason: collision with root package name */
    private static Comparator<v> f3556b;

    static /* synthetic */ int a(long j, long j2) {
        if (j > j2) {
            return 1;
        }
        return j < j2 ? -1 : 0;
    }

    private static void a() {
        if (f3555a == null) {
            f3555a = new d();
        }
    }

    public static void a(List<a> list) {
        a();
        Collections.sort(list, f3555a);
    }

    public static a a(Collection<a> collection) {
        a();
        return (a) Collections.max(collection, f3555a);
    }

    public static void b(List<v> list) {
        if (f3556b == null) {
            f3556b = new e();
        }
        Collections.sort(list, f3556b);
    }

    public static Map<Long, Integer> a(com.helpshift.j.b.a aVar, List<Long> list) {
        return aVar.a(list, new String[]{w.USER_TEXT.B, w.ACCEPTED_APP_REVIEW.B, w.SCREENSHOT.B, w.USER_RESP_FOR_TEXT_INPUT.B, w.USER_RESP_FOR_OPTION_INPUT.B});
    }

    public static boolean a(e eVar) {
        return eVar == e.NEW || eVar == e.NEW_FOR_AGENT || eVar == e.AGENT_REPLIED || eVar == e.WAITING_FOR_AGENT || eVar == e.PENDING_REASSIGNMENT || eVar == e.COMPLETED_ISSUE_CREATED;
    }

    public static boolean c(List<a> list) {
        if (j.a(list)) {
            return false;
        }
        for (a aVar : list) {
            if (a(aVar.g)) {
                return true;
            }
        }
        return false;
    }
}
