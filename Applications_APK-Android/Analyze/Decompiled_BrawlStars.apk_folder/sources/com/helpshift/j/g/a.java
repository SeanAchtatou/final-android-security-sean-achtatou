package com.helpshift.j.g;

import com.helpshift.a.a;
import com.helpshift.a.b.h;
import com.helpshift.a.b.l;
import com.helpshift.common.c.j;
import com.helpshift.common.e.ab;
import java.lang.ref.WeakReference;

/* compiled from: UserSetupVM */
public class a implements a.C0132a, h.a {

    /* renamed from: a  reason: collision with root package name */
    final com.helpshift.z.h f3607a;

    /* renamed from: b  reason: collision with root package name */
    final com.helpshift.z.h f3608b;
    final com.helpshift.z.h c;
    com.helpshift.j.a.c.a d;
    private h e;
    private ab f;
    private j g;

    public a(ab abVar, j jVar, h hVar, com.helpshift.j.a.c.a aVar) {
        this.f = abVar;
        this.e = hVar;
        this.d = aVar;
        this.g = jVar;
        com.helpshift.z.h hVar2 = new com.helpshift.z.h();
        hVar2.b(this.e.a() == l.IN_PROGRESS);
        this.f3607a = hVar2;
        this.f3608b = new com.helpshift.z.h();
        this.c = new com.helpshift.z.h();
        this.e.c = new WeakReference<>(this);
        this.g.j.a(this);
    }

    public final void b() {
        if (this.e.a() == l.COMPLETED) {
            i();
        } else {
            this.e.b();
        }
    }

    public final void c() {
        this.d = null;
    }

    private void i() {
        this.g.c(new b(this));
    }

    public final void a() {
        this.g.c(new c(this));
    }

    public final void d() {
        this.g.c(new d(this));
    }

    public final void e() {
        this.g.c(new e(this));
    }

    public final com.helpshift.z.a f() {
        return this.f3607a;
    }

    public final com.helpshift.z.a g() {
        return this.f3608b;
    }

    public final com.helpshift.z.a h() {
        return this.c;
    }

    public final void a(l lVar) {
        if (!this.f.A()) {
            e();
            return;
        }
        int i = f.f3613a[lVar.ordinal()];
        if (i == 1 || i == 2) {
            this.f3608b.b(true);
            this.f3607a.b(true);
        } else if (i == 3) {
            this.f3607a.b(true);
            this.c.b(false);
        } else if (i == 4) {
            i();
        }
    }
}
