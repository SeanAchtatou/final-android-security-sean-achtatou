package com.helpshift.j.f;

/* compiled from: ConversationCSATState */
public enum a {
    NONE(0),
    SUBMITTED_NOT_SYNCED(1),
    SUBMITTED_SYNCED(2);
    
    private final int d;

    private a(int i) {
        this.d = i;
    }

    public static a a(int i) {
        a aVar;
        a[] values = values();
        int length = values.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                aVar = null;
                break;
            }
            aVar = values[i2];
            if (aVar.d == i) {
                break;
            }
            i2++;
        }
        return aVar == null ? NONE : aVar;
    }

    public final int a() {
        return this.d;
    }
}
