package com.microsoft.appcenter.crashes.a.a;

import com.microsoft.appcenter.b.a.g;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: StackFrame */
public class f implements g {

    /* renamed from: a  reason: collision with root package name */
    public String f4638a;

    /* renamed from: b  reason: collision with root package name */
    public String f4639b;
    public Integer c;
    public String d;

    public final void a(JSONObject jSONObject) throws JSONException {
        this.f4638a = jSONObject.optString("className", null);
        this.f4639b = jSONObject.optString("methodName", null);
        this.c = com.microsoft.appcenter.b.a.a.f.a(jSONObject, "lineNumber");
        this.d = jSONObject.optString("fileName", null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        String str = this.f4638a;
        if (str == null ? fVar.f4638a != null : !str.equals(fVar.f4638a)) {
            return false;
        }
        String str2 = this.f4639b;
        if (str2 == null ? fVar.f4639b != null : !str2.equals(fVar.f4639b)) {
            return false;
        }
        Integer num = this.c;
        if (num == null ? fVar.c != null : !num.equals(fVar.c)) {
            return false;
        }
        String str3 = this.d;
        String str4 = fVar.d;
        if (str3 != null) {
            return str3.equals(str4);
        }
        if (str4 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f4638a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f4639b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Integer num = this.c;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        String str3 = this.d;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode3 + i;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        com.microsoft.appcenter.b.a.a.f.a(jSONStringer, "className", this.f4638a);
        com.microsoft.appcenter.b.a.a.f.a(jSONStringer, "methodName", this.f4639b);
        com.microsoft.appcenter.b.a.a.f.a(jSONStringer, "lineNumber", this.c);
        com.microsoft.appcenter.b.a.a.f.a(jSONStringer, "fileName", this.d);
    }
}
