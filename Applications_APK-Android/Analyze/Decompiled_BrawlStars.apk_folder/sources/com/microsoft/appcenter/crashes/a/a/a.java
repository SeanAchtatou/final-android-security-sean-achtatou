package com.microsoft.appcenter.crashes.a.a;

import com.microsoft.appcenter.b.a.a.d;
import com.microsoft.appcenter.b.a.a.f;
import java.util.Date;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: AbstractErrorLog */
public abstract class a extends com.microsoft.appcenter.b.a.a {

    /* renamed from: a  reason: collision with root package name */
    public UUID f4624a;

    /* renamed from: b  reason: collision with root package name */
    public Integer f4625b;
    public String c;
    public Long d;
    public String e;
    public Boolean f;
    public Date g;
    public String h;
    private Integer i;
    private String j;

    public void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4624a = UUID.fromString(jSONObject.getString("id"));
        this.f4625b = f.a(jSONObject, "processId");
        this.c = jSONObject.optString("processName", null);
        this.i = f.a(jSONObject, "parentProcessId");
        this.j = jSONObject.optString("parentProcessName", null);
        this.d = f.b(jSONObject, "errorThreadId");
        this.e = jSONObject.optString("errorThreadName", null);
        this.f = jSONObject.has("fatal") ? Boolean.valueOf(jSONObject.getBoolean("fatal")) : null;
        this.g = d.a(jSONObject.getString("appLaunchTimestamp"));
        this.h = jSONObject.optString("architecture", null);
    }

    public void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        f.a(jSONStringer, "id", this.f4624a);
        f.a(jSONStringer, "processId", this.f4625b);
        f.a(jSONStringer, "processName", this.c);
        f.a(jSONStringer, "parentProcessId", this.i);
        f.a(jSONStringer, "parentProcessName", this.j);
        f.a(jSONStringer, "errorThreadId", this.d);
        f.a(jSONStringer, "errorThreadName", this.e);
        f.a(jSONStringer, "fatal", this.f);
        f.a(jSONStringer, "appLaunchTimestamp", d.a(this.g));
        f.a(jSONStringer, "architecture", this.h);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        a aVar = (a) obj;
        UUID uuid = this.f4624a;
        if (uuid == null ? aVar.f4624a != null : !uuid.equals(aVar.f4624a)) {
            return false;
        }
        Integer num = this.f4625b;
        if (num == null ? aVar.f4625b != null : !num.equals(aVar.f4625b)) {
            return false;
        }
        String str = this.c;
        if (str == null ? aVar.c != null : !str.equals(aVar.c)) {
            return false;
        }
        Integer num2 = this.i;
        if (num2 == null ? aVar.i != null : !num2.equals(aVar.i)) {
            return false;
        }
        String str2 = this.j;
        if (str2 == null ? aVar.j != null : !str2.equals(aVar.j)) {
            return false;
        }
        Long l = this.d;
        if (l == null ? aVar.d != null : !l.equals(aVar.d)) {
            return false;
        }
        String str3 = this.e;
        if (str3 == null ? aVar.e != null : !str3.equals(aVar.e)) {
            return false;
        }
        Boolean bool = this.f;
        if (bool == null ? aVar.f != null : !bool.equals(aVar.f)) {
            return false;
        }
        Date date = this.g;
        if (date == null ? aVar.g != null : !date.equals(aVar.g)) {
            return false;
        }
        String str4 = this.h;
        String str5 = aVar.h;
        if (str4 != null) {
            return str4.equals(str5);
        }
        if (str5 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        UUID uuid = this.f4624a;
        int i2 = 0;
        int hashCode2 = (hashCode + (uuid != null ? uuid.hashCode() : 0)) * 31;
        Integer num = this.f4625b;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        String str = this.c;
        int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
        Integer num2 = this.i;
        int hashCode5 = (hashCode4 + (num2 != null ? num2.hashCode() : 0)) * 31;
        String str2 = this.j;
        int hashCode6 = (hashCode5 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Long l = this.d;
        int hashCode7 = (hashCode6 + (l != null ? l.hashCode() : 0)) * 31;
        String str3 = this.e;
        int hashCode8 = (hashCode7 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Boolean bool = this.f;
        int hashCode9 = (hashCode8 + (bool != null ? bool.hashCode() : 0)) * 31;
        Date date = this.g;
        int hashCode10 = (hashCode9 + (date != null ? date.hashCode() : 0)) * 31;
        String str4 = this.h;
        if (str4 != null) {
            i2 = str4.hashCode();
        }
        return hashCode10 + i2;
    }
}
