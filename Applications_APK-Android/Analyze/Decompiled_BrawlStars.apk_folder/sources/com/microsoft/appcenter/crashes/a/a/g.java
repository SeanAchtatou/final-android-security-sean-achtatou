package com.microsoft.appcenter.crashes.a.a;

import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.crashes.a.a.a.e;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: Thread */
public class g implements com.microsoft.appcenter.b.a.g {

    /* renamed from: a  reason: collision with root package name */
    public long f4640a;

    /* renamed from: b  reason: collision with root package name */
    public String f4641b;
    public List<f> c;

    public final void a(JSONObject jSONObject) throws JSONException {
        this.f4640a = jSONObject.getLong("id");
        this.f4641b = jSONObject.optString("name", null);
        this.c = f.a(jSONObject, "frames", e.a());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        if (this.f4640a != gVar.f4640a) {
            return false;
        }
        String str = this.f4641b;
        if (str == null ? gVar.f4641b != null : !str.equals(gVar.f4641b)) {
            return false;
        }
        List<f> list = this.c;
        List<f> list2 = gVar.c;
        if (list != null) {
            return list.equals(list2);
        }
        if (list2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long j = this.f4640a;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.f4641b;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        List<f> list = this.c;
        if (list != null) {
            i2 = list.hashCode();
        }
        return hashCode + i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.util.List<? extends com.microsoft.appcenter.b.a.g>):void
     arg types: [org.json.JSONStringer, java.lang.String, java.util.List<com.microsoft.appcenter.crashes.a.a.f>]
     candidates:
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONObject, java.lang.String, com.microsoft.appcenter.b.a.a.i):java.util.List<M>
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.lang.Object):void
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.util.List<? extends com.microsoft.appcenter.b.a.g>):void */
    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "id", Long.valueOf(this.f4640a));
        f.a(jSONStringer, "name", this.f4641b);
        f.a(jSONStringer, "frames", (List<? extends com.microsoft.appcenter.b.a.g>) this.c);
    }
}
