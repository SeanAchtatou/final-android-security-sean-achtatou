package com.microsoft.appcenter.crashes.b;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import com.microsoft.appcenter.crashes.a.a.c;
import com.microsoft.appcenter.crashes.a.a.e;
import com.microsoft.appcenter.crashes.a.a.g;
import com.microsoft.appcenter.o;
import com.microsoft.appcenter.utils.DeviceInfoHelper;
import com.microsoft.appcenter.utils.b.f;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/* compiled from: ErrorLogHelper */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static File f4644a;

    /* renamed from: b  reason: collision with root package name */
    private static File f4645b;
    private static File c;

    public static e a(Context context, Thread thread, c cVar, Map<Thread, StackTraceElement[]> map, long j, boolean z) {
        String str;
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        e eVar = new e();
        eVar.f4624a = UUID.randomUUID();
        eVar.k = new Date();
        eVar.m = f.a().b();
        try {
            eVar.n = DeviceInfoHelper.a(context);
        } catch (DeviceInfoHelper.DeviceInfoException e) {
            com.microsoft.appcenter.utils.a.c("AppCenterCrashes", "Could not attach device properties snapshot to error log, will attach at sending time", e);
        }
        eVar.f4625b = Integer.valueOf(Process.myPid());
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (!(activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null)) {
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.pid == Process.myPid()) {
                    eVar.c = next.processName;
                }
            }
        }
        if (eVar.c == null) {
            eVar.c = "";
        }
        if (Build.VERSION.SDK_INT >= 21) {
            str = Build.SUPPORTED_ABIS[0];
        } else {
            str = Build.CPU_ABI;
        }
        eVar.h = str;
        eVar.d = Long.valueOf(thread.getId());
        eVar.e = thread.getName();
        eVar.f = true;
        eVar.g = new Date(j);
        eVar.i = cVar;
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry next2 : map.entrySet()) {
            g gVar = new g();
            gVar.f4640a = ((Thread) next2.getKey()).getId();
            gVar.f4641b = ((Thread) next2.getKey()).getName();
            gVar.c = a((StackTraceElement[]) next2.getValue());
            arrayList.add(gVar);
        }
        eVar.j = arrayList;
        return eVar;
    }

    public static synchronized File a() {
        File file;
        synchronized (a.class) {
            if (f4644a == null) {
                f4644a = new File(o.f4701a, "error");
                com.microsoft.appcenter.utils.d.c.a(f4644a.getAbsolutePath());
            }
            file = f4644a;
        }
        return file;
    }

    public static synchronized File b() {
        File file;
        synchronized (a.class) {
            if (f4645b == null) {
                f4645b = new File(new File(a().getAbsolutePath(), "minidump"), "new");
                com.microsoft.appcenter.utils.d.c.a(f4645b.getPath());
            }
            file = f4645b;
        }
        return file;
    }

    public static synchronized File c() {
        File file;
        synchronized (a.class) {
            if (c == null) {
                c = new File(new File(a().getAbsolutePath(), "minidump"), "pending");
                com.microsoft.appcenter.utils.d.c.a(c.getPath());
            }
            file = c;
        }
        return file;
    }

    public static File[] d() {
        File[] listFiles = b().listFiles();
        return listFiles != null ? listFiles : new File[0];
    }

    public static File e() {
        return com.microsoft.appcenter.utils.d.c.a(a(), new c());
    }

    public static File a(UUID uuid, String str) {
        File[] listFiles = a().listFiles(new d(uuid, str));
        if (listFiles == null || listFiles.length <= 0) {
            return null;
        }
        return listFiles[0];
    }

    public static List<com.microsoft.appcenter.crashes.a.a.f> a(StackTraceElement[] stackTraceElementArr) {
        ArrayList arrayList = new ArrayList();
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            com.microsoft.appcenter.crashes.a.a.f fVar = new com.microsoft.appcenter.crashes.a.a.f();
            fVar.f4638a = stackTraceElement.getClassName();
            fVar.f4639b = stackTraceElement.getMethodName();
            fVar.c = Integer.valueOf(stackTraceElement.getLineNumber());
            fVar.d = stackTraceElement.getFileName();
            arrayList.add(fVar);
        }
        return arrayList;
    }

    public static void a(UUID uuid) {
        File a2 = a(uuid, ".throwable");
        if (a2 != null) {
            com.microsoft.appcenter.utils.a.c("AppCenterCrashes", "Deleting throwable file " + a2.getName());
            a2.delete();
        }
    }

    public static void b(UUID uuid) {
        File a2 = a(uuid, ".json");
        if (a2 != null) {
            com.microsoft.appcenter.utils.a.c("AppCenterCrashes", "Deleting error log file " + a2.getName());
            a2.delete();
        }
    }
}
