package com.microsoft.appcenter.crashes;

import com.microsoft.appcenter.utils.a;
import com.microsoft.appcenter.utils.d.c;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/* compiled from: WrapperSdkExceptionManager */
public class n {

    /* renamed from: a  reason: collision with root package name */
    static final Map<String, byte[]> f4666a = new HashMap();

    public static void a(UUID uuid) {
        if (uuid == null) {
            a.e("AppCenterCrashes", "Failed to delete wrapper exception data: null errorId");
            return;
        }
        File c = c(uuid);
        if (c.exists()) {
            if (b(uuid) == null) {
                a.e("AppCenterCrashes", "Failed to delete wrapper exception data: data not found");
            }
            c.delete();
        }
    }

    private static byte[] b(UUID uuid) {
        if (uuid == null) {
            a.e("AppCenterCrashes", "Failed to load wrapper exception data: null errorId");
            return null;
        }
        byte[] bArr = f4666a.get(uuid.toString());
        if (bArr != null) {
            return bArr;
        }
        File c = c(uuid);
        if (c.exists()) {
            try {
                byte[] bArr2 = (byte[]) c.c(c);
                if (bArr2 != null) {
                    f4666a.put(uuid.toString(), bArr2);
                }
                return bArr2;
            } catch (IOException | ClassNotFoundException e) {
                a.c("AppCenterCrashes", "Cannot access wrapper exception data file " + c.getName(), e);
            }
        }
        return null;
    }

    private static File c(UUID uuid) {
        File a2 = com.microsoft.appcenter.crashes.b.a.a();
        return new File(a2, uuid.toString() + ".dat");
    }
}
