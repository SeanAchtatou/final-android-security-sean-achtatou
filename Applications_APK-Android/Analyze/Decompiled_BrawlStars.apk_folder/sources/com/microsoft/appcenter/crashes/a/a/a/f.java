package com.microsoft.appcenter.crashes.a.a.a;

import com.microsoft.appcenter.b.a.a.i;
import com.microsoft.appcenter.crashes.a.a.g;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ThreadFactory */
public class f implements i<g> {

    /* renamed from: a  reason: collision with root package name */
    private static final f f4631a = new f();

    private f() {
    }

    public static f a() {
        return f4631a;
    }

    public final List<g> a(int i) {
        return new ArrayList(i);
    }

    public final /* synthetic */ com.microsoft.appcenter.b.a.g b() {
        return new g();
    }
}
