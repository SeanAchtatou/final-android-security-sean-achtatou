package com.microsoft.appcenter.crashes.a.a;

import android.util.Base64;
import com.facebook.share.internal.ShareConstants;
import com.microsoft.appcenter.b.a.a;
import com.microsoft.appcenter.b.a.a.f;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: ErrorAttachmentLog */
public class b extends a {

    /* renamed from: a  reason: collision with root package name */
    static final Charset f4632a = Charset.forName("UTF-8");

    /* renamed from: b  reason: collision with root package name */
    public UUID f4633b;
    public UUID c;
    public String d;
    public byte[] e;
    private String f;

    public final String a() {
        return "errorAttachment";
    }

    public static b a(byte[] bArr, String str, String str2) {
        b bVar = new b();
        bVar.e = bArr;
        bVar.f = str;
        bVar.d = str2;
        return bVar;
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4633b = UUID.fromString(jSONObject.getString("id"));
        this.c = UUID.fromString(jSONObject.getString("errorId"));
        this.d = jSONObject.getString("contentType");
        this.f = jSONObject.optString("fileName", null);
        try {
            this.e = Base64.decode(jSONObject.getString(ShareConstants.WEB_DIALOG_PARAM_DATA), 0);
        } catch (IllegalArgumentException e2) {
            throw new JSONException(e2.getMessage());
        }
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        f.a(jSONStringer, "id", this.f4633b);
        f.a(jSONStringer, "errorId", this.c);
        f.a(jSONStringer, "contentType", this.d);
        f.a(jSONStringer, "fileName", this.f);
        f.a(jSONStringer, ShareConstants.WEB_DIALOG_PARAM_DATA, Base64.encodeToString(this.e, 2));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        b bVar = (b) obj;
        UUID uuid = this.f4633b;
        if (uuid == null ? bVar.f4633b != null : !uuid.equals(bVar.f4633b)) {
            return false;
        }
        UUID uuid2 = this.c;
        if (uuid2 == null ? bVar.c != null : !uuid2.equals(bVar.c)) {
            return false;
        }
        String str = this.d;
        if (str == null ? bVar.d != null : !str.equals(bVar.d)) {
            return false;
        }
        String str2 = this.f;
        if (str2 == null ? bVar.f == null : str2.equals(bVar.f)) {
            return Arrays.equals(this.e, bVar.e);
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        UUID uuid = this.f4633b;
        int i = 0;
        int hashCode2 = (hashCode + (uuid != null ? uuid.hashCode() : 0)) * 31;
        UUID uuid2 = this.c;
        int hashCode3 = (hashCode2 + (uuid2 != null ? uuid2.hashCode() : 0)) * 31;
        String str = this.d;
        int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.f;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return ((hashCode4 + i) * 31) + Arrays.hashCode(this.e);
    }
}
