package com.microsoft.appcenter.crashes.a.a.a;

import com.microsoft.appcenter.b.a.a.i;
import com.microsoft.appcenter.b.a.g;
import com.microsoft.appcenter.crashes.a.a.c;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ExceptionFactory */
public class b implements i<c> {

    /* renamed from: a  reason: collision with root package name */
    private static final b f4627a = new b();

    private b() {
    }

    public static b a() {
        return f4627a;
    }

    public final List<c> a(int i) {
        return new ArrayList(i);
    }

    public final /* synthetic */ g b() {
        return new c();
    }
}
