package com.microsoft.appcenter.crashes.a.a;

import com.microsoft.appcenter.b.a.f;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: HandledErrorLog */
public class d extends f {

    /* renamed from: a  reason: collision with root package name */
    private UUID f4636a;

    /* renamed from: b  reason: collision with root package name */
    private c f4637b;

    public final String a() {
        return "handledError";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4636a = UUID.fromString(jSONObject.getString("id"));
        if (jSONObject.has("exception")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("exception");
            c cVar = new c();
            cVar.a(jSONObject2);
            this.f4637b = cVar;
        }
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        jSONStringer.key("id").value(this.f4636a);
        if (this.f4637b != null) {
            jSONStringer.key("exception").object();
            this.f4637b.a(jSONStringer);
            jSONStringer.endObject();
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        d dVar = (d) obj;
        UUID uuid = this.f4636a;
        if (uuid == null ? dVar.f4636a != null : !uuid.equals(dVar.f4636a)) {
            return false;
        }
        c cVar = this.f4637b;
        c cVar2 = dVar.f4637b;
        if (cVar != null) {
            return cVar.equals(cVar2);
        }
        if (cVar2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        UUID uuid = this.f4636a;
        int i = 0;
        int hashCode2 = (hashCode + (uuid != null ? uuid.hashCode() : 0)) * 31;
        c cVar = this.f4637b;
        if (cVar != null) {
            i = cVar.hashCode();
        }
        return hashCode2 + i;
    }
}
