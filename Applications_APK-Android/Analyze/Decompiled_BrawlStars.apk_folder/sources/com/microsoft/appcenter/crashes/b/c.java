package com.microsoft.appcenter.crashes.b;

import java.io.File;
import java.io.FilenameFilter;

/* compiled from: ErrorLogHelper */
final class c implements FilenameFilter {
    c() {
    }

    public final boolean accept(File file, String str) {
        return str.endsWith(".json");
    }
}
