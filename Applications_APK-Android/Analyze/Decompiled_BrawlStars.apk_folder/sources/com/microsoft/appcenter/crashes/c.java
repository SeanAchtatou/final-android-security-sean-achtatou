package com.microsoft.appcenter.crashes;

import com.microsoft.appcenter.utils.a;

/* compiled from: Crashes */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f4648a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Crashes f4649b;

    c(Crashes crashes, boolean z) {
        this.f4649b = crashes;
        this.f4648a = z;
    }

    public void run() {
        if (this.f4649b.e.size() <= 0) {
            return;
        }
        if (this.f4648a) {
            a.b("AppCenterCrashes", "The flag for user confirmation is set to ALWAYS_SEND, will send logs.");
            this.f4649b.b(0);
        } else if (!this.f4649b.o) {
            a.b("AppCenterCrashes", "Automatic processing disabled, will wait for explicit user confirmation.");
        } else {
            l unused = this.f4649b.k;
            a.b("AppCenterCrashes", "CrashesListener.shouldAwaitUserConfirmation returned false, will send logs.");
            this.f4649b.b(0);
        }
    }
}
