package com.microsoft.appcenter.crashes.a.a;

import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: ManagedErrorLog */
public class e extends a {
    public c i;
    public List<g> j;

    public final String a() {
        return "managedError";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        if (jSONObject.has("exception")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("exception");
            c cVar = new c();
            cVar.a(jSONObject2);
            this.i = cVar;
        }
        this.j = f.a(jSONObject, "threads", com.microsoft.appcenter.crashes.a.a.a.f.a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.util.List<? extends com.microsoft.appcenter.b.a.g>):void
     arg types: [org.json.JSONStringer, java.lang.String, java.util.List<com.microsoft.appcenter.crashes.a.a.g>]
     candidates:
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONObject, java.lang.String, com.microsoft.appcenter.b.a.a.i):java.util.List<M>
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.lang.Object):void
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.util.List<? extends com.microsoft.appcenter.b.a.g>):void */
    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        if (this.i != null) {
            jSONStringer.key("exception").object();
            this.i.a(jSONStringer);
            jSONStringer.endObject();
        }
        f.a(jSONStringer, "threads", (List<? extends g>) this.j);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        e eVar = (e) obj;
        c cVar = this.i;
        if (cVar == null ? eVar.i != null : !cVar.equals(eVar.i)) {
            return false;
        }
        List<g> list = this.j;
        List<g> list2 = eVar.j;
        if (list != null) {
            return list.equals(list2);
        }
        if (list2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        c cVar = this.i;
        int i2 = 0;
        int hashCode2 = (hashCode + (cVar != null ? cVar.hashCode() : 0)) * 31;
        List<g> list = this.j;
        if (list != null) {
            i2 = list.hashCode();
        }
        return hashCode2 + i2;
    }
}
