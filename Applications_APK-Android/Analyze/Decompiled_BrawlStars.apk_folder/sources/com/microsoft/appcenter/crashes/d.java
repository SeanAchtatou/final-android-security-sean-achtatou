package com.microsoft.appcenter.crashes;

/* compiled from: Crashes */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f4650a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Crashes f4651b;

    d(Crashes crashes, int i) {
        this.f4651b = crashes;
        this.f4650a = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.utils.d.d.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.microsoft.appcenter.utils.d.d.b(java.lang.String, java.lang.String):void
      com.microsoft.appcenter.utils.d.d.b(java.lang.String, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b9 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            int r0 = r10.f4650a
            r1 = 1
            if (r0 != r1) goto L_0x0028
            com.microsoft.appcenter.crashes.Crashes r0 = r10.f4651b
            java.util.Map r0 = r0.e
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x0013:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x00c7
            java.lang.Object r1 = r0.next()
            java.util.UUID r1 = (java.util.UUID) r1
            r0.remove()
            com.microsoft.appcenter.crashes.Crashes r2 = r10.f4651b
            r2.a(r1)
            goto L_0x0013
        L_0x0028:
            r2 = 2
            if (r0 != r2) goto L_0x0030
            java.lang.String r0 = "com.microsoft.appcenter.crashes.always.send"
            com.microsoft.appcenter.utils.d.d.b(r0, r1)
        L_0x0030:
            com.microsoft.appcenter.crashes.Crashes r0 = r10.f4651b
            java.util.Map r0 = r0.e
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x003e:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x00c7
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r3 = r1.getValue()
            com.microsoft.appcenter.crashes.Crashes$c r3 = (com.microsoft.appcenter.crashes.Crashes.c) r3
            com.microsoft.appcenter.crashes.model.a r4 = r3.f4623b
            java.lang.Throwable r4 = r4.c
            boolean r4 = r4 instanceof com.microsoft.appcenter.crashes.model.NativeException
            r5 = 0
            if (r4 == 0) goto L_0x0082
            com.microsoft.appcenter.crashes.a.a.e r4 = r3.f4622a
            com.microsoft.appcenter.crashes.a.a.c r4 = r4.i
            java.lang.String r6 = r4.g
            r4.g = r5
            if (r6 != 0) goto L_0x0067
            java.lang.String r6 = r4.c
            r4.c = r5
        L_0x0067:
            if (r6 == 0) goto L_0x007b
            java.io.File r4 = new java.io.File
            r4.<init>(r6)
            byte[] r6 = com.microsoft.appcenter.utils.d.c.b(r4)
            java.lang.String r7 = "minidump.dmp"
            java.lang.String r8 = "application/octet-stream"
            com.microsoft.appcenter.crashes.a.a.b r6 = com.microsoft.appcenter.crashes.a.a.b.a(r6, r7, r8)
            goto L_0x0084
        L_0x007b:
            java.lang.String r4 = "AppCenterCrashes"
            java.lang.String r6 = "NativeException found without minidump."
            com.microsoft.appcenter.utils.a.d(r4, r6)
        L_0x0082:
            r4 = r5
            r6 = r4
        L_0x0084:
            com.microsoft.appcenter.crashes.Crashes r7 = r10.f4651b
            com.microsoft.appcenter.a.b r7 = r7.f4529a
            com.microsoft.appcenter.crashes.a.a.e r8 = r3.f4622a
            java.lang.String r9 = "groupErrors"
            r7.a(r8, r9, r2)
            if (r6 == 0) goto L_0x00a3
            com.microsoft.appcenter.crashes.Crashes r7 = r10.f4651b
            com.microsoft.appcenter.crashes.a.a.e r8 = r3.f4622a
            java.util.UUID r8 = r8.f4624a
            java.util.Set r6 = java.util.Collections.singleton(r6)
            com.microsoft.appcenter.crashes.Crashes.a(r7, r8, r6)
            r4.delete()
        L_0x00a3:
            com.microsoft.appcenter.crashes.Crashes r4 = r10.f4651b
            boolean r4 = r4.o
            if (r4 == 0) goto L_0x00b9
            com.microsoft.appcenter.crashes.Crashes r4 = r10.f4651b
            com.microsoft.appcenter.crashes.l unused = r4.k
            com.microsoft.appcenter.crashes.Crashes r4 = r10.f4651b
            com.microsoft.appcenter.crashes.a.a.e r3 = r3.f4622a
            java.util.UUID r3 = r3.f4624a
            com.microsoft.appcenter.crashes.Crashes.a(r4, r3, r5)
        L_0x00b9:
            r0.remove()
            java.lang.Object r1 = r1.getKey()
            java.util.UUID r1 = (java.util.UUID) r1
            com.microsoft.appcenter.crashes.b.a.b(r1)
            goto L_0x003e
        L_0x00c7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.crashes.d.run():void");
    }
}
