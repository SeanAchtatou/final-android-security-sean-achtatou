package com.microsoft.appcenter.crashes.a.a;

import com.facebook.share.internal.ShareConstants;
import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import com.microsoft.appcenter.crashes.a.a.a.b;
import com.microsoft.appcenter.crashes.a.a.a.e;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: Exception */
public class c implements g {

    /* renamed from: a  reason: collision with root package name */
    public String f4634a;

    /* renamed from: b  reason: collision with root package name */
    public String f4635b;
    public String c;
    public List<f> d;
    public List<c> e;
    public String f;
    public String g;

    public final void a(JSONObject jSONObject) throws JSONException {
        this.f4634a = jSONObject.optString("type", null);
        this.f4635b = jSONObject.optString(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, null);
        this.c = jSONObject.optString("stackTrace", null);
        this.d = f.a(jSONObject, "frames", e.a());
        this.e = f.a(jSONObject, "innerExceptions", b.a());
        this.f = jSONObject.optString("wrapperSdkName", null);
        this.g = jSONObject.optString("minidumpFilePath", null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        String str = this.f4634a;
        if (str == null ? cVar.f4634a != null : !str.equals(cVar.f4634a)) {
            return false;
        }
        String str2 = this.f4635b;
        if (str2 == null ? cVar.f4635b != null : !str2.equals(cVar.f4635b)) {
            return false;
        }
        String str3 = this.c;
        if (str3 == null ? cVar.c != null : !str3.equals(cVar.c)) {
            return false;
        }
        List<f> list = this.d;
        if (list == null ? cVar.d != null : !list.equals(cVar.d)) {
            return false;
        }
        List<c> list2 = this.e;
        if (list2 == null ? cVar.e != null : !list2.equals(cVar.e)) {
            return false;
        }
        String str4 = this.f;
        if (str4 == null ? cVar.f != null : !str4.equals(cVar.f)) {
            return false;
        }
        String str5 = this.g;
        String str6 = cVar.g;
        if (str5 != null) {
            return str5.equals(str6);
        }
        if (str6 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f4634a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f4635b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        List<f> list = this.d;
        int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
        List<c> list2 = this.e;
        int hashCode5 = (hashCode4 + (list2 != null ? list2.hashCode() : 0)) * 31;
        String str4 = this.f;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.g;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode6 + i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.util.List<? extends com.microsoft.appcenter.b.a.g>):void
     arg types: [org.json.JSONStringer, java.lang.String, java.util.List<com.microsoft.appcenter.crashes.a.a.f>]
     candidates:
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONObject, java.lang.String, com.microsoft.appcenter.b.a.a.i):java.util.List<M>
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.lang.Object):void
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.util.List<? extends com.microsoft.appcenter.b.a.g>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.util.List<? extends com.microsoft.appcenter.b.a.g>):void
     arg types: [org.json.JSONStringer, java.lang.String, java.util.List<com.microsoft.appcenter.crashes.a.a.c>]
     candidates:
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONObject, java.lang.String, com.microsoft.appcenter.b.a.a.i):java.util.List<M>
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.lang.Object):void
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.util.List<? extends com.microsoft.appcenter.b.a.g>):void */
    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "type", this.f4634a);
        f.a(jSONStringer, ShareConstants.WEB_DIALOG_PARAM_MESSAGE, this.f4635b);
        f.a(jSONStringer, "stackTrace", this.c);
        f.a(jSONStringer, "frames", (List<? extends g>) this.d);
        f.a(jSONStringer, "innerExceptions", (List<? extends g>) this.e);
        f.a(jSONStringer, "wrapperSdkName", this.f);
        f.a(jSONStringer, "minidumpFilePath", this.g);
    }
}
