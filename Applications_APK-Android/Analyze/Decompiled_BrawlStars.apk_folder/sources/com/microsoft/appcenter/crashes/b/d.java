package com.microsoft.appcenter.crashes.b;

import java.io.File;
import java.io.FilenameFilter;
import java.util.UUID;

/* compiled from: ErrorLogHelper */
final class d implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UUID f4646a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f4647b;

    d(UUID uuid, String str) {
        this.f4646a = uuid;
        this.f4647b = str;
    }

    public final boolean accept(File file, String str) {
        return str.startsWith(this.f4646a.toString()) && str.endsWith(this.f4647b);
    }
}
