package com.microsoft.appcenter.crashes.b;

import java.io.File;
import java.io.FilenameFilter;

/* compiled from: ErrorLogHelper */
public final class b implements FilenameFilter {
    public final boolean accept(File file, String str) {
        return str.endsWith(".json");
    }
}
