package com.microsoft.appcenter.crashes.a.a.a;

import com.microsoft.appcenter.b.a.a.i;
import com.microsoft.appcenter.b.a.g;
import com.microsoft.appcenter.crashes.a.a.f;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StackFrameFactory */
public class e implements i<f> {

    /* renamed from: a  reason: collision with root package name */
    private static final e f4630a = new e();

    private e() {
    }

    public static e a() {
        return f4630a;
    }

    public final List<f> a(int i) {
        return new ArrayList(i);
    }

    public final /* synthetic */ g b() {
        return new f();
    }
}
