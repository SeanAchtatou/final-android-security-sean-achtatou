package com.microsoft.appcenter.crashes;

import com.microsoft.appcenter.utils.g;
import java.lang.Thread;

/* compiled from: UncaughtExceptionHandler */
class m implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    boolean f4662a = false;

    /* renamed from: b  reason: collision with root package name */
    Thread.UncaughtExceptionHandler f4663b;

    m() {
    }

    public void uncaughtException(Thread thread, Throwable th) {
        Crashes.getInstance().a(thread, th);
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.f4663b;
        if (uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(thread, th);
        } else {
            g.a(10);
        }
    }
}
