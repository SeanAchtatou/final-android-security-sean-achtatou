package com.microsoft.appcenter.crashes;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.SharedPreferences;
import com.microsoft.appcenter.a.b;
import com.microsoft.appcenter.b.a.a.g;
import com.microsoft.appcenter.b.a.a.h;
import com.microsoft.appcenter.crashes.a.a.a.d;
import com.microsoft.appcenter.crashes.a.a.e;
import com.microsoft.appcenter.crashes.model.NativeException;
import com.microsoft.appcenter.utils.DeviceInfoHelper;
import com.microsoft.appcenter.utils.b.e;
import com.microsoft.appcenter.utils.b.f;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;
import org.json.JSONException;

public class Crashes extends com.microsoft.appcenter.a {

    /* renamed from: b  reason: collision with root package name */
    private static final l f4621b = new b((byte) 0);
    private static Crashes c = null;
    private final Map<String, g> d = new HashMap();
    /* access modifiers changed from: private */
    public final Map<UUID, c> e;
    private final Map<UUID, c> f;
    private h g;
    private Context h;
    private long i;
    private m j;
    /* access modifiers changed from: private */
    public l k;
    private ComponentCallbacks2 l;
    private com.microsoft.appcenter.crashes.model.a m;
    private boolean n;
    /* access modifiers changed from: private */
    public boolean o = true;
    private boolean p;

    interface a {
        void a(com.microsoft.appcenter.crashes.model.a aVar);

        boolean a();
    }

    public final String e() {
        return "groupErrors";
    }

    public final String f() {
        return "AppCenterCrashes";
    }

    public final int h() {
        return 1;
    }

    public final String k() {
        return "Crashes";
    }

    private Crashes() {
        this.d.put("managedError", d.b());
        this.d.put("handledError", com.microsoft.appcenter.crashes.a.a.a.c.b());
        this.d.put("errorAttachment", com.microsoft.appcenter.crashes.a.a.a.a.b());
        this.g = new com.microsoft.appcenter.b.a.a.c();
        this.g.a("managedError", d.b());
        this.g.a("errorAttachment", com.microsoft.appcenter.crashes.a.a.a.a.b());
        this.k = f4621b;
        this.e = new LinkedHashMap();
        this.f = new LinkedHashMap();
    }

    public static synchronized Crashes getInstance() {
        Crashes crashes;
        synchronized (Crashes.class) {
            if (c == null) {
                c = new Crashes();
            }
            crashes = c;
        }
        return crashes;
    }

    public static com.microsoft.appcenter.utils.a.b<String> l() {
        return getInstance().m();
    }

    private synchronized com.microsoft.appcenter.utils.a.b<String> m() {
        com.microsoft.appcenter.utils.a.c cVar;
        cVar = new com.microsoft.appcenter.utils.a.c();
        a(new b(this, cVar), cVar, (Object) null);
        return cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, java.lang.String):java.lang.String
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00bf A[Catch:{ JSONException -> 0x0080 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00cf A[Catch:{ JSONException -> 0x0080 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(android.content.Context r5, com.microsoft.appcenter.a.b r6, java.lang.String r7, java.lang.String r8, boolean r9) {
        /*
            r4 = this;
            monitor-enter(r4)
            r4.h = r5     // Catch:{ all -> 0x00df }
            super.a(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x00df }
            boolean r5 = r4.b()     // Catch:{ all -> 0x00df }
            if (r5 == 0) goto L_0x00dd
            java.io.File r5 = com.microsoft.appcenter.crashes.b.a.a()     // Catch:{ all -> 0x00df }
            com.microsoft.appcenter.crashes.b.b r6 = new com.microsoft.appcenter.crashes.b.b     // Catch:{ all -> 0x00df }
            r6.<init>()     // Catch:{ all -> 0x00df }
            java.io.File[] r5 = r5.listFiles(r6)     // Catch:{ all -> 0x00df }
            r6 = 0
            if (r5 == 0) goto L_0x001d
            goto L_0x001f
        L_0x001d:
            java.io.File[] r5 = new java.io.File[r6]     // Catch:{ all -> 0x00df }
        L_0x001f:
            int r7 = r5.length     // Catch:{ all -> 0x00df }
            r8 = 0
        L_0x0021:
            if (r8 >= r7) goto L_0x009d
            r9 = r5[r8]     // Catch:{ all -> 0x00df }
            java.lang.String r0 = "AppCenterCrashes"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00df }
            r1.<init>()     // Catch:{ all -> 0x00df }
            java.lang.String r2 = "Process pending error file: "
            r1.append(r2)     // Catch:{ all -> 0x00df }
            r1.append(r9)     // Catch:{ all -> 0x00df }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00df }
            com.microsoft.appcenter.utils.a.b(r0, r1)     // Catch:{ all -> 0x00df }
            java.lang.String r0 = com.microsoft.appcenter.utils.d.c.a(r9)     // Catch:{ all -> 0x00df }
            if (r0 == 0) goto L_0x009a
            com.microsoft.appcenter.b.a.a.h r1 = r4.g     // Catch:{ JSONException -> 0x0080 }
            r2 = 0
            com.microsoft.appcenter.b.a.d r0 = r1.a(r0, r2)     // Catch:{ JSONException -> 0x0080 }
            com.microsoft.appcenter.crashes.a.a.e r0 = (com.microsoft.appcenter.crashes.a.a.e) r0     // Catch:{ JSONException -> 0x0080 }
            java.util.UUID r1 = r0.f4624a     // Catch:{ JSONException -> 0x0080 }
            com.microsoft.appcenter.crashes.model.a r0 = r4.a(r0)     // Catch:{ JSONException -> 0x0080 }
            if (r0 != 0) goto L_0x0056
            r4.a(r1)     // Catch:{ JSONException -> 0x0080 }
            goto L_0x009a
        L_0x0056:
            boolean r0 = r4.o     // Catch:{ JSONException -> 0x0080 }
            if (r0 != 0) goto L_0x0074
            java.lang.String r0 = "AppCenterCrashes"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0080 }
            r2.<init>()     // Catch:{ JSONException -> 0x0080 }
            java.lang.String r3 = "CrashesListener.shouldProcess returned true, continue processing log: "
            r2.append(r3)     // Catch:{ JSONException -> 0x0080 }
            java.lang.String r3 = r1.toString()     // Catch:{ JSONException -> 0x0080 }
            r2.append(r3)     // Catch:{ JSONException -> 0x0080 }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x0080 }
            com.microsoft.appcenter.utils.a.b(r0, r2)     // Catch:{ JSONException -> 0x0080 }
        L_0x0074:
            java.util.Map<java.util.UUID, com.microsoft.appcenter.crashes.Crashes$c> r0 = r4.e     // Catch:{ JSONException -> 0x0080 }
            java.util.Map<java.util.UUID, com.microsoft.appcenter.crashes.Crashes$c> r2 = r4.f     // Catch:{ JSONException -> 0x0080 }
            java.lang.Object r2 = r2.get(r1)     // Catch:{ JSONException -> 0x0080 }
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x0080 }
            goto L_0x009a
        L_0x0080:
            r0 = move-exception
            java.lang.String r1 = "AppCenterCrashes"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00df }
            r2.<init>()     // Catch:{ all -> 0x00df }
            java.lang.String r3 = "Error parsing error log. Deleting invalid file: "
            r2.append(r3)     // Catch:{ all -> 0x00df }
            r2.append(r9)     // Catch:{ all -> 0x00df }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00df }
            com.microsoft.appcenter.utils.a.c(r1, r2, r0)     // Catch:{ all -> 0x00df }
            r9.delete()     // Catch:{ all -> 0x00df }
        L_0x009a:
            int r8 = r8 + 1
            goto L_0x0021
        L_0x009d:
            java.lang.String r5 = "com.microsoft.appcenter.crashes.memory"
            android.content.SharedPreferences r7 = com.microsoft.appcenter.utils.d.d.f4749a     // Catch:{ all -> 0x00df }
            r8 = -1
            int r5 = r7.getInt(r5, r8)     // Catch:{ all -> 0x00df }
            r7 = 5
            if (r5 == r7) goto L_0x00b8
            r7 = 10
            if (r5 == r7) goto L_0x00b8
            r7 = 15
            if (r5 == r7) goto L_0x00b8
            r7 = 80
            if (r5 != r7) goto L_0x00b6
            goto L_0x00b8
        L_0x00b6:
            r5 = 0
            goto L_0x00b9
        L_0x00b8:
            r5 = 1
        L_0x00b9:
            r4.p = r5     // Catch:{ all -> 0x00df }
            boolean r5 = r4.p     // Catch:{ all -> 0x00df }
            if (r5 == 0) goto L_0x00c6
            java.lang.String r5 = "AppCenterCrashes"
            java.lang.String r7 = "The application received a low memory warning in the last session."
            com.microsoft.appcenter.utils.a.b(r5, r7)     // Catch:{ all -> 0x00df }
        L_0x00c6:
            java.lang.String r5 = "com.microsoft.appcenter.crashes.memory"
            com.microsoft.appcenter.utils.d.d.a(r5)     // Catch:{ all -> 0x00df }
            boolean r5 = r4.o     // Catch:{ all -> 0x00df }
            if (r5 == 0) goto L_0x00dd
            java.lang.String r5 = "com.microsoft.appcenter.crashes.always.send"
            boolean r5 = com.microsoft.appcenter.utils.d.d.a(r5, r6)     // Catch:{ all -> 0x00df }
            com.microsoft.appcenter.crashes.c r6 = new com.microsoft.appcenter.crashes.c     // Catch:{ all -> 0x00df }
            r6.<init>(r4, r5)     // Catch:{ all -> 0x00df }
            com.microsoft.appcenter.utils.b.a(r6)     // Catch:{ all -> 0x00df }
        L_0x00dd:
            monitor-exit(r4)
            return
        L_0x00df:
            r5 = move-exception
            monitor-exit(r4)
            goto L_0x00e3
        L_0x00e2:
            throw r5
        L_0x00e3:
            goto L_0x00e2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.crashes.Crashes.a(android.content.Context, com.microsoft.appcenter.a.b, java.lang.String, java.lang.String, boolean):void");
    }

    public final Map<String, g> d() {
        return this.d;
    }

    public final b.a j() {
        return new f(this);
    }

    private void n() {
        File[] d2 = com.microsoft.appcenter.crashes.b.a.d();
        int length = d2.length;
        int i2 = 0;
        while (i2 < length) {
            File file = d2[i2];
            com.microsoft.appcenter.utils.a.b("AppCenterCrashes", "Process pending minidump file: " + file);
            long lastModified = file.lastModified();
            File file2 = new File(com.microsoft.appcenter.crashes.b.a.c(), file.getName());
            NativeException nativeException = new NativeException();
            com.microsoft.appcenter.crashes.a.a.c cVar = new com.microsoft.appcenter.crashes.a.a.c();
            cVar.f4634a = "minidump";
            cVar.f = "appcenter.ndk";
            cVar.g = file2.getPath();
            e eVar = new e();
            eVar.i = cVar;
            eVar.k = new Date(lastModified);
            eVar.f = true;
            eVar.f4624a = UUID.randomUUID();
            e.a a2 = com.microsoft.appcenter.utils.b.e.a().a(lastModified);
            if (a2 == null || a2.f4730b > lastModified) {
                eVar.g = eVar.k;
            } else {
                eVar.g = new Date(a2.f4730b);
            }
            eVar.f4625b = 0;
            eVar.c = "";
            eVar.m = f.a().b();
            try {
                eVar.n = DeviceInfoHelper.a(this.h);
                eVar.n.r = "appcenter.ndk";
                a(nativeException, eVar);
                if (file.renameTo(file2)) {
                    i2++;
                } else {
                    throw new IOException("Failed to move file");
                }
            } catch (Exception e2) {
                file.delete();
                a(eVar.f4624a);
                com.microsoft.appcenter.utils.a.c("AppCenterCrashes", "Failed to process new minidump file: " + file, e2);
            }
        }
        File e3 = com.microsoft.appcenter.crashes.b.a.e();
        while (e3 != null && e3.length() == 0) {
            com.microsoft.appcenter.utils.a.d("AppCenterCrashes", "Deleting empty error file: " + e3);
            e3.delete();
            e3 = com.microsoft.appcenter.crashes.b.a.e();
        }
        if (e3 != null) {
            com.microsoft.appcenter.utils.a.b("AppCenterCrashes", "Processing crash report for the last session.");
            String a3 = com.microsoft.appcenter.utils.d.c.a(e3);
            if (a3 == null) {
                com.microsoft.appcenter.utils.a.e("AppCenterCrashes", "Error reading last session error log.");
                return;
            }
            try {
                this.m = a((com.microsoft.appcenter.crashes.a.a.e) this.g.a(a3, (String) null));
                com.microsoft.appcenter.utils.a.b("AppCenterCrashes", "Processed crash report for the last session.");
            } catch (JSONException e4) {
                com.microsoft.appcenter.utils.a.c("AppCenterCrashes", "Error parsing last session error log.", e4);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(UUID uuid) {
        com.microsoft.appcenter.crashes.b.a.b(uuid);
        b(uuid);
    }

    /* access modifiers changed from: private */
    public void b(UUID uuid) {
        this.f.remove(uuid);
        n.a(uuid);
        com.microsoft.appcenter.crashes.b.a.a(uuid);
    }

    /* access modifiers changed from: private */
    public synchronized void b(int i2) {
        a(new d(this, i2));
    }

    private UUID a(Throwable th, com.microsoft.appcenter.crashes.a.a.e eVar) throws JSONException, IOException {
        ObjectOutputStream objectOutputStream;
        File a2 = com.microsoft.appcenter.crashes.b.a.a();
        UUID uuid = eVar.f4624a;
        String uuid2 = uuid.toString();
        com.microsoft.appcenter.utils.a.b("AppCenterCrashes", "Saving uncaught exception.");
        File file = new File(a2, uuid2 + ".json");
        com.microsoft.appcenter.utils.d.c.a(file, this.g.a(eVar));
        com.microsoft.appcenter.utils.a.b("AppCenterCrashes", "Saved JSON content for ingestion into " + file);
        File file2 = new File(a2, uuid2 + ".throwable");
        if (th != null) {
            try {
                objectOutputStream = new ObjectOutputStream(new FileOutputStream(file2));
                objectOutputStream.writeObject(th);
                objectOutputStream.close();
                com.microsoft.appcenter.utils.a.a("AppCenterCrashes", "Saved Throwable as is for client side inspection in " + file2 + " throwable:", th);
            } catch (StackOverflowError e2) {
                com.microsoft.appcenter.utils.a.c("AppCenterCrashes", "Failed to store throwable", e2);
                th = null;
                file2.delete();
            } catch (Throwable th2) {
                objectOutputStream.close();
                throw th2;
            }
        }
        if (th == null) {
            if (file2.createNewFile()) {
                com.microsoft.appcenter.utils.a.b("AppCenterCrashes", "Saved empty Throwable file in " + file2);
            } else {
                throw new IOException(file2.getName());
            }
        }
        return uuid;
    }

    static class b extends a {
        private b() {
        }

        /* synthetic */ b(byte b2) {
            this();
        }
    }

    static class c {

        /* renamed from: a  reason: collision with root package name */
        final com.microsoft.appcenter.crashes.a.a.e f4622a;

        /* renamed from: b  reason: collision with root package name */
        final com.microsoft.appcenter.crashes.model.a f4623b;

        /* synthetic */ c(com.microsoft.appcenter.crashes.a.a.e eVar, com.microsoft.appcenter.crashes.model.a aVar, byte b2) {
            this(eVar, aVar);
        }

        private c(com.microsoft.appcenter.crashes.a.a.e eVar, com.microsoft.appcenter.crashes.model.a aVar) {
            this.f4622a = eVar;
            this.f4623b = aVar;
        }
    }

    public final synchronized void b(boolean z) {
        boolean b2 = b();
        this.i = b2 ? System.currentTimeMillis() : -1;
        if (b2) {
            this.j = new m();
            m mVar = this.j;
            if (!mVar.f4662a) {
                mVar.f4663b = Thread.getDefaultUncaughtExceptionHandler();
            } else {
                mVar.f4663b = null;
            }
            Thread.setDefaultUncaughtExceptionHandler(mVar);
            n();
        } else if (this.j != null) {
            Thread.setDefaultUncaughtExceptionHandler(this.j.f4663b);
            this.j = null;
        }
        if (z) {
            this.l = new e(this);
            this.h.registerComponentCallbacks(this.l);
        } else {
            for (File file : com.microsoft.appcenter.crashes.b.a.a().listFiles()) {
                com.microsoft.appcenter.utils.a.b("AppCenterCrashes", "Deleting file " + file);
                if (!file.delete()) {
                    com.microsoft.appcenter.utils.a.d("AppCenterCrashes", "Failed to delete file " + file);
                }
            }
            com.microsoft.appcenter.utils.a.c("AppCenterCrashes", "Deleted crashes local files");
            this.f.clear();
            this.m = null;
            this.h.unregisterComponentCallbacks(this.l);
            this.l = null;
            com.microsoft.appcenter.utils.d.d.a("com.microsoft.appcenter.crashes.memory");
        }
    }

    /* access modifiers changed from: package-private */
    public final com.microsoft.appcenter.crashes.model.a a(com.microsoft.appcenter.crashes.a.a.e eVar) {
        UUID uuid = eVar.f4624a;
        if (this.f.containsKey(uuid)) {
            com.microsoft.appcenter.crashes.model.a aVar = this.f.get(uuid).f4623b;
            aVar.f = eVar.n;
            return aVar;
        }
        File a2 = com.microsoft.appcenter.crashes.b.a.a(uuid, ".throwable");
        Throwable th = null;
        if (a2 == null) {
            return null;
        }
        if (a2.length() > 0) {
            try {
                th = (Throwable) com.microsoft.appcenter.utils.d.c.c(a2);
            } catch (IOException | ClassNotFoundException | RuntimeException | StackOverflowError e2) {
                com.microsoft.appcenter.utils.a.c("AppCenterCrashes", "Cannot read throwable file " + a2.getName(), e2);
            }
        }
        com.microsoft.appcenter.crashes.model.a aVar2 = new com.microsoft.appcenter.crashes.model.a();
        aVar2.f4664a = eVar.f4624a.toString();
        aVar2.f4665b = eVar.e;
        aVar2.c = th;
        aVar2.d = eVar.g;
        aVar2.e = eVar.k;
        aVar2.f = eVar.n;
        this.f.put(uuid, new c(eVar, aVar2, (byte) 0));
        return aVar2;
    }

    /* access modifiers changed from: package-private */
    public final void a(Thread thread, Throwable th) {
        try {
            LinkedList<Throwable> linkedList = new LinkedList<>();
            for (Throwable th2 = th; th2 != null; th2 = th2.getCause()) {
                linkedList.add(th2);
            }
            if (linkedList.size() > 16) {
                com.microsoft.appcenter.utils.a.d("AppCenterCrashes", "Crash causes truncated from " + linkedList.size() + " to " + 16 + " causes.");
                linkedList.subList(8, linkedList.size() - 8).clear();
            }
            com.microsoft.appcenter.crashes.a.a.c cVar = null;
            com.microsoft.appcenter.crashes.a.a.c cVar2 = null;
            for (Throwable th3 : linkedList) {
                com.microsoft.appcenter.crashes.a.a.c cVar3 = new com.microsoft.appcenter.crashes.a.a.c();
                cVar3.f4634a = th3.getClass().getName();
                cVar3.f4635b = th3.getMessage();
                StackTraceElement[] stackTrace = th3.getStackTrace();
                if (stackTrace.length > 256) {
                    StackTraceElement[] stackTraceElementArr = new StackTraceElement[256];
                    System.arraycopy(stackTrace, 0, stackTraceElementArr, 0, 128);
                    System.arraycopy(stackTrace, stackTrace.length - 128, stackTraceElementArr, 128, 128);
                    th3.setStackTrace(stackTraceElementArr);
                    com.microsoft.appcenter.utils.a.d("AppCenterCrashes", "Crash frames truncated from " + stackTrace.length + " to " + 256 + " frames.");
                    stackTrace = stackTraceElementArr;
                }
                cVar3.d = com.microsoft.appcenter.crashes.b.a.a(stackTrace);
                if (cVar2 == null) {
                    cVar2 = cVar3;
                } else {
                    cVar.e = Collections.singletonList(cVar3);
                }
                cVar = cVar3;
            }
            if (getInstance().a().a().booleanValue()) {
                if (!this.n) {
                    this.n = true;
                    a(th, com.microsoft.appcenter.crashes.b.a.a(this.h, thread, cVar2, Thread.getAllStackTraces(), this.i, true));
                }
            }
        } catch (JSONException e2) {
            com.microsoft.appcenter.utils.a.c("AppCenterCrashes", "Error serializing error log to JSON", e2);
        } catch (IOException e3) {
            com.microsoft.appcenter.utils.a.c("AppCenterCrashes", "Error writing error log to file", e3);
        }
    }

    static /* synthetic */ void a(int i2) {
        SharedPreferences.Editor edit = com.microsoft.appcenter.utils.d.d.f4749a.edit();
        edit.putInt("com.microsoft.appcenter.crashes.memory", i2);
        edit.apply();
        com.microsoft.appcenter.utils.a.b("AppCenterCrashes", String.format("The memory running level (%s) was saved.", Integer.valueOf(i2)));
    }

    static /* synthetic */ void a(Crashes crashes, UUID uuid, Iterable iterable) {
        if (iterable == null) {
            com.microsoft.appcenter.utils.a.b("AppCenterCrashes", "CrashesListener.getErrorAttachments returned null, no additional information will be attached to log: " + uuid.toString());
            return;
        }
        Iterator it = iterable.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            com.microsoft.appcenter.crashes.a.a.b bVar = (com.microsoft.appcenter.crashes.a.a.b) it.next();
            if (bVar != null) {
                bVar.f4633b = UUID.randomUUID();
                bVar.c = uuid;
                if ((bVar.f4633b == null || bVar.c == null || bVar.d == null || bVar.e == null) ? false : true) {
                    i2++;
                    crashes.f4529a.a(bVar, "groupErrors", 1);
                } else {
                    com.microsoft.appcenter.utils.a.e("AppCenterCrashes", "Not all required fields are present in ErrorAttachmentLog.");
                }
            } else {
                com.microsoft.appcenter.utils.a.d("AppCenterCrashes", "Skipping null ErrorAttachmentLog in CrashesListener.getErrorAttachments.");
            }
        }
        if (i2 > 2) {
            com.microsoft.appcenter.utils.a.d("AppCenterCrashes", "A limit of 2 attachments per error report might be enforced by server.");
        }
    }
}
