package com.microsoft.appcenter.crashes;

import com.microsoft.appcenter.b.a.d;
import com.microsoft.appcenter.crashes.Crashes;
import com.microsoft.appcenter.crashes.a.a.e;
import com.microsoft.appcenter.crashes.model.a;
import com.microsoft.appcenter.utils.b;
import java.util.UUID;

/* compiled from: Crashes */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f4654a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Crashes.a f4655b;
    final /* synthetic */ f c;

    g(f fVar, d dVar, Crashes.a aVar) {
        this.c = fVar;
        this.f4654a = dVar;
        this.f4655b = aVar;
    }

    public void run() {
        d dVar = this.f4654a;
        if (dVar instanceof e) {
            e eVar = (e) dVar;
            a a2 = this.c.f4653a.a(eVar);
            UUID uuid = eVar.f4624a;
            if (a2 != null) {
                if (this.f4655b.a()) {
                    this.c.f4653a.b(uuid);
                }
                b.a(new h(this, a2));
                return;
            }
            com.microsoft.appcenter.utils.a.d("AppCenterCrashes", "Cannot find crash report for the error log: " + uuid);
        } else if (!(dVar instanceof com.microsoft.appcenter.crashes.a.a.b) && !(dVar instanceof com.microsoft.appcenter.crashes.a.a.d)) {
            com.microsoft.appcenter.utils.a.d("AppCenterCrashes", "A different type of log comes to crashes: " + this.f4654a.getClass().getName());
        }
    }
}
