package com.microsoft.appcenter.crashes;

import android.content.ComponentCallbacks2;
import android.content.res.Configuration;

/* compiled from: Crashes */
class e implements ComponentCallbacks2 {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Crashes f4652a;

    public void onConfigurationChanged(Configuration configuration) {
    }

    e(Crashes crashes) {
        this.f4652a = crashes;
    }

    public void onTrimMemory(int i) {
        Crashes.a(i);
    }

    public void onLowMemory() {
        Crashes.a(80);
    }
}
