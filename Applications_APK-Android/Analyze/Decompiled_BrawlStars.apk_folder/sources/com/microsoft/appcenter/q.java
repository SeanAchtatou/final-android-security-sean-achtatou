package com.microsoft.appcenter;

import com.microsoft.appcenter.utils.a;
import com.microsoft.appcenter.utils.d;

/* compiled from: ServiceInstrumentationUtils */
class q {
    static boolean a(String str) {
        try {
            String string = d.a().getString("APP_CENTER_DISABLE");
            if (string == null) {
                return false;
            }
            for (String trim : string.split(",")) {
                String trim2 = trim.trim();
                if (trim2.equals("All") || trim2.equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (IllegalStateException | LinkageError unused) {
            a.b("AppCenter", "Cannot read instrumentation variables in a non-test environment.");
            return false;
        }
    }
}
