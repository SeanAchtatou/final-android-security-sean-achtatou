package com.microsoft.appcenter;

import com.microsoft.appcenter.utils.a;

/* compiled from: AbstractAppCenterService */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Runnable f4619a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Runnable f4620b;
    final /* synthetic */ a c;

    c(a aVar, Runnable runnable, Runnable runnable2) {
        this.c = aVar;
        this.f4619a = runnable;
        this.f4620b = runnable2;
    }

    public void run() {
        if (this.c.b()) {
            this.f4619a.run();
            return;
        }
        Runnable runnable = this.f4620b;
        if (runnable != null) {
            runnable.run();
            return;
        }
        a.c("AppCenter", this.c.k() + " service disabled, discarding calls.");
    }
}
