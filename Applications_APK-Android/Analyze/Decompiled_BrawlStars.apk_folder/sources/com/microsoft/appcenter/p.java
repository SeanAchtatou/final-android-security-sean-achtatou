package com.microsoft.appcenter;

import com.microsoft.appcenter.utils.a;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/* compiled from: CustomProperties */
public class p {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f4703a = Pattern.compile("^[a-zA-Z][a-zA-Z0-9]*$");

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, Object> f4704b = new HashMap();

    /* access modifiers changed from: package-private */
    public final synchronized Map<String, Object> a() {
        return new HashMap(this.f4704b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.p.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.microsoft.appcenter.p.a(java.lang.String, java.lang.Number):com.microsoft.appcenter.p
      com.microsoft.appcenter.p.a(java.lang.String, java.lang.String):com.microsoft.appcenter.p
      com.microsoft.appcenter.p.a(java.lang.String, java.lang.Object):void */
    public final synchronized p a(String str, String str2) {
        if (a(str)) {
            boolean z = false;
            if (str2 == null) {
                a.e("AppCenter", "Custom property value cannot be null, did you mean to call clear?");
            } else if (str2.length() > 128) {
                a.e("AppCenter", "Custom property \"" + str + "\" value length cannot be longer than " + 128 + " characters.");
            } else {
                z = true;
            }
            if (z) {
                a(str, (Object) str2);
            }
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.p.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.Number]
     candidates:
      com.microsoft.appcenter.p.a(java.lang.String, java.lang.Number):com.microsoft.appcenter.p
      com.microsoft.appcenter.p.a(java.lang.String, java.lang.String):com.microsoft.appcenter.p
      com.microsoft.appcenter.p.a(java.lang.String, java.lang.Object):void */
    public final synchronized p a(String str, Number number) {
        if (a(str)) {
            boolean z = false;
            if (number == null) {
                a.e("AppCenter", "Custom property value cannot be null, did you mean to call clear?");
            } else {
                double doubleValue = number.doubleValue();
                if (!Double.isInfinite(doubleValue)) {
                    if (!Double.isNaN(doubleValue)) {
                        z = true;
                    }
                }
                a.e("AppCenter", "Custom property \"" + str + "\" value cannot be NaN or infinite.");
            }
            if (z) {
                a(str, (Object) number);
            }
        }
        return this;
    }

    private void a(String str, Object obj) {
        if (this.f4704b.containsKey(str) || this.f4704b.size() < 60) {
            this.f4704b.put(str, obj);
        } else {
            a.e("AppCenter", "Custom properties cannot contain more than 60 items");
        }
    }

    private boolean a(String str) {
        if (str == null || !f4703a.matcher(str).matches()) {
            a.e("AppCenter", "Custom property \"" + str + "\" must match \"" + f4703a + "\"");
            return false;
        } else if (str.length() > 128) {
            a.e("AppCenter", "Custom property \"" + str + "\" length cannot be longer than " + 128 + " characters.");
            return false;
        } else if (!this.f4704b.containsKey(str)) {
            return true;
        } else {
            a.d("AppCenter", "Custom property \"" + str + "\" is already set or cleared and will be overridden.");
            return true;
        }
    }
}
