package com.microsoft.appcenter.utils.d;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteFullException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.text.TextUtils;
import java.io.Closeable;
import java.util.Arrays;
import java.util.Map;

/* compiled from: DatabaseManager */
public class a implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f4744a = {"oid"};

    /* renamed from: b  reason: collision with root package name */
    public final String f4745b;
    private final Context c;
    private final String d;
    /* access modifiers changed from: private */
    public final ContentValues e;
    /* access modifiers changed from: private */
    public final C0158a f;
    private SQLiteOpenHelper g;

    /* renamed from: com.microsoft.appcenter.utils.d.a$a  reason: collision with other inner class name */
    /* compiled from: DatabaseManager */
    public interface C0158a {
        void a(SQLiteDatabase sQLiteDatabase);

        boolean a(SQLiteDatabase sQLiteDatabase, int i);
    }

    public a(Context context, String str, String str2, int i, ContentValues contentValues, C0158a aVar) {
        this(context, str, str2, i, contentValues, aVar, null);
    }

    private a(Context context, String str, String str2, int i, ContentValues contentValues, C0158a aVar, String[] strArr) {
        this.c = context;
        this.d = str;
        this.f4745b = str2;
        this.e = contentValues;
        this.f = aVar;
        this.g = new b(this, context, str, null, i, null);
    }

    private static ContentValues a(Cursor cursor, ContentValues contentValues) {
        ContentValues contentValues2 = new ContentValues();
        for (int i = 0; i < cursor.getColumnCount(); i++) {
            if (!cursor.isNull(i)) {
                String columnName = cursor.getColumnName(i);
                if (columnName.equals("oid")) {
                    contentValues2.put(columnName, Long.valueOf(cursor.getLong(i)));
                } else {
                    Object obj = contentValues.get(columnName);
                    if (obj instanceof byte[]) {
                        contentValues2.put(columnName, cursor.getBlob(i));
                    } else if (obj instanceof Double) {
                        contentValues2.put(columnName, Double.valueOf(cursor.getDouble(i)));
                    } else if (obj instanceof Float) {
                        contentValues2.put(columnName, Float.valueOf(cursor.getFloat(i)));
                    } else if (obj instanceof Integer) {
                        contentValues2.put(columnName, Integer.valueOf(cursor.getInt(i)));
                    } else if (obj instanceof Long) {
                        contentValues2.put(columnName, Long.valueOf(cursor.getLong(i)));
                    } else if (obj instanceof Short) {
                        contentValues2.put(columnName, Short.valueOf(cursor.getShort(i)));
                    } else if (obj instanceof Boolean) {
                        boolean z = true;
                        if (cursor.getInt(i) != 1) {
                            z = false;
                        }
                        contentValues2.put(columnName, Boolean.valueOf(z));
                    } else {
                        contentValues2.put(columnName, cursor.getString(i));
                    }
                }
            }
        }
        return contentValues2;
    }

    public final ContentValues a(Cursor cursor) {
        return a(cursor, this.e);
    }

    public final ContentValues b(Cursor cursor) {
        try {
            if (cursor.moveToNext()) {
                return a(cursor);
            }
            return null;
        } catch (RuntimeException e2) {
            com.microsoft.appcenter.utils.a.c("AppCenter", "Failed to get next cursor value: ", e2);
            return null;
        }
    }

    public final long a(ContentValues contentValues, String str) {
        Long l = null;
        Cursor cursor = null;
        while (l == null) {
            try {
                l = Long.valueOf(b().insertOrThrow(this.f4745b, null, contentValues));
            } catch (SQLiteFullException e2) {
                com.microsoft.appcenter.utils.a.b("AppCenter", "Storage is full, trying to delete the oldest log that has the lowest priority which is lower or equal priority than the new log");
                if (cursor == null) {
                    String asString = contentValues.getAsString(str);
                    SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
                    sQLiteQueryBuilder.appendWhere(str + " <= ?");
                    cursor = a(sQLiteQueryBuilder, f4744a, new String[]{asString}, str + " , " + "oid");
                }
                if (cursor.moveToNext()) {
                    long j = cursor.getLong(0);
                    a(j);
                    com.microsoft.appcenter.utils.a.b("AppCenter", "Deleted log id=" + j);
                } else {
                    throw e2;
                }
            } catch (RuntimeException e3) {
                l = -1L;
                com.microsoft.appcenter.utils.a.c("AppCenter", String.format("Failed to insert values (%s) to database %s.", contentValues.toString(), this.d), e3);
            }
        }
        if (cursor != null) {
            try {
                cursor.close();
            } catch (RuntimeException unused) {
            }
        }
        return l.longValue();
    }

    public final void a(long j) {
        a(this.f4745b, j);
    }

    private void a(String str, long j) {
        a(str, "oid", Long.valueOf(j));
    }

    private int a(String str, String str2, String[] strArr) {
        try {
            return b().delete(str, str2, strArr);
        } catch (RuntimeException e2) {
            com.microsoft.appcenter.utils.a.c("AppCenter", String.format("Failed to delete values that match condition=\"%s\" and values=\"%s\" from database %s.", str2, Arrays.toString(strArr), this.d), e2);
            return 0;
        }
    }

    public final int a(String str, String str2, Object obj) {
        return a(str, str2 + " = ?", new String[]{String.valueOf(obj)});
    }

    public void close() {
        try {
            this.g.close();
        } catch (RuntimeException e2) {
            com.microsoft.appcenter.utils.a.c("AppCenter", "Failed to close the database.", e2);
        }
    }

    public final Cursor a(SQLiteQueryBuilder sQLiteQueryBuilder, String[] strArr, String[] strArr2, String str) throws RuntimeException {
        return a(this.f4745b, sQLiteQueryBuilder, strArr, strArr2, str);
    }

    private SQLiteDatabase b() {
        try {
            return this.g.getWritableDatabase();
        } catch (RuntimeException e2) {
            com.microsoft.appcenter.utils.a.b("AppCenter", "Failed to open database. Trying to delete database (may be corrupted).", e2);
            if (this.c.deleteDatabase(this.d)) {
                com.microsoft.appcenter.utils.a.c("AppCenter", "The database was successfully deleted.");
            } else {
                com.microsoft.appcenter.utils.a.d("AppCenter", "Failed to delete database.");
            }
            return this.g.getWritableDatabase();
        }
    }

    public final boolean b(long j) {
        try {
            SQLiteDatabase b2 = b();
            long maximumSize = b2.setMaximumSize(j);
            long pageSize = b2.getPageSize();
            long j2 = j / pageSize;
            if (j % pageSize != 0) {
                j2++;
            }
            if (maximumSize != j2 * pageSize) {
                com.microsoft.appcenter.utils.a.e("AppCenter", "Could not change maximum database size to " + j + " bytes, current maximum size is " + maximumSize + " bytes.");
                return false;
            } else if (j == maximumSize) {
                com.microsoft.appcenter.utils.a.c("AppCenter", "Changed maximum database size to " + maximumSize + " bytes.");
                return true;
            } else {
                com.microsoft.appcenter.utils.a.c("AppCenter", "Changed maximum database size to " + maximumSize + " bytes (next multiple of page size).");
                return true;
            }
        } catch (RuntimeException e2) {
            com.microsoft.appcenter.utils.a.c("AppCenter", "Could not change maximum database size.", e2);
            return false;
        }
    }

    public final long a() {
        try {
            return b().getMaximumSize();
        } catch (RuntimeException e2) {
            com.microsoft.appcenter.utils.a.c("AppCenter", "Could not get maximum database size.", e2);
            return -1;
        }
    }

    private Cursor a(String str, SQLiteQueryBuilder sQLiteQueryBuilder, String[] strArr, String[] strArr2, String str2) throws RuntimeException {
        if (sQLiteQueryBuilder == null) {
            sQLiteQueryBuilder = new SQLiteQueryBuilder();
        }
        SQLiteQueryBuilder sQLiteQueryBuilder2 = sQLiteQueryBuilder;
        sQLiteQueryBuilder2.setTables(str);
        return sQLiteQueryBuilder2.query(b(), strArr, null, strArr2, null, null, str2);
    }

    static /* synthetic */ void a(a aVar, SQLiteDatabase sQLiteDatabase, String str, ContentValues contentValues, String[] strArr) {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS `");
        sb.append(str);
        sb.append("` (oid INTEGER PRIMARY KEY AUTOINCREMENT");
        for (Map.Entry next : contentValues.valueSet()) {
            sb.append(", `");
            sb.append((String) next.getKey());
            sb.append("` ");
            Object value = next.getValue();
            if ((value instanceof Double) || (value instanceof Float)) {
                sb.append("REAL");
            } else if ((value instanceof Number) || (value instanceof Boolean)) {
                sb.append("INTEGER");
            } else if (value instanceof byte[]) {
                sb.append("BLOB");
            } else {
                sb.append("TEXT");
            }
        }
        if (strArr != null && strArr.length > 0) {
            sb.append(", UNIQUE(`");
            sb.append(TextUtils.join("`, `", strArr));
            sb.append("`)");
        }
        sb.append(");");
        sQLiteDatabase.execSQL(sb.toString());
    }
}
