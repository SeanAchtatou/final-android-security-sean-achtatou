package com.microsoft.appcenter.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.Point;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import com.facebook.places.model.PlaceFields;
import com.microsoft.appcenter.b.a.c;
import com.microsoft.appcenter.b.a.i;
import java.util.Locale;
import java.util.TimeZone;

public class DeviceInfoHelper {

    /* renamed from: a  reason: collision with root package name */
    private static i f4712a;

    public static synchronized c a(Context context) throws DeviceInfoException {
        c cVar;
        int i;
        int i2;
        synchronized (DeviceInfoHelper.class) {
            cVar = new c();
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                cVar.l = packageInfo.versionName;
                cVar.o = String.valueOf(packageInfo.versionCode);
                cVar.p = context.getPackageName();
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
                String networkCountryIso = telephonyManager.getNetworkCountryIso();
                if (!TextUtils.isEmpty(networkCountryIso)) {
                    cVar.n = networkCountryIso;
                }
                String networkOperatorName = telephonyManager.getNetworkOperatorName();
                if (!TextUtils.isEmpty(networkOperatorName)) {
                    cVar.m = networkOperatorName;
                }
            } catch (Exception e) {
                a.c("AppCenter", "Cannot retrieve package info", e);
                throw new DeviceInfoException("Cannot retrieve package info", e);
            } catch (Exception e2) {
                a.c("AppCenter", "Cannot retrieve carrier info", e2);
            }
            cVar.i = Locale.getDefault().toString();
            cVar.c = Build.MODEL;
            cVar.d = Build.MANUFACTURER;
            cVar.h = Integer.valueOf(Build.VERSION.SDK_INT);
            cVar.e = "Android";
            cVar.f = Build.VERSION.RELEASE;
            cVar.g = Build.ID;
            try {
                Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                int rotation = defaultDisplay.getRotation();
                if (rotation == 1 || rotation == 3) {
                    int i3 = point.x;
                    int i4 = point.y;
                    i2 = i3;
                    i = i4;
                } else {
                    i = point.x;
                    i2 = point.y;
                }
                cVar.k = i + "x" + i2;
            } catch (Exception e3) {
                a.c("AppCenter", "Cannot retrieve screen size", e3);
            }
            cVar.f4605a = "appcenter.android";
            cVar.f4606b = "2.3.0";
            cVar.j = Integer.valueOf((TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 60) / 1000);
            if (f4712a != null) {
                cVar.q = f4712a.q;
                cVar.r = f4712a.r;
                cVar.s = f4712a.s;
                cVar.t = f4712a.t;
                cVar.u = f4712a.u;
                cVar.v = f4712a.v;
            }
        }
        return cVar;
    }

    public static class DeviceInfoException extends Exception {
        public DeviceInfoException(String str, Throwable th) {
            super(str, th);
        }
    }
}
