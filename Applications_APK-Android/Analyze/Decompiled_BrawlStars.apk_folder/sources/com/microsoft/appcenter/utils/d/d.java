package com.microsoft.appcenter.utils.d;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: SharedPreferencesManager */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public static SharedPreferences f4749a;

    /* renamed from: b  reason: collision with root package name */
    private static Context f4750b;

    public static synchronized void a(Context context) {
        synchronized (d.class) {
            if (f4750b == null) {
                f4750b = context;
                f4749a = f4750b.getSharedPreferences("AppCenter", 0);
            }
        }
    }

    public static boolean a(String str, boolean z) {
        return f4749a.getBoolean(str, z);
    }

    public static void b(String str, boolean z) {
        SharedPreferences.Editor edit = f4749a.edit();
        edit.putBoolean(str, z);
        edit.apply();
    }

    public static String a(String str, String str2) {
        return f4749a.getString(str, str2);
    }

    public static void b(String str, String str2) {
        SharedPreferences.Editor edit = f4749a.edit();
        edit.putString(str, str2);
        edit.apply();
    }

    public static void a(String str) {
        SharedPreferences.Editor edit = f4749a.edit();
        edit.remove(str);
        edit.apply();
    }
}
