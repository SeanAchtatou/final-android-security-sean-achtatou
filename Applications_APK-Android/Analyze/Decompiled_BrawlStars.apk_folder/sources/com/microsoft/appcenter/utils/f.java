package com.microsoft.appcenter.utils;

import android.net.ConnectivityManager;
import android.net.Network;

/* compiled from: NetworkStateHelper */
class f extends ConnectivityManager.NetworkCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f4754a;

    f(e eVar) {
        this.f4754a = eVar;
    }

    public void onAvailable(Network network) {
        e.a(this.f4754a, network);
    }

    public void onLost(Network network) {
        e.b(this.f4754a, network);
    }
}
