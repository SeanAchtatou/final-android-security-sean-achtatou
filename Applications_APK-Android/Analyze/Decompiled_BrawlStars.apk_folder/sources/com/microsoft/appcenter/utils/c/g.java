package com.microsoft.appcenter.utils.c;

import com.microsoft.appcenter.utils.c.e;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.KeyGenerator;

/* compiled from: CryptoUtils */
class g implements e.C0157e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ KeyGenerator f4739a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ f f4740b;

    g(f fVar, KeyGenerator keyGenerator) {
        this.f4740b = fVar;
        this.f4739a = keyGenerator;
    }

    public final void a(AlgorithmParameterSpec algorithmParameterSpec) throws Exception {
        this.f4739a.init(algorithmParameterSpec);
    }

    public final void a() {
        this.f4739a.generateKey();
    }
}
