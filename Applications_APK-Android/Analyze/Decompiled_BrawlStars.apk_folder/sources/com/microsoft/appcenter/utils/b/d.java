package com.microsoft.appcenter.utils.b;

import java.util.Date;

/* compiled from: AuthTokenInfo */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public final String f4725a;

    /* renamed from: b  reason: collision with root package name */
    public final Date f4726b;
    public final Date c;

    public d() {
        this(null, null, null);
    }

    public d(String str, Date date, Date date2) {
        this.f4725a = str;
        this.f4726b = date;
        this.c = date2;
    }
}
