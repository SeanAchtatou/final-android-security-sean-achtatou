package com.microsoft.appcenter.utils.d;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: DatabaseManager */
class b extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String[] f4746a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f4747b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(a aVar, Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i, String[] strArr) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, i);
        this.f4747b = aVar;
        this.f4746a = strArr;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        a aVar = this.f4747b;
        a.a(aVar, sQLiteDatabase, aVar.f4745b, this.f4747b.e, this.f4746a);
        this.f4747b.f.a(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        this.f4747b.f.a(sQLiteDatabase, i);
    }
}
