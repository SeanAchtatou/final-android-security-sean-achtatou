package com.microsoft.appcenter.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;
import java.io.Closeable;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: NetworkStateHelper */
public class e implements Closeable {
    private static e c;

    /* renamed from: a  reason: collision with root package name */
    public final Set<b> f4751a = new CopyOnWriteArraySet();

    /* renamed from: b  reason: collision with root package name */
    public final AtomicBoolean f4752b = new AtomicBoolean();
    private final Context d;
    private final ConnectivityManager e;
    private ConnectivityManager.NetworkCallback f;
    private a g;

    /* compiled from: NetworkStateHelper */
    public interface b {
        void a(boolean z);
    }

    private e(Context context) {
        this.d = context.getApplicationContext();
        this.e = (ConnectivityManager) context.getSystemService("connectivity");
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                NetworkRequest.Builder builder = new NetworkRequest.Builder();
                builder.addCapability(12);
                this.f = new f(this);
                this.e.registerNetworkCallback(builder.build(), this.f);
                return;
            }
            this.g = new a(this, (byte) 0);
            this.d.registerReceiver(this.g, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            b();
        } catch (RuntimeException e2) {
            a.c("AppCenter", "Cannot access network state information.", e2);
            this.f4752b.set(true);
        }
    }

    public static synchronized e a(Context context) {
        e eVar;
        synchronized (e.class) {
            if (c == null) {
                c = new e(context);
            }
            eVar = c;
        }
        return eVar;
    }

    public boolean a() {
        if (Build.VERSION.SDK_INT >= 21) {
            Network[] allNetworks = this.e.getAllNetworks();
            if (allNetworks == null) {
                return false;
            }
            for (Network networkInfo : allNetworks) {
                NetworkInfo networkInfo2 = this.e.getNetworkInfo(networkInfo);
                if (networkInfo2 != null && networkInfo2.isConnected()) {
                    return true;
                }
            }
        } else {
            NetworkInfo[] allNetworkInfo = this.e.getAllNetworkInfo();
            if (allNetworkInfo == null) {
                return false;
            }
            for (NetworkInfo networkInfo3 : allNetworkInfo) {
                if (networkInfo3 != null && networkInfo3.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void b() {
        boolean a2 = a();
        if (this.f4752b.compareAndSet(!a2, a2)) {
            a(a2);
        }
    }

    private void a(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("Network has been ");
        sb.append(z ? "connected." : "disconnected.");
        a.b("AppCenter", sb.toString());
        for (b a2 : this.f4751a) {
            a2.a(z);
        }
    }

    public void close() {
        this.f4752b.set(false);
        if (Build.VERSION.SDK_INT >= 21) {
            this.e.unregisterNetworkCallback(this.f);
        } else {
            this.d.unregisterReceiver(this.g);
        }
    }

    public final void a(b bVar) {
        this.f4751a.add(bVar);
    }

    /* compiled from: NetworkStateHelper */
    class a extends BroadcastReceiver {
        private a() {
        }

        /* synthetic */ a(e eVar, byte b2) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            e.this.b();
        }
    }

    static /* synthetic */ void a(e eVar, Network network) {
        a.b("AppCenter", "Network " + network + " is available.");
        if (eVar.f4752b.compareAndSet(false, true)) {
            eVar.a(true);
        }
    }

    static /* synthetic */ void b(e eVar, Network network) {
        boolean z;
        a.b("AppCenter", "Network " + network + " is lost.");
        Network[] allNetworks = eVar.e.getAllNetworks();
        if (!(allNetworks == null || allNetworks.length == 0)) {
            if (!Arrays.equals(allNetworks, new Network[]{network})) {
                z = false;
                if (z && eVar.f4752b.compareAndSet(true, false)) {
                    eVar.a(false);
                    return;
                }
            }
        }
        z = true;
        if (z) {
        }
    }
}
