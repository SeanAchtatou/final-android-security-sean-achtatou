package com.microsoft.appcenter.utils.c;

import android.content.Context;
import com.microsoft.appcenter.utils.c.e;
import java.security.KeyStore;

/* compiled from: CryptoNoOpHandler */
class c implements b {
    public final String a() {
        return "None";
    }

    public final void a(e.d dVar, String str, Context context) {
    }

    public final byte[] a(e.d dVar, int i, KeyStore.Entry entry, byte[] bArr) {
        return bArr;
    }

    public final byte[] b(e.d dVar, int i, KeyStore.Entry entry, byte[] bArr) {
        return bArr;
    }

    c() {
    }
}
