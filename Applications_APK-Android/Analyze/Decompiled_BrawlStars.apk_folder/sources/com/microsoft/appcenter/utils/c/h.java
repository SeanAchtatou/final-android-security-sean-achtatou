package com.microsoft.appcenter.utils.c;

import com.microsoft.appcenter.utils.c.e;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;

/* compiled from: CryptoUtils */
class h implements e.c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Cipher f4741a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ f f4742b;

    h(f fVar, Cipher cipher) {
        this.f4742b = fVar;
        this.f4741a = cipher;
    }

    public final void a(int i, Key key) throws Exception {
        this.f4741a.init(i, key);
    }

    public final void a(int i, Key key, AlgorithmParameterSpec algorithmParameterSpec) throws Exception {
        this.f4741a.init(2, key, algorithmParameterSpec);
    }

    public final byte[] a(byte[] bArr) throws Exception {
        return this.f4741a.doFinal(bArr);
    }

    public final byte[] a(byte[] bArr, int i, int i2) throws Exception {
        return this.f4741a.doFinal(bArr, i, i2);
    }

    public final byte[] a() {
        return this.f4741a.getIV();
    }

    public final int b() {
        return this.f4741a.getBlockSize();
    }
}
