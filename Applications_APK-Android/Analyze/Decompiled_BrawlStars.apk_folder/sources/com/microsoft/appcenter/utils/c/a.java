package com.microsoft.appcenter.utils.c;

import android.content.Context;
import android.security.keystore.KeyGenParameterSpec;
import com.microsoft.appcenter.utils.c.e;
import java.security.KeyStore;
import java.util.Calendar;
import javax.crypto.spec.IvParameterSpec;

/* compiled from: CryptoAesHandler */
class a implements b {
    public final String a() {
        return "AES/CBC/PKCS7Padding/256";
    }

    a() {
    }

    public final void a(e.d dVar, String str, Context context) throws Exception {
        Calendar instance = Calendar.getInstance();
        instance.add(1, 1);
        e.C0157e a2 = dVar.a("AES", "AndroidKeyStore");
        a2.a(new KeyGenParameterSpec.Builder(str, 3).setBlockModes("CBC").setEncryptionPaddings("PKCS7Padding").setKeySize(256).setKeyValidityForOriginationEnd(instance.getTime()).build());
        a2.a();
    }

    public final byte[] a(e.d dVar, int i, KeyStore.Entry entry, byte[] bArr) throws Exception {
        e.c b2 = dVar.b("AES/CBC/PKCS7Padding", "AndroidKeyStoreBCWorkaround");
        b2.a(1, ((KeyStore.SecretKeyEntry) entry).getSecretKey());
        byte[] a2 = b2.a();
        byte[] a3 = b2.a(bArr);
        byte[] bArr2 = new byte[(a2.length + a3.length)];
        System.arraycopy(a2, 0, bArr2, 0, a2.length);
        System.arraycopy(a3, 0, bArr2, a2.length, a3.length);
        return bArr2;
    }

    public final byte[] b(e.d dVar, int i, KeyStore.Entry entry, byte[] bArr) throws Exception {
        e.c b2 = dVar.b("AES/CBC/PKCS7Padding", "AndroidKeyStoreBCWorkaround");
        int b3 = b2.b();
        b2.a(2, ((KeyStore.SecretKeyEntry) entry).getSecretKey(), new IvParameterSpec(bArr, 0, b3));
        return b2.a(bArr, b3, bArr.length - b3);
    }
}
