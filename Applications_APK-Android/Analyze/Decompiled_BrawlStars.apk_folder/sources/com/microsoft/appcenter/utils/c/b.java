package com.microsoft.appcenter.utils.c;

import android.content.Context;
import com.microsoft.appcenter.utils.c.e;
import java.security.KeyStore;

/* compiled from: CryptoHandler */
interface b {
    String a();

    void a(e.d dVar, String str, Context context) throws Exception;

    byte[] a(e.d dVar, int i, KeyStore.Entry entry, byte[] bArr) throws Exception;

    byte[] b(e.d dVar, int i, KeyStore.Entry entry, byte[] bArr) throws Exception;
}
