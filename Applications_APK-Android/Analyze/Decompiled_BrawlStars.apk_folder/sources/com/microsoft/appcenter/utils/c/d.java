package com.microsoft.appcenter.utils.c;

import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import com.microsoft.appcenter.utils.c.e;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import javax.security.auth.x500.X500Principal;

/* compiled from: CryptoRsaHandler */
class d implements b {
    public final String a() {
        return "RSA/ECB/PKCS1Padding/2048";
    }

    d() {
    }

    public final void a(e.d dVar, String str, Context context) throws Exception {
        Calendar instance = Calendar.getInstance();
        instance.add(1, 1);
        KeyPairGenerator instance2 = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
        KeyPairGeneratorSpec.Builder alias = new KeyPairGeneratorSpec.Builder(context).setAlias(str);
        instance2.initialize(alias.setSubject(new X500Principal("CN=" + str)).setStartDate(new Date()).setEndDate(instance.getTime()).setSerialNumber(BigInteger.TEN).setKeySize(2048).build());
        instance2.generateKeyPair();
    }

    private static e.c a(e.d dVar, int i) throws Exception {
        return dVar.b("RSA/ECB/PKCS1Padding", i >= 23 ? "AndroidKeyStoreBCWorkaround" : "AndroidOpenSSL");
    }

    public final byte[] a(e.d dVar, int i, KeyStore.Entry entry, byte[] bArr) throws Exception {
        e.c a2 = a(dVar, i);
        X509Certificate x509Certificate = (X509Certificate) ((KeyStore.PrivateKeyEntry) entry).getCertificate();
        try {
            x509Certificate.checkValidity();
            a2.a(1, x509Certificate.getPublicKey());
            return a2.a(bArr);
        } catch (CertificateExpiredException e) {
            throw new InvalidKeyException(e);
        }
    }

    public final byte[] b(e.d dVar, int i, KeyStore.Entry entry, byte[] bArr) throws Exception {
        e.c a2 = a(dVar, i);
        a2.a(2, ((KeyStore.PrivateKeyEntry) entry).getPrivateKey());
        return a2.a(bArr);
    }
}
