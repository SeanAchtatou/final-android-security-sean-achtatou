package com.microsoft.appcenter.utils;

import com.microsoft.appcenter.utils.d.d;
import java.util.UUID;

/* compiled from: IdHelper */
public class c {
    public static UUID a() {
        try {
            return UUID.fromString(d.a("installId", ""));
        } catch (Exception unused) {
            a.d("AppCenter", "Unable to get installID from Shared Preferences");
            UUID randomUUID = UUID.randomUUID();
            d.b("installId", randomUUID.toString());
            return randomUUID;
        }
    }
}
