package com.microsoft.appcenter.utils.b;

import android.content.Context;
import android.text.TextUtils;
import com.microsoft.appcenter.utils.c.e;
import com.microsoft.appcenter.utils.d.d;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: AuthTokenContext */
public class b {

    /* renamed from: b  reason: collision with root package name */
    private static b f4721b;

    /* renamed from: a  reason: collision with root package name */
    public final Set<a> f4722a = Collections.newSetFromMap(new ConcurrentHashMap());
    private Context c;
    private List<c> d;
    private boolean e = true;

    /* compiled from: AuthTokenContext */
    public interface a {
        void a();
    }

    public static synchronized void a(Context context) {
        synchronized (b.class) {
            b a2 = a();
            a2.c = context.getApplicationContext();
            a2.e();
        }
    }

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (f4721b == null) {
                f4721b = new b();
            }
            bVar = f4721b;
        }
        return bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002c, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.e     // Catch:{ all -> 0x002d }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r3)
            return
        L_0x0007:
            r0 = 0
            r3.e = r0     // Catch:{ all -> 0x002d }
            r0 = 0
            java.lang.Boolean r0 = r3.a(r0, r0, r0)     // Catch:{ all -> 0x002d }
            if (r0 != 0) goto L_0x0012
            goto L_0x002b
        L_0x0012:
            java.util.Set<com.microsoft.appcenter.utils.b.b$a> r1 = r3.f4722a     // Catch:{ all -> 0x002d }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x002d }
        L_0x0018:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x002d }
            if (r2 == 0) goto L_0x002b
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x002d }
            com.microsoft.appcenter.utils.b.b$a r2 = (com.microsoft.appcenter.utils.b.b.a) r2     // Catch:{ all -> 0x002d }
            r2.a()     // Catch:{ all -> 0x002d }
            r0.booleanValue()     // Catch:{ all -> 0x002d }
            goto L_0x0018
        L_0x002b:
            monitor-exit(r3)
            return
        L_0x002d:
            r0 = move-exception
            monitor-exit(r3)
            goto L_0x0031
        L_0x0030:
            throw r0
        L_0x0031:
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.utils.b.b.b():void");
    }

    private synchronized Boolean a(String str, String str2, Date date) {
        List e2 = e();
        if (e2 == null) {
            e2 = new ArrayList();
        }
        boolean z = true;
        c cVar = e2.size() > 0 ? (c) e2.get(e2.size() - 1) : null;
        if (cVar != null && TextUtils.equals(cVar.f4723a, str)) {
            return null;
        }
        if (cVar != null) {
            if (TextUtils.equals(cVar.f4724b, str2)) {
                z = false;
            }
        }
        Date date2 = new Date();
        if (!(cVar == null || cVar.d == null || !date2.after(cVar.d))) {
            if (z) {
                if (str != null) {
                    e2.add(new c(null, null, cVar.d, date2));
                }
            }
            date2 = cVar.d;
        }
        e2.add(new c(str, str2, date2, date));
        if (e2.size() > 5) {
            e2.subList(0, e2.size() - 5).clear();
            com.microsoft.appcenter.utils.a.b("AppCenter", "Size of the token history is exceeded. The oldest token has been removed.");
        }
        a(e2);
        return Boolean.valueOf(z);
    }

    public final synchronized List<d> c() {
        Date date;
        List<c> e2 = e();
        if (e2 != null) {
            if (e2.size() != 0) {
                ArrayList arrayList = new ArrayList();
                int i = 0;
                if (e2.get(0).f4723a != null) {
                    arrayList.add(new d(null, null, e2.get(0).c));
                }
                while (i < e2.size()) {
                    c cVar = e2.get(i);
                    String str = cVar.f4723a;
                    Date date2 = cVar.c;
                    if (str == null && i == 0) {
                        date2 = null;
                    }
                    Date date3 = cVar.d;
                    i++;
                    if (e2.size() > i) {
                        date = e2.get(i).c;
                    } else {
                        date = null;
                    }
                    if (date == null || date3 == null || !date.before(date3)) {
                        if (date3 == null) {
                            if (date == null) {
                            }
                        }
                        arrayList.add(new d(str, date2, date3));
                    }
                    date3 = date;
                    arrayList.add(new d(str, date2, date3));
                }
                return arrayList;
            }
        }
        return Collections.singletonList(new d());
    }

    public final synchronized void a(String str) {
        List<c> e2 = e();
        if (e2 != null) {
            if (e2.size() != 0) {
                if (e2.size() == 1) {
                    com.microsoft.appcenter.utils.a.b("AppCenter", "Couldn't remove token from history: token history contains only current one.");
                    return;
                } else if (!TextUtils.equals(e2.get(0).f4723a, str)) {
                    com.microsoft.appcenter.utils.a.b("AppCenter", "Couldn't remove token from history: the token isn't oldest or is already removed.");
                    return;
                } else {
                    e2.remove(0);
                    a(e2);
                    com.microsoft.appcenter.utils.a.b("AppCenter", "The token has been removed from token history.");
                    return;
                }
            }
        }
        com.microsoft.appcenter.utils.a.d("AppCenter", "Couldn't remove token from history: token history is empty.");
    }

    public final void a(d dVar) {
        boolean z;
        c d2 = d();
        if (d2 != null && dVar.f4725a != null && dVar.f4725a.equals(d2.f4723a)) {
            if (dVar.c == null) {
                z = false;
            } else {
                Calendar instance = Calendar.getInstance();
                instance.add(13, 600);
                z = instance.getTime().after(dVar.c);
            }
            if (z) {
                Iterator<a> it = this.f4722a.iterator();
                while (it.hasNext()) {
                    it.next();
                }
            }
        }
    }

    private synchronized c d() {
        List<c> e2 = e();
        if (e2 == null || e2.size() <= 0) {
            return null;
        }
        return e2.get(e2.size() - 1);
    }

    private List<c> e() {
        String str;
        List<c> list = this.d;
        if (list != null) {
            return list;
        }
        String a2 = d.a("AppCenter.auth_token_history", (String) null);
        if (a2 == null || a2.isEmpty()) {
            str = null;
        } else {
            str = e.a(this.c).a(a2, false).f4737a;
        }
        if (str == null || str.isEmpty()) {
            return null;
        }
        try {
            JSONArray jSONArray = new JSONArray(str);
            ArrayList arrayList = new ArrayList(jSONArray.length());
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                c cVar = new c();
                cVar.a(jSONObject);
                arrayList.add(cVar);
            }
            this.d = arrayList;
        } catch (JSONException e2) {
            com.microsoft.appcenter.utils.a.b("AppCenter", "Failed to deserialize auth token history.", e2);
        }
        return this.d;
    }

    private void a(List<c> list) {
        this.d = list;
        if (list != null) {
            try {
                d.b("AppCenter.auth_token_history", e.a(this.c).a(b(list)));
            } catch (JSONException e2) {
                com.microsoft.appcenter.utils.a.b("AppCenter", "Failed to serialize auth token history.", e2);
            }
        } else {
            d.a("AppCenter.auth_token_history");
        }
    }

    private static String b(List<c> list) throws JSONException {
        JSONStringer jSONStringer = new JSONStringer();
        jSONStringer.array();
        for (c a2 : list) {
            jSONStringer.object();
            a2.a(jSONStringer);
            jSONStringer.endObject();
        }
        jSONStringer.endArray();
        return jSONStringer.toString();
    }
}
