package com.microsoft.appcenter.utils.a;

import com.microsoft.appcenter.utils.b;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;

/* compiled from: DefaultAppCenterFuture */
public class c<T> implements b<T> {

    /* renamed from: a  reason: collision with root package name */
    T f4714a;

    /* renamed from: b  reason: collision with root package name */
    Collection<a<T>> f4715b;
    private final CountDownLatch c = new CountDownLatch(1);

    /* JADX WARNING: Can't wrap try/catch for region: R(5:0|1|4|2|3) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:0:0x0000 */
    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP:0: B:0:0x0000->B:1:?, LOOP_START, MTH_ENTER_BLOCK, SYNTHETIC, Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final T a() {
        /*
            r1 = this;
        L_0x0000:
            java.util.concurrent.CountDownLatch r0 = r1.c     // Catch:{ InterruptedException -> 0x0000 }
            r0.await()     // Catch:{ InterruptedException -> 0x0000 }
            T r0 = r1.f4714a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.utils.a.c.a():java.lang.Object");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:0|1|2) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:0:0x0000 */
    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[MTH_ENTER_BLOCK, SYNTHETIC, Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b() {
        /*
            r4 = this;
        L_0x0000:
            java.util.concurrent.CountDownLatch r0 = r4.c     // Catch:{ InterruptedException -> 0x0000 }
            r1 = 0
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException -> 0x0000 }
            boolean r0 = r0.await(r1, r3)     // Catch:{ InterruptedException -> 0x0000 }
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.utils.a.c.b():boolean");
    }

    public final synchronized void a(a aVar) {
        if (b()) {
            b.a(new d(this, aVar));
        } else {
            if (this.f4715b == null) {
                this.f4715b = new LinkedList();
            }
            this.f4715b.add(aVar);
        }
    }

    public final synchronized void a(Object obj) {
        if (!b()) {
            this.f4714a = obj;
            this.c.countDown();
            if (this.f4715b != null) {
                b.a(new e(this, obj));
            }
        }
    }
}
