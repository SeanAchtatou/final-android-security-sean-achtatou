package com.microsoft.appcenter.utils;

import android.os.Bundle;

/* compiled from: InstrumentationRegistryHelper */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f4743a = {"androidx.test.platform.app.InstrumentationRegistry", "androidx.test.InstrumentationRegistry", "android.support.test.InstrumentationRegistry"};

    public static Bundle a() throws IllegalStateException {
        String[] strArr = f4743a;
        Exception e = null;
        int i = 0;
        while (i < strArr.length) {
            try {
                return (Bundle) Class.forName(strArr[i]).getMethod("getArguments", new Class[0]).invoke(null, new Object[0]);
            } catch (Exception e2) {
                e = e2;
                i++;
            }
        }
        throw new IllegalStateException(e);
    }
}
