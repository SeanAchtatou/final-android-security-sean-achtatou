package com.microsoft.appcenter.utils.c;

import android.content.Context;
import android.os.Build;
import android.util.Base64;
import java.security.Key;
import java.security.KeyStore;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Date;
import java.util.Map;

/* compiled from: CryptoUtils */
public class e {

    /* renamed from: a  reason: collision with root package name */
    static final d f4733a = new f();
    private static e c;

    /* renamed from: b  reason: collision with root package name */
    final Map<String, a> f4734b;
    private final Context d;
    private final d e;
    private final int f;
    private final KeyStore g;

    /* compiled from: CryptoUtils */
    interface c {
        void a(int i, Key key) throws Exception;

        void a(int i, Key key, AlgorithmParameterSpec algorithmParameterSpec) throws Exception;

        byte[] a();

        byte[] a(byte[] bArr) throws Exception;

        byte[] a(byte[] bArr, int i, int i2) throws Exception;

        int b();
    }

    /* compiled from: CryptoUtils */
    interface d {
        C0157e a(String str, String str2) throws Exception;

        c b(String str, String str2) throws Exception;
    }

    /* renamed from: com.microsoft.appcenter.utils.c.e$e  reason: collision with other inner class name */
    /* compiled from: CryptoUtils */
    interface C0157e {
        void a();

        void a(AlgorithmParameterSpec algorithmParameterSpec) throws Exception;
    }

    private e(Context context) {
        this(context, f4733a, Build.VERSION.SDK_INT);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0044 A[SYNTHETIC, Splitter:B:21:0x0044] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private e(android.content.Context r2, com.microsoft.appcenter.utils.c.e.d r3, int r4) {
        /*
            r1 = this;
            r1.<init>()
            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap
            r0.<init>()
            r1.f4734b = r0
            android.content.Context r2 = r2.getApplicationContext()
            r1.d = r2
            r1.e = r3
            r1.f = r4
            r2 = 0
            java.lang.String r3 = "AppCenter"
            r0 = 19
            if (r4 < r0) goto L_0x002c
            java.lang.String r0 = "AndroidKeyStore"
            java.security.KeyStore r0 = java.security.KeyStore.getInstance(r0)     // Catch:{ Exception -> 0x0027 }
            r0.load(r2)     // Catch:{ Exception -> 0x0026 }
            r2 = r0
            goto L_0x002c
        L_0x0026:
            r2 = r0
        L_0x0027:
            java.lang.String r0 = "Cannot use secure keystore on this device."
            com.microsoft.appcenter.utils.a.e(r3, r0)
        L_0x002c:
            r1.g = r2
            if (r2 == 0) goto L_0x0042
            r0 = 23
            if (r4 < r0) goto L_0x0042
            com.microsoft.appcenter.utils.c.a r4 = new com.microsoft.appcenter.utils.c.a     // Catch:{ Exception -> 0x003d }
            r4.<init>()     // Catch:{ Exception -> 0x003d }
            r1.a(r4)     // Catch:{ Exception -> 0x003d }
            goto L_0x0042
        L_0x003d:
            java.lang.String r4 = "Cannot use modern encryption on this device."
            com.microsoft.appcenter.utils.a.e(r3, r4)
        L_0x0042:
            if (r2 == 0) goto L_0x0052
            com.microsoft.appcenter.utils.c.d r2 = new com.microsoft.appcenter.utils.c.d     // Catch:{ Exception -> 0x004d }
            r2.<init>()     // Catch:{ Exception -> 0x004d }
            r1.a(r2)     // Catch:{ Exception -> 0x004d }
            goto L_0x0052
        L_0x004d:
            java.lang.String r2 = "Cannot use old encryption on this device."
            com.microsoft.appcenter.utils.a.e(r3, r2)
        L_0x0052:
            com.microsoft.appcenter.utils.c.c r2 = new com.microsoft.appcenter.utils.c.c
            r2.<init>()
            java.util.Map<java.lang.String, com.microsoft.appcenter.utils.c.e$a> r3 = r1.f4734b
            com.microsoft.appcenter.utils.c.e$a r4 = new com.microsoft.appcenter.utils.c.e$a
            r0 = 0
            r4.<init>(r0, r0, r2)
            java.lang.String r2 = "None"
            r3.put(r2, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.utils.c.e.<init>(android.content.Context, com.microsoft.appcenter.utils.c.e$d, int):void");
    }

    public static e a(Context context) {
        if (c == null) {
            c = new e(context);
        }
        return c;
    }

    private void a(b bVar) throws Exception {
        int i;
        int i2 = 0;
        String a2 = a(bVar, 0, false);
        String a3 = a(bVar, 1, false);
        String a4 = a(bVar, 0, true);
        String a5 = a(bVar, 1, true);
        Date creationDate = this.g.getCreationDate(a2);
        Date creationDate2 = this.g.getCreationDate(a3);
        Date creationDate3 = this.g.getCreationDate(a4);
        Date creationDate4 = this.g.getCreationDate(a5);
        if (creationDate2 == null || !creationDate2.after(creationDate)) {
            i = 0;
        } else {
            a2 = a3;
            i = 1;
        }
        if (creationDate4 != null && creationDate4.after(creationDate3)) {
            i2 = 1;
        }
        if (this.f4734b.isEmpty() && !this.g.containsAlias(a2)) {
            com.microsoft.appcenter.utils.a.b("AppCenter", "Creating alias: " + a2);
            bVar.a(this.e, a2, this.d);
        }
        com.microsoft.appcenter.utils.a.b("AppCenter", "Using " + a2);
        this.f4734b.put(bVar.a(), new a(i, i2, bVar));
    }

    private static String a(b bVar, int i, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(z ? "mobile.center" : "appcenter");
        sb.append(".");
        sb.append(i);
        sb.append(".");
        sb.append(bVar.a());
        return sb.toString();
    }

    private KeyStore.Entry b(b bVar, int i, boolean z) throws Exception {
        if (this.g == null) {
            return null;
        }
        return this.g.getEntry(a(bVar, i, z), null);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:9|10|(1:12)|13|14) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        com.microsoft.appcenter.utils.a.b("AppCenter", "Alias expired: " + r1.f4736b);
        r1.f4736b = r1.f4736b ^ 1;
        r1 = a(r2, r1.f4736b, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0074, code lost:
        if (r8.g.containsAlias(r1) != false) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0076, code lost:
        com.microsoft.appcenter.utils.a.b("AppCenter", "Deleting alias: " + r1);
        r8.g.deleteEntry(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x008f, code lost:
        com.microsoft.appcenter.utils.a.b("AppCenter", "Creating alias: " + r1);
        r2.a(r8.e, r1, r8.d);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00ae, code lost:
        return a(r9);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x004c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(java.lang.String r9) {
        /*
            r8 = this;
            java.lang.String r0 = "AppCenter"
            if (r9 != 0) goto L_0x0006
            r9 = 0
            return r9
        L_0x0006:
            java.util.Map<java.lang.String, com.microsoft.appcenter.utils.c.e$a> r1 = r8.f4734b     // Catch:{ Exception -> 0x00af }
            java.util.Collection r1 = r1.values()     // Catch:{ Exception -> 0x00af }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ Exception -> 0x00af }
            java.lang.Object r1 = r1.next()     // Catch:{ Exception -> 0x00af }
            com.microsoft.appcenter.utils.c.e$a r1 = (com.microsoft.appcenter.utils.c.e.a) r1     // Catch:{ Exception -> 0x00af }
            com.microsoft.appcenter.utils.c.b r2 = r1.f4735a     // Catch:{ Exception -> 0x00af }
            r3 = 0
            com.microsoft.appcenter.utils.c.b r4 = r1.f4735a     // Catch:{ InvalidKeyException -> 0x004c }
            int r5 = r1.f4736b     // Catch:{ InvalidKeyException -> 0x004c }
            java.security.KeyStore$Entry r4 = r8.b(r4, r5, r3)     // Catch:{ InvalidKeyException -> 0x004c }
            com.microsoft.appcenter.utils.c.e$d r5 = r8.e     // Catch:{ InvalidKeyException -> 0x004c }
            int r6 = r8.f     // Catch:{ InvalidKeyException -> 0x004c }
            java.lang.String r7 = "UTF-8"
            byte[] r7 = r9.getBytes(r7)     // Catch:{ InvalidKeyException -> 0x004c }
            byte[] r4 = r2.a(r5, r6, r4, r7)     // Catch:{ InvalidKeyException -> 0x004c }
            java.lang.String r4 = android.util.Base64.encodeToString(r4, r3)     // Catch:{ InvalidKeyException -> 0x004c }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ InvalidKeyException -> 0x004c }
            r5.<init>()     // Catch:{ InvalidKeyException -> 0x004c }
            java.lang.String r6 = r2.a()     // Catch:{ InvalidKeyException -> 0x004c }
            r5.append(r6)     // Catch:{ InvalidKeyException -> 0x004c }
            java.lang.String r6 = ":"
            r5.append(r6)     // Catch:{ InvalidKeyException -> 0x004c }
            r5.append(r4)     // Catch:{ InvalidKeyException -> 0x004c }
            java.lang.String r9 = r5.toString()     // Catch:{ InvalidKeyException -> 0x004c }
            return r9
        L_0x004c:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00af }
            r4.<init>()     // Catch:{ Exception -> 0x00af }
            java.lang.String r5 = "Alias expired: "
            r4.append(r5)     // Catch:{ Exception -> 0x00af }
            int r5 = r1.f4736b     // Catch:{ Exception -> 0x00af }
            r4.append(r5)     // Catch:{ Exception -> 0x00af }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00af }
            com.microsoft.appcenter.utils.a.b(r0, r4)     // Catch:{ Exception -> 0x00af }
            int r4 = r1.f4736b     // Catch:{ Exception -> 0x00af }
            r4 = r4 ^ 1
            r1.f4736b = r4     // Catch:{ Exception -> 0x00af }
            int r1 = r1.f4736b     // Catch:{ Exception -> 0x00af }
            java.lang.String r1 = a(r2, r1, r3)     // Catch:{ Exception -> 0x00af }
            java.security.KeyStore r3 = r8.g     // Catch:{ Exception -> 0x00af }
            boolean r3 = r3.containsAlias(r1)     // Catch:{ Exception -> 0x00af }
            if (r3 == 0) goto L_0x008f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00af }
            r3.<init>()     // Catch:{ Exception -> 0x00af }
            java.lang.String r4 = "Deleting alias: "
            r3.append(r4)     // Catch:{ Exception -> 0x00af }
            r3.append(r1)     // Catch:{ Exception -> 0x00af }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00af }
            com.microsoft.appcenter.utils.a.b(r0, r3)     // Catch:{ Exception -> 0x00af }
            java.security.KeyStore r3 = r8.g     // Catch:{ Exception -> 0x00af }
            r3.deleteEntry(r1)     // Catch:{ Exception -> 0x00af }
        L_0x008f:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00af }
            r3.<init>()     // Catch:{ Exception -> 0x00af }
            java.lang.String r4 = "Creating alias: "
            r3.append(r4)     // Catch:{ Exception -> 0x00af }
            r3.append(r1)     // Catch:{ Exception -> 0x00af }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00af }
            com.microsoft.appcenter.utils.a.b(r0, r3)     // Catch:{ Exception -> 0x00af }
            com.microsoft.appcenter.utils.c.e$d r3 = r8.e     // Catch:{ Exception -> 0x00af }
            android.content.Context r4 = r8.d     // Catch:{ Exception -> 0x00af }
            r2.a(r3, r1, r4)     // Catch:{ Exception -> 0x00af }
            java.lang.String r9 = r8.a(r9)     // Catch:{ Exception -> 0x00af }
            return r9
        L_0x00af:
            java.lang.String r1 = "Failed to encrypt data."
            com.microsoft.appcenter.utils.a.e(r0, r1)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.utils.c.e.a(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0048, code lost:
        return a(r2, r1.f4736b ^ 1, r0[1], false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0049, code lost:
        com.microsoft.appcenter.utils.a.e("AppCenter", "Failed to decrypt data.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0051, code lost:
        return new com.microsoft.appcenter.utils.c.e.b(r10, null);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x003f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.microsoft.appcenter.utils.c.e.b a(java.lang.String r10, boolean r11) {
        /*
            r9 = this;
            r11 = 0
            if (r10 != 0) goto L_0x0009
            com.microsoft.appcenter.utils.c.e$b r10 = new com.microsoft.appcenter.utils.c.e$b
            r10.<init>(r11, r11)
            return r10
        L_0x0009:
            java.lang.String r0 = ":"
            java.lang.String[] r0 = r10.split(r0)
            int r1 = r0.length
            r2 = 2
            r3 = 0
            if (r1 != r2) goto L_0x001f
            java.util.Map<java.lang.String, com.microsoft.appcenter.utils.c.e$a> r1 = r9.f4734b
            r2 = r0[r3]
            java.lang.Object r1 = r1.get(r2)
            com.microsoft.appcenter.utils.c.e$a r1 = (com.microsoft.appcenter.utils.c.e.a) r1
            goto L_0x0020
        L_0x001f:
            r1 = r11
        L_0x0020:
            if (r1 != 0) goto L_0x0024
            r2 = r11
            goto L_0x0026
        L_0x0024:
            com.microsoft.appcenter.utils.c.b r2 = r1.f4735a
        L_0x0026:
            java.lang.String r4 = "Failed to decrypt data."
            java.lang.String r5 = "AppCenter"
            if (r2 != 0) goto L_0x0035
            com.microsoft.appcenter.utils.a.e(r5, r4)
            com.microsoft.appcenter.utils.c.e$b r0 = new com.microsoft.appcenter.utils.c.e$b
            r0.<init>(r10, r11)
            return r0
        L_0x0035:
            r6 = 1
            int r7 = r1.f4736b     // Catch:{ Exception -> 0x003f }
            r8 = r0[r6]     // Catch:{ Exception -> 0x003f }
            com.microsoft.appcenter.utils.c.e$b r10 = r9.a(r2, r7, r8, r3)     // Catch:{ Exception -> 0x003f }
            return r10
        L_0x003f:
            int r1 = r1.f4736b     // Catch:{ Exception -> 0x0049 }
            r1 = r1 ^ r6
            r0 = r0[r6]     // Catch:{ Exception -> 0x0049 }
            com.microsoft.appcenter.utils.c.e$b r10 = r9.a(r2, r1, r0, r3)     // Catch:{ Exception -> 0x0049 }
            return r10
        L_0x0049:
            com.microsoft.appcenter.utils.a.e(r5, r4)
            com.microsoft.appcenter.utils.c.e$b r0 = new com.microsoft.appcenter.utils.c.e$b
            r0.<init>(r10, r11)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.utils.c.e.a(java.lang.String, boolean):com.microsoft.appcenter.utils.c.e$b");
    }

    private b a(b bVar, int i, String str, boolean z) throws Exception {
        String str2 = new String(bVar.b(this.e, this.f, b(bVar, i, z), Base64.decode(str, 0)), "UTF-8");
        return new b(str2, bVar != this.f4734b.values().iterator().next().f4735a ? a(str2) : null);
    }

    /* compiled from: CryptoUtils */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        final b f4735a;

        /* renamed from: b  reason: collision with root package name */
        int f4736b;
        final int c;

        a(int i, int i2, b bVar) {
            this.f4736b = i;
            this.c = i2;
            this.f4735a = bVar;
        }
    }

    /* compiled from: CryptoUtils */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public final String f4737a;

        /* renamed from: b  reason: collision with root package name */
        final String f4738b;

        public b(String str, String str2) {
            this.f4737a = str;
            this.f4738b = str2;
        }
    }
}
