package com.microsoft.appcenter.utils.b;

import android.text.TextUtils;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: UserIdContext */
public class f {

    /* renamed from: b  reason: collision with root package name */
    private static f f4731b;

    /* renamed from: a  reason: collision with root package name */
    public final Set<Object> f4732a = Collections.newSetFromMap(new ConcurrentHashMap());
    private String c;

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (f4731b == null) {
                f4731b = new f();
            }
            fVar = f4731b;
        }
        return fVar;
    }

    public final synchronized String b() {
        return this.c;
    }

    public synchronized boolean a(String str) {
        if (TextUtils.equals(this.c, str)) {
            return false;
        }
        this.c = str;
        return true;
    }
}
