package com.microsoft.appcenter.utils.d;

import android.content.Context;
import android.text.TextUtils;
import com.microsoft.appcenter.utils.a;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/* compiled from: FileManager */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static Context f4748a;

    public static synchronized void a(Context context) {
        synchronized (c.class) {
            if (f4748a == null) {
                f4748a = context;
            }
        }
    }

    public static String a(File file) {
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            String property = System.getProperty("line.separator");
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                    sb.append(property);
                } else {
                    bufferedReader.close();
                    return sb.toString();
                }
            }
        } catch (IOException e) {
            a.c("AppCenter", "Could not read file " + file.getAbsolutePath(), e);
            return null;
        } catch (Throwable th) {
            bufferedReader.close();
            throw th;
        }
    }

    public static byte[] b(File file) {
        FileInputStream fileInputStream;
        byte[] bArr = new byte[((int) file.length())];
        try {
            fileInputStream = new FileInputStream(file);
            new DataInputStream(fileInputStream).readFully(bArr);
            fileInputStream.close();
            return bArr;
        } catch (IOException e) {
            a.c("AppCenter", "Could not read file " + file.getAbsolutePath(), e);
            return null;
        } catch (Throwable th) {
            fileInputStream.close();
            throw th;
        }
    }

    public static void a(File file, String str) throws IOException {
        if (!TextUtils.isEmpty(str) && TextUtils.getTrimmedLength(str) > 0) {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            try {
                bufferedWriter.write(str);
            } finally {
                bufferedWriter.close();
            }
        }
    }

    public static <T extends Serializable> T c(File file) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
        try {
            return (Serializable) objectInputStream.readObject();
        } finally {
            objectInputStream.close();
        }
    }

    public static File a(File file, FilenameFilter filenameFilter) {
        File file2 = null;
        if (file.exists()) {
            File[] listFiles = file.listFiles(filenameFilter);
            long j = 0;
            if (listFiles != null) {
                for (File file3 : listFiles) {
                    if (file3.lastModified() > j) {
                        j = file3.lastModified();
                        file2 = file3;
                    }
                }
            }
        }
        return file2;
    }

    public static void a(String str) {
        new File(str).mkdirs();
    }
}
