package com.microsoft.appcenter.utils;

import android.os.Handler;
import android.os.Looper;

/* compiled from: HandlerUtils */
public class b {

    /* renamed from: a  reason: collision with root package name */
    static final Handler f4720a = new Handler(Looper.getMainLooper());

    public static void a(Runnable runnable) {
        if (Thread.currentThread() == f4720a.getLooper().getThread()) {
            runnable.run();
        } else {
            f4720a.post(runnable);
        }
    }
}
