package com.microsoft.appcenter.utils.c;

import com.microsoft.appcenter.utils.c.e;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

/* compiled from: CryptoUtils */
final class f implements e.d {
    f() {
    }

    public final e.C0157e a(String str, String str2) throws Exception {
        return new g(this, KeyGenerator.getInstance(str, str2));
    }

    public final e.c b(String str, String str2) throws Exception {
        return new h(this, Cipher.getInstance(str, str2));
    }
}
