package com.microsoft.appcenter.utils.b;

import android.content.SharedPreferences;
import com.microsoft.appcenter.utils.d.d;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

/* compiled from: SessionContext */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private static e f4727a;

    /* renamed from: b  reason: collision with root package name */
    private final NavigableMap<Long, a> f4728b = new TreeMap();
    private final long c = System.currentTimeMillis();

    private e() {
        Set<String> stringSet = d.f4749a.getStringSet("sessions", null);
        if (stringSet != null) {
            for (String next : stringSet) {
                String[] split = next.split("/", -1);
                try {
                    long parseLong = Long.parseLong(split[0]);
                    String str = split[1];
                    this.f4728b.put(Long.valueOf(parseLong), new a(parseLong, str.isEmpty() ? null : UUID.fromString(str), split.length > 2 ? Long.parseLong(split[2]) : parseLong));
                } catch (RuntimeException e) {
                    com.microsoft.appcenter.utils.a.b("AppCenter", "Ignore invalid session in store: " + next, e);
                }
            }
        }
        com.microsoft.appcenter.utils.a.b("AppCenter", "Loaded stored sessions: " + this.f4728b);
        a((UUID) null);
    }

    public static synchronized e a() {
        e eVar;
        synchronized (e.class) {
            if (f4727a == null) {
                f4727a = new e();
            }
            eVar = f4727a;
        }
        return eVar;
    }

    public final synchronized void a(UUID uuid) {
        long currentTimeMillis = System.currentTimeMillis();
        this.f4728b.put(Long.valueOf(currentTimeMillis), new a(currentTimeMillis, uuid, this.c));
        if (this.f4728b.size() > 10) {
            this.f4728b.pollFirstEntry();
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (a aVar : this.f4728b.values()) {
            linkedHashSet.add(aVar.toString());
        }
        SharedPreferences.Editor edit = d.f4749a.edit();
        edit.putStringSet("sessions", linkedHashSet);
        edit.apply();
    }

    public final synchronized a a(long j) {
        Map.Entry<Long, a> floorEntry = this.f4728b.floorEntry(Long.valueOf(j));
        if (floorEntry == null) {
            return null;
        }
        return floorEntry.getValue();
    }

    public final synchronized void b() {
        this.f4728b.clear();
        d.a("sessions");
    }

    /* compiled from: SessionContext */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public final UUID f4729a;

        /* renamed from: b  reason: collision with root package name */
        public final long f4730b;
        private final long c;

        a(long j, UUID uuid, long j2) {
            this.c = j;
            this.f4729a = uuid;
            this.f4730b = j2;
        }

        public String toString() {
            String str = this.c + "/";
            if (this.f4729a != null) {
                str = str + this.f4729a;
            }
            return str + "/" + this.f4730b;
        }
    }
}
