package com.microsoft.appcenter.utils.b;

import com.microsoft.appcenter.b.a.a.d;
import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: AuthTokenHistoryEntry */
final class c implements g {

    /* renamed from: a  reason: collision with root package name */
    String f4723a;

    /* renamed from: b  reason: collision with root package name */
    String f4724b;
    Date c;
    Date d;

    c() {
    }

    c(String str, String str2, Date date, Date date2) {
        this.f4723a = str;
        this.f4724b = str2;
        this.c = date;
        this.d = date2;
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        Date date;
        Date date2 = null;
        this.f4723a = jSONObject.optString("authToken", null);
        this.f4724b = jSONObject.optString("homeAccountId", null);
        String optString = jSONObject.optString("time", null);
        if (optString != null) {
            date = d.a(optString);
        } else {
            date = null;
        }
        this.c = date;
        String optString2 = jSONObject.optString("expiresOn", null);
        if (optString2 != null) {
            date2 = d.a(optString2);
        }
        this.d = date2;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        String str;
        f.a(jSONStringer, "authToken", this.f4723a);
        f.a(jSONStringer, "homeAccountId", this.f4724b);
        Date date = this.c;
        String str2 = null;
        if (date != null) {
            str = d.a(date);
        } else {
            str = null;
        }
        f.a(jSONStringer, "time", str);
        Date date2 = this.d;
        if (date2 != null) {
            str2 = d.a(date2);
        }
        f.a(jSONStringer, "expiresOn", str2);
    }
}
