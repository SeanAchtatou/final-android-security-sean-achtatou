package com.microsoft.appcenter.a;

import android.content.Context;
import android.os.Handler;
import com.microsoft.appcenter.CancellationException;
import com.microsoft.appcenter.a.b;
import com.microsoft.appcenter.b.a.d;
import com.microsoft.appcenter.b.a.e;
import com.microsoft.appcenter.http.k;
import com.microsoft.appcenter.persistence.Persistence;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/* compiled from: DefaultChannel */
public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    final Handler f4531a;

    /* renamed from: b  reason: collision with root package name */
    private final Context f4532b;
    private String c;
    private final UUID d;
    private final Map<String, a> e;
    private final Collection<b.C0155b> f;
    private final Persistence g;
    private final com.microsoft.appcenter.b.b h;
    private final Set<com.microsoft.appcenter.b.b> i;
    private boolean j;
    private boolean k;
    private com.microsoft.appcenter.b.a.c l;
    private int m;

    private c(Context context, String str, Persistence persistence, com.microsoft.appcenter.b.b bVar, Handler handler) {
        this.f4532b = context;
        this.c = str;
        this.d = com.microsoft.appcenter.utils.c.a();
        this.e = new HashMap();
        this.f = new LinkedHashSet();
        this.g = persistence;
        this.h = bVar;
        this.i = new HashSet();
        this.i.add(this.h);
        this.f4531a = handler;
        this.j = true;
    }

    public final synchronized boolean a(long j2) {
        return this.g.a(j2);
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean a(a aVar, int i2) {
        return i2 == this.m && aVar == this.e.get(aVar.f4533a);
    }

    public final synchronized void a(String str) {
        this.c = str;
        if (this.j) {
            for (a next : this.e.values()) {
                if (next.f == this.h) {
                    b(next);
                }
            }
        }
    }

    public final synchronized void a(String str, int i2, long j2, int i3, com.microsoft.appcenter.b.b bVar, b.a aVar) {
        com.microsoft.appcenter.utils.a.b("AppCenter", "addGroup(" + str + ")");
        com.microsoft.appcenter.b.b bVar2 = bVar == null ? this.h : bVar;
        this.i.add(bVar2);
        a aVar2 = new a(str, i2, j2, i3, bVar2, aVar);
        this.e.put(str, aVar2);
        aVar2.h = this.g.b(str);
        com.microsoft.appcenter.utils.b.b.a().f4722a.add(aVar2);
        if (!(this.c == null && this.h == bVar2)) {
            b(aVar2);
        }
        for (b.C0155b a2 : this.f) {
            a2.a(str, aVar, j2);
        }
    }

    public final synchronized void b(String str) {
        com.microsoft.appcenter.utils.a.b("AppCenter", "removeGroup(" + str + ")");
        a remove = this.e.remove(str);
        if (remove != null) {
            d(remove);
            com.microsoft.appcenter.utils.b.b.a().f4722a.remove(remove);
        }
        for (b.C0155b a2 : this.f) {
            a2.a(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.a.c.a(boolean, java.lang.Exception):void
     arg types: [int, com.microsoft.appcenter.CancellationException]
     candidates:
      com.microsoft.appcenter.a.c.a(com.microsoft.appcenter.a.c$a, java.lang.String):void
      com.microsoft.appcenter.a.c.a(com.microsoft.appcenter.a.c$a, int):boolean
      com.microsoft.appcenter.a.c.a(boolean, java.lang.Exception):void */
    public final synchronized void a(boolean z) {
        if (this.j != z) {
            if (z) {
                this.j = true;
                this.k = false;
                this.m++;
                for (com.microsoft.appcenter.b.b a2 : this.i) {
                    a2.a();
                }
                for (a b2 : this.e.values()) {
                    b(b2);
                }
            } else {
                a(true, (Exception) new CancellationException());
            }
            for (b.C0155b a3 : this.f) {
                a3.a(z);
            }
        }
    }

    public final synchronized void c(String str) {
        this.h.a(str);
    }

    public final synchronized void d(String str) {
        if (this.e.containsKey(str)) {
            com.microsoft.appcenter.utils.a.b("AppCenter", "clear(" + str + ")");
            this.g.a(str);
            for (b.C0155b b2 : this.f) {
                b2.b(str);
            }
        }
    }

    private void a(boolean z, Exception exc) {
        b.a aVar;
        this.j = false;
        this.k = z;
        this.m++;
        for (a next : this.e.values()) {
            d(next);
            Iterator<Map.Entry<String, List<d>>> it = next.e.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry next2 = it.next();
                it.remove();
                if (z && (aVar = next.g) != null) {
                    for (d a2 : (List) next2.getValue()) {
                        aVar.a(a2, exc);
                    }
                }
            }
        }
        for (com.microsoft.appcenter.b.b next3 : this.i) {
            try {
                next3.close();
            } catch (IOException e2) {
                com.microsoft.appcenter.utils.a.c("AppCenter", "Failed to close ingestion: " + next3, e2);
            }
        }
        if (z) {
            for (a c2 : this.e.values()) {
                c(c2);
            }
            return;
        }
        this.g.a();
    }

    private void c(a aVar) {
        ArrayList<d> arrayList = new ArrayList<>();
        this.g.a(aVar.f4533a, Collections.emptyList(), 100, arrayList, null, null);
        if (arrayList.size() > 0 && aVar.g != null) {
            for (d dVar : arrayList) {
                aVar.g.a(dVar);
                aVar.g.a(dVar, new CancellationException());
            }
        }
        if (arrayList.size() < 100 || aVar.g == null) {
            this.g.a(aVar.f4533a);
        } else {
            c(aVar);
        }
    }

    private void d(a aVar) {
        if (aVar.i) {
            aVar.i = false;
            this.f4531a.removeCallbacks(aVar.l);
            com.microsoft.appcenter.utils.d.d.a("startTimerPrefix." + aVar.f4533a);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(a aVar) {
        Date date;
        String str;
        a aVar2 = aVar;
        synchronized (this) {
            if (this.j) {
                int i2 = aVar2.h;
                int min = Math.min(i2, aVar2.f4534b);
                com.microsoft.appcenter.utils.a.b("AppCenter", "triggerIngestion(" + aVar2.f4533a + ") pendingLogCount=" + i2);
                d(aVar);
                if (aVar2.e.size() == aVar2.d) {
                    com.microsoft.appcenter.utils.a.b("AppCenter", "Already sending " + aVar2.d + " batches of analytics data to the server.");
                    return;
                }
                com.microsoft.appcenter.utils.b.b a2 = com.microsoft.appcenter.utils.b.b.a();
                ListIterator<com.microsoft.appcenter.utils.b.d> listIterator = a2.c().listIterator();
                while (listIterator.hasNext()) {
                    com.microsoft.appcenter.utils.b.d next = listIterator.next();
                    Date date2 = null;
                    if (next != null) {
                        String str2 = next.f4725a;
                        Date date3 = next.f4726b;
                        Date date4 = next.c;
                        a2.a(next);
                        date = date3;
                        Date date5 = date4;
                        str = str2;
                        date2 = date5;
                    } else {
                        str = null;
                        date = null;
                    }
                    ArrayList<d> arrayList = new ArrayList<>(min);
                    int i3 = this.m;
                    String a3 = this.g.a(aVar2.f4533a, aVar2.k, min, arrayList, date, date2);
                    aVar2.h -= arrayList.size();
                    if (a3 != null) {
                        com.microsoft.appcenter.utils.a.b("AppCenter", "ingestLogs(" + aVar2.f4533a + "," + a3 + ") pendingLogCount=" + aVar2.h);
                        if (aVar2.g != null) {
                            for (d a4 : arrayList) {
                                aVar2.g.a(a4);
                            }
                        }
                        aVar2.e.put(a3, arrayList);
                        com.microsoft.appcenter.utils.b.a(new d(this, aVar, i3, arrayList, a3, str));
                        return;
                    } else if (listIterator.previousIndex() == 0 && date2 != null && this.g.a(date2) == 0) {
                        a2.a(str);
                    }
                }
                aVar2.h = this.g.b(aVar2.f4533a);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(a aVar, int i2, List<d> list, String str, String str2) {
        if (a(aVar, i2)) {
            e eVar = new e();
            eVar.f4613a = list;
            aVar.f.a(str2, this.c, this.d, eVar, new e(this, aVar, str));
            this.f4531a.post(new h(this, aVar, i2));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(a aVar, String str) {
        List<d> remove = aVar.e.remove(str);
        if (remove != null) {
            this.g.a(aVar.f4533a, str);
            b.a aVar2 = aVar.g;
            if (aVar2 != null) {
                for (d b2 : remove) {
                    aVar2.b(b2);
                }
            }
            b(aVar);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(a aVar, String str, Exception exc) {
        String str2 = aVar.f4533a;
        List<d> remove = aVar.e.remove(str);
        if (remove != null) {
            com.microsoft.appcenter.utils.a.c("AppCenter", "Sending logs groupName=" + str2 + " id=" + str + " failed", exc);
            boolean a2 = k.a(exc);
            if (a2) {
                aVar.h += remove.size();
            } else {
                b.a aVar2 = aVar.g;
                if (aVar2 != null) {
                    for (d a3 : remove) {
                        aVar2.a(a3, exc);
                    }
                }
            }
            a(!a2, exc);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0187, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.microsoft.appcenter.b.a.d r7, java.lang.String r8, int r9) {
        /*
            r6 = this;
            monitor-enter(r6)
            java.util.Map<java.lang.String, com.microsoft.appcenter.a.c$a> r0 = r6.e     // Catch:{ all -> 0x01a0 }
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.a.c$a r0 = (com.microsoft.appcenter.a.c.a) r0     // Catch:{ all -> 0x01a0 }
            if (r0 != 0) goto L_0x0023
            java.lang.String r7 = "AppCenter"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x01a0 }
            r9.<init>()     // Catch:{ all -> 0x01a0 }
            java.lang.String r0 = "Invalid group name:"
            r9.append(r0)     // Catch:{ all -> 0x01a0 }
            r9.append(r8)     // Catch:{ all -> 0x01a0 }
            java.lang.String r8 = r9.toString()     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.utils.a.e(r7, r8)     // Catch:{ all -> 0x01a0 }
            monitor-exit(r6)
            return
        L_0x0023:
            boolean r1 = r6.k     // Catch:{ all -> 0x01a0 }
            if (r1 == 0) goto L_0x0043
            java.lang.String r8 = "AppCenter"
            java.lang.String r9 = "Channel is disabled, the log is discarded."
            com.microsoft.appcenter.utils.a.d(r8, r9)     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.a.b$a r8 = r0.g     // Catch:{ all -> 0x01a0 }
            if (r8 == 0) goto L_0x0041
            com.microsoft.appcenter.a.b$a r8 = r0.g     // Catch:{ all -> 0x01a0 }
            r8.a(r7)     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.a.b$a r8 = r0.g     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.CancellationException r9 = new com.microsoft.appcenter.CancellationException     // Catch:{ all -> 0x01a0 }
            r9.<init>()     // Catch:{ all -> 0x01a0 }
            r8.a(r7, r9)     // Catch:{ all -> 0x01a0 }
        L_0x0041:
            monitor-exit(r6)
            return
        L_0x0043:
            java.util.Collection<com.microsoft.appcenter.a.b$b> r1 = r6.f     // Catch:{ all -> 0x01a0 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x01a0 }
        L_0x0049:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x01a0 }
            if (r2 == 0) goto L_0x0059
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.a.b$b r2 = (com.microsoft.appcenter.a.b.C0155b) r2     // Catch:{ all -> 0x01a0 }
            r2.a(r7)     // Catch:{ all -> 0x01a0 }
            goto L_0x0049
        L_0x0059:
            com.microsoft.appcenter.b.a.c r1 = r7.e()     // Catch:{ all -> 0x01a0 }
            if (r1 != 0) goto L_0x007b
            com.microsoft.appcenter.b.a.c r1 = r6.l     // Catch:{ all -> 0x01a0 }
            if (r1 != 0) goto L_0x0076
            android.content.Context r1 = r6.f4532b     // Catch:{ DeviceInfoException -> 0x006c }
            com.microsoft.appcenter.b.a.c r1 = com.microsoft.appcenter.utils.DeviceInfoHelper.a(r1)     // Catch:{ DeviceInfoException -> 0x006c }
            r6.l = r1     // Catch:{ DeviceInfoException -> 0x006c }
            goto L_0x0076
        L_0x006c:
            r7 = move-exception
            java.lang.String r8 = "AppCenter"
            java.lang.String r9 = "Device log cannot be generated"
            com.microsoft.appcenter.utils.a.c(r8, r9, r7)     // Catch:{ all -> 0x01a0 }
            monitor-exit(r6)
            return
        L_0x0076:
            com.microsoft.appcenter.b.a.c r1 = r6.l     // Catch:{ all -> 0x01a0 }
            r7.a(r1)     // Catch:{ all -> 0x01a0 }
        L_0x007b:
            java.util.Date r1 = r7.b()     // Catch:{ all -> 0x01a0 }
            if (r1 != 0) goto L_0x0089
            java.util.Date r1 = new java.util.Date     // Catch:{ all -> 0x01a0 }
            r1.<init>()     // Catch:{ all -> 0x01a0 }
            r7.a(r1)     // Catch:{ all -> 0x01a0 }
        L_0x0089:
            java.util.Collection<com.microsoft.appcenter.a.b$b> r1 = r6.f     // Catch:{ all -> 0x01a0 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x01a0 }
        L_0x008f:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x01a0 }
            if (r2 == 0) goto L_0x009f
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.a.b$b r2 = (com.microsoft.appcenter.a.b.C0155b) r2     // Catch:{ all -> 0x01a0 }
            r2.a(r7, r8, r9)     // Catch:{ all -> 0x01a0 }
            goto L_0x008f
        L_0x009f:
            java.util.Collection<com.microsoft.appcenter.a.b$b> r1 = r6.f     // Catch:{ all -> 0x01a0 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x01a0 }
            r2 = 0
        L_0x00a6:
            r3 = 0
        L_0x00a7:
            boolean r4 = r1.hasNext()     // Catch:{ all -> 0x01a0 }
            r5 = 1
            if (r4 == 0) goto L_0x00be
            java.lang.Object r4 = r1.next()     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.a.b$b r4 = (com.microsoft.appcenter.a.b.C0155b) r4     // Catch:{ all -> 0x01a0 }
            if (r3 != 0) goto L_0x00bc
            boolean r3 = r4.b(r7)     // Catch:{ all -> 0x01a0 }
            if (r3 == 0) goto L_0x00a6
        L_0x00bc:
            r3 = 1
            goto L_0x00a7
        L_0x00be:
            if (r3 == 0) goto L_0x00e1
            java.lang.String r8 = "AppCenter"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x01a0 }
            r9.<init>()     // Catch:{ all -> 0x01a0 }
            java.lang.String r0 = "Log of type '"
            r9.append(r0)     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = r7.a()     // Catch:{ all -> 0x01a0 }
            r9.append(r7)     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = "' was filtered out by listener(s)"
            r9.append(r7)     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = r9.toString()     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.utils.a.b(r8, r7)     // Catch:{ all -> 0x01a0 }
            goto L_0x0186
        L_0x00e1:
            java.lang.String r1 = r6.c     // Catch:{ all -> 0x01a0 }
            if (r1 != 0) goto L_0x010c
            com.microsoft.appcenter.b.b r1 = r0.f     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.b.b r2 = r6.h     // Catch:{ all -> 0x01a0 }
            if (r1 != r2) goto L_0x010c
            java.lang.String r8 = "AppCenter"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x01a0 }
            r9.<init>()     // Catch:{ all -> 0x01a0 }
            java.lang.String r0 = "Log of type '"
            r9.append(r0)     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = r7.a()     // Catch:{ all -> 0x01a0 }
            r9.append(r7)     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = "' was not filtered out by listener(s) but no app secret was provided. Not persisting/sending the log."
            r9.append(r7)     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = r9.toString()     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.utils.a.b(r8, r7)     // Catch:{ all -> 0x01a0 }
            monitor-exit(r6)
            return
        L_0x010c:
            com.microsoft.appcenter.persistence.Persistence r1 = r6.g     // Catch:{ PersistenceException -> 0x0188 }
            r1.a(r7, r8, r9)     // Catch:{ PersistenceException -> 0x0188 }
            java.util.Set r7 = r7.g()     // Catch:{ all -> 0x01a0 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x01a0 }
            boolean r8 = r7.hasNext()     // Catch:{ all -> 0x01a0 }
            if (r8 == 0) goto L_0x012a
            java.lang.Object r7 = r7.next()     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = com.microsoft.appcenter.b.a.b.k.a(r7)     // Catch:{ all -> 0x01a0 }
            goto L_0x012b
        L_0x012a:
            r7 = 0
        L_0x012b:
            java.util.Collection<java.lang.String> r8 = r0.k     // Catch:{ all -> 0x01a0 }
            boolean r8 = r8.contains(r7)     // Catch:{ all -> 0x01a0 }
            if (r8 == 0) goto L_0x0150
            java.lang.String r8 = "AppCenter"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x01a0 }
            r9.<init>()     // Catch:{ all -> 0x01a0 }
            java.lang.String r0 = "Transmission target ikey="
            r9.append(r0)     // Catch:{ all -> 0x01a0 }
            r9.append(r7)     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = " is paused."
            r9.append(r7)     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = r9.toString()     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.utils.a.b(r8, r7)     // Catch:{ all -> 0x01a0 }
            monitor-exit(r6)
            return
        L_0x0150:
            int r7 = r0.h     // Catch:{ all -> 0x01a0 }
            int r7 = r7 + r5
            r0.h = r7     // Catch:{ all -> 0x01a0 }
            java.lang.String r7 = "AppCenter"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x01a0 }
            r8.<init>()     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = "enqueue("
            r8.append(r9)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = r0.f4533a     // Catch:{ all -> 0x01a0 }
            r8.append(r9)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = ") pendingLogCount="
            r8.append(r9)     // Catch:{ all -> 0x01a0 }
            int r9 = r0.h     // Catch:{ all -> 0x01a0 }
            r8.append(r9)     // Catch:{ all -> 0x01a0 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.utils.a.b(r7, r8)     // Catch:{ all -> 0x01a0 }
            boolean r7 = r6.j     // Catch:{ all -> 0x01a0 }
            if (r7 == 0) goto L_0x017f
            r6.b(r0)     // Catch:{ all -> 0x01a0 }
            goto L_0x0186
        L_0x017f:
            java.lang.String r7 = "AppCenter"
            java.lang.String r8 = "Channel is temporarily disabled, log was saved to disk."
            com.microsoft.appcenter.utils.a.b(r7, r8)     // Catch:{ all -> 0x01a0 }
        L_0x0186:
            monitor-exit(r6)
            return
        L_0x0188:
            r8 = move-exception
            java.lang.String r9 = "AppCenter"
            java.lang.String r1 = "Error persisting log"
            com.microsoft.appcenter.utils.a.c(r9, r1, r8)     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.a.b$a r9 = r0.g     // Catch:{ all -> 0x01a0 }
            if (r9 == 0) goto L_0x019e
            com.microsoft.appcenter.a.b$a r9 = r0.g     // Catch:{ all -> 0x01a0 }
            r9.a(r7)     // Catch:{ all -> 0x01a0 }
            com.microsoft.appcenter.a.b$a r9 = r0.g     // Catch:{ all -> 0x01a0 }
            r9.a(r7, r8)     // Catch:{ all -> 0x01a0 }
        L_0x019e:
            monitor-exit(r6)
            return
        L_0x01a0:
            r7 = move-exception
            monitor-exit(r6)
            goto L_0x01a4
        L_0x01a3:
            throw r7
        L_0x01a4:
            goto L_0x01a3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.a.c.a(com.microsoft.appcenter.b.a.d, java.lang.String, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0122, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0124, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(com.microsoft.appcenter.a.c.a r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            java.lang.String r0 = "AppCenter"
            java.lang.String r1 = "checkPendingLogs(%s) pendingLogCount=%s batchTimeInterval=%s"
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0125 }
            r3 = 0
            java.lang.String r4 = r11.f4533a     // Catch:{ all -> 0x0125 }
            r2[r3] = r4     // Catch:{ all -> 0x0125 }
            int r3 = r11.h     // Catch:{ all -> 0x0125 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0125 }
            r4 = 1
            r2[r4] = r3     // Catch:{ all -> 0x0125 }
            r3 = 2
            long r5 = r11.c     // Catch:{ all -> 0x0125 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x0125 }
            r2[r3] = r5     // Catch:{ all -> 0x0125 }
            java.lang.String r1 = java.lang.String.format(r1, r2)     // Catch:{ all -> 0x0125 }
            com.microsoft.appcenter.utils.a.b(r0, r1)     // Catch:{ all -> 0x0125 }
            long r0 = r11.c     // Catch:{ all -> 0x0125 }
            r2 = 3000(0xbb8, double:1.482E-320)
            r5 = 0
            r6 = 0
            int r8 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r8 <= 0) goto L_0x00e8
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0125 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            r2.<init>()     // Catch:{ all -> 0x0125 }
            java.lang.String r3 = "startTimerPrefix."
            r2.append(r3)     // Catch:{ all -> 0x0125 }
            java.lang.String r3 = r11.f4533a     // Catch:{ all -> 0x0125 }
            r2.append(r3)     // Catch:{ all -> 0x0125 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0125 }
            android.content.SharedPreferences r3 = com.microsoft.appcenter.utils.d.d.f4749a     // Catch:{ all -> 0x0125 }
            long r2 = r3.getLong(r2, r6)     // Catch:{ all -> 0x0125 }
            int r8 = r11.h     // Catch:{ all -> 0x0125 }
            if (r8 <= 0) goto L_0x00ad
            int r5 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r5 == 0) goto L_0x006a
            int r5 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r5 <= 0) goto L_0x005b
            goto L_0x006a
        L_0x005b:
            long r8 = r11.c     // Catch:{ all -> 0x0125 }
            long r0 = r0 - r2
            long r8 = r8 - r0
            long r0 = java.lang.Math.max(r8, r6)     // Catch:{ all -> 0x0125 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0125 }
        L_0x0067:
            r5 = r0
            goto L_0x00fd
        L_0x006a:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            r2.<init>()     // Catch:{ all -> 0x0125 }
            java.lang.String r3 = "startTimerPrefix."
            r2.append(r3)     // Catch:{ all -> 0x0125 }
            java.lang.String r3 = r11.f4533a     // Catch:{ all -> 0x0125 }
            r2.append(r3)     // Catch:{ all -> 0x0125 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0125 }
            android.content.SharedPreferences r3 = com.microsoft.appcenter.utils.d.d.f4749a     // Catch:{ all -> 0x0125 }
            android.content.SharedPreferences$Editor r3 = r3.edit()     // Catch:{ all -> 0x0125 }
            r3.putLong(r2, r0)     // Catch:{ all -> 0x0125 }
            r3.apply()     // Catch:{ all -> 0x0125 }
            java.lang.String r0 = "AppCenter"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            r1.<init>()     // Catch:{ all -> 0x0125 }
            java.lang.String r2 = "The timer value for "
            r1.append(r2)     // Catch:{ all -> 0x0125 }
            java.lang.String r2 = r11.f4533a     // Catch:{ all -> 0x0125 }
            r1.append(r2)     // Catch:{ all -> 0x0125 }
            java.lang.String r2 = " has been saved."
            r1.append(r2)     // Catch:{ all -> 0x0125 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0125 }
            com.microsoft.appcenter.utils.a.b(r0, r1)     // Catch:{ all -> 0x0125 }
            long r0 = r11.c     // Catch:{ all -> 0x0125 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0125 }
            goto L_0x0067
        L_0x00ad:
            long r8 = r11.c     // Catch:{ all -> 0x0125 }
            long r2 = r2 + r8
            int r8 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r8 >= 0) goto L_0x00fd
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            r0.<init>()     // Catch:{ all -> 0x0125 }
            java.lang.String r1 = "startTimerPrefix."
            r0.append(r1)     // Catch:{ all -> 0x0125 }
            java.lang.String r1 = r11.f4533a     // Catch:{ all -> 0x0125 }
            r0.append(r1)     // Catch:{ all -> 0x0125 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0125 }
            com.microsoft.appcenter.utils.d.d.a(r0)     // Catch:{ all -> 0x0125 }
            java.lang.String r0 = "AppCenter"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            r1.<init>()     // Catch:{ all -> 0x0125 }
            java.lang.String r2 = "The timer for "
            r1.append(r2)     // Catch:{ all -> 0x0125 }
            java.lang.String r2 = r11.f4533a     // Catch:{ all -> 0x0125 }
            r1.append(r2)     // Catch:{ all -> 0x0125 }
            java.lang.String r2 = " channel finished."
            r1.append(r2)     // Catch:{ all -> 0x0125 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0125 }
            com.microsoft.appcenter.utils.a.b(r0, r1)     // Catch:{ all -> 0x0125 }
            goto L_0x00fd
        L_0x00e8:
            int r0 = r11.h     // Catch:{ all -> 0x0125 }
            int r1 = r11.f4534b     // Catch:{ all -> 0x0125 }
            if (r0 < r1) goto L_0x00f3
            java.lang.Long r5 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0125 }
            goto L_0x00fd
        L_0x00f3:
            int r0 = r11.h     // Catch:{ all -> 0x0125 }
            if (r0 <= 0) goto L_0x00fd
            long r0 = r11.c     // Catch:{ all -> 0x0125 }
            java.lang.Long r5 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0125 }
        L_0x00fd:
            if (r5 == 0) goto L_0x0123
            boolean r0 = r11.j     // Catch:{ all -> 0x0125 }
            if (r0 == 0) goto L_0x0104
            goto L_0x0123
        L_0x0104:
            long r0 = r5.longValue()     // Catch:{ all -> 0x0125 }
            int r2 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r2 != 0) goto L_0x0110
            r10.a(r11)     // Catch:{ all -> 0x0125 }
            goto L_0x0121
        L_0x0110:
            boolean r0 = r11.i     // Catch:{ all -> 0x0125 }
            if (r0 != 0) goto L_0x0121
            r11.i = r4     // Catch:{ all -> 0x0125 }
            android.os.Handler r0 = r10.f4531a     // Catch:{ all -> 0x0125 }
            java.lang.Runnable r11 = r11.l     // Catch:{ all -> 0x0125 }
            long r1 = r5.longValue()     // Catch:{ all -> 0x0125 }
            r0.postDelayed(r11, r1)     // Catch:{ all -> 0x0125 }
        L_0x0121:
            monitor-exit(r10)
            return
        L_0x0123:
            monitor-exit(r10)
            return
        L_0x0125:
            r11 = move-exception
            monitor-exit(r10)
            goto L_0x0129
        L_0x0128:
            throw r11
        L_0x0129:
            goto L_0x0128
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.a.c.b(com.microsoft.appcenter.a.c$a):void");
    }

    public final synchronized void a(b.C0155b bVar) {
        this.f.add(bVar);
    }

    public final synchronized void b(b.C0155b bVar) {
        this.f.remove(bVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.a.c.a(boolean, java.lang.Exception):void
     arg types: [int, com.microsoft.appcenter.CancellationException]
     candidates:
      com.microsoft.appcenter.a.c.a(com.microsoft.appcenter.a.c$a, java.lang.String):void
      com.microsoft.appcenter.a.c.a(com.microsoft.appcenter.a.c$a, int):boolean
      com.microsoft.appcenter.a.c.a(boolean, java.lang.Exception):void */
    public final synchronized void a() {
        a(false, (Exception) new CancellationException());
    }

    /* compiled from: DefaultChannel */
    class a extends com.microsoft.appcenter.utils.b.a {

        /* renamed from: a  reason: collision with root package name */
        final String f4533a;

        /* renamed from: b  reason: collision with root package name */
        final int f4534b;
        final long c;
        final int d;
        final Map<String, List<d>> e = new HashMap();
        final com.microsoft.appcenter.b.b f;
        final b.a g;
        int h;
        boolean i;
        boolean j;
        final Collection<String> k = new HashSet();
        final Runnable l = new i(this);

        a(String str, int i2, long j2, int i3, com.microsoft.appcenter.b.b bVar, b.a aVar) {
            this.f4533a = str;
            this.f4534b = i2;
            this.c = j2;
            this.d = i3;
            this.f = bVar;
            this.g = aVar;
        }

        public final void a() {
            c.this.b(this);
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public c(android.content.Context r7, java.lang.String r8, com.microsoft.appcenter.b.a.a.h r9, android.os.Handler r10) {
        /*
            r6 = this;
            com.microsoft.appcenter.persistence.a r3 = new com.microsoft.appcenter.persistence.a
            r3.<init>(r7)
            r3.e = r9
            com.microsoft.appcenter.b.a r4 = new com.microsoft.appcenter.b.a
            r4.<init>(r7, r9)
            r0 = r6
            r1 = r7
            r2 = r8
            r5 = r10
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.a.c.<init>(android.content.Context, java.lang.String, com.microsoft.appcenter.b.a.a.h, android.os.Handler):void");
    }
}
