package com.microsoft.appcenter.a;

import com.microsoft.appcenter.a.c;
import com.microsoft.appcenter.http.m;
import java.util.Map;

/* compiled from: DefaultChannel */
class e implements m {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c.a f4537a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f4538b;
    final /* synthetic */ c c;

    e(c cVar, c.a aVar, String str) {
        this.c = cVar;
        this.f4537a = aVar;
        this.f4538b = str;
    }

    public final void a(String str, Map<String, String> map) {
        this.c.f4531a.post(new f(this));
    }

    public final void a(Exception exc) {
        this.c.f4531a.post(new g(this, exc));
    }
}
