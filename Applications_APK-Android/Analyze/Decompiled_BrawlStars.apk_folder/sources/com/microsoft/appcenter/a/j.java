package com.microsoft.appcenter.a;

import android.content.Context;
import com.microsoft.appcenter.a.b;
import com.microsoft.appcenter.b.a.a.h;
import com.microsoft.appcenter.b.a.b.m;
import com.microsoft.appcenter.b.a.d;
import com.microsoft.appcenter.b.b;
import com.microsoft.appcenter.b.c;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/* compiled from: OneCollectorChannelListener */
public class j extends a {

    /* renamed from: a  reason: collision with root package name */
    public final b f4545a;

    /* renamed from: b  reason: collision with root package name */
    private final b f4546b;
    private final h c;
    private final UUID d;
    private final Map<String, a> e;

    public j(Context context, b bVar, h hVar, UUID uuid) {
        this(new c(context, hVar), bVar, hVar, uuid);
    }

    private j(c cVar, b bVar, h hVar, UUID uuid) {
        this.e = new HashMap();
        this.f4546b = bVar;
        this.c = hVar;
        this.d = uuid;
        this.f4545a = cVar;
    }

    public final void a(d dVar, String str, int i) {
        if (c(dVar)) {
            try {
                Collection<com.microsoft.appcenter.b.a.b.c> b2 = this.c.b(dVar);
                for (com.microsoft.appcenter.b.a.b.c next : b2) {
                    next.d = Long.valueOf((long) i);
                    a aVar = this.e.get(next.c);
                    if (aVar == null) {
                        aVar = new a(UUID.randomUUID().toString());
                        this.e.put(next.c, aVar);
                    }
                    m mVar = next.e.h;
                    mVar.f4602b = aVar.f4547a;
                    long j = aVar.f4548b + 1;
                    aVar.f4548b = j;
                    mVar.c = Long.valueOf(j);
                    mVar.d = this.d;
                }
                String c2 = c(str);
                for (com.microsoft.appcenter.b.a.b.c a2 : b2) {
                    this.f4546b.a(a2, c2, i);
                }
            } catch (IllegalArgumentException e2) {
                com.microsoft.appcenter.utils.a.e("AppCenter", "Cannot send a log to one collector: " + e2.getMessage());
            }
        }
    }

    public final boolean b(d dVar) {
        return c(dVar);
    }

    private static String c(String str) {
        return str + "/one";
    }

    private static boolean c(d dVar) {
        return !(dVar instanceof com.microsoft.appcenter.b.a.b.c) && !dVar.g().isEmpty();
    }

    public final void a(boolean z) {
        if (!z) {
            this.e.clear();
        }
    }

    /* compiled from: OneCollectorChannelListener */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        final String f4547a;

        /* renamed from: b  reason: collision with root package name */
        long f4548b;

        a(String str) {
            this.f4547a = str;
        }
    }

    public final void a(String str, b.a aVar, long j) {
        if (!str.endsWith("/one")) {
            this.f4546b.a(c(str), 50, j, 2, this.f4545a, aVar);
        }
    }

    public final void a(String str) {
        if (!str.endsWith("/one")) {
            this.f4546b.b(c(str));
        }
    }

    public final void b(String str) {
        if (!str.endsWith("/one")) {
            this.f4546b.d(c(str));
        }
    }
}
