package com.microsoft.appcenter.a;

import com.microsoft.appcenter.a.c;
import java.util.List;

/* compiled from: DefaultChannel */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c.a f4535a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ int f4536b;
    final /* synthetic */ List c;
    final /* synthetic */ String d;
    final /* synthetic */ String e;
    final /* synthetic */ c f;

    d(c cVar, c.a aVar, int i, List list, String str, String str2) {
        this.f = cVar;
        this.f4535a = aVar;
        this.f4536b = i;
        this.c = list;
        this.d = str;
        this.e = str2;
    }

    public void run() {
        this.f.a(this.f4535a, this.f4536b, this.c, this.d, this.e);
    }
}
