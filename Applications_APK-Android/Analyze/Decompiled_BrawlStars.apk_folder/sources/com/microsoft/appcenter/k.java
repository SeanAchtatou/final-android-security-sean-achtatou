package com.microsoft.appcenter;

import com.microsoft.appcenter.utils.a;
import com.microsoft.appcenter.utils.d.d;

/* compiled from: AppCenter */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Runnable f4697a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Runnable f4698b;
    final /* synthetic */ f c;

    k(f fVar, Runnable runnable, Runnable runnable2) {
        this.c = fVar;
        this.f4697a = runnable;
        this.f4698b = runnable2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, java.lang.String):java.lang.String
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean */
    public void run() {
        if (d.a("enabled", true)) {
            this.f4697a.run();
            return;
        }
        Runnable runnable = this.f4698b;
        if (runnable != null) {
            runnable.run();
        } else {
            a.e("AppCenter", "App Center SDK is disabled.");
        }
    }
}
