package com.microsoft.appcenter.http;

import java.util.concurrent.RejectedExecutionException;

/* compiled from: DefaultHttpClient */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f4680a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ RejectedExecutionException f4681b;
    final /* synthetic */ b c;

    c(b bVar, m mVar, RejectedExecutionException rejectedExecutionException) {
        this.c = bVar;
        this.f4680a = mVar;
        this.f4681b = rejectedExecutionException;
    }

    public void run() {
        this.f4680a.a(this.f4681b);
    }
}
