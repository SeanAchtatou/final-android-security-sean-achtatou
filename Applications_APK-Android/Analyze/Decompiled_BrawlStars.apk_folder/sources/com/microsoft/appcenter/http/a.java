package com.microsoft.appcenter.http;

import com.microsoft.appcenter.http.f;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/* compiled from: AbstractAppCallTemplate */
public abstract class a implements f.a {
    public final void a(URL url, Map<String, String> map) {
        if (com.microsoft.appcenter.utils.a.a() <= 2) {
            com.microsoft.appcenter.utils.a.a("AppCenter", "Calling " + url + "...");
            HashMap hashMap = new HashMap(map);
            String str = (String) hashMap.get("App-Secret");
            if (str != null) {
                hashMap.put("App-Secret", k.a(str));
            }
            String str2 = (String) hashMap.get("Authorization");
            if (str2 != null) {
                hashMap.put("Authorization", k.d(str2));
            }
            com.microsoft.appcenter.utils.a.a("AppCenter", "Headers: " + hashMap);
        }
    }
}
