package com.microsoft.appcenter.http;

import android.content.Context;
import com.microsoft.appcenter.utils.e;
import java.io.EOFException;
import java.io.InterruptedIOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.RejectedExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.SSLException;

/* compiled from: HttpUtils */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private static final Class[] f4691a = {EOFException.class, InterruptedIOException.class, SocketException.class, UnknownHostException.class, RejectedExecutionException.class};

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f4692b = Pattern.compile("connection (time|reset|abort)|failure in ssl library, usually a protocol error|anchor for certification path not found");
    private static final Pattern c = Pattern.compile(":[^\"]+");
    private static final Pattern d = Pattern.compile("-[^,]+(,|$)");

    public static boolean a(Throwable th) {
        String message;
        if (th instanceof HttpException) {
            int i = ((HttpException) th).f4676a;
            if (i >= 500 || i == 408 || i == 429) {
                return true;
            }
            return false;
        }
        for (Class isAssignableFrom : f4691a) {
            if (isAssignableFrom.isAssignableFrom(th.getClass())) {
                return true;
            }
        }
        Throwable cause = th.getCause();
        if (cause != null) {
            for (Class isAssignableFrom2 : f4691a) {
                if (isAssignableFrom2.isAssignableFrom(cause.getClass())) {
                    return true;
                }
            }
        }
        return (th instanceof SSLException) && (message = th.getMessage()) != null && f4692b.matcher(message.toLowerCase(Locale.US)).find();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(char[], char):void}
     arg types: [char[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(char[], char):void} */
    public static String a(String str) {
        int length = str.length();
        int i = 8;
        if (str.length() < 8) {
            i = 0;
        }
        int i2 = length - i;
        char[] cArr = new char[i2];
        Arrays.fill(cArr, '*');
        return new String(cArr) + str.substring(i2);
    }

    public static String b(String str) {
        StringBuilder sb = new StringBuilder();
        Matcher matcher = d.matcher(str);
        int i = 0;
        while (matcher.find()) {
            sb.append(str.substring(i, matcher.start()));
            sb.append("-***");
            sb.append(matcher.group(1));
            i = matcher.end();
        }
        if (i < str.length()) {
            sb.append(str.substring(i));
        }
        return sb.toString();
    }

    public static String c(String str) {
        return c.matcher(str).replaceAll(":***");
    }

    static String d(String str) {
        String str2 = str.split("\\s+")[0];
        return str2 + " ***";
    }

    public static f a(Context context) {
        return new j(new i(new b(true), e.a(context)));
    }
}
