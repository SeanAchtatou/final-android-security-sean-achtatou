package com.microsoft.appcenter.http;

import java.io.IOException;
import java.util.Map;

public class HttpException extends IOException {

    /* renamed from: a  reason: collision with root package name */
    final int f4676a;

    /* renamed from: b  reason: collision with root package name */
    final Map<String, String> f4677b;
    private final String c;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        HttpException httpException = (HttpException) obj;
        if (this.f4676a != httpException.f4676a || !this.c.equals(httpException.c) || !this.f4677b.equals(httpException.f4677b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((this.f4676a * 31) + this.c.hashCode()) * 31) + this.f4677b.hashCode();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HttpException(int r3, java.lang.String r4, java.util.Map<java.lang.String, java.lang.String> r5) {
        /*
            r2 = this;
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 == 0) goto L_0x000b
            java.lang.String r0 = java.lang.String.valueOf(r3)
            goto L_0x001f
        L_0x000b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
            java.lang.String r1 = " - "
            r0.append(r1)
            r0.append(r4)
            java.lang.String r0 = r0.toString()
        L_0x001f:
            r2.<init>(r0)
            r2.c = r4
            r2.f4676a = r3
            r2.f4677b = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.http.HttpException.<init>(int, java.lang.String, java.util.Map):void");
    }
}
