package com.microsoft.appcenter.http;

import com.microsoft.appcenter.http.f;
import com.microsoft.appcenter.utils.e;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* compiled from: HttpClientNetworkStateHandler */
public class i extends h implements e.b {

    /* renamed from: b  reason: collision with root package name */
    private final e f4689b;
    private final Set<a> c = new HashSet();

    public i(f fVar, e eVar) {
        super(fVar);
        this.f4689b = eVar;
        this.f4689b.a(this);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.microsoft.appcenter.http.l a(java.lang.String r10, java.lang.String r11, java.util.Map<java.lang.String, java.lang.String> r12, com.microsoft.appcenter.http.f.a r13, com.microsoft.appcenter.http.m r14) {
        /*
            r9 = this;
            monitor-enter(r9)
            com.microsoft.appcenter.http.i$a r8 = new com.microsoft.appcenter.http.i$a     // Catch:{ all -> 0x0037 }
            com.microsoft.appcenter.http.f r2 = r9.f4688a     // Catch:{ all -> 0x0037 }
            r0 = r8
            r1 = r9
            r3 = r10
            r4 = r11
            r5 = r12
            r6 = r13
            r7 = r14
            r0.<init>(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0037 }
            com.microsoft.appcenter.utils.e r10 = r9.f4689b     // Catch:{ all -> 0x0037 }
            java.util.concurrent.atomic.AtomicBoolean r11 = r10.f4752b     // Catch:{ all -> 0x0037 }
            boolean r11 = r11.get()     // Catch:{ all -> 0x0037 }
            if (r11 != 0) goto L_0x0022
            boolean r10 = r10.a()     // Catch:{ all -> 0x0037 }
            if (r10 == 0) goto L_0x0020
            goto L_0x0022
        L_0x0020:
            r10 = 0
            goto L_0x0023
        L_0x0022:
            r10 = 1
        L_0x0023:
            if (r10 == 0) goto L_0x0029
            r8.run()     // Catch:{ all -> 0x0037 }
            goto L_0x0035
        L_0x0029:
            java.util.Set<com.microsoft.appcenter.http.i$a> r10 = r9.c     // Catch:{ all -> 0x0037 }
            r10.add(r8)     // Catch:{ all -> 0x0037 }
            java.lang.String r10 = "AppCenter"
            java.lang.String r11 = "Call triggered with no network connectivity, waiting network to become available..."
            com.microsoft.appcenter.utils.a.b(r10, r11)     // Catch:{ all -> 0x0037 }
        L_0x0035:
            monitor-exit(r9)
            return r8
        L_0x0037:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.http.i.a(java.lang.String, java.lang.String, java.util.Map, com.microsoft.appcenter.http.f$a, com.microsoft.appcenter.http.m):com.microsoft.appcenter.http.l");
    }

    public synchronized void close() throws IOException {
        this.f4689b.f4751a.remove(this);
        this.c.clear();
        super.close();
    }

    public final void a() {
        this.f4689b.a(this);
        super.a();
    }

    public final synchronized void a(boolean z) {
        if (z) {
            if (this.c.size() > 0) {
                com.microsoft.appcenter.utils.a.b("AppCenter", "Network is available. " + this.c.size() + " pending call(s) to submit now.");
                for (a run : this.c) {
                    run.run();
                }
                this.c.clear();
            }
        }
    }

    /* compiled from: HttpClientNetworkStateHandler */
    class a extends g {
        a(f fVar, String str, String str2, Map<String, String> map, f.a aVar, m mVar) {
            super(fVar, str, str2, map, aVar, mVar);
        }
    }
}
