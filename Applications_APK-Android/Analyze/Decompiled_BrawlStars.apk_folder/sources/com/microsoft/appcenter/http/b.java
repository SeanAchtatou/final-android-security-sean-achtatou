package com.microsoft.appcenter.http;

import android.os.AsyncTask;
import com.microsoft.appcenter.http.e;
import com.microsoft.appcenter.http.f;
import com.microsoft.appcenter.utils.a;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.RejectedExecutionException;

/* compiled from: DefaultHttpClient */
public class b implements e.a, f {

    /* renamed from: a  reason: collision with root package name */
    private final Set<e> f4678a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f4679b;

    public final void a() {
    }

    public b() {
        this(true);
    }

    public b(boolean z) {
        this.f4678a = new HashSet();
        this.f4679b = z;
    }

    public final l a(String str, String str2, Map<String, String> map, f.a aVar, m mVar) {
        e eVar = new e(str, str2, map, aVar, mVar, this, this.f4679b);
        try {
            eVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        } catch (RejectedExecutionException e) {
            com.microsoft.appcenter.utils.b.a(new c(this, mVar, e));
        }
        return new d(this, eVar);
    }

    public final synchronized void a(e eVar) {
        this.f4678a.add(eVar);
    }

    public final synchronized void b(e eVar) {
        this.f4678a.remove(eVar);
    }

    public synchronized void close() {
        if (this.f4678a.size() > 0) {
            a.b("AppCenter", "Cancelling " + this.f4678a.size() + " network call(s).");
            for (e cancel : this.f4678a) {
                cancel.cancel(true);
            }
            this.f4678a.clear();
        }
    }
}
