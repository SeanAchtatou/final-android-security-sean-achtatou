package com.microsoft.appcenter.http;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: TLS1_2SocketFactory */
class n extends SSLSocketFactory {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f4693a = {"TLSv1.2"};
    private final SSLSocketFactory delegate;

    n() {
        SSLSocketFactory sSLSocketFactory = null;
        try {
            SSLContext instance = SSLContext.getInstance("TLSv1.2");
            instance.init(null, null, null);
            sSLSocketFactory = instance.getSocketFactory();
        } catch (KeyManagementException | NoSuchAlgorithmException unused) {
        }
        this.delegate = sSLSocketFactory == null ? HttpsURLConnection.getDefaultSSLSocketFactory() : sSLSocketFactory;
    }

    private static SSLSocket a(Socket socket) {
        SSLSocket sSLSocket = (SSLSocket) socket;
        sSLSocket.setEnabledProtocols(f4693a);
        return sSLSocket;
    }

    public String[] getDefaultCipherSuites() {
        return this.delegate.getDefaultCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        return this.delegate.getSupportedCipherSuites();
    }

    public /* synthetic */ Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return a(this.delegate.createSocket(socket, str, i, z));
    }

    public /* synthetic */ Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        return a(this.delegate.createSocket(inetAddress, i, inetAddress2, i2));
    }

    public /* synthetic */ Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        return a(this.delegate.createSocket(inetAddress, i));
    }

    public /* synthetic */ Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException, UnknownHostException {
        return a(this.delegate.createSocket(str, i, inetAddress, i2));
    }

    public /* synthetic */ Socket createSocket(String str, int i) throws IOException, UnknownHostException {
        return a(this.delegate.createSocket(str, i));
    }

    public /* synthetic */ Socket createSocket() throws IOException {
        return a(this.delegate.createSocket());
    }
}
