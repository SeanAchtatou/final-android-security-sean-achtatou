package com.microsoft.appcenter.http;

import android.os.Handler;
import android.os.Looper;
import com.microsoft.appcenter.http.f;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/* compiled from: HttpClientRetryer */
public class j extends h {

    /* renamed from: b  reason: collision with root package name */
    static final long[] f4690b = {TimeUnit.SECONDS.toMillis(10), TimeUnit.MINUTES.toMillis(5), TimeUnit.MINUTES.toMillis(20)};
    /* access modifiers changed from: private */
    public final Handler c;
    /* access modifiers changed from: private */
    public final Random d;

    j(f fVar) {
        this(fVar, new Handler(Looper.getMainLooper()));
    }

    private j(f fVar, Handler handler) {
        super(fVar);
        this.d = new Random();
        this.c = handler;
    }

    public final l a(String str, String str2, Map<String, String> map, f.a aVar, m mVar) {
        a aVar2 = new a(this.f4688a, str, str2, map, aVar, mVar);
        aVar2.run();
        return aVar2;
    }

    /* compiled from: HttpClientRetryer */
    class a extends g {
        private int d;

        a(f fVar, String str, String str2, Map<String, String> map, f.a aVar, m mVar) {
            super(fVar, str, str2, map, aVar, mVar);
        }

        public final void a(Exception exc) {
            String str;
            if (this.d >= j.f4690b.length || !k.a(exc)) {
                this.f4686a.a(exc);
                return;
            }
            long parseLong = (!(exc instanceof HttpException) || (str = ((HttpException) exc).f4677b.get("x-ms-retry-after-ms")) == null) ? 0 : Long.parseLong(str);
            if (parseLong == 0) {
                long[] jArr = j.f4690b;
                int i = this.d;
                this.d = i + 1;
                long j = jArr[i] / 2;
                parseLong = ((long) j.this.d.nextInt((int) j)) + j;
            }
            String str2 = "Try #" + this.d + " failed and will be retried in " + parseLong + " ms";
            if (exc instanceof UnknownHostException) {
                str2 = str2 + " (UnknownHostException)";
            }
            com.microsoft.appcenter.utils.a.b("AppCenter", str2, exc);
            j.this.c.postDelayed(this, parseLong);
        }
    }
}
