package com.microsoft.appcenter.http;

import java.io.IOException;

/* compiled from: HttpClientDecorator */
public abstract class h implements f {

    /* renamed from: a  reason: collision with root package name */
    final f f4688a;

    h(f fVar) {
        this.f4688a = fVar;
    }

    public void close() throws IOException {
        this.f4688a.close();
    }

    public void a() {
        this.f4688a.a();
    }
}
