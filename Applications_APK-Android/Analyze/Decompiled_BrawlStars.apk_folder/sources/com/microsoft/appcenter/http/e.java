package com.microsoft.appcenter.http;

import android.net.TrafficStats;
import android.os.AsyncTask;
import android.util.Pair;
import com.microsoft.appcenter.http.f;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Map;
import java.util.regex.Pattern;
import javax.net.ssl.HttpsURLConnection;

/* compiled from: DefaultHttpClientCallTask */
class e extends AsyncTask<Void, Void, Object> {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f4684a = Pattern.compile("token=[^&]+");

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f4685b = Pattern.compile("token\":\"[^\"]+\"");
    private static final Pattern c = Pattern.compile("redirect_uri\":\"[^\"]+\"");
    private final String d;
    private final String e;
    private final Map<String, String> f;
    private final f.a g;
    private final m h;
    private final a i;
    private final boolean j;

    /* compiled from: DefaultHttpClientCallTask */
    interface a {
        void a(e eVar);

        void b(e eVar);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ Object doInBackground(Object[] objArr) {
        return b();
    }

    e(String str, String str2, Map<String, String> map, f.a aVar, m mVar, a aVar2, boolean z) {
        this.d = str;
        this.e = str2;
        this.f = map;
        this.g = aVar;
        this.h = mVar;
        this.i = aVar2;
        this.j = z;
    }

    private void a(OutputStream outputStream, byte[] bArr) throws IOException {
        int i2 = 0;
        while (i2 < bArr.length) {
            outputStream.write(bArr, i2, Math.min(bArr.length - i2, 1024));
            if (!isCancelled()) {
                i2 += 1024;
            } else {
                return;
            }
        }
    }

    private String a(HttpsURLConnection httpsURLConnection) throws IOException {
        InputStream inputStream;
        StringBuilder sb = new StringBuilder(Math.max(httpsURLConnection.getContentLength(), 16));
        int responseCode = httpsURLConnection.getResponseCode();
        if (responseCode < 200 || responseCode >= 400) {
            inputStream = httpsURLConnection.getErrorStream();
        } else {
            inputStream = httpsURLConnection.getInputStream();
        }
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
            char[] cArr = new char[1024];
            do {
                int read = inputStreamReader.read(cArr);
                if (read <= 0) {
                    break;
                }
                sb.append(cArr, 0, read);
            } while (!isCancelled());
            return sb.toString();
        } finally {
            inputStream.close();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0076 A[Catch:{ all -> 0x011a, all -> 0x01cf }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x008f A[Catch:{ all -> 0x011a, all -> 0x01cf }, LOOP:0: B:27:0x0089->B:29:0x008f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00af A[SYNTHETIC, Splitter:B:34:0x00af] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.util.Pair<java.lang.String, java.util.Map<java.lang.String, java.lang.String>> a() throws java.lang.Exception {
        /*
            r12 = this;
            java.lang.String r0 = r12.d
            java.lang.String r1 = "https"
            boolean r0 = r0.startsWith(r1)
            if (r0 == 0) goto L_0x01dc
            java.net.URL r0 = new java.net.URL
            java.lang.String r1 = r12.d
            r0.<init>(r1)
            java.net.URLConnection r1 = r0.openConnection()
            boolean r2 = r1 instanceof javax.net.ssl.HttpsURLConnection
            if (r2 == 0) goto L_0x01d4
            javax.net.ssl.HttpsURLConnection r1 = (javax.net.ssl.HttpsURLConnection) r1
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x01cf }
            r3 = 21
            if (r2 > r3) goto L_0x0029
            com.microsoft.appcenter.http.n r2 = new com.microsoft.appcenter.http.n     // Catch:{ all -> 0x01cf }
            r2.<init>()     // Catch:{ all -> 0x01cf }
            r1.setSSLSocketFactory(r2)     // Catch:{ all -> 0x01cf }
        L_0x0029:
            r2 = 60000(0xea60, float:8.4078E-41)
            r1.setConnectTimeout(r2)     // Catch:{ all -> 0x01cf }
            r2 = 20000(0x4e20, float:2.8026E-41)
            r1.setReadTimeout(r2)     // Catch:{ all -> 0x01cf }
            java.lang.String r2 = r12.e     // Catch:{ all -> 0x01cf }
            r1.setRequestMethod(r2)     // Catch:{ all -> 0x01cf }
            java.lang.String r2 = r12.e     // Catch:{ all -> 0x01cf }
            java.lang.String r3 = "POST"
            boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x01cf }
            java.lang.String r3 = "application/json"
            r4 = 1
            r5 = 0
            java.lang.String r6 = "Content-Type"
            r7 = 0
            if (r2 == 0) goto L_0x0072
            com.microsoft.appcenter.http.f$a r2 = r12.g     // Catch:{ all -> 0x01cf }
            if (r2 == 0) goto L_0x0072
            com.microsoft.appcenter.http.f$a r2 = r12.g     // Catch:{ all -> 0x01cf }
            java.lang.String r2 = r2.a()     // Catch:{ all -> 0x01cf }
            java.lang.String r8 = "UTF-8"
            byte[] r8 = r2.getBytes(r8)     // Catch:{ all -> 0x01cf }
            boolean r9 = r12.j     // Catch:{ all -> 0x01cf }
            if (r9 == 0) goto L_0x0064
            int r9 = r8.length     // Catch:{ all -> 0x01cf }
            r10 = 1400(0x578, float:1.962E-42)
            if (r9 < r10) goto L_0x0064
            r5 = 1
        L_0x0064:
            java.util.Map<java.lang.String, java.lang.String> r9 = r12.f     // Catch:{ all -> 0x01cf }
            boolean r9 = r9.containsKey(r6)     // Catch:{ all -> 0x01cf }
            if (r9 != 0) goto L_0x0074
            java.util.Map<java.lang.String, java.lang.String> r9 = r12.f     // Catch:{ all -> 0x01cf }
            r9.put(r6, r3)     // Catch:{ all -> 0x01cf }
            goto L_0x0074
        L_0x0072:
            r2 = r7
            r8 = r2
        L_0x0074:
            if (r5 == 0) goto L_0x007f
            java.util.Map<java.lang.String, java.lang.String> r9 = r12.f     // Catch:{ all -> 0x01cf }
            java.lang.String r10 = "Content-Encoding"
            java.lang.String r11 = "gzip"
            r9.put(r10, r11)     // Catch:{ all -> 0x01cf }
        L_0x007f:
            java.util.Map<java.lang.String, java.lang.String> r9 = r12.f     // Catch:{ all -> 0x01cf }
            java.util.Set r9 = r9.entrySet()     // Catch:{ all -> 0x01cf }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ all -> 0x01cf }
        L_0x0089:
            boolean r10 = r9.hasNext()     // Catch:{ all -> 0x01cf }
            if (r10 == 0) goto L_0x00a5
            java.lang.Object r10 = r9.next()     // Catch:{ all -> 0x01cf }
            java.util.Map$Entry r10 = (java.util.Map.Entry) r10     // Catch:{ all -> 0x01cf }
            java.lang.Object r11 = r10.getKey()     // Catch:{ all -> 0x01cf }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x01cf }
            java.lang.Object r10 = r10.getValue()     // Catch:{ all -> 0x01cf }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ all -> 0x01cf }
            r1.setRequestProperty(r11, r10)     // Catch:{ all -> 0x01cf }
            goto L_0x0089
        L_0x00a5:
            boolean r9 = r12.isCancelled()     // Catch:{ all -> 0x01cf }
            if (r9 == 0) goto L_0x00af
            r1.disconnect()
            return r7
        L_0x00af:
            com.microsoft.appcenter.http.f$a r9 = r12.g     // Catch:{ all -> 0x01cf }
            if (r9 == 0) goto L_0x00ba
            com.microsoft.appcenter.http.f$a r9 = r12.g     // Catch:{ all -> 0x01cf }
            java.util.Map<java.lang.String, java.lang.String> r10 = r12.f     // Catch:{ all -> 0x01cf }
            r9.a(r0, r10)     // Catch:{ all -> 0x01cf }
        L_0x00ba:
            java.lang.String r0 = "AppCenter"
            r9 = 2
            if (r8 == 0) goto L_0x011f
            int r10 = com.microsoft.appcenter.utils.a.a()     // Catch:{ all -> 0x01cf }
            if (r10 > r9) goto L_0x00f1
            int r10 = r2.length()     // Catch:{ all -> 0x01cf }
            r11 = 4096(0x1000, float:5.74E-42)
            if (r10 >= r11) goto L_0x00ee
            java.util.regex.Pattern r10 = com.microsoft.appcenter.http.e.f4684a     // Catch:{ all -> 0x01cf }
            java.util.regex.Matcher r2 = r10.matcher(r2)     // Catch:{ all -> 0x01cf }
            java.lang.String r10 = "token=***"
            java.lang.String r2 = r2.replaceAll(r10)     // Catch:{ all -> 0x01cf }
            java.util.Map<java.lang.String, java.lang.String> r10 = r12.f     // Catch:{ all -> 0x01cf }
            java.lang.Object r10 = r10.get(r6)     // Catch:{ all -> 0x01cf }
            boolean r3 = r3.equals(r10)     // Catch:{ all -> 0x01cf }
            if (r3 == 0) goto L_0x00ee
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ all -> 0x01cf }
            r3.<init>(r2)     // Catch:{ all -> 0x01cf }
            java.lang.String r2 = r3.toString(r9)     // Catch:{ all -> 0x01cf }
        L_0x00ee:
            com.microsoft.appcenter.utils.a.a(r0, r2)     // Catch:{ all -> 0x01cf }
        L_0x00f1:
            if (r5 == 0) goto L_0x0108
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x01cf }
            int r3 = r8.length     // Catch:{ all -> 0x01cf }
            r2.<init>(r3)     // Catch:{ all -> 0x01cf }
            java.util.zip.GZIPOutputStream r3 = new java.util.zip.GZIPOutputStream     // Catch:{ all -> 0x01cf }
            r3.<init>(r2)     // Catch:{ all -> 0x01cf }
            r3.write(r8)     // Catch:{ all -> 0x01cf }
            r3.close()     // Catch:{ all -> 0x01cf }
            byte[] r8 = r2.toByteArray()     // Catch:{ all -> 0x01cf }
        L_0x0108:
            r1.setDoOutput(r4)     // Catch:{ all -> 0x01cf }
            int r2 = r8.length     // Catch:{ all -> 0x01cf }
            r1.setFixedLengthStreamingMode(r2)     // Catch:{ all -> 0x01cf }
            java.io.OutputStream r2 = r1.getOutputStream()     // Catch:{ all -> 0x01cf }
            r12.a(r2, r8)     // Catch:{ all -> 0x011a }
            r2.close()     // Catch:{ all -> 0x01cf }
            goto L_0x011f
        L_0x011a:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x01cf }
            throw r0     // Catch:{ all -> 0x01cf }
        L_0x011f:
            boolean r2 = r12.isCancelled()     // Catch:{ all -> 0x01cf }
            if (r2 == 0) goto L_0x0129
            r1.disconnect()
            return r7
        L_0x0129:
            int r2 = r1.getResponseCode()     // Catch:{ all -> 0x01cf }
            java.lang.String r3 = r12.a(r1)     // Catch:{ all -> 0x01cf }
            int r4 = com.microsoft.appcenter.utils.a.a()     // Catch:{ all -> 0x01cf }
            if (r4 > r9) goto L_0x0185
            java.lang.String r4 = r1.getHeaderField(r6)     // Catch:{ all -> 0x01cf }
            if (r4 == 0) goto L_0x0151
            java.lang.String r5 = "text/"
            boolean r5 = r4.startsWith(r5)     // Catch:{ all -> 0x01cf }
            if (r5 != 0) goto L_0x0151
            java.lang.String r5 = "application/"
            boolean r4 = r4.startsWith(r5)     // Catch:{ all -> 0x01cf }
            if (r4 == 0) goto L_0x014e
            goto L_0x0151
        L_0x014e:
            java.lang.String r4 = "<binary>"
            goto L_0x0169
        L_0x0151:
            java.util.regex.Pattern r4 = com.microsoft.appcenter.http.e.f4685b     // Catch:{ all -> 0x01cf }
            java.util.regex.Matcher r4 = r4.matcher(r3)     // Catch:{ all -> 0x01cf }
            java.lang.String r5 = "token\":\"***\""
            java.lang.String r4 = r4.replaceAll(r5)     // Catch:{ all -> 0x01cf }
            java.util.regex.Pattern r5 = com.microsoft.appcenter.http.e.c     // Catch:{ all -> 0x01cf }
            java.util.regex.Matcher r4 = r5.matcher(r4)     // Catch:{ all -> 0x01cf }
            java.lang.String r5 = "redirect_uri\":\"***\""
            java.lang.String r4 = r4.replaceAll(r5)     // Catch:{ all -> 0x01cf }
        L_0x0169:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x01cf }
            r5.<init>()     // Catch:{ all -> 0x01cf }
            java.lang.String r6 = "HTTP response status="
            r5.append(r6)     // Catch:{ all -> 0x01cf }
            r5.append(r2)     // Catch:{ all -> 0x01cf }
            java.lang.String r6 = " payload="
            r5.append(r6)     // Catch:{ all -> 0x01cf }
            r5.append(r4)     // Catch:{ all -> 0x01cf }
            java.lang.String r4 = r5.toString()     // Catch:{ all -> 0x01cf }
            com.microsoft.appcenter.utils.a.a(r0, r4)     // Catch:{ all -> 0x01cf }
        L_0x0185:
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x01cf }
            r0.<init>()     // Catch:{ all -> 0x01cf }
            java.util.Map r4 = r1.getHeaderFields()     // Catch:{ all -> 0x01cf }
            java.util.Set r4 = r4.entrySet()     // Catch:{ all -> 0x01cf }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x01cf }
        L_0x0196:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x01cf }
            if (r5 == 0) goto L_0x01b8
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x01cf }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ all -> 0x01cf }
            java.lang.Object r6 = r5.getKey()     // Catch:{ all -> 0x01cf }
            java.lang.Object r5 = r5.getValue()     // Catch:{ all -> 0x01cf }
            java.util.List r5 = (java.util.List) r5     // Catch:{ all -> 0x01cf }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ all -> 0x01cf }
            java.lang.Object r5 = r5.next()     // Catch:{ all -> 0x01cf }
            r0.put(r6, r5)     // Catch:{ all -> 0x01cf }
            goto L_0x0196
        L_0x01b8:
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 < r4) goto L_0x01c9
            r4 = 300(0x12c, float:4.2E-43)
            if (r2 >= r4) goto L_0x01c9
            android.util.Pair r2 = new android.util.Pair     // Catch:{ all -> 0x01cf }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x01cf }
            r1.disconnect()
            return r2
        L_0x01c9:
            com.microsoft.appcenter.http.HttpException r4 = new com.microsoft.appcenter.http.HttpException     // Catch:{ all -> 0x01cf }
            r4.<init>(r2, r3, r0)     // Catch:{ all -> 0x01cf }
            throw r4     // Catch:{ all -> 0x01cf }
        L_0x01cf:
            r0 = move-exception
            r1.disconnect()
            throw r0
        L_0x01d4:
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "App Center supports only HTTPS connection."
            r0.<init>(r1)
            throw r0
        L_0x01dc:
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "App Center support only HTTPS connection."
            r0.<init>(r1)
            goto L_0x01e5
        L_0x01e4:
            throw r0
        L_0x01e5:
            goto L_0x01e4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.http.e.a():android.util.Pair");
    }

    private Object b() {
        TrafficStats.setThreadStatsTag(-667034599);
        try {
            return a();
        } catch (Exception e2) {
            return e2;
        } finally {
            TrafficStats.clearThreadStatsTag();
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.i.a(this);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object obj) {
        this.i.b(this);
        if (obj instanceof Exception) {
            this.h.a((Exception) obj);
            return;
        }
        Pair pair = (Pair) obj;
        this.h.a((String) pair.first, (Map) pair.second);
    }

    /* access modifiers changed from: protected */
    public void onCancelled(Object obj) {
        if ((obj instanceof Pair) || (obj instanceof HttpException)) {
            onPostExecute(obj);
        } else {
            this.i.b(this);
        }
    }
}
