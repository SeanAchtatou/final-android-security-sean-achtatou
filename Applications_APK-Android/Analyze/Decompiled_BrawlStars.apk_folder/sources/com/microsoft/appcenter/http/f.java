package com.microsoft.appcenter.http;

import java.io.Closeable;
import java.net.URL;
import java.util.Map;
import org.json.JSONException;

/* compiled from: HttpClient */
public interface f extends Closeable {

    /* compiled from: HttpClient */
    public interface a {
        String a() throws JSONException;

        void a(URL url, Map<String, String> map);
    }

    l a(String str, String str2, Map<String, String> map, a aVar, m mVar);

    void a();
}
