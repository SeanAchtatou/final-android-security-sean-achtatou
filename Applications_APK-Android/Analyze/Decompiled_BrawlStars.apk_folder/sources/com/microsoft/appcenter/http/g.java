package com.microsoft.appcenter.http;

import com.microsoft.appcenter.http.f;
import java.util.Map;

/* compiled from: HttpClientCallDecorator */
abstract class g implements l, m, Runnable {

    /* renamed from: a  reason: collision with root package name */
    final m f4686a;

    /* renamed from: b  reason: collision with root package name */
    l f4687b;
    private final f c;
    private final String d;
    private final String e;
    private final Map<String, String> f;
    private final f.a g;

    g(f fVar, String str, String str2, Map<String, String> map, f.a aVar, m mVar) {
        this.c = fVar;
        this.d = str;
        this.e = str2;
        this.f = map;
        this.g = aVar;
        this.f4686a = mVar;
    }

    public synchronized void run() {
        this.f4687b = this.c.a(this.d, this.e, this.f, this.g, this);
    }

    public final void a(String str, Map<String, String> map) {
        this.f4686a.a(str, map);
    }

    public void a(Exception exc) {
        this.f4686a.a(exc);
    }
}
