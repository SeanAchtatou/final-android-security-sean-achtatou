package com.microsoft.appcenter;

import com.microsoft.appcenter.utils.a;
import java.util.concurrent.Semaphore;

/* compiled from: UncaughtExceptionHandler */
class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Semaphore f4710a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ r f4711b;

    s(r rVar, Semaphore semaphore) {
        this.f4711b = rVar;
        this.f4710a = semaphore;
    }

    public void run() {
        this.f4711b.c.a();
        a.b("AppCenter", "Channel completed shutdown.");
        this.f4710a.release();
    }
}
