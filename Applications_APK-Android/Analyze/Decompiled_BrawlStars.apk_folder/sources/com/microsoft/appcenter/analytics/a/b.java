package com.microsoft.appcenter.analytics.a;

import com.microsoft.appcenter.a.a;
import com.microsoft.appcenter.analytics.b.a.c;
import com.microsoft.appcenter.b.a.d;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: AnalyticsValidator */
public class b extends a {
    private static String a(String str, String str2) {
        if (str == null || str.isEmpty()) {
            com.microsoft.appcenter.utils.a.e("AppCenterAnalytics", str2 + " name cannot be null or empty.");
            return null;
        } else if (str.length() <= 256) {
            return str;
        } else {
            com.microsoft.appcenter.utils.a.d("AppCenterAnalytics", String.format("%s '%s' : name length cannot be longer than %s characters. Name will be truncated.", str2, str, 256));
            return str.substring(0, 256);
        }
    }

    public final boolean b(d dVar) {
        HashMap hashMap;
        d dVar2 = dVar;
        boolean z = false;
        if (dVar2 instanceof c) {
            com.microsoft.appcenter.analytics.b.a.b bVar = (com.microsoft.appcenter.analytics.b.a.b) dVar2;
            String a2 = a(bVar.f4557b, bVar.a());
            if (a2 != null) {
                Map<String, String> map = bVar.c;
                String a3 = bVar.a();
                if (map == null) {
                    hashMap = null;
                } else {
                    HashMap hashMap2 = new HashMap();
                    Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Map.Entry next = it.next();
                        String str = (String) next.getKey();
                        String str2 = (String) next.getValue();
                        if (hashMap2.size() >= 20) {
                            com.microsoft.appcenter.utils.a.d("AppCenterAnalytics", String.format("%s '%s' : properties cannot contain more than %s items. Skipping other properties.", a3, a2, 20));
                            break;
                        } else if (str == null || str.isEmpty()) {
                            com.microsoft.appcenter.utils.a.d("AppCenterAnalytics", String.format("%s '%s' : a property key cannot be null or empty. Property will be skipped.", a3, a2));
                        } else if (str2 == null) {
                            com.microsoft.appcenter.utils.a.d("AppCenterAnalytics", String.format("%s '%s' : property '%s' : property value cannot be null. Property '%s' will be skipped.", a3, a2, str, str));
                        } else {
                            if (str.length() > 125) {
                                com.microsoft.appcenter.utils.a.d("AppCenterAnalytics", String.format("%s '%s' : property '%s' : property key length cannot be longer than %s characters. Property key will be truncated.", a3, a2, str, 125));
                                str = str.substring(0, 125);
                            }
                            if (str2.length() > 125) {
                                com.microsoft.appcenter.utils.a.d("AppCenterAnalytics", String.format("%s '%s' : property '%s' : property value cannot be longer than %s characters. Property value will be truncated.", a3, a2, str, 125));
                                str2 = str2.substring(0, 125);
                            }
                            hashMap2.put(str, str2);
                        }
                    }
                    hashMap = hashMap2;
                }
                bVar.f4557b = a2;
                bVar.c = hashMap;
                z = true;
            }
            return !z;
        } else if (dVar2 instanceof com.microsoft.appcenter.analytics.b.a.a) {
            return !a((com.microsoft.appcenter.analytics.b.a.a) dVar2);
        } else {
            return false;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v13, resolved type: com.microsoft.appcenter.b.a.c.e} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v16, resolved type: com.microsoft.appcenter.b.a.c.e} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v17, resolved type: com.microsoft.appcenter.b.a.c.e} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v18, resolved type: com.microsoft.appcenter.b.a.c.e} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v19, resolved type: com.microsoft.appcenter.b.a.c.e} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v20, resolved type: com.microsoft.appcenter.b.a.c.e} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(com.microsoft.appcenter.analytics.b.a.a r14) {
        /*
            java.lang.String r0 = r14.f4557b
            java.lang.String r1 = "event"
            java.lang.String r0 = a(r0, r1)
            r1 = 0
            if (r0 != 0) goto L_0x000c
            return r1
        L_0x000c:
            java.util.List<com.microsoft.appcenter.b.a.c.f> r2 = r14.f4556a
            r3 = 1
            if (r2 != 0) goto L_0x0013
            goto L_0x0132
        L_0x0013:
            java.util.ListIterator r2 = r2.listIterator()
            r4 = 0
            r5 = 0
        L_0x0019:
            boolean r6 = r2.hasNext()
            if (r6 == 0) goto L_0x0132
            java.lang.Object r6 = r2.next()
            com.microsoft.appcenter.b.a.c.f r6 = (com.microsoft.appcenter.b.a.c.f) r6
            java.lang.String r7 = r6.f4612b
            r8 = 20
            java.lang.String r9 = "AppCenterAnalytics"
            if (r4 < r8) goto L_0x0045
            if (r5 != 0) goto L_0x0041
            java.lang.Object[] r5 = new java.lang.Object[r3]
            java.lang.Integer r6 = java.lang.Integer.valueOf(r8)
            r5[r1] = r6
            java.lang.String r6 = "Typed properties cannot contain more than %s items. Skipping other properties."
            java.lang.String r5 = java.lang.String.format(r6, r5)
            com.microsoft.appcenter.utils.a.d(r9, r5)
            r5 = 1
        L_0x0041:
            r2.remove()
            goto L_0x0019
        L_0x0045:
            if (r7 == 0) goto L_0x0128
            boolean r8 = r7.isEmpty()
            if (r8 == 0) goto L_0x004f
            goto L_0x0128
        L_0x004f:
            int r8 = r7.length()
            r10 = 2
            r11 = 125(0x7d, float:1.75E-43)
            if (r8 <= r11) goto L_0x00d7
            java.lang.Object[] r8 = new java.lang.Object[r10]
            r8[r1] = r7
            java.lang.Integer r12 = java.lang.Integer.valueOf(r11)
            r8[r3] = r12
            java.lang.String r12 = "Typed property '%s' : property key length cannot be longer than %s characters. Property key will be truncated."
            java.lang.String r8 = java.lang.String.format(r12, r8)
            com.microsoft.appcenter.utils.a.d(r9, r8)
            java.lang.String r7 = r7.substring(r1, r11)
            java.lang.String r8 = r6.a()
            java.lang.String r12 = "boolean"
            boolean r12 = r12.equals(r8)
            if (r12 == 0) goto L_0x0088
            com.microsoft.appcenter.b.a.c.a r8 = new com.microsoft.appcenter.b.a.c.a
            r8.<init>()
            com.microsoft.appcenter.b.a.c.a r6 = (com.microsoft.appcenter.b.a.c.a) r6
            boolean r6 = r6.f4607a
            r8.f4607a = r6
        L_0x0086:
            r6 = r8
            goto L_0x00d0
        L_0x0088:
            java.lang.String r12 = "dateTime"
            boolean r12 = r12.equals(r8)
            if (r12 == 0) goto L_0x009c
            com.microsoft.appcenter.b.a.c.b r8 = new com.microsoft.appcenter.b.a.c.b
            r8.<init>()
            com.microsoft.appcenter.b.a.c.b r6 = (com.microsoft.appcenter.b.a.c.b) r6
            java.util.Date r6 = r6.f4608a
            r8.f4608a = r6
            goto L_0x0086
        L_0x009c:
            java.lang.String r12 = "double"
            boolean r12 = r12.equals(r8)
            if (r12 == 0) goto L_0x00b0
            com.microsoft.appcenter.b.a.c.c r8 = new com.microsoft.appcenter.b.a.c.c
            r8.<init>()
            com.microsoft.appcenter.b.a.c.c r6 = (com.microsoft.appcenter.b.a.c.c) r6
            double r12 = r6.f4609a
            r8.f4609a = r12
            goto L_0x0086
        L_0x00b0:
            java.lang.String r12 = "long"
            boolean r8 = r12.equals(r8)
            if (r8 == 0) goto L_0x00c4
            com.microsoft.appcenter.b.a.c.d r8 = new com.microsoft.appcenter.b.a.c.d
            r8.<init>()
            com.microsoft.appcenter.b.a.c.d r6 = (com.microsoft.appcenter.b.a.c.d) r6
            long r12 = r6.f4610a
            r8.f4610a = r12
            goto L_0x0086
        L_0x00c4:
            com.microsoft.appcenter.b.a.c.e r8 = new com.microsoft.appcenter.b.a.c.e
            r8.<init>()
            com.microsoft.appcenter.b.a.c.e r6 = (com.microsoft.appcenter.b.a.c.e) r6
            java.lang.String r6 = r6.f4611a
            r8.f4611a = r6
            goto L_0x0086
        L_0x00d0:
            r6.f4612b = r7
            r2.set(r6)
            r8 = 0
            goto L_0x00d8
        L_0x00d7:
            r8 = 1
        L_0x00d8:
            boolean r12 = r6 instanceof com.microsoft.appcenter.b.a.c.e
            if (r12 == 0) goto L_0x0124
            com.microsoft.appcenter.b.a.c.e r6 = (com.microsoft.appcenter.b.a.c.e) r6
            java.lang.String r12 = r6.f4611a
            if (r12 != 0) goto L_0x00f6
            java.lang.Object[] r6 = new java.lang.Object[r10]
            r6[r1] = r7
            r6[r3] = r7
            java.lang.String r7 = "Typed property '%s' : property value cannot be null. Property '%s' will be skipped."
            java.lang.String r6 = java.lang.String.format(r7, r6)
            com.microsoft.appcenter.utils.a.d(r9, r6)
            r2.remove()
            goto L_0x0019
        L_0x00f6:
            int r13 = r12.length()
            if (r13 <= r11) goto L_0x0124
            java.lang.Object[] r10 = new java.lang.Object[r10]
            r10[r1] = r7
            java.lang.Integer r13 = java.lang.Integer.valueOf(r11)
            r10[r3] = r13
            java.lang.String r13 = "A String property '%s' : property value cannot be longer than %s characters. Property value will be truncated."
            java.lang.String r10 = java.lang.String.format(r13, r10)
            com.microsoft.appcenter.utils.a.d(r9, r10)
            java.lang.String r9 = r12.substring(r1, r11)
            if (r8 == 0) goto L_0x0122
            com.microsoft.appcenter.b.a.c.e r6 = new com.microsoft.appcenter.b.a.c.e
            r6.<init>()
            r6.f4612b = r7
            r6.f4611a = r9
            r2.set(r6)
            goto L_0x0124
        L_0x0122:
            r6.f4611a = r9
        L_0x0124:
            int r4 = r4 + 1
            goto L_0x0019
        L_0x0128:
            java.lang.String r6 = "A typed property key cannot be null or empty. Property will be skipped."
            com.microsoft.appcenter.utils.a.d(r9, r6)
            r2.remove()
            goto L_0x0019
        L_0x0132:
            r14.f4557b = r0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.analytics.a.b.a(com.microsoft.appcenter.analytics.b.a.a):boolean");
    }
}
