package com.microsoft.appcenter.analytics;

import android.os.SystemClock;
import com.microsoft.appcenter.analytics.a.c;
import com.microsoft.appcenter.utils.a;

/* compiled from: Analytics */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Runnable f4561a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Analytics f4562b;

    e(Analytics analytics, Runnable runnable) {
        this.f4562b = analytics;
        this.f4561a = runnable;
    }

    public void run() {
        this.f4561a.run();
        if (this.f4562b.i != null) {
            c c = this.f4562b.i;
            a.b("AppCenterAnalytics", "onActivityPaused");
            c.f = Long.valueOf(SystemClock.elapsedRealtime());
        }
    }
}
