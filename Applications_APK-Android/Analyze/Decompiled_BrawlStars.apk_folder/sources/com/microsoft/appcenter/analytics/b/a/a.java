package com.microsoft.appcenter.analytics.b.a;

import android.support.v4.app.NotificationCompat;
import com.microsoft.appcenter.b.a.c.b;
import com.microsoft.appcenter.b.a.c.c;
import com.microsoft.appcenter.b.a.c.d;
import com.microsoft.appcenter.b.a.c.e;
import com.microsoft.appcenter.b.a.c.f;
import com.microsoft.appcenter.b.a.g;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: EventLog */
public class a extends b {

    /* renamed from: a  reason: collision with root package name */
    public List<f> f4556a;
    private UUID d;

    public final String a() {
        return NotificationCompat.CATEGORY_EVENT;
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        ArrayList arrayList;
        f fVar;
        super.a(jSONObject);
        this.d = UUID.fromString(jSONObject.getString("id"));
        JSONArray optJSONArray = jSONObject.optJSONArray("typedProperties");
        if (optJSONArray != null) {
            arrayList = new ArrayList(optJSONArray.length());
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                String string = jSONObject2.getString("type");
                if ("boolean".equals(string)) {
                    fVar = new com.microsoft.appcenter.b.a.c.a();
                } else if ("dateTime".equals(string)) {
                    fVar = new b();
                } else if ("double".equals(string)) {
                    fVar = new c();
                } else if ("long".equals(string)) {
                    fVar = new d();
                } else if ("string".equals(string)) {
                    fVar = new e();
                } else {
                    throw new JSONException("Unsupported type: " + string);
                }
                fVar.a(jSONObject2);
                arrayList.add(fVar);
            }
        } else {
            arrayList = null;
        }
        this.f4556a = arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.util.List<? extends com.microsoft.appcenter.b.a.g>):void
     arg types: [org.json.JSONStringer, java.lang.String, java.util.List<com.microsoft.appcenter.b.a.c.f>]
     candidates:
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONObject, java.lang.String, com.microsoft.appcenter.b.a.a.i):java.util.List<M>
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.lang.Object):void
      com.microsoft.appcenter.b.a.a.f.a(org.json.JSONStringer, java.lang.String, java.util.List<? extends com.microsoft.appcenter.b.a.g>):void */
    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        jSONStringer.key("id").value(this.d);
        com.microsoft.appcenter.b.a.a.f.a(jSONStringer, "typedProperties", (List<? extends g>) this.f4556a);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        a aVar = (a) obj;
        UUID uuid = this.d;
        if (uuid == null ? aVar.d != null : !uuid.equals(aVar.d)) {
            return false;
        }
        List<f> list = this.f4556a;
        List<f> list2 = aVar.f4556a;
        if (list != null) {
            return list.equals(list2);
        }
        if (list2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        UUID uuid = this.d;
        int i = 0;
        int hashCode2 = (hashCode + (uuid != null ? uuid.hashCode() : 0)) * 31;
        List<f> list = this.f4556a;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode2 + i;
    }
}
