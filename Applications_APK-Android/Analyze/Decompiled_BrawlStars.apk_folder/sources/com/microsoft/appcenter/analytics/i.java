package com.microsoft.appcenter.analytics;

import java.util.Date;

/* compiled from: AuthenticationProvider */
public class i {

    /* renamed from: a  reason: collision with root package name */
    final String f4566a;

    /* renamed from: b  reason: collision with root package name */
    private final b f4567b;
    private a c;
    private Date d;

    /* compiled from: AuthenticationProvider */
    public interface a {
    }

    private synchronized void b() {
        if (this.c == null) {
            com.microsoft.appcenter.utils.a.b("AppCenterAnalytics", "Calling token provider=" + this.f4567b + " callback.");
            this.c = new j(this);
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        if (this.d != null && this.d.getTime() <= System.currentTimeMillis() + 600000) {
            b();
        }
    }

    /* compiled from: AuthenticationProvider */
    public enum b {
        MSA_COMPACT("p"),
        MSA_DELEGATE("d");
        
        private final String c;

        private b(String str) {
            this.c = str + ":";
        }
    }
}
