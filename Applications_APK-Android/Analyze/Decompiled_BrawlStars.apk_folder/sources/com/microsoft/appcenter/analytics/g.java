package com.microsoft.appcenter.analytics;

import android.content.Context;
import com.microsoft.appcenter.a.b;
import com.microsoft.appcenter.b.a.b.k;
import com.microsoft.appcenter.utils.d.d;
import java.util.HashMap;
import java.util.Map;

/* compiled from: AnalyticsTransmissionTarget */
public class g {

    /* renamed from: a  reason: collision with root package name */
    static i f4564a;

    /* renamed from: b  reason: collision with root package name */
    final g f4565b;
    final l c;
    Context d;
    b e;
    private final String f;
    private final Map<String, g> g = new HashMap();

    g(String str, g gVar) {
        this.f = str;
        this.f4565b = null;
        this.c = new l(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, java.lang.String):java.lang.String
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean a() {
        return d.a(Analytics.getInstance().l() + k.a(this.f), true);
    }
}
