package com.microsoft.appcenter.analytics;

import android.app.Activity;
import java.lang.ref.WeakReference;

/* compiled from: Analytics */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f4554a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Analytics f4555b;

    b(Analytics analytics, Activity activity) {
        this.f4555b = analytics;
        this.f4554a = activity;
    }

    public void run() {
        WeakReference unused = this.f4555b.f = new WeakReference(this.f4554a);
    }
}
