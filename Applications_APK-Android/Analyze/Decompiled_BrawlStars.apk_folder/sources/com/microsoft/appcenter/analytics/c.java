package com.microsoft.appcenter.analytics;

import android.app.Activity;

/* compiled from: Analytics */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Runnable f4558a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Activity f4559b;
    final /* synthetic */ Analytics c;

    c(Analytics analytics, Runnable runnable, Activity activity) {
        this.c = analytics;
        this.f4558a = runnable;
        this.f4559b = activity;
    }

    public void run() {
        this.f4558a.run();
        this.c.a(this.f4559b);
    }
}
