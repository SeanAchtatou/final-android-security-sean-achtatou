package com.microsoft.appcenter.analytics.a;

import android.os.SystemClock;
import com.microsoft.appcenter.a.a;
import com.microsoft.appcenter.a.b;
import com.microsoft.appcenter.b.a.d;
import com.microsoft.appcenter.b.a.h;
import com.microsoft.appcenter.utils.b.e;
import java.util.Date;
import java.util.UUID;

/* compiled from: SessionTracker */
public class c extends a {

    /* renamed from: a  reason: collision with root package name */
    public final b f4552a;

    /* renamed from: b  reason: collision with root package name */
    public final String f4553b;
    public UUID c;
    public long d;
    public Long e;
    public Long f;

    public c(b bVar, String str) {
        this.f4552a = bVar;
        this.f4553b = str;
    }

    public final void a(d dVar) {
        if (!(dVar instanceof com.microsoft.appcenter.analytics.b.a.d) && !(dVar instanceof h)) {
            Date b2 = dVar.b();
            if (b2 != null) {
                e.a a2 = e.a().a(b2.getTime());
                if (a2 != null) {
                    dVar.a(a2.f4729a);
                    return;
                }
                return;
            }
            dVar.a(this.c);
            this.d = SystemClock.elapsedRealtime();
        }
    }
}
