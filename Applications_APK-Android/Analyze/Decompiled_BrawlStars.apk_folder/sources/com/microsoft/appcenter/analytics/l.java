package com.microsoft.appcenter.analytics;

import com.microsoft.appcenter.a.a;

/* compiled from: PropertyConfigurator */
public class l extends a {

    /* renamed from: a  reason: collision with root package name */
    private String f4572a;

    /* renamed from: b  reason: collision with root package name */
    private String f4573b;
    private String c;
    private String d;
    private boolean e;
    private final g f;
    private final k g = new k();

    l(g gVar) {
        this.f = gVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0029, code lost:
        if ((r0 && r3.a()) != false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.microsoft.appcenter.b.a.d r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.microsoft.appcenter.b.a.b.c
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x002c
            java.lang.Object r0 = r6.f()
            com.microsoft.appcenter.analytics.g r3 = r5.f
            if (r0 != r3) goto L_0x002c
            com.microsoft.appcenter.analytics.g r0 = r3.f4565b
        L_0x0010:
            if (r0 == 0) goto L_0x001d
            boolean r4 = r0.a()
            if (r4 != 0) goto L_0x001a
            r0 = 0
            goto L_0x001e
        L_0x001a:
            com.microsoft.appcenter.analytics.g r0 = r0.f4565b
            goto L_0x0010
        L_0x001d:
            r0 = 1
        L_0x001e:
            if (r0 == 0) goto L_0x0028
            boolean r0 = r3.a()
            if (r0 == 0) goto L_0x0028
            r0 = 1
            goto L_0x0029
        L_0x0028:
            r0 = 0
        L_0x0029:
            if (r0 == 0) goto L_0x002c
            goto L_0x002d
        L_0x002c:
            r1 = 0
        L_0x002d:
            if (r1 == 0) goto L_0x00b6
            com.microsoft.appcenter.b.a.b.c r6 = (com.microsoft.appcenter.b.a.b.c) r6
            com.microsoft.appcenter.b.a.b.f r0 = r6.e
            com.microsoft.appcenter.b.a.b.a r0 = r0.f
            com.microsoft.appcenter.b.a.b.f r1 = r6.e
            com.microsoft.appcenter.b.a.b.n r1 = r1.c
            com.microsoft.appcenter.b.a.b.f r6 = r6.e
            com.microsoft.appcenter.b.a.b.e r6 = r6.d
            java.lang.String r2 = r5.f4572a
            if (r2 == 0) goto L_0x0044
            r0.c = r2
            goto L_0x0052
        L_0x0044:
            com.microsoft.appcenter.analytics.g r2 = r5.f
        L_0x0046:
            com.microsoft.appcenter.analytics.g r2 = r2.f4565b
            if (r2 == 0) goto L_0x0052
            com.microsoft.appcenter.analytics.l r3 = r2.c
            java.lang.String r3 = r3.f4572a
            if (r3 == 0) goto L_0x0046
            r0.c = r3
        L_0x0052:
            java.lang.String r2 = r5.f4573b
            if (r2 == 0) goto L_0x0059
            r0.f4586b = r2
            goto L_0x0067
        L_0x0059:
            com.microsoft.appcenter.analytics.g r2 = r5.f
        L_0x005b:
            com.microsoft.appcenter.analytics.g r2 = r2.f4565b
            if (r2 == 0) goto L_0x0067
            com.microsoft.appcenter.analytics.l r3 = r2.c
            java.lang.String r3 = r3.f4573b
            if (r3 == 0) goto L_0x005b
            r0.f4586b = r3
        L_0x0067:
            java.lang.String r2 = r5.c
            if (r2 == 0) goto L_0x006e
            r0.d = r2
            goto L_0x007c
        L_0x006e:
            com.microsoft.appcenter.analytics.g r2 = r5.f
        L_0x0070:
            com.microsoft.appcenter.analytics.g r2 = r2.f4565b
            if (r2 == 0) goto L_0x007c
            com.microsoft.appcenter.analytics.l r3 = r2.c
            java.lang.String r3 = r3.c
            if (r3 == 0) goto L_0x0070
            r0.d = r3
        L_0x007c:
            java.lang.String r0 = r5.d
            if (r0 == 0) goto L_0x0083
            r1.f4603a = r0
            goto L_0x0091
        L_0x0083:
            com.microsoft.appcenter.analytics.g r0 = r5.f
        L_0x0085:
            com.microsoft.appcenter.analytics.g r0 = r0.f4565b
            if (r0 == 0) goto L_0x0091
            com.microsoft.appcenter.analytics.l r2 = r0.c
            java.lang.String r2 = r2.d
            if (r2 == 0) goto L_0x0085
            r1.f4603a = r2
        L_0x0091:
            boolean r0 = r5.e
            if (r0 == 0) goto L_0x00b6
            com.microsoft.appcenter.analytics.g r0 = r5.f
            android.content.Context r0 = r0.d
            android.content.ContentResolver r0 = r0.getContentResolver()
            java.lang.String r1 = "android_id"
            java.lang.String r0 = android.provider.Settings.Secure.getString(r0, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "a:"
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r6.f4590a = r0
        L_0x00b6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.analytics.l.a(com.microsoft.appcenter.b.a.d):void");
    }
}
