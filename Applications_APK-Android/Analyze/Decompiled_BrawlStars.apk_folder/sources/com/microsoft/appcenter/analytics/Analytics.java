package com.microsoft.appcenter.analytics;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import com.facebook.places.model.PlaceFields;
import com.microsoft.appcenter.a;
import com.microsoft.appcenter.a.b;
import com.microsoft.appcenter.analytics.a.b;
import com.microsoft.appcenter.analytics.a.c;
import com.microsoft.appcenter.b.a.a.g;
import com.microsoft.appcenter.utils.b.e;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Analytics extends a {
    private static Analytics c;

    /* renamed from: b  reason: collision with root package name */
    g f4549b;
    private final Map<String, g> d = new HashMap();
    private final Map<String, g> e;
    /* access modifiers changed from: private */
    public WeakReference<Activity> f;
    /* access modifiers changed from: private */
    public Context g;
    private boolean h;
    /* access modifiers changed from: private */
    public c i;
    private b j;
    private b.C0155b k;
    /* access modifiers changed from: private */
    public com.microsoft.appcenter.analytics.a.a l;
    private long m;
    private boolean n = false;

    public final boolean c() {
        return false;
    }

    public final String e() {
        return "group_analytics";
    }

    public final String f() {
        return "AppCenterAnalytics";
    }

    public final String k() {
        return "Analytics";
    }

    private Analytics() {
        this.d.put("startSession", new com.microsoft.appcenter.analytics.b.a.a.c());
        this.d.put(PlaceFields.PAGE, new com.microsoft.appcenter.analytics.b.a.a.b());
        this.d.put(NotificationCompat.CATEGORY_EVENT, new com.microsoft.appcenter.analytics.b.a.a.a());
        this.d.put("commonSchemaEvent", new com.microsoft.appcenter.analytics.b.a.b.a.a());
        this.e = new HashMap();
        this.m = TimeUnit.SECONDS.toMillis(3);
    }

    public static synchronized Analytics getInstance() {
        Analytics analytics;
        synchronized (Analytics.class) {
            if (c == null) {
                c = new Analytics();
            }
            analytics = c;
        }
        return analytics;
    }

    private g b(String str) {
        g gVar = new g(str, null);
        com.microsoft.appcenter.utils.a.b("AppCenterAnalytics", "Created transmission target with token " + str);
        a aVar = new a(this, gVar);
        a(aVar, aVar, aVar);
        return gVar;
    }

    public final Map<String, g> d() {
        return this.d;
    }

    public synchronized void onActivityResumed(Activity activity) {
        b bVar = new b(this, activity);
        a(new c(this, bVar, activity), bVar, bVar);
    }

    public final long i() {
        return this.m;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006c, code lost:
        if (r1 == false) goto L_0x0093;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.app.Activity r14) {
        /*
            r13 = this;
            com.microsoft.appcenter.analytics.a.c r0 = r13.i
            if (r0 == 0) goto L_0x00c9
            java.lang.String r1 = "AppCenterAnalytics"
            java.lang.String r2 = "onActivityResumed"
            com.microsoft.appcenter.utils.a.b(r1, r2)
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            r0.e = r2
            java.util.UUID r2 = r0.c
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x006e
            java.lang.Long r2 = r0.f
            if (r2 != 0) goto L_0x0021
        L_0x001f:
            r1 = 0
            goto L_0x006c
        L_0x0021:
            long r5 = android.os.SystemClock.elapsedRealtime()
            long r7 = r0.d
            long r5 = r5 - r7
            r7 = 20000(0x4e20, double:9.8813E-320)
            int r2 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r2 < 0) goto L_0x0030
            r2 = 1
            goto L_0x0031
        L_0x0030:
            r2 = 0
        L_0x0031:
            java.lang.Long r5 = r0.e
            long r5 = r5.longValue()
            java.lang.Long r9 = r0.f
            long r9 = r9.longValue()
            long r11 = r0.d
            long r9 = java.lang.Math.max(r9, r11)
            long r5 = r5 - r9
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 < 0) goto L_0x004a
            r5 = 1
            goto L_0x004b
        L_0x004a:
            r5 = 0
        L_0x004b:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "noLogSentForLong="
            r6.append(r7)
            r6.append(r2)
            java.lang.String r7 = " wasBackgroundForLong="
            r6.append(r7)
            r6.append(r5)
            java.lang.String r6 = r6.toString()
            com.microsoft.appcenter.utils.a.b(r1, r6)
            if (r2 == 0) goto L_0x001f
            if (r5 == 0) goto L_0x001f
            r1 = 1
        L_0x006c:
            if (r1 == 0) goto L_0x0093
        L_0x006e:
            java.util.UUID r1 = java.util.UUID.randomUUID()
            r0.c = r1
            com.microsoft.appcenter.utils.b.e r1 = com.microsoft.appcenter.utils.b.e.a()
            java.util.UUID r2 = r0.c
            r1.a(r2)
            long r1 = android.os.SystemClock.elapsedRealtime()
            r0.d = r1
            com.microsoft.appcenter.analytics.b.a.d r1 = new com.microsoft.appcenter.analytics.b.a.d
            r1.<init>()
            java.util.UUID r2 = r0.c
            r1.l = r2
            com.microsoft.appcenter.a.b r2 = r0.f4552a
            java.lang.String r0 = r0.f4553b
            r2.a(r1, r0, r4)
        L_0x0093:
            boolean r0 = r13.n
            if (r0 == 0) goto L_0x00c9
            java.lang.Class r14 = r14.getClass()
            java.lang.String r14 = r14.getSimpleName()
            java.lang.String r0 = "Activity"
            boolean r0 = r14.endsWith(r0)
            if (r0 == 0) goto L_0x00b8
            int r0 = r14.length()
            r1 = 8
            if (r0 <= r1) goto L_0x00b8
            int r0 = r14.length()
            int r0 = r0 - r1
            java.lang.String r14 = r14.substring(r3, r0)
        L_0x00b8:
            com.microsoft.appcenter.analytics.b.a.c r0 = new com.microsoft.appcenter.analytics.b.a.c
            r0.<init>()
            r0.f4557b = r14
            r14 = 0
            r0.c = r14
            com.microsoft.appcenter.a.b r14 = r13.f4529a
            java.lang.String r1 = "group_analytics"
            r14.a(r0, r1, r4)
        L_0x00c9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.analytics.Analytics.a(android.app.Activity):void");
    }

    public synchronized void onActivityPaused(Activity activity) {
        d dVar = new d(this);
        a(new e(this, dVar), dVar, dVar);
    }

    public final b.a j() {
        return new f(this);
    }

    public final synchronized void b(boolean z) {
        if (z) {
            this.f4529a.a("group_analytics_critical", h(), 3000, 3, null, j());
            m();
        } else {
            this.f4529a.b("group_analytics_critical");
            if (this.j != null) {
                this.f4529a.b(this.j);
                this.j = null;
            }
            if (this.i != null) {
                this.f4529a.b(this.i);
                e.a().b();
                this.i = null;
            }
            if (this.k != null) {
                this.f4529a.b(this.k);
                this.k = null;
            }
        }
    }

    private void m() {
        Activity activity;
        if (this.h) {
            this.j = new com.microsoft.appcenter.analytics.a.b();
            this.f4529a.a(this.j);
            this.i = new c(this.f4529a, "group_analytics");
            this.f4529a.a(this.i);
            WeakReference<Activity> weakReference = this.f;
            if (!(weakReference == null || (activity = weakReference.get()) == null)) {
                a(activity);
            }
            this.k = new h();
            this.f4529a.a(this.k);
        }
    }

    public final synchronized void a(Context context, com.microsoft.appcenter.a.b bVar, String str, String str2, boolean z) {
        this.g = context;
        this.h = z;
        super.a(context, bVar, str, str2, z);
        c(str2);
    }

    public final void a(String str) {
        this.h = true;
        m();
        c(str);
    }

    private void c(String str) {
        if (str != null) {
            this.f4549b = b(str);
        }
    }

    public final synchronized void a(Runnable runnable) {
        super.a(runnable);
    }

    /* access modifiers changed from: package-private */
    public final String l() {
        return g() + "/";
    }
}
