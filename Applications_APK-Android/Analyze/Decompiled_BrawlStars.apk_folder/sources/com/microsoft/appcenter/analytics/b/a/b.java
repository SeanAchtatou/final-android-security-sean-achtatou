package com.microsoft.appcenter.analytics.b.a;

import com.microsoft.appcenter.b.a.f;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: LogWithNameAndProperties */
public abstract class b extends f {

    /* renamed from: b  reason: collision with root package name */
    public String f4557b;

    public void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4557b = jSONObject.getString("name");
    }

    public void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        jSONStringer.key("name").value(this.f4557b);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        String str = this.f4557b;
        String str2 = ((b) obj).f4557b;
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.f4557b;
        return hashCode + (str != null ? str.hashCode() : 0);
    }
}
