package com.microsoft.appcenter.b.a.a;

import com.microsoft.appcenter.b.a.b.c;
import com.microsoft.appcenter.b.a.d;
import com.microsoft.appcenter.b.a.e;
import java.util.Collection;
import org.json.JSONException;

/* compiled from: LogSerializer */
public interface h {
    d a(String str, String str2) throws JSONException;

    String a(d dVar) throws JSONException;

    String a(e eVar) throws JSONException;

    void a(String str, g gVar);

    Collection<c> b(d dVar);
}
