package com.microsoft.appcenter.b.a.a;

import com.microsoft.appcenter.b.a.d;
import com.microsoft.appcenter.b.a.e;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: DefaultLogSerializer */
public class c implements h {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, g> f4582a = new HashMap();

    private static JSONStringer a(JSONStringer jSONStringer, d dVar) throws JSONException {
        jSONStringer.object();
        dVar.a(jSONStringer);
        jSONStringer.endObject();
        return jSONStringer;
    }

    public final String a(d dVar) throws JSONException {
        return a(new JSONStringer(), dVar).toString();
    }

    public final d a(String str, String str2) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        if (str2 == null) {
            str2 = jSONObject.getString("type");
        }
        g gVar = this.f4582a.get(str2);
        if (gVar != null) {
            d a2 = gVar.a();
            a2.a(jSONObject);
            return a2;
        }
        throw new JSONException("Unknown log type: " + str2);
    }

    public final Collection<com.microsoft.appcenter.b.a.b.c> b(d dVar) {
        return this.f4582a.get(dVar.a()).a(dVar);
    }

    public final String a(e eVar) throws JSONException {
        JSONStringer jSONStringer = new JSONStringer();
        jSONStringer.object();
        jSONStringer.key("logs").array();
        for (d a2 : eVar.f4613a) {
            a(jSONStringer, a2);
        }
        jSONStringer.endArray();
        jSONStringer.endObject();
        return jSONStringer.toString();
    }

    public final void a(String str, g gVar) {
        this.f4582a.put(str, gVar);
    }
}
