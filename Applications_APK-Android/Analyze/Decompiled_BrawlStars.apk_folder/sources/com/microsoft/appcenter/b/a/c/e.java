package com.microsoft.appcenter.b.a.c;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: StringTypedProperty */
public class e extends f {

    /* renamed from: a  reason: collision with root package name */
    public String f4611a;

    public final String a() {
        return "string";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4611a = jSONObject.getString("value");
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        jSONStringer.key("value").value(this.f4611a);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        String str = this.f4611a;
        String str2 = ((e) obj).f4611a;
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.f4611a;
        return hashCode + (str != null ? str.hashCode() : 0);
    }
}
