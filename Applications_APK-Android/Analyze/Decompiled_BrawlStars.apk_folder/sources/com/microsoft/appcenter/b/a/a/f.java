package com.microsoft.appcenter.b.a.a;

import com.microsoft.appcenter.b.a.g;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: JSONUtils */
public class f {
    public static Integer a(JSONObject jSONObject, String str) throws JSONException {
        if (jSONObject.has(str)) {
            return Integer.valueOf(jSONObject.getInt(str));
        }
        return null;
    }

    public static Long b(JSONObject jSONObject, String str) throws JSONException {
        if (jSONObject.has(str)) {
            return Long.valueOf(jSONObject.getLong(str));
        }
        return null;
    }

    public static <M extends g> List<M> a(JSONObject jSONObject, String str, i iVar) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        List<M> a2 = iVar.a(optJSONArray.length());
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
            g b2 = iVar.b();
            b2.a(jSONObject2);
            a2.add(b2);
        }
        return a2;
    }

    public static List<String> c(JSONObject jSONObject, String str) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(optJSONArray.length());
        for (int i = 0; i < optJSONArray.length(); i++) {
            arrayList.add(optJSONArray.getString(i));
        }
        return arrayList;
    }

    public static void a(JSONStringer jSONStringer, String str, Object obj) throws JSONException {
        if (obj != null) {
            jSONStringer.key(str).value(obj);
        }
    }

    public static void a(JSONStringer jSONStringer, String str, List<? extends g> list) throws JSONException {
        if (list != null) {
            jSONStringer.key(str).array();
            for (g a2 : list) {
                jSONStringer.object();
                a2.a(jSONStringer);
                jSONStringer.endObject();
            }
            jSONStringer.endArray();
        }
    }

    public static void b(JSONStringer jSONStringer, String str, List<String> list) throws JSONException {
        if (list != null) {
            jSONStringer.key(str).array();
            for (String value : list) {
                jSONStringer.value(value);
            }
            jSONStringer.endArray();
        }
    }
}
