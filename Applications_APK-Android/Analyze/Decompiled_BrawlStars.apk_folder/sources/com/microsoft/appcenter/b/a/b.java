package com.microsoft.appcenter.b.a;

import com.microsoft.appcenter.b.a.a.d;
import com.microsoft.appcenter.b.a.a.f;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: CustomPropertiesLog */
public class b extends a {

    /* renamed from: a  reason: collision with root package name */
    public Map<String, Object> f4584a;

    public final String a() {
        return "customProperties";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        Object obj;
        super.a(jSONObject);
        JSONArray jSONArray = jSONObject.getJSONArray("properties");
        HashMap hashMap = new HashMap();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            String string = jSONObject2.getString("name");
            String string2 = jSONObject2.getString("type");
            if (string2.equals("clear")) {
                obj = null;
            } else if (string2.equals("boolean")) {
                obj = Boolean.valueOf(jSONObject2.getBoolean("value"));
            } else if (string2.equals("number")) {
                obj = jSONObject2.get("value");
                if (!(obj instanceof Number)) {
                    throw new JSONException("Invalid value type");
                }
            } else if (string2.equals("dateTime")) {
                obj = d.a(jSONObject2.getString("value"));
            } else if (string2.equals("string")) {
                obj = jSONObject2.getString("value");
            } else {
                throw new JSONException("Invalid value type");
            }
            hashMap.put(string, obj);
        }
        this.f4584a = hashMap;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        Map<String, Object> map = this.f4584a;
        if (map != null) {
            jSONStringer.key("properties").array();
            for (Map.Entry next : map.entrySet()) {
                jSONStringer.object();
                f.a(jSONStringer, "name", next.getKey());
                Object value = next.getValue();
                if (value == null) {
                    f.a(jSONStringer, "type", "clear");
                } else if (value instanceof Boolean) {
                    f.a(jSONStringer, "type", "boolean");
                    f.a(jSONStringer, "value", value);
                } else if (value instanceof Number) {
                    f.a(jSONStringer, "type", "number");
                    f.a(jSONStringer, "value", value);
                } else if (value instanceof Date) {
                    f.a(jSONStringer, "type", "dateTime");
                    f.a(jSONStringer, "value", d.a((Date) value));
                } else if (value instanceof String) {
                    f.a(jSONStringer, "type", "string");
                    f.a(jSONStringer, "value", value);
                } else {
                    throw new JSONException("Invalid value type");
                }
                jSONStringer.endObject();
            }
            jSONStringer.endArray();
            return;
        }
        throw new JSONException("Properties cannot be null");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        Map<String, Object> map = this.f4584a;
        Map<String, Object> map2 = ((b) obj).f4584a;
        if (map != null) {
            return map.equals(map2);
        }
        if (map2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        Map<String, Object> map = this.f4584a;
        return hashCode + (map != null ? map.hashCode() : 0);
    }
}
