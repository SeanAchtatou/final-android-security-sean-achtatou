package com.microsoft.appcenter.b.a;

import java.util.List;

/* compiled from: LogContainer */
public class e {

    /* renamed from: a  reason: collision with root package name */
    public List<d> f4613a;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        List<d> list = this.f4613a;
        List<d> list2 = ((e) obj).f4613a;
        if (list != null) {
            return list.equals(list2);
        }
        if (list2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        List<d> list = this.f4613a;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }
}
