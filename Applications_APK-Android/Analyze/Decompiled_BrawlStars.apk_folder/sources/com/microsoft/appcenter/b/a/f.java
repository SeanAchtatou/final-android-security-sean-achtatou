package com.microsoft.appcenter.b.a;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: LogWithProperties */
public abstract class f extends a {
    public Map<String, String> c;

    public void a(JSONObject jSONObject) throws JSONException {
        HashMap hashMap;
        super.a(jSONObject);
        JSONObject optJSONObject = jSONObject.optJSONObject("properties");
        if (optJSONObject == null) {
            hashMap = null;
        } else {
            HashMap hashMap2 = new HashMap(optJSONObject.length());
            Iterator<String> keys = optJSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap2.put(next, optJSONObject.getString(next));
            }
            hashMap = hashMap2;
        }
        this.c = hashMap;
    }

    public void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        Map<String, String> map = this.c;
        if (map != null) {
            jSONStringer.key("properties").object();
            for (Map.Entry next : map.entrySet()) {
                jSONStringer.key((String) next.getKey()).value(next.getValue());
            }
            jSONStringer.endObject();
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        Map<String, String> map = this.c;
        Map<String, String> map2 = ((f) obj).c;
        if (map != null) {
            return map.equals(map2);
        }
        if (map2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        Map<String, String> map = this.c;
        return hashCode + (map != null ? map.hashCode() : 0);
    }
}
