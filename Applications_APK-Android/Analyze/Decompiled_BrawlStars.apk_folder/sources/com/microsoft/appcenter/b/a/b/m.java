package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: SdkExtension */
public class m implements g {

    /* renamed from: a  reason: collision with root package name */
    String f4601a;

    /* renamed from: b  reason: collision with root package name */
    public String f4602b;
    public Long c;
    public UUID d;

    public final void a(JSONObject jSONObject) throws JSONException {
        this.f4601a = jSONObject.optString("libVer", null);
        this.f4602b = jSONObject.optString("epoch", null);
        this.c = f.b(jSONObject, "seq");
        if (jSONObject.has("installId")) {
            this.d = UUID.fromString(jSONObject.getString("installId"));
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        m mVar = (m) obj;
        String str = this.f4601a;
        if (str == null ? mVar.f4601a != null : !str.equals(mVar.f4601a)) {
            return false;
        }
        String str2 = this.f4602b;
        if (str2 == null ? mVar.f4602b != null : !str2.equals(mVar.f4602b)) {
            return false;
        }
        Long l = this.c;
        if (l == null ? mVar.c != null : !l.equals(mVar.c)) {
            return false;
        }
        UUID uuid = this.d;
        UUID uuid2 = mVar.d;
        if (uuid != null) {
            return uuid.equals(uuid2);
        }
        if (uuid2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f4601a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f4602b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Long l = this.c;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        UUID uuid = this.d;
        if (uuid != null) {
            i = uuid.hashCode();
        }
        return hashCode3 + i;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "libVer", this.f4601a);
        f.a(jSONStringer, "epoch", this.f4602b);
        f.a(jSONStringer, "seq", this.c);
        f.a(jSONStringer, "installId", this.d);
    }
}
