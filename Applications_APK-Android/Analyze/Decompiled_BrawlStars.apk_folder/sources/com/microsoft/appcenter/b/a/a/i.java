package com.microsoft.appcenter.b.a.a;

import com.microsoft.appcenter.b.a.g;
import java.util.List;

/* compiled from: ModelFactory */
public interface i<M extends g> {
    List<M> a(int i);

    M b();
}
