package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.c.a;
import com.microsoft.appcenter.b.a.c.c;
import com.microsoft.appcenter.b.a.c.d;
import com.microsoft.appcenter.b.a.c.e;
import com.microsoft.appcenter.b.a.c.f;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: CommonSchemaDataUtils */
public class b {
    public static void a(List<f> list, c cVar) {
        Iterator<f> it;
        Object obj;
        Iterator<f> it2;
        c cVar2 = cVar;
        if (list != null) {
            try {
                d dVar = new d();
                cVar2.f = dVar;
                h hVar = new h();
                Iterator<f> it3 = list.iterator();
                while (true) {
                    int i = null;
                    if (it3.hasNext()) {
                        f next = it3.next();
                        try {
                            String str = next.f4612b;
                            if (str != null) {
                                if (str.equals("baseType")) {
                                    if (!(next instanceof e)) {
                                        throw new IllegalArgumentException("baseType must be a string.");
                                    }
                                }
                                if (str.startsWith("baseType.")) {
                                    Iterator<f> it4 = it3;
                                    throw new IllegalArgumentException("baseType must be a string.");
                                } else if (!str.equals("baseData")) {
                                    if (next instanceof e) {
                                        obj = ((e) next).f4611a;
                                    } else if (next instanceof d) {
                                        obj = Long.valueOf(((d) next).f4610a);
                                    } else if (next instanceof c) {
                                        obj = Double.valueOf(((c) next).f4609a);
                                    } else if (next instanceof com.microsoft.appcenter.b.a.c.b) {
                                        obj = com.microsoft.appcenter.b.a.a.d.a(((com.microsoft.appcenter.b.a.c.b) next).f4608a);
                                    } else if (next instanceof a) {
                                        obj = Boolean.valueOf(((a) next).f4607a);
                                    } else {
                                        Iterator<f> it5 = it3;
                                        throw new IllegalArgumentException("Unsupported property type: " + next.a());
                                    }
                                    if (obj != null) {
                                        if (next instanceof d) {
                                            i = 4;
                                        } else if (next instanceof c) {
                                            i = 6;
                                        } else if (next instanceof com.microsoft.appcenter.b.a.c.b) {
                                            i = 9;
                                        }
                                        String[] split = next.f4612b.split("\\.", -1);
                                        int length = split.length - 1;
                                        JSONObject jSONObject = dVar.f4589a;
                                        JSONObject jSONObject2 = hVar.f4594a;
                                        int i2 = 0;
                                        while (i2 < length) {
                                            String str2 = split[i2];
                                            JSONObject optJSONObject = jSONObject.optJSONObject(str2);
                                            if (optJSONObject == null) {
                                                it2 = it3;
                                                if (jSONObject.has(str2)) {
                                                    com.microsoft.appcenter.utils.a.d("AppCenter", "Property key '" + str2 + "' already has a value, the old value will be overridden.");
                                                }
                                                JSONObject jSONObject3 = new JSONObject();
                                                jSONObject.put(str2, jSONObject3);
                                                jSONObject = jSONObject3;
                                            } else {
                                                it2 = it3;
                                                jSONObject = optJSONObject;
                                            }
                                            JSONObject optJSONObject2 = jSONObject2.optJSONObject("f");
                                            if (optJSONObject2 == null) {
                                                optJSONObject2 = new JSONObject();
                                                jSONObject2.put("f", optJSONObject2);
                                            }
                                            jSONObject2 = optJSONObject2.optJSONObject(str2);
                                            if (jSONObject2 == null) {
                                                jSONObject2 = new JSONObject();
                                                optJSONObject2.put(str2, jSONObject2);
                                            }
                                            i2++;
                                            it3 = it2;
                                        }
                                        it = it3;
                                        String str3 = split[length];
                                        if (jSONObject.has(str3)) {
                                            com.microsoft.appcenter.utils.a.d("AppCenter", "Property key '" + str3 + "' already has a value, the old value will be overridden.");
                                        }
                                        jSONObject.put(str3, obj);
                                        JSONObject optJSONObject3 = jSONObject2.optJSONObject("f");
                                        if (i != null) {
                                            if (optJSONObject3 == null) {
                                                optJSONObject3 = new JSONObject();
                                                jSONObject2.put("f", optJSONObject3);
                                            }
                                            optJSONObject3.put(str3, i);
                                        } else if (optJSONObject3 != null) {
                                            optJSONObject3.remove(str3);
                                        }
                                        it3 = it;
                                    } else {
                                        it = it3;
                                        try {
                                            throw new IllegalArgumentException("Value of property with key '" + str + "' cannot be null.");
                                        } catch (IllegalArgumentException e) {
                                            e = e;
                                            com.microsoft.appcenter.utils.a.d("AppCenter", e.getMessage());
                                            it3 = it;
                                        }
                                    }
                                } else {
                                    Iterator<f> it6 = it3;
                                    throw new IllegalArgumentException("baseData must be an object.");
                                }
                            } else {
                                Iterator<f> it7 = it3;
                                throw new IllegalArgumentException("Property key cannot be null.");
                            }
                        } catch (IllegalArgumentException e2) {
                            e = e2;
                            it = it3;
                            com.microsoft.appcenter.utils.a.d("AppCenter", e.getMessage());
                            it3 = it;
                        }
                    } else {
                        JSONObject jSONObject4 = dVar.f4589a;
                        String optString = jSONObject4.optString("baseType", null);
                        JSONObject optJSONObject4 = jSONObject4.optJSONObject("baseData");
                        if (optString == null && optJSONObject4 != null) {
                            com.microsoft.appcenter.utils.a.d("AppCenter", "baseData was set but baseType is missing.");
                            jSONObject4.remove("baseData");
                            hVar.f4594a.optJSONObject("f").remove("baseData");
                        }
                        if (optString != null && optJSONObject4 == null) {
                            com.microsoft.appcenter.utils.a.d("AppCenter", "baseType was set but baseData is missing.");
                            jSONObject4.remove("baseType");
                        }
                        if (!a(hVar.f4594a)) {
                            if (cVar2.e == null) {
                                cVar2.e = new f();
                            }
                            cVar2.e.f4591a = hVar;
                            return;
                        }
                        return;
                    }
                }
            } catch (JSONException unused) {
            }
        }
    }

    private static boolean a(JSONObject jSONObject) {
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            JSONObject optJSONObject = jSONObject.optJSONObject(keys.next());
            if (optJSONObject != null && a(optJSONObject)) {
                keys.remove();
            }
        }
        return jSONObject.length() == 0;
    }
}
