package com.microsoft.appcenter.b.a;

import com.microsoft.appcenter.b.a.a.d;
import com.microsoft.appcenter.b.a.a.f;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: AbstractLog */
public abstract class a implements d {

    /* renamed from: a  reason: collision with root package name */
    private final Set<String> f4580a = new LinkedHashSet();

    /* renamed from: b  reason: collision with root package name */
    private String f4581b;
    public Date k;
    public UUID l;
    public String m;
    public c n;
    public Object o;

    public final Date b() {
        return this.k;
    }

    public final void a(Date date) {
        this.k = date;
    }

    public final UUID c() {
        return this.l;
    }

    public final void a(UUID uuid) {
        this.l = uuid;
    }

    public final String d() {
        return this.m;
    }

    public final c e() {
        return this.n;
    }

    public final void a(c cVar) {
        this.n = cVar;
    }

    public final Object f() {
        return this.o;
    }

    public final synchronized void a(String str) {
        this.f4580a.add(str);
    }

    public final synchronized Set<String> g() {
        return Collections.unmodifiableSet(this.f4580a);
    }

    public void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "type", a());
        jSONStringer.key("timestamp").value(d.a(this.k));
        f.a(jSONStringer, "sid", this.l);
        f.a(jSONStringer, "distributionGroupId", this.f4581b);
        f.a(jSONStringer, "userId", this.m);
        if (this.n != null) {
            jSONStringer.key("device").object();
            this.n.a(jSONStringer);
            jSONStringer.endObject();
        }
    }

    public void a(JSONObject jSONObject) throws JSONException {
        if (jSONObject.getString("type").equals(a())) {
            this.k = d.a(jSONObject.getString("timestamp"));
            if (jSONObject.has("sid")) {
                this.l = UUID.fromString(jSONObject.getString("sid"));
            }
            this.f4581b = jSONObject.optString("distributionGroupId", null);
            this.m = jSONObject.optString("userId", null);
            if (jSONObject.has("device")) {
                c cVar = new c();
                cVar.a(jSONObject.getJSONObject("device"));
                this.n = cVar;
                return;
            }
            return;
        }
        throw new JSONException("Invalid type");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        if (!this.f4580a.equals(aVar.f4580a)) {
            return false;
        }
        Date date = this.k;
        if (date == null ? aVar.k != null : !date.equals(aVar.k)) {
            return false;
        }
        UUID uuid = this.l;
        if (uuid == null ? aVar.l != null : !uuid.equals(aVar.l)) {
            return false;
        }
        String str = this.f4581b;
        if (str == null ? aVar.f4581b != null : !str.equals(aVar.f4581b)) {
            return false;
        }
        String str2 = this.m;
        if (str2 == null ? aVar.m != null : !str2.equals(aVar.m)) {
            return false;
        }
        c cVar = this.n;
        if (cVar == null ? aVar.n != null : !cVar.equals(aVar.n)) {
            return false;
        }
        Object obj2 = this.o;
        Object obj3 = aVar.o;
        if (obj2 != null) {
            return obj2.equals(obj3);
        }
        if (obj3 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = this.f4580a.hashCode() * 31;
        Date date = this.k;
        int i = 0;
        int hashCode2 = (hashCode + (date != null ? date.hashCode() : 0)) * 31;
        UUID uuid = this.l;
        int hashCode3 = (hashCode2 + (uuid != null ? uuid.hashCode() : 0)) * 31;
        String str = this.f4581b;
        int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.m;
        int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
        c cVar = this.n;
        int hashCode6 = (hashCode5 + (cVar != null ? cVar.hashCode() : 0)) * 31;
        Object obj = this.o;
        if (obj != null) {
            i = obj.hashCode();
        }
        return hashCode6 + i;
    }
}
