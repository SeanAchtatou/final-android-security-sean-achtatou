package com.microsoft.appcenter.b;

import android.content.Context;
import com.microsoft.appcenter.b.a.a.h;
import com.microsoft.appcenter.b.a.d;
import com.microsoft.appcenter.b.a.e;
import com.microsoft.appcenter.http.f;
import com.microsoft.appcenter.http.k;
import com.microsoft.appcenter.http.l;
import com.microsoft.appcenter.http.m;
import com.microsoft.appcenter.o;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OneCollectorIngestion */
public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private final h f4615a;

    /* renamed from: b  reason: collision with root package name */
    private final f f4616b;
    private String c = "https://mobile.events.data.microsoft.com/OneCollector/1.0";

    public c(Context context, h hVar) {
        this.f4615a = hVar;
        this.f4616b = k.a(context);
    }

    public final l a(String str, String str2, UUID uuid, e eVar, m mVar) throws IllegalArgumentException {
        HashMap hashMap = new HashMap();
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();
        for (d g : eVar.f4613a) {
            linkedHashSet.addAll(g.g());
        }
        StringBuilder sb = new StringBuilder();
        for (String append : linkedHashSet) {
            sb.append(append);
            sb.append(",");
        }
        if (!linkedHashSet.isEmpty()) {
            sb.deleteCharAt(sb.length() - 1);
        }
        hashMap.put("apikey", sb.toString());
        JSONObject jSONObject = new JSONObject();
        Iterator<d> it = eVar.f4613a.iterator();
        while (it.hasNext()) {
            List<String> list = ((com.microsoft.appcenter.b.a.b.c) it.next()).e.f4592b.f4599a;
            if (list != null) {
                for (String next : list) {
                    String a2 = com.microsoft.appcenter.utils.h.a(next);
                    if (a2 != null) {
                        try {
                            jSONObject.put(next, a2);
                        } catch (JSONException e) {
                            com.microsoft.appcenter.utils.a.c("AppCenter", "Cannot serialize tickets, sending log anonymously", e);
                        }
                    }
                }
            }
        }
        if (jSONObject.length() > 0) {
            hashMap.put("Tickets", jSONObject.toString());
            if (o.f4702b) {
                hashMap.put("Strict", Boolean.TRUE.toString());
            }
        }
        hashMap.put("Content-Type", "application/x-json-stream; charset=utf-8");
        hashMap.put("Client-Version", String.format("ACS-Android-Java-no-%s-no", "2.3.0"));
        hashMap.put("Upload-Time", String.valueOf(System.currentTimeMillis()));
        return this.f4616b.a(this.c, "POST", hashMap, new a(this.f4615a, eVar), mVar);
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a() {
        this.f4616b.a();
    }

    public void close() throws IOException {
        this.f4616b.close();
    }

    /* compiled from: OneCollectorIngestion */
    static class a implements f.a {

        /* renamed from: a  reason: collision with root package name */
        private final h f4617a;

        /* renamed from: b  reason: collision with root package name */
        private final e f4618b;

        a(h hVar, e eVar) {
            this.f4617a = hVar;
            this.f4618b = eVar;
        }

        public final String a() throws JSONException {
            StringBuilder sb = new StringBuilder();
            for (d a2 : this.f4618b.f4613a) {
                sb.append(this.f4617a.a(a2));
                sb.append(10);
            }
            return sb.toString();
        }

        public final void a(URL url, Map<String, String> map) {
            if (com.microsoft.appcenter.utils.a.a() <= 2) {
                com.microsoft.appcenter.utils.a.a("AppCenter", "Calling " + url + "...");
                HashMap hashMap = new HashMap(map);
                String str = (String) hashMap.get("apikey");
                if (str != null) {
                    hashMap.put("apikey", k.b(str));
                }
                String str2 = (String) hashMap.get("Tickets");
                if (str2 != null) {
                    hashMap.put("Tickets", k.c(str2));
                }
                com.microsoft.appcenter.utils.a.a("AppCenter", "Headers: " + hashMap);
            }
        }
    }
}
