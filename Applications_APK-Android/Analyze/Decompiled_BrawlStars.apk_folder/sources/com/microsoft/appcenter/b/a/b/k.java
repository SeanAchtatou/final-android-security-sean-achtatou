package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.c;
import com.microsoft.appcenter.b.a.d;
import java.util.Locale;
import java.util.regex.Pattern;

/* compiled from: PartAUtils */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f4598a = Pattern.compile("^[a-zA-Z0-9]((\\.(?!(\\.|$)))|[_a-zA-Z0-9]){3,99}$");

    public static String a(String str) {
        return str.split("-")[0];
    }

    public static void a(c cVar, String str) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException("Name cannot be null.");
        } else if (f4598a.matcher(str).matches()) {
            cVar.f4588b = str;
        } else {
            throw new IllegalArgumentException("Name must match '" + f4598a + "' but was '" + str + "'.");
        }
    }

    public static void a(d dVar, c cVar, String str) {
        c e = dVar.e();
        cVar.f4587a = "3.0";
        cVar.k = dVar.b();
        cVar.c = "o:" + a(str);
        cVar.a(str);
        if (cVar.e == null) {
            cVar.e = new f();
        }
        cVar.e.f4592b = new l();
        cVar.e.f4592b.c = e.c;
        cVar.e.f4592b.f4600b = e.d;
        cVar.e.c = new n();
        n nVar = cVar.e.c;
        String d = dVar.d();
        if (d != null && !d.contains(":")) {
            d = "c:" + d;
        }
        nVar.f4603a = d;
        String str2 = "-";
        cVar.e.c.f4604b = e.i.replace("_", str2);
        cVar.e.e = new j();
        cVar.e.e.f4596a = e.e;
        cVar.e.e.f4597b = e.f + str2 + e.g + str2 + e.h;
        cVar.e.f = new a();
        cVar.e.f.f4586b = e.l;
        cVar.e.f.f4585a = "a:" + e.p;
        cVar.e.g = new i();
        cVar.e.g.f4595a = e.m;
        cVar.e.h = new m();
        cVar.e.h.f4601a = e.f4605a + str2 + e.f4606b;
        cVar.e.i = new g();
        Locale locale = Locale.US;
        Object[] objArr = new Object[3];
        if (e.j.intValue() >= 0) {
            str2 = "+";
        }
        objArr[0] = str2;
        objArr[1] = Integer.valueOf(Math.abs(e.j.intValue() / 60));
        objArr[2] = Integer.valueOf(Math.abs(e.j.intValue() % 60));
        cVar.e.i.f4593a = String.format(locale, "%s%02d:%02d", objArr);
        cVar.e.d = new e();
    }
}
