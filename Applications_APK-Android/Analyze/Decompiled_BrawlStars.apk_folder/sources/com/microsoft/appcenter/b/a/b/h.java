package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.g;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: MetadataExtension */
public class h implements g {

    /* renamed from: a  reason: collision with root package name */
    JSONObject f4594a = new JSONObject();

    public final void a(JSONObject jSONObject) {
        this.f4594a = jSONObject;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        Iterator<String> keys = this.f4594a.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            jSONStringer.key(next).value(this.f4594a.get(next));
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f4594a.toString().equals(((h) obj).f4594a.toString());
    }

    public int hashCode() {
        return this.f4594a.toString().hashCode();
    }
}
