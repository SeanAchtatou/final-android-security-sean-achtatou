package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: Data */
public class d implements g {

    /* renamed from: a  reason: collision with root package name */
    final JSONObject f4589a = new JSONObject();

    public final void a(JSONObject jSONObject) throws JSONException {
        JSONArray names = jSONObject.names();
        if (names != null) {
            for (int i = 0; i < names.length(); i++) {
                String string = names.getString(i);
                this.f4589a.put(string, jSONObject.get(string));
            }
        }
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "baseType", this.f4589a.optString("baseType", null));
        f.a(jSONStringer, "baseData", this.f4589a.optJSONObject("baseData"));
        JSONArray names = this.f4589a.names();
        if (names != null) {
            for (int i = 0; i < names.length(); i++) {
                String string = names.getString(i);
                if (!string.equals("baseType") && !string.equals("baseData")) {
                    jSONStringer.key(string).value(this.f4589a.get(string));
                }
            }
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f4589a.toString().equals(((d) obj).f4589a.toString());
    }

    public int hashCode() {
        return this.f4589a.toString().hashCode();
    }
}
