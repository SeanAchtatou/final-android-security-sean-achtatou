package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: UserExtension */
public class n implements g {

    /* renamed from: a  reason: collision with root package name */
    public String f4603a;

    /* renamed from: b  reason: collision with root package name */
    String f4604b;

    public final void a(JSONObject jSONObject) {
        this.f4603a = jSONObject.optString("localId", null);
        this.f4604b = jSONObject.optString("locale", null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        n nVar = (n) obj;
        String str = this.f4603a;
        if (str == null ? nVar.f4603a != null : !str.equals(nVar.f4603a)) {
            return false;
        }
        String str2 = this.f4604b;
        String str3 = nVar.f4604b;
        if (str2 != null) {
            return str2.equals(str3);
        }
        if (str3 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f4603a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f4604b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "localId", this.f4603a);
        f.a(jSONStringer, "locale", this.f4604b);
    }
}
