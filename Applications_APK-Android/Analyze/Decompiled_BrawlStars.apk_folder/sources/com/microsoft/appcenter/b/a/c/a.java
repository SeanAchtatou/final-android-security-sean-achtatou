package com.microsoft.appcenter.b.a.c;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: BooleanTypedProperty */
public class a extends f {

    /* renamed from: a  reason: collision with root package name */
    public boolean f4607a;

    public final String a() {
        return "boolean";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4607a = jSONObject.getBoolean("value");
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        jSONStringer.key("value").value(this.f4607a);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj) || this.f4607a != ((a) obj).f4607a) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (super.hashCode() * 31) + (this.f4607a ? 1 : 0);
    }
}
