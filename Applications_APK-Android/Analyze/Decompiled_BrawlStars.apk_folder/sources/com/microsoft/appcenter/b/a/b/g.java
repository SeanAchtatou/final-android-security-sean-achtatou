package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.a.f;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: LocExtension */
public class g implements com.microsoft.appcenter.b.a.g {

    /* renamed from: a  reason: collision with root package name */
    String f4593a;

    public final void a(JSONObject jSONObject) {
        this.f4593a = jSONObject.optString("tz", null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        String str = this.f4593a;
        String str2 = ((g) obj).f4593a;
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f4593a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "tz", this.f4593a);
    }
}
