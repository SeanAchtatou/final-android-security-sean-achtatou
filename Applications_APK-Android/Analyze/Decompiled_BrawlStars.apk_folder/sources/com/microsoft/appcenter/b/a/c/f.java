package com.microsoft.appcenter.b.a.c;

import com.microsoft.appcenter.b.a.g;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: TypedProperty */
public abstract class f implements g {

    /* renamed from: b  reason: collision with root package name */
    public String f4612b;

    public abstract String a();

    public void a(JSONObject jSONObject) throws JSONException {
        if (jSONObject.getString("type").equals(a())) {
            this.f4612b = jSONObject.getString("name");
            return;
        }
        throw new JSONException("Invalid type");
    }

    public void a(JSONStringer jSONStringer) throws JSONException {
        jSONStringer.key("type").value(a());
        jSONStringer.key("name").value(this.f4612b);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        String str = this.f4612b;
        String str2 = ((f) obj).f4612b;
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f4612b;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }
}
