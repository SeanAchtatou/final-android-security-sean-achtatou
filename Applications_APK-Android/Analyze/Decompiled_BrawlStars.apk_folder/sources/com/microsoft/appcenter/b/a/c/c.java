package com.microsoft.appcenter.b.a.c;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: DoubleTypedProperty */
public class c extends f {

    /* renamed from: a  reason: collision with root package name */
    public double f4609a;

    public final String a() {
        return "double";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4609a = jSONObject.getDouble("value");
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        jSONStringer.key("value").value(this.f4609a);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj) || Double.compare(((c) obj).f4609a, this.f4609a) != 0) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = super.hashCode();
        long doubleToLongBits = Double.doubleToLongBits(this.f4609a);
        return (hashCode * 31) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)));
    }
}
