package com.microsoft.appcenter.b.a.c;

import com.microsoft.appcenter.b.a.a.d;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: DateTimeTypedProperty */
public class b extends f {

    /* renamed from: a  reason: collision with root package name */
    public Date f4608a;

    public final String a() {
        return "dateTime";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4608a = d.a(jSONObject.getString("value"));
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        jSONStringer.key("value").value(d.a(this.f4608a));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        Date date = this.f4608a;
        Date date2 = ((b) obj).f4608a;
        if (date != null) {
            return date.equals(date2);
        }
        if (date2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        Date date = this.f4608a;
        return hashCode + (date != null ? date.hashCode() : 0);
    }
}
