package com.microsoft.appcenter.b.a;

import com.microsoft.appcenter.b.a.a.f;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: StartServiceLog */
public class h extends a {

    /* renamed from: a  reason: collision with root package name */
    public List<String> f4614a;

    public final String a() {
        return "startService";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4614a = f.c(jSONObject, "services");
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        f.b(jSONStringer, "services", this.f4614a);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        List<String> list = this.f4614a;
        List<String> list2 = ((h) obj).f4614a;
        if (list != null) {
            return list.equals(list2);
        }
        if (list2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        List<String> list = this.f4614a;
        return hashCode + (list != null ? list.hashCode() : 0);
    }
}
