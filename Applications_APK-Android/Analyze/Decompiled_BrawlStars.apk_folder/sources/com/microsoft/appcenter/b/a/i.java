package com.microsoft.appcenter.b.a;

import com.microsoft.appcenter.b.a.a.f;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: WrapperSdk */
public class i implements g {
    public String q;
    public String r;
    public String s;
    public String t;
    public String u;
    public String v;

    public void a(JSONObject jSONObject) throws JSONException {
        this.q = jSONObject.optString("wrapperSdkVersion", null);
        this.r = jSONObject.optString("wrapperSdkName", null);
        this.s = jSONObject.optString("wrapperRuntimeVersion", null);
        this.t = jSONObject.optString("liveUpdateReleaseLabel", null);
        this.u = jSONObject.optString("liveUpdateDeploymentKey", null);
        this.v = jSONObject.optString("liveUpdatePackageHash", null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        i iVar = (i) obj;
        String str = this.q;
        if (str == null ? iVar.q != null : !str.equals(iVar.q)) {
            return false;
        }
        String str2 = this.r;
        if (str2 == null ? iVar.r != null : !str2.equals(iVar.r)) {
            return false;
        }
        String str3 = this.s;
        if (str3 == null ? iVar.s != null : !str3.equals(iVar.s)) {
            return false;
        }
        String str4 = this.t;
        if (str4 == null ? iVar.t != null : !str4.equals(iVar.t)) {
            return false;
        }
        String str5 = this.u;
        if (str5 == null ? iVar.u != null : !str5.equals(iVar.u)) {
            return false;
        }
        String str6 = this.v;
        String str7 = iVar.v;
        if (str6 != null) {
            return str6.equals(str7);
        }
        if (str7 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.q;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.r;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.s;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.t;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.u;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.v;
        if (str6 != null) {
            i = str6.hashCode();
        }
        return hashCode5 + i;
    }

    public void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "wrapperSdkVersion", this.q);
        f.a(jSONStringer, "wrapperSdkName", this.r);
        f.a(jSONStringer, "wrapperRuntimeVersion", this.s);
        f.a(jSONStringer, "liveUpdateReleaseLabel", this.t);
        f.a(jSONStringer, "liveUpdateDeploymentKey", this.u);
        f.a(jSONStringer, "liveUpdatePackageHash", this.v);
    }
}
