package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: NetExtension */
public class i implements g {

    /* renamed from: a  reason: collision with root package name */
    String f4595a;

    public final void a(JSONObject jSONObject) {
        this.f4595a = jSONObject.optString("provider", null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        String str = this.f4595a;
        String str2 = ((i) obj).f4595a;
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f4595a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "provider", this.f4595a);
    }
}
