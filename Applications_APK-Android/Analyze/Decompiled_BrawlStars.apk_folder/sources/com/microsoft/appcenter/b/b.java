package com.microsoft.appcenter.b;

import com.microsoft.appcenter.b.a.e;
import com.microsoft.appcenter.http.l;
import com.microsoft.appcenter.http.m;
import java.io.Closeable;
import java.util.UUID;

/* compiled from: Ingestion */
public interface b extends Closeable {
    l a(String str, String str2, UUID uuid, e eVar, m mVar) throws IllegalArgumentException;

    void a();

    void a(String str);
}
