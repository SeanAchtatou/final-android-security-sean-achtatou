package com.microsoft.appcenter.b.a;

import com.microsoft.appcenter.b.a.a.f;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: Device */
public class c extends i {

    /* renamed from: a  reason: collision with root package name */
    public String f4605a;

    /* renamed from: b  reason: collision with root package name */
    public String f4606b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public Integer h;
    public String i;
    public Integer j;
    public String k;
    public String l;
    public String m;
    public String n;
    public String o;
    public String p;

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4605a = jSONObject.getString("sdkName");
        this.f4606b = jSONObject.getString("sdkVersion");
        this.c = jSONObject.getString("model");
        this.d = jSONObject.getString("oemName");
        this.e = jSONObject.getString("osName");
        this.f = jSONObject.getString("osVersion");
        this.g = jSONObject.optString("osBuild", null);
        this.h = f.a(jSONObject, "osApiLevel");
        this.i = jSONObject.getString("locale");
        this.j = Integer.valueOf(jSONObject.getInt("timeZoneOffset"));
        this.k = jSONObject.getString("screenSize");
        this.l = jSONObject.getString("appVersion");
        this.m = jSONObject.optString("carrierName", null);
        this.n = jSONObject.optString("carrierCountry", null);
        this.o = jSONObject.getString("appBuild");
        this.p = jSONObject.optString("appNamespace", null);
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        jSONStringer.key("sdkName").value(this.f4605a);
        jSONStringer.key("sdkVersion").value(this.f4606b);
        jSONStringer.key("model").value(this.c);
        jSONStringer.key("oemName").value(this.d);
        jSONStringer.key("osName").value(this.e);
        jSONStringer.key("osVersion").value(this.f);
        f.a(jSONStringer, "osBuild", this.g);
        f.a(jSONStringer, "osApiLevel", this.h);
        jSONStringer.key("locale").value(this.i);
        jSONStringer.key("timeZoneOffset").value(this.j);
        jSONStringer.key("screenSize").value(this.k);
        jSONStringer.key("appVersion").value(this.l);
        f.a(jSONStringer, "carrierName", this.m);
        f.a(jSONStringer, "carrierCountry", this.n);
        jSONStringer.key("appBuild").value(this.o);
        f.a(jSONStringer, "appNamespace", this.p);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        c cVar = (c) obj;
        String str = this.f4605a;
        if (str == null ? cVar.f4605a != null : !str.equals(cVar.f4605a)) {
            return false;
        }
        String str2 = this.f4606b;
        if (str2 == null ? cVar.f4606b != null : !str2.equals(cVar.f4606b)) {
            return false;
        }
        String str3 = this.c;
        if (str3 == null ? cVar.c != null : !str3.equals(cVar.c)) {
            return false;
        }
        String str4 = this.d;
        if (str4 == null ? cVar.d != null : !str4.equals(cVar.d)) {
            return false;
        }
        String str5 = this.e;
        if (str5 == null ? cVar.e != null : !str5.equals(cVar.e)) {
            return false;
        }
        String str6 = this.f;
        if (str6 == null ? cVar.f != null : !str6.equals(cVar.f)) {
            return false;
        }
        String str7 = this.g;
        if (str7 == null ? cVar.g != null : !str7.equals(cVar.g)) {
            return false;
        }
        Integer num = this.h;
        if (num == null ? cVar.h != null : !num.equals(cVar.h)) {
            return false;
        }
        String str8 = this.i;
        if (str8 == null ? cVar.i != null : !str8.equals(cVar.i)) {
            return false;
        }
        Integer num2 = this.j;
        if (num2 == null ? cVar.j != null : !num2.equals(cVar.j)) {
            return false;
        }
        String str9 = this.k;
        if (str9 == null ? cVar.k != null : !str9.equals(cVar.k)) {
            return false;
        }
        String str10 = this.l;
        if (str10 == null ? cVar.l != null : !str10.equals(cVar.l)) {
            return false;
        }
        String str11 = this.m;
        if (str11 == null ? cVar.m != null : !str11.equals(cVar.m)) {
            return false;
        }
        String str12 = this.n;
        if (str12 == null ? cVar.n != null : !str12.equals(cVar.n)) {
            return false;
        }
        String str13 = this.o;
        if (str13 == null ? cVar.o != null : !str13.equals(cVar.o)) {
            return false;
        }
        String str14 = this.p;
        String str15 = cVar.p;
        if (str14 != null) {
            return str14.equals(str15);
        }
        if (str15 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.f4605a;
        int i2 = 0;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.f4606b;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.f;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.g;
        int hashCode8 = (hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31;
        Integer num = this.h;
        int hashCode9 = (hashCode8 + (num != null ? num.hashCode() : 0)) * 31;
        String str8 = this.i;
        int hashCode10 = (hashCode9 + (str8 != null ? str8.hashCode() : 0)) * 31;
        Integer num2 = this.j;
        int hashCode11 = (hashCode10 + (num2 != null ? num2.hashCode() : 0)) * 31;
        String str9 = this.k;
        int hashCode12 = (hashCode11 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.l;
        int hashCode13 = (hashCode12 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.m;
        int hashCode14 = (hashCode13 + (str11 != null ? str11.hashCode() : 0)) * 31;
        String str12 = this.n;
        int hashCode15 = (hashCode14 + (str12 != null ? str12.hashCode() : 0)) * 31;
        String str13 = this.o;
        int hashCode16 = (hashCode15 + (str13 != null ? str13.hashCode() : 0)) * 31;
        String str14 = this.p;
        if (str14 != null) {
            i2 = str14.hashCode();
        }
        return hashCode16 + i2;
    }
}
