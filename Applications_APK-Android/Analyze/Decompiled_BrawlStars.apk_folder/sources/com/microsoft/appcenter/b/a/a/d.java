package com.microsoft.appcenter.b.a.a;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import org.json.JSONException;

/* compiled from: JSONDateUtils */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadLocal<DateFormat> f4583a = new e();

    private static void a(Object obj) throws JSONException {
        if (obj == null) {
            throw new JSONException("date cannot be null");
        }
    }

    public static String a(Date date) throws JSONException {
        a((Object) date);
        return f4583a.get().format(date);
    }

    public static Date a(String str) throws JSONException {
        a((Object) str);
        try {
            return f4583a.get().parse(str);
        } catch (ParseException e) {
            throw new JSONException(e.getMessage());
        }
    }
}
