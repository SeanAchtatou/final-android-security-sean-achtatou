package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: AppExtension */
public class a implements g {

    /* renamed from: a  reason: collision with root package name */
    String f4585a;

    /* renamed from: b  reason: collision with root package name */
    public String f4586b;
    public String c;
    public String d;
    private String e;

    public final void a(JSONObject jSONObject) {
        this.f4585a = jSONObject.optString("id", null);
        this.f4586b = jSONObject.optString("ver", null);
        this.c = jSONObject.optString("name", null);
        this.d = jSONObject.optString("locale", null);
        this.e = jSONObject.optString("userId", null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        String str = this.f4585a;
        if (str == null ? aVar.f4585a != null : !str.equals(aVar.f4585a)) {
            return false;
        }
        String str2 = this.f4586b;
        if (str2 == null ? aVar.f4586b != null : !str2.equals(aVar.f4586b)) {
            return false;
        }
        String str3 = this.c;
        if (str3 == null ? aVar.c != null : !str3.equals(aVar.c)) {
            return false;
        }
        String str4 = this.d;
        if (str4 == null ? aVar.d != null : !str4.equals(aVar.d)) {
            return false;
        }
        String str5 = this.e;
        String str6 = aVar.e;
        if (str5 != null) {
            return str5.equals(str6);
        }
        if (str6 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f4585a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f4586b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "id", this.f4585a);
        f.a(jSONStringer, "ver", this.f4586b);
        f.a(jSONStringer, "name", this.c);
        f.a(jSONStringer, "locale", this.d);
        f.a(jSONStringer, "userId", this.e);
    }
}
