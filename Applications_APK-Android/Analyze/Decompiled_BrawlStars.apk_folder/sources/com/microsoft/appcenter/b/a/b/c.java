package com.microsoft.appcenter.b.a.b;

import com.facebook.share.internal.ShareConstants;
import com.microsoft.appcenter.b.a.a;
import com.microsoft.appcenter.b.a.a.d;
import com.microsoft.appcenter.b.a.a.f;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: CommonSchemaLog */
public abstract class c extends a {

    /* renamed from: a  reason: collision with root package name */
    String f4587a;

    /* renamed from: b  reason: collision with root package name */
    String f4588b;
    public String c;
    public Long d;
    public f e;
    d f;
    private Double g;
    private String h;

    public final void a(JSONObject jSONObject) throws JSONException {
        this.f4587a = jSONObject.getString("ver");
        this.f4588b = jSONObject.getString("name");
        this.k = d.a(jSONObject.getString("time"));
        if (jSONObject.has("popSample")) {
            this.g = Double.valueOf(jSONObject.getDouble("popSample"));
        }
        this.c = jSONObject.optString("iKey", null);
        this.d = f.b(jSONObject, "flags");
        this.h = jSONObject.optString("cV", null);
        if (jSONObject.has("ext")) {
            f fVar = new f();
            fVar.a(jSONObject.getJSONObject("ext"));
            this.e = fVar;
        }
        if (jSONObject.has(ShareConstants.WEB_DIALOG_PARAM_DATA)) {
            d dVar = new d();
            dVar.a(jSONObject.getJSONObject(ShareConstants.WEB_DIALOG_PARAM_DATA));
            this.f = dVar;
        }
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        jSONStringer.key("ver").value(this.f4587a);
        jSONStringer.key("name").value(this.f4588b);
        jSONStringer.key("time").value(d.a(this.k));
        f.a(jSONStringer, "popSample", this.g);
        f.a(jSONStringer, "iKey", this.c);
        f.a(jSONStringer, "flags", this.d);
        f.a(jSONStringer, "cV", this.h);
        if (this.e != null) {
            jSONStringer.key("ext").object();
            this.e.a(jSONStringer);
            jSONStringer.endObject();
        }
        if (this.f != null) {
            jSONStringer.key(ShareConstants.WEB_DIALOG_PARAM_DATA).object();
            this.f.a(jSONStringer);
            jSONStringer.endObject();
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        c cVar = (c) obj;
        String str = this.f4587a;
        if (str == null ? cVar.f4587a != null : !str.equals(cVar.f4587a)) {
            return false;
        }
        String str2 = this.f4588b;
        if (str2 == null ? cVar.f4588b != null : !str2.equals(cVar.f4588b)) {
            return false;
        }
        Double d2 = this.g;
        if (d2 == null ? cVar.g != null : !d2.equals(cVar.g)) {
            return false;
        }
        String str3 = this.c;
        if (str3 == null ? cVar.c != null : !str3.equals(cVar.c)) {
            return false;
        }
        Long l = this.d;
        if (l == null ? cVar.d != null : !l.equals(cVar.d)) {
            return false;
        }
        String str4 = this.h;
        if (str4 == null ? cVar.h != null : !str4.equals(cVar.h)) {
            return false;
        }
        f fVar = this.e;
        if (fVar == null ? cVar.e != null : !fVar.equals(cVar.e)) {
            return false;
        }
        d dVar = this.f;
        d dVar2 = cVar.f;
        if (dVar != null) {
            return dVar.equals(dVar2);
        }
        if (dVar2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.f4587a;
        int i = 0;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.f4588b;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Double d2 = this.g;
        int hashCode4 = (hashCode3 + (d2 != null ? d2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Long l = this.d;
        int hashCode6 = (hashCode5 + (l != null ? l.hashCode() : 0)) * 31;
        String str4 = this.h;
        int hashCode7 = (hashCode6 + (str4 != null ? str4.hashCode() : 0)) * 31;
        f fVar = this.e;
        int hashCode8 = (hashCode7 + (fVar != null ? fVar.hashCode() : 0)) * 31;
        d dVar = this.f;
        if (dVar != null) {
            i = dVar.hashCode();
        }
        return hashCode8 + i;
    }
}
