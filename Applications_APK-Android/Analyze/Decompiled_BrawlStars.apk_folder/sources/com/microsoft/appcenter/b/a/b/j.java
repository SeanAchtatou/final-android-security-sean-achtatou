package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: OsExtension */
public class j implements g {

    /* renamed from: a  reason: collision with root package name */
    String f4596a;

    /* renamed from: b  reason: collision with root package name */
    String f4597b;

    public final void a(JSONObject jSONObject) {
        this.f4596a = jSONObject.optString("name", null);
        this.f4597b = jSONObject.optString("ver", null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        j jVar = (j) obj;
        String str = this.f4596a;
        if (str == null ? jVar.f4596a != null : !str.equals(jVar.f4596a)) {
            return false;
        }
        String str2 = this.f4597b;
        String str3 = jVar.f4597b;
        if (str2 != null) {
            return str2.equals(str3);
        }
        if (str3 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f4596a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f4597b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "name", this.f4596a);
        f.a(jSONStringer, "ver", this.f4597b);
    }
}
