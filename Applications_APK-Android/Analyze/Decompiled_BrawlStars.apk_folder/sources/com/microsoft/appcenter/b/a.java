package com.microsoft.appcenter.b;

import android.content.Context;
import com.microsoft.appcenter.b.a.a.h;
import com.microsoft.appcenter.b.a.e;
import com.microsoft.appcenter.http.f;
import com.microsoft.appcenter.http.k;
import com.microsoft.appcenter.http.l;
import com.microsoft.appcenter.http.m;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;
import org.json.JSONException;

/* compiled from: AppCenterIngestion */
public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    private final h f4576a;

    /* renamed from: b  reason: collision with root package name */
    private final f f4577b;
    private String c = "https://in.appcenter.ms";

    public a(Context context, h hVar) {
        this.f4576a = hVar;
        this.f4577b = k.a(context);
    }

    public final void a(String str) {
        this.c = str;
    }

    public final l a(String str, String str2, UUID uuid, e eVar, m mVar) throws IllegalArgumentException {
        HashMap hashMap = new HashMap();
        hashMap.put("Install-ID", uuid.toString());
        hashMap.put("App-Secret", str2);
        if (str != null) {
            hashMap.put("Authorization", String.format("Bearer %s", str));
        }
        C0156a aVar = new C0156a(this.f4576a, eVar);
        f fVar = this.f4577b;
        return fVar.a(this.c + "/logs?api-version=1.0.0", "POST", hashMap, aVar, mVar);
    }

    public void close() throws IOException {
        this.f4577b.close();
    }

    public final void a() {
        this.f4577b.a();
    }

    /* renamed from: com.microsoft.appcenter.b.a$a  reason: collision with other inner class name */
    /* compiled from: AppCenterIngestion */
    static class C0156a extends com.microsoft.appcenter.http.a {

        /* renamed from: a  reason: collision with root package name */
        private final h f4578a;

        /* renamed from: b  reason: collision with root package name */
        private final e f4579b;

        C0156a(h hVar, e eVar) {
            this.f4578a = hVar;
            this.f4579b = eVar;
        }

        public final String a() throws JSONException {
            return this.f4578a.a(this.f4579b);
        }
    }
}
