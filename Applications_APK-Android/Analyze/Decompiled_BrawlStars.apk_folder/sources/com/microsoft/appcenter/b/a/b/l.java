package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: ProtocolExtension */
public class l implements g {

    /* renamed from: a  reason: collision with root package name */
    public List<String> f4599a;

    /* renamed from: b  reason: collision with root package name */
    String f4600b;
    String c;

    public final void a(JSONObject jSONObject) throws JSONException {
        this.f4599a = f.c(jSONObject, "ticketKeys");
        this.f4600b = jSONObject.optString("devMake", null);
        this.c = jSONObject.optString("devModel", null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        l lVar = (l) obj;
        List<String> list = this.f4599a;
        if (list == null ? lVar.f4599a != null : !list.equals(lVar.f4599a)) {
            return false;
        }
        String str = this.f4600b;
        if (str == null ? lVar.f4600b != null : !str.equals(lVar.f4600b)) {
            return false;
        }
        String str2 = this.c;
        String str3 = lVar.c;
        if (str2 != null) {
            return str2.equals(str3);
        }
        if (str3 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        List<String> list = this.f4599a;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        String str = this.f4600b;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.c;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.b(jSONStringer, "ticketKeys", this.f4599a);
        f.a(jSONStringer, "devMake", this.f4600b);
        f.a(jSONStringer, "devModel", this.c);
    }
}
