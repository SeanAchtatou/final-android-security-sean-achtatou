package com.microsoft.appcenter.b.a.b;

import com.microsoft.appcenter.b.a.a.f;
import com.microsoft.appcenter.b.a.g;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: DeviceExtension */
public class e implements g {

    /* renamed from: a  reason: collision with root package name */
    public String f4590a;

    public final void a(JSONObject jSONObject) {
        this.f4590a = jSONObject.optString("localId", null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        String str = this.f4590a;
        String str2 = ((e) obj).f4590a;
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f4590a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        f.a(jSONStringer, "localId", this.f4590a);
    }
}
