package com.microsoft.appcenter.b.a.b;

import com.facebook.internal.ServerProtocol;
import com.microsoft.appcenter.b.a.g;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: Extensions */
public class f implements g {

    /* renamed from: a  reason: collision with root package name */
    h f4591a;

    /* renamed from: b  reason: collision with root package name */
    public l f4592b;
    public n c;
    public e d;
    j e;
    public a f;
    i g;
    public m h;
    g i;

    public final void a(JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("metadata")) {
            h hVar = new h();
            hVar.a(jSONObject.getJSONObject("metadata"));
            this.f4591a = hVar;
        }
        if (jSONObject.has("protocol")) {
            l lVar = new l();
            lVar.a(jSONObject.getJSONObject("protocol"));
            this.f4592b = lVar;
        }
        if (jSONObject.has("user")) {
            n nVar = new n();
            nVar.a(jSONObject.getJSONObject("user"));
            this.c = nVar;
        }
        if (jSONObject.has("device")) {
            e eVar = new e();
            eVar.a(jSONObject.getJSONObject("device"));
            this.d = eVar;
        }
        if (jSONObject.has("os")) {
            j jVar = new j();
            jVar.a(jSONObject.getJSONObject("os"));
            this.e = jVar;
        }
        if (jSONObject.has("app")) {
            a aVar = new a();
            aVar.a(jSONObject.getJSONObject("app"));
            this.f = aVar;
        }
        if (jSONObject.has("net")) {
            i iVar = new i();
            iVar.a(jSONObject.getJSONObject("net"));
            this.g = iVar;
        }
        if (jSONObject.has(ServerProtocol.DIALOG_PARAM_SDK_VERSION)) {
            m mVar = new m();
            mVar.a(jSONObject.getJSONObject(ServerProtocol.DIALOG_PARAM_SDK_VERSION));
            this.h = mVar;
        }
        if (jSONObject.has("loc")) {
            g gVar = new g();
            gVar.a(jSONObject.getJSONObject("loc"));
            this.i = gVar;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        h hVar = this.f4591a;
        if (hVar == null ? fVar.f4591a != null : !hVar.equals(fVar.f4591a)) {
            return false;
        }
        l lVar = this.f4592b;
        if (lVar == null ? fVar.f4592b != null : !lVar.equals(fVar.f4592b)) {
            return false;
        }
        n nVar = this.c;
        if (nVar == null ? fVar.c != null : !nVar.equals(fVar.c)) {
            return false;
        }
        e eVar = this.d;
        if (eVar == null ? fVar.d != null : !eVar.equals(fVar.d)) {
            return false;
        }
        j jVar = this.e;
        if (jVar == null ? fVar.e != null : !jVar.equals(fVar.e)) {
            return false;
        }
        a aVar = this.f;
        if (aVar == null ? fVar.f != null : !aVar.equals(fVar.f)) {
            return false;
        }
        i iVar = this.g;
        if (iVar == null ? fVar.g != null : !iVar.equals(fVar.g)) {
            return false;
        }
        m mVar = this.h;
        if (mVar == null ? fVar.h != null : !mVar.equals(fVar.h)) {
            return false;
        }
        g gVar = this.i;
        g gVar2 = fVar.i;
        if (gVar != null) {
            return gVar.equals(gVar2);
        }
        if (gVar2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        h hVar = this.f4591a;
        int i2 = 0;
        int hashCode = (hVar != null ? hVar.hashCode() : 0) * 31;
        l lVar = this.f4592b;
        int hashCode2 = (hashCode + (lVar != null ? lVar.hashCode() : 0)) * 31;
        n nVar = this.c;
        int hashCode3 = (hashCode2 + (nVar != null ? nVar.hashCode() : 0)) * 31;
        e eVar = this.d;
        int hashCode4 = (hashCode3 + (eVar != null ? eVar.hashCode() : 0)) * 31;
        j jVar = this.e;
        int hashCode5 = (hashCode4 + (jVar != null ? jVar.hashCode() : 0)) * 31;
        a aVar = this.f;
        int hashCode6 = (hashCode5 + (aVar != null ? aVar.hashCode() : 0)) * 31;
        i iVar = this.g;
        int hashCode7 = (hashCode6 + (iVar != null ? iVar.hashCode() : 0)) * 31;
        m mVar = this.h;
        int hashCode8 = (hashCode7 + (mVar != null ? mVar.hashCode() : 0)) * 31;
        g gVar = this.i;
        if (gVar != null) {
            i2 = gVar.hashCode();
        }
        return hashCode8 + i2;
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        if (this.f4591a != null) {
            jSONStringer.key("metadata").object();
            this.f4591a.a(jSONStringer);
            jSONStringer.endObject();
        }
        if (this.f4592b != null) {
            jSONStringer.key("protocol").object();
            this.f4592b.a(jSONStringer);
            jSONStringer.endObject();
        }
        if (this.c != null) {
            jSONStringer.key("user").object();
            this.c.a(jSONStringer);
            jSONStringer.endObject();
        }
        if (this.d != null) {
            jSONStringer.key("device").object();
            this.d.a(jSONStringer);
            jSONStringer.endObject();
        }
        if (this.e != null) {
            jSONStringer.key("os").object();
            this.e.a(jSONStringer);
            jSONStringer.endObject();
        }
        if (this.f != null) {
            jSONStringer.key("app").object();
            this.f.a(jSONStringer);
            jSONStringer.endObject();
        }
        if (this.g != null) {
            jSONStringer.key("net").object();
            this.g.a(jSONStringer);
            jSONStringer.endObject();
        }
        if (this.h != null) {
            jSONStringer.key(ServerProtocol.DIALOG_PARAM_SDK_VERSION).object();
            this.h.a(jSONStringer);
            jSONStringer.endObject();
        }
        if (this.i != null) {
            jSONStringer.key("loc").object();
            this.i.a(jSONStringer);
            jSONStringer.endObject();
        }
    }
}
