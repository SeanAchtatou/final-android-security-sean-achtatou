package com.microsoft.appcenter.b.a.c;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/* compiled from: LongTypedProperty */
public class d extends f {

    /* renamed from: a  reason: collision with root package name */
    public long f4610a;

    public final String a() {
        return "long";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f4610a = jSONObject.getLong("value");
    }

    public final void a(JSONStringer jSONStringer) throws JSONException {
        super.a(jSONStringer);
        jSONStringer.key("value").value(this.f4610a);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj) || this.f4610a != ((d) obj).f4610a) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        long j = this.f4610a;
        return (super.hashCode() * 31) + ((int) (j ^ (j >>> 32)));
    }
}
