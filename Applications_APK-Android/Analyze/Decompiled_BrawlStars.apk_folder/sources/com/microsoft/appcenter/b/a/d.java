package com.microsoft.appcenter.b.a;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

/* compiled from: Log */
public interface d extends g {
    String a();

    void a(c cVar);

    void a(String str);

    void a(Date date);

    void a(UUID uuid);

    Date b();

    UUID c();

    String d();

    c e();

    Object f();

    Set<String> g();
}
