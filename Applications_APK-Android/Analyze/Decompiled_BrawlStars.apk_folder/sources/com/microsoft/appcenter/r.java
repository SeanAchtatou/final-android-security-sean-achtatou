package com.microsoft.appcenter;

import android.os.Handler;
import com.microsoft.appcenter.a.b;
import com.microsoft.appcenter.utils.a;
import com.microsoft.appcenter.utils.d.d;
import com.microsoft.appcenter.utils.g;
import java.lang.Thread;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/* compiled from: UncaughtExceptionHandler */
class r implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    Thread.UncaughtExceptionHandler f4708a;

    /* renamed from: b  reason: collision with root package name */
    private final Handler f4709b;
    /* access modifiers changed from: private */
    public final b c;

    r(Handler handler, b bVar) {
        this.f4709b = handler;
        this.c = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, java.lang.String):java.lang.String
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean */
    public void uncaughtException(Thread thread, Throwable th) {
        f.a();
        if (d.a("enabled", true)) {
            Semaphore semaphore = new Semaphore(0);
            this.f4709b.post(new s(this, semaphore));
            try {
                if (!semaphore.tryAcquire(5000, TimeUnit.MILLISECONDS)) {
                    a.e("AppCenter", "Timeout waiting for looper tasks to complete.");
                }
            } catch (InterruptedException e) {
                a.b("AppCenter", "Interrupted while waiting looper to flush.", e);
            }
        }
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.f4708a;
        if (uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(thread, th);
        } else {
            g.a(10);
        }
    }
}
