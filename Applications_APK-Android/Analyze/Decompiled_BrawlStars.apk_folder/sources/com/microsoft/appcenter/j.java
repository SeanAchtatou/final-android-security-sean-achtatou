package com.microsoft.appcenter;

import com.microsoft.appcenter.utils.a;
import com.microsoft.appcenter.utils.b.b;
import com.microsoft.appcenter.utils.b.e;
import com.microsoft.appcenter.utils.d.c;
import com.microsoft.appcenter.utils.d.d;

/* compiled from: AppCenter */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f4695a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ f f4696b;

    j(f fVar, boolean z) {
        this.f4696b = fVar;
        this.f4695a = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, java.lang.String):java.lang.String
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean */
    public void run() {
        f fVar = this.f4696b;
        boolean z = this.f4695a;
        o.a(fVar.f4672b);
        c.a(fVar.f4672b);
        d.a(fVar.f4672b);
        b.a(fVar.f4672b);
        e.a();
        boolean a2 = d.a("enabled", true);
        fVar.g = new com.microsoft.appcenter.b.a.a.c();
        fVar.g.a("startService", new com.microsoft.appcenter.b.a.a.j());
        fVar.g.a("customProperties", new com.microsoft.appcenter.b.a.a.b());
        fVar.h = new com.microsoft.appcenter.a.c(fVar.f4672b, fVar.c, fVar.g, fVar.i);
        if (z) {
            fVar.b();
        } else {
            fVar.h.a(10485760);
        }
        fVar.h.a(a2);
        fVar.h.a("group_core", 50, 3000, 3, null, null);
        fVar.j = new com.microsoft.appcenter.a.j(fVar.f4672b, fVar.h, fVar.g, com.microsoft.appcenter.utils.c.a());
        if (fVar.f4671a != null) {
            if (fVar.c != null) {
                a.c("AppCenter", "The log url of App Center endpoint has been changed to " + fVar.f4671a);
                fVar.h.c(fVar.f4671a);
            } else {
                a.c("AppCenter", "The log url of One Collector endpoint has been changed to " + fVar.f4671a);
                com.microsoft.appcenter.a.j jVar = fVar.j;
                jVar.f4545a.a(fVar.f4671a);
            }
        }
        fVar.h.a(fVar.j);
        if (!a2) {
            com.microsoft.appcenter.utils.e.a(fVar.f4672b).close();
        }
        fVar.e = new r(fVar.i, fVar.h);
        if (a2) {
            r rVar = fVar.e;
            rVar.f4708a = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(rVar);
        }
        a.b("AppCenter", "App Center initialized.");
    }
}
