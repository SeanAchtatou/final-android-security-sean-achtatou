package com.microsoft.appcenter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.microsoft.appcenter.a.b;
import com.microsoft.appcenter.b.a.a.g;
import com.microsoft.appcenter.utils.a.c;
import com.microsoft.appcenter.utils.d.d;
import java.util.Map;

/* compiled from: AbstractAppCenterService */
public abstract class a implements n {

    /* renamed from: a  reason: collision with root package name */
    public b f4529a;

    /* renamed from: b  reason: collision with root package name */
    private m f4530b;

    public void a(String str) {
    }

    public boolean c() {
        return true;
    }

    public Map<String, g> d() {
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract String e();

    /* access modifiers changed from: protected */
    public abstract String f();

    /* access modifiers changed from: protected */
    public int h() {
        return 50;
    }

    /* access modifiers changed from: protected */
    public long i() {
        return 3000;
    }

    /* access modifiers changed from: protected */
    public b.a j() {
        return null;
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.a.a(java.lang.Runnable, com.microsoft.appcenter.utils.a.c, java.lang.Object):void
     arg types: [com.microsoft.appcenter.b, com.microsoft.appcenter.utils.a.c, boolean]
     candidates:
      com.microsoft.appcenter.a.a(java.lang.Runnable, java.lang.Runnable, java.lang.Runnable):boolean
      com.microsoft.appcenter.a.a(java.lang.Runnable, com.microsoft.appcenter.utils.a.c, java.lang.Object):void */
    public final synchronized com.microsoft.appcenter.utils.a.b<Boolean> a() {
        c cVar;
        cVar = new c();
        a((Runnable) new b(this, cVar), cVar, (Object) false);
        return cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, java.lang.String):java.lang.String
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean */
    public final synchronized boolean b() {
        return d.a(g(), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.utils.d.d.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.microsoft.appcenter.utils.d.d.b(java.lang.String, java.lang.String):void
      com.microsoft.appcenter.utils.d.d.b(java.lang.String, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(boolean r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            boolean r6 = r5.b()     // Catch:{ all -> 0x0060 }
            r0 = 1
            r1 = 2
            r2 = 0
            if (r6 != 0) goto L_0x0025
            java.lang.String r6 = r5.f()     // Catch:{ all -> 0x0060 }
            java.lang.String r3 = "%s service has already been %s."
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0060 }
            java.lang.String r4 = r5.k()     // Catch:{ all -> 0x0060 }
            r1[r2] = r4     // Catch:{ all -> 0x0060 }
            java.lang.String r2 = "disabled"
            r1[r0] = r2     // Catch:{ all -> 0x0060 }
            java.lang.String r0 = java.lang.String.format(r3, r1)     // Catch:{ all -> 0x0060 }
            com.microsoft.appcenter.utils.a.c(r6, r0)     // Catch:{ all -> 0x0060 }
            monitor-exit(r5)
            return
        L_0x0025:
            java.lang.String r6 = r5.e()     // Catch:{ all -> 0x0060 }
            com.microsoft.appcenter.a.b r3 = r5.f4529a     // Catch:{ all -> 0x0060 }
            if (r3 == 0) goto L_0x0037
            com.microsoft.appcenter.a.b r3 = r5.f4529a     // Catch:{ all -> 0x0060 }
            r3.d(r6)     // Catch:{ all -> 0x0060 }
            com.microsoft.appcenter.a.b r3 = r5.f4529a     // Catch:{ all -> 0x0060 }
            r3.b(r6)     // Catch:{ all -> 0x0060 }
        L_0x0037:
            java.lang.String r6 = r5.g()     // Catch:{ all -> 0x0060 }
            com.microsoft.appcenter.utils.d.d.b(r6, r2)     // Catch:{ all -> 0x0060 }
            java.lang.String r6 = r5.f()     // Catch:{ all -> 0x0060 }
            java.lang.String r3 = "%s service has been %s."
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0060 }
            java.lang.String r4 = r5.k()     // Catch:{ all -> 0x0060 }
            r1[r2] = r4     // Catch:{ all -> 0x0060 }
            java.lang.String r4 = "disabled"
            r1[r0] = r4     // Catch:{ all -> 0x0060 }
            java.lang.String r0 = java.lang.String.format(r3, r1)     // Catch:{ all -> 0x0060 }
            com.microsoft.appcenter.utils.a.c(r6, r0)     // Catch:{ all -> 0x0060 }
            com.microsoft.appcenter.a.b r6 = r5.f4529a     // Catch:{ all -> 0x0060 }
            if (r6 == 0) goto L_0x005e
            r5.b(r2)     // Catch:{ all -> 0x0060 }
        L_0x005e:
            monitor-exit(r5)
            return
        L_0x0060:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.a.a(boolean):void");
    }

    /* access modifiers changed from: protected */
    public synchronized void b(boolean z) {
    }

    public final synchronized void a(m mVar) {
        this.f4530b = mVar;
    }

    public synchronized void a(Context context, b bVar, String str, String str2, boolean z) {
        String e = e();
        boolean b2 = b();
        bVar.b(e);
        if (b2) {
            bVar.a(e, h(), i(), 3, null, j());
        } else {
            bVar.d(e);
        }
        this.f4529a = bVar;
        b(b2);
    }

    /* access modifiers changed from: protected */
    public final String g() {
        return "enabled_" + k();
    }

    public synchronized void a(Runnable runnable) {
        a(runnable, (Runnable) null, (Runnable) null);
    }

    /* access modifiers changed from: protected */
    public final synchronized boolean a(Runnable runnable, Runnable runnable2, Runnable runnable3) {
        if (this.f4530b == null) {
            com.microsoft.appcenter.utils.a.e("AppCenter", k() + " needs to be started before it can be used.");
            return false;
        }
        this.f4530b.a(new c(this, runnable, runnable3), runnable2);
        return true;
    }

    /* access modifiers changed from: protected */
    public final synchronized <T> void a(Runnable runnable, c<T> cVar, T t) {
        d dVar = new d(this, cVar, t);
        if (!a(new e(this, runnable), dVar, dVar)) {
            dVar.run();
        }
    }
}
