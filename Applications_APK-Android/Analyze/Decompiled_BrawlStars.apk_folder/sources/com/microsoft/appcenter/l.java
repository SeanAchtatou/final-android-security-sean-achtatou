package com.microsoft.appcenter;

import com.microsoft.appcenter.b.a.a.g;
import com.microsoft.appcenter.b.a.h;
import com.microsoft.appcenter.utils.a;
import com.microsoft.appcenter.utils.b.b;
import com.microsoft.appcenter.utils.d.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/* compiled from: AppCenter */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Collection f4699a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Collection f4700b;
    final /* synthetic */ boolean c;
    final /* synthetic */ f d;

    l(f fVar, Collection collection, Collection collection2, boolean z) {
        this.d = fVar;
        this.f4699a = collection;
        this.f4700b = collection2;
        this.c = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, java.lang.String):java.lang.String
      com.microsoft.appcenter.utils.d.d.a(java.lang.String, boolean):boolean */
    public void run() {
        f fVar = this.d;
        Collection<n> collection = this.f4699a;
        Collection<n> collection2 = this.f4700b;
        boolean z = this.c;
        for (n nVar : collection) {
            nVar.a(fVar.d);
            a.c("AppCenter", nVar.getClass().getSimpleName() + " service configuration updated.");
        }
        boolean a2 = d.a("enabled", true);
        for (n nVar2 : collection2) {
            Map<String, g> d2 = nVar2.d();
            if (d2 != null) {
                for (Map.Entry next : d2.entrySet()) {
                    fVar.g.a((String) next.getKey(), (g) next.getValue());
                }
            }
            if (!a2 && nVar2.b()) {
                nVar2.a(false);
            }
            if (z) {
                nVar2.a(fVar.f4672b, fVar.h, fVar.c, fVar.d, true);
                a.c("AppCenter", nVar2.getClass().getSimpleName() + " service started from application.");
            } else {
                nVar2.a(fVar.f4672b, fVar.h, null, null, false);
                a.c("AppCenter", nVar2.getClass().getSimpleName() + " service started from library.");
            }
        }
        if (z) {
            b.a().b();
            for (n k : collection) {
                fVar.f.add(k.k());
            }
            for (n k2 : collection2) {
                fVar.f.add(k2.k());
            }
            if (!fVar.f.isEmpty() && d.a("enabled", true)) {
                ArrayList arrayList = new ArrayList(fVar.f);
                fVar.f.clear();
                h hVar = new h();
                hVar.f4614a = arrayList;
                fVar.h.a(hVar, "group_core", 1);
            }
        }
    }
}
