package com.microsoft.appcenter.persistence;

import android.database.sqlite.SQLiteDatabase;
import com.microsoft.appcenter.utils.d.a;

/* compiled from: DatabasePersistence */
class b implements a.C0158a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f4707a;

    b(a aVar) {
        this.f4707a = aVar;
    }

    public final boolean a(SQLiteDatabase sQLiteDatabase, int i) {
        if (i < 2) {
            sQLiteDatabase.execSQL("ALTER TABLE logs ADD COLUMN `target_token` TEXT");
            sQLiteDatabase.execSQL("ALTER TABLE logs ADD COLUMN `type` TEXT");
        }
        if (i < 3) {
            sQLiteDatabase.execSQL("ALTER TABLE logs ADD COLUMN `target_key` TEXT");
        }
        if (i < 4) {
            sQLiteDatabase.execSQL("ALTER TABLE logs ADD COLUMN `priority` INTEGER DEFAULT 1");
        }
        sQLiteDatabase.execSQL("ALTER TABLE logs ADD COLUMN `timestamp` INTEGER DEFAULT 0");
        sQLiteDatabase.execSQL("CREATE INDEX `ix_logs_priority` ON logs (`priority`)");
        return true;
    }

    public final void a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE INDEX `ix_logs_priority` ON logs (`priority`)");
    }
}
