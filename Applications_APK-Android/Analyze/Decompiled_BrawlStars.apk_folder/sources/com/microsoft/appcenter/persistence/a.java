package com.microsoft.appcenter.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import com.microsoft.appcenter.b.a.d;
import com.microsoft.appcenter.o;
import com.microsoft.appcenter.utils.c.e;
import com.microsoft.appcenter.utils.d.c;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.json.JSONException;

/* compiled from: DatabasePersistence */
public class a extends Persistence {

    /* renamed from: a  reason: collision with root package name */
    static final ContentValues f4705a = a("", "", "", "", "", 0, 0L);

    /* renamed from: b  reason: collision with root package name */
    final com.microsoft.appcenter.utils.d.a f4706b;
    final Map<String, List<Long>> c;
    final Set<Long> d;
    private final Context f;
    private final File g;

    public a(Context context) {
        this(context, 5, f4705a);
    }

    private a(Context context, int i, ContentValues contentValues) {
        this.f = context;
        this.c = new HashMap();
        this.d = new HashSet();
        this.f4706b = new com.microsoft.appcenter.utils.d.a(context, "com.microsoft.appcenter.persistence", "logs", 5, contentValues, new b(this));
        this.g = new File(o.f4701a + "/appcenter/database_large_payloads");
        this.g.mkdirs();
    }

    private static ContentValues a(String str, String str2, String str3, String str4, String str5, int i, Long l) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("persistence_group", str);
        contentValues.put("log", str2);
        contentValues.put("target_token", str3);
        contentValues.put("type", str4);
        contentValues.put("target_key", str5);
        contentValues.put("priority", Integer.valueOf(i));
        contentValues.put("timestamp", l);
        return contentValues;
    }

    public final boolean a(long j) {
        return this.f4706b.b(j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x015c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x016d, code lost:
        throw new com.microsoft.appcenter.persistence.Persistence.PersistenceException("Cannot convert to JSON string.", r0);
     */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x015c A[ExcHandler: JSONException (r0v1 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:1:0x0008] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long a(com.microsoft.appcenter.b.a.d r17, java.lang.String r18, int r19) throws com.microsoft.appcenter.persistence.Persistence.PersistenceException {
        /*
            r16 = this;
            r1 = r16
            r0 = r17
            r2 = r19
            java.lang.String r3 = "AppCenter"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r4.<init>()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r5 = "Storing a log to the Persistence database for log type "
            r4.append(r5)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r5 = r17.a()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r4.append(r5)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r5 = " with flags="
            r4.append(r5)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r4.append(r2)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r4 = r4.toString()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            com.microsoft.appcenter.utils.a.b(r3, r4)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            com.microsoft.appcenter.b.a.a.h r4 = r16.b()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r4 = r4.a(r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r5 = "UTF-8"
            byte[] r5 = r4.getBytes(r5)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            int r5 = r5.length     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r6 = 1992294(0x1e6666, float:2.791799E-39)
            r7 = 1
            if (r5 < r6) goto L_0x003f
            r6 = 1
            goto L_0x0040
        L_0x003f:
            r6 = 0
        L_0x0040:
            boolean r8 = r0 instanceof com.microsoft.appcenter.b.a.b.c     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r9 = 0
            if (r8 == 0) goto L_0x006e
            if (r6 != 0) goto L_0x0066
            java.util.Set r8 = r17.g()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.util.Iterator r8 = r8.iterator()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.Object r8 = r8.next()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r10 = com.microsoft.appcenter.b.a.b.k.a(r8)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            android.content.Context r11 = r1.f     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            com.microsoft.appcenter.utils.c.e r11 = com.microsoft.appcenter.utils.c.e.a(r11)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r8 = r11.a(r8)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r11 = r10
            r10 = r8
            goto L_0x0070
        L_0x0066:
            com.microsoft.appcenter.persistence.Persistence$PersistenceException r0 = new com.microsoft.appcenter.persistence.Persistence$PersistenceException     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r2 = "Log is larger than 1992294 bytes, cannot send to OneCollector."
            r0.<init>(r2)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            throw r0     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
        L_0x006e:
            r10 = r9
            r11 = r10
        L_0x0070:
            com.microsoft.appcenter.utils.d.a r8 = r1.f4706b     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            long r12 = r8.a()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r14 = -1
            int r8 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r8 == 0) goto L_0x0152
            if (r6 != 0) goto L_0x00a8
            long r14 = (long) r5     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            int r8 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r8 <= 0) goto L_0x0084
            goto L_0x00a8
        L_0x0084:
            com.microsoft.appcenter.persistence.Persistence$PersistenceException r0 = new com.microsoft.appcenter.persistence.Persistence$PersistenceException     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r2.<init>()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r3 = "Log is too large ("
            r2.append(r3)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r2.append(r5)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r3 = " bytes) to store in database. Current maximum database size is "
            r2.append(r3)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r2.append(r12)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r3 = " bytes."
            r2.append(r3)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r0.<init>(r2)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            throw r0     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
        L_0x00a8:
            if (r6 == 0) goto L_0x00ac
            r8 = r9
            goto L_0x00ad
        L_0x00ac:
            r8 = r4
        L_0x00ad:
            java.lang.String r5 = r17.a()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r2 = r2 & 255(0xff, float:3.57E-43)
            if (r2 == r7) goto L_0x00ba
            r9 = 2
            if (r2 == r9) goto L_0x00ba
            r12 = 1
            goto L_0x00bb
        L_0x00ba:
            r12 = r2
        L_0x00bb:
            java.util.Date r2 = r17.b()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            long r13 = r2.getTime()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.Long r13 = java.lang.Long.valueOf(r13)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r7 = r18
            r9 = r10
            r10 = r5
            android.content.ContentValues r2 = a(r7, r8, r9, r10, r11, r12, r13)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            com.microsoft.appcenter.utils.d.a r5 = r1.f4706b     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r7 = "priority"
            long r7 = r5.a(r2, r7)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r9 = -1
            int r2 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r2 == 0) goto L_0x0132
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r2.<init>()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r5 = "Stored a log to the Persistence database for log type "
            r2.append(r5)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r0 = r17.a()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r2.append(r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r0 = " with databaseId="
            r2.append(r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r2.append(r7)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r0 = r2.toString()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            com.microsoft.appcenter.utils.a.b(r3, r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            if (r6 == 0) goto L_0x0131
            java.lang.String r0 = "Payload is larger than what SQLite supports, storing payload in a separate file."
            com.microsoft.appcenter.utils.a.b(r3, r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r0 = r18
            java.io.File r0 = r1.c(r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r0.mkdir()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.io.File r0 = a(r0, r7)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            com.microsoft.appcenter.utils.d.c.a(r0, r4)     // Catch:{ IOException -> 0x0129, JSONException -> 0x015c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r2.<init>()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r4 = "Payload written to "
            r2.append(r4)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r2.append(r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r0 = r2.toString()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            com.microsoft.appcenter.utils.a.b(r3, r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            goto L_0x0131
        L_0x0129:
            r0 = move-exception
            r2 = r0
            com.microsoft.appcenter.utils.d.a r0 = r1.f4706b     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r0.a(r7)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            throw r2     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
        L_0x0131:
            return r7
        L_0x0132:
            com.microsoft.appcenter.persistence.Persistence$PersistenceException r2 = new com.microsoft.appcenter.persistence.Persistence$PersistenceException     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r3.<init>()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r4 = "Failed to store a log to the Persistence database for log type "
            r3.append(r4)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r0 = r17.a()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r3.append(r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r0 = "."
            r3.append(r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r0 = r3.toString()     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            r2.<init>(r0)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            throw r2     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
        L_0x0152:
            com.microsoft.appcenter.persistence.Persistence$PersistenceException r0 = new com.microsoft.appcenter.persistence.Persistence$PersistenceException     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            java.lang.String r2 = "Failed to store a log to the Persistence database."
            r0.<init>(r2)     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
            throw r0     // Catch:{ JSONException -> 0x015c, IOException -> 0x015a }
        L_0x015a:
            r0 = move-exception
            goto L_0x015e
        L_0x015c:
            r0 = move-exception
            goto L_0x0166
        L_0x015e:
            com.microsoft.appcenter.persistence.Persistence$PersistenceException r2 = new com.microsoft.appcenter.persistence.Persistence$PersistenceException
            java.lang.String r3 = "Cannot save large payload in a file."
            r2.<init>(r3, r0)
            throw r2
        L_0x0166:
            com.microsoft.appcenter.persistence.Persistence$PersistenceException r2 = new com.microsoft.appcenter.persistence.Persistence$PersistenceException
            java.lang.String r3 = "Cannot convert to JSON string."
            r2.<init>(r3, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.persistence.a.a(com.microsoft.appcenter.b.a.d, java.lang.String, int):long");
    }

    private File c(String str) {
        return new File(this.g, str);
    }

    private static File a(File file, long j) {
        return new File(file, j + ".json");
    }

    private void b(File file, long j) {
        a(file, j).delete();
        this.f4706b.a(j);
    }

    public final void a(String str, String str2) {
        com.microsoft.appcenter.utils.a.b("AppCenter", "Deleting logs from the Persistence database for " + str + " with " + str2);
        com.microsoft.appcenter.utils.a.b("AppCenter", "The IDs for deleting log(s) is/are:");
        Map<String, List<Long>> map = this.c;
        List<Long> remove = map.remove(str + str2);
        File c2 = c(str);
        if (remove != null) {
            for (Long l : remove) {
                com.microsoft.appcenter.utils.a.b("AppCenter", "\t" + l);
                b(c2, l.longValue());
                this.d.remove(l);
            }
        }
    }

    public final void a(String str) {
        com.microsoft.appcenter.utils.a.b("AppCenter", "Deleting all logs from the Persistence database for " + str);
        File c2 = c(str);
        File[] listFiles = c2.listFiles();
        if (listFiles != null) {
            for (File delete : listFiles) {
                delete.delete();
            }
        }
        c2.delete();
        com.microsoft.appcenter.utils.d.a aVar = this.f4706b;
        com.microsoft.appcenter.utils.a.b("AppCenter", "Deleted " + aVar.a(aVar.f4745b, "persistence_group", str) + " logs.");
        Iterator<String> it = this.c.keySet().iterator();
        while (it.hasNext()) {
            if (it.next().startsWith(str)) {
                it.remove();
            }
        }
    }

    public final int b(String str) {
        return a("persistence_group = ?", str);
    }

    public final int a(Date date) {
        return a("timestamp < ?", String.valueOf(date.getTime()));
    }

    public final String a(String str, Collection<String> collection, int i, List<d> list, Date date, Date date2) {
        Cursor cursor;
        Cursor cursor2;
        String str2 = str;
        int i2 = i;
        com.microsoft.appcenter.utils.a.b("AppCenter", "Trying to get " + i2 + " logs from the Persistence database for " + str2);
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.appendWhere("persistence_group = ?");
        ArrayList arrayList = new ArrayList();
        arrayList.add(str2);
        if (!collection.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (int i3 = 0; i3 < collection.size(); i3++) {
                sb.append("?,");
            }
            sb.deleteCharAt(sb.length() - 1);
            sQLiteQueryBuilder.appendWhere(" AND ");
            sQLiteQueryBuilder.appendWhere("target_key NOT IN (" + sb.toString() + ")");
            arrayList.addAll(collection);
        }
        if (date != null) {
            sQLiteQueryBuilder.appendWhere(" AND ");
            sQLiteQueryBuilder.appendWhere("timestamp >= ?");
            arrayList.add(String.valueOf(date.getTime()));
        }
        if (date2 != null) {
            sQLiteQueryBuilder.appendWhere(" AND ");
            sQLiteQueryBuilder.appendWhere("timestamp < ?");
            arrayList.add(String.valueOf(date2.getTime()));
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        ArrayList<Long> arrayList2 = new ArrayList<>();
        File c2 = c(str);
        String[] strArr = (String[]) arrayList.toArray(new String[0]);
        try {
            cursor = this.f4706b.a(sQLiteQueryBuilder, null, strArr, "priority DESC, oid");
        } catch (RuntimeException e) {
            com.microsoft.appcenter.utils.a.c("AppCenter", "Failed to get logs: ", e);
            cursor = null;
        }
        int i4 = 0;
        while (cursor != null) {
            ContentValues b2 = this.f4706b.b(cursor);
            if (b2 == null || i4 >= i2) {
                break;
            }
            Long asLong = b2.getAsLong("oid");
            if (asLong == null) {
                com.microsoft.appcenter.utils.a.e("AppCenter", "Empty database record, probably content was larger than 2MB, need to delete as it's now corrupted.");
                Iterator<Long> it = a(sQLiteQueryBuilder, strArr).iterator();
                while (true) {
                    if (!it.hasNext()) {
                        cursor2 = cursor;
                        break;
                    }
                    Long next = it.next();
                    if (!this.d.contains(next) && !linkedHashMap.containsKey(next)) {
                        cursor2 = cursor;
                        b(c2, next.longValue());
                        com.microsoft.appcenter.utils.a.e("AppCenter", "Empty database corrupted empty record deleted, id=" + next);
                        break;
                    }
                    cursor = cursor;
                }
            } else {
                cursor2 = cursor;
                if (!this.d.contains(asLong)) {
                    try {
                        String asString = b2.getAsString("log");
                        if (asString == null) {
                            File a2 = a(c2, asLong.longValue());
                            com.microsoft.appcenter.utils.a.b("AppCenter", "Read payload file " + a2);
                            asString = c.a(a2);
                            if (asString == null) {
                                throw new JSONException("Log payload is null and not stored as a file.");
                            }
                        }
                        d a3 = b().a(asString, b2.getAsString("type"));
                        String asString2 = b2.getAsString("target_token");
                        if (asString2 != null) {
                            a3.a(e.a(this.f).a(asString2, false).f4737a);
                        }
                        linkedHashMap.put(asLong, a3);
                        i4++;
                    } catch (JSONException e2) {
                        com.microsoft.appcenter.utils.a.c("AppCenter", "Cannot deserialize a log in the database", e2);
                        arrayList2.add(asLong);
                    }
                } else {
                    continue;
                }
            }
            cursor = cursor2;
        }
        Cursor cursor3 = cursor;
        if (cursor3 != null) {
            try {
                cursor3.close();
            } catch (RuntimeException unused) {
            }
        }
        if (arrayList2.size() > 0) {
            for (Long longValue : arrayList2) {
                b(c2, longValue.longValue());
            }
            com.microsoft.appcenter.utils.a.d("AppCenter", "Deleted logs that cannot be deserialized");
        }
        if (linkedHashMap.size() <= 0) {
            com.microsoft.appcenter.utils.a.b("AppCenter", "No logs found in the Persistence database at the moment");
            return null;
        }
        String uuid = UUID.randomUUID().toString();
        com.microsoft.appcenter.utils.a.b("AppCenter", "Returning " + linkedHashMap.size() + " log(s) with an ID, " + uuid);
        com.microsoft.appcenter.utils.a.b("AppCenter", "The SID/ID pairs for returning log(s) is/are:");
        ArrayList arrayList3 = new ArrayList();
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            Long l = (Long) entry.getKey();
            this.d.add(l);
            arrayList3.add(l);
            list.add(entry.getValue());
            com.microsoft.appcenter.utils.a.b("AppCenter", "\t" + ((d) entry.getValue()).c() + " / " + l);
        }
        this.c.put(str2 + uuid, arrayList3);
        return uuid;
    }

    public final void a() {
        this.d.clear();
        this.c.clear();
        com.microsoft.appcenter.utils.a.b("AppCenter", "Cleared pending log states");
    }

    public void close() {
        this.f4706b.close();
    }

    private List<Long> a(SQLiteQueryBuilder sQLiteQueryBuilder, String[] strArr) {
        Cursor a2;
        ArrayList arrayList = new ArrayList();
        try {
            a2 = this.f4706b.a(sQLiteQueryBuilder, com.microsoft.appcenter.utils.d.a.f4744a, strArr, null);
            while (a2.moveToNext()) {
                arrayList.add(this.f4706b.a(a2).getAsLong("oid"));
            }
            a2.close();
        } catch (RuntimeException e) {
            com.microsoft.appcenter.utils.a.c("AppCenter", "Failed to get corrupted ids: ", e);
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
        return arrayList;
    }

    private int a(String str, String... strArr) {
        Cursor a2;
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.appendWhere(str);
        int i = 0;
        try {
            a2 = this.f4706b.a(sQLiteQueryBuilder, new String[]{"COUNT(*)"}, strArr, null);
            a2.moveToNext();
            i = a2.getInt(0);
            a2.close();
        } catch (RuntimeException e) {
            com.microsoft.appcenter.utils.a.c("AppCenter", "Failed to get logs count: ", e);
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
        return i;
    }
}
