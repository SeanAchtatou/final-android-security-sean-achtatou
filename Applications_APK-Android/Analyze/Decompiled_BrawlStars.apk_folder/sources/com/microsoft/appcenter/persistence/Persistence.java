package com.microsoft.appcenter.persistence;

import com.microsoft.appcenter.b.a.a.h;
import com.microsoft.appcenter.b.a.d;
import java.io.Closeable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public abstract class Persistence implements Closeable {
    public h e;

    public abstract int a(Date date);

    public abstract long a(d dVar, String str, int i) throws PersistenceException;

    public abstract String a(String str, Collection<String> collection, int i, List<d> list, Date date, Date date2);

    public abstract void a();

    public abstract void a(String str);

    public abstract void a(String str, String str2);

    public abstract boolean a(long j);

    public abstract int b(String str);

    /* access modifiers changed from: package-private */
    public final h b() {
        h hVar = this.e;
        if (hVar != null) {
            return hVar;
        }
        throw new IllegalStateException("logSerializer not configured");
    }

    public static class PersistenceException extends Exception {
        public PersistenceException(String str, Throwable th) {
            super(str, th);
        }

        PersistenceException(String str) {
            super(str);
        }
    }
}
