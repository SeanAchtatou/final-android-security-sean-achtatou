package com.microsoft.appcenter;

import android.app.Application;
import android.os.Handler;
import android.os.HandlerThread;
import com.microsoft.appcenter.a.b;
import com.microsoft.appcenter.a.j;
import com.microsoft.appcenter.b.a.a.h;
import com.microsoft.appcenter.utils.a;
import com.microsoft.appcenter.utils.a.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: AppCenter */
public class f {
    private static f k;

    /* renamed from: a  reason: collision with root package name */
    String f4671a;

    /* renamed from: b  reason: collision with root package name */
    Application f4672b;
    String c;
    String d;
    r e;
    final List<String> f = new ArrayList();
    h g;
    b h;
    Handler i;
    j j;
    private boolean l;
    private boolean m;
    private Set<n> n;
    private Set<n> o;
    private HandlerThread p;
    private m q;
    private long r = 10485760;
    private c<Boolean> s;

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (k == null) {
                k = new f();
            }
            fVar = k;
        }
        return fVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00b6, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0098 A[DONT_GENERATE] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(java.lang.String r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            boolean r0 = r7.m     // Catch:{ all -> 0x00b7 }
            if (r0 != 0) goto L_0x000e
            java.lang.String r8 = "AppCenter"
            java.lang.String r0 = "AppCenter must be configured from application, libraries cannot use call setUserId."
            com.microsoft.appcenter.utils.a.e(r8, r0)     // Catch:{ all -> 0x00b7 }
            monitor-exit(r7)
            return
        L_0x000e:
            java.lang.String r0 = r7.c     // Catch:{ all -> 0x00b7 }
            if (r0 != 0) goto L_0x001f
            java.lang.String r0 = r7.d     // Catch:{ all -> 0x00b7 }
            if (r0 != 0) goto L_0x001f
            java.lang.String r8 = "AppCenter"
            java.lang.String r0 = "AppCenter must be configured with a secret from application to call setUserId."
            com.microsoft.appcenter.utils.a.e(r8, r0)     // Catch:{ all -> 0x00b7 }
            monitor-exit(r7)
            return
        L_0x001f:
            if (r8 == 0) goto L_0x009a
            java.lang.String r0 = r7.c     // Catch:{ all -> 0x00b7 }
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x003f
            if (r8 == 0) goto L_0x003a
            int r0 = r8.length()     // Catch:{ all -> 0x00b7 }
            r3 = 256(0x100, float:3.59E-43)
            if (r0 <= r3) goto L_0x003a
            java.lang.String r0 = "AppCenter"
            java.lang.String r3 = "userId is limited to 256 characters."
            com.microsoft.appcenter.utils.a.e(r0, r3)     // Catch:{ all -> 0x00b7 }
            r0 = 0
            goto L_0x003b
        L_0x003a:
            r0 = 1
        L_0x003b:
            if (r0 != 0) goto L_0x003f
            monitor-exit(r7)
            return
        L_0x003f:
            java.lang.String r0 = r7.d     // Catch:{ all -> 0x00b7 }
            if (r0 == 0) goto L_0x009a
            if (r8 != 0) goto L_0x0047
        L_0x0045:
            r2 = 1
            goto L_0x0096
        L_0x0047:
            boolean r0 = r8.isEmpty()     // Catch:{ all -> 0x00b7 }
            if (r0 == 0) goto L_0x0055
            java.lang.String r0 = "AppCenter"
            java.lang.String r1 = "userId must not be empty."
            com.microsoft.appcenter.utils.a.e(r0, r1)     // Catch:{ all -> 0x00b7 }
            goto L_0x0096
        L_0x0055:
            java.lang.String r0 = ":"
            int r0 = r8.indexOf(r0)     // Catch:{ all -> 0x00b7 }
            if (r0 < 0) goto L_0x0045
            java.lang.String r3 = r8.substring(r2, r0)     // Catch:{ all -> 0x00b7 }
            java.lang.String r4 = "c"
            boolean r4 = r3.equals(r4)     // Catch:{ all -> 0x00b7 }
            if (r4 != 0) goto L_0x0088
            java.lang.String r0 = "AppCenter"
            java.lang.String r4 = "userId prefix must be '%s%s', '%s%s' is not supported."
            r5 = 4
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00b7 }
            java.lang.String r6 = "c"
            r5[r2] = r6     // Catch:{ all -> 0x00b7 }
            java.lang.String r6 = ":"
            r5[r1] = r6     // Catch:{ all -> 0x00b7 }
            r1 = 2
            r5[r1] = r3     // Catch:{ all -> 0x00b7 }
            r1 = 3
            java.lang.String r3 = ":"
            r5[r1] = r3     // Catch:{ all -> 0x00b7 }
            java.lang.String r1 = java.lang.String.format(r4, r5)     // Catch:{ all -> 0x00b7 }
            com.microsoft.appcenter.utils.a.e(r0, r1)     // Catch:{ all -> 0x00b7 }
            goto L_0x0096
        L_0x0088:
            int r3 = r8.length()     // Catch:{ all -> 0x00b7 }
            int r3 = r3 - r1
            if (r0 != r3) goto L_0x0045
            java.lang.String r0 = "AppCenter"
            java.lang.String r1 = "userId must not be empty."
            com.microsoft.appcenter.utils.a.e(r0, r1)     // Catch:{ all -> 0x00b7 }
        L_0x0096:
            if (r2 != 0) goto L_0x009a
            monitor-exit(r7)
            return
        L_0x009a:
            com.microsoft.appcenter.utils.b.f r0 = com.microsoft.appcenter.utils.b.f.a()     // Catch:{ all -> 0x00b7 }
            boolean r8 = r0.a(r8)     // Catch:{ all -> 0x00b7 }
            if (r8 != 0) goto L_0x00a5
            goto L_0x00b5
        L_0x00a5:
            java.util.Set<java.lang.Object> r8 = r0.f4732a     // Catch:{ all -> 0x00b7 }
            java.util.Iterator r8 = r8.iterator()     // Catch:{ all -> 0x00b7 }
        L_0x00ab:
            boolean r0 = r8.hasNext()     // Catch:{ all -> 0x00b7 }
            if (r0 == 0) goto L_0x00b5
            r8.next()     // Catch:{ all -> 0x00b7 }
            goto L_0x00ab
        L_0x00b5:
            monitor-exit(r7)
            return
        L_0x00b7:
            r8 = move-exception
            monitor-exit(r7)
            goto L_0x00bb
        L_0x00ba:
            throw r8
        L_0x00bb:
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.f.a(java.lang.String):void");
    }

    private synchronized boolean c() {
        if (d()) {
            return true;
        }
        a.e("AppCenter", "App Center hasn't been configured. You need to call AppCenter.start with appSecret or AppCenter.configure first.");
        return false;
    }

    public synchronized void a(p pVar) {
        if (pVar == null) {
            a.e("AppCenter", "Custom properties may not be null.");
            return;
        }
        Map<String, Object> a2 = pVar.a();
        if (a2.size() == 0) {
            a.e("AppCenter", "Custom properties may not be empty.");
        } else {
            a(new g(this, a2), (Runnable) null);
        }
    }

    private synchronized boolean d() {
        return this.f4672b != null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0098, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean a(android.app.Application r9, java.lang.String r10, boolean r11) {
        /*
            r8 = this;
            monitor-enter(r8)
            r0 = 0
            if (r9 != 0) goto L_0x000d
            java.lang.String r9 = "AppCenter"
            java.lang.String r10 = "Application context may not be null."
            com.microsoft.appcenter.utils.a.e(r9, r10)     // Catch:{ all -> 0x00de }
            monitor-exit(r8)
            return r0
        L_0x000d:
            boolean r1 = r8.l     // Catch:{ all -> 0x00de }
            if (r1 != 0) goto L_0x001f
            android.content.pm.ApplicationInfo r1 = r9.getApplicationInfo()     // Catch:{ all -> 0x00de }
            int r1 = r1.flags     // Catch:{ all -> 0x00de }
            r2 = 2
            r1 = r1 & r2
            if (r1 != r2) goto L_0x001f
            r1 = 5
            com.microsoft.appcenter.utils.a.a(r1)     // Catch:{ all -> 0x00de }
        L_0x001f:
            java.lang.String r1 = r8.c     // Catch:{ all -> 0x00de }
            r2 = 1
            if (r11 == 0) goto L_0x007d
            boolean r3 = r8.m     // Catch:{ all -> 0x00de }
            if (r3 == 0) goto L_0x0031
            java.lang.String r10 = "AppCenter"
            java.lang.String r3 = "App Center may only be configured once."
            com.microsoft.appcenter.utils.a.d(r10, r3)     // Catch:{ all -> 0x00de }
            r10 = 0
            goto L_0x0079
        L_0x0031:
            r8.m = r2     // Catch:{ all -> 0x00de }
            if (r10 == 0) goto L_0x0078
            java.lang.String r3 = ";"
            java.lang.String[] r10 = r10.split(r3)     // Catch:{ all -> 0x00de }
            int r3 = r10.length     // Catch:{ all -> 0x00de }
            r4 = 0
        L_0x003d:
            if (r4 >= r3) goto L_0x0078
            r5 = r10[r4]     // Catch:{ all -> 0x00de }
            java.lang.String r6 = "="
            r7 = -1
            java.lang.String[] r5 = r5.split(r6, r7)     // Catch:{ all -> 0x00de }
            r6 = r5[r0]     // Catch:{ all -> 0x00de }
            int r7 = r5.length     // Catch:{ all -> 0x00de }
            if (r7 != r2) goto L_0x0056
            boolean r5 = r6.isEmpty()     // Catch:{ all -> 0x00de }
            if (r5 != 0) goto L_0x0075
            r8.c = r6     // Catch:{ all -> 0x00de }
            goto L_0x0075
        L_0x0056:
            r7 = r5[r2]     // Catch:{ all -> 0x00de }
            boolean r7 = r7.isEmpty()     // Catch:{ all -> 0x00de }
            if (r7 != 0) goto L_0x0075
            r5 = r5[r2]     // Catch:{ all -> 0x00de }
            java.lang.String r7 = "appsecret"
            boolean r7 = r7.equals(r6)     // Catch:{ all -> 0x00de }
            if (r7 == 0) goto L_0x006b
            r8.c = r5     // Catch:{ all -> 0x00de }
            goto L_0x0075
        L_0x006b:
            java.lang.String r7 = "target"
            boolean r6 = r7.equals(r6)     // Catch:{ all -> 0x00de }
            if (r6 == 0) goto L_0x0075
            r8.d = r5     // Catch:{ all -> 0x00de }
        L_0x0075:
            int r4 = r4 + 1
            goto L_0x003d
        L_0x0078:
            r10 = 1
        L_0x0079:
            if (r10 != 0) goto L_0x007d
            monitor-exit(r8)
            return r0
        L_0x007d:
            android.os.Handler r10 = r8.i     // Catch:{ all -> 0x00de }
            if (r10 == 0) goto L_0x0099
            java.lang.String r9 = r8.c     // Catch:{ all -> 0x00de }
            if (r9 == 0) goto L_0x0097
            java.lang.String r9 = r8.c     // Catch:{ all -> 0x00de }
            boolean r9 = r9.equals(r1)     // Catch:{ all -> 0x00de }
            if (r9 != 0) goto L_0x0097
            android.os.Handler r9 = r8.i     // Catch:{ all -> 0x00de }
            com.microsoft.appcenter.h r10 = new com.microsoft.appcenter.h     // Catch:{ all -> 0x00de }
            r10.<init>(r8)     // Catch:{ all -> 0x00de }
            r9.post(r10)     // Catch:{ all -> 0x00de }
        L_0x0097:
            monitor-exit(r8)
            return r2
        L_0x0099:
            r8.f4672b = r9     // Catch:{ all -> 0x00de }
            android.os.HandlerThread r9 = new android.os.HandlerThread     // Catch:{ all -> 0x00de }
            java.lang.String r10 = "AppCenter.Looper"
            r9.<init>(r10)     // Catch:{ all -> 0x00de }
            r8.p = r9     // Catch:{ all -> 0x00de }
            android.os.HandlerThread r9 = r8.p     // Catch:{ all -> 0x00de }
            r9.start()     // Catch:{ all -> 0x00de }
            android.os.Handler r9 = new android.os.Handler     // Catch:{ all -> 0x00de }
            android.os.HandlerThread r10 = r8.p     // Catch:{ all -> 0x00de }
            android.os.Looper r10 = r10.getLooper()     // Catch:{ all -> 0x00de }
            r9.<init>(r10)     // Catch:{ all -> 0x00de }
            r8.i = r9     // Catch:{ all -> 0x00de }
            com.microsoft.appcenter.i r9 = new com.microsoft.appcenter.i     // Catch:{ all -> 0x00de }
            r9.<init>(r8)     // Catch:{ all -> 0x00de }
            r8.q = r9     // Catch:{ all -> 0x00de }
            java.util.HashSet r9 = new java.util.HashSet     // Catch:{ all -> 0x00de }
            r9.<init>()     // Catch:{ all -> 0x00de }
            r8.n = r9     // Catch:{ all -> 0x00de }
            java.util.HashSet r9 = new java.util.HashSet     // Catch:{ all -> 0x00de }
            r9.<init>()     // Catch:{ all -> 0x00de }
            r8.o = r9     // Catch:{ all -> 0x00de }
            android.os.Handler r9 = r8.i     // Catch:{ all -> 0x00de }
            com.microsoft.appcenter.j r10 = new com.microsoft.appcenter.j     // Catch:{ all -> 0x00de }
            r10.<init>(r8, r11)     // Catch:{ all -> 0x00de }
            r9.post(r10)     // Catch:{ all -> 0x00de }
            java.lang.String r9 = "AppCenter"
            java.lang.String r10 = "App Center SDK configured successfully."
            com.microsoft.appcenter.utils.a.c(r9, r10)     // Catch:{ all -> 0x00de }
            monitor-exit(r8)
            return r2
        L_0x00de:
            r9 = move-exception
            monitor-exit(r8)
            goto L_0x00e2
        L_0x00e1:
            throw r9
        L_0x00e2:
            goto L_0x00e1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.microsoft.appcenter.f.a(android.app.Application, java.lang.String, boolean):boolean");
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(Runnable runnable, Runnable runnable2) {
        if (c()) {
            k kVar = new k(this, runnable, runnable2);
            if (Thread.currentThread() == this.p) {
                runnable.run();
            } else {
                this.i.post(kVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        boolean a2 = this.h.a(this.r);
        c<Boolean> cVar = this.s;
        if (cVar != null) {
            cVar.a(Boolean.valueOf(a2));
        }
    }

    @SafeVarargs
    private final synchronized void a(boolean z, Class<? extends n>... clsArr) {
        if (clsArr == null) {
            a.e("AppCenter", "Cannot start services, services array is null. Failed to start services.");
            return;
        }
        if (this.f4672b == null) {
            StringBuilder sb = new StringBuilder();
            for (Class<? extends n> name : clsArr) {
                sb.append("\t");
                sb.append(name.getName());
                sb.append("\n");
            }
            a.e("AppCenter", "Cannot start services, App Center has not been configured. Failed to start the following services:\n" + ((Object) sb));
            return;
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (Class<? extends n> cls : clsArr) {
            if (cls == null) {
                a.d("AppCenter", "Skipping null service, please check your varargs/array does not contain any null reference.");
            } else {
                try {
                    n nVar = (n) cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
                    if (z) {
                        String k2 = nVar.k();
                        if (this.n.contains(nVar)) {
                            if (this.o.remove(nVar)) {
                                arrayList2.add(nVar);
                            } else {
                                a.d("AppCenter", "App Center has already started the service with class name: " + nVar.k());
                            }
                        } else if (this.c != null || !nVar.c()) {
                            a(nVar, arrayList);
                        } else {
                            a.e("AppCenter", "App Center was started without app secret, but the service requires it; not starting service " + k2 + ".");
                        }
                    } else if (!this.n.contains(nVar)) {
                        String k3 = nVar.k();
                        if (nVar.c()) {
                            a.e("AppCenter", "This service cannot be started from a library: " + k3 + ".");
                        } else if (a(nVar, arrayList)) {
                            this.o.add(nVar);
                        }
                    }
                } catch (Exception e2) {
                    a.c("AppCenter", "Failed to get service instance '" + cls.getName() + "', skipping it.", e2);
                }
            }
        }
        this.i.post(new l(this, arrayList2, arrayList, z));
    }

    private boolean a(n nVar, Collection<n> collection) {
        String k2 = nVar.k();
        if (q.a(k2)) {
            a.b("AppCenter", "Instrumentation variable to disable service has been set; not starting service " + k2 + ".");
            return false;
        }
        nVar.a(this.q);
        this.f4672b.registerActivityLifecycleCallbacks(nVar);
        this.n.add(nVar);
        collection.add(nVar);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.f.a(android.app.Application, java.lang.String, boolean):boolean
     arg types: [android.app.Application, java.lang.String, int]
     candidates:
      com.microsoft.appcenter.f.a(android.app.Application, java.lang.String, java.lang.Class<? extends com.microsoft.appcenter.n>[]):void
      com.microsoft.appcenter.f.a(android.app.Application, java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.microsoft.appcenter.f.a(boolean, java.lang.Class<? extends com.microsoft.appcenter.n>[]):void
     arg types: [int, java.lang.Class<? extends com.microsoft.appcenter.n>[]]
     candidates:
      com.microsoft.appcenter.f.a(com.microsoft.appcenter.n, java.util.Collection<com.microsoft.appcenter.n>):boolean
      com.microsoft.appcenter.f.a(java.lang.Runnable, java.lang.Runnable):void
      com.microsoft.appcenter.f.a(boolean, java.lang.Class<? extends com.microsoft.appcenter.n>[]):void */
    public synchronized void a(Application application, String str, Class<? extends n>[] clsArr) {
        if (str != null) {
            if (!str.isEmpty()) {
                if (a(application, str, true)) {
                    a(true, clsArr);
                }
            }
        }
        a.e("AppCenter", "appSecret may not be null or empty.");
    }
}
