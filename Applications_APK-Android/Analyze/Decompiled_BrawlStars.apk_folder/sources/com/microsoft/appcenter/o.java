package com.microsoft.appcenter;

import android.content.Context;
import com.microsoft.appcenter.utils.a;

/* compiled from: Constants */
public class o {

    /* renamed from: a  reason: collision with root package name */
    public static String f4701a = null;

    /* renamed from: b  reason: collision with root package name */
    public static boolean f4702b = false;

    public static void a(Context context) {
        if (context != null) {
            try {
                f4701a = context.getFilesDir().getAbsolutePath();
            } catch (Exception e) {
                a.c("AppCenter", "Exception thrown when accessing the application filesystem", e);
            }
        }
        if (context != null && context.getApplicationInfo() != null) {
            f4702b = (context.getApplicationInfo().flags & 2) > 0;
        }
    }
}
