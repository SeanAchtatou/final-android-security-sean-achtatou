package com.microsoft.appcenter;

import android.app.Application;
import android.content.Context;
import com.microsoft.appcenter.a.b;
import com.microsoft.appcenter.b.a.a.g;
import java.util.Map;

/* compiled from: AppCenterService */
public interface n extends Application.ActivityLifecycleCallbacks {
    void a(Context context, b bVar, String str, String str2, boolean z);

    void a(m mVar);

    void a(String str);

    void a(boolean z);

    boolean b();

    boolean c();

    Map<String, g> d();

    String k();
}
