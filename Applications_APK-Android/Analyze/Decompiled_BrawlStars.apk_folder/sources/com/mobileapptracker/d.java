package com.mobileapptracker;

import android.content.Context;
import android.provider.Settings;
import java.lang.ref.WeakReference;

final class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MATParameters f4810a;

    /* renamed from: b  reason: collision with root package name */
    private final WeakReference<Context> f4811b;

    public d(MATParameters mATParameters, Context context) {
        this.f4810a = mATParameters;
        this.f4811b = new WeakReference<>(context);
    }

    public final void run() {
        int i = 1;
        try {
            new Class[1][0] = Context.class;
            Object invoke = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getDeclaredMethod("getAdvertisingIdInfo", Context.class).invoke(null, this.f4811b.get());
            String str = (String) Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info").getDeclaredMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
            boolean booleanValue = ((Boolean) Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info").getDeclaredMethod("isLimitAdTrackingEnabled", new Class[0]).invoke(invoke, new Object[0])).booleanValue();
            if (this.f4810a.f4766b.params == null) {
                this.f4810a.setGoogleAdvertisingId(str);
                if (!booleanValue) {
                    i = 0;
                }
                this.f4810a.setGoogleAdTrackingLimited(Integer.toString(i));
            }
            this.f4810a.f4766b.setGoogleAdvertisingId(str, booleanValue);
        } catch (Exception e) {
            e.printStackTrace();
            if (this.f4810a.f4766b.params == null) {
                this.f4810a.setAndroidId(Settings.Secure.getString(this.f4811b.get().getContentResolver(), "android_id"));
            }
            this.f4810a.f4766b.setAndroidId(Settings.Secure.getString(this.f4811b.get().getContentResolver(), "android_id"));
        }
    }
}
