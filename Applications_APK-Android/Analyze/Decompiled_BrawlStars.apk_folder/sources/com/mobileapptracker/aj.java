package com.mobileapptracker;

final class aj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f4788a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4789b;

    aj(MobileAppTracker mobileAppTracker, boolean z) {
        this.f4789b = mobileAppTracker;
        this.f4788a = z;
    }

    public final void run() {
        MATParameters mATParameters;
        int i;
        if (this.f4788a) {
            mATParameters = this.f4789b.params;
            i = 1;
        } else {
            mATParameters = this.f4789b.params;
            i = 0;
        }
        mATParameters.setAllowDuplicates(Integer.toString(i));
    }
}
