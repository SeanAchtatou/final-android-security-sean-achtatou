package com.mobileapptracker;

final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f4848a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4849b;

    y(MobileAppTracker mobileAppTracker, String str) {
        this.f4849b = mobileAppTracker;
        this.f4848a = str;
    }

    public final void run() {
        MATParameters mATParameters;
        String packageName;
        String str = this.f4848a;
        if (str == null || str.equals("")) {
            mATParameters = this.f4849b.params;
            packageName = this.f4849b.mContext.getPackageName();
        } else {
            mATParameters = this.f4849b.params;
            packageName = this.f4848a;
        }
        mATParameters.setPackageName(packageName);
    }
}
