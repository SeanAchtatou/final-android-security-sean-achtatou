package com.mobileapptracker;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.util.Patterns;
import java.util.HashMap;
import java.util.Set;

final class am implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f4792a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4793b;

    am(MobileAppTracker mobileAppTracker, boolean z) {
        this.f4793b = mobileAppTracker;
        this.f4792a = z;
    }

    public final void run() {
        boolean z = this.f4793b.mContext.checkCallingOrSelfPermission("android.permission.GET_ACCOUNTS") == 0;
        if (!this.f4792a || !z) {
            this.f4793b.params.setUserEmail(null);
            return;
        }
        Account[] accountsByType = AccountManager.get(this.f4793b.mContext).getAccountsByType("com.google");
        if (accountsByType.length > 0) {
            this.f4793b.params.setUserEmail(accountsByType[0].name);
        }
        HashMap hashMap = new HashMap();
        for (Account account : AccountManager.get(this.f4793b.mContext).getAccounts()) {
            if (Patterns.EMAIL_ADDRESS.matcher(account.name).matches()) {
                hashMap.put(account.name, account.type);
            }
        }
        Set keySet = hashMap.keySet();
        this.f4793b.params.setUserEmails((String[]) keySet.toArray(new String[keySet.size()]));
    }
}
