package com.mobileapptracker;

import android.content.Context;
import android.content.SharedPreferences;
import com.facebook.share.internal.ShareConstants;
import java.util.concurrent.Semaphore;
import org.json.JSONException;
import org.json.JSONObject;

public class MATEventQueue {
    /* access modifiers changed from: private */
    public static long d;

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f4760a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Semaphore f4761b = new Semaphore(1, true);
    /* access modifiers changed from: private */
    public MobileAppTracker c;

    public class Add implements Runnable {

        /* renamed from: b  reason: collision with root package name */
        private String f4763b = null;
        private String c = null;
        private JSONObject d = null;
        private boolean e = false;

        protected Add(String str, String str2, JSONObject jSONObject, boolean z) {
            this.f4763b = str;
            this.c = str2;
            this.d = jSONObject;
            this.e = z;
        }

        public void run() {
            try {
                MATEventQueue.this.f4761b.acquire();
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("link", this.f4763b);
                    jSONObject.put(ShareConstants.WEB_DIALOG_PARAM_DATA, this.c);
                    jSONObject.put("post_body", this.d);
                    jSONObject.put("first_session", this.e);
                    int queueSize = MATEventQueue.this.getQueueSize() + 1;
                    MATEventQueue.this.setQueueSize(queueSize);
                    MATEventQueue.this.setQueueItemForKey(jSONObject, Integer.toString(queueSize));
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            } catch (Throwable th) {
                MATEventQueue.this.f4761b.release();
                throw th;
            }
            MATEventQueue.this.f4761b.release();
        }
    }

    public class Dump implements Runnable {
        protected Dump() {
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(6:40|39|41|42|43|44) */
        /* JADX WARNING: Can't wrap try/catch for region: R(9:31|(2:33|(5:34|35|36|37|38))|48|(7:51|(5:54|(1:56)|57|(6:60|(1:62)(1:63)|64|65|66|67)|59)|53|64|65|66|67)|50|64|65|66|67) */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c3, code lost:
            r3 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
            r3.printStackTrace();
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* JADX WARNING: Removed duplicated region for block: B:11:0x001e A[Catch:{ InterruptedException -> 0x0151 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r12 = this;
                com.mobileapptracker.MATEventQueue r0 = com.mobileapptracker.MATEventQueue.this
                int r0 = r0.getQueueSize()
                if (r0 != 0) goto L_0x0009
                return
            L_0x0009:
                com.mobileapptracker.MATEventQueue r1 = com.mobileapptracker.MATEventQueue.this     // Catch:{ InterruptedException -> 0x0151 }
                java.util.concurrent.Semaphore r1 = r1.f4761b     // Catch:{ InterruptedException -> 0x0151 }
                r1.acquire()     // Catch:{ InterruptedException -> 0x0151 }
                r1 = 50
                r2 = 1
                if (r0 <= r1) goto L_0x001b
                int r1 = r0 + -50
            L_0x0019:
                int r1 = r1 + r2
                goto L_0x001c
            L_0x001b:
                r1 = 1
            L_0x001c:
                if (r1 > r0) goto L_0x0142
                java.lang.String r3 = java.lang.Integer.toString(r1)     // Catch:{ InterruptedException -> 0x0151 }
                com.mobileapptracker.MATEventQueue r4 = com.mobileapptracker.MATEventQueue.this     // Catch:{ InterruptedException -> 0x0151 }
                java.lang.String r4 = r4.getKeyFromQueue(r3)     // Catch:{ InterruptedException -> 0x0151 }
                if (r4 == 0) goto L_0x014c
                org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0139 }
                r5.<init>(r4)     // Catch:{ JSONException -> 0x0139 }
                java.lang.String r6 = "link"
                java.lang.String r6 = r5.getString(r6)     // Catch:{ JSONException -> 0x0139 }
                java.lang.String r7 = "data"
                java.lang.String r7 = r5.getString(r7)     // Catch:{ JSONException -> 0x0139 }
                java.lang.String r8 = "post_body"
                org.json.JSONObject r8 = r5.getJSONObject(r8)     // Catch:{ JSONException -> 0x0139 }
                java.lang.String r9 = "first_session"
                boolean r5 = r5.getBoolean(r9)     // Catch:{ JSONException -> 0x0139 }
                if (r5 == 0) goto L_0x0065
                com.mobileapptracker.MATEventQueue r5 = com.mobileapptracker.MATEventQueue.this     // Catch:{ InterruptedException -> 0x0151 }
                com.mobileapptracker.MobileAppTracker r5 = r5.c     // Catch:{ InterruptedException -> 0x0151 }
                java.util.concurrent.ExecutorService r5 = r5.d     // Catch:{ InterruptedException -> 0x0151 }
                monitor-enter(r5)     // Catch:{ InterruptedException -> 0x0151 }
                com.mobileapptracker.MATEventQueue r9 = com.mobileapptracker.MATEventQueue.this     // Catch:{ all -> 0x0062 }
                com.mobileapptracker.MobileAppTracker r9 = r9.c     // Catch:{ all -> 0x0062 }
                java.util.concurrent.ExecutorService r9 = r9.d     // Catch:{ all -> 0x0062 }
                r10 = 60000(0xea60, double:2.9644E-319)
                r9.wait(r10)     // Catch:{ all -> 0x0062 }
                monitor-exit(r5)     // Catch:{ all -> 0x0062 }
                goto L_0x0065
            L_0x0062:
                r0 = move-exception
                monitor-exit(r5)     // Catch:{ InterruptedException -> 0x0151 }
                throw r0     // Catch:{ InterruptedException -> 0x0151 }
            L_0x0065:
                com.mobileapptracker.MATEventQueue r5 = com.mobileapptracker.MATEventQueue.this     // Catch:{ InterruptedException -> 0x0151 }
                com.mobileapptracker.MobileAppTracker r5 = r5.c     // Catch:{ InterruptedException -> 0x0151 }
                if (r5 == 0) goto L_0x0132
                com.mobileapptracker.MATEventQueue r5 = com.mobileapptracker.MATEventQueue.this     // Catch:{ InterruptedException -> 0x0151 }
                com.mobileapptracker.MobileAppTracker r5 = r5.c     // Catch:{ InterruptedException -> 0x0151 }
                boolean r5 = r5.makeRequest(r6, r7, r8)     // Catch:{ InterruptedException -> 0x0151 }
                r7 = 0
                if (r5 == 0) goto L_0x0084
                com.mobileapptracker.MATEventQueue r4 = com.mobileapptracker.MATEventQueue.this     // Catch:{ InterruptedException -> 0x0151 }
                r4.removeKeyFromQueue(r3)     // Catch:{ InterruptedException -> 0x0151 }
                long unused = com.mobileapptracker.MATEventQueue.d = r7     // Catch:{ InterruptedException -> 0x0151 }
                goto L_0x0019
            L_0x0084:
                int r1 = r1 + -1
                java.lang.String r5 = "&sdk_retry_attempt="
                int r5 = r6.indexOf(r5)     // Catch:{ InterruptedException -> 0x0151 }
                if (r5 <= 0) goto L_0x00c7
                int r5 = r5 + 19
                int r9 = r5 + 1
                r10 = -1
            L_0x0093:
                java.lang.String r11 = r6.substring(r5, r9)     // Catch:{ StringIndexOutOfBoundsException -> 0x009e }
                int r10 = java.lang.Integer.parseInt(r11)     // Catch:{  }
                int r9 = r9 + 1
                goto L_0x0093
            L_0x009e:
                int r10 = r10 + r2
                java.lang.String r5 = "&sdk_retry_attempt=\\d+"
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0151 }
                java.lang.String r11 = "&sdk_retry_attempt="
                r9.<init>(r11)     // Catch:{ InterruptedException -> 0x0151 }
                r9.append(r10)     // Catch:{ InterruptedException -> 0x0151 }
                java.lang.String r9 = r9.toString()     // Catch:{ InterruptedException -> 0x0151 }
                java.lang.String r5 = r6.replaceFirst(r5, r9)     // Catch:{ InterruptedException -> 0x0151 }
                org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00c3 }
                r6.<init>(r4)     // Catch:{ JSONException -> 0x00c3 }
                java.lang.String r4 = "link"
                r6.put(r4, r5)     // Catch:{ JSONException -> 0x00c3 }
                com.mobileapptracker.MATEventQueue r4 = com.mobileapptracker.MATEventQueue.this     // Catch:{ JSONException -> 0x00c3 }
                r4.setQueueItemForKey(r6, r3)     // Catch:{ JSONException -> 0x00c3 }
                goto L_0x00c7
            L_0x00c3:
                r3 = move-exception
                r3.printStackTrace()     // Catch:{ InterruptedException -> 0x0151 }
            L_0x00c7:
                long r3 = com.mobileapptracker.MATEventQueue.d     // Catch:{ InterruptedException -> 0x0151 }
                r5 = 30
                int r9 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
                if (r9 != 0) goto L_0x00d5
            L_0x00d1:
                long unused = com.mobileapptracker.MATEventQueue.d = r5     // Catch:{ InterruptedException -> 0x0151 }
                goto L_0x010d
            L_0x00d5:
                long r3 = com.mobileapptracker.MATEventQueue.d     // Catch:{ InterruptedException -> 0x0151 }
                r7 = 90
                int r9 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r9 > 0) goto L_0x00e3
            L_0x00df:
                long unused = com.mobileapptracker.MATEventQueue.d = r7     // Catch:{ InterruptedException -> 0x0151 }
                goto L_0x010d
            L_0x00e3:
                long r3 = com.mobileapptracker.MATEventQueue.d     // Catch:{ InterruptedException -> 0x0151 }
                r5 = 600(0x258, double:2.964E-321)
                int r9 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
                if (r9 > 0) goto L_0x00ee
                goto L_0x00d1
            L_0x00ee:
                long r3 = com.mobileapptracker.MATEventQueue.d     // Catch:{ InterruptedException -> 0x0151 }
                r7 = 3600(0xe10, double:1.7786E-320)
                int r9 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r9 > 0) goto L_0x00f9
                goto L_0x00df
            L_0x00f9:
                long r3 = com.mobileapptracker.MATEventQueue.d     // Catch:{ InterruptedException -> 0x0151 }
                int r5 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
                if (r5 > 0) goto L_0x0107
                r3 = 21600(0x5460, double:1.0672E-319)
                long unused = com.mobileapptracker.MATEventQueue.d = r3     // Catch:{ InterruptedException -> 0x0151 }
                goto L_0x010d
            L_0x0107:
                r3 = 86400(0x15180, double:4.26873E-319)
                long unused = com.mobileapptracker.MATEventQueue.d = r3     // Catch:{ InterruptedException -> 0x0151 }
            L_0x010d:
                r3 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r7 = java.lang.Math.random()     // Catch:{ InterruptedException -> 0x0151 }
                double r7 = r7 * r5
                double r7 = r7 + r3
                long r3 = com.mobileapptracker.MATEventQueue.d     // Catch:{ InterruptedException -> 0x0151 }
                double r3 = (double) r3
                java.lang.Double.isNaN(r3)
                double r7 = r7 * r3
                r3 = 4652007308841189376(0x408f400000000000, double:1000.0)
                double r7 = r7 * r3
                long r3 = (long) r7
                java.lang.Thread.sleep(r3)     // Catch:{ InterruptedException -> 0x0019 }
                goto L_0x0019
            L_0x0132:
                com.mobileapptracker.MATEventQueue r4 = com.mobileapptracker.MATEventQueue.this     // Catch:{ InterruptedException -> 0x0151 }
            L_0x0134:
                r4.removeKeyFromQueue(r3)     // Catch:{ InterruptedException -> 0x0151 }
                goto L_0x0019
            L_0x0139:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ InterruptedException -> 0x0151 }
                com.mobileapptracker.MATEventQueue r0 = com.mobileapptracker.MATEventQueue.this     // Catch:{ InterruptedException -> 0x0151 }
                r0.removeKeyFromQueue(r3)     // Catch:{ InterruptedException -> 0x0151 }
            L_0x0142:
                com.mobileapptracker.MATEventQueue r0 = com.mobileapptracker.MATEventQueue.this
                java.util.concurrent.Semaphore r0 = r0.f4761b
                r0.release()
                return
            L_0x014c:
                com.mobileapptracker.MATEventQueue r4 = com.mobileapptracker.MATEventQueue.this     // Catch:{ InterruptedException -> 0x0151 }
                goto L_0x0134
            L_0x014f:
                r0 = move-exception
                goto L_0x0156
            L_0x0151:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x014f }
                goto L_0x0142
            L_0x0156:
                com.mobileapptracker.MATEventQueue r1 = com.mobileapptracker.MATEventQueue.this
                java.util.concurrent.Semaphore r1 = r1.f4761b
                r1.release()
                goto L_0x0161
            L_0x0160:
                throw r0
            L_0x0161:
                goto L_0x0160
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobileapptracker.MATEventQueue.Dump.run():void");
        }
    }

    public MATEventQueue(Context context, MobileAppTracker mobileAppTracker) {
        this.f4760a = context.getSharedPreferences("mat_queue", 0);
        this.c = mobileAppTracker;
    }

    /* access modifiers changed from: protected */
    public synchronized String getKeyFromQueue(String str) {
        return this.f4760a.getString(str, null);
    }

    /* access modifiers changed from: protected */
    public synchronized int getQueueSize() {
        return this.f4760a.getInt("queuesize", 0);
    }

    /* access modifiers changed from: protected */
    public synchronized void removeKeyFromQueue(String str) {
        setQueueSize(getQueueSize() - 1);
        SharedPreferences.Editor edit = this.f4760a.edit();
        edit.remove(str);
        edit.commit();
    }

    /* access modifiers changed from: protected */
    public synchronized void setQueueItemForKey(JSONObject jSONObject, String str) {
        SharedPreferences.Editor edit = this.f4760a.edit();
        edit.putString(str, jSONObject.toString());
        edit.commit();
    }

    /* access modifiers changed from: protected */
    public synchronized void setQueueSize(int i) {
        SharedPreferences.Editor edit = this.f4760a.edit();
        if (i < 0) {
            i = 0;
        }
        edit.putInt("queuesize", i);
        edit.commit();
    }
}
