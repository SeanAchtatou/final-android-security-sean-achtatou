package com.mobileapptracker;

import java.util.HashMap;
import org.json.JSONObject;

public class MATEventItem {
    public String attribute_sub1;
    public String attribute_sub2;
    public String attribute_sub3;
    public String attribute_sub4;
    public String attribute_sub5;
    public String itemname;
    public int quantity;
    public double revenue;
    public double unitPrice;

    public MATEventItem(String str) {
        this.itemname = str;
    }

    public String getAttrStringByName(String str) {
        if (str.equals("itemname")) {
            return this.itemname;
        }
        if (str.equals("quantity")) {
            return Integer.toString(this.quantity);
        }
        if (str.equals("unitPrice")) {
            return Double.toString(this.unitPrice);
        }
        if (str.equals("revenue")) {
            return Double.toString(this.revenue);
        }
        if (str.equals("attribute_sub1")) {
            return this.attribute_sub1;
        }
        if (str.equals("attribute_sub2")) {
            return this.attribute_sub2;
        }
        if (str.equals("attribute_sub3")) {
            return this.attribute_sub3;
        }
        if (str.equals("attribute_sub4")) {
            return this.attribute_sub4;
        }
        if (str.equals("attribute_sub5")) {
            return this.attribute_sub5;
        }
        return null;
    }

    public JSONObject toJSON() {
        HashMap hashMap = new HashMap();
        String str = this.itemname;
        if (str != null) {
            hashMap.put("item", str);
        }
        hashMap.put("quantity", Integer.toString(this.quantity));
        hashMap.put("unit_price", Double.toString(this.unitPrice));
        double d = this.revenue;
        if (d != 0.0d) {
            hashMap.put("revenue", Double.toString(d));
        }
        String str2 = this.attribute_sub1;
        if (str2 != null) {
            hashMap.put("attribute_sub1", str2);
        }
        String str3 = this.attribute_sub2;
        if (str3 != null) {
            hashMap.put("attribute_sub2", str3);
        }
        String str4 = this.attribute_sub3;
        if (str4 != null) {
            hashMap.put("attribute_sub3", str4);
        }
        String str5 = this.attribute_sub4;
        if (str5 != null) {
            hashMap.put("attribute_sub4", str5);
        }
        String str6 = this.attribute_sub5;
        if (str6 != null) {
            hashMap.put("attribute_sub5", str6);
        }
        return new JSONObject(hashMap);
    }

    public MATEventItem withAttribute1(String str) {
        this.attribute_sub1 = str;
        return this;
    }

    public MATEventItem withAttribute2(String str) {
        this.attribute_sub2 = str;
        return this;
    }

    public MATEventItem withAttribute3(String str) {
        this.attribute_sub3 = str;
        return this;
    }

    public MATEventItem withAttribute4(String str) {
        this.attribute_sub4 = str;
        return this;
    }

    public MATEventItem withAttribute5(String str) {
        this.attribute_sub5 = str;
        return this;
    }

    public MATEventItem withQuantity(int i) {
        this.quantity = i;
        return this;
    }

    public MATEventItem withRevenue(double d) {
        this.revenue = d;
        return this;
    }

    public MATEventItem withUnitPrice(double d) {
        this.unitPrice = d;
        return this;
    }
}
