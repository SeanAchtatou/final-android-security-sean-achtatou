package com.mobileapptracker;

public class MATPreloadData {
    public String advertiserSubAd;
    public String advertiserSubAdgroup;
    public String advertiserSubCampaign;
    public String advertiserSubKeyword;
    public String advertiserSubPublisher;
    public String advertiserSubSite;
    public String agencyId;
    public String offerId;
    public String publisherId;
    public String publisherReferenceId;
    public String publisherSub1;
    public String publisherSub2;
    public String publisherSub3;
    public String publisherSub4;
    public String publisherSub5;
    public String publisherSubAd;
    public String publisherSubAdgroup;
    public String publisherSubCampaign;
    public String publisherSubKeyword;
    public String publisherSubPublisher;
    public String publisherSubSite;

    public MATPreloadData(String str) {
        this.publisherId = str;
    }

    public MATPreloadData withAdvertiserSubAd(String str) {
        this.advertiserSubAd = str;
        return this;
    }

    public MATPreloadData withAdvertiserSubAdgroup(String str) {
        this.advertiserSubAdgroup = str;
        return this;
    }

    public MATPreloadData withAdvertiserSubCampaign(String str) {
        this.advertiserSubCampaign = str;
        return this;
    }

    public MATPreloadData withAdvertiserSubKeyword(String str) {
        this.advertiserSubKeyword = str;
        return this;
    }

    public MATPreloadData withAdvertiserSubPublisher(String str) {
        this.advertiserSubPublisher = str;
        return this;
    }

    public MATPreloadData withAdvertiserSubSite(String str) {
        this.advertiserSubSite = str;
        return this;
    }

    public MATPreloadData withAgencyId(String str) {
        this.agencyId = str;
        return this;
    }

    public MATPreloadData withOfferId(String str) {
        this.offerId = str;
        return this;
    }

    public MATPreloadData withPublisherReferenceId(String str) {
        this.publisherReferenceId = str;
        return this;
    }

    public MATPreloadData withPublisherSub1(String str) {
        this.publisherSub1 = str;
        return this;
    }

    public MATPreloadData withPublisherSub2(String str) {
        this.publisherSub2 = str;
        return this;
    }

    public MATPreloadData withPublisherSub3(String str) {
        this.publisherSub3 = str;
        return this;
    }

    public MATPreloadData withPublisherSub4(String str) {
        this.publisherSub4 = str;
        return this;
    }

    public MATPreloadData withPublisherSub5(String str) {
        this.publisherSub5 = str;
        return this;
    }

    public MATPreloadData withPublisherSubAd(String str) {
        this.publisherSubAd = str;
        return this;
    }

    public MATPreloadData withPublisherSubAdgroup(String str) {
        this.publisherSubAdgroup = str;
        return this;
    }

    public MATPreloadData withPublisherSubCampaign(String str) {
        this.publisherSubCampaign = str;
        return this;
    }

    public MATPreloadData withPublisherSubKeyword(String str) {
        this.publisherSubKeyword = str;
        return this;
    }

    public MATPreloadData withPublisherSubPublisher(String str) {
        this.publisherSubPublisher = str;
        return this;
    }

    public MATPreloadData withPublisherSubSite(String str) {
        this.publisherSubSite = str;
        return this;
    }
}
