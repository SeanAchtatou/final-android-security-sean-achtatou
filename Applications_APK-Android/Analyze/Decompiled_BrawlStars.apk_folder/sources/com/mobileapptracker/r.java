package com.mobileapptracker;

final class r implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f4834a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4835b;

    r(MobileAppTracker mobileAppTracker, boolean z) {
        this.f4835b = mobileAppTracker;
        this.f4834a = z;
    }

    public final void run() {
        MATParameters mATParameters;
        int i;
        if (this.f4834a) {
            mATParameters = this.f4835b.params;
            i = 1;
        } else {
            mATParameters = this.f4835b.params;
            i = 0;
        }
        mATParameters.setIsPayingUser(Integer.toString(i));
    }
}
