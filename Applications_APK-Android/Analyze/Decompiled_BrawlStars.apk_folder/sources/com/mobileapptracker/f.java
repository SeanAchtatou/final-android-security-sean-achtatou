package com.mobileapptracker;

import com.facebook.AccessToken;
import com.facebook.appevents.codeless.internal.Constants;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class f {

    /* renamed from: a  reason: collision with root package name */
    private static MATParameters f4814a;

    public static synchronized String a(MATEvent mATEvent) {
        String str;
        String currencyCode;
        String sb;
        synchronized (f.class) {
            f4814a = MATParameters.getInstance();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("connection_type=" + f4814a.getConnectionType());
            a(sb2, "age", f4814a.getAge());
            a(sb2, "altitude", f4814a.getAltitude());
            a(sb2, "android_id", f4814a.getAndroidId());
            a(sb2, "android_id_md5", f4814a.getAndroidIdMd5());
            a(sb2, "android_id_sha1", f4814a.getAndroidIdSha1());
            a(sb2, "android_id_sha256", f4814a.getAndroidIdSha256());
            a(sb2, "app_ad_tracking", f4814a.getAppAdTrackingEnabled());
            a(sb2, NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, f4814a.getAppName());
            a(sb2, "app_version", f4814a.getAppVersion());
            a(sb2, "app_version_name", f4814a.getAppVersionName());
            a(sb2, "country_code", f4814a.getCountryCode());
            a(sb2, "device_brand", f4814a.getDeviceBrand());
            a(sb2, "device_carrier", f4814a.getDeviceCarrier());
            a(sb2, "device_cpu_type", f4814a.getDeviceCpuType());
            a(sb2, "device_cpu_subtype", f4814a.getDeviceCpuSubtype());
            a(sb2, "device_model", f4814a.getDeviceModel());
            a(sb2, "device_id", f4814a.getDeviceId());
            a(sb2, "existing_user", f4814a.getExistingUser());
            a(sb2, "facebook_user_id", f4814a.getFacebookUserId());
            a(sb2, "gender", f4814a.getGender());
            a(sb2, "google_aid", f4814a.getGoogleAdvertisingId());
            a(sb2, "google_ad_tracking_disabled", f4814a.getGoogleAdTrackingLimited());
            a(sb2, "google_user_id", f4814a.getGoogleUserId());
            a(sb2, "insdate", f4814a.getInstallDate());
            a(sb2, "installer", f4814a.getInstaller());
            a(sb2, "install_referrer", f4814a.getInstallReferrer());
            a(sb2, "is_paying_user", f4814a.getIsPayingUser());
            a(sb2, "language", f4814a.getLanguage());
            a(sb2, "last_open_log_id", f4814a.getLastOpenLogId());
            a(sb2, "latitude", f4814a.getLatitude());
            a(sb2, "longitude", f4814a.getLongitude());
            a(sb2, "mac_address", f4814a.getMacAddress());
            a(sb2, "mat_id", f4814a.getMatId());
            a(sb2, "mobile_country_code", f4814a.getMCC());
            a(sb2, "mobile_network_code", f4814a.getMNC());
            a(sb2, "open_log_id", f4814a.getOpenLogId());
            a(sb2, "os_version", f4814a.getOsVersion());
            a(sb2, "sdk_plugin", f4814a.getPluginName());
            a(sb2, "android_purchase_status", f4814a.getPurchaseStatus());
            a(sb2, "referrer_delay", f4814a.getReferrerDelay());
            a(sb2, "screen_density", f4814a.getScreenDensity());
            a(sb2, "screen_layout_size", f4814a.getScreenWidth() + "x" + f4814a.getScreenHeight());
            a(sb2, "sdk_version", f4814a.getSdkVersion());
            a(sb2, "truste_tpid", f4814a.getTRUSTeId());
            a(sb2, "twitter_user_id", f4814a.getTwitterUserId());
            a(sb2, "conversion_user_agent", f4814a.getUserAgent());
            a(sb2, "user_email_md5", f4814a.getUserEmailMd5());
            a(sb2, "user_email_sha1", f4814a.getUserEmailSha1());
            a(sb2, "user_email_sha256", f4814a.getUserEmailSha256());
            a(sb2, AccessToken.USER_ID_KEY, f4814a.getUserId());
            a(sb2, "user_name_md5", f4814a.getUserNameMd5());
            a(sb2, "user_name_sha1", f4814a.getUserNameSha1());
            a(sb2, "user_name_sha256", f4814a.getUserNameSha256());
            a(sb2, "user_phone_md5", f4814a.getPhoneNumberMd5());
            a(sb2, "user_phone_sha1", f4814a.getPhoneNumberSha1());
            a(sb2, "user_phone_sha256", f4814a.getPhoneNumberSha256());
            a(sb2, "attribute_sub1", mATEvent.getAttribute1());
            a(sb2, "attribute_sub2", mATEvent.getAttribute2());
            a(sb2, "attribute_sub3", mATEvent.getAttribute3());
            a(sb2, "attribute_sub4", mATEvent.getAttribute4());
            a(sb2, "attribute_sub5", mATEvent.getAttribute5());
            a(sb2, "content_id", mATEvent.getContentId());
            a(sb2, "content_type", mATEvent.getContentType());
            if (mATEvent.getCurrencyCode() != null) {
                str = "currency_code";
                currencyCode = mATEvent.getCurrencyCode();
            } else {
                str = "currency_code";
                currencyCode = f4814a.getCurrencyCode();
            }
            a(sb2, str, currencyCode);
            if (mATEvent.getDate1() != null) {
                a(sb2, "date1", Long.toString(mATEvent.getDate1().getTime() / 1000));
            }
            if (mATEvent.getDate2() != null) {
                a(sb2, "date2", Long.toString(mATEvent.getDate2().getTime() / 1000));
            }
            if (mATEvent.getLevel() != 0) {
                a(sb2, "level", Integer.toString(mATEvent.getLevel()));
            }
            if (mATEvent.getQuantity() != 0) {
                a(sb2, "quantity", Integer.toString(mATEvent.getQuantity()));
            }
            if (mATEvent.getRating() != 0.0d) {
                a(sb2, "rating", Double.toString(mATEvent.getRating()));
            }
            a(sb2, "search_string", mATEvent.getSearchString());
            a(sb2, "advertiser_ref_id", mATEvent.getRefId());
            a(sb2, "revenue", Double.toString(mATEvent.getRevenue()));
            if (mATEvent.getDeviceForm() != null) {
                a(sb2, "device_form", mATEvent.getDeviceForm());
            }
            sb = sb2.toString();
        }
        return sb;
    }

    public static String a(MATEvent mATEvent, MATPreloadData mATPreloadData, boolean z) {
        f4814a = MATParameters.getInstance();
        StringBuilder sb = new StringBuilder("https://");
        sb.append(f4814a.getAdvertiserId());
        sb.append(".");
        sb.append(z ? "debug.engine.mobileapptracking.com" : "engine.mobileapptracking.com");
        sb.append("/serve?ver=");
        sb.append(f4814a.getSdkVersion());
        sb.append("&transaction_id=");
        sb.append(UUID.randomUUID().toString());
        a(sb, ServerProtocol.DIALOG_PARAM_SDK_VERSION, Constants.PLATFORM);
        a(sb, NativeProtocol.WEB_DIALOG_ACTION, f4814a.getAction());
        a(sb, "advertiser_id", f4814a.getAdvertiserId());
        a(sb, "package_name", f4814a.getPackageName());
        a(sb, "referral_source", f4814a.getReferralSource());
        a(sb, "referral_url", f4814a.getReferralUrl());
        a(sb, "site_id", f4814a.getSiteId());
        a(sb, "tracking_id", f4814a.getTrackingId());
        if (mATEvent.getEventId() != 0) {
            a(sb, "site_event_id", Integer.toString(mATEvent.getEventId()));
        }
        if (!f4814a.getAction().equals("session")) {
            a(sb, "site_event_name", mATEvent.getEventName());
        }
        if (mATPreloadData != null) {
            sb.append("&attr_set=1");
            a(sb, "publisher_id", mATPreloadData.publisherId);
            a(sb, "offer_id", mATPreloadData.offerId);
            a(sb, "agency_id", mATPreloadData.agencyId);
            a(sb, "publisher_ref_id", mATPreloadData.publisherReferenceId);
            a(sb, "publisher_sub_publisher", mATPreloadData.publisherSubPublisher);
            a(sb, "publisher_sub_site", mATPreloadData.publisherSubSite);
            a(sb, "publisher_sub_campaign", mATPreloadData.publisherSubCampaign);
            a(sb, "publisher_sub_adgroup", mATPreloadData.publisherSubAdgroup);
            a(sb, "publisher_sub_ad", mATPreloadData.publisherSubAd);
            a(sb, "publisher_sub_keyword", mATPreloadData.publisherSubKeyword);
            a(sb, "advertiser_sub_publisher", mATPreloadData.advertiserSubPublisher);
            a(sb, "advertiser_sub_site", mATPreloadData.advertiserSubSite);
            a(sb, "advertiser_sub_campaign", mATPreloadData.advertiserSubCampaign);
            a(sb, "advertiser_sub_adgroup", mATPreloadData.advertiserSubAdgroup);
            a(sb, "advertiser_sub_ad", mATPreloadData.advertiserSubAd);
            a(sb, "advertiser_sub_keyword", mATPreloadData.advertiserSubKeyword);
            a(sb, "publisher_sub1", mATPreloadData.publisherSub1);
            a(sb, "publisher_sub2", mATPreloadData.publisherSub2);
            a(sb, "publisher_sub3", mATPreloadData.publisherSub3);
            a(sb, "publisher_sub4", mATPreloadData.publisherSub4);
            a(sb, "publisher_sub5", mATPreloadData.publisherSub5);
        }
        String allowDuplicates = f4814a.getAllowDuplicates();
        if (allowDuplicates != null && Integer.parseInt(allowDuplicates) == 1) {
            sb.append("&skip_dup=1");
        }
        if (z) {
            sb.append("&debug=1");
        }
        return sb.toString();
    }

    public static synchronized String a(String str, MATEncryption mATEncryption) {
        String sb;
        synchronized (f.class) {
            StringBuilder sb2 = new StringBuilder(str);
            MATParameters instance = MATParameters.getInstance();
            f4814a = instance;
            if (instance != null) {
                String googleAdvertisingId = f4814a.getGoogleAdvertisingId();
                if (googleAdvertisingId != null && !str.contains("&google_aid=")) {
                    a(sb2, "google_aid", googleAdvertisingId);
                    a(sb2, "google_ad_tracking_disabled", f4814a.getGoogleAdTrackingLimited());
                }
                String androidId = f4814a.getAndroidId();
                if (androidId != null && !str.contains("&android_id=")) {
                    a(sb2, "android_id", androidId);
                }
                String installReferrer = f4814a.getInstallReferrer();
                if (installReferrer != null && !str.contains("&install_referrer=")) {
                    a(sb2, "install_referrer", installReferrer);
                }
                String userAgent = f4814a.getUserAgent();
                if (userAgent != null && !str.contains("&conversion_user_agent=")) {
                    a(sb2, "conversion_user_agent", userAgent);
                }
                String facebookUserId = f4814a.getFacebookUserId();
                if (facebookUserId != null && !str.contains("&facebook_user_id=")) {
                    a(sb2, "facebook_user_id", facebookUserId);
                }
            }
            if (!str.contains("&system_date=")) {
                a(sb2, "system_date", Long.toString(new Date().getTime() / 1000));
            }
            sb = sb2.toString();
            try {
                sb = MATEncryption.bytesToHex(mATEncryption.encrypt(sb));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb;
    }

    public static synchronized JSONObject a(JSONArray jSONArray, String str, String str2, JSONArray jSONArray2) {
        JSONObject jSONObject;
        synchronized (f.class) {
            jSONObject = new JSONObject();
            if (jSONArray != null) {
                try {
                    jSONObject.put(ShareConstants.WEB_DIALOG_PARAM_DATA, jSONArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (str != null) {
                jSONObject.put("store_iap_data", str);
            }
            if (str2 != null) {
                jSONObject.put("store_iap_signature", str2);
            }
            if (jSONArray2 != null) {
                jSONObject.put("user_emails", jSONArray2);
            }
        }
        return jSONObject;
    }

    private static synchronized void a(StringBuilder sb, String str, String str2) {
        synchronized (f.class) {
            if (str2 != null) {
                if (!str2.equals("")) {
                    try {
                        sb.append("&" + str + "=" + URLEncoder.encode(str2, "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        "failed encoding value " + str2 + " for key " + str;
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
