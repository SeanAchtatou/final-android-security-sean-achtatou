package com.mobileapptracker;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.ServerProtocol;
import com.mobileapptracker.MATEventQueue;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;
import org.json.JSONObject;

public class MobileAppTracker {
    public static final int GENDER_FEMALE = 1;
    public static final int GENDER_MALE = 0;
    private static volatile MobileAppTracker s;

    /* renamed from: a  reason: collision with root package name */
    boolean f4767a;

    /* renamed from: b  reason: collision with root package name */
    boolean f4768b;
    boolean c;
    ExecutorService d;
    private final String e = "heF9BATUfWuISyO8";
    protected MATEventQueue eventQueue;
    private b f;
    private MATPreloadData g;
    private g h;
    private MATEncryption i;
    protected boolean initialized;
    protected boolean isRegistered;
    private MATResponse j;
    private boolean k;
    private boolean l;
    private int m;
    protected Context mContext;
    protected MATTestRequest matRequest;
    private boolean n;
    protected BroadcastReceiver networkStateReceiver;
    private boolean o;
    private boolean p;
    protected MATParameters params;
    protected ExecutorService pubQueue;
    private long q;
    private long r;

    protected MobileAppTracker() {
    }

    private String a(int i2) {
        if (!this.n) {
            return "";
        }
        b.b(this.params.getUserAgent());
        return this.f.a(this.mContext, i2);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00e3, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.mobileapptracker.MATEvent r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            boolean r0 = r7.initialized     // Catch:{ all -> 0x00e4 }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r7)
            return
        L_0x0007:
            r7.dumpQueue()     // Catch:{ all -> 0x00e4 }
            com.mobileapptracker.MATParameters r0 = r7.params     // Catch:{ all -> 0x00e4 }
            java.lang.String r1 = "conversion"
            r0.setAction(r1)     // Catch:{ all -> 0x00e4 }
            java.util.Date r0 = new java.util.Date     // Catch:{ all -> 0x00e4 }
            r0.<init>()     // Catch:{ all -> 0x00e4 }
            java.lang.String r1 = r8.getEventName()     // Catch:{ all -> 0x00e4 }
            if (r1 == 0) goto L_0x0065
            java.lang.String r1 = r8.getEventName()     // Catch:{ all -> 0x00e4 }
            boolean r2 = r7.p     // Catch:{ all -> 0x00e4 }
            if (r2 == 0) goto L_0x0027
            com.mobileapptracker.c.a(r8)     // Catch:{ all -> 0x00e4 }
        L_0x0027:
            java.lang.String r2 = "close"
            boolean r2 = r1.equals(r2)     // Catch:{ all -> 0x00e4 }
            if (r2 == 0) goto L_0x0031
            monitor-exit(r7)
            return
        L_0x0031:
            java.lang.String r2 = "open"
            boolean r2 = r1.equals(r2)     // Catch:{ all -> 0x00e4 }
            if (r2 != 0) goto L_0x0051
            java.lang.String r2 = "install"
            boolean r2 = r1.equals(r2)     // Catch:{ all -> 0x00e4 }
            if (r2 != 0) goto L_0x0051
            java.lang.String r2 = "update"
            boolean r2 = r1.equals(r2)     // Catch:{ all -> 0x00e4 }
            if (r2 != 0) goto L_0x0051
            java.lang.String r2 = "session"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x00e4 }
            if (r1 == 0) goto L_0x0065
        L_0x0051:
            com.mobileapptracker.MATParameters r1 = r7.params     // Catch:{ all -> 0x00e4 }
            java.lang.String r2 = "session"
            r1.setAction(r2)     // Catch:{ all -> 0x00e4 }
            java.util.Date r1 = new java.util.Date     // Catch:{ all -> 0x00e4 }
            long r2 = r0.getTime()     // Catch:{ all -> 0x00e4 }
            r4 = 60000(0xea60, double:2.9644E-319)
            long r2 = r2 + r4
            r1.<init>(r2)     // Catch:{ all -> 0x00e4 }
        L_0x0065:
            double r0 = r8.getRevenue()     // Catch:{ all -> 0x00e4 }
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x0079
            com.mobileapptracker.MATParameters r0 = r7.params     // Catch:{ all -> 0x00e4 }
            r1 = 1
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ all -> 0x00e4 }
            r0.setIsPayingUser(r1)     // Catch:{ all -> 0x00e4 }
        L_0x0079:
            com.mobileapptracker.MATPreloadData r0 = r7.g     // Catch:{ all -> 0x00e4 }
            boolean r1 = r7.k     // Catch:{ all -> 0x00e4 }
            java.lang.String r0 = com.mobileapptracker.f.a(r8, r0, r1)     // Catch:{ all -> 0x00e4 }
            java.lang.String r1 = com.mobileapptracker.f.a(r8)     // Catch:{ all -> 0x00e4 }
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ all -> 0x00e4 }
            r2.<init>()     // Catch:{ all -> 0x00e4 }
            java.util.List r3 = r8.getEventItems()     // Catch:{ all -> 0x00e4 }
            r4 = 0
            if (r3 == 0) goto L_0x00b0
            r3 = 0
        L_0x0092:
            java.util.List r5 = r8.getEventItems()     // Catch:{ all -> 0x00e4 }
            int r5 = r5.size()     // Catch:{ all -> 0x00e4 }
            if (r3 >= r5) goto L_0x00b0
            java.util.List r5 = r8.getEventItems()     // Catch:{ all -> 0x00e4 }
            java.lang.Object r5 = r5.get(r3)     // Catch:{ all -> 0x00e4 }
            com.mobileapptracker.MATEventItem r5 = (com.mobileapptracker.MATEventItem) r5     // Catch:{ all -> 0x00e4 }
            org.json.JSONObject r5 = r5.toJSON()     // Catch:{ all -> 0x00e4 }
            r2.put(r5)     // Catch:{ all -> 0x00e4 }
            int r3 = r3 + 1
            goto L_0x0092
        L_0x00b0:
            java.lang.String r3 = r8.getReceiptData()     // Catch:{ all -> 0x00e4 }
            java.lang.String r5 = r8.getReceiptSignature()     // Catch:{ all -> 0x00e4 }
            com.mobileapptracker.MATParameters r6 = r7.params     // Catch:{ all -> 0x00e4 }
            org.json.JSONArray r6 = r6.getUserEmails()     // Catch:{ all -> 0x00e4 }
            org.json.JSONObject r2 = com.mobileapptracker.f.a(r2, r3, r5, r6)     // Catch:{ all -> 0x00e4 }
            com.mobileapptracker.MATTestRequest r3 = r7.matRequest     // Catch:{ all -> 0x00e4 }
            if (r3 == 0) goto L_0x00cb
            com.mobileapptracker.MATTestRequest r3 = r7.matRequest     // Catch:{ all -> 0x00e4 }
            r3.constructedRequest(r0, r1, r2)     // Catch:{ all -> 0x00e4 }
        L_0x00cb:
            boolean r3 = r7.o     // Catch:{ all -> 0x00e4 }
            r7.addEventToQueue(r0, r1, r2, r3)     // Catch:{ all -> 0x00e4 }
            r7.o = r4     // Catch:{ all -> 0x00e4 }
            r7.dumpQueue()     // Catch:{ all -> 0x00e4 }
            com.mobileapptracker.MATResponse r0 = r7.j     // Catch:{ all -> 0x00e4 }
            if (r0 == 0) goto L_0x00e2
            com.mobileapptracker.MATResponse r0 = r7.j     // Catch:{ all -> 0x00e4 }
            java.lang.String r8 = r8.getRefId()     // Catch:{ all -> 0x00e4 }
            r0.enqueuedActionWithRefId(r8)     // Catch:{ all -> 0x00e4 }
        L_0x00e2:
            monitor-exit(r7)
            return
        L_0x00e4:
            r8 = move-exception
            monitor-exit(r7)
            goto L_0x00e8
        L_0x00e7:
            throw r8
        L_0x00e8:
            goto L_0x00e7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobileapptracker.MobileAppTracker.a(com.mobileapptracker.MATEvent):void");
    }

    public static synchronized MobileAppTracker getInstance() {
        MobileAppTracker mobileAppTracker;
        synchronized (MobileAppTracker.class) {
            mobileAppTracker = s;
        }
        return mobileAppTracker;
    }

    public static synchronized MobileAppTracker init(Context context, String str, String str2) {
        MobileAppTracker mobileAppTracker;
        synchronized (MobileAppTracker.class) {
            if (s == null) {
                MobileAppTracker mobileAppTracker2 = new MobileAppTracker();
                s = mobileAppTracker2;
                mobileAppTracker2.mContext = context.getApplicationContext();
                s.pubQueue = Executors.newSingleThreadExecutor();
                s.initAll(str, str2);
            }
            mobileAppTracker = s;
        }
        return mobileAppTracker;
    }

    public static boolean isOnline(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /* access modifiers changed from: protected */
    public void addEventToQueue(String str, String str2, JSONObject jSONObject, boolean z) {
        ExecutorService executorService = this.d;
        MATEventQueue mATEventQueue = this.eventQueue;
        mATEventQueue.getClass();
        executorService.execute(new MATEventQueue.Add(str, str2, jSONObject, z));
    }

    /* access modifiers changed from: protected */
    public void dumpQueue() {
        if (isOnline(this.mContext)) {
            ExecutorService executorService = this.d;
            MATEventQueue mATEventQueue = this.eventQueue;
            mATEventQueue.getClass();
            executorService.execute(new MATEventQueue.Dump());
        }
    }

    public String getAction() {
        return this.params.getAction();
    }

    public String getAdvertiserId() {
        return this.params.getAdvertiserId();
    }

    public int getAge() {
        return Integer.parseInt(this.params.getAge());
    }

    public double getAltitude() {
        return Double.parseDouble(this.params.getAltitude());
    }

    public boolean getAppAdTrackingEnabled() {
        return Integer.parseInt(this.params.getAppAdTrackingEnabled()) == 1;
    }

    public String getAppName() {
        return this.params.getAppName();
    }

    public int getAppVersion() {
        return Integer.parseInt(this.params.getAppVersion());
    }

    public String getConnectionType() {
        return this.params.getConnectionType();
    }

    public String getCountryCode() {
        return this.params.getCountryCode();
    }

    public String getCurrencyCode() {
        return this.params.getCurrencyCode();
    }

    public String getDeviceBrand() {
        return this.params.getDeviceBrand();
    }

    public String getDeviceCarrier() {
        return this.params.getDeviceCarrier();
    }

    public String getDeviceModel() {
        return this.params.getDeviceModel();
    }

    public boolean getExistingUser() {
        return Integer.parseInt(this.params.getExistingUser()) == 1;
    }

    public String getFacebookUserId() {
        return this.params.getFacebookUserId();
    }

    public int getGender() {
        return Integer.parseInt(this.params.getGender());
    }

    public boolean getGoogleAdTrackingLimited() {
        return Integer.parseInt(this.params.getGoogleAdTrackingLimited()) != 0;
    }

    public String getGoogleAdvertisingId() {
        return this.params.getGoogleAdvertisingId();
    }

    public String getGoogleUserId() {
        return this.params.getGoogleUserId();
    }

    public long getInstallDate() {
        return Long.parseLong(this.params.getInstallDate());
    }

    public String getInstallReferrer() {
        return this.params.getInstallReferrer();
    }

    public boolean getIsPayingUser() {
        return this.params.getIsPayingUser().equals(AppEventsConstants.EVENT_PARAM_VALUE_YES);
    }

    public String getLanguage() {
        return this.params.getLanguage();
    }

    public String getLastOpenLogId() {
        return this.params.getLastOpenLogId();
    }

    public double getLatitude() {
        return Double.parseDouble(this.params.getLatitude());
    }

    public double getLongitude() {
        return Double.parseDouble(this.params.getLongitude());
    }

    public String getMCC() {
        return this.params.getMCC();
    }

    public String getMNC() {
        return this.params.getMNC();
    }

    public String getMatId() {
        return this.params.getMatId();
    }

    public String getOpenLogId() {
        return this.params.getOpenLogId();
    }

    public String getOsVersion() {
        return this.params.getOsVersion();
    }

    public String getPackageName() {
        return this.params.getPackageName();
    }

    public String getPluginName() {
        return this.params.getPluginName();
    }

    public String getRefId() {
        return this.params.getRefId();
    }

    public String getReferralSource() {
        return this.params.getReferralSource();
    }

    public String getReferralUrl() {
        return this.params.getReferralUrl();
    }

    public Double getRevenue() {
        return Double.valueOf(Double.parseDouble(this.params.getRevenue()));
    }

    public String getSDKVersion() {
        return this.params.getSdkVersion();
    }

    public String getScreenDensity() {
        return this.params.getScreenDensity();
    }

    public String getScreenHeight() {
        return this.params.getScreenHeight();
    }

    public String getScreenWidth() {
        return this.params.getScreenWidth();
    }

    public String getSiteId() {
        return this.params.getSiteId();
    }

    public String getTRUSTeId() {
        return this.params.getTRUSTeId();
    }

    public String getTwitterUserId() {
        return this.params.getTwitterUserId();
    }

    public String getUserAgent() {
        return this.params.getUserAgent();
    }

    public String getUserEmail() {
        return this.params.getUserEmail();
    }

    public String getUserId() {
        return this.params.getUserId();
    }

    public String getUserName() {
        return this.params.getUserName();
    }

    /* access modifiers changed from: protected */
    public void initAll(String str, String str2) {
        this.f = b.a(str, str2, this.mContext.getPackageName());
        this.params = MATParameters.init(this, this.mContext, str, str2);
        this.d = Executors.newSingleThreadExecutor();
        this.h = new g();
        this.i = new MATEncryption(str2.trim(), "heF9BATUfWuISyO8");
        this.q = System.currentTimeMillis();
        this.f4768b = !this.mContext.getSharedPreferences("com.mobileapptracking", 0).getString("mat_referrer", "").equals("");
        this.n = false;
        this.o = true;
        this.initialized = false;
        this.isRegistered = false;
        this.k = false;
        this.p = false;
        this.eventQueue = new MATEventQueue(this.mContext, this);
        dumpQueue();
        this.networkStateReceiver = new h(this);
        if (this.isRegistered) {
            try {
                this.mContext.unregisterReceiver(this.networkStateReceiver);
            } catch (IllegalArgumentException unused) {
            }
            this.isRegistered = false;
        }
        this.mContext.registerReceiver(this.networkStateReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        this.isRegistered = true;
        this.initialized = true;
    }

    /* access modifiers changed from: protected */
    public boolean makeRequest(String str, String str2, JSONObject jSONObject) {
        String a2 = f.a(str2, this.i);
        JSONObject a3 = g.a(str + "&data=" + a2, jSONObject, this.k);
        if (a3 == null) {
            MATResponse mATResponse = this.j;
            if (mATResponse != null) {
                mATResponse.didFailWithError(a3);
            }
            return true;
        } else if (!a3.has(GraphResponse.SUCCESS_KEY)) {
            return false;
        } else {
            if (this.j != null) {
                try {
                    if (a3.getString(GraphResponse.SUCCESS_KEY).equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
                        this.j.didSucceedWithData(a3);
                    } else {
                        this.j.didFailWithError(a3);
                    }
                } catch (JSONException e2) {
                    e2.printStackTrace();
                    return false;
                }
            }
            try {
                if (a3.getString("site_event_type").equals("open")) {
                    String string = a3.getString("log_id");
                    if (getOpenLogId().equals("")) {
                        this.params.setOpenLogId(string);
                    }
                    this.params.setLastOpenLogId(string);
                }
            } catch (JSONException unused) {
            }
            return true;
        }
    }

    public void measureEvent(int i2) {
        measureEvent(new MATEvent(i2));
    }

    public void measureEvent(MATEvent mATEvent) {
        this.pubQueue.execute(new s(this, mATEvent));
    }

    public void measureEvent(String str) {
        measureEvent(new MATEvent(str));
    }

    public void measureSession() {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("com.mobileapptracking", 0);
        if (!sharedPreferences.contains("mat_installed")) {
            sharedPreferences.edit().putBoolean("mat_installed", true).commit();
            this.n = true;
        }
        this.c = false;
        measureEvent(new MATEvent("session"));
    }

    public void setAdvertiserId(String str) {
        this.pubQueue.execute(new ad(this, str));
    }

    public void setAge(int i2) {
        this.pubQueue.execute(new an(this, i2));
    }

    public void setAllowDuplicates(boolean z) {
        this.pubQueue.execute(new aj(this, z));
        if (z) {
            new Handler(Looper.getMainLooper()).post(new ak(this));
        }
    }

    public void setAltitude(double d2) {
        this.pubQueue.execute(new ao(this, d2));
    }

    public void setAndroidId(String str) {
        b.c(str);
        MATParameters mATParameters = this.params;
        if (mATParameters != null) {
            mATParameters.setAndroidId(str);
        }
        if (this.l) {
            a(this.m);
        }
    }

    public void setAndroidIdMd5(String str) {
        this.pubQueue.execute(new ap(this, str));
    }

    public void setAndroidIdSha1(String str) {
        this.pubQueue.execute(new aq(this, str));
    }

    public void setAndroidIdSha256(String str) {
        this.pubQueue.execute(new ar(this, str));
    }

    public void setAppAdTrackingEnabled(boolean z) {
        this.pubQueue.execute(new as(this, z));
    }

    public void setCurrencyCode(String str) {
        this.pubQueue.execute(new i(this, str));
    }

    public void setDebugMode(boolean z) {
        this.k = z;
        if (z) {
            new Handler(Looper.getMainLooper()).post(new al(this));
        }
    }

    public void setDeferredDeeplink(boolean z, int i2) {
        this.l = z;
        this.m = i2;
    }

    public void setDeviceBrand(String str) {
        this.pubQueue.execute(new j(this, str));
    }

    public void setDeviceId(String str) {
        this.pubQueue.execute(new k(this, str));
    }

    public void setDeviceModel(String str) {
        this.pubQueue.execute(new l(this, str));
    }

    public void setEmailCollection(boolean z) {
        this.pubQueue.execute(new am(this, z));
    }

    public void setExistingUser(boolean z) {
        this.pubQueue.execute(new m(this, z));
    }

    public void setFacebookEventLogging(boolean z, Context context, boolean z2) {
        this.p = z;
        if (z) {
            c.a(context, z2);
        }
    }

    public void setFacebookUserId(String str) {
        this.pubQueue.execute(new n(this, str));
    }

    public void setGender(int i2) {
        this.pubQueue.execute(new o(this, i2));
    }

    public void setGoogleAdvertisingId(String str, boolean z) {
        b.a(str, z ? 1 : 0);
        MATParameters mATParameters = this.params;
        if (mATParameters != null) {
            mATParameters.setGoogleAdvertisingId(str);
            this.params.setGoogleAdTrackingLimited(Integer.toString(z));
        }
        this.f4767a = true;
        if (this.f4768b && !this.c) {
            synchronized (this.d) {
                this.d.notifyAll();
                this.c = true;
            }
        }
        if (this.l) {
            a(this.m);
        }
    }

    public void setGoogleUserId(String str) {
        this.pubQueue.execute(new p(this, str));
    }

    public void setInstallReferrer(String str) {
        this.f4768b = true;
        this.r = System.currentTimeMillis();
        MATParameters mATParameters = this.params;
        if (mATParameters != null) {
            mATParameters.setReferrerDelay(this.r - this.q);
        }
        this.pubQueue.execute(new q(this, str));
    }

    public void setIsPayingUser(boolean z) {
        this.pubQueue.execute(new r(this, z));
    }

    public void setLatitude(double d2) {
        this.pubQueue.execute(new t(this, d2));
    }

    public void setLocation(Location location) {
        this.pubQueue.execute(new u(this, location));
    }

    public void setLongitude(double d2) {
        this.pubQueue.execute(new v(this, d2));
    }

    public void setMATResponse(MATResponse mATResponse) {
        this.j = mATResponse;
        b.a(mATResponse);
    }

    public void setMacAddress(String str) {
        this.pubQueue.execute(new w(this, str));
    }

    public void setOsVersion(String str) {
        this.pubQueue.execute(new x(this, str));
    }

    public void setPackageName(String str) {
        b.a(str);
        this.pubQueue.execute(new y(this, str));
    }

    public void setPhoneNumber(String str) {
        this.pubQueue.execute(new z(this, str));
    }

    public void setPluginName(String str) {
        if (Arrays.asList(a.f4769a).contains(str)) {
            this.pubQueue.execute(new ai(this, str));
        } else if (this.k) {
            throw new IllegalArgumentException("Plugin name not acceptable");
        }
    }

    public void setPreloadedApp(MATPreloadData mATPreloadData) {
        this.g = mATPreloadData;
    }

    public void setReferralSources(Activity activity) {
        this.pubQueue.execute(new aa(this, activity));
    }

    public void setSiteId(String str) {
        this.pubQueue.execute(new ab(this, str));
    }

    public void setTRUSTeId(String str) {
        this.pubQueue.execute(new ac(this, str));
    }

    public void setTwitterUserId(String str) {
        this.pubQueue.execute(new ae(this, str));
    }

    public void setUserEmail(String str) {
        this.pubQueue.execute(new af(this, str));
    }

    public void setUserId(String str) {
        this.pubQueue.execute(new ag(this, str));
    }

    public void setUserName(String str) {
        this.pubQueue.execute(new ah(this, str));
    }
}
