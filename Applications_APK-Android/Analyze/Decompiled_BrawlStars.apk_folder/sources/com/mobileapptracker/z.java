package com.mobileapptracker;

final class z implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f4850a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4851b;

    z(MobileAppTracker mobileAppTracker, String str) {
        this.f4851b = mobileAppTracker;
        this.f4850a = str;
    }

    public final void run() {
        String replaceAll = this.f4850a.replaceAll("\\D+", "");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < replaceAll.length(); i++) {
            sb.append(Integer.parseInt(String.valueOf(replaceAll.charAt(i))));
        }
        this.f4851b.params.setPhoneNumber(sb.toString());
    }
}
