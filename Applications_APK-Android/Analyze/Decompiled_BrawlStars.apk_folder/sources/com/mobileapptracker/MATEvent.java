package com.mobileapptracker;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class MATEvent implements Serializable {
    public static final String ACHIEVEMENT_UNLOCKED = "achievement_unlocked";
    public static final String ADDED_PAYMENT_INFO = "added_payment_info";
    public static final String ADD_TO_CART = "add_to_cart";
    public static final String ADD_TO_WISHLIST = "add_to_wishlist";
    public static final String CHECKOUT_INITIATED = "checkout_initiated";
    public static final String CONTENT_VIEW = "content_view";
    public static final String DEVICE_FORM_WEARABLE = "wearable";
    public static final String INVITE = "invite";
    public static final String LEVEL_ACHIEVED = "level_achieved";
    public static final String LOGIN = "login";
    public static final String PURCHASE = "purchase";
    public static final String RATED = "rated";
    public static final String REGISTRATION = "registration";
    public static final String RESERVATION = "reservation";
    public static final String SEARCH = "search";
    public static final String SHARE = "share";
    public static final String SPENT_CREDITS = "spent_credits";
    public static final String TUTORIAL_COMPLETE = "tutorial_complete";

    /* renamed from: a  reason: collision with root package name */
    private String f4758a;

    /* renamed from: b  reason: collision with root package name */
    private int f4759b;
    private double c;
    private String d;
    private String e;
    private List<MATEventItem> f;
    private String g;
    private String h;
    private String i;
    private String j;
    private int k;
    private int l;
    private String m;
    private double n;
    private Date o;
    private Date p;
    private String q;
    private String r;
    private String s;
    private String t;
    private String u;
    private String v;

    public MATEvent(int i2) {
        this.f4759b = i2;
    }

    public MATEvent(String str) {
        this.f4758a = str;
    }

    public String getAttribute1() {
        return this.q;
    }

    public String getAttribute2() {
        return this.r;
    }

    public String getAttribute3() {
        return this.s;
    }

    public String getAttribute4() {
        return this.t;
    }

    public String getAttribute5() {
        return this.u;
    }

    public String getContentId() {
        return this.j;
    }

    public String getContentType() {
        return this.i;
    }

    public String getCurrencyCode() {
        return this.d;
    }

    public Date getDate1() {
        return this.o;
    }

    public Date getDate2() {
        return this.p;
    }

    public String getDeviceForm() {
        return this.v;
    }

    public int getEventId() {
        return this.f4759b;
    }

    public List<MATEventItem> getEventItems() {
        return this.f;
    }

    public String getEventName() {
        return this.f4758a;
    }

    public int getLevel() {
        return this.k;
    }

    public int getQuantity() {
        return this.l;
    }

    public double getRating() {
        return this.n;
    }

    public String getReceiptData() {
        return this.g;
    }

    public String getReceiptSignature() {
        return this.h;
    }

    public String getRefId() {
        return this.e;
    }

    public double getRevenue() {
        return this.c;
    }

    public String getSearchString() {
        return this.m;
    }

    public MATEvent withAdvertiserRefId(String str) {
        this.e = str;
        return this;
    }

    public MATEvent withAttribute1(String str) {
        this.q = str;
        return this;
    }

    public MATEvent withAttribute2(String str) {
        this.r = str;
        return this;
    }

    public MATEvent withAttribute3(String str) {
        this.s = str;
        return this;
    }

    public MATEvent withAttribute4(String str) {
        this.t = str;
        return this;
    }

    public MATEvent withAttribute5(String str) {
        this.u = str;
        return this;
    }

    public MATEvent withContentId(String str) {
        this.j = str;
        return this;
    }

    public MATEvent withContentType(String str) {
        this.i = str;
        return this;
    }

    public MATEvent withCurrencyCode(String str) {
        this.d = str;
        return this;
    }

    public MATEvent withDate1(Date date) {
        this.o = date;
        return this;
    }

    public MATEvent withDate2(Date date) {
        this.p = date;
        return this;
    }

    public MATEvent withDeviceForm(String str) {
        this.v = str;
        return this;
    }

    public MATEvent withEventItems(List<MATEventItem> list) {
        this.f = list;
        return this;
    }

    public MATEvent withLevel(int i2) {
        this.k = i2;
        return this;
    }

    public MATEvent withQuantity(int i2) {
        this.l = i2;
        return this;
    }

    public MATEvent withRating(double d2) {
        this.n = d2;
        return this;
    }

    public MATEvent withReceipt(String str, String str2) {
        this.g = str;
        this.h = str2;
        return this;
    }

    public MATEvent withRevenue(double d2) {
        this.c = d2;
        return this;
    }

    public MATEvent withSearchString(String str) {
        this.m = str;
        return this;
    }
}
