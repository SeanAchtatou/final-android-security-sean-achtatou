package com.mobileapptracker;

import android.net.Uri;
import com.facebook.appevents.codeless.internal.Constants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

final class g {
    g() {
    }

    public static String a(int i) {
        Uri.Builder builder = new Uri.Builder();
        Uri.Builder scheme = builder.scheme("https");
        scheme.authority(b.a() + ".deeplink.mobileapptracking.com").appendPath("v1").appendPath("link.txt").appendQueryParameter("platform", Constants.PLATFORM).appendQueryParameter("advertiser_id", b.a()).appendQueryParameter("ver", "3.9.1").appendQueryParameter("package_name", b.c()).appendQueryParameter("ad_id", b.e() != null ? b.e() : b.g()).appendQueryParameter("user_agent", b.d());
        if (b.e() != null) {
            builder.appendQueryParameter("google_ad_tracking_disabled", Integer.toString(b.f()));
        }
        InputStream inputStream = null;
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(builder.build().toString()).openConnection();
            httpURLConnection.setReadTimeout(i);
            httpURLConnection.setConnectTimeout(i);
            httpURLConnection.setRequestProperty("X-MAT-Key", b.b());
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            InputStream inputStream2 = httpURLConnection.getResponseCode() == 200 ? httpURLConnection.getInputStream() : httpURLConnection.getErrorStream();
            String a2 = a(inputStream2);
            try {
                inputStream2.close();
                return a2;
            } catch (IOException e) {
                e.printStackTrace();
                return a2;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            try {
                inputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            return "";
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            throw th;
        }
    }

    private static String a(InputStream inputStream) {
        if (inputStream == null) {
            return "";
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                sb.append(readLine);
                sb.append("\n");
            } else {
                bufferedReader.close();
                return sb.toString();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01b6, code lost:
        if (r9 != null) goto L_0x01b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01bc, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01bd, code lost:
        r14.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01c1, code lost:
        throw r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0156, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x018b, code lost:
        r13 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x018d, code lost:
        r14 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x018e, code lost:
        r7 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0190, code lost:
        r13 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0191, code lost:
        r9 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        "Request error with URL " + r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01b8 A[SYNTHETIC, Splitter:B:102:0x01b8] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0065 A[Catch:{ Exception -> 0x0193 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0076 A[Catch:{ Exception -> 0x0193 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x007b A[Catch:{ Exception -> 0x0193 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0085 A[Catch:{ Exception -> 0x018d, all -> 0x018b }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a3 A[SYNTHETIC, Splitter:B:24:0x00a3] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0165  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0178 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0187 A[SYNTHETIC, Splitter:B:82:0x0187] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x018b A[ExcHandler: all (th java.lang.Throwable), Splitter:B:16:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0196 A[SYNTHETIC, Splitter:B:91:0x0196] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x01a8 A[SYNTHETIC, Splitter:B:95:0x01a8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static org.json.JSONObject a(java.lang.String r13, org.json.JSONObject r14, boolean r15) {
        /*
            java.lang.String r0 = "status"
            java.lang.String r1 = "conversion"
            java.lang.String r2 = "conversion_status"
            java.lang.String r3 = "options"
            java.lang.String r4 = "application/json"
            java.lang.String r5 = "errors"
            java.lang.String r6 = "log_action"
            r7 = 0
            java.net.URL r8 = new java.net.URL     // Catch:{ Exception -> 0x0193 }
            r8.<init>(r13)     // Catch:{ Exception -> 0x0193 }
            java.net.URLConnection r8 = r8.openConnection()     // Catch:{ Exception -> 0x0193 }
            java.net.HttpURLConnection r8 = (java.net.HttpURLConnection) r8     // Catch:{ Exception -> 0x0193 }
            r9 = 60000(0xea60, float:8.4078E-41)
            r8.setReadTimeout(r9)     // Catch:{ Exception -> 0x0193 }
            r8.setConnectTimeout(r9)     // Catch:{ Exception -> 0x0193 }
            r9 = 1
            r8.setDoInput(r9)     // Catch:{ Exception -> 0x0193 }
            if (r14 == 0) goto L_0x0057
            int r10 = r14.length()     // Catch:{ Exception -> 0x0193 }
            if (r10 != 0) goto L_0x0030
            goto L_0x0057
        L_0x0030:
            r8.setDoOutput(r9)     // Catch:{ Exception -> 0x0193 }
            java.lang.String r9 = "Content-Type"
            r8.setRequestProperty(r9, r4)     // Catch:{ Exception -> 0x0193 }
            java.lang.String r9 = "Accept"
            r8.setRequestProperty(r9, r4)     // Catch:{ Exception -> 0x0193 }
            java.lang.String r4 = "POST"
            r8.setRequestMethod(r4)     // Catch:{ Exception -> 0x0193 }
            java.io.OutputStream r4 = r8.getOutputStream()     // Catch:{ Exception -> 0x0193 }
            java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x0193 }
            java.lang.String r9 = "UTF-8"
            byte[] r14 = r14.getBytes(r9)     // Catch:{ Exception -> 0x0193 }
            r4.write(r14)     // Catch:{ Exception -> 0x0193 }
            r4.close()     // Catch:{ Exception -> 0x0193 }
            goto L_0x005c
        L_0x0057:
            java.lang.String r14 = "GET"
            r8.setRequestMethod(r14)     // Catch:{ Exception -> 0x0193 }
        L_0x005c:
            r8.connect()     // Catch:{ Exception -> 0x0193 }
            int r14 = r8.getResponseCode()     // Catch:{ Exception -> 0x0193 }
            if (r15 == 0) goto L_0x0072
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0193 }
            java.lang.String r9 = "Request completed with status "
            r4.<init>(r9)     // Catch:{ Exception -> 0x0193 }
            r4.append(r14)     // Catch:{ Exception -> 0x0193 }
            r4.toString()     // Catch:{ Exception -> 0x0193 }
        L_0x0072:
            r4 = 200(0xc8, float:2.8E-43)
            if (r14 != r4) goto L_0x007b
            java.io.InputStream r9 = r8.getInputStream()     // Catch:{ Exception -> 0x0193 }
            goto L_0x007f
        L_0x007b:
            java.io.InputStream r9 = r8.getErrorStream()     // Catch:{ Exception -> 0x0193 }
        L_0x007f:
            java.lang.String r10 = a(r9)     // Catch:{ Exception -> 0x018d, all -> 0x018b }
            if (r15 == 0) goto L_0x0092
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x018d, all -> 0x018b }
            java.lang.String r12 = "Server response: "
            r11.<init>(r12)     // Catch:{ Exception -> 0x018d, all -> 0x018b }
            r11.append(r10)     // Catch:{ Exception -> 0x018d, all -> 0x018b }
            r11.toString()     // Catch:{ Exception -> 0x018d, all -> 0x018b }
        L_0x0092:
            org.json.JSONObject r11 = new org.json.JSONObject     // Catch:{ Exception -> 0x018d, all -> 0x018b }
            r11.<init>()     // Catch:{ Exception -> 0x018d, all -> 0x018b }
            org.json.JSONTokener r12 = new org.json.JSONTokener     // Catch:{ Exception -> 0x0158, all -> 0x018b }
            r12.<init>(r10)     // Catch:{ Exception -> 0x0158, all -> 0x018b }
            org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ Exception -> 0x0158, all -> 0x018b }
            r10.<init>(r12)     // Catch:{ Exception -> 0x0158, all -> 0x018b }
            if (r15 == 0) goto L_0x015d
            int r11 = r10.length()     // Catch:{ Exception -> 0x0156, all -> 0x018b }
            if (r11 <= 0) goto L_0x015d
            boolean r11 = r10.has(r5)     // Catch:{ JSONException -> 0x0151 }
            if (r11 == 0) goto L_0x00d1
            org.json.JSONArray r11 = r10.getJSONArray(r5)     // Catch:{ JSONException -> 0x0151 }
            int r11 = r11.length()     // Catch:{ JSONException -> 0x0151 }
            if (r11 == 0) goto L_0x00d1
            org.json.JSONArray r0 = r10.getJSONArray(r5)     // Catch:{ JSONException -> 0x0151 }
            r1 = 0
            java.lang.String r0 = r0.getString(r1)     // Catch:{ JSONException -> 0x0151 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0151 }
            java.lang.String r2 = "Event was rejected by server with error: "
            r1.<init>(r2)     // Catch:{ JSONException -> 0x0151 }
            r1.append(r0)     // Catch:{ JSONException -> 0x0151 }
        L_0x00cc:
            r1.toString()     // Catch:{ JSONException -> 0x0151 }
            goto L_0x015d
        L_0x00d1:
            boolean r5 = r10.has(r6)     // Catch:{ JSONException -> 0x0151 }
            if (r5 == 0) goto L_0x012c
            java.lang.String r5 = r10.getString(r6)     // Catch:{ JSONException -> 0x0151 }
            java.lang.String r11 = "null"
            boolean r5 = r5.equals(r11)     // Catch:{ JSONException -> 0x0151 }
            if (r5 != 0) goto L_0x012c
            java.lang.String r5 = r10.getString(r6)     // Catch:{ JSONException -> 0x0151 }
            java.lang.String r11 = "false"
            boolean r5 = r5.equals(r11)     // Catch:{ JSONException -> 0x0151 }
            if (r5 != 0) goto L_0x012c
            java.lang.String r5 = r10.getString(r6)     // Catch:{ JSONException -> 0x0151 }
            java.lang.String r11 = "true"
            boolean r5 = r5.equals(r11)     // Catch:{ JSONException -> 0x0151 }
            if (r5 != 0) goto L_0x012c
            org.json.JSONObject r2 = r10.getJSONObject(r6)     // Catch:{ JSONException -> 0x0151 }
            boolean r3 = r2.has(r1)     // Catch:{ JSONException -> 0x0151 }
            if (r3 == 0) goto L_0x015d
            org.json.JSONObject r1 = r2.getJSONObject(r1)     // Catch:{ JSONException -> 0x0151 }
            boolean r2 = r1.has(r0)     // Catch:{ JSONException -> 0x0151 }
            if (r2 == 0) goto L_0x015d
            java.lang.String r0 = r1.getString(r0)     // Catch:{ JSONException -> 0x0151 }
            java.lang.String r2 = "rejected"
            boolean r0 = r0.equals(r2)     // Catch:{ JSONException -> 0x0151 }
            if (r0 == 0) goto L_0x015d
            java.lang.String r0 = "status_code"
            java.lang.String r0 = r1.getString(r0)     // Catch:{ JSONException -> 0x0151 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0151 }
            java.lang.String r2 = "Event was rejected by server: status code "
            r1.<init>(r2)     // Catch:{ JSONException -> 0x0151 }
            r1.append(r0)     // Catch:{ JSONException -> 0x0151 }
            goto L_0x00cc
        L_0x012c:
            boolean r0 = r10.has(r3)     // Catch:{ JSONException -> 0x0151 }
            if (r0 == 0) goto L_0x015d
            org.json.JSONObject r0 = r10.getJSONObject(r3)     // Catch:{ JSONException -> 0x0151 }
            boolean r1 = r0.has(r2)     // Catch:{ JSONException -> 0x0151 }
            if (r1 == 0) goto L_0x015d
            java.lang.String r0 = r0.getString(r2)     // Catch:{ JSONException -> 0x0151 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0151 }
            java.lang.String r2 = "Event was "
            r1.<init>(r2)     // Catch:{ JSONException -> 0x0151 }
            r1.append(r0)     // Catch:{ JSONException -> 0x0151 }
            java.lang.String r0 = " by server"
            r1.append(r0)     // Catch:{ JSONException -> 0x0151 }
            goto L_0x00cc
        L_0x0151:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0156, all -> 0x018b }
            goto L_0x015d
        L_0x0156:
            r0 = move-exception
            goto L_0x015a
        L_0x0158:
            r0 = move-exception
            r10 = r11
        L_0x015a:
            r0.printStackTrace()     // Catch:{ Exception -> 0x018d, all -> 0x018b }
        L_0x015d:
            java.lang.String r0 = "X-MAT-Responder"
            java.lang.String r13 = r8.getHeaderField(r0)     // Catch:{ Exception -> 0x018d, all -> 0x018b }
            if (r14 < r4) goto L_0x0174
            r15 = 300(0x12c, float:4.2E-43)
            if (r14 >= r15) goto L_0x0174
            if (r9 == 0) goto L_0x0173
            r9.close()     // Catch:{ IOException -> 0x016f }
            goto L_0x0173
        L_0x016f:
            r13 = move-exception
            r13.printStackTrace()
        L_0x0173:
            return r10
        L_0x0174:
            r15 = 400(0x190, float:5.6E-43)
            if (r14 != r15) goto L_0x0185
            if (r13 == 0) goto L_0x0185
            if (r9 == 0) goto L_0x0184
            r9.close()     // Catch:{ IOException -> 0x0180 }
            goto L_0x0184
        L_0x0180:
            r13 = move-exception
            r13.printStackTrace()
        L_0x0184:
            return r7
        L_0x0185:
            if (r9 == 0) goto L_0x01b0
            r9.close()     // Catch:{ IOException -> 0x01ac }
            goto L_0x01b0
        L_0x018b:
            r13 = move-exception
            goto L_0x01b6
        L_0x018d:
            r14 = move-exception
            r7 = r9
            goto L_0x0194
        L_0x0190:
            r13 = move-exception
            r9 = r7
            goto L_0x01b6
        L_0x0193:
            r14 = move-exception
        L_0x0194:
            if (r15 == 0) goto L_0x01a3
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ all -> 0x0190 }
            java.lang.String r0 = "Request error with URL "
            r15.<init>(r0)     // Catch:{ all -> 0x0190 }
            r15.append(r13)     // Catch:{ all -> 0x0190 }
            r15.toString()     // Catch:{ all -> 0x0190 }
        L_0x01a3:
            r14.printStackTrace()     // Catch:{ all -> 0x0190 }
            if (r7 == 0) goto L_0x01b0
            r7.close()     // Catch:{ IOException -> 0x01ac }
            goto L_0x01b0
        L_0x01ac:
            r13 = move-exception
            r13.printStackTrace()
        L_0x01b0:
            org.json.JSONObject r13 = new org.json.JSONObject
            r13.<init>()
            return r13
        L_0x01b6:
            if (r9 == 0) goto L_0x01c0
            r9.close()     // Catch:{ IOException -> 0x01bc }
            goto L_0x01c0
        L_0x01bc:
            r14 = move-exception
            r14.printStackTrace()
        L_0x01c0:
            goto L_0x01c2
        L_0x01c1:
            throw r13
        L_0x01c2:
            goto L_0x01c1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobileapptracker.g.a(java.lang.String, org.json.JSONObject, boolean):org.json.JSONObject");
    }
}
