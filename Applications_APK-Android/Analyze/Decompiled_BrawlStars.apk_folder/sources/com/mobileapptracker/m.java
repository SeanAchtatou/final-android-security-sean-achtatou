package com.mobileapptracker;

final class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f4824a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4825b;

    m(MobileAppTracker mobileAppTracker, boolean z) {
        this.f4825b = mobileAppTracker;
        this.f4824a = z;
    }

    public final void run() {
        MATParameters mATParameters;
        int i;
        if (this.f4824a) {
            mATParameters = this.f4825b.params;
            i = 1;
        } else {
            mATParameters = this.f4825b.params;
            i = 0;
        }
        mATParameters.setExistingUser(Integer.toString(i));
    }
}
