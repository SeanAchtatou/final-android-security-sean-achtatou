package com.mobileapptracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class h extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4815a;

    h(MobileAppTracker mobileAppTracker) {
        this.f4815a = mobileAppTracker;
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.f4815a.isRegistered) {
            this.f4815a.dumpQueue();
        }
    }
}
