package com.mobileapptracker;

final class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f4816a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4817b;

    i(MobileAppTracker mobileAppTracker, String str) {
        this.f4817b = mobileAppTracker;
        this.f4816a = str;
    }

    public final void run() {
        MATParameters mATParameters;
        String str;
        String str2 = this.f4816a;
        if (str2 == null || str2.equals("")) {
            mATParameters = this.f4817b.params;
            str = "USD";
        } else {
            mATParameters = this.f4817b.params;
            str = this.f4816a;
        }
        mATParameters.setCurrencyCode(str);
    }
}
