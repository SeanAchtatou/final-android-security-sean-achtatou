package com.mobileapptracker;

final class as implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f4804a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4805b;

    as(MobileAppTracker mobileAppTracker, boolean z) {
        this.f4805b = mobileAppTracker;
        this.f4804a = z;
    }

    public final void run() {
        MATParameters mATParameters;
        int i;
        if (this.f4804a) {
            mATParameters = this.f4805b.params;
            i = 1;
        } else {
            mATParameters = this.f4805b.params;
            i = 0;
        }
        mATParameters.setAppAdTrackingEnabled(Integer.toString(i));
    }
}
