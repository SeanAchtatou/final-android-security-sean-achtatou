package com.mobileapptracker;

import android.content.Context;
import android.location.Location;
import org.json.JSONArray;

public class MATParameters {
    private static MATParameters c;
    private String A = null;
    private String B = null;
    private String C = null;
    private String D = null;
    private String E = null;
    private String F = null;
    private String G = null;
    private String H = null;
    private String I = null;
    private String J = null;
    private Location K = null;
    private String L = null;
    private String M = null;
    private String N = null;
    private String O = null;
    private String P = null;
    private String Q = null;
    private String R;
    private String S;
    private String T;
    private String U = null;
    private String V = null;
    private String W = null;
    private String X = null;
    private String Y = null;
    private String Z = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f4765a;
    private String aa = null;
    private String ab = null;
    private String ac = null;
    private String ad = null;
    private String ae = null;
    private String af = null;
    private String ag = null;
    private String ah = null;
    /* access modifiers changed from: private */
    public String ai = null;
    private String aj;
    private String ak;
    private String al;
    private JSONArray am = null;
    private String an;
    private String ao;
    private String ap;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public MobileAppTracker f4766b;
    private String d = null;
    private String e = null;
    private String f = null;
    private String g = null;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;
    private String l = null;
    private String m = null;
    private String n = null;
    private String o = null;
    private String p = null;
    private String q = null;
    private String r = null;
    private String s = null;
    private String t = null;
    private String u = null;
    private String v = null;
    private String w = null;
    private String x = null;
    private String y = null;
    private String z = null;

    private synchronized String a(String str) {
        return this.f4765a.getSharedPreferences("com.mobileapptracking", 0).getString(str, "");
    }

    private synchronized void a(String str, String str2) {
        this.f4765a.getSharedPreferences("com.mobileapptracking", 0).edit().putString(str, str2).commit();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(17:1|2|3|4|5|6|7|12|(1:14)(1:15)|16|(1:18)(1:20)|19|21|(4:23|(1:25)|26|(3:28|29|30))(2:31|32)|33|(1:38)|39) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        setAppVersion(com.facebook.appevents.AppEventsConstants.EVENT_PARAM_VALUE_NO);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0075 */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00cb A[Catch:{ Exception -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00dc A[Catch:{ Exception -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0112 A[Catch:{ Exception -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0118 A[Catch:{ Exception -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0134 A[Catch:{ Exception -> 0x0184 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x015e A[SYNTHETIC, Splitter:B:31:0x015e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean a(android.content.Context r7, java.lang.String r8, java.lang.String r9) {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = 0
            java.lang.String r8 = r8.trim()     // Catch:{ Exception -> 0x0184 }
            r6.setAdvertiserId(r8)     // Catch:{ Exception -> 0x0184 }
            java.lang.String r8 = r9.trim()     // Catch:{ Exception -> 0x0184 }
            r6.setConversionKey(r8)     // Catch:{ Exception -> 0x0184 }
            java.lang.String r8 = "USD"
            r6.setCurrencyCode(r8)     // Catch:{ Exception -> 0x0184 }
            java.lang.Thread r8 = new java.lang.Thread     // Catch:{ Exception -> 0x0184 }
            com.mobileapptracker.d r9 = new com.mobileapptracker.d     // Catch:{ Exception -> 0x0184 }
            r9.<init>(r6, r7)     // Catch:{ Exception -> 0x0184 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x0184 }
            r8.start()     // Catch:{ Exception -> 0x0184 }
            android.os.Handler r8 = new android.os.Handler     // Catch:{ Exception -> 0x0184 }
            android.os.Looper r9 = android.os.Looper.getMainLooper()     // Catch:{ Exception -> 0x0184 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x0184 }
            com.mobileapptracker.e r9 = new com.mobileapptracker.e     // Catch:{ Exception -> 0x0184 }
            android.content.Context r1 = r6.f4765a     // Catch:{ Exception -> 0x0184 }
            r9.<init>(r6, r1)     // Catch:{ Exception -> 0x0184 }
            r8.post(r9)     // Catch:{ Exception -> 0x0184 }
            android.content.Context r8 = r6.f4765a     // Catch:{ Exception -> 0x0184 }
            java.lang.String r8 = r8.getPackageName()     // Catch:{ Exception -> 0x0184 }
            r6.setPackageName(r8)     // Catch:{ Exception -> 0x0184 }
            android.content.Context r9 = r6.f4765a     // Catch:{ Exception -> 0x0184 }
            android.content.pm.PackageManager r9 = r9.getPackageManager()     // Catch:{ Exception -> 0x0184 }
            android.content.pm.ApplicationInfo r1 = r9.getApplicationInfo(r8, r0)     // Catch:{ NameNotFoundException -> 0x0075 }
            java.lang.CharSequence r1 = r9.getApplicationLabel(r1)     // Catch:{ NameNotFoundException -> 0x0075 }
            java.lang.String r1 = r1.toString()     // Catch:{ NameNotFoundException -> 0x0075 }
            r6.setAppName(r1)     // Catch:{ NameNotFoundException -> 0x0075 }
            android.content.pm.ApplicationInfo r1 = r9.getApplicationInfo(r8, r0)     // Catch:{ NameNotFoundException -> 0x0075 }
            java.lang.String r1 = r1.sourceDir     // Catch:{ NameNotFoundException -> 0x0075 }
            java.io.File r2 = new java.io.File     // Catch:{ NameNotFoundException -> 0x0075 }
            r2.<init>(r1)     // Catch:{ NameNotFoundException -> 0x0075 }
            long r1 = r2.lastModified()     // Catch:{ NameNotFoundException -> 0x0075 }
            java.util.Date r3 = new java.util.Date     // Catch:{ NameNotFoundException -> 0x0075 }
            r3.<init>(r1)     // Catch:{ NameNotFoundException -> 0x0075 }
            long r1 = r3.getTime()     // Catch:{ NameNotFoundException -> 0x0075 }
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 / r3
            java.lang.String r1 = java.lang.Long.toString(r1)     // Catch:{ NameNotFoundException -> 0x0075 }
            r6.setInstallDate(r1)     // Catch:{ NameNotFoundException -> 0x0075 }
        L_0x0075:
            android.content.pm.PackageInfo r1 = r9.getPackageInfo(r8, r0)     // Catch:{ NameNotFoundException -> 0x0088 }
            int r2 = r1.versionCode     // Catch:{ NameNotFoundException -> 0x0088 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ NameNotFoundException -> 0x0088 }
            r6.setAppVersion(r2)     // Catch:{ NameNotFoundException -> 0x0088 }
            java.lang.String r1 = r1.versionName     // Catch:{ NameNotFoundException -> 0x0088 }
            r6.setAppVersionName(r1)     // Catch:{ NameNotFoundException -> 0x0088 }
            goto L_0x008d
        L_0x0088:
            java.lang.String r1 = "0"
            r6.setAppVersion(r1)     // Catch:{ Exception -> 0x0184 }
        L_0x008d:
            java.lang.String r8 = r9.getInstallerPackageName(r8)     // Catch:{ Exception -> 0x0184 }
            r6.setInstaller(r8)     // Catch:{ Exception -> 0x0184 }
            java.lang.String r8 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0184 }
            r6.setDeviceModel(r8)     // Catch:{ Exception -> 0x0184 }
            java.lang.String r8 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x0184 }
            r6.setDeviceBrand(r8)     // Catch:{ Exception -> 0x0184 }
            java.lang.String r8 = "os.arch"
            java.lang.String r8 = java.lang.System.getProperty(r8)     // Catch:{ Exception -> 0x0184 }
            r6.setDeviceCpuType(r8)     // Catch:{ Exception -> 0x0184 }
            java.lang.String r8 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x0184 }
            r6.setOsVersion(r8)     // Catch:{ Exception -> 0x0184 }
            android.content.res.Resources r8 = r7.getResources()     // Catch:{ Exception -> 0x0184 }
            android.util.DisplayMetrics r8 = r8.getDisplayMetrics()     // Catch:{ Exception -> 0x0184 }
            float r8 = r8.density     // Catch:{ Exception -> 0x0184 }
            java.lang.String r8 = java.lang.Float.toString(r8)     // Catch:{ Exception -> 0x0184 }
            r6.setScreenDensity(r8)     // Catch:{ Exception -> 0x0184 }
            java.lang.String r8 = "window"
            java.lang.Object r7 = r7.getSystemService(r8)     // Catch:{ Exception -> 0x0184 }
            android.view.WindowManager r7 = (android.view.WindowManager) r7     // Catch:{ Exception -> 0x0184 }
            int r8 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0184 }
            r9 = 13
            if (r8 < r9) goto L_0x00dc
            android.graphics.Point r8 = new android.graphics.Point     // Catch:{ Exception -> 0x0184 }
            r8.<init>()     // Catch:{ Exception -> 0x0184 }
            android.view.Display r7 = r7.getDefaultDisplay()     // Catch:{ Exception -> 0x0184 }
            r7.getSize(r8)     // Catch:{ Exception -> 0x0184 }
            int r7 = r8.x     // Catch:{ Exception -> 0x0184 }
            int r8 = r8.y     // Catch:{ Exception -> 0x0184 }
            goto L_0x00ef
        L_0x00dc:
            android.view.Display r8 = r7.getDefaultDisplay()     // Catch:{ Exception -> 0x0184 }
            int r8 = r8.getWidth()     // Catch:{ Exception -> 0x0184 }
            android.view.Display r7 = r7.getDefaultDisplay()     // Catch:{ Exception -> 0x0184 }
            int r7 = r7.getHeight()     // Catch:{ Exception -> 0x0184 }
            r5 = r8
            r8 = r7
            r7 = r5
        L_0x00ef:
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ Exception -> 0x0184 }
            r6.setScreenWidth(r7)     // Catch:{ Exception -> 0x0184 }
            java.lang.String r7 = java.lang.Integer.toString(r8)     // Catch:{ Exception -> 0x0184 }
            r6.setScreenHeight(r7)     // Catch:{ Exception -> 0x0184 }
            android.content.Context r7 = r6.f4765a     // Catch:{ Exception -> 0x0184 }
            java.lang.String r8 = "connectivity"
            java.lang.Object r7 = r7.getSystemService(r8)     // Catch:{ Exception -> 0x0184 }
            android.net.ConnectivityManager r7 = (android.net.ConnectivityManager) r7     // Catch:{ Exception -> 0x0184 }
            r8 = 1
            android.net.NetworkInfo r7 = r7.getNetworkInfo(r8)     // Catch:{ Exception -> 0x0184 }
            boolean r7 = r7.isConnected()     // Catch:{ Exception -> 0x0184 }
            if (r7 == 0) goto L_0x0118
            java.lang.String r7 = "wifi"
        L_0x0114:
            r6.setConnectionType(r7)     // Catch:{ Exception -> 0x0184 }
            goto L_0x011b
        L_0x0118:
            java.lang.String r7 = "mobile"
            goto L_0x0114
        L_0x011b:
            java.util.Locale r7 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0184 }
            java.util.Locale r9 = java.util.Locale.US     // Catch:{ Exception -> 0x0184 }
            java.lang.String r7 = r7.getDisplayLanguage(r9)     // Catch:{ Exception -> 0x0184 }
            r6.setLanguage(r7)     // Catch:{ Exception -> 0x0184 }
            android.content.Context r7 = r6.f4765a     // Catch:{ Exception -> 0x0184 }
            java.lang.String r9 = "phone"
            java.lang.Object r7 = r7.getSystemService(r9)     // Catch:{ Exception -> 0x0184 }
            android.telephony.TelephonyManager r7 = (android.telephony.TelephonyManager) r7     // Catch:{ Exception -> 0x0184 }
            if (r7 == 0) goto L_0x015e
            java.lang.String r9 = r7.getNetworkCountryIso()     // Catch:{ Exception -> 0x0184 }
            if (r9 == 0) goto L_0x0141
            java.lang.String r9 = r7.getNetworkCountryIso()     // Catch:{ Exception -> 0x0184 }
            r6.setCountryCode(r9)     // Catch:{ Exception -> 0x0184 }
        L_0x0141:
            java.lang.String r9 = r7.getNetworkOperatorName()     // Catch:{ Exception -> 0x0184 }
            r6.setDeviceCarrier(r9)     // Catch:{ Exception -> 0x0184 }
            java.lang.String r7 = r7.getNetworkOperator()     // Catch:{ Exception -> 0x0184 }
            if (r7 == 0) goto L_0x0169
            r9 = 3
            java.lang.String r1 = r7.substring(r0, r9)     // Catch:{ IndexOutOfBoundsException -> 0x0169 }
            java.lang.String r7 = r7.substring(r9)     // Catch:{ IndexOutOfBoundsException -> 0x0169 }
            r6.setMCC(r1)     // Catch:{ IndexOutOfBoundsException -> 0x0169 }
            r6.setMNC(r7)     // Catch:{ IndexOutOfBoundsException -> 0x0169 }
            goto L_0x0169
        L_0x015e:
            java.util.Locale r7 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0184 }
            java.lang.String r7 = r7.getCountry()     // Catch:{ Exception -> 0x0184 }
            r6.setCountryCode(r7)     // Catch:{ Exception -> 0x0184 }
        L_0x0169:
            java.lang.String r7 = r6.getMatId()     // Catch:{ Exception -> 0x0184 }
            if (r7 == 0) goto L_0x0175
            int r7 = r7.length()     // Catch:{ Exception -> 0x0184 }
            if (r7 != 0) goto L_0x0180
        L_0x0175:
            java.util.UUID r7 = java.util.UUID.randomUUID()     // Catch:{ Exception -> 0x0184 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0184 }
            r6.setMatId(r7)     // Catch:{ Exception -> 0x0184 }
        L_0x0180:
            monitor-exit(r6)
            return r8
        L_0x0182:
            r7 = move-exception
            goto L_0x018a
        L_0x0184:
            r7 = move-exception
            r7.printStackTrace()     // Catch:{ all -> 0x0182 }
            monitor-exit(r6)
            return r0
        L_0x018a:
            monitor-exit(r6)
            goto L_0x018d
        L_0x018c:
            throw r7
        L_0x018d:
            goto L_0x018c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobileapptracker.MATParameters.a(android.content.Context, java.lang.String, java.lang.String):boolean");
    }

    public static MATParameters getInstance() {
        return c;
    }

    public static MATParameters init(MobileAppTracker mobileAppTracker, Context context, String str, String str2) {
        if (c == null) {
            MATParameters mATParameters = new MATParameters();
            c = mATParameters;
            mATParameters.f4766b = mobileAppTracker;
            MATParameters mATParameters2 = c;
            mATParameters2.f4765a = context;
            mATParameters2.a(context, str, str2);
        }
        return c;
    }

    public void clear() {
        c = null;
    }

    public synchronized String getAction() {
        return this.d;
    }

    public synchronized String getAdvertiserId() {
        return this.e;
    }

    public synchronized String getAge() {
        return this.f;
    }

    public synchronized String getAllowDuplicates() {
        return this.g;
    }

    public synchronized String getAltitude() {
        return this.h;
    }

    public synchronized String getAndroidId() {
        return this.i;
    }

    public synchronized String getAndroidIdMd5() {
        return this.j;
    }

    public synchronized String getAndroidIdSha1() {
        return this.k;
    }

    public synchronized String getAndroidIdSha256() {
        return this.l;
    }

    public synchronized String getAppAdTrackingEnabled() {
        return this.m;
    }

    public synchronized String getAppName() {
        return this.n;
    }

    public synchronized String getAppVersion() {
        return this.o;
    }

    public synchronized String getAppVersionName() {
        return this.p;
    }

    public synchronized String getConnectionType() {
        return this.q;
    }

    public synchronized String getConversionKey() {
        return this.r;
    }

    public synchronized String getCountryCode() {
        return this.s;
    }

    public synchronized String getCurrencyCode() {
        return this.t;
    }

    public synchronized String getDeviceBrand() {
        return this.u;
    }

    public synchronized String getDeviceCarrier() {
        return this.v;
    }

    public synchronized String getDeviceCpuSubtype() {
        return this.x;
    }

    public synchronized String getDeviceCpuType() {
        return this.w;
    }

    public synchronized String getDeviceId() {
        return this.y;
    }

    public synchronized String getDeviceModel() {
        return this.z;
    }

    public synchronized String getExistingUser() {
        return this.A;
    }

    public synchronized String getFacebookUserId() {
        return this.B;
    }

    public synchronized String getGender() {
        return this.C;
    }

    public synchronized String getGoogleAdTrackingLimited() {
        return this.E;
    }

    public synchronized String getGoogleAdvertisingId() {
        return this.D;
    }

    public synchronized String getGoogleUserId() {
        return this.F;
    }

    public synchronized String getInstallDate() {
        return this.G;
    }

    public synchronized String getInstallReferrer() {
        return a("mat_referrer");
    }

    public synchronized String getInstaller() {
        return this.H;
    }

    public synchronized String getIsPayingUser() {
        return a("mat_is_paying_user");
    }

    public synchronized String getLanguage() {
        return this.I;
    }

    public synchronized String getLastOpenLogId() {
        return a("mat_log_id_last_open");
    }

    public synchronized String getLatitude() {
        return this.J;
    }

    public synchronized Location getLocation() {
        return this.K;
    }

    public synchronized String getLongitude() {
        return this.L;
    }

    public synchronized String getMCC() {
        return this.N;
    }

    public synchronized String getMNC() {
        return this.O;
    }

    public synchronized String getMacAddress() {
        return this.M;
    }

    public synchronized String getMatId() {
        if (this.f4765a.getSharedPreferences("mat_id", 0).contains("mat_id")) {
            return this.f4765a.getSharedPreferences("mat_id", 0).getString("mat_id", "");
        }
        return a("mat_id");
    }

    public synchronized String getOpenLogId() {
        return a("mat_log_id_open");
    }

    public synchronized String getOsVersion() {
        return this.P;
    }

    public synchronized String getPackageName() {
        return this.Q;
    }

    public synchronized String getPhoneNumber() {
        return a("mat_phone_number");
    }

    public synchronized String getPhoneNumberMd5() {
        return this.R;
    }

    public synchronized String getPhoneNumberSha1() {
        return this.S;
    }

    public synchronized String getPhoneNumberSha256() {
        return this.T;
    }

    public synchronized String getPluginName() {
        return this.U;
    }

    public synchronized String getPurchaseStatus() {
        return this.V;
    }

    public synchronized String getRefId() {
        return this.Z;
    }

    public synchronized String getReferralSource() {
        return this.W;
    }

    public synchronized String getReferralUrl() {
        return this.X;
    }

    public synchronized String getReferrerDelay() {
        return this.Y;
    }

    public synchronized String getRevenue() {
        return this.aa;
    }

    public synchronized String getScreenDensity() {
        return this.ab;
    }

    public synchronized String getScreenHeight() {
        return this.ac;
    }

    public synchronized String getScreenWidth() {
        return this.ad;
    }

    public synchronized String getSdkVersion() {
        return "3.9.1";
    }

    public synchronized String getSiteId() {
        return this.ae;
    }

    public synchronized String getTRUSTeId() {
        return this.ag;
    }

    public synchronized String getTrackingId() {
        return this.af;
    }

    public synchronized String getTwitterUserId() {
        return this.ah;
    }

    public synchronized String getUserAgent() {
        return this.ai;
    }

    public synchronized String getUserEmail() {
        return a("mat_user_email");
    }

    public synchronized String getUserEmailMd5() {
        return this.aj;
    }

    public synchronized String getUserEmailSha1() {
        return this.ak;
    }

    public synchronized String getUserEmailSha256() {
        return this.al;
    }

    public synchronized JSONArray getUserEmails() {
        return this.am;
    }

    public synchronized String getUserId() {
        return a("mat_user_id");
    }

    public synchronized String getUserName() {
        return a("mat_user_name");
    }

    public synchronized String getUserNameMd5() {
        return this.an;
    }

    public synchronized String getUserNameSha1() {
        return this.ao;
    }

    public synchronized String getUserNameSha256() {
        return this.ap;
    }

    public synchronized void setAction(String str) {
        this.d = str;
    }

    public synchronized void setAdvertiserId(String str) {
        this.e = str;
    }

    public synchronized void setAge(String str) {
        this.f = str;
    }

    public synchronized void setAllowDuplicates(String str) {
        this.g = str;
    }

    public synchronized void setAltitude(String str) {
        this.h = str;
    }

    public synchronized void setAndroidId(String str) {
        this.i = str;
    }

    public synchronized void setAndroidIdMd5(String str) {
        this.j = str;
    }

    public synchronized void setAndroidIdSha1(String str) {
        this.k = str;
    }

    public synchronized void setAndroidIdSha256(String str) {
        this.l = str;
    }

    public synchronized void setAppAdTrackingEnabled(String str) {
        this.m = str;
    }

    public synchronized void setAppName(String str) {
        this.n = str;
    }

    public synchronized void setAppVersion(String str) {
        this.o = str;
    }

    public synchronized void setAppVersionName(String str) {
        this.p = str;
    }

    public synchronized void setConnectionType(String str) {
        this.q = str;
    }

    public synchronized void setConversionKey(String str) {
        this.r = str;
    }

    public synchronized void setCountryCode(String str) {
        this.s = str;
    }

    public synchronized void setCurrencyCode(String str) {
        this.t = str;
    }

    public synchronized void setDeviceBrand(String str) {
        this.u = str;
    }

    public synchronized void setDeviceCarrier(String str) {
        this.v = str;
    }

    public synchronized void setDeviceCpuSubtype(String str) {
        this.x = str;
    }

    public synchronized void setDeviceCpuType(String str) {
        this.w = str;
    }

    public synchronized void setDeviceId(String str) {
        this.y = str;
    }

    public synchronized void setDeviceModel(String str) {
        this.z = str;
    }

    public synchronized void setExistingUser(String str) {
        this.A = str;
    }

    public synchronized void setFacebookUserId(String str) {
        this.B = str;
    }

    public synchronized void setGender(String str) {
        this.C = str;
    }

    public synchronized void setGoogleAdTrackingLimited(String str) {
        this.E = str;
    }

    public synchronized void setGoogleAdvertisingId(String str) {
        this.D = str;
    }

    public synchronized void setGoogleUserId(String str) {
        this.F = str;
    }

    public synchronized void setInstallDate(String str) {
        this.G = str;
    }

    public synchronized void setInstallReferrer(String str) {
        a("mat_referrer", str);
    }

    public synchronized void setInstaller(String str) {
        this.H = str;
    }

    public synchronized void setIsPayingUser(String str) {
        a("mat_is_paying_user", str);
    }

    public synchronized void setLanguage(String str) {
        this.I = str;
    }

    public synchronized void setLastOpenLogId(String str) {
        a("mat_log_id_last_open", str);
    }

    public synchronized void setLatitude(String str) {
        this.J = str;
    }

    public synchronized void setLocation(Location location) {
        this.K = location;
    }

    public synchronized void setLongitude(String str) {
        this.L = str;
    }

    public synchronized void setMCC(String str) {
        this.N = str;
    }

    public synchronized void setMNC(String str) {
        this.O = str;
    }

    public synchronized void setMacAddress(String str) {
        this.M = str;
    }

    public synchronized void setMatId(String str) {
        a("mat_id", str);
    }

    public synchronized void setOpenLogId(String str) {
        a("mat_log_id_open", str);
    }

    public synchronized void setOsVersion(String str) {
        this.P = str;
    }

    public synchronized void setPackageName(String str) {
        this.Q = str;
    }

    public synchronized void setPhoneNumber(String str) {
        a("mat_phone_number", str);
        setPhoneNumberMd5(MATEncryption.md5(str));
        setPhoneNumberSha1(MATEncryption.sha1(str));
        setPhoneNumberSha256(MATEncryption.sha256(str));
    }

    public synchronized void setPhoneNumberMd5(String str) {
        this.R = str;
    }

    public synchronized void setPhoneNumberSha1(String str) {
        this.S = str;
    }

    public synchronized void setPhoneNumberSha256(String str) {
        this.T = str;
    }

    public synchronized void setPluginName(String str) {
        this.U = null;
    }

    public synchronized void setPurchaseStatus(String str) {
        this.V = str;
    }

    public synchronized void setRefId(String str) {
        this.Z = str;
    }

    public synchronized void setReferralSource(String str) {
        this.W = str;
    }

    public synchronized void setReferralUrl(String str) {
        this.X = str;
    }

    public synchronized void setReferrerDelay(long j2) {
        this.Y = Long.toString(j2);
    }

    public synchronized void setRevenue(String str) {
        this.aa = str;
    }

    public synchronized void setScreenDensity(String str) {
        this.ab = str;
    }

    public synchronized void setScreenHeight(String str) {
        this.ac = str;
    }

    public synchronized void setScreenWidth(String str) {
        this.ad = str;
    }

    public synchronized void setSiteId(String str) {
        this.ae = str;
    }

    public synchronized void setTRUSTeId(String str) {
        this.ag = str;
    }

    public synchronized void setTrackingId(String str) {
        this.af = str;
    }

    public synchronized void setTwitterUserId(String str) {
        this.ah = str;
    }

    public synchronized void setUserEmail(String str) {
        a("mat_user_email", str);
        setUserEmailMd5(MATEncryption.md5(str));
        setUserEmailSha1(MATEncryption.sha1(str));
        setUserEmailSha256(MATEncryption.sha256(str));
    }

    public synchronized void setUserEmailMd5(String str) {
        this.aj = str;
    }

    public synchronized void setUserEmailSha1(String str) {
        this.ak = str;
    }

    public synchronized void setUserEmailSha256(String str) {
        this.al = str;
    }

    public synchronized void setUserEmails(String[] strArr) {
        this.am = new JSONArray();
        for (String put : strArr) {
            this.am.put(put);
        }
    }

    public synchronized void setUserId(String str) {
        a("mat_user_id", str);
    }

    public synchronized void setUserName(String str) {
        a("mat_user_name", str);
        setUserNameMd5(MATEncryption.md5(str));
        setUserNameSha1(MATEncryption.sha1(str));
        setUserNameSha256(MATEncryption.sha256(str));
    }

    public synchronized void setUserNameMd5(String str) {
        this.an = str;
    }

    public synchronized void setUserNameSha1(String str) {
        this.ao = str;
    }

    public synchronized void setUserNameSha256(String str) {
        this.ap = str;
    }
}
