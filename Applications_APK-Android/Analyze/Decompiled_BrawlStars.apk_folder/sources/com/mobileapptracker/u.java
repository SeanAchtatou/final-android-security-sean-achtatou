package com.mobileapptracker;

import android.location.Location;

final class u implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Location f4840a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4841b;

    u(MobileAppTracker mobileAppTracker, Location location) {
        this.f4841b = mobileAppTracker;
        this.f4840a = location;
    }

    public final void run() {
        this.f4841b.params.setLocation(this.f4840a);
    }
}
