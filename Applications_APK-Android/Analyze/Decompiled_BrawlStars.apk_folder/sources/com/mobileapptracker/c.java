package com.mobileapptracker;

import android.content.Context;
import android.os.Bundle;

final class c {

    /* renamed from: a  reason: collision with root package name */
    private static Object f4808a = null;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f4809b = false;

    public static void a(Context context, boolean z) {
        try {
            Class[] clsArr = {Context.class};
            Class[] clsArr2 = {Context.class, Boolean.TYPE};
            Object[] objArr = {context};
            Class.forName("com.facebook.AppEventsLogger").getMethod("activateApp", clsArr).invoke(null, objArr);
            f4809b = true;
            Class.forName("com.facebook.Settings").getMethod("setLimitEventAndDataUsage", clsArr2).invoke(null, context, Boolean.valueOf(z));
            f4808a = Class.forName("com.facebook.AppEventsLogger").getMethod("newLogger", clsArr).invoke(null, objArr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void a(Bundle bundle, String str, String str2) {
        if (str2 != null) {
            bundle.putString(str, str2);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:2|3|4|(2:6|(1:8)(1:9))(2:10|(1:12)(2:13|(1:15)(2:16|(1:18)(2:19|(3:21|22|23)(4:24|25|26|(1:28)(2:29|(1:31)(2:32|(1:34)(2:35|(1:37)(2:38|(1:40)(2:41|(1:43)(2:44|(1:46)(2:47|(1:49)(2:50|(4:52|53|54|55))))))))))))))|56|57|58) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:56:0x00e4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(com.mobileapptracker.MATEvent r12) {
        /*
            java.lang.Object r0 = com.mobileapptracker.c.f4808a
            if (r0 == 0) goto L_0x014f
            r0 = 3
            java.lang.Class[] r1 = new java.lang.Class[r0]     // Catch:{ Exception -> 0x014b }
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            r3 = 0
            r1[r3] = r2     // Catch:{ Exception -> 0x014b }
            java.lang.Class r2 = java.lang.Double.TYPE     // Catch:{ Exception -> 0x014b }
            r4 = 1
            r1[r4] = r2     // Catch:{ Exception -> 0x014b }
            java.lang.Class<android.os.Bundle> r2 = android.os.Bundle.class
            r5 = 2
            r1[r5] = r2     // Catch:{ Exception -> 0x014b }
            java.lang.Object r2 = com.mobileapptracker.c.f4808a     // Catch:{ Exception -> 0x014b }
            java.lang.Class r2 = r2.getClass()     // Catch:{ Exception -> 0x014b }
            java.lang.String r6 = "logEvent"
            java.lang.reflect.Method r1 = r2.getMethod(r6, r1)     // Catch:{ Exception -> 0x014b }
            java.lang.String r2 = r12.getEventName()     // Catch:{ Exception -> 0x014b }
            double r6 = r12.getRevenue()     // Catch:{ Exception -> 0x014b }
            com.mobileapptracker.MATParameters r8 = com.mobileapptracker.MATParameters.getInstance()     // Catch:{ Exception -> 0x014b }
            java.lang.String r9 = r12.getEventName()     // Catch:{ Exception -> 0x014b }
            java.util.Locale r10 = java.util.Locale.US     // Catch:{ Exception -> 0x014b }
            java.lang.String r9 = r9.toLowerCase(r10)     // Catch:{ Exception -> 0x014b }
            java.lang.String r10 = "session"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x0049
            boolean r2 = com.mobileapptracker.c.f4809b     // Catch:{ Exception -> 0x014b }
            if (r2 == 0) goto L_0x0045
            return
        L_0x0045:
            java.lang.String r2 = "fb_mobile_activate_app"
            goto L_0x00e4
        L_0x0049:
            java.lang.String r10 = "registration"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x0055
            java.lang.String r2 = "fb_mobile_complete_registration"
            goto L_0x00e4
        L_0x0055:
            java.lang.String r10 = "content_view"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x0061
            java.lang.String r2 = "fb_mobile_content_view"
            goto L_0x00e4
        L_0x0061:
            java.lang.String r10 = "search"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x006d
            java.lang.String r2 = "fb_mobile_search"
            goto L_0x00e4
        L_0x006d:
            java.lang.String r10 = "rated"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x007d
            java.lang.String r2 = "fb_mobile_rate"
            double r6 = r12.getRating()     // Catch:{ Exception -> 0x00e4 }
            goto L_0x00e4
        L_0x007d:
            java.lang.String r10 = "tutorial_complete"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x0088
            java.lang.String r2 = "fb_mobile_tutorial_completion"
            goto L_0x00e4
        L_0x0088:
            java.lang.String r10 = "add_to_cart"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x0093
            java.lang.String r2 = "fb_mobile_add_to_cart"
            goto L_0x00e4
        L_0x0093:
            java.lang.String r10 = "add_to_wishlist"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x009e
            java.lang.String r2 = "fb_mobile_add_to_wishlist"
            goto L_0x00e4
        L_0x009e:
            java.lang.String r10 = "checkout_initiated"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x00a9
            java.lang.String r2 = "fb_mobile_initiated_checkout"
            goto L_0x00e4
        L_0x00a9:
            java.lang.String r10 = "added_payment_info"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x00b4
            java.lang.String r2 = "fb_mobile_add_payment_info"
            goto L_0x00e4
        L_0x00b4:
            java.lang.String r10 = "purchase"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x00bf
            java.lang.String r2 = "fb_mobile_purchase"
            goto L_0x00e4
        L_0x00bf:
            java.lang.String r10 = "level_achieved"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x00ca
            java.lang.String r2 = "fb_mobile_level_achieved"
            goto L_0x00e4
        L_0x00ca:
            java.lang.String r10 = "achievement_unlocked"
            boolean r10 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r10 == 0) goto L_0x00d5
            java.lang.String r2 = "fb_mobile_achievement_unlocked"
            goto L_0x00e4
        L_0x00d5:
            java.lang.String r10 = "spent_credits"
            boolean r9 = r9.contains(r10)     // Catch:{ Exception -> 0x014b }
            if (r9 == 0) goto L_0x00e4
            java.lang.String r2 = "fb_mobile_spent_credits"
            int r6 = r12.getQuantity()     // Catch:{ Exception -> 0x00e4 }
            double r6 = (double) r6
        L_0x00e4:
            android.os.Bundle r9 = new android.os.Bundle     // Catch:{ Exception -> 0x014b }
            r9.<init>()     // Catch:{ Exception -> 0x014b }
            java.lang.String r10 = "fb_currency"
            java.lang.String r11 = r12.getCurrencyCode()     // Catch:{ Exception -> 0x014b }
            a(r9, r10, r11)     // Catch:{ Exception -> 0x014b }
            java.lang.String r10 = "fb_content_id"
            java.lang.String r11 = r12.getContentId()     // Catch:{ Exception -> 0x014b }
            a(r9, r10, r11)     // Catch:{ Exception -> 0x014b }
            java.lang.String r10 = "fb_content_type"
            java.lang.String r11 = r12.getContentType()     // Catch:{ Exception -> 0x014b }
            a(r9, r10, r11)     // Catch:{ Exception -> 0x014b }
            java.lang.String r10 = "fb_search_string"
            java.lang.String r11 = r12.getSearchString()     // Catch:{ Exception -> 0x014b }
            a(r9, r10, r11)     // Catch:{ Exception -> 0x014b }
            java.lang.String r10 = "fb_num_items"
            int r11 = r12.getQuantity()     // Catch:{ Exception -> 0x014b }
            java.lang.String r11 = java.lang.Integer.toString(r11)     // Catch:{ Exception -> 0x014b }
            a(r9, r10, r11)     // Catch:{ Exception -> 0x014b }
            java.lang.String r10 = "fb_level"
            int r12 = r12.getLevel()     // Catch:{ Exception -> 0x014b }
            java.lang.String r12 = java.lang.Integer.toString(r12)     // Catch:{ Exception -> 0x014b }
            a(r9, r10, r12)     // Catch:{ Exception -> 0x014b }
            java.lang.String r12 = "tune_referral_source"
            java.lang.String r8 = r8.getReferralSource()     // Catch:{ Exception -> 0x014b }
            a(r9, r12, r8)     // Catch:{ Exception -> 0x014b }
            java.lang.String r12 = "tune_source_sdk"
            java.lang.String r8 = "TUNE-MAT"
            a(r9, r12, r8)     // Catch:{ Exception -> 0x014b }
            java.lang.Object[] r12 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x014b }
            r12[r3] = r2     // Catch:{ Exception -> 0x014b }
            java.lang.Double r0 = java.lang.Double.valueOf(r6)     // Catch:{ Exception -> 0x014b }
            r12[r4] = r0     // Catch:{ Exception -> 0x014b }
            r12[r5] = r9     // Catch:{ Exception -> 0x014b }
            java.lang.Object r0 = com.mobileapptracker.c.f4808a     // Catch:{ Exception -> 0x014b }
            r1.invoke(r0, r12)     // Catch:{ Exception -> 0x014b }
            com.mobileapptracker.c.f4809b = r3     // Catch:{ Exception -> 0x014b }
            return
        L_0x014b:
            r12 = move-exception
            r12.printStackTrace()
        L_0x014f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobileapptracker.c.a(com.mobileapptracker.MATEvent):void");
    }
}
