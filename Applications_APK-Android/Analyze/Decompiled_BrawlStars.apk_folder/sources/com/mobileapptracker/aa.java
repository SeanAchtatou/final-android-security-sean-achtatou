package com.mobileapptracker;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

final class aa implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f4770a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4771b;

    aa(MobileAppTracker mobileAppTracker, Activity activity) {
        this.f4771b = mobileAppTracker;
        this.f4770a = activity;
    }

    public final void run() {
        Uri data;
        this.f4771b.params.setReferralSource(this.f4770a.getCallingPackage());
        Intent intent = this.f4770a.getIntent();
        if (intent != null && (data = intent.getData()) != null) {
            this.f4771b.params.setReferralUrl(data.toString());
        }
    }
}
