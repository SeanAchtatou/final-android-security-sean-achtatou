package com.mobileapptracker;

import android.content.Context;
import android.webkit.WebView;
import java.lang.ref.WeakReference;

final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MATParameters f4812a;

    /* renamed from: b  reason: collision with root package name */
    private final WeakReference<Context> f4813b;

    public e(MATParameters mATParameters, Context context) {
        this.f4812a = mATParameters;
        this.f4813b = new WeakReference<>(context);
    }

    public final void run() {
        try {
            WebView webView = new WebView(this.f4813b.get());
            String userAgentString = webView.getSettings().getUserAgentString();
            webView.destroy();
            this.f4812a.ai = userAgentString;
        } catch (Exception | VerifyError unused) {
        }
    }
}
