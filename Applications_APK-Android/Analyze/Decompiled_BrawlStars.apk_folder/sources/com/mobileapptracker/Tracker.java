package com.mobileapptracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.net.URLDecoder;

public class Tracker extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String stringExtra;
        if (intent != null) {
            try {
                if (intent.getAction().equals("com.android.vending.INSTALL_REFERRER") && (stringExtra = intent.getStringExtra("referrer")) != null) {
                    String decode = URLDecoder.decode(stringExtra, "UTF-8");
                    "MAT received referrer " + decode;
                    context.getSharedPreferences("com.mobileapptracking", 0).edit().putString("mat_referrer", decode).commit();
                    MobileAppTracker instance = MobileAppTracker.getInstance();
                    if (instance != null) {
                        instance.setInstallReferrer(decode);
                        if (instance.f4767a && !instance.c) {
                            synchronized (instance.d) {
                                instance.d.notifyAll();
                                instance.c = true;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
