package com.mobileapptracker;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

final class b {
    private static volatile b i;

    /* renamed from: a  reason: collision with root package name */
    private String f4806a = null;

    /* renamed from: b  reason: collision with root package name */
    private String f4807b = null;
    private String c = null;
    private String d = null;
    private int e = 0;
    private String f = null;
    private String g = null;
    private MATResponse h = null;

    private b() {
    }

    public static synchronized b a(String str, String str2, String str3) {
        b bVar;
        synchronized (b.class) {
            b bVar2 = new b();
            i = bVar2;
            bVar2.f4806a = str;
            i.f4807b = str2;
            i.c = str3;
            bVar = i;
        }
        return bVar;
    }

    public static String a() {
        return i.f4806a;
    }

    public static void a(MATResponse mATResponse) {
        i.h = mATResponse;
    }

    public static void a(String str) {
        i.c = str;
    }

    public static void a(String str, int i2) {
        i.d = str;
        i.e = i2;
    }

    public static String b() {
        return i.f4807b;
    }

    public static void b(String str) {
        i.g = str;
    }

    public static String c() {
        return i.c;
    }

    public static void c(String str) {
        i.f = str;
    }

    public static String d() {
        return i.g;
    }

    public static String e() {
        return i.d;
    }

    public static int f() {
        return i.e;
    }

    public static String g() {
        return i.f;
    }

    public final String a(Context context, int i2) {
        String str = "";
        if (!(i.f4806a == null || i.f4807b == null || i.c == null)) {
            if (i.d == null && i.f == null) {
                return str;
            }
            try {
                str = g.a(i2);
                if (str.length() != 0) {
                    if (this.h != null) {
                        this.h.didReceiveDeeplink(str);
                    }
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse(str));
                    intent.setFlags(268435456);
                    context.startActivity(intent);
                }
            } catch (Exception unused) {
            }
        }
        return str;
    }
}
