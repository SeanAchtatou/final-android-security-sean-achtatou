package com.mobileapptracker;

import android.widget.Toast;

final class ak implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MobileAppTracker f4790a;

    ak(MobileAppTracker mobileAppTracker) {
        this.f4790a = mobileAppTracker;
    }

    public final void run() {
        Toast.makeText(this.f4790a.mContext, "MAT Allow Duplicate Requests Enabled, do not release with this enabled!!", 1).show();
    }
}
