package com.facebook.marketing;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.facebook.marketing.internal.ButtonIndexer;

public class CodelessActivityLifecycleTracker {
    private static final String TAG = CodelessActivityLifecycleTracker.class.getCanonicalName();
    /* access modifiers changed from: private */
    public static final ButtonIndexer buttonIndexer = new ButtonIndexer();
    private static Boolean isAppIndexingEnabled = false;

    public static void startTracking(Application application) {
        application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            public final void onActivityCreated(Activity activity, Bundle bundle) {
            }

            public final void onActivityDestroyed(Activity activity) {
            }

            public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            }

            public final void onActivityStarted(Activity activity) {
            }

            public final void onActivityStopped(Activity activity) {
            }

            public final void onActivityResumed(Activity activity) {
                CodelessActivityLifecycleTracker.buttonIndexer.add(activity);
            }

            public final void onActivityPaused(Activity activity) {
                CodelessActivityLifecycleTracker.buttonIndexer.remove(activity);
            }
        });
    }

    public static boolean getIsAppIndexingEnabled() {
        return isAppIndexingEnabled.booleanValue();
    }

    public static void updateAppIndexing(Boolean bool) {
        isAppIndexingEnabled = bool;
    }
}
