package com.google.android.gms.analytics;

import android.app.Service;
import android.app.job.JobParameters;
import android.content.Intent;
import android.os.IBinder;
import com.google.android.gms.internal.measurement.br;
import com.google.android.gms.internal.measurement.bv;

public final class AnalyticsService extends Service implements bv {

    /* renamed from: a  reason: collision with root package name */
    private br<AnalyticsService> f1310a;

    private final br<AnalyticsService> a() {
        if (this.f1310a == null) {
            this.f1310a = new br<>(this);
        }
        return this.f1310a;
    }

    public final void onCreate() {
        super.onCreate();
        a().a();
    }

    public final void onDestroy() {
        a().b();
        super.onDestroy();
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        return a().a(intent, i2);
    }

    public final IBinder onBind(Intent intent) {
        a();
        return null;
    }

    public final boolean a(int i) {
        return stopSelfResult(i);
    }

    public final void a(JobParameters jobParameters) {
        throw new UnsupportedOperationException();
    }
}
