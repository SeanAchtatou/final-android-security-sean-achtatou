package com.google.firebase.messaging;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.share.internal.ShareConstants;
import com.google.firebase.analytics.connector.a;
import com.google.firebase.b;
import com.google.firebase.iid.w;
import com.google.firebase.iid.zzb;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Queue;

public class FirebaseMessagingService extends zzb {

    /* renamed from: b  reason: collision with root package name */
    private static final Queue<String> f2836b = new ArrayDeque(10);

    public void a(RemoteMessage remoteMessage) {
    }

    public void a(String str) {
    }

    public final Intent a(Intent intent) {
        return w.a().d.poll();
    }

    public final boolean c(Intent intent) {
        a aVar;
        if (!"com.google.firebase.messaging.NOTIFICATION_OPEN".equals(intent.getAction())) {
            return false;
        }
        PendingIntent pendingIntent = (PendingIntent) intent.getParcelableExtra("pending_intent");
        if (pendingIntent != null) {
            try {
                pendingIntent.send();
            } catch (PendingIntent.CanceledException unused) {
            }
        }
        if (!b.a(intent)) {
            return true;
        }
        if (!(intent == null || !AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(intent.getStringExtra("google.c.a.tc")) || (aVar = (a) b.c().a(a.class)) == null)) {
            String stringExtra = intent.getStringExtra("google.c.a.c_id");
            aVar.a("fcm", "_ln", stringExtra);
            Bundle bundle = new Bundle();
            bundle.putString(ShareConstants.FEED_SOURCE_PARAM, "Firebase");
            bundle.putString("medium", "notification");
            bundle.putString("campaign", stringExtra);
            aVar.a("fcm", "_cmp", bundle);
        }
        b.a("_no", intent);
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00f6, code lost:
        if (r1.equals("gcm") != false) goto L_0x0104;
     */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(android.content.Intent r11) {
        /*
            r10 = this;
            java.lang.String r0 = r11.getAction()
            java.lang.String r1 = "com.google.android.c2dm.intent.RECEIVE"
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x0055
            java.lang.String r1 = "com.google.firebase.messaging.RECEIVE_DIRECT_BOOT"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x0015
            goto L_0x0055
        L_0x0015:
            java.lang.String r1 = "com.google.firebase.messaging.NOTIFICATION_DISMISS"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x0029
            boolean r0 = com.google.firebase.messaging.b.a(r11)
            if (r0 == 0) goto L_0x0054
            java.lang.String r0 = "_nd"
            com.google.firebase.messaging.b.a(r0, r11)
            return
        L_0x0029:
            java.lang.String r1 = "com.google.firebase.messaging.NEW_TOKEN"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x003b
            java.lang.String r0 = "token"
            java.lang.String r11 = r11.getStringExtra(r0)
            r10.a(r11)
            return
        L_0x003b:
            java.lang.String r0 = "Unknown intent action: "
            java.lang.String r11 = r11.getAction()
            java.lang.String r11 = java.lang.String.valueOf(r11)
            int r1 = r11.length()
            if (r1 == 0) goto L_0x004f
            r0.concat(r11)
            goto L_0x0054
        L_0x004f:
            java.lang.String r11 = new java.lang.String
            r11.<init>(r0)
        L_0x0054:
            return
        L_0x0055:
            java.lang.String r0 = "google.message_id"
            java.lang.String r1 = r11.getStringExtra(r0)
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 == 0) goto L_0x0067
            r2 = 0
            com.google.android.gms.tasks.g r2 = com.google.android.gms.tasks.j.a(r2)
            goto L_0x0080
        L_0x0067:
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            r2.putString(r0, r1)
            com.google.firebase.iid.e r3 = com.google.firebase.iid.e.a(r10)
            com.google.firebase.iid.l r4 = new com.google.firebase.iid.l
            int r5 = r3.a()
            r4.<init>(r5, r2)
            com.google.android.gms.tasks.g r2 = r3.a(r4)
        L_0x0080:
            boolean r3 = android.text.TextUtils.isEmpty(r1)
            r4 = 1
            r5 = 3
            r6 = 0
            if (r3 == 0) goto L_0x008b
        L_0x0089:
            r1 = 0
            goto L_0x00c7
        L_0x008b:
            java.util.Queue<java.lang.String> r3 = com.google.firebase.messaging.FirebaseMessagingService.f2836b
            boolean r3 = r3.contains(r1)
            if (r3 == 0) goto L_0x00b2
            java.lang.String r3 = "FirebaseMessaging"
            boolean r3 = android.util.Log.isLoggable(r3, r5)
            if (r3 == 0) goto L_0x00b0
            java.lang.String r3 = "Received duplicate message: "
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r7 = r1.length()
            if (r7 == 0) goto L_0x00ab
            r3.concat(r1)
            goto L_0x00b0
        L_0x00ab:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r3)
        L_0x00b0:
            r1 = 1
            goto L_0x00c7
        L_0x00b2:
            java.util.Queue<java.lang.String> r3 = com.google.firebase.messaging.FirebaseMessagingService.f2836b
            int r3 = r3.size()
            r7 = 10
            if (r3 < r7) goto L_0x00c1
            java.util.Queue<java.lang.String> r3 = com.google.firebase.messaging.FirebaseMessagingService.f2836b
            r3.remove()
        L_0x00c1:
            java.util.Queue<java.lang.String> r3 = com.google.firebase.messaging.FirebaseMessagingService.f2836b
            r3.add(r1)
            goto L_0x0089
        L_0x00c7:
            if (r1 != 0) goto L_0x017c
            java.lang.String r1 = "message_type"
            java.lang.String r1 = r11.getStringExtra(r1)
            java.lang.String r3 = "gcm"
            if (r1 != 0) goto L_0x00d4
            r1 = r3
        L_0x00d4:
            r7 = -1
            int r8 = r1.hashCode()
            r9 = 2
            switch(r8) {
                case -2062414158: goto L_0x00f9;
                case 102161: goto L_0x00f2;
                case 814694033: goto L_0x00e8;
                case 814800675: goto L_0x00de;
                default: goto L_0x00dd;
            }
        L_0x00dd:
            goto L_0x0103
        L_0x00de:
            java.lang.String r3 = "send_event"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0103
            r6 = 2
            goto L_0x0104
        L_0x00e8:
            java.lang.String r3 = "send_error"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0103
            r6 = 3
            goto L_0x0104
        L_0x00f2:
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0103
            goto L_0x0104
        L_0x00f9:
            java.lang.String r3 = "deleted_messages"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0103
            r6 = 1
            goto L_0x0104
        L_0x0103:
            r6 = -1
        L_0x0104:
            if (r6 == 0) goto L_0x013d
            if (r6 == r4) goto L_0x017c
            if (r6 == r9) goto L_0x0139
            if (r6 == r5) goto L_0x0122
            java.lang.String r11 = "Received message with unknown type: "
            java.lang.String r0 = java.lang.String.valueOf(r1)
            int r1 = r0.length()
            if (r1 == 0) goto L_0x011c
            r11.concat(r0)
            goto L_0x017c
        L_0x011c:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r11)
            goto L_0x017c
        L_0x0122:
            java.lang.String r0 = r11.getStringExtra(r0)
            if (r0 != 0) goto L_0x012d
            java.lang.String r0 = "message_id"
            r11.getStringExtra(r0)
        L_0x012d:
            com.google.firebase.messaging.SendException r0 = new com.google.firebase.messaging.SendException
            java.lang.String r1 = "error"
            java.lang.String r11 = r11.getStringExtra(r1)
            r0.<init>(r11)
            goto L_0x017c
        L_0x0139:
            r11.getStringExtra(r0)
            goto L_0x017c
        L_0x013d:
            boolean r0 = com.google.firebase.messaging.b.a(r11)
            if (r0 == 0) goto L_0x0148
            java.lang.String r0 = "_nr"
            com.google.firebase.messaging.b.a(r0, r11)
        L_0x0148:
            android.os.Bundle r0 = r11.getExtras()
            if (r0 != 0) goto L_0x0153
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
        L_0x0153:
            java.lang.String r1 = "android.support.content.wakelockid"
            r0.remove(r1)
            boolean r1 = com.google.firebase.messaging.c.a(r0)
            if (r1 == 0) goto L_0x0174
            com.google.firebase.messaging.c r1 = new com.google.firebase.messaging.c
            r1.<init>(r10)
            boolean r1 = r1.c(r0)
            if (r1 != 0) goto L_0x017c
            boolean r1 = com.google.firebase.messaging.b.a(r11)
            if (r1 == 0) goto L_0x0174
            java.lang.String r1 = "_nf"
            com.google.firebase.messaging.b.a(r1, r11)
        L_0x0174:
            com.google.firebase.messaging.RemoteMessage r11 = new com.google.firebase.messaging.RemoteMessage
            r11.<init>(r0)
            r10.a(r11)
        L_0x017c:
            r0 = 1
            java.util.concurrent.TimeUnit r11 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ ExecutionException -> 0x0188, InterruptedException -> 0x0186, TimeoutException -> 0x0184 }
            com.google.android.gms.tasks.j.a(r2, r0, r11)     // Catch:{ ExecutionException -> 0x0188, InterruptedException -> 0x0186, TimeoutException -> 0x0184 }
            return
        L_0x0184:
            r11 = move-exception
            goto L_0x0189
        L_0x0186:
            r11 = move-exception
            goto L_0x0189
        L_0x0188:
            r11 = move-exception
        L_0x0189:
            java.lang.String r11 = java.lang.String.valueOf(r11)
            java.lang.String r0 = java.lang.String.valueOf(r11)
            int r0 = r0.length()
            int r0 = r0 + 20
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "Message ack failed: "
            r1.append(r0)
            r1.append(r11)
            r1.toString()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.messaging.FirebaseMessagingService.b(android.content.Intent):void");
    }

    static void a(Bundle bundle) {
        Iterator<String> it = bundle.keySet().iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (next != null && next.startsWith("google.c.")) {
                it.remove();
            }
        }
    }
}
