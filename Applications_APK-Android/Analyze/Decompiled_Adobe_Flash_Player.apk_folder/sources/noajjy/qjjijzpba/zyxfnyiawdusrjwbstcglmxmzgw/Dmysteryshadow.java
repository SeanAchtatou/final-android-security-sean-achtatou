package noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw;

import android.util.Log;
import com.google.appinventor.components.annotations.DesignerComponent;
import com.google.appinventor.components.annotations.PropertyCategory;
import com.google.appinventor.components.annotations.SimpleFunction;
import com.google.appinventor.components.annotations.SimpleObject;
import com.google.appinventor.components.annotations.SimpleProperty;
import com.google.appinventor.components.annotations.UsesPermissions;
import com.google.appinventor.components.common.ComponentCategory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.util.BluetoothReflection;
import noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.util.SdkLevel;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
@SimpleObject
@DesignerComponent(category = ComponentCategory.CONNECTIVITY, description = "Bluetooth client component", iconName = "images/bluetooth.png", nonVisible = true, version = 5)
@UsesPermissions(permissionNames = "android.permission.BLUETOOTH, android.permission.BLUETOOTH_ADMIN")
public final class Dmysteryshadow extends BluetoothConnectionBase {
    private static final String SPP_UUID = "00001101-0000-1000-8000-00805F9B34FB";
    private Set<Integer> acceptableDeviceClasses;
    private final List<Component> attachedComponents;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.BluetoothConnectionBase.<init>(noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.ComponentContainer, java.lang.String):void in method: noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.Dmysteryshadow.<init>(noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.ComponentContainer):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.BluetoothConnectionBase.<init>(noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.ComponentContainer, java.lang.String):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public Dmysteryshadow(noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.ComponentContainer r1) {
        /*
            r1 = this;
            java.lang.String r0 = "Dmysteryshadow"
            r1.<init>(r2, r0)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1.attachedComponents = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.Dmysteryshadow.<init>(noajjy.qjjijzpba.zyxfnyiawdusrjwbstcglmxmzgw.ComponentContainer):void");
    }

    /* access modifiers changed from: package-private */
    public boolean attachComponent(Component component, Set<Integer> acceptableDeviceClasses2) {
        HashSet hashSet;
        if (this.attachedComponents.isEmpty()) {
            if (acceptableDeviceClasses2 == null) {
                hashSet = null;
            } else {
                hashSet = new HashSet(acceptableDeviceClasses2);
            }
            this.acceptableDeviceClasses = hashSet;
        } else if (this.acceptableDeviceClasses == null) {
            if (acceptableDeviceClasses2 != null) {
                return false;
            }
        } else if (acceptableDeviceClasses2 == null || !this.acceptableDeviceClasses.containsAll(acceptableDeviceClasses2) || !acceptableDeviceClasses2.containsAll(this.acceptableDeviceClasses)) {
            return false;
        }
        this.attachedComponents.add(component);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void detachComponent(Component component) {
        this.attachedComponents.remove(component);
        if (this.attachedComponents.isEmpty()) {
            this.acceptableDeviceClasses = null;
        }
    }

    @SimpleFunction(description = "Checks whether the Bluetooth device with the specified address is paired.")
    public boolean IsDevicePaired(String address) {
        Object bluetoothAdapter = BluetoothReflection.getBluetoothAdapter();
        if (bluetoothAdapter == null) {
            this.form.dispatchErrorOccurredEvent(this, "IsDevicePaired", 501, new Object[0]);
            return false;
        } else if (!BluetoothReflection.isBluetoothEnabled(bluetoothAdapter)) {
            this.form.dispatchErrorOccurredEvent(this, "IsDevicePaired", 502, new Object[0]);
            return false;
        } else {
            int firstSpace = address.indexOf(" ");
            if (firstSpace != -1) {
                address = address.substring(0, firstSpace);
            }
            if (BluetoothReflection.checkBluetoothAddress(bluetoothAdapter, address)) {
                return BluetoothReflection.isBonded(BluetoothReflection.getRemoteDevice(bluetoothAdapter, address));
            }
            this.form.dispatchErrorOccurredEvent(this, "IsDevicePaired", 503, new Object[0]);
            return false;
        }
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "The addresses and names of paired Bluetooth devices")
    public List<String> AddressesAndNames() {
        List<String> addressesAndNames = new ArrayList<>();
        Object bluetoothAdapter = BluetoothReflection.getBluetoothAdapter();
        if (bluetoothAdapter != null && BluetoothReflection.isBluetoothEnabled(bluetoothAdapter)) {
            for (Object bluetoothDevice : BluetoothReflection.getBondedDevices(bluetoothAdapter)) {
                if (isDeviceClassAcceptable(bluetoothDevice)) {
                    addressesAndNames.add(BluetoothReflection.getBluetoothDeviceAddress(bluetoothDevice) + " " + BluetoothReflection.getBluetoothDeviceName(bluetoothDevice));
                }
            }
        }
        return addressesAndNames;
    }

    private boolean isDeviceClassAcceptable(Object bluetoothDevice) {
        if (this.acceptableDeviceClasses == null) {
            return true;
        }
        Object bluetoothClass = BluetoothReflection.getBluetoothClass(bluetoothDevice);
        if (bluetoothClass == null) {
            return false;
        }
        return this.acceptableDeviceClasses.contains(Integer.valueOf(BluetoothReflection.getDeviceClass(bluetoothClass)));
    }

    @SimpleFunction(description = "Connect to the Bluetooth device with the specified address and the Serial Port Profile (SPP). Returns true if the connection was successful.")
    public boolean Connect(String address) {
        return connect("Connect", address, SPP_UUID);
    }

    @SimpleFunction(description = "Connect to the Bluetooth device with the specified address and UUID. Returns true if the connection was successful.")
    public boolean ConnectWithUUID(String address, String uuid) {
        return connect("ConnectWithUUID", address, uuid);
    }

    private boolean connect(String functionName, String address, String uuidString) {
        Object bluetoothAdapter = BluetoothReflection.getBluetoothAdapter();
        if (bluetoothAdapter == null) {
            this.form.dispatchErrorOccurredEvent(this, functionName, 501, new Object[0]);
            return false;
        } else if (!BluetoothReflection.isBluetoothEnabled(bluetoothAdapter)) {
            this.form.dispatchErrorOccurredEvent(this, functionName, 502, new Object[0]);
            return false;
        } else {
            int firstSpace = address.indexOf(" ");
            if (firstSpace != -1) {
                address = address.substring(0, firstSpace);
            }
            if (!BluetoothReflection.checkBluetoothAddress(bluetoothAdapter, address)) {
                this.form.dispatchErrorOccurredEvent(this, functionName, 503, new Object[0]);
                return false;
            }
            Object bluetoothDevice = BluetoothReflection.getRemoteDevice(bluetoothAdapter, address);
            if (!BluetoothReflection.isBonded(bluetoothDevice)) {
                this.form.dispatchErrorOccurredEvent(this, functionName, 504, new Object[0]);
                return false;
            } else if (!isDeviceClassAcceptable(bluetoothDevice)) {
                this.form.dispatchErrorOccurredEvent(this, functionName, 505, new Object[0]);
                return false;
            } else {
                try {
                    UUID uuid = UUID.fromString(uuidString);
                    Disconnect();
                    try {
                        connect(bluetoothDevice, uuid);
                        return true;
                    } catch (IOException e) {
                        Disconnect();
                        this.form.dispatchErrorOccurredEvent(this, functionName, 507, new Object[0]);
                        return false;
                    }
                } catch (IllegalArgumentException e2) {
                    this.form.dispatchErrorOccurredEvent(this, functionName, 506, new Object[]{uuidString});
                    return false;
                }
            }
        }
    }

    private void connect(Object bluetoothDevice, UUID uuid) throws IOException {
        Object bluetoothSocket;
        if (this.secure || SdkLevel.getLevel() < 10) {
            bluetoothSocket = BluetoothReflection.createRfcommSocketToServiceRecord(bluetoothDevice, uuid);
        } else {
            bluetoothSocket = BluetoothReflection.createInsecureRfcommSocketToServiceRecord(bluetoothDevice, uuid);
        }
        BluetoothReflection.connectToBluetoothSocket(bluetoothSocket);
        setConnection(bluetoothSocket);
        Log.i(this.logTag, "Connected to Bluetooth device " + BluetoothReflection.getBluetoothDeviceAddress(bluetoothDevice) + " " + BluetoothReflection.getBluetoothDeviceName(bluetoothDevice) + ".");
    }
}
