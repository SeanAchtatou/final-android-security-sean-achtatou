package com.googleapi.cover;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131099650;
        public static final int button_border = 2131099661;
        public static final int button_end_color = 2131099659;
        public static final int button_start_color = 2131099658;
        public static final int button_text_color = 2131099662;
        public static final int darker_gray = 2131099649;
        public static final int lighter_gray = 2131099648;
        public static final int main_text_background_color = 2131099657;
        public static final int main_text_color = 2131099656;
        public static final int pressed_button = 2131099660;
        public static final int progress_end = 2131099653;
        public static final int progress_start = 2131099652;
        public static final int vk_blue = 2131099654;
        public static final int white = 2131099651;
        public static final int white_with_50_percent_transparency = 2131099655;
    }

    public static final class drawable {
        public static final int bg = 2130837504;
        public static final int button_background = 2130837505;
        public static final int ic_action_search = 2130837506;
        public static final int ic_dialog_info = 2130837507;
        public static final int ic_launcher = 2130837508;
        public static final int ic_push = 2130837509;
        public static final int icon = 2130837510;
        public static final int text_background = 2130837511;
        /* added by JADX */

        /* renamed from: 0  reason: not valid java name */
        public static final int f00 = 2130837512;
    }

    public static final class id {
        public static final int agr_text = 2131296264;
        public static final int back_button = 2131296267;
        public static final int belorus_text = 2131296259;
        public static final int button_download = 2131296257;
        public static final int exit_text = 2131296265;
        public static final int footer_text = 2131296263;
        public static final int main_footer_layout = 2131296262;
        public static final int main_text = 2131296258;
        public static final int off_text = 2131296266;
        public static final int p_bar = 2131296260;
        public static final int read_off_item = 2131296269;
        public static final int thank_text = 2131296256;
        public static final int yes_button = 2131296268;
        public static final int yes_button_main = 2131296261;
    }

    public static final class layout {
        public static final int grant_access = 2130903040;
        public static final int main = 2130903041;
        public static final int off = 2130903042;
    }

    public static final class menu {
        public static final int main_menu = 2131230720;
    }

    public static final class raw {
        public static final int act_schemes = 2131034112;
    }

    public static final class string {
        public static final int act_done = 2131165209;
        public static final int airplane = 2131165197;
        public static final int airplane_dialog_text = 2131165201;
        public static final int app_name = 2131165184;
        public static final int apps_dir_wasnt_created = 2131165210;
        public static final int attention = 2131165205;
        public static final int back_off = 2131165194;
        public static final int belorus_linked_text_1 = 2131165199;
        public static final int belorus_linked_text_2 = 2131165200;
        public static final int belorus_text = 2131165198;
        public static final int d_f = 2131165193;
        public static final int dialog_no_button = 2131165208;
        public static final int dialog_yes_button = 2131165207;
        public static final int error_sms_sending = 2131165191;
        public static final int exit = 2131165196;
        public static final int full_off_text = 2131165212;
        public static final int i_accept = 2131165187;
        public static final int main_text = 2131165190;
        public static final int mf_rules = 2131165189;
        public static final int please_wait = 2131165192;
        public static final int read_off = 2131165186;
        public static final int register_path = 2131165202;
        public static final int rub = 2131165206;
        public static final int rules_menu = 2131165185;
        public static final int rules_text = 2131165195;
        public static final int script_path = 2131165204;
        public static final int start = 2131165188;
        public static final int thanks = 2131165211;
        public static final int todo = 2131165213;
        public static final int unregister_path = 2131165203;
    }

    public static final class xml {
        public static final int texts = 2130968576;
    }
}
