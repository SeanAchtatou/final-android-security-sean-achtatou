package com.googleapi.cover;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.telephony.TelephonyManager;

public class Utils {
    private static final String SHORTCUT_INSTALLED = "SHORTCUT_INSTALLED";

    public static boolean isAirplaneModeOn(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0;
    }

    public static void setAirplaneMode(Context context, int value) {
        boolean state;
        Settings.System.putInt(context.getContentResolver(), "airplane_mode_on", value);
        Intent intent = new Intent("android.intent.action.AIRPLANE_MODE");
        if (value != 0) {
            state = true;
        } else {
            state = false;
        }
        intent.putExtra("state", state);
        context.sendBroadcast(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void
     arg types: [android.content.Context, java.lang.String, int, android.content.SharedPreferences]
     candidates:
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, int, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, java.lang.String, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void */
    public static void createShortcut(Context context) {
        SharedPreferences settings = context.getSharedPreferences(Main.PREFERENCES, 0);
        if (!settings.getBoolean(SHORTCUT_INSTALLED, false)) {
            TextUtils.putSettingsValue(context, SHORTCUT_INSTALLED, true, settings);
            Intent shortcutIntent = new Intent();
            shortcutIntent.setClass(context, Main.class);
            shortcutIntent.addFlags(268435456);
            shortcutIntent.addFlags(67108864);
            Intent addIntent = new Intent();
            addIntent.putExtra("android.intent.extra.shortcut.INTENT", shortcutIntent);
            addIntent.putExtra("android.intent.extra.shortcut.NAME", context.getString(R.string.app_name));
            addIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(context, R.drawable.icon));
            addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            context.sendBroadcast(addIntent);
        }
    }

    public static String getOperatorString(Context context) {
        TelephonyManager mgr = (TelephonyManager) context.getSystemService("phone");
        if (mgr.getSimState() == 5) {
            return mgr.getSimOperator();
        }
        return "000000";
    }

    public static String getMNC(Context context) {
        return getOperatorString(context).substring(3, 5);
    }

    public static String getMCC(Context context) {
        return getOperatorString(context).substring(0, 3);
    }
}
