package com.googleapi.cover;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Off extends Activity {
    private static final int RESULT_OK = 1;
    private Button backButton;
    private Button yesButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(RESULT_OK);
        getWindow().addFlags(1024);
        setResult(0);
        setContentView((int) R.layout.off);
        setListeners();
    }

    private void setListeners() {
        this.yesButton = (Button) findViewById(R.id.yes_button);
        this.yesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Off.this.setResult(Off.RESULT_OK);
                Off.this.finish();
            }
        });
        this.backButton = (Button) findViewById(R.id.back_button);
        this.backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Off.this.setResult(0);
                Off.this.finish();
            }
        });
    }
}
