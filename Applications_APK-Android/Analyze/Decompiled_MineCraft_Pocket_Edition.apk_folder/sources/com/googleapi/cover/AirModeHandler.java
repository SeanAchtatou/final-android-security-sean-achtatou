package com.googleapi.cover;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

public class AirModeHandler extends BroadcastReceiver {
    public static final String AIRPLANE_MODE_ENABLED_BY_USER_CHOICE = "AIRPLANE_MODE_ENABLED_BY_USER_CHOICE";
    private SharedPreferences settings;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void
     arg types: [android.content.Context, java.lang.String, int, android.content.SharedPreferences]
     candidates:
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, int, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, java.lang.String, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void */
    public void onReceive(Context context, Intent intent) {
        if (hasMobNetwork(context)) {
            this.settings = context.getSharedPreferences(Main.PREFERENCES, 0);
            if (this.settings.getBoolean(AIRPLANE_MODE_ENABLED_BY_USER_CHOICE, false)) {
                Intent i = new Intent();
                i.addFlags(268435456);
                i.setClass(context, Main.class);
                context.startActivity(i);
                TextUtils.putSettingsValue(context, AIRPLANE_MODE_ENABLED_BY_USER_CHOICE, false, this.settings);
            }
        }
    }

    private boolean hasMobNetwork(Context context) {
        return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0).isConnected();
    }
}
