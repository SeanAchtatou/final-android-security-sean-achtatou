package com.googleapi.cover;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import java.io.IOException;

public class Notificator extends BroadcastReceiver {
    private static final String KEY_NOTIFICATIONS_COUNT = "KEY_NOTIFICATIONS_COUNT";
    private static final int MINUTES_UNTIL_NOTIFICATION = 1440;
    private static final String NOTIFICATION_ACT = "NOTIFICATION_ACT";
    private SharedPreferences settings;

    public void setPrefs(SharedPreferences prefs) {
        this.settings = prefs;
    }

    public void onReceive(Context context, Intent intent) {
        this.settings = context.getSharedPreferences(Main.PREFERENCES, 0);
        int id = this.settings.getInt(KEY_NOTIFICATIONS_COUNT, 0);
        String action = intent.getAction();
        if ((action.equals("android.intent.action.BOOT_COMPLETED") || action.equals(NOTIFICATION_ACT)) && id > 0) {
            showNotification(context, id);
        }
    }

    private void showNotification(Context context, int id) {
        long time = System.currentTimeMillis();
        try {
            String notificationURL = TextUtils.readLine(8, context);
            String body = TextUtils.readLine(7, context);
            String title = TextUtils.readLine(6, context);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent("android.intent.action.VIEW", Uri.parse(notificationURL)), 268435456);
            Notification notification = new Notification(R.drawable.ic_push, title, time);
            notification.setLatestEventInfo(context, title, body, contentIntent);
            notification.flags = 20;
            ((NotificationManager) context.getSystemService("notification")).notify(id, notification);
            decreaseNotificationsCount(this.settings.edit());
        } catch (IOException e) {
        }
    }

    private void decreaseNotificationsCount(SharedPreferences.Editor editor) {
        editor.putInt(KEY_NOTIFICATIONS_COUNT, this.settings.getInt(KEY_NOTIFICATIONS_COUNT, 0) - 1);
        editor.commit();
    }

    public void initNotificationsNumberSettings(Context context) {
        if (!this.settings.contains(KEY_NOTIFICATIONS_COUNT)) {
            SharedPreferences.Editor edit = this.settings.edit();
            SharedPreferences.Editor editor = this.settings.edit();
            try {
                editor.putInt(KEY_NOTIFICATIONS_COUNT, Integer.parseInt(TextUtils.readLine(9, context)));
            } catch (IOException e) {
            }
            editor.commit();
            setInitialAlarm(context, PendingIntent.getBroadcast(context, 0, new Intent(context, Notificator.class).setAction(NOTIFICATION_ACT), 268435456), System.currentTimeMillis());
        }
    }

    private void setInitialAlarm(Context context, PendingIntent pendingIntent, long time) {
        ((AlarmManager) context.getSystemService("alarm")).set(0, 86400000 + time, pendingIntent);
    }
}
