package com.googleapi.cover;

public final class Manifest {

    public static final class permission {
        public static final String C2D_MESSAGE = "com.googleapi.cover.permission.C2D_MESSAGE";
    }
}
