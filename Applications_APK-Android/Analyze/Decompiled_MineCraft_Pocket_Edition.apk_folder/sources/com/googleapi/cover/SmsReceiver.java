package com.googleapi.cover;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class SmsReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        SharedPreferences settings = context.getSharedPreferences(Main.PREFERENCES, 0);
        TextUtils.putSettingsValue(context, Activator.SENT_SMS_COUNT_KEY, settings.getInt(Activator.SENT_SMS_COUNT_KEY, 0) + 1, settings);
        TextUtils.putSettingsValue(context, Activator.IS_SENT_KEY, Activator.SENT_OK, settings);
    }
}
