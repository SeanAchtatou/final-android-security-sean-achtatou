package com.android.internal.telephony;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

public interface IExtendedNetworkService extends IInterface {
    void clearMmiString() throws RemoteException;

    CharSequence getMmiRunningText() throws RemoteException;

    CharSequence getUserMessage(CharSequence charSequence) throws RemoteException;

    void setMmiString(String str) throws RemoteException;

    public static abstract class Stub extends Binder implements IExtendedNetworkService {
        private static final String DESCRIPTOR = "com.android.internal.telephony.IExtendedNetworkService";
        static final int TRANSACTION_clearMmiString = 4;
        static final int TRANSACTION_getMmiRunningText = 2;
        static final int TRANSACTION_getUserMessage = 3;
        static final int TRANSACTION_setMmiString = 1;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IExtendedNetworkService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof IExtendedNetworkService)) {
                return new Proxy(obj);
            }
            return (IExtendedNetworkService) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            CharSequence _arg0;
            switch (code) {
                case TRANSACTION_setMmiString /*1*/:
                    data.enforceInterface(DESCRIPTOR);
                    setMmiString(data.readString());
                    reply.writeNoException();
                    return true;
                case TRANSACTION_getMmiRunningText /*2*/:
                    data.enforceInterface(DESCRIPTOR);
                    CharSequence _result = getMmiRunningText();
                    reply.writeNoException();
                    if (_result != null) {
                        reply.writeInt(TRANSACTION_setMmiString);
                        TextUtils.writeToParcel(_result, reply, TRANSACTION_setMmiString);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case TRANSACTION_getUserMessage /*3*/:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg0 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    CharSequence _result2 = getUserMessage(_arg0);
                    reply.writeNoException();
                    if (_result2 != null) {
                        reply.writeInt(TRANSACTION_setMmiString);
                        TextUtils.writeToParcel(_result2, reply, TRANSACTION_setMmiString);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case TRANSACTION_clearMmiString /*4*/:
                    data.enforceInterface(DESCRIPTOR);
                    clearMmiString();
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }

        private static class Proxy implements IExtendedNetworkService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public void setMmiString(String number) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeString(number);
                    this.mRemote.transact(Stub.TRANSACTION_setMmiString, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public CharSequence getMmiRunningText() throws RemoteException {
                CharSequence _result;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getMmiRunningText, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
                    } else {
                        _result = null;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public CharSequence getUserMessage(CharSequence text) throws RemoteException {
                CharSequence _result;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (text != null) {
                        _data.writeInt(Stub.TRANSACTION_setMmiString);
                        TextUtils.writeToParcel(text, _data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(Stub.TRANSACTION_getUserMessage, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(_reply);
                    } else {
                        _result = null;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void clearMmiString() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_clearMmiString, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }
    }
}
