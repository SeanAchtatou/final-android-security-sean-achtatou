package com.jobstrak.drawingfun.sdk;

import android.app.job.JobParameters;
import androidx.annotation.RequiresApi;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.service.MainTimer;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

@RequiresApi(api = 21)
public class JobService extends android.app.job.JobService {
    public boolean onStartJob(JobParameters params) {
        startTimer("onStartJob");
        return true;
    }

    public boolean onStopJob(JobParameters params) {
        startTimer("onStopJob");
        return true;
    }

    private void startTimer(String method) {
        if (ManagerFactory.getCryopiggyManager().init(this)) {
            LogUtils.debug("JobService.%s fired", method);
            MainTimer.getInstance().startIfNotRunning();
        }
    }
}
