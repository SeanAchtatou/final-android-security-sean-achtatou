package com.yandex.metrica;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.af;
import com.yandex.metrica.impl.ob.ic;
import com.yandex.metrica.impl.ob.ig;
import com.yandex.metrica.impl.ob.ik;
import com.yandex.metrica.impl.ob.im;
import com.yandex.metrica.impl.ob.in;
import com.yandex.metrica.impl.ob.io;
import java.util.HashMap;
import java.util.Map;

public class ConfigurationService extends Service {
    @NonNull
    private Map<String, im> a = new HashMap();
    private ig b;
    @Nullable
    private String c;

    public void onCreate() {
        super.onCreate();
        af.a(getApplicationContext());
        this.c = String.format("[ConfigurationService:%s]", getPackageName());
        this.b = new ig();
        Context applicationContext = getApplicationContext();
        ik ikVar = new ik(applicationContext, this.b.a(), new ic(applicationContext));
        this.a.put("com.yandex.metrica.configuration.ACTION_INIT", new io(getApplicationContext(), ikVar));
        this.a.put("com.yandex.metrica.configuration.ACTION_SCHEDULED_START", new in(getApplicationContext(), ikVar));
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle bundle = null;
        im imVar = this.a.get(intent == null ? null : intent.getAction());
        if (imVar == null) {
            return 2;
        }
        ig igVar = this.b;
        if (intent != null) {
            bundle = intent.getExtras();
        }
        igVar.a(imVar, bundle);
        return 2;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
