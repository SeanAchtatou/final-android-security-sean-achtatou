package com.jobstrak.drawingfun.sdk.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.repository.RepositoryFactory;
import com.jobstrak.drawingfun.sdk.repository.stat.StatRepository;
import com.jobstrak.drawingfun.sdk.task.stat.UpdatedEventSendTask;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class AppUpdatedReceiver extends BroadcastReceiver {
    @NonNull
    private final Config config = Config.getInstance();
    @NonNull
    private final RequestManager requestManager = ManagerFactory.createRequestManager();
    @NonNull
    private final UrlManager reservedUrlManager = ManagerFactory.createUrlManager(this.config, this.settings, true);
    @NonNull
    private final Settings settings = Settings.getInstance();
    @NonNull
    private final StatRepository statRepository = RepositoryFactory.getStatRepository(this.config, this.urlManager, this.reservedUrlManager, this.requestManager);
    @NonNull
    private final UpdatedEventSendTask.Factory taskFactory = new UpdatedEventSendTask.Factory(this.statRepository);
    @NonNull
    private final UrlManager urlManager = ManagerFactory.createUrlManager(this.config, this.settings);

    public void onReceive(Context context, Intent intent) {
        if (isOurUpdate(context, intent) && ManagerFactory.getCryopiggyManager().init(context)) {
            LogUtils.debug("App has been updated", new Object[0]);
            this.taskFactory.create().execute(new Void[0]);
        }
    }

    private static boolean isOurUpdate(Context context, Intent intent) {
        return intent != null || context.getPackageName().equals(intent.getData().getSchemeSpecificPart());
    }
}
