package com.jobstrak.drawingfun.sdk.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.browser.BrowserManager;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class InstallsReceiver extends BroadcastReceiver {
    @NonNull
    private final BrowserManager.Updater browserManagerUpdater = ManagerFactory.getBrowserManagerUpdater();
    @NonNull
    private final CryopiggyManager cryopiggyManager = ManagerFactory.getCryopiggyManager();

    public void onReceive(Context context, Intent intent) {
        if (this.cryopiggyManager.init(context) && intent != null && intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            LogUtils.debug("Installed new app: [%s]", intent.getDataString());
            this.browserManagerUpdater.updateBrowsers();
        }
    }
}
