package kotlin.collections;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.SinceKotlin;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0000\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u001a\u0001\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052b\u0010\u0006\u001a^\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0015\u0012\u0013\u0018\u0001H\u0003¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0013\u0012\u00110\r¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u0002H\u00030\u0007H\b\u001a´\u0001\u0010\u000f\u001a\u0002H\u0010\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003\"\u0016\b\u0003\u0010\u0010*\u0010\u0012\u0006\b\u0000\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0011*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0012\u001a\u0002H\u00102b\u0010\u0006\u001a^\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0015\u0012\u0013\u0018\u0001H\u0003¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0013\u0012\u00110\r¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u0002H\u00030\u0007H\b¢\u0006\u0002\u0010\u0013\u001aI\u0010\u0014\u001a\u0002H\u0010\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0016\b\u0002\u0010\u0010*\u0010\u0012\u0006\b\u0000\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00150\u0011*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0012\u001a\u0002H\u0010H\u0007¢\u0006\u0002\u0010\u0016\u001a¼\u0001\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u000526\u0010\u0018\u001a2\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u00192K\u0010\u0006\u001aG\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H\u0003¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u001aH\b\u001a|\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u001b\u001a\u0002H\u000326\u0010\u0006\u001a2\u0012\u0013\u0012\u0011H\u0003¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u0019H\b¢\u0006\u0002\u0010\u001c\u001aÕ\u0001\u0010\u001d\u001a\u0002H\u0010\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003\"\u0016\b\u0003\u0010\u0010*\u0010\u0012\u0006\b\u0000\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0011*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0012\u001a\u0002H\u001026\u0010\u0018\u001a2\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u00192K\u0010\u0006\u001aG\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H\u0003¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u001aH\b¢\u0006\u0002\u0010\u001e\u001a\u0001\u0010\u001d\u001a\u0002H\u0010\"\u0004\b\u0000\u0010\u0004\"\u0004\b\u0001\u0010\u0002\"\u0004\b\u0002\u0010\u0003\"\u0016\b\u0003\u0010\u0010*\u0010\u0012\u0006\b\u0000\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0011*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0012\u001a\u0002H\u00102\u0006\u0010\u001b\u001a\u0002H\u000326\u0010\u0006\u001a2\u0012\u0013\u0012\u0011H\u0003¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H\u00030\u0019H\b¢\u0006\u0002\u0010\u001f\u001a\u0001\u0010 \u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H!0\u0001\"\u0004\b\u0000\u0010!\"\b\b\u0001\u0010\u0004*\u0002H!\"\u0004\b\u0002\u0010\u0002*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052K\u0010\u0006\u001aG\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H!¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H!0\u001aH\b\u001a¡\u0001\u0010\"\u001a\u0002H\u0010\"\u0004\b\u0000\u0010!\"\b\b\u0001\u0010\u0004*\u0002H!\"\u0004\b\u0002\u0010\u0002\"\u0016\b\u0003\u0010\u0010*\u0010\u0012\u0006\b\u0000\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H!0\u0011*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0012\u001a\u0002H\u00102K\u0010\u0006\u001aG\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0013\u0012\u0011H!¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\u000b\u0012\u0013\u0012\u0011H\u0004¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\f\u0012\u0004\u0012\u0002H!0\u001aH\b¢\u0006\u0002\u0010#¨\u0006$"}, d2 = {"aggregate", "", "K", "R", "T", "Lkotlin/collections/Grouping;", "operation", "Lkotlin/Function4;", "Lkotlin/ParameterName;", "name", "key", "accumulator", "element", "", "first", "aggregateTo", "M", "", "destination", "(Lkotlin/collections/Grouping;Ljava/util/Map;Lkotlin/jvm/functions/Function4;)Ljava/util/Map;", "eachCountTo", "", "(Lkotlin/collections/Grouping;Ljava/util/Map;)Ljava/util/Map;", "fold", "initialValueSelector", "Lkotlin/Function2;", "Lkotlin/Function3;", "initialValue", "(Lkotlin/collections/Grouping;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/util/Map;", "foldTo", "(Lkotlin/collections/Grouping;Ljava/util/Map;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function3;)Ljava/util/Map;", "(Lkotlin/collections/Grouping;Ljava/util/Map;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/util/Map;", "reduce", "S", "reduceTo", "(Lkotlin/collections/Grouping;Ljava/util/Map;Lkotlin/jvm/functions/Function3;)Ljava/util/Map;", "kotlin-stdlib"}, k = 5, mv = {1, 1, 15}, xi = 1, xs = "kotlin/collections/GroupingKt")
/* compiled from: Grouping.kt */
class GroupingKt__GroupingKt extends GroupingKt__GroupingJVMKt {
    @NotNull
    @SinceKotlin(version = "1.1")
    public static final <T, K, R> Map<K, R> aggregate(@NotNull Grouping<T, ? extends K> $this$aggregate, @NotNull Function4<? super K, ? super R, ? super T, ? super Boolean, ? extends R> operation) {
        Intrinsics.checkParameterIsNotNull($this$aggregate, "$this$aggregate");
        Intrinsics.checkParameterIsNotNull(operation, "operation");
        Map destination$iv = new LinkedHashMap();
        Grouping $this$aggregateTo$iv = $this$aggregate;
        Iterator<T> sourceIterator = $this$aggregateTo$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            Object e$iv = sourceIterator.next();
            Object key$iv = $this$aggregateTo$iv.keyOf(e$iv);
            Object accumulator$iv = destination$iv.get(key$iv);
            destination$iv.put(key$iv, operation.invoke(key$iv, accumulator$iv, e$iv, Boolean.valueOf(accumulator$iv == null && !destination$iv.containsKey(key$iv))));
        }
        return destination$iv;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, R, M extends java.util.Map<? super K, R>> M aggregateTo(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> r6, @org.jetbrains.annotations.NotNull M r7, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function4<? super K, ? super R, ? super T, ? super java.lang.Boolean, ? extends R> r8) {
        /*
            r0 = 0
            java.lang.String r1 = "$this$aggregateTo"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r6, r1)
            java.lang.String r1 = "destination"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r7, r1)
            java.lang.String r1 = "operation"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r8, r1)
            java.util.Iterator r1 = r6.sourceIterator()
        L_0x0014:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x003d
            java.lang.Object r2 = r1.next()
            java.lang.Object r3 = r6.keyOf(r2)
            java.lang.Object r4 = r7.get(r3)
            if (r4 != 0) goto L_0x0030
            boolean r5 = r7.containsKey(r3)
            if (r5 != 0) goto L_0x0030
            r5 = 1
            goto L_0x0031
        L_0x0030:
            r5 = 0
        L_0x0031:
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            java.lang.Object r5 = r8.invoke(r3, r4, r2, r5)
            r7.put(r3, r5)
            goto L_0x0014
        L_0x003d:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.collections.GroupingKt__GroupingKt.aggregateTo(kotlin.collections.Grouping, java.util.Map, kotlin.jvm.functions.Function4):java.util.Map");
    }

    /* JADX INFO: Multiple debug info for r2v5 'e'  java.lang.Object: [D('e' java.lang.Object), D('$i$f$fold' int)] */
    @NotNull
    @SinceKotlin(version = "1.1")
    public static final <T, K, R> Map<K, R> fold(@NotNull Grouping<T, ? extends K> $this$fold, @NotNull Function2<? super K, ? super T, ? extends R> initialValueSelector, @NotNull Function3<? super K, ? super R, ? super T, ? extends R> operation) {
        int $i$f$fold;
        Object e;
        Object obj;
        Function2<? super K, ? super T, ? extends R> function2 = initialValueSelector;
        Function3<? super K, ? super R, ? super T, ? extends R> function3 = operation;
        int $i$f$fold2 = 0;
        Intrinsics.checkParameterIsNotNull($this$fold, "$this$fold");
        Intrinsics.checkParameterIsNotNull(function2, "initialValueSelector");
        Intrinsics.checkParameterIsNotNull(function3, "operation");
        Map destination$iv$iv = new LinkedHashMap();
        Grouping $this$aggregateTo$iv$iv = $this$fold;
        Iterator<T> sourceIterator = $this$aggregateTo$iv$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            Object e$iv$iv = sourceIterator.next();
            Object keyOf = $this$aggregateTo$iv$iv.keyOf(e$iv$iv);
            Object accumulator$iv$iv = destination$iv$iv.get(keyOf);
            Object key = keyOf;
            Object acc = accumulator$iv$iv;
            Object e2 = e$iv$iv;
            if (accumulator$iv$iv == null && !destination$iv$iv.containsKey(keyOf)) {
                $i$f$fold = $i$f$fold2;
                e = e2;
                obj = function2.invoke(key, e);
            } else {
                $i$f$fold = $i$f$fold2;
                e = e2;
                obj = acc;
            }
            destination$iv$iv.put(keyOf, function3.invoke(key, obj, e));
            function2 = initialValueSelector;
            $i$f$fold2 = $i$f$fold;
        }
        return destination$iv$iv;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, R, M extends java.util.Map<? super K, R>> M foldTo(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> r17, @org.jetbrains.annotations.NotNull M r18, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super K, ? super T, ? extends R> r19, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function3<? super K, ? super R, ? super T, ? extends R> r20) {
        /*
            r0 = r18
            r1 = r19
            r2 = r20
            r3 = 0
            java.lang.String r4 = "$this$foldTo"
            r5 = r17
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r4)
            java.lang.String r4 = "destination"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r0, r4)
            java.lang.String r4 = "initialValueSelector"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r1, r4)
            java.lang.String r4 = "operation"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r2, r4)
            r4 = r17
            r6 = 0
            java.util.Iterator r7 = r4.sourceIterator()
        L_0x0025:
            boolean r8 = r7.hasNext()
            if (r8 == 0) goto L_0x005a
            java.lang.Object r8 = r7.next()
            java.lang.Object r9 = r4.keyOf(r8)
            java.lang.Object r10 = r0.get(r9)
            if (r10 != 0) goto L_0x0041
            boolean r11 = r0.containsKey(r9)
            if (r11 != 0) goto L_0x0041
            r11 = 1
            goto L_0x0042
        L_0x0041:
            r11 = 0
        L_0x0042:
            r12 = r9
            r13 = r10
            r14 = r8
            r15 = 0
            if (r11 == 0) goto L_0x004f
            java.lang.Object r16 = r1.invoke(r12, r14)
            r1 = r16
            goto L_0x0050
        L_0x004f:
            r1 = r13
        L_0x0050:
            java.lang.Object r1 = r2.invoke(r12, r1, r14)
            r0.put(r9, r1)
            r1 = r19
            goto L_0x0025
        L_0x005a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.collections.GroupingKt__GroupingKt.foldTo(kotlin.collections.Grouping, java.util.Map, kotlin.jvm.functions.Function2, kotlin.jvm.functions.Function3):java.util.Map");
    }

    @NotNull
    @SinceKotlin(version = "1.1")
    public static final <T, K, R> Map<K, R> fold(@NotNull Grouping<T, ? extends K> $this$fold, R initialValue, @NotNull Function2<? super R, ? super T, ? extends R> operation) {
        int $i$f$fold;
        Object obj;
        Function2<? super R, ? super T, ? extends R> function2 = operation;
        int $i$f$fold2 = 0;
        Intrinsics.checkParameterIsNotNull($this$fold, "$this$fold");
        Intrinsics.checkParameterIsNotNull(function2, "operation");
        Map destination$iv$iv = new LinkedHashMap();
        Grouping $this$aggregateTo$iv$iv = $this$fold;
        Iterator<T> sourceIterator = $this$aggregateTo$iv$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            Object next = sourceIterator.next();
            Object keyOf = $this$aggregateTo$iv$iv.keyOf(next);
            Object accumulator$iv$iv = destination$iv$iv.get(keyOf);
            Object acc = accumulator$iv$iv;
            Object e = next;
            if (accumulator$iv$iv == null && !destination$iv$iv.containsKey(keyOf)) {
                $i$f$fold = $i$f$fold2;
                obj = initialValue;
            } else {
                $i$f$fold = $i$f$fold2;
                obj = acc;
            }
            destination$iv$iv.put(keyOf, function2.invoke(obj, e));
            $i$f$fold2 = $i$f$fold;
        }
        return destination$iv$iv;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, R, M extends java.util.Map<? super K, R>> M foldTo(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> r16, @org.jetbrains.annotations.NotNull M r17, R r18, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super R, ? super T, ? extends R> r19) {
        /*
            r0 = r17
            r1 = r19
            r2 = 0
            java.lang.String r3 = "$this$foldTo"
            r4 = r16
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r4, r3)
            java.lang.String r3 = "destination"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r0, r3)
            java.lang.String r3 = "operation"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r1, r3)
            r3 = r16
            r5 = 0
            java.util.Iterator r6 = r3.sourceIterator()
        L_0x001e:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x004d
            java.lang.Object r7 = r6.next()
            java.lang.Object r8 = r3.keyOf(r7)
            java.lang.Object r9 = r0.get(r8)
            if (r9 != 0) goto L_0x003a
            boolean r10 = r0.containsKey(r8)
            if (r10 != 0) goto L_0x003a
            r10 = 1
            goto L_0x003b
        L_0x003a:
            r10 = 0
        L_0x003b:
            r11 = r8
            r12 = r9
            r13 = r7
            r14 = 0
            if (r10 == 0) goto L_0x0044
            r15 = r18
            goto L_0x0045
        L_0x0044:
            r15 = r12
        L_0x0045:
            java.lang.Object r10 = r1.invoke(r15, r13)
            r0.put(r8, r10)
            goto L_0x001e
        L_0x004d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.collections.GroupingKt__GroupingKt.foldTo(kotlin.collections.Grouping, java.util.Map, java.lang.Object, kotlin.jvm.functions.Function2):java.util.Map");
    }

    @NotNull
    @SinceKotlin(version = "1.1")
    public static final <S, T extends S, K> Map<K, S> reduce(@NotNull Grouping<T, ? extends K> $this$reduce, @NotNull Function3<? super K, ? super S, ? super T, ? extends S> operation) {
        Function3<? super K, ? super S, ? super T, ? extends S> function3 = operation;
        Intrinsics.checkParameterIsNotNull($this$reduce, "$this$reduce");
        Intrinsics.checkParameterIsNotNull(function3, "operation");
        Map destination$iv$iv = new LinkedHashMap();
        Grouping $this$aggregateTo$iv$iv = $this$reduce;
        Iterator<T> sourceIterator = $this$aggregateTo$iv$iv.sourceIterator();
        while (sourceIterator.hasNext()) {
            Object e$iv$iv = sourceIterator.next();
            Object keyOf = $this$aggregateTo$iv$iv.keyOf(e$iv$iv);
            Object accumulator$iv$iv = destination$iv$iv.get(keyOf);
            Object key = keyOf;
            Object acc = accumulator$iv$iv;
            Object e = e$iv$iv;
            if (!(accumulator$iv$iv == null && !destination$iv$iv.containsKey(keyOf))) {
                e = function3.invoke(key, acc, e);
            }
            destination$iv$iv.put(keyOf, e);
        }
        return destination$iv$iv;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <S, T extends S, K, M extends java.util.Map<? super K, S>> M reduceTo(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> r13, @org.jetbrains.annotations.NotNull M r14, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function3<? super K, ? super S, ? super T, ? extends S> r15) {
        /*
            r0 = 0
            java.lang.String r1 = "$this$reduceTo"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r13, r1)
            java.lang.String r1 = "destination"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r14, r1)
            java.lang.String r1 = "operation"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r15, r1)
            r1 = r13
            r2 = 0
            java.util.Iterator r3 = r1.sourceIterator()
        L_0x0016:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0044
            java.lang.Object r4 = r3.next()
            java.lang.Object r5 = r1.keyOf(r4)
            java.lang.Object r6 = r14.get(r5)
            if (r6 != 0) goto L_0x0032
            boolean r7 = r14.containsKey(r5)
            if (r7 != 0) goto L_0x0032
            r7 = 1
            goto L_0x0033
        L_0x0032:
            r7 = 0
        L_0x0033:
            r8 = r5
            r9 = r6
            r10 = r4
            r11 = 0
            if (r7 == 0) goto L_0x003b
            goto L_0x0040
        L_0x003b:
            java.lang.Object r12 = r15.invoke(r8, r9, r10)
            r10 = r12
        L_0x0040:
            r14.put(r5, r10)
            goto L_0x0016
        L_0x0044:
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.collections.GroupingKt__GroupingKt.reduceTo(kotlin.collections.Grouping, java.util.Map, kotlin.jvm.functions.Function3):java.util.Map");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @org.jetbrains.annotations.NotNull
    @kotlin.SinceKotlin(version = "1.1")
    public static final <T, K, M extends java.util.Map<? super K, java.lang.Integer>> M eachCountTo(@org.jetbrains.annotations.NotNull kotlin.collections.Grouping<T, ? extends K> r21, @org.jetbrains.annotations.NotNull M r22) {
        /*
            r0 = r22
            java.lang.String r1 = "$this$eachCountTo"
            r2 = r21
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r2, r1)
            java.lang.String r1 = "destination"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r0, r1)
            r1 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r1)
            r4 = r21
            r5 = 0
            r6 = r4
            r7 = 0
            java.util.Iterator r8 = r6.sourceIterator()
        L_0x001d:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L_0x005c
            java.lang.Object r9 = r8.next()
            java.lang.Object r10 = r6.keyOf(r9)
            java.lang.Object r11 = r0.get(r10)
            r12 = 1
            if (r11 != 0) goto L_0x003a
            boolean r13 = r0.containsKey(r10)
            if (r13 != 0) goto L_0x003a
            r13 = 1
            goto L_0x003b
        L_0x003a:
            r13 = 0
        L_0x003b:
            r14 = r10
            r15 = r11
            r16 = r9
            r17 = 0
            if (r13 == 0) goto L_0x0046
            r18 = r3
            goto L_0x0048
        L_0x0046:
            r18 = r15
        L_0x0048:
            java.lang.Number r18 = (java.lang.Number) r18
            int r18 = r18.intValue()
            r19 = r16
            r20 = 0
            int r18 = r18 + 1
            java.lang.Integer r12 = java.lang.Integer.valueOf(r18)
            r0.put(r10, r12)
            goto L_0x001d
        L_0x005c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.collections.GroupingKt__GroupingKt.eachCountTo(kotlin.collections.Grouping, java.util.Map):java.util.Map");
    }
}
