package kotlin.collections;

import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.RestrictedSuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.SequenceScope;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00040\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "T", "Lkotlin/sequences/SequenceScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlin.collections.SlidingWindowKt$windowedIterator$1", f = "SlidingWindow.kt", i = {0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 4, 4}, l = {33, 39, 46, 52, 55}, m = "invokeSuspend", n = {"gap", "buffer", "skip", "e", "gap", "buffer", "skip", "gap", "buffer", "e", "gap", "buffer", "gap", "buffer"}, s = {"I$0", "L$1", "I$1", "L$2", "I$0", "L$0", "I$1", "I$0", "L$1", "L$2", "I$0", "L$1", "I$0", "L$0"})
/* compiled from: SlidingWindow.kt */
final class SlidingWindowKt$windowedIterator$1 extends RestrictedSuspendLambda implements Function2<SequenceScope<? super List<? extends T>>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Iterator $iterator;
    final /* synthetic */ boolean $partialWindows;
    final /* synthetic */ boolean $reuseBuffer;
    final /* synthetic */ int $size;
    final /* synthetic */ int $step;
    int I$0;
    int I$1;
    Object L$0;
    Object L$1;
    Object L$2;
    Object L$3;
    int label;
    private SequenceScope p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SlidingWindowKt$windowedIterator$1(int i, int i2, Iterator it, boolean z, boolean z2, Continuation continuation) {
        super(2, continuation);
        this.$step = i;
        this.$size = i2;
        this.$iterator = it;
        this.$reuseBuffer = z;
        this.$partialWindows = z2;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        SlidingWindowKt$windowedIterator$1 slidingWindowKt$windowedIterator$1 = new SlidingWindowKt$windowedIterator$1(this.$step, this.$size, this.$iterator, this.$reuseBuffer, this.$partialWindows, continuation);
        SequenceScope sequenceScope = (SequenceScope) obj;
        slidingWindowKt$windowedIterator$1.p$ = (SequenceScope) obj;
        return slidingWindowKt$windowedIterator$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((SlidingWindowKt$windowedIterator$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v17, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v9, resolved type: kotlin.collections.RingBuffer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v23, resolved type: kotlin.collections.RingBuffer} */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x011a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x016d  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x017b  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01bd  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x00f6 A[SYNTHETIC] */
    @org.jetbrains.annotations.Nullable
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r15) {
        /*
            r14 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r14.label
            r2 = 5
            r3 = 4
            r4 = 3
            r5 = 2
            r6 = 1
            if (r1 == 0) goto L_0x0098
            r7 = 0
            r8 = 0
            if (r1 == r6) goto L_0x0076
            if (r1 == r5) goto L_0x0063
            if (r1 == r4) goto L_0x0049
            if (r1 == r3) goto L_0x0031
            if (r1 != r2) goto L_0x0029
            r0 = r8
            r1 = r7
            java.lang.Object r2 = r14.L$0
            r0 = r2
            kotlin.collections.RingBuffer r0 = (kotlin.collections.RingBuffer) r0
            int r1 = r14.I$0
            kotlin.ResultKt.throwOnFailure(r15)
            r4 = r15
            r15 = r14
            goto L_0x01ba
        L_0x0029:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0031:
            r1 = r8
            r4 = r7
            java.lang.Object r5 = r14.L$1
            r1 = r5
            kotlin.collections.RingBuffer r1 = (kotlin.collections.RingBuffer) r1
            int r4 = r14.I$0
            java.lang.Object r5 = r14.L$0
            kotlin.sequences.SequenceScope r5 = (kotlin.sequences.SequenceScope) r5
            kotlin.ResultKt.throwOnFailure(r15)
            r7 = r15
            r15 = r14
            r13 = r4
            r4 = r0
            r0 = r1
            r1 = r13
            goto L_0x019c
        L_0x0049:
            r1 = r8
            r5 = r8
            java.lang.Object r8 = r14.L$3
            java.util.Iterator r8 = (java.util.Iterator) r8
            java.lang.Object r1 = r14.L$2
            java.lang.Object r9 = r14.L$1
            r5 = r9
            kotlin.collections.RingBuffer r5 = (kotlin.collections.RingBuffer) r5
            int r7 = r14.I$0
            java.lang.Object r9 = r14.L$0
            kotlin.sequences.SequenceScope r9 = (kotlin.sequences.SequenceScope) r9
            kotlin.ResultKt.throwOnFailure(r15)
            r10 = r15
            r15 = r14
            goto L_0x0162
        L_0x0063:
            r0 = r7
            r1 = r8
            r2 = r7
            int r0 = r14.I$1
            java.lang.Object r3 = r14.L$0
            r1 = r3
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            int r2 = r14.I$0
            kotlin.ResultKt.throwOnFailure(r15)
            r4 = r15
            r15 = r14
            goto L_0x011b
        L_0x0076:
            r1 = r8
            r2 = r7
            r3 = r8
            r4 = r7
            java.lang.Object r7 = r14.L$3
            java.util.Iterator r7 = (java.util.Iterator) r7
            java.lang.Object r1 = r14.L$2
            int r2 = r14.I$1
            java.lang.Object r8 = r14.L$1
            r3 = r8
            java.util.ArrayList r3 = (java.util.ArrayList) r3
            int r4 = r14.I$0
            java.lang.Object r8 = r14.L$0
            kotlin.sequences.SequenceScope r8 = (kotlin.sequences.SequenceScope) r8
            kotlin.ResultKt.throwOnFailure(r15)
            r9 = r1
            r1 = r3
            r3 = r0
            r0 = r2
            r2 = r4
            r4 = r15
            r15 = r14
            goto L_0x00e4
        L_0x0098:
            kotlin.ResultKt.throwOnFailure(r15)
            kotlin.sequences.SequenceScope r1 = r14.p$
            int r7 = r14.$step
            int r8 = r14.$size
            int r7 = r7 - r8
            if (r7 < 0) goto L_0x011d
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>(r8)
            r3 = 0
            java.util.Iterator r4 = r14.$iterator
            r8 = r1
            r1 = r2
            r2 = r7
            r7 = r4
            r4 = r15
            r15 = r14
            r13 = r3
            r3 = r0
            r0 = r13
        L_0x00b5:
            boolean r9 = r7.hasNext()
            if (r9 == 0) goto L_0x00f6
            java.lang.Object r9 = r7.next()
            if (r0 <= 0) goto L_0x00c4
            int r0 = r0 + -1
            goto L_0x00f5
        L_0x00c4:
            r1.add(r9)
            int r10 = r1.size()
            int r11 = r15.$size
            if (r10 != r11) goto L_0x00f5
            r15.L$0 = r8
            r15.I$0 = r2
            r15.L$1 = r1
            r15.I$1 = r0
            r15.L$2 = r9
            r15.L$3 = r7
            r15.label = r6
            java.lang.Object r10 = r8.yield(r1, r15)
            if (r10 != r3) goto L_0x00e4
            return r3
        L_0x00e4:
            boolean r10 = r15.$reuseBuffer
            if (r10 == 0) goto L_0x00ec
            r1.clear()
            goto L_0x00f4
        L_0x00ec:
            java.util.ArrayList r10 = new java.util.ArrayList
            int r11 = r15.$size
            r10.<init>(r11)
            r1 = r10
        L_0x00f4:
            r0 = r2
        L_0x00f5:
            goto L_0x00b5
        L_0x00f6:
            r7 = r1
            java.util.Collection r7 = (java.util.Collection) r7
            boolean r7 = r7.isEmpty()
            r6 = r6 ^ r7
            if (r6 == 0) goto L_0x01be
            boolean r6 = r15.$partialWindows
            if (r6 != 0) goto L_0x010c
            int r6 = r1.size()
            int r7 = r15.$size
            if (r6 != r7) goto L_0x01be
        L_0x010c:
            r15.I$0 = r2
            r15.L$0 = r1
            r15.I$1 = r0
            r15.label = r5
            java.lang.Object r5 = r8.yield(r1, r15)
            if (r5 != r3) goto L_0x011b
            return r3
        L_0x011b:
            goto L_0x01be
        L_0x011d:
            kotlin.collections.RingBuffer r5 = new kotlin.collections.RingBuffer
            r5.<init>(r8)
            java.util.Iterator r8 = r14.$iterator
            r9 = r1
            r1 = r15
            r15 = r14
        L_0x0127:
            boolean r10 = r8.hasNext()
            if (r10 == 0) goto L_0x0169
            java.lang.Object r10 = r8.next()
            r5.add(r10)
            boolean r11 = r5.isFull()
            if (r11 == 0) goto L_0x0168
            boolean r11 = r15.$reuseBuffer
            if (r11 == 0) goto L_0x0142
            r11 = r5
            java.util.List r11 = (java.util.List) r11
            goto L_0x014c
        L_0x0142:
            java.util.ArrayList r11 = new java.util.ArrayList
            r12 = r5
            java.util.Collection r12 = (java.util.Collection) r12
            r11.<init>(r12)
            java.util.List r11 = (java.util.List) r11
        L_0x014c:
            r15.L$0 = r9
            r15.I$0 = r7
            r15.L$1 = r5
            r15.L$2 = r10
            r15.L$3 = r8
            r15.label = r4
            java.lang.Object r11 = r9.yield(r11, r15)
            if (r11 != r0) goto L_0x015f
            return r0
        L_0x015f:
            r13 = r10
            r10 = r1
            r1 = r13
        L_0x0162:
            int r11 = r15.$step
            r5.removeFirst(r11)
            r1 = r10
        L_0x0168:
            goto L_0x0127
        L_0x0169:
            boolean r4 = r15.$partialWindows
            if (r4 == 0) goto L_0x01bd
            r4 = r0
            r0 = r5
            r5 = r9
            r13 = r7
            r7 = r1
            r1 = r13
        L_0x0173:
            int r8 = r0.size()
            int r9 = r15.$step
            if (r8 <= r9) goto L_0x01a2
            boolean r8 = r15.$reuseBuffer
            if (r8 == 0) goto L_0x0183
            r8 = r0
            java.util.List r8 = (java.util.List) r8
            goto L_0x018d
        L_0x0183:
            java.util.ArrayList r8 = new java.util.ArrayList
            r9 = r0
            java.util.Collection r9 = (java.util.Collection) r9
            r8.<init>(r9)
            java.util.List r8 = (java.util.List) r8
        L_0x018d:
            r15.L$0 = r5
            r15.I$0 = r1
            r15.L$1 = r0
            r15.label = r3
            java.lang.Object r8 = r5.yield(r8, r15)
            if (r8 != r4) goto L_0x019c
            return r4
        L_0x019c:
            int r8 = r15.$step
            r0.removeFirst(r8)
            goto L_0x0173
        L_0x01a2:
            r3 = r0
            java.util.Collection r3 = (java.util.Collection) r3
            boolean r3 = r3.isEmpty()
            r3 = r3 ^ r6
            if (r3 == 0) goto L_0x01bb
            r15.I$0 = r1
            r15.L$0 = r0
            r15.label = r2
            java.lang.Object r2 = r5.yield(r0, r15)
            if (r2 != r4) goto L_0x01b9
            return r4
        L_0x01b9:
            r4 = r7
        L_0x01ba:
            goto L_0x01be
        L_0x01bb:
            r4 = r7
            goto L_0x01be
        L_0x01bd:
            r4 = r1
        L_0x01be:
            kotlin.Unit r0 = kotlin.Unit.INSTANCE
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.collections.SlidingWindowKt$windowedIterator$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
