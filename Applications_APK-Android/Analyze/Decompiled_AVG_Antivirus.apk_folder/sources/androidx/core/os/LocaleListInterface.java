package androidx.core.os;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Locale;
import kotlin.ULong;

interface LocaleListInterface {
    Locale get(int i);

    @Nullable
    Locale getFirstMatch(@NonNull String[] strArr);

    Object getLocaleList();

    @IntRange(from = ULong.MAX_VALUE)
    int indexOf(Locale locale);

    boolean isEmpty();

    @IntRange(from = ULong.MIN_VALUE)
    int size();

    String toLanguageTags();
}
