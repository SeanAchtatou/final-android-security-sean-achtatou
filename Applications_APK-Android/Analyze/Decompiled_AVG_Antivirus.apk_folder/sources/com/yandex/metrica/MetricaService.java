package com.yandex.metrica;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.yandex.metrica.IMetricaService;
import com.yandex.metrica.impl.ob.af;
import com.yandex.metrica.impl.ob.au;
import com.yandex.metrica.impl.ob.av;
import com.yandex.metrica.impl.ob.aw;
import com.yandex.metrica.impl.ob.bj;
import com.yandex.metrica.impl.ob.cn;
import com.yandex.metrica.impl.ob.cv;
import com.yandex.metrica.impl.ob.tp;

public class MetricaService extends Service {
    private c a = new c() {
        public void a(int i) {
            MetricaService.this.stopSelfResult(i);
        }
    };
    /* access modifiers changed from: private */
    public au b;
    private final IMetricaService.Stub c = new IMetricaService.Stub() {
        @Deprecated
        public void reportEvent(String event, int type, String value, Bundle data) throws RemoteException {
            MetricaService.this.b.a(event, type, value, data);
        }

        public void reportData(Bundle data) throws RemoteException {
            MetricaService.this.b.a(data);
        }
    };

    public interface c {
        void a(int i);
    }

    static class b extends Binder {
        b() {
        }
    }

    static class a extends Binder {
        a() {
        }
    }

    public void onCreate() {
        super.onCreate();
        af.a(getApplicationContext());
        a(getResources().getConfiguration());
        tp.a(getApplicationContext());
        this.b = new av(new aw(getApplicationContext(), this.a));
        this.b.a();
    }

    public void onStart(Intent intent, int startId) {
        this.b.a(intent, startId);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        this.b.a(intent, flags, startId);
        return 2;
    }

    public IBinder onBind(Intent intent) {
        IBinder iBinder;
        String action = intent.getAction();
        if ("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER".equals(action)) {
            iBinder = new b();
        } else if ("com.yandex.metrica.ACTION_C_BG_L".equals(action)) {
            iBinder = new a();
        } else {
            iBinder = this.c;
        }
        this.b.a(intent);
        return iBinder;
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        this.b.b(intent);
    }

    public void onDestroy() {
        this.b.b();
        super.onDestroy();
    }

    public boolean onUnbind(Intent intent) {
        this.b.c(intent);
        String action = intent.getAction();
        if ("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER".equals(action)) {
            return false;
        }
        if (!"com.yandex.metrica.ACTION_C_BG_L".equals(action) && a(intent)) {
            return false;
        }
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        a(newConfig);
    }

    private void a(Configuration configuration) {
        cn.a().b(new cv(bj.a(configuration.locale)));
    }

    private boolean a(Intent intent) {
        return intent == null || intent.getData() == null;
    }
}
