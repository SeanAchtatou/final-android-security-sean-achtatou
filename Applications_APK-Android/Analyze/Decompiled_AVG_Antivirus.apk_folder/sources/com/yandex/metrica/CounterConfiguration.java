package com.yandex.metrica;

import android.content.ContentValues;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.cg;
import com.yandex.metrica.impl.ob.mo;
import com.yandex.metrica.impl.ob.u;

@Deprecated
public class CounterConfiguration implements Parcelable {
    public static final Parcelable.Creator<CounterConfiguration> CREATOR = new Parcelable.Creator<CounterConfiguration>() {
        /* renamed from: a */
        public CounterConfiguration createFromParcel(Parcel parcel) {
            return new CounterConfiguration((ContentValues) parcel.readBundle(u.class.getClassLoader()).getParcelable("com.yandex.metrica.CounterConfiguration.data"));
        }

        /* renamed from: a */
        public CounterConfiguration[] newArray(int i) {
            return new CounterConfiguration[i];
        }
    };
    private final ContentValues a;

    public synchronized String toString() {
        return "CounterConfiguration{mParamsMapping=" + this.a + '}';
    }

    public CounterConfiguration(@NonNull CounterConfiguration other) {
        synchronized (other) {
            this.a = new ContentValues(other.a);
        }
    }

    public CounterConfiguration() {
        this.a = new ContentValues();
    }

    public CounterConfiguration(g config) {
        this();
        synchronized (this) {
            e(config.apiKey);
            a(config.sessionTimeout);
            a(config);
            b(config);
            c(config);
            d(config);
            b(config.f);
            c(config.g);
            e(config);
            f(config);
            g(config);
            h(config);
            i(config);
            b(config.statisticsSending);
        }
    }

    public CounterConfiguration(@NonNull e config) {
        this();
        synchronized (this) {
            e(config.apiKey);
            a(config.sessionTimeout);
            b(config.a);
            c(config.b);
            a(config.logs);
            b(config.statisticsSending);
        }
    }

    private void e(@Nullable String str) {
        if (cg.a((Object) str)) {
            b(str);
        }
    }

    private void a(@Nullable Integer num) {
        if (cg.a(num)) {
            c(num.intValue());
        }
    }

    private void a(g gVar) {
        if (cg.a(gVar.location)) {
            a(gVar.location);
        }
    }

    private void b(g gVar) {
        if (cg.a(gVar.locationTracking)) {
            a(gVar.locationTracking.booleanValue());
        }
    }

    private void c(g gVar) {
        if (cg.a(gVar.installedAppCollecting)) {
            b(gVar.installedAppCollecting.booleanValue());
        }
    }

    private void d(g gVar) {
        if (cg.a((Object) gVar.a)) {
            a(gVar.a);
        }
    }

    private void b(@Nullable Integer num) {
        if (cg.a(num)) {
            a(num.intValue());
        }
    }

    private void c(@Nullable Integer num) {
        if (cg.a(num)) {
            b(num.intValue());
        }
    }

    private void a(@Nullable Boolean bool) {
        if (cg.a(bool)) {
            c(bool.booleanValue());
        }
    }

    private void e(g gVar) {
        if (!TextUtils.isEmpty(gVar.appVersion)) {
            d(gVar.appVersion);
        }
    }

    private void f(g gVar) {
        if (cg.a(gVar.e)) {
            d(gVar.e.intValue());
        }
    }

    private void g(g gVar) {
        if (cg.a(gVar.j)) {
            d(gVar.j.booleanValue());
        }
    }

    private void h(g gVar) {
        if (cg.a(gVar.k)) {
            e(gVar.k.booleanValue());
        }
    }

    private void i(g gVar) {
        if (cg.a(gVar.firstActivationAsUpdate)) {
            f(gVar.firstActivationAsUpdate.booleanValue());
        }
    }

    private void b(@Nullable Boolean bool) {
        if (cg.a(bool)) {
            g(bool.booleanValue());
        }
    }

    @VisibleForTesting
    public synchronized void a(int i) {
        this.a.put("CFG_DISPATCH_PERIOD", Integer.valueOf(i));
    }

    @Nullable
    public Integer a() {
        return this.a.getAsInteger("CFG_DISPATCH_PERIOD");
    }

    @VisibleForTesting
    public synchronized void b(int i) {
        ContentValues contentValues = this.a;
        if (i <= 0) {
            i = Integer.MAX_VALUE;
        }
        contentValues.put("CFG_MAX_REPORTS_COUNT", Integer.valueOf(i));
    }

    @Nullable
    public Integer b() {
        return this.a.getAsInteger("CFG_MAX_REPORTS_COUNT");
    }

    @VisibleForTesting
    public synchronized void c(int i) {
        this.a.put("CFG_SESSION_TIMEOUT", Integer.valueOf(i));
    }

    @Nullable
    public Integer c() {
        return this.a.getAsInteger("CFG_SESSION_TIMEOUT");
    }

    public final synchronized void a(@Nullable String str) {
        ContentValues contentValues = this.a;
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        contentValues.put("CFG_DEVICE_SIZE_TYPE", str);
    }

    @Nullable
    public String d() {
        return this.a.getAsString("CFG_DEVICE_SIZE_TYPE");
    }

    @VisibleForTesting
    public synchronized void b(String str) {
        this.a.put("CFG_API_KEY", str);
    }

    public synchronized void c(String str) {
        this.a.put("CFG_UUID", str);
    }

    public String e() {
        return this.a.getAsString("CFG_API_KEY");
    }

    public synchronized void a(boolean z) {
        this.a.put("CFG_LOCATION_TRACKING", Boolean.valueOf(z));
    }

    @Nullable
    public Boolean f() {
        return this.a.getAsBoolean("CFG_LOCATION_TRACKING");
    }

    public final synchronized void d(String str) {
        this.a.put("CFG_APP_VERSION", str);
    }

    public String g() {
        return this.a.getAsString("CFG_APP_VERSION");
    }

    public synchronized void d(int i) {
        this.a.put("CFG_APP_VERSION_CODE", String.valueOf(i));
    }

    public String h() {
        return this.a.getAsString("CFG_APP_VERSION_CODE");
    }

    public synchronized void b(boolean z) {
        this.a.put("CFG_COLLECT_INSTALLED_APPS", Boolean.valueOf(z));
    }

    @Nullable
    public Boolean i() {
        return this.a.getAsBoolean("CFG_COLLECT_INSTALLED_APPS");
    }

    public final synchronized void a(Location location) {
        this.a.put("CFG_MANUAL_LOCATION", mo.a(location));
    }

    public synchronized void c(boolean z) {
        this.a.put("CFG_IS_LOG_ENABLED", Boolean.valueOf(z));
    }

    @Nullable
    public Boolean j() {
        return this.a.getAsBoolean("CFG_IS_LOG_ENABLED");
    }

    public Location k() {
        if (this.a.containsKey("CFG_MANUAL_LOCATION")) {
            return mo.a(this.a.getAsByteArray("CFG_MANUAL_LOCATION"));
        }
        return null;
    }

    @Nullable
    public Boolean l() {
        return this.a.getAsBoolean("CFG_AUTO_PRELOAD_INFO_DETECTION");
    }

    public synchronized void d(boolean z) {
        this.a.put("CFG_AUTO_PRELOAD_INFO_DETECTION", Boolean.valueOf(z));
    }

    public synchronized void e(boolean z) {
        this.a.put("CFG_PERMISSIONS_COLLECTING", Boolean.valueOf(z));
    }

    public final synchronized void f(boolean z) {
        this.a.put("CFG_IS_FIRST_ACTIVATION_AS_UPDATE", Boolean.valueOf(z));
    }

    @Nullable
    public Boolean m() {
        return this.a.getAsBoolean("CFG_IS_FIRST_ACTIVATION_AS_UPDATE");
    }

    public Boolean n() {
        return this.a.getAsBoolean("CFG_STATISTICS_SENDING");
    }

    public final synchronized void g(boolean z) {
        this.a.put("CFG_STATISTICS_SENDING", Boolean.valueOf(z));
    }

    public int describeContents() {
        return 0;
    }

    public synchronized void writeToParcel(Parcel destObj, int flags) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.yandex.metrica.CounterConfiguration.data", this.a);
        destObj.writeBundle(bundle);
    }

    public synchronized void h(boolean z) {
        i(z);
    }

    private synchronized void i(boolean z) {
        this.a.put("CFG_MAIN_REPORTER", Boolean.valueOf(z));
    }

    public synchronized void o() {
        j(true);
        i(false);
    }

    private synchronized void j(boolean z) {
        this.a.put("CFG_COMMUTATION_REPORTER", Boolean.valueOf(z));
    }

    public boolean p() {
        return a("CFG_MAIN_REPORTER", true);
    }

    public boolean q() {
        return a("CFG_COMMUTATION_REPORTER", false);
    }

    public synchronized void a(Bundle bundle) {
        bundle.putParcelable("COUNTER_CFG_OBJ", this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0058, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b(android.os.Bundle r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            if (r3 != 0) goto L_0x0005
            monitor-exit(r2)
            return
        L_0x0005:
            java.lang.String r0 = "CFG_DISPATCH_PERIOD"
            int r0 = r3.getInt(r0)     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x0016
            java.lang.String r0 = "CFG_DISPATCH_PERIOD"
            int r0 = r3.getInt(r0)     // Catch:{ all -> 0x0059 }
            r2.a(r0)     // Catch:{ all -> 0x0059 }
        L_0x0016:
            java.lang.String r0 = "CFG_SESSION_TIMEOUT"
            int r0 = r3.getInt(r0)     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x0027
            java.lang.String r0 = "CFG_SESSION_TIMEOUT"
            int r0 = r3.getInt(r0)     // Catch:{ all -> 0x0059 }
            r2.c(r0)     // Catch:{ all -> 0x0059 }
        L_0x0027:
            java.lang.String r0 = "CFG_MAX_REPORTS_COUNT"
            int r0 = r3.getInt(r0)     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x0038
            java.lang.String r0 = "CFG_MAX_REPORTS_COUNT"
            int r0 = r3.getInt(r0)     // Catch:{ all -> 0x0059 }
            r2.b(r0)     // Catch:{ all -> 0x0059 }
        L_0x0038:
            java.lang.String r0 = "CFG_API_KEY"
            java.lang.String r0 = r3.getString(r0)     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x0057
            java.lang.String r0 = "-1"
            java.lang.String r1 = "CFG_API_KEY"
            java.lang.String r1 = r3.getString(r1)     // Catch:{ all -> 0x0059 }
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0059 }
            if (r0 != 0) goto L_0x0057
            java.lang.String r0 = "CFG_API_KEY"
            java.lang.String r3 = r3.getString(r0)     // Catch:{ all -> 0x0059 }
            r2.b(r3)     // Catch:{ all -> 0x0059 }
        L_0x0057:
            monitor-exit(r2)
            return
        L_0x0059:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.CounterConfiguration.b(android.os.Bundle):void");
    }

    public static CounterConfiguration c(Bundle bundle) {
        CounterConfiguration counterConfiguration = null;
        if (bundle != null) {
            try {
                counterConfiguration = (CounterConfiguration) bundle.getParcelable("COUNTER_CFG_OBJ");
            } catch (Throwable th) {
                return null;
            }
        }
        if (counterConfiguration == null) {
            counterConfiguration = new CounterConfiguration();
        }
        counterConfiguration.b(bundle);
        return counterConfiguration;
    }

    private boolean a(@NonNull String str, boolean z) {
        Boolean asBoolean = this.a.getAsBoolean(str);
        return asBoolean == null ? z : asBoolean.booleanValue();
    }

    @VisibleForTesting
    CounterConfiguration(ContentValues cv) {
        this.a = cv;
    }
}
