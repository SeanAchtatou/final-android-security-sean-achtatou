package com.yandex.metrica;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.ReporterConfig;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class e extends ReporterConfig {
    @Nullable
    public final Integer a;
    @Nullable
    public final Integer b;
    public final Map<String, String> c;

    e(@NonNull a aVar) {
        super(aVar.a);
        this.b = aVar.b;
        this.a = aVar.c;
        this.c = aVar.d == null ? null : Collections.unmodifiableMap(aVar.d);
    }

    private e(ReporterConfig reporterConfig) {
        super(reporterConfig);
        this.a = null;
        this.b = null;
        this.c = null;
    }

    public static e a(@NonNull ReporterConfig reporterConfig) {
        return new e(reporterConfig);
    }

    public static a a(@NonNull String str) {
        return new a(str);
    }

    public static class a {
        ReporterConfig.Builder a;
        Integer b;
        Integer c;
        LinkedHashMap<String, String> d = new LinkedHashMap<>();

        public a(String str) {
            this.a = ReporterConfig.newConfigBuilder(str);
        }

        @NonNull
        public a a(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @NonNull
        public e a() {
            return new e(this);
        }
    }
}
