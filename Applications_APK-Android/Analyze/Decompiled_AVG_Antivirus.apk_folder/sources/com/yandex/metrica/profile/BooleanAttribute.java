package com.yandex.metrica.profile;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.om;
import com.yandex.metrica.impl.ob.oo;
import com.yandex.metrica.impl.ob.op;
import com.yandex.metrica.impl.ob.os;
import com.yandex.metrica.impl.ob.oy;
import com.yandex.metrica.impl.ob.oz;
import com.yandex.metrica.impl.ob.pe;
import com.yandex.metrica.impl.ob.vx;

public class BooleanAttribute {
    private final os a;

    BooleanAttribute(@NonNull String key, @NonNull vx<String> keyValidator, @NonNull om saver) {
        this.a = new os(key, keyValidator, saver);
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValue(boolean value) {
        return new UserProfileUpdate<>(new oo(this.a.a(), value, this.a.c(), new op(this.a.b())));
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValueIfUndefined(boolean value) {
        return new UserProfileUpdate<>(new oo(this.a.a(), value, this.a.c(), new oz(this.a.b())));
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValueReset() {
        return new UserProfileUpdate<>(new oy(3, this.a.a(), this.a.c(), this.a.b()));
    }
}
