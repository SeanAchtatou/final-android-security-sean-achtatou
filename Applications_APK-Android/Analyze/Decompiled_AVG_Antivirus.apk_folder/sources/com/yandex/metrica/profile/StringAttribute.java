package com.yandex.metrica.profile;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.om;
import com.yandex.metrica.impl.ob.op;
import com.yandex.metrica.impl.ob.os;
import com.yandex.metrica.impl.ob.oy;
import com.yandex.metrica.impl.ob.oz;
import com.yandex.metrica.impl.ob.pb;
import com.yandex.metrica.impl.ob.pe;
import com.yandex.metrica.impl.ob.vo;
import com.yandex.metrica.impl.ob.vx;

public class StringAttribute {
    private final vo<String> a;
    private final os b;

    StringAttribute(@NonNull String key, @NonNull vo<String> trimmer, @NonNull vx<String> keyValidator, @NonNull om saver) {
        this.b = new os(key, keyValidator, saver);
        this.a = trimmer;
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValue(@NonNull String value) {
        return new UserProfileUpdate<>(new pb(this.b.a(), value, this.a, this.b.c(), new op(this.b.b())));
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValueIfUndefined(@NonNull String value) {
        return new UserProfileUpdate<>(new pb(this.b.a(), value, this.a, this.b.c(), new oz(this.b.b())));
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValueReset() {
        return new UserProfileUpdate<>(new oy(0, this.b.a(), this.b.c(), this.b.b()));
    }
}
