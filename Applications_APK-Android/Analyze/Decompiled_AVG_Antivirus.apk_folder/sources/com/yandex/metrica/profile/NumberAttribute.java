package com.yandex.metrica.profile;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.om;
import com.yandex.metrica.impl.ob.op;
import com.yandex.metrica.impl.ob.os;
import com.yandex.metrica.impl.ob.ot;
import com.yandex.metrica.impl.ob.ou;
import com.yandex.metrica.impl.ob.ow;
import com.yandex.metrica.impl.ob.oy;
import com.yandex.metrica.impl.ob.oz;
import com.yandex.metrica.impl.ob.pe;
import com.yandex.metrica.impl.ob.vg;
import com.yandex.metrica.impl.ob.vx;

public final class NumberAttribute {
    private final os a;

    NumberAttribute(@NonNull String key, @NonNull vx<String> keyValidator, @NonNull om saver) {
        this.a = new os(key, keyValidator, saver);
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValue(double value) {
        return new UserProfileUpdate<>(new ow(this.a.a(), value, new ot(), new op(new ou(new vg(100)))));
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValueIfUndefined(double value) {
        return new UserProfileUpdate<>(new ow(this.a.a(), value, new ot(), new oz(new ou(new vg(100)))));
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValueReset() {
        return new UserProfileUpdate<>(new oy(1, this.a.a(), new ot(), new ou(new vg(100))));
    }
}
