package com.yandex.metrica.profile;

import com.yandex.metrica.impl.ob.pa;
import com.yandex.metrica.impl.ob.vq;

public class NotificationsEnabledAttribute extends BooleanAttribute {
    NotificationsEnabledAttribute() {
        super("appmetrica_notifications_enabled", new vq(), new pa());
    }
}
