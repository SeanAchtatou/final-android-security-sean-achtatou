package com.yandex.metrica.profile;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pe;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class UserProfile {
    @NonNull
    private final List<UserProfileUpdate<? extends pe>> a;

    private UserProfile(@NonNull List<UserProfileUpdate<? extends pe>> userProfileUpdates) {
        this.a = Collections.unmodifiableList(userProfileUpdates);
    }

    @NonNull
    public List<UserProfileUpdate<? extends pe>> getUserProfileUpdates() {
        return this.a;
    }

    @NonNull
    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private final LinkedList<UserProfileUpdate<? extends pe>> a = new LinkedList<>();

        Builder() {
        }

        public Builder apply(@NonNull UserProfileUpdate<? extends pe> userProfileUpdate) {
            this.a.add(userProfileUpdate);
            return this;
        }

        @NonNull
        public UserProfile build() {
            return new UserProfile(this.a);
        }
    }
}
