package com.yandex.metrica.profile;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.om;
import com.yandex.metrica.impl.ob.or;
import com.yandex.metrica.impl.ob.os;
import com.yandex.metrica.impl.ob.pe;
import com.yandex.metrica.impl.ob.vx;

public final class CounterAttribute {
    private final os a;

    CounterAttribute(@NonNull String key, @NonNull vx<String> keyValidator, @NonNull om saver) {
        this.a = new os(key, keyValidator, saver);
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withDelta(double value) {
        return new UserProfileUpdate<>(new or(this.a.a(), value));
    }
}
