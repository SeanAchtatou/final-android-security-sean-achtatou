package com.yandex.metrica.profile;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.ot;
import com.yandex.metrica.impl.ob.ou;
import com.yandex.metrica.impl.ob.vg;
import com.yandex.metrica.impl.ob.vm;

public class Attribute {
    @NonNull
    public static StringAttribute customString(@NonNull String key) {
        return new StringAttribute(key, new vm(200, "String attribute \"" + key + "\""), new ot(), new ou(new vg(100)));
    }

    @NonNull
    public static NumberAttribute customNumber(@NonNull String key) {
        return new NumberAttribute(key, new ot(), new ou(new vg(100)));
    }

    @NonNull
    public static BooleanAttribute customBoolean(@NonNull String key) {
        return new BooleanAttribute(key, new ot(), new ou(new vg(100)));
    }

    @NonNull
    public static CounterAttribute customCounter(@NonNull String key) {
        return new CounterAttribute(key, new ot(), new ou(new vg(100)));
    }

    @NonNull
    public static GenderAttribute gender() {
        return new GenderAttribute();
    }

    @NonNull
    public static BirthDateAttribute birthDate() {
        return new BirthDateAttribute();
    }

    @NonNull
    public static NotificationsEnabledAttribute notificationsEnabled() {
        return new NotificationsEnabledAttribute();
    }

    @NonNull
    public static NameAttribute name() {
        return new NameAttribute();
    }
}
