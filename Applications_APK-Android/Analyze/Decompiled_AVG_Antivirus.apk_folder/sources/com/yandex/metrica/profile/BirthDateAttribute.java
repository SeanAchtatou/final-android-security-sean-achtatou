package com.yandex.metrica.profile;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.on;
import com.yandex.metrica.impl.ob.op;
import com.yandex.metrica.impl.ob.os;
import com.yandex.metrica.impl.ob.oy;
import com.yandex.metrica.impl.ob.oz;
import com.yandex.metrica.impl.ob.pa;
import com.yandex.metrica.impl.ob.pb;
import com.yandex.metrica.impl.ob.pe;
import com.yandex.metrica.impl.ob.vh;
import com.yandex.metrica.impl.ob.vq;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class BirthDateAttribute {
    private final os a = new os("appmetrica_birth_date", new vq(), new pa());

    BirthDateAttribute() {
    }

    public UserProfileUpdate<? extends pe> withBirthDate(int year) {
        return a(a(year), "yyyy", new op(this.a.b()));
    }

    public UserProfileUpdate<? extends pe> withBirthDateIfUndefined(int year) {
        return a(a(year), "yyyy", new oz(this.a.b()));
    }

    public UserProfileUpdate<? extends pe> withBirthDate(int year, int month) {
        return a(a(year, month), "yyyy-MM", new op(this.a.b()));
    }

    public UserProfileUpdate<? extends pe> withBirthDateIfUndefined(int year, int month) {
        return a(a(year, month), "yyyy-MM", new oz(this.a.b()));
    }

    public UserProfileUpdate<? extends pe> withBirthDate(int year, int month, int dayOfMonth) {
        return a(a(year, month, dayOfMonth), "yyyy-MM-dd", new op(this.a.b()));
    }

    public UserProfileUpdate<? extends pe> withBirthDateIfUndefined(int year, int month, int dayOfMonth) {
        return a(a(year, month, dayOfMonth), "yyyy-MM-dd", new oz(this.a.b()));
    }

    public UserProfileUpdate<? extends pe> withAge(int age) {
        return a(a(Calendar.getInstance(Locale.US).get(1) - age), "yyyy", new op(this.a.b()));
    }

    public UserProfileUpdate<? extends pe> withAgeIfUndefined(int age) {
        return a(a(Calendar.getInstance(Locale.US).get(1) - age), "yyyy", new oz(this.a.b()));
    }

    public UserProfileUpdate<? extends pe> withBirthDate(@NonNull Calendar date) {
        return a(date, "yyyy-MM-dd", new op(this.a.b()));
    }

    public UserProfileUpdate<? extends pe> withBirthDateIfUndefined(@NonNull Calendar date) {
        return a(date, "yyyy-MM-dd", new oz(this.a.b()));
    }

    public UserProfileUpdate<? extends pe> withValueReset() {
        return new UserProfileUpdate<>(new oy(0, this.a.a(), new vq(), new pa()));
    }

    private Calendar a(int i) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1, i);
        return gregorianCalendar;
    }

    private Calendar a(int i, int i2) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1, i);
        gregorianCalendar.set(2, i2 - 1);
        gregorianCalendar.set(5, 1);
        return gregorianCalendar;
    }

    private Calendar a(int i, int i2, int i3) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1, i);
        gregorianCalendar.set(2, i2 - 1);
        gregorianCalendar.set(5, i3);
        return gregorianCalendar;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @SuppressLint({"SimpleDateFormat"})
    public UserProfileUpdate<? extends pe> a(@NonNull Calendar calendar, @NonNull String str, @NonNull on onVar) {
        return new UserProfileUpdate<>(new pb(this.a.a(), new SimpleDateFormat(str).format(calendar.getTime()), new vh(), new vq(), onVar));
    }
}
