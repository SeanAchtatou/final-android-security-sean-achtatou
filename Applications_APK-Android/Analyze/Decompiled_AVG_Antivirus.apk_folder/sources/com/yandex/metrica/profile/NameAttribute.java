package com.yandex.metrica.profile;

import com.yandex.metrica.impl.ob.pa;
import com.yandex.metrica.impl.ob.vm;
import com.yandex.metrica.impl.ob.vq;

public class NameAttribute extends StringAttribute {
    NameAttribute() {
        super("appmetrica_name", new vm(100, "Name attribute"), new vq(), new pa());
    }
}
