package com.yandex.metrica.profile;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pe;

public class UserProfileUpdate<T extends pe> {
    @NonNull
    final T a;

    UserProfileUpdate(@NonNull T userProfileUpdatePatcher) {
        this.a = userProfileUpdatePatcher;
    }

    @NonNull
    public T getUserProfileUpdatePatcher() {
        return this.a;
    }
}
