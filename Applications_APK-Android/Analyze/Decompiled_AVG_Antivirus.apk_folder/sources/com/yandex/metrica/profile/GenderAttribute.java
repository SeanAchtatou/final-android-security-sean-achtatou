package com.yandex.metrica.profile;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.op;
import com.yandex.metrica.impl.ob.os;
import com.yandex.metrica.impl.ob.oy;
import com.yandex.metrica.impl.ob.oz;
import com.yandex.metrica.impl.ob.pa;
import com.yandex.metrica.impl.ob.pb;
import com.yandex.metrica.impl.ob.pe;
import com.yandex.metrica.impl.ob.vh;
import com.yandex.metrica.impl.ob.vq;

public class GenderAttribute {
    private final os a = new os("appmetrica_gender", new vq(), new pa());

    GenderAttribute() {
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValue(@NonNull Gender value) {
        return new UserProfileUpdate<>(new pb(this.a.a(), value.getStringValue(), new vh(), this.a.c(), new op(this.a.b())));
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValueIfUndefined(@NonNull Gender value) {
        return new UserProfileUpdate<>(new pb(this.a.a(), value.getStringValue(), new vh(), this.a.c(), new oz(this.a.b())));
    }

    @NonNull
    public UserProfileUpdate<? extends pe> withValueReset() {
        return new UserProfileUpdate<>(new oy(0, this.a.a(), this.a.c(), this.a.b()));
    }

    public enum Gender {
        MALE("M"),
        FEMALE("F"),
        OTHER("O");
        
        @NonNull
        private final String mStringValue;

        private Gender(String stringValue) {
            this.mStringValue = stringValue;
        }

        @NonNull
        public String getStringValue() {
            return this.mStringValue;
        }
    }
}
