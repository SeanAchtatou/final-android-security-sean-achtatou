package com.yandex.metrica;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.vt;
import com.yandex.metrica.impl.ob.vx;
import com.yandex.metrica.impl.ob.vy;

public class ReporterConfig {
    @NonNull
    public final String apiKey;
    @Nullable
    public final Boolean logs;
    @Nullable
    public final Integer sessionTimeout;
    @Nullable
    public final Boolean statisticsSending;

    ReporterConfig(@NonNull Builder builder) {
        this.apiKey = builder.b;
        this.sessionTimeout = builder.c;
        this.logs = builder.d;
        this.statisticsSending = builder.e;
    }

    ReporterConfig(@NonNull ReporterConfig config) {
        this.apiKey = config.apiKey;
        this.sessionTimeout = config.sessionTimeout;
        this.logs = config.logs;
        this.statisticsSending = config.statisticsSending;
    }

    @NonNull
    public static Builder newConfigBuilder(@NonNull String apiKey2) {
        return new Builder(apiKey2);
    }

    public static class Builder {
        private static final vx<String> a = new vt(new vy());
        /* access modifiers changed from: private */
        public final String b;
        /* access modifiers changed from: private */
        @Nullable
        public Integer c;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean d;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean e;

        Builder(@NonNull String apiKey) {
            a.a(apiKey);
            this.b = apiKey;
        }

        @NonNull
        public Builder withSessionTimeout(int sessionTimeout) {
            this.c = Integer.valueOf(sessionTimeout);
            return this;
        }

        @NonNull
        public Builder withLogs() {
            this.d = true;
            return this;
        }

        @NonNull
        public Builder withStatisticsSending(boolean enabled) {
            this.e = Boolean.valueOf(enabled);
            return this;
        }

        @NonNull
        public ReporterConfig build() {
            return new ReporterConfig(this);
        }
    }
}
