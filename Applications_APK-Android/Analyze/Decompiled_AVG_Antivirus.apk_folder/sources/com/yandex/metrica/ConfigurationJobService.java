package com.yandex.metrica;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.util.SparseArray;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.af;
import com.yandex.metrica.impl.ob.ia;
import com.yandex.metrica.impl.ob.ig;
import com.yandex.metrica.impl.ob.ik;
import com.yandex.metrica.impl.ob.il;
import com.yandex.metrica.impl.ob.im;
import com.yandex.metrica.impl.ob.in;
import com.yandex.metrica.impl.ob.io;
import java.util.Locale;

@TargetApi(26)
public class ConfigurationJobService extends JobService {
    @NonNull
    SparseArray<im> a = new SparseArray<>();
    private ig b;
    @Nullable
    private String c;

    public void onCreate() {
        super.onCreate();
        af.a(getApplicationContext());
        Context applicationContext = getApplicationContext();
        this.c = String.format(Locale.US, "[ConfigurationJobService:%s]", applicationContext.getPackageName());
        this.b = new ig();
        ik ikVar = new ik(getApplicationContext(), this.b.a(), new ia(applicationContext));
        this.a.append(1512302345, new in(getApplicationContext(), ikVar));
        this.a.append(1512302346, new io(getApplicationContext(), ikVar));
    }

    public boolean onStartJob(@Nullable final JobParameters params) {
        if (params != null) {
            try {
                im imVar = this.a.get(params.getJobId());
                if (imVar != null) {
                    this.b.a(imVar, params.getTransientExtras(), new il() {
                        public void a() {
                            try {
                                ConfigurationJobService.this.jobFinished(params, false);
                            } catch (Throwable th) {
                            }
                        }
                    });
                    return true;
                }
            } catch (Throwable th) {
            }
        }
        return false;
    }

    public boolean onStopJob(@Nullable JobParameters params) {
        return false;
    }
}
