package com.yandex.metrica;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.cj;
import com.yandex.metrica.impl.ob.uv;
import java.util.HashSet;
import java.util.Set;

public class a {
    @NonNull
    private final uv a;
    private long b;
    private Set<C0001a> c;
    private final Runnable d;
    private boolean e;

    /* renamed from: com.yandex.metrica.a$a  reason: collision with other inner class name */
    public interface C0001a {
        void a();

        void b();
    }

    public a(long j) {
        this(j, cj.l().b());
    }

    a(long j, @NonNull uv uvVar) {
        this.c = new HashSet();
        this.d = new Runnable() {
            public void run() {
                a.this.d();
            }
        };
        this.e = true;
        this.a = uvVar;
        this.b = j;
    }

    public void a() {
        if (this.e) {
            this.e = false;
            this.a.b(this.d);
            c();
        }
    }

    public void b() {
        if (!this.e) {
            this.e = true;
            this.a.a(this.d, this.b);
        }
    }

    private void c() {
        for (C0001a a2 : new HashSet(this.c)) {
            a2.a();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        for (C0001a b2 : new HashSet(this.c)) {
            b2.b();
        }
    }
}
