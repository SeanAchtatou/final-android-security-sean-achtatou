package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.ConfigurationJobService;

@TargetApi(26)
public class ia implements ib, ie {
    @NonNull
    private final Context a;
    /* access modifiers changed from: private */
    @Nullable
    public final JobScheduler b;

    public ia(@NonNull Context context) {
        this(context, (JobScheduler) context.getSystemService("jobscheduler"));
    }

    public void a(long j, boolean z) {
        final JobInfo.Builder minimumLatency = new JobInfo.Builder(1512302345, new ComponentName(this.a.getPackageName(), ConfigurationJobService.class.getName())).setMinimumLatency(j);
        if (z) {
            minimumLatency.setOverrideDeadline(j);
        }
        cg.a(new Runnable() {
            public void run() {
                ia.this.b.schedule(minimumLatency.build());
            }
        }, this.b, "scheduling wakeup in [ConfigurationJobServiceController]", "JobScheduler");
    }

    public void a() {
        cg.a(new Runnable() {
            public void run() {
                ia.this.b.cancel(1512302345);
            }
        }, this.b, "cancelling scheduled wakeup in [ConfigurationJobServiceController]", "JobScheduler");
    }

    public void a(@NonNull Bundle bundle) {
        final JobInfo build = new JobInfo.Builder(1512302346, new ComponentName(this.a.getPackageName(), ConfigurationJobService.class.getName())).setTransientExtras(bundle).setOverrideDeadline(10).build();
        cg.a(new Runnable() {
            public void run() {
                ia.this.b.schedule(build);
            }
        }, this.b, "launching [ConfigurationJobServiceController] command", "JobScheduler");
    }

    @VisibleForTesting
    ia(@NonNull Context context, @Nullable JobScheduler jobScheduler) {
        this.a = context;
        this.b = jobScheduler;
    }
}
