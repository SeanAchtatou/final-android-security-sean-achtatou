package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.io.UnsupportedEncodingException;

public class vl extends ve<String> {
    @VisibleForTesting(otherwise = 3)
    public /* bridge */ /* synthetic */ int a() {
        return super.a();
    }

    @VisibleForTesting(otherwise = 3)
    @NonNull
    public /* bridge */ /* synthetic */ String b() {
        return super.b();
    }

    public vl(int i, @NonNull String str) {
        this(i, str, ti.a());
    }

    public vl(int i, @NonNull String str, @NonNull tp tpVar) {
        super(i, str, tpVar);
    }

    @Nullable
    public String a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        try {
            byte[] bytes = str.getBytes("UTF-8");
            if (bytes.length <= a()) {
                return str;
            }
            String str2 = new String(bytes, 0, a(), "UTF-8");
            try {
                if (this.a.c()) {
                    this.a.b("\"%s\" %s exceeded limit of %d bytes", b(), str, Integer.valueOf(a()));
                }
                return str2;
            } catch (UnsupportedEncodingException e) {
                return str2;
            }
        } catch (UnsupportedEncodingException e2) {
            return str;
        }
    }
}
