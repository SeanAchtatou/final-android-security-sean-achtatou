package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.WorkerThread;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class qe {
    private static final EnumSet<b> a = EnumSet.of(b.HAS_FROM_PLAY_SERVICES, b.HAS_FROM_RECEIVER_ONLY, b.RECEIVER);
    private static final EnumSet<b> b = EnumSet.of(b.HAS_FROM_PLAY_SERVICES, b.HAS_FROM_RECEIVER_ONLY);
    private final Set<a> c;
    @Nullable
    private qf d;
    @Nullable
    private qf e;
    private boolean f;
    @NonNull
    private final kj g;
    private b h;

    public interface a {
        boolean a(@NonNull qf qfVar);
    }

    private enum b {
        EMPTY,
        RECEIVER,
        WAIT_FOR_RECEIVER_ONLY,
        HAS_FROM_PLAY_SERVICES,
        HAS_FROM_RECEIVER_ONLY
    }

    @WorkerThread
    public qe(@NonNull Context context) {
        this(new kj(jo.a(context).c()));
    }

    @VisibleForTesting
    qe(@NonNull kj kjVar) {
        this.c = new HashSet();
        this.h = b.EMPTY;
        this.g = kjVar;
        this.f = this.g.d();
        if (!this.f) {
            String b2 = this.g.b();
            if (!TextUtils.isEmpty(b2)) {
                this.d = new qf(b2, 0, 0);
            }
            this.e = this.g.c();
            this.h = b.values()[this.g.d(0)];
        }
    }

    public synchronized void a(@Nullable qf qfVar) {
        if (!b.contains(this.h)) {
            this.e = qfVar;
            this.g.a(qfVar).n();
            a(b(qfVar));
        }
    }

    public synchronized void a(@Nullable String str) {
        if (!a.contains(this.h) && !TextUtils.isEmpty(str)) {
            this.d = new qf(str, 0, 0);
            this.g.a(str).n();
            a(a());
        }
    }

    public synchronized void a(@NonNull a aVar) {
        if (!this.f) {
            this.c.add(aVar);
            b();
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.qe$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[b.values().length];

        static {
            try {
                a[b.EMPTY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[b.RECEIVER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[b.WAIT_FOR_RECEIVER_ONLY.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[b.HAS_FROM_PLAY_SERVICES.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[b.HAS_FROM_RECEIVER_ONLY.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    private b b(qf qfVar) {
        int i = AnonymousClass1.a[this.h.ordinal()];
        if (i == 1) {
            return qfVar == null ? b.WAIT_FOR_RECEIVER_ONLY : b.HAS_FROM_PLAY_SERVICES;
        }
        if (i != 2) {
            return this.h;
        }
        return qfVar == null ? b.HAS_FROM_RECEIVER_ONLY : b.HAS_FROM_PLAY_SERVICES;
    }

    private b a() {
        int i = AnonymousClass1.a[this.h.ordinal()];
        if (i == 1) {
            return b.RECEIVER;
        }
        if (i != 3) {
            return this.h;
        }
        return b.HAS_FROM_RECEIVER_ONLY;
    }

    private void a(@NonNull b bVar) {
        if (bVar != this.h) {
            this.h = bVar;
            this.g.e(this.h.ordinal()).n();
            b();
        }
    }

    private void b() {
        int i = AnonymousClass1.a[this.h.ordinal()];
        if (i == 4) {
            c(this.e);
        } else if (i == 5) {
            c(this.d);
        }
    }

    private synchronized void c(@Nullable qf qfVar) {
        if (qfVar != null) {
            if (!this.c.isEmpty() && !this.f) {
                boolean z = false;
                for (a a2 : this.c) {
                    if (a2.a(qfVar)) {
                        z = true;
                    }
                }
                if (z) {
                    c();
                    this.c.clear();
                }
            }
        }
    }

    private void c() {
        this.f = true;
        this.g.e().f().n();
    }
}
