package com.yandex.metrica.impl.ac;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.GpsHelper;
import com.yandex.metrica.impl.ob.af;
import com.yandex.metrica.impl.ob.cj;
import com.yandex.metrica.impl.ob.cn;
import com.yandex.metrica.impl.ob.cq;
import com.yandex.metrica.impl.ob.cr;
import com.yandex.metrica.impl.ob.cs;
import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.sc;
import com.yandex.metrica.impl.ob.uv;
import com.yandex.metrica.impl.ob.vp;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;

public class GoogleAdvertisingIdGetter {
    /* access modifiers changed from: private */
    public volatile String a;
    /* access modifiers changed from: private */
    public volatile Boolean b;
    /* access modifiers changed from: private */
    public final Object c;
    private volatile FutureTask<Pair<String, Boolean>> d;
    /* access modifiers changed from: private */
    public sc e;
    @NonNull
    private final e f;
    @Nullable
    private Context g;
    @NonNull
    private final vp h;
    @NonNull
    private uv i;

    private static class a {
        @SuppressLint({"StaticFieldLeak"})
        static final GoogleAdvertisingIdGetter a = new GoogleAdvertisingIdGetter(new b(), cj.l().b());
    }

    interface e {
        boolean a(@Nullable sc scVar);
    }

    private static class f {
        @SuppressLint({"StaticFieldLeak"})
        static final GoogleAdvertisingIdGetter a = new GoogleAdvertisingIdGetter(new g(), af.a().j().i());
    }

    private interface h<T> {
        T b(Future<Pair<String, Boolean>> future) throws InterruptedException, ExecutionException;
    }

    public static class c {
        public final String a;
        public final Boolean b;

        public c(String str, Boolean bool) {
            this.a = str;
            this.b = bool;
        }
    }

    static class b implements e {
        b() {
        }

        public boolean a(@Nullable sc scVar) {
            return true;
        }
    }

    static class g implements e {
        g() {
        }

        public boolean a(@Nullable sc scVar) {
            return scVar != null && (scVar.o.g || !scVar.u);
        }
    }

    private GoogleAdvertisingIdGetter(@NonNull e restrictionsProvider, @NonNull uv executor) {
        this.a = null;
        this.b = null;
        this.c = new Object();
        this.f = restrictionsProvider;
        this.h = new vp();
        this.i = executor;
        cn.a().a(this, cx.class, cr.a(new cq<cx>() {
            public void a(cx cxVar) {
                synchronized (GoogleAdvertisingIdGetter.this.c) {
                    sc unused = GoogleAdvertisingIdGetter.this.e = cxVar.b;
                }
            }
        }).a());
    }

    public static GoogleAdvertisingIdGetter a() {
        return f.a;
    }

    public static GoogleAdvertisingIdGetter b() {
        return a.a;
    }

    public void a(@NonNull Context context) {
        this.g = context.getApplicationContext();
    }

    public void b(@NonNull final Context context) {
        this.g = context.getApplicationContext();
        if (this.d == null) {
            synchronized (this.c) {
                if (this.d == null && this.f.a(this.e)) {
                    this.d = new FutureTask<>(new Callable<Pair<String, Boolean>>() {
                        /* renamed from: a */
                        public Pair<String, Boolean> call() {
                            Context applicationContext = context.getApplicationContext();
                            if (GoogleAdvertisingIdGetter.this.d(applicationContext)) {
                                GoogleAdvertisingIdGetter.this.e(applicationContext);
                            }
                            if (!GoogleAdvertisingIdGetter.this.e()) {
                                GoogleAdvertisingIdGetter.this.f(applicationContext);
                            }
                            return new Pair<>(GoogleAdvertisingIdGetter.this.a, GoogleAdvertisingIdGetter.this.b);
                        }
                    });
                    this.i.a(this.d);
                }
            }
        }
    }

    public void a(@NonNull Context context, @NonNull sc scVar) {
        this.e = scVar;
        b(context);
    }

    private void a(String str) {
        cn.a().b(new cs(str));
        this.a = str;
    }

    private void a(Boolean bool) {
        this.b = bool;
    }

    private <T> T a(Context context, h hVar) {
        b(context);
        try {
            return hVar.b(this.d);
        } catch (InterruptedException | ExecutionException e2) {
            return null;
        }
    }

    public c c(Context context) {
        if (this.f.a(this.e)) {
            return (c) a(context, new h<c>() {
                /* renamed from: a */
                public c b(Future<Pair<String, Boolean>> future) throws InterruptedException, ExecutionException {
                    Pair pair = future.get();
                    return new c((String) pair.first, (Boolean) pair.second);
                }
            });
        }
        return null;
    }

    public String c() {
        f();
        return this.a;
    }

    public Boolean d() {
        f();
        return this.b;
    }

    private void f() {
        if (this.g != null && !e()) {
            c(this.g);
        }
    }

    public synchronized boolean e() {
        return (this.a == null || this.b == null) ? false : true;
    }

    /* access modifiers changed from: private */
    public boolean d(Context context) {
        try {
            return Class.forName("com.google.android.gms.common.GooglePlayServicesUtil").getMethod("isGooglePlayServicesAvailable", Context.class).invoke(null, context).equals(0);
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void e(Context context) {
        try {
            Object invoke = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", Context.class).invoke(null, context);
            Class<?> cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
            String str = (String) cls.getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
            Boolean bool = (Boolean) cls.getMethod(GpsHelper.IS_LIMIT_AD_TRACKING_ENABLED_KEY, new Class[0]).invoke(invoke, new Object[0]);
            synchronized (this) {
                a(str);
                a(bool);
            }
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public void f(Context context) {
        d dVar = new d();
        Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
        intent.setPackage("com.google.android.gms");
        if (this.h.c(context, intent, 0) != null && context.bindService(intent, dVar, 1)) {
            try {
                GoogleAdvertisingInfo create = GoogleAdvertisingInfo.GoogleAdvertisingInfoBinder.create(dVar.a());
                String id = create.getId();
                Boolean valueOf = Boolean.valueOf(create.getEnabled(true));
                synchronized (this) {
                    a(id);
                    a(valueOf);
                }
            } catch (Throwable th) {
                context.unbindService(dVar);
                throw th;
            }
            context.unbindService(dVar);
        }
    }

    private interface GoogleAdvertisingInfo extends IInterface {
        boolean getEnabled(boolean z) throws RemoteException;

        String getId() throws RemoteException;

        public static abstract class GoogleAdvertisingInfoBinder extends Binder implements GoogleAdvertisingInfo {
            public static GoogleAdvertisingInfo create(IBinder binder) {
                if (binder == null) {
                    return null;
                }
                IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                if (queryLocalInterface == null || !(queryLocalInterface instanceof GoogleAdvertisingInfo)) {
                    return new GoogleAdvertisingInfoImplementation(binder);
                }
                return (GoogleAdvertisingInfo) queryLocalInterface;
            }

            public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
                if (code == 1) {
                    data.enforceInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    String id = getId();
                    reply.writeNoException();
                    reply.writeString(id);
                    return true;
                } else if (code != 2) {
                    return super.onTransact(code, data, reply, flags);
                } else {
                    data.enforceInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                    boolean enabled = getEnabled(data.readInt() != 0);
                    reply.writeNoException();
                    reply.writeInt(enabled ? 1 : 0);
                    return true;
                }
            }

            private static class GoogleAdvertisingInfoImplementation implements GoogleAdvertisingInfo {
                private IBinder a;

                GoogleAdvertisingInfoImplementation(IBinder binder) {
                    this.a = binder;
                }

                public IBinder asBinder() {
                    return this.a;
                }

                public String getId() throws RemoteException {
                    Parcel obtain = Parcel.obtain();
                    Parcel obtain2 = Parcel.obtain();
                    try {
                        obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                        this.a.transact(1, obtain, obtain2, 0);
                        obtain2.readException();
                        return obtain2.readString();
                    } finally {
                        obtain2.recycle();
                        obtain.recycle();
                    }
                }

                public boolean getEnabled(boolean paramBoolean) throws RemoteException {
                    Parcel obtain = Parcel.obtain();
                    Parcel obtain2 = Parcel.obtain();
                    try {
                        obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                        boolean z = true;
                        obtain.writeInt(paramBoolean ? 1 : 0);
                        this.a.transact(2, obtain, obtain2, 0);
                        obtain2.readException();
                        if (obtain2.readInt() == 0) {
                            z = false;
                        }
                        return z;
                    } finally {
                        obtain2.recycle();
                        obtain.recycle();
                    }
                }
            }
        }
    }

    private class d implements ServiceConnection {
        private boolean b;
        private final BlockingQueue<IBinder> c;

        private d() {
            this.b = false;
            this.c = new LinkedBlockingQueue();
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            try {
                this.c.put(service);
            } catch (InterruptedException e) {
            }
        }

        public void onServiceDisconnected(ComponentName name) {
        }

        public IBinder a() throws InterruptedException {
            if (!this.b) {
                this.b = true;
                return this.c.take();
            }
            throw new IllegalStateException();
        }
    }
}
