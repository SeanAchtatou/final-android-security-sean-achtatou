package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.Map;

public abstract class oc {
    private static final oj d = new oj("UNDEFINED_");
    protected oj a;
    protected final String b;
    protected final SharedPreferences c;
    private final Map<String, Object> e = new HashMap();
    private boolean f = false;

    /* access modifiers changed from: protected */
    public abstract String f();

    public oc(Context context, String str) {
        this.b = str;
        this.c = a(context);
        this.a = new oj(d.a(), this.b);
    }

    private SharedPreferences a(Context context) {
        return ok.a(context, f());
    }

    /* access modifiers changed from: protected */
    public <T extends oc> T a(String str, Object obj) {
        synchronized (this) {
            if (obj != null) {
                this.e.put(str, obj);
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public <T extends oc> T h(String str) {
        synchronized (this) {
            this.e.put(str, this);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public <T extends oc> T h() {
        synchronized (this) {
            this.f = true;
            this.e.clear();
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public String i() {
        return this.b;
    }

    public void j() {
        synchronized (this) {
            a();
            this.e.clear();
            this.f = false;
        }
    }

    private void a() {
        SharedPreferences.Editor edit = this.c.edit();
        if (this.f) {
            edit.clear();
            a(edit);
            return;
        }
        for (Map.Entry next : this.e.entrySet()) {
            String str = (String) next.getKey();
            Object value = next.getValue();
            if (value == this) {
                edit.remove(str);
            } else if (value instanceof String) {
                edit.putString(str, (String) value);
            } else if (value instanceof Long) {
                edit.putLong(str, ((Long) value).longValue());
            } else if (value instanceof Integer) {
                edit.putInt(str, ((Integer) value).intValue());
            } else if (value instanceof Boolean) {
                edit.putBoolean(str, ((Boolean) value).booleanValue());
            } else if (value != null) {
                throw new UnsupportedOperationException();
            }
        }
        a(edit);
    }

    private void a(SharedPreferences.Editor editor) {
        editor.apply();
    }
}
