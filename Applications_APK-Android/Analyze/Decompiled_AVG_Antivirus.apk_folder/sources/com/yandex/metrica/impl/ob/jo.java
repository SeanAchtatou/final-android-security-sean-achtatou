package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import androidx.annotation.NonNull;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class jo {
    @SuppressLint({"StaticFieldLeak"})
    private static volatile jo a;
    private final Map<String, jn> b;
    private final Map<String, jq> c;
    private final Map<String, jj> d;
    @NonNull
    private final jl e;
    private final Context f;
    private jn g;
    private jj h;
    private jq i;
    private jq j;
    private jq k;
    private js l;
    private jr m;
    private jt n;

    public static jo a(Context context) {
        if (a == null) {
            synchronized (jo.class) {
                if (a == null) {
                    a = new jo(context.getApplicationContext());
                }
            }
        }
        return a;
    }

    public jo(Context context) {
        this(context, kb.a());
    }

    public jo(Context context, @NonNull jl jlVar) {
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.f = context;
        this.e = jlVar;
    }

    public synchronized jn a(df dfVar) {
        jn jnVar;
        String d2 = d(dfVar);
        jnVar = this.b.get(d2);
        if (jnVar == null) {
            jnVar = a(d2, this.e.a());
            this.b.put(d2, jnVar);
        }
        return jnVar;
    }

    public synchronized jn a() {
        if (this.g == null) {
            this.g = a("metrica_data.db", this.e.b());
        }
        return this.g;
    }

    public synchronized jq b(df dfVar) {
        jq jqVar;
        String dfVar2 = dfVar.toString();
        jqVar = this.c.get(dfVar2);
        if (jqVar == null) {
            jqVar = new jq(a(dfVar), "preferences");
            this.c.put(dfVar2, jqVar);
        }
        return jqVar;
    }

    @NonNull
    public synchronized jj c(@NonNull df dfVar) {
        jj jjVar;
        String dfVar2 = dfVar.toString();
        jjVar = this.d.get(dfVar2);
        if (jjVar == null) {
            jjVar = new jj(new ka(a(dfVar)), "binary_data");
            this.d.put(dfVar2, jjVar);
        }
        return jjVar;
    }

    public synchronized jj b() {
        if (this.h == null) {
            this.h = new jj(new ka(a()), "binary_data");
        }
        return this.h;
    }

    public synchronized jq c() {
        if (this.i == null) {
            this.i = new jq(a(), "preferences");
        }
        return this.i;
    }

    public synchronized jt d() {
        if (this.n == null) {
            this.n = new jt(a(), "permissions");
        }
        return this.n;
    }

    public synchronized jq e() {
        if (this.j == null) {
            this.j = new jq(a(), "startup");
        }
        return this.j;
    }

    public synchronized jq f() {
        if (this.k == null) {
            this.k = new jq("preferences", new jz(this.f, a("metrica_client_data.db")));
        }
        return this.k;
    }

    public synchronized js g() {
        if (this.l == null) {
            this.l = new js(this.f, a());
        }
        return this.l;
    }

    public synchronized jr h() {
        if (this.m == null) {
            this.m = new jr(this.f, a());
        }
        return this.m;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public jn a(String str, ju juVar) {
        return new jn(this.f, a(str), juVar);
    }

    private String a(String str) {
        if (cg.a(21)) {
            return b(str);
        }
        return str;
    }

    @TargetApi(21)
    private String b(String str) {
        try {
            File noBackupFilesDir = this.f.getNoBackupFilesDir();
            File file = new File(noBackupFilesDir, str);
            if (!file.exists()) {
                if (a(noBackupFilesDir, str)) {
                    a(noBackupFilesDir, str + "-journal");
                    a(noBackupFilesDir, str + "-shm");
                    a(noBackupFilesDir, str + "-wal");
                }
            }
            return file.getAbsolutePath();
        } catch (Throwable th) {
            return str;
        }
    }

    private boolean a(@NonNull File file, @NonNull String str) {
        File databasePath = this.f.getDatabasePath(str);
        if (databasePath == null || !databasePath.exists()) {
            return false;
        }
        return databasePath.renameTo(new File(file, str));
    }

    private static String d(df dfVar) {
        return "db_metrica_" + dfVar;
    }
}
