package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.Cdo;
import java.io.Closeable;

public class cf<C extends Cdo> implements Closeable {
    final Object a = new Object();
    @NonNull
    final bb b;
    boolean c = false;
    @NonNull
    private C d;
    @NonNull
    private final si e;

    public cf(@NonNull C c2, @NonNull si siVar, @NonNull bb bbVar) {
        this.d = c2;
        this.e = siVar;
        this.b = bbVar;
    }

    public void close() {
        synchronized (this.a) {
            if (!this.c) {
                a();
                if (this.b.isAlive()) {
                    this.b.a();
                }
                this.c = true;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
    }

    public void e() {
        synchronized (this.a) {
            if (!this.c) {
                f();
                a();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        synchronized (this.a) {
            if (!this.c) {
                c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.e.a(this.b);
    }

    @NonNull
    public C g() {
        return this.d;
    }
}
