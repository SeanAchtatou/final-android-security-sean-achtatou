package com.yandex.metrica.impl.interact;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.bj;
import com.yandex.metrica.impl.ob.cn;
import com.yandex.metrica.impl.ob.cq;
import com.yandex.metrica.impl.ob.cr;
import com.yandex.metrica.impl.ob.cv;
import com.yandex.metrica.impl.ob.s;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class DeviceInfo {
    private static final Object a = new Object();
    private static volatile DeviceInfo b;
    public final String appPlatform;
    public final String deviceRootStatus;
    public final List<String> deviceRootStatusMarkers;
    public final String deviceType;
    public String locale;
    public final String manufacturer;
    public final String model;
    public final String osVersion;
    public final String platform;
    public final String platformDeviceId;
    public final float scaleFactor;
    public final int screenDpi;
    public final int screenHeight;
    public final int screenWidth;

    public static DeviceInfo getInstance(@NonNull Context context) {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new DeviceInfo(context, s.a(context));
                }
            }
        }
        return b;
    }

    @VisibleForTesting
    DeviceInfo(@NonNull Context context, @NonNull s deviceInfo) {
        this.platform = deviceInfo.a;
        this.appPlatform = deviceInfo.a;
        this.platformDeviceId = deviceInfo.a();
        this.manufacturer = deviceInfo.b;
        this.model = deviceInfo.c;
        this.osVersion = deviceInfo.d;
        this.screenWidth = deviceInfo.f.a;
        this.screenHeight = deviceInfo.f.b;
        this.screenDpi = deviceInfo.f.c;
        this.scaleFactor = deviceInfo.f.d;
        this.deviceType = deviceInfo.g;
        this.deviceRootStatus = deviceInfo.h;
        this.deviceRootStatusMarkers = new ArrayList(deviceInfo.i);
        this.locale = bj.a(context.getResources().getConfiguration().locale);
        cn.a().a(this, cv.class, cr.a(new cq<cv>() {
            public void a(cv cvVar) {
                DeviceInfo.this.locale = cvVar.a;
            }
        }).a());
    }
}
