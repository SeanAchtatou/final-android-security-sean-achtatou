package com.yandex.metrica.impl.ob;

import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ma;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class fw extends ft {
    @NonNull
    private final vp a;

    public fw(di diVar) {
        this(diVar, new vp());
    }

    @VisibleForTesting
    public fw(di diVar, @NonNull vp vpVar) {
        super(diVar);
        this.a = vpVar;
    }

    public boolean a(@NonNull t tVar) {
        di a2 = a();
        if (!a2.t().d() || !a2.s()) {
            return false;
        }
        kh x = a2.x();
        HashSet<mb> b = b();
        try {
            ArrayList<mb> c = c();
            if (ta.a(b, c)) {
                a2.l();
                return false;
            }
            JSONArray jSONArray = new JSONArray();
            Iterator<mb> it = c.iterator();
            while (it.hasNext()) {
                jSONArray.put(it.next().a());
            }
            a2.d().e(t.a(tVar, new JSONObject().put("features", jSONArray).toString()));
            x.b(jSONArray.toString());
            return false;
        } catch (JSONException e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Nullable
    public HashSet<mb> b() {
        String e = a().x().e();
        if (TextUtils.isEmpty(e)) {
            return null;
        }
        try {
            HashSet<mb> hashSet = new HashSet<>();
            JSONArray jSONArray = new JSONArray(e);
            for (int i = 0; i < jSONArray.length(); i++) {
                hashSet.add(new mb(jSONArray.getJSONObject(i)));
            }
            return hashSet;
        } catch (JSONException e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Nullable
    public ArrayList<mb> c() {
        try {
            di a2 = a();
            PackageInfo a3 = this.a.a(a2.j(), a2.j().getPackageName(), 16384);
            ArrayList<mb> arrayList = new ArrayList<>();
            ma a4 = ma.a.a();
            if (!(a3 == null || a3.reqFeatures == null)) {
                for (FeatureInfo b : a3.reqFeatures) {
                    arrayList.add(a4.b(b));
                }
            }
            return arrayList;
        } catch (Throwable th) {
            return null;
        }
    }
}
