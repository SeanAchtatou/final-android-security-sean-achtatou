package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter;
import java.util.Map;

public class qw implements qs<qq> {
    @NonNull
    private final qo a;

    public qw(@NonNull qo qoVar) {
        this.a = qoVar;
    }

    public void a(@NonNull Uri.Builder builder, @NonNull qq qqVar) {
        builder.path("analytics/startup");
        builder.appendQueryParameter(this.a.a("deviceid"), qqVar.r());
        builder.appendQueryParameter(this.a.a("deviceid2"), qqVar.s());
        a(builder, af.a().h(), qqVar);
        builder.appendQueryParameter(this.a.a("app_platform"), qqVar.l());
        builder.appendQueryParameter(this.a.a("protocol_version"), qqVar.h());
        builder.appendQueryParameter(this.a.a("analytics_sdk_version_name"), qqVar.i());
        builder.appendQueryParameter(this.a.a(UrlManager.Parameter.MODEL), qqVar.m());
        builder.appendQueryParameter(this.a.a("manufacturer"), qqVar.g());
        builder.appendQueryParameter(this.a.a("os_version"), qqVar.n());
        builder.appendQueryParameter(this.a.a("screen_width"), String.valueOf(qqVar.w()));
        builder.appendQueryParameter(this.a.a("screen_height"), String.valueOf(qqVar.x()));
        builder.appendQueryParameter(this.a.a("screen_dpi"), String.valueOf(qqVar.y()));
        builder.appendQueryParameter(this.a.a("scalefactor"), String.valueOf(qqVar.z()));
        builder.appendQueryParameter(this.a.a("locale"), qqVar.A());
        builder.appendQueryParameter(this.a.a("device_type"), qqVar.C());
        builder.appendQueryParameter(this.a.a("queries"), String.valueOf(1));
        builder.appendQueryParameter(this.a.a("query_hosts"), String.valueOf(2));
        builder.appendQueryParameter(this.a.a("features"), ce.b(this.a.a("easy_collecting"), this.a.a("package_info"), this.a.a("socket"), this.a.a("permissions_collecting"), this.a.a("features_collecting"), this.a.a("foreground_location_collection"), this.a.a("background_location_collection"), this.a.a("foreground_lbs_collection"), this.a.a("background_lbs_collection"), this.a.a("telephony_restricted_to_location_tracking"), this.a.a("android_id"), this.a.a("google_aid"), this.a.a("wifi_around"), this.a.a("wifi_connected"), this.a.a("own_macs"), this.a.a("cells_around"), this.a.a("sim_info"), this.a.a("sim_imei"), this.a.a("access_point"), this.a.a("sdk_list")));
        builder.appendQueryParameter(this.a.a("socket"), String.valueOf(1));
        builder.appendQueryParameter(this.a.a(UrlManager.Parameter.APP_ID), qqVar.d());
        builder.appendQueryParameter(this.a.a("foreground_location_collection"), String.valueOf(1));
        builder.appendQueryParameter(this.a.a("app_debuggable"), qqVar.E());
        builder.appendQueryParameter(this.a.a("sdk_list"), String.valueOf(1));
        builder.appendQueryParameter(this.a.a("background_location_collection"), String.valueOf(1));
        if (qqVar.b()) {
            String K = qqVar.K();
            if (!TextUtils.isEmpty(K)) {
                builder.appendQueryParameter(this.a.a("country_init"), K);
            }
        } else {
            builder.appendQueryParameter(this.a.a("detect_locale"), String.valueOf(1));
        }
        Map<String, String> G = qqVar.G();
        String H = qqVar.H();
        if (!cg.a((Map) G)) {
            builder.appendQueryParameter(this.a.a("distribution_customization"), String.valueOf(1));
            a(builder, "clids_set", tu.a(G));
            if (!TextUtils.isEmpty(H)) {
                builder.appendQueryParameter(this.a.a("install_referrer"), H);
            }
        }
        a(builder, "uuid", qqVar.t());
        builder.appendQueryParameter(this.a.a("time"), String.valueOf(1));
        builder.appendQueryParameter(this.a.a("requests"), String.valueOf(1));
        builder.appendQueryParameter(this.a.a("stat_sending"), String.valueOf(1));
        builder.appendQueryParameter(this.a.a("permissions"), String.valueOf(1));
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Uri.Builder builder, @NonNull cd cdVar, @NonNull qq qqVar) {
        GoogleAdvertisingIdGetter.c D = qqVar.D();
        if (cdVar.a()) {
            builder.appendQueryParameter(this.a.a("adv_id"), "");
        } else if (D == null || TextUtils.isEmpty(D.a)) {
            builder.appendQueryParameter(this.a.a("adv_id"), "");
        } else {
            builder.appendQueryParameter(this.a.a("adv_id"), D.a);
        }
    }

    private void a(Uri.Builder builder, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            builder.appendQueryParameter(this.a.a(str), str2);
        }
    }
}
