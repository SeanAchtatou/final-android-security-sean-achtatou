package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class hd {
    private final int a;
    @NonNull
    private final hh b;
    @Nullable
    private he c;

    public hd(@NonNull Context context, @NonNull df dfVar, int i) {
        this(new hh(context, dfVar), i);
    }

    @NonNull
    public ad a(@NonNull String str) {
        if (this.c == null) {
            b();
        }
        int b2 = b(str);
        if (this.c.c().contains(Integer.valueOf(b2))) {
            return ad.NON_FIRST_OCCURENCE;
        }
        ad adVar = this.c.b() ? ad.FIRST_OCCURRENCE : ad.UNKNOWN;
        if (this.c.d() < 1000) {
            this.c.b(b2);
        } else {
            this.c.a(false);
        }
        c();
        return adVar;
    }

    public void a() {
        if (this.c == null) {
            b();
        }
        this.c.a();
        this.c.a(true);
        c();
    }

    private void b() {
        this.c = this.b.a();
        int e = this.c.e();
        int i = this.a;
        if (e != i) {
            this.c.a(i);
            c();
        }
    }

    private void c() {
        this.b.a(this.c);
    }

    private int b(@NonNull String str) {
        return str.hashCode();
    }

    @VisibleForTesting
    hd(@NonNull hh hhVar, int i) {
        this.a = i;
        this.b = hhVar;
    }
}
