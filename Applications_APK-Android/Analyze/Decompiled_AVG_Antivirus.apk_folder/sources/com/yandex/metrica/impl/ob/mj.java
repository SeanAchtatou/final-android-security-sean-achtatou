package com.yandex.metrica.impl.ob;

public class mj {
    public final long a;
    public final long b;

    public mj(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    public String toString() {
        return "IntervalRange{minInterval=" + this.a + ", maxInterval=" + this.b + '}';
    }
}
