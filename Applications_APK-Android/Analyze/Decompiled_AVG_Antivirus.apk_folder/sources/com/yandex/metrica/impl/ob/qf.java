package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class qf {
    @NonNull
    public final String a;
    public final long b;
    public final long c;

    @Nullable
    public static qf a(@NonNull byte[] bArr) throws d {
        if (cg.a(bArr)) {
            return null;
        }
        return new qf(bArr);
    }

    private qf(@NonNull byte[] bArr) throws d {
        pi a2 = pi.a(bArr);
        this.a = a2.b;
        this.b = a2.d;
        this.c = a2.c;
    }

    public qf(@NonNull String str, long j, long j2) {
        this.a = str;
        this.b = j;
        this.c = j2;
    }

    public byte[] a() {
        pi piVar = new pi();
        piVar.b = this.a;
        piVar.d = this.b;
        piVar.c = this.c;
        return e.a(piVar);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        qf qfVar = (qf) o;
        if (this.b == qfVar.b && this.c == qfVar.c) {
            return this.a.equals(qfVar.a);
        }
        return false;
    }

    public int hashCode() {
        long j = this.b;
        long j2 = this.c;
        return (((this.a.hashCode() * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "ReferrerInfo{installReferrer='" + this.a + '\'' + ", referrerClickTimestampSeconds=" + this.b + ", installBeginTimestampSeconds=" + this.c + '}';
    }
}
