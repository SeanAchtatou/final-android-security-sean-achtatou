package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public class hr {
    private final kh a;

    public hr(@NonNull kh khVar) {
        this.a = khVar;
    }

    public long a() {
        long j = this.a.j();
        long j2 = 10000000000L;
        if (j >= 10000000000L) {
            j2 = 1 + j;
        }
        this.a.e(j2).n();
        return j2;
    }
}
