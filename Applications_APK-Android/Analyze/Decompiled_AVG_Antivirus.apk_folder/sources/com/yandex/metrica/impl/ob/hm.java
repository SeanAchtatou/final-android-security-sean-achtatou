package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONObject;

public class hm {
    private final di a;
    private final hu b;
    private final ho c;
    private long d;
    private long e;
    private AtomicLong f;
    private boolean g;
    private volatile a h;
    private long i;
    private long j;
    private tw k;

    hm(di diVar, hu huVar, ho hoVar) {
        this(diVar, huVar, hoVar, new tw());
    }

    hm(di diVar, hu huVar, ho hoVar, tw twVar) {
        this.a = diVar;
        this.b = huVar;
        this.c = hoVar;
        this.k = twVar;
        h();
    }

    private void h() {
        this.e = this.c.b(this.k.c());
        this.d = this.c.a(-1L);
        this.f = new AtomicLong(this.c.c(0));
        this.g = this.c.a(true);
        this.i = this.c.d(0);
        this.j = this.c.e(this.i - this.e);
    }

    /* access modifiers changed from: protected */
    public hw a() {
        return this.c.a();
    }

    /* access modifiers changed from: protected */
    public int b() {
        return this.c.a(this.a.h().S());
    }

    public long c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public long d() {
        return Math.max(this.i - TimeUnit.MILLISECONDS.toSeconds(this.e), this.j);
    }

    /* access modifiers changed from: package-private */
    public boolean a(long j2) {
        boolean z = this.d >= 0;
        boolean i2 = i();
        boolean z2 = !a(j2, this.k.c());
        if (!z || !i2 || !z2) {
            return false;
        }
        return true;
    }

    private boolean i() {
        a j2 = j();
        if (j2 != null) {
            return j2.a(this.a.h());
        }
        return false;
    }

    private long d(long j2) {
        return TimeUnit.MILLISECONDS.toSeconds(j2 - this.e);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean a(long j2, long j3) {
        long j4 = this.i;
        boolean z = TimeUnit.MILLISECONDS.toSeconds(j3) < j4;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(j2) - j4;
        long d2 = d(j2);
        if (z || seconds >= ((long) b()) || d2 >= hp.c) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public synchronized void e() {
        this.b.a();
        this.h = null;
    }

    /* access modifiers changed from: package-private */
    public void b(long j2) {
        hu huVar = this.b;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(j2);
        this.i = seconds;
        huVar.b(seconds).h();
    }

    /* access modifiers changed from: package-private */
    public long c(long j2) {
        hu huVar = this.b;
        long d2 = d(j2);
        this.j = d2;
        huVar.c(d2);
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public long f() {
        long andIncrement = this.f.getAndIncrement();
        this.b.a(this.f.get()).h();
        return andIncrement;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return this.g && c() > 0;
    }

    public void a(boolean z) {
        if (this.g != z) {
            this.g = z;
            this.b.a(this.g).h();
        }
    }

    private a j() {
        if (this.h == null) {
            synchronized (this) {
                if (this.h == null) {
                    try {
                        String asString = this.a.i().b(c(), a()).getAsString("report_request_parameters");
                        if (!TextUtils.isEmpty(asString)) {
                            this.h = new a(new JSONObject(asString));
                        }
                    } catch (Throwable th) {
                    }
                }
            }
        }
        return this.h;
    }

    public String toString() {
        return "Session{mId=" + this.d + ", mInitTime=" + this.e + ", mCurrentReportId=" + this.f + ", mSessionRequestParams=" + this.h + ", mSleepStartSeconds=" + this.i + '}';
    }

    static class a {
        private final String a;
        private final String b;
        private final String c;
        private final String d;
        private final String e;
        private final int f;
        private final int g;

        a(JSONObject jSONObject) {
            this.a = jSONObject.optString("analyticsSdkVersionName", null);
            this.b = jSONObject.optString("kitBuildNumber", null);
            this.c = jSONObject.optString("appVer", null);
            this.d = jSONObject.optString("appBuild", null);
            this.e = jSONObject.optString("osVer", null);
            this.f = jSONObject.optInt("osApiLev", -1);
            this.g = jSONObject.optInt("attribution_id", 0);
        }

        /* access modifiers changed from: package-private */
        public boolean a(qp qpVar) {
            return TextUtils.equals(qpVar.i(), this.a) && TextUtils.equals(qpVar.j(), this.b) && TextUtils.equals(qpVar.q(), this.c) && TextUtils.equals(qpVar.p(), this.d) && TextUtils.equals(qpVar.n(), this.e) && this.f == qpVar.o() && this.g == qpVar.V();
        }

        public String toString() {
            return "SessionRequestParams{mKitVersionName='" + this.a + '\'' + ", mKitBuildNumber='" + this.b + '\'' + ", mAppVersion='" + this.c + '\'' + ", mAppBuild='" + this.d + '\'' + ", mOsVersion='" + this.e + '\'' + ", mApiLevel=" + this.f + '}';
        }
    }
}
