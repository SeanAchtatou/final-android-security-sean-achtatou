package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public interface ky<S, P> {
    @NonNull
    S a(@NonNull Object obj);

    @NonNull
    P b(@NonNull S s);
}
