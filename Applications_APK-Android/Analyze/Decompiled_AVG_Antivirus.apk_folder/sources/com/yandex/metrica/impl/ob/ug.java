package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class ug implements ul {
    @NonNull
    private final uf a;

    ug(@NonNull Context context) {
        this(new ue(context));
    }

    ug(@NonNull ue ueVar) {
        this(new uf("AES/CBC/PKCS5Padding", ueVar.a(), ueVar.b()));
    }

    @VisibleForTesting
    ug(@NonNull uf ufVar) {
        this.a = ufVar;
    }

    @NonNull
    public uk a(@NonNull t tVar) {
        String e = tVar.e();
        String str = null;
        if (!TextUtils.isEmpty(e)) {
            try {
                byte[] a2 = this.a.a(e.getBytes("UTF-8"));
                if (a2 != null) {
                    str = Base64.encodeToString(a2, 0);
                }
            } catch (Throwable th) {
            }
        }
        return new uk(tVar.c(str), a());
    }

    @NonNull
    public byte[] a(@Nullable byte[] bArr) {
        byte[] bArr2 = new byte[0];
        if (bArr == null || bArr.length <= 0) {
            return bArr2;
        }
        try {
            return this.a.b(Base64.decode(bArr, 0));
        } catch (Throwable th) {
            return bArr2;
        }
    }

    @NonNull
    public un a() {
        return un.AES_VALUE_ENCRYPTION;
    }
}
