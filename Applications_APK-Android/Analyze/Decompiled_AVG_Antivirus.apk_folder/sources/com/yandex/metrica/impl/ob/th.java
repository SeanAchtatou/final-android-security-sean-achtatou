package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class th {

    public static class a extends JSONObject {
        public a() {
        }

        public a(String str) throws JSONException {
            super(str);
        }

        public String a(String str) {
            if (super.has(str)) {
                try {
                    return super.getString(str);
                } catch (Throwable th) {
                }
            }
            return "";
        }

        public String b(String str) {
            return super.has(str) ? a(str) : "";
        }

        public Object a(String str, Object obj) {
            try {
                return super.get(str);
            } catch (Throwable th) {
                return obj;
            }
        }

        public boolean c(String str) {
            try {
                return NULL != super.get(str);
            } catch (Throwable th) {
                return false;
            }
        }

        @Nullable
        public Long d(String str) {
            try {
                return Long.valueOf(getLong(str));
            } catch (JSONException e) {
                return null;
            }
        }

        @Nullable
        public Boolean e(String str) {
            try {
                return Boolean.valueOf(getBoolean(str));
            } catch (JSONException e) {
                return null;
            }
        }
    }

    @VisibleForTesting
    public static Object a(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            if (obj.getClass().isArray()) {
                int length = Array.getLength(obj);
                ArrayList arrayList = new ArrayList(length);
                for (int i = 0; i < length; i++) {
                    arrayList.add(a(Array.get(obj, i)));
                }
                return new JSONArray((Collection) arrayList);
            } else if (obj instanceof Collection) {
                Collection<Object> collection = (Collection) obj;
                ArrayList arrayList2 = new ArrayList(collection.size());
                for (Object a2 : collection) {
                    arrayList2.add(a(a2));
                }
                return new JSONArray((Collection) arrayList2);
            } else if (!(obj instanceof Map)) {
                return obj;
            } else {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Map.Entry entry : ((Map) obj).entrySet()) {
                    String obj2 = entry.getKey().toString();
                    if (obj2 != null) {
                        linkedHashMap.put(obj2, a(entry.getValue()));
                    }
                }
                return new JSONObject(linkedHashMap);
            }
        } catch (Throwable th) {
            return null;
        }
    }

    @Nullable
    public static String a(@Nullable Map map) {
        if (cg.a(map)) {
            return null;
        }
        if (cg.a(19)) {
            return new JSONObject(map).toString();
        }
        return a((Object) map).toString();
    }

    @Nullable
    public static String a(List<String> list) {
        if (cg.a((Collection) list)) {
            return null;
        }
        if (cg.a(19)) {
            return new JSONArray((Collection) list).toString();
        }
        return a((Object) list).toString();
    }

    @Nullable
    public static HashMap<String, String> a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return a(new JSONObject(str));
        } catch (JSONException e) {
            return null;
        }
    }

    @Nullable
    public static List<String> b(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONArray jSONArray = new JSONArray(str);
            ArrayList arrayList = new ArrayList(jSONArray.length());
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    arrayList.add(jSONArray.getString(i));
                    i++;
                } catch (JSONException e) {
                    return arrayList;
                }
            }
            return arrayList;
        } catch (JSONException e2) {
            return null;
        }
    }

    @Nullable
    public static HashMap<String, String> a(JSONObject jSONObject) {
        if (JSONObject.NULL.equals(jSONObject)) {
            return null;
        }
        HashMap<String, String> hashMap = new HashMap<>();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            String optString = jSONObject.optString(next);
            if (optString != null) {
                hashMap.put(next, optString);
            }
        }
        return hashMap;
    }

    @Nullable
    public static JSONObject a(@NonNull JSONObject jSONObject, @NonNull Location location) throws JSONException {
        jSONObject.put("lat", location.getLatitude());
        jSONObject.put("lon", location.getLongitude());
        jSONObject.putOpt("timestamp", Long.valueOf(location.getTime()));
        jSONObject.putOpt("precision", location.hasAccuracy() ? Float.valueOf(location.getAccuracy()) : null);
        jSONObject.putOpt("direction", location.hasBearing() ? Float.valueOf(location.getBearing()) : null);
        jSONObject.putOpt("speed", location.hasSpeed() ? Float.valueOf(location.getSpeed()) : null);
        jSONObject.putOpt("altitude", location.hasAltitude() ? Double.valueOf(location.getAltitude()) : null);
        jSONObject.putOpt("provider", ce.c(location.getProvider(), null));
        return jSONObject;
    }

    @Nullable
    public static Long a(JSONObject jSONObject, String str) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return Long.valueOf(jSONObject.getLong(str));
            } catch (Throwable th) {
            }
        }
        return null;
    }

    @Nullable
    public static Integer b(JSONObject jSONObject, String str) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return Integer.valueOf(jSONObject.getInt(str));
            } catch (Throwable th) {
            }
        }
        return null;
    }

    @Nullable
    public static Boolean c(JSONObject jSONObject, String str) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return Boolean.valueOf(jSONObject.getBoolean(str));
            } catch (Throwable th) {
            }
        }
        return null;
    }

    public static boolean a(JSONObject jSONObject, String str, boolean z) {
        Boolean c = c(jSONObject, str);
        return c == null ? z : c.booleanValue();
    }

    @Nullable
    public static Float d(JSONObject jSONObject, String str) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return Float.valueOf((float) jSONObject.getDouble(str));
            } catch (JSONException e) {
            }
        }
        return null;
    }

    public static List<String> a(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(jSONArray.getString(i));
        }
        return arrayList;
    }

    public static JSONArray a(sm[] smVarArr) {
        JSONArray jSONArray = new JSONArray();
        if (smVarArr != null) {
            for (sm a2 : smVarArr) {
                try {
                    jSONArray.put(a(a2));
                } catch (JSONException e) {
                }
            }
        }
        return jSONArray;
    }

    public static JSONObject a(sm smVar) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("cell_id", smVar.e());
        jSONObject.put("signal_strength", smVar.a());
        jSONObject.put("lac", smVar.d());
        jSONObject.put("country_code", smVar.b());
        jSONObject.put("operator_id", smVar.c());
        jSONObject.put("operator_name", smVar.f());
        jSONObject.put("is_connected", smVar.h());
        jSONObject.put("cell_type", smVar.i());
        jSONObject.put("pci", smVar.j());
        jSONObject.put("last_visible_time_offset", smVar.k());
        return jSONObject;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public static JSONObject a() throws JSONException {
        return new JSONObject().put("stat_sending", new JSONObject().put("disabled", true));
    }
}
