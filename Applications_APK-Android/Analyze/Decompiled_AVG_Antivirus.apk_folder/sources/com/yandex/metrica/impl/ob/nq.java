package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class nq {
    @NonNull
    private no a;

    public nq(@NonNull no noVar) {
        this.a = noVar;
    }

    public boolean a(@NonNull Context context, String str) {
        if (!a().a(str)) {
            return false;
        }
        try {
            if (context.checkCallingOrSelfPermission(str) == 0) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    public boolean a(@NonNull Context context) {
        return a(context, "android.permission.ACCESS_COARSE_LOCATION");
    }

    public boolean b(@NonNull Context context) {
        return a(context, "android.permission.ACCESS_FINE_LOCATION");
    }

    public boolean c(@NonNull Context context) {
        return a(context) || b(context);
    }

    public boolean d(@NonNull Context context) {
        return a(context, "android.permission.READ_PHONE_STATE");
    }

    public boolean e(@NonNull Context context) {
        return a(context, "android.permission.ACCESS_WIFI_STATE");
    }

    public boolean f(@NonNull Context context) {
        return a(context, "android.permission.CHANGE_WIFI_STATE");
    }

    public void a(@NonNull no noVar) {
        this.a = noVar;
    }

    @VisibleForTesting
    @NonNull
    public no a() {
        return this.a;
    }
}
