package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.qj;
import com.yandex.metrica.impl.ob.qm;
import java.util.List;

public class ey extends qm {
    @Nullable
    private List<String> a;
    @NonNull
    private String b;
    private Boolean c;

    @Nullable
    public List<String> a() {
        return this.a;
    }

    public void a(@Nullable List<String> list) {
        this.a = list;
    }

    @NonNull
    public String b() {
        return this.b;
    }

    public void a(@NonNull String str) {
        this.b = str;
    }

    @Nullable
    public Boolean c() {
        return this.c;
    }

    public void a(Boolean bool) {
        this.c = bool;
    }

    public String toString() {
        return "DiagnosticRequestConfig{mDiagnosticHosts=" + this.a + ", mApiKey='" + this.b + '\'' + ", statisticsSending=" + this.c + '}';
    }

    public static final class a extends qj.a<db.a, a> {
        @NonNull
        public final String a;
        public final boolean b;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
         arg types: [java.lang.Boolean, int]
         candidates:
          com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
          com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
          com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
          com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
          com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
          com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean */
        public a(@Nullable String str, @Nullable String str2, @Nullable String str3, @NonNull String str4, @Nullable Boolean bool) {
            super(str, str2, str3);
            this.a = str4;
            this.b = ua.a(bool, true);
        }

        public a(@NonNull db.a aVar) {
            this(aVar.a, aVar.b, aVar.c, aVar.d, aVar.m);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
          com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
          com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
          com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
          com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
          com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T */
        @NonNull
        /* renamed from: a */
        public a b(@NonNull db.a aVar) {
            return new a((String) ua.a((Object) aVar.a, (Object) this.c), (String) ua.a((Object) aVar.b, (Object) this.d), (String) ua.a((Object) aVar.c, (Object) this.e), ua.b(aVar.d, this.a), (Boolean) ua.a(aVar.m, Boolean.valueOf(this.b)));
        }

        /* renamed from: b */
        public boolean a(@NonNull db.a aVar) {
            if (aVar.a != null && !aVar.a.equals(this.c)) {
                return false;
            }
            if (aVar.b != null && !aVar.b.equals(this.d)) {
                return false;
            }
            if (aVar.c != null && !aVar.c.equals(this.e)) {
                return false;
            }
            if (aVar.d == null || aVar.d.equals(this.a)) {
                return true;
            }
            return false;
        }
    }

    public static class b extends qm.a<ey, a> {
        @NonNull
        public /* bridge */ /* synthetic */ qj a(@NonNull Object obj) {
            return a((qj.c<a>) ((qj.c) obj));
        }

        @NonNull
        public /* synthetic */ qm b(@NonNull qj.c cVar) {
            return a((qj.c<a>) cVar);
        }

        @NonNull
        public /* synthetic */ qj c(@NonNull qj.c cVar) {
            return a((qj.c<a>) cVar);
        }

        public b(@NonNull Context context, @NonNull String str) {
            super(context, str);
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public ey b() {
            return new ey();
        }

        @NonNull
        public ey a(@NonNull qj.c<a> cVar) {
            ey eyVar = (ey) super.c(cVar);
            eyVar.a(cVar.a.l);
            eyVar.a(((a) cVar.b).a);
            eyVar.a(Boolean.valueOf(((a) cVar.b).b));
            return eyVar;
        }
    }
}
