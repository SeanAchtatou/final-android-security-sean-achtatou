package com.yandex.metrica.impl.ob;

public enum un {
    NONE(0),
    EXTERNALLY_ENCRYPTED_EVENT_CRYPTER(1),
    AES_VALUE_ENCRYPTION(2);
    
    private final int d;

    private un(int i) {
        this.d = i;
    }

    public int a() {
        return this.d;
    }

    public static un a(int i) {
        for (un unVar : values()) {
            if (unVar.a() == i) {
                return unVar;
            }
        }
        return null;
    }
}
