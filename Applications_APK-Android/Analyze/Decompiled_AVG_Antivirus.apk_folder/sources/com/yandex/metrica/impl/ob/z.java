package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.eu;

public class z<C extends eu> extends cf<C> {
    @NonNull
    private final fa d;
    @NonNull
    private final kh e;
    private boolean f = false;

    public z(@NonNull C c, @NonNull si siVar, @NonNull bb bbVar, @NonNull fa faVar, @NonNull kh khVar) {
        super(c, siVar, bbVar);
        this.d = faVar;
        this.e = khVar;
    }

    public void a(@NonNull t tVar) {
        if (!this.f) {
            super.f();
            this.b.a(new ew((ez) g(), tVar, this.d, this.e));
        }
    }

    public void close() {
        this.f = true;
    }
}
