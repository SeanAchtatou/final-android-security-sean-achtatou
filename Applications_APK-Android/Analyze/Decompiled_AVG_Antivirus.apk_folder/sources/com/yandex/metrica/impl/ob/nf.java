package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.pg;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class nf {
    @NonNull
    private final js a;
    @NonNull
    private final jr b;
    @NonNull
    private final nc c;
    @NonNull
    private final na d;

    public nf(@NonNull Context context) {
        this(jo.a(context).g(), jo.a(context).h(), new mg(context), new nb(), new mz());
    }

    public ne a(int i) {
        Map<Long, String> b2 = this.a.b(i);
        Map<Long, String> b3 = this.b.b(i);
        pg.b bVar = new pg.b();
        bVar.b = a(b2);
        bVar.c = b(b3);
        return new ne(b2.isEmpty() ? -1 : ((Long) Collections.max(b2.keySet())).longValue(), b3.isEmpty() ? -1 : ((Long) Collections.max(b3.keySet())).longValue(), bVar);
    }

    public void a(ne neVar) {
        if (neVar.a >= 0) {
            this.a.b(neVar.a);
        }
        if (neVar.b >= 0) {
            this.b.b(neVar.b);
        }
    }

    private pg.b.C0005b[] a(Map<Long, String> map) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : map.entrySet()) {
            pg.b.C0005b a2 = this.c.a(((Long) next.getKey()).longValue(), (String) next.getValue());
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return (pg.b.C0005b[]) arrayList.toArray(new pg.b.C0005b[arrayList.size()]);
    }

    private pg.b.a[] b(Map<Long, String> map) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : map.entrySet()) {
            pg.b.a a2 = this.d.a(((Long) next.getKey()).longValue(), (String) next.getValue());
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return (pg.b.a[]) arrayList.toArray(new pg.b.a[arrayList.size()]);
    }

    @VisibleForTesting
    nf(@NonNull js jsVar, @NonNull jr jrVar, @NonNull mg mgVar, @NonNull nb nbVar, @NonNull mz mzVar) {
        this(jsVar, jrVar, new nc(mgVar, nbVar), new na(mgVar, mzVar));
    }

    @VisibleForTesting
    nf(@NonNull js jsVar, @NonNull jr jrVar, @NonNull nc ncVar, @NonNull na naVar) {
        this.a = jsVar;
        this.b = jrVar;
        this.c = ncVar;
        this.d = naVar;
    }
}
