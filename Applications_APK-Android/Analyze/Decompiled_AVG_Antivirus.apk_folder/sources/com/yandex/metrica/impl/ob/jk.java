package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ab;
import com.yandex.metrica.impl.ob.i;
import com.yandex.metrica.impl.ob.kb;
import java.io.Closeable;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class jk implements Closeable {
    private final ReentrantReadWriteLock a = new ReentrantReadWriteLock();
    private final Lock b = this.a.readLock();
    private final Lock c = this.a.writeLock();
    private final jn d;
    private final a e;
    /* access modifiers changed from: private */
    public final Object f = new Object();
    /* access modifiers changed from: private */
    public final List<ContentValues> g = new ArrayList(3);
    private final Context h;
    private final di i;
    private final AtomicLong j = new AtomicLong();
    /* access modifiers changed from: private */
    @NonNull
    public final List<lw> k = new ArrayList();

    public jk(di diVar, jn jnVar) {
        this.d = jnVar;
        this.h = diVar.j();
        this.i = diVar;
        this.j.set(d());
        this.e = new a(diVar);
        this.e.setName(a(diVar));
        f();
    }

    public void a() {
        this.e.start();
    }

    public long b() {
        this.b.lock();
        try {
            return this.j.get();
        } finally {
            this.b.unlock();
        }
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.String[], android.database.Cursor] */
    public long a(@NonNull Set<Integer> set) {
        long j2;
        this.b.lock();
        Cursor cursor = 0;
        try {
            SQLiteDatabase readableDatabase = this.d.getReadableDatabase();
            StringBuilder sb = new StringBuilder("SELECT count() FROM reports");
            if (!set.isEmpty()) {
                sb.append(" WHERE ");
            }
            int i2 = 0;
            for (Integer next : set) {
                if (i2 > 0) {
                    sb.append(" OR ");
                }
                sb.append("type == " + next);
                i2++;
            }
            cursor = readableDatabase.rawQuery(sb.toString(), cursor);
            if (cursor.moveToFirst()) {
                j2 = cursor.getLong(0);
            } else {
                j2 = 0;
            }
            return j2;
        } finally {
            cg.a((Cursor) cursor);
            this.b.unlock();
        }
    }

    public void a(@NonNull lw lwVar) {
        this.k.add(lwVar);
    }

    private static String a(Cdo doVar) {
        return "DatabaseWorker [" + doVar.b() + "]";
    }

    public void a(long j2, hw hwVar, long j3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", Long.valueOf(j2));
        contentValues.put("start_time", Long.valueOf(j3));
        contentValues.put("server_time_offset", Long.valueOf(ty.c()));
        contentValues.put("obtained_before_first_sync", Boolean.valueOf(tt.a().d()));
        contentValues.put("type", Integer.valueOf(hwVar.a()));
        new v(this.h).a(this.i.h()).a(contentValues).a();
        a(contentValues);
    }

    public void a(@NonNull uk ukVar, int i2, @NonNull ht htVar, @NonNull i.a aVar, @NonNull dm dmVar) {
        int i3;
        ContentValues contentValues = new ContentValues();
        contentValues.put("number", Long.valueOf(htVar.c()));
        if (ab.e(i2)) {
            i3 = dmVar.a();
        } else {
            i3 = 0;
        }
        contentValues.put("global_number", Integer.valueOf(i3));
        contentValues.put("number_of_type", Integer.valueOf(dmVar.a(i2)));
        contentValues.put("time", Long.valueOf(htVar.d()));
        contentValues.put("session_id", Long.valueOf(htVar.a()));
        contentValues.put("session_type", Integer.valueOf(htVar.b().a()));
        new v(this.h).a(this.i.h()).a(contentValues).a(ukVar, aVar);
        b(contentValues);
    }

    /* JADX INFO: finally extract failed */
    private long d() {
        this.b.lock();
        try {
            long a2 = tc.a(this.d.getReadableDatabase(), "reports");
            this.b.unlock();
            return a2;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    public void a(ContentValues contentValues) {
        c(contentValues);
    }

    public void b(ContentValues contentValues) {
        synchronized (this.f) {
            this.g.add(contentValues);
        }
        synchronized (this.e) {
            this.e.notifyAll();
        }
    }

    /* JADX INFO: finally extract failed */
    public int a(long[] jArr) {
        this.c.lock();
        try {
            if (kb.a.booleanValue()) {
                e();
            }
            int delete = this.d.getWritableDatabase().delete("sessions", kb.g.d, cg.a(jArr));
            this.c.unlock();
            return delete;
        } catch (Throwable th) {
            this.c.unlock();
            throw th;
        }
    }

    private void e() {
        Cursor cursor;
        this.b.lock();
        Cursor cursor2 = null;
        try {
            SQLiteDatabase readableDatabase = this.d.getReadableDatabase();
            cursor = readableDatabase.rawQuery(" SELECT DISTINCT id From sessions order by id asc ", new String[0]);
            try {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("All sessions in db: ");
                while (cursor.moveToNext()) {
                    stringBuffer.append(cursor.getString(0));
                    stringBuffer.append(", ");
                }
                cursor2 = readableDatabase.rawQuery(" SELECT DISTINCT session_id From reports order by session_id asc ", new String[0]);
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("All sessions in reports db: ");
                while (cursor2.moveToNext()) {
                    stringBuffer2.append(cursor2.getString(0));
                    stringBuffer2.append(", ");
                }
            } catch (Throwable th) {
                th = th;
                this.b.unlock();
                cg.a(cursor);
                cg.a((Cursor) null);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            this.b.unlock();
            cg.a(cursor);
            cg.a((Cursor) null);
            throw th;
        }
        this.b.unlock();
        cg.a(cursor);
        cg.a(cursor2);
    }

    private void f() {
        try {
            this.c.lock();
            SQLiteDatabase writableDatabase = this.d.getWritableDatabase();
            if (new File(writableDatabase.getPath()).length() > 5242880) {
                this.j.addAndGet((long) (-a(writableDatabase)));
            }
        } catch (Throwable th) {
            this.c.unlock();
            throw th;
        }
        this.c.unlock();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final int a(SQLiteDatabase sQLiteDatabase) {
        try {
            Integer[] numArr = new Integer[ab.a.size()];
            Iterator it = ab.a.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                numArr[i2] = Integer.valueOf(((ab.a) it.next()).a());
                i2++;
            }
            return sQLiteDatabase.delete("reports", String.format("%1$s NOT IN (%2$s) AND (%3$s IN (SELECT %3$s FROM %4$s ORDER BY %5$s, %6$s LIMIT (SELECT count() FROM %4$s) / %7$s ) OR %5$s < %8$s )", "type", TextUtils.join(",", numArr), "id", "reports", "session_id", "number", 10, Long.valueOf(ty.b() - TimeUnit.DAYS.toSeconds(14))), null);
        } catch (Throwable th) {
            rh.a(this.h).reportError("deleteExcessiveReports exception", th);
            return 0;
        }
    }

    public void a(long j2, int i2, int i3) throws SQLiteException {
        Cursor cursor;
        if (i3 > 0) {
            this.c.lock();
            try {
                SQLiteDatabase writableDatabase = this.d.getWritableDatabase();
                String format = String.format(Locale.US, "%1$s = %2$s AND %3$s = %4$s AND %5$s <= (SELECT %5$s FROM %6$s WHERE %1$s = %2$s AND %3$s = %4$s ORDER BY %5$s ASC LIMIT %7$s, 1)", "session_id", Long.toString(j2), "session_type", Integer.toString(i2), "id", "reports", Integer.toString(i3 - 1));
                cursor = a(format);
                try {
                    List<ContentValues> a2 = a(cursor);
                    int delete = writableDatabase.delete("reports", format, null);
                    if (a2 != null) {
                        for (ContentValues g2 : a2) {
                            int g3 = g(g2);
                            for (lw b2 : this.k) {
                                b2.b(g3);
                            }
                        }
                    }
                    if (this.i.k().c() && a2 != null) {
                        a(a2, "Event removed from db");
                    }
                    this.j.addAndGet((long) (-delete));
                } catch (Throwable th) {
                    th = th;
                    cg.a(cursor);
                    this.c.unlock();
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
                cg.a(cursor);
                this.c.unlock();
                throw th;
            }
            cg.a(cursor);
            this.c.unlock();
        }
    }

    @Nullable
    private List<ContentValues> a(Cursor cursor) {
        if (cursor == null || cursor.getCount() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(cursor.getCount());
        while (cursor.moveToNext()) {
            ContentValues contentValues = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
            arrayList.add(contentValues);
        }
        return arrayList;
    }

    @Nullable
    private Cursor a(String str) {
        try {
            return this.d.getReadableDatabase().query("reports", null, str, null, null, null, null, null);
        } catch (Throwable th) {
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    public Cursor a(Map<String, String> map) {
        this.b.lock();
        try {
            Cursor query = this.d.getReadableDatabase().query("sessions", null, a("id >= ?", map), a(new String[]{Long.toString(0)}, map), null, null, "id ASC", null);
            this.b.unlock();
            return query;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public Cursor a(long j2, hw hwVar) throws SQLiteException {
        this.b.lock();
        try {
            Cursor query = this.d.getReadableDatabase().query("reports", null, "session_id = ? AND session_type = ?", new String[]{Long.toString(j2), Integer.toString(hwVar.a())}, null, null, "number ASC", null);
            this.b.unlock();
            return query;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    private void c(ContentValues contentValues) {
        if (contentValues != null) {
            this.c.lock();
            try {
                this.d.getWritableDatabase().insertOrThrow("sessions", null, contentValues);
            } catch (Throwable th) {
                this.c.unlock();
                throw th;
            }
            this.c.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(List<ContentValues> list) {
        SQLiteDatabase sQLiteDatabase;
        if (list != null && !list.isEmpty()) {
            this.c.lock();
            try {
                sQLiteDatabase = this.d.getWritableDatabase();
                try {
                    if (this.j.get() % 10 == 0) {
                        f();
                    }
                    sQLiteDatabase.beginTransaction();
                    for (ContentValues next : list) {
                        sQLiteDatabase.insertOrThrow("reports", null, next);
                        this.j.incrementAndGet();
                        a(next, "Event saved to db");
                    }
                    sQLiteDatabase.setTransactionSuccessful();
                    this.j.get();
                } catch (Throwable th) {
                    th = th;
                    cg.a(sQLiteDatabase);
                    this.c.unlock();
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                sQLiteDatabase = null;
                cg.a(sQLiteDatabase);
                this.c.unlock();
                throw th;
            }
            cg.a(sQLiteDatabase);
            this.c.unlock();
        }
    }

    private void a(ContentValues contentValues, String str) {
        if (ab.b(d(contentValues))) {
            StringBuilder sb = new StringBuilder(str);
            sb.append(": ");
            sb.append(e(contentValues));
            String f2 = f(contentValues);
            if (ab.c(g(contentValues)) && !TextUtils.isEmpty(f2)) {
                sb.append(" with value ");
                sb.append(f2);
            }
            this.i.k().a(sb.toString());
        }
    }

    private void a(List<ContentValues> list, String str) {
        for (int i2 = 0; i2 < list.size(); i2++) {
            a(list.get(i2), str);
        }
    }

    @NonNull
    public List<ContentValues> c() {
        ArrayList arrayList = new ArrayList();
        this.b.lock();
        Cursor cursor = null;
        try {
            cursor = this.d.getReadableDatabase().rawQuery(kb.g.c, null);
            while (cursor.moveToNext()) {
                ContentValues contentValues = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
                arrayList.add(contentValues);
            }
        } catch (Throwable th) {
            cg.a((Cursor) null);
            this.b.unlock();
            throw th;
        }
        cg.a(cursor);
        this.b.unlock();
        return arrayList;
    }

    public ContentValues b(long j2, hw hwVar) {
        ContentValues contentValues = new ContentValues();
        this.b.lock();
        Cursor cursor = null;
        try {
            cursor = this.d.getReadableDatabase().rawQuery(String.format(Locale.US, "SELECT report_request_parameters FROM sessions WHERE id = %s AND type = %s ORDER BY id DESC LIMIT 1", Long.valueOf(j2), Integer.valueOf(hwVar.a())), null);
            if (cursor.moveToNext()) {
                ContentValues contentValues2 = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, contentValues2);
                contentValues = contentValues2;
            }
        } catch (Throwable th) {
            cg.a((Cursor) null);
            this.b.unlock();
            throw th;
        }
        cg.a(cursor);
        this.b.unlock();
        return contentValues;
    }

    private static String a(String str, Map<String, String> map) {
        StringBuilder sb = new StringBuilder(str);
        for (String next : map.keySet()) {
            sb.append(sb.length() > 0 ? " AND " : "");
            sb.append(next + " = ? ");
        }
        if (TextUtils.isEmpty(sb.toString())) {
            return null;
        }
        return sb.toString();
    }

    private static String[] a(String[] strArr, Map<String, String> map) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(strArr));
        for (Map.Entry<String, String> value : map.entrySet()) {
            arrayList.add(value.getValue());
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    private static int d(ContentValues contentValues) {
        Integer asInteger = contentValues.getAsInteger("type");
        if (asInteger != null) {
            return asInteger.intValue();
        }
        return -1;
    }

    private String e(ContentValues contentValues) {
        return b(contentValues, "name");
    }

    private String f(ContentValues contentValues) {
        return b(contentValues, "value");
    }

    private String b(ContentValues contentValues, String str) {
        return ce.b(contentValues.getAsString(str), "");
    }

    /* access modifiers changed from: private */
    public int g(ContentValues contentValues) {
        return c(contentValues, "type");
    }

    private int c(ContentValues contentValues, String str) {
        return contentValues.getAsInteger(str).intValue();
    }

    private class a extends uz {
        @Nullable
        private di b;

        a(di diVar) {
            this.b = diVar;
        }

        public void run() {
            ArrayList arrayList;
            while (c()) {
                try {
                    synchronized (this) {
                        if (jk.this.g()) {
                            wait();
                        }
                    }
                } catch (Throwable th) {
                    b();
                }
                synchronized (jk.this.f) {
                    arrayList = new ArrayList(jk.this.g);
                    jk.this.g.clear();
                }
                jk.this.a(arrayList);
                a(arrayList);
            }
        }

        /* access modifiers changed from: package-private */
        public synchronized void a() {
            b();
            this.b = null;
        }

        /* access modifiers changed from: package-private */
        public synchronized void a(@NonNull List<ContentValues> list) {
            for (ContentValues a2 : list) {
                int a3 = jk.this.g(a2);
                for (lw a4 : jk.this.k) {
                    a4.a(a3);
                }
            }
            if (this.b != null) {
                this.b.A().a();
            }
        }
    }

    public void close() {
        this.g.clear();
        this.e.a();
    }

    /* access modifiers changed from: private */
    public boolean g() {
        boolean isEmpty;
        synchronized (this.f) {
            isEmpty = this.g.isEmpty();
        }
        return isEmpty;
    }
}
