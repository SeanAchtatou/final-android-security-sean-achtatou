package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.g;
import java.util.LinkedHashMap;
import java.util.Map;

public class x implements as {
    private Location a;
    private Boolean b;
    private Boolean c;
    private Map<String, String> d = new LinkedHashMap();
    private Map<String, String> e = new LinkedHashMap();
    private boolean f;
    private boolean g;
    private bt h;

    public Location a() {
        return this.a;
    }

    public Boolean b() {
        return this.b;
    }

    public void a(boolean z) {
        this.b = Boolean.valueOf(z);
        f();
    }

    public Boolean c() {
        return this.c;
    }

    public void setStatisticsSending(boolean value) {
        this.c = Boolean.valueOf(value);
        f();
    }

    public void a(Location location) {
        this.a = location;
    }

    public boolean d() {
        return this.f;
    }

    private void e() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d.clear();
        this.e.clear();
        this.f = false;
    }

    public g a(g gVar) {
        if (this.g) {
            return gVar;
        }
        g.a b2 = b(gVar);
        a(gVar, b2);
        this.g = true;
        e();
        return b2.b();
    }

    private g.a b(g gVar) {
        g.a a2 = g.a(gVar.apiKey);
        a2.a(gVar.b, gVar.j);
        a2.c(gVar.a);
        a2.a(gVar.preloadInfo);
        a2.a(gVar.location);
        a2.a(gVar.m);
        a(a2, gVar);
        a(this.d, a2);
        a(gVar.i, a2);
        b(this.e, a2);
        b(gVar.h, a2);
        return a2;
    }

    private void a(@NonNull g.a aVar, @NonNull g gVar) {
        if (cg.a((Object) gVar.d)) {
            aVar.a(gVar.d);
        }
        if (cg.a((Object) gVar.appVersion)) {
            aVar.a(gVar.appVersion);
        }
        if (cg.a(gVar.f)) {
            aVar.d(gVar.f.intValue());
        }
        if (cg.a(gVar.e)) {
            aVar.b(gVar.e.intValue());
        }
        if (cg.a(gVar.g)) {
            aVar.c(gVar.g.intValue());
        }
        if (cg.a(gVar.logs) && gVar.logs.booleanValue()) {
            aVar.a();
        }
        if (cg.a(gVar.sessionTimeout)) {
            aVar.a(gVar.sessionTimeout.intValue());
        }
        if (cg.a(gVar.crashReporting)) {
            aVar.a(gVar.crashReporting.booleanValue());
        }
        if (cg.a(gVar.nativeCrashReporting)) {
            aVar.b(gVar.nativeCrashReporting.booleanValue());
        }
        if (cg.a(gVar.locationTracking)) {
            aVar.d(gVar.locationTracking.booleanValue());
        }
        if (cg.a(gVar.installedAppCollecting)) {
            aVar.e(gVar.installedAppCollecting.booleanValue());
        }
        if (cg.a((Object) gVar.c)) {
            aVar.b(gVar.c);
        }
        if (cg.a(gVar.firstActivationAsUpdate)) {
            aVar.g(gVar.firstActivationAsUpdate.booleanValue());
        }
        if (cg.a(gVar.statisticsSending)) {
            aVar.f(gVar.statisticsSending.booleanValue());
        }
        if (cg.a(gVar.l)) {
            aVar.c(gVar.l.booleanValue());
        }
    }

    private void a(@Nullable Map<String, String> map, @NonNull g.a aVar) {
        if (!cg.a((Map) map)) {
            for (Map.Entry next : map.entrySet()) {
                aVar.b((String) next.getKey(), (String) next.getValue());
            }
        }
    }

    private void b(@Nullable Map<String, String> map, @NonNull g.a aVar) {
        if (!cg.a((Map) map)) {
            for (Map.Entry next : map.entrySet()) {
                aVar.a((String) next.getKey(), (String) next.getValue());
            }
        }
    }

    private void a(g gVar, g.a aVar) {
        Boolean b2 = b();
        if (a(gVar.locationTracking) && cg.a(b2)) {
            aVar.d(b2.booleanValue());
        }
        Location a2 = a();
        if (a((Object) gVar.location) && cg.a(a2)) {
            aVar.a(a2);
        }
        Boolean c2 = c();
        if (a(gVar.statisticsSending) && cg.a(c2)) {
            aVar.f(c2.booleanValue());
        }
    }

    private static boolean a(Object obj) {
        return obj == null;
    }

    public void a(bt btVar) {
        this.h = btVar;
    }

    private void f() {
        bt btVar = this.h;
        if (btVar != null) {
            btVar.a(this.b, this.c);
        }
    }
}
