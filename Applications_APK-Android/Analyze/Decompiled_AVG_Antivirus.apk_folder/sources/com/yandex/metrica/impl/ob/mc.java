package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.mh;
import java.util.List;

public class mc extends mh {
    public final long a;
    public final long b;
    public final boolean c;
    @Nullable
    public final List<mj> d;

    public mc(long j, float f, int i, int i2, long j2, int i3, boolean z, long j3, long j4, long j5, boolean z2, boolean z3, @Nullable List<mj> list) {
        super(j, f, i, i2, j2, i3, z, j5, z2);
        this.a = j3;
        this.b = j4;
        this.c = z3;
        this.d = list;
    }

    @NonNull
    public mh.a a() {
        return mh.a.BACKGROUND;
    }

    public String toString() {
        return "BackgroundCollectionConfig{collectionDuration=" + this.a + ", collectionInterval=" + this.b + ", aggressiveRelaunch=" + this.c + ", collectionIntervalRanges=" + this.d + ", updateTimeInterval=" + this.e + ", updateDistanceInterval=" + this.f + ", recordsCountToForceFlush=" + this.g + ", maxBatchSize=" + this.h + ", maxAgeToForceFlush=" + this.i + ", maxRecordsToStoreLocally=" + this.j + ", collectionEnabled=" + this.k + ", lbsUpdateTimeInterval=" + this.l + ", lbsCollectionEnabled=" + this.m + '}';
    }
}
