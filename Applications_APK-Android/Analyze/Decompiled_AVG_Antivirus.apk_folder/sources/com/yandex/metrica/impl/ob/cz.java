package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.CounterConfiguration;

public class cz {
    @Nullable
    private final da a;
    @NonNull
    private final CounterConfiguration b;

    public cz(@NonNull Bundle bundle) {
        this.a = da.a(bundle);
        this.b = CounterConfiguration.c(bundle);
    }

    public cz(@NonNull da daVar, @NonNull CounterConfiguration counterConfiguration) {
        this.a = daVar;
        this.b = counterConfiguration;
    }

    @NonNull
    public da g() {
        return this.a;
    }

    @NonNull
    public CounterConfiguration h() {
        return this.b;
    }

    public String toString() {
        return "ClientConfiguration{mProcessConfiguration=" + this.a + ", mCounterConfiguration=" + this.b + '}';
    }
}
