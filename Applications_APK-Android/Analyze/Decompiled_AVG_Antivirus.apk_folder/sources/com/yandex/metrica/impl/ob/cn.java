package com.yandex.metrica.impl.ob;

import androidx.annotation.VisibleForTesting;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class cn {
    private final uz a = va.a("YMM-BD", new Runnable() {
        public void run() {
            while (cn.this.b) {
                try {
                    ((a) cn.this.c.take()).a();
                } catch (InterruptedException e) {
                }
            }
        }
    });
    /* access modifiers changed from: private */
    public volatile boolean b = true;
    /* access modifiers changed from: private */
    public final BlockingQueue<a> c = new LinkedBlockingQueue();
    private ConcurrentHashMap<Class, CopyOnWriteArrayList<cr<? extends cp>>> d = new ConcurrentHashMap<>();
    private WeakHashMap<Object, CopyOnWriteArrayList<c>> e = new WeakHashMap<>();
    private ConcurrentHashMap<Class, cp> f = new ConcurrentHashMap<>();

    private static final class b {
        /* access modifiers changed from: private */
        public static final cn a = new cn();
    }

    private static class c {
        final CopyOnWriteArrayList<cr<? extends cp>> a;
        final cr<? extends cp> b;

        private c(CopyOnWriteArrayList<cr<? extends cp>> copyOnWriteArrayList, cr<? extends cp> crVar) {
            this.a = copyOnWriteArrayList;
            this.b = crVar;
        }

        /* access modifiers changed from: protected */
        public void a() {
            this.a.remove(this.b);
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            super.finalize();
            a();
        }
    }

    private static class a {
        private final cp a;
        private final cr<? extends cp> b;

        private a(cp cpVar, cr<? extends cp> crVar) {
            this.a = cpVar;
            this.b = crVar;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            try {
                if (!this.b.b(this.a)) {
                    this.b.a(this.a);
                }
            } catch (Throwable th) {
            }
        }
    }

    public static final cn a() {
        return b.a;
    }

    @VisibleForTesting
    cn() {
        this.a.start();
    }

    public synchronized void a(cp cpVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = this.d.get(cpVar.getClass());
        if (copyOnWriteArrayList != null) {
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                a(cpVar, (cr) it.next());
            }
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(cp cpVar, cr<? extends cp> crVar) {
        this.c.add(new a(cpVar, crVar));
    }

    public synchronized void b(cp cpVar) {
        a(cpVar);
        this.f.put(cpVar.getClass(), cpVar);
    }

    public synchronized void a(Class<? extends cp> cls) {
        this.f.remove(cls);
    }

    public synchronized void a(Object obj, Class cls, cr<? extends cp> crVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = this.d.get(cls);
        if (copyOnWriteArrayList == null) {
            copyOnWriteArrayList = new CopyOnWriteArrayList();
            this.d.put(cls, copyOnWriteArrayList);
        }
        copyOnWriteArrayList.add(crVar);
        CopyOnWriteArrayList copyOnWriteArrayList2 = this.e.get(obj);
        if (copyOnWriteArrayList2 == null) {
            copyOnWriteArrayList2 = new CopyOnWriteArrayList();
            this.e.put(obj, copyOnWriteArrayList2);
        }
        copyOnWriteArrayList2.add(new c(copyOnWriteArrayList, crVar));
        cp cpVar = this.f.get(cls);
        if (cpVar != null) {
            a(cpVar, crVar);
        }
    }

    public synchronized void a(Object obj) {
        List<c> remove = this.e.remove(obj);
        if (remove != null) {
            for (c a2 : remove) {
                a2.a();
            }
        }
    }
}
