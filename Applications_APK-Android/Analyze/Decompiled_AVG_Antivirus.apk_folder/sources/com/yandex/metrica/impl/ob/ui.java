package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class ui implements up {
    private uh a;

    public ui() {
        this(new uh());
    }

    @Nullable
    public byte[] a(@Nullable byte[] bArr) {
        return this.a.a(bArr);
    }

    @NonNull
    public uq a() {
        return uq.AES_RSA;
    }

    @VisibleForTesting
    ui(uh uhVar) {
        this.a = uhVar;
    }
}
