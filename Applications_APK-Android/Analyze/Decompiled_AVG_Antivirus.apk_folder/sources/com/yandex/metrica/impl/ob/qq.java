package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.qj;
import com.yandex.metrica.impl.ob.qm;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class qq extends qm {
    @Nullable
    private List<String> a;
    @Nullable
    private List<String> b;
    @Nullable
    private String c;
    @Nullable
    private Map<String, String> d;
    @Nullable
    private List<String> e;
    private boolean f;
    private boolean g;
    private String h;
    private long i;

    private qq() {
        this.i = 0;
    }

    public List<String> a() {
        ArrayList arrayList = new ArrayList();
        if (!cg.a((Collection) this.a)) {
            arrayList.addAll(this.a);
        }
        if (!cg.a((Collection) this.b)) {
            arrayList.addAll(this.b);
        }
        arrayList.add("https://startup.mobile.yandex.net/");
        return arrayList;
    }

    public boolean b() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.g = z;
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        if (this.i == 0) {
            this.i = j;
        }
    }

    public long c() {
        return this.i;
    }

    public long b(long j) {
        a(j);
        return c();
    }

    public List<String> F() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable List<String> list) {
        this.b = list;
    }

    @Nullable
    public Map<String, String> G() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable Map<String, String> map) {
        this.d = map;
    }

    /* access modifiers changed from: package-private */
    public void b(@Nullable List<String> list) {
        this.a = list;
    }

    @Nullable
    public String H() {
        return this.c;
    }

    /* access modifiers changed from: private */
    public void m(@Nullable String str) {
        this.c = str;
    }

    @Nullable
    public List<String> I() {
        return this.e;
    }

    public void c(@Nullable List<String> list) {
        this.e = list;
    }

    @Nullable
    public boolean J() {
        return this.f;
    }

    public void b(boolean z) {
        this.f = z;
    }

    public String K() {
        return this.h;
    }

    public void a(String str) {
        this.h = str;
    }

    public String toString() {
        return "StartupRequestConfig{mStartupHostsFromStartup=" + this.a + ", mStartupHostsFromClient=" + this.b + ", mDistributionReferrer='" + this.c + '\'' + ", mClidsFromClient=" + this.d + ", mNewCustomHosts=" + this.e + ", mHasNewCustomHosts=" + this.f + ", mSuccessfulStartup=" + this.g + ", mCountryInit='" + this.h + '\'' + ", mFirstStartupTime='" + this.i + '\'' + '}';
    }

    public static class a extends qj.a<a, a> implements qi<a, a> {
        @Nullable
        public final String a;
        @Nullable
        public final Map<String, String> b;
        public final boolean f;
        @Nullable
        public final List<String> g;

        public a(@NonNull cz czVar) {
            this(czVar.h().d(), czVar.h().g(), czVar.h().h(), czVar.g().d(), czVar.g().c(), czVar.g().a(), czVar.g().b());
        }

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable Map<String, String> map, boolean z, @Nullable List<String> list) {
            super(str, str2, str3);
            this.a = str4;
            this.b = map;
            this.f = z;
            this.g = list;
        }

        public a() {
            this(null, null, null, null, null, false, null);
        }

        /* access modifiers changed from: package-private */
        public boolean a(@NonNull a aVar) {
            boolean z = aVar.f;
            return z ? z : this.f;
        }

        /* access modifiers changed from: package-private */
        public List<String> b(@NonNull a aVar) {
            return aVar.f ? aVar.g : this.g;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
          com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
          com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
          com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
          com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
          com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T */
        @NonNull
        /* renamed from: c */
        public a b(@NonNull a aVar) {
            return new a((String) ua.a((Object) this.c, (Object) aVar.c), (String) ua.a((Object) this.d, (Object) aVar.d), (String) ua.a((Object) this.e, (Object) aVar.e), (String) ua.a((Object) this.a, (Object) aVar.a), (Map) ua.a(this.b, aVar.b), a(aVar), b(aVar));
        }

        /* renamed from: d */
        public boolean a(@NonNull a aVar) {
            return false;
        }
    }

    public static class b extends qm.a<qq, a> {
        public /* bridge */ /* synthetic */ qj a(@NonNull Object obj) {
            return a((qj.c<a>) ((qj.c) obj));
        }

        public /* synthetic */ qm b(@NonNull qj.c cVar) {
            return a((qj.c<a>) cVar);
        }

        public /* synthetic */ qj c(@NonNull qj.c cVar) {
            return a((qj.c<a>) cVar);
        }

        public b(@NonNull Context context, @NonNull String str) {
            super(context, str);
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public qq b() {
            return new qq();
        }

        public qq a(@NonNull qj.c<a> cVar) {
            qq qqVar = (qq) super.c(cVar);
            a(qqVar, cVar.a);
            qqVar.m(ua.b(((a) cVar.b).a, cVar.a.s));
            qqVar.a(((a) cVar.b).b);
            qqVar.b(((a) cVar.b).f);
            qqVar.c(((a) cVar.b).g);
            qqVar.a(cVar.a.u);
            qqVar.a(cVar.a.x);
            qqVar.a(cVar.a.D);
            return qqVar;
        }

        /* access modifiers changed from: package-private */
        public void a(@NonNull qq qqVar, @NonNull sc scVar) {
            qqVar.b(scVar.j);
            qqVar.a(scVar.k);
        }
    }
}
