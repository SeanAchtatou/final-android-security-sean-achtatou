package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

public final class ao {
    private static volatile ao a;
    private static final Object b = new Object();
    /* access modifiers changed from: private */
    @NonNull
    public String c;

    public static ao a(@NonNull Context context) {
        if (a == null) {
            synchronized (b) {
                if (a == null) {
                    a = new ao(context.getApplicationContext());
                }
            }
        }
        return a;
    }

    private ao(Context context) {
        this.c = bj.a(context.getResources().getConfiguration().locale);
        cn.a().a(this, cv.class, cr.a(new cq<cv>() {
            public void a(cv cvVar) {
                String unused = ao.this.c = cvVar.a;
            }
        }).a());
    }

    @NonNull
    public String a() {
        return this.c;
    }
}
