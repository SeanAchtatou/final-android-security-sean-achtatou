package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.io.File;
import org.json.JSONException;
import org.json.JSONObject;

public class ls {
    private final vp a = new vp();

    public void a(@NonNull Context context, @NonNull lq lqVar, @NonNull nq nqVar) {
        try {
            lr b = b(context);
            String str = null;
            if (!(b == null || b.c() == null)) {
                str = b.c().a();
            }
            String a2 = new lr(lqVar, new lt(context, str, nqVar), System.currentTimeMillis()).a();
            if (a()) {
                a(context, a2);
            }
            a(context, "credentials.dat", a2);
        } catch (JSONException e) {
        }
    }

    @SuppressLint({"WorldReadableFiles"})
    private void a(Context context, String str, String str2) {
        ag.a(context, str, str2);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(Context context, String str) {
        ag.b(context, "credentials.dat", str);
    }

    @Nullable
    public lq a(@NonNull Context context) {
        lr b = b(context);
        if (b == null) {
            return null;
        }
        return b.b();
    }

    @Nullable
    private lr b(Context context) {
        lr b = b(context, context.getPackageName());
        if (b == null) {
            return null;
        }
        if (!a()) {
            return b;
        }
        lr c = c(context, context.getPackageName());
        return c == null ? b : c;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return ag.a();
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public lr b(Context context, String str) {
        return a(context, str, context.getFileStreamPath("credentials.dat"));
    }

    /* access modifiers changed from: package-private */
    @TargetApi(21)
    @Nullable
    public lr c(Context context, String str) {
        return a(context, str, new File(context.getNoBackupFilesDir(), "credentials.dat"));
    }

    @Nullable
    private lr a(Context context, String str, File file) {
        ApplicationInfo b = this.a.b(context, str, 8192);
        if (b != null) {
            return b(context, str, a(file, context.getApplicationInfo().dataDir, b.dataDir));
        }
        return null;
    }

    @Nullable
    private lr b(Context context, String str, String str2) {
        try {
            File file = new File(str2);
            if (file.exists()) {
                String a2 = ag.a(context, file);
                if (a2 == null) {
                    return null;
                }
                return new lr(new JSONObject(a2), file.lastModified());
            }
        } catch (Throwable th) {
        }
        return null;
    }

    @NonNull
    private String a(File file, String str, String str2) {
        return file.getAbsolutePath().replace(str, str2);
    }
}
