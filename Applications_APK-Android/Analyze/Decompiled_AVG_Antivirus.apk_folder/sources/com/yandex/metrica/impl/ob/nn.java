package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class nn implements no {
    @NonNull
    private final no a;
    @NonNull
    private final no b;

    @VisibleForTesting
    nn(@NonNull no noVar, @NonNull no noVar2) {
        this.a = noVar;
        this.b = noVar2;
    }

    public boolean a(@NonNull String str) {
        return this.b.a(str) && this.a.a(str);
    }

    public a a() {
        return new a(this.a, this.b);
    }

    public static a b() {
        return new a(new np(false), new nx(null));
    }

    public static class a {
        @NonNull
        private no a;
        @NonNull
        private no b;

        private a() {
        }

        public a(@NonNull no noVar, @NonNull no noVar2) {
            this.a = noVar;
            this.b = noVar2;
        }

        public a a(@NonNull sc scVar) {
            this.b = new nx(scVar.A);
            return this;
        }

        public a a(boolean z) {
            this.a = new np(z);
            return this;
        }

        public nn a() {
            return new nn(this.a, this.b);
        }
    }

    public String toString() {
        return "AskForPermissionsStrategy{mLocationFlagStrategy=" + this.a + ", mStartupStateStrategy=" + this.b + '}';
    }
}
