package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ii;
import com.yandex.metrica.impl.ob.lp;

public class ik {
    @NonNull
    private final Context a;
    @NonNull
    private final ii b;
    /* access modifiers changed from: private */
    @NonNull
    public final ij c;
    @NonNull
    private final a d;
    @NonNull
    private final Cif e;

    public static class a {
        public mc a(@NonNull Context context) {
            return ((sc) lp.a.a(sc.class).a(context).a()).q;
        }
    }

    public ik(@NonNull Context context, @NonNull uv uvVar, @NonNull ie ieVar) {
        this(context, uvVar, ieVar, new ij(context));
    }

    public void a() {
        a(this.d.a(this.a));
    }

    private void a(@Nullable mc mcVar) {
        if (mcVar != null) {
            boolean z = mcVar.k;
            boolean z2 = mcVar.c;
            Long a2 = this.e.a(mcVar.d);
            if (!z || a2 == null || a2.longValue() <= 0) {
                b();
            } else {
                this.b.a(a2.longValue(), z2);
            }
        }
    }

    private void b() {
        this.b.a();
    }

    public void a(@Nullable final il ilVar) {
        mc a2 = this.d.a(this.a);
        if (a2 != null) {
            long j = a2.a;
            if (j > 0) {
                this.c.a(this.a.getPackageName());
                this.b.a(j, new ii.a() {
                    public void a() {
                        ik.this.c.a();
                        ik.this.b(ilVar);
                    }
                });
            } else {
                b(ilVar);
            }
        } else {
            b(ilVar);
        }
        a(a2);
    }

    /* access modifiers changed from: private */
    public void b(@Nullable il ilVar) {
        if (ilVar != null) {
            ilVar.a();
        }
    }

    private ik(@NonNull Context context, @NonNull uv uvVar, @NonNull ie ieVar, @NonNull ij ijVar) {
        this(context, new ii(uvVar, ieVar), ijVar, new a(), new Cif(context));
    }

    @VisibleForTesting
    ik(@NonNull Context context, @NonNull ii iiVar, @NonNull ij ijVar, @NonNull a aVar, @NonNull Cif ifVar) {
        this.a = context;
        this.b = iiVar;
        this.c = ijVar;
        this.d = aVar;
        this.e = ifVar;
    }
}
