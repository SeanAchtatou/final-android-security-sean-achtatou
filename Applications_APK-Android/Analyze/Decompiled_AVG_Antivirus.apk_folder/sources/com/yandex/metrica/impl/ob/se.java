package com.yandex.metrica.impl.ob;

public class se {
    public final long a;

    public se(long j) {
        this.a = j;
    }

    public String toString() {
        return "StatSending{disabledReportingInterval=" + this.a + '}';
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && getClass() == o.getClass() && this.a == ((se) o).a) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long j = this.a;
        return (int) (j ^ (j >>> 32));
    }
}
