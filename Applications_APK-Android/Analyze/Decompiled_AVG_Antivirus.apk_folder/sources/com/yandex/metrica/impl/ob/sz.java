package com.yandex.metrica.impl.ob;

import android.util.Log;
import androidx.annotation.VisibleForTesting;

public abstract class sz {
    private volatile boolean a = false;

    /* access modifiers changed from: package-private */
    public abstract String c(String str, Object[] objArr);

    /* access modifiers changed from: package-private */
    public abstract String f();

    /* access modifiers changed from: package-private */
    public abstract String g();

    public void a() {
        this.a = true;
    }

    public void b() {
        this.a = false;
    }

    public boolean c() {
        return this.a;
    }

    public sz(boolean z) {
        this.a = z;
    }

    public void a(String str) {
        a(4, str);
    }

    public void b(String str) {
        a(5, str);
    }

    public void c(String str) {
        a(6, str);
    }

    public void a(String str, Object... objArr) {
        a(4, str, objArr);
    }

    public void b(String str, Object... objArr) {
        a(5, str, objArr);
    }

    /* access modifiers changed from: package-private */
    public void a(int i, String str) {
        if (d()) {
            Log.println(i, f(), d(str));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i, String str, Object... objArr) {
        if (d()) {
            Log.println(i, f(), d(str, objArr));
        }
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return this.a;
    }

    private String d(String str) {
        return g() + e(str);
    }

    private String d(String str, Object[] objArr) {
        try {
            return g() + c(e(str), objArr);
        } catch (Throwable th) {
            return e();
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public String e() {
        return g();
    }

    private static String e(String str) {
        return str == null ? "" : str;
    }
}
