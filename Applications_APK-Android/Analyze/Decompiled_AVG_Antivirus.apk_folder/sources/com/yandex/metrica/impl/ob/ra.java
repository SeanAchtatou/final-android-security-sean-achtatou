package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.Collections;
import java.util.List;

public class ra {
    @NonNull
    public final List<rd> a;
    @NonNull
    public final String b;
    public final long c;
    public final boolean d;
    public final boolean e;

    public ra(@NonNull List<rd> list, @NonNull String str, long j, boolean z, boolean z2) {
        this.a = Collections.unmodifiableList(list);
        this.b = str;
        this.c = j;
        this.d = z;
        this.e = z2;
    }

    public String toString() {
        return "SdkFingerprintingState{sdkItemList=" + this.a + ", etag='" + this.b + '\'' + ", lastAttemptTime=" + this.c + ", hasFirstCollectionOccurred=" + this.d + ", shouldRetry=" + this.e + '}';
    }
}
