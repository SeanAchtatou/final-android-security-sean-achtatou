package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.th;
import org.json.JSONException;

public class hv implements hu {
    protected kh a;
    private final String b;
    private th.a c;

    public hv(kh khVar, String str) {
        this.a = khVar;
        this.b = str;
        th.a aVar = new th.a();
        try {
            String c2 = this.a.c(this.b);
            if (!TextUtils.isEmpty(c2)) {
                aVar = new th.a(c2);
            }
        } catch (JSONException e) {
        }
        this.c = aVar;
    }

    @Nullable
    public Long b() {
        return this.c.d("SESSION_ID");
    }

    public hv d(long j) {
        a("SESSION_ID", Long.valueOf(j));
        return this;
    }

    @Nullable
    public Long c() {
        return this.c.d("SESSION_INIT_TIME");
    }

    public hv e(long j) {
        a("SESSION_INIT_TIME", Long.valueOf(j));
        return this;
    }

    @Nullable
    public Long d() {
        return this.c.d("SESSION_COUNTER_ID");
    }

    public hv a(long j) {
        a("SESSION_COUNTER_ID", Long.valueOf(j));
        return this;
    }

    @Nullable
    public Long e() {
        return this.c.d("SESSION_SLEEP_START");
    }

    public hv b(long j) {
        a("SESSION_SLEEP_START", Long.valueOf(j));
        return this;
    }

    @Nullable
    public Long f() {
        return this.c.d("SESSION_LAST_EVENT_OFFSET");
    }

    public hv c(long j) {
        a("SESSION_LAST_EVENT_OFFSET", Long.valueOf(j));
        return this;
    }

    @Nullable
    public Boolean g() {
        return this.c.e("SESSION_IS_ALIVE_REPORT_NEEDED");
    }

    public hv a(boolean z) {
        a("SESSION_IS_ALIVE_REPORT_NEEDED", Boolean.valueOf(z));
        return this;
    }

    public void h() {
        this.a.a(this.b, this.c.toString());
        this.a.n();
    }

    public boolean i() {
        return this.c.length() > 0;
    }

    private void a(String str, Object obj) {
        try {
            this.c.put(str, obj);
        } catch (JSONException e) {
        }
    }

    public void a() {
        this.c = new th.a();
        h();
    }
}
