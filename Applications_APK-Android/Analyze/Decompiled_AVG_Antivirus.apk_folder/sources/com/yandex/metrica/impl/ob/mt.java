package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

class mt {
    @NonNull
    private final mq a;
    @NonNull
    private final ml b;
    @NonNull
    private final mw c;
    @NonNull
    private final md d;

    public mt(@NonNull Context context, @NonNull sc scVar, @NonNull uw uwVar, @Nullable mh mhVar, @NonNull js jsVar, @NonNull jr jrVar, @NonNull nq nqVar) {
        this(context, uwVar, scVar, (LocationManager) context.getSystemService("location"), ch.a(context), af.a().k(), mhVar, new mw(context, scVar, mhVar, jsVar, jrVar, uwVar), new md(mhVar, jsVar, jrVar), nqVar);
    }

    private mt(@NonNull Context context, @NonNull uw uwVar, @NonNull sc scVar, @Nullable LocationManager locationManager, @NonNull ch chVar, @NonNull sv svVar, @Nullable mh mhVar, @NonNull mw mwVar, @NonNull md mdVar, @NonNull nq nqVar) {
        this(new mq(context, uwVar.b(), scVar, locationManager, mhVar, mwVar, mdVar, nqVar), new ml(context, chVar, svVar, mwVar, mdVar, uwVar, mhVar), mwVar, mdVar);
    }

    public void a() {
        this.a.a();
        this.b.d();
    }

    @Nullable
    public Location b() {
        return this.a.b();
    }

    @Nullable
    public Location c() {
        return this.a.c();
    }

    public void d() {
        this.c.a();
    }

    public void e() {
        this.a.d();
        this.b.a();
    }

    public void f() {
        this.a.e();
        this.b.b();
    }

    @VisibleForTesting
    mt(@NonNull mq mqVar, @NonNull ml mlVar, @NonNull mw mwVar, @NonNull md mdVar) {
        this.a = mqVar;
        this.b = mlVar;
        this.c = mwVar;
        this.d = mdVar;
    }

    public void a(@NonNull sc scVar, @Nullable mh mhVar) {
        this.c.a(scVar, mhVar);
        this.d.a(mhVar);
        this.a.a(scVar, mhVar);
        this.b.a(mhVar);
    }
}
