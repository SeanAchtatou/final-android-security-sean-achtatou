package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ab;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.di;
import com.yandex.metrica.impl.ob.qe;

public class du extends di {
    @NonNull
    private final qe b;
    @NonNull
    private final a c;
    @NonNull
    private final cd d;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public du(@androidx.annotation.NonNull android.content.Context r17, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.sc r18, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.bb r19, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.df r20, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.db.a r21, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.qe r22, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.cd r23, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.sf r24) {
        /*
            r16 = this;
            com.yandex.metrica.impl.ob.di$a r3 = new com.yandex.metrica.impl.ob.di$a
            r3.<init>()
            com.yandex.metrica.impl.ob.tw r4 = new com.yandex.metrica.impl.ob.tw
            r4.<init>()
            com.yandex.metrica.impl.ob.dv r15 = new com.yandex.metrica.impl.ob.dv
            com.yandex.metrica.impl.ob.dt r11 = new com.yandex.metrica.impl.ob.dt
            r2 = r23
            r11.<init>(r2)
            com.yandex.metrica.impl.ob.af r0 = com.yandex.metrica.impl.ob.af.a()
            com.yandex.metrica.impl.ob.vc r0 = r0.j()
            com.yandex.metrica.impl.ob.uv r13 = r0.g()
            java.lang.String r0 = r20.b()
            r1 = r17
            int r14 = com.yandex.metrica.impl.ob.cg.c(r1, r0)
            r5 = r15
            r6 = r17
            r7 = r20
            r8 = r21
            r9 = r24
            r10 = r18
            r12 = r19
            r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r0 = r16
            r2 = r20
            r6 = r22
            r7 = r23
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.du.<init>(android.content.Context, com.yandex.metrica.impl.ob.sc, com.yandex.metrica.impl.ob.bb, com.yandex.metrica.impl.ob.df, com.yandex.metrica.impl.ob.db$a, com.yandex.metrica.impl.ob.qe, com.yandex.metrica.impl.ob.cd, com.yandex.metrica.impl.ob.sf):void");
    }

    @VisibleForTesting
    du(@NonNull Context context, @NonNull df dfVar, @NonNull di.a aVar, @NonNull tx txVar, @NonNull dv dvVar, @NonNull qe qeVar, @NonNull cd cdVar) {
        super(context, dfVar, aVar, txVar, dvVar);
        this.b = qeVar;
        ff e = e();
        e.a(ab.a.EVENT_TYPE_REGULAR, new gt(e.a()));
        this.c = dvVar.a(this);
        this.b.a(this.c);
        this.d = cdVar;
    }

    public class a implements qe.a {
        public a() {
        }

        public boolean a(@NonNull qf qfVar) {
            du.this.a(new t().a(qfVar.a()).a(ab.a.EVENT_TYPE_SEND_REFERRER.a()));
            return true;
        }
    }

    public synchronized void a(@NonNull db.a aVar) {
        super.a(aVar);
        B();
        this.d.a(aVar.m);
    }

    private void B() {
        this.a.a(h().R()).n();
    }
}
