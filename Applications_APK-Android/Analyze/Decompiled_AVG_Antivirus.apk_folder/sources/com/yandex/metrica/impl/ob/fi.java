package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.di;
import com.yandex.metrica.impl.ob.fb;
import com.yandex.metrica.impl.ob.fp;

public class fi<T extends fp, C extends di> extends fb<T, C> {
    public fi(fh<T> fhVar, C c) {
        super(fhVar, c);
    }

    public boolean b(t tVar) {
        return a(tVar, new fb.a<T>() {
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public boolean a(T r1, com.yandex.metrica.impl.ob.t r2) {
                /*
                    r0 = this;
                    boolean r1 = r1.a(r2)
                    return r1
                */
                throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.fi.AnonymousClass1.a(com.yandex.metrica.impl.ob.fp, com.yandex.metrica.impl.ob.t):boolean");
            }
        });
    }
}
