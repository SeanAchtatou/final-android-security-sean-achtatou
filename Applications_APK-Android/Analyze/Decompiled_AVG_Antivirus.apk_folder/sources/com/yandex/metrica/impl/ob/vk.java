package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.Map;

public class vk {
    @NonNull
    private final vj a;

    public vk(@NonNull vj vjVar) {
        this.a = vjVar;
    }

    public void a(Map<String, String> map, String str, String str2) {
        if (map != null) {
            String a2 = this.a.a().a(str);
            if (map.size() < this.a.c().a() || map.containsKey(a2)) {
                map.put(a2, this.a.b().a(str2));
            } else {
                this.a.a(a2);
            }
        }
    }
}
