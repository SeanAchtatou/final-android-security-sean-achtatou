package com.yandex.metrica.impl.ob;

import android.os.Handler;
import java.lang.ref.WeakReference;

class am implements Runnable {
    private final WeakReference<Handler> a;
    private final WeakReference<m> b;

    am(Handler handler, m mVar) {
        this.a = new WeakReference<>(handler);
        this.b = new WeakReference<>(mVar);
    }

    public void run() {
        Handler handler = this.a.get();
        m mVar = this.b.get();
        if (handler != null && mVar != null && mVar.c()) {
            al.a(handler, mVar, this);
        }
    }
}
