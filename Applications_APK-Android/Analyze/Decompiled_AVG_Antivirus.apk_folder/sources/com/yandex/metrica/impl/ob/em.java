package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.Cdo;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.dr;

public interface em<C extends dr & Cdo> {
    @NonNull
    C a(@NonNull Context context, @NonNull df dfVar, @NonNull db.a aVar, @NonNull bb bbVar, @NonNull sd sdVar);

    @NonNull
    dp b(@NonNull Context context, @NonNull df dfVar, @NonNull db.a aVar, @NonNull bb bbVar, @NonNull sd sdVar);
}
