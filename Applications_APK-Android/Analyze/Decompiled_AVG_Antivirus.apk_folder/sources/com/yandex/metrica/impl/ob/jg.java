package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.FileObserver;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.io.File;

public class jg {
    @NonNull
    private final FileObserver a;
    @NonNull
    private final File b;
    @NonNull
    private final uc<File> c;
    @NonNull
    private final uv d;

    public jg(@NonNull Context context, @NonNull uc<File> ucVar) {
        this(ag.b(context), ucVar, af.a().j().i());
    }

    private jg(@NonNull File file, @NonNull uc<File> ucVar, @NonNull uv uvVar) {
        this(new ip(file, ucVar), file, ucVar, uvVar, new iq());
    }

    @VisibleForTesting
    jg(@NonNull FileObserver fileObserver, @NonNull File file, @NonNull uc<File> ucVar, @NonNull uv uvVar, @NonNull iq iqVar) {
        this.a = fileObserver;
        this.b = file;
        this.c = ucVar;
        this.d = uvVar;
        iqVar.a(file);
    }

    public void a() {
        this.d.a(new it(this.b, this.c));
        this.a.startWatching();
    }

    public void b() {
        this.a.stopWatching();
    }
}
