package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

class mp {
    @Nullable
    private String a;
    @Nullable
    private mh b;
    @NonNull
    private me c;
    @Nullable
    private Location d;
    private long e;
    @NonNull
    private tx f;
    @NonNull
    private mw g;
    @NonNull
    private md h;

    public mp(@Nullable String str, @Nullable mh mhVar, @NonNull me meVar, @NonNull mw mwVar, @NonNull md mdVar) {
        this(str, mhVar, meVar, null, 0, new tw(), mwVar, mdVar);
    }

    mp(@Nullable String str, @Nullable mh mhVar, @NonNull me meVar, @Nullable Location location, long j, @NonNull tx txVar, @NonNull mw mwVar, @NonNull md mdVar) {
        this.a = str;
        this.b = mhVar;
        this.c = meVar;
        this.d = location;
        this.e = j;
        this.f = txVar;
        this.g = mwVar;
        this.h = mdVar;
    }

    public void a(@Nullable Location location) {
        if (d(location)) {
            b(location);
            c(location);
            b();
            a();
        }
    }

    private void a() {
        this.h.a();
    }

    private void b() {
        this.g.a();
    }

    private void b(@Nullable Location location) {
        this.d = location;
        this.e = System.currentTimeMillis();
    }

    private void c(@Nullable Location location) {
        this.c.a(this.a, location, this.b);
    }

    private boolean d(@Nullable Location location) {
        if (location == null || this.b == null) {
            return false;
        }
        if (this.d == null) {
            return true;
        }
        boolean c2 = c();
        boolean e2 = e(location);
        boolean f2 = f(location);
        if ((c2 || e2) && f2) {
            return true;
        }
        return false;
    }

    private boolean c() {
        return this.f.a() - this.e > this.b.e;
    }

    private boolean e(Location location) {
        return g(location) > this.b.f;
    }

    private boolean f(@NonNull Location location) {
        return this.d == null || location.getTime() - this.d.getTime() >= 0;
    }

    private float g(Location location) {
        return location.distanceTo(this.d);
    }

    public void a(@NonNull sc scVar, @Nullable mh mhVar) {
        this.b = mhVar;
    }
}
