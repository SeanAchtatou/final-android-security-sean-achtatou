package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.List;

public class lx {
    @NonNull
    private final List<lv> a;
    @NonNull
    private final ly b;

    public lx(@NonNull List<lv> list, @NonNull ly lyVar) {
        this.a = list;
        this.b = lyVar;
    }

    public void a() {
        b();
    }

    private void b() {
        if (this.a.isEmpty()) {
            c();
            return;
        }
        boolean z = false;
        for (lv a2 : this.a) {
            z |= a2.a();
        }
        if (z) {
            c();
        }
    }

    private void c() {
        this.b.f();
    }
}
