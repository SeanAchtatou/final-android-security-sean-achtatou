package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
public class og extends oc {
    private static final oj d = new oj("PREF_KEY_OFFSET");
    private oj e = new oj(d.a(), null);

    public og(Context context, String str) {
        super(context, str);
    }

    /* access modifiers changed from: protected */
    public String f() {
        return "_servertimeoffset";
    }

    public long a(int i) {
        return this.c.getLong(this.e.b(), (long) i);
    }

    public void a() {
        h(this.e.b()).j();
    }
}
