package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class vr implements vx<String> {
    private final String a;

    public vr(@NonNull String str) {
        this.a = str;
    }

    public vv a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            return vv.a(this);
        }
        return vv.a(this, this.a + " is empty.");
    }
}
