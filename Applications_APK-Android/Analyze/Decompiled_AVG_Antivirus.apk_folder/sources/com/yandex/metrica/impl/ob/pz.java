package com.yandex.metrica.impl.ob;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.AppMetricaDeviceIDListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.YandexMetricaConfig;

public class pz extends qb {
    private static final vx<YandexMetricaConfig> g = new vt(new vs("Config"));
    private static final vx<String> h = new vt(new vr("Native crash"));
    private static final vx<Activity> i = new vt(new vs("Activity"));
    private static final vx<Application> j = new vt(new vs("Application"));
    private static final vx<Context> k = new vt(new vs("Context"));
    private static final vx<DeferredDeeplinkParametersListener> l = new vt(new vs("Deeplink listener"));
    private static final vx<AppMetricaDeviceIDListener> m = new vt(new vs("DeviceID listener"));
    private static final vx<ReporterConfig> n = new vt(new vs("Reporter Config"));
    private static final vx<String> o = new vt(new vr("Deeplink"));
    private static final vx<String> p = new vt(new vr("Referral url"));
    private static final vx<String> q = new vt(new vy());

    public void a(String str) {
        h.a(str);
    }

    public void a(@NonNull Application application) {
        j.a(application);
    }

    public void a(@NonNull Activity activity) {
        i.a(activity);
    }

    public void b(@NonNull String str) {
        o.a(str);
    }

    public void c(@NonNull String str) {
        p.a(str);
    }

    public void a(@Nullable Location location) {
    }

    public void a(boolean z) {
    }

    public void a(@NonNull Context context, boolean z) {
        k.a(context);
    }

    public void a(@NonNull DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        l.a(deferredDeeplinkParametersListener);
    }

    public void a(@NonNull AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        m.a(appMetricaDeviceIDListener);
    }

    public void b(@NonNull Context context, boolean z) {
        k.a(context);
    }

    public void a(@NonNull Context context, @NonNull String str) {
        k.a(context);
        q.a(str);
    }

    public void a(@NonNull Context context, @NonNull ReporterConfig reporterConfig) {
        k.a(context);
        n.a(reporterConfig);
    }

    public void a(@NonNull Context context, @NonNull YandexMetricaConfig yandexMetricaConfig) {
        k.a(context);
        g.a(yandexMetricaConfig);
    }
}
