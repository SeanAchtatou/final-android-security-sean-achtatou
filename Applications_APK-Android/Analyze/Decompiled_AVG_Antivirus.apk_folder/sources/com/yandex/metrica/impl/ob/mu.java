package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;

class mu {
    @NonNull
    private c a;
    @NonNull
    private a b;
    @NonNull
    private b c;
    @NonNull
    private Context d;
    @Nullable
    private mh e;
    @NonNull
    private sc f;
    @Nullable
    private mv g;
    @NonNull
    private mw h;
    @NonNull
    private md i;
    @Nullable
    private mi j;
    @NonNull
    private Map<String, mp> k;

    public static class b {
        @NonNull
        public mp a(@Nullable String str, @Nullable mh mhVar, @NonNull mv mvVar, @NonNull mw mwVar, @NonNull md mdVar) {
            return new mp(str, mhVar, mvVar, mwVar, mdVar);
        }
    }

    public static class c {
        @NonNull
        public mv a(@NonNull Context context, @Nullable me meVar) {
            return new mv(context, meVar);
        }
    }

    public static class a {
        @NonNull
        public mi a(me meVar) {
            return new mi(meVar);
        }
    }

    public mu(@NonNull Context context, @NonNull sc scVar, @Nullable mh mhVar, @NonNull mw mwVar, @NonNull md mdVar) {
        this(context, scVar, mhVar, new c(), new a(), new b(), mwVar, mdVar);
    }

    public void a(@NonNull Location location) {
        String provider = location.getProvider();
        mp mpVar = this.k.get(provider);
        if (mpVar == null) {
            mpVar = a(provider);
            this.k.put(provider, mpVar);
        } else {
            mpVar.a(this.f, this.e);
        }
        mpVar.a(location);
    }

    @Nullable
    public Location a() {
        mi miVar = this.j;
        if (miVar == null) {
            return null;
        }
        return miVar.a();
    }

    @NonNull
    private mp a(String str) {
        if (this.g == null) {
            this.g = this.a.a(this.d, null);
        }
        if (this.j == null) {
            this.j = this.b.a(this.g);
        }
        return this.c.a(str, this.e, this.g, this.h, this.i);
    }

    public void a(@NonNull sc scVar, @Nullable mh mhVar) {
        this.f = scVar;
        this.e = mhVar;
    }

    @VisibleForTesting
    mu(@NonNull Context context, @NonNull sc scVar, @Nullable mh mhVar, @NonNull c cVar, @NonNull a aVar, @NonNull b bVar, @NonNull mw mwVar, @NonNull md mdVar) {
        this.k = new HashMap();
        this.d = context;
        this.f = scVar;
        this.e = mhVar;
        this.a = cVar;
        this.b = aVar;
        this.c = bVar;
        this.h = mwVar;
        this.i = mdVar;
    }
}
