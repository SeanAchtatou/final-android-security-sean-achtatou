package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.lp;
import java.util.Collection;
import java.util.List;

public class gb extends ft {
    private final nu a;
    private kp<Collection<nt>> b;
    @NonNull
    private final l c;
    @NonNull
    private final j d;

    public gb(di diVar, nu nuVar) {
        this(diVar, nuVar, lp.a.b(nt.class).a(diVar.j()), new l(diVar.j()), new j());
    }

    @VisibleForTesting
    gb(di diVar, nu nuVar, @NonNull kp<Collection<nt>> kpVar, @NonNull l lVar, @NonNull j jVar) {
        super(diVar);
        this.a = nuVar;
        this.b = kpVar;
        this.c = lVar;
        this.d = jVar;
    }

    public boolean a(@NonNull t tVar) {
        di a2 = a();
        a2.b().toString();
        if (!a2.t().d() || !a2.q()) {
            return false;
        }
        Collection a3 = this.b.a();
        List<nt> a4 = this.a.a(a().j(), a3);
        if (a4 != null) {
            a(a4, tVar, a2.d());
            this.b.a(a4);
            return false;
        } else if (!a2.r()) {
            return false;
        } else {
            a(a3, tVar, a2.d());
            return false;
        }
    }

    private void a(@NonNull Collection<nt> collection, @NonNull t tVar, @NonNull ea eaVar) {
        eaVar.d(t.a(tVar, collection, this.c.a(), this.d));
    }
}
