package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class gd extends ft {
    private kf a;

    public gd(di diVar) {
        this(diVar, diVar.t());
    }

    @VisibleForTesting
    gd(di diVar, kf kfVar) {
        super(diVar);
        this.a = kfVar;
    }

    public boolean a(@NonNull t tVar) {
        t tVar2;
        di a2 = a();
        if (this.a.c()) {
            return false;
        }
        if (a2.h().P()) {
            tVar2 = t.f(tVar);
        } else {
            tVar2 = t.d(tVar);
        }
        a2.d().f(tVar2.c(this.a.d("")));
        this.a.a();
        this.a.e();
        return false;
    }
}
