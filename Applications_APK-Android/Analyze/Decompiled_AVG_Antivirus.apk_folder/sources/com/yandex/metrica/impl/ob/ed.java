package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.db;

public class ed extends di {
    private final String b;
    private final cd c;

    public ed(@NonNull Context context, @NonNull df dfVar, @NonNull bb bbVar, @NonNull db.a aVar, @NonNull cd cdVar, @NonNull sc scVar, @NonNull sf sfVar) {
        super(context, scVar, bbVar, dfVar, aVar, new eb(cdVar), sfVar);
        this.b = dfVar.a();
        this.c = cdVar;
    }

    public synchronized void a(@NonNull db.a aVar) {
        super.a(aVar);
        this.c.a(this.b, aVar.m);
    }
}
