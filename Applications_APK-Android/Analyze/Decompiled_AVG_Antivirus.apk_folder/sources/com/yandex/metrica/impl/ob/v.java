package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.yandex.metrica.impl.ob.i;
import org.json.JSONException;
import org.json.JSONObject;

public final class v {
    @NonNull
    private Context a;
    /* access modifiers changed from: private */
    public ContentValues b;
    private qp c;

    public v(Context context) {
        this.a = context;
    }

    public v a(ContentValues contentValues) {
        this.b = contentValues;
        return this;
    }

    public v a(@NonNull qp qpVar) {
        this.c = qpVar;
        return this;
    }

    public void a() {
        c();
    }

    private void c() {
        JSONObject jSONObject = new JSONObject();
        try {
            a(jSONObject);
        } catch (Throwable th) {
            jSONObject = new JSONObject();
        }
        this.b.put("report_request_parameters", jSONObject.toString());
    }

    private void a(@NonNull JSONObject jSONObject) throws JSONException {
        jSONObject.putOpt("dId", this.c.r()).putOpt("uId", this.c.t()).putOpt("appVer", this.c.q()).putOpt("appBuild", this.c.p()).putOpt("analyticsSdkVersionName", this.c.i()).putOpt("kitBuildNumber", this.c.j()).putOpt("kitBuildType", this.c.k()).putOpt("osVer", this.c.n()).putOpt("osApiLev", Integer.valueOf(this.c.o())).putOpt("lang", this.c.A()).putOpt("root", this.c.u()).putOpt("app_debuggable", this.c.E()).putOpt("app_framework", this.c.v()).putOpt("attribution_id", Integer.valueOf(this.c.V()));
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull Location location) throws JSONException {
        th.a(jSONObject, location);
    }

    private void d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("enabled", this.c.N());
            Location b2 = b();
            if (b2 != null) {
                a(jSONObject, b2);
            }
            this.b.put("location_info", jSONObject.toString());
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public Location b() {
        if (!this.c.N()) {
            return null;
        }
        Location O = this.c.O();
        if (O != null) {
            return O;
        }
        Location a2 = mo.a(this.a).a();
        if (a2 == null) {
            return mo.a(this.a).b();
        }
        return a2;
    }

    private void b(@NonNull ch chVar) {
        this.b.put("wifi_network_info", chVar.a().toString());
    }

    private void a(sl slVar) {
        slVar.a(new sn() {
            public void a(sm[] smVarArr) {
                v.this.b.put("cell_info", th.a(smVarArr).toString());
            }
        });
    }

    private void a(i.a aVar) {
        this.b.put("app_environment", aVar.a);
        this.b.put("app_environment_revision", Long.valueOf(aVar.b));
    }

    private void e() {
        sv k = af.a().k();
        k.a(new sy() {
            public void a(sx sxVar) {
                sm b = sxVar.b();
                if (b != null) {
                    v.this.b.put("cellular_connection_type", b.g());
                }
            }
        });
        a(k);
    }

    public void a(@NonNull uk ukVar, @NonNull i.a aVar) {
        t tVar = ukVar.a;
        this.b.put("name", tVar.d());
        this.b.put("value", tVar.e());
        this.b.put("type", Integer.valueOf(tVar.g()));
        this.b.put("custom_type", Integer.valueOf(tVar.h()));
        this.b.put("error_environment", tVar.j());
        this.b.put("user_info", tVar.l());
        this.b.put("truncated", Integer.valueOf(tVar.o()));
        this.b.put(UrlManager.Parameter.CONNECTION_TYPE, Integer.valueOf(bj.e(this.a)));
        this.b.put("profile_id", tVar.p());
        this.b.put("encrypting_mode", Integer.valueOf(ukVar.b.a()));
        this.b.put("first_occurrence_status", Integer.valueOf(ukVar.a.q().d));
        a(aVar);
        d();
        f();
    }

    private void f() {
        e();
        ch a2 = ch.a(this.a);
        b(a2);
        a(a2);
    }

    /* access modifiers changed from: package-private */
    public void a(ch chVar) {
        String b2 = chVar.b(this.a);
        if (!TextUtils.isEmpty(b2)) {
            int c2 = chVar.c(this.a);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("ssid", b2);
                jSONObject.put("state", c2);
                this.b.put("wifi_access_point", jSONObject.toString());
            } catch (Throwable th) {
            }
        }
    }
}
