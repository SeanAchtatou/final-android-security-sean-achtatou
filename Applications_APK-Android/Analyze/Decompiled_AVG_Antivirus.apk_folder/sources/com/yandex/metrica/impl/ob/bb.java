package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;

public class bb extends uz {
    @NonNull
    private final Executor a;
    private Executor b;
    private final BlockingQueue<a> c = new LinkedBlockingQueue();
    private final Object d = new Object();
    private final Object e = new Object();
    private volatile a f;
    @NonNull
    private ni g;
    private String h;

    public bb(@NonNull Context context, @NonNull df dfVar, @NonNull Executor executor) {
        this.a = executor;
        this.b = new ur();
        this.h = String.format(Locale.US, "[%s:%s]", "NetworkTaskQueue", dfVar.toString());
        this.g = new ni(context);
    }

    public void a(be beVar) {
        synchronized (this.d) {
            a aVar = new a(beVar);
            if (!a(aVar)) {
                this.c.offer(aVar);
            }
        }
    }

    public void a() {
        synchronized (this.e) {
            a aVar = this.f;
            if (aVar != null) {
                aVar.a.w();
            }
            this.c.clear();
            b();
        }
    }

    public void run() {
        while (c()) {
            try {
                synchronized (this.e) {
                }
                this.f = this.c.take();
                be beVar = this.f.a;
                c(beVar).execute(b(beVar));
                synchronized (this.e) {
                    this.f = null;
                }
            } catch (InterruptedException e2) {
                synchronized (this.e) {
                    this.f = null;
                }
            } catch (Throwable th) {
                synchronized (this.e) {
                    this.f = null;
                    throw th;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @NonNull
    public bh b(@NonNull be beVar) {
        return new bh(this.g, beVar, this, this.h);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public Executor c(be beVar) {
        if (beVar.o()) {
            return this.a;
        }
        return this.b;
    }

    private boolean a(a aVar) {
        return this.c.contains(aVar) || aVar.equals(this.f);
    }

    private static class a {
        @NonNull
        final be a;
        @NonNull
        private final String b;

        private a(@NonNull be beVar) {
            this.a = beVar;
            this.b = beVar.n();
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            return this.b.equals(((a) o).b);
        }

        public int hashCode() {
            return this.b.hashCode();
        }
    }
}
