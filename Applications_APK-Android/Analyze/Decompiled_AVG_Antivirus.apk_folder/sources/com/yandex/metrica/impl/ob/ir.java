package com.yandex.metrica.impl.ob;

import android.util.SparseArray;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.mopub.common.Constants;
import com.yandex.metrica.impl.ob.ab;

public class ir {
    private static SparseArray<ir> c = new SparseArray<>();
    public final String a;
    public final String b;

    static {
        c.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED.a(), new ir("jvm", "binder"));
        c.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF.a(), new ir("jvm", "binder"));
        c.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT.a(), new ir("jvm", Constants.INTENT_SCHEME));
        c.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE.a(), new ir("jvm", "file"));
        c.put(ab.a.EVENT_TYPE_NATIVE_CRASH.a(), new ir("jni_native", "file"));
    }

    private ir(@NonNull String str, @NonNull String str2) {
        this.a = str;
        this.b = str2;
    }

    public static ir a(int i) {
        return c.get(i);
    }
}
