package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.n;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;

class mi extends mf {
    public static final long a = TimeUnit.MINUTES.toMillis(2);
    public static final long b = TimeUnit.SECONDS.toMillis(10);
    @Nullable
    private n.a<Location> c;
    @NonNull
    private a d;
    private long e;

    public static class a {
        public final long a;
        public final long b;
        public final long c;

        public a(long j, long j2, long j3) {
            this.a = j;
            this.b = j2;
            this.c = j3;
        }
    }

    public mi(@Nullable me meVar) {
        this(meVar, new a(a, 200, 50), b);
    }

    public void b(@Nullable String str, @Nullable Location location, @Nullable mh mhVar) {
        if (location != null) {
            a(location);
        }
    }

    private void a(@NonNull Location location) {
        n.a<Location> aVar = this.c;
        if (aVar == null || aVar.a(this.e) || a(location, this.c.a())) {
            Location location2 = new Location(location);
            n.a<Location> aVar2 = new n.a<>();
            aVar2.a((JSONArray) location2);
            this.c = aVar2;
        }
    }

    @Nullable
    public Location a() {
        n.a<Location> aVar = this.c;
        if (aVar == null) {
            return null;
        }
        return aVar.d();
    }

    private boolean a(@Nullable Location location, @Nullable Location location2) {
        return a(location, location2, this.d.a, this.d.b);
    }

    public static boolean a(@Nullable Location location, @Nullable Location location2, long j, long j2) {
        if (location2 == null) {
            return true;
        }
        if (location == null) {
            return false;
        }
        long time = location.getTime() - location2.getTime();
        boolean z = time > j;
        boolean z2 = time < (-j);
        boolean z3 = time > 0;
        if (z) {
            return true;
        }
        if (z2) {
            return false;
        }
        int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
        boolean z4 = accuracy > 0;
        boolean z5 = accuracy < 0;
        boolean z6 = ((long) accuracy) > j2;
        boolean a2 = a(location.getProvider(), location2.getProvider());
        if (z5) {
            return true;
        }
        if (z3 && !z4) {
            return true;
        }
        if (!z3 || z6 || !a2) {
            return false;
        }
        return true;
    }

    static boolean a(@Nullable String str, @Nullable String str2) {
        if (str == null) {
            return str2 == null;
        }
        return str.equals(str2);
    }

    @VisibleForTesting
    mi(@Nullable me meVar, @NonNull a aVar, long j) {
        super(meVar);
        this.d = aVar;
        this.e = j;
    }
}
