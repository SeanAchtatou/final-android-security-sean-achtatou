package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.kb;
import java.util.HashMap;
import java.util.List;

public class jp {
    @NonNull
    private final HashMap<String, List<String>> a = new HashMap<>();

    public jp() {
        this.a.put("reports", kb.f.a);
        this.a.put("sessions", kb.g.a);
        this.a.put("preferences", kb.d.a);
        this.a.put("binary_data", kb.b.a);
    }

    @NonNull
    public HashMap<String, List<String>> a() {
        return this.a;
    }
}
