package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;

public class tt {
    private volatile long a;
    private kj b;
    private tx c;

    private static class a {
        static tt a = new tt();
    }

    public static tt a() {
        return a.a;
    }

    private tt() {
    }

    public synchronized long b() {
        return this.a;
    }

    public synchronized void a(@NonNull Context context) {
        a(new kj(jo.a(context).c()), new tw());
    }

    public synchronized void a(long j, @Nullable Long l) {
        this.a = (j - this.c.a()) / 1000;
        if (this.b.c(true)) {
            boolean z = false;
            if (l != null) {
                long abs = Math.abs(j - this.c.a());
                kj kjVar = this.b;
                if (abs > TimeUnit.SECONDS.toMillis(l.longValue())) {
                    z = true;
                }
                kjVar.d(z);
            } else {
                this.b.d(false);
            }
        }
        this.b.a(this.a);
        this.b.n();
    }

    public synchronized void c() {
        this.b.d(false);
        this.b.n();
    }

    public synchronized boolean d() {
        return this.b.c(true);
    }

    @VisibleForTesting
    public void a(kj kjVar, tx txVar) {
        this.b = kjVar;
        this.a = this.b.c(0);
        this.c = txVar;
    }
}
