package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Build;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.Collection;
import java.util.List;

public class nu {
    @Nullable
    public List<nt> a(Context context, @Nullable Collection<nt> collection) {
        List<nt> a = a(context).a();
        if (ta.a(a, collection)) {
            return null;
        }
        return a;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public ns a(Context context) {
        if (Build.VERSION.SDK_INT >= 16) {
            return new nv(context);
        }
        return new ny(context);
    }
}
