package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Parcel;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.lp;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;

public class mo implements n {
    public static final long a = TimeUnit.MINUTES.toMillis(1);
    @SuppressLint({"StaticFieldLeak"})
    private static volatile mo b;
    private static final Object c = new Object();
    @NonNull
    private final Context d;
    @NonNull
    private uw e;
    @NonNull
    private final WeakHashMap<Object, Object> f;
    private boolean g;
    /* access modifiers changed from: private */
    @Nullable
    public mh h;
    /* access modifiers changed from: private */
    @NonNull
    public sc i;
    /* access modifiers changed from: private */
    @Nullable
    public mt j;
    @NonNull
    private a k;
    private Runnable l;
    @NonNull
    private js m;
    @NonNull
    private jr n;
    private final nq o;
    private final nw p;
    private boolean q;
    private final Object r;
    /* access modifiers changed from: private */
    public final Object s;

    static class a {
        a() {
        }

        @NonNull
        public mt a(@NonNull Context context, @NonNull uw uwVar, @NonNull sc scVar, @Nullable mh mhVar, @NonNull js jsVar, @NonNull jr jrVar, @NonNull nq nqVar) {
            return new mt(context, scVar, uwVar, mhVar, jsVar, jrVar, nqVar);
        }
    }

    public static mo a(Context context) {
        if (b == null) {
            synchronized (c) {
                if (b == null) {
                    b = new mo(context.getApplicationContext());
                }
            }
        }
        return b;
    }

    private mo(@NonNull Context context) {
        this(context, af.a().j().e(), new a(), jo.a(context).g(), jo.a(context).h(), (sc) lp.a.a(sc.class).a(context).a());
    }

    private void c() {
        this.e.a(new Runnable() {
            public void run() {
                try {
                    if (mo.this.j != null) {
                        mo.this.j.a();
                    }
                } catch (Throwable th) {
                }
            }
        });
    }

    public void a(@Nullable Object obj) {
        synchronized (this.r) {
            this.f.put(obj, null);
            d();
        }
    }

    public void b(@Nullable Object obj) {
        synchronized (this.r) {
            this.f.remove(obj);
            d();
        }
    }

    private void d() {
        if (this.q) {
            if (!this.g || this.f.isEmpty()) {
                e();
                this.q = false;
            }
        } else if (this.g && !this.f.isEmpty()) {
            f();
            this.q = true;
        }
    }

    private void e() {
        mt mtVar = this.j;
        if (mtVar != null) {
            mtVar.f();
        }
        h();
    }

    private void f() {
        if (this.j == null) {
            synchronized (this.s) {
                this.j = this.k.a(this.d, this.e, this.i, this.h, this.m, this.n, this.o);
            }
        }
        this.j.e();
        g();
        c();
    }

    @Nullable
    public Location a() {
        mt mtVar = this.j;
        if (mtVar == null) {
            return null;
        }
        return mtVar.b();
    }

    @Nullable
    public Location b() {
        mt mtVar = this.j;
        if (mtVar == null) {
            return null;
        }
        return mtVar.c();
    }

    private void g() {
        if (this.l == null) {
            this.l = new Runnable() {
                public void run() {
                    mt a2 = mo.this.j;
                    if (a2 != null) {
                        a2.d();
                    }
                    mo.this.i();
                }
            };
            i();
        }
    }

    private void h() {
        Runnable runnable = this.l;
        if (runnable != null) {
            this.e.b(runnable);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        this.e.a(this.l, a);
    }

    public static byte[] a(@Nullable Location location) {
        if (location != null) {
            Parcel obtain = Parcel.obtain();
            try {
                obtain.writeValue(location);
                return obtain.marshall();
            } catch (Throwable th) {
            } finally {
                obtain.recycle();
            }
        }
        return null;
    }

    public static Location a(@NonNull byte[] bArr) {
        if (bArr != null) {
            Parcel obtain = Parcel.obtain();
            try {
                obtain.unmarshall(bArr, 0, bArr.length);
                obtain.setDataPosition(0);
                return (Location) obtain.readValue(Location.class.getClassLoader());
            } catch (Throwable th) {
            } finally {
                obtain.recycle();
            }
        }
        return null;
    }

    public void a(boolean z) {
        synchronized (this.r) {
            if (this.g != z) {
                this.g = z;
                this.p.a(z);
                this.o.a(this.p.a());
                d();
            }
        }
    }

    public void a(@NonNull sc scVar, @Nullable mh mhVar) {
        synchronized (this.r) {
            this.i = scVar;
            this.h = mhVar;
            this.p.a(scVar);
            this.o.a(this.p.a());
        }
        this.e.a(new Runnable() {
            public void run() {
                synchronized (mo.this.s) {
                    if (mo.this.j != null) {
                        mo.this.j.a(mo.this.i, mo.this.h);
                    }
                }
            }
        });
    }

    @VisibleForTesting
    mo(@NonNull Context context, @NonNull uw uwVar, @NonNull a aVar, @NonNull js jsVar, @NonNull jr jrVar, @NonNull sc scVar) {
        this.g = false;
        this.p = new nw();
        this.q = false;
        this.r = new Object();
        this.s = new Object();
        this.d = context;
        this.e = uwVar;
        this.f = new WeakHashMap<>();
        this.k = aVar;
        this.m = jsVar;
        this.n = jrVar;
        this.i = scVar;
        this.o = new nq(this.p.a());
    }
}
