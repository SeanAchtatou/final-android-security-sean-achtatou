package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.c;
import com.yandex.metrica.impl.ob.pq;

public class rf {
    @NonNull
    private final rg a;
    @NonNull
    private final c b;

    public rf(@NonNull Context context) {
        this(new rg(), rh.a(context));
    }

    public void a(@NonNull pq.a.b bVar) {
        this.b.a("provided_request_result", this.a.a(bVar));
    }

    public void a(@NonNull pq.a.C0017a aVar) {
        this.b.a("provided_request_schedule", this.a.a(aVar));
    }

    public void b(@NonNull pq.a.C0017a aVar) {
        this.b.a("provided_request_send", this.a.a(aVar));
    }

    @VisibleForTesting
    rf(@NonNull rg rgVar, @NonNull c cVar) {
        this.a = rgVar;
        this.b = cVar;
    }
}
