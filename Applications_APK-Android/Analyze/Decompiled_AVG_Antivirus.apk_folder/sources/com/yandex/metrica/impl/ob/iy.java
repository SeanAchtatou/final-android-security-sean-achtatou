package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

public class iy {
    @Nullable
    public final String a;
    @Nullable
    public final Throwable b;
    @Nullable
    public final iu c;

    public iy(@Nullable Throwable th, @Nullable iu iuVar) {
        this.b = th;
        if (th == null) {
            this.a = "";
        } else {
            this.a = th.getClass().getName();
        }
        this.c = iuVar;
    }

    @Deprecated
    @Nullable
    public Throwable a() {
        return this.b;
    }
}
