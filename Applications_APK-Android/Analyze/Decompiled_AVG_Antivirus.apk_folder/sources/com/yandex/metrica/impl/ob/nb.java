package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.mh;
import com.yandex.metrica.impl.ob.pg;
import java.util.concurrent.TimeUnit;

public class nb {
    @NonNull
    public pg.b.C0005b a(@NonNull ms msVar) {
        pg.b.C0005b bVar = new pg.b.C0005b();
        Location c = msVar.c();
        bVar.b = msVar.a() == null ? bVar.b : msVar.a().longValue();
        bVar.d = TimeUnit.MILLISECONDS.toSeconds(c.getTime());
        bVar.l = a(msVar.a);
        bVar.c = TimeUnit.MILLISECONDS.toSeconds(msVar.b());
        bVar.m = TimeUnit.MILLISECONDS.toSeconds(msVar.d());
        bVar.e = c.getLatitude();
        bVar.f = c.getLongitude();
        bVar.g = Math.round(c.getAccuracy());
        bVar.h = Math.round(c.getBearing());
        bVar.i = Math.round(c.getSpeed());
        bVar.j = (int) Math.round(c.getAltitude());
        bVar.k = a(c.getProvider());
        return bVar;
    }

    /* renamed from: com.yandex.metrica.impl.ob.nb$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[mh.a.values().length];

        static {
            try {
                a[mh.a.FOREGROUND.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[mh.a.BACKGROUND.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    private static int a(@NonNull mh.a aVar) {
        int i = AnonymousClass1.a[aVar.ordinal()];
        return (i == 1 || i != 2) ? 0 : 1;
    }

    private static int a(@NonNull String str) {
        if ("gps".equals(str)) {
            return 1;
        }
        if ("network".equals(str)) {
            return 2;
        }
        return 0;
    }
}
