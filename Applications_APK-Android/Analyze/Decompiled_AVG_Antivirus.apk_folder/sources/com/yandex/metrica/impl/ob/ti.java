package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.HashMap;
import java.util.Map;

public abstract class ti {
    private static Map<String, tp> a = new HashMap();
    private static Map<String, tg> b = new HashMap();

    @NonNull
    public static tp a(@NonNull String str) {
        if (!a.containsKey(str)) {
            a.put(str, new tp(str));
        }
        return a.get(str);
    }

    @NonNull
    public static tg b(@NonNull String str) {
        if (!b.containsKey(str)) {
            b.put(str, new tg(str));
        }
        return b.get(str);
    }

    @NonNull
    public static tp a() {
        return tp.h();
    }

    @NonNull
    public static tg b() {
        return tg.h();
    }
}
