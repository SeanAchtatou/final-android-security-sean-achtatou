package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class nz {
    private Context a;

    public nz(Context context) {
        this.a = context;
    }

    public void a() {
        SharedPreferences a2 = ok.a(this.a, "_bidoptpreferences");
        if (a2.getAll().size() > 0) {
            b(a2);
            a(a2);
            a2.edit().clear().apply();
        }
    }

    private void a(SharedPreferences sharedPreferences) {
        Map<String, ?> all = sharedPreferences.getAll();
        if (all.size() > 0) {
            for (String next : a(all, oh.e.a())) {
                String string = sharedPreferences.getString(new oj(oh.e.a(), next).b(), null);
                oh ohVar = new oh(this.a, next);
                if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(ohVar.b(null))) {
                    ohVar.i(string).j();
                }
            }
        }
    }

    private List<String> a(Map<String, ?> map, String str) {
        ArrayList arrayList = new ArrayList();
        for (String next : map.keySet()) {
            if (next.startsWith(str)) {
                arrayList.add(next.replace(str, ""));
            }
        }
        return arrayList;
    }

    private void b(SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString(oh.d.a(), null);
        oh ohVar = new oh(this.a);
        if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(ohVar.a((String) null))) {
            ohVar.j(string).j();
            sharedPreferences.edit().remove(oh.d.a()).apply();
        }
    }

    public void b() {
        jq e = jo.a(this.a).e();
        SharedPreferences a2 = ok.a(this.a, "_startupserviceinfopreferences");
        b(e, a2);
        c(e, a2);
        a(e, this.a.getPackageName());
        a(e, a2);
    }

    private void a(jq jqVar, SharedPreferences sharedPreferences) {
        for (String a2 : a(sharedPreferences.getAll(), oh.e.a())) {
            a(jqVar, a2);
        }
    }

    private void a(jq jqVar, String str) {
        kk kkVar = new kk(jqVar, str);
        oh ohVar = new oh(this.a, str);
        String b = ohVar.b(null);
        if (!TextUtils.isEmpty(b)) {
            kkVar.a(b);
        }
        String a2 = ohVar.a();
        if (!TextUtils.isEmpty(a2)) {
            kkVar.h(a2);
        }
        String d = ohVar.d(null);
        if (!TextUtils.isEmpty(d)) {
            kkVar.g(d);
        }
        String f = ohVar.f(null);
        if (!TextUtils.isEmpty(f)) {
            kkVar.e(f);
        }
        String g = ohVar.g(null);
        if (!TextUtils.isEmpty(g)) {
            kkVar.d(g);
        }
        String c = ohVar.c(null);
        if (!TextUtils.isEmpty(c)) {
            kkVar.f(c);
        }
        long a3 = ohVar.a(-1);
        if (a3 != -1) {
            kkVar.a(a3);
        }
        String e = ohVar.e(null);
        if (!TextUtils.isEmpty(e)) {
            kkVar.c(e);
        }
        kkVar.n();
        ohVar.b();
    }

    private void b(jq jqVar, SharedPreferences sharedPreferences) {
        kk kkVar = new kk(jqVar, null);
        String string = sharedPreferences.getString(oh.d.a(), null);
        if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(kkVar.a().b)) {
            kkVar.b(string).n();
            sharedPreferences.edit().remove(oh.d.a()).apply();
        }
    }

    private void c(jq jqVar, SharedPreferences sharedPreferences) {
        kk kkVar = new kk(jqVar, this.a.getPackageName());
        boolean z = sharedPreferences.getBoolean(oh.f.a(), false);
        if (z) {
            kkVar.a(z).n();
        }
    }
}
