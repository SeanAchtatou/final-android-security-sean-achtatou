package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class qd implements InvocationHandler {
    @NonNull
    private Object a;
    @NonNull
    private final qg b;

    qd(@NonNull Object obj, @NonNull qg qgVar) {
        this.a = obj;
        this.b = qgVar;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (!"onInstallReferrerSetupFinished".equals(method.getName())) {
            qg qgVar = this.b;
            qgVar.a(new IllegalArgumentException("Unexpected method called " + method.getName()));
            return null;
        } else if (args.length != 1) {
            this.b.a(new IllegalArgumentException("Args size is not equal to one."));
            return null;
        } else if (args[0].equals(0)) {
            try {
                Object invoke = this.a.getClass().getMethod("getInstallReferrer", new Class[0]).invoke(this.a, new Object[0]);
                this.b.a(new qf((String) invoke.getClass().getMethod("getInstallReferrer", new Class[0]).invoke(invoke, new Object[0]), ((Long) invoke.getClass().getMethod("getReferrerClickTimestampSeconds", new Class[0]).invoke(invoke, new Object[0])).longValue(), ((Long) invoke.getClass().getMethod("getInstallBeginTimestampSeconds", new Class[0]).invoke(invoke, new Object[0])).longValue()));
                return null;
            } catch (Throwable th) {
                this.b.a(th);
                return null;
            }
        } else {
            qg qgVar2 = this.b;
            qgVar2.a(new IllegalStateException("Referrer check failed with error " + args[0]));
            return null;
        }
    }
}
