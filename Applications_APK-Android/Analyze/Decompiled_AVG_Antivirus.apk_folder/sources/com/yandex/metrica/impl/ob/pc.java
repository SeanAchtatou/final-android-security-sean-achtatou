package com.yandex.metrica.impl.ob;

import android.util.SparseArray;

public class pc {
    private static SparseArray<String> a = new SparseArray<>();

    static {
        a.put(0, "String");
        a.put(1, "Number");
        a.put(2, "Counter");
    }

    static String a(int i) {
        return a.get(i);
    }
}
