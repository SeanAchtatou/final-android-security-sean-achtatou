package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.bj;
import com.yandex.metrica.impl.ob.pn;
import com.yandex.metrica.impl.ob.pq;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class lh implements lc<pq.a, pn.a> {
    private static final Map<Integer, bj.a> a = Collections.unmodifiableMap(new HashMap<Integer, bj.a>() {
        {
            put(1, bj.a.WIFI);
            put(2, bj.a.CELL);
        }
    });
    private static final Map<bj.a, Integer> b = Collections.unmodifiableMap(new HashMap<bj.a, Integer>() {
        {
            put(bj.a.WIFI, 1);
            put(bj.a.CELL, 2);
        }
    });

    @NonNull
    /* renamed from: a */
    public pn.a b(@NonNull pq.a aVar) {
        pn.a aVar2 = new pn.a();
        Set<String> a2 = aVar.a();
        aVar2.c = (String[]) a2.toArray(new String[a2.size()]);
        aVar2.b = b(aVar);
        return aVar2;
    }

    @NonNull
    public pq.a a(@NonNull pn.a aVar) {
        return new pq.a(b(aVar), Arrays.asList(aVar.c));
    }

    @NonNull
    private List<pq.a.C0017a> b(@NonNull pn.a aVar) {
        ArrayList arrayList = new ArrayList();
        for (pn.a.C0011a aVar2 : aVar.b) {
            arrayList.add(new pq.a.C0017a(aVar2.b, aVar2.c, aVar2.d, a(aVar2.e), aVar2.f, a(aVar2.g)));
        }
        return arrayList;
    }

    @NonNull
    private ud<String, String> a(@NonNull pn.a.C0011a.C0012a[] aVarArr) {
        ud<String, String> udVar = new ud<>();
        for (pn.a.C0011a.C0012a aVar : aVarArr) {
            udVar.a(aVar.b, aVar.c);
        }
        return udVar;
    }

    private pn.a.C0011a[] b(@NonNull pq.a aVar) {
        List<pq.a.C0017a> b2 = aVar.b();
        pn.a.C0011a[] aVarArr = new pn.a.C0011a[b2.size()];
        for (int i = 0; i < b2.size(); i++) {
            aVarArr[i] = a(b2.get(i));
        }
        return aVarArr;
    }

    @NonNull
    private pn.a.C0011a a(@NonNull pq.a.C0017a aVar) {
        pn.a.C0011a aVar2 = new pn.a.C0011a();
        aVar2.b = aVar.a;
        aVar2.c = aVar.b;
        aVar2.e = b(aVar);
        aVar2.d = aVar.c;
        aVar2.f = aVar.e;
        aVar2.g = a(aVar.f);
        return aVar2;
    }

    @NonNull
    private pn.a.C0011a.C0012a[] b(@NonNull pq.a.C0017a aVar) {
        pn.a.C0011a.C0012a[] aVarArr = new pn.a.C0011a.C0012a[aVar.d.a()];
        int i = 0;
        for (Map.Entry entry : aVar.d.b()) {
            for (String str : (Collection) entry.getValue()) {
                pn.a.C0011a.C0012a aVar2 = new pn.a.C0011a.C0012a();
                aVar2.b = (String) entry.getKey();
                aVar2.c = str;
                aVarArr[i] = aVar2;
                i++;
            }
        }
        return aVarArr;
    }

    @NonNull
    private List<bj.a> a(@NonNull int[] iArr) {
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(a.get(Integer.valueOf(valueOf)));
        }
        return arrayList;
    }

    @NonNull
    private int[] a(@NonNull List<bj.a> list) {
        int[] iArr = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            iArr[i] = b.get(list.get(i)).intValue();
        }
        return iArr;
    }
}
