package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class q {
    @Nullable
    private Long a;
    @NonNull
    private tw b;

    public q() {
        this(new tw());
    }

    public void a() {
        this.a = Long.valueOf(this.b.c());
    }

    @VisibleForTesting
    q(@NonNull tw twVar) {
        this.b = twVar;
    }
}
