package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public abstract class iv {
    private final a a;

    public interface a {
        boolean a(Throwable th);
    }

    /* access modifiers changed from: package-private */
    public abstract void b(@NonNull iy iyVar);

    iv(a aVar) {
        this.a = aVar;
    }

    public void a(@NonNull iy iyVar) {
        if (this.a.a(iyVar.a())) {
            b(iyVar);
        }
    }
}
