package com.yandex.metrica.impl.ob;

public class oj {
    private final String a;
    private final String b;

    public oj(String str) {
        this(str, null);
    }

    public oj(String str, String str2) {
        this.a = str;
        this.b = a(str2);
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public final String a(String str) {
        if (str == null) {
            return this.a;
        }
        return this.a + str;
    }
}
