package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.pp;
import com.yandex.metrica.impl.ob.rq;
import com.yandex.metrica.impl.ob.sc;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@Deprecated
public class kk extends ki {
    static final oj a = new oj("PREF_KEY_UID_");
    static final oj b = new oj("PREF_KEY_DEVICE_ID_");
    private static final oj c = new oj("PREF_KEY_HOST_URL_");
    private static final oj d = new oj("PREF_KEY_HOST_URLS_FROM_STARTUP");
    private static final oj e = new oj("PREF_KEY_HOST_URLS_FROM_CLIENT");
    @Deprecated
    private static final oj f = new oj("PREF_KEY_REPORT_URL_");
    private static final oj g = new oj("PREF_KEY_REPORT_URLS_");
    @Deprecated
    private static final oj h = new oj("PREF_L_URL");
    private static final oj i = new oj("PREF_L_URLS");
    private static final oj j = new oj("PREF_KEY_GET_AD_URL");
    private static final oj k = new oj("PREF_KEY_REPORT_AD_URL");
    private static final oj l = new oj("PREF_KEY_STARTUP_OBTAIN_TIME_");
    private static final oj m = new oj("PREF_KEY_STARTUP_ENCODED_CLIDS_");
    private static final oj n = new oj("PREF_KEY_DISTRIBUTION_REFERRER_");
    private static final oj o = new oj("STARTUP_CLIDS_MATCH_WITH_APP_CLIDS_");
    @Deprecated
    private static final oj q = new oj("PREF_KEY_PINNING_UPDATE_URL");
    private static final oj r = new oj("PREF_KEY_EASY_COLLECTING_ENABLED_");
    private static final oj s = new oj("PREF_KEY_COLLECTING_PACKAGE_INFO_ENABLED_");
    private static final oj t = new oj("PREF_KEY_PERMISSIONS_COLLECTING_ENABLED_");
    private static final oj u = new oj("PREF_KEY_FEATURES_COLLECTING_ENABLED_");
    private static final oj v = new oj("SOCKET_CONFIG_");
    private static final oj w = new oj("LAST_STARTUP_REQUEST_CLIDS");
    private static final oj x = new oj("FLCC");
    private static final oj y = new oj("BKCC");
    private oj A = o(a.a());
    private oj B = o(c.a());
    private oj C = o(d.a());
    private oj D = o(e.a());
    @Deprecated
    private oj E = o(f.a());
    private oj F = o(g.a());
    @Deprecated
    private oj G = o(h.a());
    private oj H = o(i.a());
    private oj I = o(j.a());
    private oj J = o(k.a());
    private oj K = o(l.a());
    private oj L = o(m.a());
    private oj M = o(n.a());
    private oj N = o(o.a());
    private oj O = o(r.a());
    private oj P = o(s.a());
    private oj Q = o(t.a());
    private oj R = o(u.a());
    private oj S = o(v.a());
    private oj T = o(w.a());
    private oj U = o(x.a());
    private oj V = o(y.a());
    private oj z = new oj(b.a());

    public kk(jq jqVar, String str) {
        super(jqVar, str);
    }

    public kk a(String str) {
        return (kk) b(this.A.b(), str);
    }

    @Deprecated
    public kk b(String str) {
        return (kk) b(this.z.b(), str);
    }

    @Deprecated
    public kk c(String str) {
        return (kk) b(this.E.b(), str);
    }

    public kk a(List<String> list) {
        return (kk) b(this.F.b(), th.a(list));
    }

    public kk b(List<String> list) {
        return (kk) b(this.H.b(), th.a(list));
    }

    public kk d(String str) {
        return (kk) b(this.J.b(), str);
    }

    public kk e(String str) {
        return (kk) b(this.I.b(), str);
    }

    public kk f(String str) {
        return (kk) b(this.B.b(), str);
    }

    public kk a(long j2) {
        return (kk) a(this.K.b(), j2);
    }

    public kk g(String str) {
        return (kk) b(this.L.b(), str);
    }

    public kk h(String str) {
        return (kk) b(this.M.b(), str);
    }

    public kk a(boolean z2) {
        return (kk) a(this.N.b(), z2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long */
    @NonNull
    @Deprecated
    public sc a() {
        return new sc.a(new rq.a().a(b(this.O.b(), rq.b.a)).b(b(this.P.b(), rq.b.b)).c(b(this.Q.b(), rq.b.c)).d(b(this.R.b(), rq.b.d)).a()).a(q(this.A.b())).c(th.b(q(this.C.b()))).d(th.b(q(this.D.b()))).h(q(this.L.b())).a(th.b(q(this.F.b()))).b(th.b(q(this.H.b()))).e(q(this.I.b())).f(q(this.J.b())).j(c(this.M.b(), null)).a(k(q(this.U.b()))).a(l(q(this.V.b()))).a(rt.a(q(this.S.b()))).i(q(this.T.b())).b(b(this.N.b(), true)).a(b(this.K.b(), -1L)).a();
    }

    @Nullable
    private mh k(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                return new la().a(a(new JSONObject(str)));
            } catch (JSONException e2) {
            }
        }
        return null;
    }

    static pp.a.c a(@NonNull JSONObject jSONObject) {
        pp.a.c cVar = new pp.a.c();
        cVar.b = ua.a(th.a(jSONObject, "uti"), cVar.b);
        cVar.c = ua.a(th.d(jSONObject, "udi"), cVar.c);
        cVar.d = ua.a(th.b(jSONObject, "rcff"), cVar.d);
        cVar.e = ua.a(th.b(jSONObject, "mbs"), cVar.e);
        cVar.f = ua.a(th.a(jSONObject, "maff"), cVar.f);
        cVar.g = ua.a(th.b(jSONObject, "mrsl"), cVar.g);
        cVar.h = ua.a(th.c(jSONObject, "ce"), cVar.h);
        return cVar;
    }

    @Nullable
    private mc l(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                return b(new JSONObject(str));
            } catch (JSONException e2) {
            }
        }
        return null;
    }

    private mc b(@NonNull JSONObject jSONObject) {
        pp.a.C0014a aVar = new pp.a.C0014a();
        aVar.b = a(jSONObject);
        aVar.c = ua.a(th.a(jSONObject, "cd"), aVar.c);
        aVar.d = ua.a(th.a(jSONObject, "ci"), aVar.d);
        return new kw().a(aVar);
    }

    @Deprecated
    public String i(String str) {
        return c(this.E.b(), str);
    }

    @Deprecated
    public String j(String str) {
        return c(this.G.b(), str);
    }
}
