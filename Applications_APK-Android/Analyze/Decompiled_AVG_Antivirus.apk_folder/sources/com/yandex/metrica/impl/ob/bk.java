package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.PreloadInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class bk {
    private PreloadInfo a;

    public bk(PreloadInfo preloadInfo, @NonNull tp tpVar) {
        if (preloadInfo == null) {
            return;
        }
        if (!TextUtils.isEmpty(preloadInfo.getTrackingId())) {
            this.a = preloadInfo;
        } else if (tpVar.c()) {
            tpVar.c("Required field \"PreloadInfo.trackingId\" is empty!\nThis preload info will be skipped.");
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        if (this.a == null) {
            return "";
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("preloadInfo", b());
        } catch (JSONException e) {
        }
        return jSONObject.toString();
    }

    @Nullable
    public JSONObject b() {
        if (this.a == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("trackingId", this.a.getTrackingId());
            if (!this.a.getAdditionalParams().isEmpty()) {
                jSONObject.put("additionalParams", new JSONObject(this.a.getAdditionalParams()));
            }
        } catch (JSONException e) {
        }
        return jSONObject;
    }
}
