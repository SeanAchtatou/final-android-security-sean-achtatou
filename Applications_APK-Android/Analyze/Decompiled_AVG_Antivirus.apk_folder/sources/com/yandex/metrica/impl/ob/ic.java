package com.yandex.metrica.impl.ob;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.core.app.NotificationCompat;
import com.yandex.metrica.ConfigurationService;

public class ic implements ib, ie {
    /* access modifiers changed from: private */
    @NonNull
    public final Context a;
    /* access modifiers changed from: private */
    @Nullable
    public final AlarmManager b;
    /* access modifiers changed from: private */
    @NonNull
    public tx c;

    public ic(@NonNull Context context) {
        this(context, (AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM), new tw());
    }

    public void a(final long j, boolean z) {
        cg.a(new Runnable() {
            public void run() {
                ic icVar = ic.this;
                ic.this.b.set(3, ic.this.c.c() + j, icVar.a(icVar.a));
            }
        }, this.b, "scheduling wakeup in [ConfigurationServiceController]", "AlarmManager");
    }

    public void a() {
        cg.a(new Runnable() {
            public void run() {
                ic icVar = ic.this;
                ic.this.b.cancel(icVar.a(icVar.a));
            }
        }, this.b, "cancelling scheduled wakeup in [ConfigurationServiceController]", "AlarmManager");
    }

    public void a(@NonNull Bundle bundle) {
        try {
            this.a.startService(new Intent().setComponent(new ComponentName(this.a.getPackageName(), ConfigurationService.class.getName())).setAction("com.yandex.metrica.configuration.ACTION_INIT").putExtras(bundle));
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    @NonNull
    public PendingIntent a(@NonNull Context context) {
        return PendingIntent.getService(context, 7695435, b(context), 134217728);
    }

    @NonNull
    private Intent b(@NonNull Context context) {
        return new Intent(context, ConfigurationService.class).setAction("com.yandex.metrica.configuration.ACTION_SCHEDULED_START");
    }

    @VisibleForTesting
    ic(@NonNull Context context, @Nullable AlarmManager alarmManager, @NonNull tx txVar) {
        this.a = context;
        this.b = alarmManager;
        this.c = txVar;
    }
}
