package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import com.yandex.metrica.f;
import org.json.JSONException;
import org.json.JSONObject;

public class tz {

    public enum a {
        LOGIN("login"),
        LOGOUT("logout"),
        SWITCH("switch"),
        UPDATE("update");
        
        private String e;

        private a(String str) {
            this.e = str;
        }

        public String toString() {
            return this.e;
        }
    }

    public static f a(String str) {
        f fVar = new f();
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                fVar.a(jSONObject.optString("UserInfo.UserId", null));
                fVar.b(jSONObject.optString("UserInfo.Type", null));
                fVar.a(th.a(jSONObject.optJSONObject("UserInfo.Options")));
            } catch (JSONException e) {
            }
        }
        return fVar;
    }

    public static String a(a aVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("action", aVar.toString());
            return jSONObject.toString();
        } catch (JSONException e) {
            return null;
        }
    }
}
