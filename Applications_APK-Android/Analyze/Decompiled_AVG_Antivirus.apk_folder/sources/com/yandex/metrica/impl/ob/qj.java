package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter;
import java.util.Locale;

public class qj {
    private String a;
    private s b;
    private final String c = "3.6.4";
    private final String d = "45179";
    private final String e;
    private final String f;
    private final String g;
    @NonNull
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private GoogleAdvertisingIdGetter.c p;
    private String q;
    private String r;
    private sc s;

    public interface d<T extends qj, D> {
        @NonNull
        T a(Object obj);
    }

    public qj() {
        this.e = TextUtils.isEmpty("") ? "public" : "public_";
        this.f = "android";
        this.g = "2";
        this.h = bx.a();
        this.q = com.yandex.metrica.b.PHONE.name().toLowerCase(Locale.US);
    }

    public String d() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public sc e() {
        return this.s;
    }

    public synchronized boolean f() {
        return !ce.a(t(), r(), this.n);
    }

    /* access modifiers changed from: protected */
    public void a(s sVar) {
        this.b = sVar;
    }

    /* access modifiers changed from: protected */
    public void a(sc scVar) {
        this.s = scVar;
    }

    @NonNull
    public String g() {
        return ua.b(this.b.b, "");
    }

    public String h() {
        return "2";
    }

    public String i() {
        return "3.6.4";
    }

    public String j() {
        return "45179";
    }

    public String k() {
        return this.e;
    }

    public String l() {
        return "android";
    }

    @NonNull
    public String m() {
        return this.b.c;
    }

    @NonNull
    public String n() {
        return this.b.d;
    }

    public int o() {
        return this.b.e;
    }

    /* access modifiers changed from: protected */
    public void c(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.j = str;
        }
    }

    public String p() {
        return ua.b(this.j, "");
    }

    public String q() {
        return ua.b(this.i, "");
    }

    /* access modifiers changed from: protected */
    public void d(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.i = str;
        }
    }

    @NonNull
    public synchronized String r() {
        return ua.b(this.l, "");
    }

    @NonNull
    public synchronized String s() {
        return ua.b(this.m, "");
    }

    @NonNull
    public synchronized String t() {
        return ua.b(this.k, "");
    }

    /* access modifiers changed from: protected */
    public synchronized void e(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.k = str;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void f(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.l = str;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void g(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.m = str;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void h(String str) {
        this.n = str;
    }

    public void i(String str) {
        this.o = str;
    }

    @NonNull
    public String u() {
        return this.b.h;
    }

    @NonNull
    public String v() {
        return this.h;
    }

    public int w() {
        return this.b.f.a;
    }

    public int x() {
        return this.b.f.b;
    }

    public int y() {
        return this.b.f.c;
    }

    public float z() {
        return this.b.f.d;
    }

    @NonNull
    public String A() {
        return ua.b(this.r, "");
    }

    /* access modifiers changed from: package-private */
    public final void j(String str) {
        this.r = str;
    }

    @NonNull
    public String B() {
        return this.o;
    }

    @NonNull
    public String C() {
        return ua.b(this.q, com.yandex.metrica.b.PHONE.name().toLowerCase(Locale.US));
    }

    /* access modifiers changed from: package-private */
    public void k(String str) {
        this.q = str;
    }

    public GoogleAdvertisingIdGetter.c D() {
        return this.p;
    }

    /* access modifiers changed from: protected */
    public void a(GoogleAdvertisingIdGetter.c cVar) {
        this.p = cVar;
    }

    public static class c<A> {
        @NonNull
        public final sc a;
        @NonNull
        public final A b;

        public c(@NonNull sc scVar, A a2) {
            this.a = scVar;
            this.b = a2;
        }
    }

    public static abstract class a<I, O> implements qi<I, O> {
        @Nullable
        public final String c;
        @Nullable
        public final String d;
        @Nullable
        public final String e;

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3) {
            this.c = str;
            this.d = str2;
            this.e = str3;
        }
    }

    protected static abstract class b<T extends qj, A extends a> implements d<T, c<A>> {
        @NonNull
        final Context a;
        @NonNull
        final String b;

        /* access modifiers changed from: protected */
        @NonNull
        public abstract T b();

        protected b(@NonNull Context context, @NonNull String str) {
            this.a = context;
            this.b = str;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        @androidx.annotation.NonNull
        /* renamed from: c */
        public T a(@androidx.annotation.NonNull com.yandex.metrica.impl.ob.qj.c<A> r5) {
            /*
                r4 = this;
                com.yandex.metrica.impl.ob.qj r0 = r4.b()
                android.content.Context r1 = r4.a
                com.yandex.metrica.impl.ob.s r1 = com.yandex.metrica.impl.ob.s.a(r1)
                r0.a(r1)
                com.yandex.metrica.impl.ob.sc r2 = r5.a
                r0.a(r2)
                android.content.Context r2 = r4.a
                A r3 = r5.b
                com.yandex.metrica.impl.ob.qj$a r3 = (com.yandex.metrica.impl.ob.qj.a) r3
                java.lang.String r3 = r3.c
                java.lang.String r2 = r4.a(r2, r3)
                r0.k(r2)
                com.yandex.metrica.impl.ob.sc r2 = r5.a
                java.lang.String r1 = r1.a(r2)
                java.lang.String r2 = ""
                java.lang.String r1 = com.yandex.metrica.impl.ob.ua.b(r1, r2)
                r0.i(r1)
                r4.c(r0, r5)
                java.lang.String r1 = r4.b
                A r2 = r5.b
                com.yandex.metrica.impl.ob.qj$a r2 = (com.yandex.metrica.impl.ob.qj.a) r2
                java.lang.String r2 = r2.d
                android.content.Context r3 = r4.a
                r4.a(r0, r1, r2, r3)
                java.lang.String r1 = r4.b
                A r5 = r5.b
                com.yandex.metrica.impl.ob.qj$a r5 = (com.yandex.metrica.impl.ob.qj.a) r5
                java.lang.String r5 = r5.e
                android.content.Context r2 = r4.a
                r4.b(r0, r1, r5, r2)
                java.lang.String r5 = r4.b
                r0.b(r5)
                com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter r5 = com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter.a()
                android.content.Context r1 = r4.a
                com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter$c r5 = r5.c(r1)
                r0.a(r5)
                android.content.Context r5 = r4.a
                com.yandex.metrica.impl.ob.ao r5 = com.yandex.metrica.impl.ob.ao.a(r5)
                java.lang.String r5 = r5.a()
                r0.j(r5)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.qj.b.a(com.yandex.metrica.impl.ob.qj$c):com.yandex.metrica.impl.ob.qj");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        private void a(@androidx.annotation.NonNull T r2, @androidx.annotation.NonNull java.lang.String r3, @androidx.annotation.Nullable java.lang.String r4, @androidx.annotation.NonNull android.content.Context r5) {
            /*
                r1 = this;
                boolean r0 = android.text.TextUtils.isEmpty(r4)
                if (r0 == 0) goto L_0x000b
                java.lang.String r4 = com.yandex.metrica.impl.ob.cg.b(r5, r3)
            L_0x000b:
                r2.d(r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.qj.b.a(com.yandex.metrica.impl.ob.qj, java.lang.String, java.lang.String, android.content.Context):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        private void b(@androidx.annotation.NonNull T r2, @androidx.annotation.NonNull java.lang.String r3, @androidx.annotation.Nullable java.lang.String r4, @androidx.annotation.NonNull android.content.Context r5) {
            /*
                r1 = this;
                boolean r0 = android.text.TextUtils.isEmpty(r4)
                if (r0 == 0) goto L_0x000b
                java.lang.String r4 = com.yandex.metrica.impl.ob.cg.a(r5, r3)
            L_0x000b:
                r2.c(r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.qj.b.b(com.yandex.metrica.impl.ob.qj, java.lang.String, java.lang.String, android.content.Context):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        private synchronized void c(@androidx.annotation.NonNull T r2, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.qj.c<A> r3) {
            /*
                r1 = this;
                monitor-enter(r1)
                java.lang.String r0 = r1.a(r3)     // Catch:{ all -> 0x0010 }
                r2.e(r0)     // Catch:{ all -> 0x0010 }
                r1.a(r2, r3)     // Catch:{ all -> 0x0010 }
                r1.b(r2, r3)     // Catch:{ all -> 0x0010 }
                monitor-exit(r1)
                return
            L_0x0010:
                r2 = move-exception
                monitor-exit(r1)
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.qj.b.c(com.yandex.metrica.impl.ob.qj, com.yandex.metrica.impl.ob.qj$c):void");
        }

        /* access modifiers changed from: package-private */
        public void a(qj qjVar, @NonNull c cVar) {
            qjVar.f(cVar.a.b);
            qjVar.h(cVar.a.d);
        }

        /* access modifiers changed from: package-private */
        public void b(qj qjVar, @NonNull c cVar) {
            qjVar.g(cVar.a.c);
        }

        private String a(@NonNull c cVar) {
            return cVar.a.a;
        }

        /* access modifiers changed from: package-private */
        @VisibleForTesting
        public String a(@NonNull Context context, @Nullable String str) {
            return str == null ? s.a(context).g : str;
        }
    }
}
