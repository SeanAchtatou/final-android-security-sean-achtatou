package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import java.util.HashMap;

public class en implements dk {
    private final HashMap<String, dk> a = new HashMap<>();
    private final Context b;

    public en(@NonNull Context context) {
        this.b = context.getApplicationContext();
    }

    @NonNull
    public synchronized <T extends dk & dq> T a(@NonNull df dfVar, @NonNull db dbVar, @NonNull dl<T> dlVar) {
        T t;
        t = (dk) this.a.get(dfVar.toString());
        if (t == null) {
            t = (dk) dlVar.b(this.b, dfVar, dbVar);
            this.a.put(dfVar.toString(), t);
        } else {
            ((dq) t).a(dbVar);
        }
        return t;
    }

    public void c() {
        for (dk c : this.a.values()) {
            c.c();
        }
        this.a.clear();
    }
}
