package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class vf extends ve<byte[]> {
    @VisibleForTesting(otherwise = 3)
    public /* bridge */ /* synthetic */ int a() {
        return super.a();
    }

    @VisibleForTesting(otherwise = 3)
    @NonNull
    public /* bridge */ /* synthetic */ String b() {
        return super.b();
    }

    public vf(int i, @NonNull String str, @NonNull tp tpVar) {
        super(i, str, tpVar);
    }

    @Nullable
    public byte[] a(@Nullable byte[] bArr) {
        if (bArr.length <= a()) {
            return bArr;
        }
        byte[] bArr2 = new byte[a()];
        System.arraycopy(bArr, 0, bArr2, 0, a());
        if (this.a.c()) {
            this.a.b("\"%s\" %s exceeded limit of %d bytes", b(), bArr, Integer.valueOf(a()));
        }
        return bArr2;
    }
}
