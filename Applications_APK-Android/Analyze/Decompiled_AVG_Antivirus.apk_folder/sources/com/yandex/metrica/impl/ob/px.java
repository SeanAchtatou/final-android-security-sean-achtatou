package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.yandex.metrica.g;

public class px {
    @WorkerThread
    public cj a(@NonNull Context context) {
        return cj.b(context);
    }

    @WorkerThread
    @Nullable
    public cj a() {
        return cj.f();
    }

    public void a(@NonNull Context context, @NonNull g gVar) {
        cj.a(context, gVar);
    }

    public void b() {
        cj.c();
    }

    public boolean c() {
        return cj.d();
    }

    public boolean d() {
        return cj.f() != null;
    }

    public ap e() {
        return cj.g();
    }

    public void a(@Nullable Location location) {
        cj.a(location);
    }

    public void a(boolean z) {
        cj.a(z);
    }

    public void a(@NonNull Context context, boolean z) {
        cj.b(context).b(z);
    }

    public void b(@NonNull Context context, boolean z) {
        cj.b(context).c(z);
    }

    public cj f() {
        return cj.e();
    }
}
