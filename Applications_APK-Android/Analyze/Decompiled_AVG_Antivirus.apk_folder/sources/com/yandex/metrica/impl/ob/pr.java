package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class pr extends vt<Void> {
    public pr(@NonNull final px pxVar) {
        super(new vx<Void>() {
            public vv a(@Nullable Void voidR) {
                if (px.this.c()) {
                    return vv.a(this);
                }
                return vv.a(this, "YandexMetrica isn't initialized. Use YandexMetrica#activate(android.content.Context, String) method to activate.");
            }
        });
    }

    public vv a() {
        return super.a(null);
    }
}
