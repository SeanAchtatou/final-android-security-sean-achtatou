package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class ij {
    @NonNull
    private Context a;
    @NonNull
    private ServiceConnection b;
    @NonNull
    private final vp c;

    public ij(@NonNull Context context) {
        this(context, new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder service) {
            }

            public void onServiceDisconnected(ComponentName name) {
            }
        }, new vp());
    }

    public void a(@Nullable String str) {
        Intent a2;
        if (!TextUtils.isEmpty(str) && (a2 = a(this.a, str)) != null) {
            this.a.bindService(a2, this.b, 1);
        }
    }

    public void a() {
        this.a.unbindService(this.b);
    }

    @Nullable
    private Intent a(@NonNull Context context, @NonNull String str) {
        try {
            ResolveInfo resolveService = context.getPackageManager().resolveService(a(context).setPackage(str), 0);
            if (resolveService != null) {
                return new Intent().setClassName(resolveService.serviceInfo.packageName, resolveService.serviceInfo.name).setAction("com.yandex.metrica.ACTION_C_BG_L");
            }
            return null;
        } catch (Throwable th) {
            return null;
        }
    }

    @NonNull
    private Intent a(@NonNull Context context) {
        Intent intent = new Intent("com.yandex.metrica.IMetricaService", Uri.parse("metrica://" + context.getPackageName()));
        a(intent);
        return intent;
    }

    @SuppressLint({"ObsoleteSdkInt"})
    private void a(@NonNull Intent intent) {
        if (Build.VERSION.SDK_INT >= 12) {
            b(intent);
        }
    }

    @TargetApi(12)
    private void b(@NonNull Intent intent) {
        intent.addFlags(32);
    }

    @VisibleForTesting
    ij(@NonNull Context context, @NonNull ServiceConnection serviceConnection, @NonNull vp vpVar) {
        this.a = context;
        this.b = serviceConnection;
        this.c = vpVar;
    }
}
