package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

class md {
    @Nullable
    private mh a;
    @NonNull
    private final js b;
    @NonNull
    private final jr c;

    public md(@Nullable mh mhVar, @NonNull js jsVar, @NonNull jr jrVar) {
        this.a = mhVar;
        this.b = jsVar;
        this.c = jrVar;
    }

    public void a() {
        mh mhVar = this.a;
        if (mhVar != null) {
            b(mhVar);
            c(this.a);
        }
    }

    private void b(@NonNull mh mhVar) {
        if (this.b.a() > ((long) mhVar.j)) {
            this.b.c((int) (((float) mhVar.j) * 0.1f));
        }
    }

    private void c(@NonNull mh mhVar) {
        if (this.c.a() > ((long) mhVar.j)) {
            this.c.c((int) (((float) mhVar.j) * 0.1f));
        }
    }

    public void a(@Nullable mh mhVar) {
        this.a = mhVar;
    }
}
