package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.os.Build;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class tl {
    private String a;

    public String a() {
        String str = this.a;
        if (str != null) {
            return str;
        }
        this.a = Build.VERSION.SDK_INT >= 18 ? c() : b();
        return this.a;
    }

    @SuppressLint({"StaticFieldLeak"})
    private String b() {
        try {
            FutureTask futureTask = new FutureTask(new Callable<String>() {
                /* renamed from: a */
                public String call() {
                    return tl.this.d();
                }
            });
            cj.l().d().post(futureTask);
            return (String) futureTask.get(5, TimeUnit.SECONDS);
        } catch (Throwable th) {
            return null;
        }
    }

    private String c() {
        return d();
    }

    /* access modifiers changed from: private */
    @SuppressLint({"PrivateApi"})
    public String d() {
        try {
            Class<?> cls = Class.forName("android.app.ActivityThread");
            return (String) cls.getMethod("getProcessName", new Class[0]).invoke(cls.getMethod("currentActivityThread", new Class[0]).invoke(null, new Object[0]), new Object[0]);
        } catch (Throwable th) {
            throw new RuntimeException(th);
        }
    }

    public boolean a(@NonNull String str) {
        try {
            if (TextUtils.isEmpty(a())) {
                return false;
            }
            String a2 = a();
            if (a2.endsWith(":" + str)) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }
}
