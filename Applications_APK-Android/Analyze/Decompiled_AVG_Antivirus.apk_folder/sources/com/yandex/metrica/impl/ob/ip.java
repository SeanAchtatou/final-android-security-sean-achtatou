package com.yandex.metrica.impl.ob;

import android.os.FileObserver;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.File;

public class ip extends FileObserver {
    private final uc<File> a;
    private final File b;

    public ip(@NonNull File file, @NonNull uc<File> ucVar) {
        super(file.getAbsolutePath(), 8);
        this.a = ucVar;
        this.b = file;
    }

    public void onEvent(int event, @Nullable String path) {
        if (event == 8 && !TextUtils.isEmpty(path)) {
            this.a.a(new File(this.b, path));
        }
    }
}
