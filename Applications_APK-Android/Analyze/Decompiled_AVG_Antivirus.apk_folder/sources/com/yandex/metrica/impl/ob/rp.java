package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.IIdentifierCallback;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class rp {
    private static final IIdentifierCallback a = new IIdentifierCallback() {
        public void onReceive(Map<String, String> map) {
        }

        public void onRequestError(IIdentifierCallback.Reason reason) {
        }
    };
    @NonNull
    private final AtomicReference<IIdentifierCallback> b;

    public rp(@NonNull IIdentifierCallback iIdentifierCallback) {
        this.b = new AtomicReference<>(iIdentifierCallback);
    }

    public void a(Map<String, String> map) {
        this.b.getAndSet(a).onReceive(map);
    }

    public void a(IIdentifierCallback.Reason reason) {
        this.b.getAndSet(a).onRequestError(reason);
    }
}
