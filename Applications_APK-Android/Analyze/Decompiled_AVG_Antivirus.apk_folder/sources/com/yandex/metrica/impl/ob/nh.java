package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter;
import com.yandex.metrica.impl.ob.qj;
import java.util.List;

public class nh extends qj {
    @Nullable
    private final mh a;

    private nh(@Nullable mh mhVar) {
        this.a = mhVar;
    }

    @Nullable
    public mh a() {
        return this.a;
    }

    public static class a {
        public final sc a;
        public final mh b;

        public a(sc scVar, mh mhVar) {
            this.a = scVar;
            this.b = mhVar;
        }
    }

    protected static class b implements qj.d<nh, a> {
        @NonNull
        private final Context a;

        protected b(@NonNull Context context) {
            this.a = context;
        }

        @NonNull
        public nh a(a aVar) {
            nh nhVar = new nh(aVar.b);
            Context context = this.a;
            nhVar.d(cg.b(context, context.getPackageName()));
            Context context2 = this.a;
            nhVar.c(cg.a(context2, context2.getPackageName()));
            nhVar.i(ua.b(s.a(this.a).a(aVar.a), ""));
            nhVar.a(aVar.a);
            nhVar.a(s.a(this.a));
            nhVar.b(this.a.getPackageName());
            nhVar.e(aVar.a.a);
            nhVar.f(aVar.a.b);
            nhVar.g(aVar.a.c);
            nhVar.a(GoogleAdvertisingIdGetter.a().c(this.a));
            return nhVar;
        }
    }

    @Nullable
    public List<String> b() {
        return e().i;
    }
}
