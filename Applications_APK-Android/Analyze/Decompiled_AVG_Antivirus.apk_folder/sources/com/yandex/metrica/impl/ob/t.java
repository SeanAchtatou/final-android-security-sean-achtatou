package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.Base64;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ab;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class t implements Parcelable {
    public static final Parcelable.Creator<t> CREATOR = new Parcelable.Creator<t>() {
        /* renamed from: a */
        public t createFromParcel(Parcel parcel) {
            Bundle readBundle = parcel.readBundle(u.class.getClassLoader());
            t a = new t().a(readBundle.getInt("CounterReport.Type", ab.a.EVENT_TYPE_UNDEFINED.a())).b(readBundle.getInt("CounterReport.CustomType")).c(ce.b(readBundle.getString("CounterReport.Value"), "")).a(readBundle.getString("CounterReport.UserInfo")).e(readBundle.getString("CounterReport.Environment")).b(readBundle.getString("CounterReport.Event")).a(t.d(readBundle)).c(readBundle.getInt("CounterReport.TRUNCATED")).d(readBundle.getString("CounterReport.ProfileID")).a(readBundle.getLong("CounterReport.CreationElapsedRealtime")).b(readBundle.getLong("CounterReport.CreationTimestamp")).a(ad.a(Integer.valueOf(readBundle.getInt("CounterReport.UniquenessStatus"))));
            a aVar = (a) readBundle.getParcelable("CounterReport.IdentifiersData");
            if (aVar != null) {
                a.a(aVar);
            }
            return a;
        }

        /* renamed from: a */
        public t[] newArray(int i) {
            return new t[i];
        }
    };
    String a;
    String b;
    int c;
    int d;
    int e;
    private String f;
    private String g;
    @Nullable
    private Pair<String, String> h;
    private String i;
    private long j;
    private long k;
    @NonNull
    private ad l;
    @Nullable
    private a m;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle();
        bundle.putString("CounterReport.Event", this.a);
        bundle.putString("CounterReport.Value", this.b);
        bundle.putInt("CounterReport.Type", this.c);
        bundle.putInt("CounterReport.CustomType", this.d);
        bundle.putInt("CounterReport.TRUNCATED", this.e);
        bundle.putString("CounterReport.ProfileID", this.i);
        bundle.putInt("CounterReport.UniquenessStatus", this.l.d);
        a aVar = this.m;
        if (aVar != null) {
            bundle.putParcelable("CounterReport.IdentifiersData", aVar);
        }
        String str = this.g;
        if (str != null) {
            bundle.putString("CounterReport.Environment", str);
        }
        String str2 = this.f;
        if (str2 != null) {
            bundle.putString("CounterReport.UserInfo", str2);
        }
        Pair<String, String> pair = this.h;
        if (pair != null) {
            a(bundle, pair);
        }
        bundle.putLong("CounterReport.CreationElapsedRealtime", this.j);
        bundle.putLong("CounterReport.CreationTimestamp", this.k);
        dest.writeBundle(bundle);
    }

    public static class a implements Parcelable {
        public static final Parcelable.Creator<a> CREATOR = new Parcelable.Creator<a>() {
            /* renamed from: a */
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            /* renamed from: a */
            public a[] newArray(int i) {
                return new a[i];
            }
        };
        @Nullable
        private ResultReceiver a;
        @Nullable
        private List<String> b;

        public a(@Nullable List<String> list, @Nullable ResultReceiver resultReceiver) {
            this.b = list;
            this.a = resultReceiver;
        }

        @Nullable
        public List<String> a() {
            return this.b;
        }

        @Nullable
        public ResultReceiver b() {
            return this.a;
        }

        protected a(Parcel parcel) {
            Bundle readBundle = parcel.readBundle(u.class.getClassLoader());
            if (readBundle != null) {
                this.a = (ResultReceiver) readBundle.getParcelable("com.yandex.metrica.CounterConfiguration.receiver");
                this.b = readBundle.getStringArrayList("com.yandex.metrica.CounterConfiguration.identifiersList");
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("com.yandex.metrica.CounterConfiguration.receiver", this.a);
            List<String> list = this.b;
            if (list != null) {
                bundle.putStringArrayList("com.yandex.metrica.CounterConfiguration.identifiersList", new ArrayList(list));
            }
            dest.writeBundle(bundle);
        }
    }

    public t() {
        this("", 0);
    }

    public t(@Nullable t tVar) {
        this.l = ad.UNKNOWN;
        if (tVar != null) {
            this.a = tVar.d();
            this.b = tVar.e();
            this.c = tVar.g();
            this.d = tVar.h();
            this.f = tVar.l();
            this.g = tVar.j();
            this.h = tVar.k();
            this.e = tVar.o();
            this.i = tVar.i;
            this.j = tVar.r();
            this.k = tVar.s();
            this.l = tVar.l;
            this.m = tVar.m;
        }
    }

    public t(String str, int i2) {
        this("", str, i2);
    }

    public t(String str, String str2, int i2) {
        this(str, str2, i2, new tw());
    }

    @VisibleForTesting
    public t(String str, String str2, int i2, tw twVar) {
        this.l = ad.UNKNOWN;
        this.a = str2;
        this.c = i2;
        this.b = str;
        this.j = twVar.c();
        this.k = twVar.a();
    }

    public String d() {
        return this.a;
    }

    public t b(String str) {
        this.a = str;
        return this;
    }

    public String e() {
        return this.b;
    }

    public byte[] f() {
        return Base64.decode(this.b, 0);
    }

    public t c(String str) {
        this.b = str;
        return this;
    }

    public t a(@Nullable byte[] bArr) {
        this.b = new String(Base64.encode(bArr, 0));
        return this;
    }

    public int g() {
        return this.c;
    }

    public t a(int i2) {
        this.c = i2;
        return this;
    }

    public int h() {
        return this.d;
    }

    public t b(int i2) {
        this.d = i2;
        return this;
    }

    @Nullable
    public a i() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public String j() {
        return this.g;
    }

    public Pair<String, String> k() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public t e(String str) {
        this.g = str;
        return this;
    }

    /* access modifiers changed from: package-private */
    public t b(String str, String str2) {
        if (this.h == null) {
            this.h = new Pair<>(str, str2);
        }
        return this;
    }

    /* access modifiers changed from: private */
    public t a(@Nullable Pair<String, String> pair) {
        this.h = pair;
        return this;
    }

    public String l() {
        return this.f;
    }

    public t a(String str) {
        this.f = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public t c(int i2) {
        this.e = i2;
        return this;
    }

    /* access modifiers changed from: protected */
    public t a(long j2) {
        this.j = j2;
        return this;
    }

    /* access modifiers changed from: protected */
    public t b(long j2) {
        this.k = j2;
        return this;
    }

    /* access modifiers changed from: protected */
    public t a(@NonNull a aVar) {
        this.m = aVar;
        return this;
    }

    public boolean m() {
        return this.a == null;
    }

    public boolean n() {
        return ab.a.EVENT_TYPE_UNDEFINED.a() == this.c;
    }

    public int o() {
        return this.e;
    }

    @Nullable
    public String p() {
        return this.i;
    }

    public t d(@Nullable String str) {
        this.i = str;
        return this;
    }

    @NonNull
    public ad q() {
        return this.l;
    }

    @NonNull
    public t a(@NonNull ad adVar) {
        this.l = adVar;
        return this;
    }

    public long r() {
        return this.j;
    }

    public long s() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public Bundle a(Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putParcelable("CounterReport.Object", this);
        return bundle;
    }

    public String toString() {
        return String.format(Locale.US, "[event: %s, type: %s, value: %s]", this.a, ab.a.a(this.c).b(), this.b);
    }

    private static void a(@NonNull Bundle bundle, @NonNull Pair<String, String> pair) {
        bundle.putString("CounterReport.AppEnvironmentDiffKey", (String) pair.first);
        bundle.putString("CounterReport.AppEnvironmentDiffValue", (String) pair.second);
    }

    /* access modifiers changed from: private */
    @Nullable
    public static Pair<String, String> d(Bundle bundle) {
        if (!bundle.containsKey("CounterReport.AppEnvironmentDiffKey") || !bundle.containsKey("CounterReport.AppEnvironmentDiffValue")) {
            return null;
        }
        return new Pair<>(bundle.getString("CounterReport.AppEnvironmentDiffKey"), bundle.getString("CounterReport.AppEnvironmentDiffValue"));
    }

    @NonNull
    public static t b(Bundle bundle) {
        if (bundle != null) {
            try {
                t tVar = (t) bundle.getParcelable("CounterReport.Object");
                if (tVar != null) {
                    return tVar;
                }
            } catch (Throwable th) {
                return new t();
            }
        }
        return new t();
    }

    public static t a(t tVar, ab.a aVar) {
        t a2 = a(tVar);
        a2.a(aVar.a());
        return a2;
    }

    public static t a(@NonNull t tVar) {
        t tVar2 = new t(tVar);
        tVar2.b("");
        tVar2.c("");
        return tVar2;
    }

    public static t b(t tVar) {
        return a(tVar, ab.a.EVENT_TYPE_ALIVE);
    }

    public static t c(t tVar) {
        return a(tVar, ab.a.EVENT_TYPE_START);
    }

    public static t d(t tVar) {
        return a(tVar, ab.a.EVENT_TYPE_INIT);
    }

    public static t a(t tVar, di diVar) {
        Context j2 = diVar.j();
        aj a2 = new aj().a(j2);
        try {
            if (diVar.v()) {
                a2.b(j2);
            }
            if (diVar.h().F()) {
                a2.a(j2, diVar.h().G());
            }
        } catch (Throwable th) {
        }
        t a3 = a(tVar);
        a3.a(ab.a.EVENT_TYPE_IDENTITY.a()).c(a2.c());
        return a3;
    }

    public static t a(t tVar, @NonNull Collection<nt> collection, @Nullable k kVar, @NonNull j jVar) {
        String str;
        t a2 = a(tVar);
        try {
            JSONArray jSONArray = new JSONArray();
            for (nt next : collection) {
                jSONArray.put(new JSONObject().put("name", next.a).put("granted", next.b));
            }
            JSONObject jSONObject = new JSONObject();
            if (kVar != null) {
                jSONObject.put("background_restricted", kVar.b);
                jSONObject.put("app_standby_bucket", jVar.a(kVar.a));
            }
            str = new JSONObject().put("permissions", jSONArray).put("background_restrictions", jSONObject).toString();
        } catch (JSONException e2) {
            str = "";
        }
        return a2.a(ab.a.EVENT_TYPE_PERMISSIONS.a()).c(str);
    }

    public static t a(t tVar, String str) {
        return a(tVar).a(ab.a.EVENT_TYPE_APP_FEATURES.a()).c(str);
    }

    public static t e(t tVar) {
        return a(tVar, ab.a.EVENT_TYPE_FIRST_ACTIVATION);
    }

    public static t f(t tVar) {
        return a(tVar, ab.a.EVENT_TYPE_APP_UPDATE);
    }

    public static t t() {
        return new t().a(ab.a.EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG.a());
    }
}
