package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public class bh implements Runnable {
    @NonNull
    private final ni a;
    @NonNull
    private final be b;
    @NonNull
    private final ux c;
    @NonNull
    private final String d;
    @NonNull
    private final bg e;

    public bh(@NonNull ni niVar, @NonNull be beVar, @NonNull ux uxVar, @NonNull String str) {
        this(niVar, beVar, uxVar, new bg(), str);
    }

    public bh(@NonNull ni niVar, @NonNull be beVar, @NonNull ux uxVar, @NonNull bg bgVar, @NonNull String str) {
        this.a = niVar;
        this.b = beVar;
        this.c = uxVar;
        this.e = bgVar;
        this.d = str;
    }

    public void run() {
        boolean z = true;
        if (!this.c.c() || !this.a.a()) {
            z = false;
        } else {
            boolean a2 = this.b.a();
            nj c2 = this.b.c();
            if (a2 && !c2.b()) {
                a2 = false;
            }
            while (this.c.c() && r0) {
                boolean z2 = !a() && this.b.t();
            }
        }
        if (!z) {
            b();
        }
        this.b.f();
    }

    private boolean a() {
        this.e.a(this.b);
        boolean b2 = this.b.b();
        this.b.a(b2);
        return b2;
    }

    private void b() {
        this.b.g();
    }
}
