package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.ej;

public interface ek<C extends ej> {
    @NonNull
    C a(@NonNull Context context, @NonNull en enVar, @NonNull eh ehVar, @NonNull db dbVar);
}
