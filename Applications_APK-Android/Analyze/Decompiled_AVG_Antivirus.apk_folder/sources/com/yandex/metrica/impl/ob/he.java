package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.HashSet;
import java.util.Set;

public class he {
    private boolean a;
    @NonNull
    private Set<Integer> b;
    private int c;
    private int d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.he.<init>(boolean, int, int, java.util.Set<java.lang.Integer>):void
     arg types: [int, int, int, java.util.HashSet]
     candidates:
      com.yandex.metrica.impl.ob.he.<init>(boolean, int, int, int[]):void
      com.yandex.metrica.impl.ob.he.<init>(boolean, int, int, java.util.Set<java.lang.Integer>):void */
    public he() {
        this(false, 0, 0, (Set<Integer>) new HashSet());
    }

    public he(boolean z, int i, int i2, @NonNull int[] iArr) {
        this(z, i, i2, cg.a(iArr));
    }

    public he(boolean z, int i, int i2, @NonNull Set<Integer> set) {
        this.a = z;
        this.b = set;
        this.c = i;
        this.d = i2;
    }

    public void a() {
        this.b = new HashSet();
        this.d = 0;
    }

    public boolean b() {
        return this.a;
    }

    public void a(boolean z) {
        this.a = z;
    }

    @NonNull
    public Set<Integer> c() {
        return this.b;
    }

    public int d() {
        return this.d;
    }

    public int e() {
        return this.c;
    }

    public void a(int i) {
        this.c = i;
        this.d = 0;
    }

    public void b(int i) {
        this.b.add(Integer.valueOf(i));
        this.d++;
    }
}
