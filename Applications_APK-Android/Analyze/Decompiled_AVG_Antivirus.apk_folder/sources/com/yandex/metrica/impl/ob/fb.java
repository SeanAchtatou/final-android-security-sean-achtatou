package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.Cdo;

public class fb<T, C extends Cdo> {
    private final fh<T> a;
    private final C b;

    protected interface a<T> {
        boolean a(Object obj, t tVar);
    }

    protected fb(fh<T> fhVar, C c) {
        this.a = fhVar;
        this.b = c;
    }

    /* access modifiers changed from: protected */
    public boolean a(@NonNull t tVar, @NonNull a<fj> aVar) {
        for (Object a2 : a(tVar).a()) {
            if (aVar.a(a2, tVar)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public fe<T> a(t tVar) {
        return this.a.a(tVar.g());
    }
}
