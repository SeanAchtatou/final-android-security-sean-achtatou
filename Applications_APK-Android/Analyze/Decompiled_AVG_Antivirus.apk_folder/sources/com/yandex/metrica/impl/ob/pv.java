package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pu;
import java.util.HashMap;

public abstract class pv<T extends pu> {
    @NonNull
    private HashMap<String, T> a = new HashMap<>();
    /* access modifiers changed from: private */
    @NonNull
    public px b = new px();
    @NonNull
    private final uv c;

    /* access modifiers changed from: protected */
    public abstract T a(@NonNull uv uvVar, @NonNull Context context, @NonNull String str);

    protected pv(@NonNull uv uvVar) {
        this.c = uvVar;
    }

    private T b(@NonNull final Context context, @NonNull String str) {
        if (this.b.f() == null) {
            this.c.a(new Runnable() {
                public void run() {
                    pv.this.b.a(context);
                }
            });
        }
        T a2 = a(this.c, context, str);
        this.a.put(str, a2);
        return a2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public T a(@androidx.annotation.NonNull android.content.Context r3, @androidx.annotation.NonNull java.lang.String r4) {
        /*
            r2 = this;
            java.util.HashMap<java.lang.String, T> r0 = r2.a
            java.lang.Object r0 = r0.get(r4)
            com.yandex.metrica.impl.ob.pu r0 = (com.yandex.metrica.impl.ob.pu) r0
            if (r0 != 0) goto L_0x0024
            java.util.HashMap<java.lang.String, T> r1 = r2.a
            monitor-enter(r1)
            java.util.HashMap<java.lang.String, T> r0 = r2.a     // Catch:{ all -> 0x0021 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0021 }
            com.yandex.metrica.impl.ob.pu r0 = (com.yandex.metrica.impl.ob.pu) r0     // Catch:{ all -> 0x0021 }
            if (r0 != 0) goto L_0x001f
            com.yandex.metrica.impl.ob.pu r3 = r2.b(r3, r4)     // Catch:{ all -> 0x0021 }
            r3.a(r4)     // Catch:{ all -> 0x0021 }
            r0 = r3
        L_0x001f:
            monitor-exit(r1)     // Catch:{ all -> 0x0021 }
            goto L_0x0024
        L_0x0021:
            r3 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0021 }
            throw r3
        L_0x0024:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pv.a(android.content.Context, java.lang.String):com.yandex.metrica.impl.ob.pu");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public T a(@androidx.annotation.NonNull android.content.Context r4, @androidx.annotation.NonNull com.yandex.metrica.ReporterConfig r5) {
        /*
            r3 = this;
            java.util.HashMap<java.lang.String, T> r0 = r3.a
            java.lang.String r1 = r5.apiKey
            java.lang.Object r0 = r0.get(r1)
            com.yandex.metrica.impl.ob.pu r0 = (com.yandex.metrica.impl.ob.pu) r0
            if (r0 != 0) goto L_0x002a
            java.util.HashMap<java.lang.String, T> r1 = r3.a
            monitor-enter(r1)
            java.util.HashMap<java.lang.String, T> r0 = r3.a     // Catch:{ all -> 0x0027 }
            java.lang.String r2 = r5.apiKey     // Catch:{ all -> 0x0027 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0027 }
            com.yandex.metrica.impl.ob.pu r0 = (com.yandex.metrica.impl.ob.pu) r0     // Catch:{ all -> 0x0027 }
            if (r0 != 0) goto L_0x0025
            java.lang.String r0 = r5.apiKey     // Catch:{ all -> 0x0027 }
            com.yandex.metrica.impl.ob.pu r4 = r3.b(r4, r0)     // Catch:{ all -> 0x0027 }
            r4.a(r5)     // Catch:{ all -> 0x0027 }
            r0 = r4
        L_0x0025:
            monitor-exit(r1)     // Catch:{ all -> 0x0027 }
            goto L_0x002a
        L_0x0027:
            r4 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0027 }
            throw r4
        L_0x002a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pv.a(android.content.Context, com.yandex.metrica.ReporterConfig):com.yandex.metrica.impl.ob.pu");
    }
}
