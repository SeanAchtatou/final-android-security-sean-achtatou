package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

public class tg extends tj {
    private static final tg a = new tg();

    public tg(@Nullable String str) {
        super(str);
    }

    private tg() {
        this("");
    }

    public static tg h() {
        return a;
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return super.d() && "publicProd".startsWith("internal");
    }

    public String f() {
        return "AppMetricaInternal";
    }
}
