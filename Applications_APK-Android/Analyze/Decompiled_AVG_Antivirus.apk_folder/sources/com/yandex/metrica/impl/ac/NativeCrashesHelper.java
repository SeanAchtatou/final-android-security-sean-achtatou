package com.yandex.metrica.impl.ac;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ag;
import com.yandex.metrica.impl.ob.bt;
import com.yandex.metrica.impl.ob.kg;
import com.yandex.metrica.impl.ob.uc;
import com.yandex.metrica.impl.ob.uv;
import java.io.File;
import java.io.FilenameFilter;

public class NativeCrashesHelper {
    private String a;
    private final Context b;
    private boolean c;
    private boolean d;

    private static native void cancelSetUpNativeUncaughtExceptionHandler();

    private static native void logsEnabled(boolean z);

    private static native void setUpNativeUncaughtExceptionHandler(String str);

    public NativeCrashesHelper(Context context) {
        this.b = context;
    }

    public boolean a() {
        return this.c;
    }

    public synchronized void a(boolean z) {
        if (z) {
            d();
        } else {
            f();
        }
    }

    public synchronized void a(bt btVar, uv uvVar, @NonNull final kg kgVar) {
        if (g()) {
            if (ag.a() && !kgVar.f()) {
                uvVar.a(new a(btVar, this, this.b.getFilesDir().getAbsolutePath() + "/" + "YandexMetricaNativeCrashes", new uc<Boolean>() {
                    public void a(Boolean bool) {
                        kgVar.i();
                    }
                }));
            }
            uvVar.a(new a(btVar, this, this.a, new uc<Boolean>() {
                public void a(Boolean bool) {
                }
            }));
            this.c = false;
        }
    }

    private void c() {
        if (!this.d && b()) {
            b(false);
            this.a = ag.a(this.b).getAbsolutePath() + "/" + "YandexMetricaNativeCrashes";
        }
        this.d = true;
    }

    private void d() {
        try {
            c();
            if (e()) {
                setUpNativeUncaughtExceptionHandler(this.a);
                this.c = true;
            }
        } catch (Throwable th) {
            this.c = false;
        }
    }

    private boolean e() {
        return this.a != null;
    }

    private void f() {
        try {
            if (g()) {
                cancelSetUpNativeUncaughtExceptionHandler();
            }
        } catch (Throwable th) {
        }
        this.c = false;
    }

    private boolean g() {
        return e() && this.c;
    }

    private boolean b(boolean z) {
        try {
            logsEnabled(z);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    private static class a implements Runnable {
        private final bt a;
        @NonNull
        private final String b;
        @NonNull
        private final uc<Boolean> c;
        private final NativeCrashesHelper d;

        a(bt btVar, NativeCrashesHelper nativeCrashesHelper, @NonNull String str, @NonNull uc<Boolean> ucVar) {
            this.d = nativeCrashesHelper;
            this.a = btVar;
            this.b = str;
            this.c = ucVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.bt.a(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          com.yandex.metrica.impl.ob.bt.a(com.yandex.metrica.impl.ob.bv, com.yandex.metrica.impl.ob.bp):void
          com.yandex.metrica.impl.ob.bt.a(com.yandex.metrica.impl.ob.iy, com.yandex.metrica.impl.ob.bp):void
          com.yandex.metrica.impl.ob.bt.a(com.yandex.metrica.impl.ob.pk$a, com.yandex.metrica.impl.ob.bp):void
          com.yandex.metrica.impl.ob.bt.a(com.yandex.metrica.impl.ob.t, com.yandex.metrica.impl.ob.bp):void
          com.yandex.metrica.impl.ob.bt.a(java.lang.Boolean, java.lang.Boolean):void
          com.yandex.metrica.impl.ob.bt.a(java.lang.String, com.yandex.metrica.impl.ob.bp):void
          com.yandex.metrica.impl.ob.bt.a(java.util.List<java.lang.String>, android.os.ResultReceiver):void
          com.yandex.metrica.impl.ob.bt.a(boolean, com.yandex.metrica.impl.ob.bp):void
          com.yandex.metrica.impl.ob.bt.a(java.lang.String, boolean):void */
        public void run() {
            File file;
            for (String str : this.d.a(this.b)) {
                String str2 = this.b + "/" + str;
                try {
                    String b2 = ag.b(ag.a(str2));
                    if (b2 != null) {
                        this.a.a(b2, true);
                    }
                    this.c.a(true);
                    file = new File(str2);
                } catch (Throwable th) {
                    new File(str2).delete();
                    throw th;
                }
                file.delete();
            }
        }
    }

    /* access modifiers changed from: private */
    public String[] a(String str) {
        File file = new File(str + "/");
        if (!file.mkdir() && !file.exists()) {
            return new String[0];
        }
        String[] list = file.list(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                return filename.endsWith(".dmp");
            }
        });
        return list != null ? list : new String[0];
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean b() {
        try {
            System.loadLibrary("YandexMetricaNativeModule");
            return true;
        } catch (Throwable th) {
            return false;
        }
    }
}
