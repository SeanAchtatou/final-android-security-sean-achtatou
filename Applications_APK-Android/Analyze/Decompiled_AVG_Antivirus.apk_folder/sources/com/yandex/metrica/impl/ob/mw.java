package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.h;

class mw {
    @NonNull
    private final Context a;
    @NonNull
    private sc b;
    @Nullable
    private mh c;
    @NonNull
    private final bg d;
    @NonNull
    private final js e;
    @NonNull
    private final jr f;
    @NonNull
    private final tx g;
    @NonNull
    private final ng h;
    @NonNull
    private final h i;
    @NonNull
    private final h.b j;
    @NonNull
    private final ni k;
    @NonNull
    private final uv l;
    /* access modifiers changed from: private */
    public boolean m;

    public mw(@NonNull Context context, @NonNull sc scVar, @Nullable mh mhVar, @NonNull js jsVar, @NonNull jr jrVar, @NonNull uv uvVar) {
        this(context, scVar, mhVar, new bg(), jsVar, jrVar, uvVar, new tw(), new ng(), af.a().i(), new ni(context));
    }

    public void a() {
        if (d()) {
            b();
        }
    }

    private void b() {
        if (!this.m) {
            this.i.a(h.a, this.l, this.j);
        } else {
            c();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        mh mhVar = this.c;
        if (mhVar != null) {
            nd a2 = this.h.a(this.a, this.b, mhVar);
            boolean a3 = this.k.a();
            do {
                if (this.k.a() && (a3 = a2.a()) && a2.c().b()) {
                    while (this.l.c() && a3) {
                        this.d.a(a2);
                        a3 = !a2.b() && a2.t();
                    }
                }
            } while (a3);
        }
    }

    private boolean d() {
        return a(this.e) || a(this.f);
    }

    private boolean a(ji jiVar) {
        return this.c != null && (b(jiVar) || c(jiVar));
    }

    private boolean b(ji jiVar) {
        mh mhVar = this.c;
        return mhVar != null && a(jiVar, (long) mhVar.g);
    }

    private boolean a(ji jiVar, long j2) {
        return jiVar.a() >= j2;
    }

    private boolean c(ji jiVar) {
        mh mhVar = this.c;
        return mhVar != null && b(jiVar, mhVar.i);
    }

    private boolean b(ji jiVar, long j2) {
        return this.g.a() - jiVar.b() > j2;
    }

    public void a(@NonNull sc scVar, @Nullable mh mhVar) {
        this.b = scVar;
        this.c = mhVar;
    }

    @VisibleForTesting
    mw(@NonNull Context context, @NonNull sc scVar, @Nullable mh mhVar, @NonNull bg bgVar, @NonNull js jsVar, @NonNull jr jrVar, @NonNull uv uvVar, @NonNull tx txVar, @NonNull ng ngVar, @NonNull h hVar, @NonNull ni niVar) {
        this.m = false;
        this.a = context;
        this.c = mhVar;
        this.b = scVar;
        this.d = bgVar;
        this.e = jsVar;
        this.f = jrVar;
        this.l = uvVar;
        this.g = txVar;
        this.h = ngVar;
        this.i = hVar;
        this.j = new h.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.yandex.metrica.impl.ob.mw.a(com.yandex.metrica.impl.ob.mw, boolean):boolean
             arg types: [com.yandex.metrica.impl.ob.mw, int]
             candidates:
              com.yandex.metrica.impl.ob.mw.a(com.yandex.metrica.impl.ob.ji, long):boolean
              com.yandex.metrica.impl.ob.mw.a(com.yandex.metrica.impl.ob.sc, com.yandex.metrica.impl.ob.mh):void
              com.yandex.metrica.impl.ob.mw.a(com.yandex.metrica.impl.ob.mw, boolean):boolean */
            public void a() {
                boolean unused = mw.this.m = true;
                mw.this.c();
            }
        };
        this.k = niVar;
    }
}
