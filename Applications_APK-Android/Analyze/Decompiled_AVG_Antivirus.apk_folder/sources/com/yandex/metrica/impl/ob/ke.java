package com.yandex.metrica.impl.ob;

import android.util.SparseArray;
import androidx.annotation.NonNull;

class ke {
    private static volatile SparseArray<oj> a = new SparseArray<>();

    ke() {
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public synchronized String a(int i) {
        if (a.get(i) == null) {
            SparseArray<oj> sparseArray = a;
            sparseArray.put(i, new oj("EVENT_NUMBER_OF_TYPE_" + i));
        }
        return a.get(i).b();
    }
}
