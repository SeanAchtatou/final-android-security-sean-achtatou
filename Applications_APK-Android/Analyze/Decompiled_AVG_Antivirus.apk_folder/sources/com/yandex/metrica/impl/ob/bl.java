package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.SparseArray;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ab;
import com.yandex.metrica.impl.ob.ch;
import com.yandex.metrica.impl.ob.pg;
import com.yandex.metrica.impl.ob.th;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class bl {
    private static Map<hw, Integer> a;
    private static SparseArray<hw> b;
    /* access modifiers changed from: private */
    public static Map<ad, Integer> c;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(hw.FOREGROUND, 0);
        hashMap.put(hw.BACKGROUND, 1);
        a = Collections.unmodifiableMap(hashMap);
        SparseArray<hw> sparseArray = new SparseArray<>();
        sparseArray.put(0, hw.FOREGROUND);
        sparseArray.put(1, hw.BACKGROUND);
        b = sparseArray;
        HashMap hashMap2 = new HashMap();
        hashMap2.put(ad.FIRST_OCCURRENCE, 1);
        hashMap2.put(ad.NON_FIRST_OCCURENCE, 0);
        hashMap2.put(ad.UNKNOWN, -1);
        c = Collections.unmodifiableMap(hashMap2);
    }

    public static pg.c.g a(ContentValues contentValues) {
        return a(contentValues.getAsLong("start_time"), contentValues.getAsLong("server_time_offset"), contentValues.getAsBoolean("obtained_before_first_sync"));
    }

    public static pg.c.f a(st stVar) {
        pg.c.f fVar = new pg.c.f();
        if (stVar.a() != null) {
            fVar.b = stVar.a().intValue();
        }
        if (stVar.b() != null) {
            fVar.c = stVar.b().intValue();
        }
        if (!TextUtils.isEmpty(stVar.d())) {
            fVar.d = stVar.d();
        }
        fVar.e = stVar.c();
        if (!TextUtils.isEmpty(stVar.e())) {
            fVar.f = stVar.e();
        }
        return fVar;
    }

    public static hw a(int i2) {
        return b.get(i2);
    }

    public static pg.d[] a(String str) {
        try {
            return a(new JSONArray(str));
        } catch (Throwable th) {
            return null;
        }
    }

    public static pg.d[] a(JSONArray jSONArray) {
        pg.d[] dVarArr = null;
        try {
            dVarArr = new pg.d[jSONArray.length()];
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                dVarArr[i2] = a(jSONArray.getJSONObject(i2));
            }
        } catch (Throwable th) {
        }
        return dVarArr;
    }

    public static pg.d a(JSONObject jSONObject) throws JSONException {
        try {
            pg.d dVar = new pg.d();
            dVar.b = jSONObject.getString("mac");
            dVar.c = jSONObject.getInt("signal_strength");
            dVar.d = jSONObject.getString("ssid");
            dVar.e = jSONObject.optBoolean("is_connected");
            dVar.f = jSONObject.optLong("last_visible_offset_seconds", 0);
            return dVar;
        } catch (Throwable th) {
            pg.d dVar2 = new pg.d();
            dVar2.b = jSONObject.getString("mac");
            return dVar2;
        }
    }

    static pg.c.e.a.b.C0008a b(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(str);
            pg.c.e.a.b.C0008a aVar = new pg.c.e.a.b.C0008a();
            aVar.b = jSONObject.optString("ssid");
            int optInt = jSONObject.optInt("state", -1);
            if (!(optInt == 0 || optInt == 1 || optInt == 2)) {
                if (optInt == 3) {
                    aVar.c = 2;
                } else if (optInt != 4) {
                }
                return aVar;
            }
            aVar.c = 1;
            return aVar;
        } catch (Throwable th) {
            return null;
        }
    }

    public static pg.c.g a(long j2) {
        pg.c.g gVar = new pg.c.g();
        gVar.b = j2;
        gVar.c = ty.a(j2);
        return gVar;
    }

    public static pg.c.g a(Long l, Long l2, Boolean bool) {
        pg.c.g a2 = a(l.longValue());
        if (l2 != null) {
            a2.d = l2.longValue();
        }
        if (bool != null) {
            a2.e = bool.booleanValue();
        }
        return a2;
    }

    public static pg.c.e.b a(String str, int i2, pg.c.g gVar) {
        pg.c.e.b bVar = new pg.c.e.b();
        bVar.b = gVar;
        bVar.c = str;
        bVar.d = i2;
        return bVar;
    }

    static int a(hw hwVar) {
        return a.get(hwVar).intValue();
    }

    public static pg.a[] c(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                return b(new JSONArray(str));
            }
            return null;
        } catch (JSONException e2) {
            return new pg.a[]{b(new JSONObject(str))};
        } catch (Throwable th) {
            return null;
        }
    }

    @Nullable
    public static pg.a[] b(JSONArray jSONArray) {
        pg.a[] aVarArr = null;
        try {
            aVarArr = new pg.a[jSONArray.length()];
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i2);
                if (jSONObject != null) {
                    aVarArr[i2] = b(jSONObject);
                }
            }
        } catch (Throwable th) {
        }
        return aVarArr;
    }

    @VisibleForTesting
    @NonNull
    static pg.a b(JSONObject jSONObject) {
        int optInt;
        pg.a aVar = new pg.a();
        if (jSONObject.has("signal_strength") && (optInt = jSONObject.optInt("signal_strength", aVar.c)) != -1) {
            aVar.c = optInt;
        }
        if (jSONObject.has("cell_id")) {
            aVar.b = jSONObject.optInt("cell_id", aVar.b);
        }
        if (jSONObject.has("lac")) {
            aVar.d = jSONObject.optInt("lac", aVar.d);
        }
        if (jSONObject.has("country_code")) {
            aVar.e = jSONObject.optInt("country_code", aVar.e);
        }
        if (jSONObject.has("operator_id")) {
            aVar.f = jSONObject.optInt("operator_id", aVar.f);
        }
        if (jSONObject.has("operator_name")) {
            aVar.g = jSONObject.optString("operator_name", aVar.g);
        }
        if (jSONObject.has("is_connected")) {
            aVar.h = jSONObject.optBoolean("is_connected", aVar.h);
        }
        aVar.i = jSONObject.optInt("cell_type", 0);
        if (jSONObject.has("pci")) {
            aVar.j = jSONObject.optInt("pci", aVar.j);
        }
        if (jSONObject.has("last_visible_time_offset")) {
            aVar.k = jSONObject.optLong("last_visible_time_offset", aVar.k);
        }
        return aVar;
    }

    public static pg.c.b d(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                th.a aVar = new th.a(str);
                if (aVar.c("lon") && aVar.c("lat")) {
                    pg.c.b bVar = new pg.c.b();
                    try {
                        bVar.c = aVar.getDouble("lon");
                        bVar.b = aVar.getDouble("lat");
                        bVar.h = aVar.optInt("altitude");
                        bVar.f = aVar.optInt("direction");
                        bVar.e = aVar.optInt("precision");
                        bVar.g = aVar.optInt("speed");
                        bVar.d = aVar.optLong("timestamp") / 1000;
                        if (!aVar.c("provider")) {
                            return bVar;
                        }
                        String a2 = aVar.a("provider");
                        if ("gps".equals(a2)) {
                            bVar.i = 1;
                            return bVar;
                        } else if (!"network".equals(a2)) {
                            return bVar;
                        } else {
                            bVar.i = 2;
                            return bVar;
                        }
                    } catch (Throwable th) {
                        return bVar;
                    }
                }
            }
            return null;
        } catch (Throwable th2) {
            return null;
        }
    }

    @VisibleForTesting
    static int e(String str) {
        try {
            th.a aVar = new th.a(str);
            if (aVar.c("enabled")) {
                return aVar.getBoolean("enabled") ? 1 : 0;
            }
            return -1;
        } catch (Throwable th) {
            return -1;
        }
    }

    public static pg.c.e.a.b a(int i2, String str, String str2, String str3, String str4) {
        pg.c.e.a.b bVar = new pg.c.e.a.b();
        bVar.d = i2;
        if (str != null) {
            bVar.e = str;
        }
        pg.a[] c2 = c(str3);
        if (c2 != null) {
            bVar.b = c2;
        }
        bVar.c = a(str2);
        if (!TextUtils.isEmpty(str4)) {
            bVar.f = b(str4);
        }
        return bVar;
    }

    public static pg.c.e.a.C0007a f(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            com.yandex.metrica.f a2 = tz.a(str);
            pg.c.e.a.C0007a aVar = new pg.c.e.a.C0007a();
            aVar.b = a2.a();
            if (!TextUtils.isEmpty(a2.b())) {
                aVar.c = a2.b();
            }
            if (!cg.a((Map) a2.c())) {
                aVar.d = th.a((Map) a2.c());
            }
            return aVar;
        } catch (Throwable th) {
            return null;
        }
    }

    public static void a(pg.c.e eVar) {
    }

    public static pg.c.C0006c[] a(Context context) {
        List<ch.a> b2 = ch.a(context).b();
        if (cg.a((Collection) b2)) {
            return null;
        }
        pg.c.C0006c[] cVarArr = new pg.c.C0006c[b2.size()];
        for (int i2 = 0; i2 < b2.size(); i2++) {
            pg.c.C0006c cVar = new pg.c.C0006c();
            ch.a aVar = b2.get(i2);
            cVar.b = aVar.a;
            cVar.c = aVar.b;
            cVarArr[i2] = cVar;
        }
        return cVarArr;
    }

    static class c {
        private static final Map<ab.a, Class<?>> u;
        private static final Map<ab.a, Integer> v;
        protected String a;
        protected String b;
        protected int c;
        protected int d;
        protected int e;
        protected long f;
        protected String g;
        protected String h;
        protected String i;
        protected Integer j;
        protected Integer k;
        protected String l;
        protected String m;
        protected int n;
        protected int o;
        protected String p;
        protected String q;
        protected String r;
        protected un s;
        protected ad t;

        static {
            HashMap hashMap = new HashMap();
            hashMap.put(ab.a.EVENT_TYPE_REGULAR, e.class);
            hashMap.put(ab.a.EVENT_TYPE_SEND_REFERRER, k.class);
            hashMap.put(ab.a.EVENT_TYPE_ALIVE, g.class);
            hashMap.put(ab.a.EVENT_TYPE_NATIVE_CRASH, j.class);
            hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_USER, e.class);
            hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, f.class);
            hashMap.put(ab.a.EVENT_TYPE_IDENTITY, i.class);
            hashMap.put(ab.a.EVENT_TYPE_STATBOX, e.class);
            hashMap.put(ab.a.EVENT_TYPE_SET_USER_INFO, e.class);
            hashMap.put(ab.a.EVENT_TYPE_REPORT_USER_INFO, e.class);
            hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED, e.class);
            hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, f.class);
            hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, f.class);
            hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, f.class);
            hashMap.put(ab.a.EVENT_TYPE_ANR, f.class);
            hashMap.put(ab.a.EVENT_TYPE_START, g.class);
            hashMap.put(ab.a.EVENT_TYPE_CUSTOM_EVENT, d.class);
            hashMap.put(ab.a.EVENT_TYPE_APP_OPEN, e.class);
            hashMap.put(ab.a.EVENT_TYPE_PERMISSIONS, a.class);
            hashMap.put(ab.a.EVENT_TYPE_APP_FEATURES, a.class);
            hashMap.put(ab.a.EVENT_TYPE_SEND_USER_PROFILE, b.class);
            hashMap.put(ab.a.EVENT_TYPE_SEND_REVENUE_EVENT, b.class);
            u = Collections.unmodifiableMap(hashMap);
            HashMap hashMap2 = new HashMap();
            hashMap2.put(ab.a.EVENT_TYPE_INIT, 1);
            hashMap2.put(ab.a.EVENT_TYPE_REGULAR, 4);
            hashMap2.put(ab.a.EVENT_TYPE_SEND_REFERRER, 5);
            hashMap2.put(ab.a.EVENT_TYPE_ALIVE, 7);
            hashMap2.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED, 3);
            hashMap2.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, 26);
            hashMap2.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, 26);
            hashMap2.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, 26);
            hashMap2.put(ab.a.EVENT_TYPE_ANR, 25);
            hashMap2.put(ab.a.EVENT_TYPE_NATIVE_CRASH, 3);
            hashMap2.put(ab.a.EVENT_TYPE_EXCEPTION_USER, 6);
            hashMap2.put(ab.a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, 27);
            hashMap2.put(ab.a.EVENT_TYPE_IDENTITY, 8);
            hashMap2.put(ab.a.EVENT_TYPE_STATBOX, 11);
            hashMap2.put(ab.a.EVENT_TYPE_SET_USER_INFO, 12);
            hashMap2.put(ab.a.EVENT_TYPE_REPORT_USER_INFO, 12);
            hashMap2.put(ab.a.EVENT_TYPE_FIRST_ACTIVATION, 13);
            hashMap2.put(ab.a.EVENT_TYPE_START, 2);
            hashMap2.put(ab.a.EVENT_TYPE_APP_OPEN, 16);
            hashMap2.put(ab.a.EVENT_TYPE_APP_UPDATE, 17);
            hashMap2.put(ab.a.EVENT_TYPE_PERMISSIONS, 18);
            hashMap2.put(ab.a.EVENT_TYPE_APP_FEATURES, 19);
            hashMap2.put(ab.a.EVENT_TYPE_SEND_USER_PROFILE, 20);
            hashMap2.put(ab.a.EVENT_TYPE_SEND_REVENUE_EVENT, 21);
            v = Collections.unmodifiableMap(hashMap2);
        }

        static c a(int i2, boolean z) {
            c cVar;
            ab.a a2 = ab.a.a(i2);
            Class<?> a3 = a(a2, z);
            Integer num = v.get(a2);
            try {
                cVar = (c) a3.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (Throwable th) {
                cVar = new c();
            }
            return cVar.a(num);
        }

        private static Class<?> a(ab.a aVar, boolean z) {
            int i2 = AnonymousClass1.a[aVar.ordinal()];
            if (i2 != 1 && i2 != 2 && i2 != 3) {
                return u.get(aVar);
            }
            if (z) {
                return e.class;
            }
            return h.class;
        }

        /* access modifiers changed from: package-private */
        public c a(String str) {
            this.a = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c b(String str) {
            this.b = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c a(int i2) {
            this.c = i2;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c b(int i2) {
            this.d = i2;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c c(int i2) {
            this.e = i2;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c a(long j2) {
            this.f = j2;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c c(String str) {
            this.g = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c d(String str) {
            this.i = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c e(String str) {
            this.h = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c a(Integer num) {
            this.j = num;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c f(String str) {
            this.q = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c b(Integer num) {
            this.k = num;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c g(String str) {
            this.l = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c h(String str) {
            this.m = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c d(int i2) {
            this.n = i2;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c e(int i2) {
            this.o = i2;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c i(String str) {
            this.p = str;
            return this;
        }

        public c j(String str) {
            this.r = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c a(un unVar) {
            this.s = unVar;
            return this;
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public c a(@NonNull ad adVar) {
            this.t = adVar;
            return this;
        }

        /* access modifiers changed from: protected */
        public String a() {
            return this.a;
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            String str = this.b;
            return str == null ? new byte[0] : ce.c(str);
        }

        /* access modifiers changed from: protected */
        public Integer c() {
            return this.j;
        }

        /* access modifiers changed from: protected */
        public String d() {
            return this.l;
        }

        /* access modifiers changed from: package-private */
        public pg.c.e.a e() {
            pg.c.e.a aVar = new pg.c.e.a();
            pg.c.e.a.b a2 = bl.a(this.o, this.p, this.i, this.h, this.q);
            pg.c.b d2 = bl.d(this.g);
            pg.c.e.a.C0007a f2 = bl.f(this.m);
            if (a2 != null) {
                aVar.h = a2;
            }
            if (d2 != null) {
                aVar.g = d2;
            }
            if (a() != null) {
                aVar.e = a();
            }
            if (b() != null) {
                aVar.f = b();
            }
            if (d() != null) {
                aVar.i = d();
            }
            if (f2 != null) {
                aVar.j = f2;
            }
            aVar.d = c().intValue();
            aVar.b = (long) this.c;
            aVar.p = (long) this.d;
            aVar.q = (long) this.e;
            aVar.c = this.f;
            aVar.k = this.n;
            aVar.l = f();
            aVar.m = bl.e(this.g);
            String str = this.r;
            aVar.n = str == null ? new byte[0] : str.getBytes();
            Integer num = (Integer) bl.c.get(this.t);
            if (num != null) {
                aVar.o = num.intValue();
            }
            return aVar;
        }

        /* access modifiers changed from: protected */
        public int f() {
            return 0;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bl$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[ab.a.values().length];

        static {
            try {
                a[ab.a.EVENT_TYPE_INIT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[ab.a.EVENT_TYPE_FIRST_ACTIVATION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[ab.a.EVENT_TYPE_APP_UPDATE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    static class g extends c {
        g() {
        }

        /* access modifiers changed from: protected */
        public String a() {
            return "";
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return new byte[0];
        }
    }

    static class e extends c {
        private um u;

        e() {
            this(new um(af.a().b()));
        }

        e(@NonNull um umVar) {
            this.u = umVar;
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return this.u.a(this.s).a(super.b());
        }
    }

    static class f extends c {
        private um u;

        f() {
            this(new um(af.a().b()));
        }

        f(@NonNull um umVar) {
            this.u = umVar;
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return this.u.a(this.s).a(Base64.decode(super.b(), 0));
        }
    }

    static class k extends c {
        k() {
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            try {
                qf a = qf.a(Base64.decode(this.b, 0));
                ph phVar = new ph();
                phVar.b = a.a == null ? new byte[0] : a.a.getBytes();
                phVar.d = a.b;
                phVar.c = a.c;
                return e.a(phVar);
            } catch (d e) {
                return new byte[0];
            }
        }
    }

    static class b extends c {
        b() {
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return Base64.decode(this.b, 0);
        }
    }

    static class d extends e {
        d() {
        }

        /* access modifiers changed from: protected */
        public Integer c() {
            return this.k;
        }
    }

    static class a extends e {
        a() {
        }

        /* access modifiers changed from: protected */
        public String a() {
            return "";
        }
    }

    static class h extends c {
        h() {
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return new byte[0];
        }
    }

    static class j extends c {
        j() {
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return ce.c(ag.c(this.b));
        }
    }

    static class i extends e {
        i() {
        }

        public int f() {
            return this.s == un.EXTERNALLY_ENCRYPTED_EVENT_CRYPTER ? 1 : 0;
        }
    }
}
