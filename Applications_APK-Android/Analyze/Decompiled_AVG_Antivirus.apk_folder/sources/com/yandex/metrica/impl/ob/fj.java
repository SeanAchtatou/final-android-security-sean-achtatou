package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public abstract class fj {
    @NonNull
    private final dd a;

    public abstract boolean a(@NonNull t tVar, @NonNull el elVar);

    fj(@NonNull dd ddVar) {
        this.a = ddVar;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public dd a() {
        return this.a;
    }
}
