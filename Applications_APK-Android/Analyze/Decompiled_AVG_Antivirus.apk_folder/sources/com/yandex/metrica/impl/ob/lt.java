package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.yandex.metrica.impl.ob.s;
import org.json.JSONException;
import org.json.JSONObject;

@Deprecated
public class lt {
    private final String a;
    private final String b;
    private final String c;
    private final Point d;

    @SuppressLint({"NewApi", "HardwareIds", "ObsoleteSdkInt"})
    public lt(@NonNull Context context, @Nullable String str, @NonNull nq nqVar) {
        this.a = Build.MANUFACTURER;
        this.b = Build.MODEL;
        this.c = a(context, str, nqVar);
        s.b bVar = s.a(context).f;
        this.d = new Point(bVar.a, bVar.b);
    }

    @SuppressLint({"HardwareIds", "ObsoleteSdkInt", "MissingPermission", "NewApi"})
    @NonNull
    private String a(@NonNull Context context, @Nullable String str, @NonNull nq nqVar) {
        if (cg.a(28)) {
            if (nqVar.d(context)) {
                return Build.getSerial();
            }
            return ua.b(str, "");
        } else if (cg.a(8)) {
            return Build.SERIAL;
        } else {
            return ua.b(str, "");
        }
    }

    @NonNull
    public String a() {
        return this.c;
    }

    public lt(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        this.a = jSONObject.getString("manufacturer");
        this.b = jSONObject.getString(UrlManager.Parameter.MODEL);
        this.c = jSONObject.getString("serial");
        this.d = new Point(jSONObject.getInt(UrlManager.Parameter.WIDTH), jSONObject.getInt(UrlManager.Parameter.HEIGHT));
    }

    public JSONObject b() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("manufacturer", this.a);
        jSONObject.put(UrlManager.Parameter.MODEL, this.b);
        jSONObject.put("serial", this.c);
        jSONObject.put(UrlManager.Parameter.WIDTH, this.d.x);
        jSONObject.put(UrlManager.Parameter.HEIGHT, this.d.y);
        return jSONObject;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        lt ltVar = (lt) o;
        String str = this.a;
        if (str == null ? ltVar.a != null : !str.equals(ltVar.a)) {
            return false;
        }
        String str2 = this.b;
        if (str2 == null ? ltVar.b != null : !str2.equals(ltVar.b)) {
            return false;
        }
        Point point = this.d;
        if (point != null) {
            return point.equals(ltVar.d);
        }
        if (ltVar.d == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Point point = this.d;
        if (point != null) {
            i = point.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "DeviceSnapshot{mManufacturer='" + this.a + '\'' + ", mModel='" + this.b + '\'' + ", mSerial='" + this.c + '\'' + ", mScreenSize=" + this.d + '}';
    }
}
