package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.ab;
import java.util.List;

public class gl extends gm<ft> {
    public gl(@NonNull gv gvVar) {
        super(gvVar);
    }

    public void a(@NonNull ab.a aVar, @NonNull List<ft> list) {
        if (a(aVar)) {
            list.add(a().h());
        }
        if (b(aVar)) {
            list.add(a().d());
        }
    }

    private boolean a(@NonNull ab.a aVar) {
        return ab.b(aVar);
    }

    private boolean b(@NonNull ab.a aVar) {
        return ab.a(aVar);
    }
}
