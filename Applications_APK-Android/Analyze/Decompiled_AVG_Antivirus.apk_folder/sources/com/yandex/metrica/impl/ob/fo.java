package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.db;

public class fo extends fj {
    private final cd a;
    @NonNull
    private final mx b;

    public fo(@NonNull dd ddVar, @NonNull cd cdVar, @NonNull mx mxVar) {
        super(ddVar);
        this.a = cdVar;
        this.b = mxVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
     arg types: [java.lang.Boolean, int]
     candidates:
      com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
      com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
      com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
      com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
      com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
      com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean */
    public boolean a(@NonNull t tVar, @NonNull el elVar) {
        db.a a2 = elVar.b().a();
        this.a.a(a2.m);
        a(ua.a(a2.e, true));
        return false;
    }

    private void a(boolean z) {
        this.b.a(z);
    }
}
