package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.Locale;

public abstract class tj extends sz {
    private static String a = "";
    @NonNull
    private final String b;

    public tj(@Nullable String str) {
        super(false);
        this.b = String.format("[%s] ", d(str));
    }

    @NonNull
    public String g() {
        String b2 = ce.b(a, "");
        String b3 = ce.b(this.b, "");
        return b2 + b3;
    }

    public static void a(Context context) {
        a = String.format("[%s] : ", context.getPackageName());
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @NonNull
    public final String d(@Nullable String str) {
        if (TextUtils.isEmpty(str) || str.length() != 36) {
            return "";
        }
        StringBuilder sb = new StringBuilder(str);
        sb.replace(8, str.length() - 4, "-xxxx-xxxx-xxxx-xxxxxxxx");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public String c(String str, Object[] objArr) {
        return String.format(Locale.US, str, objArr);
    }
}
