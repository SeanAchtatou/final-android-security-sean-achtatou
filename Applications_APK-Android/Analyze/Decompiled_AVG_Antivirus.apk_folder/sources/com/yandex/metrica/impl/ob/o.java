package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ab;
import java.util.HashMap;

public class o extends t {
    private HashMap<a, Integer> f;
    private vo<String> g;
    private vo<String> h;
    private vo<byte[]> i;
    private vo<String> j;
    private vo<String> k;

    public enum a {
        NAME,
        VALUE,
        USER_INFO
    }

    @VisibleForTesting
    public o(@NonNull tp tpVar) {
        this.f = new HashMap<>();
        b(tpVar);
    }

    public o(String str, int i2, @NonNull tp tpVar) {
        this("", str, i2, tpVar);
    }

    public o(String str, String str2, int i2, @NonNull tp tpVar) {
        this(str, str2, i2, 0, tpVar);
    }

    public o(String str, String str2, int i2, int i3, @NonNull tp tpVar) {
        this.f = new HashMap<>();
        b(tpVar);
        this.b = g(str);
        this.a = f(str2);
        this.c = i2;
        this.d = i3;
    }

    public o(byte[] bArr, String str, int i2, @NonNull tp tpVar) {
        this.f = new HashMap<>();
        b(tpVar);
        a(bArr);
        this.a = f(str);
        this.c = i2;
    }

    public o a(@NonNull HashMap<a, Integer> hashMap) {
        this.f = hashMap;
        return this;
    }

    @NonNull
    public HashMap<a, Integer> a() {
        return this.f;
    }

    private void b(@NonNull tp tpVar) {
        this.g = new vm(1000, "event name", tpVar);
        this.h = new vl(245760, "event value", tpVar);
        this.i = new vf(245760, "event value bytes", tpVar);
        this.j = new vm(200, "user profile id", tpVar);
        this.k = new vm(10000, "UserInfo", tpVar);
    }

    private void a(String str, String str2, a aVar) {
        if (vi.a(str, str2)) {
            this.f.put(aVar, Integer.valueOf(ce.c(str).length - ce.c(str2).length));
        } else {
            this.f.remove(aVar);
        }
        u();
    }

    private void a(byte[] bArr, byte[] bArr2, a aVar) {
        if (bArr.length != bArr2.length) {
            this.f.put(aVar, Integer.valueOf(bArr.length - bArr2.length));
        } else {
            this.f.remove(aVar);
        }
        u();
    }

    private void u() {
        this.e = 0;
        for (Integer intValue : this.f.values()) {
            this.e += intValue.intValue();
        }
    }

    private String f(String str) {
        String a2 = this.g.a(str);
        a(str, a2, a.NAME);
        return a2;
    }

    private String g(String str) {
        String a2 = this.h.a(str);
        a(str, a2, a.VALUE);
        return a2;
    }

    private byte[] b(byte[] bArr) {
        byte[] a2 = this.i.a(bArr);
        a(bArr, a2, a.VALUE);
        return a2;
    }

    public t a(String str) {
        String a2 = this.k.a(str);
        a(str, a2, a.USER_INFO);
        return super.a(a2);
    }

    public t b(String str) {
        return super.b(f(str));
    }

    public t c(String str) {
        return super.c(g(str));
    }

    public final t a(@Nullable byte[] bArr) {
        return super.a(b(bArr));
    }

    @NonNull
    public t d(@Nullable String str) {
        return super.d(this.j.a(str));
    }

    public static t a(String str, String str2) {
        return new t().a(ab.a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED.a()).b(str, str2);
    }

    public static t b() {
        return new t().a(ab.a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED.a());
    }

    public static t c() {
        return new t().a(ab.a.EVENT_TYPE_SEND_USER_PROFILE.a());
    }

    public static t a(@NonNull iu iuVar) {
        return new t().a(ab.a.EVENT_TYPE_ANR.a()).a(e.a(new ja().b(iuVar)));
    }

    @NonNull
    static t a(@Nullable String str, @NonNull tp tpVar) {
        return new o(tpVar).a(ab.a.EVENT_TYPE_SET_USER_PROFILE_ID.a()).d(str);
    }

    @NonNull
    static t a(@NonNull tp tpVar) {
        return new o(tpVar).a(ab.a.EVENT_TYPE_SEND_REVENUE_EVENT.a());
    }
}
