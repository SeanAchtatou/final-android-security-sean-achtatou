package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class ki {
    public static final String p = ki.class.getSimpleName();
    private final jq a;
    private final String b;

    public ki(jq jqVar) {
        this(jqVar, null);
    }

    public ki(jq jqVar, String str) {
        this.a = jqVar;
        this.b = str;
    }

    public String m() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public oj o(String str) {
        return new oj(str, m());
    }

    /* access modifiers changed from: protected */
    public <T extends ki> T b(String str, String str2) {
        synchronized (this) {
            this.a.b(str, str2);
        }
        return this;
    }

    public <T extends ki> T a(String str, long j) {
        synchronized (this) {
            this.a.b(str, j);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public <T extends ki> T a(String str, int i) {
        synchronized (this) {
            this.a.b(str, i);
        }
        return this;
    }

    public <T extends ki> T a(String str, boolean z) {
        synchronized (this) {
            this.a.b(str, z);
        }
        return this;
    }

    public <T extends ki> T p(String str) {
        synchronized (this) {
            this.a.a(str);
        }
        return this;
    }

    public void n() {
        synchronized (this) {
            this.a.b();
        }
    }

    public long b(String str, long j) {
        return this.a.a(str, j);
    }

    /* access modifiers changed from: package-private */
    public int b(String str, int i) {
        return this.a.a(str, i);
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String q(@NonNull String str) {
        return this.a.a(str, (String) null);
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String c(@NonNull String str, @Nullable String str2) {
        return this.a.a(str, str2);
    }

    public boolean b(String str, boolean z) {
        return this.a.a(str, z);
    }

    public boolean r(@NonNull String str) {
        return this.a.b(str);
    }
}
