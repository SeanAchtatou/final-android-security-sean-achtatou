package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.SystemClock;

class al {
    private final Handler a;
    private final m b;
    private final am c;

    al(Handler handler, m mVar) {
        this.a = handler;
        this.b = mVar;
        this.c = new am(handler, mVar);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        b(this.a, this.b, this.c);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a(this.a, this.b, this.c);
    }

    static void a(Handler handler, m mVar, Runnable runnable) {
        b(handler, mVar, runnable);
        handler.postAtTime(runnable, b(mVar), a(mVar));
    }

    private static long a(m mVar) {
        return SystemClock.uptimeMillis() + ((long) c(mVar));
    }

    private static void b(Handler handler, m mVar, Runnable runnable) {
        handler.removeCallbacks(runnable, b(mVar));
    }

    private static String b(m mVar) {
        return mVar.d().h().e();
    }

    private static int c(m mVar) {
        return ua.a(mVar.d().h().c(), 10) * 500;
    }
}
