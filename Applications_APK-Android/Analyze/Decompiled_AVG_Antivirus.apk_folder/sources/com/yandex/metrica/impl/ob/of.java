package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
public class of extends oc {
    private static final oj d = new oj("SERVICE_API_LEVEL");
    private static final oj e = new oj("CLIENT_API_LEVEL");
    private oj f = new oj(d.a());
    private oj g = new oj(e.a());

    public of(Context context) {
        super(context, null);
    }

    public int a() {
        return this.c.getInt(this.f.b(), -1);
    }

    /* access modifiers changed from: protected */
    public String f() {
        return "_migrationpreferences";
    }

    public of b() {
        h(this.f.b());
        return this;
    }

    public of c() {
        h(this.g.b());
        return this;
    }
}
