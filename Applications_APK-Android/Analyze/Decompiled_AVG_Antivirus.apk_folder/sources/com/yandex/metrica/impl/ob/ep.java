package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class ep extends eg {
    ep(@NonNull Context context, @NonNull dx dxVar) {
        super(context, dxVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
     arg types: [java.lang.Boolean, int]
     candidates:
      com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
      com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
      com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
      com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
      com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
      com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean */
    /* access modifiers changed from: protected */
    public void b(@NonNull t tVar, @NonNull db dbVar) {
        a(ua.a(dbVar.b.e, true));
        b().a(tVar, dbVar);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(boolean z) {
        c().a(z);
    }
}
