package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.ab;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ff extends fh<ft> {
    private final gv a;
    private final Map<ab.a, go<ft>> b = b();
    @Nullable
    private gm<ft> c = new gl(this.a);
    @Nullable
    private gm<ft> d;

    public ff(di diVar) {
        this.a = new gv(diVar);
    }

    private HashMap<ab.a, go<ft>> b() {
        HashMap<ab.a, go<ft>> hashMap = new HashMap<>();
        hashMap.put(ab.a.EVENT_TYPE_ACTIVATION, new gk(this.a));
        hashMap.put(ab.a.EVENT_TYPE_START, new gy(this.a));
        hashMap.put(ab.a.EVENT_TYPE_REGULAR, new gs(this.a));
        gq gqVar = new gq(this.a);
        hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_USER, gqVar);
        hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, gqVar);
        hashMap.put(ab.a.EVENT_TYPE_SEND_REFERRER, gqVar);
        hashMap.put(ab.a.EVENT_TYPE_STATBOX, gqVar);
        hashMap.put(ab.a.EVENT_TYPE_CUSTOM_EVENT, gqVar);
        hashMap.put(ab.a.EVENT_TYPE_APP_OPEN, new gu(this.a));
        hashMap.put(ab.a.EVENT_TYPE_PURGE_BUFFER, new gr(this.a));
        ab.a aVar = ab.a.EVENT_TYPE_NATIVE_CRASH;
        gv gvVar = this.a;
        hashMap.put(aVar, new gx(gvVar, gvVar.l()));
        hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, new ha(this.a));
        gz gzVar = new gz(this.a);
        hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED, gzVar);
        hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, gzVar);
        hashMap.put(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, gzVar);
        hashMap.put(ab.a.EVENT_TYPE_ANR, gqVar);
        hashMap.put(ab.a.EVENT_TYPE_IDENTITY, new gp(this.a));
        hashMap.put(ab.a.EVENT_TYPE_SET_USER_INFO, new gw(this.a));
        ab.a aVar2 = ab.a.EVENT_TYPE_REPORT_USER_INFO;
        gv gvVar2 = this.a;
        hashMap.put(aVar2, new gx(gvVar2, gvVar2.g()));
        ab.a aVar3 = ab.a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED;
        gv gvVar3 = this.a;
        hashMap.put(aVar3, new gx(gvVar3, gvVar3.i()));
        ab.a aVar4 = ab.a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED;
        gv gvVar4 = this.a;
        hashMap.put(aVar4, new gx(gvVar4, gvVar4.j()));
        hashMap.put(ab.a.EVENT_TYPE_SEND_USER_PROFILE, gqVar);
        ab.a aVar5 = ab.a.EVENT_TYPE_SET_USER_PROFILE_ID;
        gv gvVar5 = this.a;
        hashMap.put(aVar5, new gx(gvVar5, gvVar5.o()));
        hashMap.put(ab.a.EVENT_TYPE_SEND_REVENUE_EVENT, gqVar);
        return hashMap;
    }

    public void a(ab.a aVar, go<ft> goVar) {
        this.b.put(aVar, goVar);
    }

    public gv a() {
        return this.a;
    }

    public fe<ft> a(int i) {
        LinkedList linkedList = new LinkedList();
        ab.a a2 = ab.a.a(i);
        gm<ft> gmVar = this.c;
        if (gmVar != null) {
            gmVar.a(a2, linkedList);
        }
        go goVar = this.b.get(a2);
        if (goVar != null) {
            goVar.a(linkedList);
        }
        gm<ft> gmVar2 = this.d;
        if (gmVar2 != null) {
            gmVar2.a(a2, linkedList);
        }
        return new fd(linkedList);
    }
}
