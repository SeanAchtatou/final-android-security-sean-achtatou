package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

public class jy {
    @NonNull
    private final Context a;
    @NonNull
    private final String b;
    private File c;
    private FileLock d;
    private RandomAccessFile e;
    private FileChannel f;

    public jy(@NonNull Context context, @NonNull String str) {
        this.a = context;
        this.b = str;
    }

    public synchronized void a() throws IOException {
        File filesDir = this.a.getFilesDir();
        this.c = new File(filesDir, new File(this.b).getName() + ".lock");
        this.e = new RandomAccessFile(this.c, "rw");
        this.f = this.e.getChannel();
        this.d = this.f.lock();
    }

    public synchronized void b() {
        String str = "";
        if (this.c != null) {
            str = this.c.getAbsolutePath();
        }
        ag.a(str, this.d);
        cg.a((Closeable) this.e);
        cg.a((Closeable) this.f);
        this.e = null;
        this.d = null;
        this.f = null;
    }
}
