package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class in extends im {
    @NonNull
    private final ik a;

    public in(@NonNull Context context, @NonNull ik ikVar) {
        super(context);
        this.a = ikVar;
    }

    public void a(@Nullable Bundle bundle, @Nullable il ilVar) {
        this.a.a(ilVar);
    }
}
