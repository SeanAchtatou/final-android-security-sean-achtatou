package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.n;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ch implements n {
    @SuppressLint({"StaticFieldLeak"})
    private static volatile ch f;
    private static final Object g = new Object();
    private final Context a;
    /* access modifiers changed from: private */
    @Nullable
    public final WifiManager b;
    /* access modifiers changed from: private */
    @NonNull
    public nq c;
    @NonNull
    private nw d;
    @NonNull
    private tv e;
    private n.a<JSONArray> h;
    private n.a<List<a>> i;
    private sc j;
    private final nr k;

    private ch(Context context) {
        this(context, (WifiManager) context.getApplicationContext().getSystemService("wifi"), new nw());
    }

    private ch(Context context, @Nullable WifiManager wifiManager, nw nwVar) {
        this(context, wifiManager, nwVar, new nq(nwVar.a()), new tv());
    }

    public static ch a(Context context) {
        if (f == null) {
            synchronized (g) {
                if (f == null) {
                    f = new ch(context.getApplicationContext());
                }
            }
        }
        return f;
    }

    public void a(@NonNull sc scVar) {
        this.j = scVar;
        this.d.a(scVar);
        this.c.a(this.d.a());
    }

    public synchronized JSONArray a() {
        if (!f()) {
            return new JSONArray();
        }
        if (this.h.b() || this.h.c()) {
            this.h.a(e());
        }
        return this.h.a();
    }

    @Nullable
    private String a(@Nullable String str) {
        if (str == null) {
            return null;
        }
        return str.toUpperCase(Locale.US).replace(":", "");
    }

    @Nullable
    private List<ScanResult> c() {
        return (List) cg.a(new Callable<List<ScanResult>>() {
            /* renamed from: a */
            public List<ScanResult> call() {
                return ch.this.b.getScanResults();
            }
        }, this.b, "getting scan results", "WifiManager");
    }

    @Nullable
    private WifiInfo d() {
        return (WifiInfo) cg.a(new Callable<WifiInfo>() {
            /* renamed from: a */
            public WifiInfo call() throws Exception {
                WifiInfo connectionInfo = ch.this.b.getConnectionInfo();
                if (connectionInfo == null) {
                    return connectionInfo;
                }
                if ("00:00:00:00:00:00".equals(connectionInfo.getBSSID()) || "02:00:00:00:00:00".equals(connectionInfo.getBSSID()) || connectionInfo.getIpAddress() == 0) {
                    return null;
                }
                return connectionInfo;
            }
        }, this.b, "getting connection info", "WifiManager");
    }

    private JSONArray e() {
        List<ScanResult> list;
        WifiInfo wifiInfo;
        JSONObject a2;
        String str = null;
        if (!j() || !this.k.a(this.a)) {
            list = null;
        } else {
            list = c();
        }
        if (!i() || !this.c.e(this.a)) {
            wifiInfo = null;
        } else {
            wifiInfo = d();
        }
        String bssid = wifiInfo == null ? null : wifiInfo.getBSSID();
        JSONArray jSONArray = new JSONArray();
        if (list != null) {
            for (ScanResult scanResult : list) {
                if (!(scanResult == null || "02:00:00:00:00:00".equals(scanResult.BSSID) || (a2 = a(bssid, scanResult)) == null)) {
                    jSONArray.put(a2);
                }
            }
        } else if (wifiInfo != null) {
            String a3 = a(bssid);
            String ssid = wifiInfo.getSSID();
            if (ssid != null) {
                str = b(ssid);
            }
            JSONObject a4 = a(a3, str, true, wifiInfo.getRssi(), 0L);
            if (a4 != null) {
                jSONArray.put(a4);
            }
        }
        return jSONArray;
    }

    private JSONObject a(@Nullable String str, @NonNull ScanResult scanResult) {
        boolean z;
        String str2;
        String str3 = null;
        boolean z2 = false;
        try {
            String str4 = scanResult.BSSID;
            if (str4 != null) {
                z2 = str4.equals(str);
                str3 = a(str4);
            }
            str2 = str3;
            z = z2;
        } catch (NoSuchFieldError e2) {
            str2 = null;
            z = false;
        }
        return a(str2, scanResult.SSID, z, scanResult.level, a(scanResult));
    }

    @Nullable
    private Long a(@NonNull ScanResult scanResult) {
        if (Build.VERSION.SDK_INT >= 17) {
            return b(scanResult);
        }
        return null;
    }

    @TargetApi(17)
    @NonNull
    private Long b(@NonNull ScanResult scanResult) {
        return Long.valueOf(this.e.a(scanResult.timestamp, TimeUnit.MICROSECONDS));
    }

    @NonNull
    private String b(@NonNull String str) {
        return str.replace("\"", "");
    }

    private JSONObject a(@Nullable String str, @Nullable String str2, boolean z, int i2, @Nullable Long l) {
        try {
            return new JSONObject().put("mac", str).put("ssid", str2).put("signal_strength", i2).put("is_connected", z).put("last_visible_offset_seconds", l);
        } catch (JSONException e2) {
            return null;
        }
    }

    private boolean f() {
        return ((Boolean) cg.a(new Callable<Boolean>() {
            /* renamed from: a */
            public Boolean call() {
                return Boolean.valueOf(ch.this.b.isWifiEnabled());
            }
        }, this.b, "getting wifi enabled state", "WifiManager", false)).booleanValue();
    }

    public List<a> b() {
        if (this.i.b() || this.i.c()) {
            ArrayList arrayList = new ArrayList();
            a(arrayList);
            this.i.a((JSONArray) arrayList);
        }
        return this.i.a();
    }

    private void a(List<a> list) {
        if (h()) {
            StringBuilder sb = new StringBuilder();
            try {
                Iterator it = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
                while (it.hasNext()) {
                    NetworkInterface networkInterface = (NetworkInterface) it.next();
                    byte[] hardwareAddress = networkInterface.getHardwareAddress();
                    if (hardwareAddress != null) {
                        for (byte valueOf : hardwareAddress) {
                            sb.append(String.format(Locale.US, "%02X:", Byte.valueOf(valueOf)));
                        }
                        if (sb.length() > 0) {
                            sb.deleteCharAt(sb.length() - 1);
                            list.add(new a(networkInterface.getName(), sb.toString()));
                            sb.setLength(0);
                        }
                    }
                }
            } catch (Throwable th) {
            }
        }
    }

    @Nullable
    public String b(final Context context) {
        return (String) cg.a(new Callable<String>() {
            /* renamed from: a */
            public String call() throws Exception {
                WifiConfiguration wifiConfiguration;
                if (!ch.this.g() || !ch.this.c.e(context) || (wifiConfiguration = (WifiConfiguration) ch.this.b.getClass().getMethod("getWifiApConfiguration", new Class[0]).invoke(ch.this.b, new Object[0])) == null) {
                    return null;
                }
                return wifiConfiguration.SSID;
            }
        }, this.b, "getting wifi access point name", "WifiManager");
    }

    public int c(final Context context) {
        return ((Integer) cg.a(new Callable<Integer>() {
            /* renamed from: a */
            public Integer call() throws Exception {
                if (!ch.this.g() || !ch.this.c.e(context)) {
                    return null;
                }
                int intValue = ((Integer) ch.this.b.getClass().getMethod("getWifiApState", new Class[0]).invoke(ch.this.b, new Object[0])).intValue();
                if (intValue > 10) {
                    intValue -= 10;
                }
                return Integer.valueOf(intValue);
            }
        }, this.b, "getting access point state", "WifiManager", -1)).intValue();
    }

    public static final class a {
        public final String a;
        public final String b;

        public a(String str, String str2) {
            this.a = str;
            this.b = str2;
        }
    }

    public void a(boolean z) {
        this.d.a(z);
        this.c.a(this.d.a());
    }

    /* access modifiers changed from: private */
    public synchronized boolean g() {
        return k() && this.j.o.k;
    }

    private synchronized boolean h() {
        return k() && this.j.o.j;
    }

    private synchronized boolean i() {
        return k() && this.j.o.i;
    }

    private synchronized boolean j() {
        return k() && this.j.o.h;
    }

    private synchronized boolean k() {
        return this.j != null;
    }

    @VisibleForTesting
    ch(Context context, @Nullable WifiManager wifiManager, @NonNull nw nwVar, @NonNull nq nqVar, @NonNull tv tvVar) {
        this.d = new nw();
        this.h = new n.a<>();
        this.i = new n.a<>();
        this.a = context;
        this.b = wifiManager;
        this.d = nwVar;
        this.c = nqVar;
        this.k = l();
        this.e = tvVar;
    }

    @NonNull
    private nr l() {
        if (cg.a(28)) {
            return new nr() {
                public boolean a(@NonNull Context context) {
                    return ch.this.c.c(context) && ch.this.c.e(context);
                }
            };
        }
        if (cg.a(26)) {
            return new nr() {
                public boolean a(@NonNull Context context) {
                    return ch.this.c.c(context) || ch.this.c.f(context);
                }
            };
        }
        return new nr() {
            public boolean a(@NonNull Context context) {
                return ch.this.c.c(context);
            }
        };
    }
}
