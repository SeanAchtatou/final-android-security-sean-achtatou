package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.qq;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class ru {
    /* access modifiers changed from: private */
    public final ud<String, rz> a = new ud<>();
    /* access modifiers changed from: private */
    public final HashMap<String, sd> b = new HashMap<>();
    private final sb c = new sb() {
        public void a(@NonNull String str, @NonNull sc scVar) {
            for (rz a2 : a(str)) {
                a2.a(scVar);
            }
        }

        public void a(@NonNull String str, @NonNull rw rwVar) {
            for (rz a2 : a(str)) {
                a2.a(rwVar);
            }
        }

        @NonNull
        public List<rz> a(@NonNull String str) {
            synchronized (ru.this.b) {
                Collection a2 = ru.this.a.a(str);
                if (a2 == null) {
                    ArrayList arrayList = new ArrayList();
                    return arrayList;
                }
                ArrayList arrayList2 = new ArrayList(a2);
                return arrayList2;
            }
        }
    };

    private static final class a {
        static final ru a = new ru();
    }

    public static final ru a() {
        return a.a;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public sd a(@NonNull Context context, @NonNull df dfVar, @NonNull qq.a aVar) {
        sd sdVar = this.b.get(dfVar.b());
        boolean z = true;
        if (sdVar == null) {
            synchronized (this.b) {
                sdVar = this.b.get(dfVar.b());
                if (sdVar == null) {
                    sd b2 = b(context, dfVar, aVar);
                    this.b.put(dfVar.b(), b2);
                    sdVar = b2;
                    z = false;
                }
            }
        }
        if (z) {
            sdVar.a(aVar);
        }
        return sdVar;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public sd b(@NonNull Context context, @NonNull df dfVar, @NonNull qq.a aVar) {
        return new sd(context, dfVar.b(), aVar, this.c);
    }

    @NonNull
    public sd a(@NonNull Context context, @NonNull df dfVar, @NonNull rz rzVar, @NonNull qq.a aVar) {
        sd a2;
        synchronized (this.b) {
            this.a.a(dfVar.b(), rzVar);
            a2 = a(context, dfVar, aVar);
        }
        return a2;
    }
}
