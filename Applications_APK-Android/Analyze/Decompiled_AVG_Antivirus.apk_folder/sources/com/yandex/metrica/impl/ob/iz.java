package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pf;

public class iz implements ky<iu, pf.a> {
    @NonNull
    private jd a;

    public iz(@NonNull jd jdVar) {
        this.a = jdVar;
    }

    @NonNull
    /* renamed from: a */
    public pf.a b(@NonNull iu iuVar) {
        pf.a aVar = new pf.a();
        aVar.b = this.a.b(iuVar.a);
        aVar.c = new pf.e[iuVar.b.size()];
        int i = 0;
        for (ix a2 : iuVar.b) {
            aVar.c[i] = this.a.b(a2);
            i++;
        }
        return aVar;
    }

    @NonNull
    public iu a(@NonNull pf.a aVar) {
        throw new UnsupportedOperationException();
    }
}
