package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.Collection;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.if  reason: invalid class name */
public class Cif {
    @NonNull
    private final tk a;
    @NonNull
    private final ih b;

    public Cif(@NonNull Context context) {
        this(new tk(), new ih(context));
    }

    @Nullable
    public Long a(@Nullable List<mj> list) {
        long j;
        if (cg.a((Collection) list)) {
            return null;
        }
        mj mjVar = list.get(Math.min(this.b.a(), list.size()) - 1);
        if (mjVar.a == mjVar.b) {
            j = mjVar.a;
        } else {
            j = this.a.a(mjVar.a, mjVar.b);
        }
        return Long.valueOf(j);
    }

    @VisibleForTesting
    Cif(@NonNull tk tkVar, @NonNull ih ihVar) {
        this.a = tkVar;
        this.b = ihVar;
    }
}
