package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.e;

public class kq<T, P extends e> implements kp<T> {
    @NonNull
    private final String a;
    @NonNull
    private final jj b;
    @NonNull
    private final ko<P> c;
    @NonNull
    private final ky<T, P> d;

    public kq(@NonNull String str, @NonNull jj jjVar, @NonNull ko<P> koVar, @NonNull ky<T, P> kyVar) {
        this.a = str;
        this.b = jjVar;
        this.c = koVar;
        this.d = kyVar;
    }

    public void a(@NonNull T t) {
        this.b.a(this.a, this.c.a(this.d.b(t)));
    }

    @NonNull
    public T a() {
        try {
            byte[] a2 = this.b.a(this.a);
            if (cg.a(a2)) {
                return this.d.a(this.c.c());
            }
            return this.d.a(this.c.b(a2));
        } catch (Throwable th) {
            return this.d.a(this.c.c());
        }
    }
}
