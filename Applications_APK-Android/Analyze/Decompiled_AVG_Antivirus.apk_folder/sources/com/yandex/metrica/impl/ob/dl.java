package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.dq;

public interface dl<T extends dq> {
    T b(@NonNull Context context, @NonNull df dfVar, @NonNull db dbVar);
}
