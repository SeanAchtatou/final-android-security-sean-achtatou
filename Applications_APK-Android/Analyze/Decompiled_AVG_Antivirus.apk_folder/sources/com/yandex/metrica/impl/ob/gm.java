package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.ab;
import java.util.List;

public abstract class gm<BaseHandler> {
    private final gv a;

    public abstract void a(ab.a aVar, @NonNull List<BaseHandler> list);

    public gm(gv gvVar) {
        this.a = gvVar;
    }

    public gv a() {
        return this.a;
    }
}
