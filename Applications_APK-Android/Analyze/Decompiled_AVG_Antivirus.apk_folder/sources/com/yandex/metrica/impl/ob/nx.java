package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class nx implements no {
    @NonNull
    private Set<String> a;

    public nx(@Nullable List<nt> list) {
        if (list == null) {
            this.a = new HashSet();
            return;
        }
        this.a = new HashSet(list.size());
        for (nt next : list) {
            if (next.b) {
                this.a.add(next.a);
            }
        }
    }

    public boolean a(@NonNull String str) {
        return this.a.contains(str);
    }

    public String toString() {
        return "StartupBasedPermissionStrategy{mEnabledPermissions=" + this.a + '}';
    }
}
