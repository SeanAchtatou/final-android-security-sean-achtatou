package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.core.view.MotionEventCompat;
import com.yandex.metrica.impl.ob.k;

public class l {
    @NonNull
    private final Context a;
    @NonNull
    private final j b;

    public l(@NonNull Context context) {
        this(context, new j());
    }

    @VisibleForTesting
    l(@NonNull Context context, @NonNull j jVar) {
        this.a = context;
        this.b = jVar;
    }

    @Nullable
    public k a() {
        if (cg.a(28)) {
            return b();
        }
        return null;
    }

    @TargetApi(MotionEventCompat.AXIS_RELATIVE_Y)
    @NonNull
    private k b() {
        Boolean bool;
        ActivityManager activityManager = (ActivityManager) this.a.getSystemService("activity");
        k.a aVar = null;
        if (activityManager != null) {
            bool = Boolean.valueOf(activityManager.isBackgroundRestricted());
        } else {
            bool = null;
        }
        UsageStatsManager usageStatsManager = (UsageStatsManager) this.a.getSystemService("usagestats");
        if (usageStatsManager != null) {
            aVar = this.b.a(usageStatsManager.getAppStandbyBucket());
        }
        return new k(aVar, bool);
    }
}
