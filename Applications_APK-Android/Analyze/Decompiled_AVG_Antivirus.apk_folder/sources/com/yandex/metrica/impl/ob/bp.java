package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.yandex.metrica.CounterConfiguration;

class bp extends cz {
    protected aa a;
    protected bk b;
    private ae c = new ae();

    protected bp(@NonNull da daVar, @NonNull CounterConfiguration counterConfiguration) {
        super(daVar, counterConfiguration);
    }

    /* access modifiers changed from: package-private */
    public void a(vk vkVar) {
        this.a = new aa(vkVar);
    }

    /* access modifiers changed from: package-private */
    public Bundle b() {
        Bundle bundle = new Bundle();
        h().a(bundle);
        g().b(bundle);
        return bundle;
    }

    /* access modifiers changed from: package-private */
    public void a(ry ryVar) {
        b(ryVar);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.c.b();
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.c.a();
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.c.c();
    }

    /* access modifiers changed from: package-private */
    public void b(ry ryVar) {
        if (ryVar != null) {
            h().c(ryVar.a());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        this.a.a(str, str2);
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.a.a();
    }

    /* access modifiers changed from: package-private */
    public bk f() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void a(bk bkVar) {
        this.b = bkVar;
    }
}
