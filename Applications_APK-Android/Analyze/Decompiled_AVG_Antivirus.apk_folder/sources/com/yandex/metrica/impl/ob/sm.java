package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrength;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;

public class sm {
    @Nullable
    private Integer a;
    @Nullable
    private final Integer b;
    @Nullable
    private final Integer c;
    @Nullable
    private final Integer d;
    @Nullable
    private final Integer e;
    @Nullable
    private final String f;
    @Nullable
    private final String g;
    private final boolean h;
    private final int i;
    @Nullable
    private final Integer j;
    @Nullable
    private final Long k;

    @TargetApi(17)
    static abstract class b {
        static final Integer a = Integer.MAX_VALUE;
        static final Integer b = Integer.MAX_VALUE;
        static final Integer c = Integer.MAX_VALUE;
        static final Integer d = Integer.MAX_VALUE;
        static final Integer e = Integer.MAX_VALUE;
        @NonNull
        private final tv f;

        /* access modifiers changed from: package-private */
        public abstract sm a(CellInfo cellInfo);

        public b() {
            this(new tv());
        }

        /* access modifiers changed from: protected */
        @TargetApi(17)
        public sm a(@Nullable Integer num, @Nullable Integer num2, @Nullable CellSignalStrength cellSignalStrength, @Nullable Integer num3, @Nullable Integer num4, boolean z, int i, @Nullable Integer num5, long j) {
            Integer num6;
            Integer num7;
            Integer num8;
            Integer num9;
            Integer num10 = num;
            Integer num11 = num2;
            Integer num12 = num3;
            Integer num13 = num4;
            Integer num14 = num5;
            if (num10 != null) {
                if (num10 == a) {
                    num10 = null;
                }
                num6 = num10;
            } else {
                num6 = null;
            }
            if (num11 != null) {
                if (num11 == b) {
                    num11 = null;
                }
                num7 = num11;
            } else {
                num7 = null;
            }
            Integer valueOf = cellSignalStrength != null ? Integer.valueOf(cellSignalStrength.getDbm()) : null;
            if (num13 != null) {
                if (num13 == c) {
                    num13 = null;
                }
                num8 = num13;
            } else {
                num8 = null;
            }
            if (num12 != null) {
                if (num12 == d) {
                    num12 = null;
                }
                num9 = num12;
            } else {
                num9 = null;
            }
            return new sm(num8, num9, num7, num6, null, null, valueOf, z, i, (num14 == null || num14 == e) ? null : num14, a(j));
        }

        @Nullable
        private Long a(long j) {
            Long l = null;
            if (j <= 0) {
                return null;
            }
            long d2 = this.f.d(j, TimeUnit.NANOSECONDS);
            if (d2 > 0 && d2 < TimeUnit.HOURS.toSeconds(1)) {
                l = Long.valueOf(d2);
            }
            if (l != null) {
                return l;
            }
            long a2 = this.f.a(j, TimeUnit.NANOSECONDS);
            if (a2 <= 0 || a2 >= TimeUnit.HOURS.toSeconds(1)) {
                return l;
            }
            return Long.valueOf(a2);
        }

        @VisibleForTesting
        public b(@NonNull tv tvVar) {
            this.f = tvVar;
        }
    }

    @TargetApi(17)
    static class c extends b {
        c() {
        }

        /* access modifiers changed from: package-private */
        public sm a(CellInfo cellInfo) {
            CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
            CellIdentityGsm cellIdentity = cellInfoGsm.getCellIdentity();
            return a(Integer.valueOf(cellIdentity.getCid()), Integer.valueOf(cellIdentity.getLac()), cellInfoGsm.getCellSignalStrength(), Integer.valueOf(cellIdentity.getMnc()), Integer.valueOf(cellIdentity.getMcc()), cellInfoGsm.isRegistered(), 1, null, cellInfoGsm.getTimeStamp());
        }
    }

    @TargetApi(17)
    static class a extends b {
        a() {
        }

        /* access modifiers changed from: package-private */
        public sm a(CellInfo cellInfo) {
            CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfo;
            return a(null, null, cellInfoCdma.getCellSignalStrength(), null, null, cellInfoCdma.isRegistered(), 2, null, cellInfoCdma.getTimeStamp());
        }
    }

    @TargetApi(17)
    static class d extends b {
        d() {
        }

        /* access modifiers changed from: package-private */
        public sm a(CellInfo cellInfo) {
            CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
            CellIdentityLte cellIdentity = cellInfoLte.getCellIdentity();
            return a(Integer.valueOf(cellIdentity.getCi()), Integer.valueOf(cellIdentity.getTac()), cellInfoLte.getCellSignalStrength(), Integer.valueOf(cellIdentity.getMnc()), Integer.valueOf(cellIdentity.getMcc()), cellInfoLte.isRegistered(), 4, Integer.valueOf(cellIdentity.getPci()), cellInfoLte.getTimeStamp());
        }
    }

    @TargetApi(18)
    static class e extends b {
        e() {
        }

        /* access modifiers changed from: package-private */
        public sm a(CellInfo cellInfo) {
            CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfo;
            CellIdentityWcdma cellIdentity = cellInfoWcdma.getCellIdentity();
            return a(Integer.valueOf(cellIdentity.getCid()), Integer.valueOf(cellIdentity.getLac()), cellInfoWcdma.getCellSignalStrength(), Integer.valueOf(cellIdentity.getMnc()), Integer.valueOf(cellIdentity.getMcc()), cellInfoWcdma.isRegistered(), 3, Integer.valueOf(cellIdentity.getPsc()), cellInfoWcdma.getTimeStamp());
        }
    }

    public sm(@Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Integer num4, @Nullable String str, @Nullable String str2, @Nullable Integer num5, boolean z, int i2, @Nullable Integer num6, @Nullable Long l) {
        this.b = num;
        this.c = num2;
        this.d = num3;
        this.e = num4;
        this.f = str;
        this.g = str2;
        this.a = num5;
        this.h = z;
        this.i = i2;
        this.j = num6;
        this.k = l;
    }

    @TargetApi(17)
    public static sm a(CellInfo cellInfo) {
        b b2 = b(cellInfo);
        if (b2 == null) {
            return null;
        }
        return b2.a(cellInfo);
    }

    @TargetApi(17)
    public static b b(CellInfo cellInfo) {
        if (cellInfo instanceof CellInfoGsm) {
            return new c();
        }
        if (cellInfo instanceof CellInfoCdma) {
            return new a();
        }
        if (cellInfo instanceof CellInfoLte) {
            return new d();
        }
        if (!cg.a(18) || !(cellInfo instanceof CellInfoWcdma)) {
            return null;
        }
        return new e();
    }

    @Nullable
    public Integer a() {
        return this.a;
    }

    @Nullable
    public Integer b() {
        return this.b;
    }

    @Nullable
    public Integer c() {
        return this.c;
    }

    @Nullable
    public Integer d() {
        return this.d;
    }

    @Nullable
    public Integer e() {
        return this.e;
    }

    @Nullable
    public String f() {
        return this.f;
    }

    @Nullable
    public String g() {
        return this.g;
    }

    public boolean h() {
        return this.h;
    }

    public void a(@Nullable Integer num) {
        this.a = num;
    }

    public int i() {
        return this.i;
    }

    @Nullable
    public Integer j() {
        return this.j;
    }

    @Nullable
    public Long k() {
        return this.k;
    }

    public String toString() {
        return "CellDescription{mSignalStrength=" + this.a + ", mMobileCountryCode=" + this.b + ", mMobileNetworkCode=" + this.c + ", mLocationAreaCode=" + this.d + ", mCellId=" + this.e + ", mOperatorName='" + this.f + '\'' + ", mNetworkType='" + this.g + '\'' + ", mConnected=" + this.h + ", mCellType=" + this.i + ", mPci=" + this.j + ", mLastVisibleTimeOffset=" + this.k + '}';
    }
}
