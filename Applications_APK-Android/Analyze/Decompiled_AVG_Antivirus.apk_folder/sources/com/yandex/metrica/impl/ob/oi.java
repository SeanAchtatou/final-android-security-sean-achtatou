package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
public class oi extends oc {
    private oj d;

    public oi(Context context) {
        this(context, null);
    }

    public oi(Context context, String str) {
        super(context, str);
        this.d = new oj("LOCATION_TRACKING_ENABLED");
    }

    /* access modifiers changed from: protected */
    public String f() {
        return "_serviceproviderspreferences";
    }

    public boolean a() {
        return this.c.getBoolean(this.d.b(), false);
    }

    public void b() {
        h(this.d.b()).j();
    }
}
