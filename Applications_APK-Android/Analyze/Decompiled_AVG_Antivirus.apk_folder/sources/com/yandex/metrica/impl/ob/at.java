package com.yandex.metrica.impl.ob;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.IMetricaService;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

public class at {
    public static final long a = TimeUnit.SECONDS.toMillis(10);
    private final Context b;
    private final uv c;
    private boolean d;
    private final List<a> e = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */
    public volatile IMetricaService f = null;
    private final Object g = new Object();
    private final Runnable h = new Runnable() {
        public void run() {
            at.this.i();
        }
    };
    private final ServiceConnection i = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            IMetricaService unused = at.this.f = IMetricaService.Stub.asInterface(service);
            at.this.j();
        }

        public void onServiceDisconnected(ComponentName name) {
            IMetricaService unused = at.this.f = null;
            at.this.k();
        }
    };

    interface a {
        void a();

        void b();
    }

    public Context a() {
        return this.b;
    }

    public at(Context context, uv uvVar) {
        this.b = context.getApplicationContext();
        this.c = uvVar;
        this.d = false;
    }

    public synchronized void b() {
        if (this.f == null) {
            try {
                this.b.bindService(bz.b(this.b), this.i, 1);
            } catch (Throwable th) {
            }
        }
    }

    public void c() {
        a(this.c);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(@NonNull uv uvVar) {
        synchronized (this.g) {
            uvVar.b(this.h);
            if (!this.d) {
                uvVar.a(this.h, a);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void d() {
        this.c.b(this.h);
    }

    public boolean e() {
        return this.f != null;
    }

    public IMetricaService f() {
        return this.f;
    }

    /* access modifiers changed from: private */
    public synchronized void i() {
        if (this.b != null && e()) {
            try {
                this.b.unbindService(this.i);
                this.f = null;
            } catch (Throwable th) {
            }
        }
        this.f = null;
        k();
    }

    /* access modifiers changed from: private */
    public void j() {
        for (a a2 : this.e) {
            a2.a();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        for (a b2 : this.e) {
            b2.b();
        }
    }

    public void a(a aVar) {
        this.e.add(aVar);
    }

    public void g() {
        synchronized (this.g) {
            this.d = true;
        }
        d();
    }

    public void h() {
        this.d = false;
        c();
    }
}
