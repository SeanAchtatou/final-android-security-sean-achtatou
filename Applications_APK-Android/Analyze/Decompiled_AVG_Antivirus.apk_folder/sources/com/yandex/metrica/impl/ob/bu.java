package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.IMetricaService;
import com.yandex.metrica.impl.ob.ab;
import com.yandex.metrica.impl.ob.at;
import com.yandex.metrica.impl.ob.o;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import org.json.JSONException;

public class bu implements at.a {
    /* access modifiers changed from: private */
    public final ai a;
    /* access modifiers changed from: private */
    public final at b;
    /* access modifiers changed from: private */
    public final Object c;
    private final uv d;

    public interface c {
        t a(t tVar);
    }

    public bu(ai aiVar) {
        this(aiVar, cj.l().c());
    }

    public bu(@NonNull ai aiVar, @NonNull uv uvVar) {
        this.c = new Object();
        this.a = aiVar;
        this.d = uvVar;
        this.b = aiVar.a();
        this.b.a(this);
    }

    public Future<Void> a(d dVar) {
        return this.d.a(dVar.c() ? new a(this, dVar) : new b(dVar));
    }

    public void a() {
        synchronized (this.c) {
            this.c.notifyAll();
        }
    }

    public void b() {
    }

    private class b implements Callable<Void> {
        final d b;

        private b(d dVar) {
            this.b = dVar;
        }

        /* renamed from: a */
        public Void call() {
            int i = 0;
            do {
                try {
                    IMetricaService f = bu.this.b.f();
                    if (f == null || !a(f, this.b)) {
                        i++;
                        if (!b() || az.a.get()) {
                        }
                    } else {
                        cn.a().a(this);
                        return null;
                    }
                } catch (Throwable th) {
                    d dVar = this.b;
                    return null;
                } finally {
                    cn.a().a(this);
                }
            } while (i < 20);
            return null;
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            bu.this.b.b();
            c();
            return true;
        }

        private boolean a(IMetricaService iMetricaService, d dVar) {
            try {
                bu.this.a.a(iMetricaService, dVar.b(), dVar.b);
                return true;
            } catch (RemoteException e) {
                return false;
            }
        }

        private void c() {
            synchronized (bu.this.c) {
                if (!bu.this.b.e()) {
                    try {
                        bu.this.c.wait(500, 0);
                    } catch (InterruptedException e) {
                        bu.this.c.notifyAll();
                    }
                }
            }
        }
    }

    @VisibleForTesting
    class a extends b {
        private final iq d;
        private final tl e;

        a(bu buVar, @NonNull d dVar) {
            this(dVar, new iq(), new tl());
        }

        @VisibleForTesting
        a(d dVar, @NonNull iq iqVar, @NonNull tl tlVar) {
            super(dVar);
            this.d = iqVar;
            this.e = tlVar;
        }

        /* renamed from: a */
        public Void call() {
            if (this.e.a("Metrica")) {
                b(this.b);
                return null;
            }
            bu.this.b.c();
            return super.call();
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            a(this.b);
            return false;
        }

        /* access modifiers changed from: package-private */
        @VisibleForTesting
        public void a(@NonNull d dVar) {
            if (dVar.d().o() == 0) {
                Context b = bu.this.a.b();
                Intent b2 = bz.b(b);
                dVar.d().a(ab.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT.a());
                b2.putExtras(dVar.d().a(dVar.a().b()));
                try {
                    b.startService(b2);
                } catch (Throwable th) {
                    b(dVar);
                }
            } else {
                b(dVar);
            }
        }

        /* access modifiers changed from: package-private */
        public void b(@NonNull d dVar) {
            PrintWriter printWriter;
            File b = ag.b(bu.this.b.a());
            if (this.d.a(b)) {
                da g = dVar.a().g();
                try {
                    printWriter = new PrintWriter(new BufferedOutputStream(new FileOutputStream(new File(b, g.e() + "-" + g.f()))));
                    try {
                        printWriter.write(new jh(dVar.a, dVar.a(), dVar.e).k());
                    } catch (IOException | JSONException e2) {
                    } catch (Throwable th) {
                        th = th;
                        cg.a((Closeable) printWriter);
                        throw th;
                    }
                } catch (IOException e3) {
                    printWriter = null;
                } catch (JSONException e4) {
                    printWriter = null;
                } catch (Throwable th2) {
                    th = th2;
                    printWriter = null;
                    cg.a((Closeable) printWriter);
                    throw th;
                }
                cg.a((Closeable) printWriter);
            }
        }
    }

    public static class d {
        /* access modifiers changed from: private */
        public t a;
        /* access modifiers changed from: private */
        public bp b;
        private boolean c = false;
        private c d;
        /* access modifiers changed from: private */
        @Nullable
        public HashMap<o.a, Integer> e;

        d(t tVar, bp bpVar) {
            this.a = tVar;
            this.b = new bp(new da(bpVar.g()), new CounterConfiguration(bpVar.h()));
        }

        /* access modifiers changed from: package-private */
        public d a(c cVar) {
            this.d = cVar;
            return this;
        }

        /* access modifiers changed from: package-private */
        public d a(@NonNull HashMap<o.a, Integer> hashMap) {
            this.e = hashMap;
            return this;
        }

        /* access modifiers changed from: package-private */
        public d a(boolean z) {
            this.c = z;
            return this;
        }

        /* access modifiers changed from: package-private */
        public bp a() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public t b() {
            c cVar = this.d;
            return cVar != null ? cVar.a(this.a) : this.a;
        }

        /* access modifiers changed from: package-private */
        public boolean c() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        @VisibleForTesting
        public t d() {
            return this.a;
        }

        public String toString() {
            return "ReportToSend{mReport=" + this.a + ", mEnvironment=" + this.b + ", mCrash=" + this.c + ", mAction=" + this.d + ", mTrimmedFields=" + this.e + '}';
        }
    }
}
