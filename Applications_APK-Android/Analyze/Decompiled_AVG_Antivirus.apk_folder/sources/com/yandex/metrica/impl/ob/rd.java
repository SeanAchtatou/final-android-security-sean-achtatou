package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.List;

public class rd {
    @NonNull
    public final String a;
    @NonNull
    public final List<String> b;

    public rd(@NonNull String str, @NonNull List<String> list) {
        this.a = str;
        this.b = list;
    }

    public String toString() {
        return "SdkItem{name='" + this.a + '\'' + ", classes=" + this.b + '}';
    }
}
