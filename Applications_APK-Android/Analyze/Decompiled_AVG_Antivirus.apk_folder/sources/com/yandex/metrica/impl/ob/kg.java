package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import java.util.List;

public class kg extends ki {
    static final oj a = new oj("UUID");
    static final oj b = new oj("DEVICE_ID_POSSIBLE");
    static final oj c = new oj("DEVICE_ID");
    static final oj d = new oj("DEVICE_ID_HASH");
    static final oj e = new oj("AD_URL_GET");
    static final oj f = new oj("AD_URL_REPORT");
    static final oj g = new oj("CUSTOM_HOSTS");
    static final oj h = new oj("SERVER_TIME_OFFSET");
    static final oj i = new oj("STARTUP_REQUEST_TIME");
    static final oj j = new oj("CLIDS");
    static final oj k = new oj("REFERRER");
    static final oj l = new oj("DEFERRED_DEEP_LINK_WAS_CHECKED");
    static final oj m = new oj("REFERRER_FROM_PLAY_SERVICES_WAS_CHECKED");
    static final oj n = new oj("DEPRECATED_NATIVE_CRASHES_CHECKED");
    static final oj o = new oj("API_LEVEL");

    public kg(jq jqVar) {
        super(jqVar);
    }

    public String a(String str) {
        return c(a.b(), str);
    }

    public String b(String str) {
        return c(c.b(), str);
    }

    public String c(String str) {
        return c(d.b(), str);
    }

    public String a() {
        return c(b.b(), "");
    }

    public String d(String str) {
        return c(e.b(), str);
    }

    public String e(String str) {
        return c(f.b(), str);
    }

    public List<String> b() {
        String c2 = c(g.b(), null);
        if (TextUtils.isEmpty(c2)) {
            return null;
        }
        return th.b(c2);
    }

    public long a(long j2) {
        return b(h.a(), j2);
    }

    public long b(long j2) {
        return b(i.b(), j2);
    }

    public String f(String str) {
        return c(j.b(), str);
    }

    public long c(long j2) {
        return b(o.b(), j2);
    }

    public String c() {
        return c(k.b(), null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean */
    public boolean d() {
        return b(l.b(), false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean */
    public boolean e() {
        return b(m.b(), false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean */
    public boolean f() {
        return b(n.b(), false);
    }

    public kg g(String str) {
        return (kg) b(a.b(), str);
    }

    public kg h(String str) {
        return (kg) b(c.b(), str);
    }

    public kg i(String str) {
        return (kg) b(d.b(), str);
    }

    public kg j(String str) {
        return (kg) b(e.b(), str);
    }

    public kg a(List<String> list) {
        return (kg) b(g.b(), th.a(list));
    }

    public kg k(String str) {
        return (kg) b(f.b(), str);
    }

    public kg d(long j2) {
        return (kg) a(h.b(), j2);
    }

    public kg e(long j2) {
        return (kg) a(i.b(), j2);
    }

    public kg l(String str) {
        return (kg) b(j.b(), str);
    }

    public kg f(long j2) {
        return (kg) a(o.b(), j2);
    }

    public kg m(String str) {
        return (kg) b(b.b(), str);
    }

    public kg n(String str) {
        return (kg) b(k.b(), str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.a(java.lang.String, boolean):T
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, int):T
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, long):T
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, boolean):T */
    public kg g() {
        return (kg) a(l.b(), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.a(java.lang.String, boolean):T
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, int):T
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, long):T
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, boolean):T */
    public kg h() {
        return (kg) a(m.b(), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.a(java.lang.String, boolean):T
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, int):T
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, long):T
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, boolean):T */
    public kg i() {
        return (kg) a(n.b(), true);
    }
}
