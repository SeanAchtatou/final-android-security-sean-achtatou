package com.yandex.metrica.impl.ob;

import java.io.IOException;

public interface pm {

    public static final class a extends e {
        public C0010a[] b;

        /* renamed from: com.yandex.metrica.impl.ob.pm$a$a  reason: collision with other inner class name */
        public static final class C0010a extends e {
            private static volatile C0010a[] d;
            public String b;
            public boolean c;

            public static C0010a[] d() {
                if (d == null) {
                    synchronized (c.a) {
                        if (d == null) {
                            d = new C0010a[0];
                        }
                    }
                }
                return d;
            }

            public C0010a() {
                e();
            }

            public C0010a e() {
                this.b = "";
                this.c = false;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c);
            }

            /* renamed from: b */
            public C0010a a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        this.b = aVar.i();
                    } else if (a == 16) {
                        this.c = aVar.h();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public a() {
            d();
        }

        public a d() {
            this.b = C0010a.d();
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            C0010a[] aVarArr = this.b;
            if (aVarArr != null && aVarArr.length > 0) {
                int i = 0;
                while (true) {
                    C0010a[] aVarArr2 = this.b;
                    if (i >= aVarArr2.length) {
                        break;
                    }
                    C0010a aVar = aVarArr2[i];
                    if (aVar != null) {
                        bVar.a(1, aVar);
                    }
                    i++;
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c = super.c();
            C0010a[] aVarArr = this.b;
            if (aVarArr != null && aVarArr.length > 0) {
                int i = 0;
                while (true) {
                    C0010a[] aVarArr2 = this.b;
                    if (i >= aVarArr2.length) {
                        break;
                    }
                    C0010a aVar = aVarArr2[i];
                    if (aVar != null) {
                        c += b.b(1, aVar);
                    }
                    i++;
                }
            }
            return c;
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    int b2 = g.b(aVar, 10);
                    C0010a[] aVarArr = this.b;
                    int length = aVarArr == null ? 0 : aVarArr.length;
                    C0010a[] aVarArr2 = new C0010a[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, aVarArr2, 0, length);
                    }
                    while (length < aVarArr2.length - 1) {
                        aVarArr2[length] = new C0010a();
                        aVar.a(aVarArr2[length]);
                        aVar.a();
                        length++;
                    }
                    aVarArr2[length] = new C0010a();
                    aVar.a(aVarArr2[length]);
                    this.b = aVarArr2;
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }

        public static a a(byte[] bArr) throws d {
            return (a) e.a(new a(), bArr);
        }
    }
}
