package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class ei {
    private final Object a = new Object();
    private final en b;
    private final HashMap<eh, ej> c = new HashMap<>();
    private final ud<a, eh> d = new ud<>();
    @NonNull
    private final Context e;
    private volatile int f = 0;

    public ei(@NonNull Context context, @NonNull en enVar) {
        this.e = context.getApplicationContext();
        this.b = enVar;
    }

    public ej a(@NonNull eh ehVar, @NonNull db dbVar) {
        ej ejVar;
        synchronized (this.a) {
            ejVar = this.c.get(ehVar);
            if (ejVar == null) {
                ejVar = ehVar.a().a(this.e, this.b, ehVar, dbVar);
                this.c.put(ehVar, ejVar);
                this.d.a(new a(ehVar), ehVar);
                this.f++;
            }
        }
        return ejVar;
    }

    public void a(@NonNull String str, int i, String str2) {
        a(str, Integer.valueOf(i), str2);
    }

    private void a(@NonNull String str, @Nullable Integer num, @Nullable String str2) {
        synchronized (this.a) {
            Collection<eh> b2 = this.d.b(new a(str, num, str2));
            if (!cg.a((Collection) b2)) {
                this.f -= b2.size();
                ArrayList arrayList = new ArrayList(b2.size());
                for (eh remove : b2) {
                    arrayList.add(this.c.remove(remove));
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((ej) it.next()).a();
                }
            }
        }
    }

    public int a() {
        return this.f;
    }

    private static final class a {
        @NonNull
        private final String a;
        @Nullable
        private final Integer b;
        @Nullable
        private final String c;

        a(@NonNull String str, @Nullable Integer num, @Nullable String str2) {
            this.a = str;
            this.b = num;
            this.c = str2;
        }

        a(@NonNull eh ehVar) {
            this(ehVar.c(), ehVar.d(), ehVar.e());
        }

        public int hashCode() {
            int hashCode = this.a.hashCode() * 31;
            Integer num = this.b;
            int i = 0;
            int hashCode2 = (hashCode + (num != null ? num.hashCode() : 0)) * 31;
            String str = this.c;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode2 + i;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            a aVar = (a) o;
            if (!this.a.equals(aVar.a)) {
                return false;
            }
            Integer num = this.b;
            if (num == null ? aVar.b != null : !num.equals(aVar.b)) {
                return false;
            }
            String str = this.c;
            if (str != null) {
                return str.equals(aVar.c);
            }
            if (aVar.c == null) {
                return true;
            }
            return false;
        }
    }
}
