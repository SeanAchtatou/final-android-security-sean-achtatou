package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import androidx.annotation.NonNull;
import androidx.core.view.MotionEventCompat;
import java.io.File;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.concurrent.Callable;

public final class bj {
    private static final vp a = new vp();
    private static final tn<Integer, a> b = new tn<Integer, a>(a.UNDEFINED) {
        {
            a(1, a.WIFI);
            a(0, a.CELL);
        }
    };
    @TargetApi(MotionEventCompat.AXIS_BRAKE)
    private static final tn<Integer, a> c = new tn<Integer, a>(a.UNDEFINED) {
        {
            a(1, a.WIFI);
            a(0, a.CELL);
        }
    };
    private static final tn<a, Integer> d = new tn<a, Integer>(2) {
        {
            a(a.CELL, 0);
            a(a.WIFI, 1);
        }
    };

    public enum a {
        WIFI,
        CELL,
        OFFLINE,
        UNDEFINED
    }

    public static final class b {
        private static final String[] a = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};

        public static boolean a() {
            try {
                if (new File("/system/app/Superuser.apk").exists()) {
                    return true;
                }
                return false;
            } catch (Throwable th) {
                return false;
            }
        }

        public static boolean b() {
            String[] strArr = a;
            int length = strArr.length;
            int i = 0;
            while (i < length) {
                String str = strArr[i];
                try {
                    if (new File(str + "su").exists()) {
                        return true;
                    }
                    i++;
                } catch (Throwable th) {
                }
            }
            return false;
        }

        public static int c() {
            return (a() || b()) ? 1 : 0;
        }
    }

    @NonNull
    public static com.yandex.metrica.b a(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        Point b2 = b(context);
        int i = b2.x;
        int i2 = b2.y;
        float f = displayMetrics.density;
        float f2 = (float) i;
        float f3 = (float) i2;
        float min = Math.min(f2 / f, f3 / f);
        float f4 = f * 160.0f;
        float f5 = f2 / f4;
        float f6 = f3 / f4;
        double sqrt = Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
        if (a(context, sqrt)) {
            return com.yandex.metrica.b.TV;
        }
        if (sqrt >= 7.0d || min >= 600.0f) {
            return com.yandex.metrica.b.TABLET;
        }
        return com.yandex.metrica.b.PHONE;
    }

    private static boolean a(Context context, double d2) {
        return d2 >= 15.0d && !a.b(context, "android.hardware.touchscreen");
    }

    @TargetApi(17)
    @NonNull
    public static Point b(Context context) {
        final WindowManager windowManager = (WindowManager) context.getSystemService("window");
        final Display display = (Display) cg.a(new Callable<Display>() {
            /* renamed from: a */
            public Display call() {
                return windowManager.getDefaultDisplay();
            }
        }, windowManager, "getting display", "WindowManager");
        return (Point) cg.a(new Callable<Point>() {
            /* renamed from: a */
            public Point call() {
                int i;
                int i2;
                if (Build.VERSION.SDK_INT >= 17) {
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    display.getRealMetrics(displayMetrics);
                    i = displayMetrics.widthPixels;
                    i2 = displayMetrics.heightPixels;
                } else if (Build.VERSION.SDK_INT >= 14) {
                    try {
                        Method method = Display.class.getMethod("getRawHeight", new Class[0]);
                        i = ((Integer) Display.class.getMethod("getRawWidth", new Class[0]).invoke(display, new Object[0])).intValue();
                        i2 = ((Integer) method.invoke(display, new Object[0])).intValue();
                    } catch (Throwable th) {
                        i = display.getWidth();
                        i2 = display.getHeight();
                    }
                } else {
                    i = display.getWidth();
                    i2 = display.getHeight();
                }
                return new Point(i, i2);
            }
        }, display, "getting display metrics", "Display", new Point(0, 0));
    }

    public static Integer c(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return null;
        }
        int intExtra = registerReceiver.getIntExtra("level", -1);
        int intExtra2 = registerReceiver.getIntExtra("scale", -1);
        if (intExtra <= -1 || intExtra2 <= 0) {
            return null;
        }
        return Integer.valueOf(Math.round((((float) intExtra) / ((float) intExtra2)) * 100.0f));
    }

    @NonNull
    public static String a(@NonNull Locale locale) {
        String language = locale.getLanguage();
        String country = locale.getCountry();
        StringBuilder sb = new StringBuilder(language);
        if (cg.a(21)) {
            String script = locale.getScript();
            if (!TextUtils.isEmpty(script)) {
                sb.append('-');
                sb.append(script);
            }
        }
        if (!TextUtils.isEmpty(country)) {
            sb.append('_');
            sb.append(country);
        }
        return sb.toString();
    }

    @NonNull
    public static a d(@NonNull Context context) {
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        return (a) cg.a(new Callable<a>() {
            /* renamed from: a */
            public a call() {
                if (cg.a(23)) {
                    return bj.c(connectivityManager);
                }
                return bj.d(connectivityManager);
            }
        }, connectivityManager, "getting connection type", "ConnectivityManager", a.UNDEFINED);
    }

    /* access modifiers changed from: private */
    @TargetApi(MotionEventCompat.AXIS_BRAKE)
    @NonNull
    public static a c(@NonNull ConnectivityManager connectivityManager) {
        a aVar = a.UNDEFINED;
        Network activeNetwork = connectivityManager.getActiveNetwork();
        if (activeNetwork == null) {
            return a.OFFLINE;
        }
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(activeNetwork);
        if (networkInfo != null && !networkInfo.isConnected()) {
            return a.OFFLINE;
        }
        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
        if (networkCapabilities == null) {
            return aVar;
        }
        for (Integer next : c.a()) {
            if (networkCapabilities.hasTransport(next.intValue())) {
                return c.a(next);
            }
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    @NonNull
    public static a d(@NonNull ConnectivityManager connectivityManager) {
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return a.OFFLINE;
        }
        return b.a(Integer.valueOf(activeNetworkInfo.getType()));
    }

    public static int e(@NonNull Context context) {
        return d.a(d(context)).intValue();
    }

    public static long a(boolean z) {
        try {
            StatFs b2 = b(z);
            return (((long) b2.getBlockCount()) * ((long) b2.getBlockSize())) / 1024;
        } catch (Throwable th) {
            return 0;
        }
    }

    public static StatFs b(boolean z) {
        if (!z) {
            return new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        }
        return new StatFs(Environment.getRootDirectory().getAbsolutePath());
    }

    public static long c(boolean z) {
        try {
            StatFs b2 = b(z);
            return (((long) b2.getAvailableBlocks()) * ((long) b2.getBlockSize())) / 1024;
        } catch (Throwable th) {
            return 0;
        }
    }
}
