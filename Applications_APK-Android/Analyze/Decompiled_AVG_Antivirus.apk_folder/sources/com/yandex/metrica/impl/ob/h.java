package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;

public class h {
    public static final long a = TimeUnit.SECONDS.toMillis(10);
    private long b;
    @NonNull
    private final tw c;

    public interface b {
        void a();
    }

    public static class a {
        /* access modifiers changed from: private */
        public boolean a;
        @NonNull
        private final b b;
        @NonNull
        private final h c;

        public a(@NonNull Runnable runnable) {
            this(runnable, af.a().i());
        }

        @VisibleForTesting
        a(@NonNull final Runnable runnable, @NonNull h hVar) {
            this.a = false;
            this.b = new b() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.yandex.metrica.impl.ob.h.a.a(com.yandex.metrica.impl.ob.h$a, boolean):boolean
                 arg types: [com.yandex.metrica.impl.ob.h$a, int]
                 candidates:
                  com.yandex.metrica.impl.ob.h.a.a(long, com.yandex.metrica.impl.ob.uv):void
                  com.yandex.metrica.impl.ob.h.a.a(com.yandex.metrica.impl.ob.h$a, boolean):boolean */
                public void a() {
                    boolean unused = a.this.a = true;
                    runnable.run();
                }
            };
            this.c = hVar;
        }

        public void a(long j, @NonNull uv uvVar) {
            if (!this.a) {
                this.c.a(j, uvVar, this.b);
            }
        }
    }

    public h() {
        this(new tw());
    }

    @VisibleForTesting
    h(@NonNull tw twVar) {
        this.c = twVar;
    }

    public void a() {
        this.b = this.c.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public void a(long j, @NonNull uv uvVar, @NonNull final b bVar) {
        uvVar.a(new Runnable() {
            public void run() {
                bVar.a();
            }
        }, Math.max(j - (this.c.a() - this.b), 0L));
    }
}
