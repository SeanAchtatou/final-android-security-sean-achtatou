package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pl;

class hf implements ky<he, pl.a> {
    hf() {
    }

    @NonNull
    /* renamed from: a */
    public pl.a b(@NonNull he heVar) {
        pl.a aVar = new pl.a();
        aVar.e = new int[heVar.c().size()];
        int i = 0;
        for (Integer intValue : heVar.c()) {
            aVar.e[i] = intValue.intValue();
            i++;
        }
        aVar.d = heVar.d();
        aVar.c = heVar.e();
        aVar.b = heVar.b();
        return aVar;
    }

    @NonNull
    public he a(@NonNull pl.a aVar) {
        return new he(aVar.b, aVar.c, aVar.d, aVar.e);
    }
}
