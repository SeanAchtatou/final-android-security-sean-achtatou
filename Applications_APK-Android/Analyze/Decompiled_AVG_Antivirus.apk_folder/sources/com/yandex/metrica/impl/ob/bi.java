package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.qs;

public abstract class bi<T extends qs> extends be<T> {
    @NonNull
    private final bd j;

    public bi(@NonNull bd bdVar, @NonNull T t) {
        super(t);
        this.j = bdVar;
    }

    public boolean b() {
        return this.j.a(k(), l(), m());
    }
}
