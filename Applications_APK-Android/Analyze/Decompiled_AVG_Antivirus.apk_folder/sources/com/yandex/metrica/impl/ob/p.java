package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.ba;
import java.util.Map;

public class p extends ba {
    /* access modifiers changed from: private */
    public final kg a;

    public p(kg kgVar) {
        this.a = kgVar;
    }

    /* access modifiers changed from: package-private */
    public SparseArray<ba.a> a() {
        return new SparseArray<ba.a>() {
            {
                put(47, new a(p.this.a));
                p pVar = p.this;
                put(66, new b(pVar.a));
            }
        };
    }

    /* access modifiers changed from: protected */
    public int a(of ofVar) {
        return (int) this.a.c(-1);
    }

    /* access modifiers changed from: protected */
    public void a(of ofVar, int i) {
        this.a.f((long) i);
        ofVar.c().j();
    }

    static class a implements ba.a {
        private kg a;

        public a(kg kgVar) {
            this.a = kgVar;
        }

        public void a(Context context) {
            od odVar = new od(context);
            if (cg.a((Map) odVar.c())) {
                return;
            }
            if (this.a.a((String) null) == null || this.a.b((String) null) == null) {
                a(odVar);
                b(odVar);
                c(odVar);
                d(odVar);
                e(odVar);
                f(odVar);
                g(odVar);
                h(odVar);
                this.a.n();
                odVar.b().j();
            }
        }

        private boolean a(long j, long j2, long j3) {
            return j != j3 && j2 == j3;
        }

        private boolean a(String str, String str2) {
            return !TextUtils.isEmpty(str) && TextUtils.isEmpty(str2);
        }

        private void a(@NonNull od odVar) {
            String b = odVar.b((String) null);
            if (a(b, this.a.b((String) null))) {
                this.a.h(b);
            }
        }

        private void b(@NonNull od odVar) {
            String a2 = odVar.a();
            if (a(a2, this.a.a())) {
                this.a.m(a2);
            }
        }

        private void c(@NonNull od odVar) {
            String a2 = odVar.a((String) null);
            if (a(a2, this.a.a((String) null))) {
                this.a.g(a2);
            }
        }

        private void d(@NonNull od odVar) {
            String c = odVar.c(null);
            if (a(c, this.a.d((String) null))) {
                this.a.j(c);
            }
        }

        private void e(@NonNull od odVar) {
            String d = odVar.d(null);
            if (a(d, this.a.e((String) null))) {
                this.a.k(d);
            }
        }

        private void f(@NonNull od odVar) {
            String e = odVar.e(null);
            if (a(e, this.a.f((String) null))) {
                this.a.l(e);
            }
        }

        private void g(@NonNull od odVar) {
            long a2 = odVar.a(-1);
            if (a(a2, this.a.a(-1), -1)) {
                this.a.d(a2);
            }
        }

        private void h(@NonNull od odVar) {
            long b = odVar.b(-1);
            if (a(b, this.a.b(-1), -1)) {
                this.a.e(b);
            }
        }
    }

    private class b implements ba.a {
        private final kg b;

        public b(kg kgVar) {
            this.b = kgVar;
        }

        public void a(Context context) {
            this.b.p(new oj("COOKIE_BROWSERS").b());
            this.b.p(new oj("BIND_ID_URL").b());
            ag.a(context, "b_meta.dat");
            ag.a(context, "browsers.dat");
        }
    }
}
