package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.h;
import com.yandex.metrica.impl.ob.lp;
import com.yandex.metrica.impl.ob.qx;

public class qz {
    /* access modifiers changed from: private */
    @NonNull
    public final qy a;
    @NonNull
    private final kp<ra> b;
    @NonNull
    private final tw c;
    @NonNull
    private final uv d;
    @NonNull
    private final h.b e;
    @NonNull
    private final h f;
    /* access modifiers changed from: private */
    @NonNull
    public final qx g;
    /* access modifiers changed from: private */
    public boolean h;
    @Nullable
    private rs i;
    private boolean j;
    private long k;
    private long l;
    private long m;
    private boolean n;
    private boolean o;
    /* access modifiers changed from: private */
    public boolean p;
    private final Object q;

    public qz(@NonNull Context context, @NonNull uv uvVar) {
        this(context, new qy(context, null, uvVar), lp.a.a(ra.class).a(context), new tw(), uvVar, af.a().i());
    }

    @VisibleForTesting
    qz(@NonNull Context context, @NonNull qy qyVar, @NonNull kp<ra> kpVar, @NonNull tw twVar, @NonNull uv uvVar, @NonNull h hVar) {
        this.p = false;
        this.q = new Object();
        this.a = qyVar;
        this.b = kpVar;
        this.g = new qx(context, kpVar, new qx.a() {
            public void a() {
                qz.this.c();
                boolean unused = qz.this.h = false;
            }
        });
        this.c = twVar;
        this.d = uvVar;
        this.e = new h.b() {
            public void a() {
                boolean unused = qz.this.p = true;
                qz.this.a.a(qz.this.g);
            }
        };
        this.f = hVar;
    }

    public void a(@Nullable sc scVar) {
        c();
        b(scVar);
    }

    private void d() {
        if (this.o) {
            f();
        } else {
            g();
        }
    }

    private void e() {
        if (this.k - this.l >= this.i.b) {
            b();
        }
    }

    private void f() {
        if (this.c.a() - this.m >= this.i.d) {
            b();
        }
    }

    private void g() {
        if (this.c.a() - this.m >= this.i.a) {
            b();
        }
    }

    public void a() {
        synchronized (this.q) {
            if (this.j && this.i != null) {
                if (this.n) {
                    d();
                } else {
                    e();
                }
            }
        }
    }

    public void b(@Nullable sc scVar) {
        boolean c2 = c(scVar);
        synchronized (this.q) {
            if (scVar != null) {
                this.j = scVar.o.e;
                this.i = scVar.B;
                this.k = scVar.C;
                this.l = scVar.D;
            }
            this.a.a(scVar);
        }
        if (c2) {
            a();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (!this.h) {
            this.h = true;
            if (!this.p) {
                this.f.a(this.i.c, this.d, this.e);
            } else {
                this.a.a(this.g);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        ra a2 = this.b.a();
        this.m = a2.c;
        this.n = a2.d;
        this.o = a2.e;
    }

    private boolean c(@Nullable sc scVar) {
        rs rsVar;
        if (scVar == null) {
            return false;
        }
        if ((this.j || !scVar.o.e) && (rsVar = this.i) != null && rsVar.equals(scVar.B) && this.k == scVar.C && this.l == scVar.D && !this.a.b(scVar)) {
            return false;
        }
        return true;
    }
}
