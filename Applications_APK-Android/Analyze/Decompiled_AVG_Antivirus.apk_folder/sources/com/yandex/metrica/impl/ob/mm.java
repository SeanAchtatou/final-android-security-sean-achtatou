package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class mm {
    @NonNull
    private final jr a;
    @NonNull
    private final mg b;

    public mm(@NonNull Context context) {
        this(jo.a(context).h(), new mg(context));
    }

    public void a(@NonNull mn mnVar) {
        String a2 = this.b.a(mnVar);
        if (!TextUtils.isEmpty(a2)) {
            this.a.b(mnVar.b(), a2);
        }
    }

    @VisibleForTesting
    mm(@NonNull jr jrVar, @NonNull mg mgVar) {
        this.a = jrVar;
        this.b = mgVar;
    }
}
