package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.bj;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class s {
    private static final Object j = new Object();
    @SuppressLint({"StaticFieldLeak"})
    private static volatile s k;
    @NonNull
    public final String a = "android";
    public final String b = Build.MANUFACTURER;
    public final String c = Build.MODEL;
    public final String d = Build.VERSION.RELEASE;
    public final int e = Build.VERSION.SDK_INT;
    @NonNull
    public final b f;
    @NonNull
    public final String g;
    @NonNull
    public final String h;
    @NonNull
    public final List<String> i;
    @NonNull
    private final a l;

    public static final class b {
        public final int a;
        public final int b;
        public final int c;
        public final float d;

        b(@NonNull Point point, int i, float f) {
            this.a = Math.max(point.x, point.y);
            this.b = Math.min(point.x, point.y);
            this.c = i;
            this.d = f;
        }
    }

    public static s a(@NonNull Context context) {
        if (k == null) {
            synchronized (j) {
                if (k == null) {
                    k = new s(context.getApplicationContext());
                }
            }
        }
        return k;
    }

    private s(@NonNull Context context) {
        this.l = new a(context);
        this.f = new b(bj.b(context), context.getResources().getDisplayMetrics().densityDpi, context.getResources().getDisplayMetrics().density);
        this.g = bj.a(context).name().toLowerCase(Locale.US);
        this.h = String.valueOf(bj.b.c());
        this.i = Collections.unmodifiableList(new ArrayList<String>() {
            {
                if (bj.b.a()) {
                    add("Superuser.apk");
                }
                if (bj.b.b()) {
                    add("su.so");
                }
            }
        });
    }

    @Nullable
    public String a() {
        return this.l.a((sc) null);
    }

    @Nullable
    public String a(@NonNull sc scVar) {
        return this.l.a(scVar);
    }

    public static class a {
        @Nullable
        private String a;
        @NonNull
        private Context b;
        /* access modifiers changed from: private */
        public sc c;

        a(@NonNull Context context) {
            this.b = context;
            cn.a().b(new ct(this.a));
            cn.a().a(this, cx.class, cr.a(new cq<cx>() {
                public void a(cx cxVar) {
                    synchronized (a.this) {
                        sc unused = a.this.c = cxVar.b;
                    }
                }
            }).a());
            this.a = b(this.c) ? a(context) : null;
        }

        @SuppressLint({"HardwareIds"})
        private String a(@NonNull Context context) {
            return Settings.Secure.getString(context.getContentResolver(), "android_id");
        }

        public String a(@NonNull sc scVar) {
            if (TextUtils.isEmpty(this.a) && b(scVar)) {
                this.a = a(this.b);
            }
            return this.a;
        }

        private synchronized boolean b(@NonNull sc scVar) {
            if (scVar == null) {
                scVar = this.c;
            }
            return c(scVar);
        }

        private boolean c(@NonNull sc scVar) {
            return scVar != null && scVar.o.f;
        }
    }
}
