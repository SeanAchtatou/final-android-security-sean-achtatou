package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

public class k {
    @Nullable
    public final a a;
    @Nullable
    public final Boolean b;

    public enum a {
        ACTIVE,
        WORKING_SET,
        FREQUENT,
        RARE
    }

    public k(@Nullable a aVar, @Nullable Boolean bool) {
        this.a = aVar;
        this.b = bool;
    }
}
