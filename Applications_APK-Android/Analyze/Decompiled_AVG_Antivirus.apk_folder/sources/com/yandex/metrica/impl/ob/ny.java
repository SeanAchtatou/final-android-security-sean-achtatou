package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.pm.PackageInfo;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;

public class ny implements ns {
    @NonNull
    private final Context a;
    @NonNull
    private final String b;
    @NonNull
    private final vp c;

    public ny(@NonNull Context context) {
        this(context, context.getPackageName(), new vp());
    }

    public ny(@NonNull Context context, @NonNull String str, @NonNull vp vpVar) {
        this.a = context;
        this.b = str;
        this.c = vpVar;
    }

    @NonNull
    public List<nt> a() {
        ArrayList arrayList = new ArrayList();
        PackageInfo a2 = this.c.a(this.a, this.b, 4096);
        if (a2 != null) {
            for (String ntVar : a2.requestedPermissions) {
                arrayList.add(new nt(ntVar, true));
            }
        }
        return arrayList;
    }
}
