package com.yandex.metrica.impl.ob;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.util.regex.Pattern;

public class bx {
    private static final Pattern a = Pattern.compile(".*at com\\.yandex\\.metrica\\.push\\.*");
    private static final Pattern b = Pattern.compile(".*at com\\.yandex\\.metrica\\.(?!push)");

    static void a(@NonNull tp tpVar) {
        String f = tpVar.f();
        Log.i(f, tpVar.g() + ("Initializing of Metrica, " + ce.b("release") + " type, Version " + "3.6.4" + ", API Level " + 81 + ", Dated " + "13.06.2019" + "."));
    }

    @NonNull
    public static String a() {
        return a.a;
    }

    public static String a(String str) {
        return str + "/" + "3.6.4" + "." + "45179" + " (" + b() + "; Android " + Build.VERSION.RELEASE + ")";
    }

    public static String b() {
        if (Build.MODEL.startsWith(Build.MANUFACTURER)) {
            return ce.b(Build.MODEL);
        }
        return ce.b(Build.MANUFACTURER) + " " + Build.MODEL;
    }

    static boolean a(Throwable th) {
        String a2 = cg.a(th);
        return !TextUtils.isEmpty(a2) && b.matcher(a2).find();
    }

    static boolean b(Throwable th) {
        String a2 = cg.a(th);
        return !TextUtils.isEmpty(a2) && a.matcher(a2).find();
    }

    @VisibleForTesting
    static class a {
        @NonNull
        static final String a = new a().a();

        a() {
        }

        /* access modifiers changed from: package-private */
        @VisibleForTesting
        @NonNull
        public String a() {
            if (a("com.unity3d.player.UnityPlayer")) {
                return "unity";
            }
            if (a("mono.MonoPackageManager")) {
                return "xamarin";
            }
            if (a("org.apache.cordova.CordovaPlugin")) {
                return "cordova";
            }
            if (a("com.facebook.react.ReactRootView")) {
                return "react";
            }
            return "native";
        }

        /* access modifiers changed from: package-private */
        @VisibleForTesting
        public boolean a(String str) {
            return bx.b(str);
        }
    }

    public static boolean b(String str) {
        try {
            return Class.forName(str) != null;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
