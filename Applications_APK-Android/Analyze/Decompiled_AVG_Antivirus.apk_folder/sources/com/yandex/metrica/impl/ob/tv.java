package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;

public class tv {
    @NonNull
    private final tw a;

    public tv() {
        this(new tw());
    }

    @VisibleForTesting
    public tv(@NonNull tw twVar) {
        this.a = twVar;
    }

    public long a(long j, @NonNull TimeUnit timeUnit) {
        return TimeUnit.MILLISECONDS.toSeconds(b(j, timeUnit));
    }

    public long b(long j, @NonNull TimeUnit timeUnit) {
        return this.a.c() - timeUnit.toMillis(j);
    }

    public long c(long j, @NonNull TimeUnit timeUnit) {
        return this.a.d() - timeUnit.toNanos(j);
    }

    public long d(long j, @NonNull TimeUnit timeUnit) {
        return TimeUnit.NANOSECONDS.toSeconds(c(j, timeUnit));
    }
}
