package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.bj;
import com.yandex.metrica.impl.ob.lp;
import com.yandex.metrica.impl.ob.nj;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class qy {
    @NonNull
    private uv a;
    @NonNull
    private final Context b;
    @NonNull
    private final kp<ra> c;
    @NonNull
    private te d;
    @NonNull
    private a e;
    @NonNull
    private kx f;
    @NonNull
    private final b g;
    @NonNull
    private final tw h;
    /* access modifiers changed from: private */
    @Nullable
    public String i;

    public static class a {
        @NonNull
        private final re a;

        public a() {
            this(new re());
        }

        @VisibleForTesting
        a(@NonNull re reVar) {
            this.a = reVar;
        }

        @NonNull
        public List<rd> a(@Nullable byte[] bArr) {
            ArrayList arrayList = new ArrayList();
            if (cg.a(bArr)) {
                return arrayList;
            }
            try {
                return this.a.a(new String(bArr, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                return arrayList;
            }
        }
    }

    static class b {
        b() {
        }

        @Nullable
        public HttpURLConnection a(@NonNull String str, @NonNull String str2) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
                if (!TextUtils.isEmpty(str)) {
                    httpURLConnection.setRequestProperty("If-None-Match", str);
                }
                httpURLConnection.setInstanceFollowRedirects(true);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setConnectTimeout(nj.a.a);
                httpURLConnection.setReadTimeout(nj.a.a);
                httpURLConnection.connect();
                return httpURLConnection;
            } catch (Throwable th) {
                return null;
            }
        }
    }

    public qy(@NonNull Context context, @Nullable String str, @NonNull uv uvVar) {
        this(context, str, lp.a.a(ra.class).a(context), new tb(), new a(), new b(), uvVar, new kx(), new tw());
    }

    @VisibleForTesting
    qy(@NonNull Context context, @Nullable String str, @NonNull kp kpVar, @NonNull te teVar, @NonNull a aVar, @NonNull b bVar, @NonNull uv uvVar, @NonNull kx kxVar, @NonNull tw twVar) {
        this.b = context;
        this.i = str;
        this.c = kpVar;
        this.d = teVar;
        this.e = aVar;
        this.g = bVar;
        this.a = uvVar;
        this.f = kxVar;
        this.h = twVar;
    }

    public void a(@NonNull final qx qxVar) {
        this.a.a(new Runnable() {
            public void run() {
                qy qyVar = qy.this;
                qyVar.a(qxVar, qyVar.i);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull qx qxVar, String str) {
        bj.a a2 = this.d.a(this.b);
        ra a3 = this.c.a();
        ra raVar = null;
        if (!(a2 == bj.a.OFFLINE || a2 == bj.a.UNDEFINED || str == null)) {
            try {
                HttpURLConnection a4 = this.g.a(a3.b, str);
                if (a4 != null) {
                    raVar = a(a4, a3);
                }
            } catch (Throwable th) {
            }
        }
        if (raVar != null) {
            qxVar.a(raVar);
        } else {
            qxVar.a();
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public ra a(@NonNull HttpURLConnection httpURLConnection, @NonNull ra raVar) throws IOException {
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == 200) {
            try {
                return new ra(this.e.a(this.f.a(ag.b(httpURLConnection.getInputStream()), "af9202nao18gswqp")), ce.a(httpURLConnection.getHeaderField("ETag")), this.h.a(), true, false);
            } catch (IOException e2) {
            }
        } else if (responseCode == 304) {
            return new ra(raVar.a, raVar.b, this.h.a(), true, false);
        }
        return null;
    }

    public void a(@Nullable sc scVar) {
        if (scVar != null) {
            this.i = scVar.h;
        }
    }

    public boolean b(@NonNull sc scVar) {
        String str = this.i;
        if (str != null) {
            return !str.equals(scVar.h);
        }
        if (scVar.h != null) {
            return true;
        }
        return false;
    }
}
