package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.po;
import java.util.ArrayList;
import java.util.List;

public class lk implements ky<ra, po.a> {
    @NonNull
    /* renamed from: a */
    public po.a b(@NonNull ra raVar) {
        po.a aVar = new po.a();
        aVar.b = new po.a.C0013a[raVar.a.size()];
        for (int i = 0; i < raVar.a.size(); i++) {
            aVar.b[i] = a(raVar.a.get(i));
        }
        aVar.c = raVar.b;
        aVar.d = raVar.c;
        aVar.e = raVar.d;
        aVar.f = raVar.e;
        return aVar;
    }

    @NonNull
    public ra a(@NonNull po.a aVar) {
        ArrayList arrayList = new ArrayList(aVar.b.length);
        for (po.a.C0013a a : aVar.b) {
            arrayList.add(a(a));
        }
        return new ra(arrayList, aVar.c, aVar.d, aVar.e, aVar.f);
    }

    @NonNull
    private po.a.C0013a a(@NonNull rd rdVar) {
        po.a.C0013a aVar = new po.a.C0013a();
        aVar.b = rdVar.a;
        List<String> list = rdVar.b;
        aVar.c = new String[list.size()];
        int i = 0;
        for (String str : list) {
            aVar.c[i] = str;
            i++;
        }
        return aVar;
    }

    @NonNull
    private rd a(@NonNull po.a.C0013a aVar) {
        ArrayList arrayList = new ArrayList();
        if (aVar.c != null && aVar.c.length > 0) {
            arrayList = new ArrayList(aVar.c.length);
            for (String add : aVar.c) {
                arrayList.add(add);
            }
        }
        return new rd(ce.a(aVar.b), arrayList);
    }
}
