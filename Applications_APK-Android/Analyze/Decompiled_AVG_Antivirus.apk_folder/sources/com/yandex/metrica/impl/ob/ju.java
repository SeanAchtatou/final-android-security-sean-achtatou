package com.yandex.metrica.impl.ob;

import android.database.sqlite.SQLiteDatabase;
import android.util.SparseArray;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class ju {
    private final String a;
    private final kc b;
    private final kc c;
    private final SparseArray<kc> d;
    private final jv e;

    public static class a {
        public ju a(@NonNull String str, @NonNull kc kcVar, @NonNull kc kcVar2, @NonNull SparseArray<kc> sparseArray, @NonNull jv jvVar) {
            return new ju(str, kcVar, kcVar2, sparseArray, jvVar);
        }
    }

    private ju(String str, kc kcVar, kc kcVar2, SparseArray<kc> sparseArray, jv jvVar) {
        this.a = str;
        this.b = kcVar;
        this.c = kcVar2;
        this.d = sparseArray;
        this.e = jvVar;
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
        try {
            if (this.e != null && !this.e.a(sQLiteDatabase)) {
                c(sQLiteDatabase);
            }
        } catch (Throwable th) {
        }
    }

    public void b(SQLiteDatabase sQLiteDatabase) {
        a(this.b, sQLiteDatabase);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(kc kcVar, SQLiteDatabase sQLiteDatabase) {
        try {
            kcVar.a(sQLiteDatabase);
        } catch (Throwable th) {
        }
    }

    public void a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        boolean z;
        if (i2 > i) {
            z = false;
            int i3 = i + 1;
            while (i3 <= i2) {
                try {
                    kc kcVar = this.d.get(i3);
                    if (kcVar != null) {
                        kcVar.a(sQLiteDatabase);
                    }
                    i3++;
                } catch (Throwable th) {
                    z = true;
                }
            }
        } else {
            z = true;
        }
        if ((!this.e.a(sQLiteDatabase)) || z) {
            c(sQLiteDatabase);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void c(SQLiteDatabase sQLiteDatabase) {
        b(this.c, sQLiteDatabase);
        a(this.b, sQLiteDatabase);
    }

    private void b(kc kcVar, SQLiteDatabase sQLiteDatabase) {
        try {
            kcVar.a(sQLiteDatabase);
        } catch (Throwable th) {
        }
    }
}
