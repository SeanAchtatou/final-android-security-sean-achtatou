package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Map;

public abstract class rk {
    @NonNull
    rn a;
    @NonNull
    Uri b;
    @NonNull
    private Socket c;

    /* access modifiers changed from: package-private */
    public abstract void a();

    rk(@NonNull Socket socket, @NonNull Uri uri, @NonNull rn rnVar) {
        this.c = socket;
        this.b = uri;
        this.a = rnVar;
    }

    /* access modifiers changed from: package-private */
    public void a(@NonNull String str, @NonNull Map<String, String> map, @NonNull byte[] bArr) {
        BufferedOutputStream bufferedOutputStream = null;
        try {
            BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(this.c.getOutputStream());
            try {
                bufferedOutputStream2.write(str.getBytes());
                a(bufferedOutputStream2);
                for (Map.Entry next : map.entrySet()) {
                    a(bufferedOutputStream2, (String) next.getKey(), (String) next.getValue());
                }
                a(bufferedOutputStream2);
                bufferedOutputStream2.write(bArr);
                bufferedOutputStream2.flush();
                this.a.a("sync_succeed", this.c.getLocalPort());
                cg.a((Closeable) bufferedOutputStream2);
            } catch (IOException e) {
                e = e;
                bufferedOutputStream = bufferedOutputStream2;
                try {
                    this.a.a("io_exception_during_sync", e);
                    cg.a((Closeable) bufferedOutputStream);
                } catch (Throwable th) {
                    th = th;
                    bufferedOutputStream2 = bufferedOutputStream;
                    cg.a((Closeable) bufferedOutputStream2);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                cg.a((Closeable) bufferedOutputStream2);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            this.a.a("io_exception_during_sync", e);
            cg.a((Closeable) bufferedOutputStream);
        }
    }

    private void a(@NonNull OutputStream outputStream, @NonNull String str, @NonNull String str2) throws IOException {
        outputStream.write((str + ": " + str2).getBytes());
        a(outputStream);
    }

    private void a(@NonNull OutputStream outputStream) throws IOException {
        outputStream.write("\r\n".getBytes());
    }
}
