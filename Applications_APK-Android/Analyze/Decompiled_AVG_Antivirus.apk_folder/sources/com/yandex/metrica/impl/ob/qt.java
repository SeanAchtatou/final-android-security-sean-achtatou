package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter;

public class qt extends qu<nh> {
    private long a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.qu.a(android.net.Uri$Builder, com.yandex.metrica.impl.ob.qj):void
     arg types: [android.net.Uri$Builder, com.yandex.metrica.impl.ob.nh]
     candidates:
      com.yandex.metrica.impl.ob.qt.a(android.net.Uri$Builder, com.yandex.metrica.impl.ob.nh):void
      com.yandex.metrica.impl.ob.qu.a(android.net.Uri$Builder, com.yandex.metrica.impl.ob.qj):void */
    public void a(@NonNull Uri.Builder builder, @NonNull nh nhVar) {
        super.a(builder, (qj) nhVar);
        builder.appendPath("location");
        builder.appendQueryParameter("deviceid", nhVar.r());
        builder.appendQueryParameter("device_type", nhVar.C());
        builder.appendQueryParameter("uuid", nhVar.t());
        builder.appendQueryParameter("analytics_sdk_version_name", nhVar.i());
        builder.appendQueryParameter("analytics_sdk_build_number", nhVar.j());
        builder.appendQueryParameter("analytics_sdk_build_type", nhVar.k());
        builder.appendQueryParameter("app_version_name", nhVar.q());
        builder.appendQueryParameter("app_build_number", nhVar.p());
        builder.appendQueryParameter("os_version", nhVar.n());
        builder.appendQueryParameter("os_api_level", String.valueOf(nhVar.o()));
        builder.appendQueryParameter("is_rooted", nhVar.u());
        builder.appendQueryParameter("app_framework", nhVar.v());
        builder.appendQueryParameter(UrlManager.Parameter.APP_ID, nhVar.d());
        builder.appendQueryParameter("app_platform", nhVar.l());
        builder.appendQueryParameter("android_id", nhVar.B());
        builder.appendQueryParameter("request_id", String.valueOf(this.a));
        GoogleAdvertisingIdGetter.c D = nhVar.D();
        String str = "";
        String str2 = D == null ? str : D.a;
        if (str2 == null) {
            str2 = str;
        }
        builder.appendQueryParameter("adv_id", str2);
        if (D != null) {
            str = a(D.b);
        }
        builder.appendQueryParameter("limit_ad_tracking", str);
    }

    public void a(long j) {
        this.a = j;
    }
}
