package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class uu implements uw {
    @NonNull
    private final Looper a;
    @NonNull
    private final Handler b;
    @NonNull
    private final uy c;

    public uu(@NonNull String str) {
        this(a(str));
    }

    @NonNull
    public Handler a() {
        return this.b;
    }

    @NonNull
    public Looper b() {
        return this.a;
    }

    public void a(@NonNull Runnable runnable) {
        this.b.post(runnable);
    }

    public <T> Future<T> a(Callable<T> callable) {
        FutureTask futureTask = new FutureTask(callable);
        a(futureTask);
        return futureTask;
    }

    public void a(@NonNull Runnable runnable, long j) {
        a(runnable, j, TimeUnit.MILLISECONDS);
    }

    public void a(@NonNull Runnable runnable, long j, @NonNull TimeUnit timeUnit) {
        this.b.postDelayed(runnable, timeUnit.toMillis(j));
    }

    public void b(@NonNull Runnable runnable) {
        this.b.removeCallbacks(runnable);
    }

    public boolean c() {
        return this.c.c();
    }

    private static uy a(@NonNull String str) {
        uy a2 = new va(str).a();
        a2.start();
        return a2;
    }

    @VisibleForTesting
    uu(@NonNull uy uyVar) {
        this(uyVar, uyVar.getLooper(), new Handler(uyVar.getLooper()));
    }

    @VisibleForTesting
    public uu(@NonNull uy uyVar, @NonNull Looper looper, @NonNull Handler handler) {
        this.c = uyVar;
        this.a = looper;
        this.b = handler;
    }
}
