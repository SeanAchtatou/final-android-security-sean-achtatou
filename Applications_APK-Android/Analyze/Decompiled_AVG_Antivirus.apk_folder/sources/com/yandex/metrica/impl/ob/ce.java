package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.regex.Pattern;

public final class ce {
    static final Pattern a = Pattern.compile("[^0-9a-zA-Z,`’\\.\\+\\-'\\s\"]");
    static final Pattern b = Pattern.compile("\\s+");

    public static boolean a(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null || str2 == null) {
            return false;
        }
        return str.equals(str2);
    }

    public static boolean a(String... strArr) {
        if (strArr == null) {
            return false;
        }
        for (String isEmpty : strArr) {
            if (TextUtils.isEmpty(isEmpty)) {
                return true;
            }
        }
        return false;
    }

    public static String b(String str, String str2) {
        return str == null ? str2 : str;
    }

    public static String c(String str, String str2) {
        return TextUtils.isEmpty(str) ? str2 : str;
    }

    public static String a(String str) {
        return b(str, "");
    }

    public static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        char charAt = str.charAt(0);
        if (Character.isUpperCase(charAt)) {
            return str;
        }
        return Character.toUpperCase(charAt) + str.substring(1);
    }

    public static byte[] c(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (Throwable th) {
            return new byte[0];
        }
    }

    public static final String b(String... strArr) {
        return TextUtils.join(",", strArr);
    }

    @NonNull
    public static byte[] d(@Nullable String str) {
        return str == null ? new byte[0] : str.getBytes();
    }

    public static String a(@NonNull byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            sb.append("0123456789abcdef".charAt((b2 & 240) >> 4));
            sb.append("0123456789abcdef".charAt(b2 & 15));
        }
        return sb.toString();
    }

    public static int d(@Nullable String str, @Nullable String str2) {
        if (str == null) {
            return str2 == null ? 0 : -1;
        }
        if (str2 == null) {
            return 1;
        }
        return str.compareTo(str2);
    }
}
