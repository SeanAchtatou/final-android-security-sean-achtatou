package com.yandex.metrica.impl.ob;

public class gv {
    private final fz a;
    private final gc b;
    private final ge c;
    private final gg d;
    private final hc e;
    private final gh f;
    private final gf g;
    private final gi h;
    private final fr i;
    private final fq j;
    private final fx k;
    private final fu l;
    private final gb m;
    private final fw n;
    private final gj o;
    private final fs p;
    private final fy q;
    private final fv r;

    public gv(di diVar) {
        this.a = new fz(diVar);
        this.b = new gc(diVar);
        this.c = new ge(diVar);
        this.d = new gg(diVar);
        this.e = new hc(diVar);
        this.f = new gh(diVar);
        this.g = new gf(diVar);
        this.h = new gi(diVar);
        this.i = new fr(diVar);
        this.j = new fq(diVar);
        this.k = new fx(diVar);
        this.l = new fu(diVar);
        this.m = new gb(diVar, new nu());
        this.n = new fw(diVar);
        this.o = new gj(diVar);
        this.p = new fs(diVar);
        this.q = new fy(diVar);
        this.r = new fv(diVar, rh.a(diVar.j()));
    }

    public fz a() {
        return this.a;
    }

    public gc b() {
        return this.b;
    }

    public ge c() {
        return this.c;
    }

    public gg d() {
        return this.d;
    }

    public hc e() {
        return this.e;
    }

    public gh f() {
        return this.f;
    }

    public gf g() {
        return this.g;
    }

    public gi h() {
        return this.h;
    }

    public fr i() {
        return this.i;
    }

    public fq j() {
        return this.j;
    }

    public fx k() {
        return this.k;
    }

    public fu l() {
        return this.l;
    }

    public gb m() {
        return this.m;
    }

    public fw n() {
        return this.n;
    }

    public gj o() {
        return this.o;
    }

    public fs p() {
        return this.p;
    }

    public fy q() {
        return this.q;
    }

    public fv r() {
        return this.r;
    }
}
