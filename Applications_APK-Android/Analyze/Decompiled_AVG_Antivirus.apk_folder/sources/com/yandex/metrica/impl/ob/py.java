package com.yandex.metrica.impl.ob;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.AppMetricaDeviceIDListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.g;
import com.yandex.metrica.profile.UserProfile;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class py extends ps<pu<qa>> {
    private pz a;

    public py(@NonNull uv uvVar) {
        this(new px(), uvVar, new pv<pu<qa>>(uvVar) {
            /* access modifiers changed from: protected */
            public pu<qa> a(@NonNull uv uvVar, @NonNull Context context, @NonNull String str) {
                return new pu<>(uvVar, context, str, new qa());
            }
        }, new pz());
    }

    private py(@NonNull px pxVar, @NonNull uv uvVar, @NonNull pv<pu<qa>> pvVar, @NonNull pz pzVar) {
        this(pxVar, uvVar, pvVar, pzVar, new pr(pxVar));
    }

    @VisibleForTesting
    py(@NonNull px pxVar, @NonNull uv uvVar, @NonNull pv<pu<qa>> pvVar, @NonNull pz pzVar, @NonNull pr prVar) {
        super(pxVar, uvVar, pvVar, prVar);
        this.a = new pz();
        this.a = pzVar;
    }

    public void a(@NonNull final Context context, @NonNull final YandexMetricaConfig yandexMetricaConfig) {
        this.a.a(context, yandexMetricaConfig);
        a().a(new Runnable() {
            public void run() {
                py.this.c().a(context, g.a(yandexMetricaConfig));
            }
        });
        c().b();
    }

    public void e() {
        d().a();
        this.a.sendEventsBuffer();
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().sendEventsBuffer();
            }
        });
    }

    public void a(@Nullable final Activity activity) {
        d().a();
        this.a.resumeSession();
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().b(activity);
            }
        });
    }

    public void b(@Nullable final Activity activity) {
        d().a();
        this.a.pauseSession();
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().c(activity);
            }
        });
    }

    public void a(@NonNull final Application application) {
        d().a();
        this.a.a(application);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().a(application);
            }
        });
    }

    public void a(@NonNull final String str) {
        d().a();
        this.a.reportEvent(str);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().reportEvent(str);
            }
        });
    }

    public void a(@NonNull final String str, @Nullable final String str2) {
        d().a();
        this.a.reportEvent(str, str2);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().reportEvent(str, str2);
            }
        });
    }

    public void a(@NonNull final String str, @Nullable Map<String, Object> map) {
        final List list;
        d().a();
        this.a.reportEvent(str, map);
        if (map == null) {
            list = null;
        } else {
            list = a(map);
        }
        a().a(new Runnable() {
            public void run() {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                List<Map.Entry> list = list;
                if (list != null) {
                    for (Map.Entry entry : list) {
                        linkedHashMap.put(entry.getKey(), entry.getValue());
                    }
                }
                py.this.c().e().reportEvent(str, linkedHashMap);
            }
        });
    }

    public void a(@NonNull final String str, @Nullable final Throwable th) {
        d().a();
        this.a.reportError(str, th);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().reportError(str, th);
            }
        });
    }

    public void a(@NonNull final Throwable th) {
        d().a();
        this.a.reportUnhandledException(th);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().reportUnhandledException(th);
            }
        });
    }

    public void b(@NonNull final String str) {
        d().a();
        this.a.a(str);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().c(str);
            }
        });
    }

    public void c(@NonNull final Activity activity) {
        d().a();
        this.a.a(activity);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().a(activity);
            }
        });
    }

    public void c(@NonNull final String str) {
        d().a();
        this.a.b(str);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().e(str);
            }
        });
    }

    public void d(@NonNull final String str) {
        d().a();
        this.a.c(str);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().f(str);
            }
        });
    }

    public void a(@Nullable final Location location) {
        this.a.a(location);
        a().a(new Runnable() {
            public void run() {
                py.this.c().a(location);
            }
        });
    }

    public void a(final boolean z) {
        this.a.a(z);
        a().a(new Runnable() {
            public void run() {
                py.this.c().a(z);
            }
        });
    }

    public void a(@NonNull final Context context, final boolean z) {
        this.a.a(context, z);
        a().a(new Runnable() {
            public void run() {
                py.this.c().a(context, z);
            }
        });
    }

    public void b(@NonNull final Context context, final boolean z) {
        this.a.b(context, z);
        a().a(new Runnable() {
            public void run() {
                py.this.c().b(context, z);
            }
        });
    }

    public void e(@Nullable final String str) {
        d().a();
        this.a.setUserProfileID(str);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().setUserProfileID(str);
            }
        });
    }

    public void a(@NonNull final UserProfile userProfile) {
        d().a();
        this.a.reportUserProfile(userProfile);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().reportUserProfile(userProfile);
            }
        });
    }

    public void a(@NonNull final Revenue revenue) {
        d().a();
        this.a.reportRevenue(revenue);
        a().a(new Runnable() {
            public void run() {
                py.this.c().e().reportRevenue(revenue);
            }
        });
    }

    public void a(@NonNull final DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        d().a();
        this.a.a(deferredDeeplinkParametersListener);
        a().a(new Runnable() {
            public void run() {
                py.this.c().f().a(deferredDeeplinkParametersListener);
            }
        });
    }

    public void a(@NonNull final AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        d().a();
        this.a.a(appMetricaDeviceIDListener);
        a().a(new Runnable() {
            public void run() {
                py.this.c().f().a(appMetricaDeviceIDListener);
            }
        });
    }

    @NonNull
    public IReporter a(@NonNull Context context, @NonNull String str) {
        this.a.a(context, str);
        return b().a(context, str);
    }

    public void a(@NonNull Context context, @NonNull ReporterConfig reporterConfig) {
        this.a.a(context, reporterConfig);
        b().a(context, reporterConfig);
    }

    @NonNull
    private <K, V> List<Map.Entry<K, V>> a(@NonNull Map map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry simpleEntry : map.entrySet()) {
            arrayList.add(new AbstractMap.SimpleEntry(simpleEntry));
        }
        return arrayList;
    }
}
