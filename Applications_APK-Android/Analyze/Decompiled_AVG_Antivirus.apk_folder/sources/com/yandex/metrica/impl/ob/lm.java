package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.cb;
import com.yandex.metrica.impl.ob.pp;
import com.yandex.metrica.impl.ob.sc;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class lm implements lc<sc, pp.a> {
    private la a = new la();
    private kw b = new kw();
    private ll c = new ll();
    private lg d = new lg();
    private li e = new li();
    private kz f = new kz();
    private le g = new le();
    private ln h = new ln();
    private lj i = new lj();

    @NonNull
    /* renamed from: a */
    public pp.a b(@NonNull sc scVar) {
        pp.a aVar = new pp.a();
        aVar.C = scVar.C;
        aVar.D = scVar.D;
        if (scVar.a != null) {
            aVar.b = scVar.a;
        }
        if (scVar.b != null) {
            aVar.y = scVar.b;
        }
        if (scVar.c != null) {
            aVar.A = scVar.c;
        }
        if (scVar.d != null) {
            aVar.z = scVar.d;
        }
        if (scVar.j != null) {
            aVar.h = (String[]) scVar.j.toArray(new String[scVar.j.size()]);
        }
        if (scVar.k != null) {
            aVar.i = (String[]) scVar.k.toArray(new String[scVar.k.size()]);
        }
        if (scVar.e != null) {
            aVar.d = (String[]) scVar.e.toArray(new String[scVar.e.size()]);
        }
        if (scVar.i != null) {
            aVar.g = (String[]) scVar.i.toArray(new String[scVar.i.size()]);
        }
        if (scVar.l != null) {
            aVar.t = (String[]) scVar.l.toArray(new String[scVar.l.size()]);
        }
        if (scVar.p != null) {
            aVar.k = this.a.b(scVar.p);
        }
        if (scVar.q != null) {
            aVar.l = this.b.b(scVar.q);
        }
        if (scVar.r != null) {
            aVar.m = this.c.b(scVar.r);
        }
        if (scVar.z != null) {
            aVar.F = this.d.b(scVar.z);
        }
        if (scVar.m != null) {
            aVar.o = scVar.m;
        }
        if (scVar.f != null) {
            aVar.e = scVar.f;
        }
        if (scVar.g != null) {
            aVar.f = scVar.g;
        }
        if (scVar.h != null) {
            aVar.B = scVar.h;
        }
        if (scVar.s != null) {
            aVar.r = scVar.s;
        }
        aVar.j = this.f.b(scVar.o);
        if (scVar.n != null) {
            aVar.p = scVar.n;
        }
        aVar.q = scVar.v;
        aVar.c = scVar.t;
        aVar.v = scVar.u;
        if (scVar.w != null) {
            aVar.n = a(scVar.w);
        }
        if (scVar.x != null) {
            aVar.s = scVar.x;
        }
        if (scVar.A != null) {
            aVar.w = this.g.a(scVar.A);
        }
        if (scVar.B != null) {
            aVar.x = this.i.b(scVar.B);
        }
        if (scVar.y != null) {
            aVar.u = this.h.b(scVar.y);
        }
        aVar.E = scVar.E;
        return aVar;
    }

    @NonNull
    public sc a(@NonNull pp.a aVar) {
        sc.a c2 = new sc.a(this.f.a(aVar.j)).a(aVar.b).b(aVar.y).c(aVar.A).d(aVar.z).h(aVar.o).e(aVar.e).a(Arrays.asList(aVar.d)).b(Arrays.asList(aVar.g)).d(Arrays.asList(aVar.i)).c(Arrays.asList(aVar.h)).f(aVar.f).g(aVar.B).e(Arrays.asList(aVar.t)).j(aVar.r).i(aVar.p).b(aVar.q).a(aVar.c).a(aVar.v).f(a(aVar.n)).b(aVar.C).c(aVar.D).k(aVar.s).c(aVar.E);
        if (aVar.k != null) {
            c2.a(this.a.a(aVar.k));
        }
        if (aVar.l != null) {
            c2.a(this.b.a(aVar.l));
        }
        if (aVar.m != null) {
            c2.a(this.c.a(aVar.m));
        }
        if (aVar.F != null) {
            c2.a(this.d.a(aVar.F));
        }
        if (aVar.w != null) {
            c2.g(this.g.a(aVar.w));
        }
        if (aVar.x != null) {
            c2.a(this.i.a(aVar.x));
        }
        if (aVar.u != null) {
            c2.a(this.h.a(aVar.u));
        }
        return c2.a();
    }

    @NonNull
    private pp.a.f[] a(@NonNull List<cb.a> list) {
        pp.a.f[] fVarArr = new pp.a.f[list.size()];
        int i2 = 0;
        for (cb.a a2 : list) {
            fVarArr[i2] = this.e.b(a2);
            i2++;
        }
        return fVarArr;
    }

    @NonNull
    private List<cb.a> a(@NonNull pp.a.f[] fVarArr) {
        ArrayList arrayList = new ArrayList(fVarArr.length);
        for (pp.a.f a2 : fVarArr) {
            arrayList.add(this.e.a(a2));
        }
        return arrayList;
    }
}
