package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public final class os {
    @NonNull
    private final vx<String> a;
    @NonNull
    private final om b;
    @NonNull
    private final String c;

    public os(@NonNull String str, @NonNull vx<String> vxVar, @NonNull om omVar) {
        this.c = str;
        this.a = vxVar;
        this.b = omVar;
    }

    @NonNull
    public String a() {
        return this.c;
    }

    @NonNull
    public om b() {
        return this.b;
    }

    @NonNull
    public vx<String> c() {
        return this.a;
    }
}
