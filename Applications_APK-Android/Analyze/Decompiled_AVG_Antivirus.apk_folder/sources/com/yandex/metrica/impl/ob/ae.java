package com.yandex.metrica.impl.ob;

import android.os.SystemClock;

class ae {
    private long a = (SystemClock.elapsedRealtime() - 2000000);
    private boolean b = true;

    ae() {
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        boolean z = this.b;
        this.b = false;
        return a(z);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.b = true;
        this.a = SystemClock.elapsedRealtime();
    }

    private boolean a(boolean z) {
        return z && SystemClock.elapsedRealtime() - this.a > 1000;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.b;
    }
}
