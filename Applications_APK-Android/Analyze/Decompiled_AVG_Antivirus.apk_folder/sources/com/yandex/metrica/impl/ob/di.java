package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.dh;
import com.yandex.metrica.impl.ob.hs;
import com.yandex.metrica.impl.ob.i;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;

public class di implements Cdo, dr, ly {
    protected kj a;
    private final Context b;
    private final df c;
    private final kh d;
    private final kf e;
    private final bs f;
    private final jk g;
    private final fi h;
    private final ff i;
    private final i j;
    @NonNull
    private final a k;
    private volatile hs l;
    private final dz m;
    @NonNull
    private final hd n;
    @NonNull
    private final tp o;
    @NonNull
    private final tg p;
    /* access modifiers changed from: private */
    @NonNull
    public final ea q;
    @NonNull
    private final dh.a r;
    @NonNull
    private final lx s;
    @NonNull
    private final lu t;
    @NonNull
    private final lz u;
    @NonNull
    private final tx v;

    public hs a() {
        return this.l;
    }

    @NonNull
    public ea d() {
        return this.q;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public di(@androidx.annotation.NonNull android.content.Context r17, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.sc r18, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.bb r19, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.df r20, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.db.a r21, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.qp.d r22, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.sf r23) {
        /*
            r16 = this;
            com.yandex.metrica.impl.ob.di$a r3 = new com.yandex.metrica.impl.ob.di$a
            r3.<init>()
            com.yandex.metrica.impl.ob.tw r4 = new com.yandex.metrica.impl.ob.tw
            r4.<init>()
            com.yandex.metrica.impl.ob.dj r15 = new com.yandex.metrica.impl.ob.dj
            com.yandex.metrica.impl.ob.af r0 = com.yandex.metrica.impl.ob.af.a()
            com.yandex.metrica.impl.ob.vc r0 = r0.j()
            com.yandex.metrica.impl.ob.uv r13 = r0.g()
            java.lang.String r0 = r20.b()
            r1 = r17
            int r14 = com.yandex.metrica.impl.ob.cg.c(r1, r0)
            r5 = r15
            r6 = r17
            r7 = r20
            r8 = r21
            r9 = r23
            r10 = r18
            r11 = r22
            r12 = r19
            r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r0 = r16
            r2 = r20
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.di.<init>(android.content.Context, com.yandex.metrica.impl.ob.sc, com.yandex.metrica.impl.ob.bb, com.yandex.metrica.impl.ob.df, com.yandex.metrica.impl.ob.db$a, com.yandex.metrica.impl.ob.qp$d, com.yandex.metrica.impl.ob.sf):void");
    }

    @VisibleForTesting
    di(@NonNull Context context, @NonNull df dfVar, @NonNull a aVar, @NonNull tx txVar, @NonNull dj djVar) {
        this.b = context.getApplicationContext();
        this.c = dfVar;
        this.k = aVar;
        this.v = txVar;
        this.o = djVar.a().a();
        this.p = djVar.a().b();
        this.d = djVar.b().b();
        this.e = djVar.b().a();
        this.a = djVar.b().c();
        this.j = aVar.a(this.c, this.o, this.d);
        this.n = djVar.c();
        this.g = djVar.a(this);
        this.f = djVar.b(this);
        this.m = djVar.c(this);
        this.r = djVar.e(this);
        this.u = djVar.a(this.g, this.m);
        this.t = djVar.a(this.g);
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.u);
        arrayList.add(this.t);
        this.s = djVar.a(arrayList, this);
        B();
        this.l = djVar.a(this, this.d, new hs.a() {
            public void a(@NonNull t tVar, @NonNull ht htVar) {
                di.this.q.a(tVar, htVar);
            }
        });
        if (this.p.c()) {
            this.p.a("Read app environment for component %s. Value: %s", this.c.toString(), this.j.b().a);
        }
        this.q = djVar.a(this.d, this.l, this.g, this.j, this.f);
        this.i = djVar.d(this);
        this.h = djVar.a(this, this.i);
        this.g.a();
    }

    /* access modifiers changed from: protected */
    public ff e() {
        return this.i;
    }

    public void a(@NonNull t tVar) {
        if (this.o.c()) {
            this.o.a(tVar, "Event received on service");
        }
        if (cg.a(this.c.a())) {
            this.h.b(tVar);
        }
    }

    public synchronized void a(@NonNull db.a aVar) {
        this.m.a(aVar);
        b(aVar);
    }

    public synchronized void f() {
        this.f.e();
    }

    @Nullable
    public String g() {
        return this.d.f();
    }

    @NonNull
    public qp h() {
        return (qp) this.m.d();
    }

    public jk i() {
        return this.g;
    }

    public df b() {
        return this.c;
    }

    public synchronized void a(@Nullable sc scVar) {
        this.m.a(scVar);
        this.s.a();
    }

    public synchronized void a(@NonNull rw rwVar) {
    }

    public Context j() {
        return this.b;
    }

    @NonNull
    public tp k() {
        return this.o;
    }

    public void l() {
        this.q.b();
    }

    public void b(t tVar) {
        this.j.a(tVar.k());
        i.a b2 = this.j.b();
        if (this.k.a(b2, this.d) && this.o.c()) {
            this.o.a("Save new app environment for %s. Value: %s", b(), b2.a);
        }
    }

    public void m() {
        this.j.a();
        this.k.b(this.j.b(), this.d);
    }

    public void a(String str) {
        this.d.a(str).n();
    }

    public void n() {
        this.d.d(o() + 1).n();
        this.m.a();
    }

    public int o() {
        return this.d.i();
    }

    public boolean p() {
        return this.q.c() && h().f();
    }

    public boolean q() {
        qp h2 = h();
        return h2.H() && h2.f() && this.v.b() - this.q.d() >= h2.J();
    }

    public boolean r() {
        qp h2 = h();
        return h2.H() && this.v.b() - this.q.d() >= h2.K();
    }

    public boolean s() {
        return this.q.e() && h().I() && h().f();
    }

    public kf t() {
        return this.e;
    }

    @Deprecated
    public final ob u() {
        return new ob(this.b, this.c.a());
    }

    public boolean v() {
        return this.a.a();
    }

    public boolean w() {
        boolean z = false;
        boolean b2 = this.e.b(false);
        boolean z2 = this.m.b().v;
        if (b2 && z2) {
            z = true;
        }
        return !z;
    }

    public void c() {
        cg.a((Closeable) this.f);
        cg.a((Closeable) this.g);
    }

    private void B() {
        long libraryApiLevel = (long) YandexMetrica.getLibraryApiLevel();
        if (this.d.g() < libraryApiLevel) {
            this.r.a(new oa(u())).a();
            this.d.d(libraryApiLevel).n();
        }
    }

    private void b(@NonNull db.a aVar) {
        if (Boolean.TRUE.equals(aVar.l)) {
            this.o.a();
        } else if (Boolean.FALSE.equals(aVar.l)) {
            this.o.b();
        }
    }

    public kh x() {
        return this.d;
    }

    public void b(@Nullable String str) {
        this.d.d(str).n();
    }

    @NonNull
    public hd y() {
        return this.n;
    }

    @Nullable
    public String z() {
        return this.d.h();
    }

    @NonNull
    public lx A() {
        return this.s;
    }

    static class a {
        private final HashMap<String, i> a = new HashMap<>();

        a() {
        }

        public synchronized i a(@NonNull df dfVar, @NonNull tp tpVar, kh khVar) {
            i iVar;
            iVar = this.a.get(dfVar.toString());
            if (iVar == null) {
                i.a d = khVar.d();
                iVar = new i(d.a, d.b, tpVar);
                this.a.put(dfVar.toString(), iVar);
            }
            return iVar;
        }

        public synchronized boolean a(i.a aVar, kh khVar) {
            if (aVar.b <= khVar.d().b) {
                return false;
            }
            khVar.a(aVar).n();
            return true;
        }

        public synchronized void b(i.a aVar, kh khVar) {
            khVar.a(aVar).n();
        }
    }
}
