package com.yandex.metrica.impl.ob;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class jw implements jv {
    private final String a;
    private final HashMap<String, List<String>> b;

    public jw(@NonNull String str, @NonNull HashMap<String, List<String>> hashMap) {
        this.a = str;
        this.b = hashMap;
    }

    public boolean a(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        boolean z = true;
        try {
            for (Map.Entry next : this.b.entrySet()) {
                cursor = null;
                cursor = sQLiteDatabase.query((String) next.getKey(), null, null, null, null, null, null);
                if (cursor == null) {
                    cg.a(cursor);
                    return false;
                }
                z &= a(cursor, (String) next.getKey(), (List) next.getValue());
                cg.a(cursor);
            }
            return z;
        } catch (Throwable th) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean a(@NonNull Cursor cursor, @NonNull String str, @NonNull List<String> list) {
        List asList = Arrays.asList(cursor.getColumnNames());
        Collections.sort(asList);
        return list.equals(asList);
    }
}
