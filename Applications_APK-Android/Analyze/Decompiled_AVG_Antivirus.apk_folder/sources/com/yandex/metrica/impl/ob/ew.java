package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.ex;

public class ew extends bi<qr> {
    @NonNull
    private final ez j;
    @NonNull
    private final t k;
    @NonNull
    private final fa l;
    @NonNull
    private final ex.a m;
    @NonNull
    private final tx n;
    @NonNull
    private td o;
    @NonNull
    private final String p;
    @NonNull
    private final kh q;
    @Nullable
    private ey r;

    public ew(@NonNull ez ezVar, @NonNull t tVar, @NonNull fa faVar, @NonNull kh khVar) {
        this(ezVar, tVar, faVar, khVar, new ex.a(), new tw(), new td(), new qr());
    }

    public ew(@NonNull ez ezVar, @NonNull t tVar, @NonNull fa faVar, @NonNull kh khVar, @NonNull ex.a aVar, @NonNull tx txVar, @NonNull td tdVar, @NonNull qr qrVar) {
        super(new y(), qrVar);
        this.j = ezVar;
        this.k = tVar;
        this.l = faVar;
        this.q = khVar;
        this.m = aVar;
        this.n = txVar;
        this.o = tdVar;
        this.p = getClass().getName() + "@" + Integer.toHexString(hashCode());
    }

    public boolean a() {
        this.r = this.j.d();
        if (!(this.r.f() && !cg.a(this.r.a()))) {
            return false;
        }
        a(this.r.a());
        byte[] a = this.m.a(this.k, this.r, this.l, this.q).a();
        byte[] bArr = null;
        try {
            bArr = this.o.a(a);
        } catch (Throwable th) {
        }
        if (!cg.a(bArr)) {
            a("Content-Encoding", "gzip");
            a = bArr;
        }
        a(a);
        return true;
    }

    public void d() {
        super.d();
        a(this.n.a());
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Uri.Builder builder) {
        ((qr) this.i).a(builder, this.r);
    }

    @NonNull
    public String n() {
        return this.p;
    }
}
