package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class tn<K, V> {
    @NonNull
    private final Map<K, V> a;
    @NonNull
    private final V b;

    public tn(@NonNull V v) {
        this(new HashMap(), v);
    }

    @VisibleForTesting
    public tn(@NonNull Map<K, V> map, @NonNull V v) {
        this.a = map;
        this.b = v;
    }

    public void a(@Nullable K k, @Nullable V v) {
        this.a.put(k, v);
    }

    @NonNull
    public V a(@Nullable K k) {
        V v = this.a.get(k);
        return v == null ? this.b : v;
    }

    @NonNull
    public Set<K> a() {
        return this.a.keySet();
    }
}
