package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class eh {
    @Nullable
    private final String a;
    @NonNull
    private final String b;
    @Nullable
    private final Integer c;
    @Nullable
    private final String d;
    @NonNull
    private final ek e;

    public eh(@Nullable String str, @NonNull String str2, @Nullable Integer num, @Nullable String str3, @NonNull ek ekVar) {
        this.a = str;
        this.b = str2;
        this.c = num;
        this.d = str3;
        this.e = ekVar;
    }

    @NonNull
    public ek a() {
        return this.e;
    }

    @Nullable
    public String b() {
        return this.a;
    }

    @NonNull
    public String c() {
        return this.b;
    }

    @Nullable
    public Integer d() {
        return this.c;
    }

    @Nullable
    public String e() {
        return this.d;
    }

    @NonNull
    public static eh a(@NonNull cz czVar) {
        return a(czVar.g().h(), czVar.h().e(), czVar.g().e().intValue(), czVar.g().f(), czVar.h().q(), czVar.h().p());
    }

    @NonNull
    public static eh a(@NonNull String str, @Nullable String str2, int i, @NonNull String str3, boolean z, boolean z2) {
        eq eqVar;
        if (z) {
            eqVar = new eq();
        } else if (z2) {
            eqVar = new eo();
        } else if ("20799a27-fa80-4b36-b2db-0f8141f24180".equals(str2)) {
            eqVar = new et();
        } else {
            eqVar = new es();
        }
        return new eh(str2, str, Integer.valueOf(i), str3, eqVar);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        eh ehVar = (eh) o;
        String str = this.a;
        if (str == null ? ehVar.a != null : !str.equals(ehVar.a)) {
            return false;
        }
        if (!this.b.equals(ehVar.b)) {
            return false;
        }
        Integer num = this.c;
        if (num == null ? ehVar.c != null : !num.equals(ehVar.c)) {
            return false;
        }
        String str2 = this.d;
        if (str2 != null) {
            return str2.equals(ehVar.d);
        }
        if (ehVar.d == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.b.hashCode()) * 31;
        Integer num = this.c;
        int hashCode2 = (hashCode + (num != null ? num.hashCode() : 0)) * 31;
        String str2 = this.d;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "ClientDescription{mApiKey='" + this.a + '\'' + ", mPackageName='" + this.b + '\'' + ", mProcessID=" + this.c + ", mProcessSessionID='" + this.d + '\'' + '}';
    }
}
