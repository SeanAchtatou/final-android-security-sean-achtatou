package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter;

public class qv extends qu<qp> {
    @Nullable
    private qn a;
    private int b;

    public void a(@NonNull qn qnVar) {
        this.a = qnVar;
    }

    public void a(int i) {
        this.b = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.qu.a(android.net.Uri$Builder, com.yandex.metrica.impl.ob.qj):void
     arg types: [android.net.Uri$Builder, com.yandex.metrica.impl.ob.qp]
     candidates:
      com.yandex.metrica.impl.ob.qv.a(android.net.Uri$Builder, com.yandex.metrica.impl.ob.qp):void
      com.yandex.metrica.impl.ob.qu.a(android.net.Uri$Builder, com.yandex.metrica.impl.ob.qj):void */
    public void a(@NonNull Uri.Builder builder, @NonNull qp qpVar) {
        super.a(builder, (qj) qpVar);
        builder.path("report");
        b(builder, qpVar);
        c(builder, qpVar);
        builder.appendQueryParameter("request_id", String.valueOf(this.b));
    }

    private void b(@NonNull Uri.Builder builder, @NonNull qp qpVar) {
        qn qnVar = this.a;
        if (qnVar != null) {
            a(builder, "deviceid", qnVar.a, qpVar.r());
            a(builder, "uuid", this.a.b, qpVar.t());
            a(builder, "analytics_sdk_version", this.a.c);
            a(builder, "analytics_sdk_version_name", this.a.d);
            a(builder, "app_version_name", this.a.g, qpVar.q());
            a(builder, "app_build_number", this.a.i, qpVar.p());
            a(builder, "os_version", this.a.j, qpVar.n());
            a(builder, "os_api_level", this.a.k);
            a(builder, "analytics_sdk_build_number", this.a.e);
            a(builder, "analytics_sdk_build_type", this.a.f);
            a(builder, "app_debuggable", this.a.h);
            a(builder, "locale", this.a.l, qpVar.A());
            a(builder, "is_rooted", this.a.m, qpVar.u());
            a(builder, "app_framework", this.a.n, qpVar.v());
            a(builder, "attribution_id", this.a.o);
        }
    }

    private void c(@NonNull Uri.Builder builder, @NonNull qp qpVar) {
        builder.appendQueryParameter("api_key_128", qpVar.c());
        builder.appendQueryParameter(UrlManager.Parameter.APP_ID, qpVar.d());
        builder.appendQueryParameter("app_platform", qpVar.l());
        builder.appendQueryParameter(UrlManager.Parameter.MODEL, qpVar.m());
        builder.appendQueryParameter("manufacturer", qpVar.g());
        builder.appendQueryParameter("screen_width", String.valueOf(qpVar.w()));
        builder.appendQueryParameter("screen_height", String.valueOf(qpVar.x()));
        builder.appendQueryParameter("screen_dpi", String.valueOf(qpVar.y()));
        builder.appendQueryParameter("scalefactor", String.valueOf(qpVar.z()));
        builder.appendQueryParameter("device_type", qpVar.C());
        builder.appendQueryParameter("android_id", qpVar.B());
        a(builder, "clids_set", qpVar.a());
        GoogleAdvertisingIdGetter.c D = qpVar.D();
        String str = "";
        String str2 = D == null ? str : D.a;
        if (str2 == null) {
            str2 = str;
        }
        builder.appendQueryParameter("adv_id", str2);
        if (D != null) {
            str = a(D.b);
        }
        builder.appendQueryParameter("limit_ad_tracking", str);
    }

    private void a(Uri.Builder builder, String str, String str2, String str3) {
        builder.appendQueryParameter(str, ce.c(str2, str3));
    }

    private void a(Uri.Builder builder, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            builder.appendQueryParameter(str, str2);
        }
    }
}
