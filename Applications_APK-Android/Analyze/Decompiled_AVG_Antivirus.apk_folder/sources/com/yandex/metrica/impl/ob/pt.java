package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.c;
import com.yandex.metrica.e;

public class pt extends pu<qb> implements c {
    public pt(@NonNull uv uvVar, @NonNull Context context, @NonNull String str) {
        super(uvVar, context, str, new qb());
    }

    public void a(@NonNull final e eVar) {
        this.b.a(new Runnable() {
            public void run() {
                pt.this.b(eVar);
            }
        });
    }

    public void sendEventsBuffer() {
        ((qb) this.a).sendEventsBuffer();
        this.b.a(new Runnable() {
            public void run() {
                pt.this.a().sendEventsBuffer();
            }
        });
    }

    public void a(@Nullable final String str, @Nullable final String str2) {
        ((qb) this.a).a(str, str2);
        this.b.a(new Runnable() {
            public void run() {
                pt.this.a().a(str, str2);
            }
        });
    }
}
