package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.mopub.common.Constants;

public class nk extends nj {
    public nk(String str) {
        super(a(str));
    }

    public boolean b() {
        return true;
    }

    private static String a(@NonNull String str) {
        if (!TextUtils.isEmpty(str)) {
            Uri parse = Uri.parse(str);
            if (Constants.HTTP.equals(parse.getScheme())) {
                return parse.buildUpon().scheme("https").build().toString();
            }
        }
        return str;
    }
}
