package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.db;

public class ec {
    @NonNull
    private db.a a;

    public ec(@NonNull db.a aVar) {
        this.a = aVar;
    }

    public void a(@NonNull db.a aVar) {
        this.a = this.a.b(aVar);
    }

    @NonNull
    public db.a a() {
        return this.a;
    }
}
