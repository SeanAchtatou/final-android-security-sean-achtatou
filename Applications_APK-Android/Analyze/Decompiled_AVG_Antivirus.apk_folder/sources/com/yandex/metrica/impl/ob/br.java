package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.e;

public class br {
    @Nullable
    private String a;
    @Nullable
    private bq b;
    @Nullable
    private ah c;

    public br(@NonNull bq bqVar, @NonNull String str) {
        this.b = bqVar;
        this.a = str;
    }

    public br(@NonNull ah ahVar) {
        this.c = ahVar;
    }

    @NonNull
    public ah a() {
        if (this.c == null) {
            this.c = (ah) this.b.b(e.a(this.a).a());
        }
        return this.c;
    }
}
