package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import java.lang.reflect.Proxy;

public class qh {
    @NonNull
    private final Context a;

    public qh(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    @SuppressLint({"PrivateApi"})
    public void a(@NonNull final qg qgVar) {
        try {
            Class<?> cls = Class.forName("com.android.installreferrer.api.InstallReferrerClient");
            Object invoke = cls.getDeclaredMethod("newBuilder", Context.class).invoke(cls, this.a);
            Class<?> cls2 = Class.forName("com.android.installreferrer.api.InstallReferrerStateListener");
            final Object invoke2 = invoke.getClass().getDeclaredMethod("build", new Class[0]).invoke(invoke, new Object[0]);
            Object newProxyInstance = Proxy.newProxyInstance(cls2.getClassLoader(), new Class[]{cls2}, new qd(invoke2, new qg() {
                public void a(@NonNull qf qfVar) {
                    qgVar.a(qfVar);
                    a();
                }

                public void a(@NonNull Throwable th) {
                    qgVar.a(th);
                    a();
                }

                public void a() {
                    Object obj = invoke2;
                    if (obj != null) {
                        try {
                            obj.getClass().getDeclaredMethod("endConnection", new Class[0]).invoke(invoke2, new Object[0]);
                        } catch (Throwable th) {
                        }
                    }
                }
            }));
            invoke2.getClass().getDeclaredMethod("startConnection", cls2).invoke(invoke2, newProxyInstance);
        } catch (Throwable th) {
            qgVar.a(th);
        }
    }
}
