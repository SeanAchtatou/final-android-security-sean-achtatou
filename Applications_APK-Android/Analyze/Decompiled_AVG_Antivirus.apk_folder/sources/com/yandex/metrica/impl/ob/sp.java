package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.telephony.CellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;

class sp extends sl implements n {
    /* access modifiers changed from: private */
    @Nullable
    public final TelephonyManager a;
    /* access modifiers changed from: private */
    public PhoneStateListener b;
    /* access modifiers changed from: private */
    public boolean c;
    private sc d;
    private final n.a<sx> e;
    private final n.a<sm[]> f;
    @NonNull
    private final uv g;
    private final Context h;
    private final so i;
    private final su j;
    private final sr k;
    @NonNull
    private final nw l;
    @NonNull
    private nq m;

    protected sp(@NonNull Context context, @NonNull uv uvVar) {
        this(context, new nw(), uvVar);
    }

    protected sp(@NonNull Context context, @NonNull nw nwVar, @NonNull uv uvVar) {
        this(context, nwVar, new nq(nwVar.a()), uvVar);
    }

    protected sp(@NonNull Context context, @NonNull nw nwVar, @NonNull nq nqVar, @NonNull uv uvVar) {
        TelephonyManager telephonyManager;
        this.c = false;
        this.e = new n.a<>();
        this.f = new n.a<>();
        this.h = context;
        try {
            telephonyManager = (TelephonyManager) context.getSystemService("phone");
        } catch (Throwable th) {
            telephonyManager = null;
        }
        this.a = telephonyManager;
        this.g = uvVar;
        this.g.a(new Runnable() {
            public void run() {
                sp spVar = sp.this;
                PhoneStateListener unused = spVar.b = new a();
            }
        });
        this.i = new so(this, nqVar);
        this.j = new su(this, nqVar);
        this.k = new sr(this, nqVar);
        this.l = nwVar;
        this.m = nqVar;
    }

    public synchronized void a() {
        this.g.a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.yandex.metrica.impl.ob.sp.a(com.yandex.metrica.impl.ob.sp, boolean):boolean
             arg types: [com.yandex.metrica.impl.ob.sp, int]
             candidates:
              com.yandex.metrica.impl.ob.sp.a(com.yandex.metrica.impl.ob.sp, android.telephony.PhoneStateListener):android.telephony.PhoneStateListener
              com.yandex.metrica.impl.ob.sp.a(com.yandex.metrica.impl.ob.sp, android.telephony.SignalStrength):void
              com.yandex.metrica.impl.ob.sp.a(com.yandex.metrica.impl.ob.sp, boolean):boolean */
            public void run() {
                if (!sp.this.c) {
                    boolean unused = sp.this.c = true;
                    if (sp.this.b != null && sp.this.a != null) {
                        try {
                            sp.this.a.listen(sp.this.b, 256);
                        } catch (Throwable th) {
                        }
                    }
                }
            }
        });
    }

    public synchronized void b() {
        this.g.a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.yandex.metrica.impl.ob.sp.a(com.yandex.metrica.impl.ob.sp, boolean):boolean
             arg types: [com.yandex.metrica.impl.ob.sp, int]
             candidates:
              com.yandex.metrica.impl.ob.sp.a(com.yandex.metrica.impl.ob.sp, android.telephony.PhoneStateListener):android.telephony.PhoneStateListener
              com.yandex.metrica.impl.ob.sp.a(com.yandex.metrica.impl.ob.sp, android.telephony.SignalStrength):void
              com.yandex.metrica.impl.ob.sp.a(com.yandex.metrica.impl.ob.sp, boolean):boolean */
            public void run() {
                if (sp.this.c) {
                    boolean unused = sp.this.c = false;
                    cn.a().a(sp.this);
                    if (sp.this.b != null && sp.this.a != null) {
                        try {
                            sp.this.a.listen(sp.this.b, 0);
                        } catch (Throwable th) {
                        }
                    }
                }
            }
        });
    }

    public synchronized void a(sy syVar) {
        if (syVar != null) {
            syVar.a(e());
        }
    }

    public synchronized void a(sn snVar) {
        if (snVar != null) {
            snVar.a(j());
        }
    }

    @Nullable
    public TelephonyManager c() {
        return this.a;
    }

    public Context d() {
        return this.h;
    }

    private class a extends PhoneStateListener {
        private a() {
        }

        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            sp.this.c(signalStrength);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public synchronized sx e() {
        sx sxVar;
        sm b2;
        if (!this.e.b()) {
            if (!this.e.c()) {
                sxVar = this.e.a();
            }
        }
        sxVar = new sx(this.i, this.j, this.k);
        sm b3 = sxVar.b();
        if (b3 != null && b3.a() == null && !this.e.b() && (b2 = this.e.a().b()) != null) {
            sxVar.b().a(b2.a());
        }
        this.e.a((JSONArray) sxVar);
        return sxVar;
    }

    private synchronized sm[] j() {
        sm[] smVarArr;
        if (!this.f.b()) {
            if (!this.f.c()) {
                smVarArr = this.f.a();
            }
        }
        smVarArr = f();
        this.f.a((JSONArray) smVarArr);
        return smVarArr;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @SuppressLint({"MissingPermission"})
    @NonNull
    public sm[] f() {
        ArrayList arrayList = new ArrayList();
        if (cg.a(17) && this.m.a(this.h)) {
            try {
                List<CellInfo> allCellInfo = this.a.getAllCellInfo();
                if (!cg.a((Collection) allCellInfo)) {
                    for (int i2 = 0; i2 < allCellInfo.size(); i2++) {
                        sm a2 = a(allCellInfo.get(i2));
                        if (a2 != null) {
                            arrayList.add(a2);
                        }
                    }
                }
            } catch (Throwable th) {
            }
        }
        if (arrayList.size() >= 1) {
            return (sm[]) arrayList.toArray(new sm[arrayList.size()]);
        }
        sm b2 = e().b();
        if (b2 == null) {
            return new sm[0];
        }
        return new sm[]{b2};
    }

    @TargetApi(17)
    @Nullable
    private sm a(CellInfo cellInfo) {
        return sm.a(cellInfo);
    }

    /* access modifiers changed from: private */
    public synchronized void c(SignalStrength signalStrength) {
        sm b2;
        if (!this.e.b() && !this.e.c() && (b2 = this.e.a().b()) != null) {
            b2.a(Integer.valueOf(a(signalStrength)));
        }
    }

    @VisibleForTesting
    static int a(SignalStrength signalStrength) {
        if (signalStrength.isGsm()) {
            return b(signalStrength);
        }
        int cdmaDbm = signalStrength.getCdmaDbm();
        int evdoDbm = signalStrength.getEvdoDbm();
        if (-120 == evdoDbm) {
            return cdmaDbm;
        }
        if (-120 == cdmaDbm) {
            return evdoDbm;
        }
        return Math.min(cdmaDbm, evdoDbm);
    }

    @VisibleForTesting
    static int b(SignalStrength signalStrength) {
        int gsmSignalStrength = signalStrength.getGsmSignalStrength();
        if (99 == gsmSignalStrength) {
            return -1;
        }
        return (gsmSignalStrength * 2) - 113;
    }

    private synchronized boolean k() {
        return this.d != null;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean g() {
        return k() && this.d.o.n;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean h() {
        return k() && this.d.o.m;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean i() {
        return k() && this.d.o.l;
    }

    public void a(@NonNull sc scVar) {
        this.d = scVar;
        this.l.a(scVar);
        this.m.a(this.l.a());
    }

    public void a(boolean z) {
        this.l.a(z);
        this.m.a(this.l.a());
    }
}
