package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.pu;

public class ps<S extends pu> {
    @NonNull
    private final px a;
    @NonNull
    private final uv b;
    @NonNull
    private final pv<S> c;
    @NonNull
    private final pr d;

    @VisibleForTesting
    ps(@NonNull px pxVar, @NonNull uv uvVar, @NonNull pv<S> pvVar, @NonNull pr prVar) {
        this.a = pxVar;
        this.b = uvVar;
        this.c = pvVar;
        this.d = prVar;
    }

    @NonNull
    public uv a() {
        return this.b;
    }

    @NonNull
    public pv<S> b() {
        return this.c;
    }

    @NonNull
    public px c() {
        return this.a;
    }

    @NonNull
    public pr d() {
        return this.d;
    }
}
