package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.bj;
import java.util.EnumSet;

public class ni {
    private static final EnumSet<bj.a> a = EnumSet.of(bj.a.OFFLINE);
    private te b = new tb();
    private final Context c;

    public ni(@NonNull Context context) {
        this.c = context;
    }

    public boolean a() {
        return !a.contains(this.b.a(this.c));
    }
}
