package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.ab;
import com.yandex.metrica.impl.ob.pg;

public class tp extends tj {
    private static final int[] a = {3, 6, 4};
    private static final tp b = new tp();

    public tp(@Nullable String str) {
        super(str);
    }

    public tp() {
        this("");
    }

    public static tp h() {
        return b;
    }

    public String f() {
        return "AppMetrica";
    }

    public void a(t tVar, String str) {
        if (ab.b(tVar.g())) {
            StringBuilder sb = new StringBuilder(str);
            sb.append(": ");
            sb.append(tVar.d());
            if (ab.c(tVar.g()) && !TextUtils.isEmpty(tVar.e())) {
                sb.append(" with value ");
                sb.append(tVar.e());
            }
            a(sb.toString());
        }
    }

    public void a(pg.c.e.a aVar, String str) {
        if (a(aVar)) {
            a(str + ": " + b(aVar));
        }
    }

    private boolean a(pg.c.e.a aVar) {
        for (int i : a) {
            if (aVar.d == i) {
                return true;
            }
        }
        return false;
    }

    private String b(pg.c.e.a aVar) {
        if (aVar.d == 3 && TextUtils.isEmpty(aVar.e)) {
            return ab.a.EVENT_TYPE_NATIVE_CRASH.b();
        }
        if (aVar.d != 4) {
            return aVar.e;
        }
        StringBuilder sb = new StringBuilder(aVar.e);
        if (aVar.f != null) {
            String str = new String(aVar.f);
            if (!TextUtils.isEmpty(str)) {
                sb.append(" with value ");
                sb.append(str);
            }
        }
        return sb.toString();
    }

    public void a(pg.c.e eVar, String str) {
        for (pg.c.e.a a2 : eVar.d) {
            a(a2, str);
        }
    }
}
