package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.mh;
import org.json.JSONException;
import org.json.JSONObject;

class my {
    @Nullable
    public static String a(@NonNull ms msVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("collection_mode", msVar.a.toString());
            jSONObject.put("lat", msVar.c().getLatitude());
            jSONObject.put("lon", msVar.c().getLongitude());
            jSONObject.putOpt("timestamp", Long.valueOf(msVar.c().getTime()));
            jSONObject.putOpt("receive_timestamp", Long.valueOf(msVar.b()));
            jSONObject.put("receive_elapsed_realtime_seconds", msVar.d());
            jSONObject.putOpt("precision", msVar.c().hasAccuracy() ? Float.valueOf(msVar.c().getAccuracy()) : null);
            jSONObject.putOpt("direction", msVar.c().hasBearing() ? Float.valueOf(msVar.c().getBearing()) : null);
            jSONObject.putOpt("speed", msVar.c().hasSpeed() ? Float.valueOf(msVar.c().getSpeed()) : null);
            jSONObject.putOpt("altitude", msVar.c().hasAltitude() ? Double.valueOf(msVar.c().getAltitude()) : null);
            jSONObject.putOpt("provider", ce.c(msVar.c().getProvider(), null));
            return jSONObject.toString();
        } catch (JSONException e) {
            return null;
        }
    }

    @Nullable
    public static ms a(long j, @NonNull String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                long optLong = jSONObject.optLong("receive_timestamp", 0);
                long optLong2 = jSONObject.optLong("receive_elapsed_realtime_seconds", 0);
                mh.a a = mh.a.a(jSONObject.optString("collection_mode"));
                Location location = new Location(jSONObject.optString("provider", null));
                location.setLongitude(jSONObject.optDouble("lon", 0.0d));
                location.setLatitude(jSONObject.optDouble("lat", 0.0d));
                location.setTime(jSONObject.optLong("timestamp", 0));
                location.setAccuracy((float) jSONObject.optDouble("precision", 0.0d));
                location.setBearing((float) jSONObject.optDouble("direction", 0.0d));
                location.setSpeed((float) jSONObject.optDouble("speed", 0.0d));
                location.setAltitude(jSONObject.optDouble("altitude", 0.0d));
                return new ms(a, optLong, optLong2, location, Long.valueOf(j));
            } catch (JSONException e) {
            }
        }
        return null;
    }

    @Nullable
    public static mn b(long j, @NonNull String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            mn mnVar = new mn();
            try {
                mnVar.a(Long.valueOf(j));
                JSONObject jSONObject = new JSONObject(str);
                mnVar.a(jSONObject.optLong("timestamp", 0));
                mnVar.b(jSONObject.optLong("elapsed_realtime_seconds", 0));
                mnVar.b(jSONObject.optJSONArray("cell_info"));
                mnVar.a(jSONObject.optJSONArray("wifi_info"));
                return mnVar;
            } catch (JSONException e) {
                return mnVar;
            }
        } catch (JSONException e2) {
            return null;
        }
    }

    @Nullable
    public static String a(@NonNull mn mnVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("timestamp", mnVar.b());
            jSONObject.put("elapsed_realtime_seconds", mnVar.e());
            jSONObject.putOpt("wifi_info", mnVar.c());
            jSONObject.putOpt("cell_info", mnVar.d());
            return jSONObject.toString();
        } catch (JSONException e) {
            return null;
        }
    }
}
