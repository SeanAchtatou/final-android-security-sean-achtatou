package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.pg;

public class na {
    @NonNull
    private final mg a;
    @NonNull
    private final mz b;

    public na(@NonNull mg mgVar, @NonNull mz mzVar) {
        this.a = mgVar;
        this.b = mzVar;
    }

    @Nullable
    public pg.b.a a(long j, @Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            mn b2 = this.a.b(j, str);
            if (b2 != null) {
                return this.b.a(b2);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
