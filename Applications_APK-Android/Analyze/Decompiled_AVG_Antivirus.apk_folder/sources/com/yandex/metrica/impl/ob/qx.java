package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class qx {
    @NonNull
    private kp<ra> a;
    @NonNull
    private ra b;
    @NonNull
    private tw c;
    @NonNull
    private rc d;
    @NonNull
    private a e;

    interface a {
        void a();
    }

    public qx(@NonNull Context context, @NonNull kp<ra> kpVar, @NonNull a aVar) {
        this(kpVar, aVar, new tw(), new rc(context, kpVar));
    }

    @VisibleForTesting
    qx(@NonNull kp<ra> kpVar, @NonNull a aVar, @NonNull tw twVar, @NonNull rc rcVar) {
        this.a = kpVar;
        this.b = this.a.a();
        this.c = twVar;
        this.d = rcVar;
        this.e = aVar;
    }

    public void a(@NonNull ra raVar) {
        this.a.a(raVar);
        this.b = raVar;
        this.d.a();
        this.e.a();
    }

    public void a() {
        ra raVar = new ra(this.b.a, this.b.b, this.c.a(), true, true);
        this.a.a(raVar);
        this.b = raVar;
        this.e.a();
    }
}
