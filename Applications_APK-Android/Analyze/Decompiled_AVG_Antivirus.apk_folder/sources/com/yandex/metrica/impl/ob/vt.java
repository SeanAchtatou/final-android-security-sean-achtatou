package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class vt<T> implements vx<T> {
    @NonNull
    private final vx<T> a;

    public vt(@NonNull vx<T> vxVar) {
        this.a = vxVar;
    }

    public vv a(@Nullable T t) {
        vv a2 = this.a.a(t);
        if (a2.a()) {
            return a2;
        }
        throw new vu(a2.b());
    }
}
