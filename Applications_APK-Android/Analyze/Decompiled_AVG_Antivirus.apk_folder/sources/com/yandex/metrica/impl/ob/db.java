package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.impl.ob.qq;
import java.util.Map;

public class db {
    public final qq.a a;
    public final a b;
    @Nullable
    public final ResultReceiver c;

    public db(@NonNull cz czVar) {
        this(new qq.a(czVar), new a(czVar.h(), czVar.g().c()), czVar.g().i());
    }

    public db(@NonNull qq.a aVar, @NonNull a aVar2, @Nullable ResultReceiver resultReceiver) {
        this.a = aVar;
        this.b = aVar2;
        this.c = resultReceiver;
    }

    public static class a implements qi<a, a> {
        @Nullable
        public final String a;
        @Nullable
        public final String b;
        @Nullable
        public final String c;
        @Nullable
        public final String d;
        @Nullable
        public final Boolean e;
        @Nullable
        public final Location f;
        @Nullable
        public final Boolean g;
        @Nullable
        public final Boolean h;
        @Nullable
        public final Integer i;
        @Nullable
        public final Integer j;
        @Nullable
        public final Integer k;
        @Nullable
        public final Boolean l;
        @Nullable
        public final Boolean m;
        @Nullable
        public final Boolean n;
        @Nullable
        public final Map<String, String> o;

        a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable Boolean bool, @Nullable Location location, @Nullable Boolean bool2, @Nullable Boolean bool3, @Nullable Boolean bool4, @Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Boolean bool5, @Nullable Boolean bool6, @Nullable Map<String, String> map) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
            this.e = bool;
            this.f = location;
            this.g = bool2;
            this.h = bool3;
            this.n = bool4;
            this.i = num;
            this.j = num2;
            this.k = num3;
            this.l = bool5;
            this.m = bool6;
            this.o = map;
        }

        public a(@NonNull CounterConfiguration counterConfiguration, @Nullable Map<String, String> map) {
            this(counterConfiguration.d(), counterConfiguration.g(), counterConfiguration.h(), counterConfiguration.e(), counterConfiguration.f(), counterConfiguration.k(), counterConfiguration.m(), counterConfiguration.l(), counterConfiguration.i(), counterConfiguration.c(), counterConfiguration.b(), counterConfiguration.a(), counterConfiguration.j(), counterConfiguration.n(), map);
        }

        public a() {
            this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
          com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
          com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
          com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
          com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
          com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T */
        @NonNull
        /* renamed from: a */
        public a b(@NonNull a aVar) {
            a aVar2 = aVar;
            return new a((String) ua.a((Object) this.a, (Object) aVar2.a), (String) ua.a((Object) this.b, (Object) aVar2.b), (String) ua.a((Object) this.c, (Object) aVar2.c), (String) ua.a((Object) this.d, (Object) aVar2.d), (Boolean) ua.a(this.e, aVar2.e), (Location) ua.a(this.f, aVar2.f), (Boolean) ua.a(this.g, aVar2.g), (Boolean) ua.a(this.h, aVar2.h), (Boolean) ua.a(this.n, aVar2.n), (Integer) ua.a(this.i, aVar2.i), (Integer) ua.a(this.j, aVar2.j), (Integer) ua.a(this.k, aVar2.k), (Boolean) ua.a(this.l, aVar2.l), (Boolean) ua.a(this.m, aVar2.m), (Map) ua.a(this.o, aVar2.o));
        }

        /* renamed from: b */
        public boolean a(@NonNull a aVar) {
            return equals(aVar);
        }

        public boolean equals(Object o2) {
            if (this == o2) {
                return true;
            }
            if (o2 == null || getClass() != o2.getClass()) {
                return false;
            }
            a aVar = (a) o2;
            String str = this.a;
            if (str == null ? aVar.a != null : !str.equals(aVar.a)) {
                return false;
            }
            String str2 = this.b;
            if (str2 == null ? aVar.b != null : !str2.equals(aVar.b)) {
                return false;
            }
            String str3 = this.c;
            if (str3 == null ? aVar.c != null : !str3.equals(aVar.c)) {
                return false;
            }
            String str4 = this.d;
            if (str4 == null ? aVar.d != null : !str4.equals(aVar.d)) {
                return false;
            }
            Boolean bool = this.e;
            if (bool == null ? aVar.e != null : !bool.equals(aVar.e)) {
                return false;
            }
            Location location = this.f;
            if (location == null ? aVar.f != null : !location.equals(aVar.f)) {
                return false;
            }
            Boolean bool2 = this.g;
            if (bool2 == null ? aVar.g != null : !bool2.equals(aVar.g)) {
                return false;
            }
            Boolean bool3 = this.h;
            if (bool3 == null ? aVar.h != null : !bool3.equals(aVar.h)) {
                return false;
            }
            Integer num = this.i;
            if (num == null ? aVar.i != null : !num.equals(aVar.i)) {
                return false;
            }
            Integer num2 = this.j;
            if (num2 == null ? aVar.j != null : !num2.equals(aVar.j)) {
                return false;
            }
            Integer num3 = this.k;
            if (num3 == null ? aVar.k != null : !num3.equals(aVar.k)) {
                return false;
            }
            Boolean bool4 = this.l;
            if (bool4 == null ? aVar.l != null : !bool4.equals(aVar.l)) {
                return false;
            }
            Boolean bool5 = this.m;
            if (bool5 == null ? aVar.m != null : !bool5.equals(aVar.m)) {
                return false;
            }
            Boolean bool6 = this.n;
            if (bool6 == null ? aVar.n != null : !bool6.equals(aVar.n)) {
                return false;
            }
            Map<String, String> map = this.o;
            if (map != null) {
                return map.equals(aVar.o);
            }
            if (aVar.o == null) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            String str = this.a;
            int i2 = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.b;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.c;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.d;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            Boolean bool = this.e;
            int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
            Location location = this.f;
            int hashCode6 = (hashCode5 + (location != null ? location.hashCode() : 0)) * 31;
            Boolean bool2 = this.g;
            int hashCode7 = (hashCode6 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
            Boolean bool3 = this.h;
            int hashCode8 = (hashCode7 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
            Integer num = this.i;
            int hashCode9 = (hashCode8 + (num != null ? num.hashCode() : 0)) * 31;
            Integer num2 = this.j;
            int hashCode10 = (hashCode9 + (num2 != null ? num2.hashCode() : 0)) * 31;
            Integer num3 = this.k;
            int hashCode11 = (hashCode10 + (num3 != null ? num3.hashCode() : 0)) * 31;
            Boolean bool4 = this.l;
            int hashCode12 = (hashCode11 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
            Boolean bool5 = this.m;
            int hashCode13 = (hashCode12 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
            Boolean bool6 = this.n;
            int hashCode14 = (hashCode13 + (bool6 != null ? bool6.hashCode() : 0)) * 31;
            Map<String, String> map = this.o;
            if (map != null) {
                i2 = map.hashCode();
            }
            return hashCode14 + i2;
        }
    }
}
