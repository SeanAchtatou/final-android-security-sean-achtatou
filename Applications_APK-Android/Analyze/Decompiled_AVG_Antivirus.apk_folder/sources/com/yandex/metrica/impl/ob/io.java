package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class io extends im {
    @NonNull
    private final ik a;

    public io(@NonNull Context context, @NonNull ik ikVar) {
        super(context);
        this.a = ikVar;
    }

    public void a(@Nullable Bundle bundle, @Nullable il ilVar) {
        if (bundle != null) {
            this.a.a();
        }
        if (ilVar != null) {
            ilVar.a();
        }
    }
}
