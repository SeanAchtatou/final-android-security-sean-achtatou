package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class mq {
    @NonNull
    private mr a;
    /* access modifiers changed from: private */
    @NonNull
    public mu b;
    @NonNull
    private mk c;
    @NonNull
    private LocationListener d;
    private boolean e;

    static class a {
        a() {
        }

        @NonNull
        public mr a(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull LocationListener locationListener, @NonNull nq nqVar) {
            return new mr(context, looper, locationManager, locationListener, nqVar);
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public mq(@androidx.annotation.NonNull android.content.Context r13, @androidx.annotation.NonNull android.os.Looper r14, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.sc r15, @androidx.annotation.Nullable android.location.LocationManager r16, @androidx.annotation.Nullable com.yandex.metrica.impl.ob.mh r17, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.mw r18, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.md r19, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.nq r20) {
        /*
            r12 = this;
            com.yandex.metrica.impl.ob.mq$a r4 = new com.yandex.metrica.impl.ob.mq$a
            r4.<init>()
            com.yandex.metrica.impl.ob.mu r11 = new com.yandex.metrica.impl.ob.mu
            r5 = r11
            r6 = r13
            r7 = r15
            r8 = r17
            r9 = r18
            r10 = r19
            r5.<init>(r6, r7, r8, r9, r10)
            com.yandex.metrica.impl.ob.mk r6 = new com.yandex.metrica.impl.ob.mk
            r1 = r13
            r3 = r16
            r7 = r20
            r6.<init>(r13, r3, r7)
            r0 = r12
            r2 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.mq.<init>(android.content.Context, android.os.Looper, com.yandex.metrica.impl.ob.sc, android.location.LocationManager, com.yandex.metrica.impl.ob.mh, com.yandex.metrica.impl.ob.mw, com.yandex.metrica.impl.ob.md, com.yandex.metrica.impl.ob.nq):void");
    }

    public void a() {
        Location a2 = this.c.a();
        if (a2 != null) {
            this.b.a(a2);
        }
    }

    @Nullable
    public Location b() {
        return this.b.a();
    }

    @Nullable
    public Location c() {
        return this.c.a();
    }

    public void d() {
        this.e = true;
        this.a.a();
    }

    public void e() {
        this.e = false;
        this.a.b();
    }

    public void a(@NonNull sc scVar, @Nullable mh mhVar) {
        this.b.a(scVar, mhVar);
        f();
    }

    private void f() {
        if (this.e) {
            e();
            d();
            a();
        }
    }

    @VisibleForTesting
    mq(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull a aVar, @NonNull mu muVar, @NonNull mk mkVar, @NonNull nq nqVar) {
        this.e = false;
        this.d = new LocationListener() {
            public void onLocationChanged(@Nullable Location location) {
                if (location != null) {
                    mq.this.b.a(location);
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
        this.c = mkVar;
        this.a = aVar.a(context, looper, locationManager, this.d, nqVar);
        this.b = muVar;
    }
}
