package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.yandex.metrica.c;
import com.yandex.metrica.e;
import com.yandex.metrica.g;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class bq {
    @NonNull
    private Context a;
    @NonNull
    private da b;
    @NonNull
    private bt c;
    @NonNull
    private Handler d;
    @NonNull
    private rx e;
    private Map<String, c> f = new HashMap();
    private final vx<String> g = new vt(new vz(this.f));
    private final List<String> h = Arrays.asList("20799a27-fa80-4b36-b2db-0f8141f24180", "0e5e9c33-f8c3-4568-86c5-2e4f57523f72");

    public bq(@NonNull Context context, @NonNull da daVar, @NonNull bt btVar, @NonNull Handler handler, @NonNull rx rxVar) {
        this.a = context;
        this.b = daVar;
        this.c = btVar;
        this.d = handler;
        this.e = rxVar;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public ap a(@NonNull g gVar, boolean z) {
        this.g.a(gVar.apiKey);
        ap apVar = new ap(this.a, this.b, gVar, this.c, this.e, new br(this, "20799a27-fa80-4b36-b2db-0f8141f24180"), new br(this, "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"));
        a(apVar);
        apVar.a(gVar, z);
        apVar.a();
        this.c.a(apVar);
        this.f.put(gVar.apiKey, apVar);
        return apVar;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(@NonNull e eVar) {
        if (this.f.containsKey(eVar.apiKey)) {
            tp a2 = ti.a(eVar.apiKey);
            if (a2.c()) {
                a2.b("Reporter with apiKey=%s already exists.", eVar.apiKey);
            }
        } else {
            b(eVar);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* access modifiers changed from: package-private */
    @NonNull
    public synchronized c b(@NonNull e eVar) {
        aq aqVar;
        c cVar = this.f.get(eVar.apiKey);
        aqVar = cVar;
        if (cVar == null) {
            if (!this.h.contains(eVar.apiKey)) {
                this.e.c();
            }
            aq aqVar2 = new aq(this.a, this.b, eVar, this.c);
            a(aqVar2);
            aqVar2.a();
            this.f.put(eVar.apiKey, aqVar2);
            aqVar = aqVar2;
        }
        return aqVar;
    }

    private void a(@NonNull m mVar) {
        mVar.a(new al(this.d, mVar));
        mVar.a(this.e);
    }
}
