package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pf;
import java.util.Arrays;
import java.util.List;

public class je implements ky<Throwable, pf.f> {
    @NonNull
    private jb a = new jb();

    @NonNull
    /* renamed from: a */
    public pf.f b(@NonNull Throwable th) {
        pf.f fVar = new pf.f();
        fVar.b = th.getClass().getName();
        fVar.c = ua.b(th.getMessage(), "");
        fVar.d = this.a.a((List<StackTraceElement>) Arrays.asList(th.getStackTrace()));
        if (th.getCause() != null) {
            fVar.e = b(th.getCause());
        }
        return fVar;
    }

    @NonNull
    public Throwable a(@NonNull pf.f fVar) {
        throw new UnsupportedOperationException();
    }
}
