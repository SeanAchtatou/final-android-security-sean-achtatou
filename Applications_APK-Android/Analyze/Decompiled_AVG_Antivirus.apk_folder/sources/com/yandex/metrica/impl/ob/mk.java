package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.LocationManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class mk {
    static final Set<String> a = new HashSet(Arrays.asList("gps"));
    @NonNull
    private Context b;
    @Nullable
    private LocationManager c;
    @NonNull
    private nq d;

    public mk(@NonNull Context context, @Nullable LocationManager locationManager, @NonNull nq nqVar) {
        this.b = context;
        this.c = locationManager;
        this.d = nqVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0027 A[SYNTHETIC] */
    @androidx.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.location.Location a() {
        /*
            r12 = this;
            android.location.LocationManager r0 = r12.c
            r1 = 0
            if (r0 == 0) goto L_0x0065
            com.yandex.metrica.impl.ob.nq r0 = r12.d
            android.content.Context r2 = r12.b
            boolean r0 = r0.a(r2)
            com.yandex.metrica.impl.ob.nq r2 = r12.d
            android.content.Context r3 = r12.b
            boolean r2 = r2.b(r3)
            android.location.LocationManager r3 = r12.c     // Catch:{ Throwable -> 0x001e }
            java.util.List r3 = r3.getAllProviders()     // Catch:{ Throwable -> 0x001e }
            goto L_0x0020
        L_0x001e:
            r3 = move-exception
            r3 = r1
        L_0x0020:
            if (r3 == 0) goto L_0x0065
            java.util.Iterator r3 = r3.iterator()
            r10 = r1
        L_0x0027:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0064
            java.lang.Object r4 = r3.next()
            java.lang.String r4 = (java.lang.String) r4
            java.util.Set<java.lang.String> r5 = com.yandex.metrica.impl.ob.mk.a
            boolean r5 = r5.contains(r4)
            if (r5 != 0) goto L_0x0063
            if (r0 == 0) goto L_0x0052
            java.lang.String r5 = "passive"
            boolean r5 = r5.equals(r4)     // Catch:{ Throwable -> 0x004f }
            if (r5 == 0) goto L_0x0048
            if (r2 == 0) goto L_0x0052
        L_0x0048:
            android.location.LocationManager r5 = r12.c     // Catch:{ Throwable -> 0x004f }
            android.location.Location r4 = r5.getLastKnownLocation(r4)     // Catch:{ Throwable -> 0x004f }
            goto L_0x0053
        L_0x004f:
            r4 = move-exception
            r11 = r1
            goto L_0x0054
        L_0x0052:
            r4 = r1
        L_0x0053:
            r11 = r4
        L_0x0054:
            if (r11 == 0) goto L_0x0063
            long r6 = com.yandex.metrica.impl.ob.mi.a
            r8 = 200(0xc8, double:9.9E-322)
            r4 = r11
            r5 = r10
            boolean r4 = com.yandex.metrica.impl.ob.mi.a(r4, r5, r6, r8)
            if (r4 == 0) goto L_0x0063
            r10 = r11
        L_0x0063:
            goto L_0x0027
        L_0x0064:
            r1 = r10
        L_0x0065:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.mk.a():android.location.Location");
    }
}
