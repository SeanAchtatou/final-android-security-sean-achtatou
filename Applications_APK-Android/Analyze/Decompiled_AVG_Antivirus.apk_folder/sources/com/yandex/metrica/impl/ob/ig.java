package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class ig {
    @NonNull
    private final uv a;

    public static class a implements Runnable {
        @NonNull
        private final im a;
        @Nullable
        private final Bundle b;
        @Nullable
        private final il c;

        public a(@NonNull im imVar, @Nullable Bundle bundle) {
            this(imVar, bundle, null);
        }

        public a(@NonNull im imVar, @Nullable Bundle bundle, @Nullable il ilVar) {
            this.a = imVar;
            this.b = bundle;
            this.c = ilVar;
        }

        public void run() {
            try {
                this.a.a(this.b, this.c);
            } catch (Throwable th) {
                il ilVar = this.c;
                if (ilVar != null) {
                    ilVar.a();
                }
            }
        }
    }

    public ig() {
        this(af.a().j().f());
    }

    @VisibleForTesting
    ig(@NonNull uv uvVar) {
        this.a = uvVar;
    }

    public void a(@NonNull im imVar, @Nullable Bundle bundle) {
        this.a.a(new a(imVar, bundle));
    }

    public void a(@NonNull im imVar, @Nullable Bundle bundle, @Nullable il ilVar) {
        this.a.a(new a(imVar, bundle, ilVar));
    }

    @NonNull
    public uv a() {
        return this.a;
    }
}
