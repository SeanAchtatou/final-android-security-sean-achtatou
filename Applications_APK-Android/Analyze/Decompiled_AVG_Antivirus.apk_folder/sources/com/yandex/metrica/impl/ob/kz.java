package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pp;
import com.yandex.metrica.impl.ob.rq;

public class kz implements lc<rq, pp.a.b> {
    @NonNull
    /* renamed from: a */
    public pp.a.b b(@NonNull rq rqVar) {
        pp.a.b bVar = new pp.a.b();
        bVar.e = rqVar.d;
        bVar.d = rqVar.c;
        bVar.c = rqVar.b;
        bVar.b = rqVar.a;
        bVar.f = rqVar.e;
        bVar.g = rqVar.f;
        bVar.h = rqVar.g;
        bVar.l = rqVar.l;
        bVar.k = rqVar.j;
        bVar.n = rqVar.n;
        bVar.m = rqVar.m;
        bVar.i = rqVar.h;
        bVar.j = rqVar.i;
        return bVar;
    }

    @NonNull
    public rq a(@NonNull pp.a.b bVar) {
        return new rq.a().a(bVar.b).m(bVar.n).l(bVar.m).k(bVar.l).j(bVar.k).i(bVar.j).h(bVar.i).g(bVar.h).f(bVar.g).d(bVar.e).e(bVar.f).c(bVar.d).b(bVar.c).a();
    }
}
