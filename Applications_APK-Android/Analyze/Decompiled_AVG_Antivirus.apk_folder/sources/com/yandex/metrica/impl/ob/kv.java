package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.io.IOException;

public interface kv<T> {
    @NonNull
    byte[] a(@NonNull Object obj);

    @NonNull
    T b(@NonNull byte[] bArr) throws IOException;

    @NonNull
    T c();
}
