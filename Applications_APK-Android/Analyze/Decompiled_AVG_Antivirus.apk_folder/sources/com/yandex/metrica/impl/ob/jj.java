package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;

public class jj {
    @NonNull
    private final jx a;
    @NonNull
    private final String b;

    public jj(@NonNull jx jxVar, @NonNull String str) {
        this.a = jxVar;
        this.b = str;
    }

    public void a(@NonNull String str, @NonNull byte[] bArr) {
        SQLiteDatabase sQLiteDatabase;
        try {
            sQLiteDatabase = this.a.a();
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("data_key", str);
                contentValues.put("value", bArr);
                sQLiteDatabase.insertWithOnConflict(this.b, null, contentValues, 5);
            } catch (Throwable th) {
                th = th;
                this.a.a(sQLiteDatabase);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            sQLiteDatabase = null;
            this.a.a(sQLiteDatabase);
            throw th;
        }
        this.a.a(sQLiteDatabase);
    }

    public byte[] a(@NonNull String str) {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase;
        Cursor cursor2 = null;
        try {
            sQLiteDatabase = this.a.a();
            try {
                cursor = sQLiteDatabase.query(this.b, null, "data_key = ?", new String[]{str}, null, null, null);
                if (cursor != null) {
                    try {
                        if (cursor.getCount() == 1 && cursor.moveToFirst()) {
                            byte[] blob = cursor.getBlob(cursor.getColumnIndex("value"));
                            cg.a(cursor);
                            this.a.a(sQLiteDatabase);
                            return blob;
                        }
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        cursor2 = cursor;
                        th = th2;
                        cg.a(cursor2);
                        this.a.a(sQLiteDatabase);
                        throw th;
                    }
                }
                cg.b(cursor);
                cg.a(cursor);
                this.a.a(sQLiteDatabase);
                return null;
            } catch (Throwable th3) {
                th = th3;
                cg.a(cursor2);
                this.a.a(sQLiteDatabase);
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            sQLiteDatabase = null;
            cg.a(cursor2);
            this.a.a(sQLiteDatabase);
            throw th;
        }
    }
}
