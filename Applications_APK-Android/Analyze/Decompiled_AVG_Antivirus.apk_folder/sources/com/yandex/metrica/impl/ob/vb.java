package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

public class vb {
    /* access modifiers changed from: package-private */
    @NonNull
    public uu a() {
        return new uu("YMM-MC");
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public Executor b() {
        return new vd();
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public uu c() {
        return new uu("YMM-MSTE");
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public uu d() {
        return new uu("YMM-TP");
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public uu e() {
        return new uu("YMM-UH-1");
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public uu f() {
        return new uu("YMM-CSE");
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public uu g() {
        return new uu("YMM-CTH");
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public uu h() {
        return new uu("YMM-SDCT");
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public uu i() {
        return new uu("YMM-DE");
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public uz a(@NonNull Runnable runnable) {
        return va.a("YMM-IB", runnable);
    }
}
