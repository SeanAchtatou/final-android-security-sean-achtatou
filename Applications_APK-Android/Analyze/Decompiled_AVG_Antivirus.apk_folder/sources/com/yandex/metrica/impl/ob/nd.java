package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.util.Collection;

public class nd extends bf<qt> {
    @NonNull
    private Context j;
    @NonNull
    private nh k;
    @NonNull
    private final cd l;
    @NonNull
    private mh m;
    @NonNull
    private kj n;
    @NonNull
    private final nf o;
    private long p;
    private ne q;

    public nd(@NonNull Context context, @NonNull nh nhVar, @NonNull cd cdVar) {
        this(context, nhVar, cdVar, new kj(jo.a(context).c()), new qt(), new nf(context));
    }

    public boolean a() {
        if (!this.l.d() && !TextUtils.isEmpty(this.k.r()) && !TextUtils.isEmpty(this.k.t()) && !cg.a((Collection) s())) {
            return D();
        }
        return false;
    }

    public boolean b() {
        boolean b = super.b();
        F();
        return b;
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Uri.Builder builder) {
        ((qt) this.i).a(builder, this.k);
    }

    /* access modifiers changed from: protected */
    public void C() {
        this.o.a(this.q);
    }

    public boolean t() {
        return super.t() & (400 != k());
    }

    private boolean D() {
        this.q = this.o.a(this.m.h);
        if (!this.q.a()) {
            return c(e.a(this.q.c));
        }
        return false;
    }

    private void E() {
        this.p = this.n.b(-1L) + 1;
        ((qt) this.i).a(this.p);
    }

    private void F() {
        this.n.c(this.p).n();
    }

    @VisibleForTesting
    nd(@NonNull Context context, @NonNull nh nhVar, @NonNull cd cdVar, @NonNull kj kjVar, @NonNull qt qtVar, @NonNull nf nfVar) {
        super(qtVar);
        this.j = context;
        this.k = nhVar;
        this.l = cdVar;
        this.m = this.k.a();
        this.n = kjVar;
        this.o = nfVar;
        E();
        a(this.k.b());
    }
}
