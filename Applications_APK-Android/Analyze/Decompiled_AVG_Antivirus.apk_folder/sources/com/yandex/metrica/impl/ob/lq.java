package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

@Deprecated
public class lq {
    @Nullable
    public final String a;
    @Nullable
    public final String b;

    public lq(@Nullable String str, @Nullable String str2) {
        this.a = str;
        this.b = str2;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        lq lqVar = (lq) o;
        String str = this.a;
        if (str == null ? lqVar.a != null : !str.equals(lqVar.a)) {
            return false;
        }
        String str2 = this.b;
        if (str2 != null) {
            return str2.equals(lqVar.b);
        }
        if (lqVar.b == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "AppMetricaDeviceIdentifiers{deviceID='" + this.a + '\'' + ", deviceIDHash='" + this.b + '\'' + '}';
    }
}
