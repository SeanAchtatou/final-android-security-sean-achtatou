package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.h;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.LongCompanionObject;

public class ac {
    @NonNull
    private final List<b> a = new ArrayList();

    static class a {
        private boolean a = false;
        private long b;
        private long c;
        private long d = LongCompanionObject.MAX_VALUE;

        a() {
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            if (!this.a && this.b - this.c < this.d) {
                return false;
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public void a(long j) {
            this.d = j;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.a = true;
        }

        /* access modifiers changed from: package-private */
        public void a(@Nullable sc scVar) {
            if (scVar != null) {
                this.b = scVar.C;
                this.c = scVar.D;
            }
        }
    }

    public static class b {
        @NonNull
        private a a;
        @NonNull
        private final h.a b;
        @NonNull
        private final uv c;

        private b(@NonNull uv uvVar, @NonNull h.a aVar, @NonNull a aVar2) {
            this.b = aVar;
            this.a = aVar2;
            this.c = uvVar;
        }

        public void a(@Nullable sc scVar) {
            this.a.a(scVar);
        }

        public void a(long j) {
            this.a.a(j);
        }

        public boolean a(int i) {
            if (!this.a.a()) {
                return false;
            }
            this.b.a(TimeUnit.SECONDS.toMillis((long) i), this.c);
            this.a.b();
            return true;
        }
    }

    public b a(@NonNull Runnable runnable, @NonNull uv uvVar) {
        return a(uvVar, new h.a(runnable), new a());
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public b a(@NonNull uv uvVar, @NonNull h.a aVar, @NonNull a aVar2) {
        b bVar = new b(uvVar, aVar, aVar2);
        this.a.add(bVar);
        return bVar;
    }

    public void a(@Nullable sc scVar) {
        for (b a2 : this.a) {
            a2.a(scVar);
        }
    }
}
