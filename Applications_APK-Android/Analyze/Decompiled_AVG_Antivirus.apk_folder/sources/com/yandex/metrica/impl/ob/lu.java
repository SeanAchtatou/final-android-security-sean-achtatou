package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.ab;
import java.util.HashSet;
import java.util.Set;

public class lu implements lv, lw {
    private final Set<Integer> a = new HashSet();
    private long b;

    public lu(@NonNull jk jkVar) {
        this.a.add(Integer.valueOf(ab.a.EVENT_TYPE_FIRST_ACTIVATION.a()));
        this.a.add(Integer.valueOf(ab.a.EVENT_TYPE_APP_UPDATE.a()));
        this.a.add(Integer.valueOf(ab.a.EVENT_TYPE_INIT.a()));
        this.a.add(Integer.valueOf(ab.a.EVENT_TYPE_IDENTITY.a()));
        jkVar.a(this);
        this.b = jkVar.a(this.a);
    }

    public boolean a() {
        return this.b > 0;
    }

    public void a(int i) {
        if (this.a.contains(Integer.valueOf(i))) {
            this.b++;
        }
    }

    public void b(int i) {
        if (this.a.contains(Integer.valueOf(i))) {
            this.b--;
        }
    }
}
