package com.yandex.metrica.impl.ob;

import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.Nullable;

public class ka implements jx {
    private final jn a;

    public ka(jn jnVar) {
        this.a = jnVar;
    }

    @Nullable
    public SQLiteDatabase a() {
        try {
            return this.a.getWritableDatabase();
        } catch (Throwable th) {
            return null;
        }
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
    }
}
