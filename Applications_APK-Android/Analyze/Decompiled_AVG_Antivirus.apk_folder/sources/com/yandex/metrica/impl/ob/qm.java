package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.qj;

public class qm extends qj {
    @NonNull
    private String a;

    @NonNull
    public String E() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void l(@NonNull String str) {
        this.a = str;
    }

    public String toString() {
        return "CoreRequestConfig{mAppDebuggable='" + this.a + '\'' + super.toString() + '}';
    }

    protected static abstract class a<T extends qm, A extends qj.a> extends qj.b<T, A> {
        private final vp c;

        protected a(@NonNull Context context, @NonNull String str) {
            this(context, str, new vp());
        }

        @VisibleForTesting
        protected a(@NonNull Context context, @NonNull String str, @NonNull vp vpVar) {
            super(context, str);
            this.c = vpVar;
        }

        @NonNull
        /* renamed from: b */
        public T c(@NonNull qj.c<A> cVar) {
            T t = (qm) super.a(cVar);
            a((qm) t);
            return t;
        }

        private void a(@NonNull qm qmVar) {
            String packageName = this.a.getPackageName();
            ApplicationInfo b = this.c.b(this.a, this.b, 0);
            if (b != null) {
                qmVar.l(a(b));
            } else if (TextUtils.equals(packageName, this.b)) {
                qmVar.l(a(this.a.getApplicationInfo()));
            } else {
                qmVar.l("0");
            }
        }

        @NonNull
        private String a(@NonNull ApplicationInfo applicationInfo) {
            return (applicationInfo.flags & 2) != 0 ? "1" : "0";
        }
    }
}
