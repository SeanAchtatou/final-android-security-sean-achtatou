package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import androidx.annotation.Nullable;
import androidx.core.view.MotionEventCompat;
import com.yandex.metrica.impl.ob.k;

public class j {
    @Nullable
    public k.a a(int i) {
        if (cg.a(28)) {
            return b(i);
        }
        return null;
    }

    @TargetApi(MotionEventCompat.AXIS_RELATIVE_Y)
    @Nullable
    private k.a b(int i) {
        if (i == 10) {
            return k.a.ACTIVE;
        }
        if (i == 20) {
            return k.a.WORKING_SET;
        }
        if (i == 30) {
            return k.a.FREQUENT;
        }
        if (i != 40) {
            return null;
        }
        return k.a.RARE;
    }

    @Nullable
    public String a(@Nullable k.a aVar) {
        if (aVar == null) {
            return null;
        }
        int i = AnonymousClass1.a[aVar.ordinal()];
        if (i == 1) {
            return "ACTIVE";
        }
        if (i == 2) {
            return "WORKING_SET";
        }
        if (i == 3) {
            return "FREQUENT";
        }
        if (i != 4) {
            return null;
        }
        return "RARE";
    }

    /* renamed from: com.yandex.metrica.impl.ob.j$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[k.a.values().length];

        static {
            try {
                a[k.a.ACTIVE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[k.a.WORKING_SET.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[k.a.FREQUENT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[k.a.RARE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }
}
