package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.i;

public class kh extends ki {
    public static final String a = null;
    @Deprecated
    public static final oj b = new oj("COLLECT_INSTALLED_APPS");
    private static final oj c = new oj("IDENTITY_SEND_TIME");
    private static final oj d = new oj("PERMISSIONS_CHECK_TIME");
    private static final oj e = new oj("USER_INFO");
    private static final oj f = new oj("PROFILE_ID");
    private static final oj g = new oj("APP_ENVIRONMENT");
    private static final oj h = new oj("APP_ENVIRONMENT_REVISION");
    private static final oj i = new oj("LAST_MIGRATION_VERSION");
    private static final oj j = new oj("LAST_APP_VERSION_WITH_FEATURES");
    private static final oj k = new oj("APPLICATION_FEATURES");
    private static final oj l = new oj("CURRENT_SESSION_ID");
    private static final oj m = new oj("ATTRIBUTION_ID");
    private static final oj n = new oj("LAST_STAT_SENDING_DISABLED_REPORTING_TIMESTAMP");
    private static final oj o = new oj("NEXT_EVENT_GLOBAL_NUMBER");
    private static final oj q = new oj("LAST_REQUEST_ID");
    private static final ke r = new ke();

    public kh(jq jqVar) {
        super(jqVar);
    }

    public int a() {
        return b(o.b(), 0);
    }

    public int a(int i2) {
        return b(r.a(i2), 0);
    }

    public long a(long j2) {
        return b(c.b(), j2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long */
    public long b() {
        return b(d.b(), 0L);
    }

    public int c() {
        return b(j.b(), -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long */
    public i.a d() {
        i.a aVar;
        synchronized (this) {
            aVar = new i.a(c(g.b(), "{}"), b(h.b(), 0L));
        }
        return aVar;
    }

    public String e() {
        return c(k.b(), "");
    }

    public String f() {
        return c(e.b(), a);
    }

    public kh b(int i2) {
        return (kh) a(o.b(), i2);
    }

    public kh a(int i2, int i3) {
        return (kh) a(r.a(i2), i3);
    }

    public kh a(i.a aVar) {
        synchronized (this) {
            b(g.b(), aVar.a);
            a(h.b(), aVar.b);
        }
        return this;
    }

    public kh b(long j2) {
        return (kh) a(c.b(), j2);
    }

    public kh c(long j2) {
        return (kh) a(d.b(), j2);
    }

    public kh a(String str) {
        return (kh) b(e.b(), str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long */
    public long g() {
        return b(i.b(), 0L);
    }

    public kh d(long j2) {
        return (kh) a(i.b(), j2);
    }

    public kh c(int i2) {
        return (kh) a(j.b(), i2);
    }

    public kh b(String str) {
        return (kh) b(k.b(), str);
    }

    public kh a(String str, String str2) {
        return (kh) b(new oj("SESSION_", str).b(), str2);
    }

    public String c(String str) {
        return c(new oj("SESSION_", str).b(), "");
    }

    @Nullable
    public String h() {
        return q(f.b());
    }

    public kh d(@Nullable String str) {
        return (kh) b(f.b(), str);
    }

    @NonNull
    public kh d(int i2) {
        return (kh) a(m.b(), i2);
    }

    public int i() {
        return b(m.b(), 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long */
    public long j() {
        return b(l.b(), -1L);
    }

    @NonNull
    public kh e(long j2) {
        return (kh) a(l.b(), j2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long */
    public long k() {
        return b(n.b(), 0L);
    }

    public kh f(long j2) {
        return (kh) a(n.b(), j2);
    }

    public int l() {
        return b(q.b(), -1);
    }

    public kh e(int i2) {
        return (kh) a(q.b(), i2);
    }
}
