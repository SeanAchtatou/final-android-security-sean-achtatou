package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter;
import com.yandex.metrica.impl.ob.cb;
import com.yandex.metrica.impl.ob.lp;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.sc;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class sd implements dn {
    @NonNull
    private final Context a;
    @NonNull
    private final df b;
    @NonNull
    private final sb c;
    @NonNull
    private volatile kp<sc> d;
    @Nullable
    private volatile cc e;
    @NonNull
    private rv f;

    public sd(@NonNull Context context, @NonNull String str, @NonNull qq.a aVar, @NonNull sb sbVar) {
        this(context, new dc(str), aVar, sbVar, lp.a.a(sc.class).a(context), new tf());
    }

    private sd(@NonNull Context context, @NonNull df dfVar, @NonNull qq.a aVar, @NonNull sb sbVar, @NonNull kp<sc> kpVar, @NonNull tf tfVar) {
        this(context, dfVar, aVar, sbVar, kpVar, kpVar.a(), tfVar);
    }

    private sd(@NonNull Context context, @NonNull df dfVar, @NonNull qq.a aVar, @NonNull sb sbVar, @NonNull kp<sc> kpVar, @NonNull sc scVar, @NonNull tf tfVar) {
        this(context, dfVar, sbVar, kpVar, scVar, tfVar, new rv(new qq.b(context, dfVar.b()), scVar, aVar));
    }

    @VisibleForTesting
    sd(@NonNull Context context, @NonNull df dfVar, @NonNull sb sbVar, @NonNull kp<sc> kpVar, @NonNull sc scVar, @NonNull tf tfVar, @NonNull rv rvVar) {
        this.a = context;
        this.b = dfVar;
        this.c = sbVar;
        this.d = kpVar;
        this.f = rvVar;
        a(scVar, tfVar);
    }

    private void a(@NonNull sc scVar, @NonNull tf tfVar) {
        boolean z;
        String str;
        sc.a a2 = scVar.a();
        GoogleAdvertisingIdGetter.c D = ((qq) this.f.d()).D();
        boolean z2 = true;
        if (D != null) {
            str = tfVar.a(D.a);
            if (!TextUtils.equals(scVar.c, str)) {
                a2 = a2.c(str);
                z = true;
            } else {
                z = false;
            }
        } else {
            a2 = a2.c("");
            str = "";
            z = true;
        }
        if (!b(scVar.a) || !c(scVar.b)) {
            if (!b(scVar.a)) {
                a2 = a2.a(tfVar.a());
            }
            if (!c(scVar.b)) {
                a2 = a2.b(str).d("");
            }
        } else {
            z2 = z;
        }
        if (z2) {
            sc a3 = a2.a();
            d(a3);
            b(a3);
            return;
        }
        b(scVar);
    }

    @NonNull
    public df b() {
        return this.b;
    }

    @Nullable
    public synchronized cc a() {
        if (!c()) {
            return null;
        }
        if (this.e == null) {
            this.e = new cc(this, d());
        }
        return this.e;
    }

    public synchronized boolean a(@Nullable List<String> list) {
        boolean z = false;
        if (list == null) {
            return false;
        }
        sc b2 = this.f.b();
        for (String next : list) {
            if (next.equals(IIdentifierCallback.YANDEX_MOBILE_METRICA_UUID)) {
                z |= !b(b2.a);
            } else if (next.equals(IIdentifierCallback.YANDEX_MOBILE_METRICA_DEVICE_ID)) {
                z |= !c(b2.b);
            } else if (next.equals(IIdentifierCallback.APP_METRICA_DEVICE_ID_HASH)) {
                z |= !d(b2.d);
            } else if (next.equals(IIdentifierCallback.YANDEX_MOBILE_METRICA_GET_AD_URL)) {
                z |= !e(b2.f);
            } else if (next.equals(IIdentifierCallback.YANDEX_MOBILE_METRICA_REPORT_AD_URL)) {
                z |= !f(b2.g);
            } else {
                z = true;
            }
        }
        return z;
    }

    public synchronized boolean c() {
        boolean z;
        z = e().E;
        if (!z) {
            z = !a(ua.a(Long.valueOf(e().t), 0));
            String a2 = tu.a(((qq) this.f.d()).G());
            if (!z && !TextUtils.isEmpty(a2) && !a2.equals(e().n)) {
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0029, code lost:
        return false;
     */
    @androidx.annotation.VisibleForTesting
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean a(long r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            com.yandex.metrica.impl.ob.rv r0 = r4.f     // Catch:{ all -> 0x002a }
            com.yandex.metrica.impl.ob.qj r0 = r0.d()     // Catch:{ all -> 0x002a }
            com.yandex.metrica.impl.ob.qq r0 = (com.yandex.metrica.impl.ob.qq) r0     // Catch:{ all -> 0x002a }
            boolean r0 = r0.f()     // Catch:{ all -> 0x002a }
            r1 = 0
            if (r0 != 0) goto L_0x0012
            monitor-exit(r4)
            return r1
        L_0x0012:
            long r2 = com.yandex.metrica.impl.ob.ty.b()     // Catch:{ all -> 0x002a }
            long r2 = r2 - r5
            r5 = 86400(0x15180, double:4.26873E-319)
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 > 0) goto L_0x0028
            r5 = 0
            int r0 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0025
            goto L_0x0028
        L_0x0025:
            r5 = 1
            monitor-exit(r4)
            return r5
        L_0x0028:
            monitor-exit(r4)
            return r1
        L_0x002a:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.sd.a(long):boolean");
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(@NonNull sc scVar) {
        this.d.a(scVar);
    }

    @NonNull
    public qq d() {
        return (qq) this.f.d();
    }

    private synchronized void f() {
        this.e = null;
    }

    public void a(@NonNull cb.b bVar, @NonNull qq qqVar, @Nullable Map<String, List<String>> map) {
        sc a2;
        synchronized (this) {
            Long valueOf = Long.valueOf(ua.a(cb.a(map), 0));
            a(bVar.t(), valueOf);
            a2 = a(bVar, qqVar, valueOf);
            new ls().a(this.a, new lq(a2.b, a2.d), new nq(nn.b().a(a2).a()));
            f();
            d(a2);
        }
        c(a2);
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    @NonNull
    public sc a(@NonNull cb.b bVar, @NonNull qq qqVar, @Nullable Long l) {
        String a2 = tu.a(qqVar.G());
        String a3 = a(bVar.k(), e().m);
        String str = e().b;
        if (TextUtils.isEmpty(str)) {
            str = bVar.i();
        }
        return new sc.a(bVar.a()).a(ty.b()).b(str).c(e().c).d(bVar.j()).a(e().a).e(bVar.e()).c(bVar.c()).d(qqVar.F()).a(bVar.f()).b(bVar.h()).f(bVar.g()).g(bVar.d()).e(bVar.w()).h(a3).i(a2).b(a(qqVar.G(), a3)).a(bVar.n()).a(bVar.r()).a(bVar.s()).f(bVar.u()).k(bVar.v()).a(bVar.o()).g(bVar.q()).a(bVar.p()).a(bVar.x()).a(true).b(ua.a(l, ty.b() * 1000)).c(((qq) this.f.d()).b(l.longValue())).c(false).j(e().s).a();
    }

    private void c(@NonNull sc scVar) {
        this.c.a(this.b.b(), scVar);
        b(scVar);
    }

    /* access modifiers changed from: package-private */
    public void b(sc scVar) {
        cn.a().b(new cx(this.b.b(), scVar));
        if (!TextUtils.isEmpty(scVar.a)) {
            cn.a().b(new cy(scVar.a, this.b.b()));
        }
        if (!TextUtils.isEmpty(scVar.b)) {
            cn.a().b(new cu(scVar.b));
        }
        if (scVar.r == null) {
            cn.a().a((Class<? extends cp>) cw.class);
        } else {
            cn.a().b(new cw(scVar.r));
        }
    }

    private void a(@Nullable Long l, @NonNull Long l2) {
        tt.a().a(l2.longValue(), l);
    }

    private void d(@NonNull sc scVar) {
        this.f.a(scVar);
        a(scVar);
        e(scVar);
    }

    @Nullable
    private static String a(@Nullable String str, @Nullable String str2) {
        if (tu.b(str)) {
            return str;
        }
        if (tu.b(str2)) {
            return str2;
        }
        return null;
    }

    private boolean a(Map<String, String> map, @Nullable String str) {
        return tu.a(str).equals(map);
    }

    @Deprecated
    private void e(sc scVar) {
        if (!TextUtils.isEmpty(scVar.b)) {
            Intent intent = new Intent("com.yandex.metrica.intent.action.SYNC");
            intent.putExtra("CAUSE", "CAUSE_DEVICE_ID");
            intent.putExtra("SYNC_TO_PKG", this.b.b());
            intent.putExtra("SYNC_DATA", scVar.b);
            intent.putExtra("SYNC_DATA_2", scVar.a);
            this.a.sendBroadcast(intent);
        }
    }

    @NonNull
    public sc e() {
        return this.f.b();
    }

    public void a(@NonNull rw rwVar) {
        f();
        this.c.a(b().b(), rwVar);
    }

    public synchronized void a(@NonNull qq.a aVar) {
        this.f.a(aVar);
        a((qq) this.f.d());
    }

    private void a(qq qqVar) {
        if (qqVar.J()) {
            boolean z = false;
            List<String> I = qqVar.I();
            sc.a aVar = null;
            if (cg.a((Collection) I) && !cg.a((Collection) qqVar.F())) {
                aVar = e().a().d((List<String>) null);
                z = true;
            }
            if (!cg.a((Collection) I) && !cg.a(I, qqVar.F())) {
                aVar = e().a().d(I);
                z = true;
            }
            if (z) {
                d(aVar.a());
            }
        }
    }

    public synchronized void a(String str) {
        d(e().a().j(str).a());
    }

    private boolean b(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean c(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean d(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean e(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean f(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }
}
