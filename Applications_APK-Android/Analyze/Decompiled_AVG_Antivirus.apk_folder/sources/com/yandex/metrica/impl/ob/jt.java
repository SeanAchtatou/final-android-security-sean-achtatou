package com.yandex.metrica.impl.ob;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;

public class jt {
    @NonNull
    private final jx a;
    @NonNull
    private String b;

    public jt(jn jnVar, @NonNull String str) {
        this(new ka(jnVar), str);
    }

    @VisibleForTesting
    jt(@NonNull jx jxVar, @NonNull String str) {
        this.a = jxVar;
        this.b = str;
    }

    @Nullable
    public List<nt> a() {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        try {
            sQLiteDatabase = this.a.a();
            try {
                cursor = sQLiteDatabase.query(this.b, null, null, null, null, null, null);
                if (cursor != null) {
                    try {
                        if (cursor.moveToFirst()) {
                            ArrayList arrayList = new ArrayList();
                            do {
                                arrayList.add(new nt(cursor.getString(cursor.getColumnIndex("name")), cursor.getLong(cursor.getColumnIndex("granted")) == 1));
                            } while (cursor.moveToNext());
                            this.a.a(sQLiteDatabase);
                            cg.a(cursor);
                            return arrayList;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        this.a.a(sQLiteDatabase);
                        cg.a(cursor);
                        throw th;
                    }
                }
                this.a.a(sQLiteDatabase);
                cg.a(cursor);
                return null;
            } catch (Throwable th3) {
                Throwable th4 = th3;
                cursor = null;
                th = th4;
                this.a.a(sQLiteDatabase);
                cg.a(cursor);
                throw th;
            }
        } catch (Throwable th5) {
            cursor = null;
            th = th5;
            sQLiteDatabase = null;
            this.a.a(sQLiteDatabase);
            cg.a(cursor);
            throw th;
        }
    }

    public void b() {
        SQLiteDatabase sQLiteDatabase = null;
        try {
            sQLiteDatabase = this.a.a();
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS permissions");
        } catch (Throwable th) {
            this.a.a(sQLiteDatabase);
            throw th;
        }
        this.a.a(sQLiteDatabase);
    }
}
