package com.yandex.metrica.impl.ob;

import java.io.IOException;

public interface po {

    public static final class a extends e {
        public C0013a[] b;
        public String c;
        public long d;
        public boolean e;
        public boolean f;

        /* renamed from: com.yandex.metrica.impl.ob.po$a$a  reason: collision with other inner class name */
        public static final class C0013a extends e {
            private static volatile C0013a[] d;
            public String b;
            public String[] c;

            public static C0013a[] d() {
                if (d == null) {
                    synchronized (c.a) {
                        if (d == null) {
                            d = new C0013a[0];
                        }
                    }
                }
                return d;
            }

            public C0013a() {
                e();
            }

            public C0013a e() {
                this.b = "";
                this.c = g.f;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                String[] strArr = this.c;
                if (strArr != null && strArr.length > 0) {
                    int i = 0;
                    while (true) {
                        String[] strArr2 = this.c;
                        if (i >= strArr2.length) {
                            break;
                        }
                        String str = strArr2[i];
                        if (str != null) {
                            bVar.a(2, str);
                        }
                        i++;
                    }
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.b(1, this.b);
                String[] strArr = this.c;
                if (strArr == null || strArr.length <= 0) {
                    return c2;
                }
                int i = 0;
                int i2 = 0;
                int i3 = 0;
                while (true) {
                    String[] strArr2 = this.c;
                    if (i >= strArr2.length) {
                        return c2 + i2 + (i3 * 1);
                    }
                    String str = strArr2[i];
                    if (str != null) {
                        i3++;
                        i2 += b.b(str);
                    }
                    i++;
                }
            }

            /* renamed from: b */
            public C0013a a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        this.b = aVar.i();
                    } else if (a == 18) {
                        int b2 = g.b(aVar, 18);
                        String[] strArr = this.c;
                        int length = strArr == null ? 0 : strArr.length;
                        String[] strArr2 = new String[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.c, 0, strArr2, 0, length);
                        }
                        while (length < strArr2.length - 1) {
                            strArr2[length] = aVar.i();
                            aVar.a();
                            length++;
                        }
                        strArr2[length] = aVar.i();
                        this.c = strArr2;
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public a() {
            d();
        }

        public a d() {
            this.b = C0013a.d();
            this.c = "";
            this.d = 0;
            this.e = false;
            this.f = false;
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            C0013a[] aVarArr = this.b;
            if (aVarArr != null && aVarArr.length > 0) {
                int i = 0;
                while (true) {
                    C0013a[] aVarArr2 = this.b;
                    if (i >= aVarArr2.length) {
                        break;
                    }
                    C0013a aVar = aVarArr2[i];
                    if (aVar != null) {
                        bVar.a(1, aVar);
                    }
                    i++;
                }
            }
            bVar.a(2, this.c);
            bVar.b(3, this.d);
            bVar.a(4, this.e);
            bVar.a(5, this.f);
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            C0013a[] aVarArr = this.b;
            if (aVarArr != null && aVarArr.length > 0) {
                int i = 0;
                while (true) {
                    C0013a[] aVarArr2 = this.b;
                    if (i >= aVarArr2.length) {
                        break;
                    }
                    C0013a aVar = aVarArr2[i];
                    if (aVar != null) {
                        c2 += b.b(1, aVar);
                    }
                    i++;
                }
            }
            return c2 + b.b(2, this.c) + b.e(3, this.d) + b.b(4, this.e) + b.b(5, this.f);
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    int b2 = g.b(aVar, 10);
                    C0013a[] aVarArr = this.b;
                    int length = aVarArr == null ? 0 : aVarArr.length;
                    C0013a[] aVarArr2 = new C0013a[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, aVarArr2, 0, length);
                    }
                    while (length < aVarArr2.length - 1) {
                        aVarArr2[length] = new C0013a();
                        aVar.a(aVarArr2[length]);
                        aVar.a();
                        length++;
                    }
                    aVarArr2[length] = new C0013a();
                    aVar.a(aVarArr2[length]);
                    this.b = aVarArr2;
                } else if (a == 18) {
                    this.c = aVar.i();
                } else if (a == 24) {
                    this.d = aVar.f();
                } else if (a == 32) {
                    this.e = aVar.h();
                } else if (a == 40) {
                    this.f = aVar.h();
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }

        public static a a(byte[] bArr) throws d {
            return (a) e.a(new a(), bArr);
        }
    }
}
