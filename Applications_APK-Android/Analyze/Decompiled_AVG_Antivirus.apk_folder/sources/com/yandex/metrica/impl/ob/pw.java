package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.g;
import com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter;
import com.yandex.metrica.impl.interact.CellularNetworkInfo;
import com.yandex.metrica.impl.interact.DeviceInfo;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public class pw {
    /* access modifiers changed from: private */
    @NonNull
    public final px a;
    @NonNull
    private final uv b;
    @NonNull
    private final pr c;
    @NonNull
    private final vx<Context> d;
    @NonNull
    private final vx<String> e;
    @NonNull
    private final a f;

    static class a {
        a() {
        }

        public mk a(@NonNull Context context, @Nullable LocationManager locationManager) {
            return new mk(context, locationManager, new nq(new nm()));
        }
    }

    public pw(@NonNull uv uvVar) {
        this(uvVar, new px());
    }

    public pw(@NonNull uv uvVar, @NonNull px pxVar) {
        this(uvVar, pxVar, new pr(pxVar), new vt(new vs("Context")), new vt(new vs("Event name")), new a());
    }

    public pw(@NonNull uv uvVar, @NonNull px pxVar, @NonNull pr prVar, @NonNull vx<Context> vxVar, @NonNull vx<String> vxVar2, @NonNull a aVar) {
        this.a = pxVar;
        this.b = uvVar;
        this.c = prVar;
        this.d = vxVar;
        this.e = vxVar2;
        this.f = aVar;
    }

    @Deprecated
    public void a(final IIdentifierCallback iIdentifierCallback, @NonNull final List<String> list) {
        this.b.a(new tr() {
            public void a() throws Exception {
                if (pw.this.a.d()) {
                    pw.this.a.a().a(iIdentifierCallback, list);
                }
            }
        });
    }

    public void a(@NonNull final Context context, @NonNull final IIdentifierCallback iIdentifierCallback, @NonNull final List<String> list) {
        this.d.a(context);
        this.b.a(new tr() {
            public void a() throws Exception {
                pw.this.a.a(context).a(iIdentifierCallback, list);
            }
        });
    }

    public boolean a() {
        return this.a.c();
    }

    @Nullable
    public Future<String> b() {
        return this.b.a(new tq<String>() {
            /* renamed from: a */
            public String b() {
                return GoogleAdvertisingIdGetter.b().c();
            }
        });
    }

    @Nullable
    public Future<Boolean> c() {
        return this.b.a(new tq<Boolean>() {
            /* renamed from: a */
            public Boolean b() {
                return GoogleAdvertisingIdGetter.b().d();
            }
        });
    }

    @NonNull
    public DeviceInfo a(Context context) {
        this.d.a(context);
        return DeviceInfo.getInstance(context);
    }

    @NonNull
    public String b(Context context) {
        this.d.a(context);
        return new CellularNetworkInfo(context).getCelluralInfo();
    }

    @Nullable
    public Integer c(Context context) {
        this.d.a(context);
        return bj.c(context);
    }

    @Deprecated
    @Nullable
    public String d() {
        if (this.a.d()) {
            return this.a.a().k();
        }
        return null;
    }

    @Nullable
    public String d(@NonNull Context context) {
        this.d.a(context);
        return this.a.a(context).k();
    }

    @Nullable
    public String e(@NonNull Context context) {
        this.d.a(context);
        return this.a.a(context).j();
    }

    @NonNull
    public String f(@NonNull Context context) {
        this.d.a(context);
        return context.getPackageName();
    }

    public void a(int i, @NonNull String str, @Nullable String str2, @Nullable Map<String, String> map) {
        this.c.a();
        this.e.a(str);
        final int i2 = i;
        final String str3 = str;
        final String str4 = str2;
        final Map<String, String> map2 = map;
        this.b.a(new tr() {
            public void a() throws Exception {
                pw.this.a.e().a(i2, str3, str4, map2);
            }
        });
    }

    public void e() {
        this.c.a();
        this.b.a(new tr() {
            public void a() throws Exception {
                pw.this.a.e().sendEventsBuffer();
            }
        });
    }

    @NonNull
    public String a(@Nullable String str) {
        return bx.a(str);
    }

    @NonNull
    public String a(int i) {
        return bm.a(i);
    }

    @NonNull
    public YandexMetricaConfig a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull String str) {
        return g.b(yandexMetricaConfig).a(Collections.singletonList(str)).b();
    }

    @NonNull
    public YandexMetricaConfig a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull List<String> list) {
        return g.b(yandexMetricaConfig).a(list).b();
    }

    public void a(@NonNull Context context, @NonNull Object obj) {
    }

    public void b(@NonNull Context context, @NonNull Object obj) {
    }

    @Nullable
    public Location g(@NonNull Context context) {
        LocationManager locationManager;
        this.d.a(context);
        try {
            locationManager = (LocationManager) context.getSystemService("location");
        } catch (Throwable th) {
            locationManager = null;
        }
        return this.f.a(context, locationManager).a();
    }
}
