package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class hz {
    @NonNull
    private Context a;
    @NonNull
    private hy b;
    @NonNull
    private id c;

    public hz(@NonNull Context context, @NonNull sc scVar) {
        this(context, new hy(context, scVar), new id(context));
    }

    @VisibleForTesting
    hz(@NonNull Context context, @NonNull hy hyVar, @NonNull id idVar) {
        this.a = context;
        this.b = hyVar;
        this.c = idVar;
    }

    public void a() {
        this.a.getPackageName();
        this.c.a().a(this.b.a());
    }
}
