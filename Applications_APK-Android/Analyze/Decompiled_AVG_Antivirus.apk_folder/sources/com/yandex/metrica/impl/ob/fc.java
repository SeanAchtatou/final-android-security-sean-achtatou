package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.ab;
import java.util.ArrayList;

public class fc extends fh<fj> {
    private final gn a;

    public fc(dd ddVar) {
        this.a = new gn(ddVar);
    }

    public fe<fj> a(int i) {
        ArrayList arrayList = new ArrayList();
        int i2 = AnonymousClass1.a[ab.a.a(i).ordinal()];
        if (i2 == 1) {
            arrayList.add(this.a.a());
        } else if (i2 == 2) {
            arrayList.add(this.a.b());
        } else if (i2 == 3) {
            arrayList.add(this.a.c());
        } else if (i2 == 4) {
            arrayList.add(this.a.d());
        }
        return new fd(arrayList);
    }

    /* renamed from: com.yandex.metrica.impl.ob.fc$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[ab.a.values().length];

        static {
            try {
                a[ab.a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[ab.a.EVENT_TYPE_STARTUP.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[ab.a.EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[ab.a.EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }
}
