package com.yandex.metrica.impl.ob;

public enum hw {
    FOREGROUND(0),
    BACKGROUND(1);
    
    private final int c;

    private hw(int i) {
        this.c = i;
    }

    public int a() {
        return this.c;
    }

    public static hw a(Integer num) {
        hw hwVar = FOREGROUND;
        if (num == null) {
            return hwVar;
        }
        int intValue = num.intValue();
        if (intValue == 0) {
            return FOREGROUND;
        }
        if (intValue != 1) {
            return hwVar;
        }
        return BACKGROUND;
    }
}
