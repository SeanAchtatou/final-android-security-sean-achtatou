package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public class cl implements ub<Thread, StackTraceElement[], ix> {
    @NonNull
    public ix a(@NonNull Thread thread, @Nullable StackTraceElement[] stackTraceElementArr) {
        List list;
        String name = thread.getName();
        int priority = thread.getPriority();
        long id = thread.getId();
        String a = a(thread);
        Integer valueOf = Integer.valueOf(thread.getState().ordinal());
        if (stackTraceElementArr == null) {
            list = null;
        } else {
            list = Arrays.asList(stackTraceElementArr);
        }
        return new ix(name, priority, id, a, valueOf, list);
    }

    @NonNull
    static String a(@NonNull Thread thread) {
        ThreadGroup threadGroup = thread.getThreadGroup();
        return threadGroup != null ? threadGroup.getName() : "";
    }
}
