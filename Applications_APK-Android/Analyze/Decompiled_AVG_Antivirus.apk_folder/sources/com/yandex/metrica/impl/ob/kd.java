package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.yandex.metrica.impl.ob.ab;
import com.yandex.metrica.impl.ob.kb;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class kd {

    public static class v extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("ALTER TABLE " + "sessions" + " ADD COLUMN " + "wifi_network_info" + " TEXT DEFAULT ''");
        }
    }

    public static class z extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("ALTER TABLE " + "sessions" + " ADD COLUMN " + "report_request_parameters" + " TEXT DEFAULT ''");
        }
    }

    public static class o extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            Cursor cursor;
            SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
            sQLiteDatabase2.execSQL("CREATE TABLE IF NOT EXISTS " + "sessions_BACKUP" + " (" + "id" + " INTEGER" + "," + "start_time" + " INTEGER" + "," + UrlManager.Parameter.CONNECTION_TYPE + " INTEGER" + "," + "network_type" + " TEXT" + "," + "country_code" + " INTEGER" + "," + "operator_id" + " INTEGER" + "," + "lac" + " INTEGER" + "," + "report_request_parameters" + " TEXT" + " );");
            StringBuilder sb = new StringBuilder();
            sb.append("id");
            sb.append(",");
            sb.append("start_time");
            sb.append(",");
            sb.append(UrlManager.Parameter.CONNECTION_TYPE);
            sb.append(",");
            sb.append("network_type");
            sb.append(",");
            sb.append("country_code");
            sb.append(",");
            sb.append("operator_id");
            sb.append(",");
            sb.append("lac");
            sb.append(",");
            sb.append("report_request_parameters");
            StringBuilder sb2 = new StringBuilder();
            sb2.append("INSERT INTO ");
            sb2.append("sessions_BACKUP");
            sb2.append(" SELECT ");
            sb2.append((CharSequence) sb);
            sb2.append(" FROM ");
            sb2.append("sessions");
            sb2.append(";");
            sQLiteDatabase2.execSQL(sb2.toString());
            sQLiteDatabase2.execSQL("DROP TABLE sessions;");
            sQLiteDatabase2.execSQL(kb.g.b);
            try {
                cursor = sQLiteDatabase2.rawQuery("SELECT * FROM sessions_BACKUP", null);
                while (cursor.moveToNext()) {
                    try {
                        ContentValues contentValues = new ContentValues();
                        DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
                        ArrayList<String> arrayList = new ArrayList<>();
                        arrayList.add("id");
                        arrayList.add("start_time");
                        arrayList.add("report_request_parameters");
                        ContentValues contentValues2 = new ContentValues(contentValues);
                        for (Map.Entry next : contentValues.valueSet()) {
                            if (!arrayList.contains(next.getKey())) {
                                contentValues2.remove((String) next.getKey());
                            }
                        }
                        for (String remove : arrayList) {
                            contentValues.remove(remove);
                        }
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("conn_type", contentValues.getAsInteger(UrlManager.Parameter.CONNECTION_TYPE));
                        jSONObject.putOpt("net_type", contentValues.get("network_type"));
                        jSONObject.putOpt("operator_id", contentValues.get("operator_id"));
                        jSONObject.putOpt("lac", contentValues.get("lac"));
                        jSONObject.putOpt("country_code", contentValues.get("country_code"));
                        contentValues2.put("network_info", jSONObject.toString());
                        sQLiteDatabase2.insertOrThrow("sessions", null, contentValues2);
                    } catch (Throwable th) {
                        th = th;
                        cg.a(cursor);
                        throw th;
                    }
                }
                cg.a(cursor);
                sQLiteDatabase2.execSQL("DROP TABLE sessions_BACKUP;");
                sQLiteDatabase2.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "wifi_network_info" + " TEXT DEFAULT ''");
                sQLiteDatabase2.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "cell_info" + " TEXT DEFAULT ''");
                sQLiteDatabase2.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "location_info" + " TEXT DEFAULT ''");
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
                cg.a(cursor);
                throw th;
            }
        }
    }

    public static class p extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "environment" + " TEXT ");
            sQLiteDatabase.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "user_info" + " TEXT ");
            sQLiteDatabase.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "session_type" + " INTEGER DEFAULT " + hw.FOREGROUND.a());
            sQLiteDatabase.execSQL("UPDATE " + "reports" + " SET " + "session_type" + " = " + hw.BACKGROUND.a() + " WHERE " + "session_id" + " = " + -2L);
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append("sessions");
            sb.append(" ADD COLUMN ");
            sb.append("server_time_offset");
            sb.append(" INTEGER ");
            sQLiteDatabase.execSQL(sb.toString());
            sQLiteDatabase.execSQL("ALTER TABLE " + "sessions" + " ADD COLUMN " + "type" + " INTEGER DEFAULT " + hw.FOREGROUND.a());
            sQLiteDatabase.execSQL("UPDATE " + "sessions" + " SET " + "type" + " = " + hw.BACKGROUND.a() + " WHERE " + "id" + " = " + -2L);
        }
    }

    public static class g extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS api_level_info (API_LEVEL INT )");
            sQLiteDatabase.insert("api_level_info", "API_LEVEL", kb.e.a());
        }
    }

    public static class h extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS api_level_info");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS device_id_info");
        }
    }

    public static class i extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS startup (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
        }
    }

    public static class j extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS GeoLocationInfo");
        }
    }

    public static class l extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(kb.a.b.a);
        }
    }

    public static class m extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(kb.a.C0003a.a);
        }
    }

    public static class n extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    public static class k extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS permissions (name TEXT PRIMARY KEY,granted INTEGER)");
        }
    }

    public static class q extends kc {
        private static final String a = ("CREATE TABLE IF NOT EXISTS reports (id INTEGER PRIMARY KEY,name TEXT,value TEXT,number INTEGER,type INTEGER,time INTEGER,session_id TEXT,wifi_network_info TEXT DEFAULT '',cell_info TEXT DEFAULT '',location_info TEXT DEFAULT '',error_environment TEXT,user_info TEXT,session_type INTEGER DEFAULT " + hw.FOREGROUND.a() + "," + "app_environment" + " TEXT DEFAULT '" + "{}" + "'," + "app_environment_revision" + " INTEGER DEFAULT " + 0L + " )");

        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            Cursor cursor;
            sQLiteDatabase.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "app_environment" + " TEXT DEFAULT '{}'");
            sQLiteDatabase.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "app_environment_revision" + " INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE reports RENAME TO reports_backup");
            sQLiteDatabase.execSQL(a);
            try {
                cursor = sQLiteDatabase.rawQuery("SELECT * FROM reports_backup", null);
                while (cursor.moveToNext()) {
                    try {
                        ContentValues contentValues = new ContentValues();
                        DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
                        String asString = contentValues.getAsString("environment");
                        contentValues.remove("environment");
                        contentValues.put("error_environment", asString);
                        sQLiteDatabase.insert("reports", null, contentValues);
                    } catch (Throwable th) {
                        th = th;
                        cg.a(cursor);
                        throw th;
                    }
                }
                cg.a(cursor);
                sQLiteDatabase.execSQL("DROP TABLE reports_backup");
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
                cg.a(cursor);
                throw th;
            }
        }
    }

    public static class r extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "truncated" + " INTEGER DEFAULT 0");
        }
    }

    public static class s extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + UrlManager.Parameter.CONNECTION_TYPE + " INTEGER DEFAULT 2");
            sQLiteDatabase.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "cellular_connection_type" + " TEXT ");
        }
    }

    public static class t extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "custom_type" + " INTEGER DEFAULT 0");
        }
    }

    public static class u extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("ALTER TABLE " + "reports" + " ADD COLUMN " + "wifi_access_point" + " TEXT ");
        }
    }

    public static class w extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL("ALTER TABLE " + "sessions" + " ADD COLUMN " + "obtained_before_first_sync" + " INTEGER DEFAULT 0");
        }
    }

    public static class x extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", "reports", "encrypting_mode", Integer.valueOf(un.NONE.a())));
            sQLiteDatabase.execSQL(String.format(Locale.US, "UPDATE %s SET %s = %d where %s=%d", "reports", "encrypting_mode", Integer.valueOf(un.EXTERNALLY_ENCRYPTED_EVENT_CRYPTER.a()), "type", Integer.valueOf(ab.a.EVENT_TYPE_IDENTITY.a())));
            sQLiteDatabase.execSQL("ALTER TABLE reports ADD COLUMN profile_id TEXT ");
        }
    }

    public static class y extends kc {
        public void a(@NonNull SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", "reports", "first_occurrence_status", Integer.valueOf(ad.UNKNOWN.d)));
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    public static class aa extends kc {
        public void a(@NonNull SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.delete("reports", "session_id = ?", new String[]{String.valueOf(-2L)});
            sQLiteDatabase.delete("sessions", "id = ?", new String[]{String.valueOf(-2L)});
        }
    }

    public static class ab extends kc {
        public void a(@NonNull SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", "reports", "global_number", 0));
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", "reports", "number_of_type", 0));
        }
    }

    public static class c extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL(kb.f.b);
            sQLiteDatabase.execSQL(kb.g.b);
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    public static class d extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS reports");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS sessions");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preferences");
        }
    }

    public static class e extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS startup (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL(kb.a.b.a);
            sQLiteDatabase.execSQL(kb.a.C0003a.a);
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    public static class f extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS device_id_info");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS api_level_info");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preferences");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS startup");
            sQLiteDatabase.execSQL(kb.a.b.b);
            sQLiteDatabase.execSQL(kb.a.C0003a.b);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS permissions");
        }
    }

    public static class a extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
        }
    }

    public static class b extends kc {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preferences");
        }
    }
}
