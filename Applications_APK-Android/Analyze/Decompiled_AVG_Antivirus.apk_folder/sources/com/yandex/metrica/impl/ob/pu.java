package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.c;
import com.yandex.metrica.e;
import com.yandex.metrica.impl.ob.qa;

public class pu<B extends qa> implements IReporter {
    @NonNull
    B a;
    @NonNull
    final uv b;
    @NonNull
    private px c = new px();
    @NonNull
    private final Context d;
    @NonNull
    private final e e;

    pu(@NonNull uv uvVar, @NonNull Context context, @NonNull String str, @NonNull B b2) {
        this.b = uvVar;
        this.d = context;
        this.e = e.a(str).a();
        this.a = b2;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final c a() {
        return this.c.a(this.d).b(this.e);
    }

    public void a(@NonNull final String str) {
        this.b.a(new Runnable() {
            public void run() {
                pu.this.b(e.a(str).a());
            }
        });
    }

    public void a(@NonNull final ReporterConfig reporterConfig) {
        this.b.a(new Runnable() {
            public void run() {
                pu.this.b(e.a(reporterConfig));
            }
        });
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void b(@NonNull e eVar) {
        this.c.a(this.d).a(eVar);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: B
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void reportEvent(@androidx.annotation.NonNull final java.lang.String r3) {
        /*
            r2 = this;
            B r0 = r2.a
            r0.reportEvent(r3)
            com.yandex.metrica.impl.ob.uv r0 = r2.b
            com.yandex.metrica.impl.ob.pu$8 r1 = new com.yandex.metrica.impl.ob.pu$8
            r1.<init>(r3)
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pu.reportEvent(java.lang.String):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: B
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void reportEvent(@androidx.annotation.NonNull final java.lang.String r3, @androidx.annotation.Nullable final java.lang.String r4) {
        /*
            r2 = this;
            B r0 = r2.a
            r0.reportEvent(r3, r4)
            com.yandex.metrica.impl.ob.uv r0 = r2.b
            com.yandex.metrica.impl.ob.pu$9 r1 = new com.yandex.metrica.impl.ob.pu$9
            r1.<init>(r3, r4)
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pu.reportEvent(java.lang.String, java.lang.String):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: B
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void reportEvent(@androidx.annotation.NonNull final java.lang.String r4, @androidx.annotation.Nullable java.util.Map<java.lang.String, java.lang.Object> r5) {
        /*
            r3 = this;
            B r0 = r3.a
            r0.reportEvent(r4, r5)
            if (r5 != 0) goto L_0x0009
            r0 = 0
            goto L_0x0012
        L_0x0009:
            java.util.ArrayList r0 = new java.util.ArrayList
            java.util.Set r1 = r5.entrySet()
            r0.<init>(r1)
        L_0x0012:
            com.yandex.metrica.impl.ob.uv r1 = r3.b
            com.yandex.metrica.impl.ob.pu$10 r2 = new com.yandex.metrica.impl.ob.pu$10
            r2.<init>(r0, r4)
            r1.a(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pu.reportEvent(java.lang.String, java.util.Map):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: B
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void reportError(@androidx.annotation.NonNull final java.lang.String r3, @androidx.annotation.Nullable final java.lang.Throwable r4) {
        /*
            r2 = this;
            B r0 = r2.a
            r0.reportError(r3, r4)
            com.yandex.metrica.impl.ob.uv r0 = r2.b
            com.yandex.metrica.impl.ob.pu$11 r1 = new com.yandex.metrica.impl.ob.pu$11
            r1.<init>(r3, r4)
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pu.reportError(java.lang.String, java.lang.Throwable):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: B
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void reportUnhandledException(@androidx.annotation.NonNull final java.lang.Throwable r3) {
        /*
            r2 = this;
            B r0 = r2.a
            r0.reportUnhandledException(r3)
            com.yandex.metrica.impl.ob.uv r0 = r2.b
            com.yandex.metrica.impl.ob.pu$12 r1 = new com.yandex.metrica.impl.ob.pu$12
            r1.<init>(r3)
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pu.reportUnhandledException(java.lang.Throwable):void");
    }

    public void resumeSession() {
        this.a.resumeSession();
        this.b.a(new Runnable() {
            public void run() {
                pu.this.a().resumeSession();
            }
        });
    }

    public void pauseSession() {
        this.a.pauseSession();
        this.b.a(new Runnable() {
            public void run() {
                pu.this.a().pauseSession();
            }
        });
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: B
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void setUserProfileID(@androidx.annotation.Nullable final java.lang.String r3) {
        /*
            r2 = this;
            B r0 = r2.a
            r0.setUserProfileID(r3)
            com.yandex.metrica.impl.ob.uv r0 = r2.b
            com.yandex.metrica.impl.ob.pu$2 r1 = new com.yandex.metrica.impl.ob.pu$2
            r1.<init>(r3)
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pu.setUserProfileID(java.lang.String):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: B
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void reportUserProfile(@androidx.annotation.NonNull final com.yandex.metrica.profile.UserProfile r3) {
        /*
            r2 = this;
            B r0 = r2.a
            r0.reportUserProfile(r3)
            com.yandex.metrica.impl.ob.uv r0 = r2.b
            com.yandex.metrica.impl.ob.pu$3 r1 = new com.yandex.metrica.impl.ob.pu$3
            r1.<init>(r3)
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pu.reportUserProfile(com.yandex.metrica.profile.UserProfile):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: B
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void reportRevenue(@androidx.annotation.NonNull final com.yandex.metrica.Revenue r3) {
        /*
            r2 = this;
            B r0 = r2.a
            r0.reportRevenue(r3)
            com.yandex.metrica.impl.ob.uv r0 = r2.b
            com.yandex.metrica.impl.ob.pu$4 r1 = new com.yandex.metrica.impl.ob.pu$4
            r1.<init>(r3)
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pu.reportRevenue(com.yandex.metrica.Revenue):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: B
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void setStatisticsSending(final boolean r3) {
        /*
            r2 = this;
            B r0 = r2.a
            r0.setStatisticsSending(r3)
            com.yandex.metrica.impl.ob.uv r0 = r2.b
            com.yandex.metrica.impl.ob.pu$5 r1 = new com.yandex.metrica.impl.ob.pu$5
            r1.<init>(r3)
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.pu.setStatisticsSending(boolean):void");
    }

    public void sendEventsBuffer() {
        this.a.sendEventsBuffer();
        this.b.a(new Runnable() {
            public void run() {
                pu.this.a().sendEventsBuffer();
            }
        });
    }
}
