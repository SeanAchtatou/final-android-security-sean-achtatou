package com.yandex.metrica.impl.ob;

public class rs {
    public final long a;
    public final long b;
    public final long c;
    public final long d;

    public rs(long j, long j2, long j3, long j4) {
        this.a = j;
        this.b = j2;
        this.c = j3;
        this.d = j4;
    }

    public String toString() {
        return "SdkFingerprintingConfig{minCollectingInterval=" + this.a + ", minFirstCollectingDelay=" + this.b + ", minCollectingDelayAfterLaunch=" + this.c + ", minRequestRetryInterval=" + this.d + '}';
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        rs rsVar = (rs) o;
        if (this.a == rsVar.a && this.b == rsVar.b && this.c == rsVar.c && this.d == rsVar.d) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long j = this.a;
        long j2 = this.b;
        long j3 = this.c;
        long j4 = this.d;
        return (((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)));
    }
}
