package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Map;

@SuppressLint({"ParcelCreator"})
public class u extends ResultReceiver {
    private a a;

    public interface a {
        void a(int i, Bundle bundle);
    }

    public u(Handler handler) {
        super(handler);
    }

    public void a(a aVar) {
        this.a = aVar;
    }

    /* access modifiers changed from: protected */
    public void onReceiveResult(int resultCode, Bundle resultData) {
        a aVar = this.a;
        if (aVar != null) {
            aVar.a(resultCode, resultData);
        }
    }

    public static void a(@Nullable ResultReceiver resultReceiver, @NonNull sc scVar) {
        if (resultReceiver != null) {
            Bundle bundle = new Bundle();
            bundle.putString("Uuid", scVar.a);
            bundle.putString("DeviceId", scVar.b);
            bundle.putString("DeviceIdHash", scVar.d);
            bundle.putString("AdUrlGet", scVar.f);
            bundle.putString("AdUrlReport", scVar.g);
            bundle.putLong("ServerTimeOffset", ty.c());
            bundle.putString("Clids", th.a((Map) tu.a(scVar.m)));
            resultReceiver.send(1, bundle);
        }
    }

    public static void a(ResultReceiver resultReceiver, rw rwVar) {
        if (resultReceiver != null) {
            resultReceiver.send(2, rwVar.a(new Bundle()));
        }
    }
}
