package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.pg;

public class nc {
    @NonNull
    private final mg a;
    @NonNull
    private final nb b;

    public nc(@NonNull mg mgVar, @NonNull nb nbVar) {
        this.a = mgVar;
        this.b = nbVar;
    }

    @Nullable
    public pg.b.C0005b a(long j, @Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            ms a2 = this.a.a(j, str);
            if (a2 != null) {
                return this.b.a(a2);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
