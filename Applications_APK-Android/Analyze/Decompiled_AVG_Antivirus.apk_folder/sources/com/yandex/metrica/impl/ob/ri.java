package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.util.Base64;
import androidx.annotation.NonNull;
import java.net.Socket;
import java.util.HashMap;
import org.json.JSONException;

class ri extends rk {
    @NonNull
    private rt c;
    @NonNull
    private rj d;

    ri(@NonNull Socket socket, @NonNull Uri uri, @NonNull rn rnVar, @NonNull rt rtVar, @NonNull rj rjVar) {
        super(socket, uri, rnVar);
        this.c = rtVar;
        this.d = rjVar;
    }

    public void a() {
        if (this.c.b.equals(this.b.getQueryParameter("t"))) {
            try {
                final byte[] b = b();
                a("HTTP/1.1 200 OK", new HashMap<String, String>() {
                    {
                        put("Content-Type", "text/plain; charset=utf-8");
                        put("Access-Control-Allow-Origin", "*");
                        put("Access-Control-Allow-Methods", "GET");
                        put("Content-Length", String.valueOf(b.length));
                    }
                }, b);
            } catch (JSONException e) {
            }
        } else {
            this.a.a("request_with_wrong_token");
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b() throws JSONException {
        return Base64.encode(new uh().a(this.d.a().getBytes()), 0);
    }
}
