package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Collections;
import java.util.List;

public class iu {
    @NonNull
    public final ix a;
    @NonNull
    public final List<ix> b;

    public iu(@NonNull ix ixVar, @Nullable List<ix> list) {
        this.a = ixVar;
        this.b = list == null ? Collections.emptyList() : Collections.unmodifiableList(list);
    }
}
