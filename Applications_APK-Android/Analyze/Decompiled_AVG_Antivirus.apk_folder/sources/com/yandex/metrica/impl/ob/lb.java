package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pp;
import java.util.ArrayList;
import java.util.List;

public class lb implements ld<mj, pp.a.C0014a.C0015a> {
    @NonNull
    public /* synthetic */ Object b(@NonNull Object obj) {
        return a((List<mj>) ((List) obj));
    }

    @NonNull
    public pp.a.C0014a.C0015a[] a(@NonNull List<mj> list) {
        pp.a.C0014a.C0015a[] aVarArr = new pp.a.C0014a.C0015a[list.size()];
        for (int i = 0; i < list.size(); i++) {
            aVarArr[i] = a(list.get(i));
        }
        return aVarArr;
    }

    @NonNull
    public List<mj> a(@NonNull pp.a.C0014a.C0015a[] aVarArr) {
        ArrayList arrayList = new ArrayList();
        for (pp.a.C0014a.C0015a a : aVarArr) {
            arrayList.add(a(a));
        }
        return arrayList;
    }

    @NonNull
    private pp.a.C0014a.C0015a a(@NonNull mj mjVar) {
        pp.a.C0014a.C0015a aVar = new pp.a.C0014a.C0015a();
        aVar.b = mjVar.a;
        aVar.c = mjVar.b;
        return aVar;
    }

    @NonNull
    private mj a(@NonNull pp.a.C0014a.C0015a aVar) {
        return new mj(aVar.b, aVar.c);
    }
}
