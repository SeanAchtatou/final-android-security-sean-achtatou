package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public class nt {
    @NonNull
    public final String a;
    public final boolean b;

    public nt(@NonNull String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        nt ntVar = (nt) o;
        if (this.b != ntVar.b) {
            return false;
        }
        return this.a.equals(ntVar.a);
    }

    public int hashCode() {
        return (this.a.hashCode() * 31) + (this.b ? 1 : 0);
    }

    public String toString() {
        return "PermissionState{name='" + this.a + '\'' + ", granted=" + this.b + '}';
    }
}
