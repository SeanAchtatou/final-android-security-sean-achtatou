package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

public class bc {
    @NonNull
    private Executor a;

    public bc() {
        this(aw.a);
    }

    public bc(@NonNull Executor executor) {
        this.a = executor;
    }

    public bb a(@NonNull Context context, @NonNull df dfVar) {
        bb bbVar = new bb(context, dfVar, this.a);
        bbVar.setName(va.a("YMM-NC[" + dfVar + "]"));
        bbVar.start();
        return bbVar;
    }
}
