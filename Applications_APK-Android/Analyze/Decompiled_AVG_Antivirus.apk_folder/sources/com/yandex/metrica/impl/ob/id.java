package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;

public class id {
    @NonNull
    private final c a;

    interface c {
        @NonNull
        ib a();
    }

    static class b implements c {
        @NonNull
        private final ic a;

        public b(@NonNull Context context) {
            this.a = new ic(context);
        }

        @NonNull
        public ib a() {
            return this.a;
        }
    }

    @TargetApi(26)
    static class a implements c {
        @NonNull
        private final ia a;

        public a(@NonNull Context context) {
            this.a = new ia(context);
        }

        @NonNull
        public ib a() {
            return this.a;
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public id(@NonNull Context context) {
        this(Build.VERSION.SDK_INT >= 26 ? new a(context) : new b(context));
    }

    id(@NonNull c cVar) {
        this.a = cVar;
    }

    public ib a() {
        return this.a.a();
    }
}
