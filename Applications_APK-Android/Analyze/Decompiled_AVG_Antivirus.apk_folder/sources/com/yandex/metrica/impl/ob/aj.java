package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.util.Base64;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class aj {
    private final JSONObject a;
    @NonNull
    private final vp b;

    public aj() {
        this(new vp());
    }

    @VisibleForTesting
    public aj(@NonNull vp vpVar) {
        this.a = new JSONObject();
        this.b = vpVar;
    }

    public aj a(Context context) {
        try {
            b();
            a();
        } catch (Throwable th) {
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public aj a() throws JSONException {
        ((JSONObject) a(this.a, "dfid", new JSONObject())).put("boot_time", ty.a() / 1000);
        return this;
    }

    /* access modifiers changed from: package-private */
    public aj a(Context context, boolean z) throws JSONException, UnsupportedEncodingException {
        JSONObject jSONObject = (JSONObject) a((JSONObject) a(this.a, "dfid", new JSONObject()), "au", new JSONObject());
        JSONArray jSONArray = (JSONArray) a(jSONObject, "aun", new JSONArray());
        JSONArray jSONArray2 = (JSONArray) a(jSONObject, "ausf", new JSONArray());
        JSONArray jSONArray3 = (JSONArray) a(jSONObject, "audf", new JSONArray());
        JSONArray jSONArray4 = (JSONArray) a(jSONObject, "aulu", new JSONArray());
        JSONArray jSONArray5 = new JSONArray();
        if (z) {
            a(jSONObject, "aufi", jSONArray5);
        }
        HashSet hashSet = new HashSet();
        for (ResolveInfo resolveInfo : cg.a(context, new String(Base64.decode("YW5kcm9pZC5pbnRlbnQuYWN0aW9uLk1BSU4=", 0), "UTF-8"), new String(Base64.decode("YW5kcm9pZC5pbnRlbnQuY2F0ZWdvcnkuTEFVTkNIRVI=", 0), "UTF-8"))) {
            ApplicationInfo applicationInfo = resolveInfo.activityInfo.applicationInfo;
            if (hashSet.add(applicationInfo.packageName)) {
                jSONArray.put(applicationInfo.packageName);
                boolean z2 = (applicationInfo.flags & 1) == 1;
                jSONArray2.put(z2);
                jSONArray4.put(new File(applicationInfo.sourceDir).lastModified());
                jSONArray3.put(true ^ applicationInfo.enabled);
                if (z) {
                    if (z2) {
                        jSONArray5.put(0);
                    } else {
                        PackageInfo a2 = this.b.a(context, applicationInfo.packageName);
                        if (a2 == null) {
                            jSONArray5.put(0);
                        } else {
                            jSONArray5.put(a2.firstInstallTime / 1000);
                        }
                    }
                }
            }
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public aj b(Context context) throws JSONException {
        JSONObject jSONObject = (JSONObject) a((JSONObject) a(this.a, "dfid", new JSONObject()), "apps", new JSONObject());
        JSONArray jSONArray = (JSONArray) a(jSONObject, "names", new JSONArray());
        JSONArray jSONArray2 = (JSONArray) a(jSONObject, "system_flags", new JSONArray());
        JSONArray jSONArray3 = (JSONArray) a(jSONObject, "disabled_flags", new JSONArray());
        JSONArray jSONArray4 = (JSONArray) a(jSONObject, "first_install_time", new JSONArray());
        JSONArray jSONArray5 = (JSONArray) a(jSONObject, "last_update_time", new JSONArray());
        jSONObject.put("version", 0);
        for (PackageInfo next : cg.a(context)) {
            jSONArray.put(next.packageName);
            jSONArray2.put((next.applicationInfo.flags & 1) == 1);
            jSONArray3.put(!next.applicationInfo.enabled);
            a(jSONArray4, next);
            b(jSONArray5, next);
        }
        return this;
    }

    private void a(JSONArray jSONArray, PackageInfo packageInfo) {
        jSONArray.put(packageInfo.firstInstallTime / 1000);
    }

    private void b(JSONArray jSONArray, PackageInfo packageInfo) {
        jSONArray.put(packageInfo.lastUpdateTime / 1000);
    }

    /* access modifiers changed from: package-private */
    public aj b() throws JSONException {
        JSONObject jSONObject = (JSONObject) a(this.a, "dfid", new JSONObject());
        long a2 = bj.a(true);
        long a3 = bj.a(false);
        long c = bj.c(true);
        long c2 = bj.c(false);
        jSONObject.put("tds", a2 + a3);
        jSONObject.put("fds", c + c2);
        return this;
    }

    static <T> T a(JSONObject jSONObject, String str, T t) throws JSONException {
        if (!jSONObject.has(str)) {
            jSONObject.put(str, t);
        }
        return jSONObject.get(str);
    }

    public String toString() {
        return this.a.toString();
    }

    public String c() {
        return this.a.toString();
    }
}
