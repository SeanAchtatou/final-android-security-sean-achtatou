package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import org.json.JSONObject;

public class fs extends ft {
    public fs(di diVar) {
        super(diVar);
    }

    public boolean a(@NonNull t tVar) {
        String e = tVar.e();
        if (TextUtils.isEmpty(e)) {
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(e);
            if (!"open".equals(jSONObject.optString("type")) || !a(jSONObject.optString("link"))) {
                return false;
            }
            b();
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    private void b() {
        a().n();
        a().y().a();
    }

    private boolean a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                String queryParameter = Uri.parse(str).getQueryParameter("referrer");
                if (!TextUtils.isEmpty(queryParameter)) {
                    for (String str2 : queryParameter.split("&")) {
                        int indexOf = str2.indexOf("=");
                        if (indexOf >= 0 && "reattribution".equals(Uri.decode(str2.substring(0, indexOf))) && "1".equals(Uri.decode(str2.substring(indexOf + 1)))) {
                            return true;
                        }
                    }
                }
            } catch (Throwable th) {
            }
        }
        return false;
    }
}
