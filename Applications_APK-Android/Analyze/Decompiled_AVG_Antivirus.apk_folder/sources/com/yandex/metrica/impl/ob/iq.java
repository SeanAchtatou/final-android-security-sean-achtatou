package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.io.File;

public class iq {
    public boolean a(@NonNull File file) {
        if (!file.exists()) {
            return b(file);
        }
        if (file.isDirectory()) {
            return true;
        }
        if (file.delete()) {
            return b(file);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean b(@NonNull File file) {
        if (file.mkdir()) {
            return true;
        }
        return false;
    }
}
