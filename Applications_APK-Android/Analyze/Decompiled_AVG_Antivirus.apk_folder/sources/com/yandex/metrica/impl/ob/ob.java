package com.yandex.metrica.impl.ob;

import android.content.Context;
import java.util.Map;

@Deprecated
public class ob extends oc {
    private final oj d = new oj("init_event_pref_key", i());
    private final oj e = new oj("init_event_pref_key");
    private final oj f = new oj("first_event_pref_key", i());
    private final oj g = new oj("fitst_event_description_key", i());

    public ob(Context context, String str) {
        super(context, str);
    }

    public void a() {
        a(this.d.b(), "DONE").j();
    }

    @Deprecated
    public String a(String str) {
        return this.c.getString(this.e.b(), str);
    }

    public String b(String str) {
        return this.c.getString(this.d.b(), str);
    }

    public String c(String str) {
        return this.c.getString(this.f.b(), str);
    }

    @Deprecated
    public void b() {
        a(this.e);
    }

    @Deprecated
    public void d(String str) {
        a(new oj("init_event_pref_key", str));
    }

    public void c() {
        a(this.d);
    }

    public void d() {
        a(this.f);
    }

    public String e(String str) {
        return this.c.getString(this.g.b(), str);
    }

    public void e() {
        a(this.g);
    }

    private void a(oj ojVar) {
        this.c.edit().remove(ojVar.b()).apply();
    }

    /* access modifiers changed from: protected */
    public String f() {
        return "_initpreferences";
    }

    /* access modifiers changed from: package-private */
    public Map<String, ?> g() {
        return this.c.getAll();
    }

    static String f(String str) {
        return new oj("init_event_pref_key", str).b();
    }

    static String g(String str) {
        return str.replace("init_event_pref_key", "");
    }
}
