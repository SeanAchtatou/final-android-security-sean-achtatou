package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.qj;
import com.yandex.metrica.impl.ob.qm;
import java.util.List;
import java.util.Map;

public class qp extends qm {
    private boolean a;
    private Location b;
    private boolean c;
    private boolean d;
    private boolean e;
    private int f;
    private int g;
    private boolean h;
    private int i;
    private Boolean j;
    private d k;
    private String l;
    private boolean m;
    private boolean n;
    private boolean o;
    private boolean p;
    private String q;
    private List<String> r;
    private boolean s;
    private int t;
    private long u;
    private long v;
    private boolean w;

    public interface d {
        boolean a(@Nullable Boolean bool);
    }

    public static abstract class b implements d {
        @NonNull
        protected final cd a;

        public b(@NonNull cd cdVar) {
            this.a = cdVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
         arg types: [java.lang.Boolean, int]
         candidates:
          com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
          com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
          com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
          com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
          com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
          com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean */
        public boolean a(@Nullable Boolean bool) {
            return ua.a(bool, true);
        }
    }

    @VisibleForTesting
    qp() {
    }

    @NonNull
    public String a() {
        return ua.b(this.q, "");
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.q = str;
    }

    public void a(List<String> list) {
        this.r = list;
    }

    public List<String> b() {
        return this.r;
    }

    public String c() {
        return this.l;
    }

    public boolean F() {
        return this.m;
    }

    public void a(boolean z) {
        this.m = z;
    }

    public boolean G() {
        return this.n;
    }

    public boolean H() {
        return this.o;
    }

    public boolean I() {
        return this.p;
    }

    public void a(long j2) {
        this.u = j2;
    }

    public long J() {
        return this.u;
    }

    public void b(long j2) {
        this.v = j2;
    }

    public long K() {
        return this.v;
    }

    public void b(boolean z) {
        this.n = z;
    }

    public void c(boolean z) {
        this.o = z;
    }

    public void d(boolean z) {
        this.p = z;
    }

    public boolean L() {
        return f() && !cg.a(b()) && X();
    }

    /* access modifiers changed from: private */
    public void m(String str) {
        this.l = str;
    }

    public boolean M() {
        return this.s;
    }

    /* access modifiers changed from: private */
    public void k(boolean z) {
        this.s = z;
    }

    public boolean N() {
        return this.a;
    }

    public void e(boolean z) {
        this.a = z;
    }

    public Location O() {
        return this.b;
    }

    public void a(Location location) {
        this.b = location;
    }

    public boolean P() {
        return this.c;
    }

    public void f(boolean z) {
        this.c = z;
    }

    public boolean Q() {
        return this.d;
    }

    public void g(boolean z) {
        this.d = z;
    }

    public boolean R() {
        return this.e;
    }

    public void h(boolean z) {
        this.e = z;
    }

    public int S() {
        return this.f;
    }

    public void a(int i2) {
        this.f = i2;
    }

    public int T() {
        return this.g;
    }

    public void b(int i2) {
        this.g = i2;
    }

    public void i(boolean z) {
        this.h = z;
    }

    public int U() {
        return this.i;
    }

    public void c(int i2) {
        this.i = i2;
    }

    public int V() {
        return this.t;
    }

    public void d(int i2) {
        this.t = i2;
    }

    public boolean W() {
        return this.k.a(this.j);
    }

    public void a(@Nullable Boolean bool, @NonNull d dVar) {
        this.j = bool;
        this.k = dVar;
    }

    public boolean X() {
        return this.w;
    }

    public void j(boolean z) {
        this.w = z;
    }

    public static final class a extends qj.a<db.a, a> {
        @Nullable
        public final String a;
        @Nullable
        public final Location b;
        public final boolean f;
        public final boolean g;
        public final boolean h;
        public final int i;
        public final int j;
        public final int k;
        public final boolean l;
        public final boolean m;
        public final boolean n;
        @Nullable
        public final Map<String, String> o;

        /* access modifiers changed from: package-private */
        public boolean a(@Nullable Location location, @Nullable Location location2) {
            if (location == location2) {
                return true;
            }
            if (location == null || location2 == null || location.getTime() != location2.getTime()) {
                return false;
            }
            if ((cg.a(17) && location.getElapsedRealtimeNanos() != location2.getElapsedRealtimeNanos()) || Double.compare(location2.getLatitude(), location.getLatitude()) != 0 || Double.compare(location2.getLongitude(), location.getLongitude()) != 0 || Double.compare(location2.getAltitude(), location.getAltitude()) != 0 || Float.compare(location2.getSpeed(), location.getSpeed()) != 0 || Float.compare(location2.getBearing(), location.getBearing()) != 0 || Float.compare(location2.getAccuracy(), location.getAccuracy()) != 0) {
                return false;
            }
            if (cg.a(26) && (Float.compare(location2.getVerticalAccuracyMeters(), location.getVerticalAccuracyMeters()) != 0 || Float.compare(location2.getSpeedAccuracyMetersPerSecond(), location.getSpeedAccuracyMetersPerSecond()) != 0 || Float.compare(location2.getBearingAccuracyDegrees(), location.getBearingAccuracyDegrees()) != 0)) {
                return false;
            }
            if (location.getProvider() == null ? location2.getProvider() != null : !location.getProvider().equals(location2.getProvider())) {
                return false;
            }
            if (location.getExtras() != null) {
                return location.getExtras().equals(location2.getExtras());
            }
            if (location2.getExtras() == null) {
                return true;
            }
            return false;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public a(@androidx.annotation.NonNull com.yandex.metrica.impl.ob.db.a r17) {
            /*
                r16 = this;
                r0 = r17
                java.lang.String r1 = r0.a
                java.lang.String r2 = r0.b
                java.lang.String r3 = r0.c
                java.lang.String r4 = r0.d
                java.lang.Boolean r5 = r0.e
                android.location.Location r6 = r0.f
                java.lang.Boolean r7 = r0.g
                java.lang.Boolean r8 = r0.h
                java.lang.Boolean r9 = r0.n
                java.lang.Integer r10 = r0.i
                java.lang.Integer r11 = r0.j
                java.lang.Integer r12 = r0.k
                java.lang.Boolean r13 = r0.l
                java.lang.Boolean r14 = r0.m
                java.util.Map<java.lang.String, java.lang.String> r15 = r0.o
                r0 = r16
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.qp.a.<init>(com.yandex.metrica.impl.ob.db$a):void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
         arg types: [java.lang.Boolean, int]
         candidates:
          com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
          com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
          com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
          com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
          com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
          com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean */
        a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable Boolean bool, @Nullable Location location, @Nullable Boolean bool2, @Nullable Boolean bool3, @Nullable Boolean bool4, @Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Boolean bool5, @Nullable Boolean bool6, @Nullable Map<String, String> map) {
            super(str, str2, str3);
            this.a = str4;
            this.f = ua.a(bool, true);
            this.b = location;
            this.g = ua.a(bool2, false);
            this.h = ua.a(bool3, false);
            this.n = ua.a(bool4, false);
            this.i = Math.max(10, ua.a(num, 10));
            this.j = ua.a(num2, 7);
            this.k = ua.a(num3, 90);
            this.l = ua.a(bool5, false);
            this.m = ua.a(bool6, true);
            this.o = map;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
          com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
          com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
          com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
          com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
          com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T */
        @NonNull
        /* renamed from: a */
        public a b(@NonNull db.a aVar) {
            db.a aVar2 = aVar;
            return new a((String) ua.a((Object) aVar2.a, (Object) this.c), (String) ua.a((Object) aVar2.b, (Object) this.d), (String) ua.a((Object) aVar2.c, (Object) this.e), (String) ua.a((Object) aVar2.d, (Object) this.a), (Boolean) ua.a(aVar2.e, Boolean.valueOf(this.f)), (Location) ua.a(aVar2.f, this.b), (Boolean) ua.a(aVar2.g, Boolean.valueOf(this.g)), (Boolean) ua.a(aVar2.h, Boolean.valueOf(this.h)), aVar2.n, (Integer) ua.a(aVar2.i, Integer.valueOf(this.i)), (Integer) ua.a(aVar2.j, Integer.valueOf(this.j)), (Integer) ua.a(aVar2.k, Integer.valueOf(this.k)), (Boolean) ua.a(aVar2.l, Boolean.valueOf(this.l)), (Boolean) ua.a(aVar2.m, Boolean.valueOf(this.m)), (Map) ua.a(aVar2.o, this.o));
        }

        /* renamed from: b */
        public boolean a(@NonNull db.a aVar) {
            Map<String, String> map;
            String str;
            if (aVar.a != null && !aVar.a.equals(this.c)) {
                return false;
            }
            if (aVar.b != null && !aVar.b.equals(this.d)) {
                return false;
            }
            if (aVar.c != null && !aVar.c.equals(this.e)) {
                return false;
            }
            if (aVar.e != null && this.f != aVar.e.booleanValue()) {
                return false;
            }
            if (aVar.g != null && this.g != aVar.g.booleanValue()) {
                return false;
            }
            if (aVar.h != null && this.h != aVar.h.booleanValue()) {
                return false;
            }
            if (aVar.i != null && this.i != aVar.i.intValue()) {
                return false;
            }
            if (aVar.j != null && this.j != aVar.j.intValue()) {
                return false;
            }
            if (aVar.k != null && this.k != aVar.k.intValue()) {
                return false;
            }
            if (aVar.l != null && this.l != aVar.l.booleanValue()) {
                return false;
            }
            if (aVar.m != null && this.m != aVar.m.booleanValue()) {
                return false;
            }
            if (aVar.n != null && this.n != aVar.n.booleanValue()) {
                return false;
            }
            if (aVar.d != null && ((str = this.a) == null || !str.equals(aVar.d))) {
                return false;
            }
            if (aVar.o != null && ((map = this.o) == null || !map.equals(aVar.o))) {
                return false;
            }
            if (aVar.f == null || a(this.b, aVar.f)) {
                return true;
            }
            return false;
        }
    }

    public static class c extends qm.a<qp, a> {
        @NonNull
        private final di c;
        @NonNull
        private final d d;

        @NonNull
        public /* bridge */ /* synthetic */ qj a(@NonNull Object obj) {
            return a((qj.c<a>) ((qj.c) obj));
        }

        @NonNull
        public /* synthetic */ qm b(@NonNull qj.c cVar) {
            return a((qj.c<a>) cVar);
        }

        @NonNull
        public /* synthetic */ qj c(@NonNull qj.c cVar) {
            return a((qj.c<a>) cVar);
        }

        public c(@NonNull di diVar, @NonNull d dVar) {
            super(diVar.j(), diVar.b().b());
            this.c = diVar;
            this.d = dVar;
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public qp b() {
            return new qp();
        }

        @NonNull
        public qp a(@NonNull qj.c<a> cVar) {
            qp qpVar = (qp) super.c(cVar);
            qpVar.m(((a) cVar.b).a);
            qpVar.k(this.c.w());
            qpVar.d(this.c.o());
            qpVar.e(((a) cVar.b).f);
            qpVar.a(((a) cVar.b).b);
            qpVar.f(((a) cVar.b).g);
            qpVar.g(((a) cVar.b).h);
            qpVar.a(((a) cVar.b).i);
            qpVar.c(((a) cVar.b).j);
            qpVar.b(((a) cVar.b).k);
            qpVar.i(((a) cVar.b).l);
            qpVar.h(((a) cVar.b).n);
            qpVar.a(Boolean.valueOf(((a) cVar.b).m), this.d);
            a(qpVar, cVar.a, ((a) cVar.b).o);
            return qpVar;
        }

        /* access modifiers changed from: package-private */
        @VisibleForTesting
        public void a(@NonNull qp qpVar, @NonNull sc scVar, @Nullable Map<String, String> map) {
            a(qpVar, scVar);
            b(qpVar, scVar);
            qpVar.a(scVar.m);
            qpVar.j(a(map, tu.a(scVar.n)));
        }

        /* access modifiers changed from: package-private */
        public boolean a(@Nullable Map<String, String> map, @Nullable Map<String, String> map2) {
            return map == null || map.isEmpty() || map.equals(map2);
        }

        /* access modifiers changed from: package-private */
        public void a(qp qpVar, sc scVar) {
            qpVar.a(scVar.e);
        }

        /* access modifiers changed from: package-private */
        public void b(qp qpVar, sc scVar) {
            qpVar.a(scVar.o.a);
            qpVar.b(scVar.o.b);
            qpVar.c(scVar.o.c);
            if (scVar.z != null) {
                qpVar.a(scVar.z.a);
                qpVar.b(scVar.z.b);
            }
            qpVar.d(scVar.o.d);
        }
    }
}
