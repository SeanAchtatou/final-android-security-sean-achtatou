package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;

public class qr implements qs<ey> {
    public void a(@NonNull Uri.Builder builder, @NonNull ey eyVar) {
        builder.appendPath("diagnostic");
        builder.appendQueryParameter("deviceid", eyVar.r());
        builder.appendQueryParameter("uuid", eyVar.t());
        builder.appendQueryParameter("app_platform", eyVar.l());
        builder.appendQueryParameter("analytics_sdk_version_name", eyVar.i());
        builder.appendQueryParameter("analytics_sdk_build_number", eyVar.j());
        builder.appendQueryParameter("analytics_sdk_build_type", eyVar.k());
        builder.appendQueryParameter("app_version_name", eyVar.q());
        builder.appendQueryParameter("app_build_number", eyVar.p());
        builder.appendQueryParameter(UrlManager.Parameter.MODEL, eyVar.m());
        builder.appendQueryParameter("manufacturer", eyVar.g());
        builder.appendQueryParameter("os_version", eyVar.n());
        builder.appendQueryParameter("os_api_level", String.valueOf(eyVar.o()));
        builder.appendQueryParameter("screen_width", String.valueOf(eyVar.w()));
        builder.appendQueryParameter("screen_height", String.valueOf(eyVar.x()));
        builder.appendQueryParameter("screen_dpi", String.valueOf(eyVar.y()));
        builder.appendQueryParameter("scalefactor", String.valueOf(eyVar.z()));
        builder.appendQueryParameter("locale", eyVar.A());
        builder.appendQueryParameter("device_type", eyVar.C());
        builder.appendQueryParameter(UrlManager.Parameter.APP_ID, eyVar.d());
        builder.appendQueryParameter("api_key_128", eyVar.b());
        builder.appendQueryParameter("app_debuggable", eyVar.E());
        builder.appendQueryParameter("is_rooted", eyVar.u());
        builder.appendQueryParameter("app_framework", eyVar.v());
    }
}
