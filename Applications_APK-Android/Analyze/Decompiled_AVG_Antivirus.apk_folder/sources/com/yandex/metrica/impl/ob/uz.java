package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public class uz extends Thread implements ux {
    private volatile boolean a = true;

    public uz() {
    }

    public uz(@NonNull Runnable runnable, @NonNull String str) {
        super(runnable, str);
    }

    public uz(@NonNull String str) {
        super(str);
    }

    public synchronized boolean c() {
        return this.a;
    }

    public synchronized void b() {
        this.a = false;
        interrupt();
    }
}
