package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;

public class us {
    @NonNull
    public Handler a() {
        return new Handler(Looper.getMainLooper());
    }

    @NonNull
    public uu b() {
        return new uu("YMM-APT");
    }

    @NonNull
    public uu c() {
        return new uu("YMM-RS");
    }

    @NonNull
    public uu d() {
        return new uu("YMM-YM");
    }
}
