package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.dd;
import com.yandex.metrica.impl.ob.fb;
import com.yandex.metrica.impl.ob.fj;

public class fk<T extends fj, C extends dd> extends fb<T, C> {
    public fk(@NonNull fh<T> fhVar, @NonNull C c) {
        super(fhVar, c);
    }

    public boolean a(@NonNull t tVar, @NonNull final el elVar) {
        return a(tVar, new fb.a<T>() {
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public boolean a(T r2, com.yandex.metrica.impl.ob.t r3) {
                /*
                    r1 = this;
                    com.yandex.metrica.impl.ob.el r0 = r3
                    boolean r2 = r2.a(r3, r0)
                    return r2
                */
                throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.fk.AnonymousClass1.a(com.yandex.metrica.impl.ob.fj, com.yandex.metrica.impl.ob.t):boolean");
            }
        });
    }
}
