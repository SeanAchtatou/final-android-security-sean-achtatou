package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.Nullable;

abstract class mf implements me {
    @Nullable
    private me a;

    public abstract void b(@Nullable String str, @Nullable Location location, @Nullable mh mhVar);

    public mf(@Nullable me meVar) {
        this.a = meVar;
    }

    public void a(@Nullable String str, @Nullable Location location, @Nullable mh mhVar) {
        b(str, location, mhVar);
        me meVar = this.a;
        if (meVar != null) {
            meVar.a(str, location, mhVar);
        }
    }
}
