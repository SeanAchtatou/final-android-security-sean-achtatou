package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.ab;
import com.yandex.metrica.impl.ob.fa;
import com.yandex.metrica.impl.ob.pg;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ex {
    public static final Map<Integer, Integer> a = Collections.unmodifiableMap(new HashMap<Integer, Integer>() {
        {
            put(Integer.valueOf(ab.a.EVENT_TYPE_DIAGNOSTIC.a()), 22);
            put(Integer.valueOf(ab.a.EVENT_TYPE_DIAGNOSTIC_STATBOX.a()), 23);
            put(Integer.valueOf(ab.a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING.a()), 24);
        }
    });
    @NonNull
    private final t b;
    @NonNull
    private final ey c;
    @NonNull
    private final fa d;
    @NonNull
    private final vl e;
    @NonNull
    private final vl f;
    @NonNull
    private final tx g;
    @NonNull
    private final dm h;

    public static class a {
        public ex a(@NonNull t tVar, @NonNull ey eyVar, @NonNull fa faVar, @NonNull kh khVar) {
            return new ex(tVar, eyVar, faVar, khVar);
        }
    }

    public ex(@NonNull t tVar, @NonNull ey eyVar, @NonNull fa faVar, @NonNull kh khVar) {
        this(tVar, eyVar, faVar, new dm(khVar), new vl(1024, "diagnostic event name"), new vl(204800, "diagnostic event value"), new tw());
    }

    public ex(@NonNull t tVar, @NonNull ey eyVar, @NonNull fa faVar, @NonNull dm dmVar, @NonNull vl vlVar, @NonNull vl vlVar2, @NonNull tx txVar) {
        this.b = tVar;
        this.c = eyVar;
        this.d = faVar;
        this.h = dmVar;
        this.f = vlVar;
        this.e = vlVar2;
        this.g = txVar;
    }

    public byte[] a() {
        pg.c cVar = new pg.c();
        pg.c.e eVar = new pg.c.e();
        int i = 0;
        cVar.b = new pg.c.e[]{eVar};
        fa.a a2 = this.d.a();
        eVar.b = a2.a;
        eVar.c = new pg.c.e.b();
        eVar.c.d = 2;
        eVar.c.b = new pg.c.g();
        eVar.c.b.b = a2.b;
        eVar.c.b.c = ty.a(a2.b);
        eVar.c.c = this.c.A();
        pg.c.e.a aVar = new pg.c.e.a();
        eVar.d = new pg.c.e.a[]{aVar};
        aVar.b = (long) a2.c;
        aVar.q = (long) this.h.a(this.b.g());
        aVar.c = this.g.b() - a2.b;
        aVar.d = a.get(Integer.valueOf(this.b.g())).intValue();
        if (!TextUtils.isEmpty(this.b.d())) {
            aVar.e = this.f.a(this.b.d());
        }
        if (!TextUtils.isEmpty(this.b.e())) {
            String e2 = this.b.e();
            String a3 = this.e.a(e2);
            if (!TextUtils.isEmpty(a3)) {
                aVar.f = a3.getBytes();
            }
            int length = e2.getBytes().length;
            if (aVar.f != null) {
                i = aVar.f.length;
            }
            aVar.k = length - i;
        }
        return e.a(cVar);
    }
}
