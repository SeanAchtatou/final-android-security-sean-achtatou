package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.i;

@Deprecated
public class oe extends oc {
    @Deprecated
    public static final oj d = new oj("APP_ENVIRONMENT");
    @Deprecated
    public static final oj e = new oj("APP_ENVIRONMENT_REVISION");
    private static final oj f = new oj("SESSION_SLEEP_START_");
    private static final oj g = new oj("SESSION_ID_");
    private static final oj h = new oj("SESSION_COUNTER_ID_");
    private static final oj i = new oj("SESSION_INIT_TIME_");
    private static final oj j = new oj("SESSION_ALIVE_TIME_");
    private static final oj k = new oj("SESSION_IS_ALIVE_REPORT_NEEDED_");
    private static final oj l = new oj("BG_SESSION_ID_");
    private static final oj m = new oj("BG_SESSION_SLEEP_START_");
    private static final oj n = new oj("BG_SESSION_COUNTER_ID_");
    private static final oj o = new oj("BG_SESSION_INIT_TIME_");
    private static final oj p = new oj("COLLECT_INSTALLED_APPS_");
    private static final oj q = new oj("IDENTITY_SEND_TIME_");
    private static final oj r = new oj("USER_INFO_");
    private static final oj s = new oj("REFERRER_");
    private static final oj t = new oj("APP_ENVIRONMENT_");
    private static final oj u = new oj("APP_ENVIRONMENT_REVISION_");
    private oj A = new oj(k.a(), i());
    private oj B = new oj(l.a(), i());
    private oj C = new oj(m.a(), i());
    private oj D = new oj(n.a(), i());
    private oj E = new oj(o.a(), i());
    private oj F = new oj(q.a(), i());
    private oj G = new oj(p.a(), i());
    private oj H = new oj(r.a(), i());
    private oj I = new oj(s.a(), i());
    private oj J = new oj(t.a(), i());
    private oj K = new oj(u.a(), i());
    private oj v = new oj(f.a(), i());
    private oj w = new oj(g.a(), i());
    private oj x = new oj(h.a(), i());
    private oj y = new oj(i.a(), i());
    private oj z = new oj(j.a(), i());

    public oe(Context context, String str) {
        super(context, str);
        a(-1);
        b(0);
        c(0);
    }

    /* access modifiers changed from: protected */
    public String f() {
        return "_boundentrypreferences";
    }

    public long a(long j2) {
        return a(this.y.b(), j2);
    }

    public long b(long j2) {
        return a(this.E.b(), j2);
    }

    public long c(long j2) {
        return a(this.F.b(), j2);
    }

    public long d(long j2) {
        return a(this.w.b(), j2);
    }

    public long e(long j2) {
        return a(this.B.b(), j2);
    }

    public long f(long j2) {
        return a(this.x.b(), j2);
    }

    private long a(String str, long j2) {
        return this.c.getLong(str, j2);
    }

    public long g(long j2) {
        return a(this.D.b(), j2);
    }

    @Nullable
    public i.a a() {
        synchronized (this) {
            if (!this.c.contains(this.J.b()) || !this.c.contains(this.K.b())) {
                return null;
            }
            i.a aVar = new i.a(this.c.getString(this.J.b(), "{}"), this.c.getLong(this.K.b(), 0));
            return aVar;
        }
    }

    public long h(long j2) {
        return a(this.v.b(), j2);
    }

    public long i(long j2) {
        return a(this.C.b(), j2);
    }

    public Boolean a(boolean z2) {
        return Boolean.valueOf(this.c.getBoolean(this.A.b(), z2));
    }

    public Boolean b() {
        int i2 = this.c.getInt(this.G.b(), -1);
        if (i2 == 0) {
            return false;
        }
        if (i2 != 1) {
            return null;
        }
        return true;
    }

    public String a(String str) {
        return this.c.getString(this.H.b(), str);
    }

    public String b(String str) {
        return this.c.getString(this.I.b(), str);
    }

    public oe a(i.a aVar) {
        synchronized (this) {
            a(this.J.b(), aVar.a);
            a(this.K.b(), Long.valueOf(aVar.b));
        }
        return this;
    }

    public oe c() {
        return (oe) h(this.I.b());
    }

    private void a(int i2) {
        ok.a(this.c, this.z.b(), i2);
    }

    private void b(int i2) {
        ok.a(this.c, this.v.b(), i2);
    }

    private void c(int i2) {
        ok.a(this.c, this.x.b(), i2);
    }

    public oe d() {
        if (this.c.contains(this.G.b())) {
            return (oe) h(this.G.b());
        }
        return this;
    }

    public boolean e() {
        return this.c.contains(this.y.b()) || this.c.contains(this.z.b()) || this.c.contains(this.A.b()) || this.c.contains(this.v.b()) || this.c.contains(this.w.b()) || this.c.contains(this.x.b()) || this.c.contains(this.E.b()) || this.c.contains(this.C.b()) || this.c.contains(this.B.b()) || this.c.contains(this.D.b()) || this.c.contains(this.J.b()) || this.c.contains(this.H.b()) || this.c.contains(this.I.b()) || this.c.contains(this.F.b());
    }

    public void g() {
        this.c.edit().remove(this.E.b()).remove(this.D.b()).remove(this.B.b()).remove(this.C.b()).remove(this.y.b()).remove(this.x.b()).remove(this.w.b()).remove(this.v.b()).remove(this.A.b()).remove(this.z.b()).remove(this.H.b()).remove(this.J.b()).remove(this.K.b()).remove(this.I.b()).remove(this.F.b()).apply();
    }
}
