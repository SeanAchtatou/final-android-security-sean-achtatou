package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public class mx {
    @NonNull
    private final mo a;
    @NonNull
    private final sv b;
    @NonNull
    private final ch c;
    private final kj d;

    public mx(@NonNull mo moVar, @NonNull sv svVar, @NonNull ch chVar, @NonNull kj kjVar) {
        this.a = moVar;
        this.b = svVar;
        this.c = chVar;
        this.d = kjVar;
        a();
    }

    private void a() {
        boolean g = this.d.g();
        this.a.a(g);
        this.c.a(g);
        this.b.a(g);
    }

    public void a(@NonNull Object obj) {
        this.a.b(obj);
        this.b.b();
    }

    public void b(@NonNull Object obj) {
        this.a.a(obj);
        this.b.a();
    }

    public void a(boolean z) {
        this.a.a(z);
        this.b.a(z);
        this.c.a(z);
        this.d.b(z);
    }

    public void a(@NonNull sc scVar, boolean z) {
        this.a.a(scVar, z ? scVar.q : scVar.p);
        this.c.a(scVar);
        this.b.a(scVar);
    }
}
