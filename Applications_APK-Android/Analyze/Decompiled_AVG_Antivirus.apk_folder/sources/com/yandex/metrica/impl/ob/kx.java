package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.Arrays;

public class kx {
    @NonNull
    private final a a;
    @NonNull
    private final td b;

    public static class a {
        /* access modifiers changed from: package-private */
        public uf a(byte[] bArr, byte[] bArr2) {
            return new uf("AES/CBC/PKCS5Padding", bArr, bArr2);
        }
    }

    public kx() {
        this(new a(), new td());
    }

    @VisibleForTesting
    public kx(@NonNull a aVar, @NonNull td tdVar) {
        this.a = aVar;
        this.b = tdVar;
    }

    @Nullable
    public byte[] a(@Nullable byte[] bArr, @NonNull String str) {
        try {
            uf a2 = this.a.a(str.getBytes(), Arrays.copyOfRange(bArr, 0, 16));
            if (cg.a(bArr)) {
                return null;
            }
            return this.b.b(a2.a(bArr, 16, bArr.length - 16));
        } catch (Throwable th) {
            return null;
        }
    }
}
