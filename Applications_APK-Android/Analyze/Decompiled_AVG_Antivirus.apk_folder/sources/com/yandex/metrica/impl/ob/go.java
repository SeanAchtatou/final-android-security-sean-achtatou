package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.List;

public abstract class go<BaseHandler> {
    private final gv a;

    public abstract void a(@NonNull List<BaseHandler> list);

    public go(gv gvVar) {
        this.a = gvVar;
    }

    public gv a() {
        return this.a;
    }
}
