package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class im {
    @NonNull
    private final Context a;

    public abstract void a(@Nullable Bundle bundle, @Nullable il ilVar);

    public im(@NonNull Context context) {
        this.a = context;
    }
}
