package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class ih {
    @NonNull
    private final Context a;
    @NonNull
    private final vp b;

    public ih(@NonNull Context context) {
        this(context, new vp());
    }

    public int a() {
        try {
            return Math.max(1, this.b.b(this.a, new Intent().setAction("com.yandex.metrica.configuration.ACTION_INIT").setPackage(this.a.getPackageName()), 128).size());
        } catch (Throwable th) {
            return 1;
        }
    }

    @VisibleForTesting
    ih(@NonNull Context context, @NonNull vp vpVar) {
        this.a = context;
        this.b = vpVar;
    }
}
