package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.telephony.TelephonyManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.MotionEventCompat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;

public class sr implements sw<List<String>> {
    /* access modifiers changed from: private */
    @NonNull
    public final sp a;
    /* access modifiers changed from: private */
    @NonNull
    public nq b;

    sr(@NonNull sp spVar, @NonNull nq nqVar) {
        this.a = spVar;
        this.b = nqVar;
    }

    @Nullable
    /* renamed from: a */
    public List<String> d() {
        ArrayList arrayList = new ArrayList();
        if (this.a.g()) {
            if (cg.a(23)) {
                arrayList.addAll(c());
            } else {
                arrayList.add(b());
            }
        }
        return arrayList;
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    @Nullable
    private String b() {
        final TelephonyManager c = this.a.c();
        return (String) cg.a(new Callable<String>() {
            /* renamed from: a */
            public String call() throws Exception {
                TelephonyManager telephonyManager;
                if (!sr.this.b.d(sr.this.a.d()) || (telephonyManager = c) == null) {
                    return null;
                }
                return telephonyManager.getDeviceId();
            }
        }, c, "getting imei", "TelephonyManager");
    }

    @TargetApi(MotionEventCompat.AXIS_BRAKE)
    @NonNull
    private List<String> c() {
        final TelephonyManager c = this.a.c();
        return (List) cg.a(new Callable<List<String>>() {
            /* renamed from: a */
            public List<String> call() {
                HashSet hashSet = new HashSet();
                if (sr.this.b.d(sr.this.a.d()) && c != null) {
                    for (int i = 0; i < 10; i++) {
                        String deviceId = c.getDeviceId(i);
                        if (deviceId != null) {
                            hashSet.add(deviceId);
                        }
                    }
                }
                return new ArrayList(hashSet);
            }
        }, c, "getting all imeis", "TelephonyManager", new ArrayList());
    }
}
