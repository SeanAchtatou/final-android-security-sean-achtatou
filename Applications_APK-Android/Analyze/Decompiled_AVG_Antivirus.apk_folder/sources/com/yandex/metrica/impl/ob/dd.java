package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.ResultReceiver;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.qe;
import com.yandex.metrica.impl.ob.t;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

public class dd implements Cdo, dq, rz {
    @NonNull
    private final Context a;
    @NonNull
    private final df b;
    @NonNull
    private ru c;
    /* access modifiers changed from: private */
    @NonNull
    public sd d;
    @NonNull
    private final ec e;
    @NonNull
    private bb f;
    @NonNull
    private fk<fj, dd> g;
    @NonNull
    private cf<dd> h;
    @NonNull
    private List<ResultReceiver> i;
    private final dg<el> j;
    private qe k;
    private final qe.a l;
    @Nullable
    private pq m;
    private final Object n;

    public dd(@NonNull Context context, @NonNull ru ruVar, @NonNull df dfVar, @NonNull db dbVar, @NonNull qe qeVar, @NonNull bb bbVar) {
        this(context, ruVar, dfVar, dbVar, new ec(dbVar.b), qeVar, bbVar);
    }

    @VisibleForTesting
    dd(@NonNull Context context, @NonNull ru ruVar, @NonNull df dfVar, @NonNull db dbVar, @NonNull ec ecVar, @NonNull qe qeVar, @NonNull bb bbVar) {
        this.j = new dg<>();
        this.n = new Object();
        this.a = context.getApplicationContext();
        this.b = dfVar;
        this.c = ruVar;
        this.e = ecVar;
        this.f = bbVar;
        this.i = new ArrayList();
        this.g = new fk<>(new fc(this), this);
        this.d = this.c.a(this.a, this.b, this, dbVar.a);
        this.h = new cf<>(this, new sh(this.d), this.f);
        this.k = qeVar;
        this.l = new qe.a() {
            public boolean a(@NonNull qf qfVar) {
                if (TextUtils.isEmpty(qfVar.a)) {
                    return false;
                }
                dd.this.d.a(qfVar.a);
                return false;
            }
        };
        this.k.a(this.l);
    }

    public void a(@NonNull db.a aVar) {
        this.e.a(aVar);
    }

    public synchronized void a(@NonNull el elVar) {
        this.j.a(elVar);
    }

    public synchronized void b(@NonNull el elVar) {
        this.j.b(elVar);
    }

    public void a(@NonNull t tVar, @NonNull el elVar) {
        this.g.a(tVar, elVar);
    }

    @NonNull
    public db.a a() {
        return this.e.a();
    }

    @NonNull
    public df b() {
        return this.b;
    }

    public void a(@Nullable sc scVar) {
        b(scVar);
        if (scVar != null) {
            if (this.m == null) {
                this.m = af.a().e();
            }
            this.m.a(scVar);
        }
    }

    public void a(@NonNull rw rwVar) {
        synchronized (this.n) {
            for (ResultReceiver a2 : this.i) {
                u.a(a2, rwVar);
            }
            this.i.clear();
        }
    }

    private void b(sc scVar) {
        synchronized (this.n) {
            for (el a2 : this.j.a()) {
                a2.a(scVar);
            }
            for (ResultReceiver a3 : this.i) {
                u.a(a3, scVar);
            }
            this.i.clear();
        }
    }

    public void c() {
        cg.a((Closeable) this.h);
        this.f.b();
    }

    @NonNull
    public Context d() {
        return this.a;
    }

    public void a(@Nullable t.a aVar) {
        ResultReceiver resultReceiver;
        List<String> list = null;
        if (aVar != null) {
            list = aVar.a();
            resultReceiver = aVar.b();
        } else {
            resultReceiver = null;
        }
        boolean a2 = this.d.a(list);
        if (!a2) {
            u.a(resultReceiver, this.d.e());
        }
        if (this.d.c()) {
            synchronized (this.n) {
                if (a2) {
                    this.i.add(resultReceiver);
                }
            }
            this.h.e();
        }
    }

    public qe e() {
        return this.k;
    }

    public void a(@NonNull db dbVar) {
        this.d.a(dbVar.a);
        a(dbVar.b);
    }
}
