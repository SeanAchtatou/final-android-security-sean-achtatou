package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public class vj {
    private final vm a;
    private final vm b;
    private final vg c;
    @NonNull
    private final tp d;
    private final String e;

    public vj(int i, int i2, int i3, @NonNull String str, @NonNull tp tpVar) {
        this.c = new vg(i);
        this.a = new vm(i2, str + "map key", tpVar);
        this.b = new vm(i3, str + "map value", tpVar);
        this.e = str;
        this.d = tpVar;
    }

    public vm a() {
        return this.a;
    }

    public vm b() {
        return this.b;
    }

    public vg c() {
        return this.c;
    }

    public void a(@NonNull String str) {
        if (this.d.c()) {
            this.d.b("The %s has reached the limit of %d items. Item with key %s will be ignored", this.e, Integer.valueOf(this.c.a()), str);
        }
    }

    @NonNull
    public static vj a(@NonNull String str, @NonNull tp tpVar) {
        return new vj(30, 50, 100, str, tpVar);
    }
}
