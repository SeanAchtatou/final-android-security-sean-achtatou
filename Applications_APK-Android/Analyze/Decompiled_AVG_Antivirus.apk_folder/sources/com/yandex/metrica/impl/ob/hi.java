package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;

public abstract class hi implements hl<hn> {
    private final di a;
    @NonNull
    private final hr b;
    private final hv c;
    private final hq d;

    public hi(@NonNull di diVar, @NonNull hr hrVar, @NonNull hv hvVar, @NonNull hq hqVar) {
        this.a = diVar;
        this.b = hrVar;
        this.c = hvVar;
        this.d = hqVar;
    }

    @Nullable
    public final hm a() {
        if (this.c.i()) {
            return new hm(this.a, this.c, b());
        }
        return null;
    }

    @NonNull
    public final hm a(@NonNull hn hnVar) {
        if (this.c.i()) {
            rh.a(this.a.j()).reportEvent("create session with non-empty storage");
        }
        return new hm(this.a, this.c, b(hnVar));
    }

    @NonNull
    private ho b(@NonNull hn hnVar) {
        long a2 = this.b.a();
        this.c.d(a2).b(TimeUnit.MILLISECONDS.toSeconds(hnVar.a)).e(hnVar.a).a(0).a(true).h();
        this.a.i().a(a2, this.d.a(), TimeUnit.MILLISECONDS.toSeconds(hnVar.b));
        return b();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @NonNull
    public ho b() {
        return ho.a(this.d).a(this.c.g()).c(this.c.d()).b(this.c.c()).a(this.c.b()).d(this.c.e()).e(this.c.f()).a();
    }
}
