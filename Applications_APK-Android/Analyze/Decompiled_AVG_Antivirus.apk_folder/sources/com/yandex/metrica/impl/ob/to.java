package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

public class to {
    public static long a(String str, long j) {
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e) {
            return j;
        }
    }

    @Nullable
    public static Long a(String str) {
        try {
            return Long.valueOf(Long.parseLong(str));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Nullable
    public static Float b(String str) {
        try {
            return Float.valueOf(Float.parseFloat(str));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Nullable
    public static Integer c(String str) {
        try {
            return Integer.valueOf(Integer.parseInt(str));
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
