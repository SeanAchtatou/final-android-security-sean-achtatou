package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public class lz implements lv, lw {
    @NonNull
    private final dz a;
    private long b;

    public lz(@NonNull jk jkVar, @NonNull dz dzVar) {
        this.a = dzVar;
        this.b = jkVar.b();
        jkVar.a(this);
    }

    public boolean a() {
        return this.b >= ((long) ((qp) this.a.d()).U());
    }

    public void a(int i) {
        this.b++;
    }

    public void b(int i) {
        this.b--;
    }
}
