package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pp;
import java.util.ArrayList;
import java.util.List;

public class le implements ld<nt, pp.a.d> {
    @NonNull
    public /* synthetic */ Object b(@NonNull Object obj) {
        return a((List<nt>) ((List) obj));
    }

    @NonNull
    public pp.a.d[] a(@NonNull List<nt> list) {
        pp.a.d[] dVarArr = new pp.a.d[list.size()];
        for (int i = 0; i < list.size(); i++) {
            dVarArr[i] = a(list.get(i));
        }
        return dVarArr;
    }

    @NonNull
    public List<nt> a(@NonNull pp.a.d[] dVarArr) {
        ArrayList arrayList = new ArrayList(dVarArr.length);
        for (pp.a.d a : dVarArr) {
            arrayList.add(a(a));
        }
        return arrayList;
    }

    @NonNull
    private pp.a.d a(@NonNull nt ntVar) {
        pp.a.d dVar = new pp.a.d();
        dVar.b = ntVar.a;
        dVar.c = ntVar.b;
        return dVar;
    }

    @NonNull
    private nt a(@NonNull pp.a.d dVar) {
        return new nt(dVar.b, dVar.c);
    }
}
