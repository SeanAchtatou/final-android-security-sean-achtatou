package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.io.File;

public class it implements Runnable {
    @NonNull
    private final File a;
    @NonNull
    private final uc<File> b;

    public it(@NonNull File file, @NonNull uc<File> ucVar) {
        this.a = file;
        this.b = ucVar;
    }

    public void run() {
        File[] listFiles;
        if (this.a.exists() && this.a.isDirectory() && (listFiles = this.a.listFiles()) != null) {
            for (File a2 : listFiles) {
                this.b.a(a2);
            }
        }
    }
}
