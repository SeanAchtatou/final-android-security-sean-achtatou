package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.mh;

public class ms {
    @NonNull
    public mh.a a;
    @Nullable
    private Long b;
    private long c;
    private long d;
    @NonNull
    private Location e;

    public ms(@NonNull mh.a aVar, long j, long j2, @NonNull Location location) {
        this(aVar, j, j2, location, null);
    }

    public ms(@NonNull mh.a aVar, long j, long j2, @NonNull Location location, @Nullable Long l) {
        this.a = aVar;
        this.b = l;
        this.c = j;
        this.d = j2;
        this.e = location;
    }

    public String toString() {
        return "LocationWrapper{collectionMode=" + this.a + ", mIncrementalId=" + this.b + ", mReceiveTimestamp=" + this.c + ", mReceiveElapsedRealtime=" + this.d + ", mLocation=" + this.e + '}';
    }

    @Nullable
    public Long a() {
        return this.b;
    }

    public long b() {
        return this.c;
    }

    @NonNull
    public Location c() {
        return this.e;
    }

    public long d() {
        return this.d;
    }
}
