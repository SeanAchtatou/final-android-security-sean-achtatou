package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.HashSet;

public class cd {
    private final a a;
    @Nullable
    private Boolean b;
    private final HashSet<String> c = new HashSet<>();
    private final HashSet<String> d = new HashSet<>();

    public interface a {
        @Nullable
        Boolean a();

        void a(boolean z);
    }

    public static class b implements a {
        private final kj a;

        public b(@NonNull kj kjVar) {
            this.a = kjVar;
        }

        public void a(boolean z) {
            this.a.e(z).n();
        }

        @Nullable
        public Boolean a() {
            return this.a.h();
        }
    }

    public cd(@NonNull a aVar) {
        this.a = aVar;
        this.b = this.a.a();
    }

    public synchronized void a(@Nullable Boolean bool) {
        if (cg.a(bool) || this.b == null) {
            this.b = Boolean.valueOf(Boolean.FALSE.equals(bool));
            this.a.a(this.b.booleanValue());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
     arg types: [java.lang.Boolean, int]
     candidates:
      com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
      com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
      com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
      com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
      com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
      com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean */
    public synchronized void a(@NonNull String str, @Nullable Boolean bool) {
        if (cg.a(bool) || (!this.d.contains(str) && !this.c.contains(str))) {
            if (ua.a(bool, true)) {
                this.d.add(str);
                this.c.remove(str);
            } else {
                this.c.add(str);
                this.d.remove(str);
            }
        }
    }

    public synchronized boolean a() {
        boolean z;
        if (this.b == null) {
            z = this.d.isEmpty() && this.c.isEmpty();
        } else {
            z = this.b.booleanValue();
        }
        return z;
    }

    public synchronized boolean b() {
        return this.b == null ? this.d.isEmpty() : this.b.booleanValue();
    }

    public synchronized boolean c() {
        return e();
    }

    public synchronized boolean d() {
        return e();
    }

    private boolean e() {
        Boolean bool = this.b;
        if (bool == null) {
            return !this.c.isEmpty() || this.d.isEmpty();
        }
        return bool.booleanValue();
    }
}
