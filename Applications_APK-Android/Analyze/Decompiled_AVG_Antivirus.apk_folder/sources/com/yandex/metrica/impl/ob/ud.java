package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ud<K, V> {
    private final HashMap<K, Collection<V>> a = new HashMap<>();

    public int a() {
        int i = 0;
        for (Collection<V> size : this.a.values()) {
            i += size.size();
        }
        return i;
    }

    @Nullable
    public Collection<V> a(@Nullable String str) {
        return this.a.get(str);
    }

    @Nullable
    public Collection<V> a(@Nullable K k, @Nullable V v) {
        Collection collection;
        Collection collection2 = this.a.get(k);
        if (collection2 == null) {
            collection = c();
        } else {
            collection = a(collection2);
        }
        collection.add(v);
        return this.a.put(k, collection);
    }

    @Nullable
    public Collection<V> b(@Nullable K k) {
        return this.a.remove(k);
    }

    @Nullable
    public Collection<V> b(@Nullable K k, @Nullable V v) {
        Collection collection = this.a.get(k);
        if (collection == null || !collection.remove(v)) {
            return null;
        }
        return a(collection);
    }

    @NonNull
    private Collection<V> c() {
        return new ArrayList();
    }

    @NonNull
    private Collection<V> a(@NonNull Collection<Integer> collection) {
        return new ArrayList(collection);
    }

    @NonNull
    public Set<? extends Map.Entry<K, ? extends Collection<V>>> b() {
        return this.a.entrySet();
    }

    public String toString() {
        return this.a.toString();
    }
}
