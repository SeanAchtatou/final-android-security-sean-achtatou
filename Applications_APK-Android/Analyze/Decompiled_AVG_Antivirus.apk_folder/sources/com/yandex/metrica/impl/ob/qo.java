package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.HashMap;
import java.util.Map;

public class qo {
    private final Map<String, String> a = new HashMap();

    public qo() {
        this.a.put("android_id", "a");
        this.a.put("background_location_collection", "blc");
        this.a.put("background_lbs_collection", "blbc");
        this.a.put("easy_collecting", "ec");
        this.a.put("access_point", "ap");
        this.a.put("cells_around", "ca");
        this.a.put("google_aid", "g");
        this.a.put("own_macs", "om");
        this.a.put("sim_imei", "sm");
        this.a.put("sim_info", "si");
        this.a.put("wifi_around", "wa");
        this.a.put("wifi_connected", "wc");
        this.a.put("features_collecting", "fc");
        this.a.put("foreground_location_collection", "flc");
        this.a.put("foreground_lbs_collection", "flbc");
        this.a.put("package_info", "pi");
        this.a.put("permissions_collecting", "pc");
        this.a.put("sdk_list", "sl");
        this.a.put("socket", "s");
        this.a.put("telephony_restricted_to_location_tracking", "trtlt");
    }

    @NonNull
    public String a(@NonNull String str) {
        if (this.a.containsKey(str)) {
            return this.a.get(str);
        }
        return str;
    }
}
