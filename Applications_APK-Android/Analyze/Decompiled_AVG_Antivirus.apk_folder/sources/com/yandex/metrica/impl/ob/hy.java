package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

class hy {
    @NonNull
    kj a;
    @NonNull
    private final dc b;
    @NonNull
    private String c;
    @NonNull
    private hx d;
    @NonNull
    private sc e;

    public hy(@NonNull Context context, @NonNull sc scVar) {
        this(context.getPackageName(), new kj(jo.a(context).c()), new hx(), scVar);
    }

    @NonNull
    public Bundle a() {
        Bundle bundle = new Bundle();
        this.d.a(bundle, this.c, this.a.g());
        return bundle;
    }

    @VisibleForTesting
    hy(@NonNull String str, @NonNull kj kjVar, @NonNull hx hxVar, @NonNull sc scVar) {
        this.c = str;
        this.a = kjVar;
        this.d = hxVar;
        this.e = scVar;
        this.b = new dc(this.c);
    }
}
