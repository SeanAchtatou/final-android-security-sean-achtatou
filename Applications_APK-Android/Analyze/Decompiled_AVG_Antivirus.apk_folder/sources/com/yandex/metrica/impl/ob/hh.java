package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class hh {
    @NonNull
    private final hf a;
    @NonNull
    private final hg b;
    @NonNull
    private final jj c;
    @NonNull
    private final String d;

    public hh(@NonNull Context context, @NonNull df dfVar) {
        this(new hg(), new hf(), jo.a(context).c(dfVar), "event_hashes");
    }

    @VisibleForTesting
    hh(@NonNull hg hgVar, @NonNull hf hfVar, @NonNull jj jjVar, @NonNull String str) {
        this.b = hgVar;
        this.a = hfVar;
        this.c = jjVar;
        this.d = str;
    }

    @NonNull
    public he a() {
        try {
            byte[] a2 = this.c.a(this.d);
            if (cg.a(a2)) {
                return this.a.a(this.b.c());
            }
            return this.a.a(this.b.b(a2));
        } catch (Throwable th) {
            return this.a.a(this.b.c());
        }
    }

    public void a(@NonNull he heVar) {
        this.c.a(this.d, this.b.a((e) this.a.b(heVar)));
    }
}
