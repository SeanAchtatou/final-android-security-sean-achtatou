package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.os.ResultReceiver;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.g;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class da implements Parcelable {
    public static final Parcelable.Creator<da> CREATOR = new Parcelable.Creator<da>() {
        /* renamed from: a */
        public da createFromParcel(Parcel parcel) {
            Bundle readBundle = parcel.readBundle(u.class.getClassLoader());
            return new da((ContentValues) readBundle.getParcelable("CFG_KEY_PROCESS_ENVIRONMENT"), (ResultReceiver) readBundle.getParcelable("CFG_KEY_PROCESS_ENVIRONMENT_RECEIVER"));
        }

        /* renamed from: a */
        public da[] newArray(int i) {
            return new da[i];
        }
    };
    public static final String a = UUID.randomUUID().toString();
    @NonNull
    private final ContentValues b;
    @Nullable
    private ResultReceiver c;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("CFG_KEY_PROCESS_ENVIRONMENT", this.b);
        bundle.putParcelable("CFG_KEY_PROCESS_ENVIRONMENT_RECEIVER", this.c);
        dest.writeBundle(bundle);
    }

    public String toString() {
        return "ProcessConfiguration{mParamsMapping=" + this.b + ", mDataResultReceiver=" + this.c + '}';
    }

    @Nullable
    public static da a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        try {
            return (da) bundle.getParcelable("PROCESS_CFG_OBJ");
        } catch (Throwable th) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public da(Context context, @Nullable ResultReceiver resultReceiver) {
        this.b = new ContentValues();
        this.b.put("PROCESS_CFG_PROCESS_ID", Integer.valueOf(Process.myPid()));
        this.b.put("PROCESS_CFG_PROCESS_SESSION_ID", a);
        this.b.put("PROCESS_CFG_SDK_API_LEVEL", (Integer) 81);
        this.b.put("PROCESS_CFG_PACKAGE_NAME", context.getPackageName());
        this.c = resultReceiver;
    }

    public da(da daVar) {
        synchronized (daVar) {
            this.b = new ContentValues(daVar.b);
            this.c = daVar.c;
        }
    }

    public da(@NonNull ContentValues contentValues, @Nullable ResultReceiver resultReceiver) {
        this.b = contentValues == null ? new ContentValues() : contentValues;
        this.c = resultReceiver;
    }

    public void a(@Nullable g gVar) {
        if (gVar != null) {
            synchronized (this) {
                b(gVar);
                c(gVar);
                d(gVar);
            }
        }
    }

    private void b(@NonNull g gVar) {
        if (cg.a((Object) gVar.d)) {
            a(gVar.d);
        }
    }

    private void c(@NonNull g gVar) {
        if (cg.a((Object) gVar.b)) {
            a(tu.c(gVar.b));
        }
    }

    private void d(@NonNull g gVar) {
        if (cg.a((Object) gVar.c)) {
            a(gVar.c);
        }
    }

    public boolean a() {
        return this.b.containsKey("PROCESS_CFG_CUSTOM_HOSTS");
    }

    @Nullable
    public List<String> b() {
        String asString = this.b.getAsString("PROCESS_CFG_CUSTOM_HOSTS");
        if (TextUtils.isEmpty(asString)) {
            return null;
        }
        return th.b(asString);
    }

    public synchronized void a(@Nullable List<String> list) {
        this.b.put("PROCESS_CFG_CUSTOM_HOSTS", th.a(list));
    }

    @Nullable
    public Map<String, String> c() {
        return th.a(this.b.getAsString("PROCESS_CFG_CLIDS"));
    }

    public synchronized void a(@Nullable Map<String, String> map) {
        this.b.put("PROCESS_CFG_CLIDS", th.a((Map) map));
    }

    @Nullable
    public String d() {
        return this.b.getAsString("PROCESS_CFG_DISTRIBUTION_REFERRER");
    }

    public synchronized void a(@Nullable String str) {
        this.b.put("PROCESS_CFG_DISTRIBUTION_REFERRER", str);
    }

    @NonNull
    public Integer e() {
        return this.b.getAsInteger("PROCESS_CFG_PROCESS_ID");
    }

    @NonNull
    public String f() {
        return this.b.getAsString("PROCESS_CFG_PROCESS_SESSION_ID");
    }

    public int g() {
        return this.b.getAsInteger("PROCESS_CFG_SDK_API_LEVEL").intValue();
    }

    @NonNull
    public String h() {
        return this.b.getAsString("PROCESS_CFG_PACKAGE_NAME");
    }

    @Nullable
    public ResultReceiver i() {
        return this.c;
    }

    public synchronized void b(@NonNull Bundle bundle) {
        bundle.putParcelable("PROCESS_CFG_OBJ", this);
    }
}
