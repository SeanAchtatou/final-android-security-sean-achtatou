package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.qj;

public abstract class qu<T extends qj> implements qs<T> {
    @Nullable
    private up a;

    public void a(@NonNull Uri.Builder builder, @NonNull T t) {
        up upVar = this.a;
        if (upVar != null && upVar.a() == uq.AES_RSA) {
            builder.appendQueryParameter("encrypted_request", "1");
        }
    }

    public String a(Boolean bool) {
        if (bool == null) {
            return "";
        }
        return String.valueOf(bool.booleanValue() ? "1" : "0");
    }

    public void a(@NonNull up upVar) {
        this.a = upVar;
    }
}
