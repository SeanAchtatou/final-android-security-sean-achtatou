package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public class dm {
    @NonNull
    private final kh a;

    public dm(@NonNull kh khVar) {
        this.a = khVar;
    }

    public int a() {
        int a2 = this.a.a();
        this.a.b(a2 + 1).n();
        return a2;
    }

    public int a(int i) {
        int a2 = this.a.a(i);
        this.a.a(i, a2 + 1).n();
        return a2;
    }
}
