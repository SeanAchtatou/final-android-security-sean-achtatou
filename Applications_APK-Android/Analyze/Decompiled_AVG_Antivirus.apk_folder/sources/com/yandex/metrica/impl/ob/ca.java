package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.cb;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ca implements bd {
    @NonNull
    private final sd a;
    @NonNull
    private final qq b;
    @NonNull
    private final cb c;
    @NonNull
    private final kx d;

    public ca(@NonNull sd sdVar, @NonNull qq qqVar) {
        this(sdVar, qqVar, new cb(), new kx());
    }

    public boolean a(int i, byte[] bArr, @NonNull Map<String, List<String>> map) {
        if (200 != i) {
            return false;
        }
        List list = map.get("Content-Encoding");
        if (!cg.a((Collection) list) && "encrypted".equals(list.get(0))) {
            bArr = this.d.a(bArr, "hBnBQbZrmjPXEWVJ");
        }
        if (bArr == null) {
            return false;
        }
        cb.b a2 = this.c.a(bArr);
        if (cb.b.a.OK != a2.l()) {
            return false;
        }
        this.a.a(a2, this.b, map);
        return true;
    }

    @VisibleForTesting
    ca(@NonNull sd sdVar, @NonNull qq qqVar, @NonNull cb cbVar, @NonNull kx kxVar) {
        this.a = sdVar;
        this.b = qqVar;
        this.c = cbVar;
        this.d = kxVar;
    }
}
