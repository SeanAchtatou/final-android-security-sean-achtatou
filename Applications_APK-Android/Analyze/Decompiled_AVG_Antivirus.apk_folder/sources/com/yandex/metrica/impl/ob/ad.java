package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public enum ad {
    UNKNOWN(0),
    FIRST_OCCURRENCE(1),
    NON_FIRST_OCCURENCE(2);
    
    public final int d;

    private ad(int i) {
        this.d = i;
    }

    @NonNull
    public static ad a(@Nullable Integer num) {
        if (num != null) {
            for (ad adVar : values()) {
                if (adVar.d == num.intValue()) {
                    return adVar;
                }
            }
        }
        return UNKNOWN;
    }
}
