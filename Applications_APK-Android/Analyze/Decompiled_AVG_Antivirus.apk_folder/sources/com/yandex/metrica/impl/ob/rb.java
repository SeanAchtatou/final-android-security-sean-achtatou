package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class rb {
    @NonNull
    public List<rd> a(@NonNull List<rd> list) {
        ArrayList arrayList = new ArrayList();
        for (rd next : list) {
            ArrayList arrayList2 = new ArrayList(next.b.size());
            for (String next2 : next.b) {
                if (bx.b(next2)) {
                    arrayList2.add(next2);
                }
            }
            if (!arrayList2.isEmpty()) {
                arrayList.add(new rd(next.a, arrayList2));
            }
        }
        return arrayList;
    }

    @NonNull
    public JSONObject b(@NonNull List<rd> list) {
        JSONObject jSONObject = new JSONObject();
        for (rd next : list) {
            try {
                jSONObject.put(next.a, new JSONObject().put("classes", new JSONArray((Collection) next.b)));
            } catch (JSONException e) {
            }
        }
        return jSONObject;
    }
}
