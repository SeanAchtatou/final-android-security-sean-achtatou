package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class ea {
    @NonNull
    private final kh a;
    @NonNull
    private hs b;
    @NonNull
    private jk c;
    @NonNull
    private final um d;
    @NonNull
    private final i e;
    @NonNull
    private final dm f;
    @NonNull
    private a g;
    @NonNull
    private final tx h;
    private final int i;
    private long j;
    private long k;
    private int l;

    public interface a {
        void a();
    }

    public ea(@NonNull kh khVar, @NonNull hs hsVar, @NonNull jk jkVar, @NonNull i iVar, @NonNull um umVar, int i2, @NonNull a aVar) {
        this(khVar, hsVar, jkVar, iVar, umVar, i2, aVar, new dm(khVar), new tw());
    }

    @VisibleForTesting
    public ea(@NonNull kh khVar, @NonNull hs hsVar, @NonNull jk jkVar, @NonNull i iVar, @NonNull um umVar, int i2, @NonNull a aVar, @NonNull dm dmVar, @NonNull tx txVar) {
        this.a = khVar;
        this.b = hsVar;
        this.c = jkVar;
        this.e = iVar;
        this.d = umVar;
        this.i = i2;
        this.f = dmVar;
        this.h = txVar;
        this.g = aVar;
        this.j = this.a.a(0L);
        this.k = this.a.b();
        this.l = this.a.c();
    }

    public void a(t tVar) {
        a(tVar, this.b.a(tVar.s() / 1000));
    }

    public void b(t tVar) {
        this.b.c(tVar);
    }

    public void c(t tVar) {
        f(tVar);
        f();
    }

    public void d(t tVar) {
        f(tVar);
        a();
    }

    public void e(t tVar) {
        f(tVar);
        b();
    }

    public void f(t tVar) {
        a(tVar, this.b.d(tVar));
    }

    @VisibleForTesting
    public void a(@NonNull t tVar, @NonNull ht htVar) {
        if (TextUtils.isEmpty(tVar.l())) {
            tVar.a(this.a.f());
        }
        tVar.d(this.a.h());
        ht htVar2 = htVar;
        this.c.a(this.d.a(tVar).a(tVar), tVar.g(), htVar2, this.e.b(), this.f);
        this.g.a();
    }

    private void f() {
        this.j = this.h.b();
        this.a.b(this.j).n();
    }

    public void a() {
        this.k = this.h.b();
        this.a.c(this.k).n();
    }

    public void b() {
        this.l = this.i;
        this.a.c(this.l).n();
    }

    public boolean c() {
        return this.h.b() - this.j > hp.a;
    }

    public long d() {
        return this.k;
    }

    public boolean e() {
        return this.l < this.i;
    }
}
