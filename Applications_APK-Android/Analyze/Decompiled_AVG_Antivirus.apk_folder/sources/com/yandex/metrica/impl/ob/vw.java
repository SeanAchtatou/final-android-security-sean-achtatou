package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import java.util.LinkedList;
import java.util.List;

public class vw implements vx<List<vv>> {
    public /* bridge */ /* synthetic */ vv a(@Nullable Object obj) {
        return a((List<vv>) ((List) obj));
    }

    public vv a(@Nullable List<vv> list) {
        LinkedList linkedList = new LinkedList();
        boolean z = true;
        for (vv next : list) {
            if (!next.a()) {
                linkedList.add(next.b());
                z = false;
            }
        }
        if (z) {
            return vv.a(this);
        }
        return vv.a(this, TextUtils.join(", ", linkedList));
    }
}
