package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.SparseArray;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.core.os.EnvironmentCompat;
import java.util.concurrent.Callable;

@SuppressLint({"NewApi"})
public class so implements sw<sm> {
    /* access modifiers changed from: private */
    @SuppressLint({"InlineApi"})
    public static final SparseArray<String> a = new SparseArray<String>() {
        {
            put(0, null);
            put(7, "1xRTT");
            put(4, "CDMA");
            put(2, "EDGE");
            put(14, "eHRPD");
            put(5, "EVDO rev.0");
            put(6, "EVDO rev.A");
            put(12, "EVDO rev.B");
            put(1, "GPRS");
            put(8, "HSDPA");
            put(10, "HSPA");
            put(15, "HSPA+");
            put(9, "HSUPA");
            put(11, "iDen");
            put(3, "UMTS");
            put(12, "EVDO rev.B");
            if (cg.a(11)) {
                put(14, "eHRPD");
                put(13, "LTE");
                if (cg.a(13)) {
                    put(15, "HSPA+");
                }
            }
        }
    };
    /* access modifiers changed from: private */
    @NonNull
    public final sp b;
    /* access modifiers changed from: private */
    @NonNull
    public nq c;

    public so(@NonNull sp spVar, @NonNull nq nqVar) {
        this.b = spVar;
        this.c = nqVar;
    }

    @Nullable
    /* renamed from: a */
    public sm d() {
        if (this.b.i()) {
            return new sm(f(), g(), c(), b(), i(), h(), null, true, 0, null, null);
        }
        return null;
    }

    @Nullable
    private Integer f() {
        final TelephonyManager c2 = this.b.c();
        return (Integer) cg.a(new Callable<Integer>() {
            /* renamed from: a */
            public Integer call() {
                String substring = c2.getNetworkOperator().substring(0, 3);
                if (!TextUtils.isEmpty(substring)) {
                    return Integer.valueOf(Integer.parseInt(substring));
                }
                return null;
            }
        }, c2, "getting phoneMcc", "TelephonyManager");
    }

    @Nullable
    private Integer g() {
        return (Integer) cg.a(new Callable<Integer>() {
            /* renamed from: a */
            public Integer call() {
                String substring = so.this.b.c().getNetworkOperator().substring(3);
                if (!TextUtils.isEmpty(substring)) {
                    return Integer.valueOf(Integer.parseInt(substring));
                }
                return null;
            }
        }, this.b.c(), "getting phoneMnc", "TelephonyManager");
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @SuppressLint({"MissingPermission"})
    @Nullable
    public Integer b() {
        final TelephonyManager c2 = this.b.c();
        return (Integer) cg.a(new Callable<Integer>() {
            /* renamed from: a */
            public Integer call() {
                int i;
                if (!so.this.c.c(so.this.b.d())) {
                    return null;
                }
                GsmCellLocation gsmCellLocation = (GsmCellLocation) c2.getCellLocation();
                if (gsmCellLocation != null) {
                    i = gsmCellLocation.getCid();
                } else {
                    i = 1;
                }
                if (1 != i) {
                    return Integer.valueOf(i);
                }
                return null;
            }
        }, c2, "getting phoneCellId", "TelephonyManager");
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @SuppressLint({"MissingPermission"})
    @Nullable
    public Integer c() {
        final TelephonyManager c2 = this.b.c();
        return (Integer) cg.a(new Callable<Integer>() {
            /* renamed from: a */
            public Integer call() {
                CellLocation cellLocation;
                int lac;
                if (!so.this.c.c(so.this.b.d()) || (cellLocation = c2.getCellLocation()) == null || 1 == (lac = ((GsmCellLocation) cellLocation).getLac())) {
                    return null;
                }
                return Integer.valueOf(lac);
            }
        }, c2, "getting phoneLac", "TelephonyManager");
    }

    @NonNull
    private String h() {
        final TelephonyManager c2 = this.b.c();
        return (String) cg.a(new Callable<String>() {
            /* renamed from: a */
            public String call() {
                return (String) so.a.get(c2.getNetworkType(), EnvironmentCompat.MEDIA_UNKNOWN);
            }
        }, c2, "getting networkType", "TelephonyManager", EnvironmentCompat.MEDIA_UNKNOWN);
    }

    @Nullable
    private String i() {
        final TelephonyManager c2 = this.b.c();
        return (String) cg.a(new Callable<String>() {
            /* renamed from: a */
            public String call() {
                return c2.getNetworkOperatorName();
            }
        }, c2, "getting network operator name", "TelephonyManager");
    }
}
