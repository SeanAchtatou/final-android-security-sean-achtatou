package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.yandex.metrica.impl.ob.bl;
import com.yandex.metrica.impl.ob.i;
import com.yandex.metrica.impl.ob.pg;
import com.yandex.metrica.impl.ob.th;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.jvm.internal.LongCompanionObject;
import org.json.JSONException;
import org.json.JSONObject;

class bo extends bf<qv> {
    pg.c j;
    jk k;
    List<Long> l;
    int m;
    int n;
    @NonNull
    private final di o;
    private final Map<String, String> p;
    private qn q;
    private c r;
    private final vo<byte[]> s;
    private final tp t;
    @Nullable
    private qp u;
    @NonNull
    private final kh v;
    private int w;

    public bo(di diVar) {
        this(diVar, diVar.i(), new kh(jo.a(diVar.j()).b(diVar.b())));
    }

    bo(@NonNull di diVar, @NonNull jk jkVar, @NonNull kh khVar) {
        this(diVar, jkVar, new qv(), khVar);
    }

    @VisibleForTesting
    bo(@NonNull di diVar, @NonNull jk jkVar, @NonNull qv qvVar, @NonNull kh khVar) {
        super(qvVar);
        this.p = new LinkedHashMap();
        this.m = 0;
        this.n = -1;
        this.o = diVar;
        this.k = jkVar;
        this.t = diVar.k();
        this.s = new vf(245760, "event value in ReportTask", this.t);
        this.v = khVar;
    }

    public boolean b() {
        boolean b2 = super.b();
        this.o.A().a();
        return b2;
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Uri.Builder builder) {
        ((qv) this.i).a(builder, this.u);
    }

    /* access modifiers changed from: package-private */
    public void a(@NonNull ContentValues contentValues) {
        this.p.clear();
        for (Map.Entry next : contentValues.valueSet()) {
            this.p.put(next.getKey(), next.getValue().toString());
        }
        String asString = contentValues.getAsString("report_request_parameters");
        if (!TextUtils.isEmpty(asString)) {
            try {
                this.q = new qn(new th.a(asString));
                ((qv) this.i).a(this.q);
            } catch (Throwable th) {
                H();
            }
        } else {
            H();
        }
    }

    private void H() {
        this.q = new qn();
        ((qv) this.i).a(this.q);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public pg.c a(c cVar, pg.c.C0006c[] cVarArr) {
        pg.c cVar2 = new pg.c();
        pg.c.d dVar = new pg.c.d();
        dVar.b = ua.a(this.q.b, this.u.t());
        dVar.c = ua.a(this.q.a, this.u.r());
        this.m += b.b(4, dVar);
        cVar2.c = dVar;
        a(cVar2);
        cVar2.b = (pg.c.e[]) cVar.a.toArray(new pg.c.e[cVar.a.size()]);
        cVar2.d = a(cVar.c);
        cVar2.e = cVarArr;
        this.m += b.h(8);
        return cVar2;
    }

    /* access modifiers changed from: package-private */
    public void a(final pg.c cVar) {
        af.a().k().a(new sy() {
            public void a(sx sxVar) {
                b(sxVar, cVar);
                a(sxVar, cVar);
            }

            private void a(sx sxVar, pg.c cVar) {
                List<st> a2 = sxVar.a();
                if (!cg.a((Collection) a2)) {
                    cVar.g = new pg.c.f[a2.size()];
                    for (int i = 0; i < a2.size(); i++) {
                        cVar.g[i] = bl.a(a2.get(i));
                        bo.this.m += b.b(cVar.g[i]);
                        bo.this.m += b.h(10);
                    }
                }
            }

            private void b(sx sxVar, pg.c cVar) {
                List<String> c = sxVar.c();
                if (!cg.a((Collection) c)) {
                    cVar.f = new String[c.size()];
                    for (int i = 0; i < c.size(); i++) {
                        String str = c.get(i);
                        if (!TextUtils.isEmpty(str)) {
                            cVar.f[i] = str;
                            bo.this.m += b.b(cVar.f[i]);
                            bo.this.m += b.h(9);
                        }
                    }
                }
            }
        });
    }

    public boolean a() {
        List<ContentValues> c2 = this.o.i().c();
        if (c2.isEmpty()) {
            return false;
        }
        a(c2.get(0));
        this.u = this.o.h();
        a(this.u.b());
        if (!this.u.L() || cg.a((Collection) s())) {
            return false;
        }
        this.l = null;
        pg.c.C0006c[] D = D();
        this.r = E();
        if (this.r.a.isEmpty()) {
            return false;
        }
        this.w = this.v.l() + 1;
        ((qv) this.i).a(this.w);
        this.j = a(this.r, D);
        this.l = this.r.b;
        c(e.a(this.j));
        return true;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public pg.c.C0006c[] D() {
        pg.c.C0006c[] a2 = bl.a(this.o.j());
        if (a2 != null) {
            int length = a2.length;
            for (int i = 0; i < length; i++) {
                this.m += b.b(a2[i]);
            }
        }
        return a2;
    }

    private pg.c.a[] a(JSONObject jSONObject) {
        int length = jSONObject.length();
        if (length <= 0) {
            return null;
        }
        pg.c.a[] aVarArr = new pg.c.a[length];
        Iterator<String> keys = jSONObject.keys();
        int i = 0;
        while (keys.hasNext()) {
            String next = keys.next();
            try {
                pg.c.a aVar = new pg.c.a();
                aVar.b = next;
                aVar.c = jSONObject.getString(next);
                aVarArr[i] = aVar;
            } catch (JSONException e) {
            }
            i++;
        }
        return aVarArr;
    }

    /* access modifiers changed from: protected */
    public void C() {
        long[] jArr;
        I();
        pg.c.e[] eVarArr = this.j.b;
        for (int i = 0; i < eVarArr.length; i++) {
            pg.c.e eVar = eVarArr[i];
            this.k.a(this.l.get(i).longValue(), bl.a(eVar.c.d).a(), eVar.d.length);
            bl.a(eVar);
        }
        hm a2 = this.o.a().a();
        if (a2 == null) {
            jArr = new long[0];
        } else {
            jArr = new long[]{a2.c(), a2.c() - 1};
        }
        this.k.a(jArr);
    }

    private void I() {
        this.v.e(this.w);
    }

    public boolean t() {
        return super.t() & (400 != k());
    }

    public void f() {
        if (x()) {
            J();
        }
        this.r = null;
    }

    private void J() {
        if (this.t.c()) {
            for (int i = 0; i < this.r.a.size(); i++) {
                this.t.a(this.r.a.get(i), "Event sent");
            }
        }
    }

    /* access modifiers changed from: protected */
    public c E() {
        Cursor cursor;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        JSONObject jSONObject = new JSONObject();
        i.a aVar = null;
        try {
            cursor = F();
            while (true) {
                try {
                    if (!cursor.moveToNext()) {
                        break;
                    }
                    ContentValues contentValues = new ContentValues();
                    tc.a(cursor, contentValues);
                    long longValue = contentValues.getAsLong("id").longValue();
                    hw a2 = hw.a(contentValues.getAsInteger("type"));
                    pg.c.e.b a3 = bl.a(this.u.A(), bl.a(a2), bl.a(contentValues));
                    this.m += b.d(1, (long) LongCompanionObject.MAX_VALUE);
                    this.m += b.b(2, a3);
                    if (this.m >= 250880) {
                        break;
                    }
                    b a4 = a(longValue, a3);
                    if (a4 != null) {
                        if (aVar != null) {
                            if (!aVar.equals(a4.b)) {
                                break;
                            }
                        } else {
                            aVar = a4.b;
                        }
                        arrayList2.add(Long.valueOf(longValue));
                        arrayList.add(a4.a);
                        try {
                            jSONObject = new JSONObject(a4.b.a);
                        } catch (JSONException e) {
                        }
                        if (a4.c) {
                            break;
                        }
                    }
                } catch (Throwable th) {
                    th = th;
                    cg.a(cursor);
                    throw th;
                }
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            cg.a(cursor);
            throw th;
        }
        cg.a(cursor);
        return new c(arrayList, arrayList2, jSONObject);
    }

    private i.a b(ContentValues contentValues) {
        return new i.a(contentValues.getAsString("app_environment"), contentValues.getAsLong("app_environment_revision").longValue());
    }

    private int a(i.a aVar) {
        try {
            pg.c.a[] a2 = a(new JSONObject(aVar.a));
            if (a2 == null) {
                return 0;
            }
            int i = 0;
            for (pg.c.a b2 : a2) {
                i += b.b(7, b2);
            }
            return i;
        } catch (JSONException e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008c, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008f, code lost:
        r9 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x008c A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x0018] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.yandex.metrica.impl.ob.bo.b a(long r7, com.yandex.metrica.impl.ob.pg.c.e.b r9) {
        /*
            r6 = this;
            com.yandex.metrica.impl.ob.pg$c$e r0 = new com.yandex.metrica.impl.ob.pg$c$e
            r0.<init>()
            r0.b = r7
            r0.c = r9
            int r9 = r9.d
            com.yandex.metrica.impl.ob.hw r9 = com.yandex.metrica.impl.ob.bl.a(r9)
            r1 = 0
            r2 = 0
            android.database.Cursor r7 = r6.a(r7, r9)     // Catch:{ Throwable -> 0x0097, all -> 0x0091 }
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ Throwable -> 0x008e, all -> 0x008c }
            r8.<init>()     // Catch:{ Throwable -> 0x008e, all -> 0x008c }
            r9 = r1
        L_0x001e:
            boolean r3 = r7.moveToNext()     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            if (r3 == 0) goto L_0x0070
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            r3.<init>()     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            com.yandex.metrica.impl.ob.tc.a(r7, r3)     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            com.yandex.metrica.impl.ob.pg$c$e$a r4 = r6.d(r3)     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            if (r4 == 0) goto L_0x006f
            com.yandex.metrica.impl.ob.i$a r3 = r6.b(r3)     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            if (r9 != 0) goto L_0x004f
            int r9 = r6.n     // Catch:{ Throwable -> 0x004c, all -> 0x008c }
            if (r9 >= 0) goto L_0x004a
            int r9 = r6.a(r3)     // Catch:{ Throwable -> 0x004c, all -> 0x008c }
            r6.n = r9     // Catch:{ Throwable -> 0x004c, all -> 0x008c }
            int r9 = r6.m     // Catch:{ Throwable -> 0x004c, all -> 0x008c }
            int r5 = r6.n     // Catch:{ Throwable -> 0x004c, all -> 0x008c }
            int r9 = r9 + r5
            r6.m = r9     // Catch:{ Throwable -> 0x004c, all -> 0x008c }
        L_0x004a:
            r9 = r3
            goto L_0x0057
        L_0x004c:
            r8 = move-exception
            r9 = r3
            goto L_0x009a
        L_0x004f:
            boolean r3 = r9.equals(r3)     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            if (r3 != 0) goto L_0x0057
            r2 = 1
            goto L_0x0070
        L_0x0057:
            r6.a(r4)     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            int r3 = r6.m     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            r5 = 3
            int r5 = com.yandex.metrica.impl.ob.b.b(r5, r4)     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            int r3 = r3 + r5
            r6.m = r3     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            int r3 = r6.m     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            r5 = 250880(0x3d400, float:3.51558E-40)
            if (r3 < r5) goto L_0x006c
            goto L_0x0070
        L_0x006c:
            r8.add(r4)     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
        L_0x006f:
            goto L_0x001e
        L_0x0070:
            int r3 = r8.size()     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            if (r3 <= 0) goto L_0x0085
            int r1 = r8.size()     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            com.yandex.metrica.impl.ob.pg$c$e$a[] r1 = new com.yandex.metrica.impl.ob.pg.c.e.a[r1]     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            java.lang.Object[] r8 = r8.toArray(r1)     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            com.yandex.metrica.impl.ob.pg$c$e$a[] r8 = (com.yandex.metrica.impl.ob.pg.c.e.a[]) r8     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            r0.d = r8     // Catch:{ Throwable -> 0x008a, all -> 0x008c }
            goto L_0x009a
        L_0x0085:
            com.yandex.metrica.impl.ob.cg.a(r7)
            return r1
        L_0x008a:
            r8 = move-exception
            goto L_0x009a
        L_0x008c:
            r8 = move-exception
            goto L_0x0093
        L_0x008e:
            r8 = move-exception
            r9 = r1
            goto L_0x009a
        L_0x0091:
            r8 = move-exception
            r7 = r1
        L_0x0093:
            com.yandex.metrica.impl.ob.cg.a(r7)
            throw r8
        L_0x0097:
            r7 = move-exception
            r7 = r1
            r9 = r7
        L_0x009a:
            com.yandex.metrica.impl.ob.cg.a(r7)
            com.yandex.metrica.impl.ob.bo$b r7 = new com.yandex.metrica.impl.ob.bo$b
            r7.<init>(r0, r9, r2)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.bo.a(long, com.yandex.metrica.impl.ob.pg$c$e$b):com.yandex.metrica.impl.ob.bo$b");
    }

    /*  JADX ERROR: UnsupportedOperationException in pass: MethodInvokeVisitor
        java.lang.UnsupportedOperationException: ArgType.getObject(), call class: class jadx.core.dex.instructions.args.ArgType$ArrayArg
        	at jadx.core.dex.instructions.args.ArgType.getObject(ArgType.java:513)
        	at jadx.core.clsp.ClspGraph.getClsDetails(ClspGraph.java:76)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:100)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void a(com.yandex.metrica.impl.ob.pg.c.e.a r4) {
        /*
            r3 = this;
            com.yandex.metrica.impl.ob.vo<byte[]> r0 = r3.s
            byte[] r1 = r4.f
            java.lang.Object r0 = r0.a(r1)
            byte[] r0 = (byte[]) r0
            byte[] r1 = r4.f
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x001e
            r4.f = r0
            int r1 = r4.k
            byte[] r2 = r4.f
            int r2 = r2.length
            int r0 = r0.length
            int r2 = r2 - r0
            int r1 = r1 + r2
            r4.k = r1
        L_0x001e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.bo.a(com.yandex.metrica.impl.ob.pg$c$e$a):void");
    }

    private bl.c c(ContentValues contentValues) {
        return bl.c.a(contentValues.getAsInteger("type").intValue(), this.u.M()).b(contentValues.getAsInteger("custom_type")).a(contentValues.getAsString("name")).b(contentValues.getAsString("value")).a(contentValues.getAsLong("time").longValue()).a(contentValues.getAsInteger("number").intValue()).b(contentValues.getAsInteger("global_number").intValue()).c(contentValues.getAsInteger("number_of_type").intValue()).e(contentValues.getAsString("cell_info")).c(contentValues.getAsString("location_info")).d(contentValues.getAsString("wifi_network_info")).g(contentValues.getAsString("error_environment")).h(contentValues.getAsString("user_info")).d(contentValues.getAsInteger("truncated").intValue()).e(contentValues.getAsInteger(UrlManager.Parameter.CONNECTION_TYPE).intValue()).i(contentValues.getAsString("cellular_connection_type")).f(contentValues.getAsString("wifi_access_point")).j(contentValues.getAsString("profile_id")).a(un.a(contentValues.getAsInteger("encrypting_mode").intValue())).a(ad.a(contentValues.getAsInteger("first_occurrence_status")));
    }

    private pg.c.e.a d(ContentValues contentValues) {
        bl.c c2 = c(contentValues);
        if (c2.c() != null) {
            return c2.e();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Cursor F() {
        return this.k.a(this.p);
    }

    /* access modifiers changed from: protected */
    public Cursor a(long j2, hw hwVar) {
        return this.k.a(j2, hwVar);
    }

    static final class c {
        final List<pg.c.e> a;
        final List<Long> b;
        final JSONObject c;

        c(List<pg.c.e> list, List<Long> list2, JSONObject jSONObject) {
            this.a = list;
            this.b = list2;
            this.c = jSONObject;
        }
    }

    static final class b {
        final pg.c.e a;
        final i.a b;
        final boolean c;

        b(pg.c.e eVar, i.a aVar, boolean z) {
            this.a = eVar;
            this.b = aVar;
            this.c = z;
        }
    }

    public static a G() {
        return new a();
    }

    static class a {
        a() {
        }

        /* access modifiers changed from: package-private */
        public bo a(di diVar) {
            return new bo(diVar);
        }
    }
}
