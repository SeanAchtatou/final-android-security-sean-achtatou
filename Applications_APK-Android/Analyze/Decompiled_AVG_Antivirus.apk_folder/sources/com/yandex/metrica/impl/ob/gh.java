package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.yandex.metrica.f;
import com.yandex.metrica.impl.ob.tz;

public class gh extends ft {
    public gh(di diVar) {
        super(diVar);
    }

    public boolean a(@NonNull t tVar) {
        b(tVar);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(t tVar) {
        String l = tVar.l();
        f a = tz.a(l);
        String g = a().g();
        f a2 = tz.a(g);
        if (!a.equals(a2)) {
            if (a(a, a2)) {
                tVar.a(g);
                a(tVar, tz.a.LOGOUT);
            } else if (c(a, a2)) {
                a(tVar, tz.a.LOGIN);
            } else if (b(a, a2)) {
                a(tVar, tz.a.SWITCH);
            } else {
                a(tVar, tz.a.UPDATE);
            }
            a().a(l);
        }
    }

    private void a(t tVar, tz.a aVar) {
        tVar.c(tz.a(aVar));
        a().d().f(tVar);
    }

    private boolean a(f fVar, f fVar2) {
        return TextUtils.isEmpty(fVar.a()) && !TextUtils.isEmpty(fVar2.a());
    }

    private boolean b(f fVar, f fVar2) {
        return !TextUtils.isEmpty(fVar.a()) && !fVar.a().equals(fVar2.a());
    }

    private boolean c(f fVar, f fVar2) {
        return !TextUtils.isEmpty(fVar.a()) && TextUtils.isEmpty(fVar2.a());
    }
}
