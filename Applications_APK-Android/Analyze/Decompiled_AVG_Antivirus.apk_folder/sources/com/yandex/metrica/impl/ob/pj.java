package com.yandex.metrica.impl.ob;

import java.io.IOException;
import java.util.Arrays;

public final class pj extends e {
    public int b;
    public double c;
    public byte[] d;
    public byte[] e;
    public byte[] f;
    public a g;
    public long h;

    public static final class a extends e {
        public byte[] b;
        public byte[] c;

        public a() {
            d();
        }

        public a d() {
            this.b = g.h;
            this.c = g.h;
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (!Arrays.equals(this.b, g.h)) {
                bVar.a(1, this.b);
            }
            if (!Arrays.equals(this.c, g.h)) {
                bVar.a(2, this.c);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (!Arrays.equals(this.b, g.h)) {
                c2 += b.b(1, this.b);
            }
            if (!Arrays.equals(this.c, g.h)) {
                return c2 + b.b(2, this.c);
            }
            return c2;
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    this.b = aVar.j();
                } else if (a == 18) {
                    this.c = aVar.j();
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }
    }

    public pj() {
        d();
    }

    public pj d() {
        this.b = 1;
        this.c = 0.0d;
        this.d = g.h;
        this.e = g.h;
        this.f = g.h;
        this.g = null;
        this.h = 0;
        this.a = -1;
        return this;
    }

    public void a(b bVar) throws IOException {
        int i = this.b;
        if (i != 1) {
            bVar.b(1, i);
        }
        if (Double.doubleToLongBits(this.c) != Double.doubleToLongBits(0.0d)) {
            bVar.a(2, this.c);
        }
        bVar.a(3, this.d);
        if (!Arrays.equals(this.e, g.h)) {
            bVar.a(4, this.e);
        }
        if (!Arrays.equals(this.f, g.h)) {
            bVar.a(5, this.f);
        }
        a aVar = this.g;
        if (aVar != null) {
            bVar.a(6, aVar);
        }
        long j = this.h;
        if (j != 0) {
            bVar.b(7, j);
        }
        super.a(bVar);
    }

    /* access modifiers changed from: protected */
    public int c() {
        int c2 = super.c();
        int i = this.b;
        if (i != 1) {
            c2 += b.e(1, i);
        }
        if (Double.doubleToLongBits(this.c) != Double.doubleToLongBits(0.0d)) {
            c2 += b.b(2, this.c);
        }
        int b2 = c2 + b.b(3, this.d);
        if (!Arrays.equals(this.e, g.h)) {
            b2 += b.b(4, this.e);
        }
        if (!Arrays.equals(this.f, g.h)) {
            b2 += b.b(5, this.f);
        }
        a aVar = this.g;
        if (aVar != null) {
            b2 += b.b(6, aVar);
        }
        long j = this.h;
        if (j != 0) {
            return b2 + b.e(7, j);
        }
        return b2;
    }

    /* renamed from: b */
    public pj a(a aVar) throws IOException {
        while (true) {
            int a2 = aVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                this.b = aVar.k();
            } else if (a2 == 17) {
                this.c = aVar.c();
            } else if (a2 == 26) {
                this.d = aVar.j();
            } else if (a2 == 34) {
                this.e = aVar.j();
            } else if (a2 == 42) {
                this.f = aVar.j();
            } else if (a2 == 50) {
                if (this.g == null) {
                    this.g = new a();
                }
                aVar.a(this.g);
            } else if (a2 == 56) {
                this.h = aVar.f();
            } else if (!g.a(aVar, a2)) {
                return this;
            }
        }
    }
}
