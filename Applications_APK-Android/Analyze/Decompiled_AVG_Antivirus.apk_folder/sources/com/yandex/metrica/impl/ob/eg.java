package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public abstract class eg implements ej {
    @NonNull
    private final Context a;
    @NonNull
    private final dx b;
    @NonNull
    private final mx c;

    /* access modifiers changed from: protected */
    public abstract void b(@NonNull t tVar, @NonNull db dbVar);

    public eg(@NonNull Context context, @NonNull dx dxVar) {
        this(context, dxVar, new mx(mo.a(context), af.a().k(), ch.a(context), new kj(jo.a(context).c())));
    }

    public void a(@NonNull t tVar, @NonNull db dbVar) {
        b(tVar, dbVar);
    }

    public void a() {
        this.b.b(this);
        this.c.a(this);
    }

    @NonNull
    public dx b() {
        return this.b;
    }

    @VisibleForTesting
    eg(@NonNull Context context, @NonNull dx dxVar, @NonNull mx mxVar) {
        this.a = context.getApplicationContext();
        this.b = dxVar;
        this.c = mxVar;
        this.b.a(this);
        this.c.b(this);
    }

    @NonNull
    public mx c() {
        return this.c;
    }
}
