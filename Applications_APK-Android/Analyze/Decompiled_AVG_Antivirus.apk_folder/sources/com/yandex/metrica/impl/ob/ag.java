package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.util.Base64;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.yandex.metrica.impl.ob.pq;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import kotlin.jvm.internal.LongCompanionObject;

public final class ag {
    public static String a(InputStream inputStream) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
        StringWriter stringWriter = new StringWriter();
        a(inputStreamReader, stringWriter);
        return stringWriter.toString();
    }

    public static String a(String str) throws IOException {
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(str);
            try {
                String a = a(fileInputStream2);
                cg.a((Closeable) fileInputStream2);
                return a;
            } catch (Throwable th) {
                th = th;
                fileInputStream = fileInputStream2;
                cg.a((Closeable) fileInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            cg.a((Closeable) fileInputStream);
            throw th;
        }
    }

    public static int a(Reader reader, Writer writer) throws IOException {
        char[] cArr = new char[4096];
        int i = 0;
        while (true) {
            int read = reader.read(cArr, 0, 4096);
            if (-1 == read) {
                return i;
            }
            writer.write(cArr, 0, read);
            i += read;
        }
    }

    public static String b(String str) {
        try {
            return Base64.encodeToString(a(str.getBytes("UTF-8")), 0);
        } catch (Throwable th) {
            return null;
        }
    }

    public static byte[] a(byte[] bArr) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream;
        GZIPOutputStream gZIPOutputStream = null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                GZIPOutputStream gZIPOutputStream2 = new GZIPOutputStream(byteArrayOutputStream);
                try {
                    gZIPOutputStream2.write(bArr);
                    gZIPOutputStream2.finish();
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    cg.a((Closeable) gZIPOutputStream2);
                    cg.a((Closeable) byteArrayOutputStream);
                    return byteArray;
                } catch (Throwable th) {
                    th = th;
                    gZIPOutputStream = gZIPOutputStream2;
                    cg.a((Closeable) gZIPOutputStream);
                    cg.a((Closeable) byteArrayOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                cg.a((Closeable) gZIPOutputStream);
                cg.a((Closeable) byteArrayOutputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            byteArrayOutputStream = null;
            cg.a((Closeable) gZIPOutputStream);
            cg.a((Closeable) byteArrayOutputStream);
            throw th;
        }
    }

    public static byte[] b(byte[] bArr) throws IOException {
        ByteArrayInputStream byteArrayInputStream;
        GZIPInputStream gZIPInputStream = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream(bArr);
            try {
                GZIPInputStream gZIPInputStream2 = new GZIPInputStream(byteArrayInputStream);
                try {
                    byte[] b = b(gZIPInputStream2);
                    cg.a((Closeable) gZIPInputStream2);
                    cg.a((Closeable) byteArrayInputStream);
                    return b;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    gZIPInputStream = gZIPInputStream2;
                    th = th2;
                    cg.a((Closeable) gZIPInputStream);
                    cg.a((Closeable) byteArrayInputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                cg.a((Closeable) gZIPInputStream);
                cg.a((Closeable) byteArrayInputStream);
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            byteArrayInputStream = null;
            cg.a((Closeable) gZIPInputStream);
            cg.a((Closeable) byteArrayInputStream);
            throw th;
        }
    }

    public static String c(String str) {
        GZIPInputStream gZIPInputStream;
        ByteArrayInputStream byteArrayInputStream;
        GZIPInputStream gZIPInputStream2 = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream(Base64.decode(str, 0));
            try {
                gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
                try {
                    String a = a(gZIPInputStream);
                    cg.a((Closeable) gZIPInputStream);
                    cg.a((Closeable) byteArrayInputStream);
                    return a;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    gZIPInputStream2 = gZIPInputStream;
                    th = th2;
                    cg.a((Closeable) gZIPInputStream2);
                    cg.a((Closeable) byteArrayInputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                cg.a((Closeable) gZIPInputStream2);
                cg.a((Closeable) byteArrayInputStream);
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            byteArrayInputStream = null;
            cg.a((Closeable) gZIPInputStream2);
            cg.a((Closeable) byteArrayInputStream);
            throw th;
        }
    }

    public static byte[] b(@Nullable InputStream inputStream) throws IOException {
        return a(inputStream, Integer.MAX_VALUE);
    }

    public static byte[] a(@Nullable InputStream inputStream, int i) throws IOException {
        if (inputStream == null) {
            return null;
        }
        byte[] bArr = new byte[8192];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i2 = 0;
        while (true) {
            try {
                int read = inputStream.read(bArr);
                if (-1 == read) {
                    break;
                } else if (i2 > i) {
                    break;
                } else if (read > 0) {
                    byteArrayOutputStream.write(bArr, 0, read);
                    i2 += read;
                }
            } finally {
                cg.a((Closeable) byteArrayOutputStream);
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    @Nullable
    public static String a(Context context, File file) {
        byte[] b = b(context, file);
        try {
            return new String(b, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            String str = new String(b);
            rh.a(context).reportError("read_share_file_with_unsupported_encoding", e);
            return str;
        }
    }

    @Nullable
    public static byte[] b(Context context, File file) {
        FileLock fileLock;
        RandomAccessFile randomAccessFile;
        try {
            randomAccessFile = new RandomAccessFile(file, "r");
            try {
                FileChannel channel = randomAccessFile.getChannel();
                fileLock = channel.lock(0, LongCompanionObject.MAX_VALUE, true);
                try {
                    ByteBuffer allocate = ByteBuffer.allocate((int) file.length());
                    channel.read(allocate);
                    allocate.flip();
                    byte[] array = allocate.array();
                    a(file.getAbsolutePath(), fileLock);
                    cg.a((Closeable) randomAccessFile);
                    return array;
                } catch (IOException | SecurityException e) {
                    a(file.getAbsolutePath(), fileLock);
                    cg.a((Closeable) randomAccessFile);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    try {
                        rh.a(context).reportError("error_during_file_reading", th);
                        a(file.getAbsolutePath(), fileLock);
                        cg.a((Closeable) randomAccessFile);
                        return null;
                    } catch (Throwable th2) {
                        th = th2;
                    }
                }
            } catch (IOException e2) {
                fileLock = null;
                a(file.getAbsolutePath(), fileLock);
                cg.a((Closeable) randomAccessFile);
                return null;
            } catch (SecurityException e3) {
                fileLock = null;
                a(file.getAbsolutePath(), fileLock);
                cg.a((Closeable) randomAccessFile);
                return null;
            } catch (Throwable th3) {
                th = th3;
                fileLock = null;
                a(file.getAbsolutePath(), fileLock);
                cg.a((Closeable) randomAccessFile);
                throw th;
            }
        } catch (IOException e4) {
            randomAccessFile = null;
            fileLock = null;
            a(file.getAbsolutePath(), fileLock);
            cg.a((Closeable) randomAccessFile);
            return null;
        } catch (SecurityException e5) {
            randomAccessFile = null;
            fileLock = null;
            a(file.getAbsolutePath(), fileLock);
            cg.a((Closeable) randomAccessFile);
            return null;
        } catch (Throwable th4) {
            th = th4;
            randomAccessFile = null;
            fileLock = null;
            a(file.getAbsolutePath(), fileLock);
            cg.a((Closeable) randomAccessFile);
            throw th;
        }
    }

    public static void a(String str, FileLock fileLock) {
        if (fileLock != null && fileLock.isValid()) {
            try {
                fileLock.release();
            } catch (IOException e) {
            }
        }
    }

    @SuppressLint({"WorldReadableFiles"})
    public static void a(Context context, String str, String str2) {
        try {
            if (b()) {
                a(str2, str, context.openFileOutput(str, 0));
                c(context, context.getFileStreamPath(str));
                return;
            }
            a(str2, str, context.openFileOutput(str, 1));
        } catch (FileNotFoundException e) {
        }
    }

    @SuppressLint({"WorldReadableFiles"})
    public static void a(@NonNull Context context, @NonNull String str) {
        if (b()) {
            c(context, str);
        }
        b(context, str);
    }

    private static void b(@NonNull Context context, @NonNull String str) {
        try {
            File file = new File(context.getFileStreamPath(str).getAbsolutePath());
            if (file.exists() && file.canWrite()) {
                file.delete();
            }
        } catch (Throwable th) {
        }
    }

    @RequiresApi(api = 21)
    private static void c(@NonNull Context context, @NonNull String str) {
        try {
            File file = new File(context.getNoBackupFilesDir(), str);
            if (file.exists() && file.canWrite()) {
                file.delete();
            }
        } catch (Throwable th) {
        }
    }

    @TargetApi(21)
    public static void b(Context context, String str, String str2) {
        File file = new File(context.getNoBackupFilesDir(), str);
        try {
            a(str2, str, new FileOutputStream(file));
            c(context, file);
        } catch (FileNotFoundException e) {
        }
    }

    @SuppressLint({"SetWorldReadable"})
    @TargetApi(9)
    public static void c(final Context context, final File file) {
        if (file.exists()) {
            file.setReadable(true, false);
            if (b()) {
                new File(context.getApplicationInfo().dataDir).setExecutable(true, false);
                return;
            }
            return;
        }
        rh.a(context).reportEvent("make_non_existed_world_readable", new HashMap<String, Object>() {
            {
                put("file_name", file.getName());
                put("applicationId", context.getPackageName());
            }
        });
    }

    private static boolean b() {
        return cg.a(24);
    }

    private static void a(String str, String str2, FileOutputStream fileOutputStream) {
        FileLock fileLock = null;
        try {
            FileChannel channel = fileOutputStream.getChannel();
            fileLock = channel.lock();
            byte[] bytes = str.getBytes("UTF-8");
            ByteBuffer allocate = ByteBuffer.allocate(bytes.length);
            allocate.put(bytes);
            allocate.flip();
            channel.write(allocate);
            channel.force(true);
        } catch (IOException e) {
        } catch (Throwable th) {
            a(str2, fileLock);
            cg.a((Closeable) fileOutputStream);
            throw th;
        }
        a(str2, fileLock);
        cg.a((Closeable) fileOutputStream);
    }

    public static byte[] c(@NonNull byte[] bArr) {
        try {
            return MessageDigest.getInstance("MD5").digest(bArr);
        } catch (NoSuchAlgorithmException e) {
            return new byte[0];
        }
    }

    public static void a(@NonNull HttpURLConnection httpURLConnection, pq.a.b bVar, @NonNull String str, int i) {
        try {
            bVar.a(a(httpURLConnection.getInputStream(), i));
        } catch (IOException e) {
        }
        try {
            bVar.b(a(httpURLConnection.getErrorStream(), i));
        } catch (IOException e2) {
        }
    }

    public static File a(@NonNull Context context) {
        if (a()) {
            return context.getNoBackupFilesDir();
        }
        return context.getFilesDir();
    }

    public static File b(@NonNull Context context) {
        return new File(a(context), "appmetrica_crashes");
    }

    public static boolean a() {
        return cg.a(21);
    }
}
