package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class rt {
    public final long a;
    public final String b;
    public final List<Integer> c;
    public final long d;
    public final int e;

    public rt(long j, @NonNull String str, @NonNull List<Integer> list, long j2, int i) {
        this.a = j;
        this.b = str;
        this.c = Collections.unmodifiableList(list);
        this.d = j2;
        this.e = i;
    }

    @Nullable
    public static rt a(@Nullable String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(str);
            return new rt(jSONObject.getLong("seconds_to_live"), jSONObject.getString("token"), a(jSONObject.getJSONArray("ports")), jSONObject.getLong("first_delay_seconds"), jSONObject.getInt("launch_delay_seconds"));
        } catch (JSONException e2) {
            return null;
        }
    }

    private static ArrayList<Integer> a(JSONArray jSONArray) throws JSONException {
        ArrayList<Integer> arrayList = new ArrayList<>(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(Integer.valueOf(jSONArray.getInt(i)));
        }
        return arrayList;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        rt rtVar = (rt) o;
        if (this.a != rtVar.a || this.d != rtVar.d || this.e != rtVar.e) {
            return false;
        }
        String str = this.b;
        if (str == null ? rtVar.b != null : !str.equals(rtVar.b)) {
            return false;
        }
        List<Integer> list = this.c;
        if (list != null) {
            return list.equals(rtVar.c);
        }
        if (rtVar.c == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long j = this.a;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.b;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        List<Integer> list = this.c;
        if (list != null) {
            i2 = list.hashCode();
        }
        long j2 = this.d;
        return ((((hashCode + i2) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.e;
    }

    public String toString() {
        return "SocketConfig{secondsToLive=" + this.a + ", token='" + this.b + '\'' + ", ports=" + this.c + ", firstDelaySeconds=" + this.d + ", launchDelaySeconds=" + this.e + '}';
    }
}
