package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.bj;
import com.yandex.metrica.impl.ob.cb;
import com.yandex.metrica.impl.ob.nj;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class pq {
    private static final Map<cb.a.C0002a, bj.a> a = Collections.unmodifiableMap(new HashMap<cb.a.C0002a, bj.a>() {
        {
            put(cb.a.C0002a.CELL, bj.a.CELL);
            put(cb.a.C0002a.WIFI, bj.a.WIFI);
        }
    });
    /* access modifiers changed from: private */
    @NonNull
    public final Context b;
    @NonNull
    private final kp<a> c;
    @NonNull
    private final uv d;
    /* access modifiers changed from: private */
    @NonNull
    public final rf e;
    /* access modifiers changed from: private */
    @NonNull
    public final cd f;
    /* access modifiers changed from: private */
    @NonNull
    public final te g;
    private a h;
    private boolean i;

    public static class a {
        @NonNull
        private final List<C0017a> a;
        @NonNull
        private final LinkedHashMap<String, Object> b = new LinkedHashMap<>();

        /* renamed from: com.yandex.metrica.impl.ob.pq$a$a  reason: collision with other inner class name */
        public static class C0017a {
            @NonNull
            public final String a;
            @NonNull
            public final String b;
            @NonNull
            public final String c;
            @NonNull
            public final ud<String, String> d;
            public final long e;
            @NonNull
            public final List<bj.a> f;

            public C0017a(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull ud<String, String> udVar, long j, @NonNull List<bj.a> list) {
                this.a = str;
                this.b = str2;
                this.c = str3;
                this.e = j;
                this.f = list;
                this.d = udVar;
            }

            public boolean equals(Object o) {
                if (this == o) {
                    return true;
                }
                if (o == null || getClass() != o.getClass()) {
                    return false;
                }
                return this.a.equals(((C0017a) o).a);
            }

            public int hashCode() {
                return this.a.hashCode();
            }
        }

        public static class b {
            @Nullable
            byte[] a;
            @Nullable
            byte[] b;
            /* access modifiers changed from: private */
            @NonNull
            public final C0017a c;
            @Nullable
            private C0018a d;
            @Nullable
            private bj.a e;
            @Nullable
            private Integer f;
            @Nullable
            private Map<String, List<String>> g;
            @Nullable
            private Throwable h;

            /* renamed from: com.yandex.metrica.impl.ob.pq$a$b$a  reason: collision with other inner class name */
            public enum C0018a {
                OFFLINE,
                INCOMPATIBLE_NETWORK_TYPE,
                COMPLETE,
                ERROR
            }

            public b(@NonNull C0017a aVar) {
                this.c = aVar;
            }

            @NonNull
            public C0017a a() {
                return this.c;
            }

            @Nullable
            public C0018a b() {
                return this.d;
            }

            public void a(@NonNull C0018a aVar) {
                this.d = aVar;
            }

            @Nullable
            public bj.a c() {
                return this.e;
            }

            public void a(@Nullable bj.a aVar) {
                this.e = aVar;
            }

            @Nullable
            public Integer d() {
                return this.f;
            }

            public void a(@Nullable Integer num) {
                this.f = num;
            }

            @Nullable
            public byte[] e() {
                return this.a;
            }

            public void a(@Nullable byte[] bArr) {
                this.a = bArr;
            }

            @Nullable
            public Map<String, List<String>> f() {
                return this.g;
            }

            public void a(@Nullable Map<String, List<String>> map) {
                this.g = map;
            }

            @Nullable
            public Throwable g() {
                return this.h;
            }

            public void a(@Nullable Throwable th) {
                this.h = th;
            }

            @Nullable
            public byte[] h() {
                return this.b;
            }

            public void b(@Nullable byte[] bArr) {
                this.b = bArr;
            }
        }

        public a(@NonNull List<C0017a> list, @NonNull List<String> list2) {
            this.a = list;
            if (!cg.a((Collection) list2)) {
                for (String put : list2) {
                    this.b.put(put, new Object());
                }
            }
        }

        public boolean a(@NonNull C0017a aVar) {
            if (this.b.get(aVar.a) != null || this.a.contains(aVar)) {
                return false;
            }
            this.a.add(aVar);
            return true;
        }

        @NonNull
        public Set<String> a() {
            HashSet hashSet = new HashSet();
            int i = 0;
            for (String add : this.b.keySet()) {
                hashSet.add(add);
                i++;
                if (i > 1000) {
                    break;
                }
            }
            return hashSet;
        }

        @NonNull
        public List<C0017a> b() {
            return this.a;
        }

        public void b(@NonNull C0017a aVar) {
            this.b.put(aVar.a, new Object());
            this.a.remove(aVar);
        }
    }

    public pq(@NonNull Context context, @NonNull kp<a> kpVar, @NonNull cd cdVar, @NonNull rf rfVar, @NonNull uv uvVar) {
        this(context, kpVar, cdVar, rfVar, uvVar, new tb());
    }

    @VisibleForTesting
    public pq(@NonNull Context context, @NonNull kp<a> kpVar, @NonNull cd cdVar, @NonNull rf rfVar, @NonNull uv uvVar, @NonNull te teVar) {
        this.i = false;
        this.b = context;
        this.c = kpVar;
        this.f = cdVar;
        this.e = rfVar;
        this.h = this.c.a();
        this.d = uvVar;
        this.g = teVar;
    }

    public synchronized void a() {
        this.d.a(new Runnable() {
            public void run() {
                pq.this.b();
            }
        });
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.i) {
            this.h = this.c.a();
            c();
            this.i = true;
        }
    }

    private void c() {
        for (a.C0017a b2 : this.h.b()) {
            b(b2);
        }
    }

    public synchronized void a(@NonNull final sc scVar) {
        final List<cb.a> list = scVar.w;
        this.d.a(new Runnable() {
            public void run() {
                pq.this.a(list, scVar.t);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@Nullable List<cb.a> list, long j) {
        if (!cg.a((Collection) list)) {
            for (cb.a next : list) {
                if (!(next.a == null || next.b == null || next.c == null || next.e == null || next.e.longValue() < 0 || cg.a((Collection) next.f))) {
                    a(new a.C0017a(next.a, next.b, next.c, a(next.d), TimeUnit.SECONDS.toMillis(next.e.longValue() + j), b(next.f)));
                }
            }
        }
    }

    private ud<String, String> a(List<Pair<String, String>> list) {
        ud<String, String> udVar = new ud<>();
        for (Pair next : list) {
            udVar.a(next.first, next.second);
        }
        return udVar;
    }

    private boolean a(@NonNull a.C0017a aVar) {
        boolean a2 = this.h.a(aVar);
        if (a2) {
            b(aVar);
            this.e.a(aVar);
        }
        d();
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private void b(@NonNull final a.C0017a aVar) {
        this.d.a(new Runnable() {
            public void run() {
                if (!pq.this.f.c()) {
                    pq.this.e.b(aVar);
                    a.b bVar = new a.b(aVar);
                    bj.a a2 = pq.this.g.a(pq.this.b);
                    bVar.a(a2);
                    if (a2 == bj.a.OFFLINE) {
                        bVar.a(a.b.C0018a.OFFLINE);
                    } else if (!aVar.f.contains(a2)) {
                        bVar.a(a.b.C0018a.INCOMPATIBLE_NETWORK_TYPE);
                    } else {
                        bVar.a(a.b.C0018a.ERROR);
                        try {
                            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(aVar.b).openConnection();
                            for (Map.Entry entry : aVar.d.b()) {
                                httpURLConnection.setRequestProperty((String) entry.getKey(), TextUtils.join(",", (Iterable) entry.getValue()));
                            }
                            httpURLConnection.setInstanceFollowRedirects(true);
                            httpURLConnection.setRequestMethod(aVar.c);
                            httpURLConnection.setConnectTimeout(nj.a.a);
                            httpURLConnection.setReadTimeout(nj.a.a);
                            httpURLConnection.connect();
                            int responseCode = httpURLConnection.getResponseCode();
                            bVar.a(a.b.C0018a.COMPLETE);
                            bVar.a(Integer.valueOf(responseCode));
                            ag.a(httpURLConnection, bVar, "[ProvidedRequestService]", 102400);
                            bVar.a(httpURLConnection.getHeaderFields());
                        } catch (Throwable th) {
                            bVar.a(th);
                        }
                    }
                    pq.this.a(bVar);
                }
            }
        }, Math.max(h.a, Math.max(aVar.e - System.currentTimeMillis(), 0L)));
    }

    /* access modifiers changed from: private */
    public synchronized void a(@NonNull a.b bVar) {
        this.h.b(bVar.c);
        d();
        this.e.a(bVar);
    }

    private void d() {
        this.c.a(this.h);
    }

    @NonNull
    private List<bj.a> b(@NonNull List<cb.a.C0002a> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (cb.a.C0002a aVar : list) {
            arrayList.add(a.get(aVar));
        }
        return arrayList;
    }
}
