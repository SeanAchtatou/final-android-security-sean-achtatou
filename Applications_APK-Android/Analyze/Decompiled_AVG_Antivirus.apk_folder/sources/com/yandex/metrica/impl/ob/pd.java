package com.yandex.metrica.impl.ob;

import android.util.SparseArray;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.pk;
import java.util.ArrayList;
import java.util.HashMap;

public class pd {
    private static final int[] a = {0, 1, 2, 3};
    private final SparseArray<HashMap<String, pk.a.C0009a>> b;
    private int c;

    public pd() {
        this(a);
    }

    @VisibleForTesting
    pd(int[] iArr) {
        this.b = new SparseArray<>();
        this.c = 0;
        for (int put : iArr) {
            this.b.put(put, new HashMap());
        }
    }

    @Nullable
    public pk.a.C0009a a(int i, @NonNull String str) {
        return (pk.a.C0009a) this.b.get(i).get(str);
    }

    /* access modifiers changed from: package-private */
    public void a(@NonNull pk.a.C0009a aVar) {
        this.b.get(aVar.c).put(new String(aVar.b), aVar);
    }

    public int a() {
        return this.c;
    }

    public void b() {
        this.c++;
    }

    @NonNull
    public pk.a c() {
        pk.a aVar = new pk.a();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.b.size(); i++) {
            SparseArray<HashMap<String, pk.a.C0009a>> sparseArray = this.b;
            for (pk.a.C0009a add : sparseArray.get(sparseArray.keyAt(i)).values()) {
                arrayList.add(add);
            }
        }
        aVar.b = (pk.a.C0009a[]) arrayList.toArray(new pk.a.C0009a[arrayList.size()]);
        return aVar;
    }
}
