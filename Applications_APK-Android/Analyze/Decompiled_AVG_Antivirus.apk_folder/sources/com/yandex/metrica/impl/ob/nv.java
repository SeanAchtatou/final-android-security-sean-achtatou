package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageInfo;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;

@TargetApi(16)
class nv implements ns {
    private final Context a;
    private final vp b;

    public nv(Context context) {
        this(context, new vp());
    }

    @VisibleForTesting
    public nv(Context context, @NonNull vp vpVar) {
        this.a = context;
        this.b = vpVar;
    }

    @NonNull
    public List<nt> a() {
        ArrayList arrayList = new ArrayList();
        vp vpVar = this.b;
        Context context = this.a;
        PackageInfo a2 = vpVar.a(context, context.getPackageName(), 4096);
        if (a2 != null) {
            for (int i = 0; i < a2.requestedPermissions.length; i++) {
                String str = a2.requestedPermissions[i];
                if ((a2.requestedPermissionsFlags[i] & 2) != 0) {
                    arrayList.add(new nt(str, true));
                } else {
                    arrayList.add(new nt(str, false));
                }
            }
        }
        return arrayList;
    }
}
