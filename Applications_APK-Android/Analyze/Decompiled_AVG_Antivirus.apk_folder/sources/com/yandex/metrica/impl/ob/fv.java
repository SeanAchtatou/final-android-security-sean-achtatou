package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.IReporter;
import java.util.HashMap;

public class fv extends ft {
    @NonNull
    private final IReporter a;

    public fv(@NonNull di diVar, @NonNull IReporter iReporter) {
        super(diVar);
        this.a = iReporter;
    }

    public boolean a(@NonNull t tVar) {
        ir a2 = ir.a(tVar.g());
        HashMap hashMap = new HashMap();
        hashMap.put("type", a2.a);
        hashMap.put("delivery_method", a2.b);
        this.a.reportEvent("crash_saved", hashMap);
        return false;
    }
}
