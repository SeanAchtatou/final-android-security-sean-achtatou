package com.yandex.metrica.impl.ob;

public class gn {
    private final fm a;
    private final fn b;
    private final fl c;
    private final fo d;

    public gn(dd ddVar) {
        this.a = new fm(ddVar);
        this.b = new fn(ddVar);
        this.c = new fl(ddVar);
        this.d = new fo(ddVar, af.a().h(), new mx(mo.a(ddVar.d()), af.a().k(), ch.a(ddVar.d()), new kj(jo.a(ddVar.d()).c())));
    }

    public fm a() {
        return this.a;
    }

    public fl b() {
        return this.c;
    }

    public fn c() {
        return this.b;
    }

    public fj d() {
        return this.d;
    }
}
