package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;

public final class sx {
    @NonNull
    private final List<st> a;
    @Nullable
    private final sm b;
    @NonNull
    private final List<String> c;

    public sx(@NonNull sw<sm> swVar, @NonNull sw<List<st>> swVar2, @NonNull sw<List<String>> swVar3) {
        this.b = swVar.d();
        this.a = swVar2.d();
        this.c = swVar3.d();
    }

    @NonNull
    public List<st> a() {
        return this.a;
    }

    @Nullable
    public sm b() {
        return this.b;
    }

    @NonNull
    public List<String> c() {
        return this.c;
    }
}
