package com.yandex.metrica.impl.ob;

import java.io.IOException;

public abstract class e {
    protected volatile int a = -1;

    public abstract e a(a aVar) throws IOException;

    public int a() {
        if (this.a < 0) {
            b();
        }
        return this.a;
    }

    public int b() {
        int c = c();
        this.a = c;
        return c;
    }

    /* access modifiers changed from: protected */
    public int c() {
        return 0;
    }

    public void a(b bVar) throws IOException {
    }

    public static final byte[] a(e eVar) {
        byte[] bArr = new byte[eVar.b()];
        a(eVar, bArr, 0, bArr.length);
        return bArr;
    }

    public static final void a(e eVar, byte[] bArr, int i, int i2) {
        try {
            b a2 = b.a(bArr, i, i2);
            eVar.a(a2);
            a2.b();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public static final <T extends e> T a(T t, byte[] bArr) throws d {
        return b(t, bArr, 0, bArr.length);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends com.yandex.metrica.impl.ob.e> T b(T r0, byte[] r1, int r2, int r3) throws com.yandex.metrica.impl.ob.d {
        /*
            com.yandex.metrica.impl.ob.a r1 = com.yandex.metrica.impl.ob.a.a(r1, r2, r3)     // Catch:{ d -> 0x0015, IOException -> 0x000c }
            r0.a(r1)     // Catch:{ d -> 0x0015, IOException -> 0x000c }
            r2 = 0
            r1.a(r2)     // Catch:{ d -> 0x0015, IOException -> 0x000c }
            return r0
        L_0x000c:
            r0 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Reading from a byte array threw an IOException (should never happen)."
            r0.<init>(r1)
            throw r0
        L_0x0015:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.e.b(com.yandex.metrica.impl.ob.e, byte[], int, int):com.yandex.metrica.impl.ob.e");
    }

    public String toString() {
        return f.a(this);
    }
}
