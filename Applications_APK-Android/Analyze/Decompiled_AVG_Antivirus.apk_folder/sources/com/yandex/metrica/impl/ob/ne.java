package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.pg;

public class ne {
    public final long a;
    public final long b;
    public final pg.b c;

    public ne(long j, long j2, pg.b bVar) {
        this.a = j;
        this.b = j2;
        this.c = bVar;
    }

    public boolean a() {
        return this.c.c.length == 0 && this.c.b.length == 0;
    }
}
