package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;
import java.util.Map;

class rm {
    @NonNull
    private Socket a;
    @NonNull
    private rn b;
    @NonNull
    private Map<String, rl> c;

    rm(@NonNull Socket socket, @NonNull rn rnVar, @NonNull Map<String, rl> map) {
        this.a = socket;
        this.b = rnVar;
        this.c = map;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x005b A[SYNTHETIC, Splitter:B:20:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0065 A[SYNTHETIC, Splitter:B:25:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r6 = this;
            r0 = 0
            java.net.Socket r1 = r6.a     // Catch:{ Throwable -> 0x004e, all -> 0x0049 }
            r2 = 1000(0x3e8, float:1.401E-42)
            r1.setSoTimeout(r2)     // Catch:{ Throwable -> 0x004e, all -> 0x0049 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x004e, all -> 0x0049 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x004e, all -> 0x0049 }
            java.net.Socket r3 = r6.a     // Catch:{ Throwable -> 0x004e, all -> 0x0049 }
            java.io.InputStream r3 = r3.getInputStream()     // Catch:{ Throwable -> 0x004e, all -> 0x0049 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x004e, all -> 0x0049 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x004e, all -> 0x0049 }
            java.lang.String r0 = r6.a(r1)     // Catch:{ Throwable -> 0x0047 }
            if (r0 == 0) goto L_0x0042
            android.net.Uri r2 = android.net.Uri.parse(r0)     // Catch:{ Throwable -> 0x0047 }
            java.lang.String r3 = r2.getPath()     // Catch:{ Throwable -> 0x0047 }
            java.util.Map<java.lang.String, com.yandex.metrica.impl.ob.rl> r4 = r6.c     // Catch:{ Throwable -> 0x0047 }
            java.lang.Object r3 = r4.get(r3)     // Catch:{ Throwable -> 0x0047 }
            com.yandex.metrica.impl.ob.rl r3 = (com.yandex.metrica.impl.ob.rl) r3     // Catch:{ Throwable -> 0x0047 }
            if (r3 == 0) goto L_0x003b
            java.net.Socket r0 = r6.a     // Catch:{ Throwable -> 0x0047 }
            com.yandex.metrica.impl.ob.rk r0 = r3.a(r0, r2)     // Catch:{ Throwable -> 0x0047 }
            r0.a()     // Catch:{ Throwable -> 0x0047 }
            goto L_0x0042
        L_0x003b:
            com.yandex.metrica.impl.ob.rn r2 = r6.b     // Catch:{ Throwable -> 0x0047 }
            java.lang.String r3 = "request_to_unknown_path"
            r2.a(r3, r0)     // Catch:{ Throwable -> 0x0047 }
        L_0x0042:
            r1.close()     // Catch:{ Throwable -> 0x005f }
            goto L_0x005e
        L_0x0047:
            r0 = move-exception
            goto L_0x0052
        L_0x0049:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0063
        L_0x004e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0052:
            com.yandex.metrica.impl.ob.rn r2 = r6.b     // Catch:{ all -> 0x0062 }
            java.lang.String r3 = "LocalHttpServer exception"
            r2.a(r3, r0)     // Catch:{ all -> 0x0062 }
            if (r1 == 0) goto L_0x0061
            r1.close()     // Catch:{ Throwable -> 0x005f }
        L_0x005e:
            goto L_0x0061
        L_0x005f:
            r0 = move-exception
            goto L_0x005e
        L_0x0061:
            return
        L_0x0062:
            r0 = move-exception
        L_0x0063:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ Throwable -> 0x0069 }
            goto L_0x006a
        L_0x0069:
            r1 = move-exception
        L_0x006a:
            throw r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.rm.a():void");
    }

    @Nullable
    private String a(@NonNull BufferedReader bufferedReader) throws IOException {
        int indexOf;
        int indexOf2;
        String readLine = bufferedReader.readLine();
        if (!TextUtils.isEmpty(readLine) && readLine.startsWith("GET /") && (indexOf2 = readLine.indexOf(32, (indexOf = readLine.indexOf(47) + 1))) > 0) {
            return readLine.substring(indexOf, indexOf2);
        }
        this.b.a("invalid_route", readLine);
        return null;
    }
}
