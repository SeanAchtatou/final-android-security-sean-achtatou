package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.di;
import java.util.concurrent.TimeUnit;

public class bs<C extends di> extends cf<C> {
    private Runnable d = new Runnable() {
        public void run() {
            bs.this.e();
        }
    };
    private final uv e;

    public bs(@NonNull C c, @NonNull si siVar, @NonNull bb bbVar, @NonNull uv uvVar) {
        super(c, siVar, bbVar);
        this.e = uvVar;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.e.b(this.d);
    }

    public void b() {
        synchronized (this.a) {
            if (!this.c) {
                a();
                d();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        super.c();
        qp h = ((di) g()).h();
        if (h.W() && cg.a(h.c())) {
            try {
                this.b.a(bo.G().a((di) g()));
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void d() {
        if (((di) g()).h().T() > 0) {
            this.e.a(this.d, TimeUnit.SECONDS.toMillis((long) ((di) g()).h().T()));
        }
    }
}
