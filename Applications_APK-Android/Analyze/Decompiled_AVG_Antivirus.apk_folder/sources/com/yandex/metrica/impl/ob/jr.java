package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class jr extends ji {
    jr(@NonNull Context context, @NonNull jn jnVar) {
        this(jnVar, new kj(jo.a(context).c()));
    }

    @VisibleForTesting
    jr(@NonNull jn jnVar, @NonNull kj kjVar) {
        super(jnVar, kjVar);
    }

    /* access modifiers changed from: protected */
    public long c(long j) {
        return c().f(j);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public kj d(long j) {
        return c().g(j);
    }

    @NonNull
    public String e() {
        return "lbs_dat";
    }
}
