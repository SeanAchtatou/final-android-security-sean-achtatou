package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.c;

public class rc {
    @NonNull
    private final kp<ra> a;
    @NonNull
    private final rb b;
    @NonNull
    private final Context c;

    public rc(@NonNull Context context, @NonNull kp<ra> kpVar) {
        this(context, kpVar, new rb());
    }

    public rc(@NonNull Context context, @NonNull kp<ra> kpVar, @NonNull rb rbVar) {
        this.a = kpVar;
        this.c = context;
        this.b = rbVar;
    }

    public void a() {
        c a2 = rh.a(this.c);
        rb rbVar = this.b;
        a2.reportEvent("sdk_list", rbVar.b(rbVar.a(this.a.a().a)).toString());
    }
}
