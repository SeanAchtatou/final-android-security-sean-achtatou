package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class el implements ej, rz {
    @NonNull
    private final Context a;
    @NonNull
    private dd b;
    @Nullable
    private final ResultReceiver c;

    public el(@NonNull Context context, @NonNull dd ddVar, @NonNull db dbVar) {
        this.a = context;
        this.b = ddVar;
        this.c = dbVar.c;
        this.b.a(this);
    }

    public void a(@NonNull t tVar, @NonNull db dbVar) {
        this.b.a(dbVar.b);
        this.b.a(tVar, this);
    }

    public void a(@Nullable sc scVar) {
        u.a(this.c, scVar);
    }

    public void a(@NonNull rw rwVar) {
    }

    public void a() {
        this.b.b(this);
    }

    @NonNull
    public dd b() {
        return this.b;
    }
}
