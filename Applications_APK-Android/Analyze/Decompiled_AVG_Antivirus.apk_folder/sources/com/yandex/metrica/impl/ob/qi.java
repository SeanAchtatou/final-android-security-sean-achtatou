package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public interface qi<I, O> {
    boolean a(@NonNull Object obj);

    @NonNull
    O b(@NonNull Object obj);
}
