package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.Random;

public class tk {
    @NonNull
    private final Random a;

    public tk() {
        this(new Random());
    }

    public tk(@NonNull Random random) {
        this.a = random;
    }

    public long a(long j, long j2) {
        if (j < j2) {
            long nextLong = this.a.nextLong();
            if (nextLong == Long.MIN_VALUE) {
                nextLong = 0;
            } else if (nextLong < 0) {
                nextLong = -nextLong;
            }
            return j + (nextLong % (j2 - j));
        }
        throw new IllegalArgumentException("min should be less than max");
    }
}
