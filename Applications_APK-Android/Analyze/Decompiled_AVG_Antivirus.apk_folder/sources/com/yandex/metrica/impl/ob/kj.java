package com.yandex.metrica.impl.ob;

import android.util.Base64;
import androidx.annotation.Nullable;

public class kj extends ki {
    static final oj a = new oj("LOCATION_TRACKING_ENABLED");
    @Deprecated
    public static final oj b = new oj("COLLECT_INSTALLED_APPS");
    public static final oj c = new oj("INSTALLED_APP_COLLECTING");
    static final oj d = new oj("REFERRER");
    static final oj e = new oj("REFERRER_FROM_PLAY_SERVICES");
    static final oj f = new oj("REFERRER_HANDLED");
    static final oj g = new oj("REFERRER_HOLDER_STATE");
    static final oj h = new oj("PREF_KEY_OFFSET");
    static final oj i = new oj("UNCHECKED_TIME");
    static final oj j = new oj("L_REQ_NUM");
    static final oj k = new oj("L_ID");
    static final oj l = new oj("LBS_ID");
    static final oj m = new oj("STATISTICS_RESTRICTED_IN_MAIN");
    static final oj n = new oj("SDKFCE");
    static final oj o = new oj("FST");
    static final oj q = new oj("LSST");
    static final oj r = new oj("FSDKFCO");
    static final oj s = new oj("SRSDKFC");
    static final oj t = new oj("LSDKFCAT");
    private static final oj u = new oj("LAST_MIGRATION_VERSION");

    public kj(jq jqVar) {
        super(jqVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean */
    public boolean a() {
        return b(c.b(), false);
    }

    public String b() {
        return c(d.b(), null);
    }

    public qf c() {
        return b(c(e.b(), null));
    }

    private qf b(String str) {
        if (str == null) {
            return null;
        }
        try {
            return qf.a(Base64.encode(str.getBytes(), 0));
        } catch (d e2) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean */
    public boolean d() {
        return b(f.b(), false);
    }

    public kj a(boolean z) {
        return (kj) a(c.b(), z);
    }

    public kj a(String str) {
        return (kj) b(d.b(), str);
    }

    public kj a(qf qfVar) {
        return (kj) b(e.b(), b(qfVar));
    }

    private String b(@Nullable qf qfVar) {
        if (qfVar == null) {
            return null;
        }
        return new String(Base64.encode(qfVar.a(), 0));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.a(java.lang.String, boolean):T
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, int):T
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, long):T
      com.yandex.metrica.impl.ob.ki.a(java.lang.String, boolean):T */
    public kj e() {
        return (kj) a(f.b(), true);
    }

    public kj f() {
        return (kj) p(d.b()).p(e.b());
    }

    public int a(int i2) {
        return b(u.b(), i2);
    }

    public kj b(int i2) {
        return (kj) a(u.b(), i2);
    }

    public void b(boolean z) {
        a(a.b(), z).n();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean */
    public boolean g() {
        return b(a.b(), false);
    }

    public long c(int i2) {
        return b(h.b(), (long) i2);
    }

    public kj a(long j2) {
        return (kj) a(h.b(), j2);
    }

    public long b(long j2) {
        return b(j.b(), j2);
    }

    public kj c(long j2) {
        return (kj) a(j.b(), j2);
    }

    public long d(long j2) {
        return b(k.b(), j2);
    }

    public kj e(long j2) {
        return (kj) a(k.b(), j2);
    }

    public long f(long j2) {
        return b(l.b(), j2);
    }

    public kj g(long j2) {
        return (kj) a(l.b(), j2);
    }

    public boolean c(boolean z) {
        return b(i.b(), z);
    }

    public kj d(boolean z) {
        return (kj) a(i.b(), z);
    }

    public int d(int i2) {
        return b(g.b(), i2);
    }

    public kj e(int i2) {
        return (kj) a(g.b(), i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
      com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean */
    @Nullable
    public Boolean h() {
        if (r(m.b())) {
            return Boolean.valueOf(b(m.b(), true));
        }
        return null;
    }

    public kj e(boolean z) {
        return (kj) a(m.b(), z);
    }
}
