package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class hk extends hi {
    hk(@NonNull di diVar, @NonNull hr hrVar) {
        this(diVar, hrVar, new hv(diVar.x(), "foreground"));
    }

    @VisibleForTesting
    hk(@NonNull di diVar, @NonNull hr hrVar, @NonNull hv hvVar) {
        super(diVar, hrVar, hvVar, hq.a(hw.FOREGROUND).a());
    }
}
