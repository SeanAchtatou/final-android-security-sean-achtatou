package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.Cdo;
import com.yandex.metrica.impl.ob.dr;

public class dy<COMPONENT extends dr & Cdo> implements dl<dx> {
    @NonNull
    private final em<COMPONENT> a;

    public dy(@NonNull em<COMPONENT> emVar) {
        this.a = emVar;
    }

    /* renamed from: a */
    public dx b(@NonNull Context context, @NonNull df dfVar, @NonNull db dbVar) {
        return new dx(context, dfVar, dbVar, this.a);
    }
}
