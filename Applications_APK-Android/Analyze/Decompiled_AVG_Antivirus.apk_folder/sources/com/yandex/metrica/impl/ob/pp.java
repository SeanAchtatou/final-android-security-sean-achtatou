package com.yandex.metrica.impl.ob;

import androidx.core.view.MotionEventCompat;
import com.jobstrak.drawingfun.lib.mopub.mobileads.resource.DrawableConstants;
import com.jobstrak.drawingfun.sdk.BuildConfig;
import com.yandex.metrica.YandexMetricaDefaultValues;
import java.io.IOException;

public interface pp {

    public static final class a extends e {
        public String A;
        public String B;
        public long C;
        public long D;
        public boolean E;
        public e F;
        public String b;
        public long c;
        public String[] d;
        public String e;
        public String f;
        public String[] g;
        public String[] h;
        public String[] i;
        public b j;
        public c k;
        public C0014a l;
        public h m;
        public f[] n;
        public String o;
        public String p;
        public boolean q;
        public String r;
        public String s;
        public String[] t;
        public i u;
        public boolean v;
        public d[] w;
        public g x;
        public String y;
        public String z;

        public static final class b extends e {
            public boolean b;
            public boolean c;
            public boolean d;
            public boolean e;
            public boolean f;
            public boolean g;
            public boolean h;
            public boolean i;
            public boolean j;
            public boolean k;
            public boolean l;
            public boolean m;
            public boolean n;
            public boolean o;

            public b() {
                d();
            }

            public b d() {
                this.b = false;
                this.c = false;
                this.d = false;
                this.e = false;
                this.f = false;
                this.g = false;
                this.h = false;
                this.i = false;
                this.j = false;
                this.k = false;
                this.l = false;
                this.m = false;
                this.n = false;
                this.o = false;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                bVar.a(4, this.e);
                bVar.a(5, this.g);
                bVar.a(6, this.h);
                bVar.a(8, this.i);
                bVar.a(9, this.j);
                bVar.a(10, this.k);
                bVar.a(11, this.l);
                bVar.a(12, this.m);
                bVar.a(13, this.n);
                bVar.a(14, this.o);
                bVar.a(15, this.f);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c) + b.b(3, this.d) + b.b(4, this.e) + b.b(5, this.g) + b.b(6, this.h) + b.b(8, this.i) + b.b(9, this.j) + b.b(10, this.k) + b.b(11, this.l) + b.b(12, this.m) + b.b(13, this.n) + b.b(14, this.o) + b.b(15, this.f);
            }

            /* renamed from: b */
            public b a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    switch (a) {
                        case 0:
                            return this;
                        case 8:
                            this.b = aVar.h();
                            break;
                        case 16:
                            this.c = aVar.h();
                            break;
                        case 24:
                            this.d = aVar.h();
                            break;
                        case 32:
                            this.e = aVar.h();
                            break;
                        case MotionEventCompat.AXIS_GENERIC_9:
                            this.g = aVar.h();
                            break;
                        case 48:
                            this.h = aVar.h();
                            break;
                        case 64:
                            this.i = aVar.h();
                            break;
                        case DrawableConstants.GradientStrip.GRADIENT_STRIP_HEIGHT_DIPS:
                            this.j = aVar.h();
                            break;
                        case 80:
                            this.k = aVar.h();
                            break;
                        case 88:
                            this.l = aVar.h();
                            break;
                        case 96:
                            this.m = aVar.h();
                            break;
                        case 104:
                            this.n = aVar.h();
                            break;
                        case 112:
                            this.o = aVar.h();
                            break;
                        case 120:
                            this.f = aVar.h();
                            break;
                        default:
                            if (g.a(aVar, a)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }
        }

        public static final class c extends e {
            public long b;
            public float c;
            public int d;
            public int e;
            public long f;
            public int g;
            public boolean h;
            public boolean i;
            public long j;

            public c() {
                d();
            }

            public c d() {
                this.b = 5000;
                this.c = 10.0f;
                this.d = 20;
                this.e = 200;
                this.f = 60000;
                this.g = 10000;
                this.h = false;
                this.i = false;
                this.j = 60000;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                bVar.a(4, this.e);
                bVar.b(5, this.f);
                bVar.a(6, this.g);
                bVar.a(7, this.h);
                bVar.a(8, this.i);
                bVar.b(9, this.j);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.e(1, this.b) + b.b(2, this.c) + b.d(3, this.d) + b.d(4, this.e) + b.e(5, this.f) + b.d(6, this.g) + b.b(7, this.h) + b.b(8, this.i) + b.e(9, this.j);
            }

            /* renamed from: b */
            public c a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (a == 21) {
                        this.c = aVar.d();
                    } else if (a == 24) {
                        this.d = aVar.g();
                    } else if (a == 32) {
                        this.e = aVar.g();
                    } else if (a == 40) {
                        this.f = aVar.f();
                    } else if (a == 48) {
                        this.g = aVar.g();
                    } else if (a == 56) {
                        this.h = aVar.h();
                    } else if (a == 64) {
                        this.i = aVar.h();
                    } else if (a == 72) {
                        this.j = aVar.f();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.pp$a$a  reason: collision with other inner class name */
        public static final class C0014a extends e {
            public c b;
            public long c;
            public long d;
            public boolean e;
            public C0015a[] f;

            /* renamed from: com.yandex.metrica.impl.ob.pp$a$a$a  reason: collision with other inner class name */
            public static final class C0015a extends e {
                private static volatile C0015a[] d;
                public long b;
                public long c;

                public static C0015a[] d() {
                    if (d == null) {
                        synchronized (c.a) {
                            if (d == null) {
                                d = new C0015a[0];
                            }
                        }
                    }
                    return d;
                }

                public C0015a() {
                    e();
                }

                public C0015a e() {
                    this.b = 0;
                    this.c = 0;
                    this.a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    bVar.b(1, this.b);
                    bVar.b(2, this.c);
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    return super.c() + b.e(1, this.b) + b.e(2, this.c);
                }

                /* renamed from: b */
                public C0015a a(a aVar) throws IOException {
                    while (true) {
                        int a = aVar.a();
                        if (a == 0) {
                            return this;
                        }
                        if (a == 8) {
                            this.b = aVar.f();
                        } else if (a == 16) {
                            this.c = aVar.f();
                        } else if (!g.a(aVar, a)) {
                            return this;
                        }
                    }
                }
            }

            public C0014a() {
                d();
            }

            public C0014a d() {
                this.b = null;
                this.c = 60000;
                this.d = 3600000;
                this.e = false;
                this.f = C0015a.d();
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                c cVar = this.b;
                if (cVar != null) {
                    bVar.a(1, cVar);
                }
                bVar.b(2, this.c);
                bVar.b(3, this.d);
                bVar.a(4, this.e);
                C0015a[] aVarArr = this.f;
                if (aVarArr != null && aVarArr.length > 0) {
                    int i = 0;
                    while (true) {
                        C0015a[] aVarArr2 = this.f;
                        if (i >= aVarArr2.length) {
                            break;
                        }
                        C0015a aVar = aVarArr2[i];
                        if (aVar != null) {
                            bVar.a(5, aVar);
                        }
                        i++;
                    }
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                c cVar = this.b;
                if (cVar != null) {
                    c2 += b.b(1, cVar);
                }
                int e2 = c2 + b.e(2, this.c) + b.e(3, this.d) + b.b(4, this.e);
                C0015a[] aVarArr = this.f;
                if (aVarArr != null && aVarArr.length > 0) {
                    int i = 0;
                    while (true) {
                        C0015a[] aVarArr2 = this.f;
                        if (i >= aVarArr2.length) {
                            break;
                        }
                        C0015a aVar = aVarArr2[i];
                        if (aVar != null) {
                            e2 += b.b(5, aVar);
                        }
                        i++;
                    }
                }
                return e2;
            }

            /* renamed from: b */
            public C0014a a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        if (this.b == null) {
                            this.b = new c();
                        }
                        aVar.a(this.b);
                    } else if (a == 16) {
                        this.c = aVar.f();
                    } else if (a == 24) {
                        this.d = aVar.f();
                    } else if (a == 32) {
                        this.e = aVar.h();
                    } else if (a == 42) {
                        int b2 = g.b(aVar, 42);
                        C0015a[] aVarArr = this.f;
                        int length = aVarArr == null ? 0 : aVarArr.length;
                        C0015a[] aVarArr2 = new C0015a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.f, 0, aVarArr2, 0, length);
                        }
                        while (length < aVarArr2.length - 1) {
                            aVarArr2[length] = new C0015a();
                            aVar.a(aVarArr2[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr2[length] = new C0015a();
                        aVar.a(aVarArr2[length]);
                        this.f = aVarArr2;
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class h extends e {
            public long b;
            public String c;
            public int[] d;
            public long e;
            public int f;

            public h() {
                d();
            }

            public h d() {
                this.b = 0;
                this.c = "";
                this.d = g.a;
                this.e = 259200;
                this.f = 10;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                bVar.a(2, this.c);
                int[] iArr = this.d;
                if (iArr != null && iArr.length > 0) {
                    int i = 0;
                    while (true) {
                        int[] iArr2 = this.d;
                        if (i >= iArr2.length) {
                            break;
                        }
                        bVar.a(3, iArr2[i]);
                        i++;
                    }
                }
                bVar.b(4, this.e);
                bVar.a(5, this.f);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int[] iArr;
                int c2 = super.c() + b.e(1, this.b) + b.b(2, this.c);
                int[] iArr2 = this.d;
                if (iArr2 != null && iArr2.length > 0) {
                    int i = 0;
                    int i2 = 0;
                    while (true) {
                        iArr = this.d;
                        if (i >= iArr.length) {
                            break;
                        }
                        i2 += b.d(iArr[i]);
                        i++;
                    }
                    c2 = c2 + i2 + (iArr.length * 1);
                }
                return c2 + b.e(4, this.e) + b.d(5, this.f);
            }

            /* renamed from: b */
            public h a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (a == 18) {
                        this.c = aVar.i();
                    } else if (a == 24) {
                        int b2 = g.b(aVar, 24);
                        int[] iArr = this.d;
                        int length = iArr == null ? 0 : iArr.length;
                        int[] iArr2 = new int[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.d, 0, iArr2, 0, length);
                        }
                        while (length < iArr2.length - 1) {
                            iArr2[length] = aVar.g();
                            aVar.a();
                            length++;
                        }
                        iArr2[length] = aVar.g();
                        this.d = iArr2;
                    } else if (a == 26) {
                        int d2 = aVar.d(aVar.n());
                        int t = aVar.t();
                        int i = 0;
                        while (aVar.r() > 0) {
                            aVar.g();
                            i++;
                        }
                        aVar.f(t);
                        int[] iArr3 = this.d;
                        int length2 = iArr3 == null ? 0 : iArr3.length;
                        int[] iArr4 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.d, 0, iArr4, 0, length2);
                        }
                        while (length2 < iArr4.length) {
                            iArr4[length2] = aVar.g();
                            length2++;
                        }
                        this.d = iArr4;
                        aVar.e(d2);
                    } else if (a == 32) {
                        this.e = aVar.f();
                    } else if (a == 40) {
                        this.f = aVar.g();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class e extends e {
            public long b;
            public long c;

            public e() {
                d();
            }

            public e d() {
                this.b = 86400;
                this.c = 432000;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                bVar.b(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.e(1, this.b) + b.e(2, this.c);
            }

            /* renamed from: b */
            public e a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (a == 16) {
                        this.c = aVar.f();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class f extends e {
            private static volatile f[] h;
            public String b;
            public String c;
            public String d;
            public C0016a[] e;
            public long f;
            public int[] g;

            /* renamed from: com.yandex.metrica.impl.ob.pp$a$f$a  reason: collision with other inner class name */
            public static final class C0016a extends e {
                private static volatile C0016a[] d;
                public String b;
                public String c;

                public static C0016a[] d() {
                    if (d == null) {
                        synchronized (c.a) {
                            if (d == null) {
                                d = new C0016a[0];
                            }
                        }
                    }
                    return d;
                }

                public C0016a() {
                    e();
                }

                public C0016a e() {
                    this.b = "";
                    this.c = "";
                    this.a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    bVar.a(1, this.b);
                    bVar.a(2, this.c);
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    return super.c() + b.b(1, this.b) + b.b(2, this.c);
                }

                /* renamed from: b */
                public C0016a a(a aVar) throws IOException {
                    while (true) {
                        int a = aVar.a();
                        if (a == 0) {
                            return this;
                        }
                        if (a == 10) {
                            this.b = aVar.i();
                        } else if (a == 18) {
                            this.c = aVar.i();
                        } else if (!g.a(aVar, a)) {
                            return this;
                        }
                    }
                }
            }

            public static f[] d() {
                if (h == null) {
                    synchronized (c.a) {
                        if (h == null) {
                            h = new f[0];
                        }
                    }
                }
                return h;
            }

            public f() {
                e();
            }

            public f e() {
                this.b = "";
                this.c = "";
                this.d = "";
                this.e = C0016a.d();
                this.f = 0;
                this.g = g.a;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                C0016a[] aVarArr = this.e;
                int i = 0;
                if (aVarArr != null && aVarArr.length > 0) {
                    int i2 = 0;
                    while (true) {
                        C0016a[] aVarArr2 = this.e;
                        if (i2 >= aVarArr2.length) {
                            break;
                        }
                        C0016a aVar = aVarArr2[i2];
                        if (aVar != null) {
                            bVar.a(4, aVar);
                        }
                        i2++;
                    }
                }
                bVar.b(5, this.f);
                int[] iArr = this.g;
                if (iArr != null && iArr.length > 0) {
                    while (true) {
                        int[] iArr2 = this.g;
                        if (i >= iArr2.length) {
                            break;
                        }
                        bVar.a(6, iArr2[i]);
                        i++;
                    }
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.b(1, this.b) + b.b(2, this.c) + b.b(3, this.d);
                C0016a[] aVarArr = this.e;
                int i = 0;
                if (aVarArr != null && aVarArr.length > 0) {
                    int i2 = c2;
                    int i3 = 0;
                    while (true) {
                        C0016a[] aVarArr2 = this.e;
                        if (i3 >= aVarArr2.length) {
                            break;
                        }
                        C0016a aVar = aVarArr2[i3];
                        if (aVar != null) {
                            i2 += b.b(4, aVar);
                        }
                        i3++;
                    }
                    c2 = i2;
                }
                int e2 = c2 + b.e(5, this.f);
                int[] iArr = this.g;
                if (iArr == null || iArr.length <= 0) {
                    return e2;
                }
                int i4 = 0;
                while (true) {
                    int[] iArr2 = this.g;
                    if (i >= iArr2.length) {
                        return e2 + i4 + (iArr2.length * 1);
                    }
                    i4 += b.d(iArr2[i]);
                    i++;
                }
            }

            /* renamed from: b */
            public f a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        this.b = aVar.i();
                    } else if (a == 18) {
                        this.c = aVar.i();
                    } else if (a == 26) {
                        this.d = aVar.i();
                    } else if (a == 34) {
                        int b2 = g.b(aVar, 34);
                        C0016a[] aVarArr = this.e;
                        int length = aVarArr == null ? 0 : aVarArr.length;
                        C0016a[] aVarArr2 = new C0016a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.e, 0, aVarArr2, 0, length);
                        }
                        while (length < aVarArr2.length - 1) {
                            aVarArr2[length] = new C0016a();
                            aVar.a(aVarArr2[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr2[length] = new C0016a();
                        aVar.a(aVarArr2[length]);
                        this.e = aVarArr2;
                    } else if (a == 40) {
                        this.f = aVar.f();
                    } else if (a == 48) {
                        int b3 = g.b(aVar, 48);
                        int[] iArr = new int[b3];
                        int i = 0;
                        for (int i2 = 0; i2 < b3; i2++) {
                            if (i2 != 0) {
                                aVar.a();
                            }
                            int g2 = aVar.g();
                            if (g2 == 1 || g2 == 2) {
                                iArr[i] = g2;
                                i++;
                            }
                        }
                        if (i != 0) {
                            int[] iArr2 = this.g;
                            int length2 = iArr2 == null ? 0 : iArr2.length;
                            if (length2 == 0 && i == iArr.length) {
                                this.g = iArr;
                            } else {
                                int[] iArr3 = new int[(length2 + i)];
                                if (length2 != 0) {
                                    System.arraycopy(this.g, 0, iArr3, 0, length2);
                                }
                                System.arraycopy(iArr, 0, iArr3, length2, i);
                                this.g = iArr3;
                            }
                        }
                    } else if (a == 50) {
                        int d2 = aVar.d(aVar.n());
                        int t = aVar.t();
                        int i3 = 0;
                        while (aVar.r() > 0) {
                            int g3 = aVar.g();
                            if (g3 == 1 || g3 == 2) {
                                i3++;
                            }
                        }
                        if (i3 != 0) {
                            aVar.f(t);
                            int[] iArr4 = this.g;
                            int length3 = iArr4 == null ? 0 : iArr4.length;
                            int[] iArr5 = new int[(i3 + length3)];
                            if (length3 != 0) {
                                System.arraycopy(this.g, 0, iArr5, 0, length3);
                            }
                            while (aVar.r() > 0) {
                                int g4 = aVar.g();
                                if (g4 == 1 || g4 == 2) {
                                    iArr5[length3] = g4;
                                    length3++;
                                }
                            }
                            this.g = iArr5;
                        }
                        aVar.e(d2);
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class i extends e {
            public long b;

            public i() {
                d();
            }

            public i d() {
                this.b = 18000000;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.e(1, this.b);
            }

            /* renamed from: b */
            public i a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class d extends e {
            private static volatile d[] d;
            public String b;
            public boolean c;

            public static d[] d() {
                if (d == null) {
                    synchronized (c.a) {
                        if (d == null) {
                            d = new d[0];
                        }
                    }
                }
                return d;
            }

            public d() {
                e();
            }

            public d e() {
                this.b = "";
                this.c = false;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c);
            }

            /* renamed from: b */
            public d a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 10) {
                        this.b = aVar.i();
                    } else if (a == 16) {
                        this.c = aVar.h();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public static final class g extends e {
            public long b;
            public long c;
            public long d;
            public long e;

            public g() {
                d();
            }

            public g d() {
                this.b = 432000000;
                this.c = 86400000;
                this.d = 10000;
                this.e = 3600000;
                this.a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                long j = this.b;
                if (j != 432000000) {
                    bVar.b(1, j);
                }
                long j2 = this.c;
                if (j2 != 86400000) {
                    bVar.b(2, j2);
                }
                long j3 = this.d;
                if (j3 != 10000) {
                    bVar.b(3, j3);
                }
                long j4 = this.e;
                if (j4 != 3600000) {
                    bVar.b(4, j4);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                long j = this.b;
                if (j != 432000000) {
                    c2 += b.e(1, j);
                }
                long j2 = this.c;
                if (j2 != 86400000) {
                    c2 += b.e(2, j2);
                }
                long j3 = this.d;
                if (j3 != 10000) {
                    c2 += b.e(3, j3);
                }
                long j4 = this.e;
                if (j4 != 3600000) {
                    return c2 + b.e(4, j4);
                }
                return c2;
            }

            /* renamed from: b */
            public g a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.a();
                    if (a == 0) {
                        return this;
                    }
                    if (a == 8) {
                        this.b = aVar.f();
                    } else if (a == 16) {
                        this.c = aVar.f();
                    } else if (a == 24) {
                        this.d = aVar.f();
                    } else if (a == 32) {
                        this.e = aVar.f();
                    } else if (!g.a(aVar, a)) {
                        return this;
                    }
                }
            }
        }

        public a() {
            d();
        }

        public a d() {
            this.b = "";
            this.c = 0;
            this.d = g.f;
            this.e = "";
            this.f = "";
            this.g = g.f;
            this.h = g.f;
            this.i = g.f;
            this.j = null;
            this.k = null;
            this.l = null;
            this.m = null;
            this.n = f.d();
            this.o = "";
            this.p = "";
            this.q = false;
            this.r = "";
            this.s = "";
            this.t = g.f;
            this.u = null;
            this.v = false;
            this.w = d.d();
            this.x = null;
            this.y = "";
            this.z = "";
            this.A = "";
            this.B = "";
            this.C = 0;
            this.D = 0;
            this.E = false;
            this.F = null;
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (!this.b.equals("")) {
                bVar.a(1, this.b);
            }
            bVar.b(3, this.c);
            String[] strArr = this.d;
            int i2 = 0;
            if (strArr != null && strArr.length > 0) {
                int i3 = 0;
                while (true) {
                    String[] strArr2 = this.d;
                    if (i3 >= strArr2.length) {
                        break;
                    }
                    String str = strArr2[i3];
                    if (str != null) {
                        bVar.a(4, str);
                    }
                    i3++;
                }
            }
            if (!this.e.equals("")) {
                bVar.a(5, this.e);
            }
            if (!this.f.equals("")) {
                bVar.a(6, this.f);
            }
            String[] strArr3 = this.g;
            if (strArr3 != null && strArr3.length > 0) {
                int i4 = 0;
                while (true) {
                    String[] strArr4 = this.g;
                    if (i4 >= strArr4.length) {
                        break;
                    }
                    String str2 = strArr4[i4];
                    if (str2 != null) {
                        bVar.a(7, str2);
                    }
                    i4++;
                }
            }
            String[] strArr5 = this.h;
            if (strArr5 != null && strArr5.length > 0) {
                int i5 = 0;
                while (true) {
                    String[] strArr6 = this.h;
                    if (i5 >= strArr6.length) {
                        break;
                    }
                    String str3 = strArr6[i5];
                    if (str3 != null) {
                        bVar.a(8, str3);
                    }
                    i5++;
                }
            }
            String[] strArr7 = this.i;
            if (strArr7 != null && strArr7.length > 0) {
                int i6 = 0;
                while (true) {
                    String[] strArr8 = this.i;
                    if (i6 >= strArr8.length) {
                        break;
                    }
                    String str4 = strArr8[i6];
                    if (str4 != null) {
                        bVar.a(9, str4);
                    }
                    i6++;
                }
            }
            b bVar2 = this.j;
            if (bVar2 != null) {
                bVar.a(10, bVar2);
            }
            c cVar = this.k;
            if (cVar != null) {
                bVar.a(11, cVar);
            }
            C0014a aVar = this.l;
            if (aVar != null) {
                bVar.a(12, aVar);
            }
            h hVar = this.m;
            if (hVar != null) {
                bVar.a(13, hVar);
            }
            f[] fVarArr = this.n;
            if (fVarArr != null && fVarArr.length > 0) {
                int i7 = 0;
                while (true) {
                    f[] fVarArr2 = this.n;
                    if (i7 >= fVarArr2.length) {
                        break;
                    }
                    f fVar = fVarArr2[i7];
                    if (fVar != null) {
                        bVar.a(14, fVar);
                    }
                    i7++;
                }
            }
            if (!this.o.equals("")) {
                bVar.a(15, this.o);
            }
            if (!this.p.equals("")) {
                bVar.a(16, this.p);
            }
            bVar.a(17, this.q);
            if (!this.r.equals("")) {
                bVar.a(18, this.r);
            }
            if (!this.s.equals("")) {
                bVar.a(19, this.s);
            }
            String[] strArr9 = this.t;
            if (strArr9 != null && strArr9.length > 0) {
                int i8 = 0;
                while (true) {
                    String[] strArr10 = this.t;
                    if (i8 >= strArr10.length) {
                        break;
                    }
                    String str5 = strArr10[i8];
                    if (str5 != null) {
                        bVar.a(20, str5);
                    }
                    i8++;
                }
            }
            i iVar = this.u;
            if (iVar != null) {
                bVar.a(21, iVar);
            }
            boolean z2 = this.v;
            if (z2) {
                bVar.a(22, z2);
            }
            d[] dVarArr = this.w;
            if (dVarArr != null && dVarArr.length > 0) {
                while (true) {
                    d[] dVarArr2 = this.w;
                    if (i2 >= dVarArr2.length) {
                        break;
                    }
                    d dVar = dVarArr2[i2];
                    if (dVar != null) {
                        bVar.a(23, dVar);
                    }
                    i2++;
                }
            }
            g gVar = this.x;
            if (gVar != null) {
                bVar.a(24, gVar);
            }
            if (!this.y.equals("")) {
                bVar.a(25, this.y);
            }
            if (!this.z.equals("")) {
                bVar.a(26, this.z);
            }
            if (!this.B.equals("")) {
                bVar.a(27, this.B);
            }
            bVar.b(28, this.C);
            bVar.b(29, this.D);
            boolean z3 = this.E;
            if (z3) {
                bVar.a(30, z3);
            }
            if (!this.A.equals("")) {
                bVar.a(31, this.A);
            }
            e eVar = this.F;
            if (eVar != null) {
                bVar.a(32, eVar);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (!this.b.equals("")) {
                c2 += b.b(1, this.b);
            }
            int e2 = c2 + b.e(3, this.c);
            String[] strArr = this.d;
            int i2 = 0;
            if (strArr != null && strArr.length > 0) {
                int i3 = 0;
                int i4 = 0;
                int i5 = 0;
                while (true) {
                    String[] strArr2 = this.d;
                    if (i3 >= strArr2.length) {
                        break;
                    }
                    String str = strArr2[i3];
                    if (str != null) {
                        i5++;
                        i4 += b.b(str);
                    }
                    i3++;
                }
                e2 = e2 + i4 + (i5 * 1);
            }
            if (!this.e.equals("")) {
                e2 += b.b(5, this.e);
            }
            if (!this.f.equals("")) {
                e2 += b.b(6, this.f);
            }
            String[] strArr3 = this.g;
            if (strArr3 != null && strArr3.length > 0) {
                int i6 = 0;
                int i7 = 0;
                int i8 = 0;
                while (true) {
                    String[] strArr4 = this.g;
                    if (i6 >= strArr4.length) {
                        break;
                    }
                    String str2 = strArr4[i6];
                    if (str2 != null) {
                        i8++;
                        i7 += b.b(str2);
                    }
                    i6++;
                }
                e2 = e2 + i7 + (i8 * 1);
            }
            String[] strArr5 = this.h;
            if (strArr5 != null && strArr5.length > 0) {
                int i9 = 0;
                int i10 = 0;
                int i11 = 0;
                while (true) {
                    String[] strArr6 = this.h;
                    if (i9 >= strArr6.length) {
                        break;
                    }
                    String str3 = strArr6[i9];
                    if (str3 != null) {
                        i11++;
                        i10 += b.b(str3);
                    }
                    i9++;
                }
                e2 = e2 + i10 + (i11 * 1);
            }
            String[] strArr7 = this.i;
            if (strArr7 != null && strArr7.length > 0) {
                int i12 = 0;
                int i13 = 0;
                int i14 = 0;
                while (true) {
                    String[] strArr8 = this.i;
                    if (i12 >= strArr8.length) {
                        break;
                    }
                    String str4 = strArr8[i12];
                    if (str4 != null) {
                        i14++;
                        i13 += b.b(str4);
                    }
                    i12++;
                }
                e2 = e2 + i13 + (i14 * 1);
            }
            b bVar = this.j;
            if (bVar != null) {
                e2 += b.b(10, bVar);
            }
            c cVar = this.k;
            if (cVar != null) {
                e2 += b.b(11, cVar);
            }
            C0014a aVar = this.l;
            if (aVar != null) {
                e2 += b.b(12, aVar);
            }
            h hVar = this.m;
            if (hVar != null) {
                e2 += b.b(13, hVar);
            }
            f[] fVarArr = this.n;
            if (fVarArr != null && fVarArr.length > 0) {
                int i15 = e2;
                int i16 = 0;
                while (true) {
                    f[] fVarArr2 = this.n;
                    if (i16 >= fVarArr2.length) {
                        break;
                    }
                    f fVar = fVarArr2[i16];
                    if (fVar != null) {
                        i15 += b.b(14, fVar);
                    }
                    i16++;
                }
                e2 = i15;
            }
            if (!this.o.equals("")) {
                e2 += b.b(15, this.o);
            }
            if (!this.p.equals("")) {
                e2 += b.b(16, this.p);
            }
            int b2 = e2 + b.b(17, this.q);
            if (!this.r.equals("")) {
                b2 += b.b(18, this.r);
            }
            if (!this.s.equals("")) {
                b2 += b.b(19, this.s);
            }
            String[] strArr9 = this.t;
            if (strArr9 != null && strArr9.length > 0) {
                int i17 = 0;
                int i18 = 0;
                int i19 = 0;
                while (true) {
                    String[] strArr10 = this.t;
                    if (i17 >= strArr10.length) {
                        break;
                    }
                    String str5 = strArr10[i17];
                    if (str5 != null) {
                        i19++;
                        i18 += b.b(str5);
                    }
                    i17++;
                }
                b2 = b2 + i18 + (i19 * 2);
            }
            i iVar = this.u;
            if (iVar != null) {
                b2 += b.b(21, iVar);
            }
            boolean z2 = this.v;
            if (z2) {
                b2 += b.b(22, z2);
            }
            d[] dVarArr = this.w;
            if (dVarArr != null && dVarArr.length > 0) {
                while (true) {
                    d[] dVarArr2 = this.w;
                    if (i2 >= dVarArr2.length) {
                        break;
                    }
                    d dVar = dVarArr2[i2];
                    if (dVar != null) {
                        b2 += b.b(23, dVar);
                    }
                    i2++;
                }
            }
            g gVar = this.x;
            if (gVar != null) {
                b2 += b.b(24, gVar);
            }
            if (!this.y.equals("")) {
                b2 += b.b(25, this.y);
            }
            if (!this.z.equals("")) {
                b2 += b.b(26, this.z);
            }
            if (!this.B.equals("")) {
                b2 += b.b(27, this.B);
            }
            int e3 = b2 + b.e(28, this.C) + b.e(29, this.D);
            boolean z3 = this.E;
            if (z3) {
                e3 += b.b(30, z3);
            }
            if (!this.A.equals("")) {
                e3 += b.b(31, this.A);
            }
            e eVar = this.F;
            if (eVar != null) {
                return e3 + b.b(32, eVar);
            }
            return e3;
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                switch (a) {
                    case 0:
                        return this;
                    case 10:
                        this.b = aVar.i();
                        break;
                    case 24:
                        this.c = aVar.f();
                        break;
                    case MotionEventCompat.AXIS_GENERIC_3:
                        int b2 = g.b(aVar, 34);
                        String[] strArr = this.d;
                        int length = strArr == null ? 0 : strArr.length;
                        String[] strArr2 = new String[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.d, 0, strArr2, 0, length);
                        }
                        while (length < strArr2.length - 1) {
                            strArr2[length] = aVar.i();
                            aVar.a();
                            length++;
                        }
                        strArr2[length] = aVar.i();
                        this.d = strArr2;
                        break;
                    case MotionEventCompat.AXIS_GENERIC_11:
                        this.e = aVar.i();
                        break;
                    case 50:
                        this.f = aVar.i();
                        break;
                    case 58:
                        int b3 = g.b(aVar, 58);
                        String[] strArr3 = this.g;
                        int length2 = strArr3 == null ? 0 : strArr3.length;
                        String[] strArr4 = new String[(b3 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.g, 0, strArr4, 0, length2);
                        }
                        while (length2 < strArr4.length - 1) {
                            strArr4[length2] = aVar.i();
                            aVar.a();
                            length2++;
                        }
                        strArr4[length2] = aVar.i();
                        this.g = strArr4;
                        break;
                    case 66:
                        int b4 = g.b(aVar, 66);
                        String[] strArr5 = this.h;
                        int length3 = strArr5 == null ? 0 : strArr5.length;
                        String[] strArr6 = new String[(b4 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.h, 0, strArr6, 0, length3);
                        }
                        while (length3 < strArr6.length - 1) {
                            strArr6[length3] = aVar.i();
                            aVar.a();
                            length3++;
                        }
                        strArr6[length3] = aVar.i();
                        this.h = strArr6;
                        break;
                    case 74:
                        int b5 = g.b(aVar, 74);
                        String[] strArr7 = this.i;
                        int length4 = strArr7 == null ? 0 : strArr7.length;
                        String[] strArr8 = new String[(b5 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.i, 0, strArr8, 0, length4);
                        }
                        while (length4 < strArr8.length - 1) {
                            strArr8[length4] = aVar.i();
                            aVar.a();
                            length4++;
                        }
                        strArr8[length4] = aVar.i();
                        this.i = strArr8;
                        break;
                    case BuildConfig.VERSION_CODE:
                        if (this.j == null) {
                            this.j = new b();
                        }
                        aVar.a(this.j);
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_DISPATCH_PERIOD_SECONDS:
                        if (this.k == null) {
                            this.k = new c();
                        }
                        aVar.a(this.k);
                        break;
                    case 98:
                        if (this.l == null) {
                            this.l = new C0014a();
                        }
                        aVar.a(this.l);
                        break;
                    case 106:
                        if (this.m == null) {
                            this.m = new h();
                        }
                        aVar.a(this.m);
                        break;
                    case 114:
                        int b6 = g.b(aVar, 114);
                        f[] fVarArr = this.n;
                        int length5 = fVarArr == null ? 0 : fVarArr.length;
                        f[] fVarArr2 = new f[(b6 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.n, 0, fVarArr2, 0, length5);
                        }
                        while (length5 < fVarArr2.length - 1) {
                            fVarArr2[length5] = new f();
                            aVar.a(fVarArr2[length5]);
                            aVar.a();
                            length5++;
                        }
                        fVarArr2[length5] = new f();
                        aVar.a(fVarArr2[length5]);
                        this.n = fVarArr2;
                        break;
                    case 122:
                        this.o = aVar.i();
                        break;
                    case 130:
                        this.p = aVar.i();
                        break;
                    case 136:
                        this.q = aVar.h();
                        break;
                    case 146:
                        this.r = aVar.i();
                        break;
                    case 154:
                        this.s = aVar.i();
                        break;
                    case 162:
                        int b7 = g.b(aVar, 162);
                        String[] strArr9 = this.t;
                        int length6 = strArr9 == null ? 0 : strArr9.length;
                        String[] strArr10 = new String[(b7 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.t, 0, strArr10, 0, length6);
                        }
                        while (length6 < strArr10.length - 1) {
                            strArr10[length6] = aVar.i();
                            aVar.a();
                            length6++;
                        }
                        strArr10[length6] = aVar.i();
                        this.t = strArr10;
                        break;
                    case 170:
                        if (this.u == null) {
                            this.u = new i();
                        }
                        aVar.a(this.u);
                        break;
                    case 176:
                        this.v = aVar.h();
                        break;
                    case 186:
                        int b8 = g.b(aVar, 186);
                        d[] dVarArr = this.w;
                        int length7 = dVarArr == null ? 0 : dVarArr.length;
                        d[] dVarArr2 = new d[(b8 + length7)];
                        if (length7 != 0) {
                            System.arraycopy(this.w, 0, dVarArr2, 0, length7);
                        }
                        while (length7 < dVarArr2.length - 1) {
                            dVarArr2[length7] = new d();
                            aVar.a(dVarArr2[length7]);
                            aVar.a();
                            length7++;
                        }
                        dVarArr2[length7] = new d();
                        aVar.a(dVarArr2[length7]);
                        this.w = dVarArr2;
                        break;
                    case 194:
                        if (this.x == null) {
                            this.x = new g();
                        }
                        aVar.a(this.x);
                        break;
                    case 202:
                        this.y = aVar.i();
                        break;
                    case 210:
                        this.z = aVar.i();
                        break;
                    case 218:
                        this.B = aVar.i();
                        break;
                    case 224:
                        this.C = aVar.f();
                        break;
                    case 232:
                        this.D = aVar.f();
                        break;
                    case 240:
                        this.E = aVar.h();
                        break;
                    case 250:
                        this.A = aVar.i();
                        break;
                    case 258:
                        if (this.F == null) {
                            this.F = new e();
                        }
                        aVar.a(this.F);
                        break;
                    default:
                        if (g.a(aVar, a)) {
                            break;
                        } else {
                            return this;
                        }
                }
            }
        }

        public static a a(byte[] bArr) throws d {
            return (a) e.a(new a(), bArr);
        }
    }
}
