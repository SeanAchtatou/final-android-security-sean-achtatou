package com.yandex.metrica.impl.ob;

import android.util.Pair;
import androidx.annotation.NonNull;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.impl.ob.pj;

class bv {
    @NonNull
    private final Revenue a;
    private final vo<String> b = new vl(30720, "revenue payload", this.e);
    private final vo<String> c = new vn(new vl(184320, "receipt data", this.e), "<truncated data was not sent, see METRIKALIB-4568>");
    private final vo<String> d = new vn(new vm(1000, "receipt signature", this.e), "<truncated data was not sent, see METRIKALIB-4568>");
    @NonNull
    private final tp e;

    bv(@NonNull Revenue revenue, @NonNull tp tpVar) {
        this.e = tpVar;
        this.a = revenue;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public Pair<byte[], Integer> a() {
        pj pjVar = new pj();
        pjVar.d = this.a.currency.getCurrencyCode().getBytes();
        if (cg.a(this.a.price)) {
            pjVar.c = this.a.price.doubleValue();
        }
        if (cg.a(this.a.priceMicros)) {
            pjVar.h = this.a.priceMicros.longValue();
        }
        pjVar.e = ce.d(new vm(200, "revenue productID", this.e).a(this.a.productID));
        pjVar.b = ua.a(this.a.quantity, 1);
        pjVar.f = ce.d(this.b.a(this.a.payload));
        int i = 0;
        if (cg.a(this.a.receipt)) {
            pj.a aVar = new pj.a();
            String a2 = this.c.a(this.a.receipt.data);
            if (vi.a(this.a.receipt.data, a2)) {
                i = this.a.receipt.data.length() + 0;
            }
            aVar.b = ce.d(a2);
            aVar.c = ce.d(this.d.a(this.a.receipt.signature));
            pjVar.g = aVar;
        }
        return new Pair<>(e.a(pjVar), Integer.valueOf(i));
    }
}
