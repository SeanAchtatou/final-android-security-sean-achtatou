package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;

public class gj extends ft {
    public gj(di diVar) {
        super(diVar);
    }

    public boolean a(@NonNull t tVar) {
        String z = a().z();
        String p = tVar.p();
        a().b(p);
        if (TextUtils.equals(z, p)) {
            return false;
        }
        a().a(o.c());
        return false;
    }
}
