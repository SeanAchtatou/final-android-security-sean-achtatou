package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.io.IOException;

public class ku<T> implements kv<T> {
    @NonNull
    private final kv<T> a;
    @NonNull
    private final uf b;

    public ku(@NonNull kv<T> kvVar, @NonNull uf ufVar) {
        this.a = kvVar;
        this.b = ufVar;
    }

    @NonNull
    public byte[] a(@NonNull T t) {
        try {
            return this.b.a(this.a.a(t));
        } catch (Throwable th) {
            return new byte[0];
        }
    }

    @NonNull
    public T b(@NonNull byte[] bArr) throws IOException {
        try {
            return this.a.b(this.b.b(bArr));
        } catch (Throwable th) {
            throw new IOException(th);
        }
    }

    @NonNull
    public T c() {
        return this.a.c();
    }
}
