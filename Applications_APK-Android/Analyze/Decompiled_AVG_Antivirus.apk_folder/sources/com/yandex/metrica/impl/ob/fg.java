package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.ab;
import java.util.LinkedList;

public class fg extends fh<ft> {
    private final gd a;
    private final hb b;
    private final ga c;

    public fg(di diVar) {
        this.a = new gd(diVar);
        this.b = new hb(diVar);
        this.c = new ga(diVar);
    }

    public fe<ft> a(int i) {
        LinkedList linkedList = new LinkedList();
        int i2 = AnonymousClass1.a[ab.a.a(i).ordinal()];
        if (i2 == 1) {
            linkedList.add(this.b);
            linkedList.add(this.a);
        } else if (i2 == 2) {
            linkedList.add(this.a);
        } else if (i2 == 3) {
            linkedList.add(this.c);
        }
        return new fd(linkedList);
    }

    /* renamed from: com.yandex.metrica.impl.ob.fg$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[ab.a.values().length];

        static {
            try {
                a[ab.a.EVENT_TYPE_START.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[ab.a.EVENT_TYPE_INIT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[ab.a.EVENT_TYPE_UPDATE_FOREGROUND_TIME.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }
}
