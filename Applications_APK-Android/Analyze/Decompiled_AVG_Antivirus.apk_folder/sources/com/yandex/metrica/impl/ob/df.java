package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class df {
    @NonNull
    private final String a;
    @Nullable
    private final String b;

    public df(@NonNull String str, @Nullable String str2) {
        this.a = str;
        this.b = str2;
    }

    public String a() {
        return this.b;
    }

    public String b() {
        return this.a;
    }

    public String toString() {
        return this.a + "_" + this.b;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        df dfVar = (df) o;
        String str = this.a;
        if (str == null ? dfVar.a != null : !str.equals(dfVar.a)) {
            return false;
        }
        String str2 = this.b;
        if (str2 != null) {
            return str2.equals(dfVar.b);
        }
        if (dfVar.b == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }
}
