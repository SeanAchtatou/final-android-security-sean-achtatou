package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Collections;
import java.util.List;

public class ix {
    @NonNull
    public final String a;
    public final int b;
    public final long c;
    @NonNull
    public final String d;
    @Nullable
    public final Integer e;
    @NonNull
    public final List<StackTraceElement> f;

    public ix(@NonNull String str, int i, long j, @NonNull String str2, @Nullable Integer num, @Nullable List<StackTraceElement> list) {
        this.a = str;
        this.b = i;
        this.c = j;
        this.d = str2;
        this.e = num;
        this.f = list == null ? Collections.emptyList() : Collections.unmodifiableList(list);
    }
}
