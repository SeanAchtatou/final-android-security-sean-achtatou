package com.yandex.metrica.impl.ob;

import android.util.Pair;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.cb;
import com.yandex.metrica.impl.ob.pp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class li implements lc<cb.a, pp.a.f> {
    private static final Map<Integer, cb.a.C0002a> a = Collections.unmodifiableMap(new HashMap<Integer, cb.a.C0002a>() {
        {
            put(1, cb.a.C0002a.WIFI);
            put(2, cb.a.C0002a.CELL);
        }
    });
    private static final Map<cb.a.C0002a, Integer> b = Collections.unmodifiableMap(new HashMap<cb.a.C0002a, Integer>() {
        {
            put(cb.a.C0002a.WIFI, 1);
            put(cb.a.C0002a.CELL, 2);
        }
    });

    @NonNull
    /* renamed from: a */
    public pp.a.f b(@NonNull cb.a aVar) {
        pp.a.f fVar = new pp.a.f();
        fVar.b = aVar.a;
        fVar.c = aVar.b;
        fVar.d = aVar.c;
        fVar.e = a(aVar.d);
        fVar.f = aVar.e == null ? 0 : aVar.e.longValue();
        fVar.g = b(aVar.f);
        return fVar;
    }

    @NonNull
    public cb.a a(@NonNull pp.a.f fVar) {
        return new cb.a(fVar.b, fVar.c, fVar.d, a(fVar.e), Long.valueOf(fVar.f), a(fVar.g));
    }

    @NonNull
    private pp.a.f.C0016a[] a(@NonNull List<Pair<String, String>> list) {
        pp.a.f.C0016a[] aVarArr = new pp.a.f.C0016a[list.size()];
        int i = 0;
        for (Pair next : list) {
            pp.a.f.C0016a aVar = new pp.a.f.C0016a();
            aVar.b = (String) next.first;
            aVar.c = (String) next.second;
            aVarArr[i] = aVar;
            i++;
        }
        return aVarArr;
    }

    @NonNull
    private List<Pair<String, String>> a(@NonNull pp.a.f.C0016a[] aVarArr) {
        ArrayList arrayList = new ArrayList(aVarArr.length);
        for (pp.a.f.C0016a aVar : aVarArr) {
            arrayList.add(new Pair(aVar.b, aVar.c));
        }
        return arrayList;
    }

    @NonNull
    private List<cb.a.C0002a> a(@NonNull int[] iArr) {
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(a.get(Integer.valueOf(valueOf)));
        }
        return arrayList;
    }

    @NonNull
    private int[] b(@NonNull List<cb.a.C0002a> list) {
        int[] iArr = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            iArr[i] = b.get(list.get(i)).intValue();
        }
        return iArr;
    }
}
