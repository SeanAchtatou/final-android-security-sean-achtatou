package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import com.google.android.gms.common.internal.ImagesContract;
import com.yandex.metrica.impl.ob.bj;
import com.yandex.metrica.impl.ob.pq;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class rg {
    private static final Map<pq.a.b.C0018a, String> a = Collections.unmodifiableMap(new HashMap<pq.a.b.C0018a, String>() {
        {
            put(pq.a.b.C0018a.COMPLETE, "complete");
            put(pq.a.b.C0018a.ERROR, "error");
            put(pq.a.b.C0018a.OFFLINE, "offline");
            put(pq.a.b.C0018a.INCOMPATIBLE_NETWORK_TYPE, "incompatible_network_type");
        }
    });
    private static final Map<bj.a, String> b = Collections.unmodifiableMap(new HashMap<bj.a, String>() {
        {
            put(bj.a.WIFI, "wifi");
            put(bj.a.CELL, "cell");
            put(bj.a.OFFLINE, "offline");
            put(bj.a.UNDEFINED, "undefined");
        }
    });

    public String a(@NonNull pq.a.b bVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("id", bVar.a().a);
            jSONObject.putOpt(ImagesContract.URL, bVar.a().b);
            jSONObject.putOpt(NotificationCompat.CATEGORY_STATUS, a.get(bVar.b()));
            jSONObject.putOpt("code", bVar.d());
            if (!cg.a(bVar.e())) {
                jSONObject.putOpt("body", a(bVar.e()));
            } else if (!cg.a(bVar.h())) {
                jSONObject.putOpt("body", a(bVar.h()));
            }
            jSONObject.putOpt("headers", a(bVar.f()));
            jSONObject.putOpt("error", a(bVar.g()));
            jSONObject.putOpt("network_type", b.get(bVar.c()));
            return jSONObject.toString();
        } catch (Throwable th) {
            return th.toString();
        }
    }

    public String a(@NonNull pq.a.C0017a aVar) {
        try {
            return new JSONObject().put("id", aVar.a).toString();
        } catch (Throwable th) {
            return th.toString();
        }
    }

    @Nullable
    private JSONObject a(@Nullable Map<String, List<String>> map) throws JSONException {
        if (cg.a((Map) map)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        for (Map.Entry next : map.entrySet()) {
            String str = (String) next.getKey();
            if (!cg.a((Collection) next.getValue())) {
                List<String> a2 = cg.a((List) next.getValue(), 10);
                ArrayList arrayList = new ArrayList();
                for (String str2 : a2) {
                    if (!TextUtils.isEmpty(str2)) {
                        arrayList.add(cg.a(str2, 100));
                    }
                }
                jSONObject.putOpt(str, TextUtils.join(",", arrayList));
            }
        }
        return jSONObject;
    }

    @Nullable
    private String a(@NonNull byte[] bArr) {
        return Base64.encodeToString(bArr, 0);
    }

    @Nullable
    private String a(@Nullable Throwable th) {
        if (th == null) {
            return null;
        }
        return th.toString() + "\n" + Log.getStackTraceString(th);
    }
}
