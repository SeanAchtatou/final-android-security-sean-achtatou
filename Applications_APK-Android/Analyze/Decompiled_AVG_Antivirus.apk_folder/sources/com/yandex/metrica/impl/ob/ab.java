package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.InputDeviceCompat;
import androidx.fragment.app.FragmentTransaction;
import com.yandex.metrica.impl.ob.o;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;

public final class ab {
    public static final EnumSet<a> a = EnumSet.of(a.EVENT_TYPE_INIT, a.EVENT_TYPE_FIRST_ACTIVATION, a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST, a.EVENT_TYPE_SEND_REFERRER, a.EVENT_TYPE_APP_UPDATE);
    private static final EnumSet<a> b = EnumSet.of(a.EVENT_TYPE_UNDEFINED, a.EVENT_TYPE_PURGE_BUFFER, a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST, a.EVENT_TYPE_SEND_REFERRER, a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, a.EVENT_TYPE_ACTIVATION);
    private static final EnumSet<a> c = EnumSet.of(a.EVENT_TYPE_SET_USER_INFO, a.EVENT_TYPE_REPORT_USER_INFO, a.EVENT_TYPE_IDENTITY, a.EVENT_TYPE_UNDEFINED, a.EVENT_TYPE_INIT, a.EVENT_TYPE_APP_UPDATE, a.EVENT_TYPE_SEND_REFERRER, a.EVENT_TYPE_ALIVE, a.EVENT_TYPE_STARTUP, a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, a.EVENT_TYPE_ACTIVATION);
    private static final EnumSet<a> d = EnumSet.of(a.EVENT_TYPE_UPDATE_FOREGROUND_TIME, a.EVENT_TYPE_SET_USER_INFO, a.EVENT_TYPE_REPORT_USER_INFO, a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE);
    private static final EnumSet<a> e = EnumSet.of(a.EVENT_TYPE_EXCEPTION_UNHANDLED, a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, a.EVENT_TYPE_EXCEPTION_USER, a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, a.EVENT_TYPE_NATIVE_CRASH, a.EVENT_TYPE_REGULAR);
    private static final EnumSet<a> f = EnumSet.of(a.EVENT_TYPE_DIAGNOSTIC, a.EVENT_TYPE_DIAGNOSTIC_STATBOX, a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING);
    private static final EnumSet<a> g = EnumSet.of(a.EVENT_TYPE_REGULAR);
    private static final EnumSet<a> h = EnumSet.of(a.EVENT_TYPE_NATIVE_CRASH);

    @SuppressLint({"UseSparseArrays"})
    public enum a {
        EVENT_TYPE_UNDEFINED(-1, "Unrecognized action"),
        EVENT_TYPE_INIT(0, "First initialization event"),
        EVENT_TYPE_REGULAR(1, "Regular event"),
        EVENT_TYPE_UPDATE_FOREGROUND_TIME(3, "Update foreground time"),
        EVENT_TYPE_EXCEPTION_USER(5, "Error from developer"),
        EVENT_TYPE_ALIVE(7, "App is still alive"),
        EVENT_TYPE_SET_USER_INFO(9, "User info"),
        EVENT_TYPE_REPORT_USER_INFO(10, "Report user info"),
        EVENT_TYPE_SEND_USER_PROFILE(40961, "Send user profile"),
        EVENT_TYPE_SET_USER_PROFILE_ID(40962, "Set user profile ID"),
        EVENT_TYPE_SEND_REVENUE_EVENT(40976, "Send revenue event"),
        EVENT_TYPE_PURGE_BUFFER(256, "Forcible buffer clearing"),
        EVENT_TYPE_NATIVE_CRASH(768, "Native crash of App"),
        EVENT_TYPE_STARTUP(1536, "Sending the startup due to lack of data"),
        EVENT_TYPE_IDENTITY(1792, "System identification"),
        EVENT_TYPE_DIAGNOSTIC(2048, "Diagnostic event"),
        EVENT_TYPE_DIAGNOSTIC_STATBOX(2049, "Diagnostic statbox event"),
        EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING(2050, "Disable stat send"),
        EVENT_TYPE_STATBOX(2304, "Event with statistical data"),
        EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST(4096, "Referrer received"),
        EVENT_TYPE_SEND_REFERRER(FragmentTransaction.TRANSIT_FRAGMENT_OPEN, "Send referrer"),
        EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES(InputDeviceCompat.SOURCE_TOUCHSCREEN, "Referrer obtained"),
        EVENT_TYPE_APP_ENVIRONMENT_UPDATED(5376, "App Environment Updated"),
        EVENT_TYPE_APP_ENVIRONMENT_CLEARED(5632, "App Environment Cleared"),
        EVENT_TYPE_EXCEPTION_UNHANDLED(5888, "Crash of App"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE(5889, "Crash of App, red from file"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT(5890, "Crash of App, passed to server via intent"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF(5891, "Crash of App"),
        EVENT_TYPE_EXCEPTION_USER_PROTOBUF(5892, "Error from developer"),
        EVENT_TYPE_ANR(5968, "ANR"),
        EVENT_TYPE_ACTIVATION(6144, "Activation of metrica"),
        EVENT_TYPE_FIRST_ACTIVATION(6145, "First activation of metrica"),
        EVENT_TYPE_START(6400, "Start of new session"),
        EVENT_TYPE_CUSTOM_EVENT(8192, "Custom event"),
        EVENT_TYPE_APP_OPEN(8208, "App open event"),
        EVENT_TYPE_APP_UPDATE(8224, "App update event"),
        EVENT_TYPE_PERMISSIONS(12288, "Permissions changed event"),
        EVENT_TYPE_APP_FEATURES(12289, "Features, required by application"),
        EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG(16384, "Update pre-activation config");
        
        static final HashMap<Integer, a> N = new HashMap<>();
        private final int O;
        private final String P;

        static {
            for (a aVar : values()) {
                N.put(Integer.valueOf(aVar.a()), aVar);
            }
        }

        private a(int i, String str) {
            this.O = i;
            this.P = str;
        }

        public int a() {
            return this.O;
        }

        public String b() {
            return this.P;
        }

        public static a a(int i) {
            a aVar = N.get(Integer.valueOf(i));
            return aVar == null ? EVENT_TYPE_UNDEFINED : aVar;
        }
    }

    public static boolean a(a aVar) {
        return !b.contains(aVar);
    }

    public static boolean b(a aVar) {
        return !c.contains(aVar);
    }

    public static boolean a(int i) {
        return d.contains(a.a(i));
    }

    public static boolean b(int i) {
        return e.contains(a.a(i));
    }

    public static boolean c(int i) {
        return g.contains(a.a(i));
    }

    public static boolean d(int i) {
        return f.contains(a.a(i));
    }

    public static boolean e(int i) {
        return !h.contains(a.a(i));
    }

    static t a(a aVar, String str, @NonNull tp tpVar) {
        return new o(str, "", aVar.a(), tpVar);
    }

    public static t a(a aVar, @NonNull tp tpVar) {
        return new o("", aVar.a(), tpVar);
    }

    public static t a(String str, @NonNull tp tpVar) {
        return new o(str, a.EVENT_TYPE_REGULAR.a(), tpVar);
    }

    static t a(String str, String str2, @NonNull tp tpVar) {
        return new o(str2, str, a.EVENT_TYPE_REGULAR.a(), tpVar);
    }

    static t b(String str, String str2, @NonNull tp tpVar) {
        return new o(str2, str, a.EVENT_TYPE_STATBOX.a(), tpVar);
    }

    public static t a() {
        t a2 = new t().a(a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING.a());
        try {
            a2.c(th.a().toString());
        } catch (JSONException e2) {
        }
        return a2;
    }

    static t a(String str, byte[] bArr, @NonNull tp tpVar) {
        return new o(bArr, str, a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF.a(), tpVar);
    }

    static t b(String str, @NonNull tp tpVar) {
        return new o(str, a.EVENT_TYPE_START.a(), tpVar);
    }

    static t c(String str, @NonNull tp tpVar) {
        return new o(str, a.EVENT_TYPE_UPDATE_FOREGROUND_TIME.a(), tpVar);
    }

    public static o b(String str, byte[] bArr, @NonNull tp tpVar) {
        return a(bArr, str, a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, tpVar);
    }

    public static t a(String str, byte[] bArr, int i, @NonNull HashMap<o.a, Integer> hashMap, @NonNull tp tpVar) {
        return a(bArr, str, a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, tpVar).a(hashMap).c(i);
    }

    private static o a(byte[] bArr, String str, a aVar, @NonNull tp tpVar) {
        return new o(bArr, str, aVar.a(), tpVar);
    }

    public static t d(String str, @NonNull tp tpVar) {
        return new o("", str, a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST.a(), tpVar);
    }

    public static t a(@Nullable qf qfVar, @NonNull tp tpVar) {
        if (qfVar == null) {
            return new o("", a.EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES.a(), tpVar);
        }
        return new o(qfVar.a, a.EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES.a(), tpVar).a(qfVar.a());
    }

    static t e(String str, @NonNull tp tpVar) {
        return c("open", str, tpVar);
    }

    static t f(String str, @NonNull tp tpVar) {
        return c("referral", str, tpVar);
    }

    static t c(String str, String str2, @NonNull tp tpVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", str);
        hashMap.put("link", str2);
        return new o(th.a((Map) hashMap), "", a.EVENT_TYPE_APP_OPEN.a(), tpVar);
    }

    public static t a(bk bkVar, @NonNull tp tpVar) {
        return new o(bkVar == null ? "" : bkVar.a(), "", a.EVENT_TYPE_ACTIVATION.a(), tpVar);
    }

    static t a(int i, String str, String str2, Map<String, Object> map, @NonNull tp tpVar) {
        return new o(str2, str, a.EVENT_TYPE_CUSTOM_EVENT.a(), i, tpVar).e(th.a((Map) map));
    }
}
