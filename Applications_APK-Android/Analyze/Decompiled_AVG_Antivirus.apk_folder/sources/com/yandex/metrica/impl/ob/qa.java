package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.profile.UserProfile;
import java.util.Map;

public class qa implements IReporter {
    static final vx<String> a = new vt(new vr("Event name"));
    static final vx<String> b = new vt(new vr("Error message"));
    static final vx<Throwable> c = new vt(new vs("Unhandled exception"));
    static final vx<UserProfile> d = new vt(new vs("User profile"));
    static final vx<Revenue> e = new vt(new vs("Revenue"));

    public void reportEvent(@NonNull String eventName) throws vu {
        a.a(eventName);
    }

    public void reportEvent(@NonNull String eventName, @Nullable String jsonValue) throws vu {
        a.a(eventName);
    }

    public void reportEvent(@NonNull String eventName, @Nullable Map<String, Object> map) throws vu {
        a.a(eventName);
    }

    public void reportError(@NonNull String message, @Nullable Throwable error) throws vu {
        b.a(message);
    }

    public void reportUnhandledException(@NonNull Throwable exception) throws vu {
        c.a(exception);
    }

    public void resumeSession() {
    }

    public void pauseSession() {
    }

    public void setUserProfileID(@Nullable String profileID) {
    }

    public void reportUserProfile(@NonNull UserProfile profile) throws vu {
        d.a(profile);
    }

    public void reportRevenue(@NonNull Revenue revenue) throws vu {
        e.a(revenue);
    }

    public void setStatisticsSending(boolean enabled) {
    }

    public void sendEventsBuffer() {
    }
}
