package com.yandex.metrica.impl.ob;

import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class cm {
    private final a a;
    private final ub<Thread, StackTraceElement[], ix> b;

    interface a {
        Thread a();

        Map<Thread, StackTraceElement[]> b();
    }

    public cm() {
        this(new a() {
            public Thread a() {
                return Looper.getMainLooper().getThread();
            }

            public Map<Thread, StackTraceElement[]> b() {
                return Thread.getAllStackTraces();
            }
        }, new cl());
    }

    @VisibleForTesting
    cm(@NonNull a aVar, @NonNull ub<Thread, StackTraceElement[], ix> ubVar) {
        this.a = aVar;
        this.b = ubVar;
    }

    public iu a() {
        Thread a2 = this.a.a();
        return new iu(b(a2), a(a2, null));
    }

    public List<ix> a(@Nullable Thread thread) {
        Thread a2 = this.a.a();
        List<ix> a3 = a(a2, thread);
        if (thread != a2) {
            a3.add(0, b(a2));
        }
        return a3;
    }

    private ix b(@NonNull Thread thread) {
        StackTraceElement[] stackTraceElementArr;
        try {
            stackTraceElementArr = thread.getStackTrace();
        } catch (SecurityException e) {
            stackTraceElementArr = null;
        }
        return this.b.a(thread, stackTraceElementArr);
    }

    private List<ix> a(@NonNull Thread thread, @Nullable Thread thread2) {
        Map<Thread, StackTraceElement[]> map;
        ArrayList arrayList = new ArrayList();
        TreeMap treeMap = new TreeMap(new Comparator<Thread>() {
            /* renamed from: a */
            public int compare(Thread thread, Thread thread2) {
                if (thread == thread2) {
                    return 0;
                }
                return ce.d(thread.getName(), thread2.getName());
            }
        });
        try {
            map = this.a.b();
        } catch (SecurityException e) {
            map = null;
        }
        if (map != null) {
            treeMap.putAll(map);
        }
        if (thread2 != null) {
            treeMap.remove(thread2);
        }
        for (Map.Entry entry : treeMap.entrySet()) {
            Thread thread3 = (Thread) entry.getKey();
            if (!(thread3 == thread || thread3 == thread2)) {
                arrayList.add(this.b.a(thread3, (StackTraceElement[]) entry.getValue()));
            }
        }
        return arrayList;
    }
}
