package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pf;
import java.util.List;

public class jb implements ld<StackTraceElement, pf.d> {
    @NonNull
    private jc a = new jc();

    @NonNull
    public /* synthetic */ Object b(@NonNull Object obj) {
        return a((List<StackTraceElement>) ((List) obj));
    }

    @NonNull
    public pf.d[] a(@NonNull List<StackTraceElement> list) {
        pf.d[] dVarArr = new pf.d[list.size()];
        int i = 0;
        for (StackTraceElement a2 : list) {
            dVarArr[i] = this.a.b(a2);
            i++;
        }
        return dVarArr;
    }

    @NonNull
    public List<StackTraceElement> a(pf.d[] dVarArr) {
        throw new UnsupportedOperationException();
    }
}
