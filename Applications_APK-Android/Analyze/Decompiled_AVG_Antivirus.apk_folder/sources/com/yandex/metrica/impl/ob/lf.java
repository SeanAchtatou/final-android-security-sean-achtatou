package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pm;
import java.util.ArrayList;
import java.util.List;

public class lf implements ky<List<nt>, pm.a> {
    @NonNull
    public /* synthetic */ Object b(@NonNull Object obj) {
        return a((List<nt>) ((List) obj));
    }

    @NonNull
    public pm.a a(@NonNull List<nt> list) {
        pm.a aVar = new pm.a();
        aVar.b = new pm.a.C0010a[list.size()];
        for (int i = 0; i < list.size(); i++) {
            aVar.b[i] = a(list.get(i));
        }
        return aVar;
    }

    @NonNull
    public List<nt> a(@NonNull pm.a aVar) {
        ArrayList arrayList = new ArrayList(aVar.b.length);
        for (pm.a.C0010a a : aVar.b) {
            arrayList.add(a(a));
        }
        return arrayList;
    }

    @NonNull
    private pm.a.C0010a a(@NonNull nt ntVar) {
        pm.a.C0010a aVar = new pm.a.C0010a();
        aVar.b = ntVar.a;
        aVar.c = ntVar.b;
        return aVar;
    }

    @NonNull
    private nt a(@NonNull pm.a.C0010a aVar) {
        return new nt(aVar.b, aVar.c);
    }
}
