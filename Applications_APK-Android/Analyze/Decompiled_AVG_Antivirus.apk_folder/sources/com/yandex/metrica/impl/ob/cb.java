package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.google.android.gms.common.internal.ImagesContract;
import com.jobstrak.drawingfun.lib.mopub.common.Constants;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.yandex.metrica.impl.ob.pp;
import com.yandex.metrica.impl.ob.rq;
import com.yandex.metrica.impl.ob.th;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class cb {
    private static final Map<String, a.C0002a> a = Collections.unmodifiableMap(new HashMap<String, a.C0002a>() {
        {
            put("wifi", a.C0002a.WIFI);
            put("cell", a.C0002a.CELL);
        }
    });

    public static class b {
        private List<nt> A = new ArrayList();
        private rq.a a = new rq.a();
        private a b;
        private boolean c;
        /* access modifiers changed from: private */
        public boolean d;
        /* access modifiers changed from: private */
        public boolean e;
        /* access modifiers changed from: private */
        public boolean f;
        /* access modifiers changed from: private */
        public boolean g;
        private List<String> h;
        private String i;
        private List<String> j;
        private String k;
        private List<String> l;
        private String m;
        private String n;
        private String o;
        private String p;
        private rt q = null;
        @Nullable
        private rr r = null;
        private mh s;
        private mc t;
        private Long u;
        private List<a> v;
        private String w;
        private List<String> x;
        private se y;
        @Nullable
        private rs z;

        public enum a {
            BAD,
            OK
        }

        public rq a() {
            return this.a.a();
        }

        public rq.a b() {
            return this.a;
        }

        /* access modifiers changed from: private */
        public void a(boolean z2) {
            this.a.a(z2);
        }

        /* access modifiers changed from: private */
        public void b(boolean z2) {
            this.a.b(z2);
        }

        /* access modifiers changed from: private */
        public void c(boolean z2) {
            this.d = z2;
        }

        /* access modifiers changed from: private */
        public void d(boolean z2) {
            this.e = z2;
        }

        /* access modifiers changed from: package-private */
        public void a(@NonNull String str, boolean z2) {
            this.A.add(new nt(str, z2));
        }

        /* access modifiers changed from: private */
        public void a(List<String> list) {
            this.h = list;
        }

        public List<String> c() {
            return this.h;
        }

        /* access modifiers changed from: private */
        public void a(@Nullable String str) {
            this.m = str;
        }

        @Nullable
        public String d() {
            return this.m;
        }

        /* access modifiers changed from: private */
        public void b(String str) {
            this.i = str;
        }

        public String e() {
            return this.i;
        }

        /* access modifiers changed from: private */
        public void b(List<String> list) {
            this.j = list;
        }

        public List<String> f() {
            return this.j;
        }

        /* access modifiers changed from: private */
        public void c(String str) {
            this.k = str;
        }

        public String g() {
            return this.k;
        }

        /* access modifiers changed from: private */
        public void c(List<String> list) {
            this.l = list;
        }

        public List<String> h() {
            return this.l;
        }

        /* access modifiers changed from: private */
        public void d(String str) {
            this.n = str;
        }

        public String i() {
            return this.n;
        }

        /* access modifiers changed from: private */
        public void e(String str) {
            this.o = str;
        }

        public String j() {
            return this.o;
        }

        /* access modifiers changed from: private */
        public void f(String str) {
            this.p = str;
        }

        public String k() {
            return this.p;
        }

        /* access modifiers changed from: private */
        public void a(a aVar) {
            this.b = aVar;
        }

        public a l() {
            return this.b;
        }

        public boolean m() {
            return this.c;
        }

        /* access modifiers changed from: private */
        public void e(boolean z2) {
            this.c = z2;
        }

        /* access modifiers changed from: private */
        public void a(rt rtVar) {
            this.q = rtVar;
        }

        public rt n() {
            return this.q;
        }

        /* access modifiers changed from: private */
        public void a(@NonNull rr rrVar) {
            this.r = rrVar;
        }

        @Nullable
        public rr o() {
            return this.r;
        }

        @NonNull
        public rs p() {
            return this.z;
        }

        public List<nt> q() {
            return this.A;
        }

        public mh r() {
            return this.s;
        }

        public mc s() {
            return this.t;
        }

        /* access modifiers changed from: private */
        public void f(boolean z2) {
            this.a.c(z2);
        }

        /* access modifiers changed from: private */
        public void g(boolean z2) {
            this.a.d(z2);
        }

        /* access modifiers changed from: private */
        public void h(boolean z2) {
            this.a.e(z2);
        }

        /* access modifiers changed from: private */
        public void a(Long l2) {
            this.u = l2;
        }

        public Long t() {
            return this.u;
        }

        /* access modifiers changed from: private */
        public void d(List<a> list) {
            this.v = list;
        }

        public List<a> u() {
            return this.v;
        }

        public String v() {
            return this.w;
        }

        /* access modifiers changed from: private */
        public void g(String str) {
            this.w = str;
        }

        public List<String> w() {
            return this.x;
        }

        /* access modifiers changed from: private */
        public void e(List<String> list) {
            this.x = list;
        }

        public se x() {
            return this.y;
        }

        /* access modifiers changed from: private */
        public void a(se seVar) {
            this.y = seVar;
        }

        /* access modifiers changed from: private */
        public void a(@NonNull rs rsVar) {
            this.z = rsVar;
        }

        /* access modifiers changed from: private */
        public void a(@NonNull mh mhVar) {
            this.s = mhVar;
        }

        /* access modifiers changed from: private */
        public void a(@NonNull mc mcVar) {
            this.t = mcVar;
        }

        /* access modifiers changed from: private */
        public void i(boolean z2) {
            this.f = z2;
        }

        /* access modifiers changed from: private */
        public void j(boolean z2) {
            this.g = z2;
        }
    }

    private String a(JSONObject jSONObject, String str) {
        try {
            return jSONObject.getJSONObject(str).getJSONArray(Constants.VIDEO_TRACKING_URLS_KEY).getString(0);
        } catch (Throwable th) {
            return "";
        }
    }

    private List<String> b(JSONObject jSONObject, String str) {
        try {
            return th.a(jSONObject.getJSONObject(str).getJSONArray(Constants.VIDEO_TRACKING_URLS_KEY));
        } catch (Throwable th) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.cb.b(com.yandex.metrica.impl.ob.cb$b, org.json.JSONObject):void
     arg types: [com.yandex.metrica.impl.ob.cb$b, com.yandex.metrica.impl.ob.th$a]
     candidates:
      com.yandex.metrica.impl.ob.cb.b(org.json.JSONObject, java.lang.String):java.util.List<java.lang.String>
      com.yandex.metrica.impl.ob.cb.b(com.yandex.metrica.impl.ob.cb$b, com.yandex.metrica.impl.ob.th$a):void
      com.yandex.metrica.impl.ob.cb.b(com.yandex.metrica.impl.ob.cb$b, org.json.JSONObject):void */
    public b a(byte[] bArr) {
        b bVar = new b();
        try {
            th.a aVar = new th.a(new String(bArr, "UTF-8"));
            d(bVar, aVar);
            g(bVar, aVar);
            k(bVar, aVar);
            l(bVar, aVar);
            f(bVar, aVar);
            a(bVar, aVar);
            b(bVar, aVar);
            c(bVar, aVar);
            if (bVar.a().c) {
                i(bVar, aVar);
            }
            if (bVar.m()) {
                j(bVar, aVar);
            }
            bVar.a(a(aVar.optJSONObject("foreground_location_collection"), bVar.d, bVar.f));
            bVar.a(c(aVar.optJSONObject("background_location_collection"), bVar.e, bVar.g));
            b(bVar, (JSONObject) aVar);
            e(bVar, aVar);
            h(bVar, aVar);
            bVar.a(b.a.OK);
            return bVar;
        } catch (Throwable th) {
            b bVar2 = new b();
            bVar2.a(b.a.BAD);
            return bVar2;
        }
    }

    private void d(@NonNull b bVar, @NonNull th.a aVar) {
        String str;
        JSONObject optJSONObject = aVar.optJSONObject(UrlManager.Parameter.DEVICE_ID);
        String str2 = "";
        if (optJSONObject != null) {
            str2 = optJSONObject.optString("hash");
            str = optJSONObject.optString("value");
        } else {
            str = str2;
        }
        bVar.d(str);
        bVar.e(str2);
    }

    private static mh a(@Nullable JSONObject jSONObject, boolean z, boolean z2) {
        return new la().a(b(jSONObject, z, z2));
    }

    private static pp.a.c b(@Nullable JSONObject jSONObject, boolean z, boolean z2) {
        pp.a.c cVar = new pp.a.c();
        cVar.b = ua.a(th.a(jSONObject, "min_update_interval_seconds"), TimeUnit.SECONDS, cVar.b);
        cVar.c = ua.a(th.d(jSONObject, "min_update_distance_meters"), cVar.c);
        cVar.d = ua.a(th.b(jSONObject, "records_count_to_force_flush"), cVar.d);
        cVar.e = ua.a(th.b(jSONObject, "max_records_count_in_batch"), cVar.e);
        cVar.f = ua.a(th.a(jSONObject, "max_age_seconds_to_force_flush"), TimeUnit.SECONDS, cVar.f);
        cVar.g = ua.a(th.b(jSONObject, "max_records_to_store_locally"), cVar.g);
        cVar.h = z;
        cVar.j = ua.a(th.a(jSONObject, "lbs_min_update_interval_seconds"), TimeUnit.SECONDS, cVar.j);
        cVar.i = z2;
        return cVar;
    }

    private static mc c(@Nullable JSONObject jSONObject, boolean z, boolean z2) {
        JSONArray jSONArray;
        pp.a.C0014a aVar = new pp.a.C0014a();
        aVar.b = b(jSONObject, z, z2);
        aVar.c = ua.a(th.a(jSONObject, "collection_duration_seconds"), TimeUnit.SECONDS, aVar.c);
        aVar.d = ua.a(th.a(jSONObject, "collection_interval_seconds"), TimeUnit.SECONDS, aVar.d);
        aVar.e = th.a(jSONObject, "aggressive_relaunch", aVar.e);
        if (jSONObject == null) {
            jSONArray = null;
        } else {
            jSONArray = jSONObject.optJSONArray("collection_interval_ranges_seconds");
        }
        aVar.f = a(jSONArray, aVar.f);
        return new kw().a(aVar);
    }

    private static pp.a.C0014a.C0015a[] a(@Nullable JSONArray jSONArray, pp.a.C0014a.C0015a[] aVarArr) {
        if (jSONArray != null && jSONArray.length() > 0) {
            try {
                aVarArr = new pp.a.C0014a.C0015a[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); i++) {
                    aVarArr[i] = new pp.a.C0014a.C0015a();
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    aVarArr[i].b = TimeUnit.SECONDS.toMillis(jSONObject.getLong("min"));
                    aVarArr[i].c = TimeUnit.SECONDS.toMillis(jSONObject.getLong("max"));
                }
            } catch (Throwable th) {
            }
        }
        return aVarArr;
    }

    private static void e(@NonNull b bVar, @NonNull th.a aVar) throws JSONException {
        if (aVar.has("requests")) {
            JSONObject jSONObject = aVar.getJSONObject("requests");
            if (jSONObject.has("list")) {
                JSONArray jSONArray = jSONObject.getJSONArray("list");
                ArrayList arrayList = new ArrayList(jSONArray.length());
                for (int i = 0; i < jSONArray.length(); i++) {
                    try {
                        arrayList.add(a(jSONArray.getJSONObject(i)));
                    } catch (JSONException e) {
                    }
                }
                if (!arrayList.isEmpty()) {
                    bVar.d(arrayList);
                }
            }
        }
    }

    @NonNull
    private static a a(@NonNull JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject("headers");
        ArrayList arrayList = new ArrayList(jSONObject2.length());
        Iterator<String> keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            JSONArray jSONArray = jSONObject2.getJSONArray(next);
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(new Pair(next, jSONArray.getString(i)));
            }
        }
        return new a(jSONObject.optString("id", null), jSONObject.optString(ImagesContract.URL, null), jSONObject.optString("method", null), arrayList, Long.valueOf(jSONObject.getLong("delay_seconds")), b(jSONObject));
    }

    @NonNull
    private static List<a.C0002a> b(@NonNull JSONObject jSONObject) throws JSONException {
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has("accept_network_types")) {
            JSONArray jSONArray = jSONObject.getJSONArray("accept_network_types");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(a.get(jSONArray.getString(i)));
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.cb.a(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.cb.a(org.json.JSONObject, boolean, boolean):com.yandex.metrica.impl.ob.mh
      com.yandex.metrica.impl.ob.cb.a(com.yandex.metrica.impl.ob.cb$b, com.yandex.metrica.impl.ob.th$a, com.yandex.metrica.impl.ob.lj):void
      com.yandex.metrica.impl.ob.cb.a(org.json.JSONObject, java.lang.String, boolean):boolean */
    private static void f(b bVar, th.a aVar) throws JSONException {
        pp.a.b bVar2 = new pp.a.b();
        pp.a.c cVar = new pp.a.c();
        JSONObject jSONObject = (JSONObject) aVar.a("features", new JSONObject());
        if (jSONObject.has("list")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("list");
            bVar.a(a(jSONObject2, "easy_collecting", bVar2.b));
            bVar.b(a(jSONObject2, "package_info", bVar2.c));
            bVar.e(a(jSONObject2, "socket", false));
            bVar.f(a(jSONObject2, "permissions_collecting", bVar2.d));
            bVar.g(a(jSONObject2, "features_collecting", bVar2.e));
            bVar.h(a(jSONObject2, "sdk_list", bVar2.f));
            bVar.c(a(jSONObject2, "foreground_location_collection", cVar.h));
            bVar.i(a(jSONObject2, "foreground_lbs_collection", cVar.i));
            bVar.d(a(jSONObject2, "background_location_collection", cVar.h));
            bVar.j(a(jSONObject2, "background_lbs_collection", cVar.i));
            bVar.b().f(a(jSONObject2, "android_id", bVar2.g)).g(a(jSONObject2, "google_aid", bVar2.h)).h(a(jSONObject2, "wifi_around", bVar2.i)).i(a(jSONObject2, "wifi_connected", bVar2.j)).j(a(jSONObject2, "own_macs", bVar2.k)).k(a(jSONObject2, "cells_around", bVar2.l)).l(a(jSONObject2, "sim_info", bVar2.m)).m(a(jSONObject2, "sim_imei", bVar2.n)).n(a(jSONObject2, "access_point", bVar2.o));
        }
    }

    @VisibleForTesting
    static void a(@NonNull b bVar, @NonNull th.a aVar) {
        JSONObject optJSONObject;
        JSONObject optJSONObject2 = aVar.optJSONObject("locale");
        String str = "";
        if (!(optJSONObject2 == null || (optJSONObject = optJSONObject2.optJSONObject("country")) == null || !optJSONObject.optBoolean("reliable", false))) {
            str = optJSONObject.optString("value", str);
        }
        bVar.g(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.cb.b.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, com.yandex.metrica.impl.ob.cb$b$a):void
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, com.yandex.metrica.impl.ob.mc):void
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, com.yandex.metrica.impl.ob.mh):void
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, com.yandex.metrica.impl.ob.rr):void
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, com.yandex.metrica.impl.ob.rs):void
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, com.yandex.metrica.impl.ob.rt):void
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, com.yandex.metrica.impl.ob.se):void
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, java.lang.Long):void
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, java.lang.String):void
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, java.util.List):void
      com.yandex.metrica.impl.ob.cb.b.a(com.yandex.metrica.impl.ob.cb$b, boolean):void
      com.yandex.metrica.impl.ob.cb.b.a(java.lang.String, boolean):void */
    @VisibleForTesting
    static void b(@NonNull b bVar, @NonNull th.a aVar) {
        JSONArray optJSONArray;
        JSONObject optJSONObject = aVar.optJSONObject("permissions");
        if (optJSONObject != null && (optJSONArray = optJSONObject.optJSONArray("list")) != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                if (optJSONObject2 != null) {
                    String optString = optJSONObject2.optString("name");
                    boolean optBoolean = optJSONObject2.optBoolean("enabled");
                    if (TextUtils.isEmpty(optString)) {
                        bVar.a("", false);
                    } else {
                        bVar.a(optString, optBoolean);
                    }
                }
            }
        }
    }

    static void c(@NonNull b bVar, @NonNull th.a aVar) {
        a(bVar, aVar, new lj());
    }

    @VisibleForTesting
    static void a(@NonNull b bVar, @NonNull th.a aVar, @NonNull lj ljVar) {
        bVar.a(ljVar.a(a(aVar)));
    }

    static pp.a.g a(@NonNull th.a aVar) {
        pp.a.g gVar = new pp.a.g();
        JSONObject optJSONObject = aVar.optJSONObject("sdk_list");
        if (optJSONObject == null) {
            return gVar;
        }
        gVar.b = ua.a(th.a(optJSONObject, "min_collecting_interval_seconds"), TimeUnit.SECONDS, gVar.b);
        gVar.c = ua.a(th.a(optJSONObject, "min_first_collecting_delay_seconds"), TimeUnit.SECONDS, gVar.c);
        gVar.d = ua.a(th.a(optJSONObject, "min_collecting_delay_after_launch_seconds"), TimeUnit.SECONDS, gVar.d);
        gVar.e = ua.a(th.a(optJSONObject, "min_request_retry_interval_seconds"), TimeUnit.SECONDS, gVar.e);
        return gVar;
    }

    private static void g(@NonNull b bVar, @NonNull th.a aVar) {
        JSONObject optJSONObject;
        JSONObject optJSONObject2;
        JSONObject optJSONObject3 = aVar.optJSONObject("queries");
        if (optJSONObject3 != null && (optJSONObject = optJSONObject3.optJSONObject("list")) != null && (optJSONObject2 = optJSONObject.optJSONObject("sdk_list")) != null) {
            bVar.a(optJSONObject2.optString(ImagesContract.URL, null));
        }
    }

    private static void h(@NonNull b bVar, @NonNull th.a aVar) {
        pp.a.i iVar = new pp.a.i();
        JSONObject optJSONObject = aVar.optJSONObject("stat_sending");
        if (optJSONObject != null) {
            iVar.b = ua.a(th.a(optJSONObject, "disabled_reporting_interval_seconds"), TimeUnit.SECONDS, iVar.b);
        }
        bVar.a(new ln().a(iVar));
    }

    private static boolean a(JSONObject jSONObject, String str, boolean z) throws JSONException {
        if (jSONObject.has(str)) {
            return jSONObject.getJSONObject(str).optBoolean("enabled", z);
        }
        return false;
    }

    private void i(b bVar, th.a aVar) {
        long j;
        long j2;
        JSONObject optJSONObject = aVar.optJSONObject("permissions_collecting");
        pp.a.e eVar = new pp.a.e();
        if (optJSONObject != null) {
            j2 = optJSONObject.optLong("check_interval_seconds", eVar.b);
            j = optJSONObject.optLong("force_send_interval_seconds", eVar.c);
        } else {
            j2 = eVar.b;
            j = eVar.c;
        }
        bVar.a(new rr(j2, j));
    }

    private void j(b bVar, th.a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject("socket");
        if (optJSONObject != null) {
            long optLong = optJSONObject.optLong("seconds_to_live");
            long optLong2 = optJSONObject.optLong("first_delay_seconds", new pp.a.h().e);
            int optInt = optJSONObject.optInt("launch_delay_seconds", new pp.a.h().f);
            String optString = optJSONObject.optString("token");
            JSONArray optJSONArray = optJSONObject.optJSONArray("ports");
            if (optLong > 0 && a(optString) && optJSONArray != null && optJSONArray.length() > 0) {
                ArrayList arrayList = new ArrayList(optJSONArray.length());
                for (int i = 0; i < optJSONArray.length(); i++) {
                    int optInt2 = optJSONArray.optInt(i);
                    if (optInt2 != 0) {
                        arrayList.add(Integer.valueOf(optInt2));
                    }
                }
                if (!arrayList.isEmpty()) {
                    bVar.a(new rt(optLong, optString, arrayList, optLong2, optInt));
                }
            }
        }
    }

    private void k(b bVar, th.a aVar) throws JSONException {
        JSONObject jSONObject = (JSONObject) aVar.a("query_hosts", new JSONObject());
        if (jSONObject.has("list")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("list");
            String a2 = a(jSONObject2, "get_ad");
            if (a(a2)) {
                bVar.b(a2);
            }
            List<String> b2 = b(jSONObject2, "report");
            if (a(b2)) {
                bVar.b(b2);
            }
            String a3 = a(jSONObject2, "report_ad");
            if (a(a3)) {
                bVar.c(a3);
            }
            List<String> b3 = b(jSONObject2, "location");
            if (a(b3)) {
                bVar.c(b3);
            }
            List<String> b4 = b(jSONObject2, "startup");
            if (a(b4)) {
                bVar.a(b4);
            }
            List<String> b5 = b(jSONObject2, "diagnostic");
            if (a(b5)) {
                bVar.e(b5);
            }
        }
    }

    private boolean a(String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean a(List<String> list) {
        return !cg.a((Collection) list);
    }

    private void l(b bVar, th.a aVar) throws JSONException {
        JSONObject optJSONObject = ((JSONObject) aVar.a("distribution_customization", new JSONObject())).optJSONObject("clids");
        if (optJSONObject != null) {
            a(bVar, optJSONObject);
        }
    }

    private void a(b bVar, JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            JSONObject optJSONObject = jSONObject.optJSONObject(next);
            if (optJSONObject != null && optJSONObject.has("value")) {
                hashMap.put(next, optJSONObject.getString("value"));
            }
        }
        bVar.f(tu.a(hashMap));
    }

    private void b(b bVar, JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject("time");
        if (optJSONObject != null) {
            try {
                bVar.a(Long.valueOf(optJSONObject.getLong("max_valid_difference_seconds")));
            } catch (JSONException e) {
            }
        }
    }

    public static Long a(@Nullable Map<String, List<String>> map) {
        if (!cg.a((Map) map)) {
            List list = map.get("Date");
            if (!cg.a((Collection) list)) {
                try {
                    return Long.valueOf(new SimpleDateFormat("E, d MMM yyyy HH:mm:ss z", Locale.US).parse((String) list.get(0)).getTime());
                } catch (Throwable th) {
                }
            }
        }
        return null;
    }

    public static class a {
        @Nullable
        public final String a;
        @Nullable
        public final String b;
        @Nullable
        public final String c;
        @NonNull
        public final List<Pair<String, String>> d;
        @Nullable
        public final Long e;
        @NonNull
        public final List<C0002a> f;

        /* renamed from: com.yandex.metrica.impl.ob.cb$a$a  reason: collision with other inner class name */
        public enum C0002a {
            WIFI,
            CELL
        }

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3, @NonNull List<Pair<String, String>> list, @Nullable Long l, @NonNull List<C0002a> list2) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = Collections.unmodifiableList(list);
            this.e = l;
            this.f = list2;
        }
    }
}
