package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pk;

public abstract class ov implements ol, pe {
    @NonNull
    private final String a;
    private final int b;
    @NonNull
    private final vx<String> c;
    @NonNull
    private final on d;
    @NonNull
    private tp e = ti.a();

    ov(int i, @NonNull String str, @NonNull vx<String> vxVar, @NonNull on onVar) {
        this.b = i;
        this.a = str;
        this.c = vxVar;
        this.d = onVar;
    }

    @NonNull
    public String c() {
        return this.a;
    }

    public int d() {
        return this.b;
    }

    @NonNull
    public on e() {
        return this.d;
    }

    @NonNull
    public final pk.a.C0009a a() {
        pk.a.C0009a aVar = new pk.a.C0009a();
        aVar.c = d();
        aVar.b = c().getBytes();
        aVar.e = new pk.a.c();
        aVar.d = new pk.a.b();
        return aVar;
    }

    public void a(@NonNull tp tpVar) {
        this.e = tpVar;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        vv a2 = this.c.a(c());
        if (a2.a()) {
            return true;
        }
        if (!this.e.c()) {
            return false;
        }
        tp tpVar = this.e;
        tpVar.b("Attribute " + c() + " of type " + pc.a(d()) + " is skipped because " + a2.b());
        return false;
    }
}
