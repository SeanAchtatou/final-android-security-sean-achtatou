package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.util.SparseArray;
import com.yandex.metrica.YandexMetrica;

public abstract class ba {

    interface a {
        void a(Context context);
    }

    /* access modifiers changed from: protected */
    public abstract int a(of ofVar);

    /* access modifiers changed from: package-private */
    public abstract SparseArray<a> a();

    /* access modifiers changed from: protected */
    public abstract void a(of ofVar, int i);

    public void a(Context context) {
        of ofVar = new of(context);
        int a2 = a(ofVar);
        int b = b();
        if (a2 < b) {
            if (a2 > 0) {
                a(context, a2, b);
            }
            a(ofVar, b);
            ofVar.j();
        }
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return YandexMetrica.getLibraryApiLevel();
    }

    private void a(Context context, int i, int i2) {
        SparseArray<a> a2 = a();
        while (true) {
            i++;
            if (i <= i2) {
                a aVar = a2.get(i);
                if (aVar != null) {
                    aVar.a(context);
                }
            } else {
                return;
            }
        }
    }
}
