package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;

public class cc extends bi<qw> {
    private final sd j;
    private boolean k;
    private rw l;
    @NonNull
    private final qq m;

    public cc(sd sdVar, qq qqVar) {
        this(sdVar, qqVar, new qw(new qo()));
    }

    public cc(sd sdVar, qq qqVar, @NonNull qw qwVar) {
        super(new ca(sdVar, qqVar), qwVar);
        this.k = false;
        this.j = sdVar;
        this.m = qqVar;
        a(this.m.a());
    }

    public boolean a() {
        if (this.h >= 0) {
            return false;
        }
        b(false);
        a("Accept-Encoding", "encrypted");
        return this.j.c();
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Uri.Builder builder) {
        ((qw) this.i).a(builder, this.m);
    }

    public boolean b() {
        if (C()) {
            return true;
        }
        if (200 != this.e) {
            return false;
        }
        boolean b = super.b();
        if (b) {
            return b;
        }
        this.l = rw.PARSE;
        return b;
    }

    public void a(Throwable th) {
        this.l = rw.NETWORK;
    }

    public void g() {
        super.g();
        this.l = rw.NETWORK;
    }

    public void f() {
        if (!x() && y()) {
            if (this.l == null) {
                this.l = rw.UNKNOWN;
            }
            this.j.a(this.l);
        }
    }

    public synchronized void b(boolean z) {
        this.k = z;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean C() {
        return this.k;
    }

    public boolean o() {
        return true;
    }

    @NonNull
    public String n() {
        return "Startup task for component: " + this.j.b().toString();
    }
}
