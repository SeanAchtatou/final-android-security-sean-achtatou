package com.yandex.metrica.impl.ob;

import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class ut {
    @NonNull
    private final us a;
    @Nullable
    private volatile uw b;
    @Nullable
    private volatile uv c;
    @Nullable
    private volatile uv d;
    @Nullable
    private volatile Handler e;

    public ut() {
        this(new us());
    }

    @NonNull
    public uv a() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = this.a.b();
                }
            }
        }
        return this.c;
    }

    @NonNull
    public uw b() {
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    this.b = this.a.d();
                }
            }
        }
        return this.b;
    }

    @NonNull
    public uv c() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    this.d = this.a.c();
                }
            }
        }
        return this.d;
    }

    @NonNull
    public Handler d() {
        if (this.e == null) {
            synchronized (this) {
                if (this.e == null) {
                    this.e = this.a.a();
                }
            }
        }
        return this.e;
    }

    @VisibleForTesting
    ut(@NonNull us usVar) {
        this.a = usVar;
    }
}
