package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.AppMetricaDeviceIDListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.c;
import com.yandex.metrica.e;
import com.yandex.metrica.g;
import com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter;
import com.yandex.metrica.impl.ob.iv;
import com.yandex.metrica.impl.ob.tm;
import com.yandex.metrica.impl.ob.u;
import java.lang.Thread;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class cj implements u.a {
    @SuppressLint({"StaticFieldLeak"})
    private static cj a;
    private static x b = new x();
    private static volatile boolean c;
    /* access modifiers changed from: private */
    public static final EnumMap<IIdentifierCallback.Reason, AppMetricaDeviceIDListener.Reason> d = new EnumMap<>(IIdentifierCallback.Reason.class);
    private static ut e = new ut();
    private final Context f;
    private final bq g;
    /* access modifiers changed from: private */
    public ap h;
    private az i;
    private final rx j;
    private final bn k;
    private iv l;
    private final da m;
    /* access modifiers changed from: private */
    public IIdentifierCallback n;
    private bt o;
    @NonNull
    private q p = new q();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(com.yandex.metrica.IIdentifierCallback$Reason, com.yandex.metrica.AppMetricaDeviceIDListener$Reason):V}
     arg types: [com.yandex.metrica.IIdentifierCallback$Reason, com.yandex.metrica.AppMetricaDeviceIDListener$Reason]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(com.yandex.metrica.IIdentifierCallback$Reason, com.yandex.metrica.AppMetricaDeviceIDListener$Reason):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(com.yandex.metrica.IIdentifierCallback$Reason, com.yandex.metrica.AppMetricaDeviceIDListener$Reason):V} */
    static {
        d.put(IIdentifierCallback.Reason.UNKNOWN, AppMetricaDeviceIDListener.Reason.UNKNOWN);
        d.put(IIdentifierCallback.Reason.INVALID_RESPONSE, AppMetricaDeviceIDListener.Reason.INVALID_RESPONSE);
        d.put(IIdentifierCallback.Reason.NETWORK, AppMetricaDeviceIDListener.Reason.NETWORK);
    }

    private cj(Context context, @NonNull String str) {
        this.f = context.getApplicationContext();
        jq f2 = jo.a(this.f).f();
        tp.a(context.getApplicationContext());
        bx.a(ti.a(str));
        GoogleAdvertisingIdGetter.b().a(this.f);
        Handler a2 = l().b().a();
        u uVar = new u(a2);
        uVar.a(this);
        this.m = new da(this.f, uVar);
        kg kgVar = new kg(f2);
        this.o = new bt(this.m, l().b(), this.f, l().b(), kgVar);
        b.a(this.o);
        new p(kgVar).a(this.f);
        this.j = new rx(this.o, kgVar, a2);
        this.o.a(this.j);
        this.k = new bn(this.o, kgVar, l().b());
        this.g = new bq(this.f, this.m, this.o, a2, this.j);
        this.p.a();
    }

    public static uv a() {
        return l().a();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.i = a(Thread.getDefaultUncaughtExceptionHandler());
        Thread.setDefaultUncaughtExceptionHandler(this.i);
    }

    public static synchronized void a(Context context, g gVar) {
        synchronized (cj.class) {
            boolean d2 = b.d();
            g a2 = b.a(gVar);
            b(context, a2);
            tp a3 = ti.a(a2.apiKey);
            tg b2 = ti.b(a2.apiKey);
            if (a.h == null) {
                a.a(gVar);
                a.m.a(gVar);
                a.a(a2, d2);
                if (Boolean.TRUE.equals(a2.logs)) {
                    a3.a();
                    b2.a();
                    ti.a().a();
                    ti.b().a();
                } else {
                    a3.b();
                    b2.b();
                    ti.a().b();
                    ti.b().b();
                }
            } else if (a3.c()) {
                a3.b("Appmetrica already has been activated!");
            }
        }
    }

    public static synchronized void a(Context context) {
        synchronized (cj.class) {
            b(context, null);
        }
    }

    public static synchronized void b(Context context, g gVar) {
        synchronized (cj.class) {
            if (a == null) {
                a = new cj(context.getApplicationContext(), gVar == null ? "" : gVar.apiKey);
                a.n();
                a.b();
            }
        }
    }

    public static void c() {
        c = true;
    }

    public static boolean d() {
        return c;
    }

    public static synchronized cj e() {
        cj cjVar;
        synchronized (cj.class) {
            cjVar = a;
        }
        return cjVar;
    }

    public static cj b(Context context) {
        a(context.getApplicationContext());
        return e();
    }

    @Nullable
    public static cj f() {
        return e();
    }

    public static synchronized ap g() {
        ap apVar;
        synchronized (cj.class) {
            apVar = e().h;
        }
        return apVar;
    }

    static synchronized boolean h() {
        boolean z;
        synchronized (cj.class) {
            z = (a == null || a.h == null) ? false : true;
        }
        return z;
    }

    private void n() {
        ak.a();
        l().b().a(new tm.a(this.f));
    }

    private void a(g gVar) {
        if (gVar != null) {
            this.j.a(gVar.d);
            this.j.a(gVar.b);
            this.j.a(gVar.c);
        }
    }

    public void a(@NonNull e eVar) {
        this.g.a(eVar);
    }

    public c b(@NonNull e eVar) {
        return this.g.b(eVar);
    }

    public void a(String str) {
        this.k.a(str);
    }

    @VisibleForTesting
    static as i() {
        return h() ? e().h : b;
    }

    private void d(boolean z) {
        if (z) {
            if (this.l == null) {
                this.l = new iw(new br(this.h), new iv.a() {
                    public boolean a(Throwable th) {
                        return cj.this.h.g();
                    }
                });
            }
            this.i.a(this.l);
        } else {
            this.i.b(this.l);
        }
        this.h.b(z);
    }

    public static void a(Location location) {
        i().a(location);
    }

    public static void a(boolean z) {
        i().a(z);
    }

    public void b(boolean z) {
        i().a(z);
    }

    public void c(boolean z) {
        i().setStatisticsSending(z);
    }

    public String j() {
        return this.j.b();
    }

    public String k() {
        return this.j.a();
    }

    public void a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        this.j.a(iIdentifierCallback, list);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting(otherwise = 2)
    public void a(g gVar, boolean z) {
        this.o.a(gVar.locationTracking, gVar.statisticsSending);
        this.h = this.g.a(gVar, z);
        this.j.c();
        d(this.h.g());
    }

    public void a(int i2, Bundle bundle) {
        if (i2 == 1) {
            this.j.a(bundle);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public az a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        az azVar = new az(uncaughtExceptionHandler);
        azVar.a(new iw(new br(this.g, "20799a27-fa80-4b36-b2db-0f8141f24180"), new iv.a() {
            public boolean a(Throwable th) {
                return bx.a(th);
            }
        }));
        azVar.a(new iw(new br(this.g, "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"), new iv.a() {
            public boolean a(Throwable th) {
                return bx.b(th);
            }
        }));
        return azVar;
    }

    public void a(DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        this.k.a(deferredDeeplinkParametersListener);
    }

    public void a(@NonNull final AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        this.n = new IIdentifierCallback() {
            public void onReceive(Map<String, String> params) {
                IIdentifierCallback unused = cj.this.n = (IIdentifierCallback) null;
                appMetricaDeviceIDListener.onLoaded(params.get(IIdentifierCallback.APP_METRICA_DEVICE_ID_HASH));
            }

            public void onRequestError(IIdentifierCallback.Reason reason) {
                IIdentifierCallback unused = cj.this.n = (IIdentifierCallback) null;
                appMetricaDeviceIDListener.onError((AppMetricaDeviceIDListener.Reason) cj.d.get(reason));
            }
        };
        this.j.a(this.n, Collections.singletonList(IIdentifierCallback.APP_METRICA_DEVICE_ID_HASH));
    }

    public static ut l() {
        return e;
    }
}
