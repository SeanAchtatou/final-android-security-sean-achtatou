package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pk;

public class pb extends oq<String> {
    private final vo<String> a;

    public pb(@NonNull String str, @NonNull String str2, @NonNull vo<String> voVar, @NonNull vx<String> vxVar, @NonNull on onVar) {
        super(0, str, str2, vxVar, onVar);
        this.a = voVar;
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull pk.a.C0009a aVar) {
        String a2 = this.a.a(b());
        aVar.e.b = a2 == null ? new byte[0] : a2.getBytes();
    }
}
