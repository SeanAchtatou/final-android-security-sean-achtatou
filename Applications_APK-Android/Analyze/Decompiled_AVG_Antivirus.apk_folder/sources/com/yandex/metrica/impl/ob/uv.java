package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public interface uv extends ux {
    <T> Future<T> a(Callable callable);

    void a(@NonNull Runnable runnable);

    void a(@NonNull Runnable runnable, long j);

    void b(@NonNull Runnable runnable);
}
