package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class fa {
    @Nullable
    private Long a;
    private int b;
    @NonNull
    private tx c;

    public static class a {
        public final long a;
        public final long b;
        public final int c;

        public a(long j, long j2, int i) {
            this.a = j;
            this.c = i;
            this.b = j2;
        }
    }

    public fa() {
        this(new tw());
    }

    public fa(@NonNull tx txVar) {
        this.c = txVar;
    }

    public a a() {
        if (this.a == null) {
            this.a = Long.valueOf(this.c.b());
        }
        a aVar = new a(this.a.longValue(), this.a.longValue(), this.b);
        this.b++;
        return aVar;
    }
}
