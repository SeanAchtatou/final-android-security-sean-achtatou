package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ab;

public class hs {
    @NonNull
    private final di a;
    @NonNull
    private final hr b;
    @NonNull
    private final a c;
    @NonNull
    private final hl<hn> d;
    @NonNull
    private final hl<hn> e;
    @Nullable
    private hm f;
    @Nullable
    private b g;

    public interface a {
        void a(@NonNull t tVar, @NonNull ht htVar);
    }

    public enum b {
        EMPTY,
        BACKGROUND,
        FOREGROUND
    }

    public hs(@NonNull di diVar, @NonNull hr hrVar, @NonNull a aVar) {
        this(diVar, hrVar, aVar, new hk(diVar, hrVar), new hj(diVar, hrVar));
    }

    @VisibleForTesting
    public hs(@NonNull di diVar, @NonNull hr hrVar, @NonNull a aVar, @NonNull hl<hn> hlVar, @NonNull hl<hn> hlVar2) {
        this.g = null;
        this.a = diVar;
        this.c = aVar;
        this.d = hlVar;
        this.e = hlVar2;
        this.b = hrVar;
    }

    /* renamed from: com.yandex.metrica.impl.ob.hs$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[b.values().length];

        static {
            try {
                a[b.FOREGROUND.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[b.BACKGROUND.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[b.EMPTY.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public void a(@NonNull t tVar) {
        f(tVar);
        int i = AnonymousClass1.a[this.g.ordinal()];
        if (i != 1) {
            if (i == 2) {
                b(this.f, tVar);
                this.f = e(tVar);
            } else if (i == 3) {
                this.f = e(tVar);
            }
        } else if (a(this.f, tVar)) {
            this.f.b(tVar.r());
        } else {
            this.f = e(tVar);
        }
    }

    public void b(@NonNull t tVar) {
        c(tVar).a(false);
        if (this.g != b.EMPTY) {
            b(this.f, tVar);
        }
        this.g = b.EMPTY;
    }

    @NonNull
    public hm c(@NonNull t tVar) {
        f(tVar);
        if (this.g != b.EMPTY && !a(this.f, tVar)) {
            this.g = b.EMPTY;
            this.f = null;
        }
        int i = AnonymousClass1.a[this.g.ordinal()];
        if (i == 1) {
            return this.f;
        }
        if (i != 2) {
            this.f = g(tVar);
            return this.f;
        }
        this.f.b(tVar.r());
        return this.f;
    }

    @NonNull
    public ht d(@NonNull t tVar) {
        return a(c(tVar), tVar.r());
    }

    @Nullable
    public hm a() {
        return this.f;
    }

    @NonNull
    public ht a(long j) {
        long a2 = this.b.a();
        this.a.i().a(a2, hw.BACKGROUND, j);
        return new ht().a(a2).a(hw.BACKGROUND).b(0).c(0);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @NonNull
    public hm e(@NonNull t tVar) {
        long r = tVar.r();
        hm a2 = this.d.a(new hn(r, tVar.s()));
        this.g = b.FOREGROUND;
        this.a.A().a();
        this.c.a(t.c(tVar), a(a2, r));
        return a2;
    }

    private void f(@NonNull t tVar) {
        if (this.g == null) {
            hm a2 = this.d.a();
            if (a(a2, tVar)) {
                this.f = a2;
                this.g = b.FOREGROUND;
                return;
            }
            hm a3 = this.e.a();
            if (a(a3, tVar)) {
                this.f = a3;
                this.g = b.BACKGROUND;
                return;
            }
            this.f = null;
            this.g = b.EMPTY;
        }
    }

    private boolean a(@Nullable hm hmVar, @NonNull t tVar) {
        if (hmVar == null) {
            return false;
        }
        if (hmVar.a(tVar.r())) {
            return true;
        }
        b(hmVar, tVar);
        return false;
    }

    private void b(@NonNull hm hmVar, @Nullable t tVar) {
        if (hmVar.g()) {
            this.c.a(t.b(tVar), a(hmVar));
            hmVar.a(false);
        }
        hmVar.e();
    }

    @NonNull
    private hm g(@NonNull t tVar) {
        this.g = b.BACKGROUND;
        long r = tVar.r();
        hm a2 = this.e.a(new hn(r, tVar.s()));
        if (this.a.t().d()) {
            this.c.a(t.c(tVar), a(a2, tVar.r()));
        } else if (tVar.g() == ab.a.EVENT_TYPE_FIRST_ACTIVATION.a()) {
            this.c.a(tVar, a(a2, r));
            this.c.a(t.c(tVar), a(a2, r));
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @NonNull
    public ht a(@NonNull hm hmVar) {
        return new ht().a(hmVar.c()).a(hmVar.a()).b(hmVar.f()).c(hmVar.d());
    }

    @NonNull
    private ht a(@NonNull hm hmVar, long j) {
        return new ht().a(hmVar.c()).b(hmVar.f()).c(hmVar.c(j)).a(hmVar.a());
    }
}
