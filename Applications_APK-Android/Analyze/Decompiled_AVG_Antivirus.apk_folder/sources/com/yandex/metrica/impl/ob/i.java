package com.yandex.metrica.impl.ob;

import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import org.json.JSONException;
import org.json.JSONObject;

public class i {
    private JSONObject a = new JSONObject();
    private long b;
    private boolean c;
    private vj d;

    public static final class a {
        public final String a;
        public final long b;

        public a(String str, long j) {
            this.a = str;
            this.b = j;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            a aVar = (a) o;
            if (this.b != aVar.b) {
                return false;
            }
            String str = this.a;
            if (str != null) {
                if (!str.equals(aVar.a)) {
                    return false;
                }
                return true;
            } else if (aVar.a == null) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            String str = this.a;
            int hashCode = str != null ? str.hashCode() : 0;
            long j = this.b;
            return (hashCode * 31) + ((int) (j ^ (j >>> 32)));
        }
    }

    public i(String str, long j, @NonNull tp tpVar) {
        this.b = j;
        try {
            this.a = new JSONObject(str);
        } catch (JSONException e) {
            this.a = new JSONObject();
            this.b = 0;
        }
        this.d = new vj(30, 50, 100, "App Environment", tpVar);
    }

    public synchronized void a() {
        this.a = new JSONObject();
        this.b = 0;
    }

    public synchronized void a(@NonNull Pair<String, String> pair) {
        a((String) pair.first, (String) pair.second);
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void a(java.lang.String r3, java.lang.String r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.yandex.metrica.impl.ob.vj r0 = r2.d     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            com.yandex.metrica.impl.ob.vm r0 = r0.a()     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            java.lang.String r3 = r0.a(r3)     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            com.yandex.metrica.impl.ob.vj r0 = r2.d     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            com.yandex.metrica.impl.ob.vm r0 = r0.b()     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            java.lang.String r4 = r0.a(r4)     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            org.json.JSONObject r0 = r2.a     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            boolean r0 = r0.has(r3)     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            if (r0 == 0) goto L_0x002f
            org.json.JSONObject r0 = r2.a     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            java.lang.String r0 = r0.getString(r3)     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            if (r4 == 0) goto L_0x002b
            boolean r1 = r4.equals(r0)     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            if (r1 != 0) goto L_0x0036
        L_0x002b:
            r2.a(r3, r4, r0)     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            goto L_0x0036
        L_0x002f:
            if (r4 == 0) goto L_0x0036
            r0 = 0
            r2.a(r3, r4, r0)     // Catch:{ JSONException -> 0x003b, all -> 0x0038 }
            goto L_0x0037
        L_0x0036:
        L_0x0037:
            goto L_0x003c
        L_0x0038:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        L_0x003b:
            r3 = move-exception
        L_0x003c:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.i.a(java.lang.String, java.lang.String):void");
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public synchronized void a(String str, @Nullable String str2, String str3) throws JSONException {
        if (this.a.length() >= this.d.c().a()) {
            if (this.d.c().a() != this.a.length() || !this.a.has(str)) {
                this.d.a(str);
            }
        }
        this.a.put(str, str2);
        this.c = true;
    }

    public synchronized a b() {
        if (this.c) {
            this.b++;
            this.c = false;
        }
        return new a(this.a.toString(), this.b);
    }

    public synchronized String toString() {
        return "Map size " + this.a.length() + ". Is changed " + this.c + ". Current revision " + this.b;
    }
}
