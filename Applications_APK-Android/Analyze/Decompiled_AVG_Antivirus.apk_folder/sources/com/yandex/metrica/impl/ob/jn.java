package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.Closeable;

public class jn extends SQLiteOpenHelper implements Closeable {
    protected final ju a;

    public jn(Context context, String str, ju juVar) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, kb.b);
        this.a = juVar;
    }

    public void onCreate(SQLiteDatabase database) {
        this.a.b(database);
    }

    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        this.a.a(database, oldVersion, newVersion);
    }

    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        this.a.a(db);
    }
}
