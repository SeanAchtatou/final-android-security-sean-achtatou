package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.io.File;
import org.json.JSONException;

public class is implements Runnable {
    @NonNull
    private final Context a;
    @NonNull
    private final File b;
    @NonNull
    private final uc<jh> c;

    public is(@NonNull Context context, @NonNull File file, @NonNull uc<jh> ucVar) {
        this.a = context;
        this.b = file;
        this.c = ucVar;
    }

    public void run() {
        if (this.b.exists()) {
            try {
                a(ag.a(this.a, this.b));
                this.b.delete();
                return;
            } catch (Throwable th) {
                return;
            }
        } else {
            return;
        }
        throw th;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                this.c.a(new jh(str));
            } catch (JSONException e) {
            }
        }
    }
}
