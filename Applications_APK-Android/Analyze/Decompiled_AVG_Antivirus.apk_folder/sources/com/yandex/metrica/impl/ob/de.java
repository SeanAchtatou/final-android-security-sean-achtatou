package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

public class de implements dl<dd> {
    @NonNull
    private final bc a;

    public de() {
        this(new bc());
    }

    public de(@NonNull bc bcVar) {
        this.a = bcVar;
    }

    /* renamed from: a */
    public dd b(@NonNull Context context, @NonNull df dfVar, @NonNull db dbVar) {
        return new dd(context, ru.a(), dfVar, dbVar, af.a().c(), this.a.a(context, dfVar));
    }
}
