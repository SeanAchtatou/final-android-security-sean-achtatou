package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.SparseArray;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ba;
import com.yandex.metrica.impl.ob.i;
import com.yandex.metrica.impl.ob.lp;
import com.yandex.metrica.impl.ob.sc;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

public class by extends ba {
    /* access modifiers changed from: private */
    public final kj a;
    /* access modifiers changed from: private */
    @NonNull
    public final Context b;

    public by(@NonNull Context context) {
        this.b = context;
        this.a = new kj(jo.a(context).c());
    }

    /* access modifiers changed from: package-private */
    public SparseArray<ba.a> a() {
        return new SparseArray<ba.a>() {
            {
                put(29, new d(by.this.b));
                put(39, new e());
                put(47, new f());
                put(60, new g());
                put(62, new h());
                put(66, new i());
                put(67, new b(lp.a.a(sc.class).a(by.this.b), new kk(jo.a(by.this.b).e(), by.this.b.getPackageName())));
                put(68, new j());
                put(72, new a(lp.a.b(nt.class).a(by.this.b), lp.a.a(sc.class).a(by.this.b), new ls()));
                put(73, new c(by.this.a, new oe(by.this.b, new dw(by.this.b.getPackageName(), null).toString())));
            }
        };
    }

    /* access modifiers changed from: protected */
    public int a(of ofVar) {
        int a2 = ofVar.a();
        if (a2 == -1) {
            return this.a.a(-1);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public void a(of ofVar, int i2) {
        this.a.b(i2).n();
        ofVar.b().j();
    }

    static class d implements ba.a {
        private oh a;
        private kk b;

        d(@NonNull Context context) {
            this.a = new oh(context);
            this.b = new kk(jo.a(context).e(), context.getPackageName());
        }

        public void a(Context context) {
            String a2 = this.a.a((String) null);
            if (!TextUtils.isEmpty(a2)) {
                this.b.b(a2).n();
                oh.a(context);
            }
        }
    }

    private static class e implements ba.a {
        private e() {
        }

        public void a(Context context) {
            oe oeVar = new oe(context, context.getPackageName());
            SharedPreferences a = ok.a(context, "_boundentrypreferences");
            String string = a.getString(oe.d.a(), null);
            long j = a.getLong(oe.e.a(), -1);
            if (string != null && j != -1) {
                oeVar.a(new i.a(string, j)).j();
                a.edit().remove(oe.d.a()).remove(oe.e.a()).apply();
            }
        }
    }

    static class g implements ba.a {
        g() {
        }

        public void a(Context context) {
            a(context, new kj(jo.a(context).c()));
        }

        private void a(Context context, kj kjVar) {
            long j = new kk(jo.a(context).e(), context.getPackageName()).a().t;
            boolean z = true;
            boolean z2 = j > 0;
            if (kjVar.c(-1) <= 0) {
                z = false;
            }
            if (z2 || z) {
                kjVar.d(false).n();
            }
        }
    }

    static class h implements ba.a {
        h() {
        }

        public void a(Context context) {
            kk kkVar = new kk(jo.a(context).e(), context.getPackageName());
            String i = kkVar.i(null);
            if (i != null) {
                kkVar.a(Collections.singletonList(i));
            }
            String j = kkVar.j(null);
            if (j != null) {
                kkVar.b(Collections.singletonList(j));
            }
        }
    }

    static class b implements ba.a {
        @NonNull
        private kp a;
        @NonNull
        private kk b;

        public b(@NonNull kp kpVar, @NonNull kk kkVar) {
            this.a = kpVar;
            this.b = kkVar;
        }

        public void a(Context context) {
            this.a.a(this.b.a());
        }
    }

    static class j implements ba.a {
        j() {
        }

        public void a(Context context) {
            kp a = lp.a.a(sc.class).a(context);
            sc scVar = (sc) a.a();
            a.a(scVar.a().a(scVar.t > 0).c(true).a());
        }
    }

    static class c implements ba.a {
        @NonNull
        private final kj a;
        @NonNull
        private final oe b;

        public c(@NonNull kj kjVar, @NonNull oe oeVar) {
            this.a = kjVar;
            this.b = oeVar;
        }

        public void a(Context context) {
            Boolean b2 = this.b.b();
            this.b.d().j();
            if (b2 != null) {
                this.a.a(b2.booleanValue()).n();
            }
        }
    }

    static class a implements ba.a {
        @NonNull
        private final kp<Collection<nt>> a;
        @NonNull
        private final kp<sc> b;
        @NonNull
        private final ls c;

        public a(@NonNull kp<Collection<nt>> kpVar, @NonNull kp<sc> kpVar2, @NonNull ls lsVar) {
            this.a = kpVar;
            this.b = kpVar2;
            this.c = lsVar;
        }

        public void a(@NonNull Context context) {
            c(context);
            sc.a a2 = this.b.a().a();
            a(context, a2);
            a(a2);
            this.b.a(a2.a());
            b(context);
        }

        private void b(@NonNull Context context) {
            context.getSharedPreferences("com.yandex.metrica.configuration", 0).edit().clear().apply();
        }

        private void a(@NonNull sc.a aVar) {
            aVar.c(true);
        }

        private void a(@NonNull Context context, @NonNull sc.a aVar) {
            lq a2 = this.c.a(context);
            if (a2 != null) {
                aVar.b(a2.a).d(a2.b);
            }
        }

        private void c(@NonNull Context context) {
            jt d = jo.a(context).d();
            List<nt> a2 = d.a();
            if (a2 != null) {
                this.a.a(a2);
                d.b();
            }
        }
    }

    static class f implements ba.a {
        f() {
        }

        public void a(Context context) {
            kj kjVar = new kj(jo.a(context).c());
            c(context, kjVar);
            b(context, kjVar);
            a(context, kjVar);
            kjVar.n();
            nz nzVar = new nz(context);
            nzVar.a();
            nzVar.b();
            b(context);
        }

        private void b(Context context) {
            new ls().a(context, new lq(ua.b(new kk(jo.a(context).e(), context.getPackageName()).a().b, ""), null), new nq(new nm()));
        }

        private void a(Context context, kj kjVar) {
            oe oeVar = new oe(context, new dw(context.getPackageName(), null).toString());
            Boolean b = oeVar.b();
            oeVar.d();
            if (b != null) {
                kjVar.a(b.booleanValue());
            }
            String b2 = oeVar.b((String) null);
            if (!TextUtils.isEmpty(b2)) {
                kjVar.a(b2);
            }
            oeVar.d().c().j();
        }

        private void b(Context context, kj kjVar) {
            og ogVar = new og(context, context.getPackageName());
            long a = ogVar.a(0);
            if (a != 0) {
                kjVar.a(a);
            }
            ogVar.a();
        }

        private void c(Context context, kj kjVar) {
            oi oiVar = new oi(context);
            if (oiVar.a()) {
                kjVar.b(true);
                oiVar.b();
            }
        }
    }

    static class i implements ba.a {
        i() {
        }

        public void a(Context context) {
            b(context);
            d(context);
        }

        private void d(Context context) {
            new kk(jo.a(context).e(), context.getPackageName()).p(new oj("LAST_STARTUP_CLIDS_SAVE_TIME").b()).n();
        }

        /* access modifiers changed from: package-private */
        public void b(@NonNull Context context) {
            for (File file : c(context).listFiles(new a(Arrays.asList(new b(new d(context.getPackageName())), new b(new c()))))) {
                try {
                    if (!file.delete()) {
                        rh.a(context).reportEvent("Can not delete file", new JSONObject().put("fileName", file.getName()).toString());
                    }
                } catch (Throwable th) {
                    rh.a(context).reportError("Can not delete file", th);
                }
            }
        }

        /* access modifiers changed from: package-private */
        @VisibleForTesting
        public File c(@NonNull Context context) {
            if (cg.a(21)) {
                return context.getNoBackupFilesDir();
            }
            return new File(context.getFilesDir().getParentFile(), "databases");
        }

        @NonNull
        static String a(@NonNull String str) {
            if (str.endsWith("-journal")) {
                return str.replace("-journal", "");
            }
            return str;
        }

        static class a implements FilenameFilter {
            final Iterable<FilenameFilter> a;

            a(Iterable<FilenameFilter> iterable) {
                this.a = iterable;
            }

            public boolean accept(File dir, String name) {
                for (FilenameFilter accept : this.a) {
                    if (accept.accept(dir, name)) {
                        return true;
                    }
                }
                return false;
            }
        }

        static class b implements FilenameFilter {
            private final FilenameFilter a;

            b(FilenameFilter filenameFilter) {
                this.a = filenameFilter;
            }

            public boolean accept(File dir, String name) {
                if (!name.startsWith("db_metrica_")) {
                    return false;
                }
                try {
                    return this.a.accept(dir, i.a(name));
                } catch (Throwable th) {
                    return false;
                }
            }
        }

        static class d implements FilenameFilter {
            private final String a;

            d(@NonNull String str) {
                this.a = str;
            }

            public boolean accept(File dir, String name) {
                return !name.contains(this.a);
            }
        }

        static class c implements FilenameFilter {
            c() {
            }

            public boolean accept(File dir, String name) {
                return name.endsWith("null");
            }
        }
    }
}
