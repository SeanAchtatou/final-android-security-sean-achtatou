package com.yandex.metrica.impl.ob;

import android.util.SparseArray;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.ju;
import com.yandex.metrica.impl.ob.kb;
import java.util.HashMap;

public class jl {
    @NonNull
    private final jm a;
    @NonNull
    private final jp b;
    @NonNull
    private final ju.a c;

    public jl(@NonNull jm jmVar, @NonNull jp jpVar) {
        this(jmVar, jpVar, new ju.a());
    }

    public jl(@NonNull jm jmVar, @NonNull jp jpVar, @NonNull ju.a aVar) {
        this.a = jmVar;
        this.b = jpVar;
        this.c = aVar;
    }

    public ju a() {
        return this.c.a("main", this.a.c(), this.a.d(), this.a.a(), new jw("main", this.b.a()));
    }

    public ju b() {
        HashMap hashMap = new HashMap();
        hashMap.put("preferences", kb.d.a);
        hashMap.put("binary_data", kb.b.a);
        hashMap.put("startup", kb.h.a);
        hashMap.put("l_dat", kb.a.a);
        hashMap.put("lbs_dat", kb.a.a);
        return this.c.a("metrica.db", this.a.g(), this.a.h(), this.a.b(), new jw("metrica.db", hashMap));
    }

    public ju c() {
        HashMap hashMap = new HashMap();
        hashMap.put("preferences", kb.d.a);
        return this.c.a("client storage", this.a.e(), this.a.f(), new SparseArray(), new jw("metrica.db", hashMap));
    }
}
