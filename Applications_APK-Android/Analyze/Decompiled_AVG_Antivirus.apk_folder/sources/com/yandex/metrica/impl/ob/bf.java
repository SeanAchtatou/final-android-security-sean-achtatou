package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.qu;
import java.io.IOException;

public abstract class bf<T extends qu> extends bi<T> {
    @NonNull
    private final up j;
    @NonNull
    private final td k;
    @NonNull
    private final tx l;

    /* access modifiers changed from: protected */
    public abstract void C();

    public bf(@NonNull T t) {
        this(new y(), new ui(), new td(), new tw(), t);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public bf(@androidx.annotation.NonNull com.yandex.metrica.impl.ob.bd r1, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.up r2, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.td r3, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.tx r4, @androidx.annotation.NonNull T r5) {
        /*
            r0 = this;
            r0.<init>(r1, r5)
            r0.j = r2
            r0.k = r3
            r0.l = r4
            com.yandex.metrica.impl.ob.up r1 = r0.j
            r5.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.bf.<init>(com.yandex.metrica.impl.ob.bd, com.yandex.metrica.impl.ob.up, com.yandex.metrica.impl.ob.td, com.yandex.metrica.impl.ob.tx, com.yandex.metrica.impl.ob.qu):void");
    }

    public void d() {
        super.d();
        a(this.l.a());
    }

    public boolean b() {
        boolean b = super.b();
        if (b || p()) {
            C();
        }
        return b;
    }

    public boolean c(@NonNull byte[] bArr) {
        byte[] a;
        try {
            byte[] a2 = this.k.a(bArr);
            if (a2 == null || (a = this.j.a(a2)) == null) {
                return false;
            }
            a(a);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public void a(byte[] bArr) {
        super.a(bArr);
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        return k() == 400;
    }
}
