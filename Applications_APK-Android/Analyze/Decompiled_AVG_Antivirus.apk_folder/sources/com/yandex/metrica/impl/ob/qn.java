package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.th;

public class qn {
    @Nullable
    public final String a;
    @Nullable
    public final String b;
    @Deprecated
    @Nullable
    public final String c;
    @Nullable
    public final String d;
    @Nullable
    public final String e;
    @Nullable
    public final String f;
    @Nullable
    public final String g;
    @Nullable
    public final String h;
    @Nullable
    public final String i;
    @Nullable
    public final String j;
    @Nullable
    public final String k;
    @Nullable
    public final String l;
    @Nullable
    public final String m;
    @Nullable
    public final String n;
    @Nullable
    public final String o;

    public qn(@NonNull th.a aVar) {
        this.a = aVar.a("dId");
        this.b = aVar.a("uId");
        this.c = aVar.b("kitVer");
        this.d = aVar.a("analyticsSdkVersionName");
        this.e = aVar.a("kitBuildNumber");
        this.f = aVar.a("kitBuildType");
        this.g = aVar.a("appVer");
        this.h = aVar.optString("app_debuggable", "0");
        this.i = aVar.a("appBuild");
        this.j = aVar.a("osVer");
        this.l = aVar.a("lang");
        this.m = aVar.a("root");
        this.n = aVar.optString("app_framework", bx.a());
        int optInt = aVar.optInt("osApiLev", -1);
        String str = null;
        this.k = optInt == -1 ? null : String.valueOf(optInt);
        int optInt2 = aVar.optInt("attribution_id", 0);
        this.o = optInt2 > 0 ? String.valueOf(optInt2) : str;
    }

    public qn() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
    }
}
