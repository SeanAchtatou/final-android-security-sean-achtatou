package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ho {
    @NonNull
    private final hw a;
    @Nullable
    private final Long b;
    @Nullable
    private final Long c;
    @Nullable
    private final Integer d;
    @Nullable
    private final Long e;
    @Nullable
    private final Boolean f;
    @Nullable
    private final Long g;
    @Nullable
    private final Long h;

    private ho(a aVar) {
        this.a = aVar.b;
        this.d = aVar.e;
        this.b = aVar.c;
        this.c = aVar.d;
        this.e = aVar.f;
        this.f = aVar.g;
        this.g = aVar.h;
        this.h = aVar.a;
    }

    public static final a a(hq hqVar) {
        return new a(hqVar);
    }

    public hw a() {
        return this.a;
    }

    public long a(long j) {
        Long l = this.b;
        return l == null ? j : l.longValue();
    }

    public long b(long j) {
        Long l = this.c;
        return l == null ? j : l.longValue();
    }

    public int a(int i) {
        Integer num = this.d;
        return num == null ? i : num.intValue();
    }

    public long c(long j) {
        Long l = this.e;
        return l == null ? j : l.longValue();
    }

    public boolean a(boolean z) {
        Boolean bool = this.f;
        return bool == null ? z : bool.booleanValue();
    }

    public long d(long j) {
        Long l = this.g;
        return l == null ? j : l.longValue();
    }

    public long e(long j) {
        Long l = this.h;
        return l == null ? j : l.longValue();
    }

    static final class a {
        @Nullable
        public Long a;
        /* access modifiers changed from: private */
        @NonNull
        public hw b;
        /* access modifiers changed from: private */
        @Nullable
        public Long c;
        /* access modifiers changed from: private */
        @Nullable
        public Long d;
        /* access modifiers changed from: private */
        @Nullable
        public Integer e;
        /* access modifiers changed from: private */
        @Nullable
        public Long f;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean g;
        /* access modifiers changed from: private */
        @Nullable
        public Long h;

        private a(hq hqVar) {
            this.b = hqVar.a();
            this.e = hqVar.b();
        }

        public a a(Long l) {
            this.c = l;
            return this;
        }

        public a b(Long l) {
            this.d = l;
            return this;
        }

        public a c(Long l) {
            this.f = l;
            return this;
        }

        public a a(Boolean bool) {
            this.g = bool;
            return this;
        }

        public a d(Long l) {
            this.h = l;
            return this;
        }

        public a e(Long l) {
            this.a = l;
            return this;
        }

        public ho a() {
            return new ho(this);
        }
    }
}
