package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import org.json.JSONException;
import org.json.JSONObject;

public final class mb {
    private final String a;
    private final int b;
    private final boolean c;

    public mb(@NonNull JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.getString("name");
        this.c = jSONObject.getBoolean("required");
        this.b = jSONObject.optInt("version", -1);
    }

    public mb(String str, int i, boolean z) {
        this.a = str;
        this.b = i;
        this.c = z;
    }

    public mb(String str, boolean z) {
        this(str, -1, z);
    }

    public JSONObject a() throws JSONException {
        JSONObject put = new JSONObject().put("name", this.a).put("required", this.c);
        int i = this.b;
        if (i != -1) {
            put.put("version", i);
        }
        return put;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        mb mbVar = (mb) o;
        if (this.b != mbVar.b || this.c != mbVar.c) {
            return false;
        }
        String str = this.a;
        if (str != null) {
            return str.equals(mbVar.a);
        }
        if (mbVar.a == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.a;
        return ((((str != null ? str.hashCode() : 0) * 31) + this.b) * 31) + (this.c ? 1 : 0);
    }
}
