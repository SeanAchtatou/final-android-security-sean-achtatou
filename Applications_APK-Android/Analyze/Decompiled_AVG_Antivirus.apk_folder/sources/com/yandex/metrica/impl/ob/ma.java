package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.pm.FeatureInfo;
import androidx.annotation.NonNull;

public abstract class ma {
    /* access modifiers changed from: protected */
    public abstract mb a(FeatureInfo featureInfo);

    public mb b(@NonNull FeatureInfo featureInfo) {
        if (featureInfo.name != null) {
            return a(featureInfo);
        }
        if (featureInfo.reqGlEsVersion == 0) {
            return a(featureInfo);
        }
        return new mb("openGlFeature", featureInfo.reqGlEsVersion, c(featureInfo));
    }

    /* access modifiers changed from: package-private */
    public boolean c(FeatureInfo featureInfo) {
        return (featureInfo.flags & 1) != 0;
    }

    public static class a {
        public static ma a() {
            if (cg.a(24)) {
                return new b();
            }
            return new c();
        }
    }

    public static class b extends ma {
        @TargetApi(24)
        public mb a(@NonNull FeatureInfo featureInfo) {
            return new mb(featureInfo.name, featureInfo.version, c(featureInfo));
        }
    }

    public static class c extends ma {
        public mb a(@NonNull FeatureInfo featureInfo) {
            return new mb(featureInfo.name, c(featureInfo));
        }
    }
}
