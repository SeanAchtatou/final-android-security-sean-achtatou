package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class mh {
    public final long e;
    public final float f;
    public final int g;
    public final int h;
    public final long i;
    public final int j;
    public final boolean k;
    public final long l;
    public final boolean m;

    public enum a {
        FOREGROUND("fg"),
        BACKGROUND("bg");
        
        @NonNull
        private final String c;

        private a(String str) {
            this.c = str;
        }

        @NonNull
        public String toString() {
            return this.c;
        }

        @NonNull
        public static a a(@Nullable String str) {
            a aVar = FOREGROUND;
            for (a aVar2 : values()) {
                if (aVar2.c.equals(str)) {
                    aVar = aVar2;
                }
            }
            return aVar;
        }
    }

    public mh(long j2, float f2, int i2, int i3, long j3, int i4, boolean z, long j4, boolean z2) {
        this.e = j2;
        this.f = f2;
        this.g = i2;
        this.h = i3;
        this.i = j3;
        this.j = i4;
        this.k = z;
        this.l = j4;
        this.m = z2;
    }

    @NonNull
    public a a() {
        return a.FOREGROUND;
    }

    public String toString() {
        return "ForegroundCollectionConfig{updateTimeInterval=" + this.e + ", updateDistanceInterval=" + this.f + ", recordsCountToForceFlush=" + this.g + ", maxBatchSize=" + this.h + ", maxAgeToForceFlush=" + this.i + ", maxRecordsToStoreLocally=" + this.j + ", collectionEnabled=" + this.k + ", lbsUpdateTimeInterval=" + this.l + ", lbsCollectionEnabled=" + this.m + '}';
    }
}
