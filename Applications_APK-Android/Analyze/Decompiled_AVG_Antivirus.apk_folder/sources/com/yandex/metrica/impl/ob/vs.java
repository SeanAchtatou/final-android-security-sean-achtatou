package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class vs<T> implements vx<T> {
    @NonNull
    private final String a;

    public vs(@NonNull String str) {
        this.a = str;
    }

    public vv a(@Nullable T t) {
        if (t != null) {
            return vv.a(this);
        }
        return vv.a(this, this.a + " is null.");
    }
}
