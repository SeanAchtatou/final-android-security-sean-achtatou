package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.impl.ob.ab;
import com.yandex.metrica.impl.ob.pk;
import com.yandex.metrica.profile.UserProfile;
import com.yandex.metrica.profile.UserProfileUpdate;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public abstract class m implements ah {
    private static final Collection<Integer> f = new HashSet(Arrays.asList(14, 15));
    private static final vx<pk.a> g = new vx<pk.a>() {
        public vv a(@NonNull pk.a aVar) {
            if (cg.a((Object[]) aVar.b)) {
                return vv.a(this, "attributes list is empty");
            }
            return vv.a(this);
        }
    };
    private static final vx<Revenue> h = new wb();
    protected final Context a;
    protected final bp b;
    @NonNull
    protected tp c;
    @NonNull
    protected tg d;
    protected final bt e;
    private al i;
    private jf j = new jf();

    m(Context context, bt btVar, @NonNull bp bpVar) {
        this.a = context.getApplicationContext();
        this.e = btVar;
        this.b = bpVar;
        this.c = ti.a(this.b.h().e());
        this.b.a(new vk(vj.a("Crash Environment", this.c)));
        this.c = ti.a(this.b.h().e());
        this.d = ti.b(this.b.h().e());
        if (Boolean.TRUE.equals(this.b.h().j())) {
            this.c.a();
            this.d.a();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.e.a(this.b);
    }

    /* access modifiers changed from: package-private */
    public void a(ry ryVar) {
        this.b.b(ryVar);
    }

    /* access modifiers changed from: package-private */
    public void a(al alVar) {
        this.i = alVar;
    }

    public void b(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.b.a(str, str2);
        } else if (this.c.c()) {
            this.c.b("Invalid Error Environment (key,value) pair: (%s,%s).", str, str2);
        }
    }

    public void a(Map<String, String> map) {
        if (!cg.a((Map) map)) {
            for (Map.Entry next : map.entrySet()) {
                b((String) next.getKey(), (String) next.getValue());
            }
        }
    }

    public void b(Map<String, String> map) {
        if (!cg.a((Map) map)) {
            for (Map.Entry next : map.entrySet()) {
                c((String) next.getKey(), (String) next.getValue());
            }
        }
    }

    public void c(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.e.a(str, str2, this.b);
        } else if (this.c.c()) {
            this.c.b("Invalid App Environment (key,value) pair: (%s,%s).", str, str2);
        }
    }

    public void b() {
        this.e.b(this.b);
    }

    public void resumeSession() {
        a((String) null);
        if (this.c.c()) {
            this.c.a("Resume session");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.e.a(this);
        this.i.b();
        this.e.a(ab.b(str, this.c), this.b);
        a(this.b.d());
    }

    private void a(boolean z) {
        if (z) {
            this.e.a(ab.a(ab.a.EVENT_TYPE_PURGE_BUFFER, this.c), this.b);
        }
    }

    public void pauseSession() {
        if (this.c.c()) {
            this.c.a("Pause session");
        }
        b((String) null);
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (!this.b.a()) {
            this.e.b(this);
            this.i.a();
            this.b.c();
            this.e.a(ab.c(str, this.c), this.b);
        }
    }

    public void reportEvent(@NonNull String eventName) {
        if (this.c.c()) {
            e(eventName);
        }
        a(ab.a(eventName, this.c));
    }

    public void reportEvent(@NonNull String eventName, String jsonValue) {
        if (this.c.c()) {
            d(eventName, jsonValue);
        }
        a(ab.a(eventName, jsonValue, this.c));
    }

    public void reportEvent(@NonNull String eventName, @Nullable Map<String, Object> attributes) {
        Map b2 = cg.b(attributes);
        this.e.a(ab.a(eventName, this.c), d(), b2);
        if (this.c.c()) {
            d(eventName, b2 == null ? null : b2.toString());
        }
    }

    private void e(String str) {
        if (this.c.c()) {
            this.c.a("Event received: " + d(str));
        }
    }

    private void d(String str, String str2) {
        if (this.c.c()) {
            this.c.a("Event received: " + d(str) + ". With value: " + d(str2));
        }
    }

    private void e(@Nullable String str, @Nullable String str2) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Statbox event received ");
            sb.append(" with name: ");
            sb.append(d(str));
            sb.append(" with value: ");
            String d2 = d(str2);
            if (d2.length() > 100) {
                sb.append(d2.substring(0, 100));
                sb.append("...");
            } else {
                sb.append(d2);
            }
            this.c.a(sb.toString());
        }
    }

    public void a(int i2, String str, String str2, Map<String, String> map) {
        if (!a(i2)) {
            a(ab.a(i2, str, str2, map == null ? null : new HashMap(map), this.c));
        }
    }

    private boolean a(int i2) {
        return !f.contains(Integer.valueOf(i2)) && i2 >= 1 && i2 <= 99;
    }

    public void reportError(@NonNull String message, Throwable error) {
        a(ab.a(message, e.a(this.j.b(new iy(error, null))), this.c));
        if (this.c.c()) {
            this.c.a("Error received: %s", d(message));
        }
    }

    public void sendEventsBuffer() {
        this.e.a(ab.a(ab.a.EVENT_TYPE_PURGE_BUFFER, this.c), this.b);
    }

    public void a(@Nullable String str, @Nullable String str2) {
        a(ab.b(str, str2, this.c));
        e(str, str2);
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        boolean z = !e();
        if (z) {
            this.e.a(ab.c("", this.c), this.b);
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public bp d() {
        return this.b;
    }

    private void a(t tVar) {
        this.e.a(tVar, this.b);
    }

    public void c(@Nullable String str) {
        f(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.bt.a(java.lang.String, com.yandex.metrica.impl.ob.bp, boolean):void
     arg types: [java.lang.String, com.yandex.metrica.impl.ob.bp, int]
     candidates:
      com.yandex.metrica.impl.ob.bt.a(com.yandex.metrica.impl.ob.bt, com.yandex.metrica.impl.ob.t, com.yandex.metrica.impl.ob.bp):com.yandex.metrica.impl.ob.t
      com.yandex.metrica.impl.ob.bt.a(com.yandex.metrica.impl.ob.t, com.yandex.metrica.impl.ob.bp, java.util.Map<java.lang.String, java.lang.Object>):java.util.concurrent.Future<java.lang.Void>
      com.yandex.metrica.impl.ob.bt.a(com.yandex.metrica.IMetricaService, com.yandex.metrica.impl.ob.t, com.yandex.metrica.impl.ob.bp):void
      com.yandex.metrica.impl.ob.bt.a(java.lang.String, java.lang.String, com.yandex.metrica.impl.ob.bp):void
      com.yandex.metrica.impl.ob.ai.a(com.yandex.metrica.IMetricaService, com.yandex.metrica.impl.ob.t, com.yandex.metrica.impl.ob.bp):void
      com.yandex.metrica.impl.ob.bt.a(java.lang.String, com.yandex.metrica.impl.ob.bp, boolean):void */
    private void f(String str) {
        this.e.a(ag.b(str), this.b, false);
        if (this.c.c()) {
            this.c.a("Error received: native");
        }
    }

    public void reportUnhandledException(@NonNull Throwable exception) {
        b(new iy(exception, null));
    }

    public void a(@NonNull iy iyVar) {
        b(iyVar);
    }

    private void b(@NonNull iy iyVar) {
        this.e.a(iyVar, this.b);
        c(iyVar);
    }

    private void c(@NonNull iy iyVar) {
        if (this.c.c()) {
            this.c.a("Unhandled exception received: " + iyVar.toString());
        }
    }

    public void reportRevenue(@NonNull Revenue revenue) {
        a(revenue);
    }

    private void a(@NonNull Revenue revenue) {
        vv a2 = h.a(revenue);
        if (a2.a()) {
            this.e.a(new bv(revenue, this.c), this.b);
            b(revenue);
        } else if (this.c.c()) {
            tp tpVar = this.c;
            tpVar.b("Passed revenue is not valid. Reason: " + a2.b());
        }
    }

    private void b(@NonNull Revenue revenue) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Revenue received ");
            sb.append("for productID: ");
            sb.append(d(revenue.productID));
            sb.append(" of quantity: ");
            if (revenue.quantity != null) {
                sb.append(revenue.quantity);
            } else {
                sb.append("<null>");
            }
            sb.append(" with price: ");
            sb.append(revenue.price);
            sb.append(" ");
            sb.append(revenue.currency);
            this.c.a(sb.toString());
        }
    }

    public void reportUserProfile(@NonNull UserProfile profile) {
        a(profile);
    }

    private void a(@NonNull UserProfile userProfile) {
        pd pdVar = new pd();
        for (UserProfileUpdate<? extends pe> userProfileUpdatePatcher : userProfile.getUserProfileUpdates()) {
            pe userProfileUpdatePatcher2 = userProfileUpdatePatcher.getUserProfileUpdatePatcher();
            userProfileUpdatePatcher2.a(this.c);
            userProfileUpdatePatcher2.a(pdVar);
        }
        pk.a c2 = pdVar.c();
        vv a2 = g.a(c2);
        if (a2.a()) {
            this.e.a(c2, this.b);
            f();
        } else if (this.c.c()) {
            tp tpVar = this.c;
            tpVar.b("UserInfo wasn't sent because " + a2.b());
        }
    }

    private void f() {
        if (this.c.c()) {
            this.c.a(new StringBuilder("User profile received").toString());
        }
    }

    public void setUserProfileID(@Nullable String userProfileID) {
        this.e.a(userProfileID, this.b);
        if (this.c.c()) {
            this.c.a("Set user profile ID: " + d(userProfileID));
        }
    }

    public void setStatisticsSending(boolean value) {
        this.b.h().g(value);
    }

    public void a(@NonNull iu iuVar) {
        this.e.a(o.a(iuVar), this.b);
    }

    public boolean e() {
        return this.b.a();
    }

    /* access modifiers changed from: protected */
    @NonNull
    public String d(@Nullable String str) {
        if (str == null) {
            return "<null>";
        }
        if (str.isEmpty()) {
            return "<empty>";
        }
        return str;
    }
}
