package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import org.json.JSONException;
import org.json.JSONObject;

@Deprecated
public class lr {
    @NonNull
    private final lq a;
    private final lt b;
    private final long c;
    private final boolean d;
    private final long e;

    public lr(@NonNull JSONObject jSONObject, long j) throws JSONException {
        this.a = new lq(jSONObject.optString(UrlManager.Parameter.DEVICE_ID, null), jSONObject.optString("device_id_hash", null));
        if (jSONObject.has("device_snapshot_key")) {
            this.b = new lt(jSONObject.optString("device_snapshot_key", null));
        } else {
            this.b = null;
        }
        this.c = jSONObject.optLong("last_elections_time", -1);
        this.d = d();
        this.e = j;
    }

    public lr(@NonNull lq lqVar, @NonNull lt ltVar, long j) {
        this.a = lqVar;
        this.b = ltVar;
        this.c = j;
        this.d = d();
        this.e = -1;
    }

    public String a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(UrlManager.Parameter.DEVICE_ID, this.a.a);
        jSONObject.put("device_id_hash", this.a.b);
        lt ltVar = this.b;
        if (ltVar != null) {
            jSONObject.put("device_snapshot_key", ltVar.b());
        }
        jSONObject.put("last_elections_time", this.c);
        return jSONObject.toString();
    }

    @NonNull
    public lq b() {
        return this.a;
    }

    @Nullable
    public lt c() {
        return this.b;
    }

    private boolean d() {
        if (this.c <= -1 || System.currentTimeMillis() - this.c >= 604800000) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "Credentials{mIdentifiers=" + this.a + ", mDeviceSnapshot=" + this.b + ", mLastElectionsTime=" + this.c + ", mFresh=" + this.d + ", mLastModified=" + this.e + '}';
    }
}
