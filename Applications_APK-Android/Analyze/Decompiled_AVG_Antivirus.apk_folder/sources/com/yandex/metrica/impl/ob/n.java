package com.yandex.metrica.impl.ob;

import java.util.concurrent.TimeUnit;
import org.json.JSONArray;

public interface n {

    public static class a<T> {
        public static final long a = TimeUnit.SECONDS.toMillis(10);
        private long b;
        private long c;
        private T d;
        private boolean e;

        public a() {
            this(a);
        }

        public a(long j) {
            this.c = 0;
            this.d = null;
            this.e = true;
            this.b = j;
        }

        public T a() {
            return this.d;
        }

        public void a(JSONArray jSONArray) {
            this.d = jSONArray;
            e();
            this.e = false;
        }

        public final boolean b() {
            return this.d == null;
        }

        private void e() {
            this.c = System.currentTimeMillis();
        }

        public final boolean a(long j) {
            long currentTimeMillis = System.currentTimeMillis() - this.c;
            return currentTimeMillis > j || currentTimeMillis < 0;
        }

        public final boolean c() {
            return a(this.b);
        }

        public T d() {
            if (c()) {
                return null;
            }
            return this.d;
        }
    }
}
