package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.UnsupportedEncodingException;

public class mg {
    @NonNull
    private Context a;

    public mg(@NonNull Context context) {
        this.a = context;
    }

    @Nullable
    public String a(@NonNull ms msVar) {
        String a2 = my.a(msVar);
        if (a2 != null) {
            return a(a2);
        }
        return null;
    }

    @Nullable
    public ms a(long j, @NonNull String str) {
        String b = b(str);
        if (b == null) {
            return null;
        }
        return my.a(j, b);
    }

    @Nullable
    public String a(@NonNull mn mnVar) {
        String a2 = my.a(mnVar);
        if (a2 == null) {
            return null;
        }
        return a(a2);
    }

    @Nullable
    public mn b(long j, @NonNull String str) {
        String b = b(str);
        if (b == null) {
            return null;
        }
        return my.b(j, b);
    }

    @Nullable
    public String a(@NonNull String str) {
        try {
            return ts.a(this.a, str);
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    @Nullable
    public String b(@NonNull String str) {
        try {
            return ts.b(this.a, str);
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }
}
