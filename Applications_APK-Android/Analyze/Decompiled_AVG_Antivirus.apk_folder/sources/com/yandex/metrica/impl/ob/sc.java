package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.cb;
import java.util.Collections;
import java.util.List;

public class sc {
    @Nullable
    public final List<nt> A;
    @NonNull
    public final rs B;
    public final long C;
    public final long D;
    public final boolean E;
    @Nullable
    public final String a;
    @Nullable
    public final String b;
    @Nullable
    public final String c;
    @Nullable
    public final String d;
    @Nullable
    public final List<String> e;
    @Nullable
    public final String f;
    @Nullable
    public final String g;
    @Nullable
    public final String h;
    @Nullable
    public final List<String> i;
    @Nullable
    public final List<String> j;
    @Nullable
    public final List<String> k;
    @Nullable
    public final List<String> l;
    @Nullable
    public final String m;
    @Nullable
    public final String n;
    @NonNull
    public final rq o;
    @Nullable
    public final mh p;
    @Nullable
    public final mc q;
    @Nullable
    public final rt r;
    @Nullable
    public final String s;
    public final long t;
    public final boolean u;
    public final boolean v;
    @Nullable
    public final List<cb.a> w;
    @Nullable
    public final String x;
    @Nullable
    public final se y;
    @Nullable
    public final rr z;

    private sc(@NonNull a aVar) {
        List<String> list;
        List<String> list2;
        List<String> list3;
        this.a = aVar.a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        List<cb.a> list4 = null;
        this.e = aVar.e == null ? null : Collections.unmodifiableList(aVar.e);
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i == null ? null : Collections.unmodifiableList(aVar.i);
        if (aVar.j == null) {
            list = null;
        } else {
            list = Collections.unmodifiableList(aVar.j);
        }
        this.j = list;
        if (aVar.k == null) {
            list2 = null;
        } else {
            list2 = Collections.unmodifiableList(aVar.k);
        }
        this.k = list2;
        if (aVar.l == null) {
            list3 = null;
        } else {
            list3 = Collections.unmodifiableList(aVar.l);
        }
        this.l = list3;
        this.m = aVar.m;
        this.n = aVar.n;
        this.o = aVar.o;
        this.p = aVar.p;
        this.q = aVar.q;
        this.r = aVar.r;
        this.z = aVar.s;
        this.s = aVar.v;
        this.t = aVar.t;
        this.u = aVar.u;
        this.v = aVar.w;
        this.w = aVar.z != null ? Collections.unmodifiableList(aVar.z) : list4;
        this.x = aVar.A;
        this.A = aVar.B;
        this.B = aVar.C;
        this.y = aVar.x;
        this.C = aVar.D;
        this.D = aVar.E;
        this.E = aVar.y;
    }

    public a a() {
        return new a(this.o).a(this.a).b(this.b).c(this.c).d(this.d).c(this.j).d(this.k).h(this.m).a(this.e).b(this.i).e(this.f).f(this.g).g(this.h).e(this.l).j(this.s).a(this.p).a(this.q).a(this.r).i(this.n).b(this.v).a(this.t).a(this.u).f(this.w).k(this.x).g(this.A).a(this.z).a(this.B).b(this.C).c(this.D).a(this.y).c(this.E);
    }

    public String toString() {
        return "StartupState{uuid='" + this.a + '\'' + ", deviceID='" + this.b + '\'' + ", deviceID2='" + this.c + '\'' + ", deviceIDHash='" + this.d + '\'' + ", reportUrls=" + this.e + ", getAdUrl='" + this.f + '\'' + ", reportAdUrl='" + this.g + '\'' + ", sdkListUrl='" + this.h + '\'' + ", locationUrls=" + this.i + ", hostUrlsFromStartup=" + this.j + ", hostUrlsFromClient=" + this.k + ", diagnosticUrls=" + this.l + ", encodedClidsFromResponse='" + this.m + '\'' + ", lastStartupRequestClids='" + this.n + '\'' + ", collectingFlags=" + this.o + ", foregroundLocationCollectionConfig=" + this.p + ", backgroundLocationCollectionConfig=" + this.q + ", socketConfig=" + this.r + ", distributionReferrer='" + this.s + '\'' + ", obtainTime=" + this.t + ", hadFirstStartup=" + this.u + ", startupClidsMatchWithAppClids=" + this.v + ", requests=" + this.w + ", countryInit='" + this.x + '\'' + ", statSending=" + this.y + ", permissionsCollectingConfig=" + this.z + ", permissions=" + this.A + ", sdkFingerprintingConfig=" + this.B + ", obtainServerTime=" + this.C + ", firstStartupServerTime=" + this.D + ", outdated=" + this.E + '}';
    }

    public static class a {
        /* access modifiers changed from: private */
        @Nullable
        public String A;
        /* access modifiers changed from: private */
        @Nullable
        public List<nt> B;
        /* access modifiers changed from: private */
        @NonNull
        public rs C;
        /* access modifiers changed from: private */
        public long D;
        /* access modifiers changed from: private */
        public long E;
        @Nullable
        String a;
        @Nullable
        String b;
        @Nullable
        String c;
        @Nullable
        String d;
        @Nullable
        List<String> e;
        @Nullable
        String f;
        @Nullable
        String g;
        @Nullable
        String h;
        @Nullable
        List<String> i;
        @Nullable
        List<String> j;
        @Nullable
        List<String> k;
        @Nullable
        List<String> l;
        @Nullable
        String m;
        @Nullable
        String n;
        @NonNull
        final rq o;
        @Nullable
        mh p;
        @Nullable
        mc q;
        @Nullable
        rt r;
        @Nullable
        rr s;
        long t;
        boolean u;
        @Nullable
        String v;
        boolean w;
        @Nullable
        se x;
        boolean y;
        /* access modifiers changed from: private */
        @Nullable
        public List<cb.a> z;

        public a(@NonNull rq rqVar) {
            this.o = rqVar;
        }

        public a a(@Nullable String str) {
            this.a = str;
            return this;
        }

        public a b(@Nullable String str) {
            this.b = str;
            return this;
        }

        public a c(@Nullable String str) {
            this.c = str;
            return this;
        }

        public a d(@Nullable String str) {
            this.d = str;
            return this;
        }

        public a a(@Nullable List<String> list) {
            this.e = list;
            return this;
        }

        public a e(@Nullable String str) {
            this.f = str;
            return this;
        }

        public a f(@Nullable String str) {
            this.g = str;
            return this;
        }

        public a g(@Nullable String str) {
            this.h = str;
            return this;
        }

        public a b(@Nullable List<String> list) {
            this.i = list;
            return this;
        }

        public a c(@Nullable List<String> list) {
            this.j = list;
            return this;
        }

        public a d(@Nullable List<String> list) {
            this.k = list;
            return this;
        }

        public a e(@Nullable List<String> list) {
            this.l = list;
            return this;
        }

        public a h(@Nullable String str) {
            this.m = str;
            return this;
        }

        public a i(@Nullable String str) {
            this.n = str;
            return this;
        }

        public a a(@Nullable mh mhVar) {
            this.p = mhVar;
            return this;
        }

        public a a(@Nullable mc mcVar) {
            this.q = mcVar;
            return this;
        }

        public a a(@Nullable rt rtVar) {
            this.r = rtVar;
            return this;
        }

        public a a(@Nullable rr rrVar) {
            this.s = rrVar;
            return this;
        }

        public a j(@Nullable String str) {
            this.v = str;
            return this;
        }

        public a a(long j2) {
            this.t = j2;
            return this;
        }

        public a a(boolean z2) {
            this.u = z2;
            return this;
        }

        public a b(boolean z2) {
            this.w = z2;
            return this;
        }

        public a f(@Nullable List<cb.a> list) {
            this.z = list;
            return this;
        }

        public a k(@Nullable String str) {
            this.A = str;
            return this;
        }

        public a g(@Nullable List<nt> list) {
            this.B = list;
            return this;
        }

        public a a(@NonNull rs rsVar) {
            this.C = rsVar;
            return this;
        }

        public a b(long j2) {
            this.D = j2;
            return this;
        }

        public a c(long j2) {
            this.E = j2;
            return this;
        }

        public a a(se seVar) {
            this.x = seVar;
            return this;
        }

        public a c(boolean z2) {
            this.y = z2;
            return this;
        }

        @NonNull
        public sc a() {
            return new sc(this);
        }
    }
}
