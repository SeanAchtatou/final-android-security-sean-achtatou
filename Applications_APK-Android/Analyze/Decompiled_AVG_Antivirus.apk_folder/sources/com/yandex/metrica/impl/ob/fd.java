package com.yandex.metrica.impl.ob;

import java.util.List;

public class fd<BaseHandler> extends fe<BaseHandler> {
    private final List<BaseHandler> a;

    /* JADX WARN: Type inference failed for: r1v0, types: [java.util.List, java.util.List<BaseHandler>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public fd(java.util.List<BaseHandler> r1) {
        /*
            r0 = this;
            r0.<init>()
            java.util.List r1 = java.util.Collections.unmodifiableList(r1)
            r0.a = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.fd.<init>(java.util.List):void");
    }

    public List<? extends BaseHandler> a() {
        return this.a;
    }
}
