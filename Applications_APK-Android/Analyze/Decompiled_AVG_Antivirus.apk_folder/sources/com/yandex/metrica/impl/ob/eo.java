package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.db;

class eo implements ek, em<du> {
    eo() {
    }

    @NonNull
    public ej a(@NonNull Context context, @NonNull en enVar, @NonNull eh ehVar, @NonNull db dbVar) {
        return new ep(context, (dx) enVar.a(new dw(ehVar.c(), ehVar.b()), dbVar, new dy(this)));
    }

    @NonNull
    /* renamed from: c */
    public du a(@NonNull Context context, @NonNull df dfVar, @NonNull db.a aVar, @NonNull bb bbVar, @NonNull sd sdVar) {
        return new du(context, sdVar.e(), bbVar, dfVar, aVar, af.a().c(), af.a().h(), new sg(sdVar));
    }

    @NonNull
    /* renamed from: d */
    public eu b(@NonNull Context context, @NonNull df dfVar, @NonNull db.a aVar, @NonNull bb bbVar, @NonNull sd sdVar) {
        return new eu(context, dfVar, bbVar, aVar, sdVar.e(), new sg(sdVar));
    }
}
