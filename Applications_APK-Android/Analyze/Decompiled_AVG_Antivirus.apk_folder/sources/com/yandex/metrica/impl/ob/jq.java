package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class jq {
    public static final String a = jq.class.getSimpleName();
    /* access modifiers changed from: private */
    public final Map<String, Object> b;
    /* access modifiers changed from: private */
    public final Map<String, Object> c;
    private final String d;
    private final a e;
    /* access modifiers changed from: private */
    public volatile boolean f;
    private final jx g;

    public jq(jn jnVar, String str) {
        this(str, new ka(jnVar));
    }

    protected jq(String str, jx jxVar) {
        this.b = new HashMap();
        this.c = new HashMap();
        this.g = jxVar;
        this.d = str;
        this.e = new a(String.format(Locale.US, "YMM-DW-%s", Integer.valueOf(va.b())));
        this.e.start();
    }

    private class a extends uz {
        public a(String str) {
            super(str);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.jq.a(com.yandex.metrica.impl.ob.jq, boolean):boolean
         arg types: [com.yandex.metrica.impl.ob.jq, int]
         candidates:
          com.yandex.metrica.impl.ob.jq.a(com.yandex.metrica.impl.ob.jq, java.util.Map):void
          com.yandex.metrica.impl.ob.jq.a(java.lang.String, int):int
          com.yandex.metrica.impl.ob.jq.a(java.lang.String, long):long
          com.yandex.metrica.impl.ob.jq.a(java.lang.String, java.lang.String):java.lang.String
          com.yandex.metrica.impl.ob.jq.a(java.lang.String, java.lang.Object):void
          com.yandex.metrica.impl.ob.jq.a(java.lang.String, boolean):boolean
          com.yandex.metrica.impl.ob.jq.a(com.yandex.metrica.impl.ob.jq, boolean):boolean */
        public void run() {
            HashMap hashMap;
            synchronized (jq.this.b) {
                jq.this.c();
                boolean unused = jq.this.f = true;
                jq.this.b.notifyAll();
            }
            while (c()) {
                synchronized (this) {
                    if (jq.this.c.size() == 0) {
                        try {
                            wait();
                        } catch (InterruptedException e) {
                        }
                    }
                    hashMap = new HashMap(jq.this.c);
                    jq.this.c.clear();
                }
                if (hashMap.size() > 0) {
                    jq.this.a(hashMap);
                    hashMap.clear();
                }
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* access modifiers changed from: private */
    public void c() {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase;
        Object obj;
        try {
            sQLiteDatabase = this.g.a();
            try {
                cursor = sQLiteDatabase.query(a(), new String[]{"key", "value", "type"}, null, null, null, null, null);
                while (cursor.moveToNext()) {
                    try {
                        String string = cursor.getString(cursor.getColumnIndex("key"));
                        String string2 = cursor.getString(cursor.getColumnIndex("value"));
                        int i = cursor.getInt(cursor.getColumnIndex("type"));
                        if (!TextUtils.isEmpty(string)) {
                            if (i != 1) {
                                if (i == 2) {
                                    obj = to.c(string2);
                                } else if (i == 3) {
                                    obj = to.a(string2);
                                } else if (i != 4) {
                                    obj = i != 5 ? null : to.b(string2);
                                } else {
                                    obj = string2;
                                }
                            } else if ("true".equals(string2)) {
                                obj = Boolean.TRUE;
                            } else {
                                obj = "false".equals(string2) ? Boolean.FALSE : null;
                            }
                            if (obj != null) {
                                this.b.put(string, obj);
                            }
                        }
                    } catch (Throwable th) {
                        th = th;
                        cg.a(cursor);
                        this.g.a(sQLiteDatabase);
                        throw th;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
                cg.a(cursor);
                this.g.a(sQLiteDatabase);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            sQLiteDatabase = null;
            cursor = null;
            cg.a(cursor);
            this.g.a(sQLiteDatabase);
            throw th;
        }
        cg.a(cursor);
        this.g.a(sQLiteDatabase);
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.d;
    }

    public void b() {
        synchronized (this.e) {
            this.e.notifyAll();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void a(Map<String, Object> map) {
        ContentValues[] contentValuesArr = new ContentValues[map.size()];
        int i = 0;
        for (Map.Entry next : map.entrySet()) {
            ContentValues contentValues = new ContentValues();
            Object value = next.getValue();
            contentValues.put("key", (String) next.getKey());
            if (value == this) {
                contentValues.putNull("value");
            } else if (value instanceof String) {
                contentValues.put("value", (String) value);
                contentValues.put("type", (Integer) 4);
            } else if (value instanceof Long) {
                contentValues.put("value", (Long) value);
                contentValues.put("type", (Integer) 3);
            } else if (value instanceof Integer) {
                contentValues.put("value", (Integer) value);
                contentValues.put("type", (Integer) 2);
            } else if (value instanceof Boolean) {
                contentValues.put("value", String.valueOf(((Boolean) value).booleanValue()));
                contentValues.put("type", (Integer) 1);
            } else if (value instanceof Float) {
                contentValues.put("value", (Float) value);
                contentValues.put("type", (Integer) 5);
            } else if (value != null) {
                throw new UnsupportedOperationException();
            }
            contentValuesArr[i] = contentValues;
            i++;
        }
        a(contentValuesArr);
    }

    private void a(ContentValues[] contentValuesArr) {
        SQLiteDatabase sQLiteDatabase;
        if (contentValuesArr != null) {
            try {
                sQLiteDatabase = this.g.a();
                try {
                    sQLiteDatabase.beginTransaction();
                    for (ContentValues contentValues : contentValuesArr) {
                        if (contentValues.getAsString("value") == null) {
                            sQLiteDatabase.delete(a(), "key = ?", new String[]{contentValues.getAsString("key")});
                        } else {
                            sQLiteDatabase.insertWithOnConflict(a(), null, contentValues, 5);
                        }
                    }
                    sQLiteDatabase.setTransactionSuccessful();
                } catch (Throwable th) {
                    th = th;
                    cg.a(sQLiteDatabase);
                    this.g.a(sQLiteDatabase);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                sQLiteDatabase = null;
                cg.a(sQLiteDatabase);
                this.g.a(sQLiteDatabase);
                throw th;
            }
            cg.a(sQLiteDatabase);
            this.g.a(sQLiteDatabase);
        }
    }

    @Nullable
    public String a(String str, String str2) {
        Object c2 = c(str);
        if (c2 instanceof String) {
            return (String) c2;
        }
        return str2;
    }

    public int a(String str, int i) {
        Object c2 = c(str);
        if (c2 instanceof Integer) {
            return ((Integer) c2).intValue();
        }
        return i;
    }

    public long a(String str, long j) {
        Object c2 = c(str);
        if (c2 instanceof Long) {
            return ((Long) c2).longValue();
        }
        return j;
    }

    public boolean a(String str, boolean z) {
        Object c2 = c(str);
        if (c2 instanceof Boolean) {
            return ((Boolean) c2).booleanValue();
        }
        return z;
    }

    public jq a(String str) {
        synchronized (this.b) {
            d();
            this.b.remove(str);
        }
        synchronized (this.e) {
            this.c.put(str, this);
            this.e.notifyAll();
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.jq.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.yandex.metrica.impl.ob.jq.a(com.yandex.metrica.impl.ob.jq, java.util.Map):void
      com.yandex.metrica.impl.ob.jq.a(com.yandex.metrica.impl.ob.jq, boolean):boolean
      com.yandex.metrica.impl.ob.jq.a(java.lang.String, int):int
      com.yandex.metrica.impl.ob.jq.a(java.lang.String, long):long
      com.yandex.metrica.impl.ob.jq.a(java.lang.String, java.lang.String):java.lang.String
      com.yandex.metrica.impl.ob.jq.a(java.lang.String, boolean):boolean
      com.yandex.metrica.impl.ob.jq.a(java.lang.String, java.lang.Object):void */
    public synchronized jq b(String str, String str2) {
        a(str, (Object) str2);
        return this;
    }

    public jq b(String str, long j) {
        a(str, Long.valueOf(j));
        return this;
    }

    public synchronized jq b(String str, int i) {
        a(str, Integer.valueOf(i));
        return this;
    }

    public jq b(String str, boolean z) {
        a(str, Boolean.valueOf(z));
        return this;
    }

    public boolean b(@NonNull String str) {
        boolean containsKey;
        synchronized (this.b) {
            d();
            containsKey = this.b.containsKey(str);
        }
        return containsKey;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(String str, Object obj) {
        synchronized (this.b) {
            d();
            this.b.put(str, obj);
        }
        synchronized (this.e) {
            this.c.put(str, obj);
            this.e.notifyAll();
        }
    }

    private Object c(String str) {
        Object obj;
        synchronized (this.b) {
            d();
            obj = this.b.get(str);
        }
        return obj;
    }

    private void d() {
        if (!this.f) {
            try {
                this.b.wait();
            } catch (InterruptedException e2) {
            }
        }
    }
}
