package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.pm;
import com.yandex.metrica.impl.ob.pn;
import com.yandex.metrica.impl.ob.po;
import com.yandex.metrica.impl.ob.pp;

public class lo {
    private ue a;

    public lo(@NonNull Context context) {
        this(new ue(context));
    }

    @VisibleForTesting
    lo(ue ueVar) {
        this.a = ueVar;
    }

    public ko<pp.a> a() {
        return new km(new kt(), new uf("AES/CBC/PKCS5Padding", this.a.a(), this.a.b()));
    }

    @NonNull
    public ko<pm.a> b() {
        return new km(new kn(), new uf("AES/CBC/PKCS5Padding", this.a.a(), this.a.b()));
    }

    @NonNull
    public ko<po.a> c() {
        return new km(new ks(), new uf("AES/CBC/PKCS5Padding", this.a.a(), this.a.b()));
    }

    public ko<pn.a> d() {
        return new km(new kr(), new uf("AES/CBC/PKCS5Padding", this.a.a(), this.a.b()));
    }
}
