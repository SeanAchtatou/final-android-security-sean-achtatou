package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class ue {
    @NonNull
    private final String a;

    public ue(@NonNull Context context) {
        this(context.getPackageName());
    }

    @VisibleForTesting
    ue(@NonNull String str) {
        this.a = str;
    }

    public byte[] a() {
        try {
            return ts.a(this.a);
        } catch (Throwable th) {
            return new byte[16];
        }
    }

    public byte[] b() {
        try {
            return ts.a(new StringBuilder(this.a).reverse().toString());
        } catch (Throwable th) {
            return new byte[16];
        }
    }
}
