package com.yandex.metrica.impl.ob;

import android.util.Base64;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.impl.ob.o;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class jh {
    private final byte[] a;
    private final String b;
    private final int c;
    @NonNull
    private final HashMap<o.a, Integer> d;
    private final String e;
    private final Integer f;
    private final String g;
    private final String h;
    private final boolean i;
    private final boolean j;

    public jh(@NonNull String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        JSONObject jSONObject2 = jSONObject.getJSONObject(NotificationCompat.CATEGORY_EVENT);
        this.a = Base64.decode(jSONObject2.getString("jvm_crash"), 0);
        this.b = jSONObject2.getString("name");
        this.c = jSONObject2.getInt("bytes_truncated");
        String optString = jSONObject2.optString("trimmed_fields");
        this.d = new HashMap<>();
        if (optString != null) {
            try {
                HashMap<String, String> a2 = th.a(optString);
                if (a2 != null) {
                    for (Map.Entry next : a2.entrySet()) {
                        this.d.put(o.a.valueOf((String) next.getKey()), Integer.valueOf(Integer.parseInt((String) next.getValue())));
                    }
                }
            } catch (Throwable th) {
            }
        }
        JSONObject jSONObject3 = jSONObject.getJSONObject("process_configuration");
        this.e = jSONObject3.getString("package_name");
        this.f = Integer.valueOf(jSONObject3.getInt("pid"));
        this.g = jSONObject3.getString("psid");
        JSONObject jSONObject4 = jSONObject.getJSONObject("reporter_configuration");
        this.h = jSONObject4.getString("api_key");
        this.i = jSONObject4.getBoolean("is_main");
        this.j = jSONObject4.getBoolean("is_commutation");
    }

    public jh(@NonNull t tVar, @NonNull cz czVar, @Nullable HashMap<o.a, Integer> hashMap) {
        this.a = tVar.f();
        this.b = tVar.d();
        this.c = tVar.o();
        if (hashMap != null) {
            this.d = hashMap;
        } else {
            this.d = new HashMap<>();
        }
        da g2 = czVar.g();
        this.e = g2.h();
        this.f = g2.e();
        this.g = g2.f();
        CounterConfiguration h2 = czVar.h();
        this.h = h2.e();
        this.i = h2.p();
        this.j = h2.q();
    }

    public byte[] a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public int c() {
        return this.c;
    }

    @NonNull
    public HashMap<o.a, Integer> d() {
        return this.d;
    }

    public Integer e() {
        return this.f;
    }

    public String f() {
        return this.g;
    }

    public String g() {
        return this.e;
    }

    public String h() {
        return this.h;
    }

    public boolean i() {
        return this.i;
    }

    public boolean j() {
        return this.j;
    }

    public String k() throws JSONException {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.d.entrySet()) {
            hashMap.put(((o.a) next.getKey()).name(), next.getValue());
        }
        return new JSONObject().put("process_configuration", new JSONObject().put("pid", this.f).put("psid", this.g).put("package_name", this.e)).put("reporter_configuration", new JSONObject().put("api_key", this.h).put("is_main", this.i).put("is_commutation", this.j)).put(NotificationCompat.CATEGORY_EVENT, new JSONObject().put("jvm_crash", Base64.encodeToString(this.a, 0)).put("name", this.b).put("bytes_truncated", this.c).put("trimmed_fields", th.a((Map) hashMap))).toString();
    }
}
