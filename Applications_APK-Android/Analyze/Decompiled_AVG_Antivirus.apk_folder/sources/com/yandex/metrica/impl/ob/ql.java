package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.qi;
import com.yandex.metrica.impl.ob.qj;
import com.yandex.metrica.impl.ob.qj.d;

public abstract class ql<T extends qj, IA, A extends qi<IA, A>, L extends qj.d<T, qj.c<A>>> {
    @Nullable
    private T a;
    @NonNull
    private L b;
    @NonNull
    private qj.c<A> c;

    public ql(@NonNull L l, @NonNull sc scVar, @NonNull A a2) {
        this.b = l;
        cn.a().a(this, cv.class, cr.a(new cq<cv>() {
            public void a(cv cvVar) {
                ql.this.a();
            }
        }).a());
        a(new qj.c(scVar, a2));
    }

    public synchronized void a() {
        this.a = null;
    }

    /* access modifiers changed from: protected */
    public synchronized void a(@NonNull qj.c cVar) {
        this.c = cVar;
    }

    public synchronized void a(@NonNull Object obj) {
        if (!((qi) this.c.b).a(obj)) {
            a(new qj.c(b(), ((qi) this.c.b).b(obj)));
            a();
        }
    }

    public synchronized void a(@NonNull sc scVar) {
        a(new qj.c(scVar, c()));
        a();
    }

    @NonNull
    public synchronized sc b() {
        return this.c.a;
    }

    @VisibleForTesting(otherwise = 4)
    @NonNull
    public synchronized A c() {
        return (qi) this.c.b;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: L
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @androidx.annotation.NonNull
    public synchronized T d() {
        /*
            r2 = this;
            monitor-enter(r2)
            T r0 = r2.a     // Catch:{ all -> 0x0013 }
            if (r0 != 0) goto L_0x000f
            L r0 = r2.b     // Catch:{ all -> 0x0013 }
            com.yandex.metrica.impl.ob.qj$c<A> r1 = r2.c     // Catch:{ all -> 0x0013 }
            com.yandex.metrica.impl.ob.qj r0 = r0.a(r1)     // Catch:{ all -> 0x0013 }
            r2.a = r0     // Catch:{ all -> 0x0013 }
        L_0x000f:
            T r0 = r2.a     // Catch:{ all -> 0x0013 }
            monitor-exit(r2)
            return r0
        L_0x0013:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.ql.d():com.yandex.metrica.impl.ob.qj");
    }
}
