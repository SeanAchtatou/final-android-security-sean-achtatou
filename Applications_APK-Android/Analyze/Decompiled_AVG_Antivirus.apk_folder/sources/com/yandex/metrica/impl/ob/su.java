package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.MotionEventCompat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class su implements sw<List<st>> {
    /* access modifiers changed from: private */
    @NonNull
    public final sp a;
    /* access modifiers changed from: private */
    @NonNull
    public nq b;

    su(@NonNull sp spVar, @NonNull nq nqVar) {
        this.a = spVar;
        this.b = nqVar;
    }

    @Nullable
    /* renamed from: a */
    public List<st> d() {
        ArrayList arrayList = new ArrayList();
        if (this.a.h()) {
            if (cg.a(23)) {
                arrayList.addAll(g());
                if (arrayList.size() == 0) {
                    arrayList.add(b());
                }
            } else {
                arrayList.add(b());
            }
        }
        return arrayList;
    }

    private st b() {
        return new st(c(), e(), h(), f(), null);
    }

    @Nullable
    private Integer c() {
        final TelephonyManager c = this.a.c();
        return (Integer) cg.a(new Callable<Integer>() {
            /* renamed from: a */
            public Integer call() {
                String substring = c.getSimOperator().substring(0, 3);
                if (TextUtils.isEmpty(substring)) {
                    return null;
                }
                return Integer.valueOf(Integer.parseInt(substring));
            }
        }, c, "getting SimMcc", "TelephonyManager");
    }

    @Nullable
    private Integer e() {
        final TelephonyManager c = this.a.c();
        return (Integer) cg.a(new Callable<Integer>() {
            /* renamed from: a */
            public Integer call() {
                String substring = c.getSimOperator().substring(3);
                if (TextUtils.isEmpty(substring)) {
                    return null;
                }
                return Integer.valueOf(Integer.parseInt(substring));
            }
        }, c, "getting SimMnc", "TelephonyManager");
    }

    @Nullable
    private String f() {
        final TelephonyManager c = this.a.c();
        return (String) cg.a(new Callable<String>() {
            /* renamed from: a */
            public String call() {
                return c.getSimOperatorName();
            }
        }, c, "getting SimOperatorName", "TelephonyManager");
    }

    @TargetApi(MotionEventCompat.AXIS_BRAKE)
    @NonNull
    private List<st> g() {
        ArrayList arrayList = new ArrayList();
        if (this.b.d(this.a.d())) {
            try {
                List<SubscriptionInfo> activeSubscriptionInfoList = SubscriptionManager.from(this.a.d()).getActiveSubscriptionInfoList();
                if (activeSubscriptionInfoList != null) {
                    for (SubscriptionInfo stVar : activeSubscriptionInfoList) {
                        arrayList.add(new st(stVar));
                    }
                }
            } catch (Throwable th) {
            }
        }
        return arrayList;
    }

    private boolean h() {
        final TelephonyManager c = this.a.c();
        return ((Boolean) cg.a(new Callable<Boolean>() {
            /* renamed from: a */
            public Boolean call() {
                if (su.this.b.d(su.this.a.d())) {
                    return Boolean.valueOf(c.isNetworkRoaming());
                }
                return null;
            }
        }, c, "getting NetworkRoaming", "TelephonyManager", false)).booleanValue();
    }
}
