package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.ey;
import java.io.Closeable;

public class eu implements Cdo, dp, ez {
    @NonNull
    private final Context a;
    @NonNull
    private final df b;
    @NonNull
    private final z c;
    @NonNull
    private ev d;
    @NonNull
    private ef e;

    static class b {
        b() {
        }

        public z<eu> a(@NonNull eu euVar, @NonNull sf sfVar, @NonNull bb bbVar, @NonNull fa faVar, @NonNull kh khVar) {
            return new z(euVar, sfVar.a(), bbVar, faVar, khVar);
        }
    }

    static class a {
        a() {
        }

        public ev a(@NonNull Context context, @NonNull df dfVar, @NonNull sc scVar, @NonNull ey.a aVar) {
            return new ev(new ey.b(context, dfVar.b()), scVar, aVar);
        }
    }

    public eu(@NonNull Context context, @NonNull df dfVar, @NonNull bb bbVar, @NonNull db.a aVar, @NonNull sc scVar, @NonNull sf sfVar) {
        this(context, dfVar, bbVar, aVar, scVar, sfVar, new fa(), new b(), new a(), new ef(context, dfVar), new kh(jo.a(context).b(dfVar)));
    }

    public eu(@NonNull Context context, @NonNull df dfVar, @NonNull bb bbVar, @NonNull db.a aVar, @NonNull sc scVar, @NonNull sf sfVar, @NonNull fa faVar, @NonNull b bVar, @NonNull a aVar2, @NonNull ef efVar, @NonNull kh khVar) {
        sc scVar2 = scVar;
        this.a = context;
        this.b = dfVar;
        this.e = efVar;
        this.c = bVar.a(this, sfVar, bbVar, faVar, khVar);
        synchronized (this) {
            this.e.a(scVar2.y);
            this.d = aVar2.a(context, dfVar, scVar, new ey.a(aVar));
        }
    }

    @NonNull
    public df b() {
        return this.b;
    }

    public void a(@NonNull db.a aVar) {
        this.d.a(aVar);
    }

    public void a(@NonNull t tVar) {
        this.c.a(tVar);
    }

    public void a() {
        if (this.e.a(((ey) this.d.d()).c())) {
            a(ab.a());
            this.e.a();
        }
    }

    public synchronized void a(@Nullable sc scVar) {
        this.d.a(scVar);
        this.e.a(scVar.y);
    }

    public void a(@NonNull rw rwVar) {
    }

    @NonNull
    public ey d() {
        return (ey) this.d.d();
    }

    public void c() {
        cg.a((Closeable) this.c);
    }
}
