package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.concurrent.TimeUnit;

class mr {
    static final long a = TimeUnit.SECONDS.toMillis(1);
    @NonNull
    private Context b;
    @NonNull
    private Looper c;
    @Nullable
    private LocationManager d;
    @NonNull
    private LocationListener e;
    @NonNull
    private nq f;

    public mr(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull LocationListener locationListener, @NonNull nq nqVar) {
        this.b = context;
        this.c = looper;
        this.d = locationManager;
        this.e = locationListener;
        this.f = nqVar;
    }

    public void a() {
        if (this.f.b(this.b)) {
            a("passive", 0.0f, a, this.e, this.c);
        }
    }

    public void b() {
        LocationManager locationManager = this.d;
        if (locationManager != null) {
            try {
                locationManager.removeUpdates(this.e);
            } catch (Throwable th) {
            }
        }
    }

    private void a(String str, float f2, long j, @NonNull LocationListener locationListener, @NonNull Looper looper) {
        LocationManager locationManager = this.d;
        if (locationManager != null) {
            try {
                locationManager.requestLocationUpdates(str, j, f2, locationListener, looper);
            } catch (Throwable th) {
            }
        }
    }
}
