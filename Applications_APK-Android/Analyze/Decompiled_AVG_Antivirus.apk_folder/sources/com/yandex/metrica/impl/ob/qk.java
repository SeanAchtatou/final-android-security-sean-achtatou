package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.qj;
import com.yandex.metrica.impl.ob.qj.a;
import com.yandex.metrica.impl.ob.qj.b;
import com.yandex.metrica.impl.ob.qm;

public abstract class qk<T extends qm, A extends qj.a<db.a, A>, L extends qj.b<T, A>> extends ql<T, db.a, A, L> {
    public qk(@NonNull L l, @NonNull sc scVar, @NonNull A a) {
        super(l, scVar, a);
    }

    public synchronized void a(@NonNull db.a aVar) {
        super.a((Object) aVar);
    }
}
