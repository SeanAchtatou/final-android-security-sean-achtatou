package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pf;

public class jc implements ky<StackTraceElement, pf.d> {
    @NonNull
    /* renamed from: a */
    public pf.d b(@NonNull StackTraceElement stackTraceElement) {
        pf.d dVar = new pf.d();
        dVar.b = stackTraceElement.getClassName();
        dVar.c = ua.b(stackTraceElement.getFileName(), "");
        dVar.d = stackTraceElement.getLineNumber();
        dVar.e = stackTraceElement.getMethodName();
        dVar.f = stackTraceElement.isNativeMethod();
        return dVar;
    }

    @NonNull
    public StackTraceElement a(@NonNull pf.d dVar) {
        throw new UnsupportedOperationException();
    }
}
