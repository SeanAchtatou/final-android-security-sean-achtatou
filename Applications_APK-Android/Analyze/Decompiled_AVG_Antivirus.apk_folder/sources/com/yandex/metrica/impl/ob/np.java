package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

class np implements no {
    private final boolean a;

    np(boolean z) {
        this.a = z;
    }

    public boolean a(@NonNull String str) {
        if ("android.permission.ACCESS_FINE_LOCATION".equals(str) || "android.permission.ACCESS_COARSE_LOCATION".equals(str)) {
            return this.a;
        }
        return true;
    }

    public String toString() {
        return "LocationFlagStrategy{mEnabled=" + this.a + '}';
    }
}
