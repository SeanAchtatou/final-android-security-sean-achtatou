package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.pk;

public abstract class oq<T> extends ov {
    @NonNull
    private final T a;

    /* access modifiers changed from: protected */
    public abstract void a(@NonNull pk.a.C0009a aVar);

    oq(int i, @NonNull String str, @NonNull T t, @NonNull vx<String> vxVar, @NonNull on onVar) {
        super(i, str, vxVar, onVar);
        this.a = t;
    }

    @NonNull
    public T b() {
        return this.a;
    }

    public void a(@NonNull pd pdVar) {
        pk.a.C0009a b;
        if (f() && (b = b(pdVar)) != null) {
            a(b);
        }
    }

    @Nullable
    private pk.a.C0009a b(@NonNull pd pdVar) {
        return e().a(pdVar, pdVar.a(d(), c()), this);
    }
}
