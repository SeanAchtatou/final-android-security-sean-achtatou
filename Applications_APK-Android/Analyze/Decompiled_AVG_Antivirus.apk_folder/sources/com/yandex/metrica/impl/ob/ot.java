package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

public class ot implements vx<String> {
    public vv a(@Nullable String str) {
        if (str == null) {
            return vv.a(this, "key is null");
        }
        if (str.startsWith("appmetrica")) {
            return vv.a(this, "key starts with appmetrica");
        }
        if (str.length() > 200) {
            return vv.a(this, "key length more then 200 characters");
        }
        return vv.a(this);
    }
}
