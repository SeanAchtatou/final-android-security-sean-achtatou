package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import java.util.UUID;

public class vy implements vx<String> {
    public vv a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return vv.a(this, "ApiKey is empty. Please, read official documentation how to obtain one: https://tech.yandex.com/metrica-mobile-sdk/doc/mobile-sdk-dg/concepts/android-initialize-docpage/");
        }
        try {
            UUID.fromString(str);
            return vv.a(this);
        } catch (Throwable th) {
            return vv.a(this, "Invalid ApiKey=" + str + ". " + "Please, read official documentation how to obtain one:" + " " + "https://tech.yandex.com/metrica-mobile-sdk/doc/mobile-sdk-dg/concepts/android-initialize-docpage/");
        }
    }
}
