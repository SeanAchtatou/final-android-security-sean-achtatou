package com.yandex.metrica.impl.ob;

import android.os.Bundle;

public enum rw {
    UNKNOWN(0),
    NETWORK(1),
    PARSE(2);
    
    private int d;

    private rw(int i) {
        this.d = i;
    }

    public int a() {
        return this.d;
    }

    public Bundle a(Bundle bundle) {
        bundle.putInt("startup_error_key_code", a());
        return bundle;
    }

    public static rw b(Bundle bundle) {
        return a(bundle.getInt("startup_error_key_code"));
    }

    private static rw a(int i) {
        rw rwVar = UNKNOWN;
        if (i == 1) {
            return NETWORK;
        }
        if (i != 2) {
            return rwVar;
        }
        return PARSE;
    }
}
