package com.yandex.metrica.impl.ob;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class va implements ThreadFactory {
    private static final AtomicInteger a = new AtomicInteger(0);
    private final String b;

    public va(String str) {
        this.b = str;
    }

    /* renamed from: a */
    public uz newThread(Runnable runnable) {
        return new uz(runnable, c());
    }

    private String c() {
        return a(this.b);
    }

    public uy a() {
        return new uy(c());
    }

    public static uz a(String str, Runnable runnable) {
        return new va(str).newThread(runnable);
    }

    public static String a(String str) {
        return str + "-" + b();
    }

    public static int b() {
        return a.incrementAndGet();
    }
}
