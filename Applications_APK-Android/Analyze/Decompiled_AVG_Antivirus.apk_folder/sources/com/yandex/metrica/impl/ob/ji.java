package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import androidx.annotation.NonNull;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import kotlin.jvm.internal.LongCompanionObject;

public abstract class ji {
    @NonNull
    private final jn a;
    @NonNull
    private final kj b;
    @NonNull
    private final AtomicLong c;
    @NonNull
    private final AtomicLong d;
    @NonNull
    private final AtomicLong e;
    @NonNull
    private final ContentValues f = new ContentValues();

    /* access modifiers changed from: protected */
    public abstract long c(long j);

    /* access modifiers changed from: protected */
    public abstract kj d(long j);

    public abstract String e();

    ji(@NonNull jn jnVar, @NonNull kj kjVar) {
        this.a = jnVar;
        this.b = kjVar;
        this.c = new AtomicLong(f());
        this.d = new AtomicLong(a((long) LongCompanionObject.MAX_VALUE));
        this.e = new AtomicLong(c(-1L));
    }

    public long a() {
        return this.c.get();
    }

    public long b() {
        return this.d.get();
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public kj c() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public jn d() {
        return this.a;
    }

    private long f() {
        try {
            return tc.a(d().getReadableDatabase(), e());
        } catch (Throwable th) {
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j, String str) {
        this.c.incrementAndGet();
        this.e.incrementAndGet();
        d(this.e.get()).n();
        if (this.d.get() > j) {
            this.d.set(j);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.c.getAndAdd((long) (-i));
        this.d.set(a((long) LongCompanionObject.MAX_VALUE));
    }

    public long a(long j) {
        Cursor cursor = null;
        try {
            cursor = d().getReadableDatabase().rawQuery(String.format(Locale.US, "Select min(%s) from %s", "timestamp", e()), null);
            if (cursor.moveToFirst()) {
                long j2 = cursor.getLong(0);
                if (j2 != 0) {
                    j = j2;
                }
            }
        } catch (Throwable th) {
            cg.a((Cursor) null);
            throw th;
        }
        cg.a(cursor);
        return j;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b(long r6, java.lang.String r8) {
        /*
            r5 = this;
            monitor-enter(r5)
            com.yandex.metrica.impl.ob.jn r0 = r5.d()     // Catch:{ Throwable -> 0x0023, all -> 0x0020 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Throwable -> 0x0023, all -> 0x0020 }
            android.content.ContentValues r1 = r5.c(r6, r8)     // Catch:{ Throwable -> 0x0023, all -> 0x0020 }
            java.lang.String r2 = r5.e()     // Catch:{ Throwable -> 0x0023, all -> 0x0020 }
            r3 = 0
            long r0 = r0.insert(r2, r3, r1)     // Catch:{ Throwable -> 0x0023, all -> 0x0020 }
            r2 = -1
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x001f
            r5.a(r6, r8)     // Catch:{ Throwable -> 0x0023, all -> 0x0020 }
        L_0x001f:
            goto L_0x0024
        L_0x0020:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x0023:
            r6 = move-exception
        L_0x0024:
            monitor-exit(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.ji.b(long, java.lang.String):void");
    }

    @NonNull
    private ContentValues c(long j, @NonNull String str) {
        this.f.clear();
        this.f.put("incremental_id", Long.valueOf(this.e.get() + 1));
        this.f.put("timestamp", Long.valueOf(j));
        this.f.put("data", str);
        return this.f;
    }

    @NonNull
    public synchronized Map<Long, String> b(int i) {
        LinkedHashMap linkedHashMap;
        linkedHashMap = new LinkedHashMap();
        try {
            Cursor query = d().getReadableDatabase().query(e(), new String[]{"incremental_id", "data"}, null, null, null, null, "incremental_id ASC", String.valueOf(i));
            while (query.moveToNext()) {
                this.f.clear();
                tc.a(query, this.f);
                linkedHashMap.put(this.f.getAsLong("incremental_id"), this.f.getAsString("data"));
            }
            cg.a(query);
        } catch (Throwable th) {
            cg.a((Cursor) null);
            throw th;
        }
        return linkedHashMap;
    }

    public synchronized int b(long j) {
        int i;
        i = 0;
        String format = String.format(Locale.US, "%s <= ?", "incremental_id");
        try {
            i = d().getWritableDatabase().delete(e(), format, new String[]{String.valueOf(j)});
            if (i > 0) {
                a(i);
            }
        } catch (Throwable th) {
        }
        return i;
    }

    public synchronized int c(int i) {
        int i2;
        i2 = 0;
        if (i < 1) {
            return 0;
        }
        String format = String.format(Locale.US, "%1$s <= (select max(%1$s) from (select %1$s from %2$s order by %1$s limit ?))", "incremental_id", e());
        try {
            i2 = d().getWritableDatabase().delete(e(), format, new String[]{String.valueOf(i)});
            if (i2 > 0) {
                a(i2);
            }
        } catch (Throwable th) {
        }
        return i2;
    }
}
