package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public class ii {
    @NonNull
    private final uv a;
    @NonNull
    private final ie b;

    public interface a {
        void a();
    }

    public ii(@NonNull uv uvVar, @NonNull ie ieVar) {
        this.a = uvVar;
        this.b = ieVar;
    }

    public void a(long j, @NonNull final a aVar) {
        this.a.a(new Runnable() {
            public void run() {
                try {
                    aVar.a();
                } catch (Throwable th) {
                }
            }
        }, j);
    }

    public void a(long j, boolean z) {
        this.b.a(j, z);
    }

    public void a() {
        this.b.a();
    }
}
