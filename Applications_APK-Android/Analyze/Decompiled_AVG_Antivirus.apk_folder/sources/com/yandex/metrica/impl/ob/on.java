package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.pk;

public abstract class on {
    @NonNull
    private final om a;

    public abstract pk.a.C0009a a(@NonNull pd pdVar, @Nullable pk.a.C0009a aVar, @NonNull ol olVar);

    on(@NonNull om omVar) {
        this.a = omVar;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public om a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(@Nullable pk.a.C0009a aVar) {
        return aVar == null || aVar.d.c;
    }
}
