package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class vm extends ve<String> {
    @VisibleForTesting(otherwise = 3)
    public /* bridge */ /* synthetic */ int a() {
        return super.a();
    }

    @VisibleForTesting(otherwise = 3)
    @NonNull
    public /* bridge */ /* synthetic */ String b() {
        return super.b();
    }

    public vm(int i, @NonNull String str) {
        this(i, str, tp.h());
    }

    public vm(int i, @NonNull String str, @NonNull tp tpVar) {
        super(i, str, tpVar);
    }

    @Nullable
    public String a(@Nullable String str) {
        if (str == null || str.length() <= a()) {
            return str;
        }
        String substring = str.substring(0, a());
        if (this.a.c()) {
            this.a.b("\"%s\" %s size exceeded limit of %d characters", b(), str, Integer.valueOf(a()));
        }
        return substring;
    }
}
