package com.yandex.metrica.impl.ob;

public class kf extends ki {
    private final oj a = new oj("init_event_pref_key");
    private final oj b = new oj("first_event_pref_key");
    private final oj c = new oj("first_event_description_key");
    private final oj d = new oj("preload_info_auto_tracking_enabled");

    public kf(jq jqVar) {
        super(jqVar);
    }

    public void a() {
        b(this.a.b(), "DONE").n();
    }

    public void b() {
        b(this.b.b(), "DONE").n();
    }

    public String a(String str) {
        return c(this.a.b(), str);
    }

    public String b(String str) {
        return c(this.b.b(), str);
    }

    public boolean c() {
        return a(null) != null;
    }

    public boolean d() {
        return b(null) != null;
    }

    public void c(String str) {
        b(this.c.b(), str).n();
    }

    public String d(String str) {
        return c(this.c.b(), str);
    }

    public void e() {
        a(this.c);
    }

    public void a(boolean z) {
        a(this.d.b(), z).n();
    }

    public boolean b(boolean z) {
        return b(this.d.b(), z);
    }

    private void a(oj ojVar) {
        p(ojVar.b()).n();
    }
}
