package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class hq {
    @NonNull
    private final hw a;
    @Nullable
    private final Integer b;

    private hq(a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
    }

    public static final a a(hw hwVar) {
        return new a(hwVar);
    }

    @NonNull
    public hw a() {
        return this.a;
    }

    @Nullable
    public Integer b() {
        return this.b;
    }

    static final class a {
        /* access modifiers changed from: private */
        @NonNull
        public hw a;
        /* access modifiers changed from: private */
        @Nullable
        public Integer b;

        private a(hw hwVar) {
            this.a = hwVar;
        }

        public a a(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        public hq a() {
            return new hq(this);
        }
    }
}
