package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class vn<T> implements vo<T> {
    @NonNull
    private final vo<T> a;
    @Nullable
    private final T b;

    public vn(@NonNull vo<T> voVar, @Nullable T t) {
        this.a = voVar;
        this.b = t;
    }

    @Nullable
    public T a(@Nullable T t) {
        if (t != this.a.a(t)) {
            return this.b;
        }
        return t;
    }
}
