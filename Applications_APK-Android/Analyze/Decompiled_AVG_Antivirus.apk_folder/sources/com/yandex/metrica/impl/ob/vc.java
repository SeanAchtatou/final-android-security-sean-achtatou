package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.Executor;

public class vc {
    @NonNull
    private final vb a;
    @Nullable
    private volatile uv b;
    @Nullable
    private volatile Executor c;
    @Nullable
    private volatile uv d;
    @Nullable
    private volatile uv e;
    @Nullable
    private volatile uw f;
    @Nullable
    private volatile uv g;
    @Nullable
    private volatile uv h;
    @Nullable
    private volatile uv i;
    @Nullable
    private volatile uv j;

    public vc() {
        this(new vb());
    }

    @NonNull
    public uv a() {
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    this.b = this.a.a();
                }
            }
        }
        return this.b;
    }

    @NonNull
    public Executor b() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = this.a.b();
                }
            }
        }
        return this.c;
    }

    @NonNull
    public uv c() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    this.d = this.a.c();
                }
            }
        }
        return this.d;
    }

    @NonNull
    public uv d() {
        if (this.e == null) {
            synchronized (this) {
                if (this.e == null) {
                    this.e = this.a.d();
                }
            }
        }
        return this.e;
    }

    @NonNull
    public uw e() {
        if (this.f == null) {
            synchronized (this) {
                if (this.f == null) {
                    this.f = this.a.e();
                }
            }
        }
        return this.f;
    }

    @NonNull
    public uv f() {
        if (this.g == null) {
            synchronized (this) {
                if (this.g == null) {
                    this.g = this.a.f();
                }
            }
        }
        return this.g;
    }

    @NonNull
    public uv g() {
        if (this.h == null) {
            synchronized (this) {
                if (this.h == null) {
                    this.h = this.a.g();
                }
            }
        }
        return this.h;
    }

    @NonNull
    public uv h() {
        if (this.i == null) {
            synchronized (this) {
                if (this.i == null) {
                    this.i = this.a.h();
                }
            }
        }
        return this.i;
    }

    @NonNull
    public uv i() {
        if (this.j == null) {
            synchronized (this) {
                if (this.j == null) {
                    this.j = this.a.i();
                }
            }
        }
        return this.j;
    }

    @NonNull
    public uz a(@NonNull Runnable runnable) {
        return this.a.a(runnable);
    }

    @VisibleForTesting
    vc(@NonNull vb vbVar) {
        this.a = vbVar;
    }
}
