package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;
import java.util.concurrent.Callable;

public abstract class tq<T> implements Callable<T> {
    public abstract T b() throws Exception;

    @Nullable
    public T call() {
        try {
            return b();
        } catch (Throwable th) {
            return null;
        }
    }
}
