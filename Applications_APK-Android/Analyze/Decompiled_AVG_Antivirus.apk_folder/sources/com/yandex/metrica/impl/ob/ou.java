package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.pk;

public class ou implements om {
    @NonNull
    private final vg a;

    public ou(@NonNull vg vgVar) {
        this.a = vgVar;
    }

    @Nullable
    public pk.a.C0009a a(@NonNull pd pdVar, @NonNull pk.a.C0009a aVar) {
        if (pdVar.a() == this.a.a()) {
            if (pdVar.a(aVar.c, new String(aVar.b)) != null) {
                pdVar.a(aVar);
            }
        } else if (pdVar.a() < this.a.a()) {
            pdVar.a(aVar);
            pdVar.b();
        }
        return aVar;
    }
}
