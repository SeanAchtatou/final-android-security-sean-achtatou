package com.yandex.metrica.impl.interact;

import android.content.Context;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.cj;
import com.yandex.metrica.impl.ob.sm;
import com.yandex.metrica.impl.ob.sv;
import com.yandex.metrica.impl.ob.sx;
import com.yandex.metrica.impl.ob.sy;
import java.util.HashMap;
import java.util.Map;

public class CellularNetworkInfo {
    /* access modifiers changed from: private */
    public String a = "";

    public CellularNetworkInfo(Context context) {
        new sv(context, cj.l().b()).a(new sy() {
            public void a(sx sxVar) {
                sm b = sxVar.b();
                if (b != null) {
                    String g = b.g();
                    String f = b.f();
                    Integer c = b.c();
                    Integer b2 = b.b();
                    Integer e = b.e();
                    Integer d = b.d();
                    Integer a2 = b.a();
                    HashMap hashMap = new HashMap();
                    hashMap.put("network_type", g);
                    hashMap.put("operator_name", f);
                    String str = null;
                    hashMap.put("country_code", b2 != null ? String.valueOf(b2) : null);
                    hashMap.put("operator_id", c != null ? String.valueOf(c) : null);
                    hashMap.put("cell_id", e != null ? String.valueOf(e) : null);
                    hashMap.put("lac", d != null ? String.valueOf(d) : null);
                    if (a2 != null) {
                        str = String.valueOf(a2);
                    }
                    hashMap.put("signal_strength", str);
                    StringBuilder sb = new StringBuilder();
                    String str2 = "";
                    for (Map.Entry entry : hashMap.entrySet()) {
                        String str3 = (String) entry.getValue();
                        if (!TextUtils.isEmpty(str3)) {
                            sb.append(str2);
                            sb.append((String) entry.getKey());
                            sb.append("=");
                            sb.append(str3);
                            str2 = "&";
                        }
                    }
                    String unused = CellularNetworkInfo.this.a = sb.toString();
                }
            }
        });
    }

    public String getCelluralInfo() {
        return this.a;
    }
}
