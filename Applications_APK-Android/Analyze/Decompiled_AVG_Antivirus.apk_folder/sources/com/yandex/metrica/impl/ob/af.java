package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.cd;
import com.yandex.metrica.impl.ob.lp;
import com.yandex.metrica.impl.ob.pq;

public final class af {
    @SuppressLint({"StaticFieldLeak"})
    private static volatile af a;
    @NonNull
    private final Context b;
    private volatile qe c;
    private volatile rf d;
    private volatile pq e;
    private volatile cd f;
    private volatile h g;
    @Nullable
    private volatile qz h;
    @Nullable
    private volatile ac i;
    @NonNull
    private volatile vc j = new vc();
    @Nullable
    private volatile sv k;

    public static void a(@NonNull Context context) {
        if (a == null) {
            synchronized (af.class) {
                if (a == null) {
                    a = new af(context.getApplicationContext());
                }
            }
        }
    }

    public static af a() {
        return a;
    }

    private af(@NonNull Context context) {
        this.b = context;
    }

    @NonNull
    public Context b() {
        return this.b;
    }

    @NonNull
    public qe c() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = new qe(this.b);
                }
            }
        }
        return this.c;
    }

    @NonNull
    public rf d() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    this.d = new rf(this.b);
                }
            }
        }
        return this.d;
    }

    @NonNull
    public pq e() {
        if (this.e == null) {
            synchronized (this) {
                if (this.e == null) {
                    this.e = new pq(this.b, lp.a.a(pq.a.class).a(this.b), a().h(), d(), this.j.h());
                }
            }
        }
        return this.e;
    }

    @NonNull
    public qz f() {
        if (this.h == null) {
            synchronized (this) {
                if (this.h == null) {
                    this.h = new qz(this.b, this.j.h());
                }
            }
        }
        return this.h;
    }

    @NonNull
    public ac g() {
        if (this.i == null) {
            synchronized (this) {
                if (this.i == null) {
                    this.i = new ac();
                }
            }
        }
        return this.i;
    }

    @NonNull
    public cd h() {
        if (this.f == null) {
            synchronized (this) {
                if (this.f == null) {
                    this.f = new cd(new cd.b(new kj(jo.a(this.b).c())));
                }
            }
        }
        return this.f;
    }

    @NonNull
    public h i() {
        if (this.g == null) {
            synchronized (this) {
                if (this.g == null) {
                    this.g = new h();
                }
            }
        }
        return this.g;
    }

    public void a(@Nullable sc scVar) {
        if (this.h != null) {
            this.h.b(scVar);
        }
        if (this.i != null) {
            this.i.a(scVar);
        }
    }

    @NonNull
    public synchronized vc j() {
        return this.j;
    }

    @NonNull
    public sv k() {
        if (this.k == null) {
            synchronized (this) {
                if (this.k == null) {
                    this.k = new sv(this.b, j().d());
                }
            }
        }
        return this.k;
    }
}
