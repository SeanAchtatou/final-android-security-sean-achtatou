package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

class wa implements vx<Integer> {
    wa() {
    }

    public vv a(@Nullable Integer num) {
        if (num == null || num.intValue() > 0) {
            return vv.a(this);
        }
        return vv.a(this, "Invalid quantity value " + num);
    }
}
