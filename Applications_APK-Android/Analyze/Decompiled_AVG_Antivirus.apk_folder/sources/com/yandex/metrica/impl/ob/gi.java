package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;

public class gi extends ft {
    public gi(di diVar) {
        super(diVar);
    }

    public boolean a(@NonNull t tVar) {
        if (!b(tVar)) {
            return false;
        }
        a().a(tVar.l());
        return false;
    }

    private boolean b(t tVar) {
        return !TextUtils.isEmpty(tVar.l()) && TextUtils.isEmpty(a().g());
    }
}
