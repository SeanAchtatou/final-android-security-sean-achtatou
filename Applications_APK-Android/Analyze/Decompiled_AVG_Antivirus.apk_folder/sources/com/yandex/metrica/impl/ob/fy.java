package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class fy extends ft {
    @NonNull
    private final hd a;

    public fy(@NonNull di diVar) {
        this(diVar, diVar.y());
    }

    public boolean a(@NonNull t tVar) {
        if (TextUtils.isEmpty(tVar.d())) {
            return false;
        }
        tVar.a(this.a.a(tVar.d()));
        return false;
    }

    @VisibleForTesting
    fy(@NonNull di diVar, @NonNull hd hdVar) {
        super(diVar);
        this.a = hdVar;
    }
}
