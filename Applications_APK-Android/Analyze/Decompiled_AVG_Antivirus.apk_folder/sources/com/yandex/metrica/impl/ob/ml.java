package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class ml {
    @NonNull
    private final ch a;
    @NonNull
    private final sv b;
    @NonNull
    private final mw c;
    @NonNull
    private final mm d;
    @NonNull
    private final md e;
    @NonNull
    private final tw f;
    @NonNull
    private final uv g;
    @Nullable
    private mh h;
    private boolean i;
    private final Runnable j;

    public ml(@NonNull Context context, @NonNull ch chVar, @NonNull sv svVar, @NonNull mw mwVar, @NonNull md mdVar, @NonNull uv uvVar, @Nullable mh mhVar) {
        this(chVar, svVar, mwVar, new mm(context), new tw(), mdVar, uvVar, mhVar);
    }

    public void a() {
        c();
    }

    public void b() {
        f();
    }

    public void c() {
        mh mhVar = this.h;
        boolean z = mhVar != null && mhVar.m;
        if (this.i != z) {
            this.i = z;
            if (this.i) {
                this.i = true;
                e();
                return;
            }
            this.i = false;
            f();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        mh mhVar = this.h;
        if (mhVar != null && mhVar.l > 0) {
            this.g.a(this.j, this.h.l);
        }
    }

    private void f() {
        this.g.b(this.j);
    }

    public void a(@Nullable mh mhVar) {
        this.h = mhVar;
        c();
    }

    public void d() {
        final mn mnVar = new mn();
        mnVar.a(this.f.a());
        mnVar.b(this.f.c());
        mnVar.a(this.a.a());
        this.b.a(new sn() {
            public void a(sm[] smVarArr) {
                mnVar.b(th.a(smVarArr));
            }
        });
        this.d.a(mnVar);
        this.c.a();
        this.e.a();
    }

    @VisibleForTesting
    ml(@NonNull ch chVar, @NonNull sv svVar, @NonNull mw mwVar, @NonNull mm mmVar, @NonNull tw twVar, @NonNull md mdVar, @NonNull uv uvVar, @Nullable mh mhVar) {
        this.j = new Runnable() {
            public void run() {
                ml.this.d();
                ml.this.e();
            }
        };
        this.a = chVar;
        this.b = svVar;
        this.c = mwVar;
        this.d = mmVar;
        this.f = twVar;
        this.e = mdVar;
        this.g = uvVar;
        this.h = mhVar;
    }
}
