package com.yandex.metrica.impl.ob;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class av implements au {
    private uv a;
    /* access modifiers changed from: private */
    public au b;

    public av(@NonNull au auVar) {
        this(af.a().j().a(), auVar);
    }

    public void a() {
        this.a.a(new tr() {
            public void a() {
                av.this.b.a();
            }
        });
    }

    public void a(final Intent intent, final int i) {
        this.a.a(new tr() {
            public void a() {
                av.this.b.a(intent, i);
            }
        });
    }

    public void a(final Intent intent, final int i, final int i2) {
        this.a.a(new tr() {
            public void a() {
                av.this.b.a(intent, i, i2);
            }
        });
    }

    public void a(final Intent intent) {
        this.a.a(new tr() {
            public void a() {
                av.this.b.a(intent);
            }
        });
    }

    public void b(final Intent intent) {
        this.a.a(new tr() {
            public void a() {
                av.this.b.b(intent);
            }
        });
    }

    public void c(final Intent intent) {
        this.a.a(new tr() {
            public void a() {
                av.this.b.c(intent);
            }
        });
    }

    public void b() {
        this.a.a(new tr() {
            public void a() throws Exception {
                av.this.b.b();
            }
        });
    }

    public void a(String str, int i, String str2, Bundle bundle) throws RemoteException {
        final String str3 = str;
        final int i2 = i;
        final String str4 = str2;
        final Bundle bundle2 = bundle;
        this.a.a(new tr() {
            public void a() throws RemoteException {
                av.this.b.a(str3, i2, str4, bundle2);
            }
        });
    }

    public void a(final Bundle bundle) throws RemoteException {
        this.a.a(new tr() {
            public void a() throws Exception {
                av.this.b.a(bundle);
            }
        });
    }

    @VisibleForTesting
    av(@NonNull uv uvVar, @NonNull au auVar) {
        this.a = uvVar;
        this.b = auVar;
    }
}
