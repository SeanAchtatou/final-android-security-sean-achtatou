package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

class mv extends mf {
    @NonNull
    private js a;
    @NonNull
    private mg b;
    @NonNull
    private tw c;

    public mv(@NonNull Context context, @Nullable me meVar) {
        this(meVar, jo.a(context).g(), new mg(context), new tw());
    }

    public void b(@Nullable String str, @Nullable Location location, @Nullable mh mhVar) {
        if (mhVar != null && location != null) {
            ms msVar = new ms(mhVar.a(), this.c.a(), this.c.c(), location);
            String a2 = this.b.a(msVar);
            if (!TextUtils.isEmpty(a2)) {
                this.a.b(msVar.b(), a2);
            }
        }
    }

    mv(@Nullable me meVar, @NonNull js jsVar, @NonNull mg mgVar, @NonNull tw twVar) {
        super(meVar);
        this.a = jsVar;
        this.b = mgVar;
        this.c = twVar;
    }
}
