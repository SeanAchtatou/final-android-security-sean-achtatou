package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.concurrent.TimeUnit;

public class ua {
    @Nullable
    public static <T> T a(@Nullable Object obj, @Nullable Object obj2) {
        return obj == null ? obj2 : obj;
    }

    @NonNull
    public static String a(@Nullable String str, @NonNull String str2) {
        return TextUtils.isEmpty(str) ? str2 : str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ua.b(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.yandex.metrica.impl.ob.ua.b(java.lang.String, java.lang.String):java.lang.String
      com.yandex.metrica.impl.ob.ua.b(java.lang.Object, java.lang.Object):T */
    @NonNull
    public static String b(@Nullable String str, @NonNull String str2) {
        return (String) b((Object) str, (Object) str2);
    }

    public static boolean a(@Nullable Boolean bool, boolean z) {
        return ((Boolean) b(bool, Boolean.valueOf(z))).booleanValue();
    }

    public static long a(@Nullable Long l, long j) {
        return ((Long) b(l, Long.valueOf(j))).longValue();
    }

    public static int a(@Nullable Integer num, int i) {
        return ((Integer) b(num, Integer.valueOf(i))).intValue();
    }

    public static float a(@Nullable Float f, float f2) {
        return ((Float) b(f, Float.valueOf(f2))).floatValue();
    }

    public static long a(@Nullable Long l, @NonNull TimeUnit timeUnit, long j) {
        return l == null ? j : timeUnit.toMillis(l.longValue());
    }

    @NonNull
    private static <T> T b(@Nullable T t, @NonNull T t2) {
        return t == null ? t2 : t;
    }
}
