package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class js extends ji {
    js(@NonNull Context context, @NonNull jn jnVar) {
        this(jnVar, new kj(jo.a(context).c()));
    }

    @VisibleForTesting
    js(@NonNull jn jnVar, @NonNull kj kjVar) {
        super(jnVar, kjVar);
    }

    /* access modifiers changed from: protected */
    public long c(long j) {
        return c().d(j);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public kj d(long j) {
        return c().e(j);
    }

    @NonNull
    public String e() {
        return "l_dat";
    }
}
