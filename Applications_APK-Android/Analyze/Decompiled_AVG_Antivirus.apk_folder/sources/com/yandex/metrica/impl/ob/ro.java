package com.yandex.metrica.impl.ob;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.MetricaService;
import com.yandex.metrica.c;
import com.yandex.metrica.impl.ob.ac;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ro implements rn, Runnable {
    /* access modifiers changed from: private */
    public final ServiceConnection a;
    private final Handler b;
    private HashMap<String, rl> c;
    /* access modifiers changed from: private */
    public final Context d;
    private boolean e;
    private ServerSocket f;
    /* access modifiers changed from: private */
    public final rj g;
    /* access modifiers changed from: private */
    public rt h;
    private uz i;
    @NonNull
    private final ac.b j;

    public static class a {
        public ro a(@NonNull Context context) {
            return new ro(context);
        }
    }

    public ro(Context context) {
        this(context, af.a().g(), af.a().j().i());
    }

    @VisibleForTesting
    ro(@NonNull Context context, @NonNull ac acVar, @NonNull uv uvVar) {
        this.a = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder service) {
            }

            public void onServiceDisconnected(ComponentName name) {
            }
        };
        this.b = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 100) {
                    ro.this.e();
                    try {
                        ro.this.d.unbindService(ro.this.a);
                    } catch (Throwable th) {
                        rh.a(ro.this.d).reportEvent("socket_unbind_has_thrown_exception");
                    }
                }
            }
        };
        this.c = new HashMap<String, rl>() {
            {
                put("p", new rl() {
                    @NonNull
                    public rk a(@NonNull Socket socket, @NonNull Uri uri) {
                        return new ri(socket, uri, ro.this, ro.this.h, ro.this.g);
                    }
                });
            }
        };
        this.g = new rj();
        this.d = context;
        this.j = acVar.a(new Runnable() {
            public void run() {
                ro.this.h();
            }
        }, uvVar);
        g();
    }

    private void g() {
        cn.a().a(this, cy.class, cr.a(new cq<cy>() {
            public void a(cy cyVar) {
                ro.this.g.a(cyVar.a);
            }
        }).a(new co<cy>() {
            public boolean a(cy cyVar) {
                return !ro.this.d.getPackageName().equals(cyVar.b);
            }
        }).a());
        cn.a().a(this, cu.class, cr.a(new cq<cu>() {
            public void a(cu cuVar) {
                ro.this.g.b(cuVar.a);
            }
        }).a());
        cn.a().a(this, cs.class, cr.a(new cq<cs>() {
            public void a(cs csVar) {
                ro.this.g.c(csVar.a);
            }
        }).a());
        cn.a().a(this, ct.class, cr.a(new cq<ct>() {
            public void a(ct ctVar) {
                ro.this.g.d(ctVar.a);
            }
        }).a());
        cn.a().a(this, cw.class, cr.a(new cq<cw>() {
            public void a(cw cwVar) {
                ro.this.a(cwVar.a);
                ro.this.c();
            }
        }).a());
    }

    public void a() {
        if (this.e) {
            b();
            Handler handler = this.b;
            handler.sendMessageDelayed(handler.obtainMessage(100), TimeUnit.SECONDS.toMillis(this.h.a));
        }
    }

    public void b() {
        this.b.removeMessages(100);
    }

    public synchronized void c() {
        if (!(this.e || this.h == null || !this.j.a(this.h.e))) {
            this.e = true;
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        d();
        this.i = af.a().j().a(this);
        this.i.start();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(rt rtVar) {
        this.h = rtVar;
        rt rtVar2 = this.h;
        if (rtVar2 != null) {
            this.j.a(rtVar2.d);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void d() {
        Intent intent = new Intent(this.d, MetricaService.class);
        intent.setAction("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER");
        try {
            if (!this.d.bindService(intent, this.a, 1)) {
                rh.a(this.d).reportEvent("socket_bind_has_failed");
            }
        } catch (Throwable th) {
            rh.a(this.d).reportEvent("socket_bind_has_thrown_exception");
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void e() {
        /*
            r2 = this;
            monitor-enter(r2)
            r0 = 0
            r2.e = r0     // Catch:{ IOException -> 0x001f, all -> 0x001c }
            com.yandex.metrica.impl.ob.uz r0 = r2.i     // Catch:{ IOException -> 0x001f, all -> 0x001c }
            r1 = 0
            if (r0 == 0) goto L_0x0010
            com.yandex.metrica.impl.ob.uz r0 = r2.i     // Catch:{ IOException -> 0x001f, all -> 0x001c }
            r0.b()     // Catch:{ IOException -> 0x001f, all -> 0x001c }
            r2.i = r1     // Catch:{ IOException -> 0x001f, all -> 0x001c }
        L_0x0010:
            java.net.ServerSocket r0 = r2.f     // Catch:{ IOException -> 0x001f, all -> 0x001c }
            if (r0 == 0) goto L_0x001b
            java.net.ServerSocket r0 = r2.f     // Catch:{ IOException -> 0x001f, all -> 0x001c }
            r0.close()     // Catch:{ IOException -> 0x001f, all -> 0x001c }
            r2.f = r1     // Catch:{ IOException -> 0x001f, all -> 0x001c }
        L_0x001b:
            goto L_0x0020
        L_0x001c:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x001f:
            r0 = move-exception
        L_0x0020:
            monitor-exit(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.ro.e():void");
    }

    public void run() {
        ServerSocket serverSocket;
        this.f = f();
        if (cg.a(26)) {
            TrafficStats.setThreadStatsTag(40230);
        }
        if (this.f != null) {
            while (this.e) {
                synchronized (this) {
                    serverSocket = this.f;
                }
                if (serverSocket != null) {
                    Socket socket = null;
                    try {
                        Socket accept = serverSocket.accept();
                        if (cg.a(26)) {
                            TrafficStats.tagSocket(accept);
                        }
                        a(accept);
                        if (accept != null) {
                            try {
                                accept.close();
                            } catch (IOException e2) {
                            }
                        }
                    } catch (Throwable th) {
                        if (socket != null) {
                            try {
                                socket.close();
                            } catch (IOException e3) {
                            }
                        }
                        throw th;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public ServerSocket f() {
        Iterator<Integer> it = this.h.c.iterator();
        ServerSocket serverSocket = null;
        Integer num = null;
        while (serverSocket == null && it.hasNext()) {
            try {
                Integer next = it.next();
                if (next != null) {
                    try {
                        serverSocket = a(next.intValue());
                    } catch (SocketException e2) {
                        num = next;
                        a("port_already_in_use", num.intValue());
                    } catch (IOException e3) {
                        num = next;
                    }
                }
                num = next;
            } catch (SocketException e4) {
                a("port_already_in_use", num.intValue());
            } catch (IOException e5) {
            }
        }
        return serverSocket;
    }

    /* access modifiers changed from: package-private */
    public ServerSocket a(int i2) throws IOException {
        return new ServerSocket(i2);
    }

    private Map<String, Object> b(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("port", String.valueOf(i2));
        return hashMap;
    }

    private void a(@NonNull Socket socket) {
        new rm(socket, this, this.c).a();
    }

    public void a(@NonNull String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("uri", str2);
        c a2 = rh.a(this.d);
        a2.reportEvent("socket_" + str, hashMap);
    }

    public void a(@NonNull String str) {
        rh.a(this.d).reportEvent(b(str));
    }

    public void a(@NonNull String str, Throwable th) {
        rh.a(this.d).reportError(b(str), th);
    }

    public void a(@NonNull String str, int i2) {
        rh.a(this.d).reportEvent(b(str), b(i2));
    }

    private String b(@NonNull String str) {
        return "socket_" + str;
    }
}
