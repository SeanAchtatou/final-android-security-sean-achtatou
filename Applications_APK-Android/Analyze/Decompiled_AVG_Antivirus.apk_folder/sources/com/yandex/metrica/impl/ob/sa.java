package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.IIdentifierCallback;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class sa {
    private final Map<String, String> a = new HashMap();
    private List<String> b;
    private long c;
    private String d;
    private final kg e;

    public enum a {
        IDENTIFIERS,
        URLS,
        ALL
    }

    public sa(kg kgVar) {
        this.e = kgVar;
        a(IIdentifierCallback.YANDEX_MOBILE_METRICA_DEVICE_ID, this.e.b((String) null));
        a(IIdentifierCallback.APP_METRICA_DEVICE_ID_HASH, this.e.c((String) null));
        a(IIdentifierCallback.YANDEX_MOBILE_METRICA_UUID, this.e.a((String) null));
        a(IIdentifierCallback.YANDEX_MOBILE_METRICA_GET_AD_URL, this.e.d((String) null));
        a(IIdentifierCallback.YANDEX_MOBILE_METRICA_REPORT_AD_URL, this.e.e((String) null));
        this.b = this.e.b();
        this.c = this.e.a(0);
        this.d = this.e.f((String) null);
        e();
    }

    private void a(@NonNull String str, @Nullable String str2) {
        if (!TextUtils.isEmpty(str2)) {
            this.a.put(str, str2);
        }
    }

    private void c(String str) {
        if (TextUtils.isEmpty(this.a.get(IIdentifierCallback.YANDEX_MOBILE_METRICA_UUID))) {
            a(IIdentifierCallback.YANDEX_MOBILE_METRICA_UUID, str);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(@NonNull List<String> list, Map<String, String> map) {
        for (String next : list) {
            String str = this.a.get(next);
            if (str != null) {
                map.put(next, str);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean a(a aVar) {
        if (a.ALL == aVar) {
            return h();
        } else if (a.IDENTIFIERS == aVar) {
            return f();
        } else if (a.URLS != aVar) {
            return false;
        } else {
            return g();
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean a(@NonNull List<String> list) {
        for (String str : list) {
            if (TextUtils.isEmpty(this.a.get(str))) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(Bundle bundle) {
        d(bundle);
        e(bundle);
        c(bundle);
        b(bundle);
        e();
    }

    private void e() {
        this.e.g(this.a.get(IIdentifierCallback.YANDEX_MOBILE_METRICA_UUID)).h(this.a.get(IIdentifierCallback.YANDEX_MOBILE_METRICA_DEVICE_ID)).i(this.a.get(IIdentifierCallback.APP_METRICA_DEVICE_ID_HASH)).j(this.a.get(IIdentifierCallback.YANDEX_MOBILE_METRICA_GET_AD_URL)).k(this.a.get(IIdentifierCallback.YANDEX_MOBILE_METRICA_REPORT_AD_URL)).d(this.c).l(this.d).n();
    }

    private void b(Bundle bundle) {
        String string = bundle.getString("Clids");
        if (!TextUtils.isEmpty(string)) {
            this.d = string;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        this.e.e(j).n();
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        long b2 = ty.b() - this.e.b(0);
        return b2 > 86400 || b2 < 0;
    }

    /* access modifiers changed from: package-private */
    public List<String> b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void b(List<String> list) {
        this.b = list;
        this.e.a(this.b);
    }

    private void c(Bundle bundle) {
        b(bundle.getLong("ServerTimeOffset"));
    }

    private synchronized boolean f() {
        return !ce.a(this.a.get(IIdentifierCallback.YANDEX_MOBILE_METRICA_UUID), this.a.get(IIdentifierCallback.YANDEX_MOBILE_METRICA_DEVICE_ID), this.a.get(IIdentifierCallback.APP_METRICA_DEVICE_ID_HASH));
    }

    private synchronized boolean g() {
        return true;
    }

    private synchronized boolean h() {
        return f() && g();
    }

    private synchronized void d(Bundle bundle) {
        c(bundle.getString("Uuid"));
        String string = bundle.getString("DeviceId");
        if (!TextUtils.isEmpty(string)) {
            a(string);
        }
        b(bundle.getString("DeviceIdHash"));
    }

    private synchronized void e(Bundle bundle) {
        String string = bundle.getString("AdUrlGet");
        if (!TextUtils.isEmpty(string)) {
            d(string);
        }
        String string2 = bundle.getString("AdUrlReport");
        if (!TextUtils.isEmpty(string2)) {
            e(string2);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(String str) {
        this.a.put(IIdentifierCallback.YANDEX_MOBILE_METRICA_DEVICE_ID, str);
    }

    /* access modifiers changed from: package-private */
    public synchronized void b(@Nullable String str) {
        this.a.put(IIdentifierCallback.APP_METRICA_DEVICE_ID_HASH, str);
    }

    private synchronized void d(String str) {
        this.a.put(IIdentifierCallback.YANDEX_MOBILE_METRICA_GET_AD_URL, str);
    }

    private synchronized void e(String str) {
        this.a.put(IIdentifierCallback.YANDEX_MOBILE_METRICA_REPORT_AD_URL, str);
    }

    private synchronized void b(long j) {
        this.c = j;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.a.get(IIdentifierCallback.YANDEX_MOBILE_METRICA_UUID);
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.a.get(IIdentifierCallback.YANDEX_MOBILE_METRICA_DEVICE_ID);
    }
}
