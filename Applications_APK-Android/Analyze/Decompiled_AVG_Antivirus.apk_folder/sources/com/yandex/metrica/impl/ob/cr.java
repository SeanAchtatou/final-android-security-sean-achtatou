package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.cp;

public class cr<T extends cp> {
    @NonNull
    private final cq<T> a;
    @Nullable
    private final co<T> b;

    private cr(@NonNull a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
    }

    public void a(@NonNull cp cpVar) {
        this.a.a(cpVar);
    }

    /* access modifiers changed from: package-private */
    public final boolean b(@NonNull cp cpVar) {
        co<T> coVar = this.b;
        if (coVar == null) {
            return false;
        }
        return coVar.a(cpVar);
    }

    @NonNull
    public static <T extends cp> a<T> a(@NonNull cq cqVar) {
        return new a<>(cqVar);
    }

    public static final class a<T extends cp> {
        @NonNull
        final cq<T> a;
        @Nullable
        co<T> b;

        a(@NonNull cq<T> cqVar) {
            this.a = cqVar;
        }

        @NonNull
        public a<T> a(@NonNull co<T> coVar) {
            this.b = coVar;
            return this;
        }

        @NonNull
        public cr<T> a() {
            return new cr<>(this);
        }
    }
}
