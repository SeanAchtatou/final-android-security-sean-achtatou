package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.db;

public class es implements ek, em<di> {
    @NonNull
    public ej a(@NonNull Context context, @NonNull en enVar, @NonNull eh ehVar, @NonNull db dbVar) {
        return new er(context, (dx) enVar.a(new df(ehVar.c(), ehVar.b()), dbVar, new dy(this)));
    }

    @NonNull
    /* renamed from: c */
    public di a(@NonNull Context context, @NonNull df dfVar, @NonNull db.a aVar, @NonNull bb bbVar, @NonNull sd sdVar) {
        return new ed(context, dfVar, bbVar, aVar, af.a().h(), sdVar.e(), new sg(sdVar));
    }

    @NonNull
    /* renamed from: d */
    public eu b(@NonNull Context context, @NonNull df dfVar, @NonNull db.a aVar, @NonNull bb bbVar, @NonNull sd sdVar) {
        return new eu(context, dfVar, bbVar, aVar, sdVar.e(), new sg(sdVar));
    }
}
