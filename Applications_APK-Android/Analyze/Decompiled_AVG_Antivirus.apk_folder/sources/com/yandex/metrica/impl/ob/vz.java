package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Map;

public class vz implements vx<String> {
    private final Map<String, ?> a;

    public vz(@NonNull Map<String, ?> map) {
        this.a = map;
    }

    public vv a(@Nullable String str) {
        if (!this.a.containsKey(str)) {
            return vv.a(this);
        }
        return vv.a(this, String.format("Failed to activate AppMetrica with provided apiKey ApiKey %s has already been used by another reporter.", str));
    }
}
