package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.Cdo;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.dr;
import java.util.ArrayList;
import java.util.List;

public class dx<COMPONENT extends dr & Cdo> implements dk, dq, rz {
    @NonNull
    private final Context a;
    @NonNull
    private final df b;
    @NonNull
    private final bb c;
    @NonNull
    private final em<COMPONENT> d;
    @NonNull
    private final sd e;
    @NonNull
    private final ec f;
    @Nullable
    private COMPONENT g;
    @Nullable
    private dp h;
    private List<rz> i;
    @NonNull
    private final dg<ej> j;

    public dx(@NonNull Context context, @NonNull df dfVar, @NonNull db dbVar, @NonNull em<COMPONENT> emVar) {
        this(context, dfVar, dbVar, new ec(dbVar.b), emVar, new bc(), new dg(), ru.a());
    }

    public dx(@NonNull Context context, @NonNull df dfVar, @NonNull db dbVar, @NonNull ec ecVar, @NonNull em<COMPONENT> emVar, @NonNull bc bcVar, @NonNull dg<ej> dgVar, @NonNull ru ruVar) {
        this.i = new ArrayList();
        this.a = context;
        this.b = dfVar;
        this.f = ecVar;
        this.c = bcVar.a(this.a, this.b);
        this.d = emVar;
        this.j = dgVar;
        this.e = ruVar.a(this.a, a(), this, dbVar.a);
    }

    public void a(@NonNull t tVar, @NonNull db dbVar) {
        dr drVar;
        b();
        if (ab.d(tVar.g())) {
            drVar = e();
        } else {
            drVar = d();
        }
        if (!ab.a(tVar.g())) {
            a(dbVar.b);
        }
        drVar.a(tVar);
    }

    private void b() {
        e().a();
    }

    public synchronized void a(@NonNull db.a aVar) {
        this.f.a(aVar);
        if (this.h != null) {
            this.h.a(aVar);
        }
        if (this.g != null) {
            ((Cdo) this.g).a(aVar);
        }
    }

    public synchronized void a(@NonNull ej ejVar) {
        this.j.a(ejVar);
    }

    public synchronized void b(@NonNull ej ejVar) {
        this.j.b(ejVar);
    }

    private COMPONENT d() {
        if (this.g == null) {
            synchronized (this) {
                this.g = this.d.a(this.a, this.b, this.f.a(), this.c, this.e);
                this.i.add(this.g);
            }
        }
        return this.g;
    }

    private dp e() {
        if (this.h == null) {
            synchronized (this) {
                this.h = this.d.b(this.a, this.b, this.f.a(), this.c, this.e);
                this.i.add(this.h);
            }
        }
        return this.h;
    }

    @VisibleForTesting
    @NonNull
    public final df a() {
        return this.b;
    }

    public synchronized void a(@Nullable sc scVar) {
        for (rz a2 : this.i) {
            a2.a(scVar);
        }
    }

    public synchronized void a(@NonNull rw rwVar) {
        for (rz a2 : this.i) {
            a2.a(rwVar);
        }
    }

    public void c() {
        COMPONENT component = this.g;
        if (component != null) {
            ((dk) component).c();
        }
        dp dpVar = this.h;
        if (dpVar != null) {
            dpVar.c();
        }
    }

    public void a(@NonNull db dbVar) {
        this.e.a(dbVar.a);
        a(dbVar.b);
    }
}
