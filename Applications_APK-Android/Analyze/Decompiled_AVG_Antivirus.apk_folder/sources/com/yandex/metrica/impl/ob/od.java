package com.yandex.metrica.impl.ob;

import android.content.Context;
import java.util.Map;

@Deprecated
public class od extends oc {
    private static final oj d = new oj("UUID");
    private static final oj e = new oj("DEVICEID");
    private static final oj f = new oj("DEVICEID_2");
    private static final oj g = new oj("DEVICEID_3");
    private static final oj h = new oj("AD_URL_GET");
    private static final oj i = new oj("AD_URL_REPORT");
    private static final oj j = new oj("HOST_URL");
    private static final oj k = new oj("SERVER_TIME_OFFSET");
    private static final oj l = new oj("STARTUP_REQUEST_TIME");
    private static final oj m = new oj("CLIDS");
    private oj n = new oj(d.a());
    private oj o = new oj(e.a());
    private oj p = new oj(f.a());
    private oj q = new oj(g.a());
    private oj r = new oj(h.a());
    private oj s = new oj(i.a());
    private oj t = new oj(j.a());
    private oj u = new oj(k.a());
    private oj v = new oj(l.a());
    private oj w = new oj(m.a());

    public od(Context context) {
        super(context, null);
    }

    /* access modifiers changed from: protected */
    public String f() {
        return "_startupinfopreferences";
    }

    public String a(String str) {
        return this.c.getString(this.n.b(), str);
    }

    public String b(String str) {
        return this.c.getString(this.q.b(), str);
    }

    public String a() {
        return this.c.getString(this.p.b(), this.c.getString(this.o.b(), ""));
    }

    public String c(String str) {
        return this.c.getString(this.r.b(), str);
    }

    public String d(String str) {
        return this.c.getString(this.s.b(), str);
    }

    public long a(long j2) {
        return this.c.getLong(this.u.a(), j2);
    }

    public long b(long j2) {
        return this.c.getLong(this.v.b(), j2);
    }

    public String e(String str) {
        return this.c.getString(this.w.b(), str);
    }

    public od b() {
        return (od) h();
    }

    public Map<String, ?> c() {
        return this.c.getAll();
    }
}
