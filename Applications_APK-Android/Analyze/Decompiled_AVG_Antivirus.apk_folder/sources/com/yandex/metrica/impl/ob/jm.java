package com.yandex.metrica.impl.ob;

import android.util.SparseArray;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.kd;

public class jm {
    @NonNull
    private final SparseArray<kc> a = new SparseArray<>();
    @NonNull
    private final SparseArray<kc> b;
    @NonNull
    private final kc c;
    @NonNull
    private final kc d;
    @NonNull
    private final kc e;
    @NonNull
    private final kc f;
    @NonNull
    private final kc g;
    @NonNull
    private final kc h;

    public jm() {
        this.a.put(6, new kd.v());
        this.a.put(7, new kd.z());
        this.a.put(14, new kd.o());
        this.a.put(29, new kd.p());
        this.a.put(37, new kd.q());
        this.a.put(39, new kd.r());
        this.a.put(45, new kd.s());
        this.a.put(47, new kd.t());
        this.a.put(50, new kd.u());
        this.a.put(60, new kd.w());
        this.a.put(66, new kd.x());
        this.a.put(67, new kd.y());
        this.a.put(73, new kd.aa());
        this.a.put(77, new kd.ab());
        this.b = new SparseArray<>();
        this.b.put(12, new kd.g());
        this.b.put(29, new kd.h());
        this.b.put(47, new kd.i());
        this.b.put(50, new kd.j());
        this.b.put(55, new kd.k());
        this.b.put(60, new kd.l());
        this.b.put(63, new kd.m());
        this.b.put(67, new kd.n());
        this.c = new kd.c();
        this.d = new kd.d();
        this.e = new kd.a();
        this.f = new kd.b();
        this.g = new kd.e();
        this.h = new kd.f();
    }

    @NonNull
    public SparseArray<kc> a() {
        return this.a;
    }

    @NonNull
    public SparseArray<kc> b() {
        return this.b;
    }

    @NonNull
    public kc c() {
        return this.c;
    }

    @NonNull
    public kc d() {
        return this.d;
    }

    @NonNull
    public kc e() {
        return this.e;
    }

    @NonNull
    public kc f() {
        return this.f;
    }

    @NonNull
    public kc g() {
        return this.g;
    }

    @NonNull
    public kc h() {
        return this.h;
    }
}
