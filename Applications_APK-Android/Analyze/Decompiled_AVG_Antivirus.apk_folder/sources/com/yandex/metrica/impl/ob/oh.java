package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
public class oh extends oc {
    static final oj d = new oj("PREF_KEY_DEVICE_ID_");
    static final oj e = new oj("PREF_KEY_UID_");
    static final oj f = new oj("STARTUP_CLIDS_MATCH_WITH_APP_CLIDS_");
    static final oj g = new oj("PREF_KEY_PINNING_UPDATE_URL");
    private static final oj h = new oj("PREF_KEY_HOST_URL_");
    private static final oj i = new oj("PREF_KEY_REPORT_URL_");
    private static final oj j = new oj("PREF_KEY_GET_AD_URL");
    private static final oj k = new oj("PREF_KEY_REPORT_AD_URL");
    private static final oj l = new oj("PREF_KEY_STARTUP_OBTAIN_TIME_");
    private static final oj m = new oj("PREF_KEY_STARTUP_ENCODED_CLIDS_");
    private static final oj n = new oj("PREF_KEY_DISTRIBUTION_REFERRER_");
    private static final oj o = new oj("PREF_KEY_EASY_COLLECTING_ENABLED_");
    private oj p;
    private oj q;
    private oj r;
    private oj s;
    private oj t;
    private oj u;
    private oj v;
    private oj w;
    private oj x;
    private oj y;

    public oh(Context context) {
        this(context, null);
    }

    public oh(Context context, String str) {
        super(context, str);
        this.p = new oj(d.a());
        this.q = new oj(e.a(), i());
        this.r = new oj(h.a(), i());
        this.s = new oj(i.a(), i());
        this.t = new oj(j.a(), i());
        this.u = new oj(k.a(), i());
        this.v = new oj(l.a(), i());
        this.w = new oj(m.a(), i());
        this.x = new oj(n.a(), i());
        this.y = new oj(o.a(), i());
    }

    /* access modifiers changed from: protected */
    public String f() {
        return "_startupserviceinfopreferences";
    }

    public long a(long j2) {
        return this.c.getLong(this.v.b(), j2);
    }

    public String a(String str) {
        return this.c.getString(this.p.b(), str);
    }

    public String b(String str) {
        return this.c.getString(this.q.b(), str);
    }

    public String c(String str) {
        return this.c.getString(this.r.b(), str);
    }

    public String d(String str) {
        return this.c.getString(this.w.b(), str);
    }

    public String e(String str) {
        return this.c.getString(this.s.b(), str);
    }

    public String f(String str) {
        return this.c.getString(this.t.b(), str);
    }

    public String g(String str) {
        return this.c.getString(this.u.b(), str);
    }

    public String a() {
        return this.c.getString(this.x.a(), null);
    }

    public oh i(String str) {
        return (oh) a(this.q.b(), str);
    }

    public oh j(String str) {
        return (oh) a(this.p.b(), str);
    }

    public static void a(Context context) {
        ok.a(context, "_startupserviceinfopreferences").edit().remove(d.a()).apply();
    }

    public void b() {
        h(this.p.b()).h(this.q.b()).h(this.r.b()).h(this.s.b()).h(this.t.b()).h(this.u.b()).h(this.v.b()).h(this.y.b()).h(this.w.b()).h(this.x.a()).h(f.a()).h(g.a()).j();
    }
}
