package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.MetricaService;
import com.yandex.metrica.impl.ac.GoogleAdvertisingIdGetter;
import com.yandex.metrica.impl.ob.ax;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.lp;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.ro;
import java.io.File;
import java.util.concurrent.Executor;

public class aw implements au {
    public static final Executor a = af.a().j().b();
    private static final uv g = af.a().j().c();
    /* access modifiers changed from: private */
    @Nullable
    public sc b;
    /* access modifiers changed from: private */
    @NonNull
    public final Context c;
    @NonNull
    private final MetricaService.c d;
    /* access modifiers changed from: private */
    @NonNull
    public final ro.a e;
    /* access modifiers changed from: private */
    @Nullable
    public ro f;
    /* access modifiers changed from: private */
    @NonNull
    public ei h;
    @NonNull
    private final en i;
    @NonNull
    private final ax j;
    @Nullable
    private mx k;
    private jg l;

    public void a(@NonNull sc scVar) {
        this.b = scVar;
        d(scVar);
        c(scVar);
        af.a().a(scVar);
    }

    public aw(@NonNull Context context, @NonNull MetricaService.c cVar) {
        this(context, cVar, new en(context));
    }

    private aw(@NonNull Context context, @NonNull MetricaService.c cVar, @NonNull en enVar) {
        this(context, cVar, enVar, new ei(context, enVar), new ax(), new ro.a());
    }

    @VisibleForTesting
    aw(@NonNull Context context, @NonNull MetricaService.c cVar, @NonNull en enVar, @NonNull ei eiVar, @NonNull ax axVar, @NonNull ro.a aVar) {
        this.c = context;
        this.d = cVar;
        this.h = eiVar;
        this.i = enVar;
        this.j = axVar;
        this.e = aVar;
        this.l = new jg(this.c, new uc<File>() {
            public void a(File file) {
                aw.this.a(file);
            }
        });
    }

    public void a() {
        new by(this.c).a(this.c);
        tt.a().a(this.c);
        this.k = new mx(mo.a(this.c), af.a().k(), ch.a(this.c), new kj(jo.a(this.c).c()));
        c();
        cn.a().a(this, cx.class, cr.a(new cq<cx>() {
            public void a(cx cxVar) {
                aw.this.a(cxVar.b);
            }
        }).a(new co<cx>() {
            public boolean a(cx cxVar) {
                return !aw.this.c.getPackageName().equals(cxVar.a);
            }
        }).a());
        this.b = (sc) lp.a.a(sc.class).a(this.c).a();
        GoogleAdvertisingIdGetter.a().a(this.c, this.b);
        f();
        af.a().e().a();
        af.a().i().a();
        this.l.a();
    }

    private void c() {
        this.j.a(new ax.b() {
            public void a() {
                aw awVar = aw.this;
                awVar.c(awVar.b);
                aw.this.i();
            }
        });
        this.j.b(new ax.b() {
            public void a() {
                aw awVar = aw.this;
                awVar.c(awVar.b);
                aw.this.h();
            }
        });
        this.j.c(new ax.b() {
            public void a() {
                aw awVar = aw.this;
                awVar.c(awVar.b);
                aw.this.j();
                aw awVar2 = aw.this;
                ro unused = awVar2.f = awVar2.e.a(aw.this.c);
            }
        });
        this.j.d(new ax.b() {
            public void a() {
                aw.this.d();
            }
        });
        this.j.e(new ax.b() {
            public void a() {
                aw.this.e();
            }
        });
    }

    public void a(Intent intent, int i2) {
        b(intent, i2);
    }

    public void a(Intent intent, int i2, int i3) {
        b(intent, i3);
    }

    public void a(Intent intent) {
        this.j.a(intent);
    }

    public void b(Intent intent) {
        this.j.b(intent);
    }

    public void c(Intent intent) {
        this.j.c(intent);
        if (intent != null) {
            String action = intent.getAction();
            Uri data = intent.getData();
            String encodedAuthority = data == null ? null : data.getEncodedAuthority();
            if ("com.yandex.metrica.IMetricaService".equals(action)) {
                a(data, encodedAuthority);
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        ro roVar = this.f;
        if (roVar != null) {
            roVar.b();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        ro roVar = this.f;
        if (roVar != null) {
            roVar.a();
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(@Nullable Uri uri, @Nullable String str) {
        if (uri != null && uri.getPath().equals("/client")) {
            this.h.a(str, Integer.parseInt(uri.getQueryParameter("pid")), uri.getQueryParameter("psid"));
        }
        if (this.h.a() <= 0) {
            e();
        }
    }

    public void b() {
        this.l.b();
        h();
        this.i.c();
        cn.a().a(this);
    }

    public void a(String str, int i2, String str2, Bundle bundle) throws RemoteException {
        bundle.setClassLoader(CounterConfiguration.class.getClassLoader());
        a(new t(str2, str, i2), bundle);
    }

    public void a(Bundle bundle) throws RemoteException {
        bundle.setClassLoader(CounterConfiguration.class.getClassLoader());
        a(t.b(bundle), bundle);
    }

    public void a(@NonNull File file) {
        g.a(new is(this.c, file, new uc<jh>() {
            public void a(jh jhVar) {
                aw.this.a(eh.a(jhVar.g(), jhVar.h(), jhVar.e().intValue(), jhVar.f(), jhVar.j(), jhVar.i()), ab.a(jhVar.b(), jhVar.a(), jhVar.c(), jhVar.d(), ti.a(jhVar.h())), new db(new qq.a(), new db.a(), null));
            }
        }));
    }

    private void f() {
        sc scVar = this.b;
        if (scVar != null) {
            b(scVar);
            d(this.b);
        }
        c(this.b);
        g();
    }

    private void g() {
        cn.a().a(this, cx.class, cr.a(new cq<cx>() {
            public void a(cx cxVar) {
                aw.this.d(cxVar.b);
                aw.this.c(cxVar.b);
            }
        }).a(new co<cx>() {
            public boolean a(cx cxVar) {
                return !aw.this.c.getPackageName().equals(cxVar.a);
            }
        }).a());
    }

    /* access modifiers changed from: package-private */
    public void b(@NonNull sc scVar) {
        rt rtVar = scVar.r;
        if (rtVar == null) {
            cn.a().a((Class<? extends cp>) cw.class);
        } else {
            cn.a().b(new cw(rtVar));
        }
    }

    private void b(Intent intent, int i2) {
        if (intent != null) {
            intent.getExtras().setClassLoader(CounterConfiguration.class.getClassLoader());
            e(intent);
        }
        this.d.a(i2);
    }

    /* access modifiers changed from: private */
    public void h() {
        mx mxVar = this.k;
        if (mxVar != null) {
            mxVar.a(this);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        mx mxVar = this.k;
        if (mxVar != null) {
            mxVar.b(this);
        }
    }

    @VisibleForTesting
    final class a implements Runnable {
        private final t b;
        private final Bundle c;
        private final Context d;

        a(Context context, t tVar, Bundle bundle) {
            this.d = context.getApplicationContext();
            this.b = tVar;
            this.c = bundle;
        }

        public void run() {
            eh a2;
            cz czVar = new cz(this.c);
            if (!aw.this.a(czVar) && (a2 = eh.a(czVar)) != null) {
                db dbVar = new db(czVar);
                aw.this.h.a(a2, dbVar).a(this.b, dbVar);
            }
        }
    }

    private boolean d(Intent intent) {
        return intent == null || intent.getData() == null;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean a(@Nullable cz czVar) {
        return czVar == null || czVar.g() == null || !this.c.getPackageName().equals(czVar.g().h()) || czVar.g().g() != 81;
    }

    private void e(Intent intent) {
        if (!d(intent)) {
            cz czVar = new cz(intent.getExtras());
            if (!a(czVar)) {
                t b2 = t.b(intent.getExtras());
                if (!b2.m() && !b2.n()) {
                    try {
                        a(eh.a(czVar), b2, new db(czVar));
                    } catch (Throwable th) {
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(@NonNull eh ehVar, @NonNull t tVar, @NonNull db dbVar) {
        this.h.a(ehVar, dbVar).a(tVar, dbVar);
        this.h.a(ehVar.c(), ehVar.d().intValue(), ehVar.e());
    }

    private void a(t tVar, Bundle bundle) {
        if (!tVar.n()) {
            g.a(new a(this.c, tVar, bundle));
        }
    }

    /* access modifiers changed from: private */
    public void c(@NonNull sc scVar) {
        mx mxVar = this.k;
        if (mxVar != null) {
            mxVar.a(scVar, this.j.c());
        }
    }

    /* access modifiers changed from: private */
    public void d(@Nullable sc scVar) {
        if (scVar != null) {
            final hz hzVar = new hz(this.c, scVar);
            af.a().j().i().a(new Runnable() {
                public void run() {
                    hzVar.a();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.b != null) {
            af.a().f().a(this.b);
        }
    }
}
