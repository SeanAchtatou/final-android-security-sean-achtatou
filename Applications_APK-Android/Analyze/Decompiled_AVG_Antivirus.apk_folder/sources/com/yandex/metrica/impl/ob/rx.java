package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.impl.ob.sa;
import com.yandex.metrica.impl.ob.u;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

public class rx implements ry {
    static final Map<rw, IIdentifierCallback.Reason> a = Collections.unmodifiableMap(new HashMap<rw, IIdentifierCallback.Reason>() {
        {
            put(rw.UNKNOWN, IIdentifierCallback.Reason.UNKNOWN);
            put(rw.NETWORK, IIdentifierCallback.Reason.NETWORK);
            put(rw.PARSE, IIdentifierCallback.Reason.INVALID_RESPONSE);
        }
    });
    private final List<String> b;
    private final bt c;
    private final sa d;
    private final kg e;
    @NonNull
    private final Handler f;
    private final u.a g;
    private final Object h;
    private final Map<rp, List<String>> i;
    private Map<String, String> j;
    private final rp k;

    public static class a implements IIdentifierCallback {
        @NonNull
        private rx a;

        public a(@NonNull rx rxVar) {
            this.a = rxVar;
        }

        public void onReceive(Map<String, String> map) {
            this.a.e();
        }

        public void onRequestError(IIdentifierCallback.Reason reason) {
            this.a.e();
        }
    }

    public rx(bt btVar, kg kgVar, @NonNull Handler handler) {
        this(btVar, kgVar, new sa(kgVar), handler);
    }

    @VisibleForTesting
    rx(bt btVar, kg kgVar, @NonNull sa saVar, @NonNull Handler handler) {
        this.b = Arrays.asList(IIdentifierCallback.YANDEX_MOBILE_METRICA_UUID, IIdentifierCallback.YANDEX_MOBILE_METRICA_DEVICE_ID, IIdentifierCallback.APP_METRICA_DEVICE_ID_HASH, IIdentifierCallback.YANDEX_MOBILE_METRICA_GET_AD_URL, IIdentifierCallback.YANDEX_MOBILE_METRICA_REPORT_AD_URL);
        this.h = new Object();
        this.i = new WeakHashMap();
        this.k = new rp(new a(this));
        this.c = btVar;
        this.e = kgVar;
        this.d = saVar;
        this.f = handler;
        this.g = new u.a() {
            public void a(int i, Bundle bundle) {
            }
        };
        d();
    }

    public String a() {
        return this.d.c();
    }

    public String b() {
        return this.d.d();
    }

    public void a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        synchronized (this.h) {
            final rp rpVar = new rp(iIdentifierCallback);
            this.c.c();
            this.i.put(rpVar, list);
            this.i.put(this.k, this.b);
            if (!this.d.a(list)) {
                a(list, new u.a() {
                    public void a(int i, Bundle bundle) {
                        if (i == 1) {
                            rx.this.a(bundle);
                        } else if (i == 2) {
                            rx.this.a(rpVar, bundle);
                        }
                    }
                });
            } else if (!this.d.a(sa.a.ALL)) {
                b(list);
            }
        }
        d();
    }

    private void f() {
        b(this.b);
    }

    private void b(@NonNull List<String> list) {
        a(list, this.g);
    }

    private void a(@NonNull List<String> list, @NonNull u.a aVar) {
        u uVar = new u(this.f);
        uVar.a(aVar);
        this.c.a(list, uVar);
    }

    public void a(Bundle bundle) {
        synchronized (this.h) {
            this.d.a(bundle);
            this.d.a(ty.b());
        }
        d();
    }

    public void c() {
        if (!this.d.a(sa.a.ALL) || this.d.a()) {
            f();
        }
    }

    public void a(List<String> list) {
        List<String> b2 = this.d.b();
        if (cg.a((Collection) list)) {
            if (!cg.a((Collection) b2)) {
                this.d.b((List<String>) null);
                this.c.a((List<String>) null);
            }
        } else if (!cg.a(list, b2)) {
            this.d.b(list);
            this.c.a(list);
        } else {
            this.c.a(b2);
        }
    }

    public void a(Map<String, String> map) {
        Map<String, String> c2 = tu.c(map);
        this.j = c2;
        this.c.a(c2);
    }

    public void a(String str) {
        this.c.b(str);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final void d() {
        WeakHashMap weakHashMap = new WeakHashMap();
        synchronized (this.h) {
            for (Map.Entry next : this.i.entrySet()) {
                if (this.d.a((List) next.getValue())) {
                    weakHashMap.put(next.getKey(), next.getValue());
                }
            }
            for (Map.Entry entry : weakHashMap.entrySet()) {
                rp rpVar = (rp) entry.getKey();
                if (rpVar != null) {
                    rpVar.a(c((List) entry.getValue()));
                    this.i.remove(rpVar);
                }
            }
        }
        weakHashMap.clear();
    }

    @Nullable
    private Map<String, String> c(@Nullable List<String> list) {
        if (list == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        this.d.a(list, hashMap);
        return hashMap;
    }

    public void a(@NonNull rp rpVar, Bundle bundle) {
        a(rpVar, (IIdentifierCallback.Reason) cg.a(a, rw.b(bundle), IIdentifierCallback.Reason.UNKNOWN));
    }

    private void a(@NonNull rp rpVar, IIdentifierCallback.Reason reason) {
        synchronized (this.h) {
            rpVar.a(reason);
            this.i.remove(rpVar);
        }
    }

    public void e() {
        this.c.d();
        this.i.remove(this.k);
    }
}
