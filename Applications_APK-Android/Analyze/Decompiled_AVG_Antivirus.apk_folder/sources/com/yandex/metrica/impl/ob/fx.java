package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class fx extends ft {
    private kf a;
    private tt b;

    public fx(di diVar) {
        this(diVar, diVar.t(), tt.a());
    }

    @VisibleForTesting
    fx(di diVar, kf kfVar, tt ttVar) {
        super(diVar);
        this.a = kfVar;
        this.b = ttVar;
    }

    public boolean a(@NonNull t tVar) {
        di a2 = a();
        qp h = a2.h();
        if (this.a.d()) {
            return false;
        }
        if (!this.a.c()) {
            if (h.P()) {
                this.b.c();
            }
            String e = tVar.e();
            this.a.c(e);
            a2.d().b(t.e(tVar).c(e));
            this.a.a(h.Q());
            a().y().a();
        }
        this.a.b();
        return false;
    }
}
