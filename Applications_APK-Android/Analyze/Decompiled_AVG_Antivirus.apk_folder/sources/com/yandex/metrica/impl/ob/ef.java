package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ef {
    @NonNull
    private final kh a;
    @NonNull
    private final tx b;
    @Nullable
    private se c;
    private long d;

    public ef(@NonNull Context context, @NonNull df dfVar) {
        this(new kh(jo.a(context).b(dfVar)), new tw());
    }

    public ef(@NonNull kh khVar, @NonNull tx txVar) {
        this.a = khVar;
        this.b = txVar;
        this.d = this.a.k();
    }

    public boolean a(@Nullable Boolean bool) {
        return Boolean.FALSE.equals(bool) && this.c != null && this.b.a() - this.d > this.c.a;
    }

    public void a() {
        this.d = this.b.a();
        this.a.f(this.d).n();
    }

    public void a(@Nullable se seVar) {
        this.c = seVar;
    }
}
