package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.i;
import java.util.LinkedList;
import java.util.List;

public class dh {
    private final di a;
    private final oa b;
    private List<g> c;

    public static class a {
        @NonNull
        private final di a;

        public a(@NonNull di diVar) {
            this.a = diVar;
        }

        /* access modifiers changed from: package-private */
        public dh a(@NonNull oa oaVar) {
            return new dh(this.a, oaVar);
        }
    }

    private dh(di diVar, oa oaVar) {
        this.a = diVar;
        this.b = oaVar;
        b();
    }

    private void b() {
        this.c = new LinkedList();
        this.c.add(new c(this.a, this.b));
        this.c.add(new e(this.a, this.b));
        List<g> list = this.c;
        di diVar = this.a;
        list.add(new d(diVar, diVar.u()));
        this.c.add(new b(this.a));
        this.c.add(new f(this.a));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (!a(this.a.b().a())) {
            for (g f2 : this.c) {
                f2.f();
            }
        }
    }

    private boolean a(String str) {
        return oa.a.values().contains(str);
    }

    static class f extends g {
        @Deprecated
        static final oj a = new oj("SESSION_SLEEP_START");
        @Deprecated
        static final oj b = new oj("SESSION_ID");
        @Deprecated
        static final oj c = new oj("SESSION_COUNTER_ID");
        @Deprecated
        static final oj d = new oj("SESSION_INIT_TIME");
        @Deprecated
        static final oj e = new oj("SESSION_IS_ALIVE_REPORT_NEEDED");
        @Deprecated
        static final oj f = new oj("BG_SESSION_ID");
        @Deprecated
        static final oj g = new oj("BG_SESSION_SLEEP_START");
        @Deprecated
        static final oj h = new oj("BG_SESSION_COUNTER_ID");
        @Deprecated
        static final oj i = new oj("BG_SESSION_INIT_TIME");
        @Deprecated
        static final oj j = new oj("BG_SESSION_IS_ALIVE_REPORT_NEEDED");
        private final kh k;

        f(di diVar) {
            super(diVar);
            this.k = diVar.x();
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return true;
        }

        /* access modifiers changed from: protected */
        public void b() {
            d();
            c();
            g();
        }

        private void g() {
            this.k.p(a.b());
            this.k.p(b.b());
            this.k.p(c.b());
            this.k.p(d.b());
            this.k.p(e.b());
            this.k.p(f.b());
            this.k.p(g.b());
            this.k.p(h.b());
            this.k.p(i.b());
            this.k.p(j.b());
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
         arg types: [java.lang.String, int]
         candidates:
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean */
        /* access modifiers changed from: package-private */
        @VisibleForTesting
        public void c() {
            long b2 = this.k.b(a.b(), -2147483648L);
            if (b2 != -2147483648L) {
                hv hvVar = new hv(this.k, "foreground");
                if (!hvVar.i()) {
                    if (b2 != 0) {
                        hvVar.b(b2);
                    }
                    long b3 = this.k.b(b.b(), -1L);
                    if (-1 != b3) {
                        hvVar.d(b3);
                    }
                    boolean b4 = this.k.b(e.b(), true);
                    if (b4) {
                        hvVar.a(b4);
                    }
                    long b5 = this.k.b(d.b(), Long.MIN_VALUE);
                    if (b5 != Long.MIN_VALUE) {
                        hvVar.e(b5);
                    }
                    long b6 = this.k.b(c.b(), 0L);
                    if (b6 != 0) {
                        hvVar.a(b6);
                    }
                    hvVar.h();
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
         arg types: [java.lang.String, int]
         candidates:
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, int):int
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, long):long
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, java.lang.String):T
          com.yandex.metrica.impl.ob.ki.b(java.lang.String, boolean):boolean */
        /* access modifiers changed from: package-private */
        @VisibleForTesting
        public void d() {
            long b2 = this.k.b(g.b(), -2147483648L);
            if (b2 != -2147483648L) {
                hv hvVar = new hv(this.k, "background");
                if (!hvVar.i()) {
                    if (b2 != 0) {
                        hvVar.b(b2);
                    }
                    long b3 = this.k.b(f.b(), -1L);
                    if (b3 != -1) {
                        hvVar.d(b3);
                    }
                    boolean b4 = this.k.b(j.b(), true);
                    if (b4) {
                        hvVar.a(b4);
                    }
                    long b5 = this.k.b(i.b(), Long.MIN_VALUE);
                    if (b5 != Long.MIN_VALUE) {
                        hvVar.e(b5);
                    }
                    long b6 = this.k.b(h.b(), 0L);
                    if (b6 != 0) {
                        hvVar.a(b6);
                    }
                    hvVar.h();
                }
            }
        }
    }

    static class b extends g {
        private final oe a;
        private final kh b;
        private final kj c;

        b(di diVar) {
            super(diVar);
            this.a = new oe(diVar.j(), diVar.b().toString());
            this.b = diVar.x();
            this.c = diVar.a;
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return this.a.e();
        }

        /* access modifiers changed from: protected */
        public void b() {
            d();
            c();
            g();
            this.a.g();
        }

        private void g() {
            i.a a2 = this.a.a();
            if (a2 != null) {
                this.b.a(a2);
            }
            String a3 = this.a.a((String) null);
            if (!TextUtils.isEmpty(a3) && TextUtils.isEmpty(this.b.f())) {
                this.b.a(a3);
            }
            long c2 = this.a.c(Long.MIN_VALUE);
            if (c2 != Long.MIN_VALUE && this.b.a(Long.MIN_VALUE) == Long.MIN_VALUE) {
                this.b.b(c2);
            }
            this.b.n();
        }

        /* access modifiers changed from: package-private */
        @VisibleForTesting
        public void c() {
            hv hvVar = new hv(this.b, "foreground");
            if (!hvVar.i()) {
                long d = this.a.d(-1);
                if (-1 != d) {
                    hvVar.d(d);
                }
                boolean booleanValue = this.a.a(true).booleanValue();
                if (booleanValue) {
                    hvVar.a(booleanValue);
                }
                long a2 = this.a.a(Long.MIN_VALUE);
                if (a2 != Long.MIN_VALUE) {
                    hvVar.e(a2);
                }
                long f = this.a.f(0);
                if (f != 0) {
                    hvVar.a(f);
                }
                long h = this.a.h(0);
                if (h != 0) {
                    hvVar.b(h);
                }
                hvVar.h();
            }
        }

        /* access modifiers changed from: package-private */
        @VisibleForTesting
        public void d() {
            hv hvVar = new hv(this.b, "background");
            if (!hvVar.i()) {
                long e = this.a.e(-1);
                if (e != -1) {
                    hvVar.d(e);
                }
                long b2 = this.a.b(Long.MIN_VALUE);
                if (b2 != Long.MIN_VALUE) {
                    hvVar.e(b2);
                }
                long g = this.a.g(0);
                if (g != 0) {
                    hvVar.a(g);
                }
                long i = this.a.i(0);
                if (i != 0) {
                    hvVar.b(i);
                }
                hvVar.h();
            }
        }
    }

    static class d extends g {
        private final ob a;
        private final kf b;

        d(di diVar, ob obVar) {
            super(diVar);
            this.a = obVar;
            this.b = diVar.t();
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return "DONE".equals(this.a.c(null)) || "DONE".equals(this.a.b(null));
        }

        /* access modifiers changed from: protected */
        public void b() {
            if ("DONE".equals(this.a.c(null))) {
                this.b.b();
            }
            String e = this.a.e(null);
            if (!TextUtils.isEmpty(e)) {
                this.b.c(e);
            }
            if ("DONE".equals(this.a.b(null))) {
                this.b.a();
            }
            this.a.d();
            this.a.e();
            this.a.c();
        }
    }

    static class e extends h {
        e(di diVar, oa oaVar) {
            super(diVar, oaVar);
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return e().t().a(null) == null;
        }

        /* access modifiers changed from: protected */
        public void b() {
            oa c = c();
            if (e() instanceof du) {
                c.c();
            } else {
                c.b();
            }
        }
    }

    static class c extends h {
        c(di diVar, oa oaVar) {
            super(diVar, oaVar);
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return e() instanceof du;
        }

        /* access modifiers changed from: protected */
        public void b() {
            c().a();
        }
    }

    private static abstract class h extends g {
        private oa a;

        h(di diVar, oa oaVar) {
            super(diVar);
            this.a = oaVar;
        }

        public oa c() {
            return this.a;
        }
    }

    private static abstract class g {
        private final di a;

        /* access modifiers changed from: protected */
        public abstract boolean a();

        /* access modifiers changed from: protected */
        public abstract void b();

        g(di diVar) {
            this.a = diVar;
        }

        /* access modifiers changed from: package-private */
        public di e() {
            return this.a;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            if (a()) {
                b();
            }
        }
    }
}
