package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.ab;

public class um {
    @NonNull
    private final tn<un, ul> a;
    @NonNull
    private final tn<ab.a, ul> b;

    public um(@NonNull Context context) {
        this(new uj(), new uo(), new ug(context));
    }

    public um(@NonNull ul ulVar, @NonNull ul ulVar2, @NonNull ul ulVar3) {
        this.a = new tn<>(ulVar);
        this.a.a(un.NONE, ulVar);
        this.a.a(un.EXTERNALLY_ENCRYPTED_EVENT_CRYPTER, ulVar2);
        this.a.a(un.AES_VALUE_ENCRYPTION, ulVar3);
        this.b = new tn<>(ulVar);
        this.b.a(ab.a.EVENT_TYPE_IDENTITY, ulVar3);
    }

    @NonNull
    public ul a(un unVar) {
        return this.a.a(unVar);
    }

    @NonNull
    public ul a(@NonNull t tVar) {
        return a(ab.a.a(tVar.g()));
    }

    @NonNull
    private ul a(@NonNull ab.a aVar) {
        return this.b.a(aVar);
    }
}
