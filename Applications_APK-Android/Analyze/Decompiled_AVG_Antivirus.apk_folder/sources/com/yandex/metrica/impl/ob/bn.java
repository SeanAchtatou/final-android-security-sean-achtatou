package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import java.util.HashMap;
import java.util.Map;

public class bn {
    private final boolean a;
    /* access modifiers changed from: private */
    public final bt b;
    /* access modifiers changed from: private */
    public final kg c;
    private String d;
    private Map<String, String> e;
    private DeferredDeeplinkParametersListener f;

    public bn(bt btVar, kg kgVar, @NonNull uv uvVar) {
        this(btVar, kgVar, new qh(btVar.b()), uvVar);
    }

    @VisibleForTesting
    bn(bt btVar, kg kgVar, @NonNull final qh qhVar, @NonNull uv uvVar) {
        this.b = btVar;
        this.c = kgVar;
        this.d = kgVar.c();
        this.a = kgVar.d();
        if (this.a) {
            this.c.n(null);
            this.d = null;
        } else {
            e(b(this.d));
        }
        if (!this.c.e()) {
            uvVar.a(new Runnable() {
                public void run() {
                    qhVar.a(new qg() {
                        public void a(@NonNull qf qfVar) {
                            bn.this.b.a(qfVar);
                            bn.this.d(qfVar.a);
                            a();
                        }

                        public void a(@NonNull Throwable th) {
                            bn.this.b.a((qf) null);
                            a();
                        }

                        private void a() {
                            bn.this.c.h();
                        }
                    });
                }
            });
        }
    }

    public void a(String str) {
        this.b.a(str);
        d(str);
    }

    /* access modifiers changed from: private */
    public void d(@Nullable String str) {
        if (!(this.a || TextUtils.isEmpty(str)) && TextUtils.isEmpty(this.d)) {
            synchronized (this) {
                this.d = str;
                this.c.n(this.d);
                e(b(str));
                a();
            }
        }
    }

    private void e(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.e = c(str);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final String b(String str) {
        return f(str).get("appmetrica_deep_link");
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final Map<String, String> c(String str) {
        Map<String, String> f2 = f(Uri.decode(str));
        HashMap hashMap = new HashMap(f2.size());
        for (Map.Entry next : f2.entrySet()) {
            hashMap.put(Uri.decode((String) next.getKey()), Uri.decode((String) next.getValue()));
        }
        return hashMap;
    }

    private static Map<String, String> f(String str) {
        HashMap hashMap = new HashMap();
        if (str != null) {
            String g = g(str);
            if (h(g)) {
                for (String str2 : g.split("&")) {
                    int indexOf = str2.indexOf("=");
                    if (indexOf >= 0) {
                        hashMap.put(str2.substring(0, indexOf), str2.substring(indexOf + 1));
                    } else {
                        hashMap.put(str2, "");
                    }
                }
            }
        }
        return hashMap;
    }

    private static String g(String str) {
        int lastIndexOf = str.lastIndexOf(63);
        if (lastIndexOf >= 0) {
            return str.substring(lastIndexOf + 1);
        }
        return str;
    }

    private static boolean h(String str) {
        return str.contains("=");
    }

    private void a() {
        if (!cg.a((Map) this.e)) {
            DeferredDeeplinkParametersListener deferredDeeplinkParametersListener = this.f;
            if (deferredDeeplinkParametersListener != null) {
                deferredDeeplinkParametersListener.onParametersLoaded(this.e);
                this.f = null;
            }
        } else if (this.d != null) {
            a(DeferredDeeplinkParametersListener.Error.PARSE_ERROR);
        }
    }

    private void a(DeferredDeeplinkParametersListener.Error error) {
        DeferredDeeplinkParametersListener deferredDeeplinkParametersListener = this.f;
        if (deferredDeeplinkParametersListener != null) {
            deferredDeeplinkParametersListener.onError(error, this.d);
            this.f = null;
        }
    }

    public synchronized void a(DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        try {
            this.f = deferredDeeplinkParametersListener;
            if (this.a) {
                a(DeferredDeeplinkParametersListener.Error.NOT_A_FIRST_LAUNCH);
            } else {
                a();
            }
            this.c.g();
        } catch (Throwable th) {
            this.c.g();
            throw th;
        }
    }
}
