package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pf;

public class jf implements ky<iy, pf.c> {
    @NonNull
    private je a = new je();
    @NonNull
    private jd b = new jd();
    @NonNull
    private iz c = new iz(this.b);

    @NonNull
    /* renamed from: a */
    public pf.c b(@NonNull iy iyVar) {
        pf.c cVar = new pf.c();
        if (iyVar.b != null) {
            cVar.b = this.a.b(iyVar.b);
        }
        if (iyVar.c != null) {
            cVar.c = this.c.b(iyVar.c);
        }
        return cVar;
    }

    @NonNull
    public iy a(@NonNull pf.c cVar) {
        throw new UnsupportedOperationException();
    }
}
