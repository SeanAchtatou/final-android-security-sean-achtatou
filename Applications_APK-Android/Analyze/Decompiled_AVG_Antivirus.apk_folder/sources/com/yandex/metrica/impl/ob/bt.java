package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.util.Base64;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.IMetricaService;
import com.yandex.metrica.impl.ac.NativeCrashesHelper;
import com.yandex.metrica.impl.ob.ab;
import com.yandex.metrica.impl.ob.bu;
import com.yandex.metrica.impl.ob.pk;
import com.yandex.metrica.impl.ob.t;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class bt implements ai {
    private final Context a;
    @NonNull
    private final kg b;
    private at c;
    private final NativeCrashesHelper d;
    private final uv e;
    private ap f;
    @NonNull
    private final r g;
    private ry h;
    private jf i = new jf();
    private final bu j;

    bt(da daVar, uv uvVar, Context context, uv uvVar2, @NonNull kg kgVar) {
        this.b = kgVar;
        this.c = new at(context, uvVar2);
        this.e = uvVar;
        this.a = context;
        this.d = new NativeCrashesHelper(context);
        this.g = new r(daVar);
        this.j = new bu(this);
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable ap apVar) {
        this.f = apVar;
    }

    /* access modifiers changed from: package-private */
    public void a(ry ryVar) {
        this.h = ryVar;
        this.g.b(ryVar);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z, bp bpVar) {
        this.d.a(z);
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable Boolean bool, @Nullable Boolean bool2) {
        if (cg.a(bool)) {
            this.g.h().a(bool.booleanValue());
        }
        if (cg.a(bool2)) {
            this.g.h().g(bool2.booleanValue());
        }
        a(t.t(), this.g);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, bp bpVar, boolean z) {
        a(ab.a(ab.a.EVENT_TYPE_NATIVE_CRASH, str, c(bpVar)), bpVar);
    }

    public void a(String str, boolean z) {
        a(str, this.f.d(), z);
    }

    public void c() {
        this.c.g();
    }

    public void d() {
        this.c.h();
    }

    /* access modifiers changed from: private */
    public t b(t tVar, bp bpVar) {
        if (tVar.g() == ab.a.EVENT_TYPE_EXCEPTION_USER.a() || tVar.g() == ab.a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF.a()) {
            tVar.e(bpVar.e());
        }
        return tVar;
    }

    /* access modifiers changed from: package-private */
    public void a(t tVar, bp bpVar) {
        a(b(tVar, bpVar), bpVar, (Map<String, Object>) null);
    }

    public Future<Void> a(t tVar, final bp bpVar, final Map<String, Object> map) {
        this.c.d();
        bu.d dVar = new bu.d(tVar, bpVar);
        if (!cg.a((Map) map)) {
            dVar.a(new bu.c() {
                public t a(t tVar) {
                    return bt.this.b(tVar.c(th.a(map)), bpVar);
                }
            });
        }
        return a(dVar);
    }

    public void a(@NonNull List<String> list, @NonNull ResultReceiver resultReceiver) {
        a(ab.a(ab.a.EVENT_TYPE_STARTUP, ti.a()).a(new t.a(list, resultReceiver)), this.g);
    }

    public void a(String str) {
        a(ab.d(str, ti.a()), this.g);
    }

    public void a(@Nullable qf qfVar) {
        a(ab.a(qfVar, ti.a()), this.g);
    }

    public void a(bp bpVar) {
        a(ab.a(bpVar.f(), c(bpVar)), bpVar);
    }

    public void a(List<String> list) {
        this.g.g().a(list);
    }

    public void a(Map<String, String> map) {
        this.g.g().a(map);
    }

    public void b(String str) {
        this.g.g().a(str);
    }

    /* access modifiers changed from: package-private */
    public void a(@NonNull iy iyVar, bp bpVar) {
        this.c.d();
        o b2 = ab.b(iyVar.a, e.a(this.i.b(iyVar)), c(bpVar));
        b2.e(bpVar.e());
        try {
            a(new bu.d(b2, bpVar).a(b2.a()).a(true)).get();
        } catch (InterruptedException | ExecutionException e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(m mVar) {
        this.c.d();
    }

    /* access modifiers changed from: package-private */
    public void b(m mVar) {
        this.c.c();
    }

    public void a(IMetricaService iMetricaService, t tVar, bp bpVar) throws RemoteException {
        if (this.d.a()) {
            this.d.a(this, this.e, this.b);
        }
        b(iMetricaService, tVar, bpVar);
        e();
    }

    public void a(String str, String str2, bp bpVar) {
        a(new bu.d(o.a(str, str2), bpVar));
    }

    public void b(bp bpVar) {
        a(new bu.d(o.b(), bpVar));
    }

    /* access modifiers changed from: package-private */
    public void a(@NonNull final pk.a aVar, @NonNull bp bpVar) {
        a(new bu.d(o.c(), bpVar).a(new bu.c() {
            public t a(t tVar) {
                return tVar.c(new String(Base64.encode(e.a(aVar), 0)));
            }
        }));
    }

    /* access modifiers changed from: package-private */
    public void a(@Nullable final String str, @NonNull bp bpVar) {
        a(new bu.d(o.a(str, c(bpVar)), bpVar).a(new bu.c() {
            public t a(t tVar) {
                return tVar.c(str);
            }
        }));
    }

    /* access modifiers changed from: package-private */
    public void a(@NonNull final bv bvVar, @NonNull bp bpVar) {
        a(new bu.d(o.a(c(bpVar)), bpVar).a(new bu.c() {
            public t a(t tVar) {
                Pair<byte[], Integer> a2 = bvVar.a();
                return tVar.c(new String(Base64.encode((byte[]) a2.first, 0))).c(((Integer) a2.second).intValue());
            }
        }));
    }

    private void e() {
        ap apVar = this.f;
        if (apVar == null || apVar.e()) {
            this.c.c();
        }
    }

    private static void b(IMetricaService iMetricaService, t tVar, bp bpVar) throws RemoteException {
        iMetricaService.reportData(tVar.a(bpVar.b()));
    }

    private Future<Void> a(bu.d dVar) {
        dVar.a().a(this.h);
        return this.j.a(dVar);
    }

    public at a() {
        return this.c;
    }

    public Context b() {
        return this.a;
    }

    @NonNull
    private tp c(@NonNull bp bpVar) {
        return ti.a(bpVar.h().e());
    }
}
