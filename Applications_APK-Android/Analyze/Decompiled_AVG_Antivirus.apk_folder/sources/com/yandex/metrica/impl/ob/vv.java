package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

public final class vv {
    private final Class<? extends vx> a;
    private final boolean b;
    private final String c;

    private vv(@NonNull vx<?> vxVar, boolean z, @NonNull String str) {
        this.a = vxVar.getClass();
        this.b = z;
        this.c = str;
    }

    public final boolean a() {
        return this.b;
    }

    @NonNull
    public final String b() {
        return this.c;
    }

    public static final vv a(@NonNull vx<?> vxVar) {
        return new vv(vxVar, true, "");
    }

    public static final vv a(@NonNull vx<?> vxVar, @NonNull String str) {
        return new vv(vxVar, false, str);
    }
}
