package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.browser.crashreports.a;
import com.yandex.metrica.a;
import com.yandex.metrica.g;
import java.util.concurrent.atomic.AtomicBoolean;

public class ap extends m implements as {
    private static final vx<String> f = new vt(new vr("Deeplink"));
    private static final vx<String> g = new vt(new vr("Referral url"));
    private boolean h;
    @NonNull
    private final a i;
    @NonNull
    private final qc j;
    @NonNull
    private final g k;
    @NonNull
    private final rx l;
    @NonNull
    private com.yandex.browser.crashreports.a m;
    private final AtomicBoolean n;
    /* access modifiers changed from: private */
    public final cm o;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ap(@androidx.annotation.NonNull android.content.Context r13, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.da r14, @androidx.annotation.NonNull com.yandex.metrica.g r15, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.bt r16, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.rx r17, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.br r18, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.br r19) {
        /*
            r12 = this;
            r2 = r15
            com.yandex.metrica.impl.ob.bp r4 = new com.yandex.metrica.impl.ob.bp
            com.yandex.metrica.CounterConfiguration r0 = new com.yandex.metrica.CounterConfiguration
            r0.<init>(r15)
            r1 = r14
            r4.<init>(r14, r0)
            com.yandex.metrica.a r5 = new com.yandex.metrica.a
            java.lang.Integer r0 = r2.sessionTimeout
            if (r0 != 0) goto L_0x001b
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS
            r6 = 10
            long r0 = r0.toMillis(r6)
            goto L_0x0022
        L_0x001b:
            java.lang.Integer r0 = r2.sessionTimeout
            int r0 = r0.intValue()
            long r0 = (long) r0
        L_0x0022:
            r5.<init>(r0)
            com.yandex.metrica.impl.ob.qc r6 = new com.yandex.metrica.impl.ob.qc
            r1 = r13
            r6.<init>(r13)
            com.yandex.metrica.impl.ob.an r8 = new com.yandex.metrica.impl.ob.an
            r8.<init>()
            com.yandex.metrica.impl.ob.uv r11 = com.yandex.metrica.impl.ob.cj.a()
            r0 = r12
            r2 = r15
            r3 = r16
            r7 = r17
            r9 = r18
            r10 = r19
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.ap.<init>(android.content.Context, com.yandex.metrica.impl.ob.da, com.yandex.metrica.g, com.yandex.metrica.impl.ob.bt, com.yandex.metrica.impl.ob.rx, com.yandex.metrica.impl.ob.br, com.yandex.metrica.impl.ob.br):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
     arg types: [java.lang.Boolean, int]
     candidates:
      com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
      com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
      com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
      com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
      com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
      com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ap(@NonNull Context context, @NonNull g gVar, @NonNull bt btVar, @NonNull bp bpVar, @NonNull a aVar, @NonNull qc qcVar, @NonNull rx rxVar, @NonNull an anVar, @NonNull br brVar, @NonNull br brVar2, @NonNull uv uvVar) {
        super(context, btVar, bpVar);
        g gVar2 = gVar;
        this.h = true;
        this.n = new AtomicBoolean(false);
        this.o = new cm();
        this.b.a(new bk(gVar2.preloadInfo, this.c));
        this.h = ua.a(gVar2.crashReporting, true);
        this.i = aVar;
        this.j = qcVar;
        this.k = gVar2;
        this.l = rxVar;
        qc qcVar2 = this.j;
        g gVar3 = this.k;
        qcVar2.a(aVar, gVar3, gVar3.m, rxVar.b(), this.c);
        final uv uvVar2 = uvVar;
        final an anVar2 = anVar;
        final br brVar3 = brVar;
        final br brVar4 = brVar2;
        this.m = new com.yandex.browser.crashreports.a(new a.C0000a() {
            public void a() {
                final iu a2 = ap.this.o.a();
                uvVar2.a(new Runnable() {
                    public void run() {
                        ap.this.a(a2);
                        if (anVar2.a(a2.a.f)) {
                            brVar3.a().a(a2);
                        }
                        if (anVar2.b(a2.a.f)) {
                            brVar4.a().a(a2);
                        }
                    }
                });
            }
        });
        if (Boolean.TRUE.equals(gVar2.l)) {
            f();
        }
    }

    public void reportError(String message, Throwable error) {
        super.reportError(message, error);
    }

    public final void f() {
        if (this.n.compareAndSet(false, true)) {
            this.m.a();
        }
    }

    public void a(Activity activity) {
        if (activity != null) {
            if (activity.getIntent() != null) {
                String dataString = activity.getIntent().getDataString();
                if (!TextUtils.isEmpty(dataString)) {
                    this.e.a(ab.e(dataString, this.c), this.b);
                }
                g(dataString);
            }
        } else if (this.c.c()) {
            this.c.b("Null activity parameter for reportAppOpen(Activity)");
        }
    }

    private void g(@Nullable String str) {
        if (this.c.c()) {
            this.c.a("App opened " + " via deeplink: " + d(str));
        }
    }

    public void e(String str) {
        f.a(str);
        this.e.a(ab.e(str, this.c), this.b);
        g(str);
    }

    public void f(String str) {
        g.a(str);
        this.e.a(ab.f(str, this.c), this.b);
        h(str);
    }

    private void h(@Nullable String str) {
        if (this.c.c()) {
            this.c.a("Referral URL received: " + d(str));
        }
    }

    public void a(Application application) {
        if (Build.VERSION.SDK_INT >= 14) {
            if (this.c.c()) {
                this.c.a("Enable activity auto tracking");
            }
            b(application);
        } else if (this.c.c()) {
            this.c.b("Could not enable activity auto tracking. API level should be more than 14 (ICE_CREAM_SANDWICH)");
        }
    }

    @TargetApi(14)
    private void b(Application application) {
        application.registerActivityLifecycleCallbacks(new w(this));
    }

    public void b(Activity activity) {
        a(d(activity));
        this.i.a();
    }

    public void c(Activity activity) {
        b(d(activity));
        this.i.b();
    }

    /* access modifiers changed from: package-private */
    public String d(Activity activity) {
        if (activity != null) {
            return activity.getClass().getSimpleName();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(g gVar, boolean z) {
        a(gVar.nativeCrashReporting);
        if (z) {
            b();
        }
        b(gVar.i);
        a(gVar.h);
    }

    public void b(boolean z) {
        this.h = z;
        if (this.c.c()) {
            this.c.a("Set report crashes enabled: %b", Boolean.valueOf(z));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean
     arg types: [java.lang.Boolean, int]
     candidates:
      com.yandex.metrica.impl.ob.ua.a(java.lang.Float, float):float
      com.yandex.metrica.impl.ob.ua.a(java.lang.Integer, int):int
      com.yandex.metrica.impl.ob.ua.a(java.lang.Long, long):long
      com.yandex.metrica.impl.ob.ua.a(java.lang.Object, java.lang.Object):T
      com.yandex.metrica.impl.ob.ua.a(java.lang.String, java.lang.String):java.lang.String
      com.yandex.metrica.impl.ob.ua.a(java.lang.Boolean, boolean):boolean */
    public void a(@Nullable Boolean bool) {
        this.e.a(ua.a(bool, true), this.b);
        if (this.c.c()) {
            this.c.a("Set report native crashes enabled: %b", bool);
        }
    }

    public void a(Location location) {
        this.b.h().a(location);
        if (this.c.c()) {
            tp tpVar = this.c;
            tpVar.a("Set location: %s" + location.toString(), new Object[0]);
        }
    }

    public void a(boolean z) {
        this.b.h().a(z);
    }

    public boolean g() {
        return this.h;
    }
}
