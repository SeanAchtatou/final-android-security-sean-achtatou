package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.util.SparseArray;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.yandex.metrica.YandexMetrica;
import java.util.List;
import java.util.Locale;

public final class kb {
    public static final Boolean a = false;
    public static final int b = YandexMetrica.getLibraryApiLevel();
    public static final SparseArray<kc> c = e.a();
    public static final SparseArray<kc> d = e.b();
    private static final jm e = new jm();
    private static final jp f = new jp();
    private static final jl g = new jl(e, f);

    public interface a {
        public static final List<String> a = cg.a("incremental_id", "timestamp", "data");

        /* renamed from: com.yandex.metrica.impl.ob.kb$a$a  reason: collision with other inner class name */
        public interface C0003a {
            public static final String a = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s (incremental_id INTEGER NOT NULL,timestamp INTEGER, data TEXT)", "lbs_dat");
            public static final String b = String.format(Locale.US, "DROP TABLE IF EXISTS %s", "lbs_dat");
        }

        public interface b {
            public static final String a = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s (incremental_id INTEGER NOT NULL,timestamp INTEGER, data TEXT)", "l_dat");
            public static final String b = String.format(Locale.US, "DROP TABLE IF EXISTS %s", "l_dat");
        }
    }

    public static final class b {
        public static final List<String> a = cg.a("data_key", "value");
    }

    public interface c {
        public static final List<String> a = cg.a("key", "value", "type");
    }

    public static final class d implements c {
    }

    public static final class f {
        public static final List<String> a = cg.a("id", "number", "global_number", "number_of_type", "name", "value", "type", "time", "session_id", "wifi_network_info", "cell_info", "location_info", "error_environment", "user_info", "session_type", "app_environment", "app_environment_revision", "truncated", UrlManager.Parameter.CONNECTION_TYPE, "cellular_connection_type", "custom_type", "wifi_access_point", "encrypting_mode", "profile_id", "first_occurrence_status");
        public static final String b = ("CREATE TABLE IF NOT EXISTS reports (id INTEGER PRIMARY KEY,name TEXT,value TEXT,number INTEGER,global_number INTEGER,number_of_type INTEGER,type INTEGER,time INTEGER,session_id TEXT,wifi_network_info TEXT DEFAULT '',cell_info TEXT DEFAULT '',location_info TEXT DEFAULT '',error_environment TEXT,user_info TEXT,session_type INTEGER DEFAULT " + hw.FOREGROUND.a() + "," + "app_environment" + " TEXT DEFAULT '" + "{}" + "'," + "app_environment_revision" + " INTEGER DEFAULT " + 0L + "," + "truncated" + " INTEGER DEFAULT 0," + UrlManager.Parameter.CONNECTION_TYPE + " INTEGER DEFAULT " + 2 + "," + "cellular_connection_type" + " TEXT," + "custom_type" + " INTEGER DEFAULT 0, " + "wifi_access_point" + " TEXT, " + "encrypting_mode" + " INTEGER DEFAULT " + un.NONE.a() + ", " + "profile_id" + " TEXT, " + "first_occurrence_status" + " INTEGER DEFAULT " + ad.UNKNOWN.d + " )");
    }

    public static final class g {
        public static final List<String> a = cg.a("id", "start_time", "network_info", "report_request_parameters", "server_time_offset", "type", "obtained_before_first_sync");
        public static final String b = ("CREATE TABLE IF NOT EXISTS sessions (id INTEGER,start_time INTEGER,network_info TEXT,report_request_parameters TEXT,server_time_offset INTEGER,type INTEGER DEFAULT " + hw.FOREGROUND.a() + "," + "obtained_before_first_sync" + " INTEGER DEFAULT 0 )");
        public static final String c = String.format(Locale.US, "SELECT DISTINCT %s  FROM %s WHERE %s >=0 AND (SELECT count() FROM %5$s WHERE %5$s.%6$s = %2$s.%3$s AND %5$s.%7$s = %2$s.%4$s) > 0 ORDER BY %3$s LIMIT 1", "report_request_parameters", "sessions", "id", "type", "reports", "session_id", "session_type");
        public static final String d = String.format(Locale.US, "(select count(%s.%s) from %s where %s.%s = %s.%s) = 0 and %s NOT IN (%s)", "reports", "id", "reports", "reports", "session_id", "sessions", "id", "id", tc.a(2));
    }

    public static final class h implements c {
    }

    public static final class e {
        public static ContentValues a() {
            ContentValues contentValues = new ContentValues();
            contentValues.put("API_LEVEL", Integer.valueOf(YandexMetrica.getLibraryApiLevel()));
            return contentValues;
        }
    }

    public static jl a() {
        return g;
    }
}
