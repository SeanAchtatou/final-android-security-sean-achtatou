package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.core.app.NotificationCompat;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class y implements bd {
    @NonNull
    private final a a;

    static class a {
        a() {
        }

        /* renamed from: com.yandex.metrica.impl.ob.y$a$a  reason: collision with other inner class name */
        static class C0019a {
            @NonNull
            final String a;

            public C0019a(@NonNull String str) {
                this.a = str;
            }
        }

        public C0019a a(@Nullable byte[] bArr) {
            try {
                if (!cg.a(bArr)) {
                    return new C0019a(new JSONObject(new String(bArr, "UTF-8")).optString(NotificationCompat.CATEGORY_STATUS));
                }
                return null;
            } catch (Throwable th) {
                return null;
            }
        }
    }

    public y() {
        this(new a());
    }

    @VisibleForTesting
    y(@NonNull a aVar) {
        this.a = aVar;
    }

    public boolean a(int i, @Nullable byte[] bArr, @Nullable Map<String, List<String>> map) {
        a.C0019a a2;
        if (200 != i || (a2 = this.a.a(bArr)) == null) {
            return false;
        }
        return "accepted".equals(a2.a);
    }
}
