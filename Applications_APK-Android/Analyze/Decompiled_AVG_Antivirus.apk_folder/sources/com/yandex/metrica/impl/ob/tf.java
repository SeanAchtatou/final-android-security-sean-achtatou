package com.yandex.metrica.impl.ob;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Locale;
import java.util.UUID;

public class tf {
    @Nullable
    public String a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return ce.a(ag.c(str.getBytes()));
    }

    @NonNull
    public String a() {
        return UUID.randomUUID().toString().replace("-", "").toLowerCase(Locale.US);
    }
}
