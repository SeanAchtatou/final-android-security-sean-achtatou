package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.WorkerThread;
import java.io.Closeable;

public class jz implements jx {
    private final Context a;
    private final String b;
    @NonNull
    private final jl c;
    @NonNull
    private final jy d;
    private jn e;

    public jz(Context context, String str) {
        this(context, str, new jy(context, str), kb.a());
    }

    @VisibleForTesting
    public jz(@NonNull Context context, @NonNull String str, @NonNull jy jyVar, @NonNull jl jlVar) {
        this.a = context;
        this.b = str;
        this.d = jyVar;
        this.c = jlVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    @androidx.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.database.sqlite.SQLiteDatabase a() {
        /*
            r4 = this;
            monitor-enter(r4)
            com.yandex.metrica.impl.ob.jy r0 = r4.d     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            r0.a()     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            com.yandex.metrica.impl.ob.jn r0 = new com.yandex.metrica.impl.ob.jn     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            android.content.Context r1 = r4.a     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            java.lang.String r2 = r4.b     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            com.yandex.metrica.impl.ob.jl r3 = r4.c     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            com.yandex.metrica.impl.ob.ju r3 = r3.c()     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            r0.<init>(r1, r2, r3)     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            r4.e = r0     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            com.yandex.metrica.impl.ob.jn r0 = r4.e     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Throwable -> 0x0022, all -> 0x001f }
            monitor-exit(r4)
            return r0
        L_0x001f:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0022:
            r0 = move-exception
            r0 = 0
            monitor-exit(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.jz.a():android.database.sqlite.SQLiteDatabase");
    }

    @WorkerThread
    public synchronized void a(SQLiteDatabase sQLiteDatabase) {
        cg.b(sQLiteDatabase);
        cg.a((Closeable) this.e);
        this.d.b();
        this.e = null;
    }
}
