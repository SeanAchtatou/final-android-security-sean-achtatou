package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pg;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;

public class mz {
    @NonNull
    public pg.b.a a(@NonNull mn mnVar) {
        pg.b.a aVar = new pg.b.a();
        aVar.b = mnVar.a() == null ? aVar.b : mnVar.a().longValue();
        aVar.c = TimeUnit.MILLISECONDS.toSeconds(mnVar.b());
        aVar.f = TimeUnit.MILLISECONDS.toSeconds(mnVar.e());
        JSONArray d = mnVar.d();
        if (d != null) {
            aVar.d = bl.b(d);
        }
        JSONArray c = mnVar.c();
        if (c != null) {
            aVar.e = bl.a(c);
        }
        return aVar;
    }
}
