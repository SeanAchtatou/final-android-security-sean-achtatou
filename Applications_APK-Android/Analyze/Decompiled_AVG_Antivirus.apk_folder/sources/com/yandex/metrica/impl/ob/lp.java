package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import com.yandex.metrica.impl.ob.pq;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public interface lp<T> {
    kp<T> a(@NonNull Context context);

    public static class a {
        private final HashMap<Class<?>, lp<?>> a;
        private final lp<sc> b;
        private final lp<pq.a> c;
        private final lp<List<nt>> d;
        private final lp<ra> e;

        /* renamed from: com.yandex.metrica.impl.ob.lp$a$a  reason: collision with other inner class name */
        private static final class C0004a {
            static final a a = new a();
        }

        public static <T> lp<T> a(Class<T> cls) {
            return C0004a.a.c(cls);
        }

        public static <T> lp<Collection<T>> b(Class<T> cls) {
            return C0004a.a.d(cls);
        }

        private a() {
            this.a = new HashMap<>();
            this.b = new lp<sc>() {
                public kp<sc> a(@NonNull Context context) {
                    return new kq("startup_state", jo.a(context).b(), new lo(context).a(), new lm());
                }
            };
            this.c = new lp<pq.a>() {
                public kp<pq.a> a(@NonNull Context context) {
                    return new kq("provided_request_state", jo.a(context).b(), new lo(context).d(), new lh());
                }
            };
            this.d = new lp<List<nt>>() {
                public kp<List<nt>> a(@NonNull Context context) {
                    return new kq("permission_list", jo.a(context).b(), new lo(context).b(), new lf());
                }
            };
            this.e = new lp<ra>() {
                public kp<ra> a(@NonNull Context context) {
                    return new kq("sdk_fingerprinting", jo.a(context).b(), new lo(context).c(), new lk());
                }
            };
            this.a.put(sc.class, this.b);
            this.a.put(pq.a.class, this.c);
            this.a.put(nt.class, this.d);
            this.a.put(ra.class, this.e);
        }

        /* access modifiers changed from: package-private */
        public <T> lp<T> c(Class<T> cls) {
            return this.a.get(cls);
        }

        /* access modifiers changed from: package-private */
        public <T> lp<Collection<T>> d(Class<T> cls) {
            return this.a.get(cls);
        }
    }
}
