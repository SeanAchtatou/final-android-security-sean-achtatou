package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.dh;
import com.yandex.metrica.impl.ob.ea;
import com.yandex.metrica.impl.ob.hs;
import com.yandex.metrica.impl.ob.qp;
import java.util.List;

class dj {
    @NonNull
    private final a a;
    @NonNull
    private final b b;
    @NonNull
    private final Context c;
    @NonNull
    private final df d;
    @NonNull
    private final db.a e;
    @NonNull
    private final sf f;
    @NonNull
    private final sc g;
    @NonNull
    private final qp.d h;
    @NonNull
    private final um i;
    @NonNull
    private final bb j;
    @NonNull
    private final uv k;
    private final int l;

    static class a {
        @Nullable
        private final String a;

        a(@Nullable String str) {
            this.a = str;
        }

        /* access modifiers changed from: package-private */
        public tp a() {
            return ti.a(this.a);
        }

        /* access modifiers changed from: package-private */
        public tg b() {
            return ti.b(this.a);
        }
    }

    static class b {
        @NonNull
        private final df a;
        @NonNull
        private final jo b;

        b(@NonNull Context context, @NonNull df dfVar) {
            this(dfVar, jo.a(context));
        }

        @VisibleForTesting
        b(@NonNull df dfVar, @NonNull jo joVar) {
            this.a = dfVar;
            this.b = joVar;
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public kf a() {
            return new kf(this.b.b(this.a));
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public kh b() {
            return new kh(this.b.b(this.a));
        }

        /* access modifiers changed from: package-private */
        @NonNull
        public kj c() {
            return new kj(this.b.c());
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    dj(@androidx.annotation.NonNull android.content.Context r14, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.df r15, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.db.a r16, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.sf r17, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.sc r18, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.qp.d r19, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.bb r20, @androidx.annotation.NonNull com.yandex.metrica.impl.ob.uv r21, int r22) {
        /*
            r13 = this;
            r1 = r14
            com.yandex.metrica.impl.ob.um r9 = new com.yandex.metrica.impl.ob.um
            r9.<init>(r14)
            com.yandex.metrica.impl.ob.dj$a r11 = new com.yandex.metrica.impl.ob.dj$a
            r3 = r16
            java.lang.String r0 = r3.d
            r11.<init>(r0)
            com.yandex.metrica.impl.ob.dj$b r12 = new com.yandex.metrica.impl.ob.dj$b
            r2 = r15
            r12.<init>(r14, r15)
            r0 = r13
            r4 = r17
            r5 = r18
            r6 = r19
            r7 = r20
            r8 = r21
            r10 = r22
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yandex.metrica.impl.ob.dj.<init>(android.content.Context, com.yandex.metrica.impl.ob.df, com.yandex.metrica.impl.ob.db$a, com.yandex.metrica.impl.ob.sf, com.yandex.metrica.impl.ob.sc, com.yandex.metrica.impl.ob.qp$d, com.yandex.metrica.impl.ob.bb, com.yandex.metrica.impl.ob.uv, int):void");
    }

    @VisibleForTesting
    dj(@NonNull Context context, @NonNull df dfVar, @NonNull db.a aVar, @NonNull sf sfVar, @NonNull sc scVar, @NonNull qp.d dVar, @NonNull bb bbVar, @NonNull uv uvVar, @NonNull um umVar, int i2, @NonNull a aVar2, @NonNull b bVar) {
        this.c = context;
        this.d = dfVar;
        this.e = aVar;
        this.f = sfVar;
        this.g = scVar;
        this.h = dVar;
        this.j = bbVar;
        this.k = uvVar;
        this.i = umVar;
        this.l = i2;
        this.a = aVar2;
        this.b = bVar;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public a a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public b b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public hd c() {
        return new hd(this.c, this.d, this.l);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public jk a(@NonNull di diVar) {
        return new jk(diVar, jo.a(this.c).a(this.d));
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public bs<di> b(@NonNull di diVar) {
        return new bs<>(diVar, this.f.a(), this.j, this.k);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public dz c(@NonNull di diVar) {
        return new dz(new qp.c(diVar, this.h), this.g, new qp.a(this.e));
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public hs a(@NonNull di diVar, @NonNull kh khVar, @NonNull hs.a aVar) {
        return new hs(diVar, new hr(khVar), aVar);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public ea a(@NonNull kh khVar, @NonNull hs hsVar, @NonNull jk jkVar, @NonNull i iVar, @NonNull final bs bsVar) {
        return new ea(khVar, hsVar, jkVar, iVar, this.i, this.l, new ea.a() {
            public void a() {
                bsVar.b();
            }
        });
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public ff d(@NonNull di diVar) {
        return new ff(diVar);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public fi<ft, di> a(@NonNull di diVar, @NonNull ff ffVar) {
        return new fi<>(ffVar, diVar);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public dh.a e(@NonNull di diVar) {
        return new dh.a(diVar);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public lu a(@NonNull jk jkVar) {
        return new lu(jkVar);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public lz a(@NonNull jk jkVar, @NonNull dz dzVar) {
        return new lz(jkVar, dzVar);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public lx a(@NonNull List<lv> list, @NonNull ly lyVar) {
        return new lx(list, lyVar);
    }
}
