package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.lang.Thread;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

class az implements Thread.UncaughtExceptionHandler {
    public static final AtomicBoolean a = new AtomicBoolean();
    private final CopyOnWriteArrayList<iv> b = new CopyOnWriteArrayList<>();
    private final Thread.UncaughtExceptionHandler c;
    private final cm d = new cm();

    public az(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.c = uncaughtExceptionHandler;
    }

    public void uncaughtException(Thread thread, Throwable ex) {
        try {
            a.set(true);
            a(new iy(ex, new iu(new ck().a(thread), this.d.a(thread))));
        } finally {
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.c;
            if (uncaughtExceptionHandler != null) {
                uncaughtExceptionHandler.uncaughtException(thread, ex);
            }
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void a(@NonNull iy iyVar) {
        Iterator<iv> it = this.b.iterator();
        while (it.hasNext()) {
            it.next().a(iyVar);
        }
    }

    public void a(iv ivVar) {
        this.b.add(ivVar);
    }

    public void b(iv ivVar) {
        this.b.remove(ivVar);
    }
}
