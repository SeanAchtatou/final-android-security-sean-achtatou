package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class sv extends sl {
    private int a;
    private sl b;

    public sv(@NonNull Context context, @NonNull uv uvVar) {
        this(context.getApplicationContext(), new vp(), uvVar);
    }

    @VisibleForTesting
    sv(Context context, @NonNull vp vpVar, @NonNull uv uvVar) {
        if (vpVar.b(context, "android.hardware.telephony")) {
            this.b = new sp(context, uvVar);
        } else {
            this.b = new sq();
        }
    }

    public synchronized void a() {
        this.a++;
        if (this.a == 1) {
            this.b.a();
        }
    }

    public synchronized void b() {
        this.a--;
        if (this.a == 0) {
            this.b.b();
        }
    }

    public synchronized void a(sy syVar) {
        this.b.a(syVar);
    }

    public synchronized void a(sn snVar) {
        this.b.a(snVar);
    }

    public void a(boolean z) {
        this.b.a(z);
    }

    public void a(@NonNull sc scVar) {
        this.b.a(scVar);
    }
}
