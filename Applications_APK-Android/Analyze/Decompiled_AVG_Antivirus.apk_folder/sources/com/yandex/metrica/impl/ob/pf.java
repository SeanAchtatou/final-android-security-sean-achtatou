package com.yandex.metrica.impl.ob;

import java.io.IOException;

public interface pf {

    public static final class d extends e {
        private static volatile d[] g;
        public String b;
        public String c;
        public int d;
        public String e;
        public boolean f;

        public static d[] d() {
            if (g == null) {
                synchronized (c.a) {
                    if (g == null) {
                        g = new d[0];
                    }
                }
            }
            return g;
        }

        public d() {
            e();
        }

        public d e() {
            this.b = "";
            this.c = "";
            this.d = 0;
            this.e = "";
            this.f = false;
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            bVar.a(1, this.b);
            if (!this.c.equals("")) {
                bVar.a(2, this.c);
            }
            bVar.c(3, this.d);
            bVar.a(4, this.e);
            bVar.a(5, this.f);
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c() + b.b(1, this.b);
            if (!this.c.equals("")) {
                c2 += b.b(2, this.c);
            }
            return c2 + b.f(3, this.d) + b.b(4, this.e) + b.b(5, this.f);
        }

        /* renamed from: b */
        public d a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    this.b = aVar.i();
                } else if (a == 18) {
                    this.c = aVar.i();
                } else if (a == 24) {
                    this.d = aVar.l();
                } else if (a == 34) {
                    this.e = aVar.i();
                } else if (a == 40) {
                    this.f = aVar.h();
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }
    }

    public static final class e extends e {
        private static volatile e[] h;
        public String b;
        public int c;
        public long d;
        public String e;
        public int f;
        public d[] g;

        public static e[] d() {
            if (h == null) {
                synchronized (c.a) {
                    if (h == null) {
                        h = new e[0];
                    }
                }
            }
            return h;
        }

        public e() {
            e();
        }

        public e e() {
            this.b = "";
            this.c = 0;
            this.d = 0;
            this.e = "";
            this.f = 0;
            this.g = d.d();
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            bVar.a(1, this.b);
            bVar.c(2, this.c);
            bVar.c(3, this.d);
            if (!this.e.equals("")) {
                bVar.a(4, this.e);
            }
            int i = this.f;
            if (i != 0) {
                bVar.b(5, i);
            }
            d[] dVarArr = this.g;
            if (dVarArr != null && dVarArr.length > 0) {
                int i2 = 0;
                while (true) {
                    d[] dVarArr2 = this.g;
                    if (i2 >= dVarArr2.length) {
                        break;
                    }
                    d dVar = dVarArr2[i2];
                    if (dVar != null) {
                        bVar.a(6, dVar);
                    }
                    i2++;
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c() + b.b(1, this.b) + b.f(2, this.c) + b.f(3, this.d);
            if (!this.e.equals("")) {
                c2 += b.b(4, this.e);
            }
            int i = this.f;
            if (i != 0) {
                c2 += b.e(5, i);
            }
            d[] dVarArr = this.g;
            if (dVarArr != null && dVarArr.length > 0) {
                int i2 = 0;
                while (true) {
                    d[] dVarArr2 = this.g;
                    if (i2 >= dVarArr2.length) {
                        break;
                    }
                    d dVar = dVarArr2[i2];
                    if (dVar != null) {
                        c2 += b.b(6, dVar);
                    }
                    i2++;
                }
            }
            return c2;
        }

        /* renamed from: b */
        public e a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    this.b = aVar.i();
                } else if (a == 16) {
                    this.c = aVar.l();
                } else if (a == 24) {
                    this.d = aVar.m();
                } else if (a == 34) {
                    this.e = aVar.i();
                } else if (a == 40) {
                    this.f = aVar.k();
                } else if (a == 50) {
                    int b2 = g.b(aVar, 50);
                    d[] dVarArr = this.g;
                    int length = dVarArr == null ? 0 : dVarArr.length;
                    d[] dVarArr2 = new d[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.g, 0, dVarArr2, 0, length);
                    }
                    while (length < dVarArr2.length - 1) {
                        dVarArr2[length] = new d();
                        aVar.a(dVarArr2[length]);
                        aVar.a();
                        length++;
                    }
                    dVarArr2[length] = new d();
                    aVar.a(dVarArr2[length]);
                    this.g = dVarArr2;
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }
    }

    public static final class a extends e {
        public e b;
        public e[] c;

        public a() {
            d();
        }

        public a d() {
            this.b = null;
            this.c = e.d();
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            e eVar = this.b;
            if (eVar != null) {
                bVar.a(1, eVar);
            }
            e[] eVarArr = this.c;
            if (eVarArr != null && eVarArr.length > 0) {
                int i = 0;
                while (true) {
                    e[] eVarArr2 = this.c;
                    if (i >= eVarArr2.length) {
                        break;
                    }
                    e eVar2 = eVarArr2[i];
                    if (eVar2 != null) {
                        bVar.a(2, eVar2);
                    }
                    i++;
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            e eVar = this.b;
            if (eVar != null) {
                c2 += b.b(1, eVar);
            }
            e[] eVarArr = this.c;
            if (eVarArr != null && eVarArr.length > 0) {
                int i = 0;
                while (true) {
                    e[] eVarArr2 = this.c;
                    if (i >= eVarArr2.length) {
                        break;
                    }
                    e eVar2 = eVarArr2[i];
                    if (eVar2 != null) {
                        c2 += b.b(2, eVar2);
                    }
                    i++;
                }
            }
            return c2;
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    if (this.b == null) {
                        this.b = new e();
                    }
                    aVar.a(this.b);
                } else if (a == 18) {
                    int b2 = g.b(aVar, 18);
                    e[] eVarArr = this.c;
                    int length = eVarArr == null ? 0 : eVarArr.length;
                    e[] eVarArr2 = new e[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.c, 0, eVarArr2, 0, length);
                    }
                    while (length < eVarArr2.length - 1) {
                        eVarArr2[length] = new e();
                        aVar.a(eVarArr2[length]);
                        aVar.a();
                        length++;
                    }
                    eVarArr2[length] = new e();
                    aVar.a(eVarArr2[length]);
                    this.c = eVarArr2;
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }
    }

    public static final class b extends e {
        public a b;

        public b() {
            d();
        }

        public b d() {
            this.b = null;
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            a aVar = this.b;
            if (aVar != null) {
                bVar.a(1, aVar);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c = super.c();
            a aVar = this.b;
            if (aVar != null) {
                return c + b.b(1, aVar);
            }
            return c;
        }

        /* renamed from: b */
        public b a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    if (this.b == null) {
                        this.b = new a();
                    }
                    aVar.a(this.b);
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }
    }

    public static final class f extends e {
        public String b;
        public String c;
        public d[] d;
        public f e;

        public f() {
            d();
        }

        public f d() {
            this.b = "";
            this.c = "";
            this.d = d.d();
            this.e = null;
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            bVar.a(1, this.b);
            if (!this.c.equals("")) {
                bVar.a(2, this.c);
            }
            d[] dVarArr = this.d;
            if (dVarArr != null && dVarArr.length > 0) {
                int i = 0;
                while (true) {
                    d[] dVarArr2 = this.d;
                    if (i >= dVarArr2.length) {
                        break;
                    }
                    d dVar = dVarArr2[i];
                    if (dVar != null) {
                        bVar.a(3, dVar);
                    }
                    i++;
                }
            }
            f fVar = this.e;
            if (fVar != null) {
                bVar.a(4, fVar);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c() + b.b(1, this.b);
            if (!this.c.equals("")) {
                c2 += b.b(2, this.c);
            }
            d[] dVarArr = this.d;
            if (dVarArr != null && dVarArr.length > 0) {
                int i = 0;
                while (true) {
                    d[] dVarArr2 = this.d;
                    if (i >= dVarArr2.length) {
                        break;
                    }
                    d dVar = dVarArr2[i];
                    if (dVar != null) {
                        c2 += b.b(3, dVar);
                    }
                    i++;
                }
            }
            f fVar = this.e;
            if (fVar != null) {
                return c2 + b.b(4, fVar);
            }
            return c2;
        }

        /* renamed from: b */
        public f a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    this.b = aVar.i();
                } else if (a == 18) {
                    this.c = aVar.i();
                } else if (a == 26) {
                    int b2 = g.b(aVar, 26);
                    d[] dVarArr = this.d;
                    int length = dVarArr == null ? 0 : dVarArr.length;
                    d[] dVarArr2 = new d[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.d, 0, dVarArr2, 0, length);
                    }
                    while (length < dVarArr2.length - 1) {
                        dVarArr2[length] = new d();
                        aVar.a(dVarArr2[length]);
                        aVar.a();
                        length++;
                    }
                    dVarArr2[length] = new d();
                    aVar.a(dVarArr2[length]);
                    this.d = dVarArr2;
                } else if (a == 34) {
                    if (this.e == null) {
                        this.e = new f();
                    }
                    aVar.a(this.e);
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }
    }

    public static final class c extends e {
        public f b;
        public a c;

        public c() {
            d();
        }

        public c d() {
            this.b = null;
            this.c = null;
            this.a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            f fVar = this.b;
            if (fVar != null) {
                bVar.a(1, fVar);
            }
            a aVar = this.c;
            if (aVar != null) {
                bVar.a(2, aVar);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            f fVar = this.b;
            if (fVar != null) {
                c2 += b.b(1, fVar);
            }
            a aVar = this.c;
            if (aVar != null) {
                return c2 + b.b(2, aVar);
            }
            return c2;
        }

        /* renamed from: b */
        public c a(a aVar) throws IOException {
            while (true) {
                int a = aVar.a();
                if (a == 0) {
                    return this;
                }
                if (a == 10) {
                    if (this.b == null) {
                        this.b = new f();
                    }
                    aVar.a(this.b);
                } else if (a == 18) {
                    if (this.c == null) {
                        this.c = new a();
                    }
                    aVar.a(this.c);
                } else if (!g.a(aVar, a)) {
                    return this;
                }
            }
        }
    }
}
