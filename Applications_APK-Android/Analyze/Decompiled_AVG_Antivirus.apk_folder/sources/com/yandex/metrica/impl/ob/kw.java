package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.pp;
import java.util.List;

public class kw implements lc<mc, pp.a.C0014a> {
    @NonNull
    private final la a;
    @NonNull
    private final lb b;

    public kw() {
        this(new la(), new lb());
    }

    @NonNull
    /* renamed from: a */
    public pp.a.C0014a b(@NonNull mc mcVar) {
        pp.a.C0014a.C0015a[] aVarArr;
        pp.a.C0014a aVar = new pp.a.C0014a();
        aVar.b = this.a.b((mh) mcVar);
        aVar.d = mcVar.b;
        aVar.c = mcVar.a;
        aVar.e = mcVar.c;
        if (mcVar.d == null) {
            aVarArr = new pp.a.C0014a.C0015a[0];
        } else {
            aVarArr = this.b.a(mcVar.d);
        }
        aVar.f = aVarArr;
        return aVar;
    }

    @NonNull
    public mc a(@NonNull pp.a.C0014a aVar) {
        List<mj> list;
        pp.a.C0014a aVar2 = aVar;
        mh a2 = this.a.a(aVar2.b);
        long j = a2.e;
        float f = a2.f;
        int i = a2.g;
        int i2 = a2.h;
        long j2 = a2.i;
        int i3 = a2.j;
        boolean z = a2.k;
        long j3 = aVar2.c;
        long j4 = aVar2.d;
        long j5 = a2.l;
        boolean z2 = a2.m;
        boolean z3 = aVar2.e;
        if (cg.a((Object[]) aVar2.f)) {
            list = null;
        } else {
            list = this.b.a(aVar2.f);
        }
        return new mc(j, f, i, i2, j2, i3, z, j3, j4, j5, z2, z3, list);
    }

    @VisibleForTesting
    kw(@NonNull la laVar, @NonNull lb lbVar) {
        this.a = laVar;
        this.b = lbVar;
    }
}
