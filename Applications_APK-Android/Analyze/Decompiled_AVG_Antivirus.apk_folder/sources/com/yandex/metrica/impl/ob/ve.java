package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

abstract class ve<T> implements vo<T> {
    @NonNull
    protected final tp a;
    private final int b;
    private final String c;

    public ve(int i, @NonNull String str, @NonNull tp tpVar) {
        this.b = i;
        this.c = str;
        this.a = tpVar;
    }

    @VisibleForTesting(otherwise = 3)
    public int a() {
        return this.b;
    }

    @VisibleForTesting(otherwise = 3)
    @NonNull
    public String b() {
        return this.c;
    }
}
