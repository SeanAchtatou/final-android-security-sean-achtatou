package com.yandex.metrica;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.impl.ob.cg;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class g extends YandexMetricaConfig {
    @Nullable
    public final String a;
    @Nullable
    public final Map<String, String> b;
    @Nullable
    public final String c;
    @Nullable
    public final List<String> d;
    @Nullable
    public final Integer e;
    @Nullable
    public final Integer f;
    @Nullable
    public final Integer g;
    @Nullable
    public final Map<String, String> h;
    @Nullable
    public final Map<String, String> i;
    @Nullable
    public final Boolean j;
    @Nullable
    public final Boolean k;
    @Nullable
    public final Boolean l;
    @Nullable
    public final d m;

    public g(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        super(yandexMetricaConfig);
        this.a = null;
        this.b = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.c = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.d = null;
        this.h = null;
        this.m = null;
        this.l = null;
    }

    @NonNull
    public static g a(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        if (yandexMetricaConfig instanceof g) {
            return (g) yandexMetricaConfig;
        }
        return new g(yandexMetricaConfig);
    }

    @NonNull
    public static a a(@NonNull String str) {
        return new a(str);
    }

    @NonNull
    public static a b(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        a a2 = a(yandexMetricaConfig.apiKey);
        if (cg.a((Object) yandexMetricaConfig.appVersion)) {
            a2.a(yandexMetricaConfig.appVersion);
        }
        if (cg.a(yandexMetricaConfig.sessionTimeout)) {
            a2.a(yandexMetricaConfig.sessionTimeout.intValue());
        }
        if (cg.a(yandexMetricaConfig.crashReporting)) {
            a2.a(yandexMetricaConfig.crashReporting.booleanValue());
        }
        if (cg.a(yandexMetricaConfig.nativeCrashReporting)) {
            a2.b(yandexMetricaConfig.nativeCrashReporting.booleanValue());
        }
        if (cg.a(yandexMetricaConfig.location)) {
            a2.a(yandexMetricaConfig.location);
        }
        if (cg.a(yandexMetricaConfig.locationTracking)) {
            a2.d(yandexMetricaConfig.locationTracking.booleanValue());
        }
        if (cg.a(yandexMetricaConfig.installedAppCollecting)) {
            a2.e(yandexMetricaConfig.installedAppCollecting.booleanValue());
        }
        if (cg.a(yandexMetricaConfig.logs) && yandexMetricaConfig.logs.booleanValue()) {
            a2.a();
        }
        if (cg.a(yandexMetricaConfig.preloadInfo)) {
            a2.a(yandexMetricaConfig.preloadInfo);
        }
        if (cg.a(yandexMetricaConfig.firstActivationAsUpdate)) {
            a2.g(yandexMetricaConfig.firstActivationAsUpdate.booleanValue());
        }
        a(yandexMetricaConfig, a2);
        return a2;
    }

    private static void a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull a aVar) {
        if (yandexMetricaConfig instanceof g) {
            g gVar = (g) yandexMetricaConfig;
            if (cg.a((Object) gVar.d)) {
                aVar.a(gVar.d);
            }
        }
    }

    public static final class a {
        @Nullable
        public String a;
        /* access modifiers changed from: private */
        @NonNull
        public YandexMetricaConfig.Builder b;
        /* access modifiers changed from: private */
        @Nullable
        public String c;
        /* access modifiers changed from: private */
        @Nullable
        public List<String> d;
        /* access modifiers changed from: private */
        @Nullable
        public Integer e;
        /* access modifiers changed from: private */
        @Nullable
        public Map<String, String> f;
        /* access modifiers changed from: private */
        @Nullable
        public Integer g;
        /* access modifiers changed from: private */
        @Nullable
        public Integer h;
        /* access modifiers changed from: private */
        @NonNull
        public LinkedHashMap<String, String> i = new LinkedHashMap<>();
        /* access modifiers changed from: private */
        @NonNull
        public LinkedHashMap<String, String> j = new LinkedHashMap<>();
        /* access modifiers changed from: private */
        @Nullable
        public Boolean k;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean l;
        /* access modifiers changed from: private */
        @Nullable
        public d m;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean n;

        protected a(@NonNull String str) {
            this.b = YandexMetricaConfig.newConfigBuilder(str);
        }

        @NonNull
        public a a(@NonNull String str) {
            this.b.withAppVersion(str);
            return this;
        }

        @NonNull
        public a a(int i2) {
            this.b.withSessionTimeout(i2);
            return this;
        }

        @NonNull
        public a b(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public a a(boolean z) {
            this.b.withCrashReporting(z);
            return this;
        }

        @NonNull
        public a b(boolean z) {
            this.b.withNativeCrashReporting(z);
            return this;
        }

        @NonNull
        public a c(boolean z) {
            this.n = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public a a() {
            this.b.withLogs();
            return this;
        }

        @NonNull
        public a a(@Nullable Location location) {
            this.b.withLocation(location);
            return this;
        }

        @NonNull
        public a d(boolean z) {
            this.b.withLocationTracking(z);
            return this;
        }

        @NonNull
        public a e(boolean z) {
            this.b.withInstalledAppCollecting(z);
            return this;
        }

        @NonNull
        public a f(boolean z) {
            this.b.withStatisticsSending(z);
            return this;
        }

        @NonNull
        public a a(@NonNull String str, @Nullable String str2) {
            this.i.put(str, str2);
            return this;
        }

        @NonNull
        public a c(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public a a(@Nullable List<String> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public a b(int i2) {
            if (i2 >= 0) {
                this.e = Integer.valueOf(i2);
                return this;
            }
            throw new IllegalArgumentException(String.format(Locale.US, "Invalid %1$s. %1$s should be positive.", "App Build Number"));
        }

        @NonNull
        public a a(@Nullable Map<String, String> map, @Nullable Boolean bool) {
            this.k = bool;
            this.f = map;
            return this;
        }

        @NonNull
        public a c(int i2) {
            this.h = Integer.valueOf(i2);
            return this;
        }

        @NonNull
        public a d(int i2) {
            this.g = Integer.valueOf(i2);
            return this;
        }

        @NonNull
        public a a(@Nullable PreloadInfo preloadInfo) {
            this.b.withPreloadInfo(preloadInfo);
            return this;
        }

        @NonNull
        public a g(boolean z) {
            this.b.handleFirstActivationAsUpdate(z);
            return this;
        }

        @NonNull
        public a b(@NonNull String str, @Nullable String str2) {
            this.j.put(str, str2);
            return this;
        }

        @NonNull
        public a a(@NonNull d dVar) {
            this.m = dVar;
            return this;
        }

        @NonNull
        public g b() {
            return new g(this);
        }
    }

    private g(@NonNull a aVar) {
        super(aVar.b);
        Map<String, String> map;
        this.e = aVar.e;
        List c2 = aVar.d;
        Map<String, String> map2 = null;
        this.d = c2 == null ? null : Collections.unmodifiableList(c2);
        this.a = aVar.c;
        Map e2 = aVar.f;
        this.b = e2 == null ? null : Collections.unmodifiableMap(e2);
        this.g = aVar.h;
        this.f = aVar.g;
        this.c = aVar.a;
        if (aVar.i == null) {
            map = null;
        } else {
            map = Collections.unmodifiableMap(aVar.i);
        }
        this.h = map;
        this.i = aVar.j != null ? Collections.unmodifiableMap(aVar.j) : map2;
        this.j = aVar.k;
        this.k = aVar.l;
        this.m = aVar.m;
        this.l = aVar.n;
    }
}
