package com.yandex.metrica;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.vs;
import com.yandex.metrica.impl.ob.vt;
import com.yandex.metrica.impl.ob.vx;
import java.util.Currency;

public class Revenue {
    @NonNull
    public final Currency currency;
    @Nullable
    public final String payload;
    @Deprecated
    @Nullable
    public final Double price;
    @Nullable
    public final Long priceMicros;
    @Nullable
    public final String productID;
    @Nullable
    public final Integer quantity;
    @Nullable
    public final Receipt receipt;

    private Revenue(@NonNull Builder builder) {
        this.price = builder.a;
        this.priceMicros = builder.b;
        this.currency = builder.c;
        this.quantity = builder.d;
        this.productID = builder.e;
        this.payload = builder.f;
        this.receipt = builder.g;
    }

    @NonNull
    @Deprecated
    public static Builder newBuilder(double price2, @NonNull Currency currency2) {
        return new Builder(price2, currency2);
    }

    @NonNull
    public static Builder newBuilderWithMicros(long priceMicros2, @NonNull Currency currency2) {
        return new Builder(priceMicros2, currency2);
    }

    public static class Builder {
        private static final vx<Currency> h = new vt(new vs("revenue currency"));
        @Nullable
        Double a;
        @Nullable
        Long b;
        @NonNull
        Currency c;
        @Nullable
        Integer d;
        @Nullable
        String e;
        @Nullable
        String f;
        @Nullable
        Receipt g;

        Builder(double price, @NonNull Currency currency) {
            h.a(currency);
            this.a = Double.valueOf(price);
            this.c = currency;
        }

        Builder(long priceMicros, @NonNull Currency currency) {
            h.a(currency);
            this.b = Long.valueOf(priceMicros);
            this.c = currency;
        }

        @NonNull
        public Builder withQuantity(@Nullable Integer quantity) {
            this.d = quantity;
            return this;
        }

        @NonNull
        public Builder withProductID(@Nullable String productID) {
            this.e = productID;
            return this;
        }

        @NonNull
        public Builder withPayload(@Nullable String payload) {
            this.f = payload;
            return this;
        }

        @NonNull
        public Builder withReceipt(@Nullable Receipt receipt) {
            this.g = receipt;
            return this;
        }

        @NonNull
        public Revenue build() {
            return new Revenue(this);
        }
    }

    public static class Receipt {
        @Nullable
        public final String data;
        @Nullable
        public final String signature;

        private Receipt(@NonNull Builder builder) {
            this.data = builder.a;
            this.signature = builder.b;
        }

        @NonNull
        public static Builder newBuilder() {
            return new Builder();
        }

        public static class Builder {
            /* access modifiers changed from: private */
            @Nullable
            public String a;
            /* access modifiers changed from: private */
            @Nullable
            public String b;

            Builder() {
            }

            @NonNull
            public Builder withData(@Nullable String data) {
                this.a = data;
                return this;
            }

            @NonNull
            public Builder withSignature(@Nullable String signature) {
                this.b = signature;
                return this;
            }

            @NonNull
            public Receipt build() {
                return new Receipt(this);
            }
        }
    }
}
