package com.yandex.metrica;

import java.util.Map;

public class f {
    private String a;
    private String b;
    private Map<String, String> c;

    public String a() {
        return this.a;
    }

    public void a(String str) {
        this.a = str;
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
    }

    public Map<String, String> c() {
        return this.c;
    }

    public void a(Map<String, String> map) {
        this.c = map;
    }

    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Map<String, String> map = this.c;
        if (map != null) {
            i = map.hashCode();
        }
        return hashCode2 + i;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        f fVar = (f) o;
        String str = this.a;
        if (str == null ? fVar.a != null : !str.equals(fVar.a)) {
            return false;
        }
        String str2 = this.b;
        if (str2 == null ? fVar.b != null : !str2.equals(fVar.b)) {
            return false;
        }
        Map<String, String> map = this.c;
        if (map != null) {
            return map.equals(fVar.c);
        }
        if (fVar.c == null) {
            return true;
        }
        return false;
    }
}
