package com.yandex.metrica;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.ar;
import com.yandex.metrica.impl.ob.vt;
import com.yandex.metrica.impl.ob.vx;
import com.yandex.metrica.impl.ob.vy;

public class YandexMetricaConfig {
    @NonNull
    public final String apiKey;
    @Nullable
    public final String appVersion;
    @Nullable
    public final Boolean crashReporting;
    @Nullable
    public final Boolean firstActivationAsUpdate;
    @Nullable
    public final Boolean installedAppCollecting;
    @Nullable
    public final Location location;
    @Nullable
    public final Boolean locationTracking;
    @Nullable
    public final Boolean logs;
    @Nullable
    public final Boolean nativeCrashReporting;
    @Nullable
    public final PreloadInfo preloadInfo;
    @Nullable
    public final Integer sessionTimeout;
    @Nullable
    public final Boolean statisticsSending;

    @NonNull
    public static Builder newConfigBuilder(@NonNull String apiKey2) {
        return new Builder(apiKey2);
    }

    public static YandexMetricaConfig fromJson(String json) {
        return new ar().a(json);
    }

    public static class Builder {
        private static final vx<String> a = new vt(new vy());
        /* access modifiers changed from: private */
        @NonNull
        public final String b;
        /* access modifiers changed from: private */
        @Nullable
        public String c;
        /* access modifiers changed from: private */
        @Nullable
        public Integer d;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean e;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean f;
        /* access modifiers changed from: private */
        @Nullable
        public Location g;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean h;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean i;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean j;
        /* access modifiers changed from: private */
        @Nullable
        public PreloadInfo k;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean l;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean m;

        protected Builder(@NonNull String apiKey) {
            a.a(apiKey);
            this.b = apiKey;
        }

        @NonNull
        public Builder withAppVersion(@Nullable String appVersion) {
            this.c = appVersion;
            return this;
        }

        @NonNull
        public Builder withSessionTimeout(int sessionTimeout) {
            this.d = Integer.valueOf(sessionTimeout);
            return this;
        }

        @NonNull
        public Builder withCrashReporting(boolean enabled) {
            this.e = Boolean.valueOf(enabled);
            return this;
        }

        @NonNull
        public Builder withNativeCrashReporting(boolean enabled) {
            this.f = Boolean.valueOf(enabled);
            return this;
        }

        @NonNull
        public Builder withLogs() {
            this.j = true;
            return this;
        }

        @NonNull
        public Builder withLocation(@Nullable Location location) {
            this.g = location;
            return this;
        }

        @NonNull
        public Builder withLocationTracking(boolean enabled) {
            this.h = Boolean.valueOf(enabled);
            return this;
        }

        @NonNull
        public Builder withInstalledAppCollecting(boolean enabled) {
            this.i = Boolean.valueOf(enabled);
            return this;
        }

        @NonNull
        public Builder withPreloadInfo(@Nullable PreloadInfo preloadInfo) {
            this.k = preloadInfo;
            return this;
        }

        @NonNull
        public Builder handleFirstActivationAsUpdate(boolean value) {
            this.l = Boolean.valueOf(value);
            return this;
        }

        @NonNull
        public Builder withStatisticsSending(boolean value) {
            this.m = Boolean.valueOf(value);
            return this;
        }

        @NonNull
        public YandexMetricaConfig build() {
            return new YandexMetricaConfig(this);
        }
    }

    protected YandexMetricaConfig(@NonNull Builder builder) {
        this.apiKey = builder.b;
        this.appVersion = builder.c;
        this.sessionTimeout = builder.d;
        this.crashReporting = builder.e;
        this.nativeCrashReporting = builder.f;
        this.location = builder.g;
        this.locationTracking = builder.h;
        this.installedAppCollecting = builder.i;
        this.logs = builder.j;
        this.preloadInfo = builder.k;
        this.firstActivationAsUpdate = builder.l;
        this.statisticsSending = builder.m;
    }

    protected YandexMetricaConfig(@NonNull YandexMetricaConfig source) {
        this.apiKey = source.apiKey;
        this.appVersion = source.appVersion;
        this.sessionTimeout = source.sessionTimeout;
        this.crashReporting = source.crashReporting;
        this.nativeCrashReporting = source.nativeCrashReporting;
        this.location = source.location;
        this.locationTracking = source.locationTracking;
        this.installedAppCollecting = source.installedAppCollecting;
        this.logs = source.logs;
        this.preloadInfo = source.preloadInfo;
        this.firstActivationAsUpdate = source.firstActivationAsUpdate;
        this.statisticsSending = source.statisticsSending;
    }

    public String toJson() {
        return new ar().a(this);
    }
}
