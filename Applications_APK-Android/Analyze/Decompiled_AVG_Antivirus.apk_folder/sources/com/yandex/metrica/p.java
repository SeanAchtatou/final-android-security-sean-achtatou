package com.yandex.metrica;

import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.interact.DeviceInfo;
import com.yandex.metrica.impl.ob.ci;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public final class p {
    private static final List<String> a = Arrays.asList(IIdentifierCallback.YANDEX_MOBILE_METRICA_UUID, IIdentifierCallback.YANDEX_MOBILE_METRICA_DEVICE_ID, IIdentifierCallback.APP_METRICA_DEVICE_ID_HASH, IIdentifierCallback.YANDEX_MOBILE_METRICA_GET_AD_URL, IIdentifierCallback.YANDEX_MOBILE_METRICA_REPORT_AD_URL);

    @Deprecated
    public static void a(IIdentifierCallback callback) {
        a(callback, a);
    }

    @Deprecated
    public static void a(IIdentifierCallback callback, @NonNull List<String> identifiers) {
        ci.a(callback, new ArrayList(identifiers));
    }

    public static void a(Context context, IIdentifierCallback callback, @NonNull List<String> identifiers) {
        ci.a(context, callback, new ArrayList(identifiers));
    }

    public static void a(Context context, IIdentifierCallback callback, @NonNull String... identifiers) {
        a(context, callback, Arrays.asList(identifiers));
    }

    public static void a(Context context, IIdentifierCallback callback) {
        a(context, callback, a);
    }

    @NonNull
    public static String u(@NonNull String sdkName) {
        return ci.a(sdkName);
    }

    @Nullable
    public static Boolean plat() {
        try {
            return ci.d().get();
        } catch (Throwable th) {
            return null;
        }
    }

    public static boolean iifa() {
        return ci.a();
    }

    @Nullable
    public static String pgai() {
        try {
            return ci.b().get();
        } catch (Throwable th) {
            return null;
        }
    }

    @NonNull
    public static DeviceInfo gdi(@NonNull Context context) {
        return ci.a(context);
    }

    @NonNull
    public static String gcni(@NonNull Context context) {
        return ci.b(context);
    }

    @Deprecated
    @Nullable
    public static String guid() {
        return ci.c();
    }

    @Nullable
    public static String guid(@NonNull Context context) {
        return ci.d(context);
    }

    @Nullable
    public static String gdid(@NonNull Context context) {
        return ci.e(context);
    }

    @NonNull
    public static String mpn(Context context) {
        return ci.f(context);
    }

    public static void rce(int type, String name, String value, Map<String, String> environment) {
        ci.a(type, name, value, environment);
    }

    @NonNull
    public static String gmsvn(int apiLevel) {
        return ci.a(apiLevel);
    }

    public static void seb() {
        ci.e();
    }

    @NonNull
    @Deprecated
    public static YandexMetricaConfig cpcwh(YandexMetricaConfig config, String h) {
        return ci.a(config, h);
    }

    @Deprecated
    public static void rolu(Context context, Object registrant) {
        ci.a(context, registrant);
    }

    @Deprecated
    public static void urolu(Context context, Object registrant) {
        ci.b(context, registrant);
    }

    @Deprecated
    @Nullable
    public static Location glkl(Context context) {
        return ci.g(context);
    }

    @Deprecated
    @Nullable
    public static Integer gbc(Context context) {
        return ci.c(context.getApplicationContext());
    }
}
