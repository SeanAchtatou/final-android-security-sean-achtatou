package com.yandex.metrica;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.yandex.metrica.impl.ob.cj;
import com.yandex.metrica.impl.ob.py;
import com.yandex.metrica.profile.UserProfile;
import java.util.Map;

public final class YandexMetrica {
    private static final py a = new py(cj.a());

    private YandexMetrica() {
    }

    public static void activate(@NonNull Context context, @NonNull YandexMetricaConfig config) {
        a.a(context, config);
    }

    public static void sendEventsBuffer() {
        a.e();
    }

    public static void resumeSession(@Nullable Activity activity) {
        a.a(activity);
    }

    public static void pauseSession(@Nullable Activity activity) {
        a.b(activity);
    }

    public static void enableActivityAutoTracking(@NonNull Application application) {
        a.a(application);
    }

    public static void reportEvent(@NonNull String eventName) {
        a.a(eventName);
    }

    public static void reportError(@NonNull String message, @Nullable Throwable error) {
        a.a(message, error);
    }

    public static void reportUnhandledException(@NonNull Throwable exception) {
        a.a(exception);
    }

    public static void reportNativeCrash(@NonNull String nativeCrash) {
        a.b(nativeCrash);
    }

    public static void reportEvent(@NonNull String eventName, @Nullable String jsonValue) {
        a.a(eventName, jsonValue);
    }

    public static void reportEvent(@NonNull String eventName, @Nullable Map<String, Object> attributes) {
        a.a(eventName, attributes);
    }

    public static void reportAppOpen(@NonNull Activity activity) {
        a.c(activity);
    }

    public static void reportAppOpen(@NonNull String deeplink) {
        a.c(deeplink);
    }

    public static void reportReferralUrl(@NonNull String referralUrl) {
        a.d(referralUrl);
    }

    public static void setLocation(@Nullable Location location) {
        a.a(location);
    }

    public static void setLocationTracking(boolean enabled) {
        a.a(enabled);
    }

    public static void setLocationTracking(@NonNull Context context, boolean enabled) {
        a.a(context, enabled);
    }

    public static void setStatisticsSending(@NonNull Context context, boolean enabled) {
        a.b(context, enabled);
    }

    public static void activateReporter(@NonNull Context context, @NonNull ReporterConfig config) {
        a.a(context, config);
    }

    @NonNull
    public static IReporter getReporter(@NonNull Context context, @NonNull String apiKey) {
        return a.a(context, apiKey);
    }

    @NonNull
    public static String getLibraryVersion() {
        return "3.6.4";
    }

    public static int getLibraryApiLevel() {
        return 81;
    }

    public static void registerReferrerBroadcastReceivers(@NonNull BroadcastReceiver... anotherReferrerReceivers) {
        MetricaEventHandler.a(anotherReferrerReceivers);
    }

    public static void requestDeferredDeeplinkParameters(@NonNull DeferredDeeplinkParametersListener listener) {
        a.a(listener);
    }

    public static void requestAppMetricaDeviceID(@NonNull AppMetricaDeviceIDListener listener) {
        a.a(listener);
    }

    public static void setUserProfileID(@Nullable String userProfileID) {
        a.e(userProfileID);
    }

    public static void reportUserProfile(@NonNull UserProfile profile) {
        a.a(profile);
    }

    public static void reportRevenue(@NonNull Revenue revenue) {
        a.a(revenue);
    }
}
