package com.yandex.metrica;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.cj;
import com.yandex.metrica.impl.ob.ti;
import com.yandex.metrica.impl.ob.tp;
import com.yandex.metrica.impl.ob.vs;
import com.yandex.metrica.impl.ob.vt;
import com.yandex.metrica.impl.ob.vx;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class MetricaEventHandler extends BroadcastReceiver {
    public static final Set<BroadcastReceiver> a = new HashSet();
    private static final vx<BroadcastReceiver[]> b = new vt(new vs("Broadcast receivers"));

    public void onReceive(Context context, Intent intent) {
        if (a(intent)) {
            a(context, intent);
        }
        tp a2 = ti.a();
        for (BroadcastReceiver next : a) {
            String format = String.format("Sending referrer to %s", next.getClass().getName());
            if (a2.c()) {
                a2.a(format);
            }
            next.onReceive(context, intent);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(Intent intent) {
        return "com.android.vending.INSTALL_REFERRER".equals(intent.getAction());
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra("referrer");
        if (!TextUtils.isEmpty(stringExtra)) {
            cj.b(context).a(stringExtra);
        }
    }

    static void a(BroadcastReceiver... broadcastReceiverArr) {
        b.a(broadcastReceiverArr);
        Collections.addAll(a, broadcastReceiverArr);
    }
}
