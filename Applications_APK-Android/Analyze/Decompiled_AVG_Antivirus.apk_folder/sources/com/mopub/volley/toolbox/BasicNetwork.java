package com.mopub.volley.toolbox;

import android.os.SystemClock;
import com.mopub.volley.Cache;
import com.mopub.volley.Network;
import com.mopub.volley.Request;
import com.mopub.volley.RetryPolicy;
import com.mopub.volley.ServerError;
import com.mopub.volley.VolleyError;
import com.mopub.volley.VolleyLog;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.impl.cookie.DateUtils;

public class BasicNetwork implements Network {
    protected static final boolean DEBUG = VolleyLog.DEBUG;
    private static int DEFAULT_POOL_SIZE = 4096;
    private static int SLOW_REQUEST_THRESHOLD_MS = 3000;
    protected final HttpStack mHttpStack;
    protected final ByteArrayPool mPool;

    public BasicNetwork(HttpStack httpStack) {
        this(httpStack, new ByteArrayPool(DEFAULT_POOL_SIZE));
    }

    public BasicNetwork(HttpStack httpStack, ByteArrayPool pool) {
        this.mHttpStack = httpStack;
        this.mPool = pool;
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x016b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0119  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.mopub.volley.NetworkResponse performRequest(com.mopub.volley.Request<?> r24) throws com.mopub.volley.VolleyError {
        /*
            r23 = this;
            r7 = r23
            r8 = r24
            long r9 = android.os.SystemClock.elapsedRealtime()
        L_0x0008:
            r1 = 0
            r2 = 0
            java.util.Map r3 = java.util.Collections.emptyMap()
            r11 = 0
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ SocketTimeoutException -> 0x019b, ConnectTimeoutException -> 0x018e, MalformedURLException -> 0x0171, IOException -> 0x0113 }
            r0.<init>()     // Catch:{ SocketTimeoutException -> 0x019b, ConnectTimeoutException -> 0x018e, MalformedURLException -> 0x0171, IOException -> 0x0113 }
            com.mopub.volley.Cache$Entry r4 = r24.getCacheEntry()     // Catch:{ SocketTimeoutException -> 0x019b, ConnectTimeoutException -> 0x018e, MalformedURLException -> 0x0171, IOException -> 0x0113 }
            r7.addCacheHeaders(r0, r4)     // Catch:{ SocketTimeoutException -> 0x019b, ConnectTimeoutException -> 0x018e, MalformedURLException -> 0x0171, IOException -> 0x0113 }
            com.mopub.volley.toolbox.HttpStack r4 = r7.mHttpStack     // Catch:{ SocketTimeoutException -> 0x019b, ConnectTimeoutException -> 0x018e, MalformedURLException -> 0x0171, IOException -> 0x0113 }
            org.apache.http.HttpResponse r4 = r4.performRequest(r8, r0)     // Catch:{ SocketTimeoutException -> 0x019b, ConnectTimeoutException -> 0x018e, MalformedURLException -> 0x0171, IOException -> 0x0113 }
            r12 = r4
            org.apache.http.StatusLine r6 = r12.getStatusLine()     // Catch:{ SocketTimeoutException -> 0x0110, ConnectTimeoutException -> 0x010d, MalformedURLException -> 0x010a, IOException -> 0x0108 }
            int r1 = r6.getStatusCode()     // Catch:{ SocketTimeoutException -> 0x0110, ConnectTimeoutException -> 0x010d, MalformedURLException -> 0x010a, IOException -> 0x0108 }
            r15 = r1
            org.apache.http.Header[] r1 = r12.getAllHeaders()     // Catch:{ SocketTimeoutException -> 0x0110, ConnectTimeoutException -> 0x010d, MalformedURLException -> 0x010a, IOException -> 0x0108 }
            java.util.Map r1 = convertHeaders(r1)     // Catch:{ SocketTimeoutException -> 0x0110, ConnectTimeoutException -> 0x010d, MalformedURLException -> 0x010a, IOException -> 0x0108 }
            r14 = r1
            r1 = 304(0x130, float:4.26E-43)
            if (r15 != r1) goto L_0x0083
            com.mopub.volley.Cache$Entry r1 = r24.getCacheEntry()     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            if (r1 != 0) goto L_0x0054
            com.mopub.volley.NetworkResponse r3 = new com.mopub.volley.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            r17 = 304(0x130, float:4.26E-43)
            r18 = 0
            r20 = 1
            long r4 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            long r21 = r4 - r9
            r16 = r3
            r19 = r14
            r16.<init>(r17, r18, r19, r20, r21)     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            return r3
        L_0x0054:
            java.util.Map<java.lang.String, java.lang.String> r3 = r1.responseHeaders     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            r3.putAll(r14)     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            com.mopub.volley.NetworkResponse r3 = new com.mopub.volley.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            r17 = 304(0x130, float:4.26E-43)
            byte[] r4 = r1.data     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            java.util.Map<java.lang.String, java.lang.String> r5 = r1.responseHeaders     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            r20 = 1
            long r18 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            long r21 = r18 - r9
            r16 = r3
            r18 = r4
            r19 = r5
            r16.<init>(r17, r18, r19, r20, r21)     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            return r3
        L_0x0073:
            r0 = move-exception
            r3 = r14
            goto L_0x0115
        L_0x0077:
            r0 = move-exception
            r3 = r14
            goto L_0x0173
        L_0x007b:
            r0 = move-exception
            r3 = r14
            goto L_0x0190
        L_0x007f:
            r0 = move-exception
            r3 = r14
            goto L_0x019d
        L_0x0083:
            org.apache.http.HttpEntity r1 = r12.getEntity()     // Catch:{ SocketTimeoutException -> 0x0104, ConnectTimeoutException -> 0x0100, MalformedURLException -> 0x00fc, IOException -> 0x00f9 }
            if (r1 == 0) goto L_0x0094
            org.apache.http.HttpEntity r1 = r12.getEntity()     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            byte[] r1 = r7.entityToBytes(r1)     // Catch:{ SocketTimeoutException -> 0x007f, ConnectTimeoutException -> 0x007b, MalformedURLException -> 0x0077, IOException -> 0x0073 }
            r20 = r1
            goto L_0x0098
        L_0x0094:
            byte[] r1 = new byte[r11]     // Catch:{ SocketTimeoutException -> 0x0104, ConnectTimeoutException -> 0x0100, MalformedURLException -> 0x00fc, IOException -> 0x00f9 }
            r20 = r1
        L_0x0098:
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x00f3, ConnectTimeoutException -> 0x00ed, MalformedURLException -> 0x00e7, IOException -> 0x00e2 }
            long r21 = r1 - r9
            r1 = r23
            r2 = r21
            r4 = r24
            r5 = r20
            r1.logSlowRequests(r2, r4, r5, r6)     // Catch:{ SocketTimeoutException -> 0x00f3, ConnectTimeoutException -> 0x00ed, MalformedURLException -> 0x00e7, IOException -> 0x00e2 }
            r1 = 200(0xc8, float:2.8E-43)
            if (r15 < r1) goto L_0x00c7
            r1 = 299(0x12b, float:4.19E-43)
            if (r15 > r1) goto L_0x00c7
            com.mopub.volley.NetworkResponse r1 = new com.mopub.volley.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x00f3, ConnectTimeoutException -> 0x00ed, MalformedURLException -> 0x00e7, IOException -> 0x00e2 }
            r17 = 0
            long r2 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x00f3, ConnectTimeoutException -> 0x00ed, MalformedURLException -> 0x00e7, IOException -> 0x00e2 }
            long r18 = r2 - r9
            r13 = r1
            r3 = r14
            r14 = r15
            r2 = r15
            r15 = r20
            r16 = r3
            r13.<init>(r14, r15, r16, r17, r18)     // Catch:{ SocketTimeoutException -> 0x00dd, ConnectTimeoutException -> 0x00d8, MalformedURLException -> 0x00d3, IOException -> 0x00cf }
            return r1
        L_0x00c7:
            r3 = r14
            r2 = r15
            java.io.IOException r1 = new java.io.IOException     // Catch:{ SocketTimeoutException -> 0x00dd, ConnectTimeoutException -> 0x00d8, MalformedURLException -> 0x00d3, IOException -> 0x00cf }
            r1.<init>()     // Catch:{ SocketTimeoutException -> 0x00dd, ConnectTimeoutException -> 0x00d8, MalformedURLException -> 0x00d3, IOException -> 0x00cf }
            throw r1     // Catch:{ SocketTimeoutException -> 0x00dd, ConnectTimeoutException -> 0x00d8, MalformedURLException -> 0x00d3, IOException -> 0x00cf }
        L_0x00cf:
            r0 = move-exception
            r2 = r20
            goto L_0x0115
        L_0x00d3:
            r0 = move-exception
            r2 = r20
            goto L_0x0173
        L_0x00d8:
            r0 = move-exception
            r2 = r20
            goto L_0x0190
        L_0x00dd:
            r0 = move-exception
            r2 = r20
            goto L_0x019d
        L_0x00e2:
            r0 = move-exception
            r3 = r14
            r2 = r20
            goto L_0x0115
        L_0x00e7:
            r0 = move-exception
            r3 = r14
            r2 = r20
            goto L_0x0173
        L_0x00ed:
            r0 = move-exception
            r3 = r14
            r2 = r20
            goto L_0x0190
        L_0x00f3:
            r0 = move-exception
            r3 = r14
            r2 = r20
            goto L_0x019d
        L_0x00f9:
            r0 = move-exception
            r3 = r14
            goto L_0x0115
        L_0x00fc:
            r0 = move-exception
            r3 = r14
            goto L_0x0173
        L_0x0100:
            r0 = move-exception
            r3 = r14
            goto L_0x0190
        L_0x0104:
            r0 = move-exception
            r3 = r14
            goto L_0x019d
        L_0x0108:
            r0 = move-exception
            goto L_0x0115
        L_0x010a:
            r0 = move-exception
            goto L_0x0173
        L_0x010d:
            r0 = move-exception
            goto L_0x0190
        L_0x0110:
            r0 = move-exception
            goto L_0x019d
        L_0x0113:
            r0 = move-exception
            r12 = r1
        L_0x0115:
            r1 = 0
            r4 = 0
            if (r12 == 0) goto L_0x016b
            org.apache.http.StatusLine r5 = r12.getStatusLine()
            int r1 = r5.getStatusCode()
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            java.lang.Integer r6 = java.lang.Integer.valueOf(r1)
            r5[r11] = r6
            r6 = 1
            java.lang.String r11 = r24.getUrl()
            r5[r6] = r11
            java.lang.String r6 = "Unexpected response code %d for %s"
            com.mopub.volley.VolleyLog.e(r6, r5)
            if (r2 == 0) goto L_0x0165
            com.mopub.volley.NetworkResponse r5 = new com.mopub.volley.NetworkResponse
            r17 = 0
            long r13 = android.os.SystemClock.elapsedRealtime()
            long r18 = r13 - r9
            r13 = r5
            r14 = r1
            r15 = r2
            r16 = r3
            r13.<init>(r14, r15, r16, r17, r18)
            r4 = r5
            r5 = 401(0x191, float:5.62E-43)
            if (r1 == r5) goto L_0x015a
            r5 = 403(0x193, float:5.65E-43)
            if (r1 != r5) goto L_0x0154
            goto L_0x015a
        L_0x0154:
            com.mopub.volley.ServerError r5 = new com.mopub.volley.ServerError
            r5.<init>(r4)
            throw r5
        L_0x015a:
            com.mopub.volley.AuthFailureError r5 = new com.mopub.volley.AuthFailureError
            r5.<init>(r4)
            java.lang.String r6 = "auth"
            attemptRetryOnException(r6, r8, r5)
            goto L_0x01a8
        L_0x0165:
            com.mopub.volley.NetworkError r5 = new com.mopub.volley.NetworkError
            r5.<init>(r4)
            throw r5
        L_0x016b:
            com.mopub.volley.NoConnectionError r5 = new com.mopub.volley.NoConnectionError
            r5.<init>(r0)
            throw r5
        L_0x0171:
            r0 = move-exception
            r12 = r1
        L_0x0173:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Bad URL "
            r4.append(r5)
            java.lang.String r5 = r24.getUrl()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.<init>(r4, r0)
            throw r1
        L_0x018e:
            r0 = move-exception
            r12 = r1
        L_0x0190:
            com.mopub.volley.TimeoutError r1 = new com.mopub.volley.TimeoutError
            r1.<init>()
            java.lang.String r4 = "connection"
            attemptRetryOnException(r4, r8, r1)
            goto L_0x01a7
        L_0x019b:
            r0 = move-exception
            r12 = r1
        L_0x019d:
            com.mopub.volley.TimeoutError r1 = new com.mopub.volley.TimeoutError
            r1.<init>()
            java.lang.String r4 = "socket"
            attemptRetryOnException(r4, r8, r1)
        L_0x01a7:
        L_0x01a8:
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.volley.toolbox.BasicNetwork.performRequest(com.mopub.volley.Request):com.mopub.volley.NetworkResponse");
    }

    private void logSlowRequests(long requestLifetime, Request<?> request, byte[] responseContents, StatusLine statusLine) {
        if (DEBUG || requestLifetime > ((long) SLOW_REQUEST_THRESHOLD_MS)) {
            Object[] objArr = new Object[5];
            objArr[0] = request;
            objArr[1] = Long.valueOf(requestLifetime);
            objArr[2] = responseContents != null ? Integer.valueOf(responseContents.length) : "null";
            objArr[3] = Integer.valueOf(statusLine.getStatusCode());
            objArr[4] = Integer.valueOf(request.getRetryPolicy().getCurrentRetryCount());
            VolleyLog.d("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", objArr);
        }
    }

    private static void attemptRetryOnException(String logPrefix, Request<?> request, VolleyError exception) throws VolleyError {
        RetryPolicy retryPolicy = request.getRetryPolicy();
        int oldTimeout = request.getTimeoutMs();
        try {
            retryPolicy.retry(exception);
            request.addMarker(String.format("%s-retry [timeout=%s]", logPrefix, Integer.valueOf(oldTimeout)));
        } catch (VolleyError e) {
            request.addMarker(String.format("%s-timeout-giveup [timeout=%s]", logPrefix, Integer.valueOf(oldTimeout)));
            throw e;
        }
    }

    private void addCacheHeaders(Map<String, String> headers, Cache.Entry entry) {
        if (entry != null) {
            if (entry.etag != null) {
                headers.put("If-None-Match", entry.etag);
            }
            if (entry.serverDate > 0) {
                headers.put("If-Modified-Since", DateUtils.formatDate(new Date(entry.serverDate)));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void logError(String what, String url, long start) {
        VolleyLog.v("HTTP ERROR(%s) %d ms to fetch %s", what, Long.valueOf(SystemClock.elapsedRealtime() - start), url);
    }

    private byte[] entityToBytes(HttpEntity entity) throws IOException, ServerError {
        PoolingByteArrayOutputStream bytes = new PoolingByteArrayOutputStream(this.mPool, (int) entity.getContentLength());
        byte[] buffer = null;
        try {
            InputStream in = entity.getContent();
            if (in != null) {
                buffer = this.mPool.getBuf(1024);
                while (true) {
                    int read = in.read(buffer);
                    int count = read;
                    if (read == -1) {
                        break;
                    }
                    bytes.write(buffer, 0, count);
                }
                return bytes.toByteArray();
            }
            throw new ServerError();
        } finally {
            try {
                entity.consumeContent();
            } catch (IOException e) {
                VolleyLog.v("Error occured when calling consumingContent", new Object[0]);
            }
            this.mPool.returnBuf(buffer);
            bytes.close();
        }
    }

    protected static Map<String, String> convertHeaders(Header[] headers) {
        Map<String, String> result = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < headers.length; i++) {
            result.put(headers[i].getName(), headers[i].getValue());
        }
        return result;
    }
}
