package com.fastnet.browseralam.b;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.google.android.gms.common.internal.ImagesContract;

public final class b extends Handler {
    private g a;

    public b(Context context) {
        this.a = (g) context;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        this.a.c(message.getData().getString(ImagesContract.URL));
    }
}
