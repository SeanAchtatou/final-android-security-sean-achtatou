package com.fastnet.browseralam.b;

import android.os.Handler;

public final class k {
    private static Handler a = new Handler();

    public static Handler a() {
        return a;
    }

    public static void a(Runnable runnable) {
        a.postDelayed(runnable, 150);
    }

    public static void a(Runnable runnable, long j) {
        a.postDelayed(runnable, j);
    }

    public static void b(Runnable runnable) {
        a.post(runnable);
    }
}
