package com.fastnet.browseralam.b;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Message;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import com.fastnet.browseralam.view.XWebView;

public interface g {
    Activity B();

    h C();

    XWebView D();

    void E();

    void I();

    e J();

    void a(int i);

    void a(int i, boolean z, int i2);

    void a(Message message);

    void a(View view, WebChromeClient.CustomViewCallback customViewCallback);

    void a(ValueCallback valueCallback);

    void a(XWebView xWebView);

    void a(String str);

    void a(String str, String str2);

    void a(String str, boolean z);

    void b(ValueCallback valueCallback);

    void c(String str);

    void u();

    void v();

    void x();

    Bitmap y();

    View z();
}
