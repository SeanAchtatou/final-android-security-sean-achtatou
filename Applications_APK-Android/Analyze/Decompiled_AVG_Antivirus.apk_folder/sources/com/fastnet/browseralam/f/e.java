package com.fastnet.browseralam.f;

import android.view.animation.Animation;

final class e implements Animation.AnimationListener {
    final /* synthetic */ g a;
    final /* synthetic */ c b;

    e(c cVar, g gVar) {
        this.b = cVar;
        this.a = gVar;
    }

    public final void onAnimationEnd(Animation animation) {
    }

    public final void onAnimationRepeat(Animation animation) {
        this.a.m();
        this.a.c();
        this.a.b(this.a.j());
        if (this.b.a) {
            this.b.a = false;
            animation.setDuration(1332);
            this.a.a(false);
            return;
        }
        float unused = this.b.k = (this.b.k + 1.0f) % 5.0f;
    }

    public final void onAnimationStart(Animation animation) {
        float unused = this.b.k = 0.0f;
    }
}
