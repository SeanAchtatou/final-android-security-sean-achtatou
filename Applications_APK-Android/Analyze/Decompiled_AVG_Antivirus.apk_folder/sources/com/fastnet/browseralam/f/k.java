package com.fastnet.browseralam.f;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class k extends Animation {
    final /* synthetic */ int a;
    final /* synthetic */ int b;
    final /* synthetic */ h c;

    k(h hVar, int i, int i2) {
        this.c = hVar;
        this.a = i;
        this.b = i2;
    }

    public final void applyTransformation(float f, Transformation transformation) {
        this.c.u.setAlpha((int) (((float) this.a) + (((float) (this.b - this.a)) * f)));
    }
}
