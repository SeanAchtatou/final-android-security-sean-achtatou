package com.fastnet.browseralam.f;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.shapes.OvalShape;

final class b extends OvalShape {
    final /* synthetic */ a a;
    private RadialGradient b;
    private Paint c = new Paint();
    private int d;

    public b(a aVar, int i, int i2) {
        this.a = aVar;
        int unused = aVar.b = i;
        this.d = i2;
        this.b = new RadialGradient((float) (this.d / 2), (float) (this.d / 2), (float) aVar.b, new int[]{1023410176, 0}, (float[]) null, Shader.TileMode.CLAMP);
        this.c.setShader(this.b);
    }

    public final void draw(Canvas canvas, Paint paint) {
        int width = this.a.getWidth();
        int height = this.a.getHeight();
        canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) ((this.d / 2) + this.a.b), this.c);
        canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (this.d / 2), paint);
    }
}
