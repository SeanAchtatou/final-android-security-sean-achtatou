package com.fastnet.browseralam.f;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class n extends Animation {
    final /* synthetic */ h a;

    n(h hVar) {
        this.a = hVar;
    }

    public final void applyTransformation(float f, Transformation transformation) {
        this.a.a((((int) (((float) ((!this.a.D ? (int) (this.a.z - ((float) Math.abs(this.a.b))) : (int) this.a.z) - this.a.a)) * f)) + this.a.a) - this.a.r.getTop());
        this.a.u.a(1.0f - f);
    }
}
