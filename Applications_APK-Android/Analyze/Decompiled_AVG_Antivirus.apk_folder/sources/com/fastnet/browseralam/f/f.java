package com.fastnet.browseralam.f;

import android.graphics.drawable.Drawable;

final class f implements Drawable.Callback {
    final /* synthetic */ c a;

    f(c cVar) {
        this.a = cVar;
    }

    public final void invalidateDrawable(Drawable drawable) {
        this.a.invalidateSelf();
    }

    public final void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        this.a.scheduleSelf(runnable, j);
    }

    public final void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        this.a.unscheduleSelf(runnable);
    }
}
