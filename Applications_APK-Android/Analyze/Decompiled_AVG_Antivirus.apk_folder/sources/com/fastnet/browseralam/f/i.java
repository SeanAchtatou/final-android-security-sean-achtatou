package com.fastnet.browseralam.f;

import android.view.animation.Animation;

final class i implements Animation.AnimationListener {
    final /* synthetic */ h a;

    i(h hVar) {
        this.a = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.f.h.a(com.fastnet.browseralam.f.h, float):void
     arg types: [com.fastnet.browseralam.f.h, int]
     candidates:
      com.fastnet.browseralam.f.h.a(int, int):android.view.animation.Animation
      com.fastnet.browseralam.f.h.a(com.fastnet.browseralam.f.h, int):void
      com.fastnet.browseralam.f.h.a(com.fastnet.browseralam.f.h, android.view.MotionEvent):void
      com.fastnet.browseralam.f.h.a(boolean, boolean):void
      com.fastnet.browseralam.f.h.a(com.fastnet.browseralam.f.h, float):void */
    public final void onAnimationEnd(Animation animation) {
        if (this.a.e) {
            this.a.u.setAlpha(255);
            this.a.u.start();
            if (this.a.A && this.a.d != null) {
                this.a.d.a();
            }
        } else {
            this.a.u.stop();
            this.a.r.setVisibility(8);
            h.f(this.a);
            if (this.a.o) {
                this.a.a(0.0f);
            } else {
                this.a.a(this.a.b - this.a.i);
            }
        }
        int unused = this.a.i = this.a.r.getTop();
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
