package com.fastnet.browseralam.f;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.b.a;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;

final class c extends Drawable implements Animatable {
    private static final Interpolator b = new LinearInterpolator();
    /* access modifiers changed from: private */
    public static final Interpolator c = new a();
    boolean a;
    private final int[] d = {-16777216};
    private final ArrayList e = new ArrayList();
    private final g f;
    private float g;
    private Resources h;
    private View i;
    private Animation j;
    /* access modifiers changed from: private */
    public float k;
    private double l;
    private double m;
    private final Drawable.Callback n = new f(this);

    public c(Context context, View view) {
        this.i = view;
        this.h = context.getResources();
        this.f = new g(this.n);
        this.f.a(this.d);
        g gVar = this.f;
        float f2 = this.h.getDisplayMetrics().density;
        this.l = ((double) f2) * 40.0d;
        this.m = ((double) f2) * 40.0d;
        gVar.a(2.5f * f2);
        gVar.a(8.75d * ((double) f2));
        gVar.b(0);
        gVar.a(10.0f * f2, f2 * 5.0f);
        gVar.a((int) this.l, (int) this.m);
        g gVar2 = this.f;
        d dVar = new d(this, gVar2);
        dVar.setRepeatCount(-1);
        dVar.setRepeatMode(1);
        dVar.setInterpolator(b);
        dVar.setAnimationListener(new e(this, gVar2));
        this.j = dVar;
    }

    static /* synthetic */ void a(float f2, g gVar) {
        c(f2, gVar);
        float b2 = b(gVar);
        gVar.b((((gVar.h() - b2) - gVar.g()) * f2) + gVar.g());
        gVar.c(gVar.h());
        gVar.d(((((float) (Math.floor((double) (gVar.l() / 0.8f)) + 1.0d)) - gVar.l()) * f2) + gVar.l());
    }

    /* access modifiers changed from: private */
    public static float b(g gVar) {
        return (float) Math.toRadians(((double) gVar.e()) / (6.283185307179586d * gVar.k()));
    }

    /* access modifiers changed from: private */
    public static void c(float f2, g gVar) {
        if (f2 > 0.75f) {
            float f3 = (f2 - 0.75f) / 0.25f;
            int i2 = gVar.i();
            int b2 = gVar.b();
            int intValue = Integer.valueOf(i2).intValue();
            int i3 = (intValue >> 24) & 255;
            int i4 = (intValue >> 16) & 255;
            int i5 = (intValue >> 8) & 255;
            int i6 = intValue & 255;
            int intValue2 = Integer.valueOf(b2).intValue();
            gVar.a((((int) (f3 * ((float) ((intValue2 & 255) - i6)))) + i6) | ((i3 + ((int) (((float) (((intValue2 >> 24) & 255) - i3)) * f3))) << 24) | ((i4 + ((int) (((float) (((intValue2 >> 16) & 255) - i4)) * f3))) << 16) | ((((int) (((float) (((intValue2 >> 8) & 255) - i5)) * f3)) + i5) << 8));
        }
    }

    public final void a() {
        this.f.a();
    }

    public final void a(float f2) {
        this.f.e(f2);
    }

    public final void a(boolean z) {
        this.f.a(z);
    }

    public final void a(int... iArr) {
        this.f.a(iArr);
        this.f.b(0);
    }

    public final void b(float f2) {
        this.f.b(0.0f);
        this.f.c(f2);
    }

    public final void c(float f2) {
        this.f.d(f2);
    }

    /* access modifiers changed from: package-private */
    public final void d(float f2) {
        this.g = f2;
        invalidateSelf();
    }

    public final void draw(Canvas canvas) {
        Rect bounds = getBounds();
        int save = canvas.save();
        canvas.rotate(this.g, bounds.exactCenterX(), bounds.exactCenterY());
        this.f.a(canvas, bounds);
        canvas.restoreToCount(save);
    }

    public final int getAlpha() {
        return this.f.d();
    }

    public final int getIntrinsicHeight() {
        return (int) this.m;
    }

    public final int getIntrinsicWidth() {
        return (int) this.l;
    }

    public final int getOpacity() {
        return -3;
    }

    public final boolean isRunning() {
        ArrayList arrayList = this.e;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            Animation animation = (Animation) arrayList.get(i2);
            if (animation.hasStarted() && !animation.hasEnded()) {
                return true;
            }
        }
        return false;
    }

    public final void setAlpha(int i2) {
        this.f.c(i2);
    }

    public final void setColorFilter(ColorFilter colorFilter) {
        this.f.a(colorFilter);
    }

    public final void start() {
        this.j.reset();
        this.f.m();
        if (this.f.j() != this.f.f()) {
            this.a = true;
            this.j.setDuration(666);
            this.i.startAnimation(this.j);
            return;
        }
        this.f.b(0);
        this.f.n();
        this.j.setDuration(1332);
        this.i.startAnimation(this.j);
    }

    public final void stop() {
        this.i.clearAnimation();
        d(0.0f);
        this.f.a(false);
        this.f.b(0);
        this.f.n();
    }
}
