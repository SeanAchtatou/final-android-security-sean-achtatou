package com.fastnet.browseralam.f;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class d extends Animation {
    final /* synthetic */ g a;
    final /* synthetic */ c b;

    d(c cVar, g gVar) {
        this.b = cVar;
        this.a = gVar;
    }

    public final void applyTransformation(float f, Transformation transformation) {
        if (this.b.a) {
            c.a(f, this.a);
            return;
        }
        float a2 = c.b(this.a);
        float h = this.a.h();
        float g = this.a.g();
        float l = this.a.l();
        c.c(f, this.a);
        if (f <= 0.5f) {
            this.a.b(g + (c.c.getInterpolation(f / 0.5f) * (0.8f - a2)));
        }
        if (f > 0.5f) {
            this.a.c(((0.8f - a2) * c.c.getInterpolation((f - 0.5f) / 0.5f)) + h);
        }
        this.a.d((0.25f * f) + l);
        this.b.d((216.0f * f) + (1080.0f * (this.b.k / 5.0f)));
    }
}
