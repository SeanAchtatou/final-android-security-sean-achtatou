package com.fastnet.browseralam.f;

import android.content.Context;
import android.graphics.drawable.ShapeDrawable;
import android.os.Build;
import android.support.v4.view.bx;
import android.view.animation.Animation;
import android.widget.ImageView;

final class a extends ImageView {
    private Animation.AnimationListener a;
    /* access modifiers changed from: private */
    public int b;

    public a(Context context) {
        super(context);
        float f = getContext().getResources().getDisplayMetrics().density;
        this.b = (int) (f * 3.5f);
        ShapeDrawable shapeDrawable = new ShapeDrawable(new b(this, this.b, (int) (20.0f * f * 2.0f)));
        bx.a(this, 1, shapeDrawable.getPaint());
        shapeDrawable.getPaint().setShadowLayer((float) this.b, (float) ((int) (0.0f * f)), (float) ((int) (1.75f * f)), 503316480);
        int i = this.b;
        setPadding(i, i, i, i);
        shapeDrawable.getPaint().setColor(-328966);
        setBackgroundDrawable(shapeDrawable);
    }

    public final void a(Animation.AnimationListener animationListener) {
        this.a = animationListener;
    }

    public final void onAnimationEnd() {
        super.onAnimationEnd();
        if (this.a != null) {
            this.a.onAnimationEnd(getAnimation());
        }
    }

    public final void onAnimationStart() {
        super.onAnimationStart();
        if (this.a != null) {
            this.a.onAnimationStart(getAnimation());
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (!(Build.VERSION.SDK_INT >= 21)) {
            setMeasuredDimension(getMeasuredWidth() + (this.b * 2), getMeasuredHeight() + (this.b * 2));
        }
    }

    public final void setBackgroundColor(int i) {
        if (getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable) getBackground()).getPaint().setColor(i);
        }
    }
}
