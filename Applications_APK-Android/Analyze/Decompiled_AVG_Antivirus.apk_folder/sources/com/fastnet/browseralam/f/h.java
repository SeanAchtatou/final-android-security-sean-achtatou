package com.fastnet.browseralam.f;

import android.app.Activity;
import android.support.v4.view.ay;
import android.support.v4.view.bx;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.ad;

public final class h {
    private static final int[] q = {16842766};
    /* access modifiers changed from: private */
    public boolean A;
    private int B;
    private int C;
    /* access modifiers changed from: private */
    public boolean D;
    private Animation.AnimationListener E = new i(this);
    /* access modifiers changed from: private */
    public ad F;
    /* access modifiers changed from: private */
    public boolean G;
    private View.OnTouchListener H = new l(this);
    private final Animation I = new n(this);
    private final Animation J = new o(this);
    protected int a;
    protected int b;
    private Activity c;
    /* access modifiers changed from: private */
    public q d;
    /* access modifiers changed from: private */
    public boolean e = false;
    private int f;
    private float g = -1.0f;
    private int h;
    /* access modifiers changed from: private */
    public int i;
    private boolean j = false;
    /* access modifiers changed from: private */
    public float k;
    private float l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public int n = -1;
    /* access modifiers changed from: private */
    public boolean o;
    private final DecelerateInterpolator p;
    /* access modifiers changed from: private */
    public a r;
    private int s = -1;
    /* access modifiers changed from: private */
    public float t;
    /* access modifiers changed from: private */
    public c u;
    private Animation v;
    private Animation w;
    private Animation x;
    private Animation y;
    /* access modifiers changed from: private */
    public float z;

    public h(Activity activity, FrameLayout frameLayout) {
        this.c = activity;
        this.f = ViewConfiguration.get(activity).getScaledTouchSlop();
        this.h = this.c.getResources().getInteger(17694721);
        this.p = new DecelerateInterpolator(2.0f);
        DisplayMetrics displayMetrics = this.c.getResources().getDisplayMetrics();
        this.B = (int) (displayMetrics.density * 40.0f);
        this.C = (int) (displayMetrics.density * 40.0f);
        this.r = new a(this.c);
        this.u = new c(this.c, this.r);
        this.u.a();
        this.r.setImageDrawable(this.u);
        this.r.setVisibility(0);
        ((FrameLayout) frameLayout.findViewById(R.id.webview_frame)).addView(this.r, 0, new FrameLayout.LayoutParams(-2, -2, 1));
        this.z = 64.0f * displayMetrics.density;
        this.g = this.z;
        this.r.setTranslationY((float) (-aq.a(56)));
    }

    private Animation a(int i2, int i3) {
        k kVar = new k(this, i2, i3);
        kVar.setDuration(300);
        this.r.a((Animation.AnimationListener) null);
        this.r.clearAnimation();
        this.r.startAnimation(kVar);
        return kVar;
    }

    /* access modifiers changed from: private */
    public void a(float f2) {
        bx.d(this.r, f2);
        bx.e(this.r, f2);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.r.offsetTopAndBottom(i2);
        this.i = this.r.getTop();
    }

    /* access modifiers changed from: private */
    public void a(Animation.AnimationListener animationListener) {
        this.v = new j(this);
        this.v.setDuration(150);
        this.r.a(animationListener);
        this.r.clearAnimation();
        this.r.startAnimation(this.v);
    }

    static /* synthetic */ void a(h hVar, MotionEvent motionEvent) {
        int b2 = ay.b(motionEvent);
        if (ay.b(motionEvent, b2) == hVar.n) {
            hVar.n = ay.b(motionEvent, b2 == 0 ? 1 : 0);
        }
    }

    private void a(boolean z2, boolean z3) {
        if (this.e != z2) {
            this.A = z3;
            this.e = z2;
            if (this.e) {
                int i2 = this.i;
                Animation.AnimationListener animationListener = this.E;
                this.a = i2;
                this.I.reset();
                this.I.setDuration(200);
                this.I.setInterpolator(this.p);
                if (animationListener != null) {
                    this.r.a(animationListener);
                }
                this.r.clearAnimation();
                this.r.startAnimation(this.I);
                return;
            }
            a(this.E);
        }
    }

    private static boolean a(Animation animation) {
        return animation != null && animation.hasStarted() && !animation.hasEnded();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.d(android.view.View, float):void
     arg types: [com.fastnet.browseralam.f.a, int]
     candidates:
      android.support.v4.view.bx.d(android.view.View, int):void
      android.support.v4.view.bx.d(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.e(android.view.View, float):void
     arg types: [com.fastnet.browseralam.f.a, int]
     candidates:
      android.support.v4.view.bx.e(android.view.View, int):void
      android.support.v4.view.bx.e(android.view.View, float):void */
    static /* synthetic */ void b(h hVar, float f2) {
        hVar.u.a(true);
        float min = Math.min(1.0f, Math.abs(f2 / hVar.g));
        float max = (((float) Math.max(((double) min) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
        float abs = Math.abs(f2) - hVar.g;
        float f3 = hVar.D ? hVar.z - ((float) hVar.b) : hVar.z;
        float max2 = Math.max(0.0f, Math.min(abs, f3 * 2.0f) / f3);
        float pow = ((float) (((double) (max2 / 4.0f)) - Math.pow((double) (max2 / 4.0f), 2.0d))) * 2.0f;
        int i2 = ((int) ((f3 * min) + (f3 * pow * 2.0f))) + hVar.b;
        if (hVar.r.getVisibility() != 0) {
            hVar.r.setVisibility(0);
        }
        if (!hVar.o) {
            bx.d((View) hVar.r, 1.0f);
            bx.e((View) hVar.r, 1.0f);
        }
        if (f2 < hVar.g) {
            if (hVar.o) {
                hVar.a(f2 / hVar.g);
            }
            if (hVar.u.getAlpha() > 76 && !a(hVar.w)) {
                hVar.w = hVar.a(hVar.u.getAlpha(), 76);
            }
            hVar.u.b(Math.min(0.8f, max * 0.8f));
            hVar.u.a(Math.min(1.0f, max));
        } else if (hVar.u.getAlpha() < 255 && !a(hVar.x)) {
            hVar.x = hVar.a(hVar.u.getAlpha(), 255);
        }
        hVar.u.c((-0.25f + (max * 0.4f) + (pow * 2.0f)) * 0.5f);
        hVar.a(i2 - hVar.i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.f.h.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.fastnet.browseralam.f.h.a(int, int):android.view.animation.Animation
      com.fastnet.browseralam.f.h.a(com.fastnet.browseralam.f.h, float):void
      com.fastnet.browseralam.f.h.a(com.fastnet.browseralam.f.h, int):void
      com.fastnet.browseralam.f.h.a(com.fastnet.browseralam.f.h, android.view.MotionEvent):void
      com.fastnet.browseralam.f.h.a(boolean, boolean):void */
    static /* synthetic */ void c(h hVar, float f2) {
        if (f2 > hVar.g) {
            hVar.a(true, true);
            return;
        }
        hVar.e = false;
        hVar.u.b(0.0f);
        m mVar = null;
        if (!hVar.o) {
            mVar = new m(hVar);
        }
        int i2 = hVar.i;
        if (hVar.o) {
            hVar.a = i2;
            hVar.t = bx.u(hVar.r);
            hVar.y = new p(hVar);
            hVar.y.setDuration(150);
            if (mVar != null) {
                hVar.r.a(mVar);
            }
            hVar.r.clearAnimation();
            hVar.r.startAnimation(hVar.y);
        } else {
            hVar.a = i2;
            hVar.J.reset();
            hVar.J.setDuration(200);
            hVar.J.setInterpolator(hVar.p);
            if (mVar != null) {
                hVar.r.a(mVar);
            }
            hVar.r.clearAnimation();
            hVar.r.startAnimation(hVar.J);
        }
        hVar.u.a(false);
    }

    static /* synthetic */ void f(h hVar) {
        hVar.r.getBackground().setAlpha(255);
        hVar.u.setAlpha(255);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.f.h.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.fastnet.browseralam.f.h.a(int, int):android.view.animation.Animation
      com.fastnet.browseralam.f.h.a(com.fastnet.browseralam.f.h, float):void
      com.fastnet.browseralam.f.h.a(com.fastnet.browseralam.f.h, int):void
      com.fastnet.browseralam.f.h.a(com.fastnet.browseralam.f.h, android.view.MotionEvent):void
      com.fastnet.browseralam.f.h.a(boolean, boolean):void */
    public final void a() {
        a(false, false);
    }

    public final void a(MotionEvent motionEvent) {
        a(this.b - this.r.getTop());
        this.n = ay.b(motionEvent, 0);
        this.m = true;
        int a2 = ay.a(motionEvent, this.n);
        this.l = a2 < 0 ? -1.0f : ay.d(motionEvent, a2);
        this.k = this.l + ((float) this.f);
        this.u.setAlpha(76);
        this.e = false;
        this.G = true;
    }

    public final void a(q qVar) {
        this.d = qVar;
    }

    public final void a(ad adVar) {
        this.F = adVar;
    }

    public final void a(int... iArr) {
        this.u.a(iArr);
    }

    public final View.OnTouchListener b() {
        return this.H;
    }

    public final View c() {
        return this.r;
    }
}
