package com.fastnet.browseralam.f;

import android.support.v4.view.ay;
import android.view.MotionEvent;
import android.view.View;

final class l implements View.OnTouchListener {
    final /* synthetic */ h a;

    l(h hVar) {
        this.a = hVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (ay.a(motionEvent)) {
            case 1:
                this.a.F.a();
                boolean unused = this.a.G = false;
                int a2 = ay.a(motionEvent, this.a.n);
                if (a2 < 0) {
                    return false;
                }
                boolean unused2 = this.a.m = false;
                h.c(this.a, (ay.d(motionEvent, a2) - this.a.k) * 0.5f);
                int unused3 = this.a.n = -1;
                return false;
            case 2:
                if (!this.a.G) {
                    return false;
                }
                int a3 = ay.a(motionEvent, this.a.n);
                if (a3 < 0) {
                    return true;
                }
                float d = (ay.d(motionEvent, a3) - this.a.k) * 0.5f;
                if (this.a.m) {
                    if (d <= 0.0f) {
                        return true;
                    }
                    h.b(this.a, d);
                }
                return true;
            case 3:
                this.a.F.a();
                return false;
            case 4:
            default:
                return false;
            case 5:
                int b = ay.b(motionEvent);
                if (b < 0) {
                    return false;
                }
                int unused4 = this.a.n = ay.b(motionEvent, b);
                return false;
            case 6:
                h.a(this.a, motionEvent);
                return false;
        }
    }
}
