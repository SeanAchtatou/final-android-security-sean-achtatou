package com.fastnet.browseralam.activity;

import android.view.MotionEvent;
import android.view.View;

final class hy implements View.OnTouchListener {
    final /* synthetic */ gr a;
    final /* synthetic */ hw b;

    hy(hw hwVar, gr grVar) {
        this.b = hwVar;
        this.a = grVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return true;
        }
        this.b.a.x.animate().translationX(0.0f).setInterpolator(this.b.s).setListener(this.b.G);
        return true;
    }
}
