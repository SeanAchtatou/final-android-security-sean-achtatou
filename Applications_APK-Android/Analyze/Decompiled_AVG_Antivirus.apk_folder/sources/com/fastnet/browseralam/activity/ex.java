package com.fastnet.browseralam.activity;

import android.webkit.WebView;

final class ex implements Runnable {
    final /* synthetic */ ei a;
    private WebView b;

    private ex(ei eiVar) {
        this.a = eiVar;
    }

    /* synthetic */ ex(ei eiVar, byte b2) {
        this(eiVar);
    }

    public final ex a(WebView webView) {
        this.b = webView;
        return this;
    }

    public final void run() {
        this.a.d.removeView(this.b);
        this.b = null;
    }
}
