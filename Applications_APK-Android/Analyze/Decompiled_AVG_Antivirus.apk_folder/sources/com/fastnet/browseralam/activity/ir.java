package com.fastnet.browseralam.activity;

import android.app.AlertDialog;
import android.view.View;
import com.fastnet.browseralam.i.aq;
import java.io.File;

final class ir implements View.OnClickListener {
    final /* synthetic */ File a;
    final /* synthetic */ AlertDialog b;
    final /* synthetic */ Cif c;

    ir(Cif ifVar, File file, AlertDialog alertDialog) {
        this.c = ifVar;
        this.a = file;
        this.b = alertDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.gr.a(java.io.File, boolean):boolean
     arg types: [java.io.File, int]
     candidates:
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.support.v7.widget.bt):android.support.v7.widget.bt
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.widget.PopupWindow):android.widget.PopupWindow
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hu):com.fastnet.browseralam.activity.hu
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.e.p):com.fastnet.browseralam.e.p
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, boolean):boolean
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.b.f.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.activity.gr.a(java.io.File, boolean):boolean */
    public final void onClick(View view) {
        if (this.c.s.a(this.a, false)) {
            aq.a(this.c.m, "Bookmarks import success");
            this.b.dismiss();
            this.c.f();
            return;
        }
        this.b.dismiss();
        aq.a(this.c.m, "Bookmarks import failed");
    }
}
