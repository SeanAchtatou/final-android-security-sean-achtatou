package com.fastnet.browseralam.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.a;
import com.fastnet.browseralam.b.i;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.Background;
import com.fastnet.browseralam.view.DrawerFrame;

public final class fz implements i {
    private static final ViewGroup.LayoutParams Q = new ViewGroup.LayoutParams(-1, -1);
    /* access modifiers changed from: private */
    public int A = aq.b();
    private int B;
    private int C;
    private int D;
    private int E;
    private final DecelerateInterpolator F = new DecelerateInterpolator(2.0f);
    private final AccelerateDecelerateInterpolator G = new AccelerateDecelerateInterpolator();
    private gn H = new ga(this);
    private View.OnClickListener I = new ge(this);
    private View.OnAttachStateChangeListener J = new gf(this);
    /* access modifiers changed from: private */
    public go K = new gh(this);
    /* access modifiers changed from: private */
    public go L = this.K;
    /* access modifiers changed from: private */
    public go M = new gl(this);
    private go N = new gm(this);
    private go O = new gb(this);
    private go P = new gc(this);
    a a = new gd(this);
    /* access modifiers changed from: private */
    public final BrowserActivity b;
    /* access modifiers changed from: private */
    public final DrawerFrame c;
    private final RecyclerView d;
    /* access modifiers changed from: private */
    public final RecyclerView e;
    private final com.fastnet.browseralam.h.a f = com.fastnet.browseralam.h.a.a();
    private final gr g;
    /* access modifiers changed from: private */
    public final Cif h;
    private FrameLayout i;
    /* access modifiers changed from: private */
    public RelativeLayout j;
    private RelativeLayout k;
    /* access modifiers changed from: private */
    public RelativeLayout l;
    /* access modifiers changed from: private */
    public RelativeLayout m;
    /* access modifiers changed from: private */
    public View n;
    /* access modifiers changed from: private */
    public View o;
    private View p;
    private Background q;
    /* access modifiers changed from: private */
    public TextView r;
    /* access modifiers changed from: private */
    public TextView s;
    /* access modifiers changed from: private */
    public TextView t;
    /* access modifiers changed from: private */
    public ImageView u;
    /* access modifiers changed from: private */
    public ImageView v;
    private gp w = new gp(this, (byte) 0);
    private boolean x = true;
    private boolean y;
    private boolean z;

    public fz(BrowserActivity browserActivity, FrameLayout frameLayout) {
        this.b = browserActivity;
        this.i = frameLayout;
        this.c = (DrawerFrame) frameLayout.findViewById(R.id.bottomDrawerFrame);
        this.p = this.c.findViewById(R.id.shadowOverlay);
        this.j = (RelativeLayout) this.c.findViewById(R.id.bottom_drawer);
        this.A = this.b.getResources().getDisplayMetrics().heightPixels - aq.a(26);
        this.j.setTranslationY((float) this.A);
        this.q = (Background) this.j.findViewById(R.id.background);
        this.q.setTranslationY(0.0f);
        this.z = com.fastnet.browseralam.h.a.C();
        TextView textView = (TextView) this.j.findViewById(R.id.bookmarks_textview);
        this.s = textView;
        this.r = textView;
        this.t = (TextView) this.j.findViewById(R.id.files_textview);
        this.s.setOnClickListener(this.I);
        this.t.setOnClickListener(this.I);
        this.D = this.b.getResources().getColor(R.color.gray_dark);
        this.E = this.b.getResources().getColor(R.color.gray_medium_dark);
        this.C = com.fastnet.browseralam.h.a.ai() ? -1 : this.D;
        this.e = (RecyclerView) this.j.findViewById(R.id.file_list);
        this.h = new Cif(this, this.b, this.i);
        this.l = (RelativeLayout) this.j.findViewById(R.id.bookmark_toolbar);
        this.m = (RelativeLayout) this.j.findViewById(R.id.file_toolbar);
        this.d = (RecyclerView) this.j.findViewById(R.id.bookmark_list);
        this.g = new gr(this, this.b, this.h, this.i);
        this.h.a(this.g);
        this.c.a(this, this.b, this.d, this.q);
        this.c.b();
        this.c.a((float) this.A);
        this.c.addOnAttachStateChangeListener(this.J);
        this.n = this.c.a();
        this.i.removeView(this.i.findViewById(R.id.bottom_toolbar));
        this.o = new View(this.b);
        this.L = this.K;
        this.B = browserActivity.getResources().getConfiguration().orientation;
        g();
    }

    /* access modifiers changed from: private */
    public gp a(RecyclerView recyclerView) {
        gp gpVar = this.w;
        gpVar.a = recyclerView;
        return gpVar;
    }

    /* access modifiers changed from: private */
    public gq a(View view) {
        return new gq(this, view);
    }

    static /* synthetic */ void e(fz fzVar) {
        fzVar.H.a();
        fzVar.r.setTextColor(fzVar.E);
        TextView textView = fzVar.t;
        fzVar.r = textView;
        textView.setTextColor(fzVar.C);
        fzVar.c.a(fzVar.e);
        fzVar.e.setVisibility(0);
        fzVar.m.setVisibility(0);
        fzVar.m.animate().setStartDelay(50).alpha(1.0f).setListener(null);
        fzVar.e.animate().alpha(1.0f).setStartDelay(50).setListener(null);
        fzVar.o.animate().alpha(0.0f).setListener(fzVar.a(fzVar.e));
        fzVar.q.animate().translationY(0.0f).setDuration(150);
        fzVar.h.a();
        fzVar.L = fzVar.N;
        fzVar.H = new gk(fzVar);
    }

    /* access modifiers changed from: private */
    public void g() {
        this.H.a();
        this.r.setTextColor(this.E);
        TextView textView = this.s;
        this.r = textView;
        textView.setTextColor(this.C);
        this.c.a(this.d);
        this.d.setVisibility(0);
        this.l.setVisibility(0);
        this.l.animate().setStartDelay(50).alpha(1.0f).setListener(null);
        this.d.animate().alpha(1.0f).setStartDelay(50).setListener(null);
        this.o.animate().alpha(0.0f).setListener(a(this.d));
        this.q.animate().translationY(0.0f).setDuration(150);
        this.g.b();
        this.L = this.M;
        if (this.z) {
            this.v.setVisibility(0);
            this.u.animate().alpha(0.0f).setListener(a(this.u));
            this.v.animate().alpha(1.0f).setListener(null);
            this.H = new gi(this);
            return;
        }
        this.H = new gj(this);
    }

    public final gr a() {
        return this.g;
    }

    public final void a(int i2) {
        this.j.animate().translationY(this.q.a()).setDuration((long) i2).setInterpolator(this.G).setListener(this.L);
        this.p.animate().alpha(0.0f).setDuration((long) i2);
    }

    public final void b(int i2) {
        if (i2 == 0) {
            this.L = this.M;
        } else if (i2 == 1) {
            this.L = this.O;
        } else if (i2 == 2) {
            this.L = this.P;
        }
    }

    public final boolean b() {
        return this.r == this.t;
    }

    public final void c() {
        if (this.c.getVisibility() == 8) {
            this.c.setVisibility(0);
            this.c.setAlpha(1.0f);
            if (this.y) {
                this.k.animate().translationY(0.0f).setStartDelay(50).setDuration(250).setInterpolator(this.F);
            }
            this.j.animate().translationY(0.0f).setDuration(300).setInterpolator(this.F).setListener(null);
            this.p.animate().alpha(0.5f).setListener(null);
            this.b.k = this.a;
            return;
        }
        a(250);
    }

    public final void d() {
        this.c.animate().alpha(0.0f).setDuration(220).setListener(this.L);
        this.p.animate().alpha(0.0f).setDuration(220);
    }

    public final void e() {
        if (this.r != this.s) {
            g();
        }
    }

    public final void f() {
    }

    public final a h() {
        return this.a;
    }
}
