package com.fastnet.browseralam.activity;

import android.graphics.drawable.Drawable;
import com.fastnet.browseralam.c.f;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

final class jo {
    final /* synthetic */ Cif a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public Drawable g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j;

    protected jo(Cif ifVar, File file, Drawable drawable) {
        this.a = ifVar;
        this.b = file.getPath();
        this.c = file.getName();
        this.g = drawable == null ? Cif.a(ifVar, this.b) : drawable;
        this.e = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").format(new Date(file.lastModified()));
        if (file.isDirectory()) {
            this.h = true;
            this.d = "Folder";
            f fVar = (f) ifVar.ab.get(this.c);
            if (fVar == null) {
                this.f = "Directory";
            } else {
                this.f = fVar.d();
            }
        } else {
            this.d = "File";
            this.f = Cif.a(file.length());
        }
    }
}
