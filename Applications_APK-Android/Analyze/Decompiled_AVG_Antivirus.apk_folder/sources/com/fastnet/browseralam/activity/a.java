package com.fastnet.browseralam.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.f;
import com.fastnet.browseralam.b.g;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.e.p;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.i.t;
import com.fastnet.browseralam.settings.x;
import com.fastnet.browseralam.view.XWebView;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class a implements View.OnLongClickListener, f {
    /* access modifiers changed from: private */
    public boolean A = true;
    /* access modifiers changed from: private */
    public boolean B;
    private boolean C;
    private boolean D;
    /* access modifiers changed from: private */
    public p E;
    /* access modifiers changed from: private */
    public AlertDialog F;
    private TextView G;
    private TextView H;
    /* access modifiers changed from: private */
    public EditText I;
    /* access modifiers changed from: private */
    public EditText J;
    /* access modifiers changed from: private */
    public PopupWindow K;
    /* access modifiers changed from: private */
    public PopupWindow L;
    /* access modifiers changed from: private */
    public x M;
    /* access modifiers changed from: private */
    public String N;
    private Bitmap O;
    private int P = aq.a(28);
    private int Q = aq.a(56);
    private int R = aq.a(192);
    private final int S = aq.a(72);
    private final int T = aq.a(8);
    /* access modifiers changed from: private */
    public Handler U = k.a();
    private Runnable V = new e(this);
    public final Animation a;
    public final Animation b;
    /* access modifiers changed from: private */
    public final BrowserActivity c;
    /* access modifiers changed from: private */
    public final Context d;
    private final g e;
    /* access modifiers changed from: private */
    public final LayoutInflater f;
    /* access modifiers changed from: private */
    public final RecyclerView g;
    /* access modifiers changed from: private */
    public final DrawerLayout h;
    /* access modifiers changed from: private */
    public LinearLayout i;
    private FrameLayout j;
    /* access modifiers changed from: private */
    public ImageView k;
    private ImageView l;
    /* access modifiers changed from: private */
    public ImageView m;
    private ImageView n;
    private ImageView o;
    private ImageView p;
    private TextView q;
    /* access modifiers changed from: private */
    public List r = new ArrayList();
    /* access modifiers changed from: private */
    public o s;
    /* access modifiers changed from: private */
    public LinearLayoutManager t;
    private com.fastnet.browseralam.c.a u;
    /* access modifiers changed from: private */
    public com.fastnet.browseralam.c.a v;
    private com.fastnet.browseralam.c.a w;
    private com.fastnet.browseralam.c.a x;
    private b y;
    private b z;

    /* JADX WARNING: Removed duplicated region for block: B:11:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x024d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a(com.fastnet.browseralam.activity.BrowserActivity r8, android.support.v4.widget.DrawerLayout r9) {
        /*
            r7 = this;
            r6 = 0
            r2 = 0
            r5 = -2
            r3 = 1
            r7.<init>()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r7.r = r0
            r7.A = r3
            r0 = 28
            int r0 = com.fastnet.browseralam.i.aq.a(r0)
            r7.P = r0
            r0 = 56
            int r0 = com.fastnet.browseralam.i.aq.a(r0)
            r7.Q = r0
            r0 = 192(0xc0, float:2.69E-43)
            int r0 = com.fastnet.browseralam.i.aq.a(r0)
            r7.R = r0
            r0 = 72
            int r0 = com.fastnet.browseralam.i.aq.a(r0)
            r7.S = r0
            r0 = 8
            int r0 = com.fastnet.browseralam.i.aq.a(r0)
            r7.T = r0
            android.os.Handler r0 = com.fastnet.browseralam.b.k.a()
            r7.U = r0
            com.fastnet.browseralam.activity.e r0 = new com.fastnet.browseralam.activity.e
            r0.<init>(r7)
            r7.V = r0
            r7.c = r8
            r7.d = r8
            r7.e = r8
            com.fastnet.browseralam.settings.x r0 = new com.fastnet.browseralam.settings.x
            r0.<init>(r8, r7)
            r7.M = r0
            com.fastnet.browseralam.activity.BrowserActivity r0 = r7.c
            android.view.LayoutInflater r0 = r0.getLayoutInflater()
            r7.f = r0
            r7.h = r9
            r0 = 2131558559(0x7f0d009f, float:1.8742437E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.LinearLayout r0 = (android.widget.LinearLayout) r0
            r7.i = r0
            android.widget.LinearLayout r0 = r7.i
            r1 = 2131558564(0x7f0d00a4, float:1.8742447E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.FrameLayout r0 = (android.widget.FrameLayout) r0
            r7.j = r0
            android.widget.LinearLayout r0 = r7.i
            r1 = 2131558566(0x7f0d00a6, float:1.8742451E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r7.k = r0
            android.widget.LinearLayout r0 = r7.i
            r1 = 2131558561(0x7f0d00a1, float:1.8742441E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r7.l = r0
            android.widget.LinearLayout r0 = r7.i
            r1 = 2131558562(0x7f0d00a2, float:1.8742443E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r7.q = r0
            android.widget.LinearLayout r0 = r7.i
            r1 = 2131558563(0x7f0d00a3, float:1.8742445E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r7.m = r0
            android.content.Context r0 = r7.d
            r1 = 2131034129(0x7f050011, float:1.7678767E38)
            android.view.animation.Animation r0 = android.view.animation.AnimationUtils.loadAnimation(r0, r1)
            r7.b = r0
            android.content.Context r0 = r7.d
            r1 = 2131034130(0x7f050012, float:1.7678769E38)
            android.view.animation.Animation r0 = android.view.animation.AnimationUtils.loadAnimation(r0, r1)
            r7.a = r0
            android.widget.LinearLayout r0 = r7.i
            r1 = 2131558571(0x7f0d00ab, float:1.8742462E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r7.p = r0
            android.widget.ImageView r0 = r7.p
            r0.setOnLongClickListener(r7)
            android.widget.LinearLayout r0 = r7.i
            r1 = 2131558570(0x7f0d00aa, float:1.874246E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r7.o = r0
            android.widget.ImageView r0 = r7.o
            r0.setOnLongClickListener(r7)
            android.widget.LinearLayout r0 = r7.i
            r1 = 2131558569(0x7f0d00a9, float:1.8742458E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r7.n = r0
            android.view.LayoutInflater r0 = r7.f
            r1 = 2130968618(0x7f04002a, float:1.7545895E38)
            android.view.View r0 = r0.inflate(r1, r6)
            r1 = 2131558581(0x7f0d00b5, float:1.8742482E38)
            android.view.View r1 = r0.findViewById(r1)
            com.fastnet.browseralam.activity.m r4 = new com.fastnet.browseralam.activity.m
            r4.<init>(r7)
            r1.setOnClickListener(r4)
            r1 = 2131558582(0x7f0d00b6, float:1.8742484E38)
            android.view.View r1 = r0.findViewById(r1)
            com.fastnet.browseralam.activity.c r4 = new com.fastnet.browseralam.activity.c
            r4.<init>(r7)
            r1.setOnClickListener(r4)
            r1 = 2131558592(0x7f0d00c0, float:1.8742504E38)
            android.view.View r1 = r0.findViewById(r1)
            com.fastnet.browseralam.activity.d r4 = new com.fastnet.browseralam.activity.d
            r4.<init>(r7)
            r1.setOnClickListener(r4)
            android.widget.PopupWindow r1 = new android.widget.PopupWindow
            r1.<init>(r0, r5, r5)
            r7.L = r1
            android.widget.PopupWindow r0 = r7.L
            r0.setFocusable(r3)
            android.widget.PopupWindow r0 = r7.L
            r0.setOutsideTouchable(r3)
            android.widget.PopupWindow r0 = r7.L
            android.graphics.drawable.ColorDrawable r1 = new android.graphics.drawable.ColorDrawable
            r1.<init>(r2)
            r0.setBackgroundDrawable(r1)
            android.view.LayoutInflater r0 = r7.f
            r1 = 2130968614(0x7f040026, float:1.7545887E38)
            android.view.View r0 = r0.inflate(r1, r6)
            android.widget.RelativeLayout r0 = (android.widget.RelativeLayout) r0
            r1 = 2131558536(0x7f0d0088, float:1.874239E38)
            android.view.View r1 = r0.findViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            r7.G = r1
            r1 = 2131558578(0x7f0d00b2, float:1.8742476E38)
            android.view.View r1 = r0.findViewById(r1)
            android.widget.EditText r1 = (android.widget.EditText) r1
            r7.I = r1
            r1 = 2131558579(0x7f0d00b3, float:1.8742478E38)
            android.view.View r1 = r0.findViewById(r1)
            android.widget.EditText r1 = (android.widget.EditText) r1
            r7.J = r1
            r1 = 2131558538(0x7f0d008a, float:1.8742395E38)
            android.view.View r1 = r0.findViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            r7.H = r1
            r1 = 2131558580(0x7f0d00b4, float:1.874248E38)
            android.view.View r1 = r0.findViewById(r1)
            com.fastnet.browseralam.activity.j r4 = new com.fastnet.browseralam.activity.j
            r4.<init>(r7)
            r1.setOnClickListener(r4)
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder
            com.fastnet.browseralam.activity.BrowserActivity r4 = r7.c
            r1.<init>(r4)
            android.app.AlertDialog r1 = r1.create()
            r7.F = r1
            android.app.AlertDialog r1 = r7.F
            r1.setView(r0)
            android.app.AlertDialog r0 = r7.F
            r0.setCanceledOnTouchOutside(r3)
            android.widget.ImageView r0 = r7.p
            r0.setOnLongClickListener(r7)
            android.widget.ImageView r0 = r7.o
            r0.setOnLongClickListener(r7)
            android.widget.ImageView r0 = r7.n
            r0.setOnLongClickListener(r7)
            android.widget.ImageView r0 = r7.n
            com.fastnet.browseralam.activity.b r1 = new com.fastnet.browseralam.activity.b
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            android.widget.ImageView r0 = r7.o
            com.fastnet.browseralam.activity.f r1 = new com.fastnet.browseralam.activity.f
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            android.widget.ImageView r0 = r7.p
            com.fastnet.browseralam.activity.g r1 = new com.fastnet.browseralam.activity.g
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            android.widget.ImageView r0 = r7.m
            com.fastnet.browseralam.activity.h r1 = new com.fastnet.browseralam.activity.h
            r1.<init>(r7)
            r0.setOnClickListener(r1)
            android.support.v4.widget.DrawerLayout r0 = r7.h
            r1 = 2131558565(0x7f0d00a5, float:1.874245E38)
            android.view.View r0 = r0.findViewById(r1)
            android.support.v7.widget.RecyclerView r0 = (android.support.v7.widget.RecyclerView) r0
            r7.g = r0
            android.support.v7.widget.RecyclerView r0 = r7.g
            r0.a(r3)
            android.support.v7.widget.LinearLayoutManager r0 = new android.support.v7.widget.LinearLayoutManager
            r0.<init>()
            r7.t = r0
            android.support.v7.widget.RecyclerView r0 = r7.g
            android.support.v7.widget.LinearLayoutManager r1 = r7.t
            r0.a(r1)
            com.fastnet.browseralam.activity.o r0 = new com.fastnet.browseralam.activity.o
            r0.<init>(r7)
            r7.s = r0
            com.fastnet.browseralam.c.a r0 = new com.fastnet.browseralam.c.a
            java.util.List r1 = r7.r
            com.fastnet.browseralam.activity.o r4 = r7.s
            r0.<init>(r1, r4)
            r7.v = r0
            r7.u = r0
            com.fastnet.browseralam.c.a r0 = new com.fastnet.browseralam.c.a
            com.fastnet.browseralam.c.a r1 = r7.u
            java.lang.String r4 = "Incognito"
            r0.<init>(r1, r4)
            r7.w = r0
            android.support.v7.widget.RecyclerView r0 = r7.g
            com.fastnet.browseralam.activity.o r1 = r7.s
            r0.a(r1)
            android.support.v7.widget.RecyclerView r0 = r7.g
            com.fastnet.browseralam.activity.u r1 = new com.fastnet.browseralam.activity.u
            r1.<init>(r7)
            r0.a(r1)
            com.fastnet.browseralam.activity.BrowserActivity r0 = r7.c
            android.graphics.Bitmap r0 = r0.t()
            r7.O = r0
            com.fastnet.browseralam.h.a.a()
            int r0 = com.fastnet.browseralam.h.a.B()
            r1 = 3
            if (r0 == r1) goto L_0x023f
            com.fastnet.browseralam.h.a.a()
            boolean r0 = com.fastnet.browseralam.h.a.o()
            if (r0 == 0) goto L_0x024b
        L_0x023f:
            r0 = r3
        L_0x0240:
            r7.D = r0
            com.fastnet.browseralam.activity.BrowserActivity r0 = r7.c
            java.io.File r0 = r0.getFilesDir()
            if (r0 != 0) goto L_0x024d
        L_0x024a:
            return
        L_0x024b:
            r0 = r2
            goto L_0x0240
        L_0x024d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r0 = r0.getPath()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "/Bookmarks/"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r7.N = r0
            java.io.File r0 = new java.io.File
            java.lang.String r1 = r7.N
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x024a
            r0.mkdir()
            goto L_0x024a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.activity.a.<init>(com.fastnet.browseralam.activity.BrowserActivity, android.support.v4.widget.DrawerLayout):void");
    }

    private static String a(int i2) {
        String str = "";
        for (int i3 = 0; i3 < i2; i3++) {
            str = str + " ";
        }
        return str;
    }

    private void a(List list, int i2, boolean z2, BufferedWriter bufferedWriter) {
        for (int i3 = 0; i3 < list.size(); i3++) {
            if (z2) {
                z2 = false;
            } else {
                b bVar = (b) list.get(i3);
                if (bVar.c()) {
                    bufferedWriter.write(a(i2) + "+" + bVar.a());
                    bufferedWriter.newLine();
                    a(bVar.e().f(), i2 + 1, true, bufferedWriter);
                } else {
                    bufferedWriter.write(a(i2) + "-" + bVar.a() + "::" + bVar.b() + "::" + bVar.h());
                    bufferedWriter.newLine();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002e A[SYNTHETIC, Splitter:B:15:0x002e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b(java.io.File r5) {
        /*
            r0 = 0
            r3 = 0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x002a }
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ IOException -> 0x002a }
            r1.<init>(r5)     // Catch:{ IOException -> 0x002a }
            r2.<init>(r1)     // Catch:{ IOException -> 0x002a }
            java.lang.String r1 = r2.readLine()     // Catch:{ IOException -> 0x0037 }
            if (r1 == 0) goto L_0x0024
            r3 = 0
            char r3 = r1.charAt(r3)     // Catch:{ IOException -> 0x0037 }
            r4 = 43
            if (r3 == r4) goto L_0x0025
            r3 = 0
            char r1 = r1.charAt(r3)     // Catch:{ IOException -> 0x0037 }
            r3 = 45
            if (r1 == r3) goto L_0x0025
        L_0x0024:
            return r0
        L_0x0025:
            r2.close()     // Catch:{ IOException -> 0x0037 }
            r0 = 1
            goto L_0x0024
        L_0x002a:
            r1 = move-exception
            r2 = r3
        L_0x002c:
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0031:
            r1.printStackTrace()
            goto L_0x0024
        L_0x0035:
            r2 = move-exception
            goto L_0x0031
        L_0x0037:
            r1 = move-exception
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.activity.a.b(java.io.File):boolean");
    }

    /* access modifiers changed from: private */
    public void d() {
        this.L.showAtLocation(this.m, 51, this.i.getWidth() - this.R, this.D ? this.P : this.Q + this.P);
    }

    public final void a() {
        this.h.e(this.i);
    }

    public final void a(b bVar) {
        if (bVar != null) {
            if (!bVar.c()) {
                this.e.D().c(bVar.b());
                this.h.f(this.i);
                return;
            }
            boolean z2 = bVar.d() == null;
            this.A = z2;
            if (z2) {
                this.v.a(false);
                this.v = this.u;
                this.v.a(true);
            } else {
                this.v.a(false);
                this.v = bVar.e();
                this.v.a(true);
            }
            this.r = this.v.f();
            this.s.c();
            t.a(this.r, this.N, this.O, this.s, this.U);
        }
    }

    public final void a(XWebView xWebView, Bitmap bitmap) {
        b bVar = new b(xWebView.x(), xWebView.y(), "", bitmap);
        this.v.a(bVar);
        if (!xWebView.L()) {
            xWebView.a(new i(this, bVar));
        } else {
            bVar.c(xWebView.M());
            t.a(new File(this.N + xWebView.M()), bitmap);
        }
        this.B = true;
        aq.a(this.c, "Penanda ditambahkan");
    }

    public final boolean a(File file) {
        this.B = false;
        List f2 = this.u.f();
        if (file == null) {
            try {
                if (this.N == null) {
                    this.N = this.c.getFilesDir().getPath() + "/Bookmarks/";
                }
                File file2 = new File(this.N);
                if (!file2.exists()) {
                    file2.mkdir();
                }
                file = new File(file2, ".bookmarks.txt");
            } catch (IOException e2) {
                e2.printStackTrace();
                return false;
            }
        }
        File file3 = new File(this.N, ".bookmarks.old.txt");
        if (file.exists()) {
            file.renameTo(file3);
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        if (!f2.contains(this.w)) {
            f2.add(this.w);
        }
        a(f2, 0, false, bufferedWriter);
        f2.remove(this.w);
        bufferedWriter.close();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0096, code lost:
        if (r1.charAt(r4) != '+') goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0098, code lost:
        r8 = r1.substring(r4 + 1, r1.length());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a2, code lost:
        if (r4 != 0) goto L_0x00c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a4, code lost:
        r0 = new com.fastnet.browseralam.c.a(r6, r8);
        r6.b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00b0, code lost:
        if (r7.size() != 0) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b2, code lost:
        r7.add(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00bb, code lost:
        r7.set(0, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ca, code lost:
        if (r4 != r7.size()) goto L_0x00e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cc, code lost:
        r9 = new com.fastnet.browseralam.c.a((com.fastnet.browseralam.c.a) r7.get(r4 - 1), r8);
        ((com.fastnet.browseralam.c.a) r7.get(r4 - 1)).b(r9);
        r7.add(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e8, code lost:
        r9 = new com.fastnet.browseralam.c.a((com.fastnet.browseralam.c.a) r7.get(r4 - 1), r8);
        ((com.fastnet.browseralam.c.a) r7.get(r4 - 1)).b(r9);
        r7.set(r4, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0108, code lost:
        if (r1.charAt(r4) != '-') goto L_0x00b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x010a, code lost:
        r0 = r1.substring(r4 + 1, r1.length()).split("::");
        r1 = new java.lang.String[3];
        java.lang.System.arraycopy(r0, 0, r1, 0, r0.length);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0123, code lost:
        if (r4 != 0) goto L_0x0139;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0125, code lost:
        r6.a(new com.fastnet.browseralam.c.b(r1[0], r1[1], r1[2], null));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0139, code lost:
        ((com.fastnet.browseralam.c.a) r7.get(r4 - 1)).a(new com.fastnet.browseralam.c.b(r1[0], r1[1], r1[2], null));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.io.File r14, boolean r15) {
        /*
            r13 = this;
            r12 = 45
            r11 = 43
            r3 = 1
            r2 = 0
            if (r14 != 0) goto L_0x0203
            java.lang.String r0 = r13.N
            if (r0 != 0) goto L_0x003b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            com.fastnet.browseralam.activity.BrowserActivity r1 = r13.c
            java.io.File r1 = r1.getFilesDir()
            java.lang.String r1 = r1.getPath()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/Bookmarks/"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r13.N = r0
            java.io.File r0 = new java.io.File
            java.lang.String r1 = r13.N
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x003b
            r0.mkdir()
        L_0x003b:
            java.io.File r0 = new java.io.File
            java.lang.String r1 = r13.N
            java.lang.String r4 = ".bookmarks.txt"
            r0.<init>(r1, r4)
            com.fastnet.browseralam.c.a r1 = new com.fastnet.browseralam.c.a
            java.util.List r4 = r13.r
            com.fastnet.browseralam.activity.o r5 = r13.s
            r1.<init>(r4, r5)
            r13.u = r1
        L_0x004f:
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00c0 }
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ IOException -> 0x00c0 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x00c0 }
            r5.<init>(r1)     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r0 = r5.readLine()     // Catch:{ IOException -> 0x00c0 }
            if (r0 == 0) goto L_0x006d
            r1 = 0
            char r1 = r0.charAt(r1)     // Catch:{ IOException -> 0x00c0 }
            if (r1 == r11) goto L_0x006f
            r1 = 0
            char r1 = r0.charAt(r1)     // Catch:{ IOException -> 0x00c0 }
            if (r1 == r12) goto L_0x006f
        L_0x006d:
            r0 = r2
        L_0x006e:
            return r0
        L_0x006f:
            com.fastnet.browseralam.c.a r6 = new com.fastnet.browseralam.c.a     // Catch:{ IOException -> 0x00c0 }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ IOException -> 0x00c0 }
            r1.<init>()     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.activity.o r4 = r13.s     // Catch:{ IOException -> 0x00c0 }
            r6.<init>(r1, r4)     // Catch:{ IOException -> 0x00c0 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ IOException -> 0x00c0 }
            r7.<init>()     // Catch:{ IOException -> 0x00c0 }
            r1 = r0
        L_0x0081:
            if (r1 == 0) goto L_0x015a
            r4 = r2
        L_0x0084:
            int r0 = r1.length()     // Catch:{ IOException -> 0x00c0 }
            if (r4 >= r0) goto L_0x00b5
            char r0 = r1.charAt(r4)     // Catch:{ IOException -> 0x00c0 }
            r8 = 32
            if (r0 == r8) goto L_0x0155
            char r0 = r1.charAt(r4)     // Catch:{ IOException -> 0x00c0 }
            if (r0 != r11) goto L_0x0104
            int r0 = r4 + 1
            int r8 = r1.length()     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r8 = r1.substring(r0, r8)     // Catch:{ IOException -> 0x00c0 }
            if (r4 != 0) goto L_0x00c6
            com.fastnet.browseralam.c.a r0 = new com.fastnet.browseralam.c.a     // Catch:{ IOException -> 0x00c0 }
            r0.<init>(r6, r8)     // Catch:{ IOException -> 0x00c0 }
            r6.b(r0)     // Catch:{ IOException -> 0x00c0 }
            int r1 = r7.size()     // Catch:{ IOException -> 0x00c0 }
            if (r1 != 0) goto L_0x00bb
            r7.add(r0)     // Catch:{ IOException -> 0x00c0 }
        L_0x00b5:
            java.lang.String r0 = r5.readLine()     // Catch:{ IOException -> 0x00c0 }
            r1 = r0
            goto L_0x0081
        L_0x00bb:
            r1 = 0
            r7.set(r1, r0)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00b5
        L_0x00c0:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x006e
        L_0x00c6:
            int r0 = r7.size()     // Catch:{ IOException -> 0x00c0 }
            if (r4 != r0) goto L_0x00e8
            int r0 = r4 + -1
            java.lang.Object r0 = r7.get(r0)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = (com.fastnet.browseralam.c.a) r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r9 = new com.fastnet.browseralam.c.a     // Catch:{ IOException -> 0x00c0 }
            int r1 = r4 + -1
            java.lang.Object r1 = r7.get(r1)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r1 = (com.fastnet.browseralam.c.a) r1     // Catch:{ IOException -> 0x00c0 }
            r9.<init>(r1, r8)     // Catch:{ IOException -> 0x00c0 }
            r0.b(r9)     // Catch:{ IOException -> 0x00c0 }
            r7.add(r9)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00b5
        L_0x00e8:
            int r0 = r4 + -1
            java.lang.Object r0 = r7.get(r0)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = (com.fastnet.browseralam.c.a) r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r9 = new com.fastnet.browseralam.c.a     // Catch:{ IOException -> 0x00c0 }
            int r1 = r4 + -1
            java.lang.Object r1 = r7.get(r1)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r1 = (com.fastnet.browseralam.c.a) r1     // Catch:{ IOException -> 0x00c0 }
            r9.<init>(r1, r8)     // Catch:{ IOException -> 0x00c0 }
            r0.b(r9)     // Catch:{ IOException -> 0x00c0 }
            r7.set(r4, r9)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00b5
        L_0x0104:
            char r0 = r1.charAt(r4)     // Catch:{ IOException -> 0x00c0 }
            if (r0 != r12) goto L_0x00b5
            int r0 = r4 + 1
            int r8 = r1.length()     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r0 = r1.substring(r0, r8)     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r1 = "::"
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ IOException -> 0x00c0 }
            r1 = 3
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ IOException -> 0x00c0 }
            r8 = 0
            r9 = 0
            int r10 = r0.length     // Catch:{ IOException -> 0x00c0 }
            java.lang.System.arraycopy(r0, r8, r1, r9, r10)     // Catch:{ IOException -> 0x00c0 }
            if (r4 != 0) goto L_0x0139
            com.fastnet.browseralam.c.b r0 = new com.fastnet.browseralam.c.b     // Catch:{ IOException -> 0x00c0 }
            r4 = 0
            r4 = r1[r4]     // Catch:{ IOException -> 0x00c0 }
            r8 = 1
            r8 = r1[r8]     // Catch:{ IOException -> 0x00c0 }
            r9 = 2
            r1 = r1[r9]     // Catch:{ IOException -> 0x00c0 }
            r9 = 0
            r0.<init>(r4, r8, r1, r9)     // Catch:{ IOException -> 0x00c0 }
            r6.a(r0)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00b5
        L_0x0139:
            int r0 = r4 + -1
            java.lang.Object r0 = r7.get(r0)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = (com.fastnet.browseralam.c.a) r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.b r4 = new com.fastnet.browseralam.c.b     // Catch:{ IOException -> 0x00c0 }
            r8 = 0
            r8 = r1[r8]     // Catch:{ IOException -> 0x00c0 }
            r9 = 1
            r9 = r1[r9]     // Catch:{ IOException -> 0x00c0 }
            r10 = 2
            r1 = r1[r10]     // Catch:{ IOException -> 0x00c0 }
            r10 = 0
            r4.<init>(r8, r9, r1, r10)     // Catch:{ IOException -> 0x00c0 }
            r0.a(r4)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00b5
        L_0x0155:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0084
        L_0x015a:
            r5.close()     // Catch:{ IOException -> 0x00c0 }
            java.util.List r0 = r6.f()     // Catch:{ IOException -> 0x00c0 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ IOException -> 0x00c0 }
        L_0x0165:
            boolean r0 = r1.hasNext()     // Catch:{ IOException -> 0x00c0 }
            if (r0 == 0) goto L_0x0186
            java.lang.Object r0 = r1.next()     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.b r0 = (com.fastnet.browseralam.c.b) r0     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r4 = r0.a()     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r5 = "Incognito"
            boolean r4 = r4.equals(r5)     // Catch:{ IOException -> 0x00c0 }
            if (r4 == 0) goto L_0x0165
            com.fastnet.browseralam.c.a r1 = r0.e()     // Catch:{ IOException -> 0x00c0 }
            r13.w = r1     // Catch:{ IOException -> 0x00c0 }
            r6.d(r0)     // Catch:{ IOException -> 0x00c0 }
        L_0x0186:
            com.fastnet.browseralam.c.a r0 = r13.w     // Catch:{ IOException -> 0x00c0 }
            if (r0 != 0) goto L_0x0195
            com.fastnet.browseralam.c.a r0 = new com.fastnet.browseralam.c.a     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r1 = r13.u     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r4 = "Incognito"
            r0.<init>(r1, r4)     // Catch:{ IOException -> 0x00c0 }
            r13.w = r0     // Catch:{ IOException -> 0x00c0 }
        L_0x0195:
            if (r14 == 0) goto L_0x01f2
            java.util.List r0 = r6.f()     // Catch:{ IOException -> 0x00c0 }
            int r0 = r0.size()     // Catch:{ IOException -> 0x00c0 }
            if (r0 != 0) goto L_0x01a4
            r0 = r2
            goto L_0x006e
        L_0x01a4:
            if (r15 == 0) goto L_0x01c0
            java.util.List r0 = r6.f()     // Catch:{ IOException -> 0x00c0 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ IOException -> 0x00c0 }
        L_0x01ae:
            boolean r0 = r1.hasNext()     // Catch:{ IOException -> 0x00c0 }
            if (r0 == 0) goto L_0x01c2
            java.lang.Object r0 = r1.next()     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.b r0 = (com.fastnet.browseralam.c.b) r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r4 = r13.u     // Catch:{ IOException -> 0x00c0 }
            r4.b(r0)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x01ae
        L_0x01c0:
            r13.u = r6     // Catch:{ IOException -> 0x00c0 }
        L_0x01c2:
            r0 = 1
            r13.B = r0     // Catch:{ IOException -> 0x00c0 }
        L_0x01c5:
            com.fastnet.browseralam.c.a r0 = r13.u     // Catch:{ IOException -> 0x00c0 }
            r1 = 1
            r0.a(r1)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = r13.v     // Catch:{ IOException -> 0x00c0 }
            r1 = 0
            r0.a(r1)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = r13.u     // Catch:{ IOException -> 0x00c0 }
            r13.v = r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = r13.u     // Catch:{ IOException -> 0x00c0 }
            java.util.List r0 = r0.f()     // Catch:{ IOException -> 0x00c0 }
            r13.r = r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.activity.o r0 = r13.s     // Catch:{ IOException -> 0x00c0 }
            r0.c()     // Catch:{ IOException -> 0x00c0 }
            if (r14 == 0) goto L_0x01f5
            com.fastnet.browseralam.activity.BrowserActivity r0 = r13.c     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r1 = r13.N     // Catch:{ IOException -> 0x00c0 }
            java.util.List r4 = r6.f()     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.i.t.a(r0, r1, r4)     // Catch:{ IOException -> 0x00c0 }
        L_0x01ef:
            r0 = r3
            goto L_0x006e
        L_0x01f2:
            r13.u = r6     // Catch:{ IOException -> 0x00c0 }
            goto L_0x01c5
        L_0x01f5:
            java.util.List r0 = r13.r     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r1 = r13.N     // Catch:{ IOException -> 0x00c0 }
            android.graphics.Bitmap r4 = r13.O     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.activity.o r5 = r13.s     // Catch:{ IOException -> 0x00c0 }
            android.os.Handler r6 = r13.U     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.i.t.a(r0, r1, r4, r5, r6)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x01ef
        L_0x0203:
            r0 = r14
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.activity.a.a(java.io.File, boolean):boolean");
    }

    public final com.fastnet.browseralam.c.a b() {
        return this.u;
    }

    public final void b(b bVar) {
        if (bVar == this.w) {
            bVar.c(this.u);
            return;
        }
        this.x = bVar.d();
        this.z = bVar;
        this.y = null;
        this.B = true;
    }

    public final void c(b bVar) {
        if (bVar == this.w) {
            bVar.c(this.u);
            this.C = false;
            return;
        }
        this.z = null;
        this.x = bVar.d();
        this.y = bVar;
        this.B = true;
    }

    public final boolean c() {
        return this.B;
    }

    public final void d(b bVar) {
        this.I.setText(bVar.a());
        if (!bVar.c()) {
            this.J.setHint(this.c.getResources().getString(R.string.hint_url));
            this.J.setText(bVar.b());
            this.J.setVisibility(0);
            this.G.setText("Edit Bookmark");
        } else {
            this.J.setVisibility(8);
            this.G.setText("Edit Folder");
        }
        this.H.setOnClickListener(new k(this, bVar));
        this.F.show();
        this.I.selectAll();
        k.a(new l(this));
    }

    public final boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.action_previous_folder:
                if (this.y == null || this.x == null) {
                    if (!(this.z == null || this.x == null)) {
                        this.z.d().c(this.z);
                        this.x.a(this.z);
                        aq.a(this.c, "moved bookmark item");
                        this.z = null;
                        this.y = null;
                        this.x = null;
                        break;
                    }
                } else {
                    this.x.a(this.y);
                    aq.a(this.c, "restored bookmark item");
                    this.y = null;
                    this.x = null;
                    break;
                }
                break;
            case R.id.action_bookmark_page:
                d();
                break;
            case R.id.new_folder:
                if (this.w != null && !this.C) {
                    this.u.a(this.w);
                    this.C = true;
                    break;
                }
        }
        return true;
    }
}
