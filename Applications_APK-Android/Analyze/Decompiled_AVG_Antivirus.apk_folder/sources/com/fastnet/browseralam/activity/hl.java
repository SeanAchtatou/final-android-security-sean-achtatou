package com.fastnet.browseralam.activity;

import android.support.v7.widget.cf;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.e.p;

final class hl extends cf implements p {
    final RelativeLayout l;
    final TextView m;
    final ImageView n;
    final ImageView o;
    boolean p;
    boolean q;
    b r;
    final /* synthetic */ hk s;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hl(hk hkVar, View view) {
        super(view);
        this.s = hkVar;
        this.o = (ImageView) view.findViewById(R.id.bookmarkIcon);
        this.m = (TextView) view.findViewById(R.id.textTab);
        this.n = (ImageView) view.findViewById(R.id.menuIcon);
        this.l = (RelativeLayout) view.findViewById(R.id.tabItem);
        view.setTag(this);
        if (hkVar.d.V) {
            this.n.setOnClickListener(hkVar.f);
        } else {
            this.n.setVisibility(8);
        }
    }

    public final int t() {
        return d();
    }

    public final boolean u() {
        return this.q;
    }

    public final boolean v() {
        return this.p;
    }

    public final void w() {
        this.o.setImageDrawable(this.s.c);
    }

    public final void x() {
        if (this.p) {
            this.o.setImageDrawable(this.s.b);
        }
    }

    public final b y() {
        return this.r;
    }
}
