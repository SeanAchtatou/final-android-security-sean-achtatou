package com.fastnet.browseralam.activity;

import android.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import java.io.File;

final class fd implements View.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ int b;
    final /* synthetic */ fl c;
    final /* synthetic */ AlertDialog d;
    final /* synthetic */ ey e;

    fd(ey eyVar, EditText editText, int i, fl flVar, AlertDialog alertDialog) {
        this.e = eyVar;
        this.a = editText;
        this.b = i;
        this.c = flVar;
        this.d = alertDialog;
    }

    public final void onClick(View view) {
        String trim = this.a.getText().toString().trim();
        if (this.b == 0) {
            File file = new File(this.e.o.b + "/" + trim);
            if (!file.exists()) {
                file.mkdir();
                int size = this.e.l.size() - 1;
                while (true) {
                    if (size <= 0) {
                        break;
                    } else if (((fl) this.e.l.get(size)).f) {
                        this.e.l.add(size + 1, new fl(this.e, file, this.e.p));
                        this.e.h.c_(size + 1);
                        break;
                    } else {
                        size--;
                    }
                }
            }
        } else if (!trim.equals("") && (this.b == 1 || this.b == 2)) {
            File file2 = new File(this.c.b);
            file2.renameTo(new File(file2.getParent() + "/" + trim));
            String unused = this.c.c = trim;
            this.e.h.b(this.e.l.indexOf(this.c));
        }
        ((InputMethodManager) this.e.f.getSystemService("input_method")).hideSoftInputFromWindow(this.a.getWindowToken(), 0);
        this.d.dismiss();
    }
}
