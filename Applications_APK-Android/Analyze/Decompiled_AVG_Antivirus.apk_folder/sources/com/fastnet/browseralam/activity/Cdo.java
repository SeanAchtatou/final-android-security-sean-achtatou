package com.fastnet.browseralam.activity;

import android.support.v4.widget.q;
import android.view.View;
import com.fastnet.browseralam.b.a;

/* renamed from: com.fastnet.browseralam.activity.do  reason: invalid class name */
final class Cdo implements q {
    a a = new dp(this);
    a b = new dq(this);
    final /* synthetic */ BrowserActivity c;

    Cdo(BrowserActivity browserActivity) {
        this.c = browserActivity;
    }

    public final void a(View view) {
        if (view == this.c.u) {
            this.c.n.a(1, this.c.t);
            this.c.k = this.a;
            return;
        }
        this.c.n.a(1, this.c.u);
        this.c.k = this.b;
    }

    public final void b(View view) {
        if (view == this.c.u) {
            this.c.n.a(0, this.c.t);
        } else {
            this.c.n.a(0, this.c.u);
        }
        this.c.k = this.c.j;
    }
}
