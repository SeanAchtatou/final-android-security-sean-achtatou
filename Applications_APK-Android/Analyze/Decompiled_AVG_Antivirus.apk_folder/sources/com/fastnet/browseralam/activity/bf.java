package com.fastnet.browseralam.activity;

import android.view.KeyEvent;
import android.widget.TextView;

final class bf implements TextView.OnEditorActionListener {
    final /* synthetic */ BrowserActivity a;

    bf(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 2 && i != 6 && i != 5 && i != 4 && i != 3) {
            return false;
        }
        this.a.L.a(this.a.D = this.a.z.getText().toString());
        return true;
    }
}
