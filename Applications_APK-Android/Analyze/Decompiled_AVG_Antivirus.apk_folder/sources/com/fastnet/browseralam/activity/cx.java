package com.fastnet.browseralam.activity;

import android.content.Intent;
import android.view.View;
import android.widget.CheckedTextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.c;
import com.fastnet.browseralam.settings.SettingsActivity;

final class cx implements View.OnClickListener {
    final /* synthetic */ BrowserActivity a;
    private CheckedTextView b;

    cx(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [?[OBJECT, ARRAY], int, int, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.action_bookmark_page:
                this.a.X.a(this.a.L, this.a.L.C());
                break;
            case R.id.page_info:
                BrowserActivity.W(this.a);
                break;
            case R.id.bookmark_page:
                if (!this.a.L.y().startsWith("file://")) {
                    this.a.X.a(this.a.L, this.a.L.C());
                    break;
                }
                break;
            case R.id.action_forward:
                this.a.L.u();
                break;
            case R.id.action_new_tab:
                this.a.a((String) null, false, -1, false);
                break;
            case R.id.incognito_tab:
                this.a.a((String) null, false, -1, true);
                break;
            case R.id.action_bookmarks:
                this.a.X.a();
                break;
            case R.id.action_history:
                this.a.R();
                break;
            case R.id.action_find:
                this.a.p.removeView(this.a.w);
                this.a.p.addView(this.a.w);
                this.a.z.requestFocus();
                this.a.cm.postDelayed(new dt(this.a), 150);
                break;
            case R.id.action_desktop_mode:
                this.b = (CheckedTextView) view;
                if (this.b.isChecked()) {
                    this.b.setChecked(false);
                    this.a.L.e(1);
                } else {
                    this.b.setChecked(true);
                    this.a.L.e(2);
                }
                this.a.L.m();
                break;
            case R.id.action_reading_mode:
                Intent intent = new Intent(this.a, ReadingActivity.class);
                intent.putExtra("ReadingUrl", this.a.L.y());
                this.a.startActivity(intent);
                break;
            case R.id.action_rendering_mode:
                BrowserActivity.X(this.a);
                break;
            case R.id.action_share:
                Intent intent2 = new Intent("android.intent.action.SEND");
                intent2.setType("text/plain");
                if (BrowserActivity.bd < 21) {
                    intent2.addFlags(524288);
                } else {
                    intent2.addFlags(524288);
                }
                intent2.putExtra("android.intent.extra.SUBJECT", this.a.L.x());
                intent2.putExtra("android.intent.extra.TEXT", this.a.L.y());
                this.a.startActivity(Intent.createChooser(intent2, "Share link"));
                break;
            case R.id.action_update:
                c.a(this.a.ao);
                break;
            case R.id.action_settings:
                boolean unused = this.a.bp = true;
                this.a.startActivity(new Intent(this.a, SettingsActivity.class));
                break;
        }
        this.a.F.dismiss();
    }
}
