package com.fastnet.browseralam.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.a;
import com.fastnet.browseralam.b.h;
import com.fastnet.browseralam.b.i;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.b.l;
import com.fastnet.browseralam.g.n;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.Background;
import com.fastnet.browseralam.view.DrawerFrame;
import com.fastnet.browseralam.view.XWebView;
import java.util.List;

public final class y extends h implements i {
    private static final ViewGroup.LayoutParams aK = new ViewGroup.LayoutParams(-1, -1);
    /* access modifiers changed from: private */
    public RelativeLayout A;
    /* access modifiers changed from: private */
    public View B;
    private View C;
    private Background D;
    /* access modifiers changed from: private */
    public TextView E;
    /* access modifiers changed from: private */
    public TextView F;
    /* access modifiers changed from: private */
    public TextView G;
    /* access modifiers changed from: private */
    public TextView H;
    /* access modifiers changed from: private */
    public TextView I;
    /* access modifiers changed from: private */
    public TextView J;
    /* access modifiers changed from: private */
    public ImageView K;
    /* access modifiers changed from: private */
    public ImageView L;
    private ImageView M;
    /* access modifiers changed from: private */
    public ImageView N;
    private ImageView O;
    private ImageView P;
    private ImageView Q;
    private ba R = new ba(this, (byte) 0);
    /* access modifiers changed from: private */
    public Bitmap S;
    /* access modifiers changed from: private */
    public boolean T = true;
    private boolean U;
    private boolean V;
    private boolean W;
    private int X;
    private int Y;
    private int Z;
    a a = new aj(this);
    /* access modifiers changed from: private */
    public ay aA;
    /* access modifiers changed from: private */
    public ay aB = new as(this);
    /* access modifiers changed from: private */
    public ay aC = new ae(this);
    private ay aD = new af(this);
    private ay aE = new ag(this);
    private ay aF = new ah(this);
    private int aG = 0;
    private String aH = "";
    private String aI = "";
    private Runnable aJ = new ai(this);
    private View.OnAttachStateChangeListener aL = new am(this);
    private int aa;
    private int ab;
    /* access modifiers changed from: private */
    public int ac;
    /* access modifiers changed from: private */
    public int ad;
    private int ae;
    private int af;
    private int ag;
    /* access modifiers changed from: private */
    public int ah;
    private int ai;
    /* access modifiers changed from: private */
    public int aj;
    /* access modifiers changed from: private */
    public int ak = aq.a(120);
    /* access modifiers changed from: private */
    public int al;
    private int am = aq.a(181);
    private int an = aq.a(171);
    private final int ao = aq.a(79);
    private final int ap = aq.a(98);
    /* access modifiers changed from: private */
    public final Handler aq = k.a();
    private final bd ar = new bd(this, (byte) 0);
    private final az as = new az(this, (byte) 0);
    private final DecelerateInterpolator at = new DecelerateInterpolator(2.0f);
    private final AccelerateDecelerateInterpolator au = new AccelerateDecelerateInterpolator();
    private ax av = new z(this);
    private float aw;
    /* access modifiers changed from: private */
    public View.OnClickListener ax = new ak(this);
    /* access modifiers changed from: private */
    public View.OnLongClickListener ay = new aq(this);
    private View.OnClickListener az = new ar(this);
    /* access modifiers changed from: private */
    public final BrowserActivity b;
    /* access modifiers changed from: private */
    public final DrawerFrame c;
    private final RecyclerView d;
    /* access modifiers changed from: private */
    public final RecyclerView e;
    /* access modifiers changed from: private */
    public final RecyclerView f;
    /* access modifiers changed from: private */
    public final RecyclerView g;
    private final RecyclerView h;
    /* access modifiers changed from: private */
    public final RecyclerView i;
    /* access modifiers changed from: private */
    public final com.fastnet.browseralam.h.a j = com.fastnet.browseralam.h.a.a();
    private final gr k;
    /* access modifiers changed from: private */
    public final Cif l;
    /* access modifiers changed from: private */
    public final List m;
    private final List n;
    /* access modifiers changed from: private */
    public com.fastnet.browseralam.g.a o;
    /* access modifiers changed from: private */
    public com.fastnet.browseralam.g.a p;
    /* access modifiers changed from: private */
    public com.fastnet.browseralam.g.a q;
    /* access modifiers changed from: private */
    public n r;
    /* access modifiers changed from: private */
    public n s;
    private n t;
    /* access modifiers changed from: private */
    public FrameLayout u;
    /* access modifiers changed from: private */
    public FrameLayout v;
    /* access modifiers changed from: private */
    public FrameLayout w;
    /* access modifiers changed from: private */
    public RelativeLayout x;
    private RelativeLayout y;
    /* access modifiers changed from: private */
    public RelativeLayout z;

    public y(BrowserActivity browserActivity, FrameLayout frameLayout, List list, List list2) {
        this.b = browserActivity;
        this.v = frameLayout;
        this.m = list;
        this.n = list2;
        this.c = (DrawerFrame) frameLayout.findViewById(R.id.bottomDrawerFrame);
        this.C = this.c.findViewById(R.id.shadowOverlay);
        this.x = (RelativeLayout) this.c.findViewById(R.id.bottom_drawer);
        int a2 = this.b.getResources().getDisplayMetrics().heightPixels - aq.a(26);
        this.ag = a2;
        this.ah = this.ag + aq.a(3);
        this.ai = this.ag - aq.a(318);
        this.aj = this.ai + aq.a(22);
        this.x.setTranslationY((float) a2);
        this.D = (Background) this.x.findViewById(R.id.background);
        this.D.setTranslationY((float) (a2 - aq.a(120)));
        this.d = (RecyclerView) this.x.findViewById(R.id.list);
        this.e = (RecyclerView) this.x.findViewById(R.id.incognitolist);
        this.f = (RecyclerView) this.c.findViewById(R.id.horizontal_tabview);
        this.g = (RecyclerView) this.c.findViewById(R.id.horizontal_tabview_incognito);
        this.J = (TextView) this.c.findViewById(R.id.card_view_title);
        this.T = !com.fastnet.browseralam.h.a.D();
        this.V = com.fastnet.browseralam.h.a.C();
        if (this.V) {
            this.ab = R.layout.card_item_short;
        } else {
            this.ab = R.layout.card_item;
        }
        this.c.a(this, this.b, this.d, this.D);
        TextView textView = (TextView) this.x.findViewById(R.id.normal_tabs);
        this.F = textView;
        this.E = textView;
        this.G = (TextView) this.x.findViewById(R.id.incognito_tabs);
        this.H = (TextView) this.x.findViewById(R.id.bookmarks_textview);
        this.I = (TextView) this.x.findViewById(R.id.files_textview);
        this.F.setOnClickListener(this.az);
        this.G.setOnClickListener(this.az);
        this.H.setOnClickListener(this.az);
        this.I.setOnClickListener(this.az);
        this.y = (RelativeLayout) this.v.findViewById(R.id.bottom_toolbar);
        this.K = (ImageView) this.y.findViewById(R.id.new_tab_button);
        this.L = (ImageView) this.y.findViewById(R.id.back_button);
        this.M = (ImageView) this.y.findViewById(R.id.forward_button);
        this.Q = (ImageView) this.y.findViewById(R.id.tabs_button);
        this.N = (ImageView) this.y.findViewById(R.id.bookmark_button);
        this.O = (ImageView) this.y.findViewById(R.id.bookmark_button2);
        this.P = (ImageView) this.y.findViewById(R.id.menu_button);
        this.K.setOnLongClickListener(this.ay);
        this.K.setOnClickListener(this.ax);
        this.Q.setOnClickListener(this.ax);
        this.P.setOnClickListener(this.ax);
        this.L.setOnClickListener(this.ax);
        this.M.setOnClickListener(this.ax);
        this.Z = this.b.getResources().getColor(R.color.gray_dark);
        this.aa = this.b.getResources().getColor(R.color.gray_medium_dark);
        this.ac = this.b.getResources().getDisplayMetrics().widthPixels;
        this.ae = a2 - aq.a(56);
        this.af = a2 - aq.a(108);
        this.ad = this.ae;
        this.al = this.am;
        this.w = (FrameLayout) this.v.findViewById(R.id.background_frame);
        this.u = (FrameLayout) this.v.findViewById(R.id.webview_frame);
        if (!this.T) {
            this.u.addOnAttachStateChangeListener(this.aL);
        } else {
            this.v.removeView(this.w);
            this.w = null;
        }
        this.i = (RecyclerView) this.x.findViewById(R.id.file_list);
        this.l = new Cif(this, this.b, this.v);
        this.z = (RelativeLayout) this.x.findViewById(R.id.bookmark_toolbar);
        this.A = (RelativeLayout) this.x.findViewById(R.id.file_toolbar);
        this.h = (RecyclerView) this.x.findViewById(R.id.bookmark_list);
        this.k = new gr(this, this.b, this.l, this.v);
        this.l.a(this.k);
        this.B = new View(this.b);
        this.aA = this.aB;
        this.X = browserActivity.getResources().getConfiguration().orientation;
    }

    /* access modifiers changed from: private */
    public ba a(RecyclerView recyclerView) {
        ba baVar = this.R;
        baVar.a = recyclerView;
        return baVar;
    }

    /* access modifiers changed from: private */
    public bc a(View view) {
        return new bc(this, view);
    }

    static /* synthetic */ void a(y yVar, XWebView xWebView) {
        Bitmap createBitmap = Bitmap.createBitmap(yVar.ac, yVar.ad, Bitmap.Config.ARGB_4444);
        yVar.u.draw(new Canvas(createBitmap));
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(createBitmap, yVar.ak, yVar.al, false);
        createBitmap.recycle();
        xWebView.a(createScaledBitmap);
        yVar.aq.post(yVar.as.a(xWebView));
    }

    private void d(XWebView xWebView) {
        this.aH = xWebView.z();
        if (!this.aI.equals(this.aH) || Math.abs(xWebView.w().getScrollY() - this.aG) >= 50) {
            this.aI = this.aH;
            this.aG = xWebView.w().getScrollY();
            l.a(this.aJ);
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        int i2;
        int i3 = 40;
        this.av.a();
        this.r = this.s;
        this.c.a(this.d);
        this.E.setTextColor(this.aa);
        float d2 = this.s.d();
        if (this.E != this.G || d2 > this.aw) {
            i2 = 0;
        } else {
            i2 = 40;
            i3 = 0;
        }
        TextView textView = this.F;
        this.E = textView;
        textView.setTextColor(this.Y);
        this.d.setVisibility(0);
        this.D.setVisibility(0);
        this.d.animate().alpha(1.0f).setDuration(100).setStartDelay((long) i2).setListener(null);
        this.B.animate().alpha(0.0f).setDuration(100).setStartDelay((long) i2).setListener(a(this.d));
        this.D.animate().translationY(d2).setStartDelay((long) i3).setDuration(100);
        this.aw = d2;
        this.av = new at(this);
    }

    static /* synthetic */ void k(y yVar) {
        yVar.av.a();
        yVar.o = yVar.q;
        yVar.E.setTextColor(yVar.aa);
        TextView textView = yVar.G;
        yVar.E = textView;
        textView.setTextColor(yVar.Y);
        yVar.g.setVisibility(0);
        yVar.J.setBackgroundColor(0);
        yVar.J.setTextColor(0);
        yVar.D.animate().translationY((float) yVar.ai).setDuration(200);
        yVar.g.animate().alpha(1.0f).setDuration(100).setListener(new bb(yVar, 1));
        yVar.B.animate().alpha(0.0f).setDuration(80).setListener(yVar.a(yVar.g));
        yVar.o.e();
        yVar.av = new aa(yVar);
    }

    /* access modifiers changed from: private */
    public void l() {
        int i2;
        int i3 = 40;
        this.av.a();
        this.r = this.t;
        this.c.a(this.e);
        this.E.setTextColor(this.aa);
        float d2 = this.t.d();
        if (this.E != this.F || d2 > this.aw) {
            i2 = 0;
        } else {
            i2 = 40;
            i3 = 0;
        }
        TextView textView = this.G;
        this.E = textView;
        textView.setTextColor(this.Y);
        this.D.setVisibility(0);
        this.e.setVisibility(0);
        this.e.animate().alpha(1.0f).setDuration(100).setStartDelay((long) i2).setListener(null);
        this.B.animate().alpha(0.0f).setDuration(100).setStartDelay((long) i2).setListener(a(this.e));
        this.D.animate().translationY(d2).setStartDelay((long) i3).setDuration(100);
        this.aw = d2;
        this.av = new au(this);
    }

    /* access modifiers changed from: private */
    public void m() {
        this.av.a();
        this.o = this.p;
        this.E.setTextColor(this.aa);
        TextView textView = this.F;
        this.E = textView;
        textView.setTextColor(this.Y);
        this.f.setVisibility(0);
        this.J.setBackgroundColor(0);
        this.J.setTextColor(0);
        this.D.animate().translationY((float) this.ai).setDuration(200);
        this.f.animate().alpha(1.0f).setDuration(100).setListener(new bb(this, 0));
        this.B.animate().alpha(0.0f).setDuration(100).setListener(a(this.f));
        this.o.e();
        this.av = new aw(this);
    }

    /* access modifiers changed from: private */
    public void n() {
        this.av.a();
        this.E.setTextColor(this.aa);
        TextView textView = this.H;
        this.E = textView;
        textView.setTextColor(this.Y);
        this.c.a(this.h);
        this.h.setVisibility(0);
        this.z.setVisibility(0);
        this.z.animate().setStartDelay(50).alpha(1.0f).setListener(null);
        this.h.animate().alpha(1.0f).setStartDelay(50).setListener(null);
        this.B.animate().alpha(0.0f).setListener(a(this.h));
        this.D.animate().translationY(0.0f).setDuration(150);
        this.k.b();
        this.aA = this.aC;
        if (this.V) {
            this.N.setVisibility(0);
            this.K.animate().alpha(0.0f).setListener(a(this.K));
            this.N.animate().alpha(1.0f).setListener(null);
            this.av = new ab(this);
            return;
        }
        this.av = new ac(this);
    }

    static /* synthetic */ void o(y yVar) {
        yVar.av.a();
        yVar.E.setTextColor(yVar.aa);
        TextView textView = yVar.I;
        yVar.E = textView;
        textView.setTextColor(yVar.Y);
        yVar.c.a(yVar.i);
        yVar.i.setVisibility(0);
        yVar.A.setVisibility(0);
        yVar.A.animate().setStartDelay(50).alpha(1.0f).setListener(null);
        yVar.i.animate().alpha(1.0f).setStartDelay(50).setListener(null);
        yVar.B.animate().alpha(0.0f).setListener(yVar.a(yVar.i));
        yVar.D.animate().translationY(0.0f).setDuration(150);
        yVar.l.a();
        yVar.aA = yVar.aD;
        yVar.av = new ad(yVar);
    }

    public final void a() {
        if (this.T) {
            k();
        } else {
            m();
        }
    }

    public final void a(int i2) {
        if (this.U) {
            this.y.animate().translationY((float) this.ap).setDuration((long) (((float) i2) * 0.8f)).setInterpolator(this.at);
        }
        this.x.animate().translationY(this.D.a()).setDuration((long) i2).setInterpolator(this.au);
        this.C.animate().alpha(0.0f).setDuration((long) i2).setListener(this.aA);
    }

    public final void a(XWebView xWebView) {
        xWebView.a(this.S);
    }

    public final void a(boolean z2) {
        if (com.fastnet.browseralam.h.a.ai()) {
            this.Y = -1;
        } else {
            this.Y = this.Z;
        }
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.u.getLayoutParams();
        this.y.setVisibility(0);
        if (com.fastnet.browseralam.h.a.D()) {
            d(z2);
        } else {
            c(z2);
        }
        if (z2 || com.fastnet.browseralam.h.a.C() != this.V) {
            this.V = com.fastnet.browseralam.h.a.C();
            if (this.V) {
                this.y.animate().translationY(0.0f).start();
                DrawerFrame drawerFrame = this.c;
                this.U = false;
                drawerFrame.a(false);
                this.b.a(this.Q);
                this.Q.setVisibility(0);
                this.O.setVisibility(8);
                layoutParams.bottomMargin = aq.a(52);
                this.u.setLayoutParams(layoutParams);
                this.ad = this.af;
                this.al = this.an;
                if (!z2 && !this.T) {
                    this.p.g((int) R.layout.card_item_short);
                    this.q.g((int) R.layout.card_item_short);
                    return;
                }
                return;
            }
            a(0);
            this.y.setTranslationY((float) this.ap);
            DrawerFrame drawerFrame2 = this.c;
            this.U = true;
            drawerFrame2.a(true);
            this.b.a((ImageView) null);
            this.Q.setVisibility(8);
            this.O.setVisibility(0);
            layoutParams.bottomMargin = 0;
            this.u.setLayoutParams(layoutParams);
            this.ad = this.ae;
            this.al = this.am;
            if (!z2 && !this.T) {
                this.p.g((int) R.layout.card_item);
                this.q.g((int) R.layout.card_item);
            }
        }
    }

    public final void a_(XWebView xWebView) {
        xWebView.w().setVisibility(8);
        this.u.addView(xWebView.w(), 0, aK);
        xWebView.w().setVisibility(0);
        this.aq.post(this.ar.a(xWebView.w()));
    }

    public final void b(int i2) {
        if (i2 == 0) {
            this.aA = this.aC;
        } else if (i2 == 1) {
            this.aA = this.aE;
        } else if (i2 == 2) {
            this.aA = this.aF;
        }
    }

    public final void b(XWebView xWebView) {
        Bitmap createBitmap = Bitmap.createBitmap(this.ac, this.ad, Bitmap.Config.ARGB_4444);
        xWebView.w().draw(new Canvas(createBitmap));
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(createBitmap, this.ak, this.al, false);
        createBitmap.recycle();
        xWebView.a(createScaledBitmap);
        this.aq.post(this.as.a(xWebView));
    }

    public final boolean b() {
        return !this.T;
    }

    public final boolean b(boolean z2) {
        if (z2) {
            if (this.E != this.G) {
                l();
            }
        } else if (this.E != this.F) {
            k();
        }
        return this.c.getVisibility() == 0;
    }

    public final void c() {
        if (this.c.getVisibility() == 8) {
            if (!this.T) {
                d(this.b.D());
            }
            this.c.setVisibility(0);
            this.c.setAlpha(1.0f);
            if (this.U) {
                this.y.animate().translationY(0.0f).setStartDelay(50).setDuration(250).setInterpolator(this.at);
            }
            this.x.animate().translationY(0.0f).setDuration(300).setInterpolator(this.at);
            this.C.animate().alpha(0.5f).setListener(null);
            this.b.k = this.a;
            return;
        }
        a(250);
    }

    public final void c(boolean z2) {
        if (z2 || !this.T) {
            this.T = true;
            if (this.s == null) {
                this.s = new n(this.b, this.d, this.m, false, this.D);
                this.t = new n(this.b, this.e, this.n, true, this.D);
                this.s.a(this);
                this.t.a(this);
                this.r = this.s;
            }
            com.fastnet.browseralam.h.a.w(false);
            this.D.setTranslationY(this.s.d());
            this.b.a(this.s);
            this.b.b(this.t);
            this.c.a(this.d);
            this.J.setVisibility(8);
            this.f.setVisibility(8);
            this.g.setVisibility(8);
            this.h.setVisibility(8);
            this.d.setVisibility(0);
            if (z2) {
                this.s.c();
                if (!this.W) {
                    this.aq.postDelayed(new av(this), 200);
                }
                this.W = true;
                k();
            }
        }
    }

    public final void d() {
        if (this.U) {
            this.y.animate().translationY((float) this.ap);
        }
        this.c.animate().alpha(0.0f).setDuration(220);
        this.C.animate().alpha(0.0f).setDuration(220).setListener(this.aA);
    }

    public final void d(boolean z2) {
        if (z2 || this.T) {
            this.T = false;
            if (this.o == null) {
                this.p = new com.fastnet.browseralam.g.a(this, this.b, this.ab, this.f, this.m, false, this.J);
                this.q = new com.fastnet.browseralam.g.a(this, this.b, this.ab, this.g, this.n, true, this.J);
                this.o = this.p;
            }
            this.h.setVisibility(8);
            this.e.setVisibility(8);
            this.d.setVisibility(8);
            this.f.setVisibility(0);
            this.J.setVisibility(0);
            com.fastnet.browseralam.h.a.w(true);
            this.b.a(this.p);
            this.b.b(this.q);
            this.c.a(this.f);
            this.D.setTranslationY((float) this.ai);
            this.c.setVisibility(0);
            if (z2) {
                this.p.c();
                this.p.g();
                if (!this.W) {
                    this.c.a((float) (this.ag - this.ai));
                }
                this.W = true;
            }
            m();
        }
    }

    public final void e() {
        n();
    }

    public final void f() {
        d(this.b.D());
    }

    public final void g() {
        this.aH = "";
    }

    public final a h() {
        return this.a;
    }

    public final gr i() {
        return this.k;
    }

    public final boolean j() {
        return this.E == this.I;
    }
}
