package com.fastnet.browseralam.activity;

import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.e.p;
import com.fastnet.browseralam.i.aq;

final class r implements View.OnClickListener {
    final /* synthetic */ o a;
    private int b = (-aq.a(78));
    private int c = (-aq.a(56));
    private RelativeLayout d;

    public r(o oVar) {
        this.a = oVar;
        View inflate = oVar.d.f.inflate((int) R.layout.floating_bookmark_edit, (ViewGroup) null);
        inflate.findViewById(R.id.edit).setOnClickListener(new s(this, oVar));
        inflate.findViewById(R.id.delete).setOnClickListener(new t(this, oVar));
        PopupWindow unused = oVar.d.K = new PopupWindow(inflate, -2, -2);
        oVar.d.K.setFocusable(true);
        oVar.d.K.setOutsideTouchable(true);
        oVar.d.K.setBackgroundDrawable(new ColorDrawable(0));
    }

    public final void onClick(View view) {
        this.d = (RelativeLayout) view.getParent();
        p unused = this.a.d.E = (p) this.d.getTag();
        this.a.d.K.showAsDropDown(view, this.b, this.c);
    }
}
