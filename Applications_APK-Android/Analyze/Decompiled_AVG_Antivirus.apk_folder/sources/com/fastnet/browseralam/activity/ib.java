package com.fastnet.browseralam.activity;

final class ib implements hv {
    final /* synthetic */ hw a;

    private ib(hw hwVar) {
        this.a = hwVar;
    }

    /* synthetic */ ib(hw hwVar, byte b) {
        this(hwVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.hw.b(com.fastnet.browseralam.activity.hw, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.hw, int]
     candidates:
      com.fastnet.browseralam.activity.hw.b(com.fastnet.browseralam.activity.hw, int):int
      com.fastnet.browseralam.activity.hw.b(com.fastnet.browseralam.activity.hw, float):void
      com.fastnet.browseralam.activity.hw.b(com.fastnet.browseralam.activity.hw, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.hw.d(com.fastnet.browseralam.activity.hw, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.hw, int]
     candidates:
      com.fastnet.browseralam.activity.hw.d(com.fastnet.browseralam.activity.hw, float):float
      com.fastnet.browseralam.activity.hw.d(com.fastnet.browseralam.activity.hw, int):void
      com.fastnet.browseralam.activity.hw.d(com.fastnet.browseralam.activity.hw, boolean):boolean */
    public final void a() {
        boolean unused = this.a.g = this.a.h = false;
        if (this.a.C != null) {
            this.a.C.x();
        }
        if ((this.a.E || this.a.D) && this.a.t != this.a.B) {
            boolean unused2 = this.a.E = this.a.D = false;
            this.a.j.setAlpha(0.0f);
            this.a.j.setVisibility(0);
            this.a.a.w.setVisibility(8);
            this.a.a.w.setImageBitmap(null);
            this.a.a.K.c(this.a.t, this.a.B);
            return;
        }
        boolean unused3 = this.a.E = this.a.D = false;
        this.a.a.w.animate().translationY((float) this.a.j.getTop()).setDuration(100).setListener(this.a.F).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.hw.d(com.fastnet.browseralam.activity.hw, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.hw, int]
     candidates:
      com.fastnet.browseralam.activity.hw.d(com.fastnet.browseralam.activity.hw, float):float
      com.fastnet.browseralam.activity.hw.d(com.fastnet.browseralam.activity.hw, int):void
      com.fastnet.browseralam.activity.hw.d(com.fastnet.browseralam.activity.hw, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.hw.c(com.fastnet.browseralam.activity.hw, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.hw, int]
     candidates:
      com.fastnet.browseralam.activity.hw.c(com.fastnet.browseralam.activity.hw, float):float
      com.fastnet.browseralam.activity.hw.c(com.fastnet.browseralam.activity.hw, int):boolean
      com.fastnet.browseralam.activity.hw.c(com.fastnet.browseralam.activity.hw, boolean):boolean */
    public final void a(float f, float f2) {
        if (this.a.j != null) {
            this.a.a.w.setTranslationY(f2 - this.a.y);
            if (this.a.a.w.getTranslationY() < this.a.z) {
                if (this.a.E) {
                    boolean unused = this.a.E = this.a.D = false;
                    this.a.C.x();
                    this.a.a(this.a.B, (float) (this.a.j.getTop() - this.a.l), (float) this.a.j.getTop());
                    this.a.a.K.b(this.a.t, this.a.B);
                    int unused2 = this.a.t = this.a.B;
                } else if (this.a.D) {
                    this.a.C.x();
                    boolean unused3 = this.a.D = this.a.E = false;
                    int unused4 = this.a.B = this.a.t;
                    this.a.a(this.a.t, (float) this.a.j.getTop(), (float) this.a.j.getBottom());
                } else if (this.a.B != 0) {
                    hw.s(this.a);
                    if (this.a.a(this.a.B)) {
                        boolean unused5 = this.a.E = true;
                        hw.d(this.a, this.a.B);
                        if (this.a.B != 0 || this.a.a.S) {
                            float unused6 = this.a.z = (float) (this.a.j.getTop() - this.a.m);
                        } else {
                            float unused7 = this.a.z = (float) (-this.a.p);
                        }
                        float unused8 = this.a.A = (float) (this.a.j.getBottom() - this.a.o);
                        return;
                    }
                    this.a.a(this.a.B, (float) (this.a.j.getTop() - this.a.l), (float) this.a.j.getTop());
                    this.a.a.K.b(this.a.t, this.a.B);
                    int unused9 = this.a.t = this.a.B;
                }
            } else if (this.a.a.w.getTranslationY() + ((float) this.a.l) <= this.a.A) {
            } else {
                if (this.a.D) {
                    boolean unused10 = this.a.D = this.a.E = false;
                    this.a.C.x();
                    this.a.a(this.a.B, (float) this.a.j.getBottom(), (float) (this.a.j.getBottom() + this.a.l));
                    this.a.a.K.b(this.a.t, this.a.B);
                    int unused11 = this.a.t = this.a.B;
                } else if (this.a.E) {
                    this.a.C.x();
                    boolean unused12 = this.a.E = this.a.D = false;
                    int unused13 = this.a.B = this.a.t;
                    this.a.a(this.a.t, (float) this.a.j.getTop(), (float) this.a.j.getBottom());
                } else if (this.a.B != this.a.u) {
                    hw.y(this.a);
                    if (this.a.a(this.a.B)) {
                        boolean unused14 = this.a.D = true;
                        hw.d(this.a, this.a.B);
                        float unused15 = this.a.z = (float) (this.a.j.getTop() + this.a.o);
                        float unused16 = this.a.A = (float) (this.a.j.getBottom() + this.a.m);
                        return;
                    }
                    this.a.a(this.a.B, (float) this.a.j.getBottom(), (float) (this.a.j.getBottom() + this.a.l));
                    this.a.a.K.b(this.a.t, this.a.B);
                    int unused17 = this.a.t = this.a.B;
                }
            }
        }
    }
}
