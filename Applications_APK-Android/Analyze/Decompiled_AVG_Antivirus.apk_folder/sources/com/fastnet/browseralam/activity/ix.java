package com.fastnet.browseralam.activity;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class ix implements Runnable {
    final /* synthetic */ EditText a;
    final /* synthetic */ Cif b;

    ix(Cif ifVar, EditText editText) {
        this.b = ifVar;
        this.a = editText;
    }

    public final void run() {
        ((InputMethodManager) this.b.m.getSystemService("input_method")).showSoftInput(this.a, 1);
    }
}
