package com.fastnet.browseralam.activity;

import android.view.View;
import com.fastnet.browseralam.R;

final class il implements View.OnClickListener {
    final /* synthetic */ Cif a;

    il(Cif ifVar) {
        this.a = ifVar;
    }

    public final void onClick(View view) {
        this.a.w.animate().alpha(0.0f).setDuration(100);
        this.a.q.animate().alpha(0.0f).setDuration(100).setListener(this.a.ar);
        this.a.x.removeView(this.a.an);
        this.a.x.findViewById(R.id.bottom_action_bar).setVisibility(0);
        this.a.p.b(0);
    }
}
