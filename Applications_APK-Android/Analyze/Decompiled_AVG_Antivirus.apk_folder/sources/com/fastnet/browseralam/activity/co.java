package com.fastnet.browseralam.activity;

import android.view.View;
import android.widget.CheckedTextView;

final class co implements View.OnClickListener {
    final /* synthetic */ int[] a;
    final /* synthetic */ BrowserActivity b;

    co(BrowserActivity browserActivity, int[] iArr) {
        this.b = browserActivity;
        this.a = iArr;
    }

    public final void onClick(View view) {
        CheckedTextView checkedTextView = (CheckedTextView) view;
        checkedTextView.setChecked(!checkedTextView.isChecked());
        if (checkedTextView.isChecked()) {
            this.a[2] = 1;
        }
        this.a[2] = 0;
    }
}
