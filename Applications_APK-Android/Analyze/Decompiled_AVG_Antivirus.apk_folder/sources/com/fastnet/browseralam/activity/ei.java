package com.fastnet.browseralam.activity;

import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.h;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.e.a;
import com.fastnet.browseralam.e.t;
import com.fastnet.browseralam.g.aj;
import com.fastnet.browseralam.g.az;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.CardFrame;
import com.fastnet.browseralam.view.XWebView;
import java.util.List;

public final class ei extends h {
    private static boolean J;
    private static final ViewGroup.LayoutParams U = new ViewGroup.LayoutParams(-1, -1);
    /* access modifiers changed from: private */
    public final int A = ((int) (((float) this.x) * 0.5f));
    private final int B = ((int) (((float) this.y) * 0.5f));
    private final int C = aq.a(60);
    /* access modifiers changed from: private */
    public final int D;
    private final DecelerateInterpolator E = new DecelerateInterpolator(1.0f);
    private final DecelerateInterpolator F = new DecelerateInterpolator(2.0f);
    private t G;
    private t H;
    private t I;
    /* access modifiers changed from: private */
    public BitmapDrawable K;
    /* access modifiers changed from: private */
    public XWebView L;
    /* access modifiers changed from: private */
    public View M;
    /* access modifiers changed from: private */
    public boolean N;
    /* access modifiers changed from: private */
    public boolean O;
    private a P = new ej(this);
    private a Q = new el(this);
    private View.OnClickListener R = new em(this);
    private View.OnTouchListener S = new en(this);
    private com.fastnet.browseralam.b.a T = new eo(this);
    /* access modifiers changed from: private */
    public Handler V = k.a();
    private final int W = aq.a(56);
    private Paint X = new Paint();
    private Paint Y = new Paint();
    private int Z;
    /* access modifiers changed from: private */
    public final BrowserActivity a;
    private int aa;
    private float ab;
    private RectF ac;
    private View.OnAttachStateChangeListener ad = new es(this);
    private final LayoutInflater b;
    /* access modifiers changed from: private */
    public final FrameLayout c;
    /* access modifiers changed from: private */
    public final FrameLayout d;
    /* access modifiers changed from: private */
    public final CardFrame e;
    /* access modifiers changed from: private */
    public final RelativeLayout f;
    /* access modifiers changed from: private */
    public final TextView g;
    private final TextView h;
    private final ImageView i;
    /* access modifiers changed from: private */
    public final View j;
    /* access modifiers changed from: private */
    public final RecyclerView k;
    /* access modifiers changed from: private */
    public final RecyclerView l;
    /* access modifiers changed from: private */
    public final aj m;
    /* access modifiers changed from: private */
    public final az n;
    /* access modifiers changed from: private */
    public final List o;
    private final List p;
    private final ex q = new ex(this, (byte) 0);
    private final ew r = new ew(this, (byte) 0);
    /* access modifiers changed from: private */
    public com.fastnet.browseralam.g.k s;
    /* access modifiers changed from: private */
    public RecyclerView t;
    /* access modifiers changed from: private */
    public FrameLayout u;
    /* access modifiers changed from: private */
    public PopupWindow v;
    /* access modifiers changed from: private */
    public final int w = aq.a();
    /* access modifiers changed from: private */
    public final int x = (aq.b() - aq.a(56));
    private final int y = (aq.b() - aq.a(80));
    /* access modifiers changed from: private */
    public final int z = ((int) (((float) this.w) * 0.5f));

    public ei(BrowserActivity browserActivity, FrameLayout frameLayout, List list, List list2) {
        this.a = browserActivity;
        this.b = browserActivity.getLayoutInflater();
        this.o = list;
        this.p = list2;
        this.c = frameLayout;
        this.e = (CardFrame) frameLayout.findViewById(R.id.xcard_container);
        this.f = (RelativeLayout) frameLayout.findViewById(R.id.toolbar_layout);
        this.u = (FrameLayout) frameLayout.findViewById(R.id.background_frame);
        this.d = (FrameLayout) frameLayout.findViewById(R.id.webview_frame);
        this.k = (RecyclerView) frameLayout.findViewById(R.id.xcard_view);
        this.l = (RecyclerView) frameLayout.findViewById(R.id.xcard_view_incognito);
        this.t = this.k;
        this.c.findViewById(R.id.xtabs_button).setOnClickListener(this.R);
        this.c.findViewById(R.id.xcard_menu).setOnClickListener(this.R);
        this.g = (TextView) frameLayout.findViewById(R.id.card_view_title);
        this.h = (TextView) frameLayout.findViewById(R.id.current_view);
        this.m = new aj(this, browserActivity, this.k, this.e, list, this.g);
        this.n = new az(this, browserActivity, this.l, this.e, list2, this.g);
        this.a.a(this.m);
        this.a.b(this.n);
        this.s = this.m;
        t i2 = this.m.i();
        this.H = i2;
        this.G = i2;
        this.I = this.n.i();
        this.i = (ImageView) frameLayout.findViewById(R.id.xtabs_button);
        this.j = frameLayout.findViewById(R.id.fab_new_tab);
        this.j.setOnTouchListener(this.S);
        this.D = Color.parseColor("#4D333333");
        this.d.addOnAttachStateChangeListener(this.ad);
        this.e.a(this, this.m, this.k, this.l);
        int a2 = aq.a(56);
        this.X = new Paint();
        this.X.setColor(-1);
        this.X.setTextAlign(Paint.Align.CENTER);
        this.X.setStyle(Paint.Style.STROKE);
        this.X.setStrokeWidth(aq.a(1.6f));
        this.ab = aq.a(0.6f);
        this.Y = new Paint();
        this.Y.setColor(-1);
        this.Y.setTextAlign(Paint.Align.CENTER);
        this.Y.setAntiAlias(true);
        this.Y.setStyle(Paint.Style.FILL);
        this.Y.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
        this.Y.setTextSize((float) aq.a(13));
        float a3 = aq.a(19.5f);
        this.Z = (int) aq.a(27.5f);
        this.aa = (int) aq.a(32.5f);
        this.ac = new RectF(a3, a3, ((float) a2) - a3, ((float) a2) - a3);
        View inflate = this.b.inflate((int) R.layout.card_menu, (ViewGroup) null);
        inflate.findViewById(R.id.card_new_tab).setOnClickListener(new ep(this));
        inflate.findViewById(R.id.card_new_incognito).setOnClickListener(new eq(this));
        inflate.findViewById(R.id.card_close_tabs).setOnClickListener(new er(this));
        this.v = new PopupWindow(inflate, aq.a(210), -2);
        this.v.setFocusable(true);
        this.v.setOutsideTouchable(true);
        this.v.setBackgroundDrawable(new ColorDrawable(0));
        J = true;
    }

    public final void a(int i2) {
        Bitmap createBitmap = Bitmap.createBitmap(this.W, this.W, Bitmap.Config.ARGB_8888);
        String valueOf = String.valueOf(i2);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawRoundRect(this.ac, this.ab, this.ab, this.X);
        canvas.drawText(valueOf, (float) this.Z, (float) this.aa, this.Y);
        this.i.setImageDrawable(new BitmapDrawable(this.a.getResources(), createBitmap));
    }

    public final void a(XWebView xWebView) {
        xWebView.a(this.K);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
     arg types: [com.fastnet.browseralam.view.XWebView, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void */
    public final void a(XWebView xWebView, FrameLayout frameLayout, int i2) {
        this.O = true;
        this.M = frameLayout;
        this.L = xWebView;
        this.d.setVisibility(0);
        this.f.setVisibility(0);
        this.g.setVisibility(8);
        if (xWebView != null) {
            if (this.a.D() != xWebView) {
                this.a.a(xWebView, false);
            } else {
                xWebView.e();
            }
        }
        this.M.animate().scaleY(2.0f).scaleX(2.0f).setDuration(240).setStartDelay((long) i2).setInterpolator(this.E).setListener(this.P);
        this.f.animate().alpha(1.0f).setInterpolator(this.E).setDuration(150).setStartDelay(100);
        ObjectAnimator.ofFloat(this.j, View.ALPHA, 1.0f, 0.0f).setDuration(100L).start();
        this.a.n();
    }

    public final void a(boolean z2) {
        if (z2) {
            this.t = this.l;
            this.n.b(true);
            this.m.c(false);
            this.s = this.n;
            this.G = this.I;
            this.h.setText("INCOGNITO");
            this.s.f();
            a(this.p.size());
            if (!this.N) {
                this.e.a(false);
                return;
            }
            return;
        }
        this.t = this.k;
        this.m.c(true);
        this.n.b(false);
        this.s = this.m;
        this.G = this.H;
        this.h.setText("TABS");
        this.s.f();
        a(this.o.size());
        if (!this.N) {
            this.e.b(false);
        }
    }

    public final void b(XWebView xWebView) {
        Bitmap createBitmap = Bitmap.createBitmap(this.w, this.y, Bitmap.Config.ARGB_8888);
        xWebView.w().draw(new Canvas(createBitmap));
        BitmapDrawable bitmapDrawable = new BitmapDrawable(this.a.getResources(), Bitmap.createScaledBitmap(createBitmap, this.z, this.B, false));
        createBitmap.recycle();
        xWebView.a(bitmapDrawable);
        this.V.post(this.r.a(xWebView));
    }

    public final boolean b() {
        return J;
    }

    public final void c(XWebView xWebView) {
        if (!this.O) {
            if (this.N) {
                this.g.setVisibility(8);
                this.s.f_();
                return;
            }
            this.c.removeView(this.k);
            this.c.removeView(this.l);
            this.k.setVisibility(0);
            this.l.setVisibility(0);
            this.e.setVisibility(0);
            aj ajVar = this.m;
            this.N = true;
            ajVar.a(true);
            this.n.a(true);
            this.s.f();
            this.L = xWebView;
            this.L.w().flingScroll(0, 0);
            Bitmap createBitmap = Bitmap.createBitmap(this.w, this.x, Bitmap.Config.ARGB_8888);
            this.d.draw(new Canvas(createBitmap));
            BitmapDrawable bitmapDrawable = new BitmapDrawable(this.a.getResources(), Bitmap.createScaledBitmap(createBitmap, this.z, this.A, false));
            createBitmap.recycle();
            xWebView.a(bitmapDrawable);
            this.G.a(false);
            this.M = this.s.g();
            this.M.setBackground(bitmapDrawable);
            this.e.addView(this.l);
            this.e.addView(this.k);
            this.d.setVisibility(8);
            this.j.setVisibility(0);
            this.M.animate().scaleX(1.0f).scaleY(1.0f).setDuration(240).setInterpolator(this.E).setListener(this.Q);
            this.f.animate().alpha(0.0f).setDuration(200).setInterpolator(this.E);
            this.j.animate().alpha(1.0f).setDuration(150).setStartDelay(200);
            this.s.d();
            this.a.k = this.T;
        }
    }

    public final void d(XWebView xWebView) {
        this.d.addView(xWebView.w(), 0, U);
        this.V.post(this.q.a(xWebView.w()));
    }
}
