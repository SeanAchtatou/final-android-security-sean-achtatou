package com.fastnet.browseralam.activity;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class fg implements Runnable {
    final /* synthetic */ EditText a;
    final /* synthetic */ ey b;

    fg(ey eyVar, EditText editText) {
        this.b = eyVar;
        this.a = editText;
    }

    public final void run() {
        ((InputMethodManager) this.b.f.getSystemService("input_method")).showSoftInput(this.a, 1);
    }
}
