package com.fastnet.browseralam.activity;

import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import com.fastnet.browseralam.window.SpeedWindow;
import com.fastnet.browseralam.window.XWindow;

final class di implements View.OnClickListener {
    final /* synthetic */ BrowserActivity a;

    di(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    public final void onClick(View view) {
        ViewGroup unused = this.a.bb = (ViewGroup) this.a.p.getParent();
        BrowserActivity unused2 = BrowserActivity.bc = this.a;
        this.a.R.f();
        this.a.S.d();
        XWindow.c(this.a.ao, SpeedWindow.class);
        XWindow.a(this.a.ao, SpeedWindow.class);
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        intent.setFlags(268435456);
        this.a.startActivity(intent);
    }
}
