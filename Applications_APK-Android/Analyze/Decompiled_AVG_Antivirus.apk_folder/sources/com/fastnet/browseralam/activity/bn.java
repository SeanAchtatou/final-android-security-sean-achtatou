package com.fastnet.browseralam.activity;

import android.view.View;
import com.fastnet.browseralam.i.aq;

final class bn implements View.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ BrowserActivity b;

    bn(BrowserActivity browserActivity, String str) {
        this.b = browserActivity;
        this.a = str;
    }

    public final void onClick(View view) {
        this.b.aN.c(this.a);
        aq.a(this.b.ao, "Url added to adblock list");
        this.b.bt.dismiss();
    }
}
