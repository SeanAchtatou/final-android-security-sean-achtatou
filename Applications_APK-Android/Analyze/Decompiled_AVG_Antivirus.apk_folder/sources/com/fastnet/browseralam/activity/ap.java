package com.fastnet.browseralam.activity;

import android.webkit.WebView;
import android.widget.FrameLayout;
import com.fastnet.browseralam.view.XWebView;

final class ap implements Runnable {
    final /* synthetic */ WebView a;
    final /* synthetic */ ao b;

    ap(ao aoVar, WebView webView) {
        this.b = aoVar;
        this.a = webView;
    }

    public final void run() {
        this.a.clearHistory();
        this.a.setVisibility(8);
        this.a.removeAllViews();
        this.a.destroyDrawingCache();
        this.a.destroy();
        this.b.a.a.w.removeAllViews();
        this.b.a.a.v.removeView(this.b.a.a.w);
        FrameLayout unused = this.b.a.a.w = null;
        for (XWebView xWebView : this.b.a.a.m) {
            if (xWebView.z().isEmpty() || xWebView.z().contains("homepage.html")) {
                xWebView.a(this.b.a.a.S);
            }
            if (!xWebView.i()) {
                xWebView.d();
            }
        }
        this.b.a.a.o.c(this.b.a.a.m.size());
    }
}
