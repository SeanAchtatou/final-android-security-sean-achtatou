package com.fastnet.browseralam.activity;

import android.support.v4.content.a;
import android.support.v7.widget.cf;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;

final class jn extends cf {
    final FrameLayout l;
    final RelativeLayout m;
    final ImageView n;
    final TextView o;
    final TextView p;
    final TextView q;
    jo r;
    boolean s = false;
    final /* synthetic */ jj t;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jn(jj jjVar, View view) {
        super(view);
        this.t = jjVar;
        this.o = (TextView) view.findViewById(R.id.fileName);
        this.n = (ImageView) view.findViewById(R.id.fileIcon);
        this.p = (TextView) view.findViewById(R.id.last_modified);
        this.q = (TextView) view.findViewById(R.id.file_size);
        this.m = (RelativeLayout) view.findViewById(R.id.file_item);
        this.l = (FrameLayout) view.findViewById(R.id.container);
    }

    public final void t() {
        this.s = true;
        this.m.setBackground(this.t.j);
    }

    public final void u() {
        this.s = false;
        this.m.setBackground(a.a(this.t.h.m, this.t.l));
    }
}
