package com.fastnet.browseralam.activity;

import android.view.View;

final class bu implements View.OnLongClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ boolean b;
    final /* synthetic */ BrowserActivity c;

    bu(BrowserActivity browserActivity, String str, boolean z) {
        this.c = browserActivity;
        this.a = str;
        this.b = z;
    }

    public final boolean onLongClick(View view) {
        boolean z = false;
        BrowserActivity browserActivity = this.c;
        String str = this.a;
        boolean z2 = !this.c.at;
        int h = this.c.L.h() + 1;
        if (!this.b) {
            z = true;
        }
        browserActivity.a(str, z2, h, z);
        this.c.bt.dismiss();
        return true;
    }
}
