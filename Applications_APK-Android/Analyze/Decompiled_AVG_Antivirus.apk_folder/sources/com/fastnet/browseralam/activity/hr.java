package com.fastnet.browseralam.activity;

import android.view.GestureDetector;
import android.view.MotionEvent;
import com.fastnet.browseralam.e.p;

final class hr extends GestureDetector.SimpleOnGestureListener {
    final /* synthetic */ gr a;
    final /* synthetic */ hq b;

    hr(hq hqVar, gr grVar) {
        this.b = hqVar;
        this.a = grVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.hq.a(com.fastnet.browseralam.activity.hq, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.hq, int]
     candidates:
      com.fastnet.browseralam.activity.hq.a(com.fastnet.browseralam.activity.hq, float):float
      com.fastnet.browseralam.activity.hq.a(com.fastnet.browseralam.activity.hq, android.view.View):android.view.View
      com.fastnet.browseralam.activity.hq.a(com.fastnet.browseralam.activity.hq, boolean):boolean */
    public final void onLongPress(MotionEvent motionEvent) {
        if (!this.b.e) {
            super.onLongPress(motionEvent);
            if (this.b.f = this.b.a.a.a(motionEvent.getX(), motionEvent.getY()) != null && !this.b.a.Y = (p) this.b.f.getTag().u()) {
                this.b.a.m.requestDisallowInterceptTouchEvent(true);
                boolean unused = this.b.g = false;
                boolean unused2 = this.b.d = true;
                float unused3 = this.b.o = motionEvent.getY();
                hq.b(this.b, this.b.o);
                this.b.c.postDelayed(this.b.x, 400);
            }
        }
    }
}
