package com.fastnet.browseralam.activity;

import android.support.v7.widget.RecyclerView;
import com.fastnet.browseralam.view.XWebView;

final class ew implements Runnable {
    final /* synthetic */ ei a;
    private boolean b;
    private int c;
    private RecyclerView d;

    private ew(ei eiVar) {
        this.a = eiVar;
    }

    /* synthetic */ ew(ei eiVar, byte b2) {
        this(eiVar);
    }

    public final ew a(XWebView xWebView) {
        this.c = xWebView.h();
        if (xWebView.D()) {
            this.d = this.a.l;
        } else {
            this.d = this.a.k;
        }
        this.b = xWebView.D();
        return this;
    }

    public final void run() {
        if (this.d.h()) {
            this.a.V.postDelayed(this, 150);
        } else if (this.b) {
            this.a.n.b(this.c);
        } else {
            this.a.m.b(this.c);
        }
    }
}
