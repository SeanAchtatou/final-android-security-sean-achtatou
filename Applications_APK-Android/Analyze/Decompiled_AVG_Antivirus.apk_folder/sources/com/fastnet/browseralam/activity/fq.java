package com.fastnet.browseralam.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.fastnet.browseralam.R;

public final class fq extends Fragment {
    private static final ViewGroup.LayoutParams ab = new ViewGroup.LayoutParams(-1, -1);
    private RecyclerView aa;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View a(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        RelativeLayout relativeLayout = (RelativeLayout) layoutInflater.inflate((int) R.layout.tablist_fragment, viewGroup, false);
        if (this.aa == null) {
            this.aa = ((BrowserActivity) a()).f();
        }
        relativeLayout.addView(this.aa, ab);
        return relativeLayout;
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
    }

    public final void a(RecyclerView recyclerView) {
        this.aa = recyclerView;
    }
}
