package com.fastnet.browseralam.activity;

import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import com.fastnet.browseralam.R;

final class bm implements AdapterView.OnItemClickListener {
    final /* synthetic */ AutoCompleteTextView a;
    final /* synthetic */ BrowserActivity b;

    bm(BrowserActivity browserActivity, AutoCompleteTextView autoCompleteTextView) {
        this.b = browserActivity;
        this.a = autoCompleteTextView;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        try {
            String str = (String) view.findViewById(R.id.title).getTag();
            this.a.setText(str);
            this.b.b(str);
            ((InputMethodManager) this.b.getSystemService("input_method")).hideSoftInputFromWindow(this.a.getWindowToken(), 0);
            if (this.b.L != null) {
                this.b.L.k();
            }
        } catch (NullPointerException e) {
            Log.e("Browser Error: ", "NullPointerException on item click");
        }
    }
}
