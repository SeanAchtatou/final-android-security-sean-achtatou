package com.fastnet.browseralam.activity;

import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.e.p;
import com.fastnet.browseralam.i.aq;

final class hn implements View.OnClickListener {
    final /* synthetic */ hk a;
    private int b = aq.a(16);
    private RelativeLayout c;

    public hn(hk hkVar) {
        this.a = hkVar;
        View inflate = hkVar.d.h.inflate((int) R.layout.floating_bookmark_edit, (ViewGroup) null);
        inflate.findViewById(R.id.edit).setOnClickListener(new ho(this, hkVar));
        inflate.findViewById(R.id.delete).setOnClickListener(new hp(this, hkVar));
        PopupWindow unused = hkVar.d.v = new PopupWindow(inflate, -2, -2);
        hkVar.d.v.setFocusable(true);
        hkVar.d.v.setOutsideTouchable(true);
        hkVar.d.v.setBackgroundDrawable(new ColorDrawable(0));
    }

    public final void onClick(View view) {
        this.c = (RelativeLayout) view.getParent();
        p unused = this.a.d.Y = (p) this.c.getTag();
        this.a.d.v.showAtLocation(view, 53, 0, this.c.getTop() + this.b);
    }
}
