package com.fastnet.browseralam.activity;

import android.graphics.Bitmap;
import com.fastnet.browseralam.b.d;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.i.t;
import java.io.File;

final class i implements d {
    final /* synthetic */ b a;
    final /* synthetic */ a b;

    i(a aVar, b bVar) {
        this.b = aVar;
        this.a = bVar;
    }

    public final void a(Bitmap bitmap, String str) {
        this.a.a(bitmap);
        this.a.c(str);
        int indexOf = this.b.r.indexOf(this.a);
        if (indexOf != -1) {
            this.b.s.b(indexOf);
        }
        t.a(new File(this.b.N + str), bitmap);
    }
}
