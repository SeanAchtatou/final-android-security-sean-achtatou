package com.fastnet.browseralam.activity;

import android.content.DialogInterface;

final class bg implements DialogInterface.OnClickListener {
    final /* synthetic */ int a;
    final /* synthetic */ boolean b;
    final /* synthetic */ BrowserActivity c;

    bg(BrowserActivity browserActivity, int i, boolean z) {
        this.c = browserActivity;
        this.a = i;
        this.b = z;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case 0:
                this.c.a(this.a, this.b);
                return;
            case 1:
                this.c.k();
                return;
            default:
                return;
        }
    }
}
