package com.fastnet.browseralam.activity;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public final class ec implements View.OnKeyListener {
    final /* synthetic */ dx a;

    public ec(dx dxVar) {
        this.a = dxVar;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        switch (i) {
            case 66:
                ((InputMethodManager) this.a.b.getSystemService("input_method")).hideSoftInputFromWindow(this.a.b.ac.getWindowToken(), 0);
                this.a.b.b(this.a.b.ac.getText().toString());
                this.a.b.v.requestFocus();
                return true;
            default:
                return false;
        }
    }
}
