package com.fastnet.browseralam.activity;

import android.animation.Animator;
import com.fastnet.browseralam.e.a;

final class ic extends a {
    final /* synthetic */ hw a;
    private boolean b;

    private ic(hw hwVar) {
        this.a = hwVar;
        this.b = false;
    }

    /* synthetic */ ic(hw hwVar, byte b2) {
        this(hwVar);
    }

    public final ic a() {
        this.b = true;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.hw.e(com.fastnet.browseralam.activity.hw, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.hw, int]
     candidates:
      com.fastnet.browseralam.activity.hw.e(com.fastnet.browseralam.activity.hw, float):float
      com.fastnet.browseralam.activity.hw.e(com.fastnet.browseralam.activity.hw, boolean):boolean */
    public final void onAnimationEnd(Animator animator) {
        this.a.j.setVisibility(0);
        this.a.a.j.setVisibility(8);
        this.a.a.i.removeView(this.a.a.o);
        boolean unused = this.a.i = false;
        if (this.b) {
            this.a.a.x.setTranslationX(0.0f);
            this.a.a.j.setAlpha(1.0f);
            this.b = false;
        }
        this.a.a.c.k = this.a.a.d.h();
    }
}
