package com.fastnet.browseralam.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Handler;
import android.support.v4.view.q;
import android.support.v7.widget.bt;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import com.fastnet.browseralam.b.a;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.e.p;
import com.fastnet.browseralam.i.aq;

final class hw implements bt, hu {
    /* access modifiers changed from: private */
    public float A = ((float) this.l);
    /* access modifiers changed from: private */
    public int B;
    /* access modifiers changed from: private */
    public p C;
    /* access modifiers changed from: private */
    public boolean D;
    /* access modifiers changed from: private */
    public boolean E;
    /* access modifiers changed from: private */
    public id F = new id(this, (byte) 0);
    /* access modifiers changed from: private */
    public ic G = new ic(this, (byte) 0);
    /* access modifiers changed from: private */
    public a H = new hz(this);
    /* access modifiers changed from: private */
    public final Runnable I = new ia(this);
    final /* synthetic */ gr a;
    private final q b;
    /* access modifiers changed from: private */
    public final Handler c = k.a();
    /* access modifiers changed from: private */
    public hv d;
    /* access modifiers changed from: private */
    public ib e = new ib(this, (byte) 0);
    private ie f = new ie(this, (byte) 0);
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public View j;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public final int l = aq.a(56);
    /* access modifiers changed from: private */
    public final int m = aq.a(80);
    private final int n = aq.a(50);
    /* access modifiers changed from: private */
    public final int o = aq.a(40);
    /* access modifiers changed from: private */
    public final int p = aq.a(500);
    /* access modifiers changed from: private */
    public final int q = aq.a(112);
    /* access modifiers changed from: private */
    public final int r = (-this.q);
    /* access modifiers changed from: private */
    public final DecelerateInterpolator s = new DecelerateInterpolator(2.0f);
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public float v;
    /* access modifiers changed from: private */
    public float w;
    /* access modifiers changed from: private */
    public float x;
    /* access modifiers changed from: private */
    public float y;
    /* access modifiers changed from: private */
    public float z = ((float) (-this.l));

    public hw(gr grVar) {
        this.a = grVar;
        this.b = new q(grVar.a.getContext(), new hx(this, grVar));
        grVar.o.setOnTouchListener(new hy(this, grVar));
    }

    /* access modifiers changed from: private */
    public void a(int i2, float f2, float f3) {
        if (i2 == 0) {
            this.z = f2 - ((float) this.p);
        } else if (a(i2 - 1)) {
            this.z = f2 - ((float) this.o);
        } else {
            this.z = f2 - ((float) this.n);
        }
        if (i2 == this.u) {
            this.A = ((float) this.p) + f3;
        } else if (a(i2 + 1)) {
            this.A = ((float) this.o) + f3;
        } else {
            this.A = ((float) this.n) + f3;
        }
    }

    /* access modifiers changed from: private */
    public boolean a(int i2) {
        return ((b) this.a.J.get(i2)).c();
    }

    static /* synthetic */ void b(hw hwVar, float f2) {
        float top = (float) hwVar.j.getTop();
        hwVar.y = f2 - top;
        hwVar.a.w.setTranslationY(f2 - hwVar.y);
        View view = hwVar.j;
        Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), hwVar.l, Bitmap.Config.ARGB_8888);
        view.draw(new Canvas(createBitmap));
        hwVar.a.w.setImageBitmap(createBitmap);
        hwVar.a.w.setTop(0);
        hwVar.a.w.setVisibility(0);
        hwVar.j.setVisibility(4);
        hwVar.u = hwVar.a.J.size() - 1;
        int t2 = hwVar.a.Y.t();
        hwVar.t = t2;
        hwVar.B = t2;
        hwVar.a(hwVar.t, top, (float) hwVar.j.getBottom());
    }

    static /* synthetic */ void d(hw hwVar, int i2) {
        hwVar.C = (p) hwVar.a.a.c(i2);
        if (hwVar.C != null) {
            hwVar.C.w();
        }
    }

    static /* synthetic */ int s(hw hwVar) {
        int i2 = hwVar.B;
        hwVar.B = i2 - 1;
        return i2;
    }

    static /* synthetic */ int y(hw hwVar) {
        int i2 = hwVar.B;
        hwVar.B = i2 + 1;
        return i2;
    }

    public final void a(boolean z2) {
    }

    public final boolean a(MotionEvent motionEvent) {
        if (this.h) {
            return true;
        }
        if (motionEvent.getAction() == 0) {
            this.g = false;
            this.v = motionEvent.getX();
            this.w = motionEvent.getY();
            if (this.i) {
                this.a.m.requestDisallowInterceptTouchEvent(true);
                if (this.w < ((float) this.j.getTop()) || this.w > ((float) this.j.getBottom()) || ((this.x == ((float) this.r) && this.v < this.a.x.getTranslationX() + ((float) this.a.x.getWidth())) || (this.x == ((float) this.q) && this.v > this.a.x.getTranslationX()))) {
                    this.a.x.animate().translationX(0.0f).setInterpolator(this.s).setListener(this.G);
                }
                return true;
            }
        } else if (motionEvent.getAction() == 2) {
            if (this.i) {
                return true;
            }
            if (Math.abs(motionEvent.getY() - this.w) < 20.0f && Math.abs(motionEvent.getX() - this.v) > 5.0f) {
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                if (this.i) {
                    return false;
                }
                View a2 = this.a.a.a(x2, y2);
                this.j = a2;
                if (a2 == null) {
                    return false;
                }
                if (this.a.Y = (p) this.j.getTag().u()) {
                    return false;
                }
                this.a.m.requestDisallowInterceptTouchEvent(true);
                this.d = this.f;
                this.g = true;
                this.a.j.setTranslationY((float) this.j.getTop());
                Bitmap createBitmap = Bitmap.createBitmap(this.j.getWidth(), this.l, Bitmap.Config.ARGB_8888);
                this.j.draw(new Canvas(createBitmap));
                this.a.x.setImageBitmap(createBitmap);
                this.a.A.setVisibility(0);
                this.a.C.setVisibility(0);
                this.a.B.setVisibility(0);
                this.a.D.setVisibility(0);
                this.a.j.setVisibility(0);
                this.j.setVisibility(4);
                this.a.i.addView(this.a.o);
                return true;
            }
        }
        this.b.a(motionEvent);
        return false;
    }

    public final void b(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            this.d.a();
            return;
        }
        if (!this.k && Math.abs(this.w - motionEvent.getY()) > 20.0f) {
            this.k = true;
        }
        this.d.a(motionEvent.getX(), motionEvent.getY());
    }

    public final void b(boolean z2) {
        if (z2) {
            this.a.j.animate().alpha(0.0f).setDuration(150);
            this.a.x.animate().translationX(0.0f).setDuration(150).setListener(this.G.a());
            return;
        }
        this.a.x.animate().translationX(0.0f).setInterpolator(this.s).setListener(this.G).start();
    }
}
