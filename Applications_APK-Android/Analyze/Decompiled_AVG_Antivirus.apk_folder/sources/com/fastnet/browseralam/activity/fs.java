package com.fastnet.browseralam.activity;

import android.widget.SeekBar;

final class fs implements SeekBar.OnSeekBarChangeListener {
    final /* synthetic */ ReadingActivity a;

    fs(ReadingActivity readingActivity) {
        this.a = readingActivity;
    }

    public final void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        this.a.k.setTextSize(ReadingActivity.b(i));
    }

    public final void onStartTrackingTouch(SeekBar seekBar) {
    }

    public final void onStopTrackingTouch(SeekBar seekBar) {
    }
}
