package com.fastnet.browseralam.activity;

import android.content.DialogInterface;
import android.view.inputmethod.InputMethodManager;

final class fc implements DialogInterface.OnCancelListener {
    final /* synthetic */ ey a;

    fc(ey eyVar) {
        this.a = eyVar;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        ((InputMethodManager) this.a.f.getSystemService("input_method")).toggleSoftInput(1, 0);
    }
}
