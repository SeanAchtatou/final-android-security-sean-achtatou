package com.fastnet.browseralam.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.DecelerateInterpolator;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.e.a;
import com.fastnet.browseralam.i.aq;

public final class jj extends bi {
    jn a;
    int b = -1;
    int c = -10;
    int d;
    int e;
    int f;
    boolean g;
    final /* synthetic */ Cif h;
    private final int i = R.layout.file_item;
    /* access modifiers changed from: private */
    public final Drawable j = new ColorDrawable(this.k);
    private final int k = Color.parseColor("#8033b5e5");
    /* access modifiers changed from: private */
    public final int l;
    private DecelerateInterpolator m = new DecelerateInterpolator();
    private int n = aq.a(20);
    /* access modifiers changed from: private */
    public boolean o;
    private a p = new jk(this);
    private View.OnClickListener q = new jl(this);
    private View.OnClickListener r = this.q;
    private View.OnLongClickListener s = new jm(this);

    public jj(Cif ifVar) {
        this.h = ifVar;
        TypedValue typedValue = new TypedValue();
        ifVar.n.getTheme().resolveAttribute(16843534, typedValue, true);
        this.l = typedValue.resourceId;
        g(196);
    }

    public final int a() {
        if (this.h.Z != null) {
            return this.h.Z.size();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ cf a(ViewGroup viewGroup, int i2) {
        return new jn(this, this.h.o.inflate(this.i, viewGroup, false));
    }

    public final /* synthetic */ void a(cf cfVar, int i2) {
        jn jnVar = (jn) cfVar;
        jo joVar = (jo) this.h.Z.get(i2);
        int unused = joVar.j = i2;
        jnVar.r = joVar;
        jnVar.o.setText(joVar.c);
        jnVar.n.setImageDrawable(joVar.g);
        jnVar.q.setText(joVar.f);
        jnVar.p.setText(joVar.e);
        jnVar.m.setTag(jnVar);
        jnVar.m.setOnClickListener(this.r);
        jnVar.m.setOnLongClickListener(this.s);
        if (this.a != null && i2 == this.b) {
            jnVar.t();
        } else if (jnVar.s) {
            jnVar.u();
        }
        if (i2 >= this.e || i2 <= this.d) {
            this.e = 0;
            return;
        }
        jnVar.m.setAlpha(0.0f);
        jnVar.m.setTranslationY((float) this.n);
        if (i2 == this.f) {
            ViewPropertyAnimator interpolator = jnVar.m.animate().alpha(1.0f).translationY(0.0f).setDuration(250).setInterpolator(this.m);
            int i3 = this.c + 10;
            this.c = i3;
            interpolator.setStartDelay((long) i3).setListener(this.p);
            return;
        }
        ViewPropertyAnimator interpolator2 = jnVar.m.animate().alpha(1.0f).translationY(0.0f).setDuration(250).setInterpolator(this.m);
        int i4 = this.c + 10;
        this.c = i4;
        interpolator2.setStartDelay((long) i4).setListener(null);
    }

    public final void a(View.OnClickListener onClickListener) {
        if (onClickListener == null) {
            this.r = this.q;
        } else {
            this.r = onClickListener;
        }
    }

    public final void d() {
        this.g = false;
        this.o = true;
        this.d = this.h.X.i;
        this.e = this.d + this.h.d;
        this.f = this.e - 1;
        this.d--;
        this.h.t.e(this.h.X.i, 0);
        c();
    }

    public final void e() {
        this.b = -1;
        if (this.a != null) {
            this.a.u();
            this.a = null;
        }
    }

    public final boolean f() {
        return this.o;
    }

    public final void g(int i2) {
        int b2 = aq.b() - aq.a(i2);
        int a2 = aq.a(56);
        this.h.d = 0;
        for (int i3 = 0; i3 < b2; i3 += a2) {
            this.h.d++;
            this.h.e += a2;
        }
        this.f = this.h.d - 1;
    }
}
