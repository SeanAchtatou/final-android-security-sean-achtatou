package com.fastnet.browseralam.activity;

import android.view.MotionEvent;
import android.view.View;

final class cs implements View.OnTouchListener {
    final /* synthetic */ BrowserActivity a;

    cs(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            view.setPressed(false);
            this.a.T.c(this.a.L);
        }
        return true;
    }
}
