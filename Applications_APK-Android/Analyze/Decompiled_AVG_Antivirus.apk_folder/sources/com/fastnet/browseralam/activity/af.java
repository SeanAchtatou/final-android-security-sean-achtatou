package com.fastnet.browseralam.activity;

import android.animation.Animator;

final class af extends ay {
    final /* synthetic */ y a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    af(y yVar) {
        super(yVar, (byte) 0);
        this.a = yVar;
    }

    public final void onAnimationEnd(Animator animator) {
        this.a.l.b();
        this.a.c.setVisibility(8);
        if (!this.a.T) {
            this.a.m();
            this.a.o.f();
            this.a.x.setTranslationY((float) this.a.aj);
        } else {
            this.a.k();
            this.a.x.setTranslationY(((float) this.a.ah) - this.a.s.d());
        }
        ay unused = this.a.aA = this.a.aB;
    }
}
