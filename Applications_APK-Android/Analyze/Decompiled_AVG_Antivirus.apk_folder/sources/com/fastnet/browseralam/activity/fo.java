package com.fastnet.browseralam.activity;

import java.io.File;
import java.util.Comparator;

final class fo implements Comparator {
    final /* synthetic */ ey a;

    private fo(ey eyVar) {
        this.a = eyVar;
    }

    /* synthetic */ fo(ey eyVar, byte b) {
        this(eyVar);
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        File file = (File) obj;
        File file2 = (File) obj2;
        if (file.isDirectory() && file2.isDirectory()) {
            return file.getName().compareTo(file2.getName());
        }
        if (file.isDirectory()) {
            return -1;
        }
        if (file2.isDirectory() || !file.isFile() || !file2.isFile()) {
            return 1;
        }
        return file.getName().compareTo(file2.getName());
    }
}
