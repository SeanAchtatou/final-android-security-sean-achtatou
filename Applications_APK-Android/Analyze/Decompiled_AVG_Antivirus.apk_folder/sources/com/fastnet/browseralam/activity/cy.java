package com.fastnet.browseralam.activity;

import android.view.View;

final class cy implements View.OnClickListener {
    final /* synthetic */ int a;
    final /* synthetic */ BrowserActivity b;

    cy(BrowserActivity browserActivity, int i) {
        this.b = browserActivity;
        this.a = i;
    }

    public final void onClick(View view) {
        this.b.k = this.b.j;
        this.b.y.animate().translationY((float) this.a).setDuration(200).setInterpolator(this.b.cc);
        this.b.H.animate().alpha(0.0f).setDuration(200).setStartDelay(50).setInterpolator(this.b.cc).setListener(new cz(this));
    }
}
