package com.fastnet.browseralam.activity;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;
import com.fastnet.browseralam.i.ag;
import com.fastnet.browseralam.i.aq;

public class ReadingActivity extends AppCompatActivity {
    /* access modifiers changed from: private */
    public ScrollView i;
    private TextView j;
    /* access modifiers changed from: private */
    public TextView k;
    /* access modifiers changed from: private */
    public LinearLayout l;
    private FrameLayout m;
    /* access modifiers changed from: private */
    public WebView n;
    /* access modifiers changed from: private */
    public RelativeLayout o;
    /* access modifiers changed from: private */
    public EditText p;
    /* access modifiers changed from: private */
    public String q;
    private boolean r;
    private String s = null;
    private a t;
    /* access modifiers changed from: private */
    public int u;
    private ag v;

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        if (this.j.getVisibility() == 4) {
            this.j.setAlpha(0.0f);
            this.j.setVisibility(0);
            this.j.setText(str);
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.j, "alpha", 1.0f);
            ofFloat.setDuration(300L);
            ofFloat.start();
        } else {
            this.j.setText(str);
        }
        if (this.k.getVisibility() == 4) {
            this.k.setAlpha(0.0f);
            this.k.setVisibility(0);
            this.k.setText(str2);
            ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.k, "alpha", 1.0f);
            ofFloat2.setDuration(300L);
            ofFloat2.start();
            return;
        }
        this.k.setText(str2);
    }

    /* access modifiers changed from: private */
    public static float b(int i2) {
        switch (i2) {
            case 0:
                return 10.0f;
            case 1:
                return 14.0f;
            case 2:
            default:
                return 18.0f;
            case 3:
                return 22.0f;
            case 4:
                return 26.0f;
            case 5:
                return 30.0f;
        }
    }

    public void onBackPressed() {
        if (this.v != null) {
            this.v.a();
        }
        this.n.stopLoading();
        this.n.onPause();
        this.n.clearHistory();
        this.n.removeAllViews();
        this.n.destroyDrawingCache();
        this.n = null;
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z;
        this.t = a.a();
        this.r = a.w();
        if (this.r) {
            setTheme(2131296376);
        }
        super.onCreate(bundle);
        setContentView((int) R.layout.reading_view);
        a((Toolbar) findViewById(R.id.toolbar));
        c().a(true);
        this.m = (FrameLayout) findViewById(R.id.frame).getParent();
        this.i = (ScrollView) findViewById(R.id.reader);
        this.j = (TextView) findViewById(R.id.textViewTitle);
        this.k = (TextView) findViewById(R.id.textViewBody);
        this.u = a.Q();
        this.k.setTextSize(b(this.u));
        this.j.setText(getString(R.string.untitled));
        this.k.setText(getString(R.string.loading));
        this.j.setVisibility(4);
        this.k.setVisibility(4);
        Intent intent = getIntent();
        if (intent == null) {
            z = false;
        } else {
            this.s = intent.getStringExtra("ReadingUrl");
            if (this.s == null) {
                z = false;
            } else {
                c().a(aq.a(this.s));
                new fy(this, this).execute(this.s);
                z = true;
            }
        }
        if (!z) {
            a(getString(R.string.untitled), getString(R.string.loading_failed));
        }
        this.m = (FrameLayout) findViewById(R.id.frame);
        this.l = (LinearLayout) findViewById(R.id.container);
        this.n = new WebView(this);
        this.n.setAnimationCacheEnabled(false);
        this.n.setDrawingCacheEnabled(false);
        this.n.setWillNotCacheDrawing(true);
        this.n.setAlwaysDrawnWithCacheEnabled(false);
        this.n.getSettings().setUseWideViewPort(true);
        this.n.getSettings().setSupportZoom(true);
        this.n.getSettings().setBuiltInZoomControls(true);
        this.o = (RelativeLayout) getLayoutInflater().inflate((int) R.layout.find_in_page, (ViewGroup) null);
        ((FrameLayout) this.m.getParent()).addView(this.o, -1);
        this.o.setVisibility(8);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.o.getLayoutParams();
        layoutParams.height = aq.a(56);
        this.o.setLayoutParams(layoutParams);
        this.p = (EditText) this.o.findViewById(R.id.search_query);
        this.p.setOnEditorActionListener(new fu(this));
        ((ImageButton) this.o.findViewById(R.id.button_next)).setOnClickListener(new fv(this));
        ((ImageButton) this.o.findViewById(R.id.button_back)).setOnClickListener(new fw(this));
        ((ImageButton) this.o.findViewById(R.id.button_quit)).setOnClickListener(new fx(this));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reading, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        boolean z = false;
        switch (menuItem.getItemId()) {
            case R.id.action_find:
                this.o.setVisibility(0);
                break;
            case R.id.text_size_item:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                View inflate = getLayoutInflater().inflate((int) R.layout.seek_layout, (ViewGroup) null);
                SeekBar seekBar = (SeekBar) inflate.findViewById(R.id.text_size_seekbar);
                seekBar.setOnSeekBarChangeListener(new fs(this));
                seekBar.setMax(5);
                seekBar.setProgress(this.u);
                builder.setView(inflate);
                builder.setTitle((int) R.string.size);
                builder.setPositiveButton(17039370, new ft(this, seekBar));
                builder.show();
                break;
            case R.id.invert_item:
                if (!this.r) {
                    z = true;
                }
                a.r(z);
                Intent intent = new Intent(this, ReadingActivity.class);
                intent.putExtra("ReadingUrl", this.s);
                startActivity(intent);
                finish();
                break;
            case R.id.page_source_item:
                this.v = new ag(this, this.s, false);
                this.v.a(this, new fr(this));
                break;
            default:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
