package com.fastnet.browseralam.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

public class MainActivity extends BrowserActivity {
    CookieManager n;

    public final synchronized void h() {
        i();
    }

    public final void j() {
        this.n = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT < 21) {
            CookieSyncManager.createInstance(this);
        }
        CookieManager cookieManager = this.n;
        a.a();
        cookieManager.setAcceptCookie(a.j());
        super.j();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        a(intent);
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        s();
    }
}
