package com.fastnet.browseralam.activity;

import android.view.View;
import com.fastnet.browseralam.R;

final class ar implements View.OnClickListener {
    final /* synthetic */ y a;

    ar(y yVar) {
        this.a = yVar;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.bookmarks_textview:
                if (this.a.E != this.a.H) {
                    this.a.n();
                    return;
                }
                return;
            case R.id.normal_tabs:
                if (this.a.E == this.a.F) {
                    return;
                }
                if (this.a.T) {
                    this.a.k();
                    return;
                } else {
                    this.a.m();
                    return;
                }
            case R.id.incognito_tabs:
                if (this.a.E == this.a.G) {
                    return;
                }
                if (this.a.T) {
                    this.a.l();
                    return;
                } else {
                    y.k(this.a);
                    return;
                }
            case R.id.files_textview:
                if (this.a.E != this.a.I) {
                    y.o(this.a);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
