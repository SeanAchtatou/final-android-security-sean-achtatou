package com.fastnet.browseralam.activity;

import android.view.MotionEvent;
import android.view.View;

final class cr implements View.OnTouchListener {
    final /* synthetic */ BrowserActivity a;

    cr(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            view.setPressed(false);
            if (this.a.ar) {
                this.a.V.c();
            } else {
                BrowserActivity.L(this.a);
            }
        }
        return true;
    }
}
