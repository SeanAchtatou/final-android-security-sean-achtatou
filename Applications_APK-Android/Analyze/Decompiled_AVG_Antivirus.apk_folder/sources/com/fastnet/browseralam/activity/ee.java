package com.fastnet.browseralam.activity;

import android.media.MediaPlayer;

final class ee implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    final /* synthetic */ BrowserActivity a;

    private ee(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    /* synthetic */ ee(BrowserActivity browserActivity, byte b) {
        this(browserActivity);
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        this.a.x();
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        return false;
    }
}
