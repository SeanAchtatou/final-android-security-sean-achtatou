package com.fastnet.browseralam.activity;

import android.support.v7.widget.cf;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;

final class fk extends cf {
    final LinearLayout l;
    final ImageView m;
    final TextView n;
    final /* synthetic */ fh o;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fk(fh fhVar, View view) {
        super(view);
        this.o = fhVar;
        this.n = (TextView) view.findViewById(R.id.fileName);
        this.m = (ImageView) view.findViewById(R.id.fileIcon);
        this.l = (LinearLayout) view.findViewById(R.id.file_row);
    }
}
