package com.fastnet.browseralam.activity;

import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.c.c;
import com.fastnet.browseralam.c.f;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class ii implements Runnable {
    final /* synthetic */ File a;
    final /* synthetic */ List b;
    final /* synthetic */ Cif c;

    ii(Cif ifVar, File file, List list) {
        this.c = ifVar;
        this.a = file;
        this.b = list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.if, java.io.File, boolean):void
     arg types: [com.fastnet.browseralam.activity.if, java.io.File, int]
     candidates:
      com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.if, java.io.File, com.fastnet.browseralam.c.f):long
      com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.if, java.io.File, boolean):void */
    public final void run() {
        c unused = this.c.ad = c.a(this.c.m.getApplicationContext());
        HashMap unused2 = this.c.ab = c.b(this.a.getPath().hashCode());
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        if (this.c.ab.size() != 0) {
            int i = 0;
            for (jo joVar : this.c.Z) {
                if (joVar.h) {
                    f fVar = (f) this.c.ab.get(joVar.c);
                    if (fVar != null) {
                        String unused3 = joVar.f = fVar.d();
                    } else if (!this.b.contains(Integer.valueOf(i))) {
                        arrayList.add(joVar.b);
                        arrayList2.add(Integer.valueOf(i));
                    }
                    i++;
                }
            }
            if (this.c.W.f()) {
                this.c.W.g = true;
            } else {
                k.b(this.c.at);
            }
            int i2 = 0;
            for (String file : arrayList) {
                Cif.a(this.c, new File(file), false);
                this.c.W.b(((Integer) arrayList2.get(i2)).intValue());
                i2++;
            }
            ArrayList arrayList3 = new ArrayList();
            for (Map.Entry value : this.c.ab.entrySet()) {
                f fVar2 = (f) value.getValue();
                if (!fVar2.f()) {
                    arrayList3.add(fVar2.g());
                }
            }
            this.c.ad.b(arrayList3);
            return;
        }
        Cif.a(this.c, this.a, true);
    }
}
