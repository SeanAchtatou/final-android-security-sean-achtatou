package com.fastnet.browseralam.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.h;
import com.fastnet.browseralam.g.l;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.CardViewPager;
import com.fastnet.browseralam.view.XWebView;
import java.util.List;

public final class eg extends h {
    private final BrowserActivity a;
    private final LayoutInflater b;
    private final FrameLayout c;
    private final FrameLayout d;
    private final FrameLayout e;
    private final CardViewPager f;
    private final RelativeLayout g;
    private final l h;
    /* access modifiers changed from: private */
    public final List i;
    private final int j = aq.a();
    private final int k = (aq.b() - aq.a(56));
    private final DecelerateInterpolator l = new DecelerateInterpolator(1.0f);
    private boolean m;
    private Bitmap n;

    public eg(BrowserActivity browserActivity, FrameLayout frameLayout, List list) {
        this.a = browserActivity;
        this.b = browserActivity.getLayoutInflater();
        this.i = list;
        this.c = frameLayout;
        this.f = (CardViewPager) frameLayout.findViewById(R.id.card_pager);
        this.g = (RelativeLayout) frameLayout.findViewById(R.id.toolbar_layout);
        this.e = (FrameLayout) frameLayout.findViewById(R.id.background_frame);
        this.d = (FrameLayout) frameLayout.findViewById(R.id.webview_frame);
        this.h = new eh(this, (byte) 0);
        this.a.a(this.h);
        this.f.a(this.h);
    }

    public final void b(XWebView xWebView) {
        Bitmap createBitmap = Bitmap.createBitmap(this.j, this.k, Bitmap.Config.ARGB_8888);
        this.d.draw(new Canvas(createBitmap));
        this.n = createBitmap;
        xWebView.a(this.n);
    }

    public final boolean b() {
        return true;
    }

    public final void c(XWebView xWebView) {
        if (!this.m) {
            xWebView.w().flingScroll(0, 0);
            this.n = Bitmap.createBitmap(this.j, this.k, Bitmap.Config.ARGB_8888);
            this.d.draw(new Canvas(this.n));
            xWebView.a(this.n);
            this.d.setVisibility(8);
            this.f.setVisibility(0);
            this.g.animate().alpha(0.0f).setDuration(200).setInterpolator(this.l);
            this.f.a();
            this.f.requestLayout();
        }
    }
}
