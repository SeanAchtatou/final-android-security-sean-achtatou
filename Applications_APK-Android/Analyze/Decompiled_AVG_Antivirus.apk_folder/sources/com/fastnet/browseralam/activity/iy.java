package com.fastnet.browseralam.activity;

import android.content.DialogInterface;
import android.widget.EditText;
import java.io.File;

final class iy implements DialogInterface.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ String b;
    final /* synthetic */ jo c;
    final /* synthetic */ Cif d;

    iy(Cif ifVar, EditText editText, String str, jo joVar) {
        this.d = ifVar;
        this.a = editText;
        this.b = str;
        this.c = joVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String trim = this.a.getText().toString().trim();
        if (this.b.contains("New Folder")) {
            File file = new File(this.c.b + "/" + trim);
            if (!file.exists()) {
                file.mkdir();
                int size = this.d.Z.size() - 1;
                while (true) {
                    if (size <= 0) {
                        break;
                    } else if (((jo) this.d.Z.get(size)).d.equals("Folder")) {
                        this.d.Z.add(size + 1, new jo(this.d, file, this.d.T));
                        this.d.W.c_(size + 1);
                        break;
                    } else {
                        size--;
                    }
                }
            }
        } else if (!trim.equals("")) {
            File file2 = new File(this.c.b);
            if (this.b.contains("Edit Folder")) {
                file2.renameTo(new File(file2.getParent() + "/" + trim));
                String unused = this.c.c = trim;
                this.d.W.b(this.d.Z.indexOf(this.c));
            } else if (this.b.contains("Edit File")) {
                file2.renameTo(new File(file2.getParent() + "/" + trim + file2.getPath()));
                String unused2 = this.c.c = trim;
                this.d.W.b(this.d.Z.indexOf(this.c));
            }
        }
        this.d.W.e();
    }
}
