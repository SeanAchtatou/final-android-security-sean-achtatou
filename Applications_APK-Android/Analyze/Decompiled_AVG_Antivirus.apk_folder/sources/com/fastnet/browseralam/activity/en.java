package com.fastnet.browseralam.activity;

import android.graphics.PorterDuff;
import android.view.MotionEvent;
import android.view.View;

final class en implements View.OnTouchListener {
    final /* synthetic */ ei a;

    en(ei eiVar) {
        this.a = eiVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [?[OBJECT, ARRAY], int, int, boolean]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.getBackground().setColorFilter(this.a.D, PorterDuff.Mode.SRC_ATOP);
        } else if (motionEvent.getAction() == 1) {
            view.getBackground().clearColorFilter();
            if (!this.a.O) {
                boolean unused = this.a.O = true;
                this.a.a.a((String) null, false, this.a.s.h() + 1, this.a.t == this.a.l);
            }
        }
        return true;
    }
}
