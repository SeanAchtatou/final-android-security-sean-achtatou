package com.fastnet.browseralam.activity;

import android.animation.Animator;
import android.graphics.drawable.ColorDrawable;

final class bb extends ay {
    final /* synthetic */ y a;
    private int c;
    private int d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bb(y yVar, int i) {
        super(yVar, (byte) 0);
        this.a = yVar;
        if (i == 0) {
            this.c = -12303292;
            this.d = -1;
            return;
        }
        this.c = -1;
        this.d = ((ColorDrawable) yVar.g.getBackground()).getColor();
    }

    public final void onAnimationEnd(Animator animator) {
        this.a.J.setVisibility(0);
        this.a.J.setTextColor(this.c);
        this.a.J.setBackgroundColor(this.d);
    }
}
