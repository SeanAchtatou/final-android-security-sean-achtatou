package com.fastnet.browseralam.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.a;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.l;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public final class ey {
    private final Pattern A = Pattern.compile("/");
    private final File a = Environment.getExternalStorageDirectory();
    private final String b = this.a.getParent();
    /* access modifiers changed from: private */
    public final fl c = new fl(this, this.a, null);
    private fl d;
    private List e;
    /* access modifiers changed from: private */
    public final Activity f;
    /* access modifiers changed from: private */
    public final LayoutInflater g;
    /* access modifiers changed from: private */
    public final fh h;
    /* access modifiers changed from: private */
    public AlertDialog i;
    private LinearLayout j;
    private RecyclerView k;
    /* access modifiers changed from: private */
    public List l = new ArrayList();
    private TextView m;
    /* access modifiers changed from: private */
    public fl n;
    /* access modifiers changed from: private */
    public fl o;
    /* access modifiers changed from: private */
    public Drawable p;
    private Drawable q;
    private Drawable r;
    private Drawable s;
    /* access modifiers changed from: private */
    public boolean t;
    private boolean u;
    /* access modifiers changed from: private */
    public fn v;
    private final String[] w = {"New Folder", "Edit File", "Edit Folder"};
    private final int x = 0;
    private final int y = 1;
    private final int z = 2;

    public ey(Activity activity, fm fmVar) {
        this.f = activity;
        this.g = activity.getLayoutInflater();
        this.h = new fh(this);
        this.p = a.a(this.f, R.drawable.folder);
        this.q = a.a(this.f, R.drawable.ic_txt_file);
        this.r = a.a(this.f, R.drawable.ic_file);
        this.s = a.a(this.f, R.drawable.ic_back);
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f);
        this.j = (LinearLayout) this.g.inflate((int) R.layout.file_chooser, (ViewGroup) null);
        builder.setView(this.j);
        this.k = (RecyclerView) this.j.findViewById(R.id.file_listview);
        this.k.a(this.h);
        this.k.a(new l());
        this.j.findViewById(R.id.dialogNewFolder).setOnClickListener(new fa(this));
        this.j.findViewById(R.id.folder_back).setOnClickListener(new fb(this));
        this.i = builder.create();
        this.i.setCanceledOnTouchOutside(true);
        d();
        this.m = (TextView) this.j.findViewById(R.id.finish);
        this.m.setOnClickListener(new ez(this, fmVar));
    }

    static /* synthetic */ void a(ey eyVar, int i2, fl flVar) {
        AlertDialog.Builder builder = new AlertDialog.Builder(eyVar.f);
        RelativeLayout relativeLayout = (RelativeLayout) eyVar.g.inflate((int) R.layout.edit_file_dialog, (ViewGroup) null);
        builder.setView(relativeLayout);
        AlertDialog create = builder.create();
        create.setCanceledOnTouchOutside(true);
        create.setOnCancelListener(new fc(eyVar));
        ((TextView) relativeLayout.findViewById(R.id.edit_title)).setText(eyVar.w[i2]);
        EditText editText = (EditText) relativeLayout.findViewById(R.id.edit_name);
        if (i2 != 0) {
            editText.setText(flVar.c);
        } else {
            editText.setText("folder");
        }
        relativeLayout.findViewById(R.id.ok).setOnClickListener(new fd(eyVar, editText, i2, flVar, create));
        relativeLayout.findViewById(R.id.cancel).setOnClickListener(new fe(eyVar, create));
        if (i2 != 0) {
            View findViewById = relativeLayout.findViewById(R.id.delete);
            findViewById.setVisibility(0);
            findViewById.setOnClickListener(new ff(eyVar, flVar, create));
        }
        create.show();
        if (i2 != 0) {
            int lastIndexOf = flVar.c.lastIndexOf(46);
            if (lastIndexOf != -1) {
                editText.setSelection(0, lastIndexOf);
            } else {
                editText.selectAll();
            }
        } else {
            editText.selectAll();
        }
        k.a(new fg(eyVar, editText), 150);
    }

    /* access modifiers changed from: private */
    public void a(fl flVar) {
        boolean z2;
        if (flVar == this.d) {
            if (!this.u) {
                z2 = false;
            } else {
                this.l = new ArrayList();
                this.l.add(this.c);
                for (fl add : this.e) {
                    this.l.add(add);
                }
                this.h.c();
                z2 = true;
            }
            if (z2) {
                return;
            }
        }
        File file = new File(flVar.b);
        if (!file.exists()) {
            file = this.a;
        }
        this.l = new ArrayList();
        this.o = flVar;
        if (!flVar.b.equals(this.c.b) && !c(flVar.b)) {
            this.n = new fl(this, file.getParentFile(), this.s);
            this.l.add(this.n);
        } else if (this.u) {
            this.n = this.d;
            this.l.add(this.d);
        } else {
            this.n = this.c;
        }
        if (file.exists() && file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                Arrays.sort(listFiles, new fo(this, (byte) 0));
                for (File file2 : listFiles) {
                    if (file2.isDirectory()) {
                        this.l.add(new fl(this, file2, this.p));
                    } else {
                        this.l.add(new fl(this, file2, this.r));
                    }
                }
            } else {
                return;
            }
        }
        this.k.a(0);
        this.h.c();
    }

    /* access modifiers changed from: private */
    public static void b(File file) {
        if (file.exists() && file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                if (file2.isDirectory()) {
                    b(file2);
                } else {
                    file2.delete();
                }
            }
        }
        file.delete();
    }

    private boolean c(String str) {
        if (!this.u) {
            return false;
        }
        for (fl a2 : this.e) {
            if (str.equals(a2.b)) {
                return true;
            }
        }
        return false;
    }

    private void d() {
        new ArrayList();
        System.getenv("EXTERNAL_STORAGE");
        String str = System.getenv("SECONDARY_STORAGE");
        System.getenv("EMULATED_STORAGE_TARGET");
        if (!TextUtils.isEmpty(str)) {
            this.u = true;
            this.e = new ArrayList();
            this.d = new fl(this);
            boolean unused = this.d.f = true;
            Drawable unused2 = this.d.e = this.s;
            String unused3 = this.d.c = "Storage";
            Drawable unused4 = this.c.e = this.p;
            for (String file : str.split(File.pathSeparator)) {
                this.e.add(new fl(this, new File(file), this.p));
            }
        }
    }

    public final ey a() {
        com.fastnet.browseralam.h.a.a();
        File file = new File(com.fastnet.browseralam.h.a.k());
        if (!file.exists() && !file.mkdir()) {
            file = new File(this.c.b + "/Download/");
        }
        a(new fl(this, file, this.p));
        return this;
    }

    public final ey a(View view) {
        this.j.addView(view, 2, new LinearLayout.LayoutParams(-1, aq.a(46)));
        return this;
    }

    public final ey a(fn fnVar) {
        this.t = true;
        this.v = fnVar;
        return this;
    }

    public final ey a(String str) {
        ((TextView) this.j.findViewById(R.id.dialog_title)).setText(str);
        return this;
    }

    public final ey b() {
        a(this.c);
        return this;
    }

    public final ey b(String str) {
        ((TextView) this.j.findViewById(R.id.finish)).setText(str);
        return this;
    }

    public final void c() {
        this.i.show();
    }
}
