package com.fastnet.browseralam.activity;

import android.view.View;

final class cc implements View.OnLongClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ boolean b;
    final /* synthetic */ BrowserActivity c;

    cc(BrowserActivity browserActivity, String str, boolean z) {
        this.c = browserActivity;
        this.a = str;
        this.b = z;
    }

    public final boolean onLongClick(View view) {
        this.c.a(this.a, !this.c.at, this.c.L.h() + 1, this.b);
        this.c.bs.dismiss();
        return true;
    }
}
