package com.fastnet.browseralam.activity;

import android.view.MotionEvent;
import android.view.View;

final class ct implements View.OnTouchListener {
    final /* synthetic */ BrowserActivity a;

    ct(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.BrowserActivity, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean */
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        this.a.ab.a(motionEvent);
        if (motionEvent.getAction() == 0) {
            view.setPressed(true);
            boolean unused = this.a.aw = false;
        } else if (motionEvent.getAction() == 1) {
            view.setPressed(false);
            if (!this.a.aw) {
                if (this.a.ar) {
                    this.a.V.c();
                } else if (this.a.as) {
                    BrowserActivity.L(this.a);
                } else {
                    this.a.T.c(this.a.L);
                }
            }
        }
        return true;
    }
}
