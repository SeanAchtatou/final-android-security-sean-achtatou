package com.fastnet.browseralam.activity;

import android.view.View;
import com.fastnet.browseralam.i.aq;

final class dc implements View.OnClickListener {
    final /* synthetic */ BrowserActivity a;

    dc(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    public final void onClick(View view) {
        if (this.a.bY.isChecked()) {
            this.a.bY.callOnClick();
        }
        if (this.a.bY.isChecked()) {
            aq.a(this.a.ao, "You must disable intercept videos first");
            return;
        }
        this.a.bZ.setChecked(!this.a.bZ.isChecked());
        this.a.L.b(this.a.bZ.isChecked());
    }
}
