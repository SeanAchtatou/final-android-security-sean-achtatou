package com.fastnet.browseralam.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.webkit.WebView;
import com.fastnet.browseralam.view.g;

final class eu implements g {
    final /* synthetic */ es a;

    eu(es esVar) {
        this.a = esVar;
    }

    public final void a(WebView webView) {
        Bitmap createBitmap = Bitmap.createBitmap(this.a.a.w, this.a.a.x, Bitmap.Config.ARGB_8888);
        this.a.a.u.draw(new Canvas(createBitmap));
        BitmapDrawable unused = this.a.a.K = new BitmapDrawable(this.a.a.a.getResources(), Bitmap.createScaledBitmap(createBitmap, this.a.a.z, this.a.a.A, false));
        createBitmap.recycle();
        this.a.a.a.runOnUiThread(new ev(this, webView));
    }
}
