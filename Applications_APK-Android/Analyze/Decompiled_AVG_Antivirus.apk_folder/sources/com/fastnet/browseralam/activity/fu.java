package com.fastnet.browseralam.activity;

import android.view.KeyEvent;
import android.widget.TextView;

final class fu implements TextView.OnEditorActionListener {
    final /* synthetic */ ReadingActivity a;

    fu(ReadingActivity readingActivity) {
        this.a = readingActivity;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6 && i != 2) {
            return false;
        }
        this.a.n.findAllAsync(this.a.q = this.a.p.getText().toString());
        return true;
    }
}
