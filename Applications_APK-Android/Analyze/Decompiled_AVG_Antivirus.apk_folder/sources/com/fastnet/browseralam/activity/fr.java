package com.fastnet.browseralam.activity;

import android.view.ViewGroup;
import com.fastnet.browseralam.i.ap;

final class fr implements ap {
    final /* synthetic */ ReadingActivity a;

    fr(ReadingActivity readingActivity) {
        this.a = readingActivity;
    }

    public final void a(String str) {
        this.a.n.loadData(str, "text/html", "utf-8");
        this.a.l.removeView(this.a.n);
        this.a.l.addView(this.a.n, new ViewGroup.LayoutParams(-1, -1));
        this.a.i.setVisibility(8);
        this.a.l.setVisibility(0);
    }
}
