package com.fastnet.browseralam.activity;

import android.view.View;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;
import com.fastnet.browseralam.i.aq;

final class bp implements View.OnLongClickListener {
    final /* synthetic */ BrowserActivity a;

    bp(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [java.lang.String, int, int, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    public final boolean onLongClick(View view) {
        a unused = this.a.aS;
        String Z = a.Z();
        if (Z != null) {
            this.a.a(Z, false, -1, false);
            aq.a(this.a.ao, (int) R.string.deleted_tab);
        }
        a unused2 = this.a.aS;
        a.f((String) null);
        return true;
    }
}
