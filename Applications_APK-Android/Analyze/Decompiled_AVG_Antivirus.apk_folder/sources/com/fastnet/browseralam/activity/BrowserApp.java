package com.fastnet.browseralam.activity;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;
import com.jobstrak.drawingfun.sdk.Cryopiggy;

public class BrowserApp extends Application {
    private static Context a;

    public static Context a() {
        return a;
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public void onCreate() {
        super.onCreate();
        Cryopiggy.init(this);
        a = getApplicationContext();
    }
}
