package com.fastnet.browseralam.activity;

import android.view.View;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class aq implements View.OnLongClickListener {
    final /* synthetic */ y a;

    aq(y yVar) {
        this.a = yVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [java.lang.String, int, int, boolean]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    public final boolean onLongClick(View view) {
        a unused = this.a.j;
        String Z = a.Z();
        if (Z != null) {
            this.a.b.a(Z, false, -1, this.a.E == this.a.G);
            com.fastnet.browseralam.i.aq.a(this.a.b, (int) R.string.deleted_tab);
        }
        return true;
    }
}
