package com.fastnet.browseralam.activity;

import android.graphics.drawable.Drawable;
import android.support.v4.content.a;
import android.support.v7.widget.bi;
import android.support.v7.widget.bt;
import android.support.v7.widget.cf;
import android.view.ViewGroup;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.g.j;
import java.util.Collections;

public final class hk extends bi implements j {
    final int a = R.layout.bottom_bookmark_item;
    final Drawable b;
    final Drawable c;
    final /* synthetic */ gr d;
    private hm e;
    /* access modifiers changed from: private */
    public hn f;
    private int g;

    public hk(gr grVar) {
        this.d = grVar;
        this.b = a.a(grVar.e, R.drawable.ic_folder);
        this.c = a.a(grVar.e, R.drawable.ic_folder_highlight);
        this.e = new hm(this, (byte) 0);
        this.f = new hn(this);
        d();
    }

    public final int a() {
        if (this.d.J != null) {
            return this.d.J.size();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ cf a(ViewGroup viewGroup, int i) {
        return new hl(this, this.d.h.inflate(this.a, viewGroup, false));
    }

    public final /* synthetic */ void a(cf cfVar, int i) {
        hl hlVar = (hl) cfVar;
        b bVar = (b) this.d.J.get(i);
        hlVar.r = bVar;
        boolean c2 = bVar.c();
        hlVar.p = c2;
        if (c2) {
            hlVar.o.setImageDrawable(this.b);
        } else {
            hlVar.o.setImageBitmap(bVar.i());
        }
        hlVar.m.setText(bVar.a());
        hlVar.l.setOnClickListener(this.e);
        if (i != 0 || this.d.S) {
            hlVar.q = false;
            hlVar.n.setVisibility(this.g);
            return;
        }
        hlVar.q = true;
        hlVar.m.setText("back");
        if (this.d.V) {
            hlVar.n.setVisibility(8);
        }
    }

    public final void b(int i, int i2) {
        Collections.swap(this.d.J, i, i2);
        a_(i, i2);
        if (i == 0) {
            this.d.L.c(0);
        }
        this.d.T = true;
    }

    public final void c(int i, int i2) {
        this.d.b((b) this.d.J.get(i));
        ((b) this.d.J.get(i2)).a((b) this.d.J.get(i));
        this.d.J.remove(i);
        d_(i);
        this.d.T = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hu):com.fastnet.browseralam.activity.hu
     arg types: [com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hq]
     candidates:
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.support.v7.widget.bt):android.support.v7.widget.bt
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.widget.PopupWindow):android.widget.PopupWindow
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.e.p):com.fastnet.browseralam.e.p
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, boolean):boolean
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.activity.gr.a(java.io.File, boolean):boolean
      com.fastnet.browseralam.b.f.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.b.f.a(java.io.File, boolean):boolean
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hu):com.fastnet.browseralam.activity.hu */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.support.v7.widget.bt):android.support.v7.widget.bt
     arg types: [com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hq]
     candidates:
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.widget.PopupWindow):android.widget.PopupWindow
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hu):com.fastnet.browseralam.activity.hu
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.e.p):com.fastnet.browseralam.e.p
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, boolean):boolean
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.activity.gr.a(java.io.File, boolean):boolean
      com.fastnet.browseralam.b.f.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.b.f.a(java.io.File, boolean):boolean
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.support.v7.widget.bt):android.support.v7.widget.bt */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hu):com.fastnet.browseralam.activity.hu
     arg types: [com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hw]
     candidates:
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.support.v7.widget.bt):android.support.v7.widget.bt
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.widget.PopupWindow):android.widget.PopupWindow
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.e.p):com.fastnet.browseralam.e.p
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, boolean):boolean
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.activity.gr.a(java.io.File, boolean):boolean
      com.fastnet.browseralam.b.f.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.b.f.a(java.io.File, boolean):boolean
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hu):com.fastnet.browseralam.activity.hu */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.support.v7.widget.bt):android.support.v7.widget.bt
     arg types: [com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hw]
     candidates:
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.widget.PopupWindow):android.widget.PopupWindow
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hu):com.fastnet.browseralam.activity.hu
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.e.p):com.fastnet.browseralam.e.p
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, boolean):boolean
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.activity.gr.a(java.io.File, boolean):boolean
      com.fastnet.browseralam.b.f.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.b.f.a(java.io.File, boolean):boolean
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.support.v7.widget.bt):android.support.v7.widget.bt */
    public final void d() {
        this.d.a.a((bi) null);
        this.d.a.a(this);
        if (this.d.V) {
            hq hqVar = new hq(this.d);
            hu unused = this.d.X = (hu) hqVar;
            this.d.a.b(this.d.W);
            this.d.a.a(hqVar);
            bt unused2 = this.d.W = (bt) hqVar;
            this.g = 0;
            return;
        }
        hw hwVar = new hw(this.d);
        hu unused3 = this.d.X = (hu) hwVar;
        this.d.a.b(this.d.W);
        this.d.a.a(hwVar);
        bt unused4 = this.d.W = (bt) hwVar;
        this.g = 8;
    }
}
