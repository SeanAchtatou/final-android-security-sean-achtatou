package com.fastnet.browseralam.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.fastnet.browseralam.h.a;

public abstract class ThemableActivity extends AppCompatActivity {
    /* access modifiers changed from: protected */
    public final void L() {
        onSaveInstanceState(new Bundle());
        Intent intent = new Intent(this, getClass());
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        a.a();
        if (a.ai()) {
            setTheme(2131296373);
        }
        super.onCreate(bundle);
    }
}
