package com.fastnet.browseralam.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.view.View;

final class cg implements View.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ String b;
    final /* synthetic */ BrowserActivity c;

    cg(BrowserActivity browserActivity, String str, String str2) {
        this.c = browserActivity;
        this.a = str;
        this.b = str2;
    }

    public final void onClick(View view) {
        ((ClipboardManager) this.c.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("label", this.a == null ? this.b : this.a));
        this.c.bs.dismiss();
    }
}
