package com.fastnet.browseralam.activity;

final class ie implements hv {
    final /* synthetic */ hw a;

    private ie(hw hwVar) {
        this.a = hwVar;
    }

    /* synthetic */ ie(hw hwVar, byte b) {
        this(hwVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.hw.e(com.fastnet.browseralam.activity.hw, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.hw, int]
     candidates:
      com.fastnet.browseralam.activity.hw.e(com.fastnet.browseralam.activity.hw, float):float
      com.fastnet.browseralam.activity.hw.e(com.fastnet.browseralam.activity.hw, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.hw.a(com.fastnet.browseralam.activity.hw, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.hw, int]
     candidates:
      com.fastnet.browseralam.activity.hw.a(com.fastnet.browseralam.activity.hw, float):float
      com.fastnet.browseralam.activity.hw.a(com.fastnet.browseralam.activity.hw, int):int
      com.fastnet.browseralam.activity.hw.a(com.fastnet.browseralam.activity.hw, android.view.View):android.view.View
      com.fastnet.browseralam.activity.hw.a(com.fastnet.browseralam.activity.hw, com.fastnet.browseralam.activity.hv):com.fastnet.browseralam.activity.hv
      com.fastnet.browseralam.activity.hw.a(com.fastnet.browseralam.activity.hw, boolean):boolean */
    public final void a() {
        if ((this.a.x <= 0.0f || this.a.x >= ((float) this.a.l)) && (this.a.x >= 0.0f || this.a.x <= ((float) (-this.a.l)))) {
            if (this.a.x < 0.0f) {
                if (this.a.x > ((float) this.a.r)) {
                    this.a.a.x.animate().translationX((float) this.a.r).setInterpolator(this.a.s).setListener(null);
                }
                float unused = this.a.x = (float) this.a.r;
                this.a.a.A.setVisibility(8);
                this.a.a.C.setVisibility(8);
            } else if (this.a.x > 0.0f) {
                if (this.a.x < ((float) this.a.q)) {
                    this.a.a.x.animate().translationX((float) this.a.q).setInterpolator(this.a.s).setListener(null);
                }
                float unused2 = this.a.x = (float) this.a.q;
                this.a.a.B.setVisibility(8);
                this.a.a.D.setVisibility(8);
            }
            boolean unused3 = this.a.g = false;
            boolean unused4 = this.a.i = true;
            this.a.a.c.k = this.a.H;
            return;
        }
        boolean unused5 = this.a.i = false;
        this.a.a.x.animate().translationX(0.0f).setInterpolator(this.a.s).setListener(this.a.G);
    }

    public final void a(float f, float f2) {
        float unused = this.a.x = f - this.a.v;
        if (this.a.x < ((float) this.a.r)) {
            this.a.a.x.setTranslationX((float) this.a.r);
        } else if (this.a.x > ((float) this.a.q)) {
            this.a.a.x.setTranslationX((float) this.a.q);
        } else {
            this.a.a.x.setTranslationX(f - this.a.v);
        }
    }
}
