package com.fastnet.browseralam.activity;

import android.animation.Animator;

final class gh extends go {
    final /* synthetic */ fz a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    gh(fz fzVar) {
        super(fzVar, (byte) 0);
        this.a = fzVar;
    }

    public final void onAnimationEnd(Animator animator) {
        this.a.c.setVisibility(8);
        this.a.c.setAlpha(1.0f);
        this.a.j.setTranslationY((float) this.a.A);
        this.a.b.n();
        this.a.n.setVisibility(0);
    }
}
