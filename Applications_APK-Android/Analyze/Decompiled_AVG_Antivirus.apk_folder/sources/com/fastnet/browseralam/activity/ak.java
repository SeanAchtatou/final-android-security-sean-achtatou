package com.fastnet.browseralam.activity;

import android.view.Menu;
import android.view.View;
import android.widget.PopupMenu;
import com.fastnet.browseralam.R;

final class ak implements View.OnClickListener {
    final /* synthetic */ y a;

    ak(y yVar) {
        this.a = yVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [?[OBJECT, ARRAY], int, int, boolean]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    public final void onClick(View view) {
        boolean z = true;
        switch (view.getId()) {
            case R.id.back_button:
                if (!this.a.b.D().t()) {
                    this.a.b.a(this.a.b.D().h(), this.a.b.D().D());
                    return;
                }
                return;
            case R.id.forward_button:
                this.a.b.D().u();
                return;
            case R.id.new_tab_button:
                if (!this.a.T) {
                    int d = this.a.o.d();
                    BrowserActivity e = this.a.b;
                    int i = d + 1;
                    if (this.a.E != this.a.G) {
                        z = false;
                    }
                    e.a((String) null, false, i, z);
                    return;
                }
                BrowserActivity e2 = this.a.b;
                if (this.a.E != this.a.G) {
                    z = false;
                }
                e2.a((String) null, false, -1, z);
                return;
            case R.id.bookmark_button:
            default:
                return;
            case R.id.tabs_button:
                this.a.c();
                return;
            case R.id.menu_button:
                PopupMenu popupMenu = new PopupMenu(this.a.b, view, 5);
                Menu menu = popupMenu.getMenu();
                if (this.a.T) {
                    menu.add("Card View");
                } else {
                    menu.add("List View");
                }
                popupMenu.setOnMenuItemClickListener(new al(this));
                popupMenu.show();
                return;
        }
    }
}
