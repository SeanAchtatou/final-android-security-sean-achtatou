package com.fastnet.browseralam.activity;

import android.graphics.drawable.Drawable;
import android.support.v4.content.a;
import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.view.ViewGroup;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.g.j;
import java.util.Collections;

public final class o extends bi implements j {
    final int a = R.layout.bookmark_item;
    final Drawable b;
    final Drawable c;
    final /* synthetic */ a d;
    private final q e;
    /* access modifiers changed from: private */
    public final r f;

    public o(a aVar) {
        this.d = aVar;
        this.b = a.a(aVar.d, R.drawable.ic_folder);
        this.c = a.a(aVar.d, R.drawable.ic_folder_highlight);
        this.e = new q(this, (byte) 0);
        this.f = new r(this);
    }

    public final int a() {
        if (this.d.r != null) {
            return this.d.r.size();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ cf a(ViewGroup viewGroup, int i) {
        return new p(this, this.d.f.inflate(this.a, viewGroup, false));
    }

    public final /* synthetic */ void a(cf cfVar, int i) {
        p pVar = (p) cfVar;
        b bVar = (b) this.d.r.get(i);
        pVar.r = bVar;
        boolean c2 = bVar.c();
        pVar.p = c2;
        if (c2) {
            pVar.o.setImageDrawable(this.b);
        } else {
            pVar.o.setImageBitmap(bVar.i());
        }
        pVar.m.setText(bVar.a());
        pVar.l.setOnClickListener(this.e);
        if (this.d.A || i != 0 || !pVar.p) {
            pVar.q = false;
            pVar.n.setVisibility(0);
            return;
        }
        pVar.m.setText("back");
        pVar.n.setVisibility(8);
    }

    public final void b(int i, int i2) {
        Collections.swap(this.d.r, i, i2);
        a_(i, i2);
        if (i == 0) {
            this.d.t.c(0);
        }
        this.d.B = true;
    }
}
