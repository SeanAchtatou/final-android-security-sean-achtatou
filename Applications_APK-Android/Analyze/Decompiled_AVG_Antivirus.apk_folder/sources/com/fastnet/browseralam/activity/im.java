package com.fastnet.browseralam.activity;

import android.view.View;
import android.widget.TextView;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.i.aq;
import java.io.File;

final class im implements View.OnClickListener {
    final /* synthetic */ TextView a;
    final /* synthetic */ TextView b;
    final /* synthetic */ Cif c;

    im(Cif ifVar, TextView textView, TextView textView2) {
        this.c = ifVar;
        this.a = textView;
        this.b = textView2;
    }

    public final void onClick(View view) {
        File file = new File(this.c.X.b, this.a.getText().toString());
        if (this.c.s.a(file)) {
            aq.a(this.c.m, "Bookmark export success");
            this.c.Z.add(new jo(this.c, file, this.c.U));
            this.c.W.c_(this.c.Z.size() - 1);
            this.c.p.b(0);
            k.a(new in(this), 400);
            return;
        }
        aq.a(this.c.m, "Bookmark export failed");
        this.b.callOnClick();
    }
}
