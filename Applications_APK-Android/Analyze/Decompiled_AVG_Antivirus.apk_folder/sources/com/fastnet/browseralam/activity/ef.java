package com.fastnet.browseralam.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.ab;
import android.support.v4.app.s;
import java.util.ArrayList;
import java.util.List;

final class ef extends ab {
    final /* synthetic */ BrowserActivity a;
    private final List b = new ArrayList();
    private final List c = new ArrayList();

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ef(BrowserActivity browserActivity, s sVar) {
        super(sVar);
        this.a = browserActivity;
    }

    public final Fragment a(int i) {
        return (Fragment) this.b.get(i);
    }

    public final void a(Fragment fragment, String str) {
        this.b.add(fragment);
        this.c.add(str);
    }

    public final int b() {
        return this.b.size();
    }

    public final CharSequence b(int i) {
        return (CharSequence) this.c.get(i);
    }
}
