package com.fastnet.browseralam.activity;

import android.support.v7.widget.RecyclerView;
import com.fastnet.browseralam.view.XWebView;

final class az implements Runnable {
    final /* synthetic */ y a;
    private boolean b;
    private int c;
    private RecyclerView d;

    private az(y yVar) {
        this.a = yVar;
    }

    /* synthetic */ az(y yVar, byte b2) {
        this(yVar);
    }

    public final az a(XWebView xWebView) {
        this.c = xWebView.h();
        if (xWebView.D()) {
            this.d = this.a.g;
        } else {
            this.d = this.a.f;
        }
        this.b = xWebView.D();
        return this;
    }

    public final void run() {
        if (this.d.h()) {
            this.a.aq.postDelayed(this, 150);
        } else if (this.b) {
            this.a.q.b(this.c);
        } else {
            this.a.p.b(this.c);
        }
    }
}
