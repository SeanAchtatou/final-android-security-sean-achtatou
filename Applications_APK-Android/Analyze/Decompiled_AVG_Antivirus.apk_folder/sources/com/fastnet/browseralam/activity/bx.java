package com.fastnet.browseralam.activity;

import android.view.View;
import com.fastnet.browseralam.i.aq;

final class bx implements View.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ BrowserActivity b;

    bx(BrowserActivity browserActivity, String str) {
        this.b = browserActivity;
        this.a = str;
    }

    public final void onClick(View view) {
        this.b.L.d(this.a);
        this.b.bt.dismiss();
        aq.a(this.b.ao, "Text copied to clipboard");
    }
}
