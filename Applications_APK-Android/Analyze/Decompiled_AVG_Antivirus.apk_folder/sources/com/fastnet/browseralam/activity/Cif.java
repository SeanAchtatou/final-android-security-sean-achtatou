package com.fastnet.browseralam.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.percent.PercentRelativeLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.i;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.b.l;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.c.c;
import com.fastnet.browseralam.c.f;
import com.fastnet.browseralam.e.a;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.DrawerFrame;
import com.google.android.gms.common.internal.ImagesContract;
import com.jobstrak.drawingfun.lib.mopub.mobileads.resource.DrawableConstants;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/* renamed from: com.fastnet.browseralam.activity.if  reason: invalid class name */
public final class Cif {
    private TextView A;
    /* access modifiers changed from: private */
    public PopupWindow B;
    /* access modifiers changed from: private */
    public DrawerFrame C;
    private RelativeLayout D;
    /* access modifiers changed from: private */
    public PercentRelativeLayout E;
    private FrameLayout F;
    /* access modifiers changed from: private */
    public ImageView G;
    private ImageView H;
    private ImageView I;
    private ImageView J;
    private ImageView K;
    private ImageView L;
    /* access modifiers changed from: private */
    public List M = new ArrayList();
    private List N = new ArrayList();
    /* access modifiers changed from: private */
    public List O = new ArrayList();
    private ImageView P;
    private ImageView Q;
    private ImageView R;
    private Drawable S;
    /* access modifiers changed from: private */
    public Drawable T;
    /* access modifiers changed from: private */
    public Drawable U;
    private Drawable V;
    /* access modifiers changed from: private */
    public jj W;
    /* access modifiers changed from: private */
    public jo X;
    private List Y = new ArrayList();
    /* access modifiers changed from: private */
    public List Z = new ArrayList();
    File a = new File(Environment.getExternalStorageDirectory().toString());
    /* access modifiers changed from: private */
    public List aa = new ArrayList();
    /* access modifiers changed from: private */
    public HashMap ab = new HashMap();
    private HashMap ac = new HashMap();
    /* access modifiers changed from: private */
    public c ad;
    private LinearLayout.LayoutParams ae = new LinearLayout.LayoutParams(-2, -2, 16.0f);
    private View.OnClickListener af = new je(this);
    private View.OnClickListener ag = new jg(this);
    private View.OnClickListener ah = new ji(this);
    private boolean ai = false;
    private AccelerateDecelerateInterpolator aj = new AccelerateDecelerateInterpolator();
    private DecelerateInterpolator ak = new DecelerateInterpolator(3.0f);
    private RelativeLayout.LayoutParams al = new RelativeLayout.LayoutParams(-1, aq.a(46));
    private int am = aq.a(46);
    /* access modifiers changed from: private */
    public RelativeLayout an;
    private FrameLayout.LayoutParams ao = new FrameLayout.LayoutParams(-1, aq.a(46), 80);
    private FrameLayout.LayoutParams ap = new FrameLayout.LayoutParams(-2, -2, 80);
    private int aq = aq.a(46);
    /* access modifiers changed from: private */
    public a ar = new iu(this);
    private Runnable as = new ja(this);
    /* access modifiers changed from: private */
    public Runnable at = new jb(this);
    File b = new File(Environment.getExternalStorageDirectory().toString());
    jo c = new jo(this, new File(Environment.getExternalStorageDirectory().toString()), null);
    int d;
    int e;
    int f;
    int g = aq.a(56);
    int h = aq.a(30);
    int i = aq.a(8);
    int j = aq.a(2);
    int k = aq.a(112);
    DecimalFormat l = new DecimalFormat("000");
    /* access modifiers changed from: private */
    public final Activity m;
    /* access modifiers changed from: private */
    public final Context n;
    /* access modifiers changed from: private */
    public final LayoutInflater o;
    /* access modifiers changed from: private */
    public final i p;
    /* access modifiers changed from: private */
    public final RecyclerView q;
    /* access modifiers changed from: private */
    public final RecyclerView r;
    /* access modifiers changed from: private */
    public gr s;
    /* access modifiers changed from: private */
    public LinearLayoutManager t;
    private LinearLayout u;
    private LinearLayout v;
    /* access modifiers changed from: private */
    public RelativeLayout w;
    /* access modifiers changed from: private */
    public RelativeLayout x;
    /* access modifiers changed from: private */
    public HorizontalScrollView y;
    /* access modifiers changed from: private */
    public TextView z;

    public Cif(i iVar, BrowserActivity browserActivity, FrameLayout frameLayout) {
        this.m = browserActivity;
        this.n = browserActivity;
        this.o = this.m.getLayoutInflater();
        this.p = iVar;
        this.ad = c.a(this.m);
        this.F = frameLayout;
        this.D = (RelativeLayout) frameLayout.findViewById(R.id.bottom_toolbar);
        this.P = (ImageView) this.D.findViewById(R.id.back_button);
        this.Q = (ImageView) this.D.findViewById(R.id.forward_button);
        this.R = (ImageView) this.D.findViewById(R.id.new_tab_button);
        this.C = (DrawerFrame) frameLayout.findViewById(R.id.bottomDrawerFrame);
        this.x = (RelativeLayout) frameLayout.findViewById(R.id.bottom_drawer);
        this.v = (LinearLayout) this.x.findViewById(R.id.bottom_action_bar);
        this.r = (RecyclerView) frameLayout.findViewById(R.id.bookmark_list);
        this.q = (RecyclerView) this.x.findViewById(R.id.file_list);
        this.q.a(true);
        this.t = new LinearLayoutManager();
        this.q.a(this.t);
        this.w = (RelativeLayout) this.x.findViewById(R.id.file_toolbar);
        this.z = (TextView) this.w.findViewById(R.id.title);
        this.G = (ImageView) this.w.findViewById(R.id.file_menu);
        this.y = (HorizontalScrollView) this.w.findViewById(R.id.tree_scroll_view);
        this.u = (LinearLayout) this.w.findViewById(R.id.tree_list);
        this.S = android.support.v4.content.a.a(this.n, R.drawable.ic_arrow_right);
        this.T = android.support.v4.content.a.a(this.n, R.drawable.folder);
        this.U = android.support.v4.content.a.a(this.n, R.drawable.ic_txt_file);
        this.V = android.support.v4.content.a.a(this.n, R.drawable.ic_file);
        this.W = new jj(this);
        this.q.a(this.W);
        TextView textView = (TextView) this.u.findViewById(R.id.rootStorage);
        textView.setOnClickListener(this.af);
        String unused = this.c.c = "Storage";
        this.M.add(textView);
        this.N.add(this.c.b);
        this.O.add(this.c);
        this.A = textView;
        this.X = this.c;
        TypedValue typedValue = new TypedValue();
        this.n.getTheme().resolveAttribute(16843534, typedValue, true);
        this.f = typedValue.resourceId;
        this.E = (PercentRelativeLayout) this.o.inflate((int) R.layout.file_edit_toolbar, (ViewGroup) null);
        this.H = (ImageView) this.E.findViewById(R.id.done);
        this.I = (ImageView) this.E.findViewById(R.id.edit);
        this.J = (ImageView) this.E.findViewById(R.id.copy);
        this.K = (ImageView) this.E.findViewById(R.id.cut);
        this.L = (ImageView) this.E.findViewById(R.id.delete);
        this.H.setOnClickListener(this.ah);
        this.I.setOnClickListener(this.ah);
        this.L.setOnClickListener(this.ah);
        this.al.bottomMargin = aq.a(52);
        this.al.addRule(12);
        this.G.setOnClickListener(new jh(this));
        this.B = new PopupWindow(this.o.inflate((int) R.layout.file_toolbar_menu, (ViewGroup) null), aq.a(200), -2);
        this.B.setFocusable(true);
        this.B.setOutsideTouchable(true);
        this.B.setBackgroundDrawable(new ColorDrawable(0));
        h();
        l.a(new ig(this));
    }

    /* access modifiers changed from: private */
    public long a(File file, f fVar) {
        File[] listFiles = file.listFiles();
        if (listFiles == null || listFiles.length == 0) {
            return 0;
        }
        long j2 = 0;
        int i2 = 1;
        for (File file2 : listFiles) {
            if (file2.isFile()) {
                j2 += file2.length();
                fVar.h();
            } else {
                f fVar2 = new f(file, this.l.format((long) i2));
                j2 += a(file2, fVar2);
                fVar.a(fVar2.b());
                i2++;
            }
        }
        fVar.a(file.getName());
        fVar.b(a(j2));
        fVar.a(file.lastModified());
        this.aa.add(fVar);
        return j2;
    }

    static /* synthetic */ Drawable a(Cif ifVar, String str) {
        String str2 = "";
        int length = str.length() - 1;
        int i2 = length - 6;
        while (length > i2) {
            char charAt = str.charAt(length);
            if (charAt == '.') {
                Drawable drawable = (Drawable) ifVar.ac.get(str2);
                return drawable != null ? drawable : ifVar.V;
            }
            str2 = charAt + str2;
            length--;
        }
        return ifVar.V;
    }

    public static String a(long j2) {
        if (j2 < 1024) {
            return j2 + " B";
        }
        int numberOfLeadingZeros = (63 - Long.numberOfLeadingZeros(j2)) / 10;
        return String.format("%.2f %sB", Double.valueOf(((double) j2) / ((double) (1 << (numberOfLeadingZeros * 10)))), Character.valueOf(" KMGTPE".charAt(numberOfLeadingZeros)));
    }

    static /* synthetic */ void a(Cif ifVar, TextView textView) {
        String charSequence = textView.getText().toString();
        AlertDialog.Builder builder = new AlertDialog.Builder(ifVar.m);
        LinearLayout linearLayout = new LinearLayout(ifVar.m);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(1);
        linearLayout.setPadding(60, 10, 60, 0);
        builder.setTitle("Rename File");
        EditText editText = new EditText(ifVar.m);
        editText.setHint("enter name");
        editText.setText(textView.getText().toString());
        editText.setSingleLine();
        linearLayout.addView(editText);
        builder.setView(linearLayout);
        builder.setPositiveButton("OK", new iv(ifVar, textView, editText));
        builder.setNegativeButton("Cancel", new iw(ifVar));
        builder.show();
        if (charSequence.endsWith(".txt")) {
            editText.setSelection(0, charSequence.length() - 4);
        } else {
            editText.selectAll();
        }
        k.a(new ix(ifVar, editText));
    }

    static /* synthetic */ void a(Cif ifVar, File file) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ifVar.m);
        View inflate = ifVar.m.getLayoutInflater().inflate((int) R.layout.bookmark_import_confirm, (ViewGroup) null);
        builder.setView(inflate);
        AlertDialog create = builder.create();
        inflate.findViewById(R.id.yes).setOnClickListener(new iq(ifVar, file, create));
        inflate.findViewById(R.id.no).setOnClickListener(new ir(ifVar, file, create));
        inflate.findViewById(R.id.cancel).setOnClickListener(new it(ifVar, create));
        create.show();
    }

    static /* synthetic */ void a(Cif ifVar, File file, boolean z2) {
        ij ijVar = new ij(ifVar, file);
        if (z2) {
            new Thread(ijVar).start();
        } else {
            l.a(ijVar);
        }
    }

    static /* synthetic */ void a(Cif ifVar, String str, String str2, jo joVar) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ifVar.m);
        LinearLayout linearLayout = new LinearLayout(ifVar.m);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(1);
        linearLayout.setPadding(60, 10, 60, 0);
        builder.setTitle(str);
        EditText editText = new EditText(ifVar.m);
        editText.setHint("enter name");
        editText.setText(str2);
        editText.setSingleLine();
        linearLayout.addView(editText);
        builder.setView(linearLayout);
        builder.setPositiveButton("OK", new iy(ifVar, editText, str, joVar));
        builder.setNegativeButton("Cancel", new iz(ifVar));
        builder.show();
        editText.selectAll();
    }

    private void a(jo joVar) {
        int indexOf = this.N.indexOf(joVar.b);
        if (indexOf != -1) {
            this.A.setTextColor(-7829368);
            this.A = (TextView) this.M.get(indexOf);
            this.A.setTextColor(-16777216);
            this.y.smoothScrollTo(((View) this.M.get(indexOf)).getLeft() - this.h, 0);
            return;
        }
        int indexOf2 = this.N.indexOf(new File(joVar.b).getParent()) + 1;
        if (indexOf2 < this.N.size() - 1) {
            this.N.subList(indexOf2, this.N.size()).clear();
            this.O.subList(indexOf2, this.O.size()).clear();
            this.u.removeAllViews();
            this.M.subList(indexOf2, this.M.size()).clear();
            for (View addView : this.M) {
                this.u.addView(addView);
            }
        }
        TextView textView = new TextView(this.n);
        textView.setLayoutParams(this.ae);
        textView.setPadding(0, this.i, 0, this.i);
        textView.setText(joVar.c);
        textView.setClickable(true);
        textView.setOnClickListener(this.ag);
        textView.setBackgroundResource(this.f);
        ImageView imageView = new ImageView(this.n);
        imageView.setLayoutParams(this.ae);
        imageView.setImageDrawable(this.S);
        this.A.setTextColor(-7829368);
        this.A = textView;
        this.A.setTextColor(-16777216);
        this.u.addView(imageView);
        this.u.addView(textView);
        this.M.add(imageView);
        this.M.add(textView);
        this.N.add("");
        this.N.add(joVar.b);
        this.O.add(null);
        this.O.add(joVar);
        k.a(new jf(this), 50);
    }

    /* access modifiers changed from: private */
    public void a(jo joVar, boolean z2) {
        int i2;
        int i3 = 0;
        File file = new File(joVar.b);
        this.Z = new ArrayList();
        int unused = this.X.i = this.t.j();
        if (!file.getPath().equals(this.b.getPath())) {
            if (!file.getParent().equals(this.b.getPath())) {
                this.a = file.getParentFile();
            } else {
                this.a = this.b;
            }
            int indexOf = this.N.indexOf(joVar.b);
            if (indexOf != -1) {
                this.X = (jo) this.O.get(indexOf);
            } else {
                this.X = joVar;
            }
        } else {
            this.X = this.c;
        }
        b();
        ArrayList arrayList = new ArrayList();
        if (file.exists() && file.isDirectory()) {
            a(joVar);
            File[] listFiles = file.listFiles();
            if (listFiles == null || listFiles.length == 0) {
                this.W.c();
                return;
            }
            Arrays.sort(listFiles, new jp(this, (byte) 0));
            int length = listFiles.length;
            int i4 = 0;
            while (i3 < length) {
                File file2 = listFiles[i3];
                String name = file2.getName();
                if (!file2.isDirectory()) {
                    if (!z2) {
                        this.Z.add(new jo(this, file2, null));
                    } else if (file2.getPath().endsWith(".txt")) {
                        this.Z.add(new jo(this, file2, null));
                    }
                    i2 = i4 + 1;
                } else if (name.startsWith(".")) {
                    i2 = i4 + 1;
                    arrayList.add(Integer.valueOf(i4));
                } else {
                    this.Z.add(new jo(this, file2, this.T));
                    i2 = i4 + 1;
                }
                i3++;
                i4 = i2;
            }
            l.a(new ii(this, file, arrayList));
        }
        this.W.d();
    }

    private void h() {
        Drawable a2 = android.support.v4.content.a.a(this.m, R.drawable.txt);
        String[] strArr = {"txt", "doc", "docx"};
        for (int i2 = 0; i2 < 3; i2++) {
            this.ac.put(strArr[i2], a2);
        }
        Drawable a3 = android.support.v4.content.a.a(this.m, R.drawable.mp4);
        String[] strArr2 = {"mp4", "avi", "mkv", "3gp"};
        for (int i3 = 0; i3 < 4; i3++) {
            this.ac.put(strArr2[i3], a3);
        }
        Drawable a4 = android.support.v4.content.a.a(this.m, R.drawable.mp3);
        String[] strArr3 = {"mp3", "m4a", "aac"};
        for (int i4 = 0; i4 < 3; i4++) {
            this.ac.put(strArr3[i4], a4);
        }
        Drawable a5 = android.support.v4.content.a.a(this.m, R.drawable.img);
        String[] strArr4 = {"png", "jpg", "jpeg", "gif"};
        for (int i5 = 0; i5 < 4; i5++) {
            this.ac.put(strArr4[i5], a5);
        }
        Drawable a6 = android.support.v4.content.a.a(this.m, R.drawable.zip);
        String[] strArr5 = {"zip", "rar", "7z", "gzip", "bz2", ".iso", ".tar"};
        for (int i6 = 0; i6 < 7; i6++) {
            this.ac.put(strArr5[i6], a6);
        }
        this.ac.put("pdf", android.support.v4.content.a.a(this.m, R.drawable.pdf));
        this.ac.put("apk", android.support.v4.content.a.a(this.m, R.drawable.apk));
    }

    static /* synthetic */ void t(Cif ifVar) {
        if (!ifVar.ai) {
            ifVar.ai = true;
            ifVar.x.addView(ifVar.E, ifVar.al);
            ifVar.E.setTranslationY((float) ifVar.am);
            ifVar.E.setVisibility(0);
            ifVar.I.setTranslationY((float) ifVar.g);
            ifVar.H.setTranslationY((float) ifVar.g);
            ifVar.J.setTranslationY((float) ifVar.g);
            ifVar.K.setTranslationY((float) ifVar.g);
            ifVar.L.setTranslationY((float) ifVar.g);
            ifVar.E.animate().translationY(0.0f).setDuration(300).setInterpolator(ifVar.ak).setListener(null);
            ifVar.H.animate().setStartDelay(50).setDuration(300).translationY(0.0f).setInterpolator(ifVar.ak);
            ifVar.I.animate().setStartDelay(55).setDuration(300).translationY(0.0f).setInterpolator(ifVar.ak);
            ifVar.J.animate().setStartDelay(60).setDuration(300).translationY(0.0f).setInterpolator(ifVar.ak);
            ifVar.K.animate().setStartDelay(65).setDuration(300).translationY(0.0f).setInterpolator(ifVar.ak);
            ifVar.L.animate().setStartDelay(70).setDuration(300).translationY(0.0f).setInterpolator(ifVar.ak);
        }
    }

    public final void a() {
        this.R.setOnLongClickListener(null);
        this.P.setOnLongClickListener(null);
        this.P.setOnClickListener(new is(this));
        this.Q.setOnClickListener(new jc(this));
        this.R.setOnClickListener(new jd(this));
    }

    public final void a(gr grVar) {
        this.s = grVar;
    }

    public final void b() {
        if (this.ai) {
            this.W.e();
            this.ai = false;
            if (this.C.getVisibility() == 8) {
                this.E.setVisibility(8);
                this.x.removeView(this.E);
                return;
            }
            this.E.animate().translationY((float) this.g).setDuration(300).setInterpolator(new DecelerateInterpolator(2.0f)).setListener(new ih(this)).start();
        }
    }

    public final void c() {
        b();
        this.q.setVisibility(8);
        this.q.setAlpha(0.0f);
        this.w.setVisibility(8);
        this.w.setAlpha(0.0f);
        this.an.setVisibility(8);
        this.v.setVisibility(0);
    }

    public final void d() {
        if (this.an == null) {
            this.an = (RelativeLayout) this.m.getLayoutInflater().inflate((int) R.layout.export_bar, (ViewGroup) null);
        }
        TextView textView = (TextView) this.an.findViewById(R.id.export_name);
        textView.setOnClickListener(new ik(this, textView));
        TextView textView2 = (TextView) this.an.findViewById(R.id.cancel_export);
        textView2.setOnClickListener(new il(this));
        this.an.findViewById(R.id.export_here).setOnClickListener(new im(this, textView, textView2));
        this.z.setText("Kirim penanda");
        this.w.setVisibility(0);
        this.w.animate().alpha(1.0f).setListener(null);
        this.q.setVisibility(0);
        this.q.animate().alpha(1.0f).setListener(null);
        this.C.a(this.q);
        this.x.addView(this.an, this.ao);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.an.getLayoutParams();
        layoutParams.addRule(12);
        layoutParams.bottomMargin = aq.a(52);
        this.an.setLayoutParams(this.v.getLayoutParams());
        this.v.setVisibility(8);
        this.p.b(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.jo, boolean):void
     arg types: [com.fastnet.browseralam.activity.jo, int]
     candidates:
      com.fastnet.browseralam.activity.if.a(java.io.File, com.fastnet.browseralam.c.f):long
      com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.if, java.lang.String):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.if, com.fastnet.browseralam.c.c):com.fastnet.browseralam.c.c
      com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.if, java.util.HashMap):java.util.HashMap
      com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.if, android.widget.TextView):void
      com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.if, com.fastnet.browseralam.activity.jo):void
      com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.if, java.io.File):void
      com.fastnet.browseralam.activity.if.a(com.fastnet.browseralam.activity.jo, boolean):void */
    public final void e() {
        this.z.setText("Import Bookmarks");
        RelativeLayout relativeLayout = (RelativeLayout) this.o.inflate((int) R.layout.cancel_textview, (ViewGroup) null);
        this.F.addView(relativeLayout, this.ap);
        relativeLayout.findViewById(R.id.cancel).setOnClickListener(new io(this));
        this.w.setVisibility(0);
        this.q.setVisibility(0);
        this.v.setVisibility(8);
        this.q.setPadding(0, 0, 0, 0);
        this.w.animate().alpha(1.0f).setListener(null);
        this.q.animate().alpha(1.0f).setListener(null);
        this.C.a(this.q);
        this.p.b(2);
        this.W.g(DrawableConstants.CtaButton.WIDTH_DIPS);
        a(this.c, true);
        this.W.a(new ip(this));
    }

    public final void f() {
        b();
        this.W.g(196);
        this.W.a((View.OnClickListener) null);
        this.F.removeView(this.F.findViewById(R.id.cancel_layout));
        this.v.setVisibility(0);
        this.w.animate().alpha(0.0f).setDuration(100);
        this.q.animate().alpha(0.0f).setDuration(100).setListener(this.ar);
        this.p.b(0);
        this.q.setPadding(0, 0, 0, aq.a(46));
    }

    public final void g() {
        com.fastnet.browseralam.c.a aVar = new com.fastnet.browseralam.c.a(this.s.e(), "chrome");
        String[] strArr = {"title", ImagesContract.URL};
        Cursor query = this.m.getContentResolver().query(Uri.parse("content://com.android.chrome.browser/bookmarks"), strArr, "bookmark = 1", null, null);
        if (query == null) {
            aq.a(this.n, "No bookmarks found");
            return;
        }
        if (query.moveToFirst()) {
            do {
                aVar.a(new b(query.getString(0), query.getString(1), null, null));
            } while (query.moveToNext());
        }
        query.close();
        if (aVar.f().size() > 1) {
            this.s.e().a(aVar);
            aq.a(this.n, "Chrome bookmarks import success");
            return;
        }
        aq.a(this.n, "No bookmarks found");
    }
}
