package com.fastnet.browseralam.activity;

import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.view.View;
import android.view.ViewGroup;
import com.fastnet.browseralam.R;

public final class fh extends bi {
    final int a = R.layout.file_row;
    final /* synthetic */ ey b;
    private View.OnClickListener c = new fi(this);
    private View.OnLongClickListener d = new fj(this);

    public fh(ey eyVar) {
        this.b = eyVar;
    }

    public final int a() {
        if (this.b.l != null) {
            return this.b.l.size();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ cf a(ViewGroup viewGroup, int i) {
        return new fk(this, this.b.g.inflate((int) R.layout.file_row, viewGroup, false));
    }

    public final /* synthetic */ void a(cf cfVar, int i) {
        fk fkVar = (fk) cfVar;
        fl flVar = (fl) this.b.l.get(i);
        fkVar.n.setText(flVar.c);
        fkVar.m.setImageDrawable(flVar.e);
        fkVar.l.setOnClickListener(this.c);
        fkVar.l.setOnLongClickListener(this.d);
        fkVar.l.setTag(flVar);
    }
}
