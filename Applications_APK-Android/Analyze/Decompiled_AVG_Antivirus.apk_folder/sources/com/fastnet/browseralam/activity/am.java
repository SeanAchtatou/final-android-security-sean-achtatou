package com.fastnet.browseralam.activity;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.fastnet.browseralam.a.b;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.view.XWebView;
import com.fastnet.browseralam.view.c;
import com.fastnet.browseralam.view.g;

final class am implements View.OnAttachStateChangeListener {
    final /* synthetic */ y a;
    private g b = new ao(this);
    private final ViewGroup.LayoutParams c = new ViewGroup.LayoutParams(-1, -1);

    am(y yVar) {
        this.a = yVar;
    }

    public final void onViewAttachedToWindow(View view) {
        if (this.a.m.size() > 0) {
            for (int size = this.a.m.size() - 2; size >= 0; size--) {
                ((XWebView) this.a.m.get(size)).e();
                this.a.w.addView(((XWebView) this.a.m.get(size)).w(), this.c);
            }
            WebView webView = new WebView(this.a.b);
            webView.setAnimationCacheEnabled(false);
            webView.setDrawingCacheEnabled(false);
            webView.setWillNotCacheDrawing(true);
            webView.loadUrl(b.a());
            webView.setWebChromeClient(new c(webView, this.b));
            this.a.w.addView(webView, this.c);
            this.a.u.removeOnAttachStateChangeListener(this);
            k.a(new an(this));
        }
    }

    public final void onViewDetachedFromWindow(View view) {
    }
}
