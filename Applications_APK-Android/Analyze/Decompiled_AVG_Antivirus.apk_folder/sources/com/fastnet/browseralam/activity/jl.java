package com.fastnet.browseralam.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.webkit.MimeTypeMap;
import java.io.File;

final class jl implements View.OnClickListener {
    final /* synthetic */ jj a;

    jl(jj jjVar) {
        this.a = jjVar;
    }

    public final void onClick(View view) {
        jo joVar = ((jn) view.getTag()).r;
        if (this.a.a != null) {
            this.a.e();
            this.a.h.b();
        } else if (joVar.h) {
            this.a.h.a(joVar, false);
        } else {
            File file = new File(joVar.b);
            MimeTypeMap singleton = MimeTypeMap.getSingleton();
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(file), singleton.getMimeTypeFromExtension(file.getPath().substring(1)));
            intent.setFlags(268435456);
            intent.addFlags(1);
            this.a.h.m.startActivity(intent);
        }
    }
}
