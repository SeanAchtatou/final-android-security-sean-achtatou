package com.fastnet.browseralam.activity;

import android.view.GestureDetector;
import android.view.MotionEvent;
import com.fastnet.browseralam.e.p;

final class v extends GestureDetector.SimpleOnGestureListener {
    final /* synthetic */ a a;
    final /* synthetic */ u b;

    v(u uVar, a aVar) {
        this.b = uVar;
        this.a = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.u.a(com.fastnet.browseralam.activity.u, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.u, int]
     candidates:
      com.fastnet.browseralam.activity.u.a(com.fastnet.browseralam.activity.u, float):float
      com.fastnet.browseralam.activity.u.a(com.fastnet.browseralam.activity.u, android.view.View):android.view.View
      com.fastnet.browseralam.activity.u.a(com.fastnet.browseralam.activity.u, boolean):boolean */
    public final void onLongPress(MotionEvent motionEvent) {
        super.onLongPress(motionEvent);
        if (this.b.f = this.b.a.g.a(motionEvent.getX(), motionEvent.getY()) != null && !this.b.a.E = (p) this.b.f.getTag().u()) {
            this.b.a.h.requestDisallowInterceptTouchEvent(true);
            boolean unused = this.b.g = false;
            boolean unused2 = this.b.d = true;
            float unused3 = this.b.o = motionEvent.getY();
            u.b(this.b, this.b.o);
            this.b.c.postDelayed(this.b.x, 400);
        }
    }
}
