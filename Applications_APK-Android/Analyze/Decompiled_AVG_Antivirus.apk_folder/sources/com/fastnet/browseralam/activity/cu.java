package com.fastnet.browseralam.activity;

import android.view.View;
import android.widget.CheckedTextView;

final class cu implements View.OnClickListener {
    final /* synthetic */ CheckedTextView a;
    final /* synthetic */ BrowserActivity b;

    cu(BrowserActivity browserActivity, CheckedTextView checkedTextView) {
        this.b = browserActivity;
        this.a = checkedTextView;
    }

    public final void onClick(View view) {
        this.a.setChecked(this.b.L.G());
        this.b.F.showAsDropDown(this.b.af, this.b.cj, this.b.ci);
    }
}
