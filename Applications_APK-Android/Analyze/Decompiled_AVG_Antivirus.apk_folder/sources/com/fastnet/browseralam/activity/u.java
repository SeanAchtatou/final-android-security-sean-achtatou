package com.fastnet.browseralam.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Handler;
import android.support.v4.view.q;
import android.support.v7.widget.bt;
import android.view.MotionEvent;
import android.view.View;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.e.p;
import com.fastnet.browseralam.i.aq;

final class u implements bt {
    final /* synthetic */ a a;
    private final q b;
    /* access modifiers changed from: private */
    public final Handler c = k.a();
    /* access modifiers changed from: private */
    public boolean d = false;
    private boolean e = false;
    /* access modifiers changed from: private */
    public View f;
    /* access modifiers changed from: private */
    public boolean g = false;
    private final int h = aq.a(56);
    private final int i = aq.a(80);
    private final int j = aq.a(50);
    private final int k = aq.a(40);
    private final int l = aq.a(500);
    private int m;
    private int n;
    /* access modifiers changed from: private */
    public float o;
    private float p;
    private float q = ((float) (-this.h));
    private float r = ((float) this.h);
    private int s;
    private p t;
    private boolean u;
    private boolean v;
    private x w = new x(this, (byte) 0);
    /* access modifiers changed from: private */
    public final Runnable x = new w(this);

    public u(a aVar) {
        this.a = aVar;
        this.b = new q(aVar.g.getContext(), new v(this, aVar));
    }

    private void a(int i2) {
        this.t = (p) this.a.g.c(i2);
        if (this.t != null) {
            this.t.w();
        }
    }

    private void a(int i2, float f2, float f3) {
        if (i2 == 0) {
            this.q = f2 - ((float) this.l);
        } else if (b(i2 - 1)) {
            this.q = f2 - ((float) this.k);
        } else {
            this.q = f2 - ((float) this.j);
        }
        if (i2 == this.n) {
            this.r = ((float) this.l) + f3;
        } else if (b(i2 + 1)) {
            this.r = ((float) this.k) + f3;
        } else {
            this.r = ((float) this.j) + f3;
        }
    }

    static /* synthetic */ void b(u uVar, float f2) {
        float top = (float) uVar.f.getTop();
        uVar.p = f2 - top;
        uVar.a.k.setTranslationY(f2 - uVar.p);
        View view = uVar.f;
        Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), uVar.h, Bitmap.Config.ARGB_8888);
        view.draw(new Canvas(createBitmap));
        uVar.a.k.setImageBitmap(createBitmap);
        uVar.a.k.setTop(0);
        uVar.a.k.setVisibility(0);
        uVar.f.setVisibility(4);
        uVar.n = uVar.a.r.size() - 1;
        int t2 = uVar.a.E.t();
        uVar.m = t2;
        uVar.s = t2;
        uVar.a(uVar.m, top, (float) uVar.f.getBottom());
    }

    private boolean b(int i2) {
        return ((b) this.a.r.get(i2)).c();
    }

    public final void a() {
        this.d = false;
        if (this.t != null) {
            this.t.x();
        }
        if ((this.v || this.u) && this.m != this.s) {
            this.u = false;
            this.v = false;
            this.f.setAlpha(0.0f);
            this.f.setVisibility(0);
            this.a.k.setVisibility(8);
            this.a.k.setImageBitmap(null);
            o i2 = this.a.s;
            int i3 = this.m;
            int i4 = this.s;
            i2.d.b((b) i2.d.r.get(i3));
            ((b) i2.d.r.get(i4)).a((b) i2.d.r.get(i3));
            i2.d.r.remove(i3);
            i2.d_(i3);
            i2.d.B = true;
            return;
        }
        this.u = false;
        this.v = false;
        this.a.k.animate().translationY((float) this.f.getTop()).setDuration(100).setListener(this.w).start();
    }

    public final void a(boolean z) {
    }

    public final boolean a(MotionEvent motionEvent) {
        if (this.d) {
            return true;
        }
        this.b.a(motionEvent);
        return false;
    }

    public final void b(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            a();
            return;
        }
        if (!this.g && Math.abs(this.o - motionEvent.getY()) > 20.0f) {
            this.g = true;
        }
        this.a.k.setTranslationY(motionEvent.getY() - this.p);
        if (this.a.k.getTranslationY() < this.q) {
            if (this.v) {
                this.u = false;
                this.v = false;
                this.t.x();
                a(this.s, (float) (this.f.getTop() - this.h), (float) this.f.getTop());
                this.a.s.b(this.m, this.s);
                this.m = this.s;
            } else if (this.u) {
                this.t.x();
                this.v = false;
                this.u = false;
                this.s = this.m;
                a(this.m, (float) this.f.getTop(), (float) this.f.getBottom());
            } else if (this.s != 0) {
                this.s--;
                if (b(this.s)) {
                    this.v = true;
                    a(this.s);
                    if (this.s != 0 || this.a.A) {
                        this.q = (float) (this.f.getTop() - this.i);
                    } else {
                        this.q = (float) (-this.l);
                    }
                    this.r = (float) (this.f.getBottom() - this.k);
                    return;
                }
                a(this.s, (float) (this.f.getTop() - this.h), (float) this.f.getTop());
                this.a.s.b(this.m, this.s);
                this.m = this.s;
            }
        } else if (this.a.k.getTranslationY() + ((float) this.h) > this.r) {
            if (this.u) {
                this.v = false;
                this.u = false;
                this.t.x();
            } else if (this.v) {
                this.t.x();
                this.u = false;
                this.v = false;
                this.s = this.m;
                a(this.m, (float) this.f.getTop(), (float) this.f.getBottom());
                return;
            } else if (this.s != this.n) {
                this.s++;
                if (b(this.s)) {
                    this.u = true;
                    a(this.s);
                    this.q = (float) (this.f.getTop() + this.k);
                    this.r = (float) (this.f.getBottom() + this.i);
                    return;
                }
            } else {
                return;
            }
            a(this.s, (float) this.f.getBottom(), (float) (this.f.getBottom() + this.h));
            this.a.s.b(this.m, this.s);
            this.m = this.s;
        }
    }
}
