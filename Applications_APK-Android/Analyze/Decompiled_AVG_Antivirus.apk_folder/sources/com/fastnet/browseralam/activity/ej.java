package com.fastnet.browseralam.activity;

import android.animation.Animator;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.e.a;

final class ej extends a {
    boolean a;
    final /* synthetic */ ei b;
    private Runnable c = new ek(this);

    ej(ei eiVar) {
        this.b = eiVar;
    }

    public final void onAnimationCancel(Animator animator) {
        if (!this.a && this.b.O) {
            this.a = true;
            this.b.M.setScaleX(2.0f);
            this.b.M.setScaleY(2.0f);
            this.b.e.removeView(this.b.k);
            this.b.e.removeView(this.b.l);
            this.b.c.removeView(this.b.k);
            this.b.c.removeView(this.b.l);
            this.b.c.addView(this.b.k, 0);
            this.b.c.addView(this.b.l, 0);
            this.b.j.setVisibility(8);
            this.b.e.setVisibility(8);
            this.b.m.a(false);
            this.b.n.a(false);
            if (this.b.t.h()) {
                k.a(this.c);
            } else {
                this.b.s.e();
            }
            boolean unused = this.b.N = this.b.O = false;
            this.a = false;
        }
    }

    public final void onAnimationEnd(Animator animator) {
        if (this.b.O) {
            this.b.e.removeView(this.b.k);
            this.b.e.removeView(this.b.l);
            this.b.c.addView(this.b.k, 0);
            this.b.c.addView(this.b.l, 0);
            this.b.j.setVisibility(8);
            this.b.e.setVisibility(8);
            this.b.m.a(false);
            this.b.n.a(false);
            k.a(this.c);
            boolean unused = this.b.N = this.b.O = false;
        }
    }
}
