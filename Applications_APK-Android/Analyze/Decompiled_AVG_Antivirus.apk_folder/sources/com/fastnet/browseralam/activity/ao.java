package com.fastnet.browseralam.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.webkit.WebView;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.g;

final class ao implements g {
    final /* synthetic */ am a;

    ao(am amVar) {
        this.a = amVar;
    }

    public final void a(WebView webView) {
        aq.a(13);
        Bitmap createBitmap = Bitmap.createBitmap(this.a.a.ac, this.a.a.ad, Bitmap.Config.ARGB_8888);
        webView.draw(new Canvas(createBitmap));
        Bitmap unused = this.a.a.S = Bitmap.createScaledBitmap(createBitmap, this.a.a.ak, this.a.a.al, false);
        createBitmap.recycle();
        this.a.a.b.runOnUiThread(new ap(this, webView));
    }
}
