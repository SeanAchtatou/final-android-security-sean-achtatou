package com.fastnet.browseralam.activity;

import android.view.View;
import android.widget.CheckedTextView;

final class cm implements View.OnClickListener {
    final /* synthetic */ int[] a;
    final /* synthetic */ BrowserActivity b;

    cm(BrowserActivity browserActivity, int[] iArr) {
        this.b = browserActivity;
        this.a = iArr;
    }

    public final void onClick(View view) {
        CheckedTextView checkedTextView = (CheckedTextView) view;
        checkedTextView.setChecked(!checkedTextView.isChecked());
        if (checkedTextView.isChecked()) {
            this.a[0] = 1;
        } else {
            this.a[0] = 0;
        }
    }
}
