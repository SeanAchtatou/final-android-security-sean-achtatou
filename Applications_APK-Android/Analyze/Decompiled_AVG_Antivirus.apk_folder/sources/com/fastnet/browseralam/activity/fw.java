package com.fastnet.browseralam.activity;

import android.view.View;

final class fw implements View.OnClickListener {
    final /* synthetic */ ReadingActivity a;

    fw(ReadingActivity readingActivity) {
        this.a = readingActivity;
    }

    public final void onClick(View view) {
        String obj = this.a.p.getText().toString();
        if (!obj.equals(this.a.q)) {
            this.a.n.findAllAsync(this.a.q = obj);
        } else {
            this.a.n.findNext(true);
        }
    }
}
