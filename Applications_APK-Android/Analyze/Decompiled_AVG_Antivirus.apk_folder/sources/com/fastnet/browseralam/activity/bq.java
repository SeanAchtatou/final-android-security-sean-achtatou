package com.fastnet.browseralam.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.view.View;

final class bq implements View.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ BrowserActivity b;

    bq(BrowserActivity browserActivity, String str) {
        this.b = browserActivity;
        this.a = str;
    }

    public final void onClick(View view) {
        ((ClipboardManager) this.b.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("label", this.a));
        this.b.bt.dismiss();
    }
}
