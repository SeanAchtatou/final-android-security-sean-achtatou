package com.fastnet.browseralam.activity;

import android.animation.Animator;

final class ae extends ay {
    final /* synthetic */ y a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ae(y yVar) {
        super(yVar, (byte) 0);
        this.a = yVar;
    }

    public final void onAnimationEnd(Animator animator) {
        this.a.c.setVisibility(8);
        this.a.c.setAlpha(1.0f);
        if (!this.a.T) {
            this.a.m();
            this.a.o.f();
            this.a.x.setTranslationY((float) this.a.aj);
        } else {
            this.a.k();
            this.a.x.setTranslationY(((float) this.a.ah) - this.a.s.d());
        }
        ay unused = this.a.aA = this.a.aB;
    }
}
