package com.fastnet.browseralam.activity;

import android.graphics.Bitmap;
import com.fastnet.browseralam.b.d;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.i.t;
import java.io.File;

final class gt implements d {
    final /* synthetic */ b a;
    final /* synthetic */ gr b;

    gt(gr grVar, b bVar) {
        this.b = grVar;
        this.a = bVar;
    }

    public final void a(Bitmap bitmap, String str) {
        this.a.a(bitmap);
        this.a.c(str);
        int indexOf = this.b.J.indexOf(this.a);
        if (indexOf != -1) {
            this.b.K.b(indexOf);
        }
        t.a(new File(this.b.Z + str), bitmap);
    }
}
