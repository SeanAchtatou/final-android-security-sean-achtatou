package com.fastnet.browseralam.activity;

import android.view.View;
import com.fastnet.browseralam.i.aq;
import java.io.File;

final class ip implements View.OnClickListener {
    final /* synthetic */ Cif a;

    ip(Cif ifVar) {
        this.a = ifVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.gr.a(java.io.File, boolean):boolean
     arg types: [java.io.File, int]
     candidates:
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.support.v7.widget.bt):android.support.v7.widget.bt
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, android.widget.PopupWindow):android.widget.PopupWindow
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.activity.hu):com.fastnet.browseralam.activity.hu
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, com.fastnet.browseralam.e.p):com.fastnet.browseralam.e.p
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.activity.gr, boolean):boolean
      com.fastnet.browseralam.activity.gr.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.b.f.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.activity.gr.a(java.io.File, boolean):boolean */
    public final void onClick(View view) {
        jo joVar = ((jn) view.getTag()).r;
        if (this.a.W.a != null) {
            this.a.W.e();
            this.a.b();
        } else if (joVar.h) {
            this.a.a(joVar, false);
        } else {
            File file = new File(joVar.b);
            if (!gr.b(file)) {
                aq.a(this.a.m, "No bookmarks found");
            } else if (this.a.s.e().f().size() > 0) {
                Cif.a(this.a, file);
            } else if (this.a.s.a(file, false)) {
                aq.a(this.a.m, "Bookmarks import success");
                this.a.f();
            } else {
                aq.a(this.a.m, "Bookmarks import failed");
            }
        }
    }
}
