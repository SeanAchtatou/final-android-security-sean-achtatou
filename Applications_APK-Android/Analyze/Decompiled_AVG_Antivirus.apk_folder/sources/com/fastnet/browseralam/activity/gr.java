package com.fastnet.browseralam.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.bt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.f;
import com.fastnet.browseralam.b.g;
import com.fastnet.browseralam.b.i;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.c.a;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.e.p;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.i.t;
import com.fastnet.browseralam.view.DrawerFrame;
import com.fastnet.browseralam.view.XWebView;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class gr implements View.OnLongClickListener, f {
    /* access modifiers changed from: private */
    public ImageView A;
    /* access modifiers changed from: private */
    public ImageView B;
    /* access modifiers changed from: private */
    public ImageView C;
    /* access modifiers changed from: private */
    public ImageView D;
    private ImageView E;
    private ImageView F;
    private ImageView G;
    private ImageView H;
    private ImageView I;
    /* access modifiers changed from: private */
    public List J = new ArrayList();
    /* access modifiers changed from: private */
    public hk K;
    /* access modifiers changed from: private */
    public LinearLayoutManager L;
    /* access modifiers changed from: private */
    public a M;
    /* access modifiers changed from: private */
    public a N;
    /* access modifiers changed from: private */
    public a O;
    private a P;
    private b Q;
    private b R;
    /* access modifiers changed from: private */
    public boolean S = true;
    /* access modifiers changed from: private */
    public boolean T;
    /* access modifiers changed from: private */
    public boolean U;
    /* access modifiers changed from: private */
    public boolean V;
    /* access modifiers changed from: private */
    public bt W;
    /* access modifiers changed from: private */
    public hu X;
    /* access modifiers changed from: private */
    public p Y;
    /* access modifiers changed from: private */
    public String Z;
    public final RecyclerView a;
    private boolean aa;
    private final Bitmap ab;
    private int ac = (-aq.a(56));
    /* access modifiers changed from: private */
    public Handler ad = k.a();
    private Runnable ae = new hb(this);
    /* access modifiers changed from: private */
    public final Cif b;
    /* access modifiers changed from: private */
    public final BrowserActivity c;
    /* access modifiers changed from: private */
    public final i d;
    /* access modifiers changed from: private */
    public final Context e;
    private final g f;
    private final com.fastnet.browseralam.h.a g;
    /* access modifiers changed from: private */
    public final LayoutInflater h;
    /* access modifiers changed from: private */
    public final FrameLayout i;
    /* access modifiers changed from: private */
    public final RelativeLayout j;
    private final RelativeLayout k;
    private final RelativeLayout l;
    /* access modifiers changed from: private */
    public final DrawerFrame m;
    private final RelativeLayout n;
    /* access modifiers changed from: private */
    public final View o;
    /* access modifiers changed from: private */
    public AlertDialog p;
    private TextView q;
    private TextView r;
    /* access modifiers changed from: private */
    public EditText s;
    /* access modifiers changed from: private */
    public EditText t;
    /* access modifiers changed from: private */
    public PopupWindow u;
    /* access modifiers changed from: private */
    public PopupWindow v;
    /* access modifiers changed from: private */
    public ImageView w;
    /* access modifiers changed from: private */
    public ImageView x;
    private ImageView y;
    private ImageView z;

    public gr(i iVar, BrowserActivity browserActivity, Cif ifVar, FrameLayout frameLayout) {
        this.c = browserActivity;
        this.e = browserActivity;
        this.f = browserActivity;
        this.d = iVar;
        this.b = ifVar;
        this.g = com.fastnet.browseralam.h.a.a();
        this.h = this.c.getLayoutInflater();
        this.i = frameLayout;
        this.m = (DrawerFrame) frameLayout.findViewById(R.id.bottomDrawerFrame);
        this.n = (RelativeLayout) this.m.findViewById(R.id.bottom_drawer);
        this.o = frameLayout.findViewById(R.id.intercept_view);
        this.i.removeView(this.o);
        this.l = (RelativeLayout) this.i.findViewById(R.id.bottom_toolbar);
        this.k = (RelativeLayout) this.n.findViewById(R.id.bookmark_toolbar);
        this.z = (ImageView) this.k.findViewById(R.id.add_folder);
        this.y = (ImageView) this.k.findViewById(R.id.toolbar_menu);
        this.w = (ImageView) this.m.findViewById(R.id.overlay);
        this.j = (RelativeLayout) this.n.findViewById(R.id.bookmark_edit);
        this.A = (ImageView) this.j.findViewById(R.id.edit);
        this.B = (ImageView) this.j.findViewById(R.id.edit_bookmark2);
        this.C = (ImageView) this.j.findViewById(R.id.delete);
        this.D = (ImageView) this.j.findViewById(R.id.delete_bookmark2);
        this.x = (ImageView) this.j.findViewById(R.id.edit_overlay);
        this.H = (ImageView) this.l.findViewById(R.id.bookmark_button);
        this.I = (ImageView) this.l.findViewById(R.id.bookmark_button2);
        this.E = (ImageView) this.l.findViewById(R.id.new_tab_button);
        this.F = (ImageView) this.l.findViewById(R.id.back_button);
        this.G = (ImageView) this.l.findViewById(R.id.forward_button);
        this.V = com.fastnet.browseralam.h.a.E();
        View inflate = this.h.inflate((int) R.layout.bookmark_toolbar_menu, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.toggle3Dot);
        if (this.V) {
            textView.setText("Show Swipe Menu");
        } else {
            textView.setText("Show 3 Dot Menu");
        }
        textView.setOnClickListener(new gx(this, textView));
        inflate.findViewById(R.id.exportBackup).setOnClickListener(new gy(this));
        inflate.findViewById(R.id.importBackup).setOnClickListener(new gz(this));
        inflate.findViewById(R.id.importChrome).setOnClickListener(new ha(this));
        this.u = new PopupWindow(inflate, -2, -2);
        this.u.setFocusable(true);
        this.u.setOutsideTouchable(true);
        this.u.setBackgroundDrawable(new ColorDrawable(0));
        this.z.setOnClickListener(new gs(this));
        hc hcVar = new hc(this);
        this.H.setOnClickListener(hcVar);
        this.I.setOnClickListener(hcVar);
        this.y.setOnClickListener(new hd(this));
        he heVar = new he(this);
        hf hfVar = new hf(this);
        this.A.setOnClickListener(heVar);
        this.B.setOnClickListener(heVar);
        this.C.setOnClickListener(hfVar);
        this.D.setOnClickListener(hfVar);
        RelativeLayout relativeLayout = (RelativeLayout) this.h.inflate((int) R.layout.bookmark_edit_dialog, (ViewGroup) null);
        this.q = (TextView) relativeLayout.findViewById(R.id.edit_title);
        this.s = (EditText) relativeLayout.findViewById(R.id.edit_name);
        this.t = (EditText) relativeLayout.findViewById(R.id.edit_url);
        this.r = (TextView) relativeLayout.findViewById(R.id.ok);
        relativeLayout.findViewById(R.id.cancel).setOnClickListener(new gu(this));
        this.p = new AlertDialog.Builder(this.c).create();
        this.p.setView(relativeLayout);
        this.p.setCanceledOnTouchOutside(true);
        this.a = (RecyclerView) this.n.findViewById(R.id.bookmark_list);
        this.a.a(true);
        this.L = new LinearLayoutManager();
        this.a.a(this.L);
        this.K = new hk(this);
        a aVar = new a(this.J, this.K);
        this.N = aVar;
        this.M = aVar;
        this.O = new a(this.M, "Incognito");
        this.a.a(this.K);
        this.ab = this.c.t();
        File filesDir = this.c.getFilesDir();
        if (filesDir != null) {
            this.Z = filesDir.getPath() + "/Bookmarks/";
            File file = new File(this.Z);
            if (!file.exists()) {
                this.aa = true;
                file.mkdir();
            }
        }
    }

    private static String a(int i2) {
        String str = "";
        for (int i3 = 0; i3 < i2; i3++) {
            str = str + " ";
        }
        return str;
    }

    private void a(List list, int i2, boolean z2, BufferedWriter bufferedWriter) {
        for (int i3 = 0; i3 < list.size(); i3++) {
            if (z2) {
                z2 = false;
            } else {
                b bVar = (b) list.get(i3);
                if (bVar.c()) {
                    bufferedWriter.write(a(i2) + "+" + bVar.a());
                    bufferedWriter.newLine();
                    a(bVar.e().f(), i2 + 1, true, bufferedWriter);
                } else {
                    bufferedWriter.write(a(i2) + "-" + bVar.a() + "::" + bVar.b() + "::" + bVar.h());
                    bufferedWriter.newLine();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002e A[SYNTHETIC, Splitter:B:15:0x002e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b(java.io.File r5) {
        /*
            r0 = 0
            r3 = 0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x002a }
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ IOException -> 0x002a }
            r1.<init>(r5)     // Catch:{ IOException -> 0x002a }
            r2.<init>(r1)     // Catch:{ IOException -> 0x002a }
            java.lang.String r1 = r2.readLine()     // Catch:{ IOException -> 0x0037 }
            if (r1 == 0) goto L_0x0024
            r3 = 0
            char r3 = r1.charAt(r3)     // Catch:{ IOException -> 0x0037 }
            r4 = 43
            if (r3 == r4) goto L_0x0025
            r3 = 0
            char r1 = r1.charAt(r3)     // Catch:{ IOException -> 0x0037 }
            r3 = 45
            if (r1 == r3) goto L_0x0025
        L_0x0024:
            return r0
        L_0x0025:
            r2.close()     // Catch:{ IOException -> 0x0037 }
            r0 = 1
            goto L_0x0024
        L_0x002a:
            r1 = move-exception
            r2 = r3
        L_0x002c:
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0031:
            r1.printStackTrace()
            goto L_0x0024
        L_0x0035:
            r2 = move-exception
            goto L_0x0031
        L_0x0037:
            r1 = move-exception
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.activity.gr.b(java.io.File):boolean");
    }

    /* access modifiers changed from: private */
    public void f() {
        this.u.showAsDropDown(this.y, 0, this.ac);
    }

    static /* synthetic */ void f(gr grVar) {
        if (grVar.Q != null && grVar.P != null) {
            grVar.P.a(grVar.Q);
            aq.a(grVar.c, "restored bookmark item");
            grVar.Q = null;
            grVar.P = null;
        } else if (grVar.R != null && grVar.P != null) {
            grVar.R.d().c(grVar.R);
            grVar.P.a(grVar.R);
            aq.a(grVar.c, "moved bookmark item");
            grVar.R = null;
            grVar.Q = null;
            grVar.P = null;
        }
    }

    public final void a() {
        this.d.e();
        this.d.c();
    }

    public final void a(b bVar) {
        if (bVar != null) {
            if (!bVar.c()) {
                this.f.D().c(bVar.b());
                this.d.d();
                return;
            }
            boolean z2 = bVar.d() == null;
            this.S = z2;
            if (z2) {
                this.N.a(false);
                this.N = this.M;
                this.N.a(true);
            } else {
                this.N.a(false);
                this.N = bVar.e();
                this.N.a(true);
            }
            this.J = this.N.f();
            this.K.c();
            t.a(this.J, this.Z, this.ab, this.K, this.ad);
        }
    }

    public final void a(XWebView xWebView, Bitmap bitmap) {
        b bVar = new b(xWebView.x(), xWebView.y(), "", bitmap);
        this.N.a(bVar);
        if (!xWebView.L()) {
            xWebView.a(new gt(this, bVar));
        } else {
            bVar.c(xWebView.M());
            t.a(new File(this.Z + xWebView.M()), bitmap);
        }
        this.T = true;
        aq.a(this.c, "Penanda ditambahkan");
    }

    public final boolean a(File file) {
        this.T = false;
        List f2 = this.M.f();
        if (file == null) {
            try {
                if (this.Z == null) {
                    this.Z = this.c.getFilesDir().getPath() + "/Bookmarks/";
                }
                File file2 = new File(this.Z);
                if (!file2.exists()) {
                    file2.mkdir();
                }
                file = new File(file2, ".bookmarks.txt");
            } catch (IOException e2) {
                e2.printStackTrace();
                return false;
            }
        }
        File file3 = new File(this.Z, ".bookmarks.old.txt");
        if (file.exists()) {
            file.renameTo(file3);
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        if (!f2.contains(this.O)) {
            f2.add(this.O);
        }
        a(f2, 0, false, bufferedWriter);
        f2.remove(this.O);
        bufferedWriter.close();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0096, code lost:
        if (r1.charAt(r4) != '+') goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0098, code lost:
        r8 = r1.substring(r4 + 1, r1.length());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a2, code lost:
        if (r4 != 0) goto L_0x00c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a4, code lost:
        r0 = new com.fastnet.browseralam.c.a(r6, r8);
        r6.b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00b0, code lost:
        if (r7.size() != 0) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b2, code lost:
        r7.add(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00bb, code lost:
        r7.set(0, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ca, code lost:
        if (r4 != r7.size()) goto L_0x00e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cc, code lost:
        r9 = new com.fastnet.browseralam.c.a((com.fastnet.browseralam.c.a) r7.get(r4 - 1), r8);
        ((com.fastnet.browseralam.c.a) r7.get(r4 - 1)).b(r9);
        r7.add(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e8, code lost:
        r9 = new com.fastnet.browseralam.c.a((com.fastnet.browseralam.c.a) r7.get(r4 - 1), r8);
        ((com.fastnet.browseralam.c.a) r7.get(r4 - 1)).b(r9);
        r7.set(r4, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0108, code lost:
        if (r1.charAt(r4) != '-') goto L_0x00b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x010a, code lost:
        r0 = r1.substring(r4 + 1, r1.length()).split("::");
        r1 = new java.lang.String[3];
        java.lang.System.arraycopy(r0, 0, r1, 0, r0.length);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0123, code lost:
        if (r4 != 0) goto L_0x0139;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0125, code lost:
        r6.a(new com.fastnet.browseralam.c.b(r1[0], r1[1], r1[2], null));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0139, code lost:
        ((com.fastnet.browseralam.c.a) r7.get(r4 - 1)).a(new com.fastnet.browseralam.c.b(r1[0], r1[1], r1[2], null));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.io.File r14, boolean r15) {
        /*
            r13 = this;
            r12 = 45
            r11 = 43
            r3 = 1
            r2 = 0
            if (r14 != 0) goto L_0x0203
            java.lang.String r0 = r13.Z
            if (r0 != 0) goto L_0x003b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            com.fastnet.browseralam.activity.BrowserActivity r1 = r13.c
            java.io.File r1 = r1.getFilesDir()
            java.lang.String r1 = r1.getPath()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/Bookmarks/"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r13.Z = r0
            java.io.File r0 = new java.io.File
            java.lang.String r1 = r13.Z
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x003b
            r0.mkdir()
        L_0x003b:
            java.io.File r0 = new java.io.File
            java.lang.String r1 = r13.Z
            java.lang.String r4 = ".bookmarks.txt"
            r0.<init>(r1, r4)
            com.fastnet.browseralam.c.a r1 = new com.fastnet.browseralam.c.a
            java.util.List r4 = r13.J
            com.fastnet.browseralam.activity.hk r5 = r13.K
            r1.<init>(r4, r5)
            r13.M = r1
        L_0x004f:
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00c0 }
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ IOException -> 0x00c0 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x00c0 }
            r5.<init>(r1)     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r0 = r5.readLine()     // Catch:{ IOException -> 0x00c0 }
            if (r0 == 0) goto L_0x006d
            r1 = 0
            char r1 = r0.charAt(r1)     // Catch:{ IOException -> 0x00c0 }
            if (r1 == r11) goto L_0x006f
            r1 = 0
            char r1 = r0.charAt(r1)     // Catch:{ IOException -> 0x00c0 }
            if (r1 == r12) goto L_0x006f
        L_0x006d:
            r0 = r2
        L_0x006e:
            return r0
        L_0x006f:
            com.fastnet.browseralam.c.a r6 = new com.fastnet.browseralam.c.a     // Catch:{ IOException -> 0x00c0 }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ IOException -> 0x00c0 }
            r1.<init>()     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.activity.hk r4 = r13.K     // Catch:{ IOException -> 0x00c0 }
            r6.<init>(r1, r4)     // Catch:{ IOException -> 0x00c0 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ IOException -> 0x00c0 }
            r7.<init>()     // Catch:{ IOException -> 0x00c0 }
            r1 = r0
        L_0x0081:
            if (r1 == 0) goto L_0x015a
            r4 = r2
        L_0x0084:
            int r0 = r1.length()     // Catch:{ IOException -> 0x00c0 }
            if (r4 >= r0) goto L_0x00b5
            char r0 = r1.charAt(r4)     // Catch:{ IOException -> 0x00c0 }
            r8 = 32
            if (r0 == r8) goto L_0x0155
            char r0 = r1.charAt(r4)     // Catch:{ IOException -> 0x00c0 }
            if (r0 != r11) goto L_0x0104
            int r0 = r4 + 1
            int r8 = r1.length()     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r8 = r1.substring(r0, r8)     // Catch:{ IOException -> 0x00c0 }
            if (r4 != 0) goto L_0x00c6
            com.fastnet.browseralam.c.a r0 = new com.fastnet.browseralam.c.a     // Catch:{ IOException -> 0x00c0 }
            r0.<init>(r6, r8)     // Catch:{ IOException -> 0x00c0 }
            r6.b(r0)     // Catch:{ IOException -> 0x00c0 }
            int r1 = r7.size()     // Catch:{ IOException -> 0x00c0 }
            if (r1 != 0) goto L_0x00bb
            r7.add(r0)     // Catch:{ IOException -> 0x00c0 }
        L_0x00b5:
            java.lang.String r0 = r5.readLine()     // Catch:{ IOException -> 0x00c0 }
            r1 = r0
            goto L_0x0081
        L_0x00bb:
            r1 = 0
            r7.set(r1, r0)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00b5
        L_0x00c0:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x006e
        L_0x00c6:
            int r0 = r7.size()     // Catch:{ IOException -> 0x00c0 }
            if (r4 != r0) goto L_0x00e8
            int r0 = r4 + -1
            java.lang.Object r0 = r7.get(r0)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = (com.fastnet.browseralam.c.a) r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r9 = new com.fastnet.browseralam.c.a     // Catch:{ IOException -> 0x00c0 }
            int r1 = r4 + -1
            java.lang.Object r1 = r7.get(r1)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r1 = (com.fastnet.browseralam.c.a) r1     // Catch:{ IOException -> 0x00c0 }
            r9.<init>(r1, r8)     // Catch:{ IOException -> 0x00c0 }
            r0.b(r9)     // Catch:{ IOException -> 0x00c0 }
            r7.add(r9)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00b5
        L_0x00e8:
            int r0 = r4 + -1
            java.lang.Object r0 = r7.get(r0)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = (com.fastnet.browseralam.c.a) r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r9 = new com.fastnet.browseralam.c.a     // Catch:{ IOException -> 0x00c0 }
            int r1 = r4 + -1
            java.lang.Object r1 = r7.get(r1)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r1 = (com.fastnet.browseralam.c.a) r1     // Catch:{ IOException -> 0x00c0 }
            r9.<init>(r1, r8)     // Catch:{ IOException -> 0x00c0 }
            r0.b(r9)     // Catch:{ IOException -> 0x00c0 }
            r7.set(r4, r9)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00b5
        L_0x0104:
            char r0 = r1.charAt(r4)     // Catch:{ IOException -> 0x00c0 }
            if (r0 != r12) goto L_0x00b5
            int r0 = r4 + 1
            int r8 = r1.length()     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r0 = r1.substring(r0, r8)     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r1 = "::"
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ IOException -> 0x00c0 }
            r1 = 3
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ IOException -> 0x00c0 }
            r8 = 0
            r9 = 0
            int r10 = r0.length     // Catch:{ IOException -> 0x00c0 }
            java.lang.System.arraycopy(r0, r8, r1, r9, r10)     // Catch:{ IOException -> 0x00c0 }
            if (r4 != 0) goto L_0x0139
            com.fastnet.browseralam.c.b r0 = new com.fastnet.browseralam.c.b     // Catch:{ IOException -> 0x00c0 }
            r4 = 0
            r4 = r1[r4]     // Catch:{ IOException -> 0x00c0 }
            r8 = 1
            r8 = r1[r8]     // Catch:{ IOException -> 0x00c0 }
            r9 = 2
            r1 = r1[r9]     // Catch:{ IOException -> 0x00c0 }
            r9 = 0
            r0.<init>(r4, r8, r1, r9)     // Catch:{ IOException -> 0x00c0 }
            r6.a(r0)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00b5
        L_0x0139:
            int r0 = r4 + -1
            java.lang.Object r0 = r7.get(r0)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = (com.fastnet.browseralam.c.a) r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.b r4 = new com.fastnet.browseralam.c.b     // Catch:{ IOException -> 0x00c0 }
            r8 = 0
            r8 = r1[r8]     // Catch:{ IOException -> 0x00c0 }
            r9 = 1
            r9 = r1[r9]     // Catch:{ IOException -> 0x00c0 }
            r10 = 2
            r1 = r1[r10]     // Catch:{ IOException -> 0x00c0 }
            r10 = 0
            r4.<init>(r8, r9, r1, r10)     // Catch:{ IOException -> 0x00c0 }
            r0.a(r4)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x00b5
        L_0x0155:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0084
        L_0x015a:
            r5.close()     // Catch:{ IOException -> 0x00c0 }
            java.util.List r0 = r6.f()     // Catch:{ IOException -> 0x00c0 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ IOException -> 0x00c0 }
        L_0x0165:
            boolean r0 = r1.hasNext()     // Catch:{ IOException -> 0x00c0 }
            if (r0 == 0) goto L_0x0186
            java.lang.Object r0 = r1.next()     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.b r0 = (com.fastnet.browseralam.c.b) r0     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r4 = r0.a()     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r5 = "Incognito"
            boolean r4 = r4.equals(r5)     // Catch:{ IOException -> 0x00c0 }
            if (r4 == 0) goto L_0x0165
            com.fastnet.browseralam.c.a r1 = r0.e()     // Catch:{ IOException -> 0x00c0 }
            r13.O = r1     // Catch:{ IOException -> 0x00c0 }
            r6.d(r0)     // Catch:{ IOException -> 0x00c0 }
        L_0x0186:
            com.fastnet.browseralam.c.a r0 = r13.O     // Catch:{ IOException -> 0x00c0 }
            if (r0 != 0) goto L_0x0195
            com.fastnet.browseralam.c.a r0 = new com.fastnet.browseralam.c.a     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r1 = r13.M     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r4 = "Incognito"
            r0.<init>(r1, r4)     // Catch:{ IOException -> 0x00c0 }
            r13.O = r0     // Catch:{ IOException -> 0x00c0 }
        L_0x0195:
            if (r14 == 0) goto L_0x01f2
            java.util.List r0 = r6.f()     // Catch:{ IOException -> 0x00c0 }
            int r0 = r0.size()     // Catch:{ IOException -> 0x00c0 }
            if (r0 != 0) goto L_0x01a4
            r0 = r2
            goto L_0x006e
        L_0x01a4:
            if (r15 == 0) goto L_0x01c0
            java.util.List r0 = r6.f()     // Catch:{ IOException -> 0x00c0 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ IOException -> 0x00c0 }
        L_0x01ae:
            boolean r0 = r1.hasNext()     // Catch:{ IOException -> 0x00c0 }
            if (r0 == 0) goto L_0x01c2
            java.lang.Object r0 = r1.next()     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.b r0 = (com.fastnet.browseralam.c.b) r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r4 = r13.M     // Catch:{ IOException -> 0x00c0 }
            r4.b(r0)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x01ae
        L_0x01c0:
            r13.M = r6     // Catch:{ IOException -> 0x00c0 }
        L_0x01c2:
            r0 = 1
            r13.T = r0     // Catch:{ IOException -> 0x00c0 }
        L_0x01c5:
            com.fastnet.browseralam.c.a r0 = r13.M     // Catch:{ IOException -> 0x00c0 }
            r1 = 1
            r0.a(r1)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = r13.N     // Catch:{ IOException -> 0x00c0 }
            r1 = 0
            r0.a(r1)     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = r13.M     // Catch:{ IOException -> 0x00c0 }
            r13.N = r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.c.a r0 = r13.M     // Catch:{ IOException -> 0x00c0 }
            java.util.List r0 = r0.f()     // Catch:{ IOException -> 0x00c0 }
            r13.J = r0     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.activity.hk r0 = r13.K     // Catch:{ IOException -> 0x00c0 }
            r0.c()     // Catch:{ IOException -> 0x00c0 }
            if (r14 == 0) goto L_0x01f5
            com.fastnet.browseralam.activity.BrowserActivity r0 = r13.c     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r1 = r13.Z     // Catch:{ IOException -> 0x00c0 }
            java.util.List r4 = r6.f()     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.i.t.a(r0, r1, r4)     // Catch:{ IOException -> 0x00c0 }
        L_0x01ef:
            r0 = r3
            goto L_0x006e
        L_0x01f2:
            r13.M = r6     // Catch:{ IOException -> 0x00c0 }
            goto L_0x01c5
        L_0x01f5:
            java.util.List r0 = r13.J     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r1 = r13.Z     // Catch:{ IOException -> 0x00c0 }
            android.graphics.Bitmap r4 = r13.ab     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.activity.hk r5 = r13.K     // Catch:{ IOException -> 0x00c0 }
            android.os.Handler r6 = r13.ad     // Catch:{ IOException -> 0x00c0 }
            com.fastnet.browseralam.i.t.a(r0, r1, r4, r5, r6)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x01ef
        L_0x0203:
            r0 = r14
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.activity.gr.a(java.io.File, boolean):boolean");
    }

    public final void b() {
        this.F.setOnClickListener(new hg(this));
        this.F.setOnLongClickListener(new hh(this));
        this.E.setOnClickListener(new hi(this));
        this.E.setOnLongClickListener(new hj(this));
    }

    public final void b(b bVar) {
        if (bVar == this.O) {
            bVar.c(this.M);
            return;
        }
        this.P = bVar.d();
        this.R = bVar;
        this.Q = null;
        this.T = true;
    }

    public final void c(b bVar) {
        if (bVar == this.O) {
            bVar.c(this.M);
            this.U = false;
            return;
        }
        this.R = null;
        this.P = bVar.d();
        this.Q = bVar;
        this.T = true;
    }

    public final boolean c() {
        return this.T;
    }

    public final void d() {
        this.N.a(new a(this.N, "folder"));
    }

    public final void d(b bVar) {
        this.s.setText(bVar.a());
        if (!bVar.c()) {
            this.t.setHint(this.c.getResources().getString(R.string.hint_url));
            this.t.setText(bVar.b());
            this.t.setVisibility(0);
            this.q.setText("Edit Bookmark");
        } else {
            this.t.setVisibility(8);
            this.q.setText("Edit Folder");
        }
        this.r.setOnClickListener(new gv(this, bVar));
        this.p.show();
        this.s.selectAll();
        k.a(new gw(this));
    }

    public final a e() {
        return this.M;
    }

    public final boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.action_previous_folder:
                if (this.Q == null || this.P == null) {
                    if (!(this.R == null || this.P == null)) {
                        this.R.d().c(this.R);
                        this.P.a(this.R);
                        aq.a(this.c, "moved bookmark item");
                        this.R = null;
                        this.Q = null;
                        this.P = null;
                        break;
                    }
                } else {
                    this.P.a(this.Q);
                    aq.a(this.c, "restored bookmark item");
                    this.Q = null;
                    this.P = null;
                    break;
                }
                break;
            case R.id.action_bookmark_page:
                f();
                break;
            case R.id.new_folder:
                if (this.O != null && !this.U) {
                    this.M.a(this.O);
                    this.U = true;
                    break;
                }
        }
        return true;
    }
}
