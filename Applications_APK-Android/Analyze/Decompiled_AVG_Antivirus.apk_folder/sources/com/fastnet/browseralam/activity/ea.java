package com.fastnet.browseralam.activity;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import com.fastnet.browseralam.b.a;

public final class ea implements View.OnFocusChangeListener {
    a a = new eb(this);
    final /* synthetic */ dx b;

    public ea(dx dxVar) {
        this.b = dxVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void */
    public final void onFocusChange(View view, boolean z) {
        if (z) {
            this.b.b.ac.addTextChangedListener(this.b.a);
            String y = this.b.b.L.y();
            if (y == null || y.startsWith("file://")) {
                this.b.b.ac.setText("");
                Drawable unused = this.b.b.ba = this.b.b.aZ;
                this.b.b.ac.setCompoundDrawables(null, null, this.b.b.aZ, null);
            } else {
                this.b.b.ac.setText(y);
                Drawable unused2 = this.b.b.ba = this.b.b.aY;
                this.b.b.ac.setCompoundDrawables(null, null, this.b.b.aY, null);
            }
            ((AutoCompleteTextView) view).selectAll();
            this.b.b.k = this.a;
            return;
        }
        this.b.b.ac.removeTextChangedListener(this.b.a);
        Drawable unused3 = this.b.b.ba = this.b.b.aX;
        this.b.b.ac.setCompoundDrawables(null, null, this.b.b.aX, null);
        ((InputMethodManager) this.b.b.getSystemService("input_method")).hideSoftInputFromWindow(this.b.b.ac.getWindowToken(), 0);
        if (this.b.b.L != null) {
            if (this.b.b.L.j() < 100) {
                this.b.b.F();
            } else {
                this.b.b.G();
            }
            this.b.b.a(this.b.b.L.y(), false);
        }
        this.b.b.k = this.b.b.j;
    }
}
