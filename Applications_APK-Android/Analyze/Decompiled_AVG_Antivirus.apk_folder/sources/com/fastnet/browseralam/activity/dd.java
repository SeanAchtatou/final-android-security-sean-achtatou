package com.fastnet.browseralam.activity;

import android.view.View;
import com.fastnet.browseralam.i.aq;

final class dd implements View.OnClickListener {
    final /* synthetic */ BrowserActivity a;

    dd(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    public final void onClick(View view) {
        if (this.a.ax) {
            aq.a(this.a.ao, "You must disable this option in settings");
            return;
        }
        if (this.a.bZ.isChecked()) {
            this.a.bZ.callOnClick();
        }
        this.a.bY.setChecked(!this.a.bY.isChecked());
        this.a.L.c(this.a.bY.isChecked());
    }
}
