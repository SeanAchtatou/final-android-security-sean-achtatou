package com.fastnet.browseralam.activity;

import android.util.Log;
import com.fastnet.browseralam.b.a;

final class bj implements a {
    final /* synthetic */ BrowserActivity a;

    bj(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    public final void a() {
        if (this.a.L == null) {
            Log.e("Lightning", "This shouldn't happen ever");
        } else if (this.a.L.H()) {
            if (!this.a.L.c()) {
                this.a.x();
            } else {
                this.a.L.t();
            }
        } else if (this.a.L.D()) {
            this.a.d(this.a.L.h());
        } else {
            this.a.c(this.a.L.h());
        }
    }
}
