package com.fastnet.browseralam.activity;

import android.graphics.drawable.Drawable;
import java.io.File;

final class fl {
    final /* synthetic */ ey a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public String c;
    private String d;
    /* access modifiers changed from: private */
    public Drawable e;
    /* access modifiers changed from: private */
    public boolean f;

    fl(ey eyVar) {
        this.a = eyVar;
    }

    protected fl(ey eyVar, File file, Drawable drawable) {
        this.a = eyVar;
        this.b = file.getPath();
        this.c = file.getName();
        this.e = drawable;
        if (file.isDirectory()) {
            this.f = true;
            this.d = "Folder";
            return;
        }
        this.d = "File";
    }
}
