package com.fastnet.browseralam.activity;

import com.fastnet.browseralam.c.c;
import com.fastnet.browseralam.c.f;
import java.io.File;

final class ij implements Runnable {
    final /* synthetic */ File a;
    final /* synthetic */ Cif b;

    ij(Cif ifVar, File file) {
        this.b = ifVar;
        this.a = file;
    }

    public final void run() {
        int i;
        int i2 = 1;
        File[] listFiles = this.a.listFiles();
        if (listFiles != null && listFiles.length != 0) {
            int length = listFiles.length;
            int i3 = 0;
            while (i3 < length) {
                File file = listFiles[i3];
                if (file.isDirectory()) {
                    i = i2 + 1;
                    f fVar = new f(this.b.b, this.b.l.format((long) i2));
                    long unused = this.b.a(file, fVar);
                    this.b.ab.put(fVar.a(), fVar);
                } else {
                    i = i2;
                }
                i3++;
                i2 = i;
            }
            if (this.b.aa.size() != 0) {
                c.a(this.b.aa);
                if (this.a == this.b.b) {
                    c.e();
                }
            }
        }
    }
}
