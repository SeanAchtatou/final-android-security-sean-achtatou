package com.fastnet.browseralam.activity;

import android.view.View;
import android.widget.TextView;
import com.fastnet.browseralam.h.a;

final class gx implements View.OnClickListener {
    final /* synthetic */ TextView a;
    final /* synthetic */ gr b;

    gx(gr grVar, TextView textView) {
        this.b = grVar;
        this.a = textView;
    }

    public final void onClick(View view) {
        a.x(this.b.V = !this.b.V);
        this.b.u.dismiss();
        if (this.b.V) {
            this.a.setText("Show Swipe Menu");
        } else {
            this.a.setText("Show 3 Dot Menu");
        }
        this.b.K.d();
    }
}
