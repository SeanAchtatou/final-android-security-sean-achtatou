package com.fastnet.browseralam.activity;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.q;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.k;
import android.support.v7.app.l;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebIconDatabase;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewDatabase;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import androidx.core.view.GravityCompat;
import androidx.core.view.InputDeviceCompat;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.b;
import com.fastnet.browseralam.b.e;
import com.fastnet.browseralam.b.f;
import com.fastnet.browseralam.b.g;
import com.fastnet.browseralam.b.h;
import com.fastnet.browseralam.c.c;
import com.fastnet.browseralam.g.af;
import com.fastnet.browseralam.g.ar;
import com.fastnet.browseralam.g.bh;
import com.fastnet.browseralam.g.m;
import com.fastnet.browseralam.g.x;
import com.fastnet.browseralam.h.a;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.i.s;
import com.fastnet.browseralam.view.AnimatedProgressBar;
import com.fastnet.browseralam.view.CustomViewPager;
import com.fastnet.browseralam.view.WebViewFrame;
import com.fastnet.browseralam.view.XWebView;
import com.fastnet.browseralam.window.SpeedWindow;
import com.fastnet.browseralam.window.XWindow;
import com.google.android.gms.actions.SearchIntents;
import com.jobstrak.drawingfun.lib.mopub.common.Constants;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BrowserActivity extends ThemableActivity implements View.OnClickListener, g {
    /* access modifiers changed from: private */
    public static BrowserActivity bc;
    /* access modifiers changed from: private */
    public static final int bd = Build.VERSION.SDK_INT;
    private static final ViewGroup.LayoutParams be = new ViewGroup.LayoutParams(-1, -1);
    private static final FrameLayout.LayoutParams bf = new FrameLayout.LayoutParams(-1, -1);
    private TextView A;
    /* access modifiers changed from: private */
    public TextView B;
    /* access modifiers changed from: private */
    public TextView C;
    /* access modifiers changed from: private */
    public String D;
    /* access modifiers changed from: private */
    public CustomViewPager E;
    /* access modifiers changed from: private */
    public PopupWindow F;
    /* access modifiers changed from: private */
    public FrameLayout G;
    /* access modifiers changed from: private */
    public View H;
    private int I = aq.a(56);
    /* access modifiers changed from: private */
    public List J = new ArrayList();
    /* access modifiers changed from: private */
    public List K = new ArrayList();
    /* access modifiers changed from: private */
    public XWebView L;
    /* access modifiers changed from: private */
    public WebView M;
    private m N;
    private m O;
    private af P;
    private af Q;
    /* access modifiers changed from: private */
    public ar R;
    /* access modifiers changed from: private */
    public bh S;
    /* access modifiers changed from: private */
    public h T = new be(this);
    private ei U;
    /* access modifiers changed from: private */
    public y V;
    private fz W;
    /* access modifiers changed from: private */
    public f X;
    private x Y;
    /* access modifiers changed from: private */
    public com.fastnet.browseralam.f.h Z;
    private int aA;
    private int aB;
    private int aC;
    /* access modifiers changed from: private */
    public int aD;
    /* access modifiers changed from: private */
    public int aE;
    private int aF;
    private Drawable aG;
    private Drawable aH;
    private Drawable aI;
    private String aJ;
    private String aK;
    private String aL;
    private String aM;
    /* access modifiers changed from: private */
    public c aN;
    private com.fastnet.browseralam.c.h aO;
    private RelativeLayout aP;
    private k aQ;
    private String aR = "file://history.html";
    /* access modifiers changed from: private */
    public a aS;
    private Bitmap aT;
    private Bitmap aU;
    private final ColorDrawable aV = new ColorDrawable();
    private Drawable aW;
    /* access modifiers changed from: private */
    public Drawable aX;
    /* access modifiers changed from: private */
    public Drawable aY;
    /* access modifiers changed from: private */
    public Drawable aZ;
    private AnimatedProgressBar aa;
    /* access modifiers changed from: private */
    public q ab;
    /* access modifiers changed from: private */
    public AutoCompleteTextView ac;
    private ImageView ad;
    private ImageView ae;
    /* access modifiers changed from: private */
    public ImageView af;
    private VideoView ag;
    private View ah;
    private View ai;
    private b aj;
    private e ak;
    private WebChromeClient.CustomViewCallback al;
    private ValueCallback am;
    private ValueCallback an;
    /* access modifiers changed from: private */
    public Activity ao;
    private boolean ap;
    private boolean aq;
    /* access modifiers changed from: private */
    public boolean ar;
    /* access modifiers changed from: private */
    public boolean as;
    /* access modifiers changed from: private */
    public boolean at;
    private boolean au;
    private boolean av;
    /* access modifiers changed from: private */
    public boolean aw;
    /* access modifiers changed from: private */
    public boolean ax;
    private int ay;
    private int az;
    private TextView bA;
    private TextView bB;
    private TextView bC;
    private TextView bD;
    private TextView bE;
    private TextView bF;
    /* access modifiers changed from: private */
    public TextView bG;
    private TextView bH;
    private TextView bI;
    private TextView bJ;
    private TextView bK;
    private TextView bL;
    private TextView bM;
    private TextView bN;
    private int[] bO = {560, 561, 562, 563, 564};
    private int[] bP = {780, 781, 782, 783, 784};
    private int[] bQ = new int[5];
    private int[] bR = new int[5];
    private boolean bS;
    private View.OnTouchListener bT = new cr(this);
    private View.OnTouchListener bU = new cs(this);
    private View.OnTouchListener bV = new ct(this);
    private View.OnClickListener bW = new cx(this);
    /* access modifiers changed from: private */
    public View bX;
    /* access modifiers changed from: private */
    public CheckedTextView bY;
    /* access modifiers changed from: private */
    public CheckedTextView bZ;
    /* access modifiers changed from: private */
    public Drawable ba;
    /* access modifiers changed from: private */
    public ViewGroup bb;
    private int bg;
    private android.support.v4.widget.q bh = new Cdo(this);
    private android.support.v4.widget.q bi = new dr(this);
    private Paint bj = new Paint();
    private Paint bk = new Paint();
    private int bl;
    private int bm;
    private float bn;
    private RectF bo;
    /* access modifiers changed from: private */
    public boolean bp;
    private boolean bq = false;
    private boolean br = false;
    /* access modifiers changed from: private */
    public k bs;
    /* access modifiers changed from: private */
    public k bt;
    private LinearLayout bu;
    private LinearLayout bv;
    private TextView bw;
    /* access modifiers changed from: private */
    public TextView bx;
    private TextView by;
    private TextView bz;
    /* access modifiers changed from: private */
    public CheckedTextView ca;
    private com.fastnet.browseralam.e.a cb;
    /* access modifiers changed from: private */
    public AccelerateDecelerateInterpolator cc = new AccelerateDecelerateInterpolator();
    private com.fastnet.browseralam.b.a cd = new df(this);
    private final int ce = aq.a(232);
    private final int cf = aq.a(250);
    private final ForegroundColorSpan cg = new ForegroundColorSpan(-12303292);
    private final ForegroundColorSpan ch = new ForegroundColorSpan(Color.parseColor("#35D002"));
    /* access modifiers changed from: private */
    public int ci = (-aq.a(56));
    /* access modifiers changed from: private */
    public int cj = (-aq.a(170));
    private com.fastnet.browseralam.f.q ck = new dm(this);
    private List cl = new ArrayList();
    /* access modifiers changed from: private */
    public Handler cm = com.fastnet.browseralam.b.k.a();
    Animator.AnimatorListener i = new bh(this);
    com.fastnet.browseralam.b.a j = new bj(this);
    com.fastnet.browseralam.b.a k = this.j;
    int[] l = new int[7];
    FrameLayout.LayoutParams m;
    /* access modifiers changed from: private */
    public DrawerLayout n;
    /* access modifiers changed from: private */
    public FrameLayout o;
    /* access modifiers changed from: private */
    public FrameLayout p;
    private dv q;
    private RecyclerView r;
    private RecyclerView s;
    /* access modifiers changed from: private */
    public LinearLayout t;
    /* access modifiers changed from: private */
    public LinearLayout u;
    /* access modifiers changed from: private */
    public LinearLayout v;
    /* access modifiers changed from: private */
    public RelativeLayout w;
    private RelativeLayout x;
    /* access modifiers changed from: private */
    public RelativeLayout y;
    /* access modifiers changed from: private */
    public EditText z;

    static /* synthetic */ void F(BrowserActivity browserActivity) {
        for (XWebView w2 : browserActivity.J) {
            w2.w().clearCache(true);
        }
        for (XWebView w3 : browserActivity.K) {
            w3.w().clearCache(true);
        }
    }

    static /* synthetic */ void L(BrowserActivity browserActivity) {
        if (DrawerLayout.g(browserActivity.u)) {
            browserActivity.n.b();
        }
        if (DrawerLayout.g(browserActivity.t)) {
            browserActivity.n.b();
        } else {
            browserActivity.n.e(browserActivity.t);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.b.f.a(java.io.File, boolean):boolean
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.fastnet.browseralam.b.f.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.b.f.a(java.io.File, boolean):boolean */
    private synchronized void M() {
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof s)) {
            Thread.setDefaultUncaughtExceptionHandler(new s());
        }
        this.aU = aq.a(getResources(), this.av);
        this.aS = a.a();
        this.av = a.ai();
        this.bg = a.B();
        if (this.bg == 0) {
            if (a.o()) {
                setContentView((int) R.layout.activity_main1);
            } else {
                setContentView((int) R.layout.activity_main);
            }
            this.p = (FrameLayout) findViewById(R.id.main_layout);
            this.n = (DrawerLayout) findViewById(R.id.drawer_layout);
            this.X = new a(this, this.n);
            this.n = (DrawerLayout) findViewById(R.id.drawer_layout);
            this.n.a();
            this.n.a((int) R.drawable.drawer_shadow_left, (int) GravityCompat.START);
            this.n.a((int) R.drawable.drawer_shadow_right, (int) GravityCompat.END);
            this.t = (LinearLayout) findViewById(R.id.right_drawer);
            this.t.setLayerType(0, null);
            this.u = (LinearLayout) findViewById(R.id.left_drawer);
            this.u.setLayerType(0, null);
            int a = aq.a() - this.I;
            int a2 = (getResources().getConfiguration().screenLayout & 15) == 4 ? aq.a(320) : aq.a(300);
            int i2 = a > a2 ? a2 : a;
            DrawerLayout.LayoutParams layoutParams = (DrawerLayout.LayoutParams) this.t.getLayoutParams();
            DrawerLayout.LayoutParams layoutParams2 = (DrawerLayout.LayoutParams) this.u.getLayoutParams();
            layoutParams.width = i2;
            layoutParams2.width = i2;
            this.t.setLayoutParams(layoutParams);
            this.t.requestLayout();
            this.u.setLayoutParams(layoutParams2);
            this.u.requestLayout();
            this.n.a(this.bh);
            findViewById(R.id.action_back).setOnClickListener(this);
            findViewById(R.id.action_forward).setOnClickListener(this);
            ImageView imageView = (ImageView) findViewById(R.id.new_tab_button);
            imageView.setOnClickListener(this);
            imageView.setOnLongClickListener(new bp(this));
            this.E = (CustomViewPager) findViewById(R.id.viewpager);
            CustomViewPager customViewPager = this.E;
            ef efVar = new ef(this, b());
            fq fqVar = new fq();
            fqVar.a(this.r);
            fp fpVar = new fp();
            fpVar.a(this.s);
            efVar.a(fqVar, "Tabs");
            efVar.a(fpVar, "Incognito");
            customViewPager.a(efVar);
            this.B = (TextView) findViewById(R.id.tabs);
            this.B.setOnClickListener(new ca(this));
            this.C = (TextView) findViewById(R.id.incognito);
            this.C.setOnClickListener(new cl(this));
            this.r = (RecyclerView) getLayoutInflater().inflate((int) R.layout.tab_listview, (ViewGroup) null);
            this.s = (RecyclerView) getLayoutInflater().inflate((int) R.layout.tab_listview, (ViewGroup) null);
            af afVar = new af(this, this.E, this.r, this.J, false);
            this.P = afVar;
            this.N = afVar;
            af afVar2 = new af(this, this.E, this.s, this.K, true);
            this.Q = afVar2;
            this.O = afVar2;
        }
        if (this.bg == 3) {
            setContentView((int) R.layout.activity_main5);
            this.p = (FrameLayout) findViewById(R.id.main_layout);
            this.n = (DrawerLayout) findViewById(R.id.drawer_layout);
            this.X = new a(this, this.n);
            this.n = (DrawerLayout) findViewById(R.id.drawer_layout);
            this.n.a();
            this.u = (LinearLayout) findViewById(R.id.left_drawer);
            this.u.setLayerType(0, null);
            int a3 = aq.a() - this.I;
            int a4 = aq.a(320);
            if (a3 > a4) {
                a3 = a4;
            }
            DrawerLayout.LayoutParams layoutParams3 = (DrawerLayout.LayoutParams) this.u.getLayoutParams();
            layoutParams3.width = a3;
            this.u.setLayoutParams(layoutParams3);
            this.u.requestLayout();
            this.n.a(this.bi);
            this.x = (RelativeLayout) findViewById(R.id.toolbar_layout);
            this.x.findViewById(R.id.quit).setOnClickListener(new cw(this));
            this.x.findViewById(R.id.window_mode).setOnClickListener(new di(this));
        } else if (this.bg == 1) {
            setContentView((int) R.layout.activity_main2);
            this.p = (FrameLayout) findViewById(R.id.main_layout);
            this.V = new y(this, this.p, this.J, this.K);
            this.X = this.V.i();
            this.T = this.V;
        } else if (this.bg == 2) {
            setContentView((int) R.layout.activity_main3);
            this.p = (FrameLayout) findViewById(R.id.main_layout);
            this.U = new ei(this, this.p, this.J, this.K);
            this.W = new fz(this, this.p);
            this.X = this.W.a();
            this.T = this.U;
        } else if (this.bg == 5) {
            setContentView((int) R.layout.activity_main4);
            this.p = (FrameLayout) findViewById(R.id.main_layout);
            eg egVar = new eg(this, this.p, this.J);
            this.X = new jq();
            this.T = egVar;
        }
        aq.a(getLayoutInflater());
        this.ao = this;
        this.J.clear();
        this.K.clear();
        this.aj = new b(this);
        this.ak = new e(this);
        if (this.x == null) {
            this.x = (RelativeLayout) findViewById(R.id.toolbar_layout);
        }
        this.o = (FrameLayout) findViewById(R.id.webview_frame);
        this.aV.setColor(((ColorDrawable) this.x.getBackground()).getColor());
        this.aa = (AnimatedProgressBar) findViewById(R.id.progress_view);
        this.w = (RelativeLayout) getLayoutInflater().inflate((int) R.layout.find_in_page, (ViewGroup) null);
        this.p.addView(this.w);
        FrameLayout.LayoutParams layoutParams4 = (FrameLayout.LayoutParams) this.w.getLayoutParams();
        layoutParams4.height = this.I;
        this.w.setLayoutParams(layoutParams4);
        this.z = (EditText) findViewById(R.id.search_query);
        this.z.setOnEditorActionListener(new bf(this));
        ((ImageButton) findViewById(R.id.button_next)).setOnClickListener(this);
        ((ImageButton) findViewById(R.id.button_back)).setOnClickListener(this);
        ((ImageButton) findViewById(R.id.button_quit)).setOnClickListener(this);
        this.p.removeView(this.w);
        this.aL = a.u();
        this.aN = c.a(getApplicationContext());
        this.aO = new com.fastnet.browseralam.c.h(this, getLayoutInflater());
        this.aR = com.fastnet.browseralam.c.h.a(this);
        this.v = (LinearLayout) findViewById(R.id.search_box);
        this.ae = (ImageView) findViewById(R.id.tabs_button);
        this.ae.setLayerType(0, null);
        this.ad = this.ae;
        this.ab = new q(this, new dw(this));
        if (a.A()) {
            this.ad.setOnTouchListener(this.bV);
        } else {
            this.ad.setOnTouchListener(this.bT);
        }
        this.ac = (AutoCompleteTextView) findViewById(R.id.search);
        this.aK = getString(R.string.untitled);
        this.aA = getResources().getColor(R.color.primary_color);
        this.aD = getResources().getColor(R.color.accent_color);
        if (this.av) {
            this.aC = -1;
            this.aE = -1;
        } else {
            this.aC = getResources().getColor(R.color.tabs);
            this.aE = -12303292;
        }
        this.aW = android.support.v4.content.a.a(this, R.drawable.ic_action_delete);
        this.aX = android.support.v4.content.a.a(this, R.drawable.ic_action_refresh);
        this.aY = android.support.v4.content.a.a(this, R.drawable.ic_action_copy);
        this.aZ = android.support.v4.content.a.a(this, R.drawable.ic_action_paste);
        this.Z = new com.fastnet.browseralam.f.h(this, this.p);
        this.Z.a(this.aD, -65536, Color.parseColor("#FFA500"), -16711936);
        this.Z.a(this.ck);
        this.af = (ImageView) findViewById(R.id.overflow_icon);
        this.G = (FrameLayout) getLayoutInflater().inflate((int) R.layout.page_info, (ViewGroup) null);
        this.y = (RelativeLayout) this.G.findViewById(R.id.page_info_view);
        this.m = (FrameLayout.LayoutParams) this.y.getLayoutParams();
        int i3 = -this.cf;
        this.y.setTranslationY((float) i3);
        this.H = this.G.findViewById(R.id.shadowOverlay);
        this.H.setOnClickListener(new cy(this, i3));
        this.G.findViewById(R.id.view_page_source).setOnClickListener(new da(this));
        this.G.findViewById(R.id.block_url).setOnClickListener(new db(this));
        this.bZ = (CheckedTextView) this.G.findViewById(R.id.intercept_url);
        this.bZ.setOnClickListener(new dc(this));
        this.bY = (CheckedTextView) this.G.findViewById(R.id.intercept_videos);
        this.bY.setOnClickListener(new dd(this));
        this.ca = (CheckedTextView) this.G.findViewById(R.id.run_background);
        this.ca.setOnClickListener(new de(this));
        this.A = (TextView) this.G.findViewById(R.id.page_url);
        this.bX = this.G.findViewById(R.id.page_info_cover);
        this.cb = com.fastnet.browseralam.e.a.a(this.bX);
        int a5 = aq.a(24);
        this.aW.setBounds(0, 0, a5, a5);
        this.aX.setBounds(0, 0, a5, a5);
        this.aY.setBounds(0, 0, a5, a5);
        this.aZ.setBounds(0, 0, a5, a5);
        this.ba = this.aX;
        dx dxVar = new dx(this, (byte) 0);
        this.ac.setCompoundDrawables(null, null, this.aX, null);
        AutoCompleteTextView autoCompleteTextView = this.ac;
        dxVar.getClass();
        autoCompleteTextView.setOnKeyListener(new ec(dxVar));
        AutoCompleteTextView autoCompleteTextView2 = this.ac;
        dxVar.getClass();
        autoCompleteTextView2.setOnFocusChangeListener(new ea(dxVar));
        AutoCompleteTextView autoCompleteTextView3 = this.ac;
        dxVar.getClass();
        autoCompleteTextView3.setOnEditorActionListener(new dz(dxVar));
        AutoCompleteTextView autoCompleteTextView4 = this.ac;
        dxVar.getClass();
        autoCompleteTextView4.setOnTouchListener(new ed(dxVar));
        if (bd >= 21) {
            this.aG = new ColorDrawable(-1);
            this.aH = new ColorDrawable(-3355444);
            this.aI = new ColorDrawable(-7829368);
        } else {
            this.aG = android.support.v4.content.a.a(this, R.drawable.card_bg);
            this.aI = android.support.v4.content.a.a(this, R.drawable.card_bg_incognito);
            this.aH = android.support.v4.content.a.a(this, R.drawable.card_bg_dark);
        }
        this.X.a((File) null, false);
        AutoCompleteTextView autoCompleteTextView5 = this.ac;
        autoCompleteTextView5.setThreshold(1);
        autoCompleteTextView5.setDropDownWidth(-1);
        autoCompleteTextView5.setDropDownAnchor(R.id.progress_view);
        autoCompleteTextView5.setOnItemClickListener(new bm(this, autoCompleteTextView5));
        autoCompleteTextView5.setSelectAllOnFocus(true);
        this.Y = new x(this.ao, this.ac, this.av);
        autoCompleteTextView5.setAdapter(this.Y);
        O();
        h();
        if (bd <= 18) {
            WebIconDatabase.getInstance().open(getDir("icons", 0).getPath());
        }
    }

    private void N() {
        int a = aq.a(56);
        this.bj = new Paint();
        this.bj.setTextAlign(Paint.Align.CENTER);
        this.bj.setStyle(Paint.Style.STROKE);
        this.bj.setStrokeWidth(aq.a(1.6f));
        this.bn = aq.a(0.6f);
        this.bk = new Paint();
        this.bk.setTextAlign(Paint.Align.CENTER);
        this.bk.setAntiAlias(true);
        this.bk.setStyle(Paint.Style.FILL);
        this.bk.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
        this.bk.setTextSize((float) aq.a(13));
        float a2 = aq.a(19.5f);
        this.bl = (int) aq.a(27.5f);
        this.bm = (int) aq.a(32.5f);
        this.bo = new RectF(a2, a2, ((float) a) - a2, ((float) a) - a2);
    }

    private void O() {
        boolean z2 = true;
        if (this.aS == null) {
            this.aS = a.a();
        }
        if (a.b()) {
            a.a(false);
            c.a();
            L();
            return;
        }
        if (this.Y != null) {
            this.Y.a();
        }
        int[] J2 = a.J();
        if (!Arrays.equals(this.l, J2)) {
            LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate((int) R.layout.overflow_menu, (ViewGroup) null);
            View findViewById = linearLayout.findViewById(R.id.menu_actions);
            linearLayout.findViewById(R.id.page_info).setOnClickListener(this.bW);
            linearLayout.findViewById(R.id.bookmark_page).setOnClickListener(this.bW);
            linearLayout.findViewById(R.id.action_forward).setOnClickListener(this.bW);
            View findViewById2 = linearLayout.findViewById(R.id.action_settings);
            findViewById2.setOnClickListener(this.bW);
            View[] viewArr = {linearLayout.findViewById(R.id.action_new_tab), linearLayout.findViewById(R.id.incognito_tab), linearLayout.findViewById(R.id.action_bookmarks), linearLayout.findViewById(R.id.action_history), linearLayout.findViewById(R.id.action_find), linearLayout.findViewById(R.id.action_desktop_mode), linearLayout.findViewById(R.id.action_reading_mode), linearLayout.findViewById(R.id.action_rendering_mode), linearLayout.findViewById(R.id.action_share), linearLayout.findViewById(R.id.action_update)};
            linearLayout.removeAllViews();
            linearLayout.addView(findViewById);
            this.l = J2;
            if (this.l == null) {
                this.l = new int[]{0, 1, 2, 3, 4, 5, 6, 7};
            }
            for (int i2 : this.l) {
                linearLayout.addView(viewArr[i2]);
                viewArr[i2].setOnClickListener(this.bW);
            }
            linearLayout.addView(findViewById2);
            CheckedTextView checkedTextView = (CheckedTextView) viewArr[5];
            this.F = new PopupWindow(linearLayout, -2, -2);
            this.F.setFocusable(true);
            this.F.setOutsideTouchable(true);
            this.F.setBackgroundDrawable(new ColorDrawable(0));
            if (bd < 19) {
                this.af.setOnClickListener(new cu(this, checkedTextView));
            } else {
                this.af.setOnClickListener(new cv(this, checkedTextView));
            }
        }
        int[] L2 = a.L();
        int[] K2 = a.K();
        if (!Arrays.equals(L2, this.bR) || !Arrays.equals(K2, this.bQ)) {
            this.bR = L2;
            if (this.bR == null) {
                this.bR = this.bP;
            }
            this.bu = (LinearLayout) getLayoutInflater().inflate((int) R.layout.image_dialog, (ViewGroup) null);
            this.bw = (TextView) this.bu.findViewById(R.id.url);
            this.bx = (TextView) this.bu.findViewById(R.id.blockUrl);
            this.bz = (TextView) this.bu.findViewById(R.id.newTabImageLink);
            this.by = (TextView) this.bu.findViewById(R.id.newTabImage);
            this.bA = (TextView) this.bu.findViewById(R.id.openImage);
            this.bB = (TextView) this.bu.findViewById(R.id.downloadImage);
            this.bC = (TextView) this.bu.findViewById(R.id.copyLink);
            this.bD = (TextView) this.bu.findViewById(R.id.shareLink);
            this.bE = (TextView) this.bu.findViewById(R.id.saveLink);
            this.bu.removeAllViews();
            this.bu.addView(this.bw);
            this.bu.addView(this.bx);
            b(this.bR);
            if (this.bu.getChildCount() == 2) {
                a.a("imageorder");
                b(this.bP);
            }
            this.bQ = K2;
            if (this.bQ == null) {
                this.bQ = this.bO;
            }
            this.bv = (LinearLayout) getLayoutInflater().inflate((int) R.layout.link_dialog, (ViewGroup) null);
            this.bF = (TextView) this.bv.findViewById(R.id.url);
            this.bG = (TextView) this.bv.findViewById(R.id.blockUrl);
            this.bH = (TextView) this.bv.findViewById(R.id.newTab1);
            this.bI = (TextView) this.bv.findViewById(R.id.newTab2);
            this.bK = (TextView) this.bv.findViewById(R.id.copyLink);
            this.bJ = (TextView) this.bv.findViewById(R.id.openLink);
            this.bM = (TextView) this.bv.findViewById(R.id.shareLink);
            this.bN = (TextView) this.bv.findViewById(R.id.saveLink);
            this.bL = (TextView) this.bv.findViewById(R.id.copyText);
            this.bv.removeAllViews();
            this.bv.addView(this.bF);
            this.bv.addView(this.bG);
            a(this.bQ);
            if (this.bv.getChildCount() == 2) {
                a.a("linkorder");
                a(this.bO);
            }
            this.bw.setOnLongClickListener(new cj(this));
            this.bF.setOnLongClickListener(new ck(this));
            this.bs = new l(this.ao).c();
            this.bs.a(this.bu);
            this.bt = new l(this.ao).c();
            this.bt.a(this.bv);
            int a = aq.a() - aq.a(30);
            this.bs.getWindow().setLayout(a, -2);
            this.bt.getWindow().setLayout(a, -2);
        }
        this.at = a.S();
        boolean q2 = a.q();
        ViewGroup viewGroup = (ViewGroup) this.o.getParent();
        int B2 = a.B();
        if (q2) {
            if (!(this.o instanceof WebViewFrame)) {
                viewGroup.removeView(this.o);
                this.o.removeAllViews();
                this.o = (FrameLayout) getLayoutInflater().inflate((int) R.layout.webview_frame_fullscreen, (ViewGroup) null);
                ((WebViewFrame) this.o).a(this, this.x, this.Z);
                if (B2 == 0) {
                    viewGroup.addView(this.o, 0);
                } else if (B2 == 1) {
                    viewGroup.addView(this.o, 0);
                } else {
                    viewGroup.addView(this.o, 3);
                }
                if (this.L != null) {
                    this.o.addView(this.L.w(), be);
                    this.o.addView(this.Z.c());
                }
            }
        } else if (this.o instanceof WebViewFrame) {
            this.o.removeAllViews();
            viewGroup.removeView(this.o);
            this.o = (FrameLayout) getLayoutInflater().inflate((int) R.layout.webview_frame, (ViewGroup) null);
            if (B2 == 0) {
                viewGroup.addView(this.o, 0);
            } else if (B2 == 1) {
                viewGroup.addView(this.o, 0);
            } else {
                viewGroup.addView(this.o, 3);
            }
            if (this.L != null) {
                this.L.w().setTranslationY(0.0f);
                this.o.addView(this.L.w(), be);
                this.o.addView(this.Z.c());
            }
        }
        if (B2 == 0) {
            this.ar = false;
            this.as = true;
            a(this.ae);
            this.N.b();
            this.O.b();
            if (a.o()) {
                e(0);
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.o.getLayoutParams();
                if (q2) {
                    layoutParams.topMargin = 0;
                } else {
                    layoutParams.topMargin = this.I;
                }
                this.o.setLayoutParams(layoutParams);
            } else {
                e(this.I);
                DrawerLayout.LayoutParams layoutParams2 = (DrawerLayout.LayoutParams) this.o.getLayoutParams();
                if (q2) {
                    layoutParams2.topMargin = 0;
                } else {
                    layoutParams2.topMargin = this.I;
                }
                this.o.setLayoutParams(layoutParams2);
            }
            boolean n2 = a.n();
            this.P.a(!n2);
            af afVar = this.Q;
            if (n2) {
                z2 = false;
            }
            afVar.a(z2);
        } else if (B2 == 1) {
            this.V.a(!this.bp);
            this.ar = true;
            this.as = false;
            FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) this.o.getLayoutParams();
            if (q2) {
                layoutParams3.topMargin = 0;
            } else {
                layoutParams3.topMargin = this.I;
            }
            this.o.setLayoutParams(layoutParams3);
        } else if (B2 == 2) {
            this.ar = false;
            this.as = false;
            a(this.ae);
            FrameLayout.LayoutParams layoutParams4 = (FrameLayout.LayoutParams) this.o.getLayoutParams();
            layoutParams4.topMargin = this.I;
            this.o.setLayoutParams(layoutParams4);
        } else {
            ar arVar = new ar(this, this.J, this.x);
            this.R = arVar;
            this.N = arVar;
            bh bhVar = new bh(this, this.K, this.x);
            this.S = bhVar;
            this.O = bhVar;
            this.ar = false;
            this.as = false;
            a(new ImageView(this));
            FrameLayout.LayoutParams layoutParams5 = (FrameLayout.LayoutParams) this.o.getLayoutParams();
            layoutParams5.topMargin = aq.a(98);
            this.o.setLayoutParams(layoutParams5);
        }
        if (a.A()) {
            this.ad.setOnTouchListener(this.bV);
        } else {
            this.ad.setOnTouchListener(this.bT);
        }
        if (B2 == 2) {
            this.ad.setOnTouchListener(this.bU);
        }
        this.au = a.U();
        if (a.s()) {
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().clearFlags(1024);
        }
        if (a.t()) {
            getWindow().setSoftInputMode(16);
        } else {
            getWindow().setSoftInputMode(48);
        }
        this.aF = a.ah();
        switch (a.ab()) {
            case 0:
                this.aJ = a.ac();
                if (!this.aJ.startsWith("http://") && !this.aJ.startsWith("https://")) {
                    this.aJ = "https://www.google.com/search?client=lightning&ie=UTF-8&oe=UTF-8&q=";
                    break;
                }
            case 1:
                this.aJ = "https://www.google.com/search?client=lightning&ie=UTF-8&oe=UTF-8&q=";
                break;
            case 2:
                this.aJ = "http://www.ask.com/web?qsrc=0&o=0&l=dir&qo=lightningBrowser&q=";
                break;
            case 3:
                this.aJ = "https://www.bing.com/search?q=";
                break;
            case 4:
                this.aJ = "https://search.yahoo.com/search?p=";
                break;
            case 5:
                this.aJ = "https://startpage.com/do/search?language=english&query=";
                break;
            case 6:
                this.aJ = "https://startpage.com/do/m/mobilesearch?language=english&query=";
                break;
            case 7:
                this.aJ = "https://duckduckgo.com/?t=lightning&q=";
                break;
            case 8:
                this.aJ = "https://duckduckgo.com/lite/?t=lightning&q=";
                break;
            case 9:
                this.aJ = "https://www.baidu.com/s?wd=";
                break;
            case 10:
                this.aJ = "https://yandex.ru/yandsearch?lr=21411&text=";
                break;
        }
        this.ax = a.V();
        if (this.ax) {
            com.fastnet.browseralam.d.b.a(this);
            if (!this.bp) {
                aq.a(this, this.cm, "Intercept all videos enabled");
            }
        } else {
            com.fastnet.browseralam.d.b.a();
        }
        j();
        com.fastnet.browseralam.a.b.b();
        com.fastnet.browseralam.a.b.a(getFilesDir());
        this.bp = false;
    }

    private void P() {
        if (!this.br) {
            this.x.setBackgroundColor(-12303292);
            this.v.setBackground(this.aI);
            this.ac.setTextColor(-1);
            this.af.setColorFilter(-1);
            this.br = true;
            this.bq = false;
        }
    }

    private void Q() {
        if (!this.bq) {
            if (this.av) {
                this.v.setBackground(this.aH);
            } else {
                this.x.setBackgroundColor(this.aA);
                this.v.setBackground(this.aG);
            }
            this.ac.setTextColor(-16777216);
            this.af.clearColorFilter();
            this.bq = true;
            this.br = false;
        }
    }

    /* access modifiers changed from: private */
    public void R() {
        this.aP = this.aO.a(this.L);
        this.L.c(this.aR);
    }

    static /* synthetic */ void W(BrowserActivity browserActivity) {
        boolean z2 = false;
        String y2 = browserActivity.L.y();
        int length = y2.length();
        int indexOf = y2.indexOf(47);
        while (true) {
            if (indexOf >= length) {
                indexOf = 0;
                break;
            } else if (y2.charAt(indexOf) != '/') {
                break;
            } else {
                indexOf++;
            }
        }
        int indexOf2 = y2.indexOf(47, indexOf);
        if (indexOf2 != -1) {
            length = indexOf2;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(y2);
        spannableStringBuilder.setSpan(browserActivity.cg, indexOf, length, 18);
        if (y2.startsWith("https")) {
            spannableStringBuilder.setSpan(browserActivity.ch, 0, 5, 18);
        }
        browserActivity.A.setText(spannableStringBuilder);
        int lineCount = browserActivity.A.getLineCount();
        if (lineCount == 0 || lineCount >= 2) {
            browserActivity.m.height = browserActivity.cf;
        } else {
            browserActivity.m.height = browserActivity.ce;
        }
        browserActivity.y.setLayoutParams(browserActivity.m);
        CheckedTextView checkedTextView = browserActivity.bY;
        if (browserActivity.ax || browserActivity.L.J()) {
            z2 = true;
        }
        checkedTextView.setChecked(z2);
        browserActivity.bZ.setChecked(browserActivity.L.I());
        browserActivity.ca.setChecked(browserActivity.L.K());
        browserActivity.p.addView(browserActivity.G);
        browserActivity.y.animate().translationY(0.0f).setDuration(220).setStartDelay(20).setInterpolator(browserActivity.cc);
        browserActivity.H.animate().alpha(1.0f).setDuration(220).setInterpolator(browserActivity.cc).setListener(null);
        browserActivity.bX.animate().alpha(0.0f).setDuration(100).setStartDelay(250).setListener(browserActivity.cb);
        browserActivity.k = browserActivity.cd;
    }

    static /* synthetic */ void X(BrowserActivity browserActivity) {
        View inflate = browserActivity.getLayoutInflater().inflate((int) R.layout.rendering_menu, (ViewGroup) null);
        PopupWindow popupWindow = new PopupWindow(inflate, -2, -2);
        inflate.findViewById(R.id.normal).setOnClickListener(new dg(browserActivity));
        inflate.findViewById(R.id.inverted).setOnClickListener(new dh(browserActivity));
        inflate.findViewById(R.id.grayscale).setOnClickListener(new dj(browserActivity));
        inflate.findViewById(R.id.invertedGrayscale).setOnClickListener(new dk(browserActivity));
        inflate.findViewById(R.id.nightMode).setOnClickListener(new dl(browserActivity));
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        popupWindow.showAsDropDown(browserActivity.af, 0, browserActivity.ci);
    }

    private static void a(List list, int i2) {
        int size = list.size();
        while (i2 < size) {
            ((XWebView) list.get(i2)).a(i2);
            i2++;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    public synchronized void a(List list, int i2, int i3) {
        if (list.size() >= 2) {
            int h = this.L.h();
            XWebView xWebView = i2 < 0 ? h == 0 ? (XWebView) list.get(list.size() - 1) : (XWebView) list.get(h - 1) : h == list.size() + -1 ? (XWebView) list.get(0) : (XWebView) list.get(h + 1);
            if (xWebView != null) {
                if (this.L != null) {
                    this.L.a(false);
                    this.L.d();
                    this.M = this.L.w();
                }
                this.L = xWebView;
                this.L.a(true);
                b(0);
                WebView w2 = this.L.w();
                w2.setAlpha(0.0f);
                this.o.addView(w2, be);
                this.o.removeView(this.Z.c());
                this.o.addView(this.Z.c());
                if (i3 == du.a) {
                    w2.setTranslationY((float) this.o.getHeight());
                    w2.setAlpha(1.0f);
                    w2.animate().translationY(0.0f).setDuration(200).setListener(this.i).start();
                } else if (i3 == du.b) {
                    w2.setTranslationY((float) (-this.o.getHeight()));
                    w2.setAlpha(1.0f);
                    w2.animate().translationY(0.0f).setDuration(200).setListener(this.i).start();
                } else if (i3 == du.c) {
                    w2.setTranslationX((float) this.o.getWidth());
                    w2.setAlpha(1.0f);
                    w2.animate().translationX(0.0f).setDuration(200).setListener(this.i).start();
                } else if (i3 == du.d) {
                    w2.setTranslationX((float) (-this.o.getWidth()));
                    w2.setAlpha(1.0f);
                    w2.animate().translationX(0.0f).setDuration(200).setListener(this.i).start();
                }
                a(this.L.y(), false);
                a(this.L.j());
                if (this.L.D()) {
                    P();
                } else {
                    Q();
                }
                this.L.k();
                this.L.e();
            }
        }
    }

    private void a(int[] iArr) {
        for (int i2 : iArr) {
            switch (i2) {
                case 560:
                    this.bv.addView(this.bH);
                    break;
                case 561:
                    this.bv.addView(this.bI);
                    break;
                case 562:
                    this.bv.addView(this.bK);
                    break;
                case 563:
                    this.bv.addView(this.bJ);
                    break;
                case 564:
                    this.bv.addView(this.bN);
                    break;
                case 565:
                    this.bv.addView(this.bM);
                    break;
                case 566:
                    this.bv.addView(this.bL);
                    break;
            }
        }
    }

    private void b(int i2) {
        String sb;
        Bitmap createBitmap = Bitmap.createBitmap(this.I, this.I, Bitmap.Config.ARGB_8888);
        if (this.L.D()) {
            sb = new StringBuilder().append(this.K.size()).toString();
            this.bj.setColor(-1);
            this.bk.setColor(-1);
        } else {
            sb = new StringBuilder().append(this.J.size()).toString();
            this.bj.setColor(this.aC);
            this.bk.setColor(this.aC);
        }
        if (i2 != 0) {
            this.bj.setColor(i2);
            this.bk.setColor(i2);
        }
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawRoundRect(this.bo, this.bn, this.bn, this.bj);
        canvas.drawText(sb, (float) this.bl, (float) this.bm, this.bk);
        this.ad.setImageDrawable(new BitmapDrawable(getResources(), createBitmap));
    }

    private synchronized void b(int i2, boolean z2) {
        if (z2) {
            this.O.b_(i2);
        } else {
            this.N.b_(i2);
        }
    }

    private void b(String str, String str2) {
        boolean D2 = this.L.D();
        this.bw.setText(str);
        this.bx.setVisibility(8);
        if (D2) {
            this.bz.setText("Open in new incognito tab");
        } else {
            this.bz.setText("Open in new tab");
        }
        this.bz.setOnClickListener(new by(this, str, D2));
        this.bz.setOnLongClickListener(new bz(this, str, D2));
        this.by.setOnClickListener(new cb(this, str2, D2));
        this.by.setOnLongClickListener(new cc(this, str2, D2));
        this.bx.setOnClickListener(new cd(this, str));
        this.bA.setOnClickListener(new ce(this, str2));
        this.bB.setOnClickListener(new cf(this, str2));
        this.bC.setOnClickListener(new cg(this, str, str2));
        this.bD.setOnClickListener(new ch(this, str2));
        this.bE.setOnClickListener(new ci(this, str2));
        if (str == null) {
            this.bz.setVisibility(8);
        }
        this.bs.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void */
    private synchronized void b(List list, int i2, int i3) {
        if (list != null) {
            this.L = (XWebView) list.get(i2);
            this.L.b(i2);
            this.O.a(i2, 1);
            this.O.b_(i3);
            this.o.removeAllViews();
            this.o.addView(this.L.w(), be);
            this.o.addView(this.Z.c());
            b(0);
            if (this.L.w() != null) {
                a(this.L.y(), false);
                a(this.L.j());
            } else {
                a("", false);
                a(0);
            }
            P();
            this.L.k();
            this.L.e();
        }
    }

    private void b(int[] iArr) {
        for (int i2 : iArr) {
            switch (i2) {
                case 780:
                    this.bu.addView(this.bz);
                    break;
                case 781:
                    this.bu.addView(this.by);
                    break;
                case 782:
                    this.bu.addView(this.bC);
                    break;
                case 783:
                    this.bu.addView(this.bB);
                    break;
                case 784:
                    this.bu.addView(this.bA);
                    break;
                case 785:
                    this.bu.addView(this.bE);
                    break;
                case 786:
                    this.bu.addView(this.bD);
                    break;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [?[OBJECT, ARRAY], int, int, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    /* access modifiers changed from: private */
    public synchronized void c(int i2) {
        int indexOf = this.J.indexOf(this.L);
        XWebView xWebView = (XWebView) this.J.get(i2);
        if (xWebView != null) {
            if (xWebView.y() != null && !xWebView.y().startsWith("file://")) {
                a.f(xWebView.y());
            }
            if (i2 < indexOf) {
                this.N.b_(i2);
            } else if (i2 + 1 < this.J.size()) {
                if (i2 == 0) {
                    c(this.J, 1, i2);
                } else if (indexOf == i2) {
                    c(this.J, i2 - 1, i2);
                } else {
                    this.N.b_(i2);
                }
            } else if (this.J.size() > 1) {
                if (indexOf == i2) {
                    c(this.J, i2 - 1, i2);
                } else {
                    this.N.b_(i2);
                }
            } else if (!this.au || (this.L.y() != null && !this.L.y().startsWith("file://") && !this.L.y().equals(this.aL))) {
                this.J.remove(i2);
                this.N.e(i2);
                if (a.g() && this.L != null && !this.L.D()) {
                    this.L.l();
                }
                if (a.i() && !this.L.D()) {
                    l();
                }
                if (a.h() && !this.L.D()) {
                    m();
                }
                if (!this.au) {
                    a((String) null, false, -1, false);
                    xWebView.s();
                } else {
                    xWebView.o();
                    xWebView.s();
                    this.L = null;
                    finish();
                }
            } else {
                this.K.clear();
                finish();
            }
            if (this.ay == -1 || i2 != this.ay || !xWebView.g()) {
                this.ay = -1;
            } else {
                finish();
            }
            b(0);
            xWebView.s();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void */
    private synchronized void c(List list, int i2, int i3) {
        if (list != null) {
            this.L = (XWebView) list.get(i2);
            this.L.b(i2);
            this.N.a(i2, 1);
            this.N.b_(i3);
            this.o.removeAllViews();
            this.o.addView(this.L.w(), be);
            this.o.addView(this.Z.c());
            b(0);
            if (this.L.w() != null) {
                a(this.L.y(), false);
                a(this.L.j());
            } else {
                a("", false);
                a(0);
            }
            Q();
            this.L.k();
            this.L.e();
        }
    }

    private void c(boolean z2) {
        Window window = getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        if (z2) {
            attributes.flags |= 1024;
        } else {
            attributes.flags &= -1025;
            if (this.ah != null) {
                this.ah.setSystemUiVisibility(0);
            } else {
                this.o.setSystemUiVisibility(0);
            }
        }
        window.setAttributes(attributes);
    }

    public static FrameLayout d() {
        bc.bb.removeView(bc.p);
        return bc.p;
    }

    private String d(String str) {
        boolean z2 = false;
        String str2 = this.aJ;
        String trim = str.trim();
        this.L.n();
        if (trim.startsWith("www.")) {
            trim = "http://" + trim;
        }
        boolean contains = trim.contains(".");
        boolean z3 = TextUtils.isDigitsOnly(trim.replace(".", "")) && trim.replace(".", "").length() >= 4 && trim.contains(".");
        boolean z4 = trim.startsWith("ftp://") || trim.startsWith("http://") || trim.startsWith("file://") || trim.startsWith("https://") || z3;
        if (trim.contains(" ") || !contains) {
            z2 = true;
        }
        if (z3 && (!trim.startsWith("http://") || !trim.startsWith("https://"))) {
            trim = "http://" + trim;
        }
        if (!z2) {
            return !z4 ? "http://" + trim : trim;
        }
        try {
            trim = URLEncoder.encode(trim, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str2 + trim;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
     arg types: [com.fastnet.browseralam.view.XWebView, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [?[OBJECT, ARRAY], int, int, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    /* access modifiers changed from: private */
    public synchronized void d(int i2) {
        int indexOf = this.K.indexOf(this.L);
        XWebView xWebView = (XWebView) this.K.get(i2);
        if (indexOf > i2) {
            this.O.b_(i2);
        } else if (i2 + 1 < this.K.size()) {
            if (i2 == 0) {
                b(this.K, 1, i2);
            } else if (indexOf == i2) {
                b(this.K, i2 - 1, i2);
            } else {
                this.O.b_(i2);
            }
        } else if (this.K.size() <= 1) {
            this.K.remove(0);
            this.O.e(i2);
            this.O.b();
            if (this.J.size() > 0) {
                a((XWebView) this.J.get(this.J.size() - 1), true);
            } else {
                a((String) null, false, -1, false);
            }
            if (this.ar) {
                this.V.a();
            } else if (this.as) {
                this.E.b(0);
            }
        } else if (indexOf == i2) {
            b(this.K, i2 - 1, i2);
        } else {
            this.O.b_(i2);
        }
        b(0);
        xWebView.s();
    }

    public static BrowserActivity e() {
        return bc;
    }

    private void e(int i2) {
        DrawerLayout.LayoutParams layoutParams = (DrawerLayout.LayoutParams) this.t.getLayoutParams();
        layoutParams.topMargin = i2;
        this.t.setLayoutParams(layoutParams);
        DrawerLayout.LayoutParams layoutParams2 = (DrawerLayout.LayoutParams) this.u.getLayoutParams();
        layoutParams2.topMargin = i2;
        this.u.setLayoutParams(layoutParams2);
    }

    private void e(String str) {
        boolean D2 = this.L.D();
        this.bG.setVisibility(8);
        this.bF.setText(str);
        if (D2) {
            this.bH.setText("Open link in new incognito tab");
            this.bI.setText("Open link in new tab");
        }
        this.bG.setOnClickListener(new bn(this, str));
        this.bJ.setOnClickListener(new bo(this, str));
        this.bK.setOnClickListener(new bq(this, str));
        this.bH.setOnClickListener(new br(this, str, D2));
        this.bI.setOnClickListener(new bs(this, str, D2));
        this.bH.setOnLongClickListener(new bt(this, str, D2));
        this.bI.setOnLongClickListener(new bu(this, str, D2));
        this.bM.setOnClickListener(new bv(this, str));
        this.bN.setOnClickListener(new bw(this, str));
        this.bL.setOnClickListener(new bx(this, str));
        this.bt.show();
    }

    public final boolean A() {
        return this.av;
    }

    public final Activity B() {
        return this.ao;
    }

    public final h C() {
        return this.T;
    }

    public final XWebView D() {
        return this.L;
    }

    public final void E() {
        if (this.aQ != null) {
            this.aQ.show();
            return;
        }
        int[] iArr = {1, 1, 1};
        LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate((int) R.layout.clearhistory_dialog, (ViewGroup) null);
        linearLayout.findViewById(R.id.cbHistory).setOnClickListener(new cm(this, iArr));
        linearLayout.findViewById(R.id.cbCache).setOnClickListener(new cn(this, iArr));
        linearLayout.findViewById(R.id.cbCookies).setOnClickListener(new co(this, iArr));
        this.aQ = new l(this).a("Clear browsing data").b(linearLayout).a("OK", new cq(this, iArr)).b("Cancel", new cp(this)).c();
        this.aQ.setCanceledOnTouchOutside(true);
        this.aQ.show();
    }

    public final void F() {
        if (!this.ac.hasFocus()) {
            this.ba = this.aW;
            this.ac.setCompoundDrawables(null, null, this.aW, null);
        }
    }

    public final void G() {
        if (!this.ac.hasFocus()) {
            this.ba = this.aX;
            this.ac.setCompoundDrawables(null, null, this.aX, null);
        }
    }

    public final void H() {
        if (this.L == null) {
            return;
        }
        if (this.L.j() < 100) {
            this.L.n();
            a(100);
            return;
        }
        this.L.m();
    }

    public final void I() {
        this.ac.requestFocus();
        ((InputMethodManager) getSystemService("input_method")).showSoftInput(this.ac, 1);
    }

    public final e J() {
        return this.ak;
    }

    public final void a(int i2) {
        if (i2 >= 100) {
            G();
        } else {
            F();
        }
        this.aa.a(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.b(int, boolean):void
     arg types: [int, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.b(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.b(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.b(int, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0015, code lost:
        if (r2 < r1.J.size()) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0009, code lost:
        if (r2 >= r1.K.size()) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(int r2, boolean r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            if (r3 == 0) goto L_0x000d
            java.util.List r0 = r1.K     // Catch:{ all -> 0x0025 }
            int r0 = r0.size()     // Catch:{ all -> 0x0025 }
            if (r2 < r0) goto L_0x000d
        L_0x000b:
            monitor-exit(r1)
            return
        L_0x000d:
            if (r3 != 0) goto L_0x0017
            java.util.List r0 = r1.J     // Catch:{ all -> 0x0025 }
            int r0 = r0.size()     // Catch:{ all -> 0x0025 }
            if (r2 >= r0) goto L_0x000b
        L_0x0017:
            com.fastnet.browseralam.view.XWebView r0 = r1.L     // Catch:{ all -> 0x0025 }
            boolean r0 = r0.D()     // Catch:{ all -> 0x0025 }
            if (r0 == 0) goto L_0x002d
            if (r3 == 0) goto L_0x0028
            r1.d(r2)     // Catch:{ all -> 0x0025 }
            goto L_0x000b
        L_0x0025:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0028:
            r0 = 0
            r1.b(r2, r0)     // Catch:{ all -> 0x0025 }
            goto L_0x000b
        L_0x002d:
            if (r3 == 0) goto L_0x0034
            r0 = 1
            r1.b(r2, r0)     // Catch:{ all -> 0x0025 }
            goto L_0x000b
        L_0x0034:
            r1.c(r2)     // Catch:{ all -> 0x0025 }
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void");
    }

    public final void a(int i2, boolean z2, int i3) {
        if (z2) {
            this.O.a(i2, i3);
        } else {
            this.N.a(i2, i3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [java.lang.String, int, int, boolean]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [java.lang.String, int, int, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    public final void a(Intent intent) {
        String str;
        if (intent != null) {
            String dataString = intent.getDataString();
            str = dataString == null ? intent.getStringExtra(SearchIntents.EXTRA_QUERY) : dataString;
        } else {
            str = null;
        }
        if (str != null) {
            if (this.ap) {
                this.ay = this.J.size();
                a(d(str), false, -1, false);
                ((XWebView) this.J.get(this.ay)).f();
                if (!str.startsWith("file://")) {
                    return;
                }
                if (this.bg == 1 && this.V.j()) {
                    this.V.a(300);
                } else if (this.bg == 2 && this.W.b()) {
                    this.W.a(300);
                }
            } else if (str.startsWith(Constants.HTTP)) {
                a(str, false, this.L.h(), this.L.D());
            } else if (str.startsWith("www")) {
                a("http://" + str, false, this.L.h(), this.L.D());
            } else {
                a(d(str), false, this.L.h(), this.L.D());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [java.lang.String, int, int, boolean]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    public final void a(Message message) {
        if (message != null) {
            WebView.WebViewTransport webViewTransport = (WebView.WebViewTransport) message.obj;
            if (this.at) {
                XWebView a = new XWebView(this, "", this.L.D(), true).a();
                a.a(this.Z);
                webViewTransport.setWebView(a.w());
                message.sendToTarget();
                return;
            }
            int h = this.L.h() + 1;
            a("", false, h, this.L.D());
            if (this.L.D()) {
                webViewTransport.setWebView(((XWebView) this.K.get(h)).w());
            } else {
                webViewTransport.setWebView(((XWebView) this.J.get(h)).w());
            }
            message.sendToTarget();
        }
    }

    public final void a(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        if (view != null) {
            if (this.ah == null || customViewCallback == null) {
                try {
                    view.setKeepScreenOn(true);
                } catch (SecurityException e) {
                }
                this.az = getRequestedOrientation();
                FrameLayout frameLayout = (FrameLayout) getWindow().getDecorView();
                this.q = new dv(this);
                this.ah = view;
                this.q.addView(this.ah, bf);
                frameLayout.addView(this.q, bf);
                c(true);
                this.L.c(8);
                if ((view instanceof FrameLayout) && (((FrameLayout) view).getFocusedChild() instanceof VideoView)) {
                    this.ag = (VideoView) ((FrameLayout) view).getFocusedChild();
                    this.ag.setOnErrorListener(new ee(this, (byte) 0));
                    this.ag.setOnCompletionListener(new ee(this, (byte) 0));
                }
                if (bd < 19) {
                    frameLayout.setSystemUiVisibility(2);
                } else {
                    frameLayout.setSystemUiVisibility(InputDeviceCompat.SOURCE_TOUCHSCREEN);
                }
                this.al = customViewCallback;
                return;
            }
            customViewCallback.onCustomViewHidden();
        }
    }

    public final void a(ValueCallback valueCallback) {
        this.am = valueCallback;
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.addCategory("android.intent.category.OPENABLE");
        intent.setType("*/*");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.title_file_chooser)), 1);
    }

    public final void a(ImageView imageView) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.v.getLayoutParams();
        if (imageView == null || imageView == this.ae) {
            this.ad = this.ae;
            this.ad.setVisibility(0);
            layoutParams.rightMargin = aq.a(96);
            this.v.setLayoutParams(layoutParams);
            N();
        } else {
            this.ae.setVisibility(8);
            this.ad = imageView;
            layoutParams.rightMargin = aq.a(40);
            this.v.setLayoutParams(layoutParams);
            N();
        }
        if (this.L != null) {
            b(0);
        }
    }

    public final void a(com.fastnet.browseralam.b.a aVar) {
        this.k = aVar;
    }

    public final void a(com.fastnet.browseralam.b.c cVar) {
        this.cl.add(cVar);
    }

    public final void a(m mVar) {
        this.N = mVar;
    }

    public final synchronized void a(XWebView xWebView) {
        if (this.aB == 0) {
            xWebView.p();
        }
        this.aB++;
        int h = this.L.h() + 1;
        if (xWebView.D()) {
            this.K.add(h, xWebView);
            a(this.K, h);
            this.O.d(h);
        } else {
            this.J.add(h, xWebView);
            a(this.J, h);
            this.N.d(h);
        }
        b(this.aD);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void */
    public final synchronized void a(XWebView xWebView, boolean z2) {
        if (xWebView != null) {
            if (this.L != null) {
                this.L.a(false);
                this.L.d();
            }
            this.L = xWebView;
            this.L.a(true);
            this.o.removeAllViews();
            this.o.addView(this.L.w(), be);
            this.o.addView(this.Z.c());
            if (this.L.w() != null) {
                a(this.L.y(), false);
                a(this.L.j());
            } else {
                a("", false);
                a(0);
            }
            if (this.L.D()) {
                P();
            } else {
                Q();
            }
            this.L.k();
            this.L.e();
            b(0);
            if (z2 && this.as) {
                this.cm.postDelayed(new bi(this), 150);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [java.lang.String, int, int, boolean]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    public final synchronized void a(String str) {
        a(str, false, this.L.h() + 1, this.L.D());
    }

    public final void a(String str, String str2) {
        if (str2 != null && !str2.startsWith("file://")) {
            if (this.aN == null) {
                this.aN = c.a(this.ao);
            }
            com.fastnet.browseralam.b.l.a(new bl(this, str, str2));
        }
    }

    public final void a(String str, boolean z2) {
        if (str != null && this.ac != null && !this.ac.hasFocus()) {
            if (str.startsWith("file://")) {
                if (!str.contains("history.html")) {
                    str = "";
                } else {
                    return;
                }
            } else if (z2) {
                switch (this.aF) {
                    case 0:
                        this.ac.setText(str);
                        return;
                    case 1:
                        if (this.L == null || this.L.x().isEmpty()) {
                            this.ac.setText(this.aK);
                            return;
                        } else {
                            this.ac.setText(this.L.x());
                            return;
                        }
                    case 2:
                        this.ac.setText(aq.a(str));
                        return;
                    default:
                        return;
                }
            }
            this.ac.setText(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
     arg types: [com.fastnet.browseralam.view.XWebView, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void */
    public final synchronized boolean a(String str, boolean z2, int i2, boolean z3) {
        XWebView xWebView = new XWebView(this.ao, str, z3, z2);
        xWebView.a(this.Z);
        if (this.aB == 0) {
            xWebView.p();
        }
        this.aB++;
        if (z3) {
            if (!this.L.D() || i2 < 0) {
                this.K.add(xWebView);
                i2 = this.K.size() - 1;
                a(this.K, i2);
            } else {
                this.K.add(i2, xWebView);
                a(this.K, i2);
            }
            this.O.d(i2);
        } else {
            if (i2 >= 0) {
                this.J.add(i2, xWebView);
                a(this.J, i2);
            } else {
                this.J.add(xWebView);
                i2 = this.J.size() - 1;
                a(this.J, i2);
            }
            this.N.d(i2);
        }
        if (!z2) {
            a(xWebView, true);
        } else {
            b(this.aD);
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(android.webkit.ValueCallback r9) {
        /*
            r8 = this;
            r7 = 1
            r6 = 0
            r1 = 0
            android.webkit.ValueCallback r0 = r8.an
            if (r0 == 0) goto L_0x000c
            android.webkit.ValueCallback r0 = r8.an
            r0.onReceiveValue(r1)
        L_0x000c:
            r8.an = r9
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r2 = "android.media.action.IMAGE_CAPTURE"
            r0.<init>(r2)
            android.app.Activity r2 = r8.ao
            android.content.pm.PackageManager r2 = r2.getPackageManager()
            android.content.ComponentName r2 = r0.resolveActivity(r2)
            if (r2 == 0) goto L_0x004c
            java.io.File r3 = com.fastnet.browseralam.i.aq.c()     // Catch:{ IOException -> 0x0082 }
            java.lang.String r2 = "PhotoPath"
            java.lang.String r4 = r8.aM     // Catch:{ IOException -> 0x0091 }
            r0.putExtra(r2, r4)     // Catch:{ IOException -> 0x0091 }
        L_0x002c:
            if (r3 == 0) goto L_0x008c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "file:"
            r1.<init>(r2)
            java.lang.String r2 = r3.getAbsolutePath()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r8.aM = r1
            java.lang.String r1 = "output"
            android.net.Uri r2 = android.net.Uri.fromFile(r3)
            r0.putExtra(r1, r2)
        L_0x004c:
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r1 = "android.intent.action.GET_CONTENT"
            r2.<init>(r1)
            java.lang.String r1 = "android.intent.category.OPENABLE"
            r2.addCategory(r1)
            java.lang.String r1 = "image/*"
            r2.setType(r1)
            if (r0 == 0) goto L_0x008e
            android.content.Intent[] r1 = new android.content.Intent[r7]
            r1[r6] = r0
            r0 = r1
        L_0x0064:
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.CHOOSER"
            r1.<init>(r3)
            java.lang.String r3 = "android.intent.extra.INTENT"
            r1.putExtra(r3, r2)
            java.lang.String r2 = "android.intent.extra.TITLE"
            java.lang.String r3 = "Image Chooser"
            r1.putExtra(r2, r3)
            java.lang.String r2 = "android.intent.extra.INITIAL_INTENTS"
            r1.putExtra(r2, r0)
            android.app.Activity r0 = r8.ao
            r0.startActivityForResult(r1, r7)
            return
        L_0x0082:
            r2 = move-exception
            r3 = r1
        L_0x0084:
            java.lang.String r4 = "Lightning"
            java.lang.String r5 = "Unable to create Image File"
            android.util.Log.e(r4, r5, r2)
            goto L_0x002c
        L_0x008c:
            r0 = r1
            goto L_0x004c
        L_0x008e:
            android.content.Intent[] r0 = new android.content.Intent[r6]
            goto L_0x0064
        L_0x0091:
            r2 = move-exception
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.activity.BrowserActivity.b(android.webkit.ValueCallback):void");
    }

    public final void b(m mVar) {
        this.O = mVar;
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        boolean z2 = false;
        if (!str.equals("")) {
            String str2 = this.aJ;
            String trim = str.trim();
            this.L.n();
            if (trim.startsWith("www.")) {
                trim = "http://" + trim;
            } else if (trim.startsWith("ftp.")) {
                trim = "ftp://" + trim;
            }
            boolean contains = trim.contains(".");
            boolean z3 = TextUtils.isDigitsOnly(trim.replace(".", "")) && trim.replace(".", "").length() >= 4 && trim.contains(".");
            boolean contains2 = trim.contains("about:");
            boolean z4 = trim.startsWith("ftp://") || trim.startsWith("http://") || trim.startsWith("file://") || trim.startsWith("https://") || z3;
            if ((trim.contains(" ") || !contains) && !contains2) {
                z2 = true;
            }
            if (z3 && (!trim.startsWith("http://") || !trim.startsWith("https://"))) {
                trim = "http://" + trim;
            }
            if (z2) {
                try {
                    trim = URLEncoder.encode(trim, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                this.L.c(str2 + trim);
            } else if (!z4) {
                this.L.c("http://" + trim);
            } else {
                this.L.c(trim);
            }
        }
    }

    public final void b(boolean z2) {
        if (!z2) {
            overridePendingTransition(0, 0);
        }
        ((ViewGroup) this.p.getParent()).removeView(this.p);
        this.bb.addView(this.p, 0, be);
    }

    public final void c(String str) {
        WebView.HitTestResult hitTestResult = null;
        if (this.L.w() != null) {
            hitTestResult = this.L.w().getHitTestResult();
        }
        if (str != null) {
            if (hitTestResult == null || hitTestResult.getExtra() == null || !(hitTestResult.getType() == 8 || hitTestResult.getType() == 5)) {
                e(str);
            } else {
                b(str, hitTestResult.getExtra());
            }
        } else if (hitTestResult != null && hitTestResult.getExtra() != null) {
            String extra = hitTestResult.getExtra();
            if (hitTestResult.getType() == 8 || hitTestResult.getType() == 5) {
                b(extra, extra);
            } else {
                e(extra);
            }
        }
    }

    public final RecyclerView f() {
        return this.r;
    }

    public void finish() {
        bc = null;
        super.finish();
    }

    public final RecyclerView g() {
        return this.s;
    }

    public synchronized void h() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [java.lang.String, int, int, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [?[OBJECT, ARRAY], int, int, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    public final void i() {
        String str;
        this.aB = 0;
        if (getIntent() != null) {
            str = getIntent().getDataString();
            if (str != null && str.startsWith("file://")) {
                aq.a(this, getResources().getString(R.string.message_blocked_local));
                str = null;
            }
        } else {
            str = null;
        }
        if (a.T()) {
            String G2 = a.G();
            a.e("");
            String[] b = aq.b(G2);
            int length = b.length;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                String str2 = b[i2];
                if (str2.length() > 0) {
                    if (str != null && str.compareTo(str2) == 0) {
                        str = null;
                    }
                    a(str2, false, -1, false);
                    i3++;
                }
                i2++;
                str = str;
                i3 = i3;
            }
            if (str != null) {
                a(str, false, -1, false);
            } else if (i3 == 0) {
                a((String) null, false, -1, false);
            }
        } else {
            a(str, false, -1, false);
        }
        this.N.b();
    }

    public void j() {
    }

    public final void k() {
        if (a.g() && this.L != null && !this.L.D()) {
            this.L.l();
        }
        if (a.i() && !this.L.D()) {
            l();
        }
        if (a.h() && !this.L.D()) {
            m();
        }
        this.L = null;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.J.size()) {
                if (this.J.get(i3) != null) {
                    ((XWebView) this.J.get(i3)).s();
                }
                i2 = i3 + 1;
            } else {
                this.J = new ArrayList();
                finish();
                return;
            }
        }
    }

    public final void l() {
        this.aO.c();
        c.b();
        WebViewDatabase instance = WebViewDatabase.getInstance(this);
        instance.clearFormData();
        instance.clearHttpAuthUsernamePassword();
        aq.a(this);
        R();
    }

    public final void m() {
        WebStorage.getInstance().deleteAllData();
        CookieManager instance = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT >= 21) {
            instance.removeAllCookies(null);
            return;
        }
        CookieSyncManager.createInstance(this);
        instance.removeAllCookie();
    }

    public final void n() {
        this.k = this.j;
    }

    public final boolean o() {
        return this.ap;
    }

    public void onActionModeFinished(ActionMode actionMode) {
        if (this.bS) {
            this.x.setTranslationY(0.0f);
            this.bS = false;
        }
        super.onActionModeFinished(actionMode);
    }

    public void onActionModeStarted(ActionMode actionMode) {
        if (this.ac.hasFocus()) {
            this.bS = true;
            this.x.setTranslationY((float) this.I);
        }
        super.onActionModeStarted(actionMode);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri[] uriArr;
        if (bd < 21 && i2 == 1) {
            if (this.am != null) {
                this.am.onReceiveValue((intent == null || i3 != -1) ? null : intent.getData());
                this.am = null;
            } else {
                return;
            }
        }
        if (i2 != 1 || this.an == null) {
            super.onActivityResult(i2, i3, intent);
            return;
        }
        if (i3 == -1) {
            if (intent != null) {
                String dataString = intent.getDataString();
                if (dataString != null) {
                    uriArr = new Uri[]{Uri.parse(dataString)};
                    this.an.onReceiveValue(uriArr);
                    this.an = null;
                }
            } else if (this.aM != null) {
                uriArr = new Uri[]{Uri.parse(this.aM)};
                this.an.onReceiveValue(uriArr);
                this.an = null;
            }
        }
        uriArr = null;
        this.an.onReceiveValue(uriArr);
        this.an = null;
    }

    public void onBackPressed() {
        this.k.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean
     arg types: [?[OBJECT, ARRAY], int, int, boolean]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.util.List, int, int):void
      android.support.v4.app.FragmentActivity.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.h.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean, int, boolean):boolean */
    public void onClick(View view) {
        boolean z2 = true;
        switch (view.getId()) {
            case R.id.new_tab_button:
                if (this.E.b() != 1) {
                    z2 = false;
                }
                a((String) null, false, -1, z2);
                return;
            case R.id.button_next:
                String obj = this.z.getText().toString();
                if (!obj.equals(this.D)) {
                    XWebView xWebView = this.L;
                    this.D = obj;
                    xWebView.a(obj);
                    return;
                }
                this.L.q();
                return;
            case R.id.button_back:
                String obj2 = this.z.getText().toString();
                if (!obj2.equals(this.D)) {
                    XWebView xWebView2 = this.L;
                    this.D = obj2;
                    xWebView2.a(obj2);
                    return;
                }
                this.L.r();
                return;
            case R.id.button_quit:
                this.L.w().clearMatches();
                ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.z.getWindowToken(), 0);
                this.p.removeView(this.w);
                return;
            case R.id.action_forward:
                this.L.u();
                return;
            case R.id.action_back:
                if (!this.L.t()) {
                    a(this.L.h(), this.L.D());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.cl.size() > 0) {
            ((com.fastnet.browseralam.b.c) this.cl.get(0)).a();
            ((com.fastnet.browseralam.b.c) this.cl.get(1)).a();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        M();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.aN != null) {
            this.aN.close();
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 66) {
            if (this.ac.hasFocus()) {
                b(this.ac.getText().toString());
            }
        } else if (i2 == 82 && Build.VERSION.SDK_INT <= 16 && Build.MANUFACTURER.compareTo("LGE") == 0) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyLongPress(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return true;
        }
        int h = this.L.h();
        boolean D2 = this.L.D();
        l lVar = new l(this.ao);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.ao, 17367050);
        arrayAdapter.add(this.ao.getString(R.string.close_tab));
        arrayAdapter.add(this.ao.getString(R.string.close_all_tabs));
        lVar.a(arrayAdapter, new bg(this, h, D2));
        lVar.d();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.ap = true;
        if (this.L != null && this.bg != 3) {
            this.L.o();
            this.L.d();
        } else if (!XWindow.i()) {
            bc = null;
        }
        if (this.X.c()) {
            com.fastnet.browseralam.b.l.a(new bk(this));
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.ap = false;
        bc = this;
        if (this.L != null) {
            this.L.p();
            this.L.e();
            this.aN = c.a(getApplicationContext());
            if (c.c()) {
                new com.fastnet.browseralam.i.c(this).a();
            }
            if (this.aP == null) {
                this.aP = this.aO.a();
            }
            if (this.L.F()) {
                this.aO.a(this.L);
            }
            if (this.aq) {
                p();
                b(true);
                XWindow.b(this, SpeedWindow.class);
            }
        }
        if (this.bp) {
            O();
            if (this.J != null) {
                for (int i2 = 0; i2 < this.J.size(); i2++) {
                    if (this.J.get(i2) != null) {
                        ((XWebView) this.J.get(i2)).b();
                    } else {
                        this.J.remove(i2);
                    }
                }
            }
            if (this.K != null) {
                for (int i3 = 0; i3 < this.K.size(); i3++) {
                    if (this.K.get(i3) != null) {
                        ((XWebView) this.K.get(i3)).b();
                    } else {
                        this.K.remove(i3);
                    }
                }
            }
            a();
        }
    }

    public void onTrimMemory(int i2) {
        if (i2 > 60 && Build.VERSION.SDK_INT < 19) {
            Log.d("Lightning", "Low Memory, Free Memory");
            for (XWebView w2 : this.J) {
                w2.w().freeMemory();
            }
        }
    }

    public final void p() {
        this.R.g();
        this.S.e();
    }

    public final void q() {
        this.ap = true;
        this.aq = true;
        if (this.L != null) {
            this.L.o();
            this.L.d();
        }
    }

    public final void r() {
        this.R.h();
        this.S.f();
    }

    public final void s() {
        if (a.T()) {
            String str = "";
            int i2 = 0;
            while (i2 < this.J.size()) {
                String str2 = ((XWebView) this.J.get(i2)).y() != null ? str + ((XWebView) this.J.get(i2)).y() + "|$|SEPARATOR|$|" : str;
                i2++;
                str = str2;
            }
            a.e(str);
        }
    }

    public final Bitmap t() {
        return this.aU;
    }

    public final void u() {
        if (this.L.F()) {
            this.o.removeView(this.aP);
            this.o.addView(this.aP);
            a(100);
            this.ac.setText("Speed://History");
        }
    }

    public final void v() {
        this.ac.setText("");
        this.o.removeView(this.aP);
    }

    public final void w() {
        Message obtainMessage = this.aj.obtainMessage();
        if (obtainMessage != null) {
            obtainMessage.setTarget(this.aj);
            this.L.w().requestFocusNodeHref(obtainMessage);
        }
    }

    public final void x() {
        if (this.ah != null && this.al != null && this.L != null) {
            this.L.c(0);
            try {
                this.ah.setKeepScreenOn(false);
            } catch (SecurityException e) {
            }
            c(a.s());
            FrameLayout frameLayout = (FrameLayout) getWindow().getDecorView();
            if (frameLayout != null) {
                frameLayout.removeView(this.q);
                frameLayout.setSystemUiVisibility(0);
            }
            if (bd < 19) {
                try {
                    this.al.onCustomViewHidden();
                } catch (Throwable th) {
                }
            }
            this.q = null;
            this.ah = null;
            if (this.ag != null) {
                this.ag.setOnErrorListener(null);
                this.ag.setOnCompletionListener(null);
                this.ag = null;
            }
            setRequestedOrientation(this.az);
        }
    }

    public final Bitmap y() {
        if (this.aT == null) {
            this.aT = BitmapFactory.decodeResource(getResources(), 17301540);
        }
        return this.aT;
    }

    public final View z() {
        if (this.ai == null) {
            this.ai = LayoutInflater.from(this).inflate((int) R.layout.video_loading_progress, (ViewGroup) null);
        }
        return this.ai;
    }
}
