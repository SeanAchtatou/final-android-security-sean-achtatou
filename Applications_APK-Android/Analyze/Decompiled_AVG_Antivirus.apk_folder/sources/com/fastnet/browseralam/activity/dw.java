package com.fastnet.browseralam.activity;

import android.view.GestureDetector;
import android.view.MotionEvent;

public final class dw extends GestureDetector.SimpleOnGestureListener {
    final /* synthetic */ BrowserActivity a;

    public dw(BrowserActivity browserActivity) {
        this.a = browserActivity;
    }

    private static boolean a(double d, float f, float f2) {
        return d >= ((double) f) && d < ((double) f2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
     arg types: [com.fastnet.browseralam.activity.BrowserActivity, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean */
    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        boolean unused = this.a.aw = true;
        float x = motionEvent.getX();
        double atan2 = ((((Math.atan2((double) (motionEvent.getY() - motionEvent2.getY()), (double) (motionEvent2.getX() - x)) + 3.141592653589793d) * 180.0d) / 3.141592653589793d) + 180.0d) % 360.0d;
        if (a(atan2, 45.0f, 135.0f)) {
            if (!this.a.L.D()) {
                this.a.a(this.a.J, -1, du.a);
                return false;
            }
            this.a.a(this.a.K, -1, du.a);
            return false;
        } else if (a(atan2, 0.0f, 45.0f) || a(atan2, 315.0f, 360.0f)) {
            if (!this.a.L.D()) {
                this.a.a(this.a.J, 1, du.d);
            }
            this.a.a(this.a.K, 1, du.d);
            return false;
        } else if (a(atan2, 225.0f, 315.0f)) {
            if (!this.a.L.D()) {
                this.a.a(this.a.J, 1, du.b);
            }
            this.a.a(this.a.K, 1, du.b);
            return false;
        } else if (!this.a.L.D()) {
            this.a.a(this.a.J, -1, du.c);
            return false;
        } else {
            this.a.a(this.a.K, -1, du.c);
            return false;
        }
    }
}
