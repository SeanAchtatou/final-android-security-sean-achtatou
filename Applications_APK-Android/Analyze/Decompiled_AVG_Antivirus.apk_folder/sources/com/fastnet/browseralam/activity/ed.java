package com.fastnet.browseralam.activity;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.view.MotionEvent;
import android.view.View;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;

public final class ed implements View.OnTouchListener {
    final /* synthetic */ dx a;

    public ed(dx dxVar) {
        this.a = dxVar;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.a.b.ac.getCompoundDrawables()[2] != null) {
            if (motionEvent.getX() > ((float) ((this.a.b.ac.getWidth() - this.a.b.ac.getPaddingRight()) - this.a.b.ba.getIntrinsicWidth()))) {
                if (motionEvent.getAction() == 1) {
                    if (this.a.b.ac.hasFocus()) {
                        ClipboardManager clipboardManager = (ClipboardManager) this.a.b.getSystemService("clipboard");
                        if (this.a.b.ba == this.a.b.aZ) {
                            if (clipboardManager.hasPrimaryClip() && clipboardManager.getPrimaryClipDescription().hasMimeType("text/plain")) {
                                this.a.b.ac.append(clipboardManager.getPrimaryClip().getItemAt(0).getText().toString());
                            }
                        } else if (this.a.b.ba == this.a.b.aY) {
                            clipboardManager.setPrimaryClip(ClipData.newPlainText("label", this.a.b.ac.getText().toString()));
                            aq.a(this.a.b.ao, this.a.b.ao.getResources().getString(R.string.message_text_copied));
                        }
                    } else {
                        this.a.b.H();
                    }
                }
                return true;
            }
        }
        return false;
    }
}
