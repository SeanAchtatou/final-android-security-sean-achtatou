package com.fastnet.browseralam.activity;

import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;

final class dy implements TextWatcher {
    final /* synthetic */ dx a;

    dy(dx dxVar) {
        this.a = dxVar;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (charSequence.toString().trim().length() == 0) {
            Drawable unused = this.a.b.ba = this.a.b.aZ;
            this.a.b.ac.setCompoundDrawables(null, null, this.a.b.aZ, null);
            return;
        }
        Drawable unused2 = this.a.b.ba = this.a.b.aY;
        this.a.b.ac.setCompoundDrawables(null, null, this.a.b.aY, null);
    }
}
