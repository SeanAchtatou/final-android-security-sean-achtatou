package com.fastnet.browseralam.activity;

import android.animation.Animator;
import com.fastnet.browseralam.e.a;

final class el extends a {
    boolean a;
    final /* synthetic */ ei b;

    el(ei eiVar) {
        this.b = eiVar;
    }

    public final void onAnimationCancel(Animator animator) {
        this.a = true;
        this.b.s.e_(8);
    }

    public final void onAnimationEnd(Animator animator) {
        if (this.a) {
            this.a = false;
            this.b.f.setVisibility(0);
            return;
        }
        this.b.L.d();
        this.b.g.setVisibility(0);
        this.b.f.setVisibility(8);
        this.b.s.e_(0);
    }
}
