package com.fastnet.browseralam.activity;

import android.view.View;

final class fv implements View.OnClickListener {
    final /* synthetic */ ReadingActivity a;

    fv(ReadingActivity readingActivity) {
        this.a = readingActivity;
    }

    public final void onClick(View view) {
        String obj = this.a.p.getText().toString();
        if (!obj.equals(this.a.q)) {
            this.a.n.findAllAsync(this.a.q = obj);
        } else {
            this.a.n.findNext(false);
        }
    }
}
