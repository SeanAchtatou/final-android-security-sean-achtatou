package com.fastnet.browseralam.activity;

import android.webkit.WebView;
import android.widget.FrameLayout;
import com.fastnet.browseralam.view.XWebView;

final class ev implements Runnable {
    final /* synthetic */ WebView a;
    final /* synthetic */ eu b;

    ev(eu euVar, WebView webView) {
        this.b = euVar;
        this.a = webView;
    }

    public final void run() {
        this.a.clearHistory();
        this.a.setVisibility(8);
        this.a.removeAllViews();
        this.a.destroyDrawingCache();
        this.a.destroy();
        this.b.a.a.u.removeAllViews();
        this.b.a.a.c.removeView(this.b.a.a.u);
        FrameLayout unused = this.b.a.a.u = null;
        for (XWebView xWebView : this.b.a.a.o) {
            if (xWebView.z().isEmpty() || xWebView.z().contains("homepage.html")) {
                xWebView.a(this.b.a.a.K);
            }
            if (!xWebView.i()) {
                xWebView.d();
            }
        }
        if (this.b.a.a.o.size() > 1) {
            this.b.a.a.m.c(this.b.a.a.o.size());
        }
    }
}
