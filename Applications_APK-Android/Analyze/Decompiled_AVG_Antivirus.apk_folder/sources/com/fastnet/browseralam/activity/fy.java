package com.fastnet.browseralam.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.reading.HtmlFetcher;
import com.fastnet.browseralam.reading.JResult;
import java.util.ArrayList;
import java.util.List;

final class fy extends AsyncTask {
    final /* synthetic */ ReadingActivity a;
    private final Context b;
    private ProgressDialog c;
    private String d;
    private List e;

    public fy(ReadingActivity readingActivity, Context context) {
        this.a = readingActivity;
        this.b = context;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Void doInBackground(String... strArr) {
        try {
            JResult a2 = new HtmlFetcher().a(strArr[0]);
            this.d = a2.f();
            this.e = a2.e();
            return null;
        } catch (Exception e2) {
            this.d = "";
            this.e = new ArrayList();
            e2.printStackTrace();
            return null;
        } catch (OutOfMemoryError e3) {
            System.gc();
            this.d = "";
            this.e = new ArrayList();
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        Void voidR = (Void) obj;
        this.c.dismiss();
        if (this.d.isEmpty() || this.e.isEmpty()) {
            this.a.a(this.a.getString(R.string.untitled), this.a.getString(R.string.loading_failed));
        } else {
            StringBuilder sb = new StringBuilder();
            for (String append : this.e) {
                sb.append(append).append("\n\n");
            }
            this.a.a(this.d, sb.toString());
        }
        super.onPostExecute(voidR);
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.c = new ProgressDialog(this.b);
        this.c.setProgressStyle(0);
        this.c.setCancelable(false);
        this.c.setIndeterminate(true);
        this.c.setMessage(this.b.getString(R.string.loading));
        this.c.show();
    }
}
