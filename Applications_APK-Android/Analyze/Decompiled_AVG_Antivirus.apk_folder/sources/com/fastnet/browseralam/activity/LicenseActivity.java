package com.fastnet.browseralam.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.settings.ThemableSettingsActivity;

public class LicenseActivity extends ThemableSettingsActivity implements View.OnClickListener {
    private void a(String str) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str), this, MainActivity.class));
        finish();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.browserLicense:
                a("http://www.mozilla.org/MPL/2.0/");
                return;
            case R.id.LightningSource:
                a("https://github.com/anthonycr/Lightning-Browser");
                return;
            case R.id.licenseAOSP:
                a("http://www.apache.org/licenses/LICENSE-2.0");
                return;
            case R.id.licenseHosts:
                a("http://hosts-file.net/");
                return;
            case R.id.licenseOrbot:
                a("http://www.gnu.org/licenses/lgpl.html");
                return;
            case R.id.licenseSnactory:
                a("http://www.apache.org/licenses/LICENSE-2.0");
                return;
            case R.id.licenseJsoup:
                a("http://jsoup.org/license");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.license_activity);
        a((Toolbar) findViewById(R.id.toolbar));
        c().a(true);
        findViewById(R.id.browserLicense).setOnClickListener(this);
        findViewById(R.id.LightningSource).setOnClickListener(this);
        findViewById(R.id.licenseAOSP).setOnClickListener(this);
        findViewById(R.id.licenseHosts).setOnClickListener(this);
        findViewById(R.id.licenseOrbot).setOnClickListener(this);
        findViewById(R.id.licenseSnactory).setOnClickListener(this);
        findViewById(R.id.licenseJsoup).setOnClickListener(this);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        finish();
        return super.onOptionsItemSelected(menuItem);
    }
}
