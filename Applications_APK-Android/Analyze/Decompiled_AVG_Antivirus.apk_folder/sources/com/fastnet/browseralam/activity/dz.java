package com.fastnet.browseralam.activity;

import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public final class dz implements TextView.OnEditorActionListener {
    final /* synthetic */ dx a;

    public dz(dx dxVar) {
        this.a = dxVar;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 2 && i != 6 && i != 5 && i != 4 && i != 3 && keyEvent.getAction() != 66) {
            return false;
        }
        ((InputMethodManager) this.a.b.getSystemService("input_method")).hideSoftInputFromWindow(this.a.b.ac.getWindowToken(), 0);
        this.a.b.b(this.a.b.ac.getText().toString());
        this.a.b.v.requestFocus();
        return true;
    }
}
