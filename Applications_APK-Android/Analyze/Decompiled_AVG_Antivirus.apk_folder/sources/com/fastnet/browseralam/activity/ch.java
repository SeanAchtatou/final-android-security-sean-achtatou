package com.fastnet.browseralam.activity;

import android.content.Intent;
import android.view.View;

final class ch implements View.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ BrowserActivity b;

    ch(BrowserActivity browserActivity, String str) {
        this.b = browserActivity;
        this.a = str;
    }

    public final void onClick(View view) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.addFlags(524288);
        intent.putExtra("android.intent.extra.SUBJECT", this.b.L.x());
        intent.putExtra("android.intent.extra.TEXT", this.a);
        this.b.startActivity(Intent.createChooser(intent, "Share link"));
        this.b.bs.dismiss();
    }
}
