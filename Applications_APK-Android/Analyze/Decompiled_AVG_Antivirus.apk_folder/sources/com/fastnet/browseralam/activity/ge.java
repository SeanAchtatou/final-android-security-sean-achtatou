package com.fastnet.browseralam.activity;

import android.view.View;
import com.fastnet.browseralam.R;

final class ge implements View.OnClickListener {
    final /* synthetic */ fz a;

    ge(fz fzVar) {
        this.a = fzVar;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.bookmarks_textview:
                if (this.a.r != this.a.s) {
                    this.a.g();
                    return;
                }
                return;
            case R.id.normal_tabs:
            case R.id.incognito_tabs:
            default:
                return;
            case R.id.files_textview:
                if (this.a.r != this.a.t) {
                    fz.e(this.a);
                    return;
                }
                return;
        }
    }
}
