package com.fastnet.browseralam.activity;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.fastnet.browseralam.a.b;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.view.XWebView;
import com.fastnet.browseralam.view.c;
import com.fastnet.browseralam.view.g;

final class es implements View.OnAttachStateChangeListener {
    final /* synthetic */ ei a;
    private g b = new eu(this);
    private final ViewGroup.LayoutParams c = new ViewGroup.LayoutParams(-1, -1);

    es(ei eiVar) {
        this.a = eiVar;
    }

    public final void onViewAttachedToWindow(View view) {
        if (this.a.o.size() > 0) {
            for (int size = this.a.o.size() - 2; size >= 0; size--) {
                ((XWebView) this.a.o.get(size)).e();
                this.a.u.addView(((XWebView) this.a.o.get(size)).w(), this.c);
            }
            WebView webView = new WebView(this.a.a);
            webView.setAnimationCacheEnabled(false);
            webView.setDrawingCacheEnabled(false);
            webView.setWillNotCacheDrawing(true);
            webView.loadUrl(b.a());
            webView.setWebChromeClient(new c(webView, this.b));
            this.a.u.addView(webView, this.c);
            this.a.d.removeOnAttachStateChangeListener(this);
            k.a(new et(this), 300);
        }
    }

    public final void onViewDetachedFromWindow(View view) {
    }
}
