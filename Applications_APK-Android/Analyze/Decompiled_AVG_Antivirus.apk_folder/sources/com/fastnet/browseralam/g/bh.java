package com.fastnet.browseralam.g;

import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.a.a;
import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.support.v7.widget.cz;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.b.c;
import com.fastnet.browseralam.e.q;
import com.fastnet.browseralam.e.s;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.XWebView;
import java.util.Collections;
import java.util.List;

public final class bh extends bi implements q, m {
    /* access modifiers changed from: private */
    public int A;
    /* access modifiers changed from: private */
    public int B;
    /* access modifiers changed from: private */
    public float C;
    private boolean D;
    /* access modifiers changed from: private */
    public boolean E;
    private View.OnClickListener F = new bi(this);
    private View.OnClickListener G = new bj(this);
    /* access modifiers changed from: private */
    public int H;
    /* access modifiers changed from: private */
    public c I = new bk(this);
    private RelativeLayout.LayoutParams J = new RelativeLayout.LayoutParams(-1, aq.a(42));
    private RelativeLayout.LayoutParams K = new RelativeLayout.LayoutParams(-1, aq.a(42));
    private RelativeLayout.LayoutParams L = new RelativeLayout.LayoutParams(-1, aq.a(42));
    /* access modifiers changed from: private */
    public final BrowserActivity a;
    private final LayoutInflater b;
    /* access modifiers changed from: private */
    public final RecyclerView c;
    /* access modifiers changed from: private */
    public final RecyclerView d;
    private final LinearLayoutManager e;
    /* access modifiers changed from: private */
    public final RelativeLayout f;
    /* access modifiers changed from: private */
    public final List g;
    private final bm h;
    private final bl i;
    private final boolean j;
    /* access modifiers changed from: private */
    public final ImageView k;
    /* access modifiers changed from: private */
    public final ImageView l;
    /* access modifiers changed from: private */
    public final ImageView m;
    /* access modifiers changed from: private */
    public final ImageView n;
    private final ImageView o;
    private final ImageView p;
    private final View q;
    /* access modifiers changed from: private */
    public int r;
    private int s = aq.a(42);
    private int t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public int x;
    /* access modifiers changed from: private */
    public int y;
    /* access modifiers changed from: private */
    public int z;

    public bh(BrowserActivity browserActivity, List list, RelativeLayout relativeLayout) {
        this.a = browserActivity;
        this.b = browserActivity.getLayoutInflater();
        this.f = relativeLayout;
        this.t = R.layout.top_tab_item;
        this.g = list;
        this.j = true;
        this.h = new bm(this, (byte) 0);
        this.i = new bl(this, (byte) 0);
        this.u = -12303292;
        this.v = Color.parseColor("#666666");
        this.c = (RecyclerView) relativeLayout.findViewById(R.id.incognito_list);
        this.d = (RecyclerView) relativeLayout.findViewById(R.id.normal_list);
        this.k = (ImageView) relativeLayout.findViewById(R.id.incognito_mode);
        this.l = (ImageView) relativeLayout.findViewById(R.id.normal_mode);
        this.k.setOnClickListener(this.G);
        this.c.a(this);
        this.c.setOverScrollMode(1);
        this.c.a(true);
        this.e = new LinearLayoutManager(0);
        this.c.a(this.e);
        this.c.i().h();
        this.c.i().e();
        ((cz) this.c.i()).k();
        new a(new s(this).h()).a(this.c);
        this.B = aq.a(42);
        this.A = aq.a(202);
        this.r = aq.a() - this.A;
        this.z = aq.a() - aq.a(160);
        int a2 = aq.a(200);
        this.w = a2;
        this.x = a2;
        this.y = aq.a(260);
        if (this.r > this.y * 2) {
            this.w = this.y;
        }
        this.H = this.a.getResources().getConfiguration().orientation;
        this.a.a(this.I);
        this.J.leftMargin = this.s;
        this.J.rightMargin = aq.a(160);
        this.K.rightMargin = aq.a(56);
        this.L.rightMargin = this.J.rightMargin;
        this.q = relativeLayout.findViewById(R.id.button_background);
        this.m = (ImageView) relativeLayout.findViewById(R.id.new_tab_button);
        this.n = (ImageView) relativeLayout.findViewById(R.id.new_tab_incognito);
        this.o = (ImageView) relativeLayout.findViewById(R.id.window_mode);
        this.p = (ImageView) relativeLayout.findViewById(R.id.quit);
        this.n.setOnClickListener(this.F);
    }

    public final int a() {
        return this.g.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final cf a(ViewGroup viewGroup, int i2) {
        return new bn(this, this.b.inflate(this.t, viewGroup, false));
    }

    public final void a(int i2, int i3) {
        b(i2);
    }

    public final void a(cf cfVar, int i2) {
        if (i2 != this.g.size()) {
            bn bnVar = (bn) cfVar;
            XWebView xWebView = (XWebView) this.g.get(i2);
            bnVar.l.setText(xWebView.x());
            bnVar.m.setImageBitmap(xWebView.C());
            if (xWebView.i()) {
                bnVar.l.setTextAppearance(this.a, R.style.boldText);
                bnVar.o.setBackgroundColor(this.u);
                bnVar.p = true;
            } else {
                bnVar.o.setBackgroundColor(this.v);
                bnVar.l.setTextAppearance(this.a, R.style.normalText);
                bnVar.p = false;
            }
            bnVar.o.setTag(bnVar);
            bnVar.o.setOnClickListener(this.h);
            bnVar.n.setOnClickListener(this.i);
            if (bnVar.a.getLayoutParams().width != this.w) {
                bnVar.a.getLayoutParams().width = this.w;
                bnVar.a.requestLayout();
            }
            if (this.D) {
                this.D = false;
                float size = ((float) (this.g.size() * this.w)) + ((float) this.B);
                if (size < ((float) this.z)) {
                    this.n.animate().translationX(size).setDuration(140);
                } else if (this.C < ((float) this.z)) {
                    this.n.animate().translationX((float) this.z).setDuration(140);
                }
                this.C = size;
            }
        }
    }

    public final void a_(int i2) {
        this.a.a(i2, this.j);
    }

    public final void b() {
        c();
    }

    public final boolean b(int i2, int i3) {
        Collections.swap(this.g, i2, i3);
        ((XWebView) this.g.get(i3)).a(i3);
        ((XWebView) this.g.get(i2)).a(i2);
        a_(i2, i3);
        return true;
    }

    public final void b_(int i2) {
        this.g.remove(i2);
        List list = this.g;
        int size = list.size();
        for (int i3 = i2; i3 < size; i3++) {
            ((XWebView) list.get(i3)).a(i3);
        }
        e(i2);
    }

    public final void d() {
        this.E = true;
        this.A = aq.a(56);
        this.o.setVisibility(8);
        this.p.setVisibility(8);
        this.q.getLayoutParams().width = this.K.rightMargin;
        this.c.setLayoutParams(this.K);
    }

    public final void d(int i2) {
        c_(i2);
        this.D = true;
        if (this.c.getVisibility() == 8) {
            this.c.setVisibility(0);
            this.d.setVisibility(8);
            if (this.g.size() == 1) {
                c();
                this.c.setLayoutParams(this.J);
                this.d.setLayoutParams(this.J);
                this.k.setVisibility(8);
                this.l.setVisibility(0);
                ((ar) this.d.a()).e();
                this.n.setVisibility(0);
                this.m.setVisibility(8);
            }
        }
        if (this.w * this.g.size() > this.r) {
            this.w = this.r / this.g.size();
            if (this.w < this.x) {
                this.w = this.x;
            }
            c(this.g.size());
        }
    }

    public final void e() {
        this.E = false;
        this.A = aq.a(160);
        this.c.setLayoutParams(this.L);
        this.q.getLayoutParams().width = this.L.rightMargin;
        this.o.setVisibility(0);
        this.p.setVisibility(0);
    }

    public final void e(int i2) {
        f(i2);
        if (this.g.size() == 0) {
            this.d.setLayoutParams(this.L);
            this.c.setLayoutParams(this.L);
            this.d.setVisibility(0);
            this.c.setVisibility(8);
            this.n.setVisibility(8);
            this.m.setVisibility(0);
            this.k.setVisibility(8);
            this.l.setVisibility(8);
            ((ar) this.d.a()).d();
        } else if (this.w * this.g.size() < this.r) {
            this.w = this.r / this.g.size();
            if (this.w > this.y) {
                this.w = this.y;
            }
            c(this.g.size());
            this.D = true;
        }
    }

    public final void f() {
        this.r = (this.f.getWidth() - this.A) - this.B;
        this.z = this.f.getWidth() - this.A;
        if (this.g.size() != 0) {
            int size = this.w * this.g.size();
            if (size > this.r) {
                int size2 = this.r / this.g.size();
                if (size2 < this.x) {
                    size2 = this.x;
                }
                if (this.w != size2) {
                    this.w = size2;
                    c(this.g.size());
                }
            } else if (size < this.r) {
                int size3 = this.r / this.g.size();
                if (size3 > this.y) {
                    size3 = this.y;
                }
                if (this.w != size3) {
                    this.w = size3;
                    c(this.g.size());
                }
            }
            this.C = (float) ((this.g.size() * this.w) + this.B);
            if (this.C < ((float) this.z)) {
                this.n.setTranslationX(this.C);
            } else {
                this.n.setTranslationX((float) this.z);
            }
        }
    }
}
