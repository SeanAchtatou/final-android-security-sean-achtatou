package com.fastnet.browseralam.g;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.bu;
import com.fastnet.browseralam.view.XWebView;

final class h extends bu {
    int a;
    final /* synthetic */ a b;

    private h(a aVar) {
        this.b = aVar;
        this.a = 0;
    }

    /* synthetic */ h(a aVar, byte b2) {
        this(aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.g.a.a(com.fastnet.browseralam.g.a, boolean):boolean
     arg types: [com.fastnet.browseralam.g.a, int]
     candidates:
      com.fastnet.browseralam.g.a.a(com.fastnet.browseralam.g.a, int):int
      com.fastnet.browseralam.g.a.a(com.fastnet.browseralam.g.a, android.view.View):android.view.View
      com.fastnet.browseralam.g.a.a(android.view.ViewGroup, int):android.support.v7.widget.cf
      com.fastnet.browseralam.g.a.a(int, int):void
      com.fastnet.browseralam.g.a.a(android.support.v7.widget.cf, int):void
      android.support.v7.widget.bi.a(android.view.ViewGroup, int):android.support.v7.widget.cf
      android.support.v7.widget.bi.a(android.support.v7.widget.cf, int):void
      com.fastnet.browseralam.g.m.a(int, int):void
      com.fastnet.browseralam.g.a.a(com.fastnet.browseralam.g.a, boolean):boolean */
    public final void a(RecyclerView recyclerView, int i) {
        d dVar;
        super.a(recyclerView, i);
        if (i == 0 && this.b.D && (dVar = (d) this.b.b.c(this.b.B)) != null) {
            this.a = 0;
            int x = ((int) dVar.l.getX()) + this.b.s;
            if (x < this.b.t) {
                this.a = -(this.b.t - x);
            } else if (x > this.b.t) {
                this.a = x - this.b.t;
            }
            this.b.b.a(this.a, 0);
            boolean unused = this.b.D = false;
        }
    }

    public final void a(RecyclerView recyclerView, int i, int i2) {
        super.a(recyclerView, i, i2);
        int unused = this.b.w = this.b.w + i;
        if (this.b.w < 0) {
            int unused2 = this.b.w = 0;
        }
        if (this.b.w > this.b.z) {
            int unused3 = this.b.z = this.b.z + this.b.p;
            int unused4 = this.b.A = this.b.A + this.b.p;
            a.n(this.b);
            if (this.b.B > this.b.f.size() - 1) {
                int unused5 = this.b.B = this.b.f.size() - 1;
            }
            this.b.d.setText(((XWebView) this.b.f.get(this.b.B)).x());
        } else if (this.b.w < this.b.A) {
            int unused6 = this.b.A = this.b.A - this.b.p;
            int unused7 = this.b.z = this.b.z - this.b.p;
            if (this.b.B != 0) {
                a.q(this.b);
                this.b.d.setText(((XWebView) this.b.f.get(this.b.B)).x());
            }
        }
    }
}
