package com.fastnet.browseralam.g;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.bu;
import com.fastnet.browseralam.view.XWebView;

final class aq extends bu {
    final /* synthetic */ aj a;

    private aq(aj ajVar) {
        this.a = ajVar;
    }

    /* synthetic */ aq(aj ajVar, byte b) {
        this(ajVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.g.aj.a(com.fastnet.browseralam.g.aj, boolean):boolean
     arg types: [com.fastnet.browseralam.g.aj, int]
     candidates:
      com.fastnet.browseralam.g.aj.a(com.fastnet.browseralam.g.aj, android.widget.FrameLayout):android.widget.FrameLayout
      com.fastnet.browseralam.g.aj.a(com.fastnet.browseralam.g.aj, com.fastnet.browseralam.g.ap):com.fastnet.browseralam.g.ap
      com.fastnet.browseralam.g.aj.a(com.fastnet.browseralam.g.aj, int):void
      com.fastnet.browseralam.g.aj.a(android.view.ViewGroup, int):android.support.v7.widget.cf
      com.fastnet.browseralam.g.aj.a(int, int):void
      com.fastnet.browseralam.g.aj.a(android.support.v7.widget.cf, int):void
      android.support.v7.widget.bi.a(android.view.ViewGroup, int):android.support.v7.widget.cf
      android.support.v7.widget.bi.a(android.support.v7.widget.cf, int):void
      com.fastnet.browseralam.g.m.a(int, int):void
      com.fastnet.browseralam.g.aj.a(com.fastnet.browseralam.g.aj, boolean):boolean */
    public final void a(RecyclerView recyclerView, int i) {
        super.a(recyclerView, i);
        if (i == 0 && this.a.L) {
            ap unused = this.a.A = (ap) this.a.b.c(this.a.K);
            if (this.a.A != null) {
                int x = ((int) this.a.A.m.getX()) + this.a.p;
                int k = x < this.a.r ? -(this.a.r - x) : x > this.a.r ? x - this.a.r : 0;
                if (k != 0) {
                    this.a.b.a(k, 0);
                }
                boolean unused2 = this.a.L = false;
            }
        }
    }

    public final void a(RecyclerView recyclerView, int i, int i2) {
        super.a(recyclerView, i, i2);
        int unused = this.a.F = this.a.F + i;
        if (this.a.F < 0) {
            int unused2 = this.a.F = 0;
        }
        if (this.a.F == 0) {
            int unused3 = this.a.I = this.a.G;
            int unused4 = this.a.J = this.a.H;
        }
        if (this.a.F > this.a.I) {
            int unused5 = this.a.I = this.a.I + this.a.o;
            int unused6 = this.a.J = this.a.J + this.a.o;
            aj.w(this.a);
            if (this.a.K > this.a.g.size() - 1) {
                int unused7 = this.a.K = this.a.g.size() - 1;
            }
            this.a.e.setText(((XWebView) this.a.g.get(this.a.K)).x());
        } else if (this.a.F < this.a.J) {
            int unused8 = this.a.J = this.a.J - this.a.o;
            int unused9 = this.a.I = this.a.I - this.a.o;
            if (this.a.K != 0) {
                aj.z(this.a);
                this.a.e.setText(((XWebView) this.a.g.get(this.a.K)).x());
            }
        }
    }
}
