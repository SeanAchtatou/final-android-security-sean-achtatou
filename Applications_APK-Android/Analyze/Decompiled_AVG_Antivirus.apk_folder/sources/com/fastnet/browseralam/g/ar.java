package com.fastnet.browseralam.g;

import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.a.a;
import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.support.v7.widget.cz;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.b.c;
import com.fastnet.browseralam.e.q;
import com.fastnet.browseralam.e.s;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.XWebView;
import java.util.Collections;
import java.util.List;

public final class ar extends bi implements q, m {
    /* access modifiers changed from: private */
    public int A;
    /* access modifiers changed from: private */
    public int B;
    /* access modifiers changed from: private */
    public float C;
    private boolean D;
    /* access modifiers changed from: private */
    public boolean E;
    private View.OnClickListener F = new as(this);
    private View.OnClickListener G = new at(this);
    private View.OnClickListener H = new au(this);
    /* access modifiers changed from: private */
    public int I;
    /* access modifiers changed from: private */
    public c J = new av(this);
    private RelativeLayout.LayoutParams K = new RelativeLayout.LayoutParams(-1, aq.a(42));
    private RelativeLayout.LayoutParams L = new RelativeLayout.LayoutParams(-1, aq.a(42));
    private RelativeLayout.LayoutParams M = new RelativeLayout.LayoutParams(-1, aq.a(42));
    /* access modifiers changed from: private */
    public final BrowserActivity a;
    private final LayoutInflater b;
    /* access modifiers changed from: private */
    public final RecyclerView c;
    /* access modifiers changed from: private */
    public final RecyclerView d;
    private final LinearLayoutManager e;
    /* access modifiers changed from: private */
    public final RelativeLayout f;
    /* access modifiers changed from: private */
    public final List g;
    private final ax h;
    private final aw i;
    private final boolean j = false;
    /* access modifiers changed from: private */
    public final ImageView k;
    /* access modifiers changed from: private */
    public final ImageView l;
    /* access modifiers changed from: private */
    public final ImageView m;
    /* access modifiers changed from: private */
    public final ImageView n;
    private final ImageView o;
    private final ImageView p;
    private final View q;
    /* access modifiers changed from: private */
    public int r;
    private int s = aq.a(42);
    private int t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public int x;
    /* access modifiers changed from: private */
    public int y;
    /* access modifiers changed from: private */
    public int z;

    public ar(BrowserActivity browserActivity, List list, RelativeLayout relativeLayout) {
        this.a = browserActivity;
        this.b = browserActivity.getLayoutInflater();
        this.f = relativeLayout;
        this.t = R.layout.top_tab_item;
        this.g = list;
        this.h = new ax(this, (byte) 0);
        this.i = new aw(this, (byte) 0);
        if (browserActivity.A()) {
            this.u = -12303292;
        } else {
            this.u = Color.parseColor("#F1F1F1");
        }
        this.v = Color.parseColor("#CDCDCD");
        this.c = (RecyclerView) relativeLayout.findViewById(R.id.normal_list);
        this.d = (RecyclerView) relativeLayout.findViewById(R.id.incognito_list);
        this.k = (ImageView) relativeLayout.findViewById(R.id.normal_mode);
        this.l = (ImageView) relativeLayout.findViewById(R.id.incognito_mode);
        this.k.setOnClickListener(this.H);
        this.c.a(this);
        this.c.setOverScrollMode(1);
        this.c.a(false);
        this.e = new LinearLayoutManager(0);
        this.c.a(this.e);
        this.c.i().h();
        this.c.i().e();
        ((cz) this.c.i()).k();
        new a(new s(this).h()).a(this.c);
        this.B = aq.a(160);
        int a2 = aq.a() - this.B;
        this.r = a2;
        this.x = a2;
        this.C = (float) a2;
        int a3 = aq.a(200);
        this.w = a3;
        this.y = a3;
        this.z = aq.a(260);
        if (this.r > this.z * 2) {
            this.w = this.z;
        }
        this.I = this.a.getResources().getConfiguration().orientation;
        this.a.a(this.J);
        this.K.leftMargin = this.s;
        this.L.rightMargin = aq.a(56);
        this.M.rightMargin = aq.a(160);
        this.q = relativeLayout.findViewById(R.id.button_background);
        this.n = (ImageView) relativeLayout.findViewById(R.id.new_tab_button);
        this.m = (ImageView) relativeLayout.findViewById(R.id.new_tab_incognito);
        this.o = (ImageView) relativeLayout.findViewById(R.id.window_mode);
        this.p = (ImageView) relativeLayout.findViewById(R.id.quit);
        this.n.setTranslationX((float) this.w);
        this.n.setOnClickListener(this.F);
    }

    public final int a() {
        return this.g.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final cf a(ViewGroup viewGroup, int i2) {
        return new ay(this, this.b.inflate(this.t, viewGroup, false));
    }

    public final void a(int i2, int i3) {
        b(i2);
    }

    public final void a(cf cfVar, int i2) {
        if (i2 != this.g.size()) {
            ay ayVar = (ay) cfVar;
            XWebView xWebView = (XWebView) this.g.get(i2);
            ayVar.l.setText(xWebView.x());
            ayVar.m.setImageBitmap(xWebView.C());
            if (xWebView.i()) {
                ayVar.l.setTextAppearance(this.a, R.style.boldText);
                ayVar.p.setBackgroundColor(this.u);
                ayVar.o.setVisibility(8);
                ayVar.q = true;
            } else {
                ayVar.p.setBackgroundColor(this.v);
                ayVar.l.setTextAppearance(this.a, R.style.normalText);
                ayVar.o.setVisibility(0);
                ayVar.q = false;
            }
            ayVar.p.setTag(ayVar);
            ayVar.p.setOnClickListener(this.h);
            ayVar.n.setOnClickListener(this.i);
            if (ayVar.a.getLayoutParams().width != this.w) {
                ayVar.a.getLayoutParams().width = this.w;
                ayVar.a.requestLayout();
            }
            if (this.D) {
                this.D = false;
                float size = ((float) (this.g.size() * this.w)) + ((float) this.A);
                if (size < ((float) this.x)) {
                    this.n.animate().translationX(size).setDuration(140);
                } else if (this.C < ((float) this.x)) {
                    this.n.animate().translationX((float) this.x).setDuration(140);
                }
                this.C = size;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void */
    public final void a_(int i2) {
        this.a.a(i2, false);
    }

    public final void b() {
        this.D = true;
        c();
    }

    public final boolean b(int i2, int i3) {
        Collections.swap(this.g, i2, i3);
        ((XWebView) this.g.get(i3)).a(i3);
        ((XWebView) this.g.get(i2)).a(i2);
        a_(i2, i3);
        return true;
    }

    public final void b_(int i2) {
        this.g.remove(i2);
        List list = this.g;
        int size = list.size();
        for (int i3 = i2; i3 < size; i3++) {
            ((XWebView) list.get(i3)).a(i3);
        }
        d_(i2);
        if (this.g.size() != 0 && this.w * this.g.size() < this.r) {
            this.w = this.r / this.g.size();
            if (this.w > this.z) {
                this.w = this.z;
            }
            c(this.g.size());
            this.D = true;
        }
    }

    public final void d() {
        this.A = 0;
        this.r = this.f.getWidth() - aq.a(160);
        this.C = ((float) (this.g.size() * this.w)) + ((float) this.A);
        if (this.C < ((float) this.x)) {
            this.n.setTranslationX(this.C);
        } else {
            this.n.setTranslationX((float) this.x);
        }
    }

    public final void d(int i2) {
        c_(i2);
        this.D = true;
        if (this.w * this.g.size() > this.r) {
            this.w = this.r / this.g.size();
            if (this.w < this.y) {
                this.w = this.y;
            }
            c(this.g.size());
        }
        if (this.c.getVisibility() == 8) {
            this.c.setVisibility(0);
            this.d.setVisibility(8);
        }
    }

    public final void e() {
        this.A = aq.a(42);
        this.r -= this.A;
        this.C = ((float) (this.g.size() * this.w)) + ((float) this.A);
        if (this.C < ((float) this.x)) {
            this.n.setTranslationX(this.C);
        } else {
            this.n.setTranslationX((float) this.x);
        }
    }

    public final void e(int i2) {
        f(i2);
    }

    public final void f() {
        this.E = true;
        this.B = aq.a(56);
        this.o.setVisibility(8);
        this.p.setVisibility(8);
        this.q.getLayoutParams().width = this.L.rightMargin;
        this.c.setLayoutParams(this.L);
    }

    public final void g() {
        this.E = false;
        this.I = 0;
        this.J.a();
        this.c.setLayoutParams(this.M);
        this.q.getLayoutParams().width = this.M.rightMargin;
        this.o.setVisibility(0);
        this.p.setVisibility(0);
    }

    public final void h() {
        this.r = (this.f.getWidth() - this.B) - this.A;
        this.x = this.f.getWidth() - this.B;
        int size = this.w * this.g.size();
        if (size > this.r) {
            int size2 = this.r / this.g.size();
            if (size2 < this.y) {
                size2 = this.y;
            }
            if (this.w != size2) {
                this.w = size2;
                c(this.g.size());
            }
        } else if (size < this.r) {
            int size3 = this.r / this.g.size();
            if (size3 > this.z) {
                size3 = this.z;
            }
            if (this.w != size3) {
                this.w = size3;
                c(this.g.size());
            }
        }
        this.C = (float) ((this.g.size() * this.w) + this.A);
        if (this.C < ((float) this.x)) {
            this.n.setTranslationX(this.C);
        } else {
            this.n.setTranslationX((float) this.x);
        }
    }
}
