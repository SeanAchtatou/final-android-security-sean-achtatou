package com.fastnet.browseralam.g;

import android.support.v7.widget.cf;
import android.view.View;
import android.widget.ImageView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.e.r;

public final class d extends cf implements r {
    final ImageView l;
    boolean m = false;
    final /* synthetic */ a n;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(a aVar, View view) {
        super(view);
        this.n = aVar;
        this.l = (ImageView) view.findViewById(R.id.card);
        this.l.setOnClickListener(aVar.k);
    }

    public final void b() {
        this.l.animate().scaleY(1.0f).scaleX(1.0f).setDuration(30).start();
    }

    public final int c() {
        return d();
    }

    public final void c_() {
        this.l.animate().scaleY(1.04f).scaleX(1.04f).setDuration(30).start();
    }
}
