package com.fastnet.browseralam.g;

import android.support.v7.widget.bl;

final class ao implements bl {
    final /* synthetic */ aj a;
    private int b;
    private int c;

    ao(aj ajVar) {
        this.a = ajVar;
    }

    public final int a(int i, int i2) {
        if (!this.a.w) {
            return i2;
        }
        if (i2 == 0) {
            this.b = 0;
            this.c = this.a.b.indexOfChild(this.a.y);
        }
        if (i2 == i - 1) {
            return this.c != -1 ? this.c : i2;
        }
        if (this.b == this.c) {
            this.b++;
        }
        if (this.b == i) {
            return i2;
        }
        int i3 = this.b;
        this.b = i3 + 1;
        return i3;
    }
}
