package com.fastnet.browseralam.g;

import android.graphics.PorterDuff;
import android.support.v7.widget.cf;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.e.r;

public final class ai extends cf implements r {
    final TextView l;
    final ImageView m;
    final ImageView n;
    final RelativeLayout o;
    boolean p = false;
    final /* synthetic */ af q;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ai(af afVar, View view) {
        super(view);
        this.q = afVar;
        this.l = (TextView) view.findViewById(R.id.textTab);
        this.m = (ImageView) view.findViewById(R.id.faviconTab);
        this.n = (ImageView) view.findViewById(R.id.delete_tab);
        this.o = (RelativeLayout) view.findViewById(R.id.tabItem);
        this.n.setColorFilter(R.color.gray_extra_dark, PorterDuff.Mode.SRC_IN);
    }

    public final void b() {
        if (this.p) {
            this.o.setBackgroundColor(this.q.l);
        } else {
            this.o.setBackgroundColor(0);
        }
    }

    public final int c() {
        return d();
    }

    public final void c_() {
        this.o.setBackgroundColor(-3355444);
    }
}
