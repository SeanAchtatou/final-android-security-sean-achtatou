package com.fastnet.browseralam.g;

import android.widget.Filter;
import com.fastnet.browseralam.c.c;
import com.fastnet.browseralam.c.g;
import java.util.List;
import java.util.Locale;

public final class ad extends Filter {
    final /* synthetic */ x a;

    public ad(x xVar) {
        this.a = xVar;
    }

    public final CharSequence convertResultToString(Object obj) {
        return ((g) obj).e();
    }

    /* access modifiers changed from: protected */
    public final Filter.FilterResults performFiltering(CharSequence charSequence) {
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (charSequence != null) {
            String lowerCase = charSequence.toString().toLowerCase(Locale.getDefault());
            if (this.a.k && !this.a.l) {
                x.a(this.a, lowerCase);
            }
            x xVar = this.a;
            c unused = this.a.e;
            List unused2 = xVar.a = c.b(charSequence.toString());
            filterResults.count = 1;
        }
        return filterResults;
    }

    /* access modifiers changed from: protected */
    public final void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        List unused = this.a.d = this.a.b();
        this.a.notifyDataSetChanged();
    }
}
