package com.fastnet.browseralam.g;

import android.graphics.PorterDuff;
import android.support.v7.widget.cf;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.e.r;

public final class ay extends cf implements r {
    final TextView l;
    final ImageView m;
    final ImageView n;
    final View o;
    final RelativeLayout p;
    boolean q = false;
    final /* synthetic */ ar r;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ay(ar arVar, View view) {
        super(view);
        this.r = arVar;
        this.l = (TextView) view.findViewById(R.id.textTab);
        this.m = (ImageView) view.findViewById(R.id.faviconTab);
        this.n = (ImageView) view.findViewById(R.id.delete_tab);
        this.p = (RelativeLayout) view.findViewById(R.id.tabItem);
        this.o = view.findViewById(R.id.border);
        this.n.setColorFilter(R.color.gray_extra_dark, PorterDuff.Mode.SRC_IN);
    }

    public final void b() {
        if (this.q) {
            this.p.setBackgroundColor(this.r.u);
        } else {
            this.p.setBackgroundColor(this.r.v);
        }
    }

    public final int c() {
        return d();
    }

    public final void c_() {
        this.p.setBackgroundColor(-3355444);
    }
}
