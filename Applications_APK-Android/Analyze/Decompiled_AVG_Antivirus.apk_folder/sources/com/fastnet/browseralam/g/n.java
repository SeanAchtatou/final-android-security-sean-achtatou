package com.fastnet.browseralam.g;

import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.a.a;
import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.support.v7.widget.cz;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.activity.y;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.e.q;
import com.fastnet.browseralam.e.s;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.XWebView;
import java.util.Collections;
import java.util.List;

public final class n extends bi implements q, m {
    /* access modifiers changed from: private */
    public final BrowserActivity a;
    private final RecyclerView b;
    private final int c;
    private final t d;
    private final v e;
    /* access modifiers changed from: private */
    public final List f;
    /* access modifiers changed from: private */
    public final boolean g;
    /* access modifiers changed from: private */
    public y h;
    /* access modifiers changed from: private */
    public View i;
    private float j;
    /* access modifiers changed from: private */
    public float k;
    private float l;
    private float m;
    private int n = aq.a(54);
    /* access modifiers changed from: private */
    public Handler o = k.a();
    /* access modifiers changed from: private */
    public s p = new s(this, (byte) 0);
    /* access modifiers changed from: private */
    public Runnable q = new o(this);
    private Runnable r = new p(this);
    private Runnable s = new q(this);

    public n(BrowserActivity browserActivity, RecyclerView recyclerView, List list, boolean z, View view) {
        this.a = browserActivity;
        this.b = recyclerView;
        this.c = R.layout.bottom_tab_item;
        this.f = list;
        this.g = z;
        this.d = new t(this, (byte) 0);
        this.e = new v(this, (byte) 0);
        this.i = view;
        this.k = view.getTranslationY();
        this.l = (float) aq.a(12);
        this.j = this.k;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        linearLayoutManager.a(true);
        this.b.a(linearLayoutManager);
        this.b.a(this);
        this.b.setOverScrollMode(1);
        this.b.a(true);
        this.b.a(new r(this, (byte) 0));
        this.b.i().h();
        this.b.i().e();
        ((cz) this.b.i()).k();
        new a(new s(this)).a(this.b);
    }

    public final int a() {
        if (this.f != null) {
            return this.f.size();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ cf a(ViewGroup viewGroup, int i2) {
        return new w(this, LayoutInflater.from(viewGroup.getContext()).inflate(this.c, viewGroup, false));
    }

    public final void a(int i2, int i3) {
        b(i2);
    }

    public final void a(cf cfVar, int i2) {
        w wVar = (w) cfVar;
        XWebView xWebView = (XWebView) this.f.get(i2);
        wVar.l.setText(xWebView.x());
        wVar.m.setImageBitmap(xWebView.C());
        if (xWebView.i()) {
            wVar.l.setTextAppearance(this.a, R.style.boldText);
            wVar.o.setBackgroundColor(-3355444);
            wVar.q = true;
        } else {
            wVar.o.setBackground(wVar.p);
            wVar.l.setTextAppearance(this.a, R.style.normalText);
            wVar.q = false;
        }
        wVar.o.setTag(wVar);
        wVar.o.setOnClickListener(this.d);
        wVar.n.setOnClickListener(this.e);
    }

    public final void a(y yVar) {
        this.h = yVar;
    }

    public final void a_(int i2) {
        this.a.a(i2, this.g);
    }

    public final void b() {
        c();
    }

    public final boolean b(int i2, int i3) {
        Collections.swap(this.f, i2, i3);
        ((XWebView) this.f.get(i3)).a(i3);
        ((XWebView) this.f.get(i2)).a(i2);
        a_(i2, i3);
        return true;
    }

    public final void b_(int i2) {
        if (this.m > 0.0f) {
            this.m -= (float) this.n;
        } else {
            this.k += (float) this.n;
        }
        this.f.remove(i2);
        List list = this.f;
        int size = list.size();
        for (int i3 = i2; i3 < size; i3++) {
            ((XWebView) list.get(i3)).a(i3);
        }
        d_(i2);
    }

    public final float d() {
        this.k = this.j - ((float) (this.f.size() * this.n));
        if (this.k < 0.0f) {
            this.k = 0.0f;
        }
        return this.k;
    }

    public final void d(int i2) {
        if (this.k - ((float) this.n) >= 0.0f) {
            this.k -= (float) this.n;
        } else {
            this.k = 0.0f;
            this.m += (float) this.n;
        }
        this.o.post(this.r);
        c_(i2);
        if (this.h.b(this.g)) {
            this.o.postDelayed(this.s, 150);
        }
    }

    public final void e(int i2) {
        if (this.m > 0.0f) {
            this.m -= (float) this.n;
        } else {
            this.k += (float) this.n;
        }
        f(i2);
    }
}
