package com.fastnet.browseralam.g;

import android.view.View;
import android.widget.FrameLayout;
import com.fastnet.browseralam.view.XWebView;

final class bb implements View.OnClickListener {
    final /* synthetic */ az a;

    bb(az azVar) {
        this.a = azVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.g.az.a(com.fastnet.browseralam.g.az, boolean):boolean
     arg types: [com.fastnet.browseralam.g.az, int]
     candidates:
      com.fastnet.browseralam.g.az.a(com.fastnet.browseralam.g.az, android.widget.FrameLayout):android.widget.FrameLayout
      com.fastnet.browseralam.g.az.a(com.fastnet.browseralam.g.az, com.fastnet.browseralam.g.bf):com.fastnet.browseralam.g.bf
      com.fastnet.browseralam.g.az.a(com.fastnet.browseralam.g.az, int):void
      com.fastnet.browseralam.g.az.a(android.view.ViewGroup, int):android.support.v7.widget.cf
      com.fastnet.browseralam.g.az.a(int, int):void
      com.fastnet.browseralam.g.az.a(android.support.v7.widget.cf, int):void
      android.support.v7.widget.bi.a(android.view.ViewGroup, int):android.support.v7.widget.cf
      android.support.v7.widget.bi.a(android.support.v7.widget.cf, int):void
      com.fastnet.browseralam.g.m.a(int, int):void
      com.fastnet.browseralam.g.az.a(com.fastnet.browseralam.g.az, boolean):boolean */
    public final void onClick(View view) {
        if (!this.a.x && !this.a.k.b()) {
            boolean unused = this.a.x = true;
            this.a.k.a(false);
            FrameLayout unused2 = this.a.y = (FrameLayout) view;
            int unused3 = this.a.u = this.a.t;
            bf unused4 = this.a.z = (bf) this.a.y.getTag();
            FrameLayout unused5 = this.a.y = this.a.z.m;
            int unused6 = this.a.t = this.a.z.d();
            int x = ((int) this.a.y.getX()) + this.a.p;
            int k = x < this.a.r ? -(this.a.r - x) : x > this.a.r ? x - this.a.r : 0;
            boolean unused7 = this.a.w = true;
            boolean unused8 = this.a.L = false;
            this.a.b.a(k, 0);
            int i = k != 0 ? 50 : k;
            this.a.z.n.setVisibility(8);
            this.a.i.a((XWebView) this.a.g.get(this.a.t), this.a.y, i);
        }
    }
}
