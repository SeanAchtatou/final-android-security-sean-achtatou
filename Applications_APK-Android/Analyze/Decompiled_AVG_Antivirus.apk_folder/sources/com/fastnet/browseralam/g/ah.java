package com.fastnet.browseralam.g;

import android.view.View;
import com.fastnet.browseralam.e.r;
import com.fastnet.browseralam.view.XWebView;

final class ah implements View.OnClickListener {
    final /* synthetic */ af a;

    private ah(af afVar) {
        this.a = afVar;
    }

    /* synthetic */ ah(af afVar, byte b) {
        this(afVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
     arg types: [com.fastnet.browseralam.view.XWebView, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void */
    public final void onClick(View view) {
        int c = ((r) view.getTag()).c();
        if (c < this.a.f.size() && this.a.a.D() != this.a.f.get(c)) {
            this.a.a.a((XWebView) this.a.f.get(c), true);
        }
    }
}
