package com.fastnet.browseralam.g;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.fastnet.browseralam.R;

final class ae {
    TextView a;
    TextView b;
    ImageView c;
    final /* synthetic */ x d;

    public ae(x xVar, View view) {
        this.d = xVar;
        this.a = (TextView) view.findViewById(R.id.title);
        this.b = (TextView) view.findViewById(R.id.url);
        this.c = (ImageView) view.findViewById(R.id.select_text);
        if (xVar.j) {
            this.a.setTextColor(-1);
        }
        view.setTag(this);
        this.c.setOnClickListener(xVar.o);
    }
}
