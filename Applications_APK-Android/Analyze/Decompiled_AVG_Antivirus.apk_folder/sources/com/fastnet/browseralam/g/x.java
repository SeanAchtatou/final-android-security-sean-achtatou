package com.fastnet.browseralam.g;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.l;
import com.fastnet.browseralam.c.c;
import com.fastnet.browseralam.c.g;
import com.fastnet.browseralam.h.a;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public final class x extends BaseAdapter implements Filterable {
    private static final int m = Build.VERSION.SDK_INT;
    /* access modifiers changed from: private */
    public List a;
    private List b;
    /* access modifiers changed from: private */
    public List c;
    /* access modifiers changed from: private */
    public List d;
    /* access modifiers changed from: private */
    public c e;
    /* access modifiers changed from: private */
    public final Activity f;
    private final LayoutInflater g;
    private final File h;
    /* access modifiers changed from: private */
    public final AutoCompleteTextView i;
    /* access modifiers changed from: private */
    public final boolean j;
    /* access modifiers changed from: private */
    public boolean k = true;
    /* access modifiers changed from: private */
    public boolean l = false;
    private ad n;
    /* access modifiers changed from: private */
    public View.OnClickListener o = new z(this);
    private XmlPullParserFactory p;
    private XmlPullParser q;
    /* access modifiers changed from: private */
    public Runnable r = new ab(this);

    public x(Activity activity, AutoCompleteTextView autoCompleteTextView, boolean z) {
        this.e = c.a(activity.getApplicationContext());
        this.f = activity;
        this.g = this.f.getLayoutInflater();
        this.i = autoCompleteTextView;
        this.h = activity.getCacheDir();
        activity.getTheme();
        this.d = new ArrayList();
        this.a = new ArrayList();
        this.b = new ArrayList();
        this.c = new ArrayList();
        a.a();
        this.k = a.r();
        this.j = z;
        try {
            if (this.p == null) {
                this.p = XmlPullParserFactory.newInstance();
                this.p.setNamespaceAware(true);
            }
            if (this.q == null) {
                this.q = this.p.newPullParser();
            }
        } catch (XmlPullParserException e2) {
        }
        l.a(new y(this));
        this.i.setDropDownVerticalOffset(0);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006a A[SYNTHETIC, Splitter:B:25:0x006a] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0074 A[SYNTHETIC, Splitter:B:31:0x0074] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List a(java.lang.String r7) {
        /*
            r6 = this;
            r2 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.lang.String r1 = "ISO-8859-1"
            java.lang.String r7 = java.net.URLEncoder.encode(r7, r1)     // Catch:{ UnsupportedEncodingException -> 0x007e }
        L_0x000c:
            java.io.File r3 = r6.b(r7)
            boolean r1 = r3.exists()
            if (r1 != 0) goto L_0x0017
        L_0x0016:
            return r0
        L_0x0017:
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0066, all -> 0x0070 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0066, all -> 0x0070 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0066, all -> 0x0070 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0066, all -> 0x0070 }
            org.xmlpull.v1.XmlPullParser r2 = r6.q     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r3 = "ISO-8859-1"
            r2.setInput(r1, r3)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            org.xmlpull.v1.XmlPullParser r2 = r6.q     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            int r3 = r2.getEventType()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r2 = 0
        L_0x002f:
            r4 = 1
            if (r3 == r4) goto L_0x0060
            r4 = 2
            if (r3 != r4) goto L_0x0059
            java.lang.String r3 = "suggestion"
            org.xmlpull.v1.XmlPullParser r4 = r6.q     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            java.lang.String r4 = r4.getName()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            if (r3 == 0) goto L_0x0059
            org.xmlpull.v1.XmlPullParser r3 = r6.q     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r4 = 0
            java.lang.String r5 = "data"
            java.lang.String r3 = r3.getAttributeValue(r4, r5)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            com.fastnet.browseralam.c.g r4 = new com.fastnet.browseralam.c.g     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r4.<init>(r3)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            r0.add(r4)     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            int r2 = r2 + 1
            r3 = 5
            if (r2 >= r3) goto L_0x0060
        L_0x0059:
            org.xmlpull.v1.XmlPullParser r3 = r6.q     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            int r3 = r3.next()     // Catch:{ Exception -> 0x007c, all -> 0x007a }
            goto L_0x002f
        L_0x0060:
            r1.close()     // Catch:{ IOException -> 0x0064 }
            goto L_0x0016
        L_0x0064:
            r1 = move-exception
            goto L_0x0016
        L_0x0066:
            r1 = move-exception
            r1 = r2
        L_0x0068:
            if (r1 == 0) goto L_0x0016
            r1.close()     // Catch:{ IOException -> 0x006e }
            goto L_0x0016
        L_0x006e:
            r1 = move-exception
            goto L_0x0016
        L_0x0070:
            r0 = move-exception
            r1 = r2
        L_0x0072:
            if (r1 == 0) goto L_0x0077
            r1.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0077:
            throw r0
        L_0x0078:
            r1 = move-exception
            goto L_0x0077
        L_0x007a:
            r0 = move-exception
            goto L_0x0072
        L_0x007c:
            r2 = move-exception
            goto L_0x0068
        L_0x007e:
            r1 = move-exception
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.g.x.a(java.lang.String):java.util.List");
    }

    static /* synthetic */ void a(x xVar, Context context) {
        File file = new File(context.getCacheDir().toString());
        String[] list = file.list(new ac(xVar, (byte) 0));
        long currentTimeMillis = System.currentTimeMillis() - 86400000;
        int length = list.length;
        for (int i2 = 0; i2 < length; i2++) {
            File file2 = new File(file.getPath() + list[i2]);
            if (currentTimeMillis > file2.lastModified()) {
                file2.delete();
            }
        }
    }

    static /* synthetic */ void a(x xVar, String str) {
        xVar.l = true;
        l.a(new aa(xVar, str));
    }

    private File b(String str) {
        File file = new File(this.h, str.hashCode() + ".sgg");
        if (System.currentTimeMillis() - 86400000 < file.lastModified()) {
            return file;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) this.f.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager == null ? null : connectivityManager.getActiveNetworkInfo();
        if (!(activeNetworkInfo != null && activeNetworkInfo.isConnected())) {
            return file;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://google.com/complete/search?q=" + str + "&output=toolbar&hl=en").openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            if (inputStream != null) {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                while (true) {
                    int read = inputStream.read();
                    if (read == -1) {
                        break;
                    }
                    fileOutputStream.write(read);
                }
                fileOutputStream.flush();
                fileOutputStream.close();
            }
            file.setLastModified(System.currentTimeMillis());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return file;
    }

    public final void a() {
        a.a();
        this.k = a.r();
        if (!this.k && this.c != null) {
            this.c.clear();
        }
    }

    public final List b() {
        int i2 = 2;
        int i3 = 0;
        ArrayList arrayList = new ArrayList();
        int size = this.c == null ? 0 : this.c.size();
        int size2 = this.a == null ? 0 : this.a.size();
        int size3 = this.b == null ? 0 : this.b.size();
        int i4 = size3 + size2 < 3 ? (5 - size3) - size2 : size3 < 2 ? 4 - size3 : size2 <= 0 ? 3 : 2;
        int i5 = size + size3 < 4 ? (5 - size) - size3 : 1;
        if (size + size2 < 3) {
            i2 = (5 - size) - size2;
        }
        if (!this.k) {
            i5++;
            i2++;
        }
        int i6 = 0;
        while (i6 < size3 && i6 < i2) {
            arrayList.add(this.b.get(i6));
            i6++;
        }
        int i7 = 0;
        while (i7 < size2 && i7 < i5) {
            arrayList.add(this.a.get(i7));
            i7++;
        }
        while (i3 < size && i3 < i4) {
            arrayList.add(this.c.get(i3));
            i3++;
        }
        return arrayList;
    }

    public final int getCount() {
        if (this.d != null) {
            return this.d.size();
        }
        return 0;
    }

    public final Filter getFilter() {
        if (this.n == null) {
            this.n = new ad(this);
        }
        return this.n;
    }

    public final Object getItem(int i2) {
        return this.d.get(i2);
    }

    public final long getItemId(int i2) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i2, View view, ViewGroup viewGroup) {
        ae aeVar;
        if (view == null) {
            view = this.g.inflate((int) R.layout.two_line_autocomplete, viewGroup, false);
            aeVar = new ae(this, view);
        } else {
            aeVar = (ae) view.getTag();
        }
        if (i2 < this.d.size()) {
            g gVar = (g) this.d.get(i2);
            aeVar.a.setText(gVar.f());
            if (gVar.e() == null) {
                aeVar.a.setTag(gVar.f());
                aeVar.b.setVisibility(8);
                aeVar.b.setText((CharSequence) null);
                aeVar.c.setTag(gVar.f());
            } else {
                aeVar.a.setTag(gVar.e());
                aeVar.b.setVisibility(0);
                aeVar.b.setText(gVar.e());
                aeVar.c.setTag(gVar.e());
            }
        }
        return view;
    }
}
