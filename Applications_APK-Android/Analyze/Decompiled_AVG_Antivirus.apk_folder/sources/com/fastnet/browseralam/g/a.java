package com.fastnet.browseralam.g;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.activity.y;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.e.c;
import com.fastnet.browseralam.e.o;
import com.fastnet.browseralam.e.q;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.XWebView;
import java.util.Collections;
import java.util.List;

public final class a extends bi implements q, m {
    private static final ViewGroup.LayoutParams o = new ViewGroup.LayoutParams(-1, -2);
    /* access modifiers changed from: private */
    public int A = this.y;
    /* access modifiers changed from: private */
    public int B = 0;
    private ImageView C = null;
    /* access modifiers changed from: private */
    public boolean D = false;
    private Runnable E = new c(this);
    private final int F = 0;
    private final int G = 1;
    private final int H = 2;
    private final int I = 3;
    /* access modifiers changed from: private */
    public final BrowserActivity a;
    /* access modifiers changed from: private */
    public final RecyclerView b;
    private final LayoutInflater c;
    /* access modifiers changed from: private */
    public final TextView d;
    private final boolean e;
    /* access modifiers changed from: private */
    public List f;
    private int g = R.layout.card_item;
    /* access modifiers changed from: private */
    public y h;
    /* access modifiers changed from: private */
    public LinearLayoutManager i;
    private c j;
    /* access modifiers changed from: private */
    public e k = new e(this, (byte) 0);
    /* access modifiers changed from: private */
    public Handler l = k.a();
    private Drawable m;
    private Drawable n;
    /* access modifiers changed from: private */
    public int p = aq.a(125);
    private int q = aq.a(120);
    private int r = aq.a(118);
    /* access modifiers changed from: private */
    public int s = aq.a(64);
    /* access modifiers changed from: private */
    public int t = 0;
    /* access modifiers changed from: private */
    public int u = 0;
    /* access modifiers changed from: private */
    public View v;
    /* access modifiers changed from: private */
    public int w = 0;
    private int x = ((int) (((float) this.p) * 0.5f));
    private int y = (-this.x);
    /* access modifiers changed from: private */
    public int z = this.x;

    public a(y yVar, BrowserActivity browserActivity, int i2, RecyclerView recyclerView, List list, boolean z2, TextView textView) {
        this.h = yVar;
        this.a = browserActivity;
        this.c = this.a.getLayoutInflater();
        this.g = i2;
        this.b = recyclerView;
        this.f = list;
        this.e = z2;
        this.d = textView;
        this.m = android.support.v4.content.a.a(this.a, R.drawable.large_tab_card);
        this.n = android.support.v4.content.a.a(this.a, R.drawable.large_tab_card_highlight);
        this.i = new LinearLayoutManager(0);
        this.b.a(this.i);
        this.b.a(this);
        this.b.setOverScrollMode(1);
        this.b.a(true);
        RecyclerView recyclerView2 = this.b;
        c cVar = new c(this);
        this.j = cVar;
        recyclerView2.a(cVar);
        this.b.i().h();
        this.b.i().e();
        this.j.k();
        new android.support.v7.widget.a.a(new o(this)).a(this.b);
        this.b.a(new g(this));
        this.b.a(new h(this, (byte) 0));
        this.b.setOnTouchListener(new i(this, (byte) 0));
        this.t = (int) (((double) aq.a()) * 0.5d);
    }

    /* access modifiers changed from: private */
    public void h(int i2) {
        this.B = i2;
        this.w = ((i2 + 1) * this.p) - this.q;
        this.z = this.x + this.w;
        this.A = this.y + this.w;
        this.d.setText(((XWebView) this.f.get(i2)).x());
    }

    static /* synthetic */ int n(a aVar) {
        int i2 = aVar.B;
        aVar.B = i2 + 1;
        return i2;
    }

    static /* synthetic */ int q(a aVar) {
        int i2 = aVar.B;
        aVar.B = i2 - 1;
        return i2;
    }

    public final int a() {
        if (this.f != null) {
            return this.f.size();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final cf a(ViewGroup viewGroup, int i2) {
        return new d(this, this.c.inflate(this.g, viewGroup, false));
    }

    public final void a(int i2, int i3) {
        if (i3 == 1) {
            b(i2);
        } else if (i3 == 2 && i2 == this.B) {
            this.d.setText(((XWebView) this.f.get(i2)).x());
        }
    }

    public final void a(cf cfVar, int i2) {
        d dVar = (d) cfVar;
        XWebView xWebView = (XWebView) this.f.get(i2);
        dVar.l.setImageBitmap(xWebView.A());
        if (xWebView.i()) {
            dVar.l.setBackground(this.n);
            dVar.m = true;
            this.u = i2;
            this.v = dVar.l;
        } else {
            dVar.l.setBackground(this.m);
            dVar.m = false;
        }
        dVar.l.setTag(dVar);
    }

    public final void a_(int i2) {
        this.a.a(i2, this.e);
    }

    public final void b() {
        c();
        g();
    }

    public final boolean b(int i2, int i3) {
        Collections.swap(this.f, i2, i3);
        ((XWebView) this.f.get(i3)).a(i3);
        ((XWebView) this.f.get(i2)).a(i2);
        a_(i2, i3);
        return true;
    }

    public final void b_(int i2) {
        this.f.remove(i2);
        List list = this.f;
        int size = list.size();
        for (int i3 = i2; i3 < size; i3++) {
            ((XWebView) list.get(i3)).a(i3);
        }
        d dVar = (d) this.b.c(i2);
        if (dVar != null) {
            dVar.l.setImageBitmap(null);
            dVar.l.setBackground(null);
        }
        d_(i2);
        if (i2 <= this.B && this.f.size() != 0) {
            if (i2 == this.f.size()) {
                h(i2 - 1);
            } else if (i2 != this.u || i2 != this.B) {
                h(i2);
            } else if (i2 == 0) {
                h(0);
            } else {
                this.D = false;
                this.b.a(-this.p, 0);
            }
        }
    }

    public final int d() {
        return this.B;
    }

    public final void d(int i2) {
        if (((XWebView) this.f.get(i2)).E()) {
            this.h.a_((XWebView) this.f.get(i2));
            c_(i2);
            return;
        }
        this.j.l();
        c_(i2);
        h(i2);
        if (i2 == this.f.size() - 1) {
            this.b.a(this.t, 0);
        } else {
            this.b.a(i2);
        }
        this.l.postDelayed(this.E, 150);
    }

    public final void e() {
        if (this.f.size() == 0) {
            this.d.setText((CharSequence) null);
        } else {
            this.d.setText(((XWebView) this.f.get(this.B)).x());
        }
    }

    public final void e(int i2) {
        d_(i2);
    }

    public final void f() {
        this.i.e(this.u, this.r);
        if (this.f.size() != 0) {
            if (this.u >= this.f.size()) {
                this.u = this.f.size() - 1;
            }
            h(this.u);
        }
    }

    public final void g() {
        this.l.postDelayed(new b(this), 200);
    }

    public final void g(int i2) {
        this.g = i2;
        this.b.a((bi) null);
        this.b.a(this);
        c();
    }
}
