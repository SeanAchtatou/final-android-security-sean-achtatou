package com.fastnet.browseralam.g;

import android.support.v7.widget.cf;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.e.r;

public final class ap extends cf implements r {
    final /* synthetic */ aj l;
    /* access modifiers changed from: private */
    public final FrameLayout m;
    /* access modifiers changed from: private */
    public final ImageView n;
    /* access modifiers changed from: private */
    public boolean o = false;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ap(aj ajVar, View view) {
        super(view);
        this.l = ajVar;
        this.m = (FrameLayout) view.findViewById(R.id.card);
        this.n = (ImageView) view.findViewById(R.id.delete);
        this.m.setOnClickListener(ajVar.D);
    }

    public final void b() {
        this.m.animate().scaleY(1.0f).scaleX(1.0f).setDuration(30).start();
    }

    public final int c() {
        return d();
    }

    public final void c_() {
        this.m.animate().scaleY(1.04f).scaleX(1.04f).setDuration(30).start();
    }
}
