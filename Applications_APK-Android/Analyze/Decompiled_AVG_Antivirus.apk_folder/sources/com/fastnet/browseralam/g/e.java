package com.fastnet.browseralam.g;

import android.view.View;
import com.fastnet.browseralam.e.r;
import com.fastnet.browseralam.view.XWebView;

final class e implements View.OnClickListener {
    int a;
    final /* synthetic */ a b;

    private e(a aVar) {
        this.b = aVar;
        this.a = 0;
    }

    /* synthetic */ e(a aVar, byte b2) {
        this(aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
     arg types: [com.fastnet.browseralam.view.XWebView, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.g.a.a(com.fastnet.browseralam.g.a, boolean):boolean
     arg types: [com.fastnet.browseralam.g.a, int]
     candidates:
      com.fastnet.browseralam.g.a.a(com.fastnet.browseralam.g.a, int):int
      com.fastnet.browseralam.g.a.a(com.fastnet.browseralam.g.a, android.view.View):android.view.View
      com.fastnet.browseralam.g.a.a(android.view.ViewGroup, int):android.support.v7.widget.cf
      com.fastnet.browseralam.g.a.a(int, int):void
      com.fastnet.browseralam.g.a.a(android.support.v7.widget.cf, int):void
      android.support.v7.widget.bi.a(android.view.ViewGroup, int):android.support.v7.widget.cf
      android.support.v7.widget.bi.a(android.support.v7.widget.cf, int):void
      com.fastnet.browseralam.g.m.a(int, int):void
      com.fastnet.browseralam.g.a.a(com.fastnet.browseralam.g.a, boolean):boolean */
    public final void onClick(View view) {
        int c = ((r) view.getTag()).c();
        int unused = this.b.u = c;
        View unused2 = this.b.v = view;
        if (this.b.a.D() != this.b.f.get(c)) {
            this.b.a.a((XWebView) this.b.f.get(c), true);
        }
        this.a = 0;
        int x = ((int) view.getX()) + this.b.s;
        if (x < this.b.t) {
            this.a = -(this.b.t - x);
        } else if (x > this.b.t) {
            this.a = x - this.b.t;
        }
        boolean unused3 = this.b.D = false;
        this.b.b.a(this.a, 0);
        this.b.h.g();
        this.b.l.postDelayed(new f(this), 150);
    }
}
