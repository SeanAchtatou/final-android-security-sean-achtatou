package com.fastnet.browseralam.g;

import android.animation.Animator;

final class s implements Animator.AnimatorListener {
    final /* synthetic */ n a;

    private s(n nVar) {
        this.a = nVar;
    }

    /* synthetic */ s(n nVar, byte b) {
        this(nVar);
    }

    public final void onAnimationCancel(Animator animator) {
        if (this.a.i.getTranslationY() != this.a.k) {
            this.a.i.setTranslationY(this.a.k);
        }
    }

    public final void onAnimationEnd(Animator animator) {
    }

    public final void onAnimationRepeat(Animator animator) {
    }

    public final void onAnimationStart(Animator animator) {
    }
}
