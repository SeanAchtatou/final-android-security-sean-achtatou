package com.fastnet.browseralam.g;

import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.a.a;
import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.support.v7.widget.cz;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.e.q;
import com.fastnet.browseralam.e.s;
import com.fastnet.browseralam.view.CustomViewPager;
import com.fastnet.browseralam.view.XWebView;
import java.util.Collections;
import java.util.List;

public final class af extends bi implements q, m {
    /* access modifiers changed from: private */
    public final BrowserActivity a;
    private final LayoutInflater b;
    private final RecyclerView c;
    private final LinearLayoutManager d;
    private final CustomViewPager e;
    /* access modifiers changed from: private */
    public final List f;
    private final ah g;
    private final ag h;
    /* access modifiers changed from: private */
    public final boolean i;
    private final int j;
    private int k = R.layout.tab_list_item;
    /* access modifiers changed from: private */
    public int l;

    public af(BrowserActivity browserActivity, CustomViewPager customViewPager, RecyclerView recyclerView, List list, boolean z) {
        int i2 = 0;
        this.a = browserActivity;
        this.b = browserActivity.getLayoutInflater();
        this.c = recyclerView;
        this.e = customViewPager;
        this.f = list;
        this.i = z;
        this.g = new ah(this, (byte) 0);
        this.h = new ag(this, (byte) 0);
        this.j = z ? 1 : i2;
        this.l = Color.parseColor("#EEEEEE");
        this.c.a(this);
        this.c.setOverScrollMode(1);
        this.c.a(true);
        this.d = new LinearLayoutManager();
        this.c.a(this.d);
        this.c.i().h();
        this.c.i().e();
        ((cz) this.c.i()).k();
        new a(new s(this)).a(this.c);
    }

    public final int a() {
        if (this.f != null) {
            return this.f.size();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ cf a(ViewGroup viewGroup, int i2) {
        return new ai(this, this.b.inflate(this.k, viewGroup, false));
    }

    public final void a(int i2, int i3) {
        b(i2);
    }

    public final void a(cf cfVar, int i2) {
        ai aiVar = (ai) cfVar;
        XWebView xWebView = (XWebView) this.f.get(i2);
        aiVar.l.setText(xWebView.x());
        aiVar.m.setImageBitmap(xWebView.C());
        if (xWebView.i()) {
            aiVar.l.setTextAppearance(this.a, R.style.boldText);
            aiVar.o.setBackgroundColor(this.l);
            aiVar.p = true;
        } else {
            aiVar.o.setBackgroundColor(0);
            aiVar.l.setTextAppearance(this.a, R.style.normalText);
            aiVar.p = false;
        }
        aiVar.o.setTag(aiVar);
        aiVar.o.setOnClickListener(this.g);
        aiVar.n.setOnClickListener(this.h);
    }

    public final void a(boolean z) {
        this.d.a(z);
        this.c.a(this.d);
    }

    public final void a_(int i2) {
        this.a.a(i2, this.i);
    }

    public final void b() {
        c();
    }

    public final boolean b(int i2, int i3) {
        Collections.swap(this.f, i2, i3);
        ((XWebView) this.f.get(i3)).a(i3);
        ((XWebView) this.f.get(i2)).a(i2);
        a_(i2, i3);
        return true;
    }

    public final void b_(int i2) {
        this.f.remove(i2);
        List list = this.f;
        int size = list.size();
        for (int i3 = i2; i3 < size; i3++) {
            ((XWebView) list.get(i3)).a(i3);
        }
        d_(i2);
        if (this.i && this.f.size() == 0) {
            this.e.b(0);
        }
    }

    public final void d(int i2) {
        c_(i2);
    }

    public final void e(int i2) {
        f(i2);
    }
}
