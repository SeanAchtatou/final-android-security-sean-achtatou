package com.fastnet.browseralam.g;

import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.a.a;
import android.support.v7.widget.bi;
import android.support.v7.widget.bl;
import android.support.v7.widget.cf;
import android.support.v7.widget.cz;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.activity.ei;
import com.fastnet.browseralam.e.ad;
import com.fastnet.browseralam.e.o;
import com.fastnet.browseralam.e.q;
import com.fastnet.browseralam.e.t;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.CardFrame;
import com.fastnet.browseralam.view.XWebView;
import java.util.Collections;
import java.util.List;

public final class az extends bi implements q, k, m {
    private static final ViewGroup.LayoutParams B = new ViewGroup.LayoutParams(-1, -2);
    /* access modifiers changed from: private */
    public bf A;
    private View.OnClickListener C = new ba(this);
    /* access modifiers changed from: private */
    public View.OnClickListener D = new bb(this);
    private ad E = new bc(this);
    /* access modifiers changed from: private */
    public int F = 0;
    /* access modifiers changed from: private */
    public int G = ((int) (((float) this.n) * 0.5f));
    /* access modifiers changed from: private */
    public int H = ((-this.G) - this.q);
    /* access modifiers changed from: private */
    public int I = this.G;
    /* access modifiers changed from: private */
    public int J = this.H;
    /* access modifiers changed from: private */
    public int K = 0;
    /* access modifiers changed from: private */
    public boolean L;
    private boolean M;
    private View.OnTouchListener N = new bd(this);
    private bl O = new be(this);
    /* access modifiers changed from: private */
    public final BrowserActivity a;
    /* access modifiers changed from: private */
    public final RecyclerView b;
    private final CardFrame c;
    private final LayoutInflater d;
    /* access modifiers changed from: private */
    public final TextView e;
    /* access modifiers changed from: private */
    public final boolean f;
    /* access modifiers changed from: private */
    public List g;
    private int h = R.layout.xcard_item;
    /* access modifiers changed from: private */
    public ei i;
    private LinearLayoutManager j;
    /* access modifiers changed from: private */
    public t k;
    private int l = Color.parseColor("#0073EF");
    private int m = aq.b();
    private int n = ((int) (((double) aq.a()) * 0.5d));
    /* access modifiers changed from: private */
    public int o = (this.n + aq.a(20));
    /* access modifiers changed from: private */
    public int p = (this.n / 2);
    private int q = aq.a(20);
    /* access modifiers changed from: private */
    public int r = 0;
    private int s = -1;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public boolean v;
    /* access modifiers changed from: private */
    public boolean w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public FrameLayout y;
    /* access modifiers changed from: private */
    public bf z;

    public az(ei eiVar, BrowserActivity browserActivity, RecyclerView recyclerView, CardFrame cardFrame, List list, TextView textView) {
        this.i = eiVar;
        this.a = browserActivity;
        this.d = this.a.getLayoutInflater();
        this.b = recyclerView;
        this.c = cardFrame;
        this.g = list;
        this.f = true;
        this.e = textView;
        this.j = new LinearLayoutManager(0);
        this.b.a(this.j);
        this.b.a(this);
        this.b.setOverScrollMode(2);
        this.b.a(true);
        RecyclerView recyclerView2 = this.b;
        t tVar = new t(this.E);
        this.k = tVar;
        recyclerView2.a(tVar);
        this.b.i().h();
        this.b.i().e();
        ((cz) this.b.i()).k();
        new a(new o(this)).a(this.b);
        int a2 = (this.n - ((int) (((double) this.n) * 0.5d))) - aq.a(10);
        int b2 = aq.b() - aq.a(56);
        this.b.setPadding(a2, (((int) (((double) b2) * 0.5d)) - ((int) (((double) b2) * 0.25d))) + aq.a(56), a2, 0);
        this.b.setOnTouchListener(this.N);
        this.b.a(new bg(this, (byte) 0));
        this.b.a(this.O);
        this.r = aq.a() / 2;
    }

    /* access modifiers changed from: private */
    public void g(int i2) {
        if (i2 == this.g.size() - 1) {
            this.K = i2 - 1;
        } else if (i2 == 0) {
            this.K = 0;
        } else {
            this.K = i2 + 1;
        }
        this.A = (bf) this.b.c(this.K);
    }

    private void h(int i2) {
        this.K = i2;
        this.F = this.o * i2;
        this.I = this.G + this.F;
        this.J = this.H + this.F;
    }

    static /* synthetic */ int w(az azVar) {
        int i2 = azVar.K;
        azVar.K = i2 + 1;
        return i2;
    }

    static /* synthetic */ int z(az azVar) {
        int i2 = azVar.K;
        azVar.K = i2 - 1;
        return i2;
    }

    public final int a() {
        if (this.g != null) {
            return this.g.size();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final cf a(ViewGroup viewGroup, int i2) {
        return new bf(this, this.d.inflate(this.h, viewGroup, false));
    }

    public final void a(int i2, int i3) {
        if (i3 == 1) {
            if (this.g.size() == 0) {
                if (!this.v) {
                    this.i.a(false);
                } else {
                    this.i.a(0);
                }
            } else if (!this.x) {
                if (!this.v) {
                    this.b.setVisibility(0);
                    if (((XWebView) this.g.get(i2)).i()) {
                        this.s = i2;
                    }
                } else {
                    this.s = -1;
                }
                b(i2);
            }
        } else if (i3 == 2 && i2 == this.K) {
            this.e.setText(((XWebView) this.g.get(i2)).x());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.g.bf.a(com.fastnet.browseralam.g.bf, boolean):boolean
     arg types: [com.fastnet.browseralam.g.bf, int]
     candidates:
      android.support.v7.widget.cf.a(int, int):void
      android.support.v7.widget.cf.a(int, boolean):void
      android.support.v7.widget.cf.a(android.support.v7.widget.bw, boolean):void
      com.fastnet.browseralam.g.bf.a(com.fastnet.browseralam.g.bf, boolean):boolean */
    public final void a(cf cfVar, int i2) {
        bf bfVar = (bf) cfVar;
        XWebView xWebView = (XWebView) this.g.get(i2);
        bfVar.m.setBackground(xWebView.B());
        if (xWebView.i()) {
            boolean unused = bfVar.o = true;
            if (i2 == this.s) {
                bfVar.m.setScaleX(2.0f);
                bfVar.m.setScaleY(2.0f);
                if (this.y != null) {
                    this.y.setScaleX(1.0f);
                    this.y.setScaleY(1.0f);
                }
                this.s = -1;
            }
            this.t = i2;
            this.y = bfVar.m;
            this.z = bfVar;
            if (!this.v) {
                this.K = i2;
                this.A = bfVar;
                this.b.setVisibility(8);
            }
        } else {
            boolean unused2 = bfVar.o = false;
            bfVar.n.setVisibility(0);
        }
        bfVar.m.setTag(bfVar);
        bfVar.n.setTag(bfVar);
        bfVar.n.setOnClickListener(this.C);
    }

    public final void a(boolean z2) {
        this.v = z2;
        if (this.v) {
            this.w = true;
        }
    }

    public final void a_(int i2) {
        g(i2);
        this.a.a(i2, this.f);
    }

    public final void b() {
        c();
    }

    public final void b(boolean z2) {
        this.M = z2;
        if (!z2 && this.z != null) {
            this.z.m.setScaleX(1.0f);
            this.z.m.setScaleY(1.0f);
            this.y = null;
        }
    }

    public final boolean b(int i2, int i3) {
        Collections.swap(this.g, i2, i3);
        ((XWebView) this.g.get(i3)).a(i3);
        ((XWebView) this.g.get(i2)).a(i2);
        a_(i2, i3);
        return true;
    }

    public final void b_(int i2) {
        this.g.remove(i2);
        List list = this.g;
        int size = list.size();
        for (int i3 = i2; i3 < size; i3++) {
            ((XWebView) list.get(i3)).a(i3);
        }
        if (!this.v) {
            if (i2 == 0) {
                this.s = 0;
            }
            ((ViewGroup) this.y.getParent()).removeView(this.y);
            this.y.setBackground(null);
            this.y.removeAllViews();
            this.y = null;
            this.b.setVisibility(0);
        }
        this.k.a(true);
        d_(i2);
        if (i2 <= this.K) {
            if (this.g.size() != 0) {
                if (i2 == this.g.size()) {
                    h(i2 - 1);
                } else if (i2 == this.t && i2 == this.K) {
                    h(i2);
                    this.L = false;
                    this.j.e(i2, 0);
                } else {
                    h(i2);
                }
            } else {
                return;
            }
        }
        this.i.a(this.g.size());
    }

    public final void d() {
        this.j.e(this.t, 0);
        if (this.g.size() != 0) {
            if (this.t >= this.g.size()) {
                this.t = this.g.size() - 1;
            }
            h(this.t);
        }
    }

    public final void d(int i2) {
        c_(i2);
        this.i.a(this.g.size());
        if (((XWebView) this.g.get(i2)).E()) {
            this.i.d((XWebView) this.g.get(i2));
        } else if (!this.v) {
            if (!this.M) {
                this.i.a(true);
            }
            this.s = i2;
            this.b.setVisibility(0);
            this.j.e(i2, 0);
        } else {
            this.k.a(i2);
            this.w = true;
            this.L = true;
            this.b.b(i2);
        }
    }

    public final void e() {
        this.x = false;
        this.w = false;
        b(this.u);
        b(this.t);
        if (this.z.m.getAlpha() == 0.0f) {
            this.z.m.setAlpha(1.0f);
        }
    }

    public final void e(int i2) {
        d_(i2);
    }

    public final void e_(int i2) {
        this.z.n.setVisibility(i2);
        this.w = false;
    }

    public final void f() {
        if (this.g.size() == 0) {
            this.e.setText((CharSequence) null);
            return;
        }
        if (this.K < 0 || this.K >= this.g.size()) {
            this.K = this.t;
        }
        this.e.setText(((XWebView) this.g.get(this.K)).x());
    }

    public final void f_() {
        if (this.A != null) {
            this.A.m.callOnClick();
        }
    }

    public final View g() {
        return this.z.a;
    }

    public final int h() {
        return this.K;
    }

    public final t i() {
        return this.k;
    }
}
