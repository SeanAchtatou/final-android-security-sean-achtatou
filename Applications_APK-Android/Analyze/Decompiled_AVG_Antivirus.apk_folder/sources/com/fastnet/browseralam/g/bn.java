package com.fastnet.browseralam.g;

import android.graphics.PorterDuff;
import android.support.v7.widget.cf;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.e.r;

public final class bn extends cf implements r {
    final TextView l;
    final ImageView m;
    final ImageView n;
    final RelativeLayout o;
    boolean p = false;
    final /* synthetic */ bh q;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bn(bh bhVar, View view) {
        super(view);
        this.q = bhVar;
        this.l = (TextView) view.findViewById(R.id.textTab);
        this.m = (ImageView) view.findViewById(R.id.faviconTab);
        this.n = (ImageView) view.findViewById(R.id.delete_tab);
        this.o = (RelativeLayout) view.findViewById(R.id.tabItem);
        this.n.setColorFilter(R.color.gray_extra_dark, PorterDuff.Mode.SRC_IN);
        this.l.setTextColor(-1);
    }

    public final void b() {
        if (this.p) {
            this.o.setBackgroundColor(this.q.u);
        } else {
            this.o.setBackgroundColor(this.q.v);
        }
    }

    public final int c() {
        return d();
    }

    public final void c_() {
        this.o.setBackgroundColor(-3355444);
    }
}
