package com.fastnet.browseralam.e;

import android.animation.Animator;
import android.view.View;

public abstract class a implements Animator.AnimatorListener {
    public static a a(View view) {
        return new b(view);
    }

    public void onAnimationCancel(Animator animator) {
        onAnimationEnd(animator);
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationStart(Animator animator) {
    }
}
