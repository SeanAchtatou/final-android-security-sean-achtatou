package com.fastnet.browseralam.e;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.a.g;
import android.support.v7.widget.cf;

public final class s extends g {
    private final q a;
    private boolean b = true;
    private boolean c = true;
    private int d = 3;
    private int e = 48;
    private int f;

    public s(q qVar) {
        this.a = qVar;
    }

    public final int a() {
        return b(this.d, this.e);
    }

    public final void a(Canvas canvas, RecyclerView recyclerView, cf cfVar, float f2, float f3, int i, boolean z) {
        float f4;
        float f5 = 0.0f;
        if (i != 2 || this.f != 0) {
            if (i == 1) {
                cfVar.a.setAlpha(1.0f - (Math.abs(f2) / ((float) cfVar.a.getWidth())));
                cfVar.a.setTranslationX(f2);
            }
            f5 = f3;
            f4 = f2;
        } else if (!this.c || f3 >= 0.0f) {
            if (!this.c && f2 < 0.0f) {
                f4 = 0.0f;
                f5 = f3;
            }
            f5 = f3;
            f4 = f2;
        } else {
            f4 = f2;
        }
        super.a(canvas, recyclerView, cfVar, f4, f5, i, z);
    }

    public final void a(RecyclerView recyclerView, cf cfVar) {
        super.a(recyclerView, cfVar);
        cfVar.a.setAlpha(1.0f);
        ((r) cfVar).b();
    }

    public final void a(cf cfVar) {
        this.a.a_(((r) cfVar).c());
    }

    public final void a(cf cfVar, int i) {
        if (i != 0) {
            this.f = cfVar.d();
            ((r) cfVar).c_();
        }
        super.a(cfVar, i);
    }

    public final boolean a(cf cfVar, cf cfVar2) {
        this.f = cfVar2.d();
        this.a.b(cfVar.d(), this.f);
        return true;
    }

    public final boolean c() {
        return true;
    }

    public final boolean d() {
        return this.b;
    }

    public final s h() {
        this.b = false;
        this.c = false;
        this.d = 48;
        this.e = 3;
        return this;
    }
}
