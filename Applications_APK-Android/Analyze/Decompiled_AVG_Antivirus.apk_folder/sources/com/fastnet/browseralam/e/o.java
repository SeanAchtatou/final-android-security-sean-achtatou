package com.fastnet.browseralam.e;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.a.g;
import android.support.v7.widget.cf;

public final class o extends g {
    private final q a;
    private int b;

    public o(q qVar) {
        this.a = qVar;
    }

    public final int a() {
        return b(48, 1);
    }

    public final void a(Canvas canvas, RecyclerView recyclerView, cf cfVar, float f, float f2, int i, boolean z) {
        float f3 = 0.0f;
        if (i == 1) {
            cfVar.a.setAlpha(1.0f - (Math.abs(f2) / ((float) cfVar.a.getWidth())));
            cfVar.a.setTranslationY(f2);
            f3 = f;
        } else if (!(i == 2 && this.b == 0 && f < 0.0f)) {
            f3 = f;
        }
        super.a(canvas, recyclerView, cfVar, f3, f2, i, z);
    }

    public final void a(RecyclerView recyclerView, cf cfVar) {
        super.a(recyclerView, cfVar);
        cfVar.a.setAlpha(1.0f);
        ((r) cfVar).b();
    }

    public final void a(cf cfVar) {
        this.a.a_(((r) cfVar).c());
    }

    public final void a(cf cfVar, int i) {
        if (i != 0 && i == 2) {
            r rVar = (r) cfVar;
            this.b = rVar.c();
            rVar.c_();
        }
        super.a(cfVar, i);
    }

    public final boolean a(cf cfVar, cf cfVar2) {
        this.b = cfVar2.d();
        this.a.b(cfVar.d(), this.b);
        return true;
    }

    public final boolean c() {
        return true;
    }

    public final boolean d() {
        return true;
    }
}
