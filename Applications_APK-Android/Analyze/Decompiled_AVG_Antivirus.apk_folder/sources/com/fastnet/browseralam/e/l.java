package com.fastnet.browseralam.e;

import android.support.v7.widget.cf;

final class l {
    public cf a;
    public cf b;
    public int c;
    public int d;
    public int e;
    public int f;

    private l(cf cfVar, cf cfVar2) {
        this.a = cfVar;
        this.b = cfVar2;
    }

    private l(cf cfVar, cf cfVar2, int i, int i2, int i3, int i4) {
        this(cfVar, cfVar2);
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.f = i4;
    }

    /* synthetic */ l(cf cfVar, cf cfVar2, int i, int i2, int i3, int i4, byte b2) {
        this(cfVar, cfVar2, i, i2, i3, i4);
    }

    public final String toString() {
        return "ChangeInfo{oldHolder=" + this.a + ", newHolder=" + this.b + ", fromX=" + this.c + ", fromY=" + this.d + ", toX=" + this.e + ", toY=" + this.f + '}';
    }
}
