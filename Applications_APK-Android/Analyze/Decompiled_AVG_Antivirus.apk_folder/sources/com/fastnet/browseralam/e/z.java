package com.fastnet.browseralam.e;

import android.support.v4.view.bx;
import android.support.v4.view.dv;
import android.support.v4.view.el;
import android.support.v7.widget.cf;
import android.view.View;

final class z extends af {
    final /* synthetic */ cf a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ dv d;
    final /* synthetic */ t e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    z(t tVar, cf cfVar, int i, int i2, dv dvVar) {
        super((byte) 0);
        this.e = tVar;
        this.a = cfVar;
        this.b = i;
        this.c = i2;
        this.d = dvVar;
    }

    public final void a(View view) {
    }

    public final void b(View view) {
        this.d.a((el) null);
        this.e.e(this.a);
        this.e.j.remove(this.a);
        this.e.l();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.a(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.a(int, int):int
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.bi):void
      android.support.v4.view.bx.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.bx.a(android.view.View, boolean):void
      android.support.v4.view.bx.a(android.view.View, int):boolean
      android.support.v4.view.bx.a(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, boolean):void
      android.support.v4.view.bx.b(android.view.View, int):boolean
      android.support.v4.view.bx.b(android.view.View, float):void */
    public final void c(View view) {
        if (this.b != 0) {
            bx.a(view, 0.0f);
        }
        if (this.c != 0) {
            bx.b(view, 0.0f);
        }
    }
}
