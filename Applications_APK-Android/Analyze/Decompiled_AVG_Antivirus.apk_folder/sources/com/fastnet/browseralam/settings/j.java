package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;

final class j implements View.OnClickListener {
    final /* synthetic */ AlertDialog a;
    final /* synthetic */ AdvancedSettingsActivity b;

    j(AdvancedSettingsActivity advancedSettingsActivity, AlertDialog alertDialog) {
        this.b = advancedSettingsActivity;
        this.a = alertDialog;
    }

    public final void onClick(View view) {
        this.a.dismiss();
    }
}
