package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class ak implements DialogInterface.OnClickListener {
    final /* synthetic */ DisplaySettingsActivity a;

    ak(DisplaySettingsActivity displaySettingsActivity) {
        this.a = displaySettingsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        a.c(i);
        switch (i) {
            case 0:
                this.a.k.setText(this.a.i.getString(R.string.name_normal));
                return;
            case 1:
                this.a.k.setText(this.a.i.getString(R.string.name_inverted));
                return;
            case 2:
                this.a.k.setText(this.a.i.getString(R.string.name_grayscale));
                return;
            case 3:
                this.a.k.setText(this.a.i.getString(R.string.name_inverted_grayscale));
                return;
            default:
                return;
        }
    }
}
