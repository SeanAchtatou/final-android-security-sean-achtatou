package com.fastnet.browseralam.settings;

import android.view.View;
import android.widget.CheckedTextView;

final class f implements View.OnClickListener {
    final /* synthetic */ AdvancedSettingsActivity a;

    f(AdvancedSettingsActivity advancedSettingsActivity) {
        this.a = advancedSettingsActivity;
    }

    public final void onClick(View view) {
        CheckedTextView checkedTextView = (CheckedTextView) view;
        checkedTextView.setChecked(!checkedTextView.isChecked());
    }
}
