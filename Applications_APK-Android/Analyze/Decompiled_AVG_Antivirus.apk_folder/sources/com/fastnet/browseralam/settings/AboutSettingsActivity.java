package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.LicenseActivity;
import com.fastnet.browseralam.activity.MainActivity;
import com.fastnet.browseralam.i.c;
import com.fastnet.browseralam.i.q;
import com.mopub.volley.BuildConfig;

public class AboutSettingsActivity extends ThemableSettingsActivity implements View.OnClickListener {
    private int i;
    /* access modifiers changed from: private */
    public q j;

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0028 A[SYNTHETIC, Splitter:B:11:0x0028] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008d A[SYNTHETIC, Splitter:B:21:0x008d] */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r7 = this;
            r0 = 0
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0097, all -> 0x0087 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0097, all -> 0x0087 }
            android.content.res.AssetManager r3 = r7.getAssets()     // Catch:{ IOException -> 0x0097, all -> 0x0087 }
            java.lang.String r4 = "changelog.txt"
            java.io.InputStream r3 = r3.open(r4)     // Catch:{ IOException -> 0x0097, all -> 0x0087 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0097, all -> 0x0087 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0097, all -> 0x0087 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r2.<init>()     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
        L_0x001a:
            java.lang.String r0 = r1.readLine()     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            if (r0 == 0) goto L_0x002c
            r2.append(r0)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            goto L_0x001a
        L_0x0024:
            r0 = move-exception
            r0 = r1
        L_0x0026:
            if (r0 == 0) goto L_0x002b
            r0.close()     // Catch:{ IOException -> 0x0091 }
        L_0x002b:
            return
        L_0x002c:
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r0.<init>(r7)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            android.view.LayoutInflater r3 = r7.getLayoutInflater()     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r4 = 2130968631(0x7f040037, float:1.7545921E38)
            r5 = 0
            android.view.View r3 = r3.inflate(r4, r5)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r0.setView(r3)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            android.app.AlertDialog r4 = r0.create()     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r0 = 1
            r4.setCanceledOnTouchOutside(r0)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r0 = 2131558537(0x7f0d0089, float:1.8742393E38)
            android.view.View r0 = r3.findViewById(r0)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            android.widget.TextView r0 = (android.widget.TextView) r0     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            android.text.Spanned r2 = android.text.Html.fromHtml(r2)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r0.setText(r2)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r0 = 2131558538(0x7f0d008a, float:1.8742395E38)
            android.view.View r0 = r3.findViewById(r0)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            com.fastnet.browseralam.settings.a r2 = new com.fastnet.browseralam.settings.a     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r2.<init>(r7, r4)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r0.setOnClickListener(r2)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r4.show()     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            android.view.Window r0 = r4.getWindow()     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r2 = -1
            int r3 = com.fastnet.browseralam.i.aq.b()     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r4 = 112(0x70, float:1.57E-43)
            int r4 = com.fastnet.browseralam.i.aq.a(r4)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            int r3 = r3 - r4
            r0.setLayout(r2, r3)     // Catch:{ IOException -> 0x0024, all -> 0x0095 }
            r1.close()     // Catch:{ IOException -> 0x0085 }
            goto L_0x002b
        L_0x0085:
            r0 = move-exception
            goto L_0x002b
        L_0x0087:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x008b:
            if (r1 == 0) goto L_0x0090
            r1.close()     // Catch:{ IOException -> 0x0093 }
        L_0x0090:
            throw r0
        L_0x0091:
            r0 = move-exception
            goto L_0x002b
        L_0x0093:
            r1 = move-exception
            goto L_0x0090
        L_0x0095:
            r0 = move-exception
            goto L_0x008b
        L_0x0097:
            r1 = move-exception
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.settings.AboutSettingsActivity.d():void");
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layoutSource:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("fusionxr@gmail.com"), this, MainActivity.class));
                finish();
                return;
            case R.id.textView1:
            case R.id.isImportAvailable:
            case R.id.versionCode:
            case R.id.textView13:
            default:
                return;
            case R.id.layoutVersion:
                this.i++;
                if (this.i == 10) {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://imgs.xkcd.com/comics/compiling.png"), this, MainActivity.class));
                    finish();
                    this.i = 0;
                    return;
                }
                d();
                return;
            case R.id.checkUpdate:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Sudah versi terbaru");
                builder.setPositiveButton("OK", new b(this));
                builder.setCancelable(true);
                AlertDialog create = builder.create();
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setProgressStyle(0);
                progressDialog.setMessage("Checking for update...");
                progressDialog.setIndeterminate(true);
                progressDialog.setCanceledOnTouchOutside(false);
                this.j = new c(this, progressDialog, create);
                c.a(this, this.j);
                progressDialog.setButton(-2, "CANCEL", new d(this));
                progressDialog.show();
                return;
            case R.id.layoutLicense:
                startActivity(new Intent(this, LicenseActivity.class));
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView((int) R.layout.about_settings);
        a((Toolbar) findViewById(R.id.toolbar));
        c().a(true);
        try {
            str = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            str = BuildConfig.VERSION_NAME;
        }
        ((TextView) findViewById(R.id.versionCode)).setText(str);
        ((LinearLayout) findViewById(R.id.layoutLicense)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.layoutSource)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.layoutVersion)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.checkUpdate)).setOnClickListener(this);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.j != null) {
            this.j.a();
        }
    }
}
