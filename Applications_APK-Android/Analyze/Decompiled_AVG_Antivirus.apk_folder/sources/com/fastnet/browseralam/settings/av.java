package com.fastnet.browseralam.settings;

import android.view.View;
import android.widget.CheckBox;
import com.fastnet.browseralam.R;

final class av implements View.OnClickListener {
    final /* synthetic */ DisplaySettingsActivity a;

    private av(DisplaySettingsActivity displaySettingsActivity) {
        this.a = displaySettingsActivity;
    }

    /* synthetic */ av(DisplaySettingsActivity displaySettingsActivity, byte b) {
        this(displaySettingsActivity);
    }

    public final void onClick(View view) {
        boolean z = true;
        switch (view.getId()) {
            case R.id.rTextSize:
                DisplaySettingsActivity.l(this.a);
                return;
            case R.id.textSize:
            case R.id.renderText:
            case R.id.cbHideStatusBar:
            case R.id.cbStackTabsTop:
            case R.id.cbDrawerCards:
            case R.id.cbFullScreen:
            case R.id.cbDarkTheme:
            case R.id.cbWideViewPort:
            case R.id.cbOverView:
            case R.id.cbResizeWindow:
            default:
                return;
            case R.id.layoutRendering:
                DisplaySettingsActivity.m(this.a);
                return;
            case R.id.rEditMenu:
                DisplaySettingsActivity.a(this.a);
                return;
            case R.id.rEditLinkDialog:
                DisplaySettingsActivity.b(this.a);
                return;
            case R.id.rEditImageDialog:
                DisplaySettingsActivity.c(this.a);
                return;
            case R.id.rHideStatusBar:
                CheckBox d = this.a.m;
                if (this.a.m.isChecked()) {
                    z = false;
                }
                d.setChecked(z);
                return;
            case R.id.rStackTabsTop:
                CheckBox f = this.a.o;
                if (this.a.o.isChecked()) {
                    z = false;
                }
                f.setChecked(z);
                return;
            case R.id.rDrawerCards:
                CheckBox g = this.a.q;
                if (this.a.q.isChecked()) {
                    z = false;
                }
                g.setChecked(z);
                return;
            case R.id.rFullScreen:
                CheckBox h = this.a.p;
                if (this.a.p.isChecked()) {
                    z = false;
                }
                h.setChecked(z);
                return;
            case R.id.rDarkTheme:
                CheckBox n = this.a.u;
                if (this.a.u.isChecked()) {
                    z = false;
                }
                n.setChecked(z);
                return;
            case R.id.rWideViewPort:
                CheckBox i = this.a.r;
                if (this.a.r.isChecked()) {
                    z = false;
                }
                i.setChecked(z);
                return;
            case R.id.rOverView:
                CheckBox j = this.a.s;
                if (this.a.s.isChecked()) {
                    z = false;
                }
                j.setChecked(z);
                return;
            case R.id.rResizeWindow:
                CheckBox e = this.a.n;
                if (this.a.n.isChecked()) {
                    z = false;
                }
                e.setChecked(z);
                return;
            case R.id.rTextReflow:
                CheckBox k = this.a.t;
                if (this.a.t.isChecked()) {
                    z = false;
                }
                k.setChecked(z);
                return;
        }
    }
}
