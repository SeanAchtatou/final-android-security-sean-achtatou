package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

public class AdvancedSettingsActivity extends ThemableSettingsActivity {
    /* access modifiers changed from: private */
    public Context i;
    private a j;
    /* access modifiers changed from: private */
    public k k;
    /* access modifiers changed from: private */
    public ScrollView l;
    /* access modifiers changed from: private */
    public RecyclerView m;
    /* access modifiers changed from: private */
    public RelativeLayout n;
    /* access modifiers changed from: private */
    public ImageView o;
    /* access modifiers changed from: private */
    public ImageView p;
    /* access modifiers changed from: private */
    public CheckBox q;
    /* access modifiers changed from: private */
    public CheckBox r;
    /* access modifiers changed from: private */
    public CheckBox s;
    /* access modifiers changed from: private */
    public CheckBox t;
    /* access modifiers changed from: private */
    public CheckBox u;
    /* access modifiers changed from: private */
    public CheckBox v;
    /* access modifiers changed from: private */
    public TextView w;
    private CompoundButton.OnCheckedChangeListener x = new e(this);

    static /* synthetic */ void h(AdvancedSettingsActivity advancedSettingsActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(advancedSettingsActivity.i);
        View inflate = advancedSettingsActivity.getLayoutInflater().inflate((int) R.layout.adblock_help, (ViewGroup) null);
        builder.setView(inflate);
        AlertDialog create = builder.create();
        ((TextView) inflate.findViewById(R.id.help_text)).setText("You can add custom urls to the adblock list by long clicking an image  or link to show the dialog menu, then long click the url text to show the option. You can also click the page info button from the main menu to add the current page url to the list. Blocked urls will show up in this list from which you can remove if you wish. The urls will be loaded in addition to the default adblock list. I do not plan to enable addition of bulk hosts since the adblocker uses a significant amount of memory by default, however there is no limit on the number of urls you can add. You will be able to keep the blocked urls during updates but will be deleted if you uninstall the app.");
        inflate.findViewById(R.id.adblock_demo).setOnClickListener(new h(advancedSettingsActivity));
        inflate.findViewById(R.id.ok).setOnClickListener(new j(advancedSettingsActivity, create));
        create.show();
    }

    static /* synthetic */ void i(AdvancedSettingsActivity advancedSettingsActivity) {
        if (a.W()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(advancedSettingsActivity);
            View inflate = advancedSettingsActivity.getLayoutInflater().inflate((int) R.layout.intercept_video, (ViewGroup) null);
            builder.setView(inflate);
            AlertDialog create = builder.create();
            CheckedTextView checkedTextView = (CheckedTextView) inflate.findViewById(R.id.hide_message);
            checkedTextView.setOnClickListener(new f(advancedSettingsActivity));
            inflate.findViewById(R.id.ok).setOnClickListener(new g(advancedSettingsActivity, checkedTextView, create));
            create.show();
        }
    }

    public void onBackPressed() {
        if (this.l.getVisibility() == 8) {
            this.o.setVisibility(8);
            this.p.setVisibility(8);
            this.w.setText("Pengaturan lainnya");
            this.m.setVisibility(8);
            this.l.setVisibility(0);
            this.n.setVisibility(8);
            return;
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.advanced_settings);
        a((Toolbar) findViewById(R.id.toolbar));
        c().a(true);
        this.i = this;
        this.l = (ScrollView) findViewById(R.id.scrollView1);
        this.m = (RecyclerView) findViewById(R.id.adblock_list);
        this.w = (TextView) findViewById(R.id.settings_title);
        this.o = (ImageView) findViewById(R.id.add_url);
        this.p = (ImageView) findViewById(R.id.help);
        this.k = new k(this);
        this.j = a.a();
        this.n = (RelativeLayout) findViewById(R.id.rOnlyCustomAdblock);
        this.q = (CheckBox) findViewById(R.id.cbOpenLinksBackground);
        this.r = (CheckBox) findViewById(R.id.cbRestoreTabs);
        this.t = (CheckBox) findViewById(R.id.cbExitOnTabClose);
        this.u = (CheckBox) findViewById(R.id.cbInterceptVideo);
        this.s = (CheckBox) findViewById(R.id.cbHardwareRendering);
        this.v = (CheckBox) findViewById(R.id.cbOnlyCustomAdblock);
        this.q.setChecked(a.S());
        this.r.setChecked(a.T());
        this.t.setChecked(a.U());
        this.u.setChecked(a.V());
        this.s.setChecked(a.X());
        this.v.setChecked(a.Y());
        w wVar = new w(this, (byte) 0);
        ((RelativeLayout) findViewById(R.id.rOpenLinksBackground)).setOnClickListener(wVar);
        ((RelativeLayout) findViewById(R.id.rRestoreTabs)).setOnClickListener(wVar);
        ((RelativeLayout) findViewById(R.id.rExitOnTabClose)).setOnClickListener(wVar);
        ((RelativeLayout) findViewById(R.id.rInterceptVideo)).setOnClickListener(wVar);
        ((RelativeLayout) findViewById(R.id.rHardwareRendering)).setOnClickListener(wVar);
        ((RelativeLayout) findViewById(R.id.rAdblockList)).setOnClickListener(wVar);
        this.n.setOnClickListener(wVar);
        this.p.setOnClickListener(wVar);
        this.r.setOnCheckedChangeListener(this.x);
        this.t.setOnCheckedChangeListener(this.x);
        this.q.setOnCheckedChangeListener(this.x);
        this.u.setOnCheckedChangeListener(this.x);
        this.s.setOnCheckedChangeListener(this.x);
        this.v.setOnCheckedChangeListener(this.x);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (this.l.getVisibility() == 8) {
            this.o.setVisibility(8);
            this.p.setVisibility(8);
            this.w.setText("Pengaturan lainnya");
            this.m.setVisibility(8);
            this.l.setVisibility(0);
            this.n.setVisibility(8);
        } else {
            finish();
        }
        return true;
    }
}
