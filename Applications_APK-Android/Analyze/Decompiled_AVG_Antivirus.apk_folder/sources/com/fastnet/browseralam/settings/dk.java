package com.fastnet.browseralam.settings;

import android.view.View;
import android.widget.RadioButton;

final class dk implements View.OnClickListener {
    final /* synthetic */ RadioButton a;
    final /* synthetic */ SettingsActivity b;

    dk(SettingsActivity settingsActivity, RadioButton radioButton) {
        this.b = settingsActivity;
        this.a = radioButton;
    }

    public final void onClick(View view) {
        this.a.setChecked(true);
    }
}
