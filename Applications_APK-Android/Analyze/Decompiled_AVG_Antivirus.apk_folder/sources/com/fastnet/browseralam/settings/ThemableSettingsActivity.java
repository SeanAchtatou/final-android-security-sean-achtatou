package com.fastnet.browseralam.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.fastnet.browseralam.h.a;

public abstract class ThemableSettingsActivity extends AppCompatActivity {
    private boolean i;

    /* access modifiers changed from: protected */
    public final void f() {
        onSaveInstanceState(new Bundle());
        Intent intent = new Intent(this, getClass());
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        a.a();
        this.i = a.ai();
        if (this.i) {
            setTheme(2131296376);
        }
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        a.a();
        if (a.ai() != this.i) {
            f();
        }
    }
}
