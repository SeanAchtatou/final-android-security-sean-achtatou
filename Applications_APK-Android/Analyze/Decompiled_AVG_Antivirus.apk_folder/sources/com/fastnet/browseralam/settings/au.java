package com.fastnet.browseralam.settings;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.a.g;
import android.support.v7.widget.cf;

public final class au extends g {
    final /* synthetic */ DisplaySettingsActivity a;
    private final aq b;
    private int c;
    private int d;

    public au(DisplaySettingsActivity displaySettingsActivity, aq aqVar) {
        this.a = displaySettingsActivity;
        this.b = aqVar;
    }

    public final int a() {
        return b(3, 48);
    }

    public final void a(Canvas canvas, RecyclerView recyclerView, cf cfVar, float f, float f2, int i, boolean z) {
        float f3 = 0.0f;
        if (i != 2 || ((this.c != 0 || f2 >= 0.0f) && (this.c != this.d || f2 <= 0.0f))) {
            f3 = f2;
        }
        super.a(canvas, recyclerView, cfVar, f, f3, i, z);
    }

    public final void a(RecyclerView recyclerView, cf cfVar) {
        super.a(recyclerView, cfVar);
        cfVar.a.setAlpha(1.0f);
        ((at) cfVar).u();
    }

    public final void a(cf cfVar) {
    }

    public final void a(cf cfVar, int i) {
        if (i != 0) {
            at atVar = (at) cfVar;
            this.c = atVar.v();
            this.d = this.a.C.length - 1;
            atVar.t();
        }
        super.a(cfVar, i);
    }

    public final boolean a(cf cfVar, cf cfVar2) {
        int v = ((at) cfVar2).v();
        aq aqVar = this.b;
        int i = this.c;
        String str = aqVar.a.C[i];
        aqVar.a.C[i] = aqVar.a.C[v];
        aqVar.a.C[v] = str;
        int[] iArr = aqVar.a.B[i];
        aqVar.a.B[i] = aqVar.a.B[v];
        aqVar.a.B[v] = iArr;
        aqVar.a_(i, v);
        this.c = v;
        return true;
    }

    public final boolean c() {
        return true;
    }

    public final boolean d() {
        return false;
    }
}
