package com.fastnet.browseralam.settings;

import android.widget.CompoundButton;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class e implements CompoundButton.OnCheckedChangeListener {
    final /* synthetic */ AdvancedSettingsActivity a;

    e(AdvancedSettingsActivity advancedSettingsActivity) {
        this.a = advancedSettingsActivity;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        switch (compoundButton.getId()) {
            case R.id.cbOpenLinksBackground:
                a.B(z);
                return;
            case R.id.rRestoreTabs:
            case R.id.rExitOnTabClose:
            case R.id.rInterceptVideo:
            case R.id.rHardwareRendering:
            case R.id.rAdblockList:
            case R.id.adblock_list:
            case R.id.rOnlyCustomAdblock:
            default:
                return;
            case R.id.cbRestoreTabs:
                a.C(z);
                return;
            case R.id.cbExitOnTabClose:
                a.D(z);
                return;
            case R.id.cbInterceptVideo:
                a.F(z);
                if (z) {
                    AdvancedSettingsActivity.i(this.a);
                    return;
                }
                return;
            case R.id.cbHardwareRendering:
                a.E(z);
                return;
            case R.id.cbOnlyCustomAdblock:
                a.G(z);
                return;
        }
    }
}
