package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import android.webkit.WebView;
import com.fastnet.browseralam.i.aq;

final class cb implements DialogInterface.OnClickListener {
    final /* synthetic */ bz a;

    cb(bz bzVar) {
        this.a = bzVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        new WebView(this.a.a.E).clearCache(true);
        aq.a(this.a.a.E, "Cache cleared");
    }
}
