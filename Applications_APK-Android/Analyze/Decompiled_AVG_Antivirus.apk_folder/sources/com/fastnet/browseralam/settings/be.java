package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class be implements DialogInterface.OnClickListener {
    final /* synthetic */ bd a;

    be(bd bdVar) {
        this.a = bdVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        a unused = this.a.a.j;
        a.g(i + 1);
        switch (i + 1) {
            case 1:
                this.a.a.m.setText(this.a.a.getResources().getString(R.string.agent_default));
                a unused2 = this.a.a.j;
                a.h((String) null);
                return;
            case 2:
                this.a.a.m.setText(this.a.a.getResources().getString(R.string.agent_desktop));
                a unused3 = this.a.a.j;
                a.h("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36");
                return;
            case 3:
                this.a.a.m.setText(this.a.a.getResources().getString(R.string.agent_mobile));
                a unused4 = this.a.a.j;
                a.h("Mozilla/5.0 (Linux; U; Android 4.4; en-us; Nexus 4 Build/JOP24G) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
                return;
            case 4:
                this.a.a.m.setText(this.a.a.getResources().getString(R.string.agent_custom));
                this.a.a.d();
                return;
            default:
                return;
        }
    }
}
