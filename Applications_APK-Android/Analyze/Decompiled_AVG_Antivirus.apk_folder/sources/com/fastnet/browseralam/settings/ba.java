package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class ba implements DialogInterface.OnClickListener {
    final /* synthetic */ az a;

    ba(az azVar) {
        this.a = azVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        a unused = this.a.a.j;
        a.d(i);
        switch (i) {
            case 0:
                GeneralSettingsActivity generalSettingsActivity = this.a.a;
                AlertDialog.Builder builder = new AlertDialog.Builder(generalSettingsActivity);
                builder.setTitle(generalSettingsActivity.getResources().getString(R.string.custom_url));
                EditText editText = new EditText(generalSettingsActivity);
                editText.setText(a.ac());
                builder.setView(editText);
                builder.setPositiveButton(generalSettingsActivity.getResources().getString(R.string.action_ok), new bc(generalSettingsActivity, editText));
                builder.show();
                return;
            case 1:
                this.a.a.p.setText("Google");
                return;
            case 2:
                this.a.a.p.setText("Ask");
                return;
            case 3:
                this.a.a.p.setText("Bing");
                return;
            case 4:
                this.a.a.p.setText("Yahoo");
                return;
            case 5:
                this.a.a.p.setText("StartPage");
                return;
            case 6:
                this.a.a.p.setText("StartPage (Mobile)");
                return;
            case 7:
                this.a.a.p.setText("DuckDuckGo");
                return;
            case 8:
                this.a.a.p.setText("DuckDuckGo Lite");
                return;
            case 9:
                this.a.a.p.setText("Baidu");
                return;
            case 10:
                this.a.a.p.setText("Yandex");
                return;
            default:
                return;
        }
    }
}
