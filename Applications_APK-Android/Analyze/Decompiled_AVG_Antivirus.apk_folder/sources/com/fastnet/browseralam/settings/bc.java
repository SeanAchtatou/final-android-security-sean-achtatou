package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import android.widget.EditText;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class bc implements DialogInterface.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ GeneralSettingsActivity b;

    bc(GeneralSettingsActivity generalSettingsActivity, EditText editText) {
        this.b = generalSettingsActivity;
        this.a = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String obj = this.a.getText().toString();
        a unused = this.b.j;
        a.g(obj);
        this.b.p.setText(this.b.getResources().getString(R.string.custom_url) + ": " + obj);
    }
}
