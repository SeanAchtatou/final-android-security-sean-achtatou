package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.activity.a;
import com.fastnet.browseralam.activity.ey;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.i.aq;
import com.google.android.gms.common.internal.ImagesContract;
import java.io.File;

public final class x {
    /* access modifiers changed from: private */
    public final BrowserActivity a;
    private final LayoutInflater b;
    /* access modifiers changed from: private */
    public final a c;

    public x(BrowserActivity browserActivity, a aVar) {
        this.a = browserActivity;
        this.b = browserActivity.getLayoutInflater();
        this.c = aVar;
    }

    static /* synthetic */ void a(x xVar, AlertDialog alertDialog, File file) {
        AlertDialog.Builder builder = new AlertDialog.Builder(xVar.a);
        View inflate = xVar.a.getLayoutInflater().inflate((int) R.layout.bookmark_import_confirm, (ViewGroup) null);
        builder.setView(inflate);
        AlertDialog create = builder.create();
        inflate.findViewById(R.id.yes).setOnClickListener(new ad(xVar, file, create, alertDialog));
        inflate.findViewById(R.id.no).setOnClickListener(new ae(xVar, file, create, alertDialog));
        inflate.findViewById(R.id.cancel).setOnClickListener(new af(xVar, create));
        create.show();
    }

    static /* synthetic */ void a(x xVar, TextView textView) {
        String charSequence = textView.getText().toString();
        AlertDialog.Builder builder = new AlertDialog.Builder(xVar.a);
        LinearLayout linearLayout = new LinearLayout(xVar.a);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(1);
        linearLayout.setPadding(60, 10, 60, 0);
        builder.setTitle("Rename File");
        EditText editText = new EditText(xVar.a);
        editText.setHint("enter name");
        editText.setText(textView.getText().toString());
        editText.setSingleLine();
        linearLayout.addView(editText);
        builder.setView(linearLayout);
        builder.setPositiveButton("OK", new ag(xVar, textView, editText));
        builder.setNegativeButton("Cancel", new ah(xVar, editText));
        builder.show();
        if (charSequence.endsWith(".txt")) {
            editText.setSelection(0, charSequence.length() - 4);
        } else {
            editText.selectAll();
        }
        k.a(new z(xVar, editText));
    }

    public final void a() {
        View inflate = this.b.inflate((int) R.layout.export_bar, (ViewGroup) null);
        inflate.findViewById(R.id.cancel_export).setVisibility(8);
        inflate.findViewById(R.id.export_here).setVisibility(8);
        TextView textView = (TextView) inflate.findViewById(R.id.export_name);
        textView.setOnClickListener(new y(this, textView));
        new ey(this.a, new aa(this, textView)).a(inflate).a("Kirim penanda").b("KIRIM DISINI").b().c();
    }

    public final void b() {
        new ey(this.a, new ac(this)).a(new ab(this)).b("BATAL").a("Ambil penanda").b().c();
    }

    public final void c() {
        com.fastnet.browseralam.c.a aVar = new com.fastnet.browseralam.c.a(this.c.b(), "chrome");
        String[] strArr = {"title", ImagesContract.URL};
        Cursor query = this.a.getContentResolver().query(Uri.parse("content://com.android.chrome.browser/bookmarks"), strArr, "bookmark = 1", null, null);
        if (query == null) {
            aq.a(this.a, "No bookmarks found");
            return;
        }
        if (query.moveToFirst()) {
            do {
                aVar.a(new b(query.getString(0), query.getString(1), null, null));
            } while (query.moveToNext());
        }
        query.close();
        if (aVar.f().size() > 1) {
            this.c.b().a(aVar);
            aq.a(this.a, "Chrome bookmarks import success");
            return;
        }
        aq.a(this.a, "No bookmarks found");
    }
}
