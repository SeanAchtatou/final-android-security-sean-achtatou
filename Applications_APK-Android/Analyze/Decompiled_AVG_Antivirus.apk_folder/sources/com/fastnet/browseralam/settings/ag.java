package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

final class ag implements DialogInterface.OnClickListener {
    final /* synthetic */ TextView a;
    final /* synthetic */ EditText b;
    final /* synthetic */ x c;

    ag(x xVar, TextView textView, EditText editText) {
        this.c = xVar;
        this.a = textView;
        this.b = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.setText(this.b.getText().toString());
        ((InputMethodManager) this.c.a.getSystemService("input_method")).hideSoftInputFromWindow(this.b.getWindowToken(), 0);
    }
}
