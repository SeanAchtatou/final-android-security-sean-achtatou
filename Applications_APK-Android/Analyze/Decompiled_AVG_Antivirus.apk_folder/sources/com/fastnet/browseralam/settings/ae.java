package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import com.fastnet.browseralam.i.aq;
import java.io.File;

final class ae implements View.OnClickListener {
    final /* synthetic */ File a;
    final /* synthetic */ AlertDialog b;
    final /* synthetic */ AlertDialog c;
    final /* synthetic */ x d;

    ae(x xVar, File file, AlertDialog alertDialog, AlertDialog alertDialog2) {
        this.d = xVar;
        this.a = file;
        this.b = alertDialog;
        this.c = alertDialog2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.a.a(java.io.File, boolean):boolean
     arg types: [java.io.File, int]
     candidates:
      com.fastnet.browseralam.activity.a.a(com.fastnet.browseralam.activity.a, android.widget.PopupWindow):android.widget.PopupWindow
      com.fastnet.browseralam.activity.a.a(com.fastnet.browseralam.activity.a, com.fastnet.browseralam.e.p):com.fastnet.browseralam.e.p
      com.fastnet.browseralam.activity.a.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.b.f.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.activity.a.a(java.io.File, boolean):boolean */
    public final void onClick(View view) {
        if (this.d.c.a(this.a, false)) {
            aq.a(this.d.a, "Bookmarks import success");
            this.b.dismiss();
            this.c.dismiss();
            return;
        }
        this.b.dismiss();
        aq.a(this.d.a, "Bookmarks import failed");
    }
}
