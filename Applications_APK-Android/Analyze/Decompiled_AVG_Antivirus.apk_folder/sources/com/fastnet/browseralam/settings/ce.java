package com.fastnet.browseralam.settings;

import android.os.Build;
import android.view.View;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;

final class ce implements View.OnClickListener {
    final /* synthetic */ PrivacySettingsActivity a;

    ce(PrivacySettingsActivity privacySettingsActivity) {
        this.a = privacySettingsActivity;
    }

    public final void onClick(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.a.s.setChecked(!this.a.s.isChecked());
        } else {
            aq.a(this.a.E, this.a.E.getString(R.string.available_lollipop));
        }
    }
}
