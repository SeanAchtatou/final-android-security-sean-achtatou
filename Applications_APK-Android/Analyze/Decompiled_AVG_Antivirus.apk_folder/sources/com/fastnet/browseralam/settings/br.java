package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import com.fastnet.browseralam.R;

final class br implements View.OnClickListener {
    final /* synthetic */ PrivacySettingsActivity a;

    br(PrivacySettingsActivity privacySettingsActivity) {
        this.a = privacySettingsActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        builder.setTitle(this.a.getResources().getString(R.string.title_clear_history));
        builder.setMessage(this.a.getResources().getString(R.string.dialog_history)).setPositiveButton(this.a.getResources().getString(R.string.action_yes), new bt(this)).setNegativeButton(this.a.getResources().getString(R.string.action_no), new bs(this)).show();
    }
}
