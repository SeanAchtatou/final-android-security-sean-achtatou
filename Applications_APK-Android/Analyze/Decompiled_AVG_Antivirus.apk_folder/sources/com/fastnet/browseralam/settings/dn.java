package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;
import com.fastnet.browseralam.i.aq;

final class dn implements DialogInterface.OnCancelListener {
    final /* synthetic */ SettingsActivity a;

    dn(SettingsActivity settingsActivity) {
        this.a = settingsActivity;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        boolean z;
        a unused = this.a.j;
        if (a.l() == 0) {
            this.a.y.setChecked(false);
        }
        try {
            z = this.a.getPackageManager().getApplicationInfo("com.adobe.flashplayer", 0) != null;
        } catch (PackageManager.NameNotFoundException e) {
            z = false;
        }
        if (!z) {
            aq.a(this.a, this.a.getResources().getString(R.string.title_warning), this.a.getResources().getString(R.string.dialog_adobe_not_installed));
            this.a.y.setChecked(false);
            a unused2 = this.a.j;
            a.a(0);
        }
    }
}
