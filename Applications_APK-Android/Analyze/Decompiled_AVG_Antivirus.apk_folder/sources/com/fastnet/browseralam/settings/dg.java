package com.fastnet.browseralam.settings;

import android.widget.RadioGroup;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class dg implements RadioGroup.OnCheckedChangeListener {
    final /* synthetic */ SettingsActivity a;

    dg(SettingsActivity settingsActivity) {
        this.a = settingsActivity;
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.radioDrawers:
                a unused = this.a.j;
                a.b(0);
                return;
            case R.id.radioBottom:
                a unused2 = this.a.j;
                a.b(1);
                return;
            case R.id.radioCards:
                a unused3 = this.a.j;
                a.b(2);
                return;
            case R.id.radioTablet:
                a unused4 = this.a.j;
                a.b(3);
                return;
            default:
                return;
        }
    }
}
