package com.fastnet.browseralam.settings;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;

final class cl extends Handler {
    final Context a;

    public cl(Context context) {
        this.a = context;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                aq.a(this.a, this.a.getResources().getString(R.string.message_clear_history));
                break;
            case 2:
                aq.a(this.a, this.a.getResources().getString(R.string.message_cookies_cleared));
                break;
        }
        super.handleMessage(message);
    }
}
