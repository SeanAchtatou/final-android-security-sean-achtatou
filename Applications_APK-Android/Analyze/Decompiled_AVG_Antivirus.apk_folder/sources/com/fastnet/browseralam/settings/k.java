package com.fastnet.browseralam.settings;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.c.c;
import java.util.List;

final class k extends bi {
    final /* synthetic */ AdvancedSettingsActivity a;
    private final LinearLayoutManager b;
    /* access modifiers changed from: private */
    public final Activity c = this.a;
    private final int d = R.layout.url_item;
    private final s e = new s(this);
    /* access modifiers changed from: private */
    public c f;
    /* access modifiers changed from: private */
    public List g;
    /* access modifiers changed from: private */
    public PopupWindow h;

    public k(AdvancedSettingsActivity advancedSettingsActivity) {
        this.a = advancedSettingsActivity;
        RecyclerView unused = advancedSettingsActivity.m = (RecyclerView) advancedSettingsActivity.findViewById(R.id.adblock_list);
        advancedSettingsActivity.m.a(true);
        advancedSettingsActivity.m.a(this);
        this.b = new LinearLayoutManager();
        advancedSettingsActivity.m.a(this.b);
        this.f = c.a(this.c);
        advancedSettingsActivity.o.setOnClickListener(new l(this, advancedSettingsActivity));
    }

    public final int a() {
        if (this.g != null) {
            return this.g.size();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ cf a(ViewGroup viewGroup, int i) {
        return new v(this, this.c.getLayoutInflater().inflate((int) R.layout.url_item, viewGroup, false));
    }

    public final /* synthetic */ void a(cf cfVar, int i) {
        v vVar = (v) cfVar;
        int unused = vVar.o = i;
        vVar.m.setText((CharSequence) this.g.get(i));
        vVar.n.setTag(vVar);
        vVar.n.setOnClickListener(this.e);
    }

    public final void d() {
        this.a.w.setText("Adblock Settings");
        this.a.l.setVisibility(8);
        this.a.m.setVisibility(0);
        this.a.n.setVisibility(0);
        this.a.o.setVisibility(0);
        this.a.p.setVisibility(0);
        if (this.g == null) {
            this.g = c.d();
        }
    }
}
