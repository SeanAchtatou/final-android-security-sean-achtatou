package com.fastnet.browseralam.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

public class GeneralSettingsActivity extends ThemableSettingsActivity {
    private static final int i = Build.VERSION.SDK_INT;
    /* access modifiers changed from: private */
    public a j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public TextView m;
    /* access modifiers changed from: private */
    public TextView n;
    /* access modifiers changed from: private */
    public TextView o;
    /* access modifiers changed from: private */
    public TextView p;
    /* access modifiers changed from: private */
    public TextView q;
    /* access modifiers changed from: private */
    public CheckBox r;
    /* access modifiers changed from: private */
    public Activity s;
    /* access modifiers changed from: private */
    public CharSequence[] t;

    public final void d() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.s);
        builder.setTitle(getResources().getString(R.string.title_user_agent));
        EditText editText = new EditText(this);
        builder.setView(editText);
        builder.setPositiveButton(getResources().getString(R.string.action_ok), new bh(this, editText));
        builder.show();
    }

    public final void e() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.s);
        builder.setTitle(getResources().getString(R.string.title_custom_homepage));
        EditText editText = new EditText(this);
        this.l = a.u();
        if (!this.l.startsWith("about:")) {
            editText.setText(this.l);
        } else {
            editText.setText("http://www.google.com");
        }
        builder.setView(editText);
        builder.setPositiveButton(getResources().getString(R.string.action_ok), new bj(this, editText));
        builder.show();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.general_settings);
        a((Toolbar) findViewById(R.id.toolbar));
        c().a(true);
        this.j = a.a();
        this.s = this;
        this.p = (TextView) findViewById(R.id.searchText);
        switch (a.ab()) {
            case 0:
                this.p.setText(getResources().getString(R.string.custom_url));
                break;
            case 1:
                this.p.setText("Google");
                break;
            case 2:
                this.p.setText("Ask");
                break;
            case 3:
                this.p.setText("Bing");
                break;
            case 4:
                this.p.setText("Yahoo");
                break;
            case 5:
                this.p.setText("StartPage");
                break;
            case 6:
                this.p.setText("StartPage (Mobile)");
                break;
            case 7:
                this.p.setText("DuckDuckGo");
                break;
            case 8:
                this.p.setText("DuckDuckGo Lite");
                break;
            case 9:
                this.p.setText("Baidu");
                break;
            case 10:
                this.p.setText("Yandex");
                break;
        }
        this.m = (TextView) findViewById(R.id.agentText);
        this.o = (TextView) findViewById(R.id.homepageText);
        this.n = (TextView) findViewById(R.id.downloadText);
        this.k = a.ak();
        this.l = a.u();
        this.n.setText(a.k());
        if (this.l.contains("about:home")) {
            this.o.setText(getResources().getString(R.string.action_homepage));
        } else if (this.l.contains("about:blank")) {
            this.o.setText(getResources().getString(R.string.action_blank));
        } else if (this.l.contains("about:bookmarks")) {
            this.o.setText(getResources().getString(R.string.action_bookmarks));
        } else {
            this.o.setText(this.l);
        }
        switch (this.k) {
            case 1:
                this.m.setText(getResources().getString(R.string.agent_default));
                break;
            case 2:
                this.m.setText(getResources().getString(R.string.agent_desktop));
                break;
            case 3:
                this.m.setText(getResources().getString(R.string.agent_mobile));
                break;
            case 4:
                this.m.setText(getResources().getString(R.string.agent_custom));
                break;
        }
        this.r = (CheckBox) findViewById(R.id.cbGoogleSuggestions);
        this.r.setChecked(a.r());
        findViewById(R.id.layoutDownload).setOnClickListener(new aw(this));
        this.q = (TextView) findViewById(R.id.urlText);
        this.t = getResources().getStringArray(R.array.url_content_array);
        this.q.setText(this.t[a.ah()]);
        findViewById(R.id.rUrlBarContents).setOnClickListener(new bn(this));
        ((RelativeLayout) findViewById(R.id.layoutUserAgent)).setOnClickListener(new bd(this));
        ((RelativeLayout) findViewById(R.id.layoutHomepage)).setOnClickListener(new bk(this));
        ((RelativeLayout) findViewById(R.id.layoutSearch)).setOnClickListener(new az(this));
        ((RelativeLayout) findViewById(R.id.rGoogleSuggestions)).setOnClickListener(new ay(this));
        this.r.setOnCheckedChangeListener(new ax(this));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        finish();
        return true;
    }
}
