package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import android.widget.CheckedTextView;
import com.fastnet.browseralam.h.a;

final class g implements View.OnClickListener {
    final /* synthetic */ CheckedTextView a;
    final /* synthetic */ AlertDialog b;
    final /* synthetic */ AdvancedSettingsActivity c;

    g(AdvancedSettingsActivity advancedSettingsActivity, CheckedTextView checkedTextView, AlertDialog alertDialog) {
        this.c = advancedSettingsActivity;
        this.a = checkedTextView;
        this.b = alertDialog;
    }

    public final void onClick(View view) {
        if (this.a.isChecked()) {
            a.an();
        }
        this.b.dismiss();
    }
}
