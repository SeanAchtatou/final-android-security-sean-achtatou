package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import android.widget.EditText;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class bh implements DialogInterface.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ GeneralSettingsActivity b;

    bh(GeneralSettingsActivity generalSettingsActivity, EditText editText) {
        this.b = generalSettingsActivity;
        this.a = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String obj = this.a.getText().toString();
        a unused = this.b.j;
        a.h(obj);
        this.b.m.setText(this.b.getResources().getString(R.string.agent_custom));
    }
}
