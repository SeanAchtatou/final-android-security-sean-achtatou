package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import com.fastnet.browseralam.h.a;
import com.jobstrak.drawingfun.lib.mopub.mobileads.resource.DrawableConstants;

final class ai implements DialogInterface.OnClickListener {
    final /* synthetic */ DisplaySettingsActivity a;

    ai(DisplaySettingsActivity displaySettingsActivity) {
        this.a = displaySettingsActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i + 1) {
            case 1:
                a.e(200);
                break;
            case 2:
                a.e((int) DrawableConstants.CtaButton.WIDTH_DIPS);
                break;
            case 3:
                a.e(100);
                break;
            case 4:
                a.e(75);
                break;
            case 5:
                a.e(50);
                break;
        }
        this.a.l.setText(DisplaySettingsActivity.e());
    }
}
