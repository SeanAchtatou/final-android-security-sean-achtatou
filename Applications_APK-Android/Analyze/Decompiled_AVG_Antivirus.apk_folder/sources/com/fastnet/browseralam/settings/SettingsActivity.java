package com.fastnet.browseralam.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

public class SettingsActivity extends ThemableSettingsActivity {
    private static final int i = Build.VERSION.SDK_INT;
    /* access modifiers changed from: private */
    public CheckBox A;
    /* access modifiers changed from: private */
    public CheckBox B;
    /* access modifiers changed from: private */
    public CheckBox C;
    /* access modifiers changed from: private */
    public CheckBox D;
    /* access modifiers changed from: private */
    public CheckBox E;
    /* access modifiers changed from: private */
    public CheckBox F;
    /* access modifiers changed from: private */
    public CheckBox G;
    /* access modifiers changed from: private */
    public CheckBox H;
    /* access modifiers changed from: private */
    public CheckBox I;
    /* access modifiers changed from: private */
    public TextView J;
    /* access modifiers changed from: private */
    public boolean K;
    private boolean L;
    /* access modifiers changed from: private */
    public int M;
    /* access modifiers changed from: private */
    public a j;
    /* access modifiers changed from: private */
    public Context k;
    private Activity l;
    private RelativeLayout m;
    private RelativeLayout n;
    private RelativeLayout o;
    private RelativeLayout p;
    private RelativeLayout q;
    private RelativeLayout r;
    private RelativeLayout s;
    private RelativeLayout t;
    private RelativeLayout u;
    private RelativeLayout v;
    /* access modifiers changed from: private */
    public RelativeLayout w;
    /* access modifiers changed from: private */
    public RelativeLayout x;
    /* access modifiers changed from: private */
    public CheckBox y;
    /* access modifiers changed from: private */
    public CheckBox z;

    static /* synthetic */ void a(SettingsActivity settingsActivity) {
        RelativeLayout relativeLayout = (RelativeLayout) settingsActivity.getLayoutInflater().inflate((int) R.layout.layout_chooser, (ViewGroup) null);
        AlertDialog.Builder builder = new AlertDialog.Builder(settingsActivity.l);
        builder.setView(relativeLayout);
        AlertDialog create = builder.create();
        RadioButton radioButton = (RadioButton) relativeLayout.findViewById(R.id.radioDrawers);
        RadioButton radioButton2 = (RadioButton) relativeLayout.findViewById(R.id.radioBottom);
        RadioButton radioButton3 = (RadioButton) relativeLayout.findViewById(R.id.radioCards);
        RadioButton radioButton4 = (RadioButton) relativeLayout.findViewById(R.id.radioTablet);
        switch (settingsActivity.M) {
            case 0:
                radioButton.setChecked(true);
                break;
            case 1:
                radioButton2.setChecked(true);
                break;
            case 2:
                radioButton3.setChecked(true);
                break;
            case 3:
                radioButton4.setChecked(true);
                break;
        }
        ((RadioGroup) relativeLayout.findViewById(R.id.radios)).setOnCheckedChangeListener(new dg(settingsActivity));
        relativeLayout.findViewById(R.id.layoutDrawers).setOnClickListener(new dh(settingsActivity, radioButton));
        relativeLayout.findViewById(R.id.layoutBottom).setOnClickListener(new dj(settingsActivity, radioButton2));
        relativeLayout.findViewById(R.id.layoutCards).setOnClickListener(new dk(settingsActivity, radioButton3));
        relativeLayout.findViewById(R.id.ok).setOnClickListener(new dl(settingsActivity, create, radioButton2, radioButton, radioButton3));
        relativeLayout.findViewById(R.id.cancel).setOnClickListener(new dm(settingsActivity, create));
        create.setCanceledOnTouchOutside(true);
        create.show();
    }

    static /* synthetic */ void c(SettingsActivity settingsActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(settingsActivity.l);
        builder.setTitle(settingsActivity.k.getResources().getString(R.string.title_flash));
        builder.setMessage(settingsActivity.getResources().getString(R.string.flash)).setCancelable(true).setPositiveButton(settingsActivity.getResources().getString(R.string.action_manual), new dq(settingsActivity)).setNegativeButton(settingsActivity.getResources().getString(R.string.action_auto), new dp(settingsActivity)).setNeutralButton(settingsActivity.getResources().getString(R.string.action_cancel), new Cdo(settingsActivity)).setOnCancelListener(new dn(settingsActivity));
        AlertDialog create = builder.create();
        create.setCanceledOnTouchOutside(true);
        create.show();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z2 = true;
        super.onCreate(bundle);
        setContentView((int) R.layout.settings);
        this.k = this;
        this.l = this;
        a((Toolbar) findViewById(R.id.toolbar));
        c().a(true);
        this.j = a.a();
        this.M = a.B();
        this.K = a.o();
        this.m = (RelativeLayout) findViewById(R.id.layoutFlash);
        this.n = (RelativeLayout) findViewById(R.id.layoutAdBlock);
        this.o = (RelativeLayout) findViewById(R.id.layoutImages);
        this.p = (RelativeLayout) findViewById(R.id.rBlockPopups);
        this.q = (RelativeLayout) findViewById(R.id.rBlockPopupWindows);
        this.r = (RelativeLayout) findViewById(R.id.layoutEnableJS);
        this.s = (RelativeLayout) findViewById(R.id.layoutUseOrbot);
        this.u = (RelativeLayout) findViewById(R.id.layoutRefresh);
        this.t = (RelativeLayout) findViewById(R.id.layoutSwipeTabs);
        this.w = (RelativeLayout) findViewById(R.id.layoutBottomToolbar);
        this.x = (RelativeLayout) findViewById(R.id.rFullscreenDrawer);
        this.J = (TextView) findViewById(R.id.app_layout);
        if (this.M == 0) {
            this.J.setText("Side Drawers");
            this.x.setVisibility(0);
            this.w.setVisibility(8);
        } else if (this.M == 1) {
            this.J.setText("Bottom Drawer");
            this.w.setVisibility(0);
            this.x.setVisibility(8);
        } else if (this.M == 2) {
            this.J.setText("Cards Layout");
            this.w.setVisibility(8);
            this.x.setVisibility(8);
        } else {
            this.J.setText("Tablet Mode");
            this.w.setVisibility(8);
            this.x.setVisibility(8);
        }
        if (i > 19) {
            this.m.setVisibility(8);
            findViewById(R.id.flash_divider).setVisibility(8);
        }
        this.L = a.l() != 0;
        boolean d = a.d();
        boolean y2 = a.y();
        this.y = (CheckBox) findViewById(R.id.cbFlash);
        this.z = (CheckBox) findViewById(R.id.cbAdblock);
        this.A = (CheckBox) findViewById(R.id.cbImageBlock);
        this.B = (CheckBox) findViewById(R.id.cbBlockPopups);
        this.C = (CheckBox) findViewById(R.id.cbBlockPopupWindows);
        this.D = (CheckBox) findViewById(R.id.cbJavascript);
        this.E = (CheckBox) findViewById(R.id.cbOrbot);
        this.F = (CheckBox) findViewById(R.id.cbRefresh);
        this.G = (CheckBox) findViewById(R.id.cbSwipeTabs);
        this.H = (CheckBox) findViewById(R.id.cbBottomToolbar);
        this.I = (CheckBox) findViewById(R.id.cbFullscreenDrawer);
        this.D.setChecked(y2);
        CheckBox checkBox = this.y;
        if (a.l() == 0) {
            z2 = false;
        }
        checkBox.setChecked(z2);
        this.z.setChecked(a.c());
        this.A.setChecked(d);
        this.B.setChecked(a.e());
        this.C.setChecked(a.I());
        this.E.setChecked(a.aj());
        this.F.setChecked(a.z());
        this.G.setChecked(a.A());
        this.H.setChecked(a.C());
        this.I.setChecked(this.K);
        this.y.setOnClickListener(new cv(this));
        this.z.setOnCheckedChangeListener(new cw(this));
        this.A.setOnCheckedChangeListener(new cy(this));
        this.B.setOnCheckedChangeListener(new cz(this));
        this.C.setOnCheckedChangeListener(new da(this));
        this.D.setOnCheckedChangeListener(new db(this));
        this.F.setOnCheckedChangeListener(new dc(this));
        this.G.setOnCheckedChangeListener(new dd(this));
        this.H.setOnCheckedChangeListener(new de(this));
        this.I.setOnCheckedChangeListener(new df(this));
        this.m.setOnClickListener(new du(this));
        this.n.setOnClickListener(new dv(this));
        this.o.setOnClickListener(new dw(this));
        this.p.setOnClickListener(new cn(this));
        this.q.setOnClickListener(new co(this));
        this.r.setOnClickListener(new cp(this));
        this.s.setOnClickListener(new cq(this));
        this.u.setOnClickListener(new cr(this));
        this.t.setOnClickListener(new cs(this));
        this.w.setOnClickListener(new ct(this));
        this.x.setOnClickListener(new cu(this));
        this.v = (RelativeLayout) findViewById(R.id.layoutAppLayout);
        this.v.setOnClickListener(new cm(this));
        findViewById(R.id.layoutGeneral).setOnClickListener(new cx(this));
        findViewById(R.id.layoutDisplay).setOnClickListener(new di(this));
        findViewById(R.id.layoutPrivacy).setOnClickListener(new dr(this));
        findViewById(R.id.layoutAdvanced).setOnClickListener(new ds(this));
        findViewById(R.id.layoutAbout).setOnClickListener(new dt(this));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        finish();
        return true;
    }
}
