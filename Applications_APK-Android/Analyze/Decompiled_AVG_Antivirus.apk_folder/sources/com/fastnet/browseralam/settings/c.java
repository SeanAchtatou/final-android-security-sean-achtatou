package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import com.fastnet.browseralam.i.q;
import com.fastnet.browseralam.i.r;

final class c implements q {
    final /* synthetic */ ProgressDialog a;
    final /* synthetic */ AlertDialog b;
    final /* synthetic */ AboutSettingsActivity c;
    private r d;

    c(AboutSettingsActivity aboutSettingsActivity, ProgressDialog progressDialog, AlertDialog alertDialog) {
        this.c = aboutSettingsActivity;
        this.a = progressDialog;
        this.b = alertDialog;
    }

    public final void a() {
        this.d.a();
    }

    public final void a(r rVar) {
        this.d = rVar;
    }

    public final void a(boolean z) {
        this.a.dismiss();
        if (!z) {
            this.b.show();
        }
    }
}
