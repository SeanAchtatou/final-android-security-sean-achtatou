package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class bl implements DialogInterface.OnClickListener {
    final /* synthetic */ bk a;

    bl(bk bkVar) {
        this.a = bkVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (i + 1) {
            case 1:
                a unused = this.a.a.j;
                a.d("about:home");
                this.a.a.o.setText(this.a.a.getResources().getString(R.string.action_homepage));
                return;
            case 2:
                a unused2 = this.a.a.j;
                a.d("about:blank");
                this.a.a.o.setText(this.a.a.getResources().getString(R.string.action_blank));
                return;
            case 3:
                a unused3 = this.a.a.j;
                a.d("about:bookmarks");
                this.a.a.o.setText(this.a.a.getResources().getString(R.string.action_bookmarks));
                return;
            case 4:
                this.a.a.e();
                return;
            default:
                return;
        }
    }
}
