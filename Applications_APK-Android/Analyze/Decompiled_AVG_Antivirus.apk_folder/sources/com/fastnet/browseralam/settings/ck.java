package com.fastnet.browseralam.settings;

import android.widget.CompoundButton;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class ck implements CompoundButton.OnCheckedChangeListener {
    final /* synthetic */ PrivacySettingsActivity a;

    private ck(PrivacySettingsActivity privacySettingsActivity) {
        this.a = privacySettingsActivity;
    }

    /* synthetic */ ck(PrivacySettingsActivity privacySettingsActivity, byte b) {
        this(privacySettingsActivity);
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        switch (compoundButton.getId()) {
            case R.id.cbLocation:
                a unused = this.a.j;
                a.y(z);
                return;
            case R.id.rAllowCookies:
            case R.id.rAllowIncognitoCookies:
            case R.id.rThirdParty:
            case R.id.rSavePasswords:
            case R.id.rBrowserHistory:
            case R.id.rClearCacheExit:
            case R.id.rClearHistoryExit:
            case R.id.rClearCookiesExit:
            default:
                return;
            case R.id.cbAllowCookies:
                a unused2 = this.a.j;
                a.i(z);
                return;
            case R.id.cbAllowIncognitoCookies:
                a unused3 = this.a.j;
                a.q(z);
                return;
            case R.id.cbThirdParty:
                a unused4 = this.a.j;
                a.e(z);
                return;
            case R.id.cbSavePasswords:
                a unused5 = this.a.j;
                a.H(z);
                return;
            case R.id.cbBrowserHistory:
                a unused6 = this.a.j;
                a.I(z);
                return;
            case R.id.cbClearCacheExit:
                a unused7 = this.a.j;
                a.f(z);
                return;
            case R.id.cbClearHistoryExit:
                a unused8 = this.a.j;
                a.h(z);
                return;
            case R.id.cbClearCookiesExit:
                a unused9 = this.a.j;
                a.g(z);
                return;
        }
    }
}
