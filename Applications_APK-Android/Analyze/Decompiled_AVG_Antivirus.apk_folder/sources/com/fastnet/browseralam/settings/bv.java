package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import com.fastnet.browseralam.R;

final class bv implements View.OnClickListener {
    final /* synthetic */ PrivacySettingsActivity a;

    bv(PrivacySettingsActivity privacySettingsActivity) {
        this.a = privacySettingsActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        builder.setTitle(this.a.getResources().getString(R.string.title_clear_cookies));
        builder.setMessage(this.a.getResources().getString(R.string.dialog_cookies)).setPositiveButton(this.a.getResources().getString(R.string.action_yes), new bx(this)).setNegativeButton(this.a.getResources().getString(R.string.action_no), new bw(this)).show();
    }
}
