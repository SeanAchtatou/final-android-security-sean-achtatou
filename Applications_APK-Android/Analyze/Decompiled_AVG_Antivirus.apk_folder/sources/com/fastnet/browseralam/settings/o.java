package com.fastnet.browseralam.settings;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class o implements Runnable {
    final /* synthetic */ EditText a;
    final /* synthetic */ k b;

    o(k kVar, EditText editText) {
        this.b = kVar;
        this.a = editText;
    }

    public final void run() {
        ((InputMethodManager) this.b.c.getSystemService("input_method")).showSoftInput(this.a, 1);
    }
}
