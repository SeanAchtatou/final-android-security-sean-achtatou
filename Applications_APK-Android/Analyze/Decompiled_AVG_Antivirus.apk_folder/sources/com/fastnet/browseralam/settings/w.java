package com.fastnet.browseralam.settings;

import android.view.View;
import android.widget.CheckBox;
import com.fastnet.browseralam.R;

final class w implements View.OnClickListener {
    final /* synthetic */ AdvancedSettingsActivity a;

    private w(AdvancedSettingsActivity advancedSettingsActivity) {
        this.a = advancedSettingsActivity;
    }

    /* synthetic */ w(AdvancedSettingsActivity advancedSettingsActivity, byte b) {
        this(advancedSettingsActivity);
    }

    public final void onClick(View view) {
        boolean z = true;
        switch (view.getId()) {
            case R.id.help:
                AdvancedSettingsActivity.h(this.a);
                return;
            case R.id.rOpenLinksBackground:
                CheckBox a2 = this.a.q;
                if (this.a.q.isChecked()) {
                    z = false;
                }
                a2.setChecked(z);
                return;
            case R.id.cbOpenLinksBackground:
            case R.id.cbRestoreTabs:
            case R.id.cbExitOnTabClose:
            case R.id.cbInterceptVideo:
            case R.id.cbHardwareRendering:
            case R.id.adblock_list:
            default:
                return;
            case R.id.rRestoreTabs:
                CheckBox b = this.a.r;
                if (this.a.r.isChecked()) {
                    z = false;
                }
                b.setChecked(z);
                return;
            case R.id.rExitOnTabClose:
                CheckBox c = this.a.t;
                if (this.a.t.isChecked()) {
                    z = false;
                }
                c.setChecked(z);
                return;
            case R.id.rInterceptVideo:
                CheckBox d = this.a.u;
                if (this.a.u.isChecked()) {
                    z = false;
                }
                d.setChecked(z);
                return;
            case R.id.rHardwareRendering:
                CheckBox e = this.a.s;
                if (this.a.s.isChecked()) {
                    z = false;
                }
                e.setChecked(z);
                return;
            case R.id.rAdblockList:
                this.a.k.d();
                return;
            case R.id.rOnlyCustomAdblock:
                CheckBox g = this.a.v;
                if (this.a.v.isChecked()) {
                    z = false;
                }
                g.setChecked(z);
                return;
        }
    }
}
