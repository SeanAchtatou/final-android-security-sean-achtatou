package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class az implements View.OnClickListener {
    final /* synthetic */ GeneralSettingsActivity a;

    az(GeneralSettingsActivity generalSettingsActivity) {
        this.a = generalSettingsActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.s);
        builder.setTitle(this.a.getResources().getString(R.string.title_search_engine));
        CharSequence[] charSequenceArr = {this.a.getResources().getString(R.string.custom_url), "Google", "Ask", "Bing", "Yahoo", "StartPage", "StartPage (Mobile)", "DuckDuckGo (Privacy)", "DuckDuckGo Lite (Privacy)", "Baidu (Chinese)", "Yandex (Russian)"};
        a unused = this.a.j;
        builder.setSingleChoiceItems(charSequenceArr, a.ab(), new ba(this));
        builder.setNeutralButton(this.a.getResources().getString(R.string.action_ok), new bb(this));
        builder.show();
    }
}
