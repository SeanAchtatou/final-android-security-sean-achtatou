package com.fastnet.browseralam.settings;

import android.view.View;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;

final class cq implements View.OnClickListener {
    final /* synthetic */ SettingsActivity a;

    cq(SettingsActivity settingsActivity) {
        this.a = settingsActivity;
    }

    public final void onClick(View view) {
        if (this.a.E.isEnabled()) {
            this.a.E.setChecked(!this.a.E.isChecked());
        } else {
            aq.a(this.a.k, this.a.getResources().getString(R.string.install_orbot));
        }
    }
}
