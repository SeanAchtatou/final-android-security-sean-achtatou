package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;

final class h implements View.OnClickListener {
    final /* synthetic */ AdvancedSettingsActivity a;

    h(AdvancedSettingsActivity advancedSettingsActivity) {
        this.a = advancedSettingsActivity;
    }

    public final void onClick(View view) {
        LinearLayout linearLayout = (LinearLayout) this.a.getLayoutInflater().inflate((int) R.layout.adblock_demo_dialog, (ViewGroup) null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.i);
        ((TextView) linearLayout.findViewById(R.id.url)).setText("http://www.LongPressMe.com");
        linearLayout.findViewById(R.id.url).setOnLongClickListener(new i(this, (TextView) linearLayout.findViewById(R.id.blockUrl)));
        builder.setView(linearLayout);
        builder.show();
    }
}
