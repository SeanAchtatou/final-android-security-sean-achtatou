package com.fastnet.browseralam.settings;

import android.widget.TextView;
import com.fastnet.browseralam.activity.fm;
import com.fastnet.browseralam.i.aq;
import java.io.File;

final class aa implements fm {
    final /* synthetic */ TextView a;
    final /* synthetic */ x b;

    aa(x xVar, TextView textView) {
        this.b = xVar;
        this.a = textView;
    }

    public final void a(String str) {
        String trim = this.a.getText().toString().trim();
        if (this.b.c.a(new File(str + (trim.equals("") ? "/bookmark-export.txt" : "/" + trim)))) {
            aq.a(this.b.a, "Bookmarks export success");
        } else {
            aq.a(this.b.a, "Bookmarks export failed");
        }
    }
}
