package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import com.fastnet.browseralam.activity.a;
import com.fastnet.browseralam.activity.fn;
import com.fastnet.browseralam.i.aq;
import java.io.File;

final class ab implements fn {
    final /* synthetic */ x a;

    ab(x xVar) {
        this.a = xVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.a.a(java.io.File, boolean):boolean
     arg types: [java.io.File, int]
     candidates:
      com.fastnet.browseralam.activity.a.a(com.fastnet.browseralam.activity.a, android.widget.PopupWindow):android.widget.PopupWindow
      com.fastnet.browseralam.activity.a.a(com.fastnet.browseralam.activity.a, com.fastnet.browseralam.e.p):com.fastnet.browseralam.e.p
      com.fastnet.browseralam.activity.a.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.b.f.a(com.fastnet.browseralam.view.XWebView, android.graphics.Bitmap):void
      com.fastnet.browseralam.activity.a.a(java.io.File, boolean):boolean */
    public final boolean a(AlertDialog alertDialog, String str) {
        if (str.endsWith(".txt")) {
            File file = new File(str);
            if (!a.b(file)) {
                aq.a(this.a.a, "No bookmarks found");
                return false;
            } else if (this.a.c.b().f().size() > 0) {
                x.a(this.a, alertDialog, file);
                return false;
            } else if (this.a.c.a(file, false)) {
                aq.a(this.a.a, "Bookmarks import success");
                return true;
            } else {
                aq.a(this.a.a, "Bookmarks import failed");
                return false;
            }
        } else {
            aq.a(this.a.a, "invalid file");
            return false;
        }
    }
}
