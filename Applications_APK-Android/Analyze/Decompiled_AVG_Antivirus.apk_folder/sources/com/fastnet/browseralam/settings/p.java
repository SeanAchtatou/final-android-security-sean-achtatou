package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

final class p implements View.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ AlertDialog b;
    final /* synthetic */ k c;

    p(k kVar, EditText editText, AlertDialog alertDialog) {
        this.c = kVar;
        this.a = editText;
        this.b = alertDialog;
    }

    public final void onClick(View view) {
        this.c.g.add(this.a.getText().toString());
        this.c.c_(this.c.g.size() - 1);
        this.b.dismiss();
        this.c.f.c((String) this.c.g.get(this.c.g.size() - 1));
    }
}
