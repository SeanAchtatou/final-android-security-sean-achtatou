package com.fastnet.browseralam.settings;

import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;

final class s implements View.OnClickListener {
    final /* synthetic */ k a;
    private int b = aq.a(78);
    /* access modifiers changed from: private */
    public v c;

    public s(k kVar) {
        this.a = kVar;
        View inflate = kVar.a.getLayoutInflater().inflate((int) R.layout.floating_url_edit, (ViewGroup) null);
        inflate.findViewById(R.id.edit).setOnClickListener(new t(this, kVar));
        inflate.findViewById(R.id.delete).setOnClickListener(new u(this, kVar));
        PopupWindow unused = kVar.h = new PopupWindow(inflate, aq.a(108), aq.a(54));
        kVar.h.setFocusable(true);
        kVar.h.setOutsideTouchable(true);
        kVar.h.setBackgroundDrawable(new ColorDrawable(0));
    }

    public final void onClick(View view) {
        this.c = (v) view.getTag();
        int unused = this.c.o = this.c.d();
        this.a.h.showAtLocation(view, 53, 0, this.c.a.getTop() + this.b);
    }
}
