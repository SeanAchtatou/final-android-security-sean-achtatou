package com.fastnet.browseralam.settings;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.a.a;
import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.view.l;
import com.jobstrak.drawingfun.lib.mopub.mobileads.resource.DrawableConstants;

final class aq extends bi {
    final /* synthetic */ DisplaySettingsActivity a;
    private final LinearLayoutManager b;
    private final Activity c = this.a;
    private final RecyclerView d;
    private final a e;
    private final int f = R.layout.dialog_item;
    /* access modifiers changed from: private */
    public final Drawable g = new ColorDrawable(-3355444);
    /* access modifiers changed from: private */
    public final int h;
    /* access modifiers changed from: private */
    public View.OnClickListener i = new ar(this);

    public aq(DisplaySettingsActivity displaySettingsActivity, View view) {
        this.a = displaySettingsActivity;
        this.d = (RecyclerView) view.findViewById(R.id.edit_dialog_list);
        this.d.a(true);
        this.d.a(this);
        this.b = new l();
        this.d.a(this.b);
        this.e = new a(new au(displaySettingsActivity, this));
        this.e.a(this.d);
        this.g.setAlpha(DrawableConstants.CtaButton.WIDTH_DIPS);
        TypedValue typedValue = new TypedValue();
        displaySettingsActivity.i.getTheme().resolveAttribute(16843534, typedValue, true);
        this.h = typedValue.resourceId;
    }

    public final int a() {
        if (this.a.B != null) {
            return this.a.C.length;
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ cf a(ViewGroup viewGroup, int i2) {
        return new as(this, this.c.getLayoutInflater().inflate((int) R.layout.dialog_item, viewGroup, false));
    }

    public final /* synthetic */ void a(cf cfVar, int i2) {
        boolean z = true;
        as asVar = (as) cfVar;
        asVar.n.setText(this.a.C[i2]);
        int unused = asVar.p = i2;
        asVar.o.setTag(Integer.valueOf(i2));
        CheckBox b2 = asVar.o;
        if (this.a.B[i2][1] != 1) {
            z = false;
        }
        b2.setChecked(z);
    }
}
