package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import com.fastnet.browseralam.R;

final class bz implements View.OnClickListener {
    final /* synthetic */ PrivacySettingsActivity a;

    bz(PrivacySettingsActivity privacySettingsActivity) {
        this.a = privacySettingsActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        builder.setTitle(this.a.getResources().getString(R.string.clear_cache));
        builder.setMessage(this.a.getResources().getString(R.string.dialog_cache)).setPositiveButton(this.a.getResources().getString(R.string.action_yes), new cb(this)).setNegativeButton(this.a.getResources().getString(R.string.action_no), new ca(this)).show();
    }
}
