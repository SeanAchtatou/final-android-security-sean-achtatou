package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import com.fastnet.browseralam.c.c;

final class m implements View.OnClickListener {
    final /* synthetic */ v a;
    final /* synthetic */ EditText b;
    final /* synthetic */ AlertDialog c;
    final /* synthetic */ int d;
    final /* synthetic */ k e;

    m(k kVar, v vVar, EditText editText, AlertDialog alertDialog, int i) {
        this.e = kVar;
        this.a = vVar;
        this.b = editText;
        this.c = alertDialog;
        this.d = i;
    }

    public final void onClick(View view) {
        this.a.m.setText(this.b.getText().toString());
        this.c.dismiss();
        c.a(this.d);
        this.e.f.c(this.a.m.getText().toString());
    }
}
