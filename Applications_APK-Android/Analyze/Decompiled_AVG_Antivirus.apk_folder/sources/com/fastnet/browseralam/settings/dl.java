package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import android.widget.RadioButton;
import com.fastnet.browseralam.h.a;

final class dl implements View.OnClickListener {
    final /* synthetic */ AlertDialog a;
    final /* synthetic */ RadioButton b;
    final /* synthetic */ RadioButton c;
    final /* synthetic */ RadioButton d;
    final /* synthetic */ SettingsActivity e;

    dl(SettingsActivity settingsActivity, AlertDialog alertDialog, RadioButton radioButton, RadioButton radioButton2, RadioButton radioButton3) {
        this.e = settingsActivity;
        this.a = alertDialog;
        this.b = radioButton;
        this.c = radioButton2;
        this.d = radioButton3;
    }

    public final void onClick(View view) {
        this.a.dismiss();
        a unused = this.e.j;
        int q = this.e.M;
        a unused2 = this.e.j;
        a.a(q != a.B());
        SettingsActivity settingsActivity = this.e;
        a unused3 = this.e.j;
        int unused4 = settingsActivity.M = a.B();
        if (this.b.isChecked()) {
            this.e.J.setText("Bottom Drawer");
            this.e.w.setVisibility(0);
            this.e.x.setVisibility(8);
        } else if (this.c.isChecked()) {
            this.e.J.setText("Side Drawers");
            this.e.x.setVisibility(0);
            this.e.w.setVisibility(8);
        } else if (this.d.isChecked()) {
            this.e.J.setText("Cards Layout");
            this.e.w.setVisibility(8);
            this.e.x.setVisibility(8);
        } else {
            this.e.J.setText("Tablet Mode");
            this.e.w.setVisibility(8);
            this.e.x.setVisibility(8);
        }
    }
}
