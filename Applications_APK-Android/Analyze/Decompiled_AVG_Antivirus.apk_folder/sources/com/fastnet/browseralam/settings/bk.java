package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class bk implements View.OnClickListener {
    final /* synthetic */ GeneralSettingsActivity a;

    bk(GeneralSettingsActivity generalSettingsActivity) {
        this.a = generalSettingsActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.s);
        builder.setTitle(this.a.getResources().getString(R.string.home));
        GeneralSettingsActivity generalSettingsActivity = this.a;
        a unused = this.a.j;
        String unused2 = generalSettingsActivity.l = a.u();
        builder.setSingleChoiceItems((int) R.array.homepage, (this.a.l.contains("about:home") ? 1 : this.a.l.contains("about:blank") ? 2 : this.a.l.contains("about:bookmarks") ? 3 : 4) - 1, new bl(this));
        builder.setNeutralButton(this.a.getResources().getString(R.string.action_ok), new bm(this));
        builder.show();
    }
}
