package com.fastnet.browseralam.settings;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebIconDatabase;
import android.webkit.WebStorage;
import android.webkit.WebViewDatabase;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;
import com.fastnet.browseralam.i.aq;

public class PrivacySettingsActivity extends ThemableSettingsActivity {
    private static final int i = Build.VERSION.SDK_INT;
    private RelativeLayout A;
    private RelativeLayout B;
    private RelativeLayout C;
    private RelativeLayout D;
    /* access modifiers changed from: private */
    public Context E;
    private boolean F;
    private Handler G;
    private ck H = new ck(this, (byte) 0);
    /* access modifiers changed from: private */
    public a j;
    /* access modifiers changed from: private */
    public CheckBox k;
    /* access modifiers changed from: private */
    public CheckBox l;
    /* access modifiers changed from: private */
    public CheckBox m;
    /* access modifiers changed from: private */
    public CheckBox n;
    /* access modifiers changed from: private */
    public CheckBox o;
    /* access modifiers changed from: private */
    public CheckBox p;
    /* access modifiers changed from: private */
    public CheckBox q;
    /* access modifiers changed from: private */
    public CheckBox r;
    /* access modifiers changed from: private */
    public CheckBox s;
    private RelativeLayout t;
    private RelativeLayout u;
    private RelativeLayout v;
    private RelativeLayout w;
    private RelativeLayout x;
    private RelativeLayout y;
    private RelativeLayout z;

    public final void d() {
        deleteDatabase("speedHistoryManager");
        WebViewDatabase instance = WebViewDatabase.getInstance(this);
        instance.clearFormData();
        instance.clearHttpAuthUsernamePassword();
        if (i < 18) {
            instance.clearUsernamePassword();
            WebIconDatabase.getInstance().removeAllIcons();
        }
        aq.a(this);
        this.G.sendEmptyMessage(1);
    }

    public final void e() {
        WebStorage.getInstance().deleteAllData();
        CookieManager instance = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT >= 21) {
            instance.removeAllCookies(null);
        } else {
            CookieSyncManager.createInstance(this);
            instance.removeAllCookie();
        }
        this.G.sendEmptyMessage(2);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.privacy_settings);
        a((Toolbar) findViewById(R.id.toolbar));
        c().a(true);
        this.j = a.a();
        this.F = a.ae();
        this.E = this;
        this.t = (RelativeLayout) findViewById(R.id.rLocation);
        this.x = (RelativeLayout) findViewById(R.id.rSavePasswords);
        this.y = (RelativeLayout) findViewById(R.id.rClearCacheExit);
        this.z = (RelativeLayout) findViewById(R.id.rClearHistoryExit);
        this.A = (RelativeLayout) findViewById(R.id.rClearCookiesExit);
        this.B = (RelativeLayout) findViewById(R.id.rClearCache);
        this.C = (RelativeLayout) findViewById(R.id.rClearHistory);
        this.D = (RelativeLayout) findViewById(R.id.rClearCookies);
        this.u = (RelativeLayout) findViewById(R.id.rAllowCookies);
        this.v = (RelativeLayout) findViewById(R.id.rAllowIncognitoCookies);
        this.w = (RelativeLayout) findViewById(R.id.rThirdParty);
        this.k = (CheckBox) findViewById(R.id.cbLocation);
        this.n = (CheckBox) findViewById(R.id.cbSavePasswords);
        this.p = (CheckBox) findViewById(R.id.cbClearCacheExit);
        this.q = (CheckBox) findViewById(R.id.cbClearHistoryExit);
        this.r = (CheckBox) findViewById(R.id.cbClearCookiesExit);
        this.l = (CheckBox) findViewById(R.id.cbAllowCookies);
        this.m = (CheckBox) findViewById(R.id.cbAllowIncognitoCookies);
        this.s = (CheckBox) findViewById(R.id.cbThirdParty);
        this.o = (CheckBox) findViewById(R.id.cbBrowserHistory);
        this.k.setChecked(a.F());
        this.n.setChecked(a.aa());
        this.p.setChecked(a.g());
        this.q.setChecked(a.i());
        this.r.setChecked(a.h());
        this.s.setChecked(a.f());
        this.l.setChecked(a.j());
        this.m.setChecked(a.v());
        this.s.setEnabled(Build.VERSION.SDK_INT >= 21);
        this.t.setOnClickListener(new cf(this));
        this.u.setOnClickListener(new cc(this));
        this.v.setOnClickListener(new cd(this));
        this.w.setOnClickListener(new ce(this));
        this.x.setOnClickListener(new cg(this));
        this.y.setOnClickListener(new ch(this));
        this.z.setOnClickListener(new ci(this));
        this.A.setOnClickListener(new cj(this));
        this.B.setOnClickListener(new bz(this));
        this.C.setOnClickListener(new br(this));
        this.D.setOnClickListener(new bv(this));
        this.k.setOnCheckedChangeListener(this.H);
        this.l.setOnCheckedChangeListener(this.H);
        this.m.setOnCheckedChangeListener(this.H);
        this.s.setOnCheckedChangeListener(this.H);
        this.n.setOnCheckedChangeListener(this.H);
        this.p.setOnCheckedChangeListener(this.H);
        this.q.setOnCheckedChangeListener(this.H);
        this.r.setOnCheckedChangeListener(this.H);
        this.o.setOnCheckedChangeListener(this.H);
        if (!this.F) {
            this.o.setChecked(false);
            this.o.setEnabled(false);
        } else {
            this.o.setEnabled(true);
            this.o.setChecked(a.ad());
            findViewById(R.id.rBrowserHistory).setOnClickListener(new bq(this));
        }
        this.G = new cl(this.E);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        finish();
        return true;
    }
}
