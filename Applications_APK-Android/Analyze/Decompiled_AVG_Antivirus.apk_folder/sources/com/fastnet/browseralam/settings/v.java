package com.fastnet.browseralam.settings;

import android.support.v7.widget.cf;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.fastnet.browseralam.R;

public final class v extends cf {
    final /* synthetic */ k l;
    /* access modifiers changed from: private */
    public final TextView m;
    /* access modifiers changed from: private */
    public final ImageView n;
    /* access modifiers changed from: private */
    public int o;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(k kVar, View view) {
        super(view);
        this.l = kVar;
        this.m = (TextView) view.findViewById(R.id.url);
        this.n = (ImageView) view.findViewById(R.id.menu);
    }
}
