package com.fastnet.browseralam.settings;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class z implements Runnable {
    final /* synthetic */ EditText a;
    final /* synthetic */ x b;

    z(x xVar, EditText editText) {
        this.b = xVar;
        this.a = editText;
    }

    public final void run() {
        ((InputMethodManager) this.b.a.getSystemService("input_method")).showSoftInput(this.a, 1);
    }
}
