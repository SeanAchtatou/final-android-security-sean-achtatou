package com.fastnet.browseralam.settings;

import android.widget.CompoundButton;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class ap implements CompoundButton.OnCheckedChangeListener {
    final /* synthetic */ DisplaySettingsActivity a;

    private ap(DisplaySettingsActivity displaySettingsActivity) {
        this.a = displaySettingsActivity;
    }

    /* synthetic */ ap(DisplaySettingsActivity displaySettingsActivity, byte b) {
        this(displaySettingsActivity);
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        switch (compoundButton.getId()) {
            case R.id.cbHideStatusBar:
                a.o(z);
                return;
            case R.id.rStackTabsTop:
            case R.id.rDrawerCards:
            case R.id.rFullScreen:
            case R.id.rDarkTheme:
            case R.id.rWideViewPort:
            case R.id.rOverView:
            case R.id.rResizeWindow:
            case R.id.rTextReflow:
            case R.id.textView3:
            default:
                return;
            case R.id.cbStackTabsTop:
                a.j(z);
                return;
            case R.id.cbDrawerCards:
                a.l(z);
                return;
            case R.id.cbFullScreen:
                a.m(z);
                return;
            case R.id.cbDarkTheme:
                a.K(z);
                a.a(true);
                this.a.f();
                return;
            case R.id.cbWideViewPort:
                a.L(z);
                return;
            case R.id.cbOverView:
                a.z(z);
                return;
            case R.id.cbResizeWindow:
                a.p(z);
                return;
            case R.id.cbTextReflow:
                a.J(z);
                return;
        }
    }
}
