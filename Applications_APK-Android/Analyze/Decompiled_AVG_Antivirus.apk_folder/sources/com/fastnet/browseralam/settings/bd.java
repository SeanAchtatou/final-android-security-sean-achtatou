package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class bd implements View.OnClickListener {
    final /* synthetic */ GeneralSettingsActivity a;

    bd(GeneralSettingsActivity generalSettingsActivity) {
        this.a = generalSettingsActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.s);
        builder.setTitle(this.a.getResources().getString(R.string.title_user_agent));
        GeneralSettingsActivity generalSettingsActivity = this.a;
        a unused = this.a.j;
        int unused2 = generalSettingsActivity.k = a.ak();
        builder.setSingleChoiceItems((int) R.array.user_agent, this.a.k - 1, new be(this));
        builder.setNeutralButton(this.a.getResources().getString(R.string.action_ok), new bf(this));
        builder.setOnCancelListener(new bg(this));
        builder.show();
    }
}
