package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class ah implements DialogInterface.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ x b;

    ah(x xVar, EditText editText) {
        this.b = xVar;
        this.a = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        ((InputMethodManager) this.b.a.getSystemService("input_method")).hideSoftInputFromWindow(this.a.getWindowToken(), 0);
    }
}
