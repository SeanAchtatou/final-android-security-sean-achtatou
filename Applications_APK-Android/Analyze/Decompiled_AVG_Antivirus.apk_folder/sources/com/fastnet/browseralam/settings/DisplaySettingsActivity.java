package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;
import com.fastnet.browseralam.i.aq;
import com.jobstrak.drawingfun.lib.mopub.mobileads.resource.DrawableConstants;

public class DisplaySettingsActivity extends ThemableSettingsActivity {
    /* access modifiers changed from: private */
    public AlertDialog A;
    /* access modifiers changed from: private */
    public int[][] B;
    /* access modifiers changed from: private */
    public String[] C;
    private int[][] D = {new int[]{560, 1}, new int[]{561, 1}, new int[]{562, 1}, new int[]{563, 1}, new int[]{564, 1}, new int[]{565, 0}, new int[]{566, 0}};
    private String[] E = {"Open in new tab", "Open in incognito tab", "Copy link address", "Open link", "Save link as", "Share link", "Copy link text"};
    private int[][] F = {new int[]{780, 1}, new int[]{781, 1}, new int[]{782, 1}, new int[]{783, 1}, new int[]{784, 1}, new int[]{785, 0}, new int[]{786, 0}};
    private String[] G = {"Open link in new tab", "Open image in new tab", "Copy link address", "Download image", "Open image", "Save image as", "Share link"};
    /* access modifiers changed from: private */
    public Context i;
    private a j;
    /* access modifiers changed from: private */
    public TextView k;
    /* access modifiers changed from: private */
    public TextView l;
    /* access modifiers changed from: private */
    public CheckBox m;
    /* access modifiers changed from: private */
    public CheckBox n;
    /* access modifiers changed from: private */
    public CheckBox o;
    /* access modifiers changed from: private */
    public CheckBox p;
    /* access modifiers changed from: private */
    public CheckBox q;
    /* access modifiers changed from: private */
    public CheckBox r;
    /* access modifiers changed from: private */
    public CheckBox s;
    /* access modifiers changed from: private */
    public CheckBox t;
    /* access modifiers changed from: private */
    public CheckBox u;
    private int[][] v = {new int[]{0, 1}, new int[]{1, 1}, new int[]{2, 1}, new int[]{3, 1}, new int[]{4, 1}, new int[]{5, 1}, new int[]{6, 1}, new int[]{7, 1}, new int[]{8, 0}, new int[]{9, 0}};
    private String[] w = {"New Tab", "New Incognito Tab", "Bookmarks", "History", "Find In Page", "Desktop Mode", "Reader Mode", "Rendering Mode", "Share Link", "Check For Updates"};
    /* access modifiers changed from: private */
    public String x;
    private View y;
    private aq z;

    static /* synthetic */ void a(DisplaySettingsActivity displaySettingsActivity) {
        if (displaySettingsActivity.A == null) {
            displaySettingsActivity.y = displaySettingsActivity.getLayoutInflater().inflate((int) R.layout.edit_link_dialog, (ViewGroup) null);
            displaySettingsActivity.z = new aq(displaySettingsActivity, displaySettingsActivity.y);
        }
        ((TextView) displaySettingsActivity.y.findViewById(R.id.edit_dialog_title)).setText("Edit Overflow Menu");
        String O = a.O();
        if (O != null) {
            String[] split = O.split(",");
            for (int i2 = 0; i2 < split.length; i2++) {
                String[] split2 = split[i2].split(":");
                displaySettingsActivity.v[i2] = new int[]{Integer.parseInt(split2[0]), split2[1].equals("1") ? 1 : 0};
                displaySettingsActivity.w[i2] = split2[2];
            }
        }
        displaySettingsActivity.B = displaySettingsActivity.v;
        displaySettingsActivity.C = displaySettingsActivity.w;
        displaySettingsActivity.z.c();
        displaySettingsActivity.g();
        displaySettingsActivity.A.show();
        displaySettingsActivity.A.getWindow().setLayout(-1, aq.b() - aq.a(112));
        displaySettingsActivity.x = "menuorder";
    }

    static /* synthetic */ void b(DisplaySettingsActivity displaySettingsActivity) {
        if (displaySettingsActivity.A == null) {
            displaySettingsActivity.y = displaySettingsActivity.getLayoutInflater().inflate((int) R.layout.edit_link_dialog, (ViewGroup) null);
            displaySettingsActivity.z = new aq(displaySettingsActivity, displaySettingsActivity.y);
        }
        ((TextView) displaySettingsActivity.y.findViewById(R.id.edit_dialog_title)).setText("Edit Link Dialog");
        String M = a.M();
        if (M != null) {
            String[] split = M.split(",");
            for (int i2 = 0; i2 < split.length; i2++) {
                String[] split2 = split[i2].split(":");
                displaySettingsActivity.D[i2] = new int[]{Integer.parseInt(split2[0]), split2[1].equals("1") ? 1 : 0};
                displaySettingsActivity.E[i2] = split2[2];
            }
        }
        displaySettingsActivity.B = displaySettingsActivity.D;
        displaySettingsActivity.C = displaySettingsActivity.E;
        displaySettingsActivity.z.c();
        displaySettingsActivity.g();
        displaySettingsActivity.A.show();
        displaySettingsActivity.A.getWindow().setLayout(-1, -2);
        displaySettingsActivity.x = "linkorder";
    }

    static /* synthetic */ void c(DisplaySettingsActivity displaySettingsActivity) {
        if (displaySettingsActivity.A == null) {
            displaySettingsActivity.y = displaySettingsActivity.getLayoutInflater().inflate((int) R.layout.edit_link_dialog, (ViewGroup) null);
            displaySettingsActivity.z = new aq(displaySettingsActivity, displaySettingsActivity.y);
        }
        ((TextView) displaySettingsActivity.y.findViewById(R.id.edit_dialog_title)).setText("Edit Image Dialog");
        String N = a.N();
        if (N != null) {
            String[] split = N.split(",");
            for (int i2 = 0; i2 < split.length; i2++) {
                String[] split2 = split[i2].split(":");
                displaySettingsActivity.F[i2] = new int[]{Integer.parseInt(split2[0]), split2[1].equals("1") ? 1 : 0};
                displaySettingsActivity.G[i2] = split2[2];
            }
        }
        displaySettingsActivity.B = displaySettingsActivity.F;
        displaySettingsActivity.C = displaySettingsActivity.G;
        displaySettingsActivity.z.c();
        displaySettingsActivity.g();
        displaySettingsActivity.A.show();
        displaySettingsActivity.A.getWindow().setLayout(-1, -2);
        displaySettingsActivity.x = "imageorder";
    }

    /* access modifiers changed from: private */
    public static String e() {
        switch (a.ag()) {
            case 50:
                return "Smallest";
            case 75:
                return "Small";
            case 100:
                return "Normal";
            case DrawableConstants.CtaButton.WIDTH_DIPS /*150*/:
                return "Large";
            case 200:
                return "Largest";
            default:
                return "Normal";
        }
    }

    private void g() {
        if (this.A == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.i);
            builder.setView(this.y);
            this.A = builder.create();
            this.y.findViewById(R.id.save).setOnClickListener(new am(this));
            this.y.findViewById(R.id.reset).setOnClickListener(new an(this));
            this.y.findViewById(R.id.cancel).setOnClickListener(new ao(this));
            aq.a(this.i, "Long press to rearrange");
        }
    }

    static /* synthetic */ void l(DisplaySettingsActivity displaySettingsActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(displaySettingsActivity);
        builder.setTitle(displaySettingsActivity.getResources().getString(R.string.title_text_size));
        int ag = a.ag();
        switch (ag) {
            case 50:
                ag = 4;
                break;
            case 75:
                ag = 3;
                break;
            case 100:
                ag = 2;
                break;
            case DrawableConstants.CtaButton.WIDTH_DIPS /*150*/:
                ag = 1;
                break;
            case 200:
                ag = 0;
                break;
        }
        builder.setSingleChoiceItems((int) R.array.text_size, ag, new ai(displaySettingsActivity));
        builder.setNeutralButton(displaySettingsActivity.getResources().getString(R.string.action_ok), new aj(displaySettingsActivity));
        builder.show();
    }

    static /* synthetic */ void m(DisplaySettingsActivity displaySettingsActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(displaySettingsActivity.i);
        builder.setTitle(displaySettingsActivity.getResources().getString(R.string.rendering_mode));
        builder.setSingleChoiceItems(new CharSequence[]{displaySettingsActivity.i.getString(R.string.name_normal), displaySettingsActivity.i.getString(R.string.name_inverted), displaySettingsActivity.i.getString(R.string.name_grayscale), displaySettingsActivity.i.getString(R.string.name_inverted_grayscale)}, a.R(), new ak(displaySettingsActivity));
        builder.setNeutralButton(displaySettingsActivity.getResources().getString(R.string.action_ok), new al(displaySettingsActivity));
        builder.show();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.display_settings);
        a((Toolbar) findViewById(R.id.toolbar));
        c().a(true);
        this.j = a.a();
        this.i = this;
        av avVar = new av(this, (byte) 0);
        ap apVar = new ap(this, (byte) 0);
        findViewById(R.id.rResizeWindow);
        ((RelativeLayout) findViewById(R.id.rEditMenu)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rEditLinkDialog)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rEditImageDialog)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rHideStatusBar)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rStackTabsTop)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rDrawerCards)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rFullScreen)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rWideViewPort)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rOverView)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rTextReflow)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rTextSize)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.layoutRendering)).setOnClickListener(avVar);
        ((RelativeLayout) findViewById(R.id.rDarkTheme)).setOnClickListener(avVar);
        this.m = (CheckBox) findViewById(R.id.cbHideStatusBar);
        this.o = (CheckBox) findViewById(R.id.cbStackTabsTop);
        this.n = (CheckBox) findViewById(R.id.cbResizeWindow);
        this.q = (CheckBox) findViewById(R.id.cbDrawerCards);
        this.p = (CheckBox) findViewById(R.id.cbFullScreen);
        this.r = (CheckBox) findViewById(R.id.cbWideViewPort);
        this.s = (CheckBox) findViewById(R.id.cbOverView);
        this.t = (CheckBox) findViewById(R.id.cbTextReflow);
        this.u = (CheckBox) findViewById(R.id.cbDarkTheme);
        this.m.setChecked(a.s());
        this.n.setChecked(a.t());
        this.o.setChecked(a.n());
        this.q.setChecked(a.p());
        this.p.setChecked(a.q());
        this.r.setChecked(a.al());
        this.s.setChecked(a.H());
        this.t.setChecked(a.af());
        this.u.setChecked(a.ai());
        this.m.setOnCheckedChangeListener(apVar);
        this.n.setOnCheckedChangeListener(apVar);
        this.o.setOnCheckedChangeListener(apVar);
        this.q.setOnCheckedChangeListener(apVar);
        this.p.setOnCheckedChangeListener(apVar);
        this.r.setOnCheckedChangeListener(apVar);
        this.s.setOnCheckedChangeListener(apVar);
        this.t.setOnCheckedChangeListener(apVar);
        this.u.setOnCheckedChangeListener(apVar);
        this.k = (TextView) findViewById(R.id.renderText);
        this.l = (TextView) findViewById(R.id.textSize);
        this.l.setText(e());
        switch (a.R()) {
            case 0:
                this.k.setText(this.i.getString(R.string.name_normal));
                return;
            case 1:
                this.k.setText(this.i.getString(R.string.name_inverted));
                return;
            case 2:
                this.k.setText(this.i.getString(R.string.name_grayscale));
                return;
            case 3:
                this.k.setText(this.i.getString(R.string.name_inverted_grayscale));
                return;
            default:
                return;
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        finish();
        return true;
    }
}
