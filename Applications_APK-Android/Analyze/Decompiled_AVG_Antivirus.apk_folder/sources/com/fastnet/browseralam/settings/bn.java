package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.h.a;

final class bn implements View.OnClickListener {
    final /* synthetic */ GeneralSettingsActivity a;

    bn(GeneralSettingsActivity generalSettingsActivity) {
        this.a = generalSettingsActivity;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.s);
        builder.setTitle(this.a.getResources().getString(R.string.url_contents));
        a unused = this.a.j;
        builder.setSingleChoiceItems(this.a.t, a.ah(), new bo(this));
        builder.setNeutralButton(this.a.getResources().getString(R.string.action_ok), new bp(this));
        builder.show();
    }
}
