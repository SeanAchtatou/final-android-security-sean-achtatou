package com.fastnet.browseralam.settings;

import android.content.DialogInterface;
import android.widget.EditText;
import com.fastnet.browseralam.h.a;

final class bj implements DialogInterface.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ GeneralSettingsActivity b;

    bj(GeneralSettingsActivity generalSettingsActivity, EditText editText) {
        this.b = generalSettingsActivity;
        this.a = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String obj = this.a.getText().toString();
        a unused = this.b.j;
        a.d(obj);
        this.b.o.setText(obj);
    }
}
