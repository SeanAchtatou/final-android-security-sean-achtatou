package com.fastnet.browseralam.settings;

import android.support.v7.widget.cf;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;

public final class as extends cf implements at {
    final /* synthetic */ aq l;
    private final LinearLayout m;
    /* access modifiers changed from: private */
    public final TextView n;
    /* access modifiers changed from: private */
    public final CheckBox o;
    /* access modifiers changed from: private */
    public int p;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public as(aq aqVar, View view) {
        super(view);
        this.l = aqVar;
        this.m = (LinearLayout) view.findViewById(R.id.dialog_item);
        this.n = (TextView) view.findViewById(R.id.dialog_item_text);
        this.o = (CheckBox) view.findViewById(R.id.checkbox);
        this.o.setOnClickListener(aqVar.i);
    }

    public final void t() {
        this.m.setBackground(this.l.g);
    }

    public final void u() {
        this.m.setBackgroundResource(this.l.h);
    }

    public final int v() {
        return d();
    }
}
