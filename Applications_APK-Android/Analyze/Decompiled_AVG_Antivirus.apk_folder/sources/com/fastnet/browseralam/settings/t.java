package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.k;

final class t implements View.OnClickListener {
    final /* synthetic */ k a;
    final /* synthetic */ s b;

    t(s sVar, k kVar) {
        this.b = sVar;
        this.a = kVar;
    }

    public final void onClick(View view) {
        k kVar = this.b.a;
        v a2 = this.b.c;
        int hashCode = a2.m.getText().hashCode();
        View inflate = kVar.a.getLayoutInflater().inflate((int) R.layout.edit_name_dialog, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.edit_title)).setText("Edit Url");
        AlertDialog.Builder builder = new AlertDialog.Builder(kVar.a.i);
        builder.setView(inflate);
        AlertDialog create = builder.create();
        create.setCanceledOnTouchOutside(true);
        EditText editText = (EditText) inflate.findViewById(R.id.edit_name);
        editText.setText(a2.m.getText().toString());
        inflate.findViewById(R.id.ok).setOnClickListener(new m(kVar, a2, editText, create, hashCode));
        inflate.findViewById(R.id.cancel).setOnClickListener(new n(kVar, create));
        create.show();
        editText.selectAll();
        k.a(new o(kVar, editText));
        this.b.a.h.dismiss();
    }
}
