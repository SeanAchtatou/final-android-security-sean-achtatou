package com.fastnet.browseralam.settings;

import android.content.Intent;
import android.view.View;

final class dr implements View.OnClickListener {
    final /* synthetic */ SettingsActivity a;

    dr(SettingsActivity settingsActivity) {
        this.a = settingsActivity;
    }

    public final void onClick(View view) {
        this.a.startActivity(new Intent(this.a.k, PrivacySettingsActivity.class));
    }
}
