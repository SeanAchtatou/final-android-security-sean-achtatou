package com.fastnet.browseralam.settings;

import android.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b.k;

final class l implements View.OnClickListener {
    final /* synthetic */ AdvancedSettingsActivity a;
    final /* synthetic */ k b;

    l(k kVar, AdvancedSettingsActivity advancedSettingsActivity) {
        this.b = kVar;
        this.a = advancedSettingsActivity;
    }

    public final void onClick(View view) {
        k kVar = this.b;
        View inflate = kVar.a.getLayoutInflater().inflate((int) R.layout.edit_name_dialog, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.edit_title)).setText("Add Url");
        AlertDialog.Builder builder = new AlertDialog.Builder(kVar.a.i);
        builder.setView(inflate);
        AlertDialog create = builder.create();
        create.setCanceledOnTouchOutside(true);
        EditText editText = (EditText) inflate.findViewById(R.id.edit_name);
        inflate.findViewById(R.id.ok).setOnClickListener(new p(kVar, editText, create));
        inflate.findViewById(R.id.cancel).setOnClickListener(new q(kVar, create));
        create.show();
        k.a(new r(kVar, editText));
    }
}
