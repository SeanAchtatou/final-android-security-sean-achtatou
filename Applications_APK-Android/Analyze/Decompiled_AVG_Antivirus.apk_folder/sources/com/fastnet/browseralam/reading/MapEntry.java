package com.fastnet.browseralam.reading;

import java.io.Serializable;
import java.util.Map;

public class MapEntry implements Serializable, Map.Entry {
    private Object a;
    private Object b;

    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MapEntry mapEntry = (MapEntry) obj;
        if (this.a == mapEntry.a || (this.a != null && this.a.equals(mapEntry.a))) {
            return this.b == mapEntry.b || (this.b != null && this.b.equals(mapEntry.b));
        }
        return false;
    }

    public Object getKey() {
        return this.a;
    }

    public Object getValue() {
        return this.b;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.a != null ? this.a.hashCode() : 0) + 133) * 19;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }

    public Object setValue(Object obj) {
        this.b = obj;
        return obj;
    }

    public String toString() {
        return getKey() + ", " + getValue();
    }
}
