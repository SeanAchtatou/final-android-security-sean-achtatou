package com.fastnet.browseralam.reading;

import java.util.LinkedHashSet;

final class b extends LinkedHashSet {
    final /* synthetic */ HtmlFetcher a;

    b(HtmlFetcher htmlFetcher) {
        this.a = htmlFetcher;
        add("bit.ly");
        add("cli.gs");
        add("deck.ly");
        add("fb.me");
        add("feedproxy.google.com");
        add("flic.kr");
        add("fur.ly");
        add("goo.gl");
        add("is.gd");
        add("ink.co");
        add("j.mp");
        add("lnkd.in");
        add("on.fb.me");
        add("ow.ly");
        add("plurl.us");
        add("sns.mx");
        add("snurl.com");
        add("su.pr");
        add("t.co");
        add("tcrn.ch");
        add("tl.gd");
        add("tiny.cc");
        add("tinyurl.com");
        add("tmi.me");
        add("tr.im");
        add("twurl.nl");
    }
}
