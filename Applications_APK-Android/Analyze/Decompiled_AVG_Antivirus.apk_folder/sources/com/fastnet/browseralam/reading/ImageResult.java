package com.fastnet.browseralam.reading;

public class ImageResult {
    public final String a;
    public final Integer b;
    public final String c;
    public final int d;
    public final int e;
    public final String f;
    public final boolean g;

    public ImageResult(String str, Integer num, String str2, int i, int i2, String str3, boolean z) {
        this.a = str;
        this.b = num;
        this.c = str2;
        this.d = i;
        this.e = i2;
        this.f = str3;
        this.g = z;
    }
}
