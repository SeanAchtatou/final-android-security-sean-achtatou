package com.fastnet.browseralam.reading;

import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ArticleTextExtractor {
    private static final Pattern a = Pattern.compile("p|div|td|h1|h2|article|section");
    private static final Pattern h = Pattern.compile("hidden|display: ?none|font-size: ?small");
    private static final Set i = new a();
    private static final OutputFormatter j = new OutputFormatter();
    private String b = "com(bx|ment|munity)|dis(qus|cuss)|e(xtra|[-]?mail)|foot|header|menu|re(mark|ply)|rss|sh(are|outbox)|sponsora(d|ll|gegate|rchive|ttachment)|(pag(er|ination))|popup|print|login|si(debar|gn|ngle)";
    private Pattern c = Pattern.compile("com(bx|ment|munity)|dis(qus|cuss)|e(xtra|[-]?mail)|foot|header|menu|re(mark|ply)|rss|sh(are|outbox)|sponsora(d|ll|gegate|rchive|ttachment)|(pag(er|ination))|popup|print|login|si(debar|gn|ngle)");
    private String d = "(^(body|content|h?entry|main|page|post|text|blog|story|haupt))|arti(cle|kel)|instapaper_body";
    private Pattern e = Pattern.compile("(^(body|content|h?entry|main|page|post|text|blog|story|haupt))|arti(cle|kel)|instapaper_body");
    private String f = "nav($|igation)|user|com(ment|bx)|(^com-)|contact|foot|masthead|(me(dia|ta))|outbrain|promo|related|scroll|(sho(utbox|pping))|sidebar|sponsor|tags|tool|widget|player|disclaimer|toc|infobox|vcard";
    private Pattern g = Pattern.compile("nav($|igation)|user|com(ment|bx)|(^com-)|contact|foot|masthead|(me(dia|ta))|outbrain|promo|related|scroll|(sho(utbox|pping))|sidebar|sponsor|tags|tool|widget|player|disclaimer|toc|infobox|vcard");
    private OutputFormatter k = j;

    public class ImageComparator implements Comparator {
        public ImageComparator() {
        }

        public /* synthetic */ int compare(Object obj, Object obj2) {
            return ((ImageResult) obj2).b.compareTo(((ImageResult) obj).b);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b1, code lost:
        if (r0.className().toLowerCase(java.util.Locale.getDefault()).equals("caption") != false) goto L_0x00b4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int a(org.jsoup.nodes.Element r14) {
        /*
            r13 = 50
            r12 = 5
            r4 = -30
            r0 = 0
            r1 = 0
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>(r12)
            org.jsoup.select.Elements r2 = r14.children()
            java.util.Iterator r6 = r2.iterator()
            r2 = r0
        L_0x0015:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x00c6
            java.lang.Object r0 = r6.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r3 = r0.ownText()
            int r7 = r3.length()
            r8 = 20
            if (r7 < r8) goto L_0x0015
            r8 = 200(0xc8, float:2.8E-43)
            if (r7 <= r8) goto L_0x0038
            int r8 = r7 / 10
            int r8 = java.lang.Math.max(r13, r8)
            int r2 = r2 + r8
        L_0x0038:
            java.lang.String r8 = r0.tagName()
            java.lang.String r9 = "h1"
            boolean r8 = r8.equals(r9)
            if (r8 != 0) goto L_0x0050
            java.lang.String r8 = r0.tagName()
            java.lang.String r9 = "h2"
            boolean r8 = r8.equals(r9)
            if (r8 == 0) goto L_0x0054
        L_0x0050:
            int r0 = r2 + 30
            r2 = r0
            goto L_0x0015
        L_0x0054:
            java.lang.String r8 = r0.tagName()
            java.lang.String r9 = "div"
            boolean r8 = r8.equals(r9)
            if (r8 != 0) goto L_0x006c
            java.lang.String r8 = r0.tagName()
            java.lang.String r9 = "p"
            boolean r8 = r8.equals(r9)
            if (r8 == 0) goto L_0x0116
        L_0x006c:
            java.lang.String r8 = "&quot;"
            int r8 = com.fastnet.browseralam.reading.SHelper.a(r3, r8)
            java.lang.String r9 = "&lt;"
            int r9 = com.fastnet.browseralam.reading.SHelper.a(r3, r9)
            int r8 = r8 + r9
            java.lang.String r9 = "&gt;"
            int r9 = com.fastnet.browseralam.reading.SHelper.a(r3, r9)
            int r8 = r8 + r9
            java.lang.String r9 = "px"
            int r9 = com.fastnet.browseralam.reading.SHelper.a(r3, r9)
            int r8 = r8 + r9
            if (r8 <= r12) goto L_0x00b8
            r3 = r4
        L_0x008a:
            a(r0, r3)
            int r2 = r2 + r3
            java.lang.String r3 = r0.tagName()
            java.lang.String r8 = "p"
            boolean r3 = r3.equals(r8)
            if (r3 == 0) goto L_0x009f
            if (r7 <= r13) goto L_0x009f
            r5.add(r0)
        L_0x009f:
            java.lang.String r3 = r0.className()
            java.util.Locale r7 = java.util.Locale.getDefault()
            java.lang.String r3 = r3.toLowerCase(r7)
            java.lang.String r7 = "caption"
            boolean r3 = r3.equals(r7)
            if (r3 == 0) goto L_0x0116
            r1 = r2
        L_0x00b4:
            r2 = r1
            r1 = r0
            goto L_0x0015
        L_0x00b8:
            int r3 = r3.length()
            double r8 = (double) r3
            r10 = 4627730092099895296(0x4039000000000000, double:25.0)
            double r8 = r8 / r10
            long r8 = java.lang.Math.round(r8)
            int r3 = (int) r8
            goto L_0x008a
        L_0x00c6:
            if (r1 == 0) goto L_0x00ca
            int r2 = r2 + 30
        L_0x00ca:
            int r0 = r5.size()
            r1 = 2
            if (r0 < r1) goto L_0x0115
            org.jsoup.select.Elements r0 = r14.children()
            java.util.Iterator r1 = r0.iterator()
        L_0x00d9:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0115
            java.lang.Object r0 = r1.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r3 = "h1;h2;h3;h4;h5;h6"
            java.lang.String r5 = r0.tagName()
            boolean r3 = r3.contains(r5)
            if (r3 == 0) goto L_0x0105
            int r2 = r2 + 20
        L_0x00f3:
            java.lang.String r3 = "p"
            java.lang.String r5 = r0.tagName()
            boolean r3 = r3.contains(r5)
            if (r3 == 0) goto L_0x00d9
            r3 = 30
            a(r0, r3)
            goto L_0x00d9
        L_0x0105:
            java.lang.String r3 = "table;li;td;th"
            java.lang.String r5 = r0.tagName()
            boolean r3 = r3.contains(r5)
            if (r3 == 0) goto L_0x00f3
            a(r0, r4)
            goto L_0x00f3
        L_0x0115:
            return r2
        L_0x0116:
            r0 = r1
            r1 = r2
            goto L_0x00b4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.reading.ArticleTextExtractor.a(org.jsoup.nodes.Element):int");
    }

    private JResult a(JResult jResult, Document document, OutputFormatter outputFormatter) {
        Element element;
        List emptyList;
        int i2;
        if (document == null) {
            throw new NullPointerException("missing document");
        }
        String a2 = a(document.title());
        if (a2.isEmpty()) {
            a2 = SHelper.b(document.select("head title").text());
            if (a2.isEmpty()) {
                a2 = SHelper.b(document.select("head meta[name=title]").attr("content"));
                if (a2.isEmpty()) {
                    a2 = SHelper.b(document.select("head meta[property=og:title]").attr("content"));
                    if (a2.isEmpty()) {
                        a2 = SHelper.b(document.select("head meta[name=twitter:title]").attr("content"));
                    }
                }
            }
        }
        jResult.i(a2);
        String b2 = SHelper.b(document.select("head meta[name=description]").attr("content"));
        if (b2.isEmpty()) {
            b2 = SHelper.b(document.select("head meta[property=og:description]").attr("content"));
            if (b2.isEmpty()) {
                b2 = SHelper.b(document.select("head meta[name=twitter:description]").attr("content"));
            }
        }
        jResult.f(b2);
        String a3 = SHelper.a(document.select("head link[rel=canonical]").attr("href"));
        if (a3.isEmpty()) {
            a3 = SHelper.a(document.select("head meta[property=og:url]").attr("content"));
            if (a3.isEmpty()) {
                a3 = SHelper.a(document.select("head meta[name=twitter:url]").attr("content"));
            }
        }
        jResult.c(a3);
        a(document);
        Element element2 = null;
        Iterator it = b(document).iterator();
        int i3 = 0;
        while (true) {
            if (!it.hasNext()) {
                element = element2;
                break;
            }
            element = (Element) it.next();
            int i4 = this.e.matcher(element.className()).find() ? 35 : 0;
            if (this.e.matcher(element.id()).find()) {
                i4 += 40;
            }
            if (this.c.matcher(element.className()).find()) {
                i4 -= 20;
            }
            if (this.c.matcher(element.id()).find()) {
                i4 -= 20;
            }
            if (this.g.matcher(element.className()).find()) {
                i4 -= 50;
            }
            if (this.g.matcher(element.id()).find()) {
                i4 -= 50;
            }
            String attr = element.attr("style");
            if (attr != null && !attr.isEmpty() && h.matcher(attr).find()) {
                i4 -= 50;
            }
            int round = i4 + ((int) Math.round((((double) element.ownText().length()) / 100.0d) * 10.0d)) + a(element);
            if (round > i3) {
                if (round > 200) {
                    break;
                }
                i2 = round;
            } else {
                element = element2;
                i2 = i3;
            }
            i3 = i2;
            element2 = element;
        }
        if (element != null) {
            ArrayList arrayList = new ArrayList();
            Element a4 = a(element, arrayList);
            if (a4 != null) {
                jResult.g(SHelper.a(a4.attr("src")));
                jResult.b(arrayList);
            }
            String a5 = outputFormatter.a(element);
            if (a5.length() > jResult.f().length()) {
                jResult.h(a5);
            }
            jResult.a(outputFormatter.b(element));
        }
        if (jResult.c().isEmpty()) {
            String a6 = SHelper.a(document.select("head meta[property=og:image]").attr("content"));
            if (a6.isEmpty()) {
                a6 = SHelper.a(document.select("head meta[name=twitter:image]").attr("content"));
                if (a6.isEmpty()) {
                    a6 = SHelper.a(document.select("link[rel=image_src]").attr("href"));
                    if (a6.isEmpty()) {
                        a6 = SHelper.a(document.select("head meta[name=thumbnail]").attr("content"));
                    }
                }
            }
            jResult.g(a6);
        }
        jResult.e(SHelper.a(document.select("link[rel=alternate]").select("link[type=application/rss+xml]").attr("href")));
        jResult.j(SHelper.a(document.select("head meta[property=og:video]").attr("content")));
        String a7 = SHelper.a(document.select("head link[rel=icon]").attr("href"));
        if (a7.isEmpty()) {
            a7 = SHelper.a(document.select("head link[rel^=shortcut],link[rel$=icon]").attr("href"));
        }
        jResult.d(a7);
        String b3 = SHelper.b(document.select("head meta[name=keywords]").attr("content"));
        if (b3 != null) {
            if (b3.startsWith("[") && b3.endsWith("]")) {
                b3 = b3.substring(1, b3.length() - 1);
            }
            String[] split = b3.split("\\s*,\\s*");
            if (split.length > 1 || (split.length > 0 && !"".equals(split[0]))) {
                emptyList = Arrays.asList(split);
                jResult.a((Collection) emptyList);
                return jResult;
            }
        }
        emptyList = Collections.emptyList();
        jResult.a((Collection) emptyList);
        return jResult;
    }

    private static String a(String str) {
        int i2 = 0;
        StringBuilder sb = new StringBuilder();
        String[] split = str.split("\\|");
        for (String str2 : split) {
            if (!i.contains(str2.toLowerCase(Locale.getDefault()).trim()) && (i2 != split.length - 1 || sb.length() <= str2.length())) {
                if (i2 > 0) {
                    sb.append("|");
                }
                sb.append(str2);
                i2++;
            }
        }
        return SHelper.b(sb.toString());
    }

    private static Document a(Document document) {
        Iterator it = document.getElementsByTag("script").iterator();
        while (it.hasNext()) {
            ((Element) it.next()).remove();
        }
        Iterator it2 = document.getElementsByTag("noscript").iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).remove();
        }
        Iterator it3 = document.getElementsByTag("style").iterator();
        while (it3.hasNext()) {
            ((Element) it3.next()).remove();
        }
        return document;
    }

    private Element a(Element element, List list) {
        int i2;
        boolean z;
        int i3;
        Element element2;
        String attr;
        int i4 = 0;
        Element element3 = null;
        Elements select = element.select("img");
        if (select.isEmpty()) {
            select = element.parent().select("img");
        }
        double d2 = 1.0d;
        Iterator it = select.iterator();
        while (it.hasNext()) {
            Element element4 = (Element) it.next();
            String attr2 = element4.attr("src");
            if (!attr2.isEmpty()) {
                if (!(SHelper.a(attr2, "ad") >= 2)) {
                    int i5 = 0;
                    int i6 = 0;
                    try {
                        i6 = Integer.parseInt(element4.attr(UrlManager.Parameter.HEIGHT));
                        i5 = i6 >= 50 ? 20 : -20;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    int i7 = 0;
                    try {
                        i7 = Integer.parseInt(element4.attr(UrlManager.Parameter.WIDTH));
                        i5 = i7 >= 50 ? i5 + 20 : i5 - 20;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    String attr3 = element4.attr("alt");
                    int i8 = attr3.length() > 35 ? i5 + 20 : i5;
                    String attr4 = element4.attr("title");
                    if (attr4.length() > 35) {
                        i8 += 20;
                    }
                    if (element4.parent() == null || (attr = element4.parent().attr("rel")) == null || !attr.contains("nofollow")) {
                        i2 = i8;
                        z = false;
                    } else {
                        i2 = i8 - 40;
                        z = attr.contains("nofollow");
                    }
                    int i9 = (int) (((double) i2) * d2);
                    if (i9 > i4) {
                        d2 /= 2.0d;
                        element2 = element4;
                        i3 = i9;
                    } else {
                        i3 = i4;
                        element2 = element3;
                    }
                    list.add(new ImageResult(attr2, Integer.valueOf(i9), attr4, i6, i7, attr3, z));
                    element3 = element2;
                    i4 = i3;
                }
            }
        }
        Collections.sort(list, new ImageComparator());
        return element3;
    }

    private static void a(Element element, int i2) {
        b(element, b(element) + i2);
    }

    private static int b(Element element) {
        try {
            return Integer.parseInt(element.attr("gravityScore"));
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    private static Collection b(Document document) {
        HashSet hashSet = new HashSet(64);
        int i2 = 100;
        Iterator it = document.select("body").select("*").iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                return hashSet;
            }
            Element element = (Element) it.next();
            if (a.matcher(element.tagName()).matches()) {
                hashSet.add(element);
                b(element, i3);
                i2 = i3 / 2;
            } else {
                i2 = i3;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
      org.jsoup.nodes.Node.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element */
    private static void b(Element element, int i2) {
        element.attr("gravityScore", Integer.toString(i2));
    }

    public final JResult a(JResult jResult, String str) {
        OutputFormatter outputFormatter = this.k;
        if (!str.isEmpty()) {
            return a(jResult, Jsoup.parse(str), outputFormatter);
        }
        throw new IllegalArgumentException("html string is empty!?");
    }
}
