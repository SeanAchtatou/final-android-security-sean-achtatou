package com.fastnet.browseralam.reading;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class JResult implements Serializable {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private List l;
    private Collection m;
    private List n = null;

    public final JResult a(String str) {
        this.b = str;
        return this;
    }

    public final JResult a(List list) {
        this.l = list;
        return this;
    }

    public final String a() {
        return this.i == null ? "" : this.i;
    }

    public final void a(Collection collection) {
        this.m = collection;
    }

    public final JResult b(String str) {
        this.c = str;
        return this;
    }

    public final String b() {
        return this.g == null ? "" : this.g;
    }

    public final void b(List list) {
        this.n = list;
    }

    public final JResult c(String str) {
        this.d = str;
        return this;
    }

    public final String c() {
        return this.e == null ? "" : this.e;
    }

    public final JResult d(String str) {
        this.i = str;
        return this;
    }

    public final String d() {
        return this.h == null ? "" : this.h;
    }

    public final JResult e(String str) {
        this.g = str;
        return this;
    }

    public final List e() {
        return this.l == null ? new ArrayList() : this.l;
    }

    public final JResult f(String str) {
        this.j = str;
        return this;
    }

    public final String f() {
        return this.a == null ? "" : this.a;
    }

    public final JResult g(String str) {
        this.e = str;
        return this;
    }

    public final String g() {
        return this.f == null ? "" : this.f;
    }

    public final JResult h(String str) {
        this.h = str;
        return this;
    }

    public final JResult i(String str) {
        this.a = str;
        return this;
    }

    public final JResult j(String str) {
        this.f = str;
        return this;
    }

    public final JResult k(String str) {
        this.k = str;
        return this;
    }

    public String toString() {
        return "title:" + f() + " imageUrl:" + c() + " text:" + this.h;
    }
}
