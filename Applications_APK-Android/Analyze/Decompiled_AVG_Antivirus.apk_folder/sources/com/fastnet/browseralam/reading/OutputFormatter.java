package com.fastnet.browseralam.reading;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

public class OutputFormatter {
    private static final List d = Arrays.asList("strong", "b", "i");
    protected final int a;
    protected final List b;
    protected String c;
    private Pattern e;

    public OutputFormatter() {
        this(d);
    }

    private OutputFormatter(List list) {
        this.e = Pattern.compile("display\\:none|visibility\\:hidden");
        this.c = "p";
        this.a = 50;
        this.b = list;
    }

    private void a(Element element, StringBuilder sb) {
        for (Node node : element.childNodes()) {
            if (!a(node)) {
                if (node instanceof TextNode) {
                    sb.append(((TextNode) node).text());
                } else if (node instanceof Element) {
                    Element element2 = (Element) node;
                    if (sb.length() > 0 && element2.isBlock()) {
                        if (!(sb.length() != 0 && Character.isWhitespace(sb.charAt(sb.length() + -1)))) {
                            sb.append(" ");
                            a(element2, sb);
                        }
                    }
                    if (element2.tagName().equals("br")) {
                        sb.append(" ");
                    }
                    a(element2, sb);
                }
            }
        }
    }

    private void a(Element element, StringBuilder sb, String str) {
        Iterator it = element.select(str).iterator();
        while (it.hasNext()) {
            Element element2 = (Element) it.next();
            Element element3 = element2;
            while (true) {
                if (element3 != null && !element3.equals(element)) {
                    if (a((Node) element3)) {
                        break;
                    }
                    element3 = element3.parent();
                } else {
                    StringBuilder sb2 = new StringBuilder(200);
                    a(element2, sb2);
                    String sb3 = sb2.toString();
                }
            }
            StringBuilder sb22 = new StringBuilder(200);
            a(element2, sb22);
            String sb32 = sb22.toString();
            if (!sb32.isEmpty() && sb32.length() >= this.a && sb32.length() <= SHelper.o(sb32) * 2) {
                sb.append(sb32);
                sb.append("\n\n");
            }
        }
    }

    private boolean a(Node node) {
        if (node.attr("class") != null && node.attr("class").toLowerCase(Locale.getDefault()).contains("caption")) {
            return true;
        }
        return this.e.matcher(node.attr("style")).find() || this.e.matcher(node.attr("class")).find();
    }

    public final String a(Element element) {
        Iterator it = element.select("*[gravityScore]").iterator();
        while (it.hasNext()) {
            Element element2 = (Element) it.next();
            if (Integer.parseInt(element2.attr("gravityScore")) < 0 || element2.text().length() < this.a) {
                element2.remove();
            }
        }
        StringBuilder sb = new StringBuilder();
        a(element, sb, this.c);
        String b2 = SHelper.b(sb.toString());
        if (b2.length() > 100) {
            return b2;
        }
        if (b2.isEmpty() || (!element.text().isEmpty() && b2.length() <= element.ownText().length())) {
            b2 = element.text();
        }
        return Jsoup.parse(b2).text();
    }

    public final List b(Element element) {
        ArrayList arrayList = new ArrayList();
        Iterator it = element.select(this.c).iterator();
        while (it.hasNext()) {
            Element element2 = (Element) it.next();
            if (element2.hasText()) {
                arrayList.add(element2.text());
            }
        }
        return arrayList;
    }
}
