package com.fastnet.browseralam.reading;

import com.jobstrak.drawingfun.lib.mopub.common.Constants;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URLDecoder;
import java.security.SecureRandom;
import java.util.regex.Pattern;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

public class SHelper {
    private static final Pattern a = Pattern.compile(" ");

    public static int a(String str, String str2) {
        int indexOf = str.indexOf(str2);
        if (indexOf >= 0) {
            return a(str.substring(str2.length() + indexOf), str2) + 1;
        }
        return 0;
    }

    public static String a(String str) {
        if (str.isEmpty()) {
            return str;
        }
        String trim = str.trim();
        return trim.contains(" ") ? a.matcher(trim).replaceAll("%20") : trim;
    }

    public static String a(String str, boolean z) {
        String substring = str.startsWith("http://") ? str.substring(7) : str.startsWith("https://") ? str.substring(8) : str;
        if (z) {
            if (substring.startsWith("www.")) {
                substring = substring.substring(4);
            }
            if (substring.startsWith("m.")) {
                substring = substring.substring(2);
            }
        }
        int indexOf = substring.indexOf("/");
        return indexOf > 0 ? substring.substring(0, indexOf) : substring;
    }

    public static void a() {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookieManager);
    }

    public static String b(String str) {
        if (str.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt == ' ' || charAt == 9 || charAt == 10) {
                z = true;
            } else {
                if (z) {
                    sb.append(' ');
                }
                sb.append(charAt);
                z = false;
            }
        }
        return sb.toString().trim();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.reading.SHelper.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.reading.SHelper.a(java.lang.String, java.lang.String):int
      com.fastnet.browseralam.reading.SHelper.a(java.lang.String, boolean):java.lang.String */
    public static String b(String str, String str2) {
        if (str2.startsWith(Constants.HTTP)) {
            return str2;
        }
        if ("favicon.ico".equals(str2)) {
            str2 = "/favicon.ico";
        }
        if (str2.startsWith("//")) {
            return str.startsWith("https:") ? "https:" + str2 : "http:" + str2;
        }
        if (str2.startsWith("/")) {
            return "http://" + a(str, false) + str2;
        }
        if (!str2.startsWith("../")) {
            return str2;
        }
        int lastIndexOf = str.lastIndexOf("/");
        if (lastIndexOf > 0 && lastIndexOf + 1 < str.length()) {
            str = str.substring(0, lastIndexOf + 1);
        }
        return str + str2;
    }

    public static void b() {
        System.setProperty("http.agent", "");
    }

    public static String c(String str) {
        StringBuilder sb = new StringBuilder();
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isDigit(charAt) || Character.isLetter(charAt) || charAt == '-' || charAt == '_') {
                z = true;
                sb.append(charAt);
            } else if (z) {
                break;
            }
        }
        return sb.toString().trim();
    }

    public static void c() {
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init(new KeyManager[0], new TrustManager[]{new c((byte) 0)}, new SecureRandom());
            SSLContext.setDefault(instance);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String d(String str) {
        return b(str, "/favicon.ico");
    }

    public static boolean e(String str) {
        return str.endsWith(".mpeg") || str.endsWith(".mpg") || str.endsWith(".avi") || str.endsWith(".mov") || str.endsWith(".mpg4") || str.endsWith(".mp4") || str.endsWith(".flv") || str.endsWith(".wmv");
    }

    public static boolean f(String str) {
        return str.endsWith(".mp3") || str.endsWith(".ogg") || str.endsWith(".m3u") || str.endsWith(".wav");
    }

    public static boolean g(String str) {
        return str.endsWith(".pdf") || str.endsWith(".ppt") || str.endsWith(".doc") || str.endsWith(".swf") || str.endsWith(".rtf") || str.endsWith(".xls");
    }

    public static boolean h(String str) {
        return str.endsWith(".gz") || str.endsWith(".tgz") || str.endsWith(".zip") || str.endsWith(".rar") || str.endsWith(".deb") || str.endsWith(".rpm") || str.endsWith(".7z");
    }

    public static boolean i(String str) {
        return str.endsWith(".exe") || str.endsWith(".bin") || str.endsWith(".bat") || str.endsWith(".dmg");
    }

    public static boolean j(String str) {
        return str.endsWith(".png") || str.endsWith(".jpeg") || str.endsWith(".gif") || str.endsWith(".jpg") || str.endsWith(".bmp") || str.endsWith(".ico") || str.endsWith(".eps");
    }

    public static String k(String str) {
        if (str.startsWith("http://www.google.com/url?")) {
            for (String str2 : p(str.substring(26)).split("\\&")) {
                if (str2.startsWith("q=")) {
                    return str2.substring(2);
                }
            }
        }
        return null;
    }

    public static String l(String str) {
        if (str.startsWith("http://www.facebook.com/l.php?u=")) {
            return p(str.substring(32));
        }
        return null;
    }

    public static String m(String str) {
        return str.replaceFirst("#!", "");
    }

    public static String n(String str) {
        int i;
        int i2 = -1;
        int indexOf = str.indexOf("://");
        if (indexOf > 0) {
            str = str.substring(indexOf + 3);
        }
        String[] split = str.split("/");
        int i3 = 0;
        int i4 = -1;
        int i5 = -1;
        int i6 = -1;
        int i7 = -1;
        while (true) {
            if (i3 >= split.length) {
                break;
            }
            String str2 = split[i3];
            if (str2.length() == 4) {
                try {
                    i7 = Integer.parseInt(str2);
                    if (i7 < 1970 || i7 > 3000) {
                        i7 = -1;
                    } else {
                        i6 = i3;
                    }
                } catch (Exception e) {
                }
            } else if (str2.length() != 2) {
                continue;
            } else if (i4 < 0 && i3 == i6 + 1) {
                try {
                    i5 = Integer.parseInt(str2);
                    if (i5 <= 0 || i5 > 12) {
                        i5 = -1;
                    } else {
                        i4 = i3;
                    }
                } catch (Exception e2) {
                }
            } else if (i3 == i4 + 1) {
                try {
                    i = Integer.parseInt(str2);
                } catch (Exception e3) {
                    e3.printStackTrace();
                    i = -1;
                }
                if (i > 0 && i <= 31) {
                    i2 = i;
                    break;
                }
            } else {
                continue;
            }
            i3++;
        }
        if (i7 < 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i7);
        if (i5 <= 0) {
            return sb.toString();
        }
        sb.append('/');
        if (i5 < 10) {
            sb.append('0');
        }
        sb.append(i5);
        if (i2 <= 0) {
            return sb.toString();
        }
        sb.append('/');
        if (i2 < 10) {
            sb.append('0');
        }
        sb.append(i2);
        return sb.toString();
    }

    public static int o(String str) {
        int i = 0;
        int length = str.length();
        for (int i2 = 0; i2 < length; i2++) {
            if (Character.isLetter(str.charAt(i2))) {
                i++;
            }
        }
        return i;
    }

    private static String p(String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }
}
