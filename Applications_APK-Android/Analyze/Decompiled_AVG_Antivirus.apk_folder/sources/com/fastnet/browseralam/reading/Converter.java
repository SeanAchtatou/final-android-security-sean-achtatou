package com.fastnet.browseralam.reading;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public class Converter {
    private int a = 500000;
    private String b;
    private String c;

    public Converter() {
    }

    public Converter(String str) {
        this.c = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e3 A[SYNTHETIC, Splitter:B:39:0x00e3] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f5 A[SYNTHETIC, Splitter:B:47:0x00f5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.io.InputStream r7, int r8, java.lang.String r9) {
        /*
            r6 = this;
            r6.b = r9
            java.lang.String r0 = r6.b
            if (r0 == 0) goto L_0x000e
            java.lang.String r0 = r6.b
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0012
        L_0x000e:
            java.lang.String r0 = "UTF-8"
            r6.b = r0
        L_0x0012:
            r2 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0118, all -> 0x0115 }
            r0 = 2048(0x800, float:2.87E-42)
            r1.<init>(r7, r0)     // Catch:{ IOException -> 0x0118, all -> 0x0115 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x00be }
            r2.<init>()     // Catch:{ IOException -> 0x00be }
            r0 = 4096(0x1000, float:5.74E-42)
            r1.mark(r0)     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            java.lang.String r0 = "charset="
            java.lang.String r3 = r6.b     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            java.lang.String r0 = a(r0, r2, r1, r3)     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            if (r0 == 0) goto L_0x00a9
            r6.b = r0     // Catch:{ UnsupportedEncodingException -> 0x0040 }
        L_0x0030:
            java.lang.String r0 = r6.b     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            boolean r0 = java.nio.charset.Charset.isSupported(r0)     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            if (r0 != 0) goto L_0x0075
            java.io.UnsupportedEncodingException r0 = new java.io.UnsupportedEncodingException     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            java.lang.String r3 = r6.b     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            r0.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            throw r0     // Catch:{ UnsupportedEncodingException -> 0x0040 }
        L_0x0040:
            r0 = move-exception
            java.lang.String r3 = "Lightning"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00be }
            java.lang.String r5 = "Using default encoding:UTF-8 problem:"
            r4.<init>(r5)     // Catch:{ IOException -> 0x00be }
            java.lang.String r0 = r0.getMessage()     // Catch:{ IOException -> 0x00be }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ IOException -> 0x00be }
            java.lang.String r4 = " encoding:"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ IOException -> 0x00be }
            java.lang.String r4 = r6.b     // Catch:{ IOException -> 0x00be }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ IOException -> 0x00be }
            java.lang.String r4 = " "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ IOException -> 0x00be }
            java.lang.String r4 = r6.c     // Catch:{ IOException -> 0x00be }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ IOException -> 0x00be }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00be }
            android.util.Log.d(r3, r0)     // Catch:{ IOException -> 0x00be }
            java.lang.String r0 = "UTF-8"
            r6.b = r0     // Catch:{ IOException -> 0x00be }
        L_0x0075:
            int r0 = r2.size()     // Catch:{ IOException -> 0x00be }
            r3 = 2048(0x800, float:2.87E-42)
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x00be }
        L_0x007d:
            if (r0 < r8) goto L_0x00f9
            java.lang.String r0 = "Lightning"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00be }
            java.lang.String r4 = "Maxbyte of "
            r3.<init>(r4)     // Catch:{ IOException -> 0x00be }
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ IOException -> 0x00be }
            java.lang.String r4 = " exceeded! Maybe html is now broken but try it nevertheless. Url: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x00be }
            java.lang.String r4 = r6.c     // Catch:{ IOException -> 0x00be }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x00be }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00be }
            android.util.Log.d(r0, r3)     // Catch:{ IOException -> 0x00be }
        L_0x009f:
            java.lang.String r0 = r6.b     // Catch:{ IOException -> 0x00be }
            java.lang.String r0 = r2.toString(r0)     // Catch:{ IOException -> 0x00be }
            r1.close()     // Catch:{ Exception -> 0x0106 }
        L_0x00a8:
            return r0
        L_0x00a9:
            java.lang.String r0 = "Lightning"
            java.lang.String r3 = "no charset found in first stage"
            android.util.Log.d(r0, r3)     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            java.lang.String r0 = "encoding="
            java.lang.String r3 = r6.b     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            java.lang.String r0 = a(r0, r2, r1, r3)     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            if (r0 == 0) goto L_0x00e9
            r6.b = r0     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            goto L_0x0030
        L_0x00be:
            r0 = move-exception
        L_0x00bf:
            java.lang.String r2 = "Lightning"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f2 }
            r3.<init>()     // Catch:{ all -> 0x00f2 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f2 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00f2 }
            java.lang.String r3 = " url:"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x00f2 }
            java.lang.String r3 = r6.c     // Catch:{ all -> 0x00f2 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x00f2 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f2 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00f2 }
            if (r1 == 0) goto L_0x00e6
            r1.close()     // Catch:{ Exception -> 0x010b }
        L_0x00e6:
            java.lang.String r0 = ""
            goto L_0x00a8
        L_0x00e9:
            java.lang.String r0 = "Lightning"
            java.lang.String r3 = "no charset found in second stage"
            android.util.Log.d(r0, r3)     // Catch:{ UnsupportedEncodingException -> 0x0040 }
            goto L_0x0030
        L_0x00f2:
            r0 = move-exception
        L_0x00f3:
            if (r1 == 0) goto L_0x00f8
            r1.close()     // Catch:{ Exception -> 0x0110 }
        L_0x00f8:
            throw r0
        L_0x00f9:
            int r4 = r1.read(r3)     // Catch:{ IOException -> 0x00be }
            if (r4 < 0) goto L_0x009f
            int r0 = r0 + r4
            r5 = 0
            r2.write(r3, r5, r4)     // Catch:{ IOException -> 0x00be }
            goto L_0x007d
        L_0x0106:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00a8
        L_0x010b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00e6
        L_0x0110:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00f8
        L_0x0115:
            r0 = move-exception
            r1 = r2
            goto L_0x00f3
        L_0x0118:
            r0 = move-exception
            r1 = r2
            goto L_0x00bf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.reading.Converter.a(java.io.InputStream, int, java.lang.String):java.lang.String");
    }

    public static String a(String str) {
        String str2 = "";
        for (String trim : str != null ? str.split(";") : new String[0]) {
            String lowerCase = trim.trim().toLowerCase(Locale.getDefault());
            if (lowerCase.startsWith("charset=")) {
                str2 = lowerCase.substring(8);
            }
        }
        return str2.length() == 0 ? "ISO-8859-1" : str2;
    }

    private static String a(String str, ByteArrayOutputStream byteArrayOutputStream, BufferedInputStream bufferedInputStream, String str2) {
        int min;
        int i;
        int i2 = Integer.MAX_VALUE;
        byte[] bArr = new byte[2048];
        int i3 = 0;
        while (i3 < 2048) {
            int read = bufferedInputStream.read(bArr);
            if (read < 0) {
                break;
            }
            i3 += read;
            byteArrayOutputStream.write(bArr, 0, read);
        }
        String byteArrayOutputStream2 = byteArrayOutputStream.toString(str2);
        int indexOf = byteArrayOutputStream2.indexOf(str);
        int length = str.length();
        if (indexOf > 0) {
            char charAt = byteArrayOutputStream2.charAt(indexOf + length);
            if (charAt == '\'') {
                i = indexOf + 1;
                min = byteArrayOutputStream2.indexOf("'", i + length);
            } else if (charAt == '\"') {
                i = indexOf + 1;
                min = byteArrayOutputStream2.indexOf("\"", i + length);
            } else {
                int indexOf2 = byteArrayOutputStream2.indexOf("\"", indexOf + length);
                if (indexOf2 < 0) {
                    indexOf2 = Integer.MAX_VALUE;
                }
                int indexOf3 = byteArrayOutputStream2.indexOf(" ", indexOf + length);
                if (indexOf3 >= 0) {
                    i2 = indexOf3;
                }
                min = Math.min(indexOf2, i2);
                int indexOf4 = byteArrayOutputStream2.indexOf("'", indexOf + length);
                if (indexOf4 > 0) {
                    min = Math.min(min, indexOf4);
                    i = indexOf;
                } else {
                    i = indexOf;
                }
            }
            if (min > i + length && min < i + length + 40) {
                String c2 = SHelper.c(byteArrayOutputStream2.substring(i + length, min));
                try {
                    bufferedInputStream.reset();
                    byteArrayOutputStream.reset();
                    return c2;
                } catch (IOException e) {
                    Log.e("Lightning", "Couldn't reset stream to re-read with new encoding " + c2 + " " + e.toString());
                }
            }
        }
        return null;
    }

    public final String a(InputStream inputStream, String str) {
        return a(inputStream, this.a, str);
    }
}
