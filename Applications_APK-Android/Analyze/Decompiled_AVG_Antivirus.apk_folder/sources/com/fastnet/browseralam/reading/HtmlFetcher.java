package com.fastnet.browseralam.reading;

import android.util.Log;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class HtmlFetcher {
    private String a = "https://github.com/karussell/snacktory";
    private String b = ("Mozilla/5.0 (compatible; Snacktory; +" + this.a + ")");
    private String c = "max-age=0";
    private String d = "en-us";
    private String e = "application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    private String f = "UTF-8";
    private SCache g;
    private final AtomicInteger h = new AtomicInteger(0);
    private int i = -1;
    private ArticleTextExtractor j = new ArticleTextExtractor();
    private final Set k = new b(this);

    static {
        SHelper.a();
        SHelper.b();
        SHelper.c();
    }

    private JResult a(String str, String str2) {
        JResult a2;
        if (this.g == null || (a2 = this.g.a()) == null) {
            return null;
        }
        a2.a(str);
        a2.b(str2);
        this.h.addAndGet(1);
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.reading.SHelper.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.reading.SHelper.a(java.lang.String, java.lang.String):int
      com.fastnet.browseralam.reading.SHelper.a(java.lang.String, boolean):java.lang.String */
    private String a(String str, int i2) {
        String str2;
        int i3 = -1;
        try {
            HttpURLConnection b2 = b(str, i2);
            b2.setInstanceFollowRedirects(false);
            b2.setRequestMethod("HEAD");
            b2.connect();
            int responseCode = b2.getResponseCode();
            try {
                b2.getInputStream().close();
                if (responseCode == 200) {
                    Log.e("Lightning", responseCode + " url:" + str + " resolved:" + ((String) null));
                    return str;
                }
                String headerField = b2.getHeaderField("Location");
                try {
                    if (responseCode / 100 != 3 || headerField == null) {
                        Log.e("Lightning", responseCode + " url:" + str + " resolved:" + headerField);
                        return str;
                    }
                    String replaceAll = headerField.replaceAll(" ", "+");
                    String b3 = (str.startsWith("http://bit.ly") || str.startsWith("http://is.gd")) ? b(replaceAll) : replaceAll;
                    try {
                        if (this.k.contains(SHelper.a(b3, true))) {
                            b3 = a(b3, i2);
                        }
                        Log.e("Lightning", responseCode + " url:" + str + " resolved:" + b3);
                        return b3;
                    } catch (Exception e2) {
                        Exception exc = e2;
                        i3 = responseCode;
                        str2 = b3;
                        e = exc;
                        try {
                            Log.e("Lightning", "getResolvedUrl:" + str + " Error:" + e.getMessage());
                            Log.e("Lightning", i3 + " url:" + str + " resolved:" + str2);
                            return "";
                        } catch (Throwable th) {
                            th = th;
                            Log.e("Lightning", i3 + " url:" + str + " resolved:" + str2);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        Throwable th3 = th2;
                        i3 = responseCode;
                        str2 = b3;
                        th = th3;
                        Log.e("Lightning", i3 + " url:" + str + " resolved:" + str2);
                        throw th;
                    }
                } catch (Exception e3) {
                    e = e3;
                    int i4 = responseCode;
                    str2 = headerField;
                    i3 = i4;
                    Log.e("Lightning", "getResolvedUrl:" + str + " Error:" + e.getMessage());
                    Log.e("Lightning", i3 + " url:" + str + " resolved:" + str2);
                    return "";
                } catch (Throwable th4) {
                    th = th4;
                    int i5 = responseCode;
                    str2 = headerField;
                    i3 = i5;
                    Log.e("Lightning", i3 + " url:" + str + " resolved:" + str2);
                    throw th;
                }
            } catch (Exception e4) {
                e = e4;
                i3 = responseCode;
                str2 = null;
            } catch (Throwable th5) {
                th = th5;
                i3 = responseCode;
                str2 = null;
                Log.e("Lightning", i3 + " url:" + str + " resolved:" + str2);
                throw th;
            }
        } catch (Exception e5) {
            e = e5;
            str2 = null;
        } catch (Throwable th6) {
            th = th6;
            str2 = null;
            Log.e("Lightning", i3 + " url:" + str + " resolved:" + str2);
            throw th;
        }
    }

    private static String b(String str) {
        StringBuilder sb = new StringBuilder();
        for (char c2 : str.toCharArray()) {
            if (c2 < 128) {
                sb.append(c2);
            } else {
                sb.append(String.format("%%%02X", Integer.valueOf(c2)));
            }
        }
        return sb.toString();
    }

    private HttpURLConnection b(String str, int i2) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection(Proxy.NO_PROXY);
        httpURLConnection.setRequestProperty("User-Agent", this.b);
        httpURLConnection.setRequestProperty("Accept", this.e);
        httpURLConnection.setRequestProperty("Accept-Language", this.d);
        httpURLConnection.setRequestProperty("content-charset", this.f);
        httpURLConnection.addRequestProperty("Referer", this.a);
        httpURLConnection.setRequestProperty("Cache-Control", this.c);
        httpURLConnection.setRequestProperty("Accept-Encoding", "gzip, deflate");
        httpURLConnection.setConnectTimeout(i2);
        httpURLConnection.setReadTimeout(i2);
        return httpURLConnection;
    }

    public final JResult a(String str) {
        String m = SHelper.m(str);
        String k2 = SHelper.k(m);
        if (k2 == null && (k2 = SHelper.l(m)) == null) {
            k2 = m;
        }
        JResult a2 = a(k2, str);
        if (a2 != null) {
            return a2;
        }
        String a3 = a(k2, 5000);
        if (a3.isEmpty()) {
            Log.d("Lightning", "resolved url is empty. Url is: " + k2);
            return new JResult().a(k2);
        }
        if (a3 != null && a3.trim().length() > k2.length()) {
            k2 = SHelper.b(k2, a3);
        }
        JResult a4 = a(k2, str);
        if (a4 != null) {
            return a4;
        }
        JResult jResult = new JResult();
        jResult.a(k2);
        jResult.b(str);
        jResult.k(SHelper.n(k2));
        String lowerCase = k2.toLowerCase(Locale.getDefault());
        if (!SHelper.g(lowerCase) && !SHelper.i(lowerCase) && !SHelper.h(lowerCase)) {
            if (SHelper.e(lowerCase) || SHelper.f(lowerCase)) {
                jResult.j(k2);
            } else if (SHelper.j(lowerCase)) {
                jResult.g(k2);
            } else {
                ArticleTextExtractor articleTextExtractor = this.j;
                HttpURLConnection b2 = b(k2, 5000);
                b2.setInstanceFollowRedirects(true);
                String contentEncoding = b2.getContentEncoding();
                String a5 = new Converter(k2).a((contentEncoding == null || !contentEncoding.equalsIgnoreCase("gzip")) ? (contentEncoding == null || !contentEncoding.equalsIgnoreCase("deflate")) ? b2.getInputStream() : new InflaterInputStream(b2.getInputStream(), new Inflater(true)) : new GZIPInputStream(b2.getInputStream()), Converter.a(b2.getContentType()));
                Log.d("Lightning", a5.length() + " FetchAsString:" + k2);
                articleTextExtractor.a(jResult, a5);
                if (jResult.a().isEmpty()) {
                    jResult.d(SHelper.d(k2));
                }
                jResult.d(SHelper.b(k2, jResult.a()));
                jResult.g(SHelper.b(k2, jResult.c()));
                jResult.j(SHelper.b(k2, jResult.g()));
                jResult.e(SHelper.b(k2, jResult.b()));
            }
        }
        String d2 = jResult.d();
        if (d2 == null) {
            d2 = "";
        } else if (this.i >= 0 && d2.length() > this.i) {
            d2 = d2.substring(0, this.i);
        }
        jResult.h(d2);
        synchronized (jResult) {
            jResult.notifyAll();
        }
        return jResult;
    }
}
