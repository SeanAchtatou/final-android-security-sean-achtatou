package com.fastnet.browseralam.d;

import android.app.Activity;
import com.fastnet.browseralam.view.XWebView;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

final class i implements Runnable {
    final /* synthetic */ String a;

    i(String str) {
        this.a = str;
    }

    public final void run() {
        String contentType;
        try {
            if (!this.a.startsWith("file")) {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.a).openConnection();
                httpURLConnection.setConnectTimeout(500);
                httpURLConnection.setReadTimeout(500);
                httpURLConnection.setRequestMethod("HEAD");
                httpURLConnection.connect();
                if (!b.e && (contentType = httpURLConnection.getContentType()) != null && contentType.startsWith("video") && b.b.get() != null && b.c.get() != null && ((XWebView) b.c.get()).w() != null && !this.a.equals(b.d)) {
                    ((Activity) b.b.get()).runOnUiThread(new j(this));
                }
            }
        } catch (IOException e) {
        }
    }
}
