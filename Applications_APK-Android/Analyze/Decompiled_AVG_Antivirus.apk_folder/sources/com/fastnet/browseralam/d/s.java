package com.fastnet.browseralam.d;

import android.app.DownloadManager;
import android.content.Context;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;

public final class s extends Thread {
    private final Context a;
    private final DownloadManager.Request b;
    private final String c;
    private final String d;
    private final String e;

    public s(Context context, DownloadManager.Request request, String str, String str2, String str3) {
        this.a = context.getApplicationContext();
        this.b = request;
        this.c = str;
        this.d = str2;
        this.e = str3;
        aq.a(context, (int) R.string.download_pending);
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r1v2 */
    /* JADX WARN: Type inference failed for: r1v3, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r1v14 */
    /* JADX WARN: Type inference failed for: r1v15 */
    /* JADX WARN: Type inference failed for: r1v16 */
    /* JADX WARN: Type inference failed for: r1v17 */
    /* JADX WARN: Type inference failed for: r1v18 */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x00ff, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0102, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x010b, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0111, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0114, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0117, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x011a, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0120, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x012c, code lost:
        r0 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0131, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00a6, code lost:
        r0 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x00b6, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x00b9, code lost:
        r1.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x00e3, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x00e4, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x00ec, code lost:
        r0 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x00f0, code lost:
        r0 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x00f8, code lost:
        r0 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x00fc, code lost:
        r2 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x012b A[ExcHandler: IllegalArgumentException (e java.lang.IllegalArgumentException), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0130 A[ExcHandler: IllegalArgumentException (e java.lang.IllegalArgumentException), Splitter:B:11:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00b6 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x00e3 A[ExcHandler: all (r1v13 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:11:0x000e] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            r1 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ IllegalArgumentException -> 0x012b, IOException -> 0x00a5, all -> 0x00b6 }
            java.lang.String r2 = r6.c     // Catch:{ IllegalArgumentException -> 0x012b, IOException -> 0x00eb, all -> 0x00b6 }
            r0.<init>(r2)     // Catch:{ IllegalArgumentException -> 0x012b, IOException -> 0x00ef, all -> 0x00b6 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IllegalArgumentException -> 0x012b, IOException -> 0x00f3, all -> 0x00b6 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IllegalArgumentException -> 0x012b, IOException -> 0x00f7, all -> 0x00b6 }
            java.lang.String r2 = r6.d     // Catch:{ IllegalArgumentException -> 0x0130, IOException -> 0x00fb, all -> 0x00e3 }
            if (r2 == 0) goto L_0x0028
            java.lang.String r2 = r6.d     // Catch:{ IllegalArgumentException -> 0x0130, IOException -> 0x00fe, all -> 0x00e3 }
            int r2 = r2.length()     // Catch:{ IllegalArgumentException -> 0x0130, IOException -> 0x0101, all -> 0x00e3 }
            if (r2 <= 0) goto L_0x0028
            java.lang.String r2 = "Cookie"
            java.lang.String r3 = r6.d     // Catch:{ IllegalArgumentException -> 0x0130, IOException -> 0x0107, all -> 0x00e3 }
            r0.addRequestProperty(r2, r3)     // Catch:{ IllegalArgumentException -> 0x0130, IOException -> 0x010a, all -> 0x00e3 }
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = r6.e     // Catch:{ IllegalArgumentException -> 0x0130, IOException -> 0x0110, all -> 0x00e3 }
            r0.setRequestProperty(r2, r3)     // Catch:{ IllegalArgumentException -> 0x0130, IOException -> 0x0113, all -> 0x00e3 }
        L_0x0028:
            r0.connect()     // Catch:{ IllegalArgumentException -> 0x0130, IOException -> 0x0116, all -> 0x00e3 }
            int r2 = r0.getResponseCode()     // Catch:{ IllegalArgumentException -> 0x0130, IOException -> 0x0119, all -> 0x00e3 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 != r3) goto L_0x013d
            java.lang.String r2 = "Content-Type"
            java.lang.String r2 = r0.getHeaderField(r2)     // Catch:{ IllegalArgumentException -> 0x0130, IOException -> 0x011f, all -> 0x00e3 }
            if (r2 == 0) goto L_0x013a
            r3 = 59
            int r3 = r2.indexOf(r3)     // Catch:{ IllegalArgumentException -> 0x0134, IOException -> 0x0122, all -> 0x00e3 }
            r4 = -1
            if (r3 == r4) goto L_0x0049
            r4 = 0
            java.lang.String r2 = r2.substring(r4, r3)     // Catch:{ IllegalArgumentException -> 0x0134, IOException -> 0x0124, all -> 0x00e3 }
        L_0x0049:
            java.lang.String r3 = "Content-Disposition"
            java.lang.String r3 = r0.getHeaderField(r3)     // Catch:{ IllegalArgumentException -> 0x0134, IOException -> 0x0128, all -> 0x00e3 }
            if (r3 == 0) goto L_0x0052
            r1 = r3
        L_0x0052:
            if (r0 == 0) goto L_0x0137
            r0.disconnect()
            r0 = r2
        L_0x0058:
            if (r0 == 0) goto L_0x00ca
            java.lang.String r2 = "text/plain"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x00da }
            if (r2 != 0) goto L_0x006a
            java.lang.String r2 = "application/octet-stream"
            boolean r2 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x00da }
            if (r2 == 0) goto L_0x007f
        L_0x006a:
            android.webkit.MimeTypeMap r2 = android.webkit.MimeTypeMap.getSingleton()     // Catch:{ Exception -> 0x00da }
            java.lang.String r3 = r6.c     // Catch:{ Exception -> 0x00da }
            java.lang.String r3 = android.webkit.MimeTypeMap.getFileExtensionFromUrl(r3)     // Catch:{ Exception -> 0x00da }
            java.lang.String r2 = r2.getMimeTypeFromExtension(r3)     // Catch:{ Exception -> 0x00da }
            if (r2 == 0) goto L_0x007f
            android.app.DownloadManager$Request r3 = r6.b     // Catch:{ Exception -> 0x00da }
            r3.setMimeType(r2)     // Catch:{ Exception -> 0x00da }
        L_0x007f:
            java.lang.String r2 = r6.c     // Catch:{ Exception -> 0x00da }
            java.lang.String r0 = android.webkit.URLUtil.guessFileName(r2, r1, r0)     // Catch:{ Exception -> 0x00da }
            com.fastnet.browseralam.h.a.a()     // Catch:{ Exception -> 0x00da }
            java.lang.String r1 = com.fastnet.browseralam.h.a.k()     // Catch:{ Exception -> 0x00da }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x00da }
            r2.<init>(r1)     // Catch:{ Exception -> 0x00da }
            boolean r1 = r2.exists()     // Catch:{ Exception -> 0x00da }
            if (r1 != 0) goto L_0x00bd
            boolean r1 = r2.mkdir()     // Catch:{ Exception -> 0x00da }
            if (r1 != 0) goto L_0x00bd
            android.content.Context r0 = r6.a     // Catch:{ Exception -> 0x00da }
            java.lang.String r1 = "Invalid download directory"
            com.fastnet.browseralam.i.aq.a(r0, r1)     // Catch:{ Exception -> 0x00da }
        L_0x00a4:
            return
        L_0x00a5:
            r0 = move-exception
            r0 = r1
            r2 = r1
        L_0x00a8:
            r5 = r0
            r0 = r2
            r2 = r5
            if (r2 == 0) goto L_0x00b0
            r2.disconnect()     // Catch:{ all -> 0x00e8 }
        L_0x00b0:
            if (r2 == 0) goto L_0x0058
            r2.disconnect()
            goto L_0x0058
        L_0x00b6:
            r0 = move-exception
        L_0x00b7:
            if (r1 == 0) goto L_0x00bc
            r1.disconnect()
        L_0x00bc:
            throw r0
        L_0x00bd:
            android.net.Uri r1 = android.net.Uri.fromFile(r2)     // Catch:{ Exception -> 0x00da }
            android.net.Uri r0 = android.net.Uri.withAppendedPath(r1, r0)     // Catch:{ Exception -> 0x00da }
            android.app.DownloadManager$Request r1 = r6.b     // Catch:{ Exception -> 0x00da }
            r1.setDestinationUri(r0)     // Catch:{ Exception -> 0x00da }
        L_0x00ca:
            android.content.Context r0 = r6.a     // Catch:{ Exception -> 0x00da }
            java.lang.String r1 = "download"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x00da }
            android.app.DownloadManager r0 = (android.app.DownloadManager) r0     // Catch:{ Exception -> 0x00da }
            android.app.DownloadManager$Request r1 = r6.b     // Catch:{ Exception -> 0x00da }
            r0.enqueue(r1)     // Catch:{ Exception -> 0x00da }
            goto L_0x00a4
        L_0x00da:
            r0 = move-exception
            android.content.Context r0 = r6.a
            java.lang.String r1 = "Download failed"
            com.fastnet.browseralam.i.aq.a(r0, r1)
            goto L_0x00a4
        L_0x00e3:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00b7
        L_0x00e8:
            r0 = move-exception
            r1 = r2
            goto L_0x00b7
        L_0x00eb:
            r0 = move-exception
            r0 = r1
            r2 = r1
            goto L_0x00a8
        L_0x00ef:
            r0 = move-exception
            r0 = r1
            r2 = r1
            goto L_0x00a8
        L_0x00f3:
            r0 = move-exception
            r0 = r1
            r2 = r1
            goto L_0x00a8
        L_0x00f7:
            r0 = move-exception
            r0 = r1
            r2 = r1
            goto L_0x00a8
        L_0x00fb:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x00fe:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x0101:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x0104:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x0107:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x010a:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x010d:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x0110:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x0113:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x0116:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x0119:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x011c:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x011f:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x0122:
            r3 = move-exception
            goto L_0x00a8
        L_0x0124:
            r3 = move-exception
            goto L_0x00a8
        L_0x0126:
            r3 = move-exception
            goto L_0x00a8
        L_0x0128:
            r3 = move-exception
            goto L_0x00a8
        L_0x012b:
            r0 = move-exception
            r0 = r1
            r2 = r1
            goto L_0x00a8
        L_0x0130:
            r2 = move-exception
            r2 = r1
            goto L_0x00a8
        L_0x0134:
            r3 = move-exception
            goto L_0x00a8
        L_0x0137:
            r0 = r2
            goto L_0x0058
        L_0x013a:
            r2 = r1
            goto L_0x0049
        L_0x013d:
            r2 = r1
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.d.s.run():void");
    }
}
