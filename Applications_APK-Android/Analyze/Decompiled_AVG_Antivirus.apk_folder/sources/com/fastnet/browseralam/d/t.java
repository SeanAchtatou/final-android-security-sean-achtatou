package com.fastnet.browseralam.d;

import com.jobstrak.drawingfun.lib.mopub.common.Constants;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class t {
    static final Pattern a = Pattern.compile("(?:(http|https|file)\\:\\/\\/)?(?:([-A-Za-z0-9$_.+!*'(),;?&=]+(?:\\:[-A-Za-z0-9$_.+!*'(),;?&=]+)?)@)?([a-zA-Z0-9 -퟿豈-﷏ﷰ-￯%_-][a-zA-Z0-9 -퟿豈-﷏ﷰ-￯%_\\.-]*|\\[[0-9a-fA-F:\\.]+\\])?(?:\\:([0-9]*))?(\\/?[^#]*)?.*", 2);
    private String b;
    private String c;
    private int d;
    private String e;
    private String f;

    public t(String str) {
        if (str == null) {
            throw new IllegalArgumentException("address can't be null");
        }
        this.b = "";
        this.c = "";
        this.d = -1;
        this.e = "/";
        this.f = "";
        Matcher matcher = a.matcher(str);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Parsing of address '" + str + "' failed");
        }
        String group = matcher.group(1);
        if (group != null) {
            this.b = group.toLowerCase(Locale.ROOT);
        }
        String group2 = matcher.group(2);
        if (group2 != null) {
            this.f = group2;
        }
        String group3 = matcher.group(3);
        if (group3 != null) {
            this.c = group3;
        }
        String group4 = matcher.group(4);
        if (group4 != null && !group4.isEmpty()) {
            try {
                this.d = Integer.parseInt(group4);
            } catch (NumberFormatException e2) {
                throw new RuntimeException("Parsing of port number failed", e2);
            }
        }
        String group5 = matcher.group(5);
        if (group5 != null && !group5.isEmpty()) {
            if (group5.charAt(0) == '/') {
                this.e = group5;
            } else {
                this.e = "/" + group5;
            }
        }
        if (this.d == 443 && "".equals(this.b)) {
            this.b = "https";
        } else if (this.d == -1) {
            if ("https".equals(this.b)) {
                this.d = 443;
            } else {
                this.d = 80;
            }
        }
        if ("".equals(this.b)) {
            this.b = Constants.HTTP;
        }
    }

    public final String a() {
        return this.c;
    }

    public final void a(String str) {
        this.e = str;
    }

    public final String b() {
        return this.e;
    }

    public final String toString() {
        String str = "";
        if ((this.d != 443 && "https".equals(this.b)) || (this.d != 80 && Constants.HTTP.equals(this.b))) {
            str = ":" + Integer.toString(this.d);
        }
        String str2 = "";
        if (!this.f.isEmpty()) {
            str2 = this.f + '@';
        }
        return this.b + "://" + str2 + this.c + str + this.e;
    }
}
