package com.fastnet.browseralam.d;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class p implements View.OnClickListener {
    final /* synthetic */ AlertDialog a;
    final /* synthetic */ String b;

    p(AlertDialog alertDialog, String str) {
        this.a = alertDialog;
        this.b = str;
    }

    public final void onClick(View view) {
        this.a.dismiss();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(this.b), "video/*");
        ((Activity) b.b.get()).startActivity(Intent.createChooser(intent, "Complete action using"));
    }
}
