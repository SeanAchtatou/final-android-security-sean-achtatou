package com.fastnet.browseralam.d;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.net.Uri;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.fastnet.browseralam.i.aq;
import java.io.File;

final class o implements View.OnClickListener {
    final /* synthetic */ AlertDialog a;
    final /* synthetic */ String b;
    final /* synthetic */ TextView c;
    final /* synthetic */ EditText d;
    final /* synthetic */ Activity e;

    o(AlertDialog alertDialog, String str, TextView textView, EditText editText, Activity activity) {
        this.a = alertDialog;
        this.b = str;
        this.c = textView;
        this.d = editText;
        this.e = activity;
    }

    public final void onClick(View view) {
        try {
            this.a.dismiss();
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(this.b));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(1);
            request.setDestinationUri(Uri.withAppendedPath(Uri.fromFile(new File(this.c.getText().toString())), this.d.getText().toString()));
            request.allowScanningByMediaScanner();
            ((DownloadManager) this.e.getSystemService("download")).enqueue(request);
        } catch (Exception e2) {
            aq.a(this.e, "Download failed");
        }
    }
}
