package com.fastnet.browseralam.d;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.URLUtil;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;
import java.io.File;

public final class a {
    private static String a(String str) {
        boolean z;
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            }
            char c = charArray[i];
            if (c == '[' || c == ']' || c == '|') {
                z = true;
            } else {
                i++;
            }
        }
        if (!z) {
            return str;
        }
        StringBuilder sb = new StringBuilder("");
        for (char c2 : charArray) {
            if (c2 == '[' || c2 == ']' || c2 == '|') {
                sb.append('%');
                sb.append(Integer.toHexString(c2));
            } else {
                sb.append(c2);
            }
        }
        return sb.toString();
    }

    public static void a(Activity activity, String str, String str2, String str3, String str4) {
        String string;
        int i;
        if (str3 == null || !str3.regionMatches(true, 0, "attachment", 0, 10)) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse(str), str4);
            intent.addFlags(268435456);
            ResolveInfo resolveActivity = activity.getPackageManager().resolveActivity(intent, 65536);
            if (resolveActivity != null) {
                ComponentName componentName = activity.getComponentName();
                if (!componentName.getPackageName().equals(resolveActivity.activityInfo.packageName) || !componentName.getClassName().equals(resolveActivity.activityInfo.name)) {
                    try {
                        activity.startActivity(intent);
                        return;
                    } catch (ActivityNotFoundException e) {
                    }
                }
            }
        }
        String guessFileName = URLUtil.guessFileName(str, str3, str4);
        String externalStorageState = Environment.getExternalStorageState();
        if (!externalStorageState.equals("mounted")) {
            if (externalStorageState.equals("shared")) {
                string = activity.getString(R.string.download_sdcard_busy_dlg_msg);
                i = R.string.download_sdcard_busy_dlg_title;
            } else {
                string = activity.getString(R.string.download_no_sdcard_dlg_msg, new Object[]{guessFileName});
                i = R.string.download_no_sdcard_dlg_title;
            }
            new AlertDialog.Builder(activity).setTitle(i).setIcon(17301543).setMessage(string).setPositiveButton((int) R.string.action_ok, (DialogInterface.OnClickListener) null).show();
            return;
        }
        try {
            t tVar = new t(str);
            tVar.a(a(tVar.b()));
            String tVar2 = tVar.toString();
            try {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(tVar2));
                request.setMimeType(str4);
                com.fastnet.browseralam.h.a.a();
                File file = new File(com.fastnet.browseralam.h.a.k());
                if (file.exists() || file.mkdir()) {
                    request.setDestinationUri(Uri.withAppendedPath(Uri.fromFile(file), guessFileName));
                    request.allowScanningByMediaScanner();
                    request.setDescription(tVar.a());
                    String cookie = CookieManager.getInstance().getCookie(str);
                    request.addRequestHeader("cookie", cookie);
                    request.setNotificationVisibility(1);
                    if (str4 != null) {
                        try {
                            ((DownloadManager) activity.getSystemService("download")).enqueue(request);
                            aq.a(activity, (int) R.string.download_pending);
                        } catch (Exception e2) {
                            aq.a(activity, "Download failed");
                        }
                    } else if (!TextUtils.isEmpty(tVar2)) {
                        new s(activity, request, tVar2, cookie, str2).start();
                    }
                } else {
                    aq.a(activity, "Invalid download directory");
                }
            } catch (IllegalArgumentException e3) {
                aq.a(activity, (int) R.string.cannot_download);
            }
        } catch (Exception e4) {
            Log.e("DLHandler", "Exception while trying to parse url '" + str + '\'', e4);
        }
    }
}
