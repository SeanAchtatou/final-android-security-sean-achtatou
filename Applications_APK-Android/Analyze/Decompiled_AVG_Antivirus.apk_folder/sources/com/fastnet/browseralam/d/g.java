package com.fastnet.browseralam.d;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.net.Uri;
import android.widget.EditText;
import android.widget.TextView;
import com.fastnet.browseralam.i.aq;
import java.io.File;

final class g implements DialogInterface.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ TextView b;
    final /* synthetic */ EditText c;
    final /* synthetic */ Activity d;

    g(String str, TextView textView, EditText editText, Activity activity) {
        this.a = str;
        this.b = textView;
        this.c = editText;
        this.d = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        try {
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(this.a));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(1);
            request.setDestinationUri(Uri.withAppendedPath(Uri.fromFile(new File(this.b.getText().toString())), this.c.getText().toString()));
            request.allowScanningByMediaScanner();
            ((DownloadManager) this.d.getSystemService("download")).enqueue(request);
        } catch (Exception e) {
            aq.a(this.d, "Download failed");
        }
    }
}
