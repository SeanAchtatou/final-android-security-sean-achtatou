package com.fastnet.browseralam.d;

import android.view.View;
import android.widget.TextView;

final class k implements View.OnClickListener {
    boolean a;
    final /* synthetic */ TextView b;

    k(TextView textView) {
        this.b = textView;
    }

    public final void onClick(View view) {
        if (this.a) {
            this.b.setSingleLine(true);
            this.b.setMaxLines(1);
            this.a = false;
        } else {
            this.b.setSingleLine(false);
            this.b.setMaxLines(6);
            this.a = true;
        }
        this.b.requestLayout();
    }
}
