package com.fastnet.browseralam.d;

import android.app.Activity;
import android.app.AlertDialog;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import com.fastnet.browseralam.R;

public final class u implements DownloadListener {
    /* access modifiers changed from: private */
    public final Activity a;

    public u(Activity activity) {
        this.a = activity;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        String guessFileName = URLUtil.guessFileName(str, str3, str4);
        v vVar = new v(this, str, str2, str3, str4);
        new AlertDialog.Builder(this.a).setTitle(guessFileName).setMessage(this.a.getResources().getString(R.string.dialog_download)).setPositiveButton(this.a.getResources().getString(R.string.action_download), vVar).setNegativeButton(this.a.getResources().getString(R.string.action_cancel), vVar).show();
    }
}
