package com.fastnet.browseralam.d;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.ey;
import com.fastnet.browseralam.h.a;
import com.fastnet.browseralam.view.XWebView;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class b {
    /* access modifiers changed from: private */
    public static ExecutorService a;
    /* access modifiers changed from: private */
    public static WeakReference b;
    /* access modifiers changed from: private */
    public static WeakReference c;
    /* access modifiers changed from: private */
    public static String d = "";
    /* access modifiers changed from: private */
    public static boolean e = false;
    private static View.OnAttachStateChangeListener f = new c();

    public static void a() {
        if (a != null) {
            a.shutdownNow();
            a = null;
            if (c.get() != null && ((XWebView) c.get()).w() != null) {
                ((XWebView) c.get()).w().removeOnAttachStateChangeListener(f);
            }
        }
    }

    public static void a(Activity activity) {
        if (a == null || a.isShutdown()) {
            a = Executors.newSingleThreadExecutor();
        }
        b = new WeakReference(activity);
        c = new WeakReference(null);
        e = false;
    }

    public static void a(Activity activity, WebView webView, String str) {
        if (!str.equals(d)) {
            d = str;
            webView.pauseTimers();
            a.a();
            String k = a.k();
            File file = new File(k);
            File file2 = (file.exists() || file.mkdir()) ? file : new File(com.fastnet.browseralam.a.a.b);
            new r(k);
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            RelativeLayout relativeLayout = (RelativeLayout) activity.getLayoutInflater().inflate((int) R.layout.download_dialog, (ViewGroup) null);
            ((TextView) relativeLayout.findViewById(R.id.dialog_title)).setText("Download Video");
            TextView textView = (TextView) relativeLayout.findViewById(R.id.url);
            textView.setText(str);
            textView.setOnClickListener(new k(textView));
            EditText editText = (EditText) relativeLayout.findViewById(R.id.editName);
            int indexOf = str.indexOf(".mp4");
            editText.setText(indexOf != -1 ? str.substring(str.lastIndexOf(47, indexOf) + 1, indexOf + 4) : str.substring(str.lastIndexOf(47) + 1, str.length()));
            TextView textView2 = (TextView) relativeLayout.findViewById(R.id.download_path);
            textView2.setText(file2.getPath());
            textView2.setOnClickListener(new m(new ey(activity, new l(textView2)).a()));
            builder.setView(relativeLayout);
            builder.setCancelable(true);
            AlertDialog show = builder.show();
            show.setCanceledOnTouchOutside(true);
            show.setOnCancelListener(new n(webView));
            relativeLayout.findViewById(R.id.download).setOnClickListener(new o(show, str, textView2, editText, activity));
            relativeLayout.findViewById(R.id.open).setOnClickListener(new p(show, str));
            relativeLayout.findViewById(R.id.cancel).setOnClickListener(new q(show, webView));
        }
    }

    public static void a(Activity activity, XWebView xWebView) {
        if (a == null || a.isShutdown()) {
            a = Executors.newSingleThreadExecutor();
        }
        b = new WeakReference(activity);
        c = new WeakReference(xWebView);
        xWebView.w().addOnAttachStateChangeListener(f);
        e = false;
    }

    public static void a(Activity activity, String str) {
        a.a();
        String k = a.k();
        File file = new File(k);
        File file2 = (file.exists() || file.mkdir()) ? file : new File(com.fastnet.browseralam.a.a.b);
        new r(k);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        RelativeLayout relativeLayout = (RelativeLayout) activity.getLayoutInflater().inflate((int) R.layout.download_dialog, (ViewGroup) null);
        relativeLayout.findViewById(R.id.url).setVisibility(8);
        EditText editText = (EditText) relativeLayout.findViewById(R.id.editName);
        editText.setText(URLUtil.guessFileName(str, null, null));
        TextView textView = (TextView) relativeLayout.findViewById(R.id.download_path);
        textView.setText(file2.getPath());
        textView.setOnClickListener(new f(new ey(activity, new e(textView)).a()));
        builder.setView(relativeLayout);
        relativeLayout.removeView(relativeLayout.findViewById(R.id.download));
        relativeLayout.removeView(relativeLayout.findViewById(R.id.open));
        relativeLayout.removeView(relativeLayout.findViewById(R.id.cancel));
        builder.setPositiveButton("Download", new g(str, textView, editText, activity));
        builder.setNegativeButton("Cancel", new h());
        builder.setCancelable(true);
        builder.show().setCanceledOnTouchOutside(true);
    }

    public static void a(String str) {
        if (!e) {
            a.execute(new i(str));
        }
    }
}
