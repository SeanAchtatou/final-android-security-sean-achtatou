package com.fastnet.browseralam.a;

import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserApp;
import com.fastnet.browseralam.c.c;
import com.fastnet.browseralam.h.a;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public final class b {
    private static a a = a.a();
    private static String b;
    private static String c;
    private static String d;
    private static final String e = ("<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta content=\"en-us\" http-equiv=\"Content-Language\" /><meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" /><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\"><title>" + BrowserApp.a().getString(R.string.home) + "</title></head><style>body{background:#FFFFFF;text-align:center;margin:0px;}#search_input{height:40px; width:100%;outline:none;border:none;font-size: 16px;background-color:transparent;}span { display: block; overflow: hidden; padding-left:5px;vertical-align:middle;}.search_bar{display:table;vertical-align:middle; width:90%; height:40px; max-width:500px; margin:0 auto; background-color:#fff;font-family: Arial; color: #444; -moz-border-radius: 1px;-webkit-border-radius: 2px; border-radius: 2px; border-width:1px; border-style: solid; border-color: #b3b3b3;  }#search_submit{outline:none;height:37px;float:right;color:#404040;font-size:16px;font-weight:bold;border:none; background-color:transparent;}.outer { display: table; position: absolute; height: 100%; width: 100%;}.middle { display: table-cell; vertical-align: middle;}.inner { margin-left: auto; margin-right: auto; margin-bottom:20%; <!-->maybe bad for small screens</!--> width: 100%;} img.smaller{width:80%; max-width:400px;}.box { vertical-align:middle;position:relative; display: block; margin: 10px;padding-left:10px;padding-right:10px;padding-top:5px;padding-bottom:5px; background-color:#fff;font-family: Arial;color: #444;font-size: 12px;-moz-border-radius: 2px;-webkit-border-radius: 2px;border-radius: 2px;}</style><body> <div class=\"outer\"><div class=\"middle\"><div class=\"inner\"><img id=\"logo\" class=\"smaller\" src=\"");
    private static final String f = ("<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\"><title>" + BrowserApp.a().getString(R.string.action_blank) + "</title></head><body></body></html>");

    public static String a() {
        return b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    public static void a(File file) {
        String str;
        String str2;
        if (c == null && file != null) {
            c = file.getAbsolutePath();
        }
        String u = a.u();
        if (u.equals("about:blank")) {
            File file2 = new File(file, "homepage.html");
            try {
                FileWriter fileWriter = new FileWriter(file2, false);
                fileWriter.write(f);
                fileWriter.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            b = "file://" + file2;
        } else if (!u.startsWith("about")) {
            b = u;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(e);
            switch (a.ab()) {
                case 0:
                    str = "file:///android_asset/lightning.png";
                    str2 = a.ac();
                    break;
                case 1:
                    str = d != null ? d : "file:///android_asset/google.png";
                    str2 = "https://www.google.com/search?client=lightning&ie=UTF-8&oe=UTF-8&q=";
                    break;
                case 2:
                    str = "file:///android_asset/ask.png";
                    str2 = "http://www.ask.com/web?qsrc=0&o=0&l=dir&qo=lightningBrowser&q=";
                    break;
                case 3:
                    str = "file:///android_asset/bing.png";
                    str2 = "https://www.bing.com/search?q=";
                    break;
                case 4:
                    str = "file:///android_asset/yahoo.png";
                    str2 = "https://search.yahoo.com/search?p=";
                    break;
                case 5:
                    str = "file:///android_asset/startpage.png";
                    str2 = "https://startpage.com/do/search?language=english&query=";
                    break;
                case 6:
                    str = "file:///android_asset/startpage.png";
                    str2 = "https://startpage.com/do/m/mobilesearch?language=english&query=";
                    break;
                case 7:
                    str = "file:///android_asset/duckduckgo.png";
                    str2 = "https://duckduckgo.com/?t=lightning&q=";
                    break;
                case 8:
                    str = "file:///android_asset/duckduckgo.png";
                    str2 = "https://duckduckgo.com/lite/?t=lightning&q=";
                    break;
                case 9:
                    str = "file:///android_asset/baidu.png";
                    str2 = "https://www.baidu.com/s?wd=";
                    break;
                case 10:
                    str = "file:///android_asset/yandex.png";
                    str2 = "https://yandex.ru/yandsearch?lr=21411&text=";
                    break;
                default:
                    str = "file:///android_asset/google.png";
                    str2 = "https://www.google.com/search?client=lightning&ie=UTF-8&oe=UTF-8&q=";
                    break;
            }
            sb.append(str);
            sb.append("\" ></br></br><form onsubmit=\"return search()\" class=\"search_bar\"><input type=\"submit\" id=\"search_submit\" value=\"\" ><span><input class=\"search\" type=\"text\" value=\"\" placeholder=\"Search\" id=\"search_input\" onclick=\"movecursor()\"></span></form></br></br></div></div></div><script type=\"text/javascript\">function movecursor(){ Android.moveCursor(); } function loadDoodle(url) { document.getElementById(\"logo\").src = url; }function search(){ if(document.getElementById(\"search_input\").value != \"\"){ window.location.href = \"");
            sb.append(str2);
            sb.append("\" + document.getElementById(\"search_input\").value;document.getElementById(\"search_input\").value = \"\";}return false;} </script></body></html>");
            File file3 = new File(file, "homepage.html");
            try {
                FileWriter fileWriter2 = new FileWriter(file3, false);
                fileWriter2.write(sb.toString());
                fileWriter2.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            b = "file://" + file3;
        }
    }

    public static void a(String str) {
        d = str;
    }

    public static void b() {
        d = c.f();
    }
}
