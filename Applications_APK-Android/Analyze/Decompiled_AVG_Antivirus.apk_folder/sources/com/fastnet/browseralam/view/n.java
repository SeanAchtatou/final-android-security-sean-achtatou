package com.fastnet.browseralam.view;

import android.view.MotionEvent;
import android.view.View;

final class n implements View.OnTouchListener {
    final /* synthetic */ DrawerFrame a;

    n(DrawerFrame drawerFrame) {
        this.a = drawerFrame;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        this.a.setVisibility(0);
        this.a.q.setVisibility(8);
        boolean unused = this.a.v = true;
        boolean unused2 = this.a.t = false;
        this.a.d = this.a.m.getTranslationY() - ((float) this.a.y);
        this.a.m.animate().translationY(this.a.d).setDuration(100).setListener(null).start();
        this.a.s.postDelayed(this.a.L, 100);
        return false;
    }
}
