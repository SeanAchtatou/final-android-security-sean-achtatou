package com.fastnet.browseralam.view;

import android.content.DialogInterface;
import android.webkit.GeolocationPermissions;
import com.fastnet.browseralam.view.XWebView;

final class at implements DialogInterface.OnClickListener {
    final /* synthetic */ GeolocationPermissions.Callback a;
    final /* synthetic */ String b;
    final /* synthetic */ XWebView.SpeedChromeClient c;

    at(XWebView.SpeedChromeClient speedChromeClient, GeolocationPermissions.Callback callback, String str) {
        this.c = speedChromeClient;
        this.a = callback;
        this.b = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.invoke(this.b, false, true);
    }
}
