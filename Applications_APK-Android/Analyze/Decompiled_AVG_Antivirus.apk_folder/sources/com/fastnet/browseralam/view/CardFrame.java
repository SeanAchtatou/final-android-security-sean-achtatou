package com.fastnet.browseralam.view;

import android.content.Context;
import android.support.v4.view.q;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import com.fastnet.browseralam.activity.ei;
import com.fastnet.browseralam.g.aj;
import com.fastnet.browseralam.i.aq;

public class CardFrame extends FrameLayout {
    private ei a;
    private aj b;
    private RecyclerView c;
    private RecyclerView d;
    private RecyclerView e;
    private q f;
    private DecelerateInterpolator g = new DecelerateInterpolator(2.0f);
    /* access modifiers changed from: private */
    public boolean h;
    private int i;
    private int j;
    private int k = aq.a(15);
    private int l = 20;
    private float m;
    private float n;
    private float o;
    private int p = 1;
    private int q = (-this.k);
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public int s = aq.a(1000);
    private int t = aq.a();
    private int u = (this.t / 2);
    private GestureDetector.SimpleOnGestureListener v = new k(this);

    public CardFrame(Context context) {
        super(context);
    }

    public CardFrame(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CardFrame(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public final void a(ei eiVar, aj ajVar, RecyclerView recyclerView, RecyclerView recyclerView2) {
        this.a = eiVar;
        this.b = ajVar;
        this.d = recyclerView;
        this.e = recyclerView2;
        this.c = this.d;
        this.i = aq.a();
        this.j = -this.i;
        this.e.setTranslationX((float) this.i);
        this.f = new q(getContext(), this.v);
    }

    public final void a(boolean z) {
        if (z) {
            this.e.animate().translationX(0.0f).setDuration(200).setInterpolator(this.g);
            this.d.animate().translationX((float) this.j).setDuration(200).setInterpolator(this.g);
            this.a.a(true);
        } else {
            this.e.setTranslationX(0.0f);
            this.d.setTranslationX((float) this.j);
        }
        this.n = (float) this.j;
        this.c = this.e;
        this.p = -1;
        this.q = this.k;
        this.h = true;
    }

    public final void b(boolean z) {
        if (z) {
            this.d.animate().translationX(0.0f).setDuration(200).setInterpolator(this.g);
            this.e.animate().translationX((float) this.i).setDuration(200).setInterpolator(this.g);
            this.a.a(false);
        } else {
            this.d.setTranslationX(0.0f);
            this.e.setTranslationX((float) this.i);
        }
        this.n = 0.0f;
        this.c = this.d;
        this.p = 1;
        this.q = -this.k;
        this.h = false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 2) {
            if (!this.c.canScrollHorizontally(this.p)) {
                float x = motionEvent.getX() - this.m;
                if ((!this.h && x < ((float) this.q)) || (this.h && x > ((float) this.q))) {
                    this.m = motionEvent.getX();
                    this.r = false;
                    return true;
                }
            }
        } else if (motionEvent.getAction() == 0) {
            this.b.b(false);
            this.m = motionEvent.getX();
        } else if (motionEvent.getAction() == 1) {
            this.b.b(true);
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.f.a(motionEvent);
        if (motionEvent.getAction() == 2) {
            float x = motionEvent.getX();
            this.n += x - this.m;
            if (this.n > 0.0f) {
                this.n = 0.0f;
            }
            if (this.n < ((float) this.j)) {
                this.n = (float) this.j;
            }
            this.o = this.n + ((float) this.i);
            this.d.setTranslationX(this.n);
            this.e.setTranslationX(this.o);
            this.m = x;
            return true;
        } else if (motionEvent.getAction() != 1) {
            return true;
        } else {
            this.b.b(true);
            if (this.r) {
                return false;
            }
            if (this.o > ((float) this.u)) {
                b(true);
                return true;
            }
            a(true);
            return true;
        }
    }
}
