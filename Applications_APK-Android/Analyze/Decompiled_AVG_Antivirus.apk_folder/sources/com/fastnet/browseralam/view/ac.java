package com.fastnet.browseralam.view;

import android.graphics.Bitmap;
import android.webkit.WebView;
import com.fastnet.browseralam.h.a;

final class ac extends av {
    final /* synthetic */ XWebView a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private ac(XWebView xWebView) {
        super(xWebView, (byte) 0);
        this.a = xWebView;
    }

    /* synthetic */ ac(XWebView xWebView, byte b) {
        this(xWebView);
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (!this.a.q.c(str)) {
            this.a.c.a(this.a);
            a unused = XWebView.p;
            if (a.D()) {
                this.a.a.setWebViewClient(new as(this.a, (byte) 0));
            } else {
                this.a.a.setWebViewClient(this.a.f);
            }
        }
    }
}
