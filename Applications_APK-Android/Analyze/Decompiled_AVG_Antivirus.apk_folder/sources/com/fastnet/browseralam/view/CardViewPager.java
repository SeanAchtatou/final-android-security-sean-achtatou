package com.fastnet.browseralam.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Scroller;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.g.l;
import com.fastnet.browseralam.i.aq;

public class CardViewPager extends FrameLayout implements GestureDetector.OnGestureListener {
    private Scroller a;
    private l b;
    private LayoutInflater c;
    private GestureDetector d = new GestureDetector(this);
    private Rect[] e;
    private final int f = aq.b();
    private final int g = aq.a();
    private int h = aq.a(56);
    private int i = (this.h * 3);
    private int j = (this.f - this.h);
    private float[] k = new float[0];
    private int[] l = new int[0];
    private float m;
    private float n;
    private int o;
    private int p;

    public CardViewPager(Context context) {
        super(context);
        b();
    }

    public CardViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
    }

    public CardViewPager(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        b();
    }

    @TargetApi(21)
    public CardViewPager(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        b();
    }

    private void b() {
        this.a = new Scroller(getContext());
        this.c = LayoutInflater.from(getContext());
    }

    public final void a() {
        removeAllViews();
        this.e = new Rect[this.b.a()];
        this.k = new float[this.b.a()];
        this.l = new int[this.k.length];
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.b.a()) {
                View inflate = this.c.inflate((int) R.layout.card_pager_item, (ViewGroup) null);
                ((TextView) inflate.findViewById(R.id.title)).setText(this.b.a(i3));
                ((ImageView) inflate.findViewById(R.id.icon)).setImageBitmap(this.b.c(i3));
                ((ImageView) inflate.findViewById(R.id.view_holder)).setImageBitmap(this.b.b(i3));
                addView(inflate, i3, generateDefaultLayoutParams());
                this.e[i3] = new Rect();
                this.k[i3] = (float) (this.i * i3);
                this.l[i3] = aq.a(4) * i3;
                int i4 = (int) this.k[i3];
                inflate.setTranslationY((float) i4);
                this.e[i3].set(getPaddingLeft(), getPaddingTop() + i4, this.g, i4 + this.j);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public final void a(l lVar) {
        this.b = lVar;
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        this.a.fling(0, this.p, 0, (int) (-f3), 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        postInvalidate();
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.d.onTouchEvent(motionEvent)) {
            for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
                MotionEvent obtain = MotionEvent.obtain(motionEvent);
                obtain.setAction(3);
                obtain.offsetLocation((float) (-this.e[childCount].left), (float) (-this.e[childCount].top));
                getChildAt(childCount).dispatchTouchEvent(obtain);
            }
        } else if (motionEvent.getAction() == 0) {
            if (!this.a.isFinished()) {
                this.a.forceFinished(true);
            }
            this.m = motionEvent.getY();
            int childCount2 = getChildCount() - 1;
            while (true) {
                if (childCount2 < 0) {
                    break;
                } else if (this.e[childCount2].contains((int) motionEvent.getX(), (int) this.m)) {
                    this.o = childCount2;
                    break;
                } else {
                    childCount2--;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        for (int i6 = 0; i6 < getChildCount(); i6++) {
            getChildAt(i6).layout(0, 0, getWidth(), this.j);
        }
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        this.p = (int) Math.max(0.0f, Math.min(((float) this.p) + f3, (float) (getChildCount() * this.g)));
        postInvalidate();
        return true;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 2) {
            if (!this.a.isFinished()) {
                this.a.forceFinished(true);
            }
            this.n = motionEvent.getY();
            float f2 = this.n - this.m;
            float[] fArr = this.k;
            int i2 = this.o;
            float f3 = fArr[i2] + f2;
            fArr[i2] = f3;
            if (f3 < ((float) this.l[this.o])) {
                f3 = (float) this.l[this.o];
            }
            getChildAt(this.o).setTranslationY(f3);
            for (int i3 = this.o; i3 > 0; i3--) {
                if (i3 != this.o) {
                    float f4 = (-0.0045f * this.k[i3]) + 2.9f;
                    if (f4 < 1.0f) {
                        f4 = 1.0f;
                    }
                    if (f4 > 3.0f) {
                        f4 = 3.0f;
                    }
                    float[] fArr2 = this.k;
                    fArr2[i3] = (f2 / f4) + fArr2[i3];
                    if (this.k[i3] < ((float) this.l[i3])) {
                        getChildAt(i3).setTranslationY((float) this.l[i3]);
                    } else {
                        getChildAt(i3).setTranslationY(this.k[i3]);
                    }
                }
            }
            int i4 = (int) this.k[this.o];
            this.e[this.o].set(getPaddingLeft(), getPaddingTop() + i4, getWidth(), i4 + this.f);
            this.m = this.n;
        }
        return true;
    }
}
