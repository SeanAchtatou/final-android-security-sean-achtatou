package com.fastnet.browseralam.view;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class s extends GestureDetector.SimpleOnGestureListener {
    final /* synthetic */ DrawerFrame a;

    private s(DrawerFrame drawerFrame) {
        this.a = drawerFrame;
    }

    /* synthetic */ s(DrawerFrame drawerFrame, byte b) {
        this(drawerFrame);
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        int i = 100;
        if (f2 > 600.0f) {
            boolean unused = this.a.F = true;
            int i2 = (int) (1000.0d / (((double) f2) * 0.003d));
            if (i2 >= 100) {
                i = i2;
            }
            if (i > 300) {
                i = 300;
            }
            this.a.k.a(i);
        } else if (this.a.v && f2 < 0.0f) {
            this.a.j.a(this.a.i);
            if (this.a.d > 0.0f) {
                boolean unused2 = this.a.F = true;
                int i3 = (int) (1000.0d / (((double) f2) * 0.003d));
                if (i3 >= 100) {
                    i = i3;
                }
                if (i > 250) {
                    i = 250;
                }
                this.a.n.animate().translationY(0.0f).setDuration((long) i).setInterpolator(this.a.H);
                this.a.m.animate().translationY(0.0f).setDuration((long) i).setInterpolator(this.a.H);
            }
        }
        return false;
    }
}
