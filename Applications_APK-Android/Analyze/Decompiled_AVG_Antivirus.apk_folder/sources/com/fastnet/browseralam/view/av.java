package com.fastnet.browseralam.view;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.http.SslError;
import android.os.Message;
import android.support.v7.app.k;
import android.support.v7.app.l;
import android.text.method.PasswordTransformationMethod;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;
import java.io.ByteArrayInputStream;
import java.net.URISyntaxException;

class av extends WebViewClient {
    private ByteArrayInputStream a;
    final /* synthetic */ XWebView b;
    private WebResourceResponse c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public float e;

    private av(XWebView xWebView) {
        this.b = xWebView;
        this.a = new ByteArrayInputStream("".getBytes());
        this.c = new WebResourceResponse("text/plain", "utf-8", this.a);
        this.d = false;
        this.e = 0.0f;
    }

    /* synthetic */ av(XWebView xWebView, byte b2) {
        this(xWebView);
    }

    public void onFormResubmission(WebView webView, Message message, Message message2) {
        l lVar = new l(this.b.b);
        lVar.a(this.b.b.getString(R.string.title_form_resubmission));
        lVar.b(this.b.b.getString(R.string.message_form_resubmission)).b().a(this.b.b.getString(R.string.action_yes), new bc(this, message2)).b(this.b.b.getString(R.string.action_no), new bb(this, message));
        lVar.c().show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void */
    public void onPageFinished(WebView webView, String str) {
        if (webView.isShown()) {
            this.b.c.a(str, true);
        }
        if (webView.getTitle() == null || webView.getTitle().isEmpty()) {
            String unused = this.b.g = this.b.b.getString(R.string.untitled);
        } else {
            String unused2 = this.b.g = webView.getTitle();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void */
    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (this.b.c()) {
            this.b.c.a(str, false);
        }
        Bitmap unused = this.b.i = XWebView.o;
        this.b.c.a(this.b.u, this.b.z, 3);
    }

    public void onReceivedHttpAuthRequest(WebView webView, HttpAuthHandler httpAuthHandler, String str, String str2) {
        l lVar = new l(this.b.b);
        EditText editText = new EditText(this.b.b);
        EditText editText2 = new EditText(this.b.b);
        LinearLayout linearLayout = new LinearLayout(this.b.b);
        linearLayout.setOrientation(1);
        linearLayout.addView(editText);
        linearLayout.addView(editText2);
        editText.setHint(this.b.b.getString(R.string.hint_username));
        editText.setSingleLine();
        editText2.setInputType(128);
        editText2.setSingleLine();
        editText2.setTransformationMethod(new PasswordTransformationMethod());
        editText2.setHint(this.b.b.getString(R.string.hint_password));
        lVar.a(this.b.b.getString(R.string.title_sign_in));
        lVar.b(linearLayout);
        lVar.b().a(this.b.b.getString(R.string.title_sign_in), new ax(this, editText, editText2, httpAuthHandler)).b(this.b.b.getString(R.string.action_cancel), new aw(this, httpAuthHandler));
        lVar.c().show();
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        l lVar = new l(this.b.b);
        lVar.a(this.b.b.getString(R.string.title_warning));
        lVar.b(this.b.b.getString(R.string.message_untrusted_certificate)).b().a(this.b.b.getString(R.string.action_yes), new ba(this, sslErrorHandler)).b(this.b.b.getString(R.string.action_no), new az(this, sslErrorHandler));
        k c2 = lVar.c();
        if (sslError.getPrimaryError() == 3) {
            c2.show();
        } else {
            sslErrorHandler.proceed();
        }
    }

    public void onScaleChanged(WebView webView, float f, float f2) {
        if (webView.isShown() && this.b.E && XWebView.F >= 19 && !this.d && Math.abs(this.e - f2) > 0.01f) {
            this.d = webView.postDelayed(new ay(this, f2, webView), 100);
        }
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
        return this.b.q.c(webResourceRequest.getUrl().toString()) ? this.c : super.shouldInterceptRequest(webView, webResourceRequest);
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        return this.b.q.c(str) ? this.c : super.shouldInterceptRequest(webView, str);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (this.b.q.c(str)) {
            boolean unused = this.b.r = true;
            return true;
        } else if (str.startsWith("about:")) {
            return super.shouldOverrideUrlLoading(webView, str);
        } else {
            if (str.startsWith("mailto:")) {
                MailTo parse = MailTo.parse(str);
                Activity unused2 = this.b.b;
                this.b.b.startActivity(aq.a(parse.getTo(), parse.getSubject(), parse.getBody(), parse.getCc()));
                webView.reload();
                return true;
            }
            if (str.startsWith("intent://")) {
                try {
                    Intent parseUri = Intent.parseUri(str, 1);
                    if (parseUri != null) {
                        try {
                            this.b.b.startActivity(parseUri);
                            return true;
                        } catch (ActivityNotFoundException e2) {
                            return true;
                        }
                    }
                } catch (URISyntaxException e3) {
                    return false;
                }
            }
            return this.b.s.a(this.b.a, str);
        }
    }
}
