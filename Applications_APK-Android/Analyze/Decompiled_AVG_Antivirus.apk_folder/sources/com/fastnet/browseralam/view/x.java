package com.fastnet.browseralam.view;

import android.os.Handler;
import android.os.Message;
import android.view.WindowManager;

final class x extends Handler {
    final /* synthetic */ WebViewFrame a;
    private int b;

    public x(WebViewFrame webViewFrame) {
        this.a = webViewFrame;
        this.b = (int) (1000.0f / ((WindowManager) webViewFrame.a.getSystemService("window")).getDefaultDisplay().getRefreshRate());
    }

    public final void a() {
        sendEmptyMessageDelayed(1, (long) this.b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.view.WebViewFrame.a(com.fastnet.browseralam.view.WebViewFrame, boolean):boolean
     arg types: [com.fastnet.browseralam.view.WebViewFrame, int]
     candidates:
      com.fastnet.browseralam.view.WebViewFrame.a(com.fastnet.browseralam.view.WebViewFrame, float):float
      com.fastnet.browseralam.view.WebViewFrame.a(com.fastnet.browseralam.view.WebViewFrame, int):int
      com.fastnet.browseralam.view.WebViewFrame.a(com.fastnet.browseralam.view.WebViewFrame, boolean):boolean */
    public final void handleMessage(Message message) {
        if (this.a.h.computeScrollOffset()) {
            int currY = this.a.h.getCurrY();
            float unused = this.a.p = ((float) currY) - this.a.n;
            if (this.a.A == 0.0f) {
                this.a.c.flingScroll(this.a.F, this.a.G);
                this.a.h.forceFinished(true);
                boolean unused2 = this.a.r = false;
            } else if (this.a.A == ((float) this.a.y)) {
                this.a.c.flingScroll(this.a.F, this.a.G);
                this.a.h.forceFinished(true);
                boolean unused3 = this.a.r = false;
            } else {
                boolean unused4 = this.a.a();
                float unused5 = this.a.n = (float) currY;
                sendEmptyMessageDelayed(1, (long) this.b);
            }
        } else {
            boolean unused6 = this.a.r = false;
            this.a.b();
        }
    }
}
