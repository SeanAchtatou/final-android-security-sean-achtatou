package com.fastnet.browseralam.view;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.q;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.b.a;
import com.fastnet.browseralam.b.i;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.i.aq;

public class DrawerFrame extends FrameLayout {
    private final int A = aq.a(52);
    private final int B = aq.a(98);
    private int C = 0;
    private int D = 0;
    private float E = 0.0f;
    /* access modifiers changed from: private */
    public boolean F = false;
    private final DecelerateInterpolator G = new DecelerateInterpolator();
    /* access modifiers changed from: private */
    public final DecelerateInterpolator H = new DecelerateInterpolator();
    /* access modifiers changed from: private */
    public View.OnTouchListener I = new m(this);
    /* access modifiers changed from: private */
    public boolean J;
    /* access modifiers changed from: private */
    public View.OnTouchListener K = new n(this);
    /* access modifiers changed from: private */
    public final Runnable L = new p(this);
    /* access modifiers changed from: private */
    public Runnable M = new r(this);
    float a;
    float b;
    float c;
    float d;
    float e;
    float f;
    float g = 0.0f;
    float h;
    a i = new q(this);
    /* access modifiers changed from: private */
    public BrowserActivity j;
    /* access modifiers changed from: private */
    public i k;
    private RecyclerView l;
    /* access modifiers changed from: private */
    public RelativeLayout m;
    /* access modifiers changed from: private */
    public RelativeLayout n;
    private FrameLayout o;
    private Background p;
    /* access modifiers changed from: private */
    public View q;
    private q r;
    /* access modifiers changed from: private */
    public final Handler s = k.a();
    /* access modifiers changed from: private */
    public boolean t = false;
    private boolean u = false;
    /* access modifiers changed from: private */
    public boolean v = false;
    private boolean w = false;
    /* access modifiers changed from: private */
    public boolean x = false;
    /* access modifiers changed from: private */
    public final int y = aq.a(16);
    private final int z = aq.a(56);

    public DrawerFrame(Context context) {
        super(context);
    }

    public DrawerFrame(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DrawerFrame(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public final View a() {
        return this.q;
    }

    public final void a(float f2) {
        this.q.setOnTouchListener(new o(this, f2));
    }

    public final void a(RecyclerView recyclerView) {
        this.l = recyclerView;
        this.E = (float) ((int) (((double) (((float) this.D) - this.p.getTranslationY())) * 0.5d));
        this.x = recyclerView.getId() == R.id.horizontal_tabview || recyclerView.getId() == R.id.horizontal_tabview_incognito;
    }

    public final void a(i iVar, BrowserActivity browserActivity, RecyclerView recyclerView, Background background) {
        this.k = iVar;
        this.j = browserActivity;
        this.m = (RelativeLayout) findViewById(R.id.bottom_drawer);
        this.r = new q(browserActivity, new s(this, (byte) 0));
        this.l = recyclerView;
        this.o = (FrameLayout) getParent();
        this.n = (RelativeLayout) this.o.findViewById(R.id.bottom_toolbar);
        this.q = this.o.findViewById(R.id.touch_area);
        this.q.setOnTouchListener(this.I);
        this.p = background;
        this.C = aq.a() - aq.a(80);
        this.D = aq.b() - aq.a(32);
        this.E = (float) aq.a(100);
    }

    public final void a(boolean z2) {
        this.w = z2;
    }

    public final void b() {
        this.J = true;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2 = true;
        if (motionEvent.getAction() == 0) {
            this.t = true;
            this.a = motionEvent.getY();
            this.c = motionEvent.getX();
            if (this.v) {
                return true;
            }
            this.d = 0.0f;
            this.e = 0.0f;
            this.f = this.p.a();
            if (this.a < this.p.getTranslationY() && (this.a > ((float) this.z) || this.c < ((float) this.C))) {
                requestDisallowInterceptTouchEvent(true);
                this.k.a(250);
            }
            if (this.l.computeVerticalScrollOffset() > 0) {
                z2 = false;
            }
            this.u = z2;
        } else if (motionEvent.getAction() == 2) {
            if (this.u && motionEvent.getY() - this.a > 20.0f && Math.abs(this.c - motionEvent.getX()) < 20.0f) {
                this.u = false;
                this.a = motionEvent.getY();
                this.h = 0.0f;
                return true;
            }
        } else if (motionEvent.getAction() == 1) {
            if (this.v) {
                this.k.a(100);
            }
            this.v = false;
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.r.a(motionEvent);
        if (motionEvent.getAction() == 2) {
            this.b = motionEvent.getY();
            this.g = this.b - this.a;
            this.d += this.g;
            if (this.d < 0.0f) {
                this.m.setTranslationY(0.0f);
            } else {
                this.m.setTranslationY(this.d);
            }
            if (this.w) {
                this.e += this.g * 0.53f;
                if (this.e > ((float) this.A)) {
                    this.n.setTranslationY((float) this.A);
                } else if (this.e < 0.0f) {
                    this.n.setTranslationY(0.0f);
                } else {
                    this.n.setTranslationY(this.e);
                }
            }
            this.a = this.b;
            return false;
        } else if (motionEvent.getAction() == 1) {
            this.v = false;
            if (this.F) {
                this.F = false;
                return false;
            } else if (((double) this.d) > ((double) this.f) * 0.5d) {
                this.k.a(200);
                return false;
            } else {
                if (this.d > 0.0f) {
                    if (this.w) {
                        this.n.animate().translationY(0.0f).setDuration(150).setInterpolator(this.G);
                    }
                    this.m.animate().translationY(0.0f).setDuration(200).setInterpolator(this.G);
                } else if (this.x) {
                    this.k.f();
                }
                this.j.a(this.i);
                return false;
            }
        } else if (motionEvent.getAction() != 0) {
            return false;
        } else {
            if (this.a < this.p.getTranslationY()) {
                if (this.a <= ((float) this.A) && this.c >= ((float) this.C)) {
                    return false;
                }
                this.k.a(300);
                return false;
            } else if (!this.v) {
                return true;
            } else {
                this.h = this.d;
                this.e = (float) this.B;
                return true;
            }
        }
    }
}
