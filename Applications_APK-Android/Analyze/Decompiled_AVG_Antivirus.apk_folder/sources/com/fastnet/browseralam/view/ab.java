package com.fastnet.browseralam.view;

import android.view.MotionEvent;
import android.view.View;
import com.fastnet.browseralam.i.aq;

final class ab implements View.OnTouchListener {
    final /* synthetic */ XWebView a;
    private boolean b;
    private float c;
    private float d;
    private int e = aq.a(10);

    ab(XWebView xWebView) {
        this.a = xWebView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (!this.a.a.hasFocus()) {
                    this.a.a.requestFocus();
                }
                this.c = motionEvent.getX();
                this.d = motionEvent.getY();
                this.b = false;
                this.a.O.removeMessages(1);
                this.a.O.a();
                break;
            case 2:
                if (!this.b && (Math.abs(this.c - motionEvent.getX()) > ((float) this.e) || Math.abs(this.d - motionEvent.getY()) > ((float) this.e))) {
                    this.b = true;
                }
            case 1:
            case 3:
                this.a.O.removeMessages(1);
                break;
        }
        return false;
    }
}
