package com.fastnet.browseralam.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.Scroller;
import com.fastnet.browseralam.i.aq;

public class XCardPager extends FrameLayout implements GestureDetector.OnGestureListener {
    private Scroller a;
    private LayoutInflater b;
    private GestureDetector c = new GestureDetector(this);
    private Rect[] d;
    private final int e = aq.a();
    private final int f = aq.b();
    private final int g = aq.a(112);
    private final int h = (aq.b() - this.g);
    private int i;
    private int j;
    private float[] k;
    private float l;

    public XCardPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public XCardPager(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a();
    }

    private void a() {
        this.a = new Scroller(getContext());
        this.b = LayoutInflater.from(getContext());
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.c.onTouchEvent(motionEvent)) {
            for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
                MotionEvent obtain = MotionEvent.obtain(motionEvent);
                obtain.setAction(3);
                obtain.offsetLocation((float) (-this.d[childCount].left), (float) (-this.d[childCount].top));
                getChildAt(childCount).dispatchTouchEvent(obtain);
            }
        } else if (motionEvent.getAction() == 2) {
            if (!this.a.isFinished()) {
                this.a.forceFinished(true);
            }
            float y = motionEvent.getY() - this.l;
            float[] fArr = this.k;
            int i2 = this.i;
            fArr[i2] = y + fArr[i2];
            getChildAt(this.i).setTranslationY(this.k[this.i]);
            int i3 = (int) this.k[this.i];
            this.d[this.i].set(getPaddingLeft(), getPaddingTop() + i3, (getWidth() + getPaddingLeft()) - getPaddingRight(), i3 + this.f);
        } else if (motionEvent.getAction() == 0) {
            if (!this.a.isFinished()) {
                this.a.forceFinished(true);
            }
            this.l = motionEvent.getY();
            for (int childCount2 = getChildCount() - 1; childCount2 >= 0; childCount2--) {
                if (this.d[childCount2].contains((int) motionEvent.getX(), (int) this.l)) {
                    this.i = childCount2;
                    MotionEvent obtain2 = MotionEvent.obtain(motionEvent);
                    obtain2.offsetLocation((float) (-this.d[childCount2].left), (float) (-this.d[childCount2].top));
                    if (getChildAt(childCount2).dispatchTouchEvent(obtain2)) {
                        break;
                    }
                }
            }
        }
        return true;
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        this.a.fling(0, this.j, 0, (int) (-f3), 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        postInvalidate();
        return true;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        this.j = (int) Math.max(0.0f, Math.min(((float) this.j) + f3, (float) ((getChildCount() - 1) * ((getWidth() - getPaddingLeft()) - getPaddingRight()))));
        postInvalidate();
        return true;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }
}
