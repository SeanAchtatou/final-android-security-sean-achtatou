package com.fastnet.browseralam.view;

import android.os.Build;
import android.view.ViewTreeObserver;

final class a implements ViewTreeObserver.OnGlobalLayoutListener {
    final /* synthetic */ AnimatedProgressBar a;

    a(AnimatedProgressBar animatedProgressBar) {
        this.a = animatedProgressBar;
    }

    public final void onGlobalLayout() {
        if (Build.VERSION.SDK_INT < 16) {
            this.a.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        } else {
            this.a.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
        this.a.f.bottom = this.a.getBottom() - this.a.getTop();
        int unused = this.a.g = this.a.getMeasuredWidth();
    }
}
