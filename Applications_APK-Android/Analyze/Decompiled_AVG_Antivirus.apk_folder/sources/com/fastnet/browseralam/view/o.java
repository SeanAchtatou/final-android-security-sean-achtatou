package com.fastnet.browseralam.view;

import android.view.MotionEvent;
import android.view.View;

final class o implements View.OnTouchListener {
    final /* synthetic */ float a;
    final /* synthetic */ DrawerFrame b;

    o(DrawerFrame drawerFrame, float f) {
        this.b = drawerFrame;
        this.a = f;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        this.b.setVisibility(0);
        boolean unused = this.b.v = true;
        boolean unused2 = this.b.t = false;
        this.b.d = this.a - ((float) this.b.y);
        this.b.m.animate().translationY(this.b.d).setDuration(100).setListener(null).start();
        this.b.s.postDelayed(this.b.L, 100);
        if (this.b.J) {
            this.b.q.setVisibility(8);
            this.b.q.setOnTouchListener(this.b.K);
        } else {
            this.b.q.setOnTouchListener(this.b.I);
        }
        return false;
    }
}
