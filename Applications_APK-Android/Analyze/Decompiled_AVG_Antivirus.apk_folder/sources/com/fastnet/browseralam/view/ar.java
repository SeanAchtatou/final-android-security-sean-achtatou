package com.fastnet.browseralam.view;

import android.webkit.WebView;
import com.fastnet.browseralam.R;

final class ar extends av {
    final /* synthetic */ XWebView a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private ar(XWebView xWebView) {
        super(xWebView, (byte) 0);
        this.a = xWebView;
    }

    /* synthetic */ ar(XWebView xWebView, byte b) {
        this(xWebView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(java.lang.String, boolean):void */
    public final void onPageFinished(WebView webView, String str) {
        if (webView.isShown()) {
            this.a.c.a(str, true);
        }
        if (webView.getTitle() == null || webView.getTitle().isEmpty()) {
            String unused = this.a.g = this.a.b.getString(R.string.untitled);
        } else {
            String unused2 = this.a.g = webView.getTitle();
        }
        this.a.a.loadUrl("javascript:(function(){N=document.createElement('link');S='*{background:#151515 !important;color:grey !important}:link,:link *{color:#ddddff !important}:visited,:visited *{color:#ddffdd !important}';N.rel='stylesheet';N.href='data:text/css,'+escape(S);document.getElementsByTagName('head')[0].appendChild(N);})()");
    }
}
