package com.fastnet.browseralam.view;

import android.content.DialogInterface;
import android.util.Log;
import android.webkit.HttpAuthHandler;
import android.widget.EditText;

final class ax implements DialogInterface.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ EditText b;
    final /* synthetic */ HttpAuthHandler c;
    final /* synthetic */ av d;

    ax(av avVar, EditText editText, EditText editText2, HttpAuthHandler httpAuthHandler) {
        this.d = avVar;
        this.a = editText;
        this.b = editText2;
        this.c = httpAuthHandler;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.c.proceed(this.a.getText().toString().trim(), this.b.getText().toString().trim());
        Log.d("Lightning", "Request Login");
    }
}
