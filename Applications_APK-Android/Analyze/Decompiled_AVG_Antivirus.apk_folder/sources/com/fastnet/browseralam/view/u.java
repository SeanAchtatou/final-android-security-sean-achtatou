package com.fastnet.browseralam.view;

import android.animation.ValueAnimator;

final class u implements ValueAnimator.AnimatorUpdateListener {
    final /* synthetic */ int a;
    final /* synthetic */ WebViewFrame b;

    u(WebViewFrame webViewFrame, int i) {
        this.b = webViewFrame;
        this.a = i;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.b.c.scrollTo(0, this.a - ((Integer) valueAnimator.getAnimatedValue()).intValue());
    }
}
