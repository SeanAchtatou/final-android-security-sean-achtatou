package com.fastnet.browseralam.view;

import android.view.MotionEvent;
import android.view.View;
import com.fastnet.browseralam.i.aq;

final class y implements View.OnTouchListener {
    final /* synthetic */ XWebView a;
    private boolean b;
    private boolean c;
    private float d;
    private float e;
    private int f = aq.a(10);

    y(XWebView xWebView) {
        this.a = xWebView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (!this.a.a.hasFocus()) {
                    this.a.a.requestFocus();
                }
                this.c = this.a.a.canScrollVertically(-1);
                this.d = motionEvent.getX();
                this.e = motionEvent.getY();
                this.b = false;
                this.a.O.removeMessages(1);
                this.a.O.a();
                break;
            case 2:
                if (this.b) {
                    if (!this.c && motionEvent.getY() > this.e && Math.abs(this.d - motionEvent.getX()) < ((float) this.f)) {
                        this.a.R.a(motionEvent);
                        this.a.a.setOnTouchListener(this.a.Q);
                        break;
                    }
                } else if (Math.abs(this.d - motionEvent.getX()) > ((float) this.f) || Math.abs(this.e - motionEvent.getY()) > ((float) this.f)) {
                    this.b = true;
                }
            case 1:
            case 3:
                this.a.O.removeMessages(1);
                break;
        }
        return false;
    }
}
