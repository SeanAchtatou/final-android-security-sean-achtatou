package com.fastnet.browseralam.view;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class k extends GestureDetector.SimpleOnGestureListener {
    final /* synthetic */ CardFrame a;

    k(CardFrame cardFrame) {
        this.a = cardFrame;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (!this.a.h && f > ((float) (-this.a.s))) {
            return false;
        }
        if (this.a.h && f < ((float) this.a.s)) {
            return false;
        }
        boolean unused = this.a.r = true;
        if (!this.a.h) {
            this.a.a(true);
        } else {
            this.a.b(true);
        }
        return true;
    }
}
