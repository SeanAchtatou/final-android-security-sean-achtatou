package com.fastnet.browseralam.view;

import android.graphics.Bitmap;
import com.fastnet.browseralam.b.d;
import com.fastnet.browseralam.i.aq;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

final class be implements Runnable {
    final /* synthetic */ bd a;

    be(bd bdVar) {
        this.a = bdVar;
    }

    public final void run() {
        try {
            int unused = this.a.a.J = aq.a(this.a.a.h).hashCode();
            String str = this.a.a.J + ".png";
            if (this.a.a.K != null) {
                if (this.a.a.L.equals(this.a.a.h)) {
                    this.a.a.K.a(this.a.a.i, str);
                } else {
                    d unused2 = this.a.a.K = null;
                }
            }
            File file = new File(this.a.a.N, str);
            if (!file.exists()) {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                this.a.a.i.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
