package com.fastnet.browseralam.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v4.view.q;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.b.e;
import com.fastnet.browseralam.f.h;
import com.fastnet.browseralam.i.aq;

public class WebViewFrame extends FrameLayout {
    private static final float k = ((float) aq.a(28));
    private static final float l = ((float) aq.a(28));
    /* access modifiers changed from: private */
    public float A = 0.0f;
    private float B = ((float) this.y);
    private ValueAnimator C = new ValueAnimator();
    private GestureDetector.SimpleOnGestureListener D = new v(this);
    private View.OnLongClickListener E = new w(this);
    /* access modifiers changed from: private */
    public int F;
    /* access modifiers changed from: private */
    public int G;
    /* access modifiers changed from: private */
    public BrowserActivity a;
    private XWebView b;
    /* access modifiers changed from: private */
    public WebView c;
    private h d;
    private RelativeLayout e;
    private q f;
    private VelocityTracker g;
    /* access modifiers changed from: private */
    public Scroller h;
    /* access modifiers changed from: private */
    public x i;
    private e j;
    private float m;
    /* access modifiers changed from: private */
    public float n;
    private float o;
    /* access modifiers changed from: private */
    public float p;
    private boolean q;
    /* access modifiers changed from: private */
    public boolean r;
    private boolean s;
    /* access modifiers changed from: private */
    public boolean t;
    private boolean u;
    private boolean v = false;
    private int w = aq.a(15);
    private int x = aq.a(5);
    /* access modifiers changed from: private */
    public int y = aq.a(56);
    private DecelerateInterpolator z = new DecelerateInterpolator(2.0f);

    public WebViewFrame(Context context) {
        super(context);
    }

    public WebViewFrame(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public WebViewFrame(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    /* access modifiers changed from: private */
    public boolean a() {
        if (this.A > ((float) this.y)) {
            this.A = (float) this.y;
            this.B = 0.0f;
            this.v = true;
        } else if (this.A < 0.0f) {
            this.A = 0.0f;
            this.B = (float) this.y;
            this.v = false;
        }
        this.e.setTranslationY(-this.A);
        this.c.setTranslationY(this.B);
        if ((this.A >= ((float) this.y) || this.p <= 0.0f) && (this.A <= 0.0f || this.p >= 0.0f)) {
            return false;
        }
        this.A += this.p;
        this.B -= this.p;
        return true;
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.A != 0.0f && this.A != ((float) this.y)) {
            if (this.A > k) {
                if (this.A < ((float) this.y)) {
                    this.e.animate().translationY((float) (-this.y)).setInterpolator(this.z).setDuration(200);
                    this.c.animate().translationY(0.0f).setInterpolator(this.z).setDuration(200);
                    if (!this.q && !this.r) {
                        int scrollY = this.c.getScrollY();
                        this.C.setIntValues(0, (int) this.B);
                        this.C.addUpdateListener(new u(this, scrollY));
                        this.C.start();
                    }
                    this.A = (float) this.y;
                    this.B = 0.0f;
                    this.v = true;
                }
            } else if (this.A > 0.0f) {
                this.e.animate().translationY(0.0f).setInterpolator(this.z).setDuration(200);
                this.c.animate().translationY((float) this.y).setInterpolator(this.z).setDuration(200);
                if (!this.q && !this.r) {
                    int scrollY2 = this.c.getScrollY();
                    this.C.setIntValues(0, this.y - ((int) this.B));
                    this.C.addUpdateListener(new t(this, scrollY2));
                    this.C.start();
                }
                this.A = 0.0f;
                this.B = (float) this.y;
                this.v = false;
            }
        }
    }

    public final void a(BrowserActivity browserActivity, RelativeLayout relativeLayout, h hVar) {
        this.a = browserActivity;
        this.e = relativeLayout;
        this.d = hVar;
        this.C.setDuration(200L);
        this.C.setInterpolator(this.z);
        this.f = new q(browserActivity, this.D);
        this.h = new Scroller(browserActivity);
        this.g = VelocityTracker.obtain();
        this.i = new x(this);
        this.j = browserActivity.J();
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, layoutParams);
        this.c = (WebView) view;
        this.b = this.a.D();
        this.c.setOnTouchListener(null);
        this.c.setTranslationY((float) this.y);
        this.e.setTranslationY(0.0f);
        this.c.setOnLongClickListener(this.E);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (!this.c.hasFocus()) {
                    this.c.requestFocus();
                }
                this.q = !this.c.canScrollVertically(-1);
                this.m = motionEvent.getX();
                this.n = motionEvent.getY();
                this.s = false;
                this.t = false;
                this.r = false;
                this.u = !this.q;
                this.j.removeMessages(1);
                this.j.a();
                this.h.forceFinished(true);
                this.h.setFinalY(0);
                this.g.addMovement(motionEvent);
                break;
            case 1:
                this.g.addMovement(motionEvent);
                if (!(this.A == ((float) this.y) || this.A == 0.0f)) {
                    this.g.computeCurrentVelocity(1000);
                    this.G = -((int) this.g.getYVelocity());
                    this.F = -((int) this.g.getXVelocity());
                    if (Math.abs(this.G) > 0) {
                        this.r = true;
                        this.n = (float) this.c.getScrollY();
                        this.h.fling(0, (int) this.n, this.F, this.G, 0, Integer.MAX_VALUE, 0, Integer.MAX_VALUE);
                        this.i.a();
                    }
                }
                if (!this.r) {
                    b();
                }
                this.j.removeMessages(1);
                break;
            case 2:
                if (!this.s) {
                    this.o = motionEvent.getY();
                    this.p = this.n - this.o;
                    if (a()) {
                        this.g.addMovement(motionEvent);
                    }
                    this.n = this.o;
                    if (!this.t && (Math.abs(this.m - motionEvent.getX()) > ((float) this.x) || Math.abs(this.p) > ((float) this.x))) {
                        this.t = true;
                        this.j.removeMessages(1);
                    }
                    if (!this.u) {
                        if (this.v) {
                            this.u = true;
                        }
                        if (Math.abs(this.m - motionEvent.getX()) <= ((float) this.w) && this.p <= 0.0f) {
                            if (this.p <= -3.0f) {
                                this.d.a(motionEvent);
                                this.c.setOnTouchListener(this.d.b());
                                requestDisallowInterceptTouchEvent(true);
                                this.j.removeMessages(1);
                                this.s = true;
                                break;
                            }
                        } else {
                            this.u = true;
                            break;
                        }
                    }
                }
                break;
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return super.onTouchEvent(motionEvent);
    }
}
