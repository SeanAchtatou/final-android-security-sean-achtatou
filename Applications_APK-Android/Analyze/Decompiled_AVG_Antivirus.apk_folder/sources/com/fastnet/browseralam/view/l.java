package com.fastnet.browseralam.view;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.bw;
import android.support.v7.widget.cc;
import android.view.View;

public final class l extends LinearLayoutManager {
    private final int[] a = new int[2];
    private int b = 100;
    private boolean c;

    private void a(bw bwVar, int i, int i2, int i3, int[] iArr) {
        try {
            View b2 = bwVar.b(i);
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) b2.getLayoutParams();
            int r = r() + t();
            int s = s() + u();
            int i4 = layoutParams.leftMargin + layoutParams.rightMargin;
            int i5 = layoutParams.topMargin + layoutParams.bottomMargin;
            b2.measure(a(i2, r + i4 + o(b2) + n(b2), layoutParams.width, e()), a(i3, s + i5 + l(b2) + m(b2), layoutParams.height, f()));
            iArr[0] = f(b2) + layoutParams.leftMargin + layoutParams.rightMargin;
            iArr[1] = layoutParams.topMargin + g(b2) + layoutParams.bottomMargin;
            bwVar.a(b2);
        } catch (IndexOutOfBoundsException e) {
        }
    }

    public final void a(int i) {
        if (!(this.a == null || g() == i)) {
            this.a[0] = 0;
            this.a[1] = 0;
        }
        super.a(i);
    }

    public final void a(bw bwVar, cc ccVar, int i, int i2) {
        int i3;
        int i4;
        int i5;
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        boolean z = mode == 1073741824;
        boolean z2 = mode2 == 1073741824;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        if (!z || !z2) {
            boolean z3 = g() == 1;
            if (this.a[0] == 0 && this.a[1] == 0) {
                if (z3) {
                    this.a[0] = size;
                    this.a[1] = this.b;
                } else {
                    this.a[0] = this.b;
                    this.a[1] = size2;
                }
            }
            int i6 = 0;
            int i7 = 0;
            bwVar.a();
            int e = ccVar.e();
            int v = v();
            int i8 = 0;
            while (true) {
                if (i8 >= v) {
                    i3 = i7;
                    i4 = i6;
                    break;
                }
                if (!z3) {
                    if (!this.c && i8 < e) {
                        a(bwVar, i8, makeMeasureSpec, i2, this.a);
                    }
                    i4 = i6 + this.a[0];
                    i5 = i8 == 0 ? this.a[1] : i7;
                    if (i4 >= size) {
                        i3 = i5;
                        break;
                    }
                } else {
                    if (!this.c && i8 < e) {
                        a(bwVar, i8, i, makeMeasureSpec, this.a);
                    }
                    i5 = this.a[1] + i7;
                    i4 = i8 == 0 ? this.a[0] : i6;
                    if (i5 >= size2) {
                        i3 = i5;
                        break;
                    }
                }
                i8++;
                i7 = i5;
                i6 = i4;
            }
            if ((!z3 || i3 >= size2) && (z3 || i4 >= size)) {
                super.a(bwVar, ccVar, i, i2);
            } else {
                f(z ? size : r() + t() + i4, z2 ? size2 : s() + u() + i3);
            }
        } else {
            super.a(bwVar, ccVar, i, i2);
        }
    }
}
