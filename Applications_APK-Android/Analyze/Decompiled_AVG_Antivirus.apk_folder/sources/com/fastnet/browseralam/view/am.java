package com.fastnet.browseralam.view;

import android.webkit.JavascriptInterface;

public final class am {
    final /* synthetic */ XWebView a;

    public am(XWebView xWebView) {
        this.a = xWebView;
    }

    @JavascriptInterface
    public final void moveCursor() {
        this.a.b.runOnUiThread(new an(this));
    }

    @JavascriptInterface
    public final void removeHistoryPage() {
        this.a.b.runOnUiThread(new ap(this));
    }

    @JavascriptInterface
    public final void returnLinkText(String str) {
        this.a.b.runOnUiThread(new aq(this, str));
    }

    @JavascriptInterface
    public final void showHistoryPage() {
        this.a.b.runOnUiThread(new ao(this));
    }
}
