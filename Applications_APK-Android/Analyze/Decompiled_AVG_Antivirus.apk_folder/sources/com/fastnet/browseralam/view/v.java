package com.fastnet.browseralam.view;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class v extends GestureDetector.SimpleOnGestureListener {
    final /* synthetic */ WebViewFrame a;

    v(WebViewFrame webViewFrame) {
        this.a = webViewFrame;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.view.WebViewFrame.a(com.fastnet.browseralam.view.WebViewFrame, boolean):boolean
     arg types: [com.fastnet.browseralam.view.WebViewFrame, int]
     candidates:
      com.fastnet.browseralam.view.WebViewFrame.a(com.fastnet.browseralam.view.WebViewFrame, float):float
      com.fastnet.browseralam.view.WebViewFrame.a(com.fastnet.browseralam.view.WebViewFrame, int):int
      com.fastnet.browseralam.view.WebViewFrame.a(com.fastnet.browseralam.view.WebViewFrame, boolean):boolean */
    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (!(this.a.A == ((float) this.a.y) || this.a.A == 0.0f)) {
            boolean unused = this.a.r = true;
            int unused2 = this.a.F = (int) f;
            int unused3 = this.a.G = (int) f2;
            float unused4 = this.a.n = (float) this.a.c.getScrollY();
            this.a.h.fling(0, (int) this.a.n, (int) f, (int) f2, 0, Integer.MAX_VALUE, 0, Integer.MAX_VALUE);
            this.a.i.sendEmptyMessage(1);
        }
        return false;
    }
}
