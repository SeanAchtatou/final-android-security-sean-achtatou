package com.fastnet.browseralam.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.a.b;
import com.fastnet.browseralam.b.d;
import com.fastnet.browseralam.b.e;
import com.fastnet.browseralam.b.g;
import com.fastnet.browseralam.b.j;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.b.l;
import com.fastnet.browseralam.c.h;
import com.fastnet.browseralam.d.u;
import com.fastnet.browseralam.h.a;
import com.fastnet.browseralam.i.af;
import com.fastnet.browseralam.i.aq;
import java.io.File;
import kotlin.jvm.internal.LongCompanionObject;

public final class XWebView {
    /* access modifiers changed from: private */
    public static final int F = Build.VERSION.SDK_INT;
    private static final float[] G = {-1.0f, 0.0f, 0.0f, 0.0f, 255.0f, 0.0f, -1.0f, 0.0f, 0.0f, 255.0f, 0.0f, 0.0f, -1.0f, 0.0f, 255.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f};
    /* access modifiers changed from: private */
    public static Handler I = k.a();
    private static String m;
    private static String n;
    /* access modifiers changed from: private */
    public static Bitmap o;
    /* access modifiers changed from: private */
    public static a p;
    private boolean A;
    private boolean B;
    private boolean C;
    private boolean D;
    /* access modifiers changed from: private */
    public boolean E;
    private j H;
    /* access modifiers changed from: private */
    public int J;
    /* access modifiers changed from: private */
    public d K;
    /* access modifiers changed from: private */
    public String L = "";
    /* access modifiers changed from: private */
    public bd M = new bd(this);
    /* access modifiers changed from: private */
    public File N = h.b();
    /* access modifiers changed from: private */
    public e O;
    private View.OnTouchListener P;
    /* access modifiers changed from: private */
    public View.OnTouchListener Q;
    /* access modifiers changed from: private */
    public com.fastnet.browseralam.f.h R;
    private View.OnTouchListener S = new y(this);
    private View.OnTouchListener T = new z(this);
    private ad U = new aa(this);
    private View.OnTouchListener V = new ab(this);
    /* access modifiers changed from: private */
    public WebView a;
    /* access modifiers changed from: private */
    public final Activity b;
    /* access modifiers changed from: private */
    public final g c;
    private com.fastnet.browseralam.b.h d;
    private final Bitmap e;
    /* access modifiers changed from: private */
    public av f = new av(this, (byte) 0);
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public Bitmap i;
    private Bitmap j;
    private BitmapDrawable k;
    private WebSettings l;
    /* access modifiers changed from: private */
    public final com.fastnet.browseralam.i.a q;
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public af s;
    private final Paint t = new Paint();
    /* access modifiers changed from: private */
    public int u;
    private boolean v;
    private boolean w;
    private boolean x;
    private boolean y;
    /* access modifiers changed from: private */
    public boolean z;

    public class SpeedChromeClient extends WebChromeClient {
        private boolean b = a.e();

        public SpeedChromeClient() {
            a.a();
        }

        public Bitmap getDefaultVideoPoster() {
            return XWebView.this.c.y();
        }

        public View getVideoLoadingProgressView() {
            return XWebView.this.c.z();
        }

        public boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
            XWebView.this.c.a(message);
            return true;
        }

        public void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
            AlertDialog.Builder builder = new AlertDialog.Builder(XWebView.this.b);
            builder.setTitle(XWebView.this.b.getString(R.string.location));
            builder.setMessage((str.length() > 50 ? ((Object) str.subSequence(0, 50)) + "..." : str) + XWebView.this.b.getString(R.string.message_location)).setCancelable(true).setPositiveButton(XWebView.this.b.getString(R.string.action_allow), new au(this, callback, str)).setNegativeButton(XWebView.this.b.getString(R.string.action_dont_allow), new at(this, callback, str));
            builder.create().show();
        }

        public void onHideCustomView() {
            XWebView.this.c.x();
            super.onHideCustomView();
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            if (!this.b) {
                return super.onJsAlert(webView, str, str2, jsResult);
            }
            jsResult.cancel();
            return true;
        }

        public void onProgressChanged(WebView webView, int i) {
            if (webView.isShown()) {
                XWebView.this.c.a(i);
            }
        }

        public void onReceivedIcon(WebView webView, Bitmap bitmap) {
            if (XWebView.this.i == XWebView.o) {
                Bitmap unused = XWebView.this.i = bitmap;
                XWebView.this.c.a(XWebView.this.u, XWebView.this.z, 3);
                l.a(XWebView.this.M.a());
            }
        }

        public void onReceivedTitle(WebView webView, String str) {
            if (!str.isEmpty()) {
                String unused = XWebView.this.g = str;
            } else {
                String unused2 = XWebView.this.g = XWebView.this.b.getString(R.string.untitled);
            }
            XWebView.this.c.a(XWebView.this.u, XWebView.this.z, 2);
            if (!XWebView.this.z) {
                XWebView.this.c.a(str, webView.getUrl());
            }
        }

        @Deprecated
        public void onShowCustomView(View view, int i, WebChromeClient.CustomViewCallback customViewCallback) {
            XWebView.this.c.a(view, customViewCallback);
            super.onShowCustomView(view, i, customViewCallback);
        }

        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
            Activity B = XWebView.this.c.B();
            g a2 = XWebView.this.c;
            B.getRequestedOrientation();
            a2.a(view, customViewCallback);
            super.onShowCustomView(view, customViewCallback);
        }

        public boolean onShowFileChooser(WebView webView, ValueCallback valueCallback, WebChromeClient.FileChooserParams fileChooserParams) {
            XWebView.this.c.b(valueCallback);
            return true;
        }

        public void openFileChooser(ValueCallback valueCallback) {
            XWebView.this.c.a(valueCallback);
        }

        public void openFileChooser(ValueCallback valueCallback, String str) {
            XWebView.this.c.a(valueCallback);
        }

        public void openFileChooser(ValueCallback valueCallback, String str, String str2) {
            XWebView.this.c.a(valueCallback);
        }
    }

    public XWebView(Activity activity, String str, boolean z2, boolean z3) {
        this.b = activity;
        this.c = (g) activity;
        p = a.a();
        boolean ai = a.ai();
        this.e = aq.a(activity.getResources(), ai);
        this.i = this.e;
        this.g = this.b.getString(R.string.action_new_tab);
        this.h = str == null ? "" : str;
        this.q = com.fastnet.browseralam.i.a.a(activity.getApplicationContext());
        this.z = z2;
        this.A = z3;
        boolean b2 = this.c.C().b();
        if (b2) {
            this.d = this.c.C();
        }
        o = aq.a(activity.getResources(), ai);
        this.x = a.q();
        this.B = a.V();
        this.a = new WebView(activity);
        this.s = new af(this.c);
        this.a.setDrawingCacheBackgroundColor(0);
        this.a.setFocusableInTouchMode(true);
        this.a.setFocusable(true);
        this.a.setAnimationCacheEnabled(false);
        this.a.setDrawingCacheEnabled(false);
        this.a.setWillNotCacheDrawing(true);
        this.a.setAlwaysDrawnWithCacheEnabled(false);
        this.a.setBackgroundColor(activity.getResources().getColor(17170443));
        this.a.setScrollbarFadingEnabled(true);
        this.a.setScrollBarSize(aq.a(2));
        this.a.setSaveEnabled(true);
        this.a.setDownloadListener(new u(activity));
        this.O = this.c.J();
        n = this.a.getSettings().getUserAgentString();
        this.l = this.a.getSettings();
        WebSettings settings = this.a.getSettings();
        if (F < 18) {
            settings.setAppCacheMaxSize(LongCompanionObject.MAX_VALUE);
        }
        if (F < 17) {
            settings.setEnableSmoothTransition(true);
        }
        if (F > 16) {
            settings.setMediaPlaybackRequiresUserGesture(true);
        }
        if (F >= 21 && !this.z) {
            settings.setMixedContentMode(2);
        } else if (F >= 21) {
            settings.setMixedContentMode(1);
        }
        settings.setDomStorageEnabled(true);
        settings.setAppCacheEnabled(true);
        settings.setCacheMode(-1);
        settings.setDatabaseEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccess(true);
        settings.setDefaultTextEncodingName("utf-8");
        if (F > 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        settings.setAppCachePath(activity.getDir("appcache", 0).getPath());
        settings.setGeolocationDatabasePath(activity.getDir("geolocation", 0).getPath());
        if (F < 19) {
            settings.setDatabasePath(activity.getDir("databases", 0).getPath());
        }
        b();
        if (!b2 || str == null) {
            T();
            this.a.setWebChromeClient(new SpeedChromeClient());
        } else {
            this.a.setWebViewClient(new as(this, (byte) 0));
            this.a.setWebChromeClient(new h(this, activity, this.M));
        }
        if (str != null) {
            if (!str.trim().isEmpty()) {
                this.a.loadUrl(str);
            }
            if (b2) {
                this.d.b(this);
            }
        } else {
            this.a.loadUrl(b.a());
            if (b2) {
                this.d.a(this);
            }
        }
        this.a.addJavascriptInterface(new am(this), "Android");
    }

    private void S() {
        this.a.setLayerType(2, this.t);
    }

    /* access modifiers changed from: private */
    public void T() {
        if (a.x()) {
            c("javascript:(function(){N=document.createElement('link');S='*{background:#151515 !important;color:grey !important}:link,:link *{color:#ddddff !important}:visited,:visited *{color:#ddffdd !important}';N.rel='stylesheet';N.href='data:text/css,'+escape(S);document.getElementsByTagName('head')[0].appendChild(N);})()");
            this.a.setWebViewClient(new ar(this, (byte) 0));
        } else if (this.B) {
            this.a.setWebViewClient(new ae(this, (byte) 0));
        } else {
            this.a.setWebViewClient(this.f);
        }
    }

    private void U() {
        this.R.a(this.U);
    }

    public final Bitmap A() {
        return this.j;
    }

    public final BitmapDrawable B() {
        return this.k;
    }

    public final Bitmap C() {
        return this.i;
    }

    public final boolean D() {
        return this.z;
    }

    public final boolean E() {
        return this.A;
    }

    public final boolean F() {
        String url;
        return (this.a == null || (url = this.a.getUrl()) == null || !url.contains("history.html")) ? false : true;
    }

    public final boolean G() {
        return this.l.getUserAgentString().equals("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36");
    }

    public final boolean H() {
        return this.a.canGoBack();
    }

    public final boolean I() {
        return this.C;
    }

    public final boolean J() {
        return this.B;
    }

    public final boolean K() {
        return this.D;
    }

    public final boolean L() {
        return this.a.getProgress() == 100 || this.i != o;
    }

    public final String M() {
        return this.J + ".png";
    }

    public final void N() {
        if (this.x) {
            this.a.setOnTouchListener(this.V);
        } else {
            this.a.setOnTouchListener(this.P);
        }
    }

    public final XWebView a() {
        this.a.setWebViewClient(new ac(this, (byte) 0));
        return this;
    }

    public final void a(int i2) {
        this.u = i2;
    }

    public final void a(Bitmap bitmap) {
        this.j = bitmap;
    }

    public final void a(BitmapDrawable bitmapDrawable) {
        this.k = bitmapDrawable;
    }

    public final void a(d dVar) {
        this.K = dVar;
        this.L = this.h;
    }

    public final void a(j jVar) {
        this.H = jVar;
    }

    public final void a(com.fastnet.browseralam.f.h hVar) {
        this.R = hVar;
        this.Q = hVar.b();
    }

    public final synchronized void a(String str) {
        if (F > 16) {
            this.a.findAllAsync(str);
        } else {
            this.a.findAll(str);
        }
    }

    public final void a(boolean z2) {
        this.y = z2;
        this.c.a(this.u, this.z, 1);
        if (z2) {
            U();
        }
    }

    public final synchronized void b() {
        boolean z2 = true;
        synchronized (this) {
            m = a.u();
            this.q.a(this.b);
            if (this.l == null && this.a != null) {
                this.l = this.a.getSettings();
            } else if (this.l == null) {
            }
            d(a.R());
            if (!this.z) {
                this.l.setGeolocationEnabled(a.F());
            } else {
                this.l.setGeolocationEnabled(false);
            }
            this.l.setPluginState(a.m());
            this.l.setUserAgentString(a.b(n));
            if (!a.aa() || this.z) {
                if (F < 18) {
                    this.l.setSavePassword(false);
                }
                this.l.setSaveFormData(false);
            } else {
                if (F < 18) {
                    this.l.setSavePassword(true);
                }
                this.l.setSaveFormData(true);
            }
            if (a.y()) {
                this.l.setJavaScriptEnabled(true);
                this.l.setJavaScriptCanOpenWindowsAutomatically(true);
            }
            if (a.af()) {
                this.E = true;
                this.l.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
                if (F >= 19) {
                    try {
                        this.l.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
                    } catch (Exception e2) {
                        Log.e("Lightning", "Problem setting LayoutAlgorithm to TEXT_AUTOSIZING");
                        aq.a(this.b, "failed to enable text reflow");
                    }
                }
            } else {
                this.E = false;
                this.l.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
            }
            this.l.setBlockNetworkImage(a.d());
            this.l.setSupportMultipleWindows(!a.I());
            this.l.setUseWideViewPort(a.al());
            this.l.setLoadWithOverviewMode(a.H());
            this.l.setTextZoom(a.ag());
            if (F >= 21) {
                CookieManager instance = CookieManager.getInstance();
                WebView webView = this.a;
                if (a.f()) {
                    z2 = false;
                }
                instance.setAcceptThirdPartyCookies(webView, z2);
            }
            this.x = a.q();
            if (!a.z()) {
                this.P = this.T;
            } else {
                this.P = this.S;
            }
            this.a.setOnTouchListener(this.P);
            boolean V2 = a.V();
            if (V2 != this.B) {
                this.B = V2;
                if (this.B) {
                    this.a.setWebViewClient(new ae(this, (byte) 0));
                } else {
                    this.a.setWebViewClient(this.f);
                }
            }
        }
    }

    public final void b(int i2) {
        this.y = true;
        this.u = i2;
        U();
    }

    public final void b(Bitmap bitmap) {
        this.i = bitmap;
    }

    public final void b(String str) {
        this.g = str;
    }

    public final void b(boolean z2) {
        this.C = z2;
        if (z2) {
            this.a.setWebViewClient(new ah(this));
        } else {
            this.a.setWebViewClient(this.f);
        }
    }

    public final void c(int i2) {
        this.a.setVisibility(i2);
    }

    public final synchronized void c(String str) {
        WebView webView = this.a;
        this.h = str;
        webView.loadUrl(str);
    }

    public final void c(boolean z2) {
        this.B = z2;
        if (z2) {
            com.fastnet.browseralam.d.b.a(this.b, this);
            this.a.setWebViewClient(new ae(this, (byte) 0));
            return;
        }
        com.fastnet.browseralam.d.b.a();
        this.a.setWebViewClient(this.f);
    }

    public final boolean c() {
        return this.a != null && this.a.isShown();
    }

    public final synchronized void d() {
        if (this.a != null && !this.D) {
            this.a.onPause();
        }
    }

    public final void d(int i2) {
        switch (i2) {
            case 0:
                this.t.setColorFilter(null);
                if (!a.X()) {
                    this.a.setLayerType(0, null);
                    break;
                } else {
                    this.a.setLayerType(2, null);
                    break;
                }
            case 1:
                this.t.setColorFilter(new ColorMatrixColorFilter(G));
                S();
                break;
            case 2:
                ColorMatrix colorMatrix = new ColorMatrix();
                colorMatrix.setSaturation(0.0f);
                this.t.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
                S();
                break;
            case 3:
                ColorMatrix colorMatrix2 = new ColorMatrix();
                colorMatrix2.set(G);
                ColorMatrix colorMatrix3 = new ColorMatrix();
                colorMatrix3.setSaturation(0.0f);
                ColorMatrix colorMatrix4 = new ColorMatrix();
                colorMatrix4.setConcat(colorMatrix2, colorMatrix3);
                this.t.setColorFilter(new ColorMatrixColorFilter(colorMatrix4));
                S();
                break;
            case 4:
                this.w = true;
                this.a.loadUrl("javascript:(function(){N=document.createElement('link');S='*{background:#151515 !important;color:grey !important}:link,:link *{color:#ddddff !important}:visited,:visited *{color:#ddffdd !important}';N.rel='stylesheet';N.href='data:text/css,'+escape(S);document.getElementsByTagName('head')[0].appendChild(N);})()");
                this.a.setWebViewClient(new ar(this, (byte) 0));
                break;
        }
        if (i2 != 4 && this.w) {
            this.w = false;
            T();
        }
    }

    public final void d(String str) {
        this.a.loadUrl("javascript:(function(){var els = document.getElementsByTagName(\"a\");for (var i = 0, l = els.length; i < l; i++) {    var el = els[i];    if (el.href === '" + str + "') {        Android.returnLinkText(el.innerHTML);    }}})()");
    }

    public final void d(boolean z2) {
        this.D = z2;
    }

    public final synchronized void e() {
        if (this.a != null) {
            this.a.onResume();
        }
    }

    public final void e(int i2) {
        if (i2 != 1) {
            this.l.setUserAgentString("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36");
        } else if (F > 16) {
            this.l.setUserAgentString(WebSettings.getDefaultUserAgent(this.b));
        } else {
            this.l.setUserAgentString(n);
        }
    }

    public final void f() {
        this.v = true;
    }

    public final boolean g() {
        return this.v;
    }

    public final int h() {
        return this.u;
    }

    public final boolean i() {
        return this.y;
    }

    public final int j() {
        return this.a.getProgress();
    }

    public final void k() {
        this.a.requestFocus();
    }

    public final void l() {
        this.a.clearCache(true);
    }

    public final synchronized void m() {
        this.a.reload();
    }

    public final synchronized void n() {
        this.a.stopLoading();
    }

    public final synchronized void o() {
        if (this.a != null) {
            this.a.pauseTimers();
        }
    }

    public final synchronized void p() {
        if (this.a != null) {
            this.a.resumeTimers();
        }
    }

    public final synchronized void q() {
        this.a.findNext(false);
    }

    public final synchronized void r() {
        this.a.findNext(true);
    }

    public final synchronized void s() {
        this.a.stopLoading();
        this.a.onPause();
        this.a.clearHistory();
        this.a.setVisibility(8);
        this.a.removeAllViews();
        this.a.destroyDrawingCache();
        this.a.destroy();
        this.a = null;
        if (this.H != null) {
            this.H.a();
        }
    }

    public final synchronized boolean t() {
        boolean z2;
        if (this.a.canGoBack()) {
            this.a.goBack();
            z2 = true;
        } else {
            z2 = false;
        }
        return z2;
    }

    public final synchronized void u() {
        if (this.a.canGoForward()) {
            this.a.goForward();
        }
    }

    public final String v() {
        return this.a.getSettings().getUserAgentString();
    }

    public final WebView w() {
        return this.a;
    }

    public final String x() {
        return this.g;
    }

    public final String y() {
        return this.a.getUrl();
    }

    public final String z() {
        return this.h;
    }
}
