package com.fastnet.browseralam.view;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.MailTo;
import android.support.v7.app.k;
import android.support.v7.app.l;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;
import java.net.URISyntaxException;

final class ah extends av {
    final /* synthetic */ XWebView a;
    /* access modifiers changed from: private */
    public k c;
    /* access modifiers changed from: private */
    public TextView d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ah(XWebView xWebView) {
        super(xWebView, (byte) 0);
        this.a = xWebView;
        l lVar = new l(xWebView.b);
        View inflate = xWebView.b.getLayoutInflater().inflate((int) R.layout.intercept_dialog, (ViewGroup) null);
        lVar.b(inflate);
        this.c = lVar.c();
        this.c.setCanceledOnTouchOutside(false);
        this.d = (TextView) inflate.findViewById(R.id.intercept_url);
        inflate.findViewById(R.id.allow).setOnClickListener(new ai(this, xWebView));
        inflate.findViewById(R.id.block).setOnClickListener(new aj(this, xWebView));
        inflate.findViewById(R.id.cancel).setOnClickListener(new ak(this, xWebView));
        this.c.setOnCancelListener(new al(this, xWebView));
    }

    static /* synthetic */ void a(ah ahVar, String str) {
        if (str.startsWith("mailto:")) {
            MailTo parse = MailTo.parse(str);
            Activity unused = ahVar.a.b;
            ahVar.a.b.startActivity(aq.a(parse.getTo(), parse.getSubject(), parse.getBody(), parse.getCc()));
        } else if (str.startsWith("intent://")) {
            try {
                Intent parseUri = Intent.parseUri(str, 1);
                if (parseUri != null) {
                    try {
                        ahVar.a.b.startActivity(parseUri);
                        return;
                    } catch (ActivityNotFoundException e) {
                        return;
                    }
                }
            } catch (URISyntaxException e2) {
                return;
            }
        }
        if (!ahVar.a.s.a(ahVar.a.a, str)) {
            ahVar.a.a.loadUrl(str);
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (!this.a.q.c(str)) {
            webView.pauseTimers();
            this.d.setText(str);
            this.c.show();
        }
        return true;
    }
}
