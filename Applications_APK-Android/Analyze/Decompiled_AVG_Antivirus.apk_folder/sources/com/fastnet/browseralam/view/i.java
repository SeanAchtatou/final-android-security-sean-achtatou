package com.fastnet.browseralam.view;

import android.graphics.Picture;
import android.webkit.WebView;

final class i implements WebView.PictureListener {
    final /* synthetic */ h a;

    i(h hVar) {
        this.a = hVar;
    }

    public final void onNewPicture(WebView webView, Picture picture) {
        if (this.a.i && picture != null) {
            this.a.d.removeCallbacks(this.a.k);
            this.a.d.postDelayed(this.a.k, 200);
        }
    }
}
