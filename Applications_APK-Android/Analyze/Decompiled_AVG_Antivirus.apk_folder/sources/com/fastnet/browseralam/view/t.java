package com.fastnet.browseralam.view;

import android.animation.ValueAnimator;

final class t implements ValueAnimator.AnimatorUpdateListener {
    final /* synthetic */ int a;
    final /* synthetic */ WebViewFrame b;

    t(WebViewFrame webViewFrame, int i) {
        this.b = webViewFrame;
        this.a = i;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.b.c.scrollTo(0, ((Integer) valueAnimator.getAnimatedValue()).intValue() + this.a);
    }
}
