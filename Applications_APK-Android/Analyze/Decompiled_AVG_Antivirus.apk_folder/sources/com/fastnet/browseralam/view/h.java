package com.fastnet.browseralam.view;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.b.l;
import java.lang.ref.WeakReference;

public class h extends WebChromeClient {
    /* access modifiers changed from: private */
    public WeakReference a;
    private BrowserActivity b;
    /* access modifiers changed from: private */
    public com.fastnet.browseralam.b.h c;
    /* access modifiers changed from: private */
    public Handler d = k.a();
    private bd e;
    private Bitmap f;
    private Bitmap g;
    private boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    private int j = 70;
    /* access modifiers changed from: private */
    public Runnable k = new j(this);

    public h() {
    }

    public h(XWebView xWebView, Activity activity, bd bdVar) {
        this.a = new WeakReference(xWebView);
        this.b = (BrowserActivity) activity;
        this.e = bdVar;
        this.h = xWebView.D();
        this.f = this.b.t();
        this.c = this.b.C();
        String z = xWebView.z();
        if (z != null && !z.isEmpty()) {
            xWebView.w().setPictureListener(new i(this));
        }
    }

    public boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
        this.b.a(message);
        return true;
    }

    public void onHideCustomView() {
        this.b.x();
        super.onHideCustomView();
    }

    public void onProgressChanged(WebView webView, int i2) {
        if (i2 > this.j) {
            if (i2 == 100) {
                this.i = true;
            }
            if (i2 - this.j > 10 && webView.getContentHeight() > 0) {
                this.c.b((XWebView) this.a.get());
                this.j = i2;
            }
            if (webView.isShown()) {
                this.b.a(i2);
            }
        }
    }

    public void onReceivedIcon(WebView webView, Bitmap bitmap) {
        if (this.g == this.f) {
            this.g = bitmap;
            ((XWebView) this.a.get()).b(bitmap);
            this.b.a(((XWebView) this.a.get()).h(), this.h, 3);
            l.a(this.e.a());
        }
    }

    public void onReceivedTitle(WebView webView, String str) {
        if (!str.isEmpty()) {
            ((XWebView) this.a.get()).b(str);
        } else {
            ((XWebView) this.a.get()).b(this.b.getString(R.string.untitled));
        }
        this.b.a(((XWebView) this.a.get()).h(), this.h, 2);
        if (!this.h) {
            this.b.a(str, webView.getUrl());
        }
    }

    @Deprecated
    public void onShowCustomView(View view, int i2, WebChromeClient.CustomViewCallback customViewCallback) {
        this.b.a(view, customViewCallback);
        super.onShowCustomView(view, i2, customViewCallback);
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        Activity B = this.b.B();
        BrowserActivity browserActivity = this.b;
        B.getRequestedOrientation();
        browserActivity.a(view, customViewCallback);
        super.onShowCustomView(view, customViewCallback);
    }
}
