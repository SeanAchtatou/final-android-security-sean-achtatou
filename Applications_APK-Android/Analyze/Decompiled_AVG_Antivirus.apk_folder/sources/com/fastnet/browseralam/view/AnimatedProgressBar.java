package com.fastnet.browseralam.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.b;

public class AnimatedProgressBar extends LinearLayout {
    /* access modifiers changed from: private */
    public int a = 0;
    private boolean b = true;
    /* access modifiers changed from: private */
    public int c = 0;
    private int d;
    private final Paint e = new Paint();
    /* access modifiers changed from: private */
    public final Rect f = new Rect();
    /* access modifiers changed from: private */
    public int g;
    private DecelerateInterpolator h = new DecelerateInterpolator();

    public AnimatedProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public AnimatedProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, attributeSet);
    }

    /* access modifiers changed from: private */
    public void a() {
        animate().alpha(0.0f).setDuration(200).setInterpolator(this.h).start();
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.fastnet.browseralam.view.AnimatedProgressBar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, b.h, 0, 0);
        try {
            int color = obtainStyledAttributes.getColor(1, 4342338);
            this.d = obtainStyledAttributes.getColor(0, 2201331);
            this.b = obtainStyledAttributes.getBoolean(2, false);
            obtainStyledAttributes.recycle();
            ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.animated_progress_bar, (ViewGroup) this, true);
            setBackgroundColor(color);
            this.e.setColor(this.d);
            this.e.setStrokeWidth(10.0f);
            getViewTreeObserver().addOnGlobalLayoutListener(new a(this));
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    public final void a(int i) {
        if (i > 100) {
            i = 100;
        } else if (i < 0) {
            i = 0;
        }
        if (getAlpha() < 1.0f) {
            animate().alpha(1.0f).setDuration(200).setInterpolator(this.h).start();
        }
        this.f.left = 0;
        this.f.top = 0;
        if (i < this.a && !this.b) {
            this.c = 0;
        } else if (i == this.a) {
            if (i == 100) {
                a();
                return;
            }
            return;
        }
        this.a = i;
        b bVar = new b(this, this.c, ((this.g * this.a) / 100) - this.c, this.g);
        bVar.setDuration(500);
        bVar.setInterpolator(this.h);
        startAnimation(bVar);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.f.right = this.f.left + this.c;
        canvas.drawRect(this.f, this.e);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.a = bundle.getInt("progressState");
            parcelable = bundle.getParcelable("instanceState");
        }
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt("progressState", this.a);
        return bundle;
    }
}
