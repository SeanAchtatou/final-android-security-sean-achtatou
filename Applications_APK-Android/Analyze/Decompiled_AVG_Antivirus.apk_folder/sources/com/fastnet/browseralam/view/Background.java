package com.fastnet.browseralam.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.fastnet.browseralam.i.aq;

public class Background extends View {
    private final int a;
    private final int b = aq.a(79);
    private final int c = aq.a(26);
    private final int d = aq.a(0);

    public Background(Context context) {
        super(context);
        this.a = context.getResources().getDisplayMetrics().heightPixels - this.c;
    }

    public Background(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context.getResources().getDisplayMetrics().heightPixels - this.c;
    }

    public Background(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = context.getResources().getDisplayMetrics().heightPixels - this.c;
    }

    public final float a() {
        return ((float) this.a) - getTranslationY();
    }
}
