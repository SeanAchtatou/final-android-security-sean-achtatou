package com.fastnet.browseralam.view;

import android.view.MotionEvent;
import android.view.View;
import com.fastnet.browseralam.b.l;

final class m implements View.OnTouchListener {
    final /* synthetic */ DrawerFrame a;

    m(DrawerFrame drawerFrame) {
        this.a = drawerFrame;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.a.x) {
            l.a(this.a.M);
        }
        this.a.setVisibility(0);
        boolean unused = this.a.v = true;
        boolean unused2 = this.a.t = false;
        this.a.d = this.a.m.getTranslationY() - ((float) this.a.y);
        this.a.m.animate().translationY(this.a.d).setDuration(100).start();
        this.a.s.postDelayed(this.a.L, 100);
        return false;
    }
}
