package com.fastnet.browseralam.view;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class b extends Animation {
    final /* synthetic */ int a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ AnimatedProgressBar d;

    b(AnimatedProgressBar animatedProgressBar, int i, int i2, int i3) {
        this.d = animatedProgressBar;
        this.a = i;
        this.b = i2;
        this.c = i3;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        int i = this.a + ((int) (((float) this.b) * f));
        if (i <= this.c) {
            int unused = this.d.c = i;
            this.d.invalidate();
        }
        if (((double) (1.0f - f)) < 5.0E-4d && this.d.a >= 100) {
            this.d.a();
        }
    }

    public final boolean willChangeBounds() {
        return false;
    }
}
