package com.fastnet.browseralam.view;

import android.graphics.Bitmap;
import android.os.Message;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public final class c extends h {
    public c(WebView webView, g gVar) {
        webView.setPictureListener(new d(this, gVar));
    }

    public final boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
        return false;
    }

    public final void onHideCustomView() {
        super.onHideCustomView();
    }

    public final void onProgressChanged(WebView webView, int i) {
    }

    public final void onReceivedIcon(WebView webView, Bitmap bitmap) {
    }

    public final void onReceivedTitle(WebView webView, String str) {
    }

    public final void onShowCustomView(View view, int i, WebChromeClient.CustomViewCallback customViewCallback) {
    }

    public final void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        super.onShowCustomView(view, customViewCallback);
    }
}
