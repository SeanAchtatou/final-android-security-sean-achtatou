package com.fastnet.browseralam.view;

import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.fastnet.browseralam.d.b;
import java.io.ByteArrayInputStream;

final class ae extends av {
    final /* synthetic */ XWebView a;
    private ByteArrayInputStream c;
    private WebResourceResponse d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private ae(XWebView xWebView) {
        super(xWebView, (byte) 0);
        this.a = xWebView;
        this.c = new ByteArrayInputStream("".getBytes());
        this.d = new WebResourceResponse("text/plain", "utf-8", this.c);
    }

    /* synthetic */ ae(XWebView xWebView, byte b) {
        this(xWebView);
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
        String uri = webResourceRequest.getUrl().toString();
        if (this.a.q.c(uri)) {
            return this.d;
        }
        if (uri.endsWith(".mp4")) {
            XWebView.I.postDelayed(new af(this, uri), 150);
        } else {
            b.a(uri);
        }
        return super.shouldInterceptRequest(webView, webResourceRequest);
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        if (this.a.q.c(str)) {
            return this.d;
        }
        if (str.endsWith(".mp4")) {
            XWebView.I.postDelayed(new ag(this, str), 150);
        } else {
            b.a(str);
        }
        return super.shouldInterceptRequest(webView, str);
    }
}
