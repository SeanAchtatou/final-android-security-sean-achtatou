package com.fastnet.browseralam.view;

import android.content.ClipData;
import android.content.ClipboardManager;

final class aq implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ am b;

    aq(am amVar, String str) {
        this.b = amVar;
        this.a = str;
    }

    public final void run() {
        ((ClipboardManager) this.b.a.b.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("label", this.a));
    }
}
