package com.fastnet.browseralam.c;

import android.graphics.Bitmap;
import com.fastnet.browseralam.R;

public final class g implements Comparable {
    private int a;
    private String b;
    private String c;
    private String d;
    private String e;
    private Bitmap f;
    private int g;
    private int h;
    private boolean i;

    public g() {
        this.a = 0;
        this.b = "";
        this.c = "";
        this.d = "";
        this.e = "";
        this.f = null;
        this.g = 0;
        this.h = 0;
    }

    public g(String str) {
        this.a = 0;
        this.b = "";
        this.c = "";
        this.d = "";
        this.e = "";
        this.f = null;
        this.g = 0;
        this.h = 0;
        this.b = null;
        this.c = str;
        this.f = null;
        this.g = R.drawable.ic_search;
    }

    public g(String str, byte b2) {
        this.a = 0;
        this.b = "";
        this.c = "";
        this.d = "";
        this.e = "";
        this.f = null;
        this.g = 0;
        this.h = 0;
        this.c = str;
        this.i = true;
    }

    public final int a() {
        return this.a;
    }

    public final void a(int i2) {
        this.a = i2;
    }

    public final void a(Bitmap bitmap) {
        this.f = bitmap;
    }

    public final void a(String str) {
        this.e = str;
    }

    public final void b() {
        this.g = R.drawable.ic_history;
    }

    public final void b(String str) {
        if (str == null) {
            str = "";
        }
        this.b = str;
    }

    public final String c() {
        return this.e;
    }

    public final void c(String str) {
        if (str == null) {
            str = "";
        }
        this.c = str;
    }

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return this.c.compareTo(((g) obj).c);
    }

    public final Bitmap d() {
        return this.f;
    }

    public final String e() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        if (this.a != gVar.a) {
            return false;
        }
        if (this.g != gVar.g) {
            return false;
        }
        if (this.f == null ? gVar.f != null : !this.f.equals(gVar.f)) {
            return false;
        }
        return this.c.equals(gVar.c) && this.b.equals(gVar.b);
    }

    public final String f() {
        return this.c;
    }

    public final boolean g() {
        return this.i;
    }

    public final int hashCode() {
        return (((this.f != null ? this.f.hashCode() : 0) + (((((this.a * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31)) * 31) + this.g;
    }

    public final String toString() {
        return this.c;
    }
}
