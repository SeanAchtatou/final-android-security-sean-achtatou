package com.fastnet.browseralam.c;

import android.support.v7.widget.bi;
import android.support.v7.widget.cf;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.fastnet.browseralam.R;

final class o extends bi {
    final int a = R.layout.history_item;
    final int b = R.layout.history_header;
    final w c = new w(this, (byte) 0);
    final r d = new r(this, (byte) 0);
    final /* synthetic */ h e;

    public o(h hVar) {
        this.e = hVar;
    }

    public final int a() {
        if (this.e.a != null) {
            return this.e.a.size();
        }
        return 0;
    }

    public final int a(int i) {
        if (i == 0) {
            return 2;
        }
        if (i == this.e.m) {
            return 3;
        }
        return ((g) this.e.a.get(i)).g() ? 1 : 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final cf a(ViewGroup viewGroup, int i) {
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        switch (i) {
            case 0:
                return new t(this, from.inflate(this.a, viewGroup, false));
            case 1:
                return new s(this, from.inflate(this.b, viewGroup, false));
            case 2:
                return new s(this, from.inflate((int) R.layout.history_title, viewGroup, false));
            default:
                return new v(this, from.inflate((int) R.layout.history_time, viewGroup, false));
        }
    }

    public final void a(cf cfVar, int i) {
        if (i != 0) {
            g gVar = (g) this.e.a.get(i);
            if (i == this.e.m) {
                v vVar = (v) cfVar;
                vVar.l.setOnClickListener(new p(this));
                vVar.n.setOnClickListener(new q(this));
            } else if (gVar.g()) {
                ((s) cfVar).l.setText(gVar.f());
            } else {
                t tVar = (t) cfVar;
                tVar.q = gVar;
                tVar.m.setText(gVar.f());
                tVar.p.setImageBitmap(gVar.d());
                tVar.n.setText(gVar.e());
                tVar.m.setTag(tVar);
                tVar.o.setTag(tVar);
            }
        }
    }
}
