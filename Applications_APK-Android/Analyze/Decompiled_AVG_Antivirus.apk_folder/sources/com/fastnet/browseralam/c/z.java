package com.fastnet.browseralam.c;

import android.graphics.Bitmap;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

final class z implements Runnable {
    final /* synthetic */ y a;

    z(y yVar) {
        this.a = yVar;
    }

    public final void run() {
        try {
            this.a.c.equals(this.a.d);
            File file = new File(h.q, this.a.c + ".png");
            if (!file.exists()) {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                this.a.b.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
                Bitmap unused = this.a.b = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
