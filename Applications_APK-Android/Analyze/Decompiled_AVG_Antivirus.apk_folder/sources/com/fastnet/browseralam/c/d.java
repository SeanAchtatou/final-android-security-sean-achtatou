package com.fastnet.browseralam.c;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import com.fastnet.browseralam.i.a;
import com.fastnet.browseralam.i.aq;
import com.google.android.gms.common.internal.ImagesContract;

final class d implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ c b;

    d(c cVar, String str) {
        this.b = cVar;
        this.a = str;
    }

    public final void run() {
        try {
            String a2 = aq.a(this.a);
            a.a(a2);
            Cursor rawQuery = c.c.rawQuery("SELECT * FROM adblock WHERE hash = " + a2.hashCode(), null);
            if (rawQuery.moveToFirst()) {
                rawQuery.close();
                return;
            }
            rawQuery.close();
            ContentValues contentValues = new ContentValues();
            contentValues.put(ImagesContract.URL, a2);
            contentValues.put("hash", Integer.valueOf(a2.hashCode()));
            c.c.insertOrThrow("adblock", null, contentValues);
        } catch (IllegalStateException e) {
            Log.e("Lightning", "IllegalStateException in updateHistory");
        } catch (NullPointerException e2) {
            Log.e("Lightning", "NullPointerException in updateHistory");
        } catch (SQLiteException e3) {
            Log.e("Lightning", "SQLiteException in updateHistory");
        }
    }
}
