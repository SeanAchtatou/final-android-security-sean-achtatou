package com.fastnet.browseralam.c;

import android.view.View;

final class j implements View.OnAttachStateChangeListener {
    final /* synthetic */ h a;

    j(h hVar) {
        this.a = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.c.h.b(com.fastnet.browseralam.c.h, boolean):boolean
     arg types: [com.fastnet.browseralam.c.h, int]
     candidates:
      com.fastnet.browseralam.c.h.b(com.fastnet.browseralam.c.h, int):int
      com.fastnet.browseralam.c.h.b(com.fastnet.browseralam.c.h, boolean):boolean */
    public final void onViewAttachedToWindow(View view) {
        boolean unused = this.a.k = false;
        this.a.s.removeCallbacks(this.a.v);
        if (this.a.c.size() == 0 && !this.a.l) {
            this.a.a(this.a.i.D());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.c.h.b(com.fastnet.browseralam.c.h, boolean):boolean
     arg types: [com.fastnet.browseralam.c.h, int]
     candidates:
      com.fastnet.browseralam.c.h.b(com.fastnet.browseralam.c.h, int):int
      com.fastnet.browseralam.c.h.b(com.fastnet.browseralam.c.h, boolean):boolean */
    public final void onViewDetachedFromWindow(View view) {
        boolean unused = this.a.k = true;
        this.a.s.postDelayed(this.a.v, 150);
    }
}
