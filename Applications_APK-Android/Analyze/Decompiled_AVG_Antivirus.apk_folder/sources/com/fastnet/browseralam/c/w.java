package com.fastnet.browseralam.c;

import android.view.MotionEvent;
import android.view.View;

final class w implements View.OnTouchListener {
    int a;
    t b;
    final /* synthetic */ o c;

    private w(o oVar) {
        this.c = oVar;
        this.a = 0;
    }

    /* synthetic */ w(o oVar, byte b2) {
        this(oVar);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            if (this.c.e.x != null) {
                this.c.e.x.m.setPaintFlags(this.c.e.y);
            }
            this.b = (t) view.getTag();
            int unused = this.c.e.y = this.c.e.x = this.b.m.getPaintFlags();
            this.b.m.setPaintFlags(this.c.e.y | 8);
            t tVar = this.b;
            tVar.r = false;
            tVar.t.e.s.postDelayed(tVar.s, 500);
            this.a = (int) motionEvent.getY();
        } else if (action == 2) {
            if (Math.abs(((float) this.a) - motionEvent.getY()) > 10.0f) {
                this.b.t();
                this.b.m.setPaintFlags(this.c.e.y);
                this.b.r = true;
            }
        } else if (action == 1) {
            this.b.t();
            this.b.m.setPaintFlags(this.c.e.y);
            if (!this.b.r) {
                this.c.e.j.a(this.b.n.getText().toString());
            }
        }
        return true;
    }
}
