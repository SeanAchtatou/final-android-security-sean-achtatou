package com.fastnet.browseralam.c;

import com.fastnet.browseralam.g.j;
import java.util.ArrayList;
import java.util.List;

public final class a extends b {
    private a a;
    private String b;
    private String c;
    private int d;
    private boolean e;
    private boolean f;
    private List g;
    private j h;

    public a(a aVar, String str) {
        this.b = "";
        this.c = "";
        this.g = new ArrayList();
        this.g = new ArrayList();
        this.a = aVar;
        this.g.add(this.a);
        this.c = str;
        this.h = aVar.h;
    }

    public a(List list, j jVar) {
        this.b = "";
        this.c = "";
        this.g = new ArrayList();
        this.e = true;
        this.g = list;
        this.h = jVar;
        this.f = true;
    }

    public final String a() {
        return this.c;
    }

    public final void a(int i) {
        this.d = i;
    }

    public final void a(a aVar) {
        aVar.a = this;
        if (this.g.size() > 0) {
            this.g.add(1, aVar);
            if (this.f) {
                this.h.c_(1);
                return;
            }
            return;
        }
        this.g.add(aVar);
        if (this.f) {
            this.h.c_(this.g.size() - 1);
        }
    }

    public final void a(b bVar) {
        bVar.c(this);
        if (!bVar.c() || this.g.size() <= 2) {
            this.g.add(bVar);
            if (this.f) {
                this.h.c_(this.g.size() - 1);
                return;
            }
            return;
        }
        this.g.add(1, bVar);
        if (this.f) {
            this.h.c_(1);
        }
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a(boolean z) {
        this.f = z;
    }

    public final String b() {
        return this.b;
    }

    public final void b(a aVar) {
        aVar.a = this;
        this.g.add(aVar);
        if (this.f) {
            this.h.c_(this.g.size() - 1);
        }
    }

    public final void b(b bVar) {
        this.g.add(bVar);
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void c(a aVar) {
        this.a = aVar;
        this.g.set(0, this.a);
    }

    public final void c(b bVar) {
        for (int i = 0; i < this.g.size(); i++) {
            if (this.g.get(i) == bVar) {
                this.g.remove(i);
                if (this.f) {
                    this.h.d_(i);
                    return;
                }
                return;
            }
        }
    }

    public final boolean c() {
        return true;
    }

    public final a d() {
        return this.a;
    }

    public final void d(b bVar) {
        for (int i = 0; i < this.g.size(); i++) {
            if (this.g.get(i) == bVar) {
                this.g.remove(i);
                return;
            }
        }
    }

    public final a e() {
        return this;
    }

    public final List f() {
        return this.g;
    }

    public final int g() {
        return this.d;
    }
}
