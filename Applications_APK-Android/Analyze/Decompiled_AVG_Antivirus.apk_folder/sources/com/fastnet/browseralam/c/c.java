package com.fastnet.browseralam.c;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import com.fastnet.browseralam.b.l;
import com.fastnet.browseralam.i.aq;
import com.google.android.gms.common.internal.ImagesContract;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class c extends SQLiteOpenHelper {
    private static String a = "";
    private static String b = "";
    /* access modifiers changed from: private */
    public static SQLiteDatabase c;
    private static c d;
    private static boolean e = false;

    private c(Context context) {
        super(context.getApplicationContext(), "speedHistoryManager", (SQLiteDatabase.CursorFactory) null, 7);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        c = writableDatabase;
        Cursor rawQuery = writableDatabase.rawQuery("SELECT * FROM datesHistory ORDER BY rowid DESC", new String[0]);
        rawQuery.moveToFirst();
        String string = rawQuery.getString(rawQuery.getColumnIndex("date"));
        if (rawQuery.moveToNext()) {
            b = rawQuery.getString(0);
        }
        if (b == null || b.isEmpty()) {
            b = "-1";
        }
        rawQuery.close();
        a = string;
        c();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0011, code lost:
        if ((com.fastnet.browseralam.c.c.c == null || !com.fastnet.browseralam.c.c.c.isOpen()) != false) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fastnet.browseralam.c.c a(android.content.Context r1) {
        /*
            com.fastnet.browseralam.c.c r0 = com.fastnet.browseralam.c.c.d
            if (r0 == 0) goto L_0x0013
            android.database.sqlite.SQLiteDatabase r0 = com.fastnet.browseralam.c.c.c
            if (r0 == 0) goto L_0x0010
            android.database.sqlite.SQLiteDatabase r0 = com.fastnet.browseralam.c.c.c
            boolean r0 = r0.isOpen()
            if (r0 != 0) goto L_0x001d
        L_0x0010:
            r0 = 1
        L_0x0011:
            if (r0 == 0) goto L_0x001a
        L_0x0013:
            com.fastnet.browseralam.c.c r0 = new com.fastnet.browseralam.c.c     // Catch:{ Exception -> 0x001f }
            r0.<init>(r1)     // Catch:{ Exception -> 0x001f }
            com.fastnet.browseralam.c.c.d = r0     // Catch:{ Exception -> 0x001f }
        L_0x001a:
            com.fastnet.browseralam.c.c r0 = com.fastnet.browseralam.c.c.d
            return r0
        L_0x001d:
            r0 = 0
            goto L_0x0011
        L_0x001f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fastnet.browseralam.c.c.a(android.content.Context):com.fastnet.browseralam.c.c");
    }

    public static List a(int i, String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(null);
        String str2 = "SELECT * FROM history ORDER BY id DESC LIMIT 100";
        if (i > 0) {
            str2 = "select * from (" + ("select * from history order by id desc limit 100" + i) + ") order by id desc limit " + i + ",100";
        }
        arrayList.add(new g("Today - " + new SimpleDateFormat("cccc d MMMM yyyy").format(new Date()), (byte) 0));
        Cursor rawQuery = c.rawQuery(str2, null);
        if (rawQuery.moveToFirst()) {
            do {
                if (rawQuery.getInt(4) == 1) {
                    arrayList.add(new g(rawQuery.getString(2), (byte) 0));
                } else {
                    g gVar = new g();
                    gVar.a(Integer.parseInt(rawQuery.getString(0)));
                    gVar.b(rawQuery.getString(1));
                    gVar.c(rawQuery.getString(2));
                    gVar.a(rawQuery.getString(3));
                    arrayList.add(gVar);
                }
            } while (rawQuery.moveToNext());
        }
        if (arrayList.size() > 101) {
            arrayList.add(null);
        }
        if (i > 0) {
            arrayList.set(1, new g("Continued - " + str, (byte) 0));
            if (str == null) {
                rawQuery.moveToFirst();
                Cursor rawQuery2 = c.rawQuery("SELECT * FROM history WHERE type = 1 AND id > " + rawQuery.getString(0) + " LIMIT 1", new String[0]);
                if (rawQuery2.moveToNext()) {
                    arrayList.set(1, new g("Continued - " + rawQuery2.getString(1), (byte) 0));
                }
            }
        }
        rawQuery.close();
        return arrayList;
    }

    public static void a() {
        e = true;
    }

    public static void a(int i) {
        c.delete("adblock", "hash = " + i, null);
    }

    private static void a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE history(id INTEGER PRIMARY KEY AUTOINCREMENT,url TEXT,title TEXT,favicon TEXT,type INTEGER,hash INTEGER)");
        sQLiteDatabase.execSQL("CREATE INDEX hash_index ON history (hash);");
        sQLiteDatabase.execSQL("CREATE TABLE datesHistory(id INTEGER,date TEXT,position INTEGER)");
        String format = new SimpleDateFormat("cccc d MMMM yyyy").format(new Date());
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", format);
        sQLiteDatabase.insert("datesHistory", null, contentValues);
    }

    public static void a(String str) {
        c.delete("history", "id = " + str, null);
    }

    public static void a(List list) {
        SQLiteStatement compileStatement = c.compileStatement("INSERT INTO filetable (name, parent, count, size, modified, path) VALUES (?, ?, ?, ?, ?, ?)");
        c.beginTransaction();
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                f fVar = (f) it.next();
                compileStatement.clearBindings();
                compileStatement.bindString(1, fVar.a());
                compileStatement.bindLong(2, (long) fVar.c());
                compileStatement.bindLong(3, (long) fVar.b());
                compileStatement.bindString(4, fVar.d());
                compileStatement.bindLong(5, fVar.e());
                compileStatement.bindString(6, fVar.g());
                compileStatement.execute();
            }
            c.setTransactionSuccessful();
        } finally {
            c.endTransaction();
        }
    }

    public static HashMap b(int i) {
        HashMap hashMap = new HashMap();
        Cursor rawQuery = c.rawQuery("SELECT * FROM filetable WHERE parent = " + i, null);
        if (rawQuery.moveToFirst()) {
            do {
                hashMap.put(rawQuery.getString(0), new f(rawQuery.getString(0), rawQuery.getInt(1), rawQuery.getInt(2), rawQuery.getString(3), rawQuery.getLong(4), rawQuery.getString(5)));
            } while (rawQuery.moveToNext());
        }
        rawQuery.close();
        return hashMap;
    }

    public static List b(String str) {
        ArrayList arrayList = new ArrayList();
        Cursor rawQuery = c.rawQuery("SELECT * FROM history WHERE title LIKE '%" + str + "%' OR url LIKE '%" + str + "%' ORDER BY id DESC LIMIT 5", null);
        if (rawQuery.moveToFirst()) {
            int i = 0;
            do {
                g gVar = new g();
                gVar.a(Integer.parseInt(rawQuery.getString(0)));
                gVar.b(rawQuery.getString(1));
                gVar.c(rawQuery.getString(2));
                gVar.b();
                arrayList.add(gVar);
                i++;
                if (!rawQuery.moveToNext()) {
                    break;
                }
            } while (i < 5);
        }
        rawQuery.close();
        return arrayList;
    }

    public static void b() {
        c.execSQL("DROP TABLE IF EXISTS history");
        c.execSQL("DROP TABLE IF EXISTS datesHistory");
        a(c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static boolean c() {
        String format = new SimpleDateFormat("cccc d MMMM yyyy").format(new Date());
        if (format.equals(a)) {
            return false;
        }
        a = format;
        Cursor rawQuery = c.rawQuery("SELECT * FROM history ORDER BY id DESC ", new String[0]);
        if (rawQuery.getCount() == 0) {
            c.execSQL("UPDATE datesHistory SET date = ? WHERE rowid = (SELECT MAX(rowid) FROM datesHistory)", new String[]{format});
            return true;
        }
        rawQuery.moveToFirst();
        if (rawQuery.getInt(rawQuery.getColumnIndex("type")) == 1) {
            c.execSQL("UPDATE datesHistory SET date = ? WHERE rowid = (SELECT MAX(rowid) FROM datesHistory)", new String[]{format});
        } else {
            Cursor rawQuery2 = c.rawQuery("SELECT * FROM datesHistory ORDER BY rowid DESC LIMIT 1", new String[0]);
            rawQuery2.moveToFirst();
            int i = rawQuery.getInt(rawQuery.getColumnIndex("id"));
            c.rawQuery("UPDATE datesHistory SET position = ? WHERE rowid = (SELECT MAX(rowid) FROM datesHistory)", new String[]{new StringBuilder().append(i + 1).toString()});
            b = new StringBuilder().append(i + 1).toString();
            ContentValues contentValues = new ContentValues();
            contentValues.put("title", rawQuery2.getString(1));
            contentValues.put("type", (Integer) 1);
            c.insert("history", null, contentValues);
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("date", format);
            c.insert("datesHistory", null, contentValues2);
            rawQuery2.close();
        }
        rawQuery.close();
        return true;
    }

    public static List d() {
        Cursor rawQuery = c.rawQuery("SELECT * FROM adblock", null);
        ArrayList arrayList = new ArrayList();
        if (rawQuery.moveToFirst()) {
            do {
                arrayList.add(rawQuery.getString(1));
            } while (rawQuery.moveToNext());
        }
        rawQuery.close();
        return arrayList;
    }

    public static void d(String str) {
        c.execSQL("UPDATE doodleTable SET url = ? ", new String[]{str});
    }

    public static void e() {
        c.execSQL("CREATE INDEX IF NOT EXISTS file_hash_index ON filetable (parent);");
    }

    public static String f() {
        String str = null;
        Cursor rawQuery = c.rawQuery("SELECT * FROM doodleTable", null);
        if (rawQuery.moveToFirst()) {
            str = rawQuery.getString(0);
            if (rawQuery.moveToNext()) {
                str = rawQuery.getString(0);
            }
        }
        rawQuery.close();
        return str;
    }

    public final synchronized void a(String str, String str2) {
        try {
            Cursor rawQuery = c.rawQuery("SELECT * FROM history WHERE id > ? AND hash = " + str.hashCode(), new String[]{b});
            if (rawQuery.moveToFirst()) {
                c.delete("history", "id =" + rawQuery.getInt(0), null);
            }
            rawQuery.close();
            ContentValues contentValues = new ContentValues();
            contentValues.put(ImagesContract.URL, str2);
            contentValues.put("hash", Integer.valueOf(str.hashCode()));
            contentValues.put("title", str);
            contentValues.put("favicon", String.valueOf(aq.a(str2).hashCode()) + ".png");
            c.insertOrThrow("history", null, contentValues);
        } catch (IllegalStateException e2) {
            Log.e("Lightning", "IllegalStateException in updateHistory");
        } catch (NullPointerException e3) {
            Log.e("Lightning", "NullPointerException in updateHistory");
        } catch (SQLiteException e4) {
            Log.e("Lightning", "SQLiteException in updateHistory");
        }
        return;
    }

    public final void b(List list) {
        new Thread(new e(this, list));
    }

    public final void c(String str) {
        if (str != null) {
            l.a(new d(this, str));
        }
    }

    public final synchronized void close() {
        if (e) {
            e = false;
        } else {
            if (c != null) {
                c.close();
            }
            super.close();
        }
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        a(sQLiteDatabase);
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS adblock(hash INTEGER,url TEXT)");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS filetable(name TEXT, parent INTEGER, count INTEGER, size INTEGER, modified INTEGER, path TEXT)");
        sQLiteDatabase.execSQL("CREATE TABLE doodleTable (url TEXT)");
        ContentValues contentValues = new ContentValues();
        contentValues.putNull(ImagesContract.URL);
        sQLiteDatabase.insert("doodleTable", null, contentValues);
        com.fastnet.browseralam.i.c.b();
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS history");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS datesHistory");
        onCreate(sQLiteDatabase);
    }
}
