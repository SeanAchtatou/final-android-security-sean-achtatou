package com.fastnet.browseralam.c;

import android.graphics.Bitmap;
import java.util.List;

public class b {
    private a a;
    private String b = "";
    private String c = "";
    private Bitmap d = null;
    private String e = "";
    private int f;

    public b() {
    }

    public b(String str, String str2, String str3, Bitmap bitmap) {
        this.b = str2;
        this.c = str;
        this.e = str3;
        this.d = bitmap;
    }

    public String a() {
        return this.c;
    }

    public void a(int i) {
        this.f = i;
    }

    public final void a(Bitmap bitmap) {
        this.d = bitmap;
    }

    public void a(b bVar) {
    }

    public void a(String str) {
        this.c = str;
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
    }

    public void c(a aVar) {
        this.a = aVar;
    }

    public final void c(String str) {
        this.e = str;
    }

    public boolean c() {
        return false;
    }

    public a d() {
        return this.a;
    }

    public a e() {
        return null;
    }

    public List f() {
        return null;
    }

    public int g() {
        return this.f;
    }

    public final String h() {
        return this.e;
    }

    public final Bitmap i() {
        return this.d;
    }
}
