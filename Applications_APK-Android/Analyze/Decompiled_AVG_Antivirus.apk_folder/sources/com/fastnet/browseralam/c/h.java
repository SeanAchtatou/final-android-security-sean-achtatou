package com.fastnet.browseralam.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.b.g;
import com.fastnet.browseralam.b.j;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.b.l;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.view.XWebView;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class h {
    /* access modifiers changed from: private */
    public static File q;
    private static y z;
    /* access modifiers changed from: private */
    public List a = new ArrayList();
    private HashMap b = new HashMap();
    /* access modifiers changed from: private */
    public List c = new ArrayList();
    /* access modifiers changed from: private */
    public RecyclerView d;
    private RelativeLayout e;
    private Button f;
    private c g;
    /* access modifiers changed from: private */
    public o h;
    /* access modifiers changed from: private */
    public BrowserActivity i;
    /* access modifiers changed from: private */
    public g j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public int n;
    private int o = aq.a(56);
    private int p = aq.a(15);
    private View.OnAttachStateChangeListener r = new j(this);
    /* access modifiers changed from: private */
    public Handler s = k.a();
    /* access modifiers changed from: private */
    public Runnable t = new k(this);
    private Runnable u = new l(this);
    /* access modifiers changed from: private */
    public Runnable v = new m(this);
    private j w = new n(this);
    /* access modifiers changed from: private */
    public t x;
    /* access modifiers changed from: private */
    public int y;

    public h(BrowserActivity browserActivity, LayoutInflater layoutInflater) {
        this.i = browserActivity;
        this.j = browserActivity;
        this.g = c.a(this.i);
        this.e = (RelativeLayout) layoutInflater.inflate((int) R.layout.history_layout, (ViewGroup) null);
        this.f = (Button) this.e.findViewById(R.id.clear_history);
        this.f.setOnClickListener(new aa(this, (byte) 0));
        this.d = (RecyclerView) this.e.findViewById(R.id.history_list);
        this.h = new o(this);
        this.d.a(this.h);
        this.d.a(new LinearLayoutManager());
        this.d.a(new x(this, (byte) 0));
        File file = new File(browserActivity.getFilesDir(), "HistoryFavicons/");
        q = file;
        if (!file.exists()) {
            q.mkdir();
        }
        z = new y(this, (byte) 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    public static String a(Context context) {
        File file = new File(context.getFilesDir(), "history.html");
        try {
            FileWriter fileWriter = new FileWriter(file, false);
            fileWriter.write("<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"><title>History</title></head><body onload=Android.showHistoryPage(); onfocus=Android.showHistoryPage(); ><div id=\"content\"><script type=\"text/javascript\">function showHistory(){Android.showHistoryPage();  return false;} var myEvent = window.attachEvent || window.addEventListener; var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload'; myEvent(chkevent, function(e) { Android.removeHistoryPage(); });  </script></div></body></html>");
            fileWriter.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return "file://" + file;
    }

    public static File b() {
        return q;
    }

    static /* synthetic */ void e(h hVar) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        int size = hVar.a.size();
        int i2 = size == 103 ? size - 1 : size;
        String str = q.getPath() + "/";
        for (int i3 = 2; i3 < i2 && !hVar.k; i3++) {
            g gVar = (g) hVar.a.get(i3);
            Bitmap bitmap = (Bitmap) hVar.b.get(gVar.c());
            if (bitmap == null) {
                if (new File(str + gVar.c()).exists() && (bitmap = BitmapFactory.decodeFile(str + gVar.c(), options)) != null) {
                    hVar.b.put(gVar.c(), bitmap);
                }
            }
            gVar.a(bitmap);
        }
        hVar.s.post(hVar.u);
    }

    static /* synthetic */ void l(h hVar) {
        boolean z2;
        boolean[] zArr = new boolean[hVar.c.size()];
        int i2 = 0;
        while (true) {
            if (i2 < hVar.c.size()) {
                if (((WeakReference) hVar.c.get(i2)).get() != null && ((XWebView) ((WeakReference) hVar.c.get(i2)).get()).F()) {
                    z2 = true;
                    break;
                } else {
                    zArr[i2] = true;
                    i2++;
                }
            } else {
                z2 = false;
                break;
            }
        }
        for (int i3 = 0; i3 < zArr.length; i3++) {
            if (zArr[i3]) {
                ((XWebView) ((WeakReference) hVar.c.get(i3)).get()).a((j) null);
                hVar.c.remove(i3);
            }
        }
        if (!z2) {
            hVar.a = new ArrayList();
            hVar.b = new HashMap();
        }
    }

    public final RelativeLayout a() {
        this.e.removeOnAttachStateChangeListener(this.r);
        this.e.addOnAttachStateChangeListener(this.r);
        return this.e;
    }

    public final RelativeLayout a(XWebView xWebView) {
        this.k = false;
        if (xWebView != null) {
            Iterator it = this.c.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((WeakReference) it.next()).get() == xWebView) {
                        break;
                    }
                } else {
                    this.c.add(new WeakReference(xWebView));
                    xWebView.a(this.w);
                    break;
                }
            }
        }
        if (!this.l) {
            l.a(new i(this));
        }
        return this.e;
    }

    public final void c() {
        aq.a(q);
        File file = new File(this.i.getFilesDir(), "HistoryFavicons/");
        q = file;
        file.mkdir();
    }
}
