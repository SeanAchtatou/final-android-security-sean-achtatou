package com.fastnet.browseralam.c;

import android.support.v7.widget.cf;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;

final class t extends cf {
    final RelativeLayout l;
    final TextView m;
    final TextView n;
    final ImageView o;
    final ImageView p;
    g q;
    public boolean r;
    final Runnable s = new u(this);
    final /* synthetic */ o t;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t(o oVar, View view) {
        super(view);
        this.t = oVar;
        this.p = (ImageView) view.findViewById(R.id.favicon);
        this.m = (TextView) view.findViewById(R.id.title);
        this.n = (TextView) view.findViewById(R.id.url);
        this.o = (ImageView) view.findViewById(R.id.delete_tab);
        this.l = (RelativeLayout) view.findViewById(R.id.historyItem);
        this.m.setOnTouchListener(oVar.c);
        this.o.setOnClickListener(oVar.d);
    }

    /* access modifiers changed from: package-private */
    public final void t() {
        this.t.e.s.removeCallbacks(this.s);
    }
}
