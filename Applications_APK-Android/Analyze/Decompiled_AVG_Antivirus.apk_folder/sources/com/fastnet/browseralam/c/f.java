package com.fastnet.browseralam.c;

import java.io.File;

public final class f {
    private String a;
    private int b;
    private int c;
    private String d;
    private String e;
    private long f;
    private boolean g;

    public f(File file, String str) {
        this.c = file.getPath().hashCode();
        this.e = Long.toString((long) this.c) + str;
    }

    public f(String str, int i, int i2, String str2, long j, String str3) {
        this.a = str;
        this.c = i;
        this.b = i2;
        this.d = i2 + " files  |  " + str2;
        this.f = j;
        this.e = str3;
    }

    public final String a() {
        return this.a;
    }

    public final void a(int i) {
        this.b += i;
    }

    public final void a(long j) {
        this.f = j;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final int b() {
        return this.b;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final int c() {
        return this.c;
    }

    public final String d() {
        this.g = true;
        return this.d;
    }

    public final long e() {
        return this.f;
    }

    public final boolean f() {
        return this.g;
    }

    public final String g() {
        return this.e;
    }

    public final void h() {
        this.b++;
    }
}
