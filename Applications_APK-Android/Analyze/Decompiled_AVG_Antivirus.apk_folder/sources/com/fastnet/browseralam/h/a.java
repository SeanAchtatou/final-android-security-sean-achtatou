package com.fastnet.browseralam.h;

import android.content.SharedPreferences;
import android.os.Environment;
import android.webkit.WebSettings;
import com.fastnet.browseralam.activity.BrowserApp;
import com.fastnet.browseralam.i.aq;
import com.jobstrak.drawingfun.sdk.utils.PrefsUtils;
import java.util.ArrayList;

public final class a {
    private static a a;
    private static SharedPreferences b;

    private a() {
        b = BrowserApp.a().getSharedPreferences(PrefsUtils.PREFS_SETTINGS, 0);
    }

    public static void A(boolean z) {
        a("newwindows", z);
    }

    public static boolean A() {
        return b.getBoolean("swipetabs", false);
    }

    public static int B() {
        return b.getInt("layoutmode", 0);
    }

    public static void B(boolean z) {
        a("linksbackground", z);
    }

    public static void C(boolean z) {
        a("restoreclosed", z);
    }

    public static boolean C() {
        return b.getBoolean("bottomtoolbar", false);
    }

    public static void D(boolean z) {
        a("keepbrowseropen", z);
    }

    public static boolean D() {
        return b.getBoolean("cardview", false);
    }

    public static void E(boolean z) {
        a("hardwarerendering", z);
    }

    public static boolean E() {
        return b.getBoolean("3dotbookmark", false);
    }

    public static void F(boolean z) {
        a("interceptvideo", z);
    }

    public static boolean F() {
        return b.getBoolean("location", false);
    }

    public static String G() {
        return b.getString("memory", "");
    }

    public static void G(boolean z) {
        a("customadblock", z);
    }

    public static void H(boolean z) {
        a("passwords", z);
    }

    public static boolean H() {
        return b.getBoolean("overviewmode", true);
    }

    public static void I(boolean z) {
        a("syncHistory", z);
    }

    public static boolean I() {
        return b.getBoolean("newwindows", false);
    }

    public static void J(boolean z) {
        a("textreflow", z);
    }

    public static int[] J() {
        String string = b.getString("menuorder", null);
        if (string == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        String[] split = string.split(",");
        for (String split2 : split) {
            String[] split3 = split2.split(":");
            if (split3[1].equals("1")) {
                arrayList.add(Integer.valueOf(Integer.parseInt(split3[0])));
            }
        }
        int[] iArr = new int[arrayList.size()];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = ((Integer) arrayList.get(i)).intValue();
        }
        return iArr;
    }

    public static void K(boolean z) {
        a("darkTheme", z);
    }

    public static int[] K() {
        String string = b.getString("linkorder", null);
        if (string == null) {
            return null;
        }
        int[] iArr = new int[7];
        String[] split = string.split(",");
        for (int i = 0; i < split.length; i++) {
            String[] split2 = split[i].split(":");
            if (split2[1].equals("1")) {
                iArr[i] = Integer.parseInt(split2[0]);
            }
        }
        return iArr;
    }

    public static void L(boolean z) {
        a("wideviewport", z);
    }

    public static int[] L() {
        String string = b.getString("imageorder", null);
        if (string == null) {
            return null;
        }
        int[] iArr = new int[7];
        String[] split = string.split(",");
        for (int i = 0; i < split.length; i++) {
            String[] split2 = split[i].split(":");
            if (split2[1].equals("1")) {
                iArr[i] = Integer.parseInt(split2[0]);
            }
        }
        return iArr;
    }

    public static String M() {
        return b.getString("linkorder", null);
    }

    public static String N() {
        return b.getString("imageorder", null);
    }

    public static String O() {
        return b.getString("menuorder", null);
    }

    public static int[] P() {
        String string = b.getString("window", null);
        if (string == null) {
            return null;
        }
        String[] split = string.split(",");
        int[] iArr = new int[4];
        for (int i = 0; i < 4; i++) {
            iArr[i] = Integer.parseInt(split[i]);
        }
        int a2 = aq.a(100);
        if (iArr[0] < a2 || iArr[1] < a2) {
            return null;
        }
        return iArr;
    }

    public static int Q() {
        return b.getInt("readingTextSize", 2);
    }

    public static int R() {
        return b.getInt("renderMode", 0);
    }

    public static boolean S() {
        return b.getBoolean("linksbackground", true);
    }

    public static boolean T() {
        return b.getBoolean("restoreclosed", true);
    }

    public static boolean U() {
        return b.getBoolean("keepbrowseropen", true);
    }

    public static boolean V() {
        return b.getBoolean("interceptvideo", false);
    }

    public static boolean W() {
        return b.getBoolean("intercepthelp", true);
    }

    public static boolean X() {
        return b.getBoolean("hardwarerendering", false);
    }

    public static boolean Y() {
        return b.getBoolean("customadblock", false);
    }

    public static String Z() {
        return b.getString("saveUrl", null);
    }

    public static a a() {
        if (a == null) {
            a = new a();
        }
        return a;
    }

    public static void a(int i) {
        a("enableflash", i);
    }

    public static void a(String str) {
        a(str, (String) null);
    }

    private static void a(String str, int i) {
        b.edit().putInt(str, i).apply();
    }

    private static void a(String str, String str2) {
        b.edit().putString(str, str2).apply();
    }

    private static void a(String str, boolean z) {
        b.edit().putBoolean(str, z).apply();
    }

    public static void a(String str, String[] strArr, int[][] iArr) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strArr.length; i++) {
            sb.append(Integer.toString(iArr[i][0]));
            if (iArr[i][1] == 1) {
                sb.append(":1:");
            } else {
                sb.append(":0:");
            }
            sb.append(strArr[i]);
            if (i == strArr.length - 1) {
                break;
            }
            sb.append(',');
        }
        a(str, sb.toString());
    }

    public static void a(boolean z) {
        a("restart", z);
    }

    public static void a(int[] iArr) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            sb.append(iArr[i]);
            if (i != 3) {
                sb.append(',');
            }
        }
        a("window", sb.toString());
    }

    public static boolean aa() {
        return b.getBoolean("passwords", true);
    }

    public static int ab() {
        return b.getInt("search", 1);
    }

    public static String ac() {
        return b.getString("searchurl", "https://www.google.com/search?client=lightning&ie=UTF-8&oe=UTF-8&q=");
    }

    public static boolean ad() {
        return b.getBoolean("syncHistory", true);
    }

    public static boolean ae() {
        return b.getBoolean("SystemBrowser", false);
    }

    public static boolean af() {
        return b.getBoolean("textreflow", false);
    }

    public static int ag() {
        return b.getInt("textsize", 100);
    }

    public static int ah() {
        return b.getInt("urlContent", 0);
    }

    public static boolean ai() {
        return b.getBoolean("darkTheme", false);
    }

    public static boolean aj() {
        return b.getBoolean("useProxy", false);
    }

    public static int ak() {
        return b.getInt("agentchoose", 1);
    }

    public static boolean al() {
        return b.getBoolean("wideviewport", true);
    }

    public static void am() {
        a("readingTextSize", 2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.h.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.h.a.a(java.lang.String, int):void
      com.fastnet.browseralam.h.a.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.h.a.a(java.lang.String, boolean):void */
    public static void an() {
        a("intercepthelp", false);
    }

    public static String b(String str) {
        return b.getString("userAgentString", str);
    }

    public static void b(int i) {
        a("layoutmode", i);
    }

    public static void b(boolean z) {
        a("AdBlock", z);
    }

    public static boolean b() {
        return b.getBoolean("restart", false);
    }

    public static void c(int i) {
        a("renderMode", i);
    }

    public static void c(String str) {
        a("showDownloadDialog", str);
    }

    public static void c(boolean z) {
        a("blockimages", z);
    }

    public static boolean c() {
        return b.getBoolean("AdBlock", true);
    }

    public static void d(int i) {
        a("search", i);
    }

    public static void d(String str) {
        a("home", str);
    }

    public static void d(boolean z) {
        a("blockpopups", z);
    }

    public static boolean d() {
        return b.getBoolean("blockimages", false);
    }

    public static void e(int i) {
        a("textsize", i);
    }

    public static void e(String str) {
        a("memory", str);
    }

    public static void e(boolean z) {
        a("thirdParty", z);
    }

    public static boolean e() {
        return b.getBoolean("blockpopups", false);
    }

    public static void f(int i) {
        a("urlContent", i);
    }

    public static void f(String str) {
        a("saveUrl", str);
    }

    public static void f(boolean z) {
        a("cache", z);
    }

    public static boolean f() {
        return b.getBoolean("thirdParty", false);
    }

    public static void g(int i) {
        a("agentchoose", i);
    }

    public static void g(String str) {
        a("searchurl", str);
    }

    public static void g(boolean z) {
        a("clearCookiesExit", z);
    }

    public static boolean g() {
        return b.getBoolean("cache", false);
    }

    public static void h(String str) {
        a("userAgentString", str);
    }

    public static void h(boolean z) {
        a("clearHistoryExit", z);
    }

    public static boolean h() {
        return b.getBoolean("clearCookiesExit", false);
    }

    public static void i(boolean z) {
        a("cookies", z);
    }

    public static boolean i() {
        return b.getBoolean("clearHistoryExit", false);
    }

    public static void j(boolean z) {
        a("stacktabstop", z);
    }

    public static boolean j() {
        return b.getBoolean("cookies", true);
    }

    public static String k() {
        return b.getString("showDownloadDialog", Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_DOWNLOADS);
    }

    public static void k(boolean z) {
        a("fullscreendrawer", z);
    }

    public static int l() {
        return b.getInt("enableflash", 0);
    }

    public static void l(boolean z) {
        a("drawercards", z);
    }

    public static WebSettings.PluginState m() {
        b.getInt("enableflash", 0);
        switch (b.getInt("enableflash", 0)) {
            case 0:
                return WebSettings.PluginState.OFF;
            case 1:
                return WebSettings.PluginState.ON_DEMAND;
            case 2:
                return WebSettings.PluginState.ON;
            default:
                return WebSettings.PluginState.OFF;
        }
    }

    public static void m(boolean z) {
        a("fullscreen", z);
    }

    public static void n(boolean z) {
        a("GoogleSearchSuggestions", z);
    }

    public static boolean n() {
        return b.getBoolean("stacktabstop", false);
    }

    public static void o(boolean z) {
        a("hidestatus", z);
    }

    public static boolean o() {
        return b.getBoolean("fullscreendrawer", false);
    }

    public static void p(boolean z) {
        a("resizewindow", z);
    }

    public static boolean p() {
        return b.getBoolean("drawercards", false);
    }

    public static void q(boolean z) {
        a("incognitocookies", z);
    }

    public static boolean q() {
        return b.getBoolean("fullscreen", false);
    }

    public static void r(boolean z) {
        a("invertColors", z);
    }

    public static boolean r() {
        return b.getBoolean("GoogleSearchSuggestions", true);
    }

    public static void s(boolean z) {
        a("java", z);
    }

    public static boolean s() {
        return b.getBoolean("hidestatus", false);
    }

    public static void t(boolean z) {
        a("pullrefresh", z);
    }

    public static boolean t() {
        return b.getBoolean("resizewindow", false);
    }

    public static String u() {
        return b.getString("home", "about:home");
    }

    public static void u(boolean z) {
        a("swipetabs", z);
    }

    public static void v(boolean z) {
        a("bottomtoolbar", z);
    }

    public static boolean v() {
        return b.getBoolean("incognitocookies", false);
    }

    public static void w(boolean z) {
        a("cardview", z);
    }

    public static boolean w() {
        return b.getBoolean("invertColors", false);
    }

    public static void x(boolean z) {
        a("3dotbookmark", z);
    }

    public static boolean x() {
        return b.getInt("renderMode", 0) == 4;
    }

    public static void y(boolean z) {
        a("location", z);
    }

    public static boolean y() {
        return b.getBoolean("java", true);
    }

    public static void z(boolean z) {
        a("overviewmode", z);
    }

    public static boolean z() {
        return b.getBoolean("pullrefresh", true);
    }
}
