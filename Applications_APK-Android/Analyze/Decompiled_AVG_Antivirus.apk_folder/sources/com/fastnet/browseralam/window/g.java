package com.fastnet.browseralam.window;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.activity.MainActivity;
import com.fastnet.browseralam.h.a;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.window.XWindow;

final class g implements View.OnClickListener {
    final /* synthetic */ View a;
    final /* synthetic */ a b;

    g(a aVar, View view) {
        this.b = aVar;
        this.a = view;
    }

    public final void onClick(View view) {
        XWindow.XLayoutParams c = this.b.getLayoutParams();
        int[] iArr = {c.width, c.height, c.x, c.y};
        a.a();
        a.a(iArr);
        this.a.setVisibility(8);
        l a2 = this.b.a();
        if (a2.a != null) {
            a2.a.x = 0;
            a2.a.y = -aq.a(40);
            a2.a.width = a2.d.i;
            a2.a.height = a2.d.j - a2.a.y;
        }
        a2.a();
        BrowserActivity.e().p();
        Intent intent = new Intent(this.b.getContext().getApplicationContext(), MainActivity.class);
        intent.addFlags(268435456);
        this.b.getContext().getApplicationContext().startActivity(intent);
        Handler handler = new Handler();
        handler.postDelayed(new h(this, handler), 10);
    }
}
