package com.fastnet.browseralam.window;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.window.XWindow;
import java.util.LinkedList;

public final class a extends FrameLayout {
    public Class a;
    public int b;
    public int c;
    public boolean d;
    public XWindow.XLayoutParams e;
    public int f;
    public n g;
    public Bundle h;
    int i;
    int j;
    /* access modifiers changed from: private */
    public final XWindow k;
    private View.OnAttachStateChangeListener l = new c(this);
    private XWindow.XLayoutParams m;
    private l n;
    private int o = aq.a(26);
    /* access modifiers changed from: private */
    public int p = (aq.b() - aq.a(66));

    public a(XWindow xWindow, int i2) {
        super(xWindow);
        View findViewById;
        xWindow.setTheme(XWindow.d());
        this.k = xWindow;
        this.a = xWindow.getClass();
        this.b = i2;
        this.e = xWindow.a(i2);
        this.n = new l(this);
        this.f = xWindow.c();
        this.g = new n();
        this.g.i = ((float) this.e.width) / ((float) this.e.height);
        this.h = new Bundle();
        DisplayMetrics displayMetrics = this.k.getResources().getDisplayMetrics();
        this.i = displayMetrics.widthPixels;
        this.j = (int) (((float) displayMetrics.heightPixels) - (displayMetrics.density * 25.0f));
        View b2 = xWindow.b();
        FrameLayout frameLayout = (FrameLayout) b2.findViewById(R.id.body);
        View findViewById2 = b2.findViewById(R.id.titlebar);
        findViewById2.setOnTouchListener(new e(this));
        ((TextView) b2.findViewById(R.id.title)).setText(this.k.a());
        View findViewById3 = b2.findViewById(R.id.hide);
        findViewById3.setOnClickListener(new f(this));
        findViewById3.setVisibility(8);
        View findViewById4 = b2.findViewById(R.id.maximize);
        findViewById4.setOnClickListener(new g(this, findViewById2));
        View findViewById5 = b2.findViewById(R.id.close);
        findViewById5.setOnClickListener(new i(this));
        View findViewById6 = b2.findViewById(R.id.corner);
        findViewById6.setOnTouchListener(new j(this));
        if (aq.a(this.f, m.g)) {
            findViewById3.setVisibility(0);
        }
        if (aq.a(this.f, m.d)) {
            findViewById4.setVisibility(8);
        }
        if (aq.a(this.f, m.b)) {
            findViewById5.setVisibility(8);
        }
        if (aq.a(this.f, m.e)) {
            findViewById2.setOnTouchListener(null);
        }
        if (aq.a(this.f, m.c)) {
            findViewById6.setVisibility(8);
        }
        addView(b2);
        frameLayout.setOnTouchListener(new b(this, xWindow, i2));
        if (!aq.a(this.f, m.o)) {
            a(frameLayout);
        }
        if (!aq.a(this.f, m.p) && !aq.a(this.f, m.q) && (findViewById = frameLayout.findViewById(R.id.corner)) != null) {
            findViewById.setOnTouchListener(new k(this));
        }
        setTag(BrowserActivity.e());
        addOnAttachStateChangeListener(this.l);
    }

    private static void a(View view) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(view);
        while (true) {
            View view2 = (View) linkedList.poll();
            if (view2 == null) {
                return;
            }
            if (view2 instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view2;
                for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                    linkedList.add(viewGroup.getChildAt(i2));
                }
            }
        }
    }

    public final l a() {
        return this.n;
    }

    public final boolean a(boolean z) {
        if (aq.a(this.f, m.m) || z == this.d) {
            return false;
        }
        this.d = z;
        XWindow.g();
        if (!aq.a(this.f, m.n)) {
            View findViewById = findViewById(R.id.content);
            if (!z && !aq.a(this.f, m.a)) {
                findViewById.setBackgroundResource(0);
            }
        }
        XWindow.XLayoutParams c2 = getLayoutParams();
        c2.a(z);
        this.k.a(c2);
        if (z) {
            XWindow.a(this);
        } else if (XWindow.k() == this) {
            XWindow.a((a) null);
        }
        return true;
    }

    public final XWindow.XLayoutParams b() {
        this.n = new l(this);
        return this.n.a;
    }

    /* renamed from: c */
    public final XWindow.XLayoutParams getLayoutParams() {
        XWindow.XLayoutParams xLayoutParams = (XWindow.XLayoutParams) super.getLayoutParams();
        return xLayoutParams == null ? this.e : xLayoutParams;
    }

    public final boolean dispatchKeyEvent(KeyEvent keyEvent) {
        XWindow.h();
        if (keyEvent.getAction() != 1 || keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        this.k.b(this);
        return true;
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.m = getLayoutParams();
            if (XWindow.k() != this) {
                this.k.d(this.b);
            }
        }
        if (motionEvent.getPointerCount() < 2 || !aq.a(this.f, m.l) || (motionEvent.getAction() & 255) != 5) {
            return false;
        }
        this.g.f = 1.0d;
        this.g.e = -1.0d;
        this.g.g = (double) this.m.width;
        this.g.h = (double) this.m.height;
        return true;
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 4:
                if (XWindow.k() == this) {
                    this.k.b(this);
                }
                XWindow.e();
                break;
        }
        if (motionEvent.getPointerCount() >= 2 && aq.a(this.f, m.l)) {
            float x = motionEvent.getX(0);
            float y = motionEvent.getY(0);
            float x2 = motionEvent.getX(1);
            double sqrt = Math.sqrt(Math.pow((double) (y - motionEvent.getY(1)), 2.0d) + Math.pow((double) (x - x2), 2.0d));
            switch (motionEvent.getAction() & 255) {
                case 2:
                    if (this.g.e == -1.0d) {
                        this.g.e = sqrt;
                    }
                    this.g.f *= sqrt / this.g.e;
                    this.g.e = sqrt;
                    l lVar = this.n;
                    lVar.b = 0.5f;
                    lVar.c = 0.5f;
                    lVar.a((int) (this.g.g * this.g.f), (int) (this.g.h * this.g.f)).a();
                    break;
            }
            XWindow.f();
        }
        return true;
    }
}
