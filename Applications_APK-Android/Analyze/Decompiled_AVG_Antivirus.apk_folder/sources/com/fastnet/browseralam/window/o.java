package com.fastnet.browseralam.window;

import android.util.SparseArray;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class o {
    public Map a = new HashMap();

    public final int a() {
        return this.a.size();
    }

    public final a a(int i, Class cls) {
        SparseArray sparseArray = (SparseArray) this.a.get(cls);
        if (sparseArray == null) {
            return null;
        }
        return (a) sparseArray.get(i);
    }

    public final Set a(Class cls) {
        SparseArray sparseArray = (SparseArray) this.a.get(cls);
        if (sparseArray == null) {
            return new HashSet();
        }
        HashSet hashSet = new HashSet();
        for (int i = 0; i < sparseArray.size(); i++) {
            hashSet.add(Integer.valueOf(sparseArray.keyAt(i)));
        }
        return hashSet;
    }

    public final void b(int i, Class cls) {
        SparseArray sparseArray = (SparseArray) this.a.get(cls);
        if (sparseArray != null) {
            sparseArray.remove(i);
            if (sparseArray.size() == 0) {
                this.a.remove(cls);
            }
        }
    }
}
