package com.fastnet.browseralam.window;

import android.os.Handler;
import android.os.Message;
import com.fastnet.browseralam.activity.BrowserActivity;

final class r extends Handler {
    final /* synthetic */ XWindow a;
    private boolean b;

    public r(XWindow xWindow) {
        this.a = xWindow;
    }

    public final void a() {
        if (!this.b) {
            this.b = true;
            sendEmptyMessageDelayed(1, 32);
        }
    }

    public final void b() {
        removeMessages(1);
        this.b = false;
        BrowserActivity.e().r();
    }

    public final void handleMessage(Message message) {
        this.a.a(this.a.k);
        BrowserActivity.e().r();
        sendEmptyMessageDelayed(1, 32);
    }
}
