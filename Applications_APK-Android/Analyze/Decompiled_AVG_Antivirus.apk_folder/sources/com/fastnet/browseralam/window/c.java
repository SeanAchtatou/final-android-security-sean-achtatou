package com.fastnet.browseralam.window;

import android.os.Handler;
import android.view.View;

final class c implements View.OnAttachStateChangeListener {
    final /* synthetic */ a a;

    c(a aVar) {
        this.a = aVar;
    }

    public final void onViewAttachedToWindow(View view) {
        new Handler().postDelayed(new d(this), 100);
        view.removeOnAttachStateChangeListener(this);
    }

    public final void onViewDetachedFromWindow(View view) {
    }
}
