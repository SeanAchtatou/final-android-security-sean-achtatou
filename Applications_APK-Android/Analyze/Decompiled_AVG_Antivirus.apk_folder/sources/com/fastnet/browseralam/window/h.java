package com.fastnet.browseralam.window;

import android.os.Handler;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.window.XWindow;

final class h implements Runnable {
    final int a = (this.d.b.j + aq.a(40));
    final int b = this.d.b.i;
    final /* synthetic */ Handler c;
    final /* synthetic */ g d;

    h(g gVar, Handler handler) {
        this.d = gVar;
        this.c = handler;
    }

    public final void run() {
        XWindow.XLayoutParams c2 = this.d.b.getLayoutParams();
        if (BrowserActivity.e().o()) {
            this.c.postDelayed(this, 20);
        } else if (c2.height == this.a && c2.width == this.b) {
            this.d.a.setVisibility(0);
            BrowserActivity.e().b(false);
            ((XWindow) this.d.b.getContext()).j();
        } else {
            this.c.postDelayed(this, 20);
        }
    }
}
