package com.fastnet.browseralam.window;

import android.animation.Animator;
import com.fastnet.browseralam.e.a;

final class q extends a {
    final /* synthetic */ a a;
    final /* synthetic */ int b;
    final /* synthetic */ XWindow c;

    q(XWindow xWindow, a aVar, int i) {
        this.c = xWindow;
        this.a = aVar;
        this.b = i;
    }

    public final void onAnimationEnd(Animator animator) {
        try {
            this.c.e.removeView(this.a);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.a.c = 0;
        XWindow.c.b(this.b, this.c.getClass());
        boolean unused = this.c.g = false;
        this.c.stopForeground(true);
    }
}
