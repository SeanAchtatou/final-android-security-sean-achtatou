package com.fastnet.browseralam.window;

import android.view.MotionEvent;
import android.view.View;

final class b implements View.OnTouchListener {
    final /* synthetic */ XWindow a;
    final /* synthetic */ int b;
    final /* synthetic */ a c;

    b(a aVar, XWindow xWindow, int i) {
        this.c = aVar;
        this.a = xWindow;
        this.b = i;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        this.a.a(this.b, this.c, motionEvent);
        XWindow.e();
        return true;
    }
}
