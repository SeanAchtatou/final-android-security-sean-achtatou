package com.fastnet.browseralam.window;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.bd;
import android.util.SparseArray;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RemoteViews;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.i.aq;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class XWindow extends Service {
    static XWindow b;
    static o c = new o();
    static a d = null;
    private static boolean j;
    private NotificationManager a;
    WindowManager e;
    LayoutInflater f;
    /* access modifiers changed from: private */
    public boolean g;
    private bd h;
    private RemoteViews i;
    /* access modifiers changed from: private */
    public XLayoutParams k;
    private r l;

    public class XLayoutParams extends WindowManager.LayoutParams {
        public int a;
        public int b;
        public int c;
        public int d;
        public int e;

        private XLayoutParams(int i) {
            super(200, 200, 2002, 262176, -3);
            int c2 = XWindow.this.c();
            a(false);
            if (!aq.a(c2, m.j)) {
                this.flags |= 512;
            }
            this.x = ((XWindow.c.a() * 100) + (i * 100)) % (XWindow.this.e.getDefaultDisplay().getWidth() - this.width);
            int i2 = this.height;
            Display defaultDisplay = XWindow.this.e.getDefaultDisplay();
            this.y = (((((i * 100) * 200) / (defaultDisplay.getWidth() - this.width)) + this.x) + (XWindow.c.a() * 100)) % (defaultDisplay.getHeight() - i2);
            this.gravity = 51;
            this.a = 10;
            this.c = 0;
            this.b = 0;
            this.e = Integer.MAX_VALUE;
            this.d = Integer.MAX_VALUE;
        }

        private XLayoutParams(XWindow xWindow, int i, int i2, int i3) {
            this(i);
            this.width = i2;
            this.height = i3;
        }

        public XLayoutParams(XWindow xWindow, int i, int i2, int i3, int i4, int i5) {
            this(xWindow, i, i2, i3);
            if (i4 != -2147483647) {
                this.x = i4;
            }
            if (i5 != -2147483647) {
                this.y = i5;
            }
            Display defaultDisplay = xWindow.e.getDefaultDisplay();
            int width = defaultDisplay.getWidth();
            int height = defaultDisplay.getHeight();
            if (this.x == Integer.MAX_VALUE) {
                this.x = width - i2;
            } else if (this.x == Integer.MIN_VALUE) {
                this.x = (width - i2) / 2;
            }
            if (this.y == Integer.MAX_VALUE) {
                this.y = height - i3;
            } else if (this.y == Integer.MIN_VALUE) {
                this.y = (height - i3) / 2;
            }
        }

        public final void a(boolean z) {
            if (z) {
                this.flags ^= 8;
            } else {
                this.flags |= 8;
            }
        }
    }

    public static void a(Context context, Class cls) {
        boolean z = c.a(0, cls) != null;
        context.startService(new Intent(context, cls).putExtra("id", 0).setAction(z ? "RESTORE" : "SHOW").setData(z ? Uri.parse("standout://" + cls + '/' + 0) : null));
    }

    public static void a(a aVar) {
        d = aVar;
    }

    public static void b(Context context, Class cls) {
        context.startService(new Intent(context, cls).putExtra("id", 0).setAction("CLOSE"));
    }

    public static void c(Context context, Class cls) {
        context.startService(new Intent(context, cls).setAction("CLOSE_ALL"));
    }

    public static int d() {
        return 0;
    }

    private synchronized a e(int i2) {
        a aVar;
        j = true;
        a g2 = g(i2);
        if (g2 == null) {
            g2 = new a(this, i2);
        }
        if (g2.c == 1) {
            d(i2);
            aVar = g2;
        } else {
            g2.c = 1;
            XLayoutParams c2 = g2.getLayoutParams();
            try {
                if (g2.getChildAt(0).findViewById(R.id.main_layout) == null) {
                    a((FrameLayout) g2.getChildAt(0));
                }
                this.e.addView(g2, c2);
                g2.getChildAt(0).animate().alpha(1.0f).setDuration(100).setListener(null);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            o oVar = c;
            Class<?> cls = getClass();
            SparseArray sparseArray = (SparseArray) oVar.a.get(cls);
            if (sparseArray == null) {
                sparseArray = new SparseArray();
                oVar.a.put(cls, sparseArray);
            }
            sparseArray.put(i2, g2);
            Context applicationContext = getApplicationContext();
            this.i = new RemoteViews(getPackageName(), (int) R.layout.notification);
            this.i.setOnClickPendingIntent(R.id.restore, PendingIntent.getService(this, 0, new Intent(applicationContext, SpeedWindow.class).setAction("RESTORE"), 134217728));
            this.i.setTextViewText(R.id.text, "Speed is running");
            this.i.setViewVisibility(R.id.restore, 8);
            this.i.setViewVisibility(R.id.close, 8);
            this.h = new bd(applicationContext);
            bd bdVar = this.h;
            bdVar.B.icon = R.drawable.notification;
            bd b2 = bdVar.a().b();
            b2.B.contentView = this.i;
            Notification c3 = b2.c();
            if (c3 != null) {
                if (!this.g) {
                    startForeground(getClass().hashCode() - 1, c3);
                    this.g = true;
                } else {
                    this.a.notify(getClass().hashCode() - 1, c3);
                }
            } else if (!this.g) {
                throw new RuntimeException("Your StandOutWindow service must provide a persistent notification. The notification prevents Androidfrom killing your service in low memory situations.");
            }
            d(i2);
            aVar = g2;
        }
        return aVar;
    }

    public static boolean e() {
        return false;
    }

    public static void f() {
    }

    private synchronized void f(int i2) {
        a g2 = g(i2);
        if (g2 == null) {
            throw new IllegalArgumentException("Tried to bringToFront(" + i2 + ") a null window.");
        } else if (g2.c == 0) {
            throw new IllegalStateException("Tried to bringToFront(" + i2 + ") a window that is not shown.");
        } else if (g2.c != 2) {
            XLayoutParams c2 = g2.getLayoutParams();
            try {
                this.e.removeView(g2);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                this.e.addView(g2, c2);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    private a g(int i2) {
        return c.a(i2, getClass());
    }

    public static boolean g() {
        return false;
    }

    public static boolean h() {
        return false;
    }

    public static boolean i() {
        return j;
    }

    public static a k() {
        return d;
    }

    private synchronized void l() {
        LinkedList linkedList = new LinkedList();
        for (Integer intValue : c.a(getClass())) {
            linkedList.add(Integer.valueOf(intValue.intValue()));
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            c(((Integer) it.next()).intValue());
        }
    }

    public abstract XLayoutParams a(int i2);

    public abstract String a();

    public abstract void a(FrameLayout frameLayout);

    public final void a(XLayoutParams xLayoutParams) {
        a aVar = d;
        if (aVar != null && aVar.c != 0 && aVar.c != 2) {
            try {
                aVar.setLayoutParams(xLayoutParams);
                this.e.updateViewLayout(aVar, xLayoutParams);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public final boolean a(int i2, a aVar, MotionEvent motionEvent) {
        boolean z = false;
        int i3 = aVar.g.c - aVar.g.a;
        int i4 = aVar.g.d - aVar.g.b;
        switch (motionEvent.getAction()) {
            case 0:
                this.k = aVar.b();
                aVar.g.c = (int) motionEvent.getRawX();
                aVar.g.d = (int) motionEvent.getRawY();
                aVar.g.a = aVar.g.c;
                aVar.g.b = aVar.g.d;
                break;
            case 1:
                aVar.g.j = false;
                if (motionEvent.getPointerCount() != 1) {
                    if (aq.a(aVar.f, m.h)) {
                        f(i2);
                        break;
                    }
                } else {
                    if (Math.abs(i3) < this.k.a && Math.abs(i4) < this.k.a) {
                        z = true;
                    }
                    if (z && aq.a(aVar.f, m.i)) {
                        f(i2);
                        break;
                    }
                }
                break;
            case 2:
                int rawX = ((int) motionEvent.getRawX()) - aVar.g.c;
                int rawY = ((int) motionEvent.getRawY()) - aVar.g.d;
                aVar.g.c = (int) motionEvent.getRawX();
                aVar.g.d = (int) motionEvent.getRawY();
                if (aVar.g.j || Math.abs(i3) >= this.k.a || Math.abs(i4) >= this.k.a) {
                    aVar.g.j = true;
                    if (motionEvent.getPointerCount() == 1) {
                        this.k.x += rawX;
                        this.k.y += rawY;
                    }
                    l a2 = aVar.a();
                    int i5 = this.k.x;
                    int i6 = this.k.y;
                    if (a2.a != null) {
                        if (a2.b < 0.0f || a2.b > 1.0f || a2.c < 0.0f || a2.c > 1.0f) {
                            throw new IllegalStateException("Anchor point must be between 0 and 1, inclusive.");
                        }
                        if (i5 != Integer.MIN_VALUE) {
                            a2.a.x = (int) (((float) i5) - (((float) a2.a.width) * a2.b));
                        }
                        if (i6 != Integer.MIN_VALUE) {
                            a2.a.y = (int) (((float) i6) - (((float) a2.a.height) * a2.c));
                        }
                        a2.a.y = Math.min(Math.max(a2.a.y, 0), a2.d.p);
                    }
                    a2.a();
                    break;
                }
                break;
        }
        return true;
    }

    public final boolean a(a aVar, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.k = aVar.b();
                aVar.g.c = (int) motionEvent.getRawX();
                aVar.g.d = (int) motionEvent.getRawY();
                aVar.g.a = aVar.g.c;
                aVar.g.b = aVar.g.d;
                this.l.a();
                return true;
            case 1:
            case 3:
                this.l.b();
                return true;
            case 2:
                int rawX = ((int) motionEvent.getRawX()) - aVar.g.c;
                int rawY = ((int) motionEvent.getRawY()) - aVar.g.d;
                XLayoutParams xLayoutParams = this.k;
                xLayoutParams.width = rawX + xLayoutParams.width;
                XLayoutParams xLayoutParams2 = this.k;
                xLayoutParams2.height = rawY + xLayoutParams2.height;
                if (this.k.width >= this.k.b && this.k.width <= this.k.d) {
                    aVar.g.c = (int) motionEvent.getRawX();
                }
                if (this.k.height >= this.k.c && this.k.height <= this.k.e) {
                    aVar.g.d = (int) motionEvent.getRawY();
                }
                aVar.a().a(this.k.width, this.k.height);
                return true;
            default:
                return true;
        }
    }

    public abstract View b();

    public final synchronized void b(int i2) {
        j = false;
        a g2 = g(i2);
        if (g2 == null) {
            throw new IllegalArgumentException("Tried to hide(" + i2 + ") a null window.");
        } else if (g2.c != 0) {
            if (aq.a(g2.f, m.g)) {
                g2.c = 2;
                try {
                    g2.getChildAt(0).animate().alpha(0.0f).setDuration(100).setListener(new p(this, g2));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                this.i.setTextViewText(R.id.text, "Speed is minimized");
                this.i.setViewVisibility(R.id.restore, 0);
                this.i.setViewVisibility(R.id.close, 0);
                this.a.notify(getClass().hashCode() - 1, this.h.c());
            } else {
                c(i2);
            }
        }
    }

    public final synchronized boolean b(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Tried to unfocus a null window.");
        }
        return aVar.a(false);
    }

    public int c() {
        return 0;
    }

    public final synchronized void c(int i2) {
        j = false;
        a g2 = g(i2);
        if (g2 != null) {
            if (g2.c != 2) {
                this.a.cancel(getClass().hashCode() + i2);
                b(g2);
                g2.c = 2;
                g2.getChildAt(0).animate().alpha(0.0f).setDuration(100).setListener(new q(this, g2, i2));
            }
        }
    }

    public final synchronized boolean d(int i2) {
        boolean z = false;
        synchronized (this) {
            a g2 = g(i2);
            if (g2 != null) {
                if (!aq.a(g2.f, m.m)) {
                    if (d != null) {
                        b(d);
                    }
                    z = g2.a(true);
                }
            }
        }
        return z;
    }

    public final synchronized void j() {
        j = false;
        a g2 = g(0);
        if (g2 == null) {
            throw new IllegalArgumentException("Tried to close(0) a null window.");
        } else if (g2.c != 2) {
            this.a.cancel(getClass().hashCode() + 0);
            b(g2);
            g2.c = 2;
            try {
                this.e.removeView(g2);
                g2.c = 0;
                c.b(0, getClass());
                this.g = false;
                stopForeground(true);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.e = (WindowManager) getSystemService("window");
        this.a = (NotificationManager) getSystemService(NotificationAd.BANNER_ID);
        this.f = (LayoutInflater) getSystemService("layout_inflater");
        this.g = false;
        this.l = new r(this);
    }

    public void onDestroy() {
        super.onDestroy();
        b = null;
        l();
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        super.onStartCommand(intent, i2, i3);
        if (intent == null) {
            return 2;
        }
        String action = intent.getAction();
        int intExtra = intent.getIntExtra("id", 0);
        if (intExtra == -1) {
            throw new RuntimeException("ID cannot equals StandOutWindow.ONGOING_NOTIFICATION_ID");
        } else if ("SHOW".equals(action) || "RESTORE".equals(action)) {
            e(intExtra);
            return 2;
        } else if ("HIDE".equals(action)) {
            b(intExtra);
            return 2;
        } else if ("CLOSE".equals(action)) {
            c(intExtra);
            return 2;
        } else if ("CLOSE_ALL".equals(action)) {
            l();
            return 2;
        } else if (!"SEND_DATA".equals(action)) {
            return 2;
        } else {
            intent.getBundleExtra("wei.mark.standout.data");
            intent.getIntExtra("requestCode", 0);
            intent.getSerializableExtra("wei.mark.standout.fromCls");
            intent.getIntExtra("fromId", 0);
            return 2;
        }
    }
}
