package com.fastnet.browseralam.window;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.h.a;
import com.fastnet.browseralam.i.aq;
import com.fastnet.browseralam.window.XWindow;

public class SpeedWindow extends XWindow {
    FrameLayout.LayoutParams a = new FrameLayout.LayoutParams(-1, -1);

    public final XWindow.XLayoutParams a(int i) {
        a.a();
        int[] P = a.P();
        XWindow.XLayoutParams xLayoutParams = new XWindow.XLayoutParams(this, i, aq.a() - aq.a(56), aq.b() - aq.a(112), Integer.MIN_VALUE, Integer.MIN_VALUE);
        if (P == null) {
            return xLayoutParams;
        }
        return new XWindow.XLayoutParams(this, i, P[0], P[1], P[2], P[3]);
    }

    public final String a() {
        return "";
    }

    public final void a(FrameLayout frameLayout) {
        frameLayout.addView(BrowserActivity.d(), 1, this.a);
    }

    public final View b() {
        FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(BrowserActivity.e()).inflate((int) R.layout.window_frame, (ViewGroup) null);
        this.a.topMargin = aq.a(40);
        frameLayout.addView(BrowserActivity.d(), 1, this.a);
        return frameLayout;
    }

    public final int c() {
        return m.a | m.f | m.g | m.i;
    }
}
