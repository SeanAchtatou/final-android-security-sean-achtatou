package com.fastnet.browseralam.window;

import com.fastnet.browseralam.window.XWindow;

public final class l {
    XWindow.XLayoutParams a;
    float b = 0.0f;
    float c = 0.0f;
    final /* synthetic */ a d;

    public l(a aVar) {
        this.d = aVar;
        this.a = aVar.getLayoutParams();
    }

    public final l a(int i, int i2) {
        this.a.width = i;
        this.a.height = i2;
        int i3 = this.a.d;
        int i4 = this.a.e;
        this.a.width = Math.min(Math.max(this.a.width, this.a.b), i3);
        this.a.height = Math.min(Math.max(this.a.height, this.a.c), i4);
        return this;
    }

    public final void a() {
        this.d.k.a(this.a);
    }
}
