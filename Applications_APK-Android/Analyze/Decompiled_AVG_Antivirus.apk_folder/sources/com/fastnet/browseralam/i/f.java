package com.fastnet.browseralam.i;

import android.app.AlertDialog;
import android.app.ProgressDialog;

final class f implements q {
    final /* synthetic */ ProgressDialog a;
    final /* synthetic */ AlertDialog b;
    private r c;

    f(ProgressDialog progressDialog, AlertDialog alertDialog) {
        this.a = progressDialog;
        this.b = alertDialog;
    }

    public final void a() {
        this.c.a();
    }

    public final void a(r rVar) {
        this.c = rVar;
    }

    public final void a(boolean z) {
        this.a.dismiss();
        if (!z) {
            this.b.show();
        }
    }
}
