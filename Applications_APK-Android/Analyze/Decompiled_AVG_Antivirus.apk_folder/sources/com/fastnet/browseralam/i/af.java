package com.fastnet.browseralam.i;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.webkit.WebView;
import com.fastnet.browseralam.b.g;
import java.net.URISyntaxException;
import java.util.List;
import java.util.regex.Pattern;

public final class af {
    static final Pattern a = Pattern.compile("(?i)((?:http|https|file):\\/\\/|(?:inline|data|about|javascript):|(?:.*:.*@))(.*)");
    private final Activity b;

    public af(g gVar) {
        this.b = gVar.B();
    }

    private boolean a(Intent intent) {
        List<ResolveInfo> queryIntentActivities = this.b.getPackageManager().queryIntentActivities(intent, 64);
        if (queryIntentActivities == null || queryIntentActivities.isEmpty()) {
            return false;
        }
        for (ResolveInfo resolveInfo : queryIntentActivities) {
            IntentFilter intentFilter = resolveInfo.filter;
            if (intentFilter != null && intentFilter.countDataAuthorities() != 0 && intentFilter.countDataPaths() != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean a(WebView webView, String str) {
        try {
            Intent parseUri = Intent.parseUri(str, 1);
            if (this.b.getPackageManager().resolveActivity(parseUri, 0) == null) {
                String str2 = parseUri.getPackage();
                if (str2 == null) {
                    return false;
                }
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:" + str2));
                intent.addCategory("android.intent.category.BROWSABLE");
                this.b.startActivity(intent);
                return true;
            }
            parseUri.addCategory("android.intent.category.BROWSABLE");
            parseUri.setComponent(null);
            if (webView != null) {
                parseUri.putExtra(this.b.getPackageName() + ".Origin", 1);
            }
            if (a.matcher(str).matches() && !a(parseUri)) {
                return false;
            }
            try {
                if (this.b.startActivityIfNeeded(parseUri, -1)) {
                    return true;
                }
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
            return false;
        } catch (URISyntaxException e2) {
            return false;
        }
    }
}
