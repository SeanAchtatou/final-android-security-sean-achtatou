package com.fastnet.browseralam.i;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.view.XWebView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.jvm.internal.CharCompanionObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public final class ag {
    private static String x = "<!DOCTYPE html><html><head><meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" /><meta name=\"viewport\" content=\"maximum-scale=2.0\"></head><body>";
    private static String y = "</body></html>";
    String a;
    StringBuilder b = new StringBuilder();
    URLConnection c;
    InputStream d;
    BufferedReader e;
    List f = new ArrayList();
    List g = new ArrayList();
    List h = new ArrayList();
    int i;
    int j;
    int k;
    int l = 40;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public WeakReference o;
    /* access modifiers changed from: private */
    public WeakReference p;
    /* access modifiers changed from: private */
    public Handler q;
    /* access modifiers changed from: private */
    public boolean r = true;
    /* access modifiers changed from: private */
    public boolean s;
    /* access modifiers changed from: private */
    public View.OnAttachStateChangeListener t = new al(this);
    /* access modifiers changed from: private */
    public Runnable u = new am(this);
    /* access modifiers changed from: private */
    public Runnable v = new an(this);
    /* access modifiers changed from: private */
    public Runnable w = new ao(this);

    public ag(Activity activity, String str, boolean z) {
        try {
            if (activity instanceof BrowserActivity) {
                this.o = new WeakReference((BrowserActivity) activity);
            }
            this.a = str;
            this.c = new URL(str).openConnection();
            this.c.setConnectTimeout(5000);
            this.c.setReadTimeout(5000);
            if (z) {
                this.c.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36");
            }
        } catch (Exception e2) {
            aq.a(activity, "Invalid Url");
            a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
      org.jsoup.nodes.Node.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element */
    static /* synthetic */ void a(ag agVar) {
        agVar.c.connect();
        agVar.d = agVar.c.getInputStream();
        agVar.e = new BufferedReader(new InputStreamReader(agVar.d));
        while (true) {
            String readLine = agVar.e.readLine();
            if (readLine == null) {
                agVar.d.close();
                agVar.e.close();
                if (agVar.r) {
                    ((BrowserActivity) agVar.o.get()).runOnUiThread(agVar.v);
                }
                Document parse = Jsoup.parse(agVar.b.toString());
                if (agVar.r) {
                    ((BrowserActivity) agVar.o.get()).runOnUiThread(agVar.v);
                    agVar.c();
                }
                Elements select = parse.select("a[href]");
                Elements select2 = parse.select("[src]");
                Elements select3 = parse.select("link[href]");
                Iterator it = select.iterator();
                while (it.hasNext()) {
                    Element element = (Element) it.next();
                    agVar.f.add(element.absUrl("href"));
                    element.attr("href", "¹");
                }
                Iterator it2 = select2.iterator();
                while (it2.hasNext()) {
                    Element element2 = (Element) it2.next();
                    agVar.g.add(element2.absUrl("href"));
                    element2.attr("href", "²");
                }
                Iterator it3 = select3.iterator();
                while (it3.hasNext()) {
                    Element element3 = (Element) it3.next();
                    agVar.h.add(element3.absUrl("href"));
                    element3.attr("href", "³");
                }
                parse.outputSettings().indentAmount(2);
                agVar.b = new StringBuilder(x);
                agVar.a(agVar.b, parse.html());
                agVar.b.append(y);
                return;
            } else if (!agVar.m) {
                agVar.b.append(readLine);
            } else {
                return;
            }
        }
    }

    private void a(StringBuilder sb, CharSequence charSequence) {
        char charAt;
        int length = charSequence.length();
        int i2 = length / 60;
        this.j = i2;
        this.k = i2;
        this.i = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (this.i < length) {
            if (this.m) {
                throw new IOException("cancelled");
            }
            char charAt2 = charSequence.charAt(this.i);
            if (charAt2 == 10) {
                sb.append("<br>");
            } else if (charAt2 == 185) {
                sb.append("<a href=\"" + ((String) this.f.get(i5)) + "\">" + ((String) this.f.get(i5)) + "</a>");
                i5++;
            } else if (charAt2 == 178) {
                sb.append("<a href=\"" + ((String) this.g.get(i4)) + "\">" + ((String) this.g.get(i4)) + "</a>");
                i4++;
            } else if (charAt2 == 179) {
                sb.append("<a href=\"" + ((String) this.h.get(i3)) + "\">" + ((String) this.h.get(i3)) + "</a>");
                i3++;
            } else if (charAt2 == 'h' && charSequence.charAt(this.i + 1) == 't' && charSequence.charAt(this.i + 2) == 't' && charSequence.charAt(this.i + 3) == 'p') {
                sb.append("<a href=\"");
                char charAt3 = charSequence.charAt(this.i - 1);
                int i6 = this.i;
                while (i6 < length) {
                    char charAt4 = charSequence.charAt(i6);
                    if (charAt4 == charAt3) {
                        break;
                    }
                    sb.append(charAt4);
                    i6++;
                }
                sb.append("\">");
                while (this.i < i6) {
                    sb.append(charSequence.charAt(this.i));
                    this.i++;
                }
                sb.append("</a>" + charAt3);
            } else if (charAt2 == '<') {
                sb.append("&lt;");
            } else if (charAt2 == '>') {
                sb.append("&gt;");
            } else if (charAt2 == '&') {
                sb.append("&amp;");
            } else if (charAt2 < 55296 || charAt2 > 57343) {
                if (charAt2 > '~' || charAt2 < ' ') {
                    sb.append("&#").append((int) charAt2).append(";");
                } else if (charAt2 == ' ') {
                    while (this.i + 1 < length && charSequence.charAt(this.i + 1) == ' ') {
                        sb.append("&nbsp;");
                        this.i++;
                    }
                    sb.append(' ');
                } else {
                    sb.append(charAt2);
                }
            } else if (charAt2 < 56320 && this.i + 1 < length && (charAt = charSequence.charAt(this.i + 1)) >= 56320 && charAt <= 57343) {
                this.i++;
                sb.append("&#").append(((charAt2 - 55296) << 10) | 65536 | (charAt - CharCompanionObject.MIN_LOW_SURROGATE)).append(";");
            }
            this.i++;
            i5 = i5;
            i4 = i4;
            i3 = i3;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.q.postDelayed(this.u, 50);
    }

    public final void a() {
        try {
            this.m = true;
            if (!this.n && this.c != null) {
                this.c.setConnectTimeout(0);
                this.c.setReadTimeout(0);
                if (this.d != null) {
                    this.d.close();
                }
                if (this.e != null) {
                    this.e.close();
                }
            }
        } catch (IOException e2) {
        }
    }

    public final void a(Activity activity, ap apVar) {
        new Thread(new ah(this, activity, apVar)).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.graphics.drawable.Drawable):android.graphics.drawable.Drawable
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, android.view.ViewGroup):android.view.ViewGroup
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, java.lang.String):java.lang.String
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.util.List, int):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.activity.BrowserActivity, boolean):boolean
      com.fastnet.browseralam.activity.BrowserActivity.a(int, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.activity.BrowserActivity.a(com.fastnet.browseralam.view.XWebView, boolean):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.b.g.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.fastnet.browseralam.b.g.a(java.lang.String, java.lang.String):void
      com.fastnet.browseralam.activity.BrowserActivity.a(java.lang.String, boolean):void */
    public final void b() {
        if (!this.m) {
            ((BrowserActivity) this.o.get()).a("");
            ((BrowserActivity) this.o.get()).a(10);
            ((BrowserActivity) this.o.get()).a("page-source:" + this.a, false);
            this.p = new WeakReference(((BrowserActivity) this.o.get()).D());
            ((XWebView) this.p.get()).w().addOnAttachStateChangeListener(this.t);
            this.q = new Handler();
            new Thread(new aj(this)).start();
        }
    }
}
