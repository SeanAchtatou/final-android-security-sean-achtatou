package com.fastnet.browseralam.i;

import android.view.View;

final class al implements View.OnAttachStateChangeListener {
    final /* synthetic */ ag a;

    al(ag agVar) {
        this.a = agVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.i.ag.a(com.fastnet.browseralam.i.ag, boolean):boolean
     arg types: [com.fastnet.browseralam.i.ag, int]
     candidates:
      com.fastnet.browseralam.i.ag.a(java.lang.StringBuilder, java.lang.CharSequence):void
      com.fastnet.browseralam.i.ag.a(android.app.Activity, com.fastnet.browseralam.i.ap):void
      com.fastnet.browseralam.i.ag.a(com.fastnet.browseralam.i.ag, boolean):boolean */
    public final void onViewAttachedToWindow(View view) {
        boolean unused = this.a.r = true;
        this.a.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fastnet.browseralam.i.ag.a(com.fastnet.browseralam.i.ag, boolean):boolean
     arg types: [com.fastnet.browseralam.i.ag, int]
     candidates:
      com.fastnet.browseralam.i.ag.a(java.lang.StringBuilder, java.lang.CharSequence):void
      com.fastnet.browseralam.i.ag.a(android.app.Activity, com.fastnet.browseralam.i.ap):void
      com.fastnet.browseralam.i.ag.a(com.fastnet.browseralam.i.ag, boolean):boolean */
    public final void onViewDetachedFromWindow(View view) {
        boolean unused = this.a.r = false;
        if (!this.a.s) {
            this.a.q.postDelayed(this.a.w, 50);
            this.a.q.removeCallbacks(this.a.u);
        }
    }
}
