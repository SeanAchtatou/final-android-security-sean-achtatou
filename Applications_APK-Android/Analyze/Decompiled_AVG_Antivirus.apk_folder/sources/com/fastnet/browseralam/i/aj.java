package com.fastnet.browseralam.i;

import com.fastnet.browseralam.activity.BrowserActivity;
import com.fastnet.browseralam.view.XWebView;

final class aj implements Runnable {
    final /* synthetic */ ag a;

    aj(ag agVar) {
        this.a = agVar;
    }

    public final void run() {
        try {
            ag.a(this.a);
            if (this.a.p.get() != null) {
                ((BrowserActivity) this.a.o.get()).runOnUiThread(new ak(this));
            }
            if (this.a.p.get() != null) {
                ((XWebView) this.a.p.get()).w().removeOnAttachStateChangeListener(this.a.t);
            }
            boolean unused = this.a.n = true;
        } catch (Exception e) {
            if (this.a.p.get() != null && ((XWebView) this.a.p.get()).w() != null) {
                ((XWebView) this.a.p.get()).w().removeOnAttachStateChangeListener(this.a.t);
            }
        }
    }
}
