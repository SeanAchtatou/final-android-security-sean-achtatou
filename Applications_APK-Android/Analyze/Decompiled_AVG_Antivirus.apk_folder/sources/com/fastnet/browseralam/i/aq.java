package com.fastnet.browseralam.i;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.TextView;
import android.widget.Toast;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.d.a;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class aq {
    private static TextView a;
    private static DisplayMetrics b;
    private static float c;
    private static Bitmap d;
    private static Bitmap e;

    static {
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        b = displayMetrics;
        c = displayMetrics.density;
    }

    public static float a(float f) {
        return (c * f) + 0.5f;
    }

    public static int a() {
        return b.widthPixels;
    }

    public static int a(int i) {
        return (int) ((((float) i) * c) + 0.5f);
    }

    public static Intent a(String str, String str2, String str3, String str4) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{str});
        intent.putExtra("android.intent.extra.TEXT", str3);
        intent.putExtra("android.intent.extra.SUBJECT", str2);
        intent.putExtra("android.intent.extra.CC", str4);
        intent.setType("message/rfc822");
        return intent;
    }

    public static Bitmap a(Resources resources, boolean z) {
        if (z) {
            if (e == null) {
                e = BitmapFactory.decodeResource(resources, R.drawable.ic_webpage_dark);
            }
            return e;
        }
        if (d == null) {
            d = BitmapFactory.decodeResource(resources, R.drawable.ic_webpage);
        }
        return d;
    }

    public static String a(String str) {
        int indexOf = str.indexOf(46, 8);
        if (indexOf == -1) {
            return str;
        }
        int lastIndexOf = str.lastIndexOf(47, indexOf) + 1;
        int indexOf2 = str.indexOf(47, lastIndexOf);
        if (indexOf2 == -1) {
            indexOf2 = str.length();
        }
        String substring = str.substring(lastIndexOf, indexOf2);
        return substring.startsWith("www.") ? substring.substring(4) : substring;
    }

    public static void a(Activity activity, String str, String str2, String str3) {
        URLUtil.guessFileName(str, null, null);
        a.a(activity, str, str2, str3, null);
    }

    public static void a(Context context) {
        try {
            File cacheDir = context.getCacheDir();
            if (cacheDir != null && cacheDir.isDirectory()) {
                a(cacheDir);
            }
        } catch (Exception e2) {
        }
    }

    public static void a(Context context, int i) {
        a.setText(context.getResources().getText(i));
        Toast toast = new Toast(context);
        toast.setDuration(0);
        toast.setView(a);
        toast.show();
    }

    public static void a(Context context, Handler handler, String str) {
        handler.postDelayed(new as(context, str), 300);
    }

    public static void a(Context context, String str) {
        if (a == null) {
            a(LayoutInflater.from(context));
        }
        a.setText(str);
        Toast toast = new Toast(context);
        toast.setDuration(0);
        toast.setView(a);
        toast.show();
    }

    public static void a(Context context, String str, String str2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(str);
        builder.setMessage(str2).setCancelable(true).setPositiveButton(context.getResources().getString(R.string.action_ok), new ar());
        builder.create().show();
    }

    public static void a(LayoutInflater layoutInflater) {
        a = (TextView) layoutInflater.inflate((int) R.layout.toast_layout, (ViewGroup) null);
    }

    public static boolean a(int i, int i2) {
        return (i & i2) == i2;
    }

    public static boolean a(File file) {
        if (file != null && file.isDirectory()) {
            for (String file2 : file.list()) {
                if (!a(new File(file, file2))) {
                    return false;
                }
            }
        }
        return file != null && file.delete();
    }

    public static int b() {
        return b.heightPixels;
    }

    public static String[] b(String str) {
        return str.split("\\|\\$\\|SEPARATOR\\|\\$\\|");
    }

    public static File c() {
        return File.createTempFile("JPEG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + "_", ".jpg", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));
    }
}
