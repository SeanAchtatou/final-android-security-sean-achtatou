package com.fastnet.browseralam.i;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import com.fastnet.browseralam.c.c;
import com.fastnet.browseralam.h.a;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

final class b implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ a b;

    b(a aVar, Context context) {
        this.b = aVar;
        this.a = context;
    }

    public final void run() {
        AssetManager assets = this.a.getAssets();
        try {
            HashSet a2 = a.a;
            c.a(this.a);
            a2.addAll(c.d());
            a.a();
            if (!a.Y()) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(assets.open("hosts.txt")));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        a.a.add(readLine);
                    } else {
                        bufferedReader.close();
                        return;
                    }
                }
            }
        } catch (IOException e) {
            Log.wtf("AdBlock", "Reading blocked domains list from file 'hosts.txt' failed.", e);
        }
    }
}
