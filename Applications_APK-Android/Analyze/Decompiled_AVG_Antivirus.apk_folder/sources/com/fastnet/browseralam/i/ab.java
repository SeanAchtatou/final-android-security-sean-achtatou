package com.fastnet.browseralam.i;

import android.os.Handler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.fastnet.browseralam.b.k;
import com.fastnet.browseralam.c.b;

final class ab extends WebViewClient {
    int a = 0;
    final /* synthetic */ WebView b;
    final /* synthetic */ aa c;
    /* access modifiers changed from: private */
    public Handler d = k.a();
    private Runnable e = new ac(this);

    ab(aa aaVar, WebView webView) {
        this.c = aaVar;
        this.b = webView;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (t.e != t.d) {
            this.a = 40;
            return;
        }
        this.a = 0;
        this.d.removeCallbacks(this.e);
        if (t.d >= this.c.b.size() - 1) {
            this.b.setWebViewClient(null);
            this.b.setWebChromeClient(null);
            this.b.stopLoading();
            this.b.onPause();
            this.b.destroyDrawingCache();
            this.b.destroy();
            return;
        }
        this.d.postDelayed(this.e, 200);
        t.f();
        while (t.d < this.c.b.size() && ((str = t.a(((b) this.c.b.get(t.d)).b())) == null || str.isEmpty())) {
            t.f();
            int unused = t.d = t.d;
        }
        this.b.loadUrl("http://" + str);
    }
}
