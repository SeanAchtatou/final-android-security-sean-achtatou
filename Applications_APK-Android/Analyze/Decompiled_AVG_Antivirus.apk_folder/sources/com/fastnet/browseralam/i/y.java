package com.fastnet.browseralam.i;

import android.graphics.Bitmap;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

final class y implements Runnable {
    final /* synthetic */ File a;
    final /* synthetic */ Bitmap b;

    y(File file, Bitmap bitmap) {
        this.a = file;
        this.b = bitmap;
    }

    public final void run() {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(this.a);
            this.b.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
