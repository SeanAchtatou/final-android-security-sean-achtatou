package com.fastnet.browseralam.i;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import java.io.File;

final class l extends BroadcastReceiver {
    final /* synthetic */ long a;
    final /* synthetic */ DownloadManager b;
    final /* synthetic */ k c;

    l(k kVar, long j, DownloadManager downloadManager) {
        this.c = kVar;
        this.a = j;
        this.b = downloadManager;
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getLongExtra("extra_download_id", -1) == this.a) {
            Bundle extras = intent.getExtras();
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(extras.getLong("extra_download_id"));
            Cursor query2 = this.b.query(query);
            if (!query2.moveToFirst()) {
                query2.close();
            } else if (query2.getInt(query2.getColumnIndex(NotificationCompat.CATEGORY_STATUS)) == 8) {
                String string = query2.getString(query2.getColumnIndex("local_filename"));
                query2.close();
                Intent intent2 = new Intent("android.intent.action.VIEW");
                intent2.setDataAndType(Uri.fromFile(new File(string)), "application/vnd.android.package-archive");
                intent2.setFlags(268435456);
                this.c.c.startActivity(intent2);
            }
        }
    }
}
