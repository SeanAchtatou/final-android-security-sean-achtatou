package com.fastnet.browseralam.i;

import android.app.Activity;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

final class n extends WebChromeClient {
    int[] a = this.c;
    int[] b = null;
    final /* synthetic */ int[] c;
    final /* synthetic */ q d;
    final /* synthetic */ Activity e;
    final /* synthetic */ WebView f;

    n(int[] iArr, q qVar, Activity activity, WebView webView) {
        this.c = iArr;
        this.d = qVar;
        this.e = activity;
        this.f = webView;
    }

    public final void onReceivedTitle(WebView webView, String str) {
        if (str.contains("not found")) {
            this.d.a();
            if (this.b != null) {
                this.d.a(true);
                c.a(this.e, "https://github.com/fusionxr/Speed-Browser/raw/master/builds/", c.d(this.b));
                return;
            }
            this.d.a(false);
            return;
        }
        this.b = this.a;
        this.a = c.c(this.a);
        this.f.loadUrl("https://github.com/fusionxr/Speed-Browser/blob/master/builds/" + c.d(this.a));
    }
}
