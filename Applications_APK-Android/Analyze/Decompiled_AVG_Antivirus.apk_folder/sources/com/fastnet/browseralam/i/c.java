package com.fastnet.browseralam.i;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import com.fastnet.browseralam.R;
import com.fastnet.browseralam.a.b;
import com.fastnet.browseralam.activity.BrowserActivity;
import com.jobstrak.drawingfun.lib.mopub.common.Constants;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

public final class c {
    /* access modifiers changed from: private */
    public static WeakReference a;
    /* access modifiers changed from: private */
    public static WeakReference b;
    /* access modifiers changed from: private */
    public static HttpURLConnection c = null;

    public c(BrowserActivity browserActivity) {
        b = new WeakReference(browserActivity);
        a = new WeakReference(browserActivity);
    }

    public static void a(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Sudah versi terbaru");
        builder.setPositiveButton("OK", new p());
        builder.setCancelable(true);
        AlertDialog create = builder.create();
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(0);
        progressDialog.setMessage("Checking for update...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        f fVar = new f(progressDialog, create);
        a(activity, fVar);
        progressDialog.setButton(-2, "CANCEL", new g(fVar));
        progressDialog.show();
    }

    public static void a(Activity activity, q qVar) {
        int[] g = g();
        String d = d(g);
        WebView webView = new WebView(activity);
        webView.setWebChromeClient(new n(g, qVar, activity, webView));
        webView.loadUrl("https://github.com/fusionxr/Speed-Browser/blob/master/builds/" + d);
        qVar.a(new o(webView));
    }

    static /* synthetic */ void a(Activity activity, String str, String str2) {
        View inflate = activity.getLayoutInflater().inflate((int) R.layout.update_dialog, (ViewGroup) null);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(inflate);
        AlertDialog create = builder.create();
        create.setCanceledOnTouchOutside(true);
        ((TextView) inflate.findViewById(R.id.edit_name)).setText("Speed Browser " + str2);
        inflate.findViewById(R.id.ok).setOnClickListener(new k(str, str2, activity, create));
        inflate.findViewById(R.id.cancel).setOnClickListener(new m(create));
        create.show();
    }

    public static void a(boolean z) {
        if (z) {
            try {
                if (a.get() == null) {
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://www.google.com").openConnection();
        c = httpURLConnection;
        httpURLConnection.setConnectTimeout(1000);
        c.setReadTimeout(1000);
        c.connect();
        InputStream inputStream = c.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        if (!z || a.get() != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(bufferedReader.readLine());
            sb.append(bufferedReader.readLine());
            sb.append(bufferedReader.readLine());
            inputStream.close();
            bufferedReader.close();
            int indexOf = sb.indexOf("/logos/doodles");
            if (indexOf == -1) {
                com.fastnet.browseralam.c.c.d(null);
                return;
            }
            String substring = sb.substring(sb.lastIndexOf("\"", indexOf) + 1, sb.indexOf("\"", indexOf));
            String str = (substring.startsWith(Constants.HTTP) || substring.startsWith("www")) ? substring : "http://www.google.com" + substring;
            b.a(str);
            b.a((File) null);
            com.fastnet.browseralam.c.c.d(str);
            if (!str.isEmpty() && z && a.get() != null) {
                ((BrowserActivity) a.get()).runOnUiThread(new i(str));
            }
        }
    }

    public static void b() {
        new Thread(new h()).start();
    }

    /* access modifiers changed from: private */
    public static int[] c(int[] iArr) {
        int i = iArr[0];
        int i2 = iArr[1];
        int i3 = iArr[2];
        if (i3 == 9) {
            i2++;
        }
        if (i2 == 10) {
            i++;
            i2 = 0;
        }
        return new int[]{i, i2, i3 + 1};
    }

    /* access modifiers changed from: private */
    public static String d(int... iArr) {
        String str = "";
        for (int i = 0; i < iArr.length; i++) {
            str = str + iArr[i] + ".";
        }
        return str + "apk";
    }

    private static int[] g() {
        int length = "5.7.5".length();
        int numericValue = Character.getNumericValue("5.7.5".charAt(length - 1));
        int numericValue2 = Character.getNumericValue("5.7.5".charAt(length - 3));
        int numericValue3 = Character.getNumericValue("5.7.5".charAt(length - 5));
        int i = numericValue + 1;
        if (i == 10) {
            numericValue2++;
            i = 0;
        }
        if (numericValue2 == 10) {
            numericValue3++;
            numericValue2 = 0;
        }
        return new int[]{numericValue3, numericValue2, i};
    }

    public final void a() {
        new Thread(new d(this)).start();
    }

    public final void c() {
        try {
            int[] g = g();
            int i = 0;
            int[] iArr = null;
            while (i < 10 && b.get() != null) {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("https://github.com/fusionxr/Speed-Browser/raw/master/builds/" + d(g)).openConnection();
                c = httpURLConnection;
                httpURLConnection.setConnectTimeout(500);
                c.setReadTimeout(500);
                c.setRequestMethod("HEAD");
                c.connect();
                if (c.getResponseCode() == 200) {
                    i++;
                    int[] iArr2 = g;
                    g = c(g);
                    iArr = iArr2;
                } else if (iArr != null && b.get() != null) {
                    ((Activity) b.get()).runOnUiThread(new j(this, iArr));
                    return;
                } else {
                    return;
                }
            }
        } catch (Exception e) {
        }
    }
}
