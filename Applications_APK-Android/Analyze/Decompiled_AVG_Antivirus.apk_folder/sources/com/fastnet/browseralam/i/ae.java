package com.fastnet.browseralam.i;

import android.graphics.Bitmap;
import android.webkit.WebView;
import com.fastnet.browseralam.c.b;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

final class ae implements Runnable {
    final Bitmap a = this.d;
    final int b = t.e;
    final String c = t.a(this.e.getUrl());
    final /* synthetic */ Bitmap d;
    final /* synthetic */ WebView e;
    final /* synthetic */ ad f;

    ae(ad adVar, Bitmap bitmap, WebView webView) {
        this.f = adVar;
        this.d = bitmap;
        this.e = webView;
    }

    public final void run() {
        try {
            if (this.c != null) {
                String str = this.c.hashCode() + ".png";
                ((b) this.f.a.b.get(this.b)).a(this.a);
                ((b) this.f.a.b.get(this.b)).c(str);
                FileOutputStream fileOutputStream = new FileOutputStream(new File(this.f.a.d + str));
                this.a.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        } catch (IOException e2) {
        }
    }
}
