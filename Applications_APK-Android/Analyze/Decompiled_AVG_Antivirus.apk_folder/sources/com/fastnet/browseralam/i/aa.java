package com.fastnet.browseralam.i;

import android.app.Activity;
import android.webkit.WebView;
import java.lang.ref.WeakReference;
import java.util.List;

final class aa implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ List b;
    final /* synthetic */ WeakReference c;
    final /* synthetic */ String d;

    aa(Activity activity, List list, WeakReference weakReference, String str) {
        this.a = activity;
        this.b = list;
        this.c = weakReference;
        this.d = str;
    }

    public final void run() {
        WebView webView = new WebView(this.a);
        webView.setWebViewClient(new ab(this, webView));
        webView.setWebChromeClient(new ad(this));
        webView.loadUrl("");
    }
}
