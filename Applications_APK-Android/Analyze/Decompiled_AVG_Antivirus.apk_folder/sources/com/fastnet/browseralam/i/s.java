package com.fastnet.browseralam.i;

import android.os.Environment;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;

public final class s implements Thread.UncaughtExceptionHandler {
    private static final String b = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/SpeedBrowser/");
    private Thread.UncaughtExceptionHandler a = Thread.getDefaultUncaughtExceptionHandler();

    public final void uncaughtException(Thread thread, Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        String obj = stringWriter.toString();
        printWriter.close();
        try {
            File file = new File(b);
            if (!file.exists()) {
                file.mkdir();
            }
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(b + "crashlog.txt"));
            bufferedWriter.write(obj);
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.a == null) {
            this.a = Thread.getDefaultUncaughtExceptionHandler();
        }
        if (this.a != null) {
            this.a.uncaughtException(thread, th);
        }
    }
}
