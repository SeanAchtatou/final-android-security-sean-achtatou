package com.fastnet.browseralam.i;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.view.View;

final class k implements View.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ String b;
    final /* synthetic */ Activity c;
    final /* synthetic */ AlertDialog d;

    k(String str, String str2, Activity activity, AlertDialog alertDialog) {
        this.a = str;
        this.b = str2;
        this.c = activity;
        this.d = alertDialog;
    }

    public final void onClick(View view) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(this.a + this.b));
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(1);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, this.b);
        DownloadManager downloadManager = (DownloadManager) this.c.getSystemService("download");
        long enqueue = downloadManager.enqueue(request);
        this.d.dismiss();
        IntentFilter intentFilter = new IntentFilter("android.intent.action.DOWNLOAD_COMPLETE");
        this.c.registerReceiver(new l(this, enqueue, downloadManager), intentFilter);
    }
}
