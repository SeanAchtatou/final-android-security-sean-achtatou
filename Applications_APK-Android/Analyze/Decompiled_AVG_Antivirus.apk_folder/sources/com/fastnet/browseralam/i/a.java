package com.fastnet.browseralam.i;

import android.app.Activity;
import android.content.Context;
import java.util.HashSet;
import java.util.Locale;

public final class a {
    /* access modifiers changed from: private */
    public static final HashSet a = new HashSet();
    private static final Locale c = Locale.getDefault();
    private static a d;
    private boolean b = com.fastnet.browseralam.h.a.c();

    private a(Context context) {
        com.fastnet.browseralam.h.a.a();
        if (this.b && a.size() == 0) {
            b(context);
        }
    }

    public static a a(Context context) {
        if (d == null) {
            d = new a(context);
        }
        return d;
    }

    public static void a(String str) {
        a.add(str);
    }

    private void b(Context context) {
        new Thread(new b(this, context)).start();
    }

    public static void b(String str) {
        a.remove(str);
    }

    public final void a(Activity activity) {
        com.fastnet.browseralam.h.a.a();
        this.b = com.fastnet.browseralam.h.a.c();
        if (this.b && a.size() == 0) {
            b(activity);
        }
    }

    public final boolean c(String str) {
        if (!this.b || str == null) {
            return false;
        }
        int i = str.charAt(5) == '/' ? 7 : 8;
        int indexOf = str.indexOf(47, 8);
        if (indexOf == -1) {
            indexOf = str.length();
        }
        String substring = str.substring(i, indexOf);
        if (substring.startsWith("www.")) {
            substring = substring.substring(4);
        }
        return a.contains(substring);
    }
}
