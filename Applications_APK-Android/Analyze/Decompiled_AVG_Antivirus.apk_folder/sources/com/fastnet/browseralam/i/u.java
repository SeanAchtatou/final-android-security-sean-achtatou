package com.fastnet.browseralam.i;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.g.j;
import java.io.File;
import java.util.List;

final class u implements Runnable {
    final /* synthetic */ List a;
    final /* synthetic */ Bitmap b;
    final /* synthetic */ String c;
    final /* synthetic */ Handler d;
    final /* synthetic */ j e;

    u(List list, Bitmap bitmap, String str, Handler handler, j jVar) {
        this.a = list;
        this.b = bitmap;
        this.c = str;
        this.d = handler;
        this.e = jVar;
    }

    public final void run() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.a.size() && !t.b) {
                b bVar = (b) this.a.get(i2);
                if (!bVar.c()) {
                    if (bVar.h().isEmpty()) {
                        bVar.a(this.b);
                    } else {
                        bVar.a(i2);
                        File file = new File(this.c + bVar.h());
                        bVar.a(!file.exists() ? this.b : BitmapFactory.decodeFile(file.getPath(), options));
                        this.d.post(new v(this, bVar));
                    }
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
