package com.fastnet.browseralam.i;

import android.app.Activity;
import com.fastnet.browseralam.c.b;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

final class z implements Runnable {
    final /* synthetic */ List a;
    final /* synthetic */ String b;
    final /* synthetic */ Activity c;

    z(List list, String str, Activity activity) {
        this.a = list;
        this.b = str;
        this.c = activity;
    }

    public final void run() {
        ArrayList arrayList = new ArrayList();
        for (b bVar : this.a) {
            if (bVar.c()) {
                t.b(this.b, arrayList, bVar.f());
            } else if (!new File(this.b + bVar.h()).exists()) {
                arrayList.add(bVar);
            }
        }
        if (arrayList.size() != 0) {
            this.c.runOnUiThread(new aa(this.c, arrayList, new WeakReference(this.c), this.b));
        }
    }
}
