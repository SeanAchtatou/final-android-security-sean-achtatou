package com.fastnet.browseralam.i;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Handler;
import com.fastnet.browseralam.b.l;
import com.fastnet.browseralam.c.b;
import com.fastnet.browseralam.g.j;
import java.io.File;
import java.util.List;

public final class t {
    /* access modifiers changed from: private */
    public static boolean a;
    /* access modifiers changed from: private */
    public static boolean b;
    /* access modifiers changed from: private */
    public static boolean c;
    /* access modifiers changed from: private */
    public static int d = -1;
    /* access modifiers changed from: private */
    public static int e = -1;

    static /* synthetic */ String a(String str) {
        int indexOf = str.indexOf(46);
        if (indexOf == -1) {
            return null;
        }
        int lastIndexOf = str.lastIndexOf(47, indexOf);
        int i = lastIndexOf == -1 ? 0 : lastIndexOf + 1;
        int indexOf2 = str.indexOf(47, i);
        if (indexOf2 == -1) {
            indexOf2 = str.length();
        }
        String substring = str.substring(i, indexOf2);
        return substring.startsWith("www.") ? substring.substring(4) : substring;
    }

    public static void a(Activity activity, String str, List list) {
        new Thread(new z(list, str, activity)).start();
    }

    public static void a(File file, Bitmap bitmap) {
        l.a(new y(file, bitmap));
    }

    public static synchronized void a(List list, String str, Bitmap bitmap, j jVar, Handler handler) {
        synchronized (t.class) {
            if (a) {
                a = false;
                b = false;
                c = true;
                l.a(new u(list, bitmap, str, handler, jVar));
            } else {
                a = true;
                c = false;
                b = true;
                l.a(new w(list, bitmap, str, handler, jVar));
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(String str, List list, List list2) {
        int i = 1;
        while (true) {
            int i2 = i;
            if (i2 < list2.size()) {
                b bVar = (b) list2.get(i2);
                if (bVar.c()) {
                    b(str, list, bVar.f());
                } else if (!new File(str + bVar.h()).exists()) {
                    list.add(bVar);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    static /* synthetic */ int f() {
        int i = d + 1;
        d = i;
        return i;
    }
}
