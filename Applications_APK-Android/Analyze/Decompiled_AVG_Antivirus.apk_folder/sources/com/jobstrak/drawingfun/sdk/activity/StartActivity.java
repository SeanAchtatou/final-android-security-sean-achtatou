package com.jobstrak.drawingfun.sdk.activity;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;

public class StartActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PermissionsActivity.start(this);
    }
}
