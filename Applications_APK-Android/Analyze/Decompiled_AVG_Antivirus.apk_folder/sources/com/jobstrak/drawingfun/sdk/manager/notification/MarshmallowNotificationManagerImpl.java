package com.jobstrak.drawingfun.sdk.manager.notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.core.view.MotionEventCompat;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;
import java.util.Random;
import java.util.UUID;

@TargetApi(MotionEventCompat.AXIS_BRAKE)
public class MarshmallowNotificationManagerImpl extends BaseNotificationManagerImpl {
    public MarshmallowNotificationManagerImpl(@NonNull CryopiggyManager cryopiggyManager, @NonNull Random random) {
        super(cryopiggyManager);
    }

    public Notification buildNotification(@NonNull Context context, @NonNull NotificationAd notificationAd) {
        String channelId = UUID.randomUUID().toString();
        if (Build.VERSION.SDK_INT >= 26) {
            ((NotificationManager) context.getSystemService(NotificationManager.class)).createNotificationChannel(new NotificationChannel(channelId, "￸", 4));
        }
        Notification.Builder builder = new Notification.Builder(context).setAutoCancel(true).setGroup(channelId).setDefaults(-1).setContentText(notificationAd.getText()).setContentTitle(notificationAd.getTitle()).setLargeIcon(notificationAd.getLargeIcon()).setPriority(2).setContentIntent(getBrowserPendingIntent(context, notificationAd.getUrl()));
        if (Build.VERSION.SDK_INT >= 26) {
            builder.setChannelId(channelId);
        }
        Bitmap smallIcon = notificationAd.getSmallIcon();
        if (smallIcon != null) {
            builder.setSmallIcon(Icon.createWithBitmap(smallIcon));
        } else {
            builder.setSmallIcon(Icon.createWithBitmap(notificationAd.getLargeIcon()));
        }
        Bitmap image = notificationAd.getImage();
        if (image != null) {
            builder.setStyle(new Notification.BigPictureStyle().bigPicture(image));
        }
        return builder.build();
    }
}
