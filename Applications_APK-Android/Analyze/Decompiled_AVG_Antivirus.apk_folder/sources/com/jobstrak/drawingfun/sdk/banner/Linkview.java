package com.jobstrak.drawingfun.sdk.banner;

import android.app.Activity;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.banner.Banner;
import com.jobstrak.drawingfun.sdk.model.HTMLAd;
import java.util.Map;

public class Linkview extends Html {
    public Linkview(@NonNull Activity activity, @NonNull Banner.Callback callback, @NonNull Map<String, String> properties) {
        super(activity, callback, properties, HTMLAd.Type.LINKVIEW);
    }
}
