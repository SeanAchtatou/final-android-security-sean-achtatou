package com.jobstrak.drawingfun.sdk.service.validator.device;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.NetworkUtils;

public class NetworkStateValidator extends Validator {
    @NonNull
    private final Config config;

    public NetworkStateValidator(@NonNull Config config2) {
        this.config = config2;
    }

    public boolean validate(long currentTime) {
        return NetworkUtils.isConnected(this.config.isOnlyFastConnection());
    }

    public String getReason() {
        return "no internet connection or it's slow";
    }
}
