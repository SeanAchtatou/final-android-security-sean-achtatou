package com.jobstrak.drawingfun.sdk.service.validator.testage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.model.TestageItem;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageValidator;

public class TestageAdPerDayLimitValidator implements TestageValidator {
    @NonNull
    private final Settings settings;

    public TestageAdPerDayLimitValidator(@NonNull Settings settings2) {
        this.settings = settings2;
    }

    public TestageValidator.Result validate(long currentTime, @NonNull TestageItem item) {
        final int limit = item.getCount();
        return new TestageValidator.Result(limit <= 0 || limit > this.settings.getCurrentTestageAdCount(item.getId())) {
            public String reason() {
                return String.format("daily limit exceeded (%d)", Integer.valueOf(limit));
            }
        };
    }
}
