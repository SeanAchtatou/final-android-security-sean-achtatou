package com.jobstrak.drawingfun.sdk.service.validator.lockscreen;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class LockscreenAdDelayStateValidator extends Validator {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;

    public LockscreenAdDelayStateValidator(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        int delay = this.config.getLockscreenAdDelay();
        return delay <= 0 || this.settings.getCurrentLockscreenLockCount() % delay == 0;
    }

    public String getReason() {
        int delay = this.config.getLockscreenAdDelay();
        return String.format("lockscreen ad delayed (%s/%s)", Integer.valueOf(this.settings.getCurrentLockscreenLockCount() % delay), Integer.valueOf(delay));
    }
}
