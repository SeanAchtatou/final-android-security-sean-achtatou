package com.jobstrak.drawingfun.sdk.manager.backstage;

import androidx.annotation.NonNull;

public interface BackstageManager {
    void openUrl(@NonNull String str);
}
