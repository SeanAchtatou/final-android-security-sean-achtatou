package com.jobstrak.drawingfun.sdk.manager.testage;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.activity.TestageActivity;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.manager.main.NotInitializedException;
import com.jobstrak.drawingfun.sdk.model.TestageExecItem;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class TestageManagerImpl implements TestageManager {
    @NonNull
    private final CryopiggyManager cryopiggyManager;

    public TestageManagerImpl(@NonNull CryopiggyManager cryopiggyManager2) {
        this.cryopiggyManager = cryopiggyManager2;
    }

    public void execute(@NonNull TestageExecItem item) {
        try {
            LogUtils.debug("Executing testage: %s", item);
            Context context = this.cryopiggyManager.getContext();
            context.startActivity(new Intent(context, TestageActivity.class).putExtra(TestageActivity.EXTRA_ITEM, item).addFlags(268435456));
        } catch (NotInitializedException e) {
            LogUtils.error(e);
        }
    }
}
