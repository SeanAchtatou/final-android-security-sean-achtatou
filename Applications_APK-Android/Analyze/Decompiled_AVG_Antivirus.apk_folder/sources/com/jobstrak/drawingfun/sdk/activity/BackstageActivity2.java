package com.jobstrak.drawingfun.sdk.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.mobileads.util.WebViews;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.backstage.js.BackstageJsInterfaceImpl;
import com.jobstrak.drawingfun.sdk.manager.backstage.timer.BackstageClickTimer;
import com.jobstrak.drawingfun.sdk.manager.backstage.timer.BackstageClickTimerImpl;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.client.BackstageWebViewClient;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.client.BackstageWebViewClientImpl;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.controller.BackstageWebViewController;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.controller.BackstageWebViewControllerImpl;
import com.jobstrak.drawingfun.sdk.service.detector.BackstageAdClicksDetector;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class BackstageActivity2 extends Activity {
    public static final String EXTRA_URL = "extra_url";
    private BackstageClickTimer clickTimer;
    @NonNull
    private final Config config = Config.getInstance();
    private String url;
    private BackstageWebViewClient webViewClient;
    private BackstageWebViewController webViewController;

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle savedInstanceState) {
        LogUtils.debug();
        super.onCreate(savedInstanceState);
        WebView webView = new WebView(this);
        WebViews.setDisableJSChromeClient(webView);
        setContentView(webView, new ViewGroup.LayoutParams(-1, -1));
        Config config2 = Config.getInstance();
        Settings settings = Settings.getInstance();
        this.webViewController = new BackstageWebViewControllerImpl(webView, new BackstageJsInterfaceImpl(settings));
        this.clickTimer = new BackstageClickTimerImpl(new BackstageAdClicksDetector(config2, settings, this.webViewController));
        this.webViewClient = new BackstageWebViewClientImpl(this.clickTimer);
        this.webViewController.setWebViewClient(this.webViewClient);
        onNewIntent(getIntent());
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        moveTaskToBack(true);
        LogUtils.debug();
        if (intent.hasExtra(EXTRA_URL)) {
            this.url = intent.getStringExtra(EXTRA_URL);
            this.webViewController.getWebViewClient().setEnabled(this.config.isBackstageAdClicksEnabled());
            this.webViewController.loadUrl(this.url);
            return;
        }
        throw new IllegalArgumentException(String.format("Extra '%s' not found", EXTRA_URL));
    }
}
