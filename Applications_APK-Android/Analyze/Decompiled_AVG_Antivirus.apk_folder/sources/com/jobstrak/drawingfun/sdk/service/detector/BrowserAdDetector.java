package com.jobstrak.drawingfun.sdk.service.detector;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.browser.BrowserManager;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.validator.CustomTabShowingStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.service.validator.browser.BrowserAdEnableStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.browser.BrowserAdPauseStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.browser.BrowserAdPerDayLimitValidator;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.ad.ShowLinkAdTask;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class BrowserAdDetector extends Detector {
    @NonNull
    private final BrowserManager browserManager;
    @NonNull
    private final TaskFactory<ShowLinkAdTask> showBrowserAdTaskFactory;
    private final Validator[] validators;

    public BrowserAdDetector(@NonNull Config config, @NonNull Settings settings, @NonNull BrowserManager browserManager2, @NonNull TaskFactory<ShowLinkAdTask> showBrowserAdTaskFactory2) {
        super(config, settings);
        this.validators = new Validator[]{new BrowserAdEnableStateValidator(), new BrowserAdPerDayLimitValidator(), new BrowserAdPauseStateValidator(settings), new CustomTabShowingStateValidator()};
        this.browserManager = browserManager2;
        this.showBrowserAdTaskFactory = showBrowserAdTaskFactory2;
    }

    public void tick(Detector.Delegate delegate) {
    }

    public boolean detect(Detector.Delegate delegate) {
        Object obj;
        long currentTime = delegate.getCurrentTime();
        String foregroundPackage = delegate.getForegroundPackage();
        if (!this.browserManager.isBrowser(foregroundPackage)) {
            LogUtils.debug("App %s is not a browser", foregroundPackage);
            return false;
        }
        LogUtils.debug("Browser %s detected", foregroundPackage);
        for (Validator validator : this.validators) {
            if (!validator.validate(currentTime)) {
                LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validator.getReason());
                return true;
            }
        }
        int browserAdCount = getConfig().getBrowserAdCount();
        int currentBrowserAdCount = getSettings().getCurrentBrowserAdCount() + 1;
        Object[] objArr = new Object[2];
        objArr[0] = Integer.valueOf(currentBrowserAdCount);
        if (browserAdCount == 0) {
            obj = "inf";
        } else {
            obj = Integer.valueOf(browserAdCount);
        }
        objArr[1] = obj;
        LogUtils.debug("Show browser ad (%s/%s)", objArr);
        getSettings().setNextLinkAdTime(getConfig().getBrowserAdDelay() + currentTime);
        getSettings().setCurrentBrowserAdCount(currentBrowserAdCount);
        getSettings().save();
        this.showBrowserAdTaskFactory.create().execute(foregroundPackage);
        return true;
    }
}
