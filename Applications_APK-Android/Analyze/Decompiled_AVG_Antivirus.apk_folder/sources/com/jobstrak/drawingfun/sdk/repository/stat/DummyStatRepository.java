package com.jobstrak.drawingfun.sdk.repository.stat;

import com.jobstrak.drawingfun.sdk.model.Stat;
import java.util.List;

public class DummyStatRepository implements StatRepository {
    public void addEvent(String type) {
    }

    public void addStatsFromArray(List<Stat> list) {
    }
}
