package com.jobstrak.drawingfun.sdk.service.validator.testage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.TestageItem;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageValidator;
import com.jobstrak.drawingfun.sdk.utils.StringUtils;

public class TestageAdUrlStateValidator implements TestageValidator {
    public TestageValidator.Result validate(long currentTime, @NonNull TestageItem item) {
        return new TestageValidator.Result(!StringUtils.isEmpty(item.getUrl())) {
            public String reason() {
                return "url is empty";
            }
        };
    }
}
