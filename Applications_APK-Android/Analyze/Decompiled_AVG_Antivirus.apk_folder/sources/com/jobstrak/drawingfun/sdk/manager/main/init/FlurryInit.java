package com.jobstrak.drawingfun.sdk.manager.main.init;

import android.content.Context;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.flurry.FlurryManager;

public class FlurryInit extends Init {
    @NonNull
    private final Context context;
    @NonNull
    private final String flurryKey;
    @NonNull
    private final FlurryManager flurryManager;

    public FlurryInit(@NonNull Context context2, @NonNull FlurryManager flurryManager2, @NonNull String flurryKey2) {
        this.context = context2;
        this.flurryKey = flurryKey2;
        this.flurryManager = flurryManager2;
    }

    /* access modifiers changed from: protected */
    public void doExecute() {
        this.flurryManager.init(this.context, this.flurryKey);
    }
}
