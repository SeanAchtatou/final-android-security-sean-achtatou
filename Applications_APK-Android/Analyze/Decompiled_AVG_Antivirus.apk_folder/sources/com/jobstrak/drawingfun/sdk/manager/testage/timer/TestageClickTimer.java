package com.jobstrak.drawingfun.sdk.manager.testage.timer;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.activity.TestageActivity;
import com.jobstrak.drawingfun.sdk.manager.backstage.timer.BackstageClickTimer;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.controller.BackstageWebViewController;
import com.jobstrak.drawingfun.sdk.model.TestageExecItem;
import com.jobstrak.drawingfun.sdk.model.TestageItem;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class TestageClickTimer implements BackstageClickTimer {
    private static final int MAX_TIMER_DELAY = 16000;
    private static final int MIN_TIMER_DELAY = 8000;
    @NonNull
    private static final Random RANDOM = new Random();
    /* access modifiers changed from: private */
    @NonNull
    public final Handler handler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    @NonNull
    public final TestageExecItem item;
    /* access modifiers changed from: private */
    @NonNull
    public TestageActivity testageActivity;
    private Timer timer;
    /* access modifiers changed from: private */
    @NonNull
    public final BackstageWebViewController webViewController;

    public TestageClickTimer(@NonNull TestageActivity testageActivity2, @NonNull BackstageWebViewController webViewController2, @NonNull TestageExecItem item2) {
        this.testageActivity = testageActivity2;
        this.webViewController = webViewController2;
        this.item = item2;
    }

    public void startTimer() {
        LogUtils.debug();
        Timer timer2 = this.timer;
        if (timer2 != null) {
            timer2.cancel();
        }
        this.timer = new Timer();
        int delay = randDelay();
        this.timer.schedule(new TimerTask() {
            public void run() {
                TestageClickTimer.this.handler.post(new Runnable() {
                    public void run() {
                        if (!TextUtils.isEmpty(TestageClickTimer.this.item.getCheckboxAction())) {
                            String checkboxAction = TestageClickTimer.this.item.getCheckboxAction();
                            char c = 65535;
                            int hashCode = checkboxAction.hashCode();
                            if (hashCode != -292418033) {
                                if (hashCode == 94627080 && checkboxAction.equals(TestageItem.CHECKBOX_ACTION_CHECK)) {
                                    c = 0;
                                }
                            } else if (checkboxAction.equals(TestageItem.CHECKBOX_ACTION_UNCHECK)) {
                                c = 1;
                            }
                            if (c == 0) {
                                LogUtils.debug("Checking checkbox...", new Object[0]);
                                TestageClickTimer.this.webViewController.setChecked(TestageClickTimer.this.item.getCheckboxAttrName(), TestageClickTimer.this.item.getCheckboxAttrValue(), true);
                            } else if (c == 1) {
                                LogUtils.debug("Unchecking checkbox...", new Object[0]);
                                TestageClickTimer.this.webViewController.setChecked(TestageClickTimer.this.item.getCheckboxAttrName(), TestageClickTimer.this.item.getCheckboxAttrValue(), false);
                            }
                        }
                        if (!TextUtils.isEmpty(TestageClickTimer.this.item.getAttrName())) {
                            LogUtils.debug("Clicking...", new Object[0]);
                            TestageClickTimer.this.webViewController.clickOnElement(TestageClickTimer.this.item.getAttrName(), TestageClickTimer.this.item.getAttrValue(), TestageClickTimer.this.item.getNum());
                            return;
                        }
                        LogUtils.debug("Finishing testage activity...", new Object[0]);
                        TestageClickTimer.this.testageActivity.finish();
                        TestageActivity unused = TestageClickTimer.this.testageActivity = null;
                    }
                });
            }
        }, (long) delay);
        LogUtils.debug("Started with delay of %dms", Integer.valueOf(delay));
    }

    public void stopTimer() {
        LogUtils.debug();
        Timer timer2 = this.timer;
        if (timer2 != null) {
            timer2.cancel();
            this.timer = null;
        }
    }

    private static int randDelay() {
        return rand(MIN_TIMER_DELAY, MAX_TIMER_DELAY);
    }

    private static int rand(int min, int max) {
        return RANDOM.nextInt((max - min) + 1) + min;
    }
}
