package com.jobstrak.drawingfun.sdk.banner;

import android.app.Activity;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import com.google.android.gms.common.internal.ImagesContract;
import com.jobstrak.drawingfun.lib.mopub.common.DataKeys;
import com.jobstrak.drawingfun.lib.mopub.mobileads.CustomEventInterstitial;
import com.jobstrak.drawingfun.lib.mopub.mobileads.MoPubErrorCode;
import com.jobstrak.drawingfun.lib.mopub.mraid.MraidInterstitial;
import com.jobstrak.drawingfun.lib.okhttp3.OkHttpClient;
import com.jobstrak.drawingfun.lib.okhttp3.Request;
import com.jobstrak.drawingfun.sdk.banner.Banner;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Mraidview extends Banner implements CustomEventInterstitial.CustomEventInterstitialListener {
    /* access modifiers changed from: private */
    public final MraidInterstitial interstitial = new MraidInterstitial();
    /* access modifiers changed from: private */
    public final Map<String, Object> localExtras = new HashMap();
    /* access modifiers changed from: private */
    public final Map<String, String> serverExtras = new HashMap();

    public Mraidview(@NonNull Activity activity, @NonNull Banner.Callback callback, @NonNull Map<String, String> properties) {
        super(activity, callback, properties);
        this.localExtras.put(DataKeys.BROADCAST_IDENTIFIER_KEY, 1234L);
    }

    public void load() {
        LogUtils.debug();
        fireOnRequest();
        new AsyncTask<String, Void, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... urls) {
                LogUtils.debug("Retrieving mraid ad...", new Object[0]);
                try {
                    return new OkHttpClient.Builder().build().newCall(new Request.Builder().url(urls[0]).get().build()).execute().body().string();
                } catch (IOException e) {
                    LogUtils.error("An error occurred while retrieving mraid ad", e, new Object[0]);
                    return null;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String htmlResponseBody) {
                super.onPostExecute((Object) htmlResponseBody);
                if (htmlResponseBody != null) {
                    Mraidview.this.serverExtras.put(DataKeys.HTML_RESPONSE_BODY_KEY, htmlResponseBody);
                    MraidInterstitial access$200 = Mraidview.this.interstitial;
                    Activity activity = Mraidview.this.getActivity();
                    Mraidview mraidview = Mraidview.this;
                    access$200.loadInterstitial(activity, mraidview, mraidview.localExtras, Mraidview.this.serverExtras);
                    return;
                }
                LogUtils.debug("htmlResponseBody == null", new Object[0]);
                Mraidview.this.fireOnFail();
            }
        }.execute(getProperties().get(ImagesContract.URL));
    }

    public void show() {
        LogUtils.debug();
        this.interstitial.showInterstitial();
    }

    public void onInterstitialLoaded() {
        LogUtils.debug();
        fireOnLoad();
    }

    public void onInterstitialFailed(MoPubErrorCode errorCode) {
        LogUtils.debug(errorCode.toString(), new Object[0]);
        fireOnFail();
    }

    public void onInterstitialShown() {
        LogUtils.debug();
        fireOnShow();
    }

    public void onInterstitialClicked() {
        LogUtils.debug();
        fireOnClick();
    }

    public void onLeaveApplication() {
        LogUtils.debug();
        fireOnDismiss();
    }

    public void onInterstitialDismissed() {
        LogUtils.debug();
        fireOnDismiss();
    }
}
