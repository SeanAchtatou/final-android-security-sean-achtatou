package com.jobstrak.drawingfun.sdk.repository.system;

import android.content.Context;
import android.util.SparseArray;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.processes.models.AndroidAppProcess;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class LollipopMR1SystemRepository implements SystemRepository {
    private static final int MIN_UID = 10000;

    @NonNull
    public String getForegroundPackageName(@NonNull Context context) {
        SparseArray<AndroidAppProcess> foregroundApps = getRunningForegroundApps(context.getPackageManager());
        logAll(foregroundApps);
        return foregroundApps.size() > 0 ? foregroundApps.valueAt(0).getPackageName() : "";
    }

    private void logAll(SparseArray<AndroidAppProcess> foregroundApps) {
        LogUtils.verbose("+", new Object[0]);
        int s = foregroundApps.size();
        for (int i = 0; i < s; i++) {
            int oomScore = foregroundApps.keyAt(i);
            AndroidAppProcess process = foregroundApps.get(oomScore);
            LogUtils.verbose("pkg=%s oomScore=%d pid=%d uid=%d", process.getPackageName(), Integer.valueOf(oomScore), Integer.valueOf(process.pid), Integer.valueOf(process.uid));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004b, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        com.jobstrak.drawingfun.sdk.utils.LogUtils.error("Error occurred while reading from /proc/%d/%s", r9, java.lang.Integer.valueOf(r6), "oom_score");
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005f A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:5:0x001f] */
    @androidx.annotation.NonNull
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.util.SparseArray<com.jobstrak.drawingfun.lib.processes.models.AndroidAppProcess> getRunningForegroundApps(@androidx.annotation.NonNull android.content.pm.PackageManager r13) {
        /*
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/proc"
            r0.<init>(r1)
            java.io.File[] r0 = r0.listFiles()
            android.util.SparseArray r1 = new android.util.SparseArray
            r1.<init>()
            int r2 = r0.length
            r3 = 0
            r4 = 0
        L_0x0013:
            if (r4 >= r2) goto L_0x0072
            r5 = r0[r4]
            boolean r6 = r5.isDirectory()
            if (r6 == 0) goto L_0x006f
            r6 = 0
            r7 = 1
            java.lang.String r8 = r5.getName()     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            int r8 = java.lang.Integer.parseInt(r8)     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            r6 = r8
            com.jobstrak.drawingfun.lib.processes.models.AndroidAppProcess r8 = new com.jobstrak.drawingfun.lib.processes.models.AndroidAppProcess     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            r8.<init>(r6)     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            boolean r9 = r8.foreground     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            if (r9 == 0) goto L_0x006f
            int r9 = r8.uid     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            r10 = 10000(0x2710, float:1.4013E-41)
            if (r9 < r10) goto L_0x006f
            java.lang.String r9 = r8.getPackageName()     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            android.content.Intent r9 = r13.getLaunchIntentForPackage(r9)     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            if (r9 == 0) goto L_0x006f
            int r9 = r8.oom_score()     // Catch:{ IOException -> 0x004b, Exception -> 0x005f }
            if (r9 <= r7) goto L_0x004a
            r1.put(r9, r8)     // Catch:{ IOException -> 0x004b, Exception -> 0x005f }
        L_0x004a:
            goto L_0x006f
        L_0x004b:
            r9 = move-exception
            java.lang.String r10 = "Error occurred while reading from /proc/%d/%s"
            r11 = 2
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r6)     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            r11[r3] = r12     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            java.lang.String r12 = "oom_score"
            r11[r7] = r12     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            com.jobstrak.drawingfun.sdk.utils.LogUtils.error(r10, r9, r11)     // Catch:{ IOException -> 0x0061, Exception -> 0x005f }
            goto L_0x006f
        L_0x005f:
            r7 = move-exception
            goto L_0x006f
        L_0x0061:
            r8 = move-exception
            java.lang.Object[] r7 = new java.lang.Object[r7]
            java.lang.Integer r9 = java.lang.Integer.valueOf(r6)
            r7[r3] = r9
            java.lang.String r9 = "Error occurred while reading from /proc/%d"
            com.jobstrak.drawingfun.sdk.utils.LogUtils.error(r9, r8, r7)
        L_0x006f:
            int r4 = r4 + 1
            goto L_0x0013
        L_0x0072:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.sdk.repository.system.LollipopMR1SystemRepository.getRunningForegroundApps(android.content.pm.PackageManager):android.util.SparseArray");
    }
}
