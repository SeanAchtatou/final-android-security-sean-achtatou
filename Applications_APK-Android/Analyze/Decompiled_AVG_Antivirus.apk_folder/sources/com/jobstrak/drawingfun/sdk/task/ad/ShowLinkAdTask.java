package com.jobstrak.drawingfun.sdk.task.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.data.Stats;
import com.jobstrak.drawingfun.sdk.manager.browser.BrowserManager;
import com.jobstrak.drawingfun.sdk.model.LinkAd;
import com.jobstrak.drawingfun.sdk.model.Stat;
import com.jobstrak.drawingfun.sdk.repository.ad.AdRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class ShowLinkAdTask extends BaseTask<LinkAd> {
    @NonNull
    private final AdRepository adRepository;
    @NonNull
    private final BrowserManager browserManager;
    @Nullable
    private String browserPackageName;
    @NonNull
    private final Stats stats;
    @NonNull
    private final LinkAd.Type type;

    private ShowLinkAdTask(@NonNull AdRepository adRepository2, @NonNull BrowserManager browserManager2, @NonNull LinkAd.Type type2, @NonNull Stats stats2) {
        this.type = type2;
        this.stats = stats2;
        this.adRepository = adRepository2;
        this.browserManager = browserManager2;
    }

    public void execute(@Nullable String browserPackageName2) {
        this.browserPackageName = browserPackageName2;
        super.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public LinkAd doInBackground() {
        try {
            LogUtils.debug("Retrieving LINK ad...", new Object[0]);
            Stats stats2 = this.stats;
            stats2.add(new Stat("link_" + this.type.toString(), Stat.TYPE_REQUEST));
            return this.adRepository.getLinkAd(this.type);
        } catch (Exception e) {
            LogUtils.error("Error occurred while retrieving LINK ad", e, new Object[0]);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(LinkAd linkAd) {
        super.onPostExecute((Object) linkAd);
        if (linkAd != null) {
            if (this.browserPackageName != null) {
                this.browserManager.openUrlInBrowser(linkAd.getUrl(), this.browserPackageName);
            } else {
                this.browserManager.openUrlInBrowser(linkAd.getUrl());
            }
            Stats stats2 = this.stats;
            stats2.add(new Stat("link_" + this.type.toString(), Stat.TYPE_IMPRESSION));
            return;
        }
        LogUtils.debug("linkAd == null", new Object[0]);
    }

    public static class Factory implements TaskFactory<ShowLinkAdTask> {
        @NonNull
        private final AdRepository adRepository;
        @NonNull
        private final BrowserManager browserManager;
        @NonNull
        private final Stats stats;
        @NonNull
        private final LinkAd.Type type;

        public Factory(@NonNull AdRepository adRepository2, @NonNull BrowserManager browserManager2, @NonNull LinkAd.Type type2, @NonNull Stats stats2) {
            this.type = type2;
            this.stats = stats2;
            this.adRepository = adRepository2;
            this.browserManager = browserManager2;
        }

        @NonNull
        public ShowLinkAdTask create() {
            return new ShowLinkAdTask(this.adRepository, this.browserManager, this.type, this.stats);
        }
    }
}
