package com.jobstrak.drawingfun.sdk.service.validator.browser;

import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BrowserAdEnableStateValidator extends Validator {
    public boolean validate(long currentTime) {
        return Config.getInstance().isBrowserAdEnabled();
    }

    public String getReason() {
        return "browser ad is disabled";
    }
}
