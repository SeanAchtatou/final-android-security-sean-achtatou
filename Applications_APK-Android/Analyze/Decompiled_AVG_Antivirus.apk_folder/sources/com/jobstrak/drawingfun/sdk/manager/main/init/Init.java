package com.jobstrak.drawingfun.sdk.manager.main.init;

import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public abstract class Init {
    /* access modifiers changed from: protected */
    public abstract void doExecute() throws InitException;

    public void execute() throws InitException {
        LogUtils.debug("Executing " + getClass().getSimpleName() + "...", new Object[0]);
        doExecute();
    }

    public void abandon() {
        if (isAbandonable()) {
            LogUtils.debug("Abandoning " + getClass().getSimpleName() + "...", new Object[0]);
            ((Abandonable) this).doAbandon();
            return;
        }
        LogUtils.error(String.format("%s is not %s", getClass().getSimpleName(), Abandonable.class.getSimpleName()), new Object[0]);
    }

    public boolean isAbandonable() {
        return this instanceof Abandonable;
    }
}
