package com.jobstrak.drawingfun.sdk.banner;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.DataKeys;
import com.jobstrak.drawingfun.lib.mopub.mobileads.CustomEventInterstitial;
import com.jobstrak.drawingfun.lib.mopub.mobileads.MoPubErrorCode;
import com.jobstrak.drawingfun.lib.mopub.mraid.MraidInterstitial;
import com.jobstrak.drawingfun.sdk.activity.WebActivity;
import com.jobstrak.drawingfun.sdk.banner.Banner;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.manager.browser.BrowserManager;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.model.HTMLAd;
import com.jobstrak.drawingfun.sdk.repository.RepositoryFactory;
import com.jobstrak.drawingfun.sdk.repository.ad.AdRepository;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.ad.ShowHTMLBannerTask;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Html extends Banner implements WebActivity.HTMLBannerListener, CustomEventInterstitial.CustomEventInterstitialListener {
    @Nullable
    private HTMLAd ad;
    private final AdRepository adRepository;
    private HTMLAd.Type adType;
    private final AndroidManager androidManager;
    private final BrowserManager browserManager;
    private final Config config;
    private final MraidInterstitial interstitial;
    private final Map<String, Object> localExtras;
    private final RequestManager requestManager;
    private final Map<String, String> serverExtras;
    private final Settings settings;
    private final UrlManager urlManager;

    public Html(@NonNull Activity activity, @NonNull Banner.Callback callback, @NonNull Map<String, String> properties) {
        this(activity, callback, properties, HTMLAd.Type.HTML);
    }

    public Html(@NonNull Activity activity, @NonNull Banner.Callback callback, @NonNull Map<String, String> properties, @NonNull HTMLAd.Type adType2) {
        super(activity, callback, properties);
        this.config = Config.getInstance();
        this.settings = Settings.getInstance();
        this.androidManager = ManagerFactory.getAndroidManager();
        this.browserManager = ManagerFactory.getBrowserManager();
        this.requestManager = ManagerFactory.createRequestManager();
        this.urlManager = ManagerFactory.createUrlManager(this.config, this.settings);
        this.adRepository = RepositoryFactory.getAdRepository(this.urlManager, this.requestManager);
        this.adType = adType2;
        this.interstitial = new MraidInterstitial();
        this.serverExtras = new HashMap();
        this.localExtras = new HashMap();
        this.localExtras.put(DataKeys.BROADCAST_IDENTIFIER_KEY, 1234L);
    }

    public void load() {
        LogUtils.debug();
        createShowHtmlBannerTaskFactory().create().execute(this.adType, this);
        fireOnRequest();
    }

    public void show() {
        LogUtils.debug();
        HTMLAd hTMLAd = this.ad;
        if (hTMLAd != null) {
            String type = hTMLAd.getType();
            char c = 65535;
            int hashCode = type.hashCode();
            if (hashCode != 104156535) {
                if (hashCode != 150940456) {
                    if (hashCode == 1224424441 && type.equals(HTMLAd.TYPE_WEBVIEW)) {
                        c = 0;
                    }
                } else if (type.equals(HTMLAd.TYPE_BROWSER)) {
                    c = 1;
                }
            } else if (type.equals(HTMLAd.TYPE_MRAID)) {
                c = 2;
            }
            if (c == 0) {
                showBannerInWebview();
            } else if (c == 1) {
                showBannerInBrowser();
            } else if (c != 2) {
                LogUtils.error("Unknown html ad type: %s", this.ad.getType());
            } else {
                showMraidInWebview();
            }
        } else {
            LogUtils.error("ad == null", new Object[0]);
            fireOnFail();
        }
    }

    public void onShow() {
        LogUtils.debug();
        fireOnShow();
    }

    public void onClick() {
        LogUtils.debug();
        fireOnClick();
    }

    public void onFail() {
        LogUtils.debug();
        fireOnFail();
    }

    public void onDismiss() {
        LogUtils.debug();
        fireOnDismiss();
    }

    public void onLoad(HTMLAd ad2) {
        LogUtils.debug();
        this.ad = ad2;
        fireOnLoad();
    }

    public void onInterstitialLoaded() {
        LogUtils.debug();
        this.interstitial.showInterstitial();
    }

    public void onInterstitialFailed(MoPubErrorCode errorCode) {
        LogUtils.debug(errorCode.toString(), new Object[0]);
        fireOnFail();
    }

    public void onInterstitialShown() {
        LogUtils.debug();
        fireOnShow();
    }

    public void onInterstitialClicked() {
        LogUtils.debug();
        fireOnClick();
    }

    public void onLeaveApplication() {
        LogUtils.debug();
        fireOnDismiss();
    }

    public void onInterstitialDismissed() {
        LogUtils.debug();
        fireOnDismiss();
    }

    private void showBannerInWebview() {
        LogUtils.debug();
        String listenerUuid = UUID.randomUUID().toString();
        WebActivity.registerListener(listenerUuid, this);
        Bundle extras = new Bundle();
        extras.putSerializable(WebActivity.EXTRA_BANNER, this.ad);
        extras.putString(WebActivity.EXTRA_LISTENER_UUID, listenerUuid);
        this.androidManager.startActivity(WebActivity.class, extras);
    }

    private void showBannerInBrowser() {
        LogUtils.debug();
        postponeLinkAd();
        this.browserManager.openUrlInBrowser(this.ad.getUrl());
        fireOnShow();
        fireOnDismiss();
    }

    private void showMraidInWebview() {
        LogUtils.debug();
        this.serverExtras.put(DataKeys.HTML_RESPONSE_BODY_KEY, this.ad.getHtmlData());
        this.interstitial.loadInterstitial(getActivity(), this, this.localExtras, this.serverExtras);
    }

    private void postponeLinkAd() {
        this.settings.setNextLinkAdTime(TimeUtils.getCurrentTime() + this.config.getBrowserAdDelay());
        this.settings.save();
    }

    private TaskFactory<ShowHTMLBannerTask> createShowHtmlBannerTaskFactory() {
        return new ShowHTMLBannerTask.Factory(this.adRepository, this.androidManager);
    }
}
