package com.jobstrak.drawingfun.sdk.repository.stat;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.Stat;
import java.util.List;

public interface StatRepository {
    void addEvent(@NonNull String str) throws Exception;

    void addStatsFromArray(@NonNull List<Stat> list) throws Exception;
}
