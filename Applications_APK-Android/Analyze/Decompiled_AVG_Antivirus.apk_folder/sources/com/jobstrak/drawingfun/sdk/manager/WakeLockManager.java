package com.jobstrak.drawingfun.sdk.manager;

import android.content.Context;
import android.os.PowerManager;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tJ\u0006\u0010\n\u001a\u00020\u0007R\u0014\u0010\u0003\u001a\b\u0018\u00010\u0004R\u00020\u0005X\u000e¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/manager/WakeLockManager;", "", "()V", "wakeLock", "Landroid/os/PowerManager$WakeLock;", "Landroid/os/PowerManager;", "acquire", "", "context", "Landroid/content/Context;", "release", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: WakeLockManager.kt */
public final class WakeLockManager {
    public static final WakeLockManager INSTANCE = new WakeLockManager();
    private static PowerManager.WakeLock wakeLock;

    private WakeLockManager() {
    }

    public final void acquire(@NotNull Context context) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        if (wakeLock != null) {
            LogUtils.debug("Acquiring wakelock...", new Object[0]);
            Object systemService = context.getSystemService("power");
            if (systemService != null) {
                PowerManager.WakeLock $this$apply = ((PowerManager) systemService).newWakeLock(1, "app:wakee");
                $this$apply.acquire();
                wakeLock = $this$apply;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.os.PowerManager");
        }
    }

    public final void release() {
        PowerManager.WakeLock wakeLock2 = wakeLock;
        if (wakeLock2 != null) {
            LogUtils.debug("Releasing wakelock...", new Object[0]);
            wakeLock2.release();
            wakeLock = null;
        }
    }
}
