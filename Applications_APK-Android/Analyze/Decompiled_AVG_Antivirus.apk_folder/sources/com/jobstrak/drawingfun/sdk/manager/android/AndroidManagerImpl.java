package com.jobstrak.drawingfun.sdk.manager.android;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.BackupService;
import com.jobstrak.drawingfun.sdk.data.meta.MetaData;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.manager.main.NotInitializedException;
import com.jobstrak.drawingfun.sdk.manager.worker.WorkerManager;
import com.jobstrak.drawingfun.sdk.receiver.ScreenReceiver;
import com.jobstrak.drawingfun.sdk.repository.system.SystemRepository;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.SystemUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.List;

public class AndroidManagerImpl implements AndroidManager {
    private static final Class<? extends Service> SERVICE_CLASS = BackupService.class;
    @NonNull
    private final CryopiggyManager cryopiggyManager;
    @NonNull
    private final SystemRepository systemRepository;
    @NonNull
    private final WorkerManager workerManager;

    public AndroidManagerImpl(@NonNull CryopiggyManager cryopiggyManager2, @NonNull SystemRepository systemRepository2, @NonNull WorkerManager workerManager2) {
        this.cryopiggyManager = cryopiggyManager2;
        this.systemRepository = systemRepository2;
        this.workerManager = workerManager2;
    }

    public void startActivity(@NonNull Intent intent) throws NotInitializedException {
        this.cryopiggyManager.getContext().startActivity(intent);
    }

    public void startActivity(@NonNull Class<? extends Activity> activityClass) {
        startActivity(activityClass, null);
    }

    public void startActivity(@NonNull Class<? extends Activity> activityClass, @Nullable Bundle extras) {
        try {
            LogUtils.debug("Starting %s activity...", activityClass.getSimpleName());
            Intent intent = new Intent(this.cryopiggyManager.getContext(), activityClass).addFlags(268435456);
            if (extras != null) {
                intent.putExtras(extras);
            }
            startActivity(intent);
        } catch (NotInitializedException e) {
            LogUtils.error("Can't start %s activity", e, activityClass.getSimpleName());
        }
    }

    public void startService() {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            LogUtils.debug("Starting service...", new Object[0]);
            Intent intent = new Intent(context, SERVICE_CLASS).setAction(BackupService.ACT_START);
            int i = Build.VERSION.SDK_INT;
            context.startService(intent);
            return;
        }
        LogUtils.error("context == null", new Object[0]);
    }

    public void stopService() {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            LogUtils.debug("Stopping service...", new Object[0]);
            context.startService(new Intent(context, SERVICE_CLASS).setAction(BackupService.ACT_STOP));
            return;
        }
        LogUtils.error("context == null", new Object[0]);
    }

    public void startScreenReceiver() {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            LogUtils.debug("Starting screen receiver...", new Object[0]);
            ScreenReceiver.init(context);
            return;
        }
        LogUtils.error("context == null", new Object[0]);
    }

    public void stopScreenReceiver() {
        LogUtils.debug("Stopping screen receiver...", new Object[0]);
    }

    public void createOverlayView() {
        Context context = this.cryopiggyManager.optContext();
        if (context == null) {
            LogUtils.error("context == null", new Object[0]);
        } else if (hasPermission("android.permission.SYSTEM_ALERT_WINDOW")) {
            LogUtils.debug();
            context.startService(new Intent(context, SERVICE_CLASS).setAction(BackupService.ACT_CREATE_OVERLAY_VIEW));
        } else {
            LogUtils.warning("Can't create overlay view due to lack of %s", "android.permission.SYSTEM_ALERT_WINDOW");
        }
    }

    public void removeOverlayView() {
        Context context = this.cryopiggyManager.optContext();
        if (context == null) {
            LogUtils.error("context == null", new Object[0]);
        } else if (hasPermission("android.permission.SYSTEM_ALERT_WINDOW")) {
            LogUtils.debug();
            context.startService(new Intent(context, SERVICE_CLASS).setAction(BackupService.ACT_REMOVE_OVERLAY_VIEW));
        } else {
            LogUtils.warning("Can't remove overlay view due to lack of %s", "android.permission.SYSTEM_ALERT_WINDOW");
        }
    }

    public List<ResolveInfo> queryIntentActivities(Intent intent) throws NotInitializedException {
        return this.cryopiggyManager.getContext().getPackageManager().queryIntentActivities(intent, 131072);
    }

    @Nullable
    public ResolveInfo resolveActivity(Intent intent) throws NotInitializedException {
        return this.cryopiggyManager.getContext().getPackageManager().resolveActivity(intent, 131072);
    }

    @NonNull
    public String getForegroundPackageName() {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            return this.systemRepository.getForegroundPackageName(context);
        }
        LogUtils.error("context == null", new Object[0]);
        return "";
    }

    @NonNull
    public String getPackageName() {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            return context.getPackageName();
        }
        LogUtils.error("context == null", new Object[0]);
        return "";
    }

    @NonNull
    public String getInstallerPackageName() {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            return context.getPackageManager().getInstallerPackageName(context.getPackageName());
        }
        LogUtils.error("context == null", new Object[0]);
        return "";
    }

    @Nullable
    public MetaData readMetaData() {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            return MetaData.read(context);
        }
        LogUtils.error("context == null", new Object[0]);
        return null;
    }

    @NonNull
    public Point getScreenSize() {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            return new Point(metrics.widthPixels, metrics.heightPixels);
        }
        LogUtils.error("context == null", new Object[0]);
        return new Point(0, 0);
    }

    @NonNull
    public String readFileFromAssets(@NonNull String filePath) {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            BufferedReader in = null;
            try {
                StringBuilder buf = new StringBuilder();
                BufferedReader in2 = new BufferedReader(new InputStreamReader(context.getAssets().open(filePath), "UTF-8"));
                while (true) {
                    String readLine = in2.readLine();
                    String str = readLine;
                    if (readLine == null) {
                        break;
                    }
                    buf.append(str);
                }
                String sb = buf.toString();
                try {
                    in2.close();
                } catch (IOException e) {
                    LogUtils.error(e);
                }
                return sb;
            } catch (IOException e2) {
                LogUtils.error(e2);
                if (in == null) {
                    return "";
                }
                try {
                    in.close();
                    return "";
                } catch (IOException e3) {
                    LogUtils.error(e3);
                    return "";
                }
            } catch (Throwable th) {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e4) {
                        LogUtils.error(e4);
                    }
                }
                throw th;
            }
        } else {
            LogUtils.error("context == null", new Object[0]);
            return "";
        }
    }

    @NonNull
    public String getImei() {
        Context context = this.cryopiggyManager.optContext();
        if (context == null) {
            LogUtils.error("context == null", new Object[0]);
            return "";
        } else if (hasPermission("android.permission.READ_PHONE_STATE")) {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } else {
            LogUtils.warning("Can't get IMEI due to lack of %s", "android.permission.READ_PHONE_STATE");
            return "";
        }
    }

    public boolean hasPermission(@NonNull String permission) {
        if (this.cryopiggyManager.optContext() == null) {
            LogUtils.error("context == null", new Object[0]);
            return false;
        } else if (checkSelfPermission(permission) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isTablet() {
        Context context = this.cryopiggyManager.optContext();
        if (context == null) {
            LogUtils.error("context == null", new Object[0]);
            return false;
        } else if (((TelephonyManager) context.getSystemService("phone")).getPhoneType() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public void setLauncherIconVisibility(boolean visible) {
        setLauncherIconVisibility(visible, false);
    }

    public void setLauncherIconVisibility(boolean visible, boolean killApp) {
        Context context = this.cryopiggyManager.optContext();
        int i = 0;
        if (context != null) {
            try {
                LogUtils.debug("Setting launcher icon visibility to %s...", Boolean.valueOf(visible));
                PackageManager packageManager = context.getPackageManager();
                Intent launchIntent = SystemUtils.getLaunchIntentForPackage(packageManager, context.getPackageName());
                if (launchIntent != null) {
                    ComponentName componentName = new ComponentName(context, launchIntent.getComponent().getClassName());
                    int newState = visible ? 1 : 2;
                    Method method = PackageManager.class.getMethod("setComponentEnabledSetting", ComponentName.class, Integer.TYPE, Integer.TYPE);
                    Object[] objArr = new Object[3];
                    objArr[0] = componentName;
                    objArr[1] = Integer.valueOf(newState);
                    if (!killApp) {
                        i = 1;
                    }
                    objArr[2] = Integer.valueOf(i);
                    method.invoke(packageManager, objArr);
                    return;
                }
                LogUtils.wtf("No launch intent for the package", new Object[0]);
            } catch (Exception e) {
                LogUtils.error(e);
            }
        } else {
            LogUtils.error("context == null", new Object[0]);
        }
    }

    private int checkSelfPermission(@NonNull String permission) {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            return context.checkPermission(permission, Process.myPid(), Process.myUid());
        }
        LogUtils.error("context == null", new Object[0]);
        return -1;
    }
}
