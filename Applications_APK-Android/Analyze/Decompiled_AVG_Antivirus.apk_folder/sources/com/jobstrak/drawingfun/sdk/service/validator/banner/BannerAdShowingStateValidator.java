package com.jobstrak.drawingfun.sdk.service.validator.banner;

import com.jobstrak.drawingfun.sdk.activity.BaseActivity;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BannerAdShowingStateValidator extends Validator {
    public boolean validate(long currentTime) {
        return !BaseActivity.isShowing();
    }

    public String getReason() {
        return "banner is already showing";
    }
}
