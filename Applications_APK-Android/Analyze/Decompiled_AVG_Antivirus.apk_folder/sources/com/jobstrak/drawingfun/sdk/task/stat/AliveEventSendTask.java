package com.jobstrak.drawingfun.sdk.task.stat;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.repository.stat.StatRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class AliveEventSendTask extends BaseTask<Void> {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;
    @NonNull
    private final StatRepository statRepository;

    private AliveEventSendTask(@NonNull Config config2, @NonNull Settings settings2, @NonNull StatRepository statRepository2) {
        this.config = config2;
        this.settings = settings2;
        this.statRepository = statRepository2;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground() {
        try {
            LogUtils.debug("Sending alive event...", new Object[0]);
            this.statRepository.addEvent("alive");
            this.settings.setNextAliveEventSendTime(TimeUtils.getCurrentTime() + this.config.getAliveDelay());
            this.settings.save();
            return null;
        } catch (Exception e) {
            LogUtils.error("Error occurred while sending alive event", e, new Object[0]);
            return null;
        }
    }

    public static class Factory implements TaskFactory<AliveEventSendTask> {
        @NonNull
        private final Config config;
        @NonNull
        private final Settings settings;
        @NonNull
        private final StatRepository statRepository;

        public Factory(@NonNull Config config2, @NonNull Settings settings2, @NonNull StatRepository statRepository2) {
            this.config = config2;
            this.settings = settings2;
            this.statRepository = statRepository2;
        }

        @NonNull
        public AliveEventSendTask create() {
            return new AliveEventSendTask(this.config, this.settings, this.statRepository);
        }
    }
}
