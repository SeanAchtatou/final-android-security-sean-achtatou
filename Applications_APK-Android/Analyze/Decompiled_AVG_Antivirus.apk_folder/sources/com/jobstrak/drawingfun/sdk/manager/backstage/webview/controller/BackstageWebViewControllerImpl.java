package com.jobstrak.drawingfun.sdk.manager.backstage.webview.controller;

import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.manager.backstage.js.BackstageJsInterface;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.client.BackstageWebViewClient;
import com.jobstrak.drawingfun.sdk.utils.SecureLogUtils;

public class BackstageWebViewControllerImpl implements BackstageWebViewController {
    private static final String JS_INTERFACE_NAME = "Backstage";
    @NonNull
    private final WebView webView;
    private BackstageWebViewClient webViewClient;

    public BackstageWebViewControllerImpl(@NonNull WebView webView2, @NonNull BackstageJsInterface backstageJsInterface) {
        this.webView = webView2;
        this.webView.getSettings().setUseWideViewPort(true);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setLoadWithOverviewMode(true);
        this.webView.addJavascriptInterface(backstageJsInterface, JS_INTERFACE_NAME);
    }

    @Nullable
    public BackstageWebViewClient getWebViewClient() {
        return this.webViewClient;
    }

    public void setWebViewClient(@Nullable BackstageWebViewClient client) {
        SecureLogUtils.debug();
        this.webViewClient = client;
        this.webView.setWebViewClient(client);
    }

    public void loadUrl(@NonNull String url) {
        SecureLogUtils.debug();
        this.webView.loadUrl(url);
    }

    public void clickOnLongestLink() {
        SecureLogUtils.debug();
        this.webView.loadUrl("javascript:(function(){ var link;\nvar documents = [document];\n\nvar iframes = document.getElementsByTagName('iframe');\nfor (var i = 0; i < iframes.length; i++) {\n    documents.push(iframes[i].contentDocument);\n}\n\nfor (var i = 0; i < documents.length; i++) {\n    var links = documents[i].getElementsByTagName('a'); \n    for (var j = 0; j < links.length; j++) { \n        if (!link || links[j].href.length > link.href.length) { \n            link = links[j];    \n        } \n    }  \n}\n\nif (link) { \n    var clickEvent = document.createEvent('HTMLEvents');  \n    clickEvent.initEvent('click', true, true);  \n    link.dispatchEvent(clickEvent);  \n    Backstage.onSuccess();\n} else { \n    Backstage.onError();\n} })()");
    }

    public void clickOnElement(@NonNull String elementId) {
        SecureLogUtils.debug(elementId, new Object[0]);
        WebView webView2 = this.webView;
        webView2.loadUrl("javascript:(function(){ \n    var link = document.getElementById('" + elementId + "');\n" + "    if (link) { \n" + "        var clickEvent = document.createEvent('HTMLEvents');\n" + "        clickEvent.initEvent('click', true, true);\n" + "        link.dispatchEvent(clickEvent);\n" + "    " + JS_INTERFACE_NAME + ".onSuccess();\n" + "} else { \n" + "    " + JS_INTERFACE_NAME + ".onError();\n" + "}" + "})()");
    }

    public void clickOnElement(@NonNull String elementAttrName, @NonNull String elementAttrValue) {
        clickOnElement(elementAttrName, elementAttrValue, 0);
    }

    public void clickOnElement(@NonNull String elementAttrName, @NonNull String elementAttrValue, int elementNum) {
        SecureLogUtils.debug("%s %s %d", elementAttrName, elementAttrValue, Integer.valueOf(elementNum));
        WebView webView2 = this.webView;
        webView2.loadUrl("javascript:(function(){ \n    var link = document.querySelectorAll('[" + elementAttrName + "=\"" + elementAttrValue + "\"]')[" + elementNum + "];\n" + "    if (link) { \n" + "        var clickEvent = document.createEvent('HTMLEvents');\n" + "        clickEvent.initEvent('click', true, true);\n" + "        link.dispatchEvent(clickEvent);\n" + "    " + JS_INTERFACE_NAME + ".onSuccess();\n" + "} else { \n" + "    " + JS_INTERFACE_NAME + ".onError();\n" + "}" + "})()");
    }

    public void setChecked(@NonNull String elementAttrName, @NonNull String elementAttrValue, boolean checked) {
        SecureLogUtils.debug("%s %s %s", elementAttrName, elementAttrValue, Boolean.valueOf(checked));
        WebView webView2 = this.webView;
        webView2.loadUrl("javascript:(function(){ \n    document.querySelector('[" + elementAttrName + "=\"" + elementAttrValue + "\"]').checked = " + checked + ";\n" + "})()");
    }
}
