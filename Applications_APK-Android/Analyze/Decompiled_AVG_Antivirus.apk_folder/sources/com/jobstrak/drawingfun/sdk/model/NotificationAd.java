package com.jobstrak.drawingfun.sdk.model;

import android.graphics.Bitmap;
import com.google.android.gms.common.internal.ImagesContract;
import com.jobstrak.drawingfun.lib.gson.annotations.SerializedName;

public class NotificationAd {
    public static final String BANNER_ID = "notification";
    private transient int id;
    private Bitmap image;
    @SerializedName("img_image")
    String imageSrc;
    private Bitmap largeIcon;
    @SerializedName("img_big")
    String largeIconSrc;
    private Bitmap smallIcon;
    @SerializedName("img_small")
    String smallIconSrc;
    @SerializedName("text")
    String text;
    @SerializedName("title")
    String title;
    @SerializedName(ImagesContract.URL)
    String url;

    public NotificationAd() {
    }

    public NotificationAd(String text2, String title2, String url2, String smallIconSrc2, String largeIconSrc2) {
        this(text2, title2, url2, smallIconSrc2, largeIconSrc2, null);
    }

    public NotificationAd(String text2, String title2, String url2, String smallIconSrc2, String largeIconSrc2, String imageSrc2) {
        this.text = text2;
        this.title = title2;
        this.url = url2;
        this.smallIconSrc = smallIconSrc2;
        this.largeIconSrc = largeIconSrc2;
        this.imageSrc = imageSrc2;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getUrl() {
        return this.url;
    }

    public String getText() {
        return this.text;
    }

    public String getTitle() {
        return this.title;
    }

    public String getLargeIconSrc() {
        return this.largeIconSrc;
    }

    public String getSmallIconSrc() {
        return this.smallIconSrc;
    }

    public Bitmap getLargeIcon() {
        return this.largeIcon;
    }

    public void setLargeIcon(Bitmap largeIcon2) {
        this.largeIcon = largeIcon2;
    }

    public Bitmap getSmallIcon() {
        return this.smallIcon;
    }

    public void setSmallIcon(Bitmap smallIcon2) {
        this.smallIcon = smallIcon2;
    }

    public String getImageSrc() {
        return this.imageSrc;
    }

    public Bitmap getImage() {
        return this.image;
    }

    public void setImage(Bitmap image2) {
        this.image = image2;
    }
}
