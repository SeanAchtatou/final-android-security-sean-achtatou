package com.jobstrak.drawingfun.sdk.task.ad;

import android.text.TextUtils;
import android.util.SparseIntArray;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.gson.JsonSyntaxException;
import com.jobstrak.drawingfun.lib.gson.annotations.SerializedName;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.data.Stats;
import com.jobstrak.drawingfun.sdk.manager.download.DownloadManager;
import com.jobstrak.drawingfun.sdk.manager.notification.NotificationManager;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;
import com.jobstrak.drawingfun.sdk.model.NotificationAdWaterfallItem;
import com.jobstrak.drawingfun.sdk.model.NotificationAdWaterfallOrderType;
import com.jobstrak.drawingfun.sdk.model.Stat;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.JsonUtils;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.SystemUtils;
import com.jobstrak.drawingfun.sdk.waterfall.Waterfall;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.jetbrains.annotations.Nullable;

public class ShowNotificationAdTask2 extends BaseTask<NotificationAd> {
    @NonNull
    private final Config config;
    @NonNull
    private final DownloadManager downloadManager;
    @NonNull
    private final NotificationManager notificationManager;
    @NonNull
    private final RequestManager requestManager;
    /* access modifiers changed from: private */
    @NonNull
    public final Settings settings;
    @NonNull
    private final Stats stats;

    public ShowNotificationAdTask2(@NonNull Stats stats2, @NonNull Config config2, @NonNull Settings settings2, @NonNull RequestManager requestManager2, @NonNull DownloadManager downloadManager2, @NonNull NotificationManager notificationManager2) {
        this.stats = stats2;
        this.config = config2;
        this.settings = settings2;
        this.requestManager = requestManager2;
        this.downloadManager = downloadManager2;
        this.notificationManager = notificationManager2;
    }

    /* access modifiers changed from: protected */
    public NotificationAd doInBackground() {
        Item item;
        final NotificationAdWaterfallItem[] waterfallItems = this.config.getNotificationAdWaterfall();
        Integer num = null;
        int i = 0;
        if (waterfallItems.length == 0) {
            LogUtils.debug("Notification ad waterfall is empty", new Object[0]);
            return null;
        }
        LogUtils.debug("Calculating next notification ad waterfall position...", new Object[0]);
        Waterfall.Type type = this.config.getNotificationAdWaterfallOrderType() == NotificationAdWaterfallOrderType.FLAT ? Waterfall.Type.FLAT : Waterfall.Type.ROUNDROBIN;
        Waterfall.Adapter adapter = new Waterfall.Adapter() {
            private final SparseIntArray positionById = new SparseIntArray();

            {
                for (int i = 0; i < waterfallItems.length; i++) {
                    this.positionById.put(getId(i), i);
                }
            }

            public int getId(int position) {
                return waterfallItems[position].getUrl().hashCode();
            }

            public int getImpressionsLimit(int position) {
                return waterfallItems[position].getDailyLimit();
            }

            @Nullable
            public Integer getPosition(int id) {
                int position = this.positionById.get(id, -1);
                if (position >= 0) {
                    return Integer.valueOf(position);
                }
                return null;
            }

            public int getItemsCount() {
                return waterfallItems.length;
            }
        };
        Waterfall waterfall = Waterfall.generate(type, adapter, new Waterfall.Gateway() {
            public int getImpressionsCount(int id) {
                return ShowNotificationAdTask2.this.settings.getNotificationAdCount(id);
            }

            @Nullable
            public Integer getLastRequestedItemId() {
                return ShowNotificationAdTask2.this.settings.getLastNotificationAdRequestedItemId();
            }
        });
        LogUtils.debug("Available items count in notification ad's waterfall: " + waterfall.getPositionsCount(), new Object[0]);
        String ip = "";
        if (waterfall.hasNextPosition()) {
            try {
                LogUtils.debug("Checking ip address...", new Object[0]);
                ip = ((String) this.requestManager.sendRequest("http://checkip.amazonaws.com/", String.class)).trim();
            } catch (Exception e) {
                LogUtils.error("An error occurred while requesting ip", e, new Object[0]);
            }
        }
        while (waterfall.hasNextPosition()) {
            int nextPosition = waterfall.pollNextPosition().intValue();
            LogUtils.debug("Processing next notification ad item...", new Object[i]);
            this.stats.add(new Stat(NotificationAd.BANNER_ID, Stat.TYPE_REQUEST));
            if (type == Waterfall.Type.FLAT) {
                this.settings.setLastNotificationAdRequestedItemId(Integer.valueOf(adapter.getId(nextPosition)));
            } else {
                this.settings.setLastNotificationAdRequestedItemId(num);
            }
            this.settings.save();
            Map<String, String> replacers = new HashMap<>(5);
            replacers.put("ip", ip);
            replacers.put("user_agent", SystemUtils.getDefaultUserAgent());
            replacers.put("timestamp", Long.toString(System.currentTimeMillis() / 1000));
            replacers.put("lang", Locale.getDefault().getLanguage());
            replacers.put("uniqueId", this.settings.getClientId());
            replacers.put("subId", this.settings.getAppId());
            String adUrl = waterfallItems[nextPosition].getUrl();
            String adUrl2 = adUrl;
            for (Map.Entry<String, String> replacer : replacers.entrySet()) {
                try {
                    Object[] objArr = new Object[1];
                    objArr[i] = replacer.getKey();
                    adUrl2 = adUrl2.replace(String.format("{%s}", objArr), URLEncoder.encode((String) replacer.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e2) {
                    LogUtils.error("Error occurred while encoding an url", e2, new Object[i]);
                }
            }
            try {
                Object[] objArr2 = new Object[1];
                objArr2[i] = adUrl2;
                LogUtils.debug("Requesting notification ad from: %s", objArr2);
                String response = (String) this.requestManager.sendRequest(adUrl2, String.class);
                Object[] objArr3 = new Object[1];
                objArr3[i] = response;
                LogUtils.debug("Notification ad response: %s", objArr3);
                try {
                    item = ((Item[]) JsonUtils.arrayFromGson(response, Item.class))[i];
                } catch (JsonSyntaxException e3) {
                    item = (Item) JsonUtils.fromJSON(response, Item.class);
                }
                NotificationAd ad = new NotificationAd(item.text, item.title, item.linkUrl, null, item.iconUrl, item.imageUrl);
                LogUtils.debug("Downloading an icon...", new Object[0]);
                ad.setLargeIcon(this.downloadManager.downloadImage(ad.getLargeIconSrc()));
                if (!TextUtils.isEmpty(ad.getImageSrc())) {
                    LogUtils.debug("Downloading a big image...", new Object[0]);
                    ad.setImage(this.downloadManager.downloadImage(ad.getImageSrc()));
                }
                ad.setId(adapter.getId(nextPosition));
                return ad;
            } catch (Exception e4) {
                LogUtils.error("Error occurred while requesting notification ad", e4, new Object[0]);
                num = null;
                i = 0;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(NotificationAd notificationAd) {
        super.onPostExecute((Object) notificationAd);
        if (notificationAd != null) {
            this.notificationManager.showNotification(notificationAd);
            this.settings.incNotificationAdCount(notificationAd.getId());
            this.settings.save();
            this.stats.add(new Stat(NotificationAd.BANNER_ID, Stat.TYPE_IMPRESSION));
            return;
        }
        LogUtils.error("ad == null", new Object[0]);
    }

    private static class Item {
        @SerializedName("icon")
        String iconUrl;
        @SerializedName("image")
        String imageUrl;
        @SerializedName("link")
        String linkUrl;
        @SerializedName("text")
        String text;
        @SerializedName("title")
        String title;

        private Item() {
        }
    }

    public static class Factory implements TaskFactory<ShowNotificationAdTask2> {
        @NonNull
        private final Config config;
        @NonNull
        private final DownloadManager downloadManager;
        @NonNull
        private final NotificationManager notificationManager;
        @NonNull
        private final RequestManager requestManager;
        @NonNull
        private final Settings settings;
        @NonNull
        private final Stats stats;

        public Factory(@NonNull Stats stats2, @NonNull Config config2, @NonNull Settings settings2, @NonNull RequestManager requestManager2, @NonNull DownloadManager downloadManager2, @NonNull NotificationManager notificationManager2) {
            this.stats = stats2;
            this.config = config2;
            this.settings = settings2;
            this.requestManager = requestManager2;
            this.downloadManager = downloadManager2;
            this.notificationManager = notificationManager2;
        }

        @NonNull
        public ShowNotificationAdTask2 create() {
            return new ShowNotificationAdTask2(this.stats, this.config, this.settings, this.requestManager, this.downloadManager, this.notificationManager);
        }
    }
}
