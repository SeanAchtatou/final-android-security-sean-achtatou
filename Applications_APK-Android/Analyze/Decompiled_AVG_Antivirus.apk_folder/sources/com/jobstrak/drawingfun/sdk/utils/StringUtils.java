package com.jobstrak.drawingfun.sdk.utils;

import androidx.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public class StringUtils {
    private StringUtils() {
        throw new AssertionError("No instances");
    }

    public static boolean isEmpty(@Nullable CharSequence s) {
        return s == null || s.length() == 0;
    }

    public static boolean wildcardMatch(@Nullable String wildcardPattern, @Nullable String s) {
        if (isEmpty(wildcardPattern) && isEmpty(s)) {
            return true;
        }
        if (isEmpty(wildcardPattern) || isEmpty(s)) {
            return false;
        }
        return s.matches(wildcardPattern.replaceAll(".", "[$0]").replace("[*]", ".*"));
    }

    public static boolean wildcardAnyMatch(@Nullable String[] wildcardPatterns, @Nullable String s) {
        return wildcardAnyMatch(Arrays.asList(wildcardPatterns), s);
    }

    public static boolean wildcardAnyMatch(@Nullable List<String> wildcardPatterns, @Nullable String s) {
        if (wildcardPatterns == null) {
            return false;
        }
        for (String pattern : wildcardPatterns) {
            if (wildcardMatch(pattern, s)) {
                return true;
            }
        }
        return false;
    }

    public static boolean wildcardAllMatch(@Nullable String[] wildcardPatterns, @Nullable String s) {
        return wildcardAllMatch(Arrays.asList(wildcardPatterns), s);
    }

    public static boolean wildcardAllMatch(@Nullable List<String> wildcardPatterns, @Nullable String s) {
        if (wildcardPatterns == null) {
            return true;
        }
        for (String pattern : wildcardPatterns) {
            if (!wildcardMatch(pattern, s)) {
                return false;
            }
        }
        return true;
    }
}
