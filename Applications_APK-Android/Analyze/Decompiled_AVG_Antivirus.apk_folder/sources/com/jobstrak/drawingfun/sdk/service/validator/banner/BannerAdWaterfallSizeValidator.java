package com.jobstrak.drawingfun.sdk.service.validator.banner;

import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BannerAdWaterfallSizeValidator extends Validator {
    public boolean validate(long currentTime) {
        return Config.getInstance().getWaterfall().size() > 0;
    }

    public String getReason() {
        return "waterfall is empty";
    }
}
