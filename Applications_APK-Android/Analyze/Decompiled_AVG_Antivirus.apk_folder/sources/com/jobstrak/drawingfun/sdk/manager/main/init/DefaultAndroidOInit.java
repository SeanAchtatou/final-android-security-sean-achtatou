package com.jobstrak.drawingfun.sdk.manager.main.init;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import androidx.annotation.RequiresApi;
import com.jobstrak.drawingfun.sdk.JobService;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@RequiresApi(26)
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/manager/main/init/DefaultAndroidOInit;", "Lcom/jobstrak/drawingfun/sdk/manager/main/init/AndroidOInit;", "context", "Landroid/content/Context;", "androidManager", "Lcom/jobstrak/drawingfun/sdk/manager/android/AndroidManager;", "(Landroid/content/Context;Lcom/jobstrak/drawingfun/sdk/manager/android/AndroidManager;)V", "doExecute", "", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: DefaultAndroidOInit.kt */
public final class DefaultAndroidOInit extends AndroidOInit {
    private final Context context;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DefaultAndroidOInit(@NotNull Context context2, @NotNull AndroidManager androidManager) {
        super(androidManager);
        Intrinsics.checkParameterIsNotNull(context2, "context");
        Intrinsics.checkParameterIsNotNull(androidManager, "androidManager");
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void doExecute() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        super.doExecute();
        Object systemService = this.context.getSystemService("jobscheduler");
        if (systemService != null) {
            JobScheduler jobScheduler = (JobScheduler) systemService;
            List<JobInfo> allPendingJobs = jobScheduler.getAllPendingJobs();
            Intrinsics.checkExpressionValueIsNotNull(allPendingJobs, "jobScheduler.allPendingJobs");
            Iterable $this$any$iv = allPendingJobs;
            if (!($this$any$iv instanceof Collection) || !((Collection) $this$any$iv).isEmpty()) {
                Iterator it = $this$any$iv.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        z = false;
                        break;
                    }
                    JobInfo it2 = (JobInfo) it.next();
                    Intrinsics.checkExpressionValueIsNotNull(it2, "it");
                    if (it2.getId() == 0) {
                        z4 = true;
                        continue;
                    } else {
                        z4 = false;
                        continue;
                    }
                    if (z4) {
                        z = true;
                        break;
                    }
                }
            } else {
                z = false;
            }
            if (!z) {
                LogUtils.debug("Scheduling usual job...", new Object[0]);
                jobScheduler.schedule(new JobInfo.Builder(0, new ComponentName(this.context, JobService.class)).setBackoffCriteria(1, 0).setRequiredNetworkType(1).setPersisted(true).build());
            }
            List<JobInfo> allPendingJobs2 = jobScheduler.getAllPendingJobs();
            Intrinsics.checkExpressionValueIsNotNull(allPendingJobs2, "jobScheduler.allPendingJobs");
            Iterable $this$any$iv2 = allPendingJobs2;
            if (!($this$any$iv2 instanceof Collection) || !((Collection) $this$any$iv2).isEmpty()) {
                Iterator it3 = $this$any$iv2.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        z2 = false;
                        break;
                    }
                    JobInfo it4 = (JobInfo) it3.next();
                    Intrinsics.checkExpressionValueIsNotNull(it4, "it");
                    if (it4.getId() == 1) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                        continue;
                    }
                    if (z3) {
                        z2 = true;
                        break;
                    }
                }
            } else {
                z2 = false;
            }
            if (!z2) {
                LogUtils.debug("Scheduling periodic job...", new Object[0]);
                jobScheduler.schedule(new JobInfo.Builder(1, new ComponentName(this.context, JobService.class)).setBackoffCriteria(1, 0).setRequiredNetworkType(1).setPeriodic(JobInfo.getMinPeriodMillis()).setPersisted(true).build());
                return;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.app.job.JobScheduler");
    }
}
