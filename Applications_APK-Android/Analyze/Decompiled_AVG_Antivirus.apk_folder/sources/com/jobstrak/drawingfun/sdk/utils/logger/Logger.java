package com.jobstrak.drawingfun.sdk.utils.logger;

import kotlin.Metadata;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J=\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\u0010\b\u001a\u0004\u0018\u00010\t2\u0012\u0010\n\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00010\u000b\"\u00020\u0001H&¢\u0006\u0002\u0010\f¨\u0006\r"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/utils/logger/Logger;", "", "log", "", "level", "", "msg", "", "throwable", "", "args", "", "(ILjava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: Logger.kt */
public interface Logger {
    void log(int i, @Nullable String str, @Nullable Throwable th, @NotNull Object... objArr);
}
