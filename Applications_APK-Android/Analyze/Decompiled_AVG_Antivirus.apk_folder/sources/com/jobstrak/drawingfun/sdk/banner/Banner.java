package com.jobstrak.drawingfun.sdk.banner;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Map;

public abstract class Banner {
    @NonNull
    private Activity activity;
    @Nullable
    private Callback callback;
    @NonNull
    private final Map<String, String> properties;
    @Nullable
    private State state;

    public interface Callback {
        void onClick(@NonNull Banner banner);

        void onDismiss(@NonNull Banner banner);

        void onFail(@NonNull Banner banner, @Nullable String str, @Nullable Exception exc);

        void onLoad(@NonNull Banner banner);

        void onRequest(@NonNull Banner banner);

        void onShow(@NonNull Banner banner);
    }

    enum State {
        REQUESTED,
        LOADED,
        SHOWED,
        FAILED,
        DISMISSED,
        CLICKED
    }

    public abstract void load();

    public abstract void show();

    public Banner(@NonNull Activity activity2, @NonNull Callback callback2, @NonNull Map<String, String> properties2) {
        this.activity = activity2;
        this.callback = callback2;
        this.properties = properties2;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Activity getActivity() {
        return this.activity;
    }

    @NonNull
    public Map<String, String> getProperties() {
        return this.properties;
    }

    public void removeCallback() {
        this.callback = null;
    }

    /* access modifiers changed from: protected */
    public void fireOnRequest() {
        this.state = State.REQUESTED;
        Callback callback2 = this.callback;
        if (callback2 != null) {
            callback2.onRequest(this);
        }
    }

    /* access modifiers changed from: protected */
    public void fireOnShow() {
        if (this.state == State.LOADED) {
            this.state = State.SHOWED;
            Callback callback2 = this.callback;
            if (callback2 != null) {
                callback2.onShow(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void fireOnDismiss() {
        if (this.state == State.SHOWED) {
            this.state = State.DISMISSED;
            Callback callback2 = this.callback;
            if (callback2 != null) {
                callback2.onDismiss(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void fireOnFail() {
        fireOnFail(null);
    }

    /* access modifiers changed from: protected */
    public void fireOnFail(@Nullable String reason) {
        fireOnFail(reason, null);
    }

    /* access modifiers changed from: protected */
    public void fireOnFail(@Nullable String reason, @Nullable Exception e) {
        this.state = State.FAILED;
        Callback callback2 = this.callback;
        if (callback2 != null) {
            callback2.onFail(this, reason, e);
        }
    }

    /* access modifiers changed from: protected */
    public void fireOnClick() {
        if (this.state == State.SHOWED) {
            this.state = State.CLICKED;
            Callback callback2 = this.callback;
            if (callback2 != null) {
                callback2.onClick(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void fireOnLoad() {
        if (this.state == State.REQUESTED) {
            this.state = State.LOADED;
            Callback callback2 = this.callback;
            if (callback2 != null) {
                callback2.onLoad(this);
            }
        }
    }
}
