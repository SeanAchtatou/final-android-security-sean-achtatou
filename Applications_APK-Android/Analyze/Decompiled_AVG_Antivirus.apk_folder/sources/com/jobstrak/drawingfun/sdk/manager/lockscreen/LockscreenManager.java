package com.jobstrak.drawingfun.sdk.manager.lockscreen;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.LockscreenAd;

public interface LockscreenManager {
    void hideBanner();

    boolean isBannerClicked();

    boolean isBannerVisible();

    void openClickedBanner();

    void showBanner(@NonNull LockscreenAd lockscreenAd);
}
