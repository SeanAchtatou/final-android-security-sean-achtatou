package com.jobstrak.drawingfun.sdk.task.stat;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Stats;
import com.jobstrak.drawingfun.sdk.manager.request.exception.HttpException;
import com.jobstrak.drawingfun.sdk.model.Stat;
import com.jobstrak.drawingfun.sdk.repository.stat.StatRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StatsSendTask extends BaseTask<Void> {
    @NonNull
    private final StatRepository statRepository;
    @NonNull
    private final Stats stats;

    private StatsSendTask(@NonNull Stats stats2, @NonNull StatRepository statRepository2) {
        this.stats = stats2;
        this.statRepository = statRepository2;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground() {
        List<Stat> statList = extractStats();
        try {
            sendStats(statList);
            return null;
        } catch (Exception e) {
            LogUtils.error("Error occurred while sending stats", e, new Object[0]);
            if (!(e instanceof HttpException) || ((HttpException) e).getHttpCode() < 500) {
                return null;
            }
            this.stats.addAll(statList);
            return null;
        }
    }

    private void sendStats(List<Stat> statList) throws Exception {
        LogUtils.debug("Sending stats...", new Object[0]);
        this.statRepository.addStatsFromArray(statList);
    }

    private List<Stat> extractStats() {
        LogUtils.debug("Extracting stats...", new Object[0]);
        List<Stat> statList = new ArrayList<>(this.stats.getAll());
        statList.removeAll(Collections.singleton(null));
        LogUtils.debug("{%s}", TextUtils.join(", ", statList));
        this.stats.clear();
        return statList;
    }

    public static class Factory implements TaskFactory<StatsSendTask> {
        @NonNull
        private final StatRepository statRepository;
        @NonNull
        private final Stats stats;

        public Factory(@NonNull Stats stats2, @NonNull StatRepository statRepository2) {
            this.stats = stats2;
            this.statRepository = statRepository2;
        }

        @NonNull
        public StatsSendTask create() {
            return new StatsSendTask(this.stats, this.statRepository);
        }
    }
}
