package com.jobstrak.drawingfun.sdk.utils;

import com.jobstrak.drawingfun.sdk.data.SdkConfig;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.utils.logger.CryoLogger;
import com.jobstrak.drawingfun.sdk.utils.logger.Logger;
import kotlin.jvm.functions.Function2;

public class SecureLogUtils {
    private static final Logger logger = new CryoLogger(SdkConfig.getSdkName(), new Function2<CryopiggyManager, Integer, Boolean>() {
        public Boolean invoke(CryopiggyManager cryopiggyManager, Integer level) {
            return false;
        }
    });

    private static void log(int level, String msg, Throwable throwable, Object... args) {
        logger.log(level, msg, throwable, args);
    }

    public static void debug() {
        log(3, null, null, new Object[0]);
    }

    public static void debug(String msg, Object... args) {
        log(3, msg, null, args);
    }

    public static void verbose(String msg, Object... args) {
        log(2, msg, null, args);
    }

    public static void info(String msg, Object... args) {
        log(4, msg, null, args);
    }

    public static void warning(String msg, Object... args) {
        log(5, msg, null, args);
    }

    public static void error(String msg, Object... args) {
        log(6, msg, null, args);
    }

    public static void error(Throwable throwable) {
        log(6, null, throwable, new Object[0]);
    }

    public static void error(String msg, Throwable throwable, Object... args) {
        log(6, msg, throwable, args);
    }

    public static void wtf(String msg, Object... args) {
        log(7, msg, null, args);
    }

    public static void wtf(Throwable throwable) {
        log(7, null, throwable, new Object[0]);
    }

    public static void wtf(String msg, Throwable throwable, Object... args) {
        log(7, msg, throwable, args);
    }
}
