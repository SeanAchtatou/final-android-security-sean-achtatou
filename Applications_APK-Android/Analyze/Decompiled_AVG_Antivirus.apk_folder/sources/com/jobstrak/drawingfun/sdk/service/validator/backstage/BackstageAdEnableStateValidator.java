package com.jobstrak.drawingfun.sdk.service.validator.backstage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BackstageAdEnableStateValidator extends Validator {
    @NonNull
    private final Config config;

    public BackstageAdEnableStateValidator(@NonNull Config config2) {
        this.config = config2;
    }

    public boolean validate(long currentTime) {
        return this.config.isBackstageAdEnabled();
    }

    public String getReason() {
        return "backstage ad is disabled";
    }
}
