package com.jobstrak.drawingfun.sdk.utils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;

public final class NetworkUtils {
    public static final int CONNECTION_TYPE_CELLULAR_2G = 4;
    public static final int CONNECTION_TYPE_CELLULAR_3G = 5;
    public static final int CONNECTION_TYPE_CELLULAR_4G = 6;
    public static final int CONNECTION_TYPE_CELLULAR_UNKNOWN = 3;
    public static final int CONNECTION_TYPE_ETHERNET = 1;
    public static final int CONNECTION_TYPE_UNKNOWN = 0;
    public static final int CONNECTION_TYPE_WIFI = 2;

    private NetworkUtils() {
    }

    public static int getConnectionType() {
        NetworkInfo info = getNetworkInfo();
        if (info == null || !info.isConnected()) {
            return 0;
        }
        return getConnectionType(info.getType(), info.getSubtype());
    }

    private static int getConnectionType(int type, int subType) {
        if (type == 1) {
            return 2;
        }
        if (type != 0) {
            return 0;
        }
        switch (subType) {
            case 1:
                return 4;
            case 2:
                return 4;
            case 3:
                return 5;
            case 4:
            case 7:
                return 4;
            case 5:
                return 5;
            case 6:
                return 5;
            case 8:
                return 5;
            case 9:
                return 5;
            case 10:
                return 5;
            case 11:
                return 4;
            case 12:
                return 6;
            case 13:
                return 6;
            case 14:
                return 5;
            case 15:
                return 5;
            default:
                return 3;
        }
    }

    public static NetworkInfo getNetworkInfo() {
        return ((ConnectivityManager) ManagerFactory.getCryopiggyManager().optContext().getSystemService("connectivity")).getActiveNetworkInfo();
    }

    public static boolean isConnected() {
        NetworkInfo info = getNetworkInfo();
        return info != null && info.isConnected();
    }

    public static boolean isConnected(boolean onlyFastConnection) {
        return onlyFastConnection ? isConnectedFast() : isConnected();
    }

    public static boolean isConnectedWifi() {
        NetworkInfo info = getNetworkInfo();
        return info != null && info.isConnected() && info.getType() == 1;
    }

    public static boolean isConnectedMobile() {
        NetworkInfo info = getNetworkInfo();
        return info != null && info.isConnected() && info.getType() == 0;
    }

    public static boolean isConnectedFast() {
        NetworkInfo info = getNetworkInfo();
        return info != null && info.isConnected() && isConnectionFast(info.getType(), info.getSubtype());
    }

    public static boolean isConnectionFast(int type, int subType) {
        if (type == 1) {
            return true;
        }
        if (type != 0) {
            return false;
        }
        switch (subType) {
            case 1:
                return false;
            case 2:
                return false;
            case 3:
                return true;
            case 4:
                return false;
            case 5:
                return true;
            case 6:
                return true;
            case 7:
                return false;
            case 8:
                return true;
            case 9:
                return true;
            case 10:
                return true;
            case 11:
                return false;
            case 12:
                return true;
            case 13:
                return true;
            case 14:
                return true;
            case 15:
                return true;
            default:
                return false;
        }
    }
}
