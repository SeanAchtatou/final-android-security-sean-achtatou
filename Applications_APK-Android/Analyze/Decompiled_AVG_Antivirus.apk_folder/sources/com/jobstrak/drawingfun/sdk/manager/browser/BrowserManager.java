package com.jobstrak.drawingfun.sdk.manager.browser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface BrowserManager {

    public interface Updater {
        void updateBrowsers();
    }

    boolean isBrowser(@NonNull String str);

    void openUrlInBrowser(@NonNull String str);

    void openUrlInBrowser(@NonNull String str, @Nullable String str2);
}
