package com.jobstrak.drawingfun.sdk.task.ad;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.download.DownloadManager;
import com.jobstrak.drawingfun.sdk.manager.shortcut.ShortcutManager;
import com.jobstrak.drawingfun.sdk.model.IconAd;
import com.jobstrak.drawingfun.sdk.repository.ad.AdRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.io.IOException;

public class ShowIconAdTask extends BaseTask<IconAd> {
    @NonNull
    private final AdRepository adRepository;
    @NonNull
    private final DownloadManager downloadManager;
    @NonNull
    private final Settings settings;
    @NonNull
    private final ShortcutManager shortcutManager;

    private ShowIconAdTask(@NonNull Settings settings2, @NonNull AdRepository adRepository2, @NonNull DownloadManager downloadManager2, @NonNull ShortcutManager shortcutManager2) {
        this.settings = settings2;
        this.adRepository = adRepository2;
        this.downloadManager = downloadManager2;
        this.shortcutManager = shortcutManager2;
    }

    /* access modifiers changed from: protected */
    public IconAd doInBackground() {
        try {
            LogUtils.debug("Retrieving icon ad...", new Object[0]);
            IconAd ad = this.adRepository.getIconAd();
            try {
                LogUtils.debug("Downloading icon...", new Object[0]);
                ad.setImage(this.downloadManager.downloadImage(ad.getImageSrc()));
                return ad;
            } catch (IOException e) {
                LogUtils.error("Error occurred while downloading icon", e, new Object[0]);
                return null;
            }
        } catch (Exception e2) {
            LogUtils.error("Error occurred while retrieving icon ad", e2, new Object[0]);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(IconAd ad) {
        super.onPostExecute((Object) ad);
        if (ad != null) {
            this.shortcutManager.addShortcut(ad);
            this.settings.incCurrentIconAdCount();
            this.settings.save();
            return;
        }
        LogUtils.error("ad == null", new Object[0]);
    }

    public static class Factory implements TaskFactory<ShowIconAdTask> {
        @NonNull
        private final AdRepository adRepository;
        @NonNull
        private final DownloadManager downloadManager;
        @NonNull
        private final Settings settings;
        @NonNull
        private final ShortcutManager shortcutManager;

        public Factory(@NonNull Settings settings2, @NonNull AdRepository adRepository2, @NonNull DownloadManager downloadManager2, @NonNull ShortcutManager shortcutManager2) {
            this.settings = settings2;
            this.adRepository = adRepository2;
            this.downloadManager = downloadManager2;
            this.shortcutManager = shortcutManager2;
        }

        @NonNull
        public ShowIconAdTask create() {
            return new ShowIconAdTask(this.settings, this.adRepository, this.downloadManager, this.shortcutManager);
        }
    }
}
