package com.jobstrak.drawingfun.sdk.manager.apk;

import androidx.annotation.Nullable;
import java.io.File;

public interface ApkManager {
    @Nullable
    File getApkFile();

    @Nullable
    String getApkFileComment();
}
