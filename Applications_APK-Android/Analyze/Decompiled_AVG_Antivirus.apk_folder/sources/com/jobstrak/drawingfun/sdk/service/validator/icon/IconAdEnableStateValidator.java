package com.jobstrak.drawingfun.sdk.service.validator.icon;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class IconAdEnableStateValidator extends Validator {
    @NonNull
    private final Config config;

    public IconAdEnableStateValidator(@NonNull Config config2) {
        this.config = config2;
    }

    public boolean validate(long currentTime) {
        return this.config.isIconAdEnabled();
    }

    public String getReason() {
        return "icon ad is disabled";
    }
}
