package com.jobstrak.drawingfun.sdk.manager.main.init;

public interface Abandonable {
    void doAbandon();
}
