package com.jobstrak.drawingfun.sdk.manager.request.exception;

public class HttpException extends Exception {
    private int httpCode;

    public HttpException(int httpCode2) {
        this.httpCode = httpCode2;
    }

    public int getHttpCode() {
        return this.httpCode;
    }
}
