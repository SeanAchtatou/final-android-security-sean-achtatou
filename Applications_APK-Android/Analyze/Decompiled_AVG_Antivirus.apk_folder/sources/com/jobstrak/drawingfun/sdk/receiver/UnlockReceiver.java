package com.jobstrak.drawingfun.sdk.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;

public class UnlockReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Log.d("Cryopiggy", "[UnlockReceiver] Received " + intent.getAction());
        ManagerFactory.getCryopiggyManager().init(context);
    }
}
