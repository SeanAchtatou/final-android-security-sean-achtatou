package com.jobstrak.drawingfun.sdk.service.validator.server;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class InitValidator extends Validator {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;

    public InitValidator(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        return !TextUtils.isEmpty(this.settings.getAppId()) && !TextUtils.isEmpty(this.config.getServer());
    }

    public String getReason() {
        return "SDK isn't initiated";
    }
}
