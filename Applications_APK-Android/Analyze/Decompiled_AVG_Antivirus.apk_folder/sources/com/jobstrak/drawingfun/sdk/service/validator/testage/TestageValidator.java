package com.jobstrak.drawingfun.sdk.service.validator.testage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.TestageItem;

public interface TestageValidator {
    Result validate(long j, @NonNull TestageItem testageItem);

    public static abstract class Result {
        private boolean valid;

        public abstract String reason();

        public Result(boolean valid2) {
            this.valid = valid2;
        }

        public final boolean valid() {
            return this.valid;
        }
    }
}
