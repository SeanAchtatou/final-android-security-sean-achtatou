package com.jobstrak.drawingfun.sdk.service.alarm;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.stat.AliveEventSendTask;
import com.jobstrak.drawingfun.sdk.utils.NetworkUtils;

public class AliveEventSendAlarm extends Alarm {
    @NonNull
    private final TaskFactory<AliveEventSendTask> aliveEventSendTask;

    public AliveEventSendAlarm(@NonNull Config config, @NonNull Settings settings, @NonNull TaskFactory<AliveEventSendTask> aliveEventSendTask2) {
        super(config, settings);
        this.aliveEventSendTask = aliveEventSendTask2;
    }

    public boolean isNeedExecute(long currentTime) {
        return isRegistered() && isItTime(currentTime) && isConnected();
    }

    public void execute(long currentTime) {
        this.aliveEventSendTask.create().execute(new Void[0]);
        setNextAliveEventSendTime(currentTime);
    }

    private boolean isRegistered() {
        return getSettings().getClientId() != null;
    }

    private boolean isItTime(long currentTime) {
        return getSettings().getNextAliveEventSendTime() <= currentTime;
    }

    private boolean isConnected() {
        return NetworkUtils.isConnected();
    }

    private void setNextAliveEventSendTime(long currentTime) {
        getSettings().setNextAliveEventSendTime(900000 + currentTime);
        getSettings().save();
    }
}
