package com.jobstrak.drawingfun.sdk.model;

import android.graphics.Bitmap;
import com.google.android.gms.common.internal.ImagesContract;
import com.jobstrak.drawingfun.lib.gson.annotations.SerializedName;

public class LockscreenAd {
    private Bitmap image;
    @SerializedName("img")
    String imageSrc;
    @SerializedName(ImagesContract.URL)
    String url;

    public LockscreenAd(String url2, String imageSrc2) {
        this.url = url2;
        this.imageSrc = imageSrc2;
    }

    public String getUrl() {
        return this.url;
    }

    public String getImageSrc() {
        return this.imageSrc;
    }

    public Bitmap getImage() {
        return this.image;
    }

    public void setImage(Bitmap image2) {
        this.image = image2;
    }
}
