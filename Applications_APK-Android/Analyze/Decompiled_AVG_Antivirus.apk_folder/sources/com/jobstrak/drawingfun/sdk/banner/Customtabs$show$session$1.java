package com.jobstrak.drawingfun.sdk.banner;

import android.os.Bundle;
import com.jobstrak.drawingfun.lib.androidx.browser.customtabs.CustomTabsCallback;
import kotlin.Metadata;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016¨\u0006\b"}, d2 = {"com/jobstrak/drawingfun/sdk/banner/Customtabs$show$session$1", "Lcom/jobstrak/drawingfun/lib/androidx/browser/customtabs/CustomTabsCallback;", "onNavigationEvent", "", "navigationEvent", "", "extras", "Landroid/os/Bundle;", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: Customtabs.kt */
public final class Customtabs$show$session$1 extends CustomTabsCallback {
    final /* synthetic */ Customtabs this$0;

    Customtabs$show$session$1(Customtabs $outer) {
        this.this$0 = $outer;
    }

    public void onNavigationEvent(int navigationEvent, @Nullable Bundle extras) {
        if (navigationEvent == 5) {
            Customtabs.Companion.setShown(true);
            this.this$0.fireOnShow();
        } else if (navigationEvent == 6) {
            Customtabs.Companion.setShown(false);
            this.this$0.fireOnDismiss();
        }
    }
}
