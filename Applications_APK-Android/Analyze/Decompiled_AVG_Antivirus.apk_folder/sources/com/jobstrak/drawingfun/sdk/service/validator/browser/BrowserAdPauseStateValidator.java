package com.jobstrak.drawingfun.sdk.service.validator.browser;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class BrowserAdPauseStateValidator extends Validator {
    @NonNull
    private final Settings settings;

    public BrowserAdPauseStateValidator(@NonNull Settings settings2) {
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        return this.settings.getNextLinkAdTime() - currentTime <= 0;
    }

    public String getReason() {
        return String.format("browser (link) ad is paused (%s left)", TimeUtils.parseTime(this.settings.getNextLinkAdTime() - TimeUtils.getCurrentTime()));
    }
}
