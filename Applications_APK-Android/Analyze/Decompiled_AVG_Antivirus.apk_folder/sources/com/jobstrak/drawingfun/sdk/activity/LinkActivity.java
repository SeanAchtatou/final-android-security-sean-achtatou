package com.jobstrak.drawingfun.sdk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.data.Stats;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.browser.BrowserManager;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;
import com.jobstrak.drawingfun.sdk.model.Stat;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class LinkActivity extends Activity {
    private static final String EXTRA_TYPE = "extra_type";
    private static final String EXTRA_URL = "extra_url";
    public static final int TYPE_NONE = 0;
    public static final int TYPE_NOTIFICATION = 1;

    public static Intent createIntent(@NonNull Context context, @NonNull String url) {
        return new Intent(context, LinkActivity.class).putExtra("extra_url", url);
    }

    public static Intent createIntentForType(@NonNull Context context, @NonNull String url, @NonNull int type) {
        return createIntent(context, url).putExtra(EXTRA_TYPE, type);
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.debug();
        Config config = Config.getInstance();
        Settings settings = Settings.getInstance();
        BrowserManager browserManager = ManagerFactory.getBrowserManager();
        long currentTime = TimeUtils.getCurrentTime();
        Bundle extras = getIntent().getExtras();
        if (extras.containsKey("extra_url")) {
            settings.setNextLinkAdTime(config.getBrowserAdDelay() + currentTime);
            settings.save();
            browserManager.openUrlInBrowser(extras.getString("extra_url"));
            if (extras.getInt(EXTRA_TYPE, 0) == 1) {
                Stats.getInstance().add(new Stat(NotificationAd.BANNER_ID, Stat.TYPE_CLICK));
            }
        } else {
            LogUtils.error("Extra 'EXTRA_URL' not found", new Object[0]);
        }
        finish();
    }
}
