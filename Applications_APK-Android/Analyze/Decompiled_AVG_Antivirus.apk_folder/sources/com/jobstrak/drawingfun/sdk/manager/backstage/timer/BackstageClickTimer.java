package com.jobstrak.drawingfun.sdk.manager.backstage.timer;

public interface BackstageClickTimer {
    void startTimer();

    void stopTimer();
}
