package com.jobstrak.drawingfun.sdk.service.validator;

import com.jobstrak.drawingfun.sdk.banner.Customtabs;
import kotlin.Metadata;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/service/validator/CustomTabShowingStateValidator;", "Lcom/jobstrak/drawingfun/sdk/service/validator/Validator;", "()V", "getReason", "", "validate", "", "currentTime", "", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: CustomTabShowingStateValidator.kt */
public final class CustomTabShowingStateValidator extends Validator {
    public boolean validate(long currentTime) {
        return !Customtabs.Companion.isShown();
    }

    @NotNull
    public String getReason() {
        return "custom tabs are being shown";
    }
}
