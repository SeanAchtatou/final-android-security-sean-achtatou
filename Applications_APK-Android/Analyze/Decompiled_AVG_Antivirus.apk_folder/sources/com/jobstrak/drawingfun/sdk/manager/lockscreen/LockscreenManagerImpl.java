package com.jobstrak.drawingfun.sdk.manager.lockscreen;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.browser.BrowserManager;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.model.LockscreenAd;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.ScreenUtils;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class LockscreenManagerImpl implements LockscreenManager {
    private static final int BANNER_HEIGHT = 50;
    private static final int BANNER_WIDTH = 320;
    private final BrowserManager browserManager = ManagerFactory.getBrowserManager();
    /* access modifiers changed from: private */
    @Nullable
    public LockscreenAd clickedAd;
    @NonNull
    private final CryopiggyManager cryopiggyManager;
    /* access modifiers changed from: private */
    @Nullable
    public ImageView view;

    public LockscreenManagerImpl(@NonNull CryopiggyManager cryopiggyManager2) {
        this.cryopiggyManager = cryopiggyManager2;
    }

    public void showBanner(@NonNull final LockscreenAd lockscreenAd) {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            LogUtils.debug("Showing lockscreen ad...", new Object[0]);
            if (this.view == null) {
                this.view = buildImageView(context);
                this.view.setImageBitmap(lockscreenAd.getImage());
                this.view.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        LockscreenManagerImpl.this.postponeLinkAd();
                        LockscreenAd unused = LockscreenManagerImpl.this.clickedAd = lockscreenAd;
                        LockscreenManagerImpl.hideView(v);
                        ImageView unused2 = LockscreenManagerImpl.this.view = null;
                    }
                });
                getWindowManager(context).addView(this.view, buildLayoutParams());
                showView(this.view);
                return;
            }
            LogUtils.warning("The ad is already showing", new Object[0]);
            return;
        }
        LogUtils.error("context == null", new Object[0]);
    }

    public void hideBanner() {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            LogUtils.debug("Hiding lockscreen ad...", new Object[0]);
            if (this.view != null) {
                getWindowManager(context).removeViewImmediate(this.view);
                this.view = null;
                return;
            }
            LogUtils.warning("The ad is already hidden", new Object[0]);
            return;
        }
        LogUtils.error("context == null", new Object[0]);
    }

    public boolean isBannerVisible() {
        return this.view != null;
    }

    public boolean isBannerClicked() {
        return this.clickedAd != null;
    }

    public void openClickedBanner() {
        LogUtils.debug();
        this.browserManager.openUrlInBrowser(this.clickedAd.getUrl());
        this.clickedAd = null;
    }

    /* access modifiers changed from: private */
    public void postponeLinkAd() {
        Settings settings = Settings.getInstance();
        settings.setNextLinkAdTime(TimeUtils.getCurrentTime() + Config.getInstance().getBrowserAdDelay());
        settings.save();
    }

    private static void showView(@NonNull final View view2) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view2, View.ALPHA, 0.0f, 1.0f);
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                View view = view2;
                if (view != null) {
                    view.setVisibility(0);
                }
            }
        });
        animator.setDuration(250L);
        animator.start();
    }

    /* access modifiers changed from: private */
    public static void hideView(@NonNull final View view2) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view2, View.ALPHA, 1.0f, 0.0f);
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                LockscreenManagerImpl.getWindowManager(view2.getContext()).removeViewImmediate(view2);
            }
        });
        animator.setDuration(250L);
        animator.start();
    }

    private static ImageView buildImageView(@NonNull Context context) {
        ImageView view2 = new ImageView(context);
        view2.setVisibility(4);
        view2.setScaleType(ImageView.ScaleType.FIT_XY);
        return view2;
    }

    @NonNull
    private static WindowManager.LayoutParams buildLayoutParams() {
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(ScreenUtils.dp(BANNER_WIDTH), ScreenUtils.dp(BANNER_HEIGHT), 2010, 786472, -3);
        params.gravity = 80;
        return params;
    }

    /* access modifiers changed from: private */
    @NonNull
    public static WindowManager getWindowManager(@NonNull Context context) {
        return (WindowManager) context.getSystemService("window");
    }
}
