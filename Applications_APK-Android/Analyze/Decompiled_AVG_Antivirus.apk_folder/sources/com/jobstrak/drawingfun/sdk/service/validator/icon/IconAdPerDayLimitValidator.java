package com.jobstrak.drawingfun.sdk.service.validator.icon;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class IconAdPerDayLimitValidator extends Validator {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;

    public IconAdPerDayLimitValidator(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        int limit = this.config.getIconAdCount();
        return limit <= 0 || this.settings.getCurrentIconAdCount() < limit;
    }

    public String getReason() {
        return String.format("daily icon ad limit exceeded (%s)", Integer.valueOf(this.config.getIconAdCount()));
    }
}
