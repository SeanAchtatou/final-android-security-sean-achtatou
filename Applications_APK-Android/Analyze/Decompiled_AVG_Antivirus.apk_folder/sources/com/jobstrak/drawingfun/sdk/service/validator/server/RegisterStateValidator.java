package com.jobstrak.drawingfun.sdk.service.validator.server;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.server.RegisterTask;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class RegisterStateValidator extends Validator {
    @NonNull
    private TaskFactory<RegisterTask> registerTask;
    @NonNull
    private Settings settings;
    private long timeLeft;

    public RegisterStateValidator(@NonNull Settings settings2, @NonNull TaskFactory<RegisterTask> registerTask2) {
        this.settings = settings2;
        this.registerTask = registerTask2;
    }

    public boolean validate(long currentTime) {
        if (isRegistered()) {
            return true;
        }
        if (isItTime(currentTime)) {
            this.registerTask.create().execute(new Void[0]);
            setNextRegisterTryTime(currentTime);
        }
        return false;
    }

    public String getReason() {
        String str;
        Object[] objArr = new Object[1];
        if (this.timeLeft > 0) {
            str = "after " + TimeUtils.parseTime(this.timeLeft);
        } else {
            str = "right now";
        }
        objArr[0] = str;
        return String.format("client is not registered (next try %s)", objArr);
    }

    private boolean isRegistered() {
        return this.settings.getClientId() != null;
    }

    private boolean isItTime(long currentTime) {
        this.timeLeft = this.settings.getNextRegisterTryTime() - currentTime;
        return this.timeLeft <= 0;
    }

    private void setNextRegisterTryTime(long currentTime) {
        this.settings.setNextRegisterTryTime(900000 + currentTime);
        this.settings.save();
    }
}
