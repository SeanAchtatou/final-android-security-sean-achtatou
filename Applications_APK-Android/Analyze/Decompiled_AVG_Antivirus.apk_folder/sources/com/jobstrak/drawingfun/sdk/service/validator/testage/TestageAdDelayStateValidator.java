package com.jobstrak.drawingfun.sdk.service.validator.testage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.model.TestageItem;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageValidator;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class TestageAdDelayStateValidator implements TestageValidator {
    @NonNull
    private final Settings settings;

    public TestageAdDelayStateValidator(@NonNull Settings settings2) {
        this.settings = settings2;
    }

    public TestageValidator.Result validate(long currentTime, @NonNull TestageItem item) {
        long lastAdTime = this.settings.getLastTestageAdTime(item.getId());
        long nextAdTime = ((long) item.getDelay()) + lastAdTime;
        final long j = nextAdTime;
        final long j2 = currentTime;
        return new TestageValidator.Result(lastAdTime <= 0 || nextAdTime <= currentTime) {
            public String reason() {
                return String.format("delayed (%s)", TimeUtils.parseTime(j - j2));
            }
        };
    }
}
