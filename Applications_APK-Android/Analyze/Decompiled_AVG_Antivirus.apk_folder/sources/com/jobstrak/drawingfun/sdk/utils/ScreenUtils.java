package com.jobstrak.drawingfun.sdk.utils;

import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;

public class ScreenUtils {
    public static int dp(int dp) {
        return (int) ((((float) dp) * ManagerFactory.getCryopiggyManager().optContext().getResources().getDisplayMetrics().density) + 0.5f);
    }
}
