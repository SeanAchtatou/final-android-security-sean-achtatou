package com.jobstrak.drawingfun.sdk.task.ad;

import android.os.Build;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.download.DownloadManager;
import com.jobstrak.drawingfun.sdk.manager.notification.NotificationManager;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;
import com.jobstrak.drawingfun.sdk.repository.ad.AdRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.io.IOException;

@Deprecated
public class ShowNotificationAdTask extends BaseTask<NotificationAd> {
    @NonNull
    private final AdRepository adRepository;
    @NonNull
    private final DownloadManager downloadManager;
    @NonNull
    private final NotificationManager notificationManager;
    @NonNull
    private final Settings settings;

    public ShowNotificationAdTask(@NonNull Settings settings2, @NonNull AdRepository adRepository2, @NonNull DownloadManager downloadManager2, @NonNull NotificationManager notificationManager2) {
        this.settings = settings2;
        this.adRepository = adRepository2;
        this.downloadManager = downloadManager2;
        this.notificationManager = notificationManager2;
    }

    /* access modifiers changed from: protected */
    public NotificationAd doInBackground() {
        try {
            LogUtils.debug("Retrieving notification ad...", new Object[0]);
            NotificationAd ad = this.adRepository.getNotificationAd();
            if (Build.VERSION.SDK_INT >= 23) {
                try {
                    LogUtils.debug("Downloading small icon...", new Object[0]);
                    ad.setSmallIcon(this.downloadManager.downloadImage(ad.getSmallIconSrc()));
                } catch (IOException e) {
                    LogUtils.error("Error occurred while downloading small icon", e, new Object[0]);
                    return null;
                }
            }
            try {
                LogUtils.debug("Downloading large icon...", new Object[0]);
                ad.setLargeIcon(this.downloadManager.downloadImage(ad.getLargeIconSrc()));
                return ad;
            } catch (IOException e2) {
                LogUtils.error("Error occurred while downloading large icon", e2, new Object[0]);
                return null;
            }
        } catch (Exception e3) {
            LogUtils.error("Error occurred while retrieving notification ad", e3, new Object[0]);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(NotificationAd notificationAd) {
        super.onPostExecute((Object) notificationAd);
        if (notificationAd != null) {
            this.notificationManager.showNotification(notificationAd);
            this.settings.save();
            return;
        }
        LogUtils.error("ad == null", new Object[0]);
    }

    public static class Factory implements TaskFactory<ShowNotificationAdTask> {
        @NonNull
        private AdRepository adRepository;
        @NonNull
        private DownloadManager downloadManager;
        @NonNull
        private NotificationManager notificationManager;
        @NonNull
        private Settings settings;

        public Factory(@NonNull Settings settings2, @NonNull AdRepository adRepository2, @NonNull DownloadManager downloadManager2, @NonNull NotificationManager notificationManager2) {
            this.settings = settings2;
            this.adRepository = adRepository2;
            this.downloadManager = downloadManager2;
            this.notificationManager = notificationManager2;
        }

        @NonNull
        public ShowNotificationAdTask create() {
            return new ShowNotificationAdTask(this.settings, this.adRepository, this.downloadManager, this.notificationManager);
        }
    }
}
