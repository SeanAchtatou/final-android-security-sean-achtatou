package com.jobstrak.drawingfun.sdk.service.detector;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.service.validator.device.DuplicateServiceValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.NetworkStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.ConfigStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitialDelayValidator;
import com.jobstrak.drawingfun.sdk.service.validator.unlock.UnlockAdEnableStateValidator;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.ad.ShowLinkAdTask;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class UnlockAdDetector extends Detector {
    @NonNull
    private final TaskFactory<ShowLinkAdTask> showLinkAdTaskTaskFactory;
    @NonNull
    private final Validator[] validators;

    public UnlockAdDetector(@NonNull Config config, @NonNull Settings settings, @NonNull TaskFactory<ShowLinkAdTask> showLinkAdTaskTaskFactory2) {
        super(config, settings);
        this.validators = new Validator[]{new DuplicateServiceValidator(), new InitValidator(config, settings), new ConfigStateValidator(settings), new InitialDelayValidator(config, settings), new NetworkStateValidator(config), new UnlockAdEnableStateValidator(config)};
        this.showLinkAdTaskTaskFactory = showLinkAdTaskTaskFactory2;
    }

    public void tick(@Nullable Detector.Delegate delegate) {
    }

    public boolean detect(@Nullable Detector.Delegate delegate) {
        LogUtils.debug();
        for (Validator validator : this.validators) {
            if (!validator.validate(delegate.getCurrentTime())) {
                LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validator.getReason());
                return false;
            }
        }
        int unlockAdCount = getConfig().getUnlockAdCount();
        int unlockAdDelay = getConfig().getUnlockAdDelay();
        int currentUnlockAdCount = getSettings().getCurrentUnlockAdCount();
        LogUtils.debug("+ [%s, %s, %s]", Integer.valueOf(unlockAdCount), Integer.valueOf(unlockAdDelay), Integer.valueOf(currentUnlockAdCount));
        getSettings().setCurrentUnlockAdCount(currentUnlockAdCount + 1);
        getSettings().save();
        if (unlockAdDelay <= 0 || (currentUnlockAdCount + 1) % unlockAdDelay == 0) {
            if (unlockAdCount > 0) {
                if ((unlockAdDelay > 0 ? currentUnlockAdCount / unlockAdDelay : currentUnlockAdCount) >= unlockAdCount) {
                    LogUtils.debug("Limit per day is reached", new Object[0]);
                    return false;
                }
            }
            getSettings().setNextLinkAdTime(delegate.getCurrentTime() + getConfig().getBrowserAdDelay());
            getSettings().save();
            LogUtils.debug("Showing...", new Object[0]);
            this.showLinkAdTaskTaskFactory.create().execute(new Void[0]);
            return true;
        }
        LogUtils.debug("Delayed", new Object[0]);
        return false;
    }
}
