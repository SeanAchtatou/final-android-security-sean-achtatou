package com.jobstrak.drawingfun.sdk.service.alarm;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;

public abstract class Alarm {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;

    /* access modifiers changed from: protected */
    public abstract void execute(long j);

    /* access modifiers changed from: protected */
    public abstract boolean isNeedExecute(long j);

    public Alarm(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    @NonNull
    public final Config getConfig() {
        return this.config;
    }

    @NonNull
    public final Settings getSettings() {
        return this.settings;
    }

    public final void executeIfNeeded(long currentTime) {
        if (isNeedExecute(currentTime)) {
            execute(currentTime);
        }
    }
}
