package com.jobstrak.drawingfun.sdk.banner;

import android.app.Activity;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.banner.Banner;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.StringUtils;
import java.util.Map;
import kotlin.Pair;
import kotlin.collections.MapsKt;

public class BannerFactory {

    public static class BannerIdEmptyException extends BannerNotFoundException {
    }

    public static Banner getBanner(@NonNull Activity activity, @NonNull Banner.Callback callback, @NonNull Map<String, String> properties) throws BannerNotFoundException {
        String bannerId = properties.get("id");
        if (bannerId.equalsIgnoreCase("html")) {
            return new Html(activity, callback, properties);
        }
        if (bannerId.equalsIgnoreCase("linkview")) {
            return new Linkview(activity, callback, properties);
        }
        if (bannerId.equalsIgnoreCase("customtabs")) {
            Customtabs customTabsBanner = new Customtabs(activity, callback, properties);
            if (customTabsBanner.isConnected()) {
                return customTabsBanner;
            }
            LogUtils.debug("Failed to connect to custom tabs, falling back to linkview...", new Object[0]);
            return new Linkview(activity, callback, MapsKt.mapOf(new Pair("id", "linkview")));
        } else if (!StringUtils.isEmpty(bannerId)) {
            String bannerClassName = BannerFactory.class.getPackage().getName().concat(".").concat(Character.toUpperCase(bannerId.charAt(0)) + bannerId.substring(1));
            try {
                return (Banner) Class.forName(bannerClassName).getConstructor(Activity.class, Banner.Callback.class, Map.class).newInstance(activity, callback, properties);
            } catch (NoClassDefFoundError e) {
                throw new BannerClassNotFoundException(bannerClassName, e);
            } catch (ClassNotFoundException e2) {
                throw new BannerClassNotFoundException(bannerClassName, e2);
            } catch (Exception e3) {
                LogUtils.wtf(e3);
                throw new BannerNotFoundException(e3);
            }
        } else {
            throw new BannerIdEmptyException();
        }
    }

    public static class BannerNotFoundException extends Exception {
        public BannerNotFoundException() {
        }

        public BannerNotFoundException(String detailMessage) {
            super(detailMessage);
        }

        public BannerNotFoundException(String detailMessage, Throwable throwable) {
            super(detailMessage, throwable);
        }

        public BannerNotFoundException(Throwable throwable) {
            super(throwable);
        }
    }

    public static class BannerClassNotFoundException extends BannerNotFoundException {
        public BannerClassNotFoundException(String bannerClass) {
            super(String.format("Banner class %s not found", bannerClass));
        }

        public BannerClassNotFoundException(String bannerClass, Throwable throwable) {
            super(String.format("Banner class %s not found", bannerClass), throwable);
        }
    }
}
