package com.jobstrak.drawingfun.sdk.repository.stat;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.gson.ExclusionStrategy;
import com.jobstrak.drawingfun.lib.gson.FieldAttributes;
import com.jobstrak.drawingfun.lib.gson.GsonBuilder;
import com.jobstrak.drawingfun.lib.okhttp3.RequestBody;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.model.Stat;
import java.util.List;

public class CryopiggyStatRepository implements StatRepository {
    @NonNull
    private final RequestManager requestManager;
    /* access modifiers changed from: private */
    public final boolean sendExtra;
    @NonNull
    private final UrlManager urlManager;

    public CryopiggyStatRepository(@NonNull UrlManager urlManager2, @NonNull RequestManager requestManager2, boolean sendExtra2) {
        this.urlManager = urlManager2;
        this.requestManager = requestManager2;
        this.sendExtra = sendExtra2;
    }

    public void addEvent(@NonNull String type) throws Exception {
        String url = this.urlManager.create("/v2/event").toString();
        UrlManager.Builder<RequestBody> builder = this.urlManager.createBody();
        builder.addParameters(UrlManager.Parameter.CLIENT_ID, UrlManager.Parameter.SDK_VER);
        builder.addCustomParameter("type", type);
        if (this.sendExtra) {
            builder.addParameters(UrlManager.Parameter.EXTRA);
        }
        this.requestManager.sendRequest(url, "POST", builder.build());
    }

    public void addStatsFromArray(@NonNull List<Stat> stats) throws Exception {
        RequestBody body = RequestBody.create(RequestManager.JSON, new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getName().equals(UrlManager.Parameter.EXTRA) && !CryopiggyStatRepository.this.sendExtra;
            }

            public boolean shouldSkipClass(Class<?> cls) {
                return false;
            }
        }).create().toJson(stats));
        this.requestManager.sendRequest(this.urlManager.create("/v2/stat").build(), "PUT", body);
    }
}
