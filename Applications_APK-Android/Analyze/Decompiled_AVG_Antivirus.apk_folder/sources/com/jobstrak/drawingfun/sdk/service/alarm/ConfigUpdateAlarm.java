package com.jobstrak.drawingfun.sdk.service.alarm;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.server.UpdateConfigTask;
import com.jobstrak.drawingfun.sdk.utils.NetworkUtils;

public class ConfigUpdateAlarm extends Alarm {
    @NonNull
    private final TaskFactory<UpdateConfigTask> updateConfigTask;

    public ConfigUpdateAlarm(@NonNull Config config, @NonNull Settings settings, @NonNull TaskFactory<UpdateConfigTask> updateConfigTask2) {
        super(config, settings);
        this.updateConfigTask = updateConfigTask2;
    }

    public boolean isNeedExecute(long currentTime) {
        return isRegistered() && isItTime(currentTime) && isConnected();
    }

    public void execute(long currentTime) {
        this.updateConfigTask.create().execute(new Void[0]);
        setNextConfigUpdateTime(currentTime);
    }

    private boolean isRegistered() {
        return getSettings().getClientId() != null;
    }

    private boolean isItTime(long currentTime) {
        return getSettings().getNextConfigUpdateTime() <= currentTime;
    }

    private boolean isConnected() {
        return NetworkUtils.isConnected(getConfig().isOnlyFastConnection());
    }

    private void setNextConfigUpdateTime(long currentTime) {
        getSettings().setNextConfigUpdateTime(900000 + currentTime);
        getSettings().save();
    }
}
