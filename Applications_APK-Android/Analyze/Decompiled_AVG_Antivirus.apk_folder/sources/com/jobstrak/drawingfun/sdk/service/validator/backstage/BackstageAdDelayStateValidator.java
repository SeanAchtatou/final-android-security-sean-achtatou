package com.jobstrak.drawingfun.sdk.service.validator.backstage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class BackstageAdDelayStateValidator extends Validator {
    @NonNull
    private final Settings settings;

    public BackstageAdDelayStateValidator(@NonNull Settings settings2) {
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        return this.settings.getNextBackstageAdTime() <= currentTime;
    }

    public String getReason() {
        return String.format("backstage ad delayed (%s left)", TimeUtils.parseTime(this.settings.getNextBackstageAdTime() - TimeUtils.getCurrentTime()));
    }
}
