package com.jobstrak.drawingfun.sdk.manager.backstage.webview.client;

import android.webkit.WebViewClient;

public abstract class BackstageWebViewClient extends WebViewClient {
    public abstract void setEnabled(boolean z);
}
