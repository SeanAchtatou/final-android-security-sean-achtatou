package com.jobstrak.drawingfun.sdk.waterfall;

import java.util.LinkedList;
import java.util.Queue;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u0000 \r2\u00020\u0001:\u0004\f\r\u000e\u000fB\u0019\b\u0002\u0012\u0010\b\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0005J\u0006\u0010\u0006\u001a\u00020\u0004J\u0006\u0010\u0007\u001a\u00020\bJ\r\u0010\t\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\nJ\r\u0010\u000b\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\nR\u0016\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/waterfall/Waterfall;", "", "queue", "Ljava/util/Queue;", "", "(Ljava/util/Queue;)V", "getPositionsCount", "hasNextPosition", "", "peekNextPosition", "()Ljava/lang/Integer;", "pollNextPosition", "Adapter", "Companion", "Gateway", "Type", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: Waterfall.kt */
public final class Waterfall {
    public static final Companion Companion = new Companion(null);
    private final Queue<Integer> queue;

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H&J\b\u0010\u0006\u001a\u00020\u0003H&J\u0017\u0010\u0007\u001a\u0004\u0018\u00010\u00032\u0006\u0010\b\u001a\u00020\u0003H&¢\u0006\u0002\u0010\t¨\u0006\n"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/waterfall/Waterfall$Adapter;", "", "getId", "", "position", "getImpressionsLimit", "getItemsCount", "getPosition", "id", "(I)Ljava/lang/Integer;", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Waterfall.kt */
    public interface Adapter {
        int getId(int i);

        int getImpressionsLimit(int i);

        int getItemsCount();

        @Nullable
        Integer getPosition(int i);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H&J\u000f\u0010\u0005\u001a\u0004\u0018\u00010\u0003H&¢\u0006\u0002\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/waterfall/Waterfall$Gateway;", "", "getImpressionsCount", "", "id", "getLastRequestedItemId", "()Ljava/lang/Integer;", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Waterfall.kt */
    public interface Gateway {
        int getImpressionsCount(int i);

        @Nullable
        Integer getLastRequestedItemId();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/waterfall/Waterfall$Type;", "", "(Ljava/lang/String;I)V", "FLAT", "ROUNDROBIN", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Waterfall.kt */
    public enum Type {
        FLAT,
        ROUNDROBIN
    }

    @JvmStatic
    @NotNull
    public static final Waterfall generate(@NotNull Type type, @NotNull Adapter adapter, @NotNull Gateway gateway) {
        return Companion.generate(type, adapter, gateway);
    }

    private Waterfall(Queue<Integer> queue2) {
        this.queue = queue2;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    /* synthetic */ Waterfall(Queue queue2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : queue2);
    }

    public /* synthetic */ Waterfall(Queue queue2, DefaultConstructorMarker $constructor_marker) {
        this(queue2);
    }

    @Nullable
    public final Integer peekNextPosition() {
        Queue<Integer> queue2 = this.queue;
        if (queue2 != null) {
            return queue2.peek();
        }
        return null;
    }

    @Nullable
    public final Integer pollNextPosition() {
        Queue<Integer> queue2 = this.queue;
        if (queue2 != null) {
            return queue2.poll();
        }
        return null;
    }

    public final boolean hasNextPosition() {
        Queue<Integer> queue2 = this.queue;
        if (queue2 != null) {
            return !queue2.isEmpty();
        }
        return false;
    }

    public final int getPositionsCount() {
        Queue<Integer> queue2 = this.queue;
        if (queue2 != null) {
            return queue2.size();
        }
        return 0;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007¨\u0006\u000b"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/waterfall/Waterfall$Companion;", "", "()V", "generate", "Lcom/jobstrak/drawingfun/sdk/waterfall/Waterfall;", "type", "Lcom/jobstrak/drawingfun/sdk/waterfall/Waterfall$Type;", "adapter", "Lcom/jobstrak/drawingfun/sdk/waterfall/Waterfall$Adapter;", "gateway", "Lcom/jobstrak/drawingfun/sdk/waterfall/Waterfall$Gateway;", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Waterfall.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }

        @JvmStatic
        @NotNull
        public final Waterfall generate(@NotNull Type type, @NotNull Adapter adapter, @NotNull Gateway gateway) {
            Integer position;
            Intrinsics.checkParameterIsNotNull(type, "type");
            Intrinsics.checkParameterIsNotNull(adapter, "adapter");
            Intrinsics.checkParameterIsNotNull(gateway, "gateway");
            int itemsCount = adapter.getItemsCount();
            if (itemsCount == 0) {
                return new Waterfall(null, 1, null);
            }
            int lastPosition = -1;
            if (type == Type.FLAT) {
                Integer lastRequestedItemId = gateway.getLastRequestedItemId();
                lastPosition = (lastRequestedItemId == null || (position = adapter.getPosition(lastRequestedItemId.intValue())) == null) ? -1 : position.intValue();
            }
            int firstNextPosition = lastPosition + 1;
            if (firstNextPosition >= itemsCount) {
                firstNextPosition = 0;
            }
            LinkedList queue = new LinkedList();
            int nextPosition = firstNextPosition;
            do {
                int limit = adapter.getImpressionsLimit(nextPosition);
                if (limit <= 0 || gateway.getImpressionsCount(adapter.getId(nextPosition)) < limit) {
                    queue.add(Integer.valueOf(nextPosition));
                }
                nextPosition++;
                if (nextPosition >= itemsCount) {
                    nextPosition = 0;
                    continue;
                }
            } while (nextPosition != firstNextPosition);
            return new Waterfall(queue, null);
        }
    }
}
