package com.jobstrak.drawingfun.sdk.service.validator.testage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.model.TestageItem;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageValidator;

public class TestageClicksPerDayLimitValidator implements TestageValidator {
    @NonNull
    private final Settings settings;

    public TestageClicksPerDayLimitValidator(@NonNull Settings settings2) {
        this.settings = settings2;
    }

    public TestageValidator.Result validate(long currentTime, @NonNull TestageItem item) {
        final int limit = item.getClicksCount();
        return new TestageValidator.Result(limit <= 0 || limit > this.settings.getCurrentTestageClicksCount(item.getId())) {
            public String reason() {
                return String.format("daily limit exceeded (%d)", Integer.valueOf(limit));
            }
        };
    }
}
