package com.jobstrak.drawingfun.sdk.manager.shortcut;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.IconAd;

public interface ShortcutManager {
    void addShortcut(@NonNull IconAd iconAd);
}
