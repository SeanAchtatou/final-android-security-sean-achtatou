package com.jobstrak.drawingfun.sdk.utils;

import com.jobstrak.drawingfun.lib.gson.Gson;
import com.jobstrak.drawingfun.lib.gson.GsonBuilder;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class JsonUtils {
    private static final Gson gson;

    static {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.excludeFieldsWithModifiers(128);
        gson = gsonBuilder.create();
    }

    public static String toJSON(Object object) {
        return gson.toJson(object);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.jobstrak.drawingfun.lib.gson.Gson.fromJson(java.lang.String, java.lang.Class):T
     arg types: [java.lang.String, java.lang.Class<T>]
     candidates:
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(com.jobstrak.drawingfun.lib.gson.JsonElement, java.lang.Class):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(com.jobstrak.drawingfun.lib.gson.JsonElement, java.lang.reflect.Type):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(com.jobstrak.drawingfun.lib.gson.stream.JsonReader, java.lang.reflect.Type):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(java.lang.String, java.lang.Class):T */
    public static <T> T fromJSON(String json, Class<T> clazz) {
        return gson.fromJson(json, (Class) clazz);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.jobstrak.drawingfun.lib.gson.Gson.fromJson(java.lang.String, java.lang.Class):T
     arg types: [java.lang.String, java.lang.Class<?>]
     candidates:
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(com.jobstrak.drawingfun.lib.gson.JsonElement, java.lang.Class):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(com.jobstrak.drawingfun.lib.gson.JsonElement, java.lang.reflect.Type):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(com.jobstrak.drawingfun.lib.gson.stream.JsonReader, java.lang.reflect.Type):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
      com.jobstrak.drawingfun.lib.gson.Gson.fromJson(java.lang.String, java.lang.Class):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<T>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    public static <T> T[] arrayFromGson(String json, Class<T> clazz) {
        return (Object[]) gson.fromJson(json, (Class) Array.newInstance((Class<?>) clazz, 0).getClass());
    }

    public static <T> List<T> listFromGson(String json, Class<T> clazz) {
        return Arrays.asList(arrayFromGson(json, clazz));
    }
}
