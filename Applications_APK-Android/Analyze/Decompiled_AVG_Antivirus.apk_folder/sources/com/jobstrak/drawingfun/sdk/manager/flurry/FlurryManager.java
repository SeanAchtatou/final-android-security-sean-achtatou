package com.jobstrak.drawingfun.sdk.manager.flurry;

import android.content.Context;
import androidx.annotation.NonNull;

public interface FlurryManager {
    void init(@NonNull Context context, @NonNull String str);

    void onEndSession(@NonNull Context context);

    void onStartSession(@NonNull Context context);
}
