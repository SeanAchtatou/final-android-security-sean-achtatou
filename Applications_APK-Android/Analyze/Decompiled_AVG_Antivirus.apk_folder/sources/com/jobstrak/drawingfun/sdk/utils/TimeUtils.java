package com.jobstrak.drawingfun.sdk.utils;

import android.util.Base64;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {
    public static long getCurrentTime() {
        long currentTime = System.currentTimeMillis();
        LogUtils.debug("Current time is " + currentTime, new Object[0]);
        return currentTime;
    }

    public static void setCurrentTime(long currentTime) {
    }

    public static String parseTime(long time) {
        String pattern;
        if (time < 1000) {
            pattern = new String(Base64.decode("PCAxcw==", 0));
        } else if (time < 60000) {
            pattern = new String(Base64.decode("c3Mncyc=", 0));
        } else if (time < 3600000) {
            pattern = new String(Base64.decode("bW0nbScgc3Mncyc=", 0));
        } else {
            pattern = new String(Base64.decode("SEgnaCcgbW0nbScgc3Mncyc=", 0));
        }
        return new SimpleDateFormat(pattern).format(new Date(time));
    }

    public static String formatDuration(long durationMs) {
        long durationMs2 = durationMs / 1000;
        long seconds = durationMs2 % 60;
        long minutes = (durationMs2 / 60) % 60;
        long hours = durationMs2 / 3600;
        if (hours > 0) {
            return String.format("%02d:%02d:%02d", Long.valueOf(hours), Long.valueOf(minutes), Long.valueOf(seconds));
        }
        return String.format("%02d:%02d", Long.valueOf(minutes), Long.valueOf(seconds));
    }

    public static long getStartOfCurrentDay() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar.getTimeInMillis();
    }
}
