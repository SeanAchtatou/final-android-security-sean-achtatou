package com.jobstrak.drawingfun.sdk.data;

import com.jobstrak.drawingfun.sdk.Cryopiggy;

public final class SdkConfig {
    public static String getSdkName() {
        return Cryopiggy.class.getSimpleName();
    }
}
