package com.jobstrak.drawingfun.sdk.manager.testage.js;

import android.webkit.JavascriptInterface;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.activity.TestageActivity;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.backstage.js.BackstageJsInterface;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class TestageJsInterface implements BackstageJsInterface {
    @NonNull
    private Settings settings;
    @NonNull
    private TestageActivity testageActivity;
    private int testageItemId;

    public TestageJsInterface(@NonNull TestageActivity activity, @NonNull Settings settings2, int testageItemId2) {
        this.testageActivity = activity;
        this.settings = settings2;
        this.testageItemId = testageItemId2;
    }

    @JavascriptInterface
    public void onSuccess() {
        LogUtils.debug();
        this.testageActivity.finish();
        this.testageActivity = null;
    }

    @JavascriptInterface
    public void onError() {
        LogUtils.debug();
        this.settings.decCurrentTestageClicksCount(this.testageItemId);
        this.settings.save();
        this.testageActivity.finish();
        this.testageActivity = null;
    }
}
