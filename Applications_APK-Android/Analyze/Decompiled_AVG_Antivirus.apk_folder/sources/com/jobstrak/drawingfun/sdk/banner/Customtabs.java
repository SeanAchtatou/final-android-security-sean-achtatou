package com.jobstrak.drawingfun.sdk.banner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import com.jobstrak.drawingfun.lib.androidx.browser.customtabs.CustomTabsClient;
import com.jobstrak.drawingfun.lib.androidx.browser.customtabs.CustomTabsIntent;
import com.jobstrak.drawingfun.lib.androidx.browser.customtabs.CustomTabsService;
import com.jobstrak.drawingfun.sdk.banner.Banner;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.model.HTMLAd;
import com.jobstrak.drawingfun.sdk.repository.RepositoryFactory;
import com.jobstrak.drawingfun.sdk.repository.ad.AdRepository;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.GlobalScope;
import kotlinx.coroutines.Job;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000S\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004*\u0001\u0015\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ\b\u0010\u0019\u001a\u00020\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u001aH\u0016J\b\u0010\u001c\u001a\u00020\u001aH\u0016R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\n \u000e*\u0004\u0018\u00010\r0\rX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0013R\u0010\u0010\u0014\u001a\u00020\u0015X\u0004¢\u0006\u0004\n\u0002\u0010\u0016R\u0016\u0010\u0017\u001a\n \u000e*\u0004\u0018\u00010\u00180\u0018X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/banner/Customtabs;", "Lcom/jobstrak/drawingfun/sdk/banner/Banner;", "activity", "Landroid/app/Activity;", "callback", "Lcom/jobstrak/drawingfun/sdk/banner/Banner$Callback;", "properties", "", "", "(Landroid/app/Activity;Lcom/jobstrak/drawingfun/sdk/banner/Banner$Callback;Ljava/util/Map;)V", "ad", "Lcom/jobstrak/drawingfun/sdk/model/HTMLAd;", "adRepository", "Lcom/jobstrak/drawingfun/sdk/repository/ad/AdRepository;", "kotlin.jvm.PlatformType", "client", "Lcom/jobstrak/drawingfun/lib/androidx/browser/customtabs/CustomTabsClient;", "isConnected", "", "()Z", "serviceConnection", "com/jobstrak/drawingfun/sdk/banner/Customtabs$serviceConnection$1", "Lcom/jobstrak/drawingfun/sdk/banner/Customtabs$serviceConnection$1;", "urlManager", "Lcom/jobstrak/drawingfun/sdk/manager/url/UrlManager;", "load", "", "removeCallback", "show", "Companion", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: Customtabs.kt */
public final class Customtabs extends Banner {
    public static final Companion Companion = new Companion(null);
    /* access modifiers changed from: private */
    public static boolean isShown;
    /* access modifiers changed from: private */
    public HTMLAd ad;
    /* access modifiers changed from: private */
    public final AdRepository adRepository = RepositoryFactory.getAdRepository(this.urlManager, ManagerFactory.createRequestManager());
    /* access modifiers changed from: private */
    public CustomTabsClient client;
    private final boolean isConnected;
    private final Customtabs$serviceConnection$1 serviceConnection = new Customtabs$serviceConnection$1(this);
    private final UrlManager urlManager = ManagerFactory.createUrlManager(Config.getInstance(), Settings.getInstance());

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Customtabs(@NotNull Activity activity, @NotNull Banner.Callback callback, @NotNull Map<String, String> properties) {
        super(activity, callback, properties);
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        Intrinsics.checkParameterIsNotNull(callback, "callback");
        Intrinsics.checkParameterIsNotNull(properties, "properties");
        String packageName = Companion.findCustomTabsProviderPackageName(activity);
        boolean z = false;
        LogUtils.debug("Custom tabs provider package name: " + packageName, new Object[0]);
        this.isConnected = packageName != null ? CustomTabsClient.bindCustomTabsService(activity, packageName, this.serviceConnection) : z;
    }

    public final boolean isConnected() {
        return this.isConnected;
    }

    public void load() {
        LogUtils.debug();
        if (this.isConnected) {
            fireOnRequest();
            Job unused = BuildersKt__Builders_commonKt.launch$default(GlobalScope.INSTANCE, Dispatchers.getMain(), null, new Customtabs$load$1(this, null), 2, null);
            return;
        }
        fireOnFail("isConnected == false");
    }

    public void show() {
        LogUtils.debug();
        CustomTabsClient customTabsClient = this.client;
        if (customTabsClient != null) {
            if (customTabsClient == null) {
                Intrinsics.throwNpe();
            }
            CustomTabsIntent intent = new CustomTabsIntent.Builder(customTabsClient.newSession(new Customtabs$show$session$1(this))).build();
            Context activity = getActivity();
            HTMLAd hTMLAd = this.ad;
            if (hTMLAd == null) {
                Intrinsics.throwNpe();
            }
            intent.launchUrl(activity, Uri.parse(hTMLAd.getUrl()));
            return;
        }
        fireOnFail("client == null");
    }

    public void removeCallback() {
        super.removeCallback();
        getActivity().unbindService(this.serviceConnection);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\u0004\u0018\u00010\t*\u00020\nH\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0003\u0010\u0005\"\u0004\b\u0006\u0010\u0007¨\u0006\u000b"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/banner/Customtabs$Companion;", "", "()V", "isShown", "", "()Z", "setShown", "(Z)V", "findCustomTabsProviderPackageName", "", "Landroid/content/Context;", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Customtabs.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }

        public final boolean isShown() {
            return Customtabs.isShown;
        }

        public final void setShown(boolean z) {
            Customtabs.isShown = z;
        }

        /* JADX INFO: Multiple debug info for r5v4 java.lang.Iterable: [D('$this$filter$iv' java.lang.Iterable), D('$this$map$iv' java.lang.Iterable)] */
        /* JADX INFO: Multiple debug info for r5v6 java.util.List: [D('customTabsProviders' java.util.List), D('$this$map$iv' java.lang.Iterable)] */
        /* access modifiers changed from: private */
        public final String findCustomTabsProviderPackageName(@NotNull Context $this$findCustomTabsProviderPackageName) {
            ActivityInfo activityInfo;
            PackageManager packageManager = $this$findCustomTabsProviderPackageName.getPackageManager();
            Intent viewActionIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://example.com"));
            ResolveInfo resolveActivity = packageManager.resolveActivity(viewActionIntent, 0);
            String str = null;
            String defaultProvider = (resolveActivity == null || (activityInfo = resolveActivity.activityInfo) == null) ? null : activityInfo.packageName;
            List<ResolveInfo> $this$filter$iv = packageManager.queryIntentActivities(viewActionIntent, 0);
            Intrinsics.checkExpressionValueIsNotNull($this$filter$iv, "packageManager.queryInte…ties(viewActionIntent, 0)");
            Collection destination$iv$iv = new ArrayList();
            for (Object element$iv$iv : $this$filter$iv) {
                if (packageManager.resolveService(new Intent(CustomTabsService.ACTION_CUSTOM_TABS_CONNECTION).setPackage(((ResolveInfo) element$iv$iv).activityInfo.packageName), 0) != null) {
                    destination$iv$iv.add(element$iv$iv);
                }
            }
            Iterable<ResolveInfo> $this$map$iv = (List) destination$iv$iv;
            Collection destination$iv$iv2 = new ArrayList(CollectionsKt.collectionSizeOrDefault($this$map$iv, 10));
            for (ResolveInfo it : $this$map$iv) {
                destination$iv$iv2.add(it.activityInfo.packageName);
            }
            List customTabsProviders = (List) destination$iv$iv2;
            LogUtils.debug("Custom tabs providers: " + CollectionsKt.joinToString$default(customTabsProviders, null, null, null, 0, null, null, 63, null), new Object[0]);
            Iterator it2 = customTabsProviders.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Object next = it2.next();
                if (Intrinsics.areEqual((String) next, "com.android.chrome")) {
                    str = next;
                    break;
                }
            }
            String customTabsProvider = str;
            if (customTabsProvider == null && customTabsProviders.contains(defaultProvider)) {
                customTabsProvider = defaultProvider;
            }
            return customTabsProvider != null ? customTabsProvider : (String) CollectionsKt.firstOrNull(customTabsProviders);
        }
    }
}
