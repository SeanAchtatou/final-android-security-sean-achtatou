package com.jobstrak.drawingfun.sdk.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.FrameLayout;
import com.jobstrak.drawingfun.lib.homewatcher.HomeWatcher;
import com.jobstrak.drawingfun.lib.homewatcher.OnHomePressedListener;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.StringUtils;
import java.util.List;

public abstract class BaseActivity extends Activity implements OnHomePressedListener {
    private static final int COUNT_DOWN_INTERVAL = 15000;
    private static final int MILLIS_IN_FUTURE = 900000;
    private static volatile int showing = 0;
    /* access modifiers changed from: private */
    public final Config config = Config.getInstance();
    private FrameLayout contentView;
    private boolean destroyed = false;
    private FinishTimer finishTimer;
    private HomeWatcher homeWatcher;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.contentView = new FrameLayout(this);
        this.contentView.setBackgroundResource(17301673);
        setContentView(this.contentView, new FrameLayout.LayoutParams(-1, -1));
        this.homeWatcher = new HomeWatcher(this, this);
        this.homeWatcher.startWatch();
        showing++;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        HomeWatcher homeWatcher2 = this.homeWatcher;
        if (homeWatcher2 != null) {
            homeWatcher2.stopWatch();
        }
        FinishTimer finishTimer2 = this.finishTimer;
        if (finishTimer2 != null) {
            finishTimer2.cancel();
        }
        showing--;
        this.destroyed = true;
    }

    /* access modifiers changed from: protected */
    public FrameLayout getContentView() {
        return this.contentView;
    }

    public static boolean isShowing() {
        return showing > 0;
    }

    /* access modifiers changed from: protected */
    public void startFinishTimer() {
        LogUtils.debug();
        this.finishTimer = new FinishTimer();
        this.finishTimer.start();
    }

    public void onHomePressed() {
        LogUtils.debug();
        finish();
    }

    public void onRecentAppsPressed() {
        LogUtils.debug();
        finish();
    }

    public void finish() {
        LogUtils.debug();
        if (!this.destroyed) {
            super.finish();
        }
    }

    private class FinishTimer extends CountDownTimer {
        private final List<String> restrictedApps = BaseActivity.this.config.getRestrictedApps();

        public FinishTimer() {
            super(BaseActivity.this.config.getAdMaxDisplayTime(), BaseActivity.this.config.getAdMaxDisplayTime() / 60);
        }

        public void onTick(long millisUntilFinished) {
            AndroidManager androidManager = ManagerFactory.getAndroidManager();
            String foregroundPackage = androidManager.getForegroundPackageName();
            if (StringUtils.wildcardAnyMatch(this.restrictedApps, foregroundPackage) && !androidManager.getPackageName().equals(foregroundPackage)) {
                LogUtils.debug("Restricted " + foregroundPackage + " is in the foreground now", new Object[0]);
                finishActivity();
            }
        }

        public void onFinish() {
            LogUtils.debug();
            finishActivity();
        }

        private void finishActivity() {
            BaseActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    BaseActivity.this.finish();
                }
            });
        }
    }
}
