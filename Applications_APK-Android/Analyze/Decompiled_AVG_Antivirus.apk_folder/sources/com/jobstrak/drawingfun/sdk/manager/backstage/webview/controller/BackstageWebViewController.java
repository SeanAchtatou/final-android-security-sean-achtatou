package com.jobstrak.drawingfun.sdk.manager.backstage.webview.controller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.client.BackstageWebViewClient;

public interface BackstageWebViewController {
    void clickOnElement(@NonNull String str);

    void clickOnElement(@NonNull String str, @NonNull String str2);

    void clickOnElement(@NonNull String str, @NonNull String str2, int i);

    void clickOnLongestLink();

    @Nullable
    BackstageWebViewClient getWebViewClient();

    void loadUrl(@NonNull String str);

    void setChecked(@NonNull String str, @NonNull String str2, boolean z);

    void setWebViewClient(@Nullable BackstageWebViewClient backstageWebViewClient);
}
