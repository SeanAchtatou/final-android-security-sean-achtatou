package com.jobstrak.drawingfun.sdk.service.validator.lockscreen;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class LockscreenAdEnableStateValidator extends Validator {
    @NonNull
    private final Config config;

    public LockscreenAdEnableStateValidator(@NonNull Config config2) {
        this.config = config2;
    }

    public boolean validate(long currentTime) {
        return this.config.isLockscreenAdEnabled();
    }

    public String getReason() {
        return "lockscreen ad is disabled";
    }
}
