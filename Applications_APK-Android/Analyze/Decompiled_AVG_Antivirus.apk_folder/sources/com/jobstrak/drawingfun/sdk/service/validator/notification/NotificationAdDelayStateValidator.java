package com.jobstrak.drawingfun.sdk.service.validator.notification;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class NotificationAdDelayStateValidator extends Validator {
    @NonNull
    private final Settings settings;

    public NotificationAdDelayStateValidator(@NonNull Settings settings2) {
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        return this.settings.getNextNotificationAdTime() <= currentTime;
    }

    public String getReason() {
        return String.format("notification ad delayed (%s left)", TimeUtils.parseTime(this.settings.getNextNotificationAdTime() - TimeUtils.getCurrentTime()));
    }
}
