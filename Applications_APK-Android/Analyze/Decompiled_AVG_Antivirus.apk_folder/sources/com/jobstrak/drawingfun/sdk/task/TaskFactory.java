package com.jobstrak.drawingfun.sdk.task;

import androidx.annotation.NonNull;

public interface TaskFactory<T> {
    @NonNull
    T create();
}
