package com.jobstrak.drawingfun.sdk.service.validator.backstage.clicks;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BackstageAdClicksDelayStateValidator extends Validator {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;

    public BackstageAdClicksDelayStateValidator(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        int clicksDelay = this.config.getBackstageAdClicksDelay();
        return clicksDelay <= 0 || (this.settings.getCurrentBackstageAdClicksCount() + 1) % clicksDelay == 0;
    }

    public String getReason() {
        int clicksDelay = this.config.getBackstageAdClicksDelay();
        return String.format("backstage ad clicks delayed (%d left)", Integer.valueOf(clicksDelay - ((this.settings.getCurrentBackstageAdClicksCount() + 1) % clicksDelay)));
    }
}
