package com.jobstrak.drawingfun.sdk.manager;

import android.os.Build;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.apk.ApkManager;
import com.jobstrak.drawingfun.sdk.manager.apk.ApkManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.backstage.BackstageManager;
import com.jobstrak.drawingfun.sdk.manager.backstage.BackstageManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.browser.BrowserManager;
import com.jobstrak.drawingfun.sdk.manager.browser.BrowserManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.download.DownloadManager;
import com.jobstrak.drawingfun.sdk.manager.download.DownloadManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.flurry.FlurryManager;
import com.jobstrak.drawingfun.sdk.manager.flurry.FlurryManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.google.GoogleManager;
import com.jobstrak.drawingfun.sdk.manager.google.GoogleManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.lockscreen.LockscreenManager;
import com.jobstrak.drawingfun.sdk.manager.lockscreen.LockscreenManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.notification.MarshmallowNotificationManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.notification.NotificationManager;
import com.jobstrak.drawingfun.sdk.manager.notification.NotificationManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.shortcut.ShortcutManager;
import com.jobstrak.drawingfun.sdk.manager.shortcut.ShortcutManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.testage.TestageManager;
import com.jobstrak.drawingfun.sdk.manager.testage.TestageManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManagerImpl;
import com.jobstrak.drawingfun.sdk.manager.worker.WorkerManager;
import com.jobstrak.drawingfun.sdk.repository.RepositoryFactory;
import com.jobstrak.drawingfun.sdk.repository.system.SystemRepository;
import com.jobstrak.drawingfun.sdk.util.Supplier;
import java.util.Random;
import java.util.UUID;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ManagerFactory {
    private static volatile AndroidManager androidManager = new AndroidManagerImpl(cryopiggyManager, systemRepository, workerManager);
    private static volatile ApkManager apkManager = new ApkManagerImpl(cryopiggyManager);
    private static volatile BackstageManager backstageManager = new BackstageManagerImpl(cryopiggyManager);
    private static volatile BrowserManagerImpl browserManager = new BrowserManagerImpl(androidManager);
    private static volatile CryopiggyManager cryopiggyManager = new CryopiggyManagerImpl();
    private static volatile GoogleManager googleManager = new GoogleManagerImpl(cryopiggyManager);
    private static volatile LockscreenManager lockscreenManager = new LockscreenManagerImpl(cryopiggyManager);
    private static volatile NotificationManager notificationManager;
    private static volatile ShortcutManager shortcutManager = new ShortcutManagerImpl(cryopiggyManager);
    private static volatile SystemRepository systemRepository = RepositoryFactory.getSystemRepository();
    private static volatile TestageManager testageManager = new TestageManagerImpl(cryopiggyManager);
    private static volatile WorkerManager workerManager = new WorkerManager() {
        @NotNull
        public UUID getActiveId(boolean isPeriodic) {
            return null;
        }

        public void setActiveId(boolean isPeriodic, @Nullable UUID id) {
        }

        public void startWorker(boolean isPeriodic) {
        }

        public void startWorkersIfNotRunning() {
        }

        public void stopWorkersIfRunning() {
        }
    };

    static {
        if (Build.VERSION.SDK_INT >= 23) {
            notificationManager = new MarshmallowNotificationManagerImpl(cryopiggyManager, new Random());
        } else {
            notificationManager = new NotificationManagerImpl(cryopiggyManager, new Random());
        }
    }

    public static RequestManager createRequestManager() {
        return new RequestManagerImpl();
    }

    public static DownloadManager createDownloadManager() {
        return new DownloadManagerImpl();
    }

    public static FlurryManager createFlurryManager() {
        return new FlurryManagerImpl();
    }

    public static AndroidManager getAndroidManager() {
        return androidManager;
    }

    public static ShortcutManager getShortcutManager() {
        return shortcutManager;
    }

    public static NotificationManager getNotificationManager() {
        return notificationManager;
    }

    public static LockscreenManager getLockscreenManager() {
        return lockscreenManager;
    }

    public static GoogleManager getGoogleManager() {
        return googleManager;
    }

    public static UrlManager createUrlManager(@NonNull Config config, @NonNull Settings settings) {
        return createUrlManager(config, settings, false);
    }

    public static UrlManager createUrlManager(@NonNull final Config config, @NonNull Settings settings, final boolean reserved) {
        return createUrlManager(settings, new UrlManagerImpl.StoreFactoryImpl(new Supplier<String>() {
            public String get() {
                return reserved ? config.getReservedServer() : config.getServer();
            }
        }));
    }

    public static UrlManager createUrlManager(@NonNull Settings settings, @NonNull UrlManagerImpl.StoreFactory storeFactory) {
        return new UrlManagerImpl(settings, storeFactory, googleManager, androidManager, apkManager);
    }

    public static CryopiggyManager getCryopiggyManager() {
        return cryopiggyManager;
    }

    public static BackstageManager getBackstageManager() {
        return backstageManager;
    }

    public static TestageManager getTestageManager() {
        return testageManager;
    }

    public static ApkManager getApkManager() {
        return apkManager;
    }

    public static BrowserManager getBrowserManager() {
        return browserManager;
    }

    public static BrowserManager.Updater getBrowserManagerUpdater() {
        return browserManager;
    }

    public static WorkerManager getWorkerManager() {
        return workerManager;
    }
}
