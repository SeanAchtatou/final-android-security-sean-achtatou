package com.jobstrak.drawingfun.sdk.repository.system;

import android.app.ActivityManager;
import android.content.Context;
import androidx.annotation.NonNull;

public class LollipopSystemRepository implements SystemRepository {
    public static final int AID_APP = 10000;

    @NonNull
    public String getForegroundPackageName(@NonNull Context context) {
        for (ActivityManager.RunningAppProcessInfo process : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (checkForeground(process)) {
                return process.processName;
            }
        }
        return "";
    }

    private static boolean checkForeground(@NonNull ActivityManager.RunningAppProcessInfo process) {
        return 10000 <= process.uid && process.importance == 100;
    }
}
