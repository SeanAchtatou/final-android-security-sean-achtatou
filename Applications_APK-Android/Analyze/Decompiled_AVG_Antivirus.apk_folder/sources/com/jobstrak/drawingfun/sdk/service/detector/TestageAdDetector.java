package com.jobstrak.drawingfun.sdk.service.detector;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.testage.TestageManager;
import com.jobstrak.drawingfun.sdk.model.TestageExecItem;
import com.jobstrak.drawingfun.sdk.model.TestageItem;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.service.validator.device.DuplicateServiceValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.NetworkStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.ConfigStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitialDelayValidator;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageAdDelayStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageAdPerDayLimitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageAdUrlStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageClicksButtonStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageClicksDelayStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageClicksEnableStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageClicksPerDayLimitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageItemsStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageValidator;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.util.List;

public class TestageAdDetector extends Detector {
    @NonNull
    private final TestageValidator[] testageClicksValidators;
    @NonNull
    private final TestageManager testageManager;
    @NonNull
    private final TestageValidator[] testageValidators;
    @NonNull
    private final Validator[] validators;

    public TestageAdDetector(@NonNull Config config, @NonNull Settings settings, @NonNull TestageManager testageManager2) {
        super(config, settings);
        this.testageManager = testageManager2;
        this.validators = new Validator[]{new DuplicateServiceValidator(), new InitValidator(config, settings), new ConfigStateValidator(settings), new InitialDelayValidator(config, settings), new NetworkStateValidator(config), new TestageItemsStateValidator(config)};
        this.testageValidators = new TestageValidator[]{new TestageAdUrlStateValidator(), new TestageAdPerDayLimitValidator(settings), new TestageAdDelayStateValidator(settings)};
        this.testageClicksValidators = new TestageValidator[]{new TestageClicksEnableStateValidator(), new TestageClicksButtonStateValidator(), new TestageClicksPerDayLimitValidator(settings), new TestageClicksDelayStateValidator(settings)};
    }

    public void tick(Detector.Delegate delegate) {
    }

    public boolean detect(Detector.Delegate delegate) {
        TestageExecItem execItem;
        for (Validator validator : this.validators) {
            if (!validator.validate(delegate.getCurrentTime())) {
                LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validator.getReason());
                return true;
            }
        }
        List<TestageItem> items = getConfig().getTestageItems();
        int size = items.size();
        for (int i = 0; i < size; i++) {
            TestageItem item = items.get(i);
            if (validateTestageItem(delegate.getCurrentTime(), item)) {
                int itemId = item.getId();
                if (validateTestageClicks(delegate.getCurrentTime(), item)) {
                    LogUtils.debug("Clicks are enabled", new Object[0]);
                    execItem = new TestageExecItem(item);
                    getSettings().incCurrentTestageClicksCount(itemId);
                } else {
                    execItem = new TestageExecItem(itemId, item.getUrl());
                }
                this.testageManager.execute(execItem);
                getSettings().incCurrentTestageAdCount(itemId);
                getSettings().setLastTestageAdTime(itemId, delegate.getCurrentTime());
                getSettings().save();
            }
        }
        return true;
    }

    private boolean validateTestageClicks(long currentTime, TestageItem item) {
        LogUtils.debug("Validating testage clicks...", item);
        for (TestageValidator validator : this.testageClicksValidators) {
            TestageValidator.Result validationResult = validator.validate(currentTime, item);
            if (!validationResult.valid()) {
                LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validationResult.reason());
                return false;
            }
        }
        return true;
    }

    private boolean validateTestageItem(long currentTime, TestageItem item) {
        LogUtils.debug("Validating testage item: %s", item);
        for (TestageValidator validator : this.testageValidators) {
            TestageValidator.Result validationResult = validator.validate(currentTime, item);
            if (!validationResult.valid()) {
                LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validationResult.reason());
                return false;
            }
        }
        return true;
    }
}
