package com.jobstrak.drawingfun.sdk.activity;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.banner.Banner;
import com.jobstrak.drawingfun.sdk.banner.BannerFactory;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.data.Stats;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.model.Stat;
import com.jobstrak.drawingfun.sdk.utils.JsonUtils;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Map;

public class BannerActivity extends BaseActivity {
    @NonNull
    private final AndroidManager androidManager = ManagerFactory.getAndroidManager();
    @NonNull
    private final BannerCallback bannerCallback = new BannerCallback(this.stats, this.settings, this, this.androidManager);
    @NonNull
    private final Config config = Config.getInstance();
    @Nullable
    private Banner currentBanner;
    @NonNull
    private final Settings settings = Settings.getInstance();
    @NonNull
    private final Stats stats = Stats.getInstance();
    @NonNull
    private final Deque<Map<String, String>> waterfall = getWaterfall();

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        moveTaskToBack(true);
        doIteration();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        LogUtils.debug();
        super.onDestroy();
        removeCallbackFromCurrentBanner();
        this.androidManager.removeOverlayView();
    }

    private void removeCallbackFromCurrentBanner() {
        Banner banner = this.currentBanner;
        if (banner != null) {
            banner.removeCallback();
        }
    }

    /* access modifiers changed from: private */
    public void doIteration() {
        if (!this.waterfall.isEmpty()) {
            loadBanner(this.waterfall.pop());
            saveWaterfallIfNeeded();
            return;
        }
        LogUtils.error("No more banners in waterfall!", new Object[0]);
        finish();
    }

    private void saveWaterfallIfNeeded() {
        boolean waterfallFromStart = this.config.isWaterfallFromStart();
        LogUtils.debug("{waterfallFromStart == %s}", String.valueOf(waterfallFromStart));
        if (!waterfallFromStart) {
            saveWaterfall(this.waterfall);
        }
    }

    private void loadBanner(@NonNull Map<String, String> properties) {
        try {
            String bannerId = properties.get("id");
            if (checkRepeats(bannerId, properties)) {
                removeCallbackFromCurrentBanner();
                LogUtils.debug("Loading %s banner...", bannerId);
                this.currentBanner = BannerFactory.getBanner(this, this.bannerCallback, properties);
                this.currentBanner.load();
                return;
            }
            LogUtils.debug("Limit of repeats per day for %s banner reached!", bannerId);
            doIteration();
        } catch (Exception e) {
            LogUtils.error(e);
            doIteration();
        }
    }

    private boolean checkRepeats(@NonNull String bannerId, @NonNull Map<String, String> properties) {
        LogUtils.debug("Checking repeats of %s banner...", bannerId);
        int repeats = properties.containsKey("repeats") ? Integer.valueOf(properties.get("repeats")).intValue() : 0;
        int bannerImpressions = this.settings.getBannerImpressions(bannerId);
        LogUtils.debug("{bannerImpressions = %d, repeats = %d}", Integer.valueOf(bannerImpressions), Integer.valueOf(repeats));
        if (repeats <= 0 || bannerImpressions < repeats) {
            return true;
        }
        return false;
    }

    @NonNull
    private Deque<Map<String, String>> getWaterfall() {
        Deque<Map<String, String>> waterfall2;
        LogUtils.debug("Getting waterfall...", new Object[0]);
        List<Map<String, String>> configWaterfall = this.config.getWaterfall();
        Deque<Map<String, String>> settingsWaterfall = this.settings.getWaterfall();
        LogUtils.debug("{settingsWaterfall = %s, configWaterfall = %s}", settingsWaterfall, configWaterfall);
        int configWaterfallHash = configWaterfall.hashCode();
        int settingsWaterfallHash = this.settings.getWaterfallHash();
        LogUtils.debug("{settingsWaterfallHash = %d, configWaterfallHash = %d}", Integer.valueOf(settingsWaterfallHash), Integer.valueOf(configWaterfallHash));
        if (settingsWaterfall == null || settingsWaterfall.isEmpty() || settingsWaterfallHash != configWaterfallHash) {
            LogUtils.debug("Creating new waterfall...", new Object[0]);
            waterfall2 = new ArrayDeque<>(configWaterfall);
        } else {
            waterfall2 = new ArrayDeque<>(settingsWaterfall);
        }
        if (!waterfall2.equals(settingsWaterfall)) {
            saveWaterfall(waterfall2);
            saveWaterfallHash(configWaterfallHash);
        }
        return waterfall2;
    }

    private void saveWaterfallHash(int waterfallHash) {
        LogUtils.debug("Saving waterfall hash: %d...", Integer.valueOf(waterfallHash));
        this.settings.setWaterfallHash(waterfallHash);
    }

    private void saveWaterfall(Deque<Map<String, String>> waterfall2) {
        LogUtils.debug("Saving waterfall...", new Object[0]);
        LogUtils.debug(JsonUtils.toJSON(waterfall2), new Object[0]);
        this.settings.setWaterfall(waterfall2);
        this.settings.save();
    }

    private static class BannerCallback implements Banner.Callback {
        @Nullable
        private final WeakReference<BannerActivity> activity;
        @NonNull
        private final AndroidManager androidManager;
        @NonNull
        private final Settings settings;
        @NonNull
        private final Stats stats;

        public BannerCallback(@NonNull Stats stats2, @NonNull Settings settings2, @NonNull BannerActivity activity2, @NonNull AndroidManager androidManager2) {
            this.stats = stats2;
            this.settings = settings2;
            this.androidManager = androidManager2;
            this.activity = new WeakReference<>(activity2);
        }

        public void onShow(@NonNull Banner banner) {
            log(banner, "showed");
            if (banner.getClass().getSimpleName().equals("Startapp")) {
                this.androidManager.createOverlayView();
            }
            this.stats.add(createStat(banner, Stat.TYPE_IMPRESSION));
            this.settings.incBannerImpressions(banner.getProperties().get("id"));
            WeakReference<BannerActivity> weakReference = this.activity;
            if (weakReference != null && weakReference.get() != null) {
                this.activity.get().startFinishTimer();
            }
        }

        public void onDismiss(@NonNull Banner banner) {
            log(banner, "dismissed");
            WeakReference<BannerActivity> weakReference = this.activity;
            if (weakReference != null && weakReference.get() != null) {
                this.activity.get().finish();
            } else if (banner.getClass().getSimpleName().equals("Startapp")) {
                this.androidManager.removeOverlayView();
            }
        }

        public void onFail(@NonNull Banner banner, String reason, Exception e) {
            LogUtils.error("Banner %s failed: %s", e, banner.getProperties().get("id"), reason);
            WeakReference<BannerActivity> weakReference = this.activity;
            if (weakReference != null && weakReference.get() != null) {
                this.activity.get().doIteration();
            }
        }

        public void onClick(@NonNull Banner banner) {
            log(banner, "clicked");
            this.stats.add(createStat(banner, Stat.TYPE_CLICK));
            WeakReference<BannerActivity> weakReference = this.activity;
            if (weakReference != null && weakReference.get() != null) {
                this.activity.get().finish();
            }
        }

        public void onRequest(@NonNull Banner banner) {
            log(banner, "requested");
            this.stats.add(createStat(banner, Stat.TYPE_REQUEST));
        }

        public void onLoad(@NonNull Banner banner) {
            log(banner, "loaded");
            banner.show();
        }

        private final void log(@NonNull Banner banner, @NonNull String action) {
            LogUtils.debug("Banner %s %s", banner.getProperties().get("id"), action);
        }

        private final Stat createStat(@NonNull Banner banner, @NonNull String statType) {
            return new Stat(banner.getProperties().get("id"), statType);
        }
    }
}
