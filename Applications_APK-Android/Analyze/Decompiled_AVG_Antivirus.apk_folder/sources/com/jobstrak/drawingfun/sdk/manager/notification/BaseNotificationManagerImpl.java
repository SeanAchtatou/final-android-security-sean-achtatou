package com.jobstrak.drawingfun.sdk.manager.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.activity.LinkActivity;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public abstract class BaseNotificationManagerImpl implements NotificationManager {
    @NonNull
    private final CryopiggyManager cryopiggyManager;
    private int lastNotificationId;

    public abstract Notification buildNotification(@NonNull Context context, @NonNull NotificationAd notificationAd);

    public BaseNotificationManagerImpl(@NonNull CryopiggyManager cryopiggyManager2) {
        this.cryopiggyManager = cryopiggyManager2;
    }

    public void showNotification(@NonNull NotificationAd notificationAd) {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            LogUtils.debug("Building a notification...", new Object[0]);
            Notification notification = buildNotification(context, notificationAd);
            LogUtils.debug("Showing a notification...", new Object[0]);
            NotificationManager androidNotificationManager = getAndroidNotificationManager();
            int i = this.lastNotificationId + 1;
            this.lastNotificationId = i;
            androidNotificationManager.notify(i, notification);
            return;
        }
        LogUtils.error("context == null", new Object[0]);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public PendingIntent getBrowserPendingIntent(@NonNull Context context, @NonNull String url) {
        return PendingIntent.getActivity(context, 0, LinkActivity.createIntentForType(context, url, 1), 134217728);
    }

    private NotificationManager getAndroidNotificationManager() {
        return (NotificationManager) this.cryopiggyManager.optContext().getSystemService(NotificationAd.BANNER_ID);
    }
}
