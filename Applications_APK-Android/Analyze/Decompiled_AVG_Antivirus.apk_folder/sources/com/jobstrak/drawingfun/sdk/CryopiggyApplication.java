package com.jobstrak.drawingfun.sdk;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

public class CryopiggyApplication extends Application {
    public void onCreate() {
        super.onCreate();
        Cryopiggy.init(this);
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
