package com.jobstrak.drawingfun.sdk.service.alarm;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.data.Stats;
import com.jobstrak.drawingfun.sdk.model.Stat;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.stat.WorkingEventSendTask;
import com.jobstrak.drawingfun.sdk.utils.NetworkUtils;

public class WorkingEventSendAlarm extends Alarm {
    @NonNull
    private final Stats stats;
    @NonNull
    private final TaskFactory<WorkingEventSendTask> workingEventSendTask;

    public WorkingEventSendAlarm(@NonNull Stats stats2, @NonNull Config config, @NonNull Settings settings, @NonNull TaskFactory<WorkingEventSendTask> workingEventSendTask2) {
        super(config, settings);
        this.stats = stats2;
        this.workingEventSendTask = workingEventSendTask2;
    }

    public boolean isNeedExecute(long currentTime) {
        return isRegistered() && isItTime(currentTime) && isImpressionWasDone() && isConnected();
    }

    public void execute(long currentTime) {
        this.workingEventSendTask.create().execute(new Void[0]);
        setNextWorkingEventSendTime(currentTime);
    }

    private boolean isRegistered() {
        return getSettings().getClientId() != null;
    }

    private boolean isItTime(long currentTime) {
        return getSettings().getNextWorkingEventSendTime() <= currentTime;
    }

    private boolean isImpressionWasDone() {
        long currentMaxTimestamp = getMaxImpressionTimestamp();
        if (currentMaxTimestamp <= 86400000 + getSettings().getLastWorkingEventImpTimestamp()) {
            return false;
        }
        getSettings().setLastWorkingEventImpTimestamp(currentMaxTimestamp);
        return true;
    }

    private long getMaxImpressionTimestamp() {
        long timestamp = 0;
        for (Stat stat : this.stats.getAll()) {
            if (stat.getType().equals(Stat.TYPE_IMPRESSION)) {
                timestamp = Math.max(stat.getTimestamp(), timestamp);
            }
        }
        return timestamp;
    }

    private boolean isConnected() {
        return NetworkUtils.isConnected();
    }

    private void setNextWorkingEventSendTime(long currentTime) {
        getSettings().setNextWorkingEventSendTime(900000 + currentTime);
        getSettings().save();
    }
}
