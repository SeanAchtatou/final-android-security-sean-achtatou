package com.jobstrak.drawingfun.sdk.utils;

import android.text.TextUtils;
import android.util.Base64;

public final class XorUtils {
    private XorUtils() {
        throw new AssertionError("No instances");
    }

    public static String encode(String in, String key) {
        return Base64.encodeToString(xor(in.getBytes(), key.getBytes()), 2);
    }

    public static String decode(String in, String key) {
        if (TextUtils.isEmpty(in) || TextUtils.isEmpty(key)) {
            return null;
        }
        return new String(xor(Base64.decode(in, 2), key.getBytes()));
    }

    private static byte[] xor(byte[] in, byte[] key) {
        int length = in.length;
        byte[] out = new byte[length];
        for (int i = 0; i < length; i++) {
            out[i] = (byte) (in[i] ^ key[i % key.length]);
        }
        return out;
    }
}
