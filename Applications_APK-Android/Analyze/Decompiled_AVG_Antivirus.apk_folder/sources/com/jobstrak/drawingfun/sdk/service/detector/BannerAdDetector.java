package com.jobstrak.drawingfun.sdk.service.detector;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.activity.BannerActivity;
import com.jobstrak.drawingfun.sdk.activity.BaseActivity;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.service.validator.banner.BannerAdPauseStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.banner.BannerAdPerAppLimitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.banner.BannerAdPerDayLimitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.banner.BannerAdShowingStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.banner.BannerAdWaterfallSizeValidator;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.StringUtils;

public class BannerAdDetector extends Detector {
    private static int currentAppAdCount = 0;
    @Nullable
    private static String currentForegroundPackage = null;
    @NonNull
    private final AndroidManager androidManager;
    @NonNull
    private final CryopiggyManager cryopiggyManager;
    private final Validator[] validators = {new BannerAdWaterfallSizeValidator(), new BannerAdPerAppLimitValidator(), new BannerAdPerDayLimitValidator(), new BannerAdShowingStateValidator(), new BannerAdPauseStateValidator()};

    public BannerAdDetector(@NonNull Config config, @NonNull Settings settings, @NonNull AndroidManager androidManager2, @NonNull CryopiggyManager cryopiggyManager2) {
        super(config, settings);
        this.androidManager = androidManager2;
        this.cryopiggyManager = cryopiggyManager2;
    }

    public void tick(Detector.Delegate delegate) {
        String foregroundPackage = delegate.getForegroundPackage();
        if (TextUtils.equals(foregroundPackage, currentForegroundPackage)) {
            return;
        }
        if (!BaseActivity.isShowing() || !this.cryopiggyManager.optContext().getPackageName().equals(foregroundPackage)) {
            currentAppAdCount = 0;
            currentForegroundPackage = foregroundPackage;
        }
    }

    public boolean detect(Detector.Delegate delegate) {
        Object obj;
        long currentTime = delegate.getCurrentTime();
        boolean bannersPrePaused = delegate.isBannersPrePaused();
        String foregroundPackage = delegate.getForegroundPackage();
        for (String restrictedApp : getConfig().getRestrictedApps()) {
            if (StringUtils.wildcardMatch(restrictedApp, foregroundPackage)) {
                LogUtils.debug("App %s is restricted app", foregroundPackage);
                return false;
            }
        }
        boolean isThisApp = this.cryopiggyManager.optContext().getPackageName().equals(foregroundPackage);
        if (!getConfig().isShowAdInApp() && isThisApp) {
            LogUtils.debug("Show ad in app is restricted", new Object[0]);
            return false;
        } else if (getConfig().isShowAdInOtherApps() || isThisApp) {
            LogUtils.debug("Unrestricted app %s detected", foregroundPackage);
            for (Validator validator : this.validators) {
                if (!validator.validate(currentTime)) {
                    LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validator.getReason());
                    return true;
                }
            }
            if (bannersPrePaused) {
                int bannerAdCount = getConfig().getAdCount();
                int currentBannerAdCount = getSettings().getCurrentBannerAdCount() + 1;
                Object[] objArr = new Object[2];
                objArr[0] = Integer.valueOf(currentBannerAdCount);
                if (bannerAdCount == 0) {
                    obj = "inf";
                } else {
                    obj = Integer.valueOf(bannerAdCount);
                }
                objArr[1] = obj;
                LogUtils.debug("Show banner ad (%s/%s)", objArr);
                delegate.unPrePause();
                currentAppAdCount++;
                getSettings().setNextBannerAdTime(getConfig().getAdDelay() + currentTime);
                getSettings().setCurrentBannerAdCount(currentBannerAdCount);
                getSettings().save();
                this.androidManager.startActivity(BannerActivity.class);
            } else {
                delegate.prePause();
                getSettings().setNextBannerAdTime(getConfig().getPreAdDelay() + currentTime);
                getSettings().save();
            }
            return true;
        } else {
            LogUtils.debug("Show ad in other apps is restricted", new Object[0]);
            return false;
        }
    }

    public static int getCurrentAppAdCount() {
        return currentAppAdCount;
    }

    @Nullable
    public static String getCurrentForegroundPackage() {
        return currentForegroundPackage;
    }
}
