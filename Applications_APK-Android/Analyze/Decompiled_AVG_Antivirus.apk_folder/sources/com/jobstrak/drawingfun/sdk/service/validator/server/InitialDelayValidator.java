package com.jobstrak.drawingfun.sdk.service.validator.server;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class InitialDelayValidator extends Validator {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;
    private long timeLeft = 0;

    public InitialDelayValidator(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        this.timeLeft = (this.settings.getRegisterTime() + this.config.getFirstAdDelay()) - currentTime;
        return this.timeLeft <= 0;
    }

    public String getReason() {
        return String.format("initial delay (%s left)", TimeUtils.formatDuration(this.timeLeft));
    }
}
