package com.jobstrak.drawingfun.sdk.utils.logger;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.jobstrak.drawingfun.sdk.BuildConfig;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.JvmOverloads;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import kotlin.text.StringsKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0002\u0018\u00002\u00020\u0001BL\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012'\u0010\u0004\u001a#\u0012\u0004\u0012\u00020\u0006\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\u000b0\u0005\u0012\b\b\u0002\u0010\f\u001a\u00020\u0006\u0012\b\b\u0002\u0010\r\u001a\u00020\u000e¢\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J=\u0010\u0014\u001a\u00020\u00152\u0006\u0010\n\u001a\u00020\u00072\b\u0010\u0016\u001a\u0004\u0018\u00010\u00032\b\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0012\u0010\u0019\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u001b0\u001a\"\u00020\u001bH\u0016¢\u0006\u0002\u0010\u001cR\u000e\u0010\f\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R/\u0010\u0004\u001a#\u0012\u0004\u0012\u00020\u0006\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\u000b0\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/utils/logger/CryoLogger;", "Lcom/jobstrak/drawingfun/sdk/utils/logger/Logger;", "tag", "", "shouldLog", "Lkotlin/Function2;", "Lcom/jobstrak/drawingfun/sdk/manager/main/CryopiggyManager;", "", "Lkotlin/ParameterName;", "name", "level", "", "cryopiggyManager", "dateFormatter", "Ljava/text/DateFormat;", "(Ljava/lang/String;Lkotlin/jvm/functions/Function2;Lcom/jobstrak/drawingfun/sdk/manager/main/CryopiggyManager;Ljava/text/DateFormat;)V", "getClassName", "element", "Ljava/lang/StackTraceElement;", "getMethodName", "log", "", "msg", "throwable", "", "args", "", "", "(ILjava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: CryoLogger.kt */
public final class CryoLogger implements Logger {
    private final CryopiggyManager cryopiggyManager;
    private final DateFormat dateFormatter;
    private final Function2<CryopiggyManager, Integer, Boolean> shouldLog;
    private final String tag;

    @JvmOverloads
    public CryoLogger(@NotNull String str, @NotNull Function2<? super CryopiggyManager, ? super Integer, Boolean> function2) {
        this(str, function2, null, null, 12, null);
    }

    @JvmOverloads
    public CryoLogger(@NotNull String str, @NotNull Function2<? super CryopiggyManager, ? super Integer, Boolean> function2, @NotNull CryopiggyManager cryopiggyManager2) {
        this(str, function2, cryopiggyManager2, null, 8, null);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [kotlin.jvm.functions.Function2<? super com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager, ? super java.lang.Integer, java.lang.Boolean>, java.lang.Object, kotlin.jvm.functions.Function2<com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager, java.lang.Integer, java.lang.Boolean>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @kotlin.jvm.JvmOverloads
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CryoLogger(@org.jetbrains.annotations.NotNull java.lang.String r2, @org.jetbrains.annotations.NotNull kotlin.jvm.functions.Function2<? super com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager, ? super java.lang.Integer, java.lang.Boolean> r3, @org.jetbrains.annotations.NotNull com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager r4, @org.jetbrains.annotations.NotNull java.text.DateFormat r5) {
        /*
            r1 = this;
            java.lang.String r0 = "tag"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r2, r0)
            java.lang.String r0 = "shouldLog"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
            java.lang.String r0 = "cryopiggyManager"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r4, r0)
            java.lang.String r0 = "dateFormatter"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r5, r0)
            r1.<init>()
            r1.tag = r2
            r1.shouldLog = r3
            r1.cryopiggyManager = r4
            r1.dateFormatter = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.sdk.utils.logger.CryoLogger.<init>(java.lang.String, kotlin.jvm.functions.Function2, com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager, java.text.DateFormat):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    @kotlin.jvm.JvmOverloads
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ CryoLogger(java.lang.String r1, kotlin.jvm.functions.Function2 r2, com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager r3, java.text.DateFormat r4, int r5, kotlin.jvm.internal.DefaultConstructorMarker r6) {
        /*
            r0 = this;
            r6 = r5 & 4
            if (r6 == 0) goto L_0x000d
            com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager r3 = com.jobstrak.drawingfun.sdk.manager.ManagerFactory.getCryopiggyManager()
            java.lang.String r6 = "ManagerFactory.getCryopiggyManager()"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r3, r6)
        L_0x000d:
            r5 = r5 & 8
            if (r5 == 0) goto L_0x001a
            java.text.SimpleDateFormat r4 = new java.text.SimpleDateFormat
            java.lang.String r5 = "yyyy-MM-dd HH:mm:ss.SSS"
            r4.<init>(r5)
            java.text.DateFormat r4 = (java.text.DateFormat) r4
        L_0x001a:
            r0.<init>(r1, r2, r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.sdk.utils.logger.CryoLogger.<init>(java.lang.String, kotlin.jvm.functions.Function2, com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager, java.text.DateFormat, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.text.StringsKt__StringsKt.lastIndexOf$default(java.lang.CharSequence, char, int, boolean, int, java.lang.Object):int
     arg types: [java.lang.String, int, int, int, int, ?[OBJECT, ARRAY]]
     candidates:
      kotlin.text.StringsKt__StringsKt.lastIndexOf$default(java.lang.CharSequence, java.lang.String, int, boolean, int, java.lang.Object):int
      kotlin.text.StringsKt__StringsKt.lastIndexOf$default(java.lang.CharSequence, char, int, boolean, int, java.lang.Object):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.text.StringsKt__StringsKt.indexOf$default(java.lang.CharSequence, java.lang.String, int, boolean, int, java.lang.Object):int
     arg types: [java.lang.String, java.lang.String, int, int, int, ?[OBJECT, ARRAY]]
     candidates:
      kotlin.text.StringsKt__StringsKt.indexOf$default(java.lang.CharSequence, char, int, boolean, int, java.lang.Object):int
      kotlin.text.StringsKt__StringsKt.indexOf$default(java.lang.CharSequence, java.lang.String, int, boolean, int, java.lang.Object):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    public void log(int level, @Nullable String msg, @Nullable Throwable throwable, @NotNull Object... args) {
        String str;
        String callerMethodName;
        String callerMethodName2;
        String levelString;
        IOException e;
        int i = level;
        String str2 = msg;
        Throwable th = throwable;
        Object[] objArr = args;
        Intrinsics.checkParameterIsNotNull(objArr, "args");
        if (this.shouldLog.invoke(this.cryopiggyManager, Integer.valueOf(level)).booleanValue()) {
            if (!TextUtils.isEmpty(str2)) {
                StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
                if (str2 == null) {
                    Intrinsics.throwNpe();
                }
                Object[] copyOf = Arrays.copyOf(objArr, objArr.length);
                str = String.format(str2, Arrays.copyOf(copyOf, copyOf.length));
                Intrinsics.checkExpressionValueIsNotNull(str, "java.lang.String.format(format, *args)");
            } else if (th != null) {
                str = throwable.getClass().getName() + ": " + throwable.getMessage();
            } else {
                str = "+";
            }
            String msg2 = str;
            StackTraceElement[] elements = new Throwable().getStackTrace();
            if (elements.length >= 3) {
                StackTraceElement stackTraceElement = elements[2];
                Intrinsics.checkExpressionValueIsNotNull(stackTraceElement, "elements[2]");
                String callerClassName = getClassName(stackTraceElement);
                int lastIndexOf$default = StringsKt.lastIndexOf$default((CharSequence) callerClassName, '.', 0, false, 6, (Object) null) + 1;
                if (callerClassName != null) {
                    String substring = callerClassName.substring(lastIndexOf$default);
                    Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
                    String callerClassName2 = substring;
                    if (StringsKt.indexOf$default((CharSequence) callerClassName2, "$", 0, false, 6, (Object) null) > 0) {
                        int indexOf$default = StringsKt.indexOf$default((CharSequence) callerClassName2, "$", 0, false, 6, (Object) null);
                        if (callerClassName2 != null) {
                            String substring2 = callerClassName2.substring(0, indexOf$default);
                            Intrinsics.checkExpressionValueIsNotNull(substring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                            callerClassName2 = substring2;
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    StackTraceElement stackTraceElement2 = elements[2];
                    Intrinsics.checkExpressionValueIsNotNull(stackTraceElement2, "elements[2]");
                    String callerMethodName3 = getMethodName(stackTraceElement2);
                    int lastIndexOf$default2 = StringsKt.lastIndexOf$default((CharSequence) callerMethodName3, '_', 0, false, 6, (Object) null) + 1;
                    if (callerMethodName3 != null) {
                        String substring3 = callerMethodName3.substring(lastIndexOf$default2);
                        Intrinsics.checkExpressionValueIsNotNull(substring3, "(this as java.lang.String).substring(startIndex)");
                        String callerMethodName4 = substring3;
                        if (Intrinsics.areEqual(callerMethodName4, "<init>")) {
                            callerMethodName2 = callerClassName2;
                            callerMethodName = callerMethodName2;
                        } else {
                            callerMethodName = callerMethodName4;
                            callerMethodName2 = callerClassName2;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                callerMethodName = "?";
                callerMethodName2 = "?";
            }
            String message = '[' + (callerMethodName2 + ' ' + callerMethodName + "()") + "] " + msg2;
            switch (i) {
                case 2:
                    Log.v(this.tag, message, th);
                    levelString = "V";
                    break;
                case 3:
                    Log.d(this.tag, message, th);
                    levelString = "D";
                    break;
                case 4:
                    Log.i(this.tag, message, th);
                    levelString = "I";
                    break;
                case 5:
                    Log.w(this.tag, message, th);
                    levelString = "W";
                    break;
                case 6:
                    Log.e(this.tag, message, th);
                    levelString = "E";
                    break;
                case 7:
                    Log.wtf(this.tag, message, th);
                    levelString = "WTF";
                    break;
                default:
                    levelString = "";
                    break;
            }
            if (i >= 3) {
                FileWriter fw = null;
                try {
                    String timestamp = this.dateFormatter.format(new Date());
                    Context optContext = this.cryopiggyManager.optContext();
                    if (optContext == null) {
                        Intrinsics.throwNpe();
                    }
                    Intrinsics.checkExpressionValueIsNotNull(optContext, "cryopiggyManager.optContext()!!");
                    FileWriter fw2 = new FileWriter(new File(optContext.getFilesDir(), BuildConfig.LOG_FILENAME), true);
                    fw2.write(timestamp + " [" + levelString + "] " + message + 10);
                    try {
                        fw2.close();
                        return;
                    } catch (IOException e2) {
                        e = e2;
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                    if (fw != null) {
                        try {
                            fw.close();
                            return;
                        } catch (IOException e4) {
                            e = e4;
                        }
                    } else {
                        return;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    if (fw != null) {
                        try {
                            fw.close();
                        } catch (IOException e5) {
                            e5.printStackTrace();
                        }
                    }
                    throw th3;
                }
            } else {
                return;
            }
        } else {
            return;
        }
        e.printStackTrace();
    }

    private final String getClassName(StackTraceElement element) {
        String className = element.getClassName();
        Intrinsics.checkExpressionValueIsNotNull(className, "element.className");
        return className;
    }

    private final String getMethodName(StackTraceElement element) {
        String methodName = element.getMethodName();
        Intrinsics.checkExpressionValueIsNotNull(methodName, "element.methodName");
        return methodName;
    }
}
