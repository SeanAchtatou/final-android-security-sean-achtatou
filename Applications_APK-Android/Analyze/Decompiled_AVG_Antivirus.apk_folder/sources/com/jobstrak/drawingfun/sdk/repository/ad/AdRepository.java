package com.jobstrak.drawingfun.sdk.repository.ad;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.HTMLAd;
import com.jobstrak.drawingfun.sdk.model.IconAd;
import com.jobstrak.drawingfun.sdk.model.LinkAd;
import com.jobstrak.drawingfun.sdk.model.LockscreenAd;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;

public interface AdRepository {
    @NonNull
    HTMLAd getHTMLAd(@NonNull HTMLAd.Type type) throws Exception;

    @NonNull
    IconAd getIconAd() throws Exception;

    @NonNull
    LinkAd getLinkAd(LinkAd.Type type) throws Exception;

    @NonNull
    LockscreenAd getLockscreenAd() throws Exception;

    @NonNull
    NotificationAd getNotificationAd() throws Exception;
}
