package com.jobstrak.drawingfun.sdk.manager.main.init;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import com.jobstrak.drawingfun.sdk.AlarmBroadcastReceiver;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@RequiresApi(21)
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/manager/main/init/AlarmAndroidOInit;", "Lcom/jobstrak/drawingfun/sdk/manager/main/init/AndroidOInit;", "context", "Landroid/content/Context;", "androidManager", "Lcom/jobstrak/drawingfun/sdk/manager/android/AndroidManager;", "(Landroid/content/Context;Lcom/jobstrak/drawingfun/sdk/manager/android/AndroidManager;)V", "doExecute", "", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: AlarmAndroidOInit.kt */
public final class AlarmAndroidOInit extends AndroidOInit {
    private final Context context;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmAndroidOInit(@NotNull Context context2, @NotNull AndroidManager androidManager) {
        super(androidManager);
        Intrinsics.checkParameterIsNotNull(context2, "context");
        Intrinsics.checkParameterIsNotNull(androidManager, "androidManager");
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void doExecute() {
        super.doExecute();
        Intent intent = new Intent(this.context, AlarmBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.context, 0, intent, 536870912);
        StringBuilder sb = new StringBuilder();
        sb.append("pending intent is null == ");
        sb.append(pendingIntent == null);
        LogUtils.debug(sb.toString(), new Object[0]);
        if (pendingIntent == null) {
            LogUtils.debug("Setting an alarm...", new Object[0]);
            Object systemService = this.context.getSystemService(NotificationCompat.CATEGORY_ALARM);
            if (systemService != null) {
                ((AlarmManager) systemService).setInexactRepeating(3, SystemClock.elapsedRealtime() + 7200000, 7200000, PendingIntent.getBroadcast(this.context, 0, intent, 0));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.app.AlarmManager");
        }
    }
}
