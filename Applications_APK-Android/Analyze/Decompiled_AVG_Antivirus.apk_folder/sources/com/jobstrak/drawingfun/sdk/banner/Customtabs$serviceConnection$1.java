package com.jobstrak.drawingfun.sdk.banner;

import android.content.ComponentName;
import com.jobstrak.drawingfun.lib.androidx.browser.customtabs.CustomTabsClient;
import com.jobstrak.drawingfun.lib.androidx.browser.customtabs.CustomTabsServiceConnection;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016¨\u0006\t"}, d2 = {"com/jobstrak/drawingfun/sdk/banner/Customtabs$serviceConnection$1", "Lcom/jobstrak/drawingfun/lib/androidx/browser/customtabs/CustomTabsServiceConnection;", "onCustomTabsServiceConnected", "", "name", "Landroid/content/ComponentName;", "client", "Lcom/jobstrak/drawingfun/lib/androidx/browser/customtabs/CustomTabsClient;", "onServiceDisconnected", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: Customtabs.kt */
public final class Customtabs$serviceConnection$1 extends CustomTabsServiceConnection {
    final /* synthetic */ Customtabs this$0;

    Customtabs$serviceConnection$1(Customtabs $outer) {
        this.this$0 = $outer;
    }

    public void onCustomTabsServiceConnected(@NotNull ComponentName name, @NotNull CustomTabsClient client) {
        Intrinsics.checkParameterIsNotNull(name, "name");
        Intrinsics.checkParameterIsNotNull(client, "client");
        LogUtils.debug("onCustomTabsServiceConnected", new Object[0]);
        this.this$0.client = client;
    }

    public void onServiceDisconnected(@NotNull ComponentName name) {
        Intrinsics.checkParameterIsNotNull(name, "name");
        LogUtils.debug("onServiceDisconnected", new Object[0]);
        this.this$0.client = null;
    }
}
