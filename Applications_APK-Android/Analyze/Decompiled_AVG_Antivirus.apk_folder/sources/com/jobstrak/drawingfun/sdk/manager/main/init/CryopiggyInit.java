package com.jobstrak.drawingfun.sdk.manager.main.init;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.SdkConfig;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class CryopiggyInit extends Init {
    @NonNull
    private final String appId;
    @NonNull
    private final Config config;
    @NonNull
    private final CryopiggyManager cryopiggyManager;
    @NonNull
    private final boolean lightMode;
    @Nullable
    private final String server;
    @NonNull
    private final Settings settings;

    public CryopiggyInit(@NonNull CryopiggyManager cryopiggyManager2, @NonNull Config config2, @NonNull Settings settings2, @NonNull String appId2, @Nullable String server2, @NonNull boolean lightMode2) {
        this.cryopiggyManager = cryopiggyManager2;
        this.config = config2;
        this.settings = settings2;
        this.appId = appId2;
        this.server = server2;
        this.lightMode = lightMode2;
    }

    /* access modifiers changed from: protected */
    public void doExecute() throws InitException {
        if (!TextUtils.isEmpty(this.settings.getAppId()) && this.settings.isLightMode() != this.lightMode) {
            LogUtils.debug("Flavor was changed", new Object[0]);
            this.cryopiggyManager.clear();
        }
        if (TextUtils.equals(this.appId, this.settings.getAppId())) {
            return;
        }
        if (!TextUtils.isEmpty(this.appId)) {
            if (!TextUtils.isEmpty(this.settings.getAppId())) {
                LogUtils.debug("Application ID was changed", new Object[0]);
                this.cryopiggyManager.clear();
            }
            setupServerIfNeeded(this.config, this.server);
            setupApplication(this.settings, this.appId, this.lightMode);
            return;
        }
        throw new InitException(SdkConfig.getSdkName() + " is disabled due to lack of application ID");
    }

    private static void setupServerIfNeeded(@NonNull Config config2, @Nullable String server2) {
        if (!TextUtils.isEmpty(server2)) {
            config2.setServer(server2);
            config2.save();
        }
    }

    private static void setupApplication(@NonNull Settings settings2, @NonNull String appId2, @NonNull boolean lightMode2) {
        settings2.setAppId(appId2);
        settings2.setLightMode(lightMode2);
        settings2.save();
    }
}
