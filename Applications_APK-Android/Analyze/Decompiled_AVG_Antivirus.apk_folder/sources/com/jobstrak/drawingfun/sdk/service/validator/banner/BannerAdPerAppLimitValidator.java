package com.jobstrak.drawingfun.sdk.service.validator.banner;

import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.detector.BannerAdDetector;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BannerAdPerAppLimitValidator extends Validator {
    private int bannerAdCount;

    public boolean validate(long currentTime) {
        this.bannerAdCount = Config.getInstance().getAdCountPerApp();
        return this.bannerAdCount == 0 || BannerAdDetector.getCurrentAppAdCount() < this.bannerAdCount;
    }

    public String getReason() {
        return String.format("banner ad count per app (%s) reached (%s/%s)", BannerAdDetector.getCurrentForegroundPackage(), Integer.valueOf(BannerAdDetector.getCurrentAppAdCount()), Integer.valueOf(this.bannerAdCount));
    }
}
