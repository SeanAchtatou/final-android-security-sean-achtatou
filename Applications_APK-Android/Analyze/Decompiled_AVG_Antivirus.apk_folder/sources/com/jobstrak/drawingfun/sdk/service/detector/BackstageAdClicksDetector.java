package com.jobstrak.drawingfun.sdk.service.detector;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.controller.BackstageWebViewController;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.service.validator.backstage.clicks.BackstageAdClicksDelayStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.backstage.clicks.BackstageAdClicksEnableStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.backstage.clicks.BackstageAdClicksPerDayLimitValidator;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class BackstageAdClicksDetector extends Detector {
    @NonNull
    private final Validator[] validators;
    @NonNull
    private final BackstageWebViewController webViewController;

    public BackstageAdClicksDetector(@NonNull Config config, @NonNull Settings settings, @NonNull BackstageWebViewController webViewController2) {
        super(config, settings);
        this.webViewController = webViewController2;
        this.validators = new Validator[]{new BackstageAdClicksEnableStateValidator(config), new BackstageAdClicksPerDayLimitValidator(config, settings), new BackstageAdClicksDelayStateValidator(config, settings)};
    }

    public void tick(Detector.Delegate delegate) {
    }

    public boolean detect(Detector.Delegate delegate) {
        for (Validator validator : this.validators) {
            if (!validator.validate(delegate.getCurrentTime())) {
                LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validator.getReason());
                if (validator instanceof BackstageAdClicksDelayStateValidator) {
                    getSettings().incCurrentBackstageAdClicksCount();
                    getSettings().save();
                }
                return true;
            }
        }
        this.webViewController.getWebViewClient().setEnabled(false);
        this.webViewController.clickOnLongestLink();
        return true;
    }
}
