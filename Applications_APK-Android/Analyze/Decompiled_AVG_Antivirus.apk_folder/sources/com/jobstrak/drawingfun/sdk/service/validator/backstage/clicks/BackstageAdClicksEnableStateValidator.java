package com.jobstrak.drawingfun.sdk.service.validator.backstage.clicks;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BackstageAdClicksEnableStateValidator extends Validator {
    @NonNull
    private final Config config;

    public BackstageAdClicksEnableStateValidator(@NonNull Config config2) {
        this.config = config2;
    }

    public boolean validate(long currentTime) {
        return this.config.isBackstageAdClicksEnabled();
    }

    public String getReason() {
        return "backstage ad clicks is disabled";
    }
}
