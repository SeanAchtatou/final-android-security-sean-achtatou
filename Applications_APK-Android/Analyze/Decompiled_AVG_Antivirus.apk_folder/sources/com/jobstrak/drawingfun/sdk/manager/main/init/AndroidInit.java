package com.jobstrak.drawingfun.sdk.manager.main.init;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;

public class AndroidInit extends Init implements Abandonable {
    @NonNull
    private final AndroidManager androidManager;

    public AndroidInit(@NonNull AndroidManager androidManager2) {
        this.androidManager = androidManager2;
    }

    /* access modifiers changed from: protected */
    public void doExecute() {
        this.androidManager.startService();
        this.androidManager.startScreenReceiver();
    }

    public void doAbandon() {
        this.androidManager.stopService();
        this.androidManager.stopScreenReceiver();
    }
}
