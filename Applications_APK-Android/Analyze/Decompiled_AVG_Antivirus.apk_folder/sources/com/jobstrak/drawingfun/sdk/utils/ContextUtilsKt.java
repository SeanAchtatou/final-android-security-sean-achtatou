package com.jobstrak.drawingfun.sdk.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Process;
import java.util.List;
import java.util.NoSuchElementException;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\"\u0010\u0010\u0000\u001a\u0004\u0018\u00010\u0001X\u000e¢\u0006\u0002\n\u0000\"\u0015\u0010\u0002\u001a\u00020\u0003*\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0002\u0010\u0005\"\u0015\u0010\u0006\u001a\u00020\u0001*\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"cachedProcessName", "", "isMainProcess", "", "Landroid/content/Context;", "(Landroid/content/Context;)Z", "processName", "getProcessName", "(Landroid/content/Context;)Ljava/lang/String;", "sdk_lightDebug"}, k = 2, mv = {1, 1, 15})
/* compiled from: ContextUtils.kt */
public final class ContextUtilsKt {
    private static String cachedProcessName;

    @NotNull
    public static final String getProcessName(@NotNull Context $this$processName) {
        boolean z;
        String str;
        Intrinsics.checkParameterIsNotNull($this$processName, "$this$processName");
        if (cachedProcessName == null) {
            int pid = Process.myPid();
            Object systemService = $this$processName.getSystemService("activity");
            if (systemService != null) {
                List<ActivityManager.RunningAppProcessInfo> $this$first$iv = ((ActivityManager) systemService).getRunningAppProcesses();
                Intrinsics.checkExpressionValueIsNotNull($this$first$iv, "manager.runningAppProcesses");
                for (Object element$iv : $this$first$iv) {
                    if (((ActivityManager.RunningAppProcessInfo) element$iv).pid == pid) {
                        z = true;
                        continue;
                    } else {
                        z = false;
                        continue;
                    }
                    if (z) {
                        ActivityManager.RunningAppProcessInfo runningAppProcessInfo = (ActivityManager.RunningAppProcessInfo) element$iv;
                        if (runningAppProcessInfo == null || (str = runningAppProcessInfo.processName) == null) {
                            str = "";
                        }
                        cachedProcessName = str;
                    }
                }
                throw new NoSuchElementException("Collection contains no element matching the predicate.");
            }
            throw new TypeCastException("null cannot be cast to non-null type android.app.ActivityManager");
        }
        String str2 = cachedProcessName;
        if (str2 == null) {
            Intrinsics.throwNpe();
        }
        return str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.text.StringsKt__StringsKt.contains$default(java.lang.CharSequence, char, boolean, int, java.lang.Object):boolean
     arg types: [java.lang.String, int, int, int, ?[OBJECT, ARRAY]]
     candidates:
      kotlin.text.StringsKt__StringsKt.contains$default(java.lang.CharSequence, java.lang.CharSequence, boolean, int, java.lang.Object):boolean
      kotlin.text.StringsKt__StringsKt.contains$default(java.lang.CharSequence, char, boolean, int, java.lang.Object):boolean */
    public static final boolean isMainProcess(@NotNull Context $this$isMainProcess) {
        Intrinsics.checkParameterIsNotNull($this$isMainProcess, "$this$isMainProcess");
        return !StringsKt.contains$default((CharSequence) getProcessName($this$isMainProcess), ':', false, 2, (Object) null);
    }
}
