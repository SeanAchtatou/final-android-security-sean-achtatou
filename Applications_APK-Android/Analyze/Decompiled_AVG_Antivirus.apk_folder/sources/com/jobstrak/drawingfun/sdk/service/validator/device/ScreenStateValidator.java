package com.jobstrak.drawingfun.sdk.service.validator.device;

import com.jobstrak.drawingfun.sdk.receiver.ScreenReceiver;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class ScreenStateValidator extends Validator {
    public boolean validate(long currentTime) {
        return ScreenReceiver.isInteractive.get();
    }

    public String getReason() {
        return "screen is off";
    }
}
