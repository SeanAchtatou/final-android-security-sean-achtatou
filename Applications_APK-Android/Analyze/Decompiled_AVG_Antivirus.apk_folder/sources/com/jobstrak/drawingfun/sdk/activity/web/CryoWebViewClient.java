package com.jobstrak.drawingfun.sdk.activity.web;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.okhttp3.Headers;
import com.jobstrak.drawingfun.lib.okhttp3.OkHttpClient;
import com.jobstrak.drawingfun.lib.okhttp3.Request;
import com.jobstrak.drawingfun.lib.okhttp3.Response;
import com.jobstrak.drawingfun.sdk.activity.WebActivity;
import com.jobstrak.drawingfun.sdk.util.UnsafeOkHttpClientProvider;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.io.IOException;
import java.util.Map;

public class CryoWebViewClient extends WebViewClient {
    private static final String MARKET_URL = "http://play.google.com/store/apps/%s?%s";
    private final OkHttpClient client = UnsafeOkHttpClientProvider.provideBuilder().followRedirects(true).build();
    @NonNull
    private final Map<String, String> headers;
    @NonNull
    private final String userAgent;
    @NonNull
    private final WebActivity webActivity;

    public CryoWebViewClient(@NonNull WebActivity webActivity2, @NonNull String userAgent2, @NonNull Map<String, String> headers2) {
        this.webActivity = webActivity2;
        this.userAgent = userAgent2;
        this.headers = headers2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0075 A[SYNTHETIC, Splitter:B:30:0x0075] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean shouldOverrideUrlLoading(android.webkit.WebView r7, java.lang.String r8) {
        /*
            r6 = this;
            r0 = 1
            java.lang.Object[] r1 = new java.lang.Object[r0]
            r2 = 0
            r1[r2] = r8
            java.lang.String r3 = "Loading URL in WebView: %s"
            com.jobstrak.drawingfun.sdk.utils.LogUtils.debug(r3, r1)
            android.net.Uri r1 = android.net.Uri.parse(r8)
            java.lang.String r3 = r1.getScheme()
            int r4 = r3.hashCode()
            r5 = -1183762788(0xffffffffb971369c, float:-2.3003895E-4)
            if (r4 == r5) goto L_0x002c
            r5 = -1081306052(0xffffffffbf8c943c, float:-1.0982738)
            if (r4 == r5) goto L_0x0022
        L_0x0021:
            goto L_0x0036
        L_0x0022:
            java.lang.String r4 = "market"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0021
            r3 = 0
            goto L_0x0037
        L_0x002c:
            java.lang.String r4 = "intent"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0021
            r3 = 1
            goto L_0x0037
        L_0x0036:
            r3 = -1
        L_0x0037:
            java.lang.String r4 = "An error occurred while opening GPM"
            if (r3 == 0) goto L_0x0075
            if (r3 == r0) goto L_0x0055
            java.util.Map<java.lang.String, java.lang.String> r3 = r6.headers
            if (r3 == 0) goto L_0x0054
            boolean r3 = r3.isEmpty()
            if (r3 != 0) goto L_0x0054
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 19
            if (r3 >= r4) goto L_0x004e
            goto L_0x0054
        L_0x004e:
            java.util.Map<java.lang.String, java.lang.String> r2 = r6.headers
            r7.loadUrl(r8, r2)
            return r0
        L_0x0054:
            return r2
        L_0x0055:
            android.net.Uri$Builder r3 = r1.buildUpon()
            java.lang.String r5 = "https"
            android.net.Uri$Builder r3 = r3.scheme(r5)
            android.net.Uri r3 = r3.build()
            r6.openMarket(r3)     // Catch:{ ActivityNotFoundException -> 0x0067 }
            return r0
        L_0x0067:
            r0 = move-exception
            java.lang.Object[] r5 = new java.lang.Object[r2]
            com.jobstrak.drawingfun.sdk.utils.LogUtils.error(r4, r0, r5)
            java.lang.String r4 = r3.toString()
            r7.loadUrl(r4)
            return r2
        L_0x0075:
            r6.openMarket(r1)     // Catch:{ ActivityNotFoundException -> 0x0079 }
            return r0
        L_0x0079:
            r0 = move-exception
            java.lang.Object[] r3 = new java.lang.Object[r2]
            com.jobstrak.drawingfun.sdk.utils.LogUtils.error(r4, r0, r3)
            r6.loadMarketUrl(r7, r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.sdk.activity.web.CryoWebViewClient.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
    }

    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        return interceptRequest(view, url, null);
    }

    @TargetApi(21)
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        return interceptRequest(view, request.getUrl().toString(), request.getRequestHeaders());
    }

    @Nullable
    private WebResourceResponse interceptRequest(WebView view, String url, Map<String, String> requestHeaders) {
        LogUtils.debug("%s", url);
        Map<String, String> map = this.headers;
        if (map == null || map.isEmpty()) {
            return super.shouldInterceptRequest(view, url);
        }
        Headers.Builder headersBuilder = new Headers.Builder();
        if (requestHeaders != null) {
            for (Map.Entry<String, String> h : requestHeaders.entrySet()) {
                headersBuilder.add(((String) h.getKey()).trim(), ((String) h.getValue()).trim());
            }
        } else {
            headersBuilder.add("User-Agent", this.userAgent);
        }
        for (Map.Entry<String, String> h2 : this.headers.entrySet()) {
            headersBuilder.add(((String) h2.getKey()).trim(), ((String) h2.getValue()).trim());
        }
        try {
            Response response = this.client.newCall(new Request.Builder().url(url.trim()).headers(headersBuilder.build()).get().build()).execute();
            return new WebResourceResponse(null, response.header("content-encoding", "utf-8"), response.body().byteStream());
        } catch (IOException e) {
            LogUtils.error(e);
            return null;
        }
    }

    private void openMarket(Uri uri) {
        LogUtils.debug("Opening Google Play Market...", new Object[0]);
        this.webActivity.startActivity(new Intent("android.intent.action.VIEW", uri));
        this.webActivity.finish();
    }

    private void loadMarketUrl(WebView webView, Uri uri) {
        webView.loadUrl(String.format(MARKET_URL, uri.getHost(), uri.getQuery()));
    }
}
