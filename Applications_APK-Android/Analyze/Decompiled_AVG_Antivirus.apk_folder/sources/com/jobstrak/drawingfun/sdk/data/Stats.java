package com.jobstrak.drawingfun.sdk.data;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.gson.Gson;
import com.jobstrak.drawingfun.lib.gson.reflect.TypeToken;
import com.jobstrak.drawingfun.sdk.model.Stat;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.PrefsUtils;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Stats {
    private static final transient Type listType = new TypeToken<List<Stat>>() {
    }.getType();
    private static volatile transient Stats stats;
    private static final transient Object sync = new Object();
    @NonNull
    private List<Stat> list = new ArrayList();

    @NonNull
    public static Stats getInstance() {
        Stats localInstance = stats;
        if (localInstance == null) {
            synchronized (sync) {
                localInstance = stats;
                if (localInstance == null) {
                    Stats loadStats = loadStats();
                    stats = loadStats;
                    localInstance = loadStats;
                }
            }
        }
        return localInstance;
    }

    private Stats() {
    }

    private Stats(@NonNull List<Stat> list2) {
        this.list = list2;
    }

    private void save() {
        synchronized (sync) {
            PrefsUtils.getSharedPreferences().edit().putString(PrefsUtils.PREFS_STATS, new Gson().toJson(new ArrayList(this.list))).apply();
        }
    }

    private static Stats loadStats() {
        String json = PrefsUtils.getSharedPreferences().getString(PrefsUtils.PREFS_STATS, null);
        if (json != null) {
            return new Stats(new ArrayList((Collection) new Gson().fromJson(json, listType)));
        }
        return new Stats();
    }

    public void add(@NonNull Stat stat) {
        LogUtils.debug("Stat added: " + stat.toString(), new Object[0]);
        this.list.add(stat);
        save();
    }

    public int size() {
        return this.list.size();
    }

    @NonNull
    public List<Stat> getAll() {
        return new ArrayList(this.list);
    }

    public void clear() {
        this.list.clear();
        save();
    }

    public void addAll(@NonNull List<Stat> stats2) {
        this.list.addAll(stats2);
        save();
    }
}
