package com.jobstrak.drawingfun.sdk.service.detector;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.service.validator.device.DuplicateServiceValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.NetworkStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.icon.IconAdDelayStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.icon.IconAdEnableStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.icon.IconAdPerDayLimitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.ConfigStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitialDelayValidator;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.ad.ShowIconAdTask;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class IconAdDetector extends Detector {
    @NonNull
    private TaskFactory<ShowIconAdTask> showIconAdTaskTaskFactory;
    @NonNull
    private final Validator[] validators;

    public IconAdDetector(@NonNull Config config, @NonNull Settings settings, @NonNull TaskFactory<ShowIconAdTask> showIconAdTaskTaskFactory2) {
        super(config, settings);
        this.validators = new Validator[]{new DuplicateServiceValidator(), new InitValidator(config, settings), new ConfigStateValidator(settings), new InitialDelayValidator(config, settings), new NetworkStateValidator(config), new IconAdEnableStateValidator(config), new IconAdPerDayLimitValidator(config, settings), new IconAdDelayStateValidator(config, settings)};
        this.showIconAdTaskTaskFactory = showIconAdTaskTaskFactory2;
    }

    public void tick(Detector.Delegate delegate) {
    }

    public boolean detect(Detector.Delegate delegate) {
        for (Validator validator : this.validators) {
            if (!validator.validate(delegate.getCurrentTime())) {
                LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validator.getReason());
                return true;
            }
        }
        this.showIconAdTaskTaskFactory.create().execute(new Void[0]);
        getSettings().setNextIconAdTime(delegate.getCurrentTime() + getConfig().getIconAdDelay());
        getSettings().save();
        return true;
    }
}
