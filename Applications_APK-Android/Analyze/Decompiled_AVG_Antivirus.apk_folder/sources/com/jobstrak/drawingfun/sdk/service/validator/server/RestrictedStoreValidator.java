package com.jobstrak.drawingfun.sdk.service.validator.server;

import android.text.TextUtils;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.StringUtils;

public class RestrictedStoreValidator extends Validator {
    private final Config config;
    private final String storePackageName;

    public RestrictedStoreValidator(Config config2, String storePackageName2) {
        this.config = config2;
        this.storePackageName = storePackageName2;
    }

    public boolean validate(long currentTime) {
        return TextUtils.isEmpty(this.storePackageName) || !StringUtils.wildcardAnyMatch(this.config.getRestrictedStores(), this.storePackageName);
    }

    public String getReason() {
        return "has been downloaded from the restricted store: " + this.storePackageName;
    }
}
