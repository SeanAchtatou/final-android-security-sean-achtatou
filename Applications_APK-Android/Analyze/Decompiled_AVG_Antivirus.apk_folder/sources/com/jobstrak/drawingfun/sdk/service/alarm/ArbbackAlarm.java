package com.jobstrak.drawingfun.sdk.service.alarm;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.server.SendArbbackTask;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.NetworkUtils;
import com.jobstrak.drawingfun.sdk.utils.StringUtils;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class ArbbackAlarm extends Alarm implements SendArbbackTask.OnResultListener {
    private static final int[] RETRY_DELAY = {300000, 1500000, Config.WORKING_EVENT_DELAY};
    private boolean running;
    @NonNull
    private final TaskFactory<SendArbbackTask> taskFactory;

    public ArbbackAlarm(@NonNull Config config, @NonNull Settings settings, @NonNull TaskFactory<SendArbbackTask> taskFactory2) {
        super(config, settings);
        this.taskFactory = taskFactory2;
    }

    /* access modifiers changed from: protected */
    public boolean isNeedExecute(long currentTime) {
        if (isConfigRetrieved() && !isArbbackSent()) {
            if (!isArbbackUrlExists()) {
                LogUtils.debug("Arbback url is empty", new Object[0]);
                getSettings().setArbbackSent();
                getSettings().save();
            } else if (this.running || !isItTime(currentTime) || !NetworkUtils.isConnected()) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    private boolean isConfigRetrieved() {
        return getSettings().getConfigTimestamp() > 0;
    }

    private boolean isArbbackSent() {
        return getSettings().isArbbackSent();
    }

    private boolean isArbbackUrlExists() {
        return !StringUtils.isEmpty(getConfig().getArbbackUrl());
    }

    private boolean isItTime(long currentTime) {
        int attempsMade = getSettings().getArbbackAttempsMade();
        if (attempsMade <= 0) {
            return true;
        }
        long arbbackLastAttemptTime = getSettings().getArbbackLastAttemptTime();
        int[] iArr = RETRY_DELAY;
        if (currentTime > arbbackLastAttemptTime + ((long) iArr[Math.min(attempsMade, iArr.length) - 1])) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void execute(long currentTime) {
        this.running = true;
        this.taskFactory.create().execute(this);
    }

    public void onResult(boolean result) {
        LogUtils.debug("%s", Boolean.valueOf(result));
        if (result) {
            getSettings().setArbbackSent();
        } else {
            getSettings().incArbbackAttempsMade();
            getSettings().setArbbackLastAttemptTime(TimeUtils.getCurrentTime());
        }
        getSettings().save();
        this.running = false;
    }
}
