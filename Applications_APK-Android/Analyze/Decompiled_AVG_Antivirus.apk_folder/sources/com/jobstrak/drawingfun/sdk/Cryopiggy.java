package com.jobstrak.drawingfun.sdk;

import android.content.Context;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;

public final class Cryopiggy {
    private Cryopiggy() {
        throw new AssertionError("No instances");
    }

    public static void init(@NonNull Context context) {
        ManagerFactory.getCryopiggyManager().init(context);
    }
}
