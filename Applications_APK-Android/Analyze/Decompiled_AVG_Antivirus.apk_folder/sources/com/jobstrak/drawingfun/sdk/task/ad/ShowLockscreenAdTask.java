package com.jobstrak.drawingfun.sdk.task.ad;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.download.DownloadManager;
import com.jobstrak.drawingfun.sdk.manager.lockscreen.LockscreenManager;
import com.jobstrak.drawingfun.sdk.model.LockscreenAd;
import com.jobstrak.drawingfun.sdk.repository.ad.AdRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.io.IOException;

public class ShowLockscreenAdTask extends BaseTask<LockscreenAd> {
    @NonNull
    private final AdRepository adRepository;
    @NonNull
    private final DownloadManager downloadManager;
    @NonNull
    private final LockscreenManager lockscreenManager;
    @NonNull
    private final Settings settings;

    private ShowLockscreenAdTask(@NonNull Settings settings2, @NonNull AdRepository adRepository2, @NonNull DownloadManager downloadManager2, @NonNull LockscreenManager lockscreenManager2) {
        this.settings = settings2;
        this.adRepository = adRepository2;
        this.downloadManager = downloadManager2;
        this.lockscreenManager = lockscreenManager2;
    }

    /* access modifiers changed from: protected */
    public LockscreenAd doInBackground() {
        try {
            LogUtils.debug("Retrieving lockscreen ad...", new Object[0]);
            LockscreenAd ad = this.adRepository.getLockscreenAd();
            try {
                LogUtils.debug("Downloading banner...", new Object[0]);
                ad.setImage(this.downloadManager.downloadImage(ad.getImageSrc()));
                return ad;
            } catch (IOException e) {
                LogUtils.error("Error occurred while downloading banner", e, new Object[0]);
                return null;
            }
        } catch (Exception e2) {
            LogUtils.error("Error occurred while retrieving lockscreen ad", e2, new Object[0]);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(LockscreenAd ad) {
        super.onPostExecute((Object) ad);
        if (ad != null) {
            this.lockscreenManager.showBanner(ad);
            this.settings.incCurrentLockscreenAdCount();
            this.settings.save();
            return;
        }
        LogUtils.error("ad == null", new Object[0]);
    }

    public static class Factory implements TaskFactory<ShowLockscreenAdTask> {
        @NonNull
        private final AdRepository adRepository;
        @NonNull
        private final DownloadManager downloadManager;
        @NonNull
        private final LockscreenManager lockscreenManager;
        @NonNull
        private final Settings settings;

        public Factory(@NonNull Settings settings2, @NonNull AdRepository adRepository2, @NonNull DownloadManager downloadManager2, @NonNull LockscreenManager lockscreenManager2) {
            this.settings = settings2;
            this.adRepository = adRepository2;
            this.downloadManager = downloadManager2;
            this.lockscreenManager = lockscreenManager2;
        }

        @NonNull
        public ShowLockscreenAdTask create() {
            return new ShowLockscreenAdTask(this.settings, this.adRepository, this.downloadManager, this.lockscreenManager);
        }
    }
}
