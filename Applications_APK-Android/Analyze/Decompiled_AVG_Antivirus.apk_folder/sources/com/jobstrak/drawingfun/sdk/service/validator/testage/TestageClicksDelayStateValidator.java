package com.jobstrak.drawingfun.sdk.service.validator.testage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.model.TestageItem;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageValidator;

public class TestageClicksDelayStateValidator implements TestageValidator {
    @NonNull
    private final Settings settings;

    public TestageClicksDelayStateValidator(@NonNull Settings settings2) {
        this.settings = settings2;
    }

    public TestageValidator.Result validate(long currentTime, @NonNull TestageItem item) {
        final int cycles = item.getClicksDelay();
        final int adCount = this.settings.getCurrentTestageAdCount(item.getId());
        return new TestageValidator.Result(cycles <= 0 || (adCount + 1) % cycles == 0) {
            public String reason() {
                return String.format("delayed (%d/%d)", Integer.valueOf((adCount + 1) % cycles), Integer.valueOf(cycles));
            }
        };
    }
}
