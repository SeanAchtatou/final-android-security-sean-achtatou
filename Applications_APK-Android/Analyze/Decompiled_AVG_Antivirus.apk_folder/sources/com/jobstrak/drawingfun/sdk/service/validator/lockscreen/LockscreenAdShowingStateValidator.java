package com.jobstrak.drawingfun.sdk.service.validator.lockscreen;

import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class LockscreenAdShowingStateValidator extends Validator {
    public boolean validate(long currentTime) {
        return !ManagerFactory.getLockscreenManager().isBannerVisible();
    }

    public String getReason() {
        return "lockscreen banner is already showing";
    }
}
