package com.jobstrak.drawingfun.sdk.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.mopub.mobileads.util.WebViews;
import com.jobstrak.drawingfun.sdk.activity.web.CryoWebViewClient;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.model.HTMLAd;
import com.jobstrak.drawingfun.sdk.utils.ResourcesUtils;
import com.jobstrak.drawingfun.sdk.utils.ScreenUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class WebActivity extends BaseActivity {
    public static final String EXTRA_BANNER = "extra_banner";
    public static final String EXTRA_LISTENER_UUID = "extra_listener_uuid";
    private static final String X_REQUESTED_WITH = "X-Requested-With";
    public static final Map<String, HTMLBannerListener> listeners = new HashMap();
    private HTMLBannerListener listener;

    public interface HTMLBannerListener {
        void onClick();

        void onDismiss();

        void onFail();

        void onLoad(HTMLAd hTMLAd);

        void onShow();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Map<String, String> headers;
        super.onCreate(savedInstanceState);
        if (getIntent().hasExtra(EXTRA_BANNER)) {
            HTMLAd banner = (HTMLAd) getIntent().getSerializableExtra(EXTRA_BANNER);
            if (getIntent().hasExtra(EXTRA_LISTENER_UUID)) {
                this.listener = listeners.get(getIntent().getStringExtra(EXTRA_LISTENER_UUID));
                if (banner.getBundle() != null) {
                    headers = Collections.singletonMap(X_REQUESTED_WITH, banner.getBundle());
                } else {
                    headers = Collections.emptyMap();
                }
                final WebView bannerWebView = getWebView(headers);
                FrameLayout.LayoutParams bannerWebViewLayoutParams = new FrameLayout.LayoutParams(-1, -1);
                ImageView closeIcon = new ImageView(this);
                FrameLayout.LayoutParams closeIconLayoutParams = new FrameLayout.LayoutParams(-2, -2);
                closeIconLayoutParams.gravity = 5;
                closeIconLayoutParams.rightMargin = ScreenUtils.dp(16);
                closeIconLayoutParams.topMargin = ScreenUtils.dp(8);
                closeIcon.setImageBitmap(ResourcesUtils.getCloseIcon());
                closeIcon.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        WebActivity.this.finish();
                    }
                });
                getContentView().removeAllViews();
                getContentView().addView(bannerWebView, bannerWebViewLayoutParams);
                getContentView().addView(closeIcon, closeIconLayoutParams);
                long delayBeforeHtml = Config.getInstance().getDelayBeforeHtml();
                if (delayBeforeHtml > 0) {
                    final ProgressBar progressBar = new ProgressBar(this);
                    FrameLayout.LayoutParams progressBarLayoutParams = new FrameLayout.LayoutParams(ScreenUtils.dp(64), ScreenUtils.dp(64));
                    progressBarLayoutParams.gravity = 17;
                    progressBar.setIndeterminate(true);
                    getContentView().addView(progressBar, progressBarLayoutParams);
                    bannerWebView.setVisibility(4);
                    progressBar.postDelayed(new Runnable() {
                        public void run() {
                            bannerWebView.setVisibility(0);
                            progressBar.setVisibility(8);
                        }
                    }, delayBeforeHtml);
                }
                if (banner.getHtmlData() != null) {
                    bannerWebView.loadDataWithBaseURL(banner.getUrl(), banner.getHtmlData(), "text/html", "UTF-8", null);
                } else {
                    bannerWebView.loadUrl(banner.getUrl(), headers);
                }
                this.listener.onShow();
                return;
            }
            throw new IllegalArgumentException(String.format("Extra '%s' not found", EXTRA_LISTENER_UUID));
        }
        throw new IllegalArgumentException(String.format("Extra '%s' not found", EXTRA_BANNER));
    }

    @NonNull
    private WebView getWebView(@NonNull Map<String, String> headers) {
        WebView bannerWebView = new WebView(this);
        WebViews.setDisableJSChromeClient(bannerWebView);
        String userAgent = bannerWebView.getSettings().getUserAgentString().replace("; wv", "");
        bannerWebView.setWebViewClient(new CryoWebViewClient(this, userAgent, headers));
        bannerWebView.getSettings().setUseWideViewPort(true);
        bannerWebView.getSettings().setJavaScriptEnabled(true);
        bannerWebView.getSettings().setLoadWithOverviewMode(true);
        bannerWebView.getSettings().setUserAgentString(userAgent);
        return bannerWebView;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        HTMLBannerListener hTMLBannerListener = this.listener;
        if (hTMLBannerListener != null) {
            hTMLBannerListener.onDismiss();
        }
        finish();
    }

    public static void registerListener(String uuid, HTMLBannerListener listener2) {
        listeners.put(uuid, listener2);
    }
}
