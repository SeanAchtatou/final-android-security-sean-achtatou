package com.jobstrak.drawingfun.sdk.manager.flurry;

import android.content.Context;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class FlurryManagerImpl implements FlurryManager {
    public void init(@NonNull Context context, @NonNull String flurryKey) {
        if (flurryKey != null) {
            try {
                Class.forName("com.flurry.android.FlurryAgent").getMethod("init", Context.class, String.class).invoke(null, context, flurryKey);
                LogUtils.debug("Flurry enabled", new Object[0]);
            } catch (ClassNotFoundException e) {
                LogUtils.debug("Flurry disabled due to lack of FlurrySDK", new Object[0]);
            } catch (Exception e2) {
                LogUtils.error(e2);
            }
        } else {
            LogUtils.debug("Flurry disabled due to lack of Flurry key in meta data", new Object[0]);
        }
    }

    public void onStartSession(@NonNull Context context) {
        try {
            Class.forName("com.flurry.android.FlurryAgent").getMethod("onStartSession", Context.class).invoke(null, context);
        } catch (ClassNotFoundException | Exception e) {
        }
    }

    public void onEndSession(@NonNull Context context) {
        try {
            Class.forName("com.flurry.android.FlurryAgent").getMethod("onEndSession", Context.class).invoke(null, context);
        } catch (ClassNotFoundException | Exception e) {
        }
    }
}
