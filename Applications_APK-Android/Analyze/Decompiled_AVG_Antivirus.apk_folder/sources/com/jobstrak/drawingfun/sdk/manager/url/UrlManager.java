package com.jobstrak.drawingfun.sdk.manager.url;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.okhttp3.RequestBody;

public interface UrlManager {

    public interface Builder<T> {
        @NonNull
        Builder<T> addCustomParameter(@NonNull String str, @NonNull String str2);

        @NonNull
        Builder<T> addParameters(@NonNull String... strArr);

        @NonNull
        T build();
    }

    public static class Link {
        public static final String HOST = "host";
        public static final String PATH = "path";
        public static final String SCHEME = "scheme";
    }

    public static class Parameter {
        public static final String APP_ID = "app_id";
        public static final String A_VER = "a_ver";
        public static final String BRAND = "brand";
        public static final String CLIENT_ID = "client_id";
        public static final String CONNECTION_TYPE = "connection_type";
        public static final String DEVICE_ID = "device_id";
        public static final String EXTRA = "extra";
        public static final String GAID = "gaid";
        public static final String HEIGHT = "height";
        public static final String IMEI = "imei";
        public static final String IS_TABLET = "is_tablet";
        public static final String IS_WIFI = "is_wifi";
        public static final String LIGHT = "light";
        public static final String MODEL = "model";
        public static final String SDK_VER = "sdk_ver";
        public static final String TS = "ts";
        public static final String UA = "ua";
        public static final String WIDTH = "width";
    }

    public interface Store {
        @Nullable
        String getValue(@NonNull String str);
    }

    @NonNull
    Builder<String> create(@NonNull String str);

    @NonNull
    Builder<RequestBody> createBody();
}
