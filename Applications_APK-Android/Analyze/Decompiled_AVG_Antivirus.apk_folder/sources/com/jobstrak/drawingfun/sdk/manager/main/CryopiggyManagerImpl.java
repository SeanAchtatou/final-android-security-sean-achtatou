package com.jobstrak.drawingfun.sdk.manager.main;

import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.BuildConfig;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.SdkConfig;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.data.Stats;
import com.jobstrak.drawingfun.sdk.data.meta.MetaData;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.manager.main.init.AndroidInit;
import com.jobstrak.drawingfun.sdk.manager.main.init.AppMetricaInit;
import com.jobstrak.drawingfun.sdk.manager.main.init.CryopiggyInit;
import com.jobstrak.drawingfun.sdk.manager.main.init.DefaultAndroidOInit;
import com.jobstrak.drawingfun.sdk.manager.main.init.FlurryInit;
import com.jobstrak.drawingfun.sdk.manager.main.init.Init;
import com.jobstrak.drawingfun.sdk.manager.main.init.InitException;
import com.jobstrak.drawingfun.sdk.manager.main.init.WakeLockTimerInit;
import com.jobstrak.drawingfun.sdk.manager.main.init.WorkManagerInit;
import com.jobstrak.drawingfun.sdk.utils.ContextUtilsKt;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.lang.ref.WeakReference;

public class CryopiggyManagerImpl implements CryopiggyManager {
    @Nullable
    private AndroidManager androidManager;
    @Nullable
    private Config config;
    @Nullable
    private WeakReference<Context> context;
    @Nullable
    private Init[] inits;
    @Nullable
    private MetaData metaData;
    @Nullable
    private Settings settings;

    public boolean init(@NonNull Context context2) {
        Init androidInit;
        Context context3 = context2;
        String str = "enabled";
        if (context3 == null) {
            throw new NullPointerException("context == null");
        } else if (context2.getApplicationContext() == null) {
            throw new NullPointerException("application context == null");
        } else if (!ContextUtilsKt.isMainProcess(context2)) {
            return false;
        } else {
            try {
                new AppMetricaInit(context2.getApplicationContext(), BuildConfig.APPMETRICA_KEY).execute();
            } catch (Exception e) {
                LogUtils.error(e);
            }
            if (optContext() == null) {
                Context appContext = context2.getApplicationContext();
                this.context = new WeakReference<>(appContext);
                this.androidManager = ManagerFactory.getAndroidManager();
                this.metaData = this.androidManager.readMetaData();
                this.config = Config.getInstance();
                this.settings = Settings.getInstance();
                if (Build.VERSION.SDK_INT >= 26) {
                    androidInit = new DefaultAndroidOInit(appContext, this.androidManager);
                } else {
                    androidInit = new AndroidInit(this.androidManager);
                }
                this.inits = new Init[]{new WorkManagerInit(context3), new FlurryInit(context3, ManagerFactory.createFlurryManager(), this.metaData.getFlurryKey()), new CryopiggyInit(this, this.config, this.settings, this.metaData.getAppId(), this.metaData.getServer(), true), new WakeLockTimerInit(context3), androidInit};
                try {
                    LogUtils.debug("Starting %s...", SdkConfig.getSdkName());
                    LogUtils.debug("> Version: %s (%s)", BuildConfig.VERSION_NAME, 82);
                    LogUtils.debug("> Flavor: %s", "light");
                    LogUtils.debug("> Stats: %s", str);
                    Object[] objArr = new Object[1];
                    if (BuildConfig.DEBUG) {
                        str = "disabled";
                    }
                    objArr[0] = str;
                    LogUtils.debug("> Proguard: %s", objArr);
                    executeInits();
                } catch (InitException e2) {
                    LogUtils.error(e2);
                    abandonInits();
                    return false;
                }
            }
            return true;
        }
    }

    private void executeInits() throws InitException {
        Init[] initArr = this.inits;
        if (initArr != null) {
            for (Init init : initArr) {
                init.execute();
            }
            return;
        }
        LogUtils.error("inits == null", new Object[0]);
    }

    private void abandonInits() {
        Init[] initArr = this.inits;
        if (initArr != null) {
            for (Init init : initArr) {
                if (init.isAbandonable()) {
                    init.abandon();
                }
            }
            return;
        }
        LogUtils.error("inits == null", new Object[0]);
    }

    public void clear() {
        LogUtils.debug("Deleting all persisted data...", new Object[0]);
        if (this.context != null) {
            abandonInits();
            Stats.getInstance().clear();
            Settings.clear();
            Config.clear();
            return;
        }
        LogUtils.error("context == null", new Object[0]);
    }

    @NonNull
    public Context getContext() throws NotInitializedException {
        WeakReference<Context> weakReference = this.context;
        if (weakReference != null) {
            return weakReference.get();
        }
        throw new NotInitializedException();
    }

    @Nullable
    public Context optContext() {
        WeakReference<Context> weakReference = this.context;
        if (weakReference == null) {
            return null;
        }
        return weakReference.get();
    }

    public boolean isInitialized() {
        return this.context != null;
    }

    public boolean isDebug() {
        MetaData metaData2 = this.metaData;
        return metaData2 != null && metaData2.isDebug();
    }
}
