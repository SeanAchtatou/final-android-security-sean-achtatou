package com.jobstrak.drawingfun.sdk.service.alarm;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.data.Stats;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.stat.StatsSendTask;
import com.jobstrak.drawingfun.sdk.utils.NetworkUtils;

public class StatsSendAlarm extends Alarm {
    private static final int GOAL = 6;
    @NonNull
    private final Stats stats;
    @NonNull
    private final TaskFactory<StatsSendTask> statsSendTask;

    public StatsSendAlarm(@NonNull Stats stats2, @NonNull Config config, @NonNull Settings settings, @NonNull TaskFactory<StatsSendTask> statsSendTask2) {
        super(config, settings);
        this.stats = stats2;
        this.statsSendTask = statsSendTask2;
    }

    public boolean isNeedExecute(long currentTime) {
        return isEnabled() && isRegistered() && isItTime() && isConnected();
    }

    public void execute(long currentTime) {
        this.statsSendTask.create().execute(new Void[0]);
    }

    private boolean isEnabled() {
        return true;
    }

    private boolean isRegistered() {
        return getSettings().getClientId() != null;
    }

    private boolean isItTime() {
        return this.stats.size() >= 6;
    }

    private boolean isConnected() {
        return NetworkUtils.isConnected(getConfig().isOnlyFastConnection());
    }
}
