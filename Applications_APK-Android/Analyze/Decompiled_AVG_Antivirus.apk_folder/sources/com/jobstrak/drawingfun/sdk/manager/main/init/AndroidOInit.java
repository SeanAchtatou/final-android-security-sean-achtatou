package com.jobstrak.drawingfun.sdk.manager.main.init;

import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\b\u0010\b\u001a\u00020\u0007H\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/manager/main/init/AndroidOInit;", "Lcom/jobstrak/drawingfun/sdk/manager/main/init/Init;", "Lcom/jobstrak/drawingfun/sdk/manager/main/init/Abandonable;", "androidManager", "Lcom/jobstrak/drawingfun/sdk/manager/android/AndroidManager;", "(Lcom/jobstrak/drawingfun/sdk/manager/android/AndroidManager;)V", "doAbandon", "", "doExecute", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: AndroidOInit.kt */
public class AndroidOInit extends Init implements Abandonable {
    private final AndroidManager androidManager;

    public AndroidOInit(@NotNull AndroidManager androidManager2) {
        Intrinsics.checkParameterIsNotNull(androidManager2, "androidManager");
        this.androidManager = androidManager2;
    }

    /* access modifiers changed from: protected */
    public void doExecute() {
        this.androidManager.startService();
        this.androidManager.startScreenReceiver();
    }

    public void doAbandon() {
        this.androidManager.stopService();
        this.androidManager.stopScreenReceiver();
    }
}
