package com.jobstrak.drawingfun.sdk.manager.main;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface CryopiggyManager {
    void clear();

    @NonNull
    Context getContext() throws NotInitializedException;

    boolean init(@NonNull Context context);

    boolean isDebug();

    boolean isInitialized();

    @Nullable
    Context optContext();
}
