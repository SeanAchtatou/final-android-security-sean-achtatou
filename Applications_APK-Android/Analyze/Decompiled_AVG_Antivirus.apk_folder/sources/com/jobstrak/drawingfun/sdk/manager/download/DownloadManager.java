package com.jobstrak.drawingfun.sdk.manager.download;

import android.graphics.Bitmap;
import java.io.IOException;
import java.net.URL;

public interface DownloadManager {
    Bitmap downloadImage(String str) throws IOException;

    Bitmap downloadImage(URL url) throws IOException;
}
