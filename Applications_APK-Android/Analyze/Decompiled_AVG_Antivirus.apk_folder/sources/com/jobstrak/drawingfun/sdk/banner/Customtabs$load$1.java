package com.jobstrak.drawingfun.sdk.banner;

import com.jobstrak.drawingfun.sdk.model.HTMLAd;
import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.BuildersKt;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Dispatchers;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"<anonymous>", "", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "com.jobstrak.drawingfun.sdk.banner.Customtabs$load$1", f = "Customtabs.kt", i = {}, l = {73}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Customtabs.kt */
final class Customtabs$load$1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    Object L$0;
    int label;
    private CoroutineScope p$;
    final /* synthetic */ Customtabs this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    Customtabs$load$1(Customtabs customtabs, Continuation continuation) {
        super(2, continuation);
        this.this$0 = customtabs;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        Customtabs$load$1 customtabs$load$1 = new Customtabs$load$1(this.this$0, continuation);
        CoroutineScope coroutineScope = (CoroutineScope) obj;
        customtabs$load$1.p$ = (CoroutineScope) obj;
        return customtabs$load$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((Customtabs$load$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object obj;
        Customtabs customtabs;
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            CoroutineScope coroutineScope = this.p$;
            Customtabs customtabs2 = this.this$0;
            this.L$0 = customtabs2;
            this.label = 1;
            obj = BuildersKt.withContext(Dispatchers.getIO(), new AnonymousClass1(this, null), this);
            if (obj == coroutine_suspended) {
                return coroutine_suspended;
            }
            customtabs = customtabs2;
        } else if (i == 1) {
            customtabs = (Customtabs) this.L$0;
            try {
                ResultKt.throwOnFailure(result);
                obj = result;
            } catch (Exception e) {
                this.this$0.fireOnFail("Failed to get custom tabs ad", e);
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        customtabs.ad = (HTMLAd) obj;
        this.this$0.fireOnLoad();
        return Unit.INSTANCE;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"<anonymous>", "Lcom/jobstrak/drawingfun/sdk/model/HTMLAd;", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
    @DebugMetadata(c = "com.jobstrak.drawingfun.sdk.banner.Customtabs$load$1$1", f = "Customtabs.kt", i = {}, l = {}, m = "invokeSuspend", n = {}, s = {})
    /* renamed from: com.jobstrak.drawingfun.sdk.banner.Customtabs$load$1$1  reason: invalid class name */
    /* compiled from: Customtabs.kt */
    static final class AnonymousClass1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super HTMLAd>, Object> {
        int label;
        private CoroutineScope p$;
        final /* synthetic */ Customtabs$load$1 this$0;

        {
            this.this$0 = r1;
        }

        @NotNull
        public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            AnonymousClass1 r0 = new AnonymousClass1(this.this$0, continuation);
            CoroutineScope coroutineScope = (CoroutineScope) obj;
            r0.p$ = (CoroutineScope) obj;
            return r0;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((AnonymousClass1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        @Nullable
        public final Object invokeSuspend(@NotNull Object result) {
            IntrinsicsKt.getCOROUTINE_SUSPENDED();
            if (this.label == 0) {
                ResultKt.throwOnFailure(result);
                CoroutineScope coroutineScope = this.p$;
                return this.this$0.this$0.adRepository.getHTMLAd(HTMLAd.Type.CUSTOMTABS);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
