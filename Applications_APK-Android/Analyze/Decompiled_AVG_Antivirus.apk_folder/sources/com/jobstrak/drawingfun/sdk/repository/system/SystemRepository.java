package com.jobstrak.drawingfun.sdk.repository.system;

import android.content.Context;
import androidx.annotation.NonNull;

public interface SystemRepository {
    @NonNull
    String getForegroundPackageName(@NonNull Context context);
}
