package com.jobstrak.drawingfun.sdk.repository;

import android.os.Build;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.repository.ad.AdRepository;
import com.jobstrak.drawingfun.sdk.repository.ad.CryopiggyAdRepository;
import com.jobstrak.drawingfun.sdk.repository.server.CryopiggyServerRepository;
import com.jobstrak.drawingfun.sdk.repository.server.ServerRepository;
import com.jobstrak.drawingfun.sdk.repository.stat.CompositeStatRepository;
import com.jobstrak.drawingfun.sdk.repository.stat.CryopiggyStatRepository;
import com.jobstrak.drawingfun.sdk.repository.stat.StatRepository;
import com.jobstrak.drawingfun.sdk.repository.system.LollipopMR1SystemRepository;
import com.jobstrak.drawingfun.sdk.repository.system.NougatSystemRepository;
import com.jobstrak.drawingfun.sdk.repository.system.PreLollipopSystemRepository;
import com.jobstrak.drawingfun.sdk.repository.system.SystemRepository;
import javax.crypto.SecretKey;

public final class RepositoryFactory {
    private static final int VERSION_CODE_NOUGAT = 24;

    public static AdRepository getAdRepository(@NonNull UrlManager urlManager, @NonNull RequestManager requestManager) {
        return new CryopiggyAdRepository(urlManager, requestManager);
    }

    public static ServerRepository getServerRepository(@NonNull UrlManager urlManager, @NonNull RequestManager requestManager, @NonNull CryopiggyManager cryopiggyManager, @NonNull SecretKey secretKey) {
        return new CryopiggyServerRepository(urlManager, requestManager, cryopiggyManager, secretKey);
    }

    public static StatRepository getStatRepository(@NonNull Config config, @NonNull UrlManager urlManager, @NonNull UrlManager reservedUrlManager, @NonNull RequestManager requestManager) {
        return new CompositeStatRepository(config, new CryopiggyStatRepository(urlManager, requestManager, false), new CryopiggyStatRepository(reservedUrlManager, requestManager, true));
    }

    public static SystemRepository getSystemRepository() {
        if (Build.VERSION.SDK_INT >= 24) {
            return new NougatSystemRepository();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            return new LollipopMR1SystemRepository();
        }
        return new PreLollipopSystemRepository();
    }
}
