package com.jobstrak.drawingfun.sdk.model;

public enum NotificationAdWaterfallOrderType {
    FLAT,
    ROUNDROBIN
}
