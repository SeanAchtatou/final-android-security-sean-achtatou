package com.jobstrak.drawingfun.sdk;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 21)
public class JobService2 extends JobService {
    private final Handler handler = new Handler(Looper.getMainLooper());
    private Runnable runnable = null;

    public boolean onStartJob(final JobParameters params) {
        Runnable runnable2 = this.runnable;
        if (runnable2 != null) {
            this.handler.removeCallbacks(runnable2);
        }
        Cryopiggy.init(this);
        this.runnable = new Runnable() {
            public void run() {
                JobService2.this.jobFinished(params, false);
            }
        };
        this.handler.postDelayed(this.runnable, 180000);
        return true;
    }

    public boolean onStopJob(JobParameters params) {
        Runnable runnable2 = this.runnable;
        if (runnable2 == null) {
            return true;
        }
        this.handler.removeCallbacks(runnable2);
        return true;
    }
}
