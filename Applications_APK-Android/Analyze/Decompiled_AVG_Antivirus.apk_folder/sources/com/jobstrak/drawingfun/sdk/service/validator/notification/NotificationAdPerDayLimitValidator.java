package com.jobstrak.drawingfun.sdk.service.validator.notification;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class NotificationAdPerDayLimitValidator extends Validator {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;

    public NotificationAdPerDayLimitValidator(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        int limit = this.config.getNotificationAdCount();
        return limit <= 0 || this.settings.getNotificationAdCountAll() < limit;
    }

    public String getReason() {
        return String.format("daily notification ad limit exceeded (%s)", Integer.valueOf(this.config.getNotificationAdCount()));
    }
}
