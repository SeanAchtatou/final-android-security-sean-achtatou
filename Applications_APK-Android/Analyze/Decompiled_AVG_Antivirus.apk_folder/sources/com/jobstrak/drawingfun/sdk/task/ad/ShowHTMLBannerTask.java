package com.jobstrak.drawingfun.sdk.task.ad;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.okhttp3.Interceptor;
import com.jobstrak.drawingfun.lib.okhttp3.OkHttpClient;
import com.jobstrak.drawingfun.lib.okhttp3.Request;
import com.jobstrak.drawingfun.lib.okhttp3.Response;
import com.jobstrak.drawingfun.sdk.activity.WebActivity;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.model.HTMLAd;
import com.jobstrak.drawingfun.sdk.repository.ad.AdRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.SystemUtils;
import java.io.IOException;

public class ShowHTMLBannerTask extends BaseTask<HTMLAd> {
    @NonNull
    private final AdRepository adRepository;
    @NonNull
    private final AndroidManager androidManager;
    @Nullable
    private WebActivity.HTMLBannerListener listener;
    @Nullable
    private HTMLAd.Type type;

    private ShowHTMLBannerTask(@NonNull AdRepository adRepository2, @NonNull AndroidManager androidManager2) {
        this.adRepository = adRepository2;
        this.androidManager = androidManager2;
    }

    public void execute(@NonNull HTMLAd.Type type2, @NonNull WebActivity.HTMLBannerListener listener2) {
        this.type = type2;
        this.listener = listener2;
        super.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public HTMLAd doInBackground() {
        try {
            LogUtils.debug("Retrieving %s ad...", this.type.toString());
            HTMLAd htmlAd = this.adRepository.getHTMLAd(this.type);
            if (htmlAd != null) {
                if (!TextUtils.isEmpty(htmlAd.getUrl())) {
                    String htmlAdType = htmlAd.getType();
                    LogUtils.debug("type == " + htmlAdType, new Object[0]);
                    if (htmlAdType.equals(HTMLAd.TYPE_BROWSER)) {
                        return htmlAd;
                    }
                    String userAgent = SystemUtils.getDefaultUserAgent().replace("; wv", "");
                    LogUtils.debug("User-Agent: %s", userAgent);
                    Response response = new OkHttpClient.Builder().followRedirects(true).addNetworkInterceptor(new LoggingInterceptor()).build().newCall(new Request.Builder().url(htmlAd.getUrl()).addHeader("User-Agent", userAgent).get().build()).execute();
                    htmlAd.setBundle(response.header("bundle"));
                    if (response.header(HTMLAd.TYPE_MRAID) != null && response.header(HTMLAd.TYPE_MRAID).equals("true")) {
                        htmlAd.setType(HTMLAd.TYPE_MRAID);
                    }
                    if (!response.isSuccessful() || response.code() == 204) {
                        LogUtils.debug("HTMLAd received, but response code = %d", Integer.valueOf(response.code()));
                        return null;
                    }
                    try {
                        LogUtils.debug("Reading response body...", new Object[0]);
                        htmlAd.setHtmlData(response.body().string());
                    } catch (IOException e) {
                        LogUtils.error("An error occurred while reading response body", e, new Object[0]);
                    }
                    return htmlAd;
                }
            }
            LogUtils.error("url == null", new Object[0]);
            return null;
        } catch (Exception e2) {
            LogUtils.error("Error occurred while retrieving HTML ad", e2, new Object[0]);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(HTMLAd htmlAd) {
        super.onPostExecute((Object) htmlAd);
        if (htmlAd != null) {
            this.listener.onLoad(htmlAd);
            return;
        }
        LogUtils.debug("htmlAd == null", new Object[0]);
        this.listener.onFail();
    }

    public static class Factory implements TaskFactory<ShowHTMLBannerTask> {
        @NonNull
        private final AdRepository adRepository;
        @NonNull
        private final AndroidManager androidManager;

        public Factory(@NonNull AdRepository adRepository2, @NonNull AndroidManager androidManager2) {
            this.adRepository = adRepository2;
            this.androidManager = androidManager2;
        }

        @NonNull
        public ShowHTMLBannerTask create() {
            return new ShowHTMLBannerTask(this.adRepository, this.androidManager);
        }
    }

    private class LoggingInterceptor implements Interceptor {
        private LoggingInterceptor() {
        }

        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request request = chain.request();
            LogUtils.debug("Loading URL in OkHttp: %s", request.url());
            return chain.proceed(request);
        }
    }
}
