package com.jobstrak.drawingfun.sdk.manager.main.init;

import android.content.Context;
import com.jobstrak.drawingfun.sdk.manager.WakeLockManager;
import com.jobstrak.drawingfun.sdk.service.MainTimer;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/manager/main/init/WakeLockTimerInit;", "Lcom/jobstrak/drawingfun/sdk/manager/main/init/Init;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "doExecute", "", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: WakeLockTimerInit.kt */
public final class WakeLockTimerInit extends Init {
    private final Context context;

    public WakeLockTimerInit(@NotNull Context context2) {
        Intrinsics.checkParameterIsNotNull(context2, "context");
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void doExecute() {
        MainTimer.getInstance().startIfNotRunning();
        WakeLockManager.INSTANCE.acquire(this.context);
    }
}
