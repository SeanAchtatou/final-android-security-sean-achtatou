package com.jobstrak.drawingfun.sdk.service.detector;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.urlbuilder.UrlBuilder;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.backstage.BackstageManager;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.service.validator.backstage.BackstageAdDelayStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.backstage.BackstageAdEnableStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.backstage.BackstageAdPerDayLimitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.backstage.BackstageAdUrlStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.DuplicateServiceValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.NetworkStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.ConfigStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitialDelayValidator;
import com.jobstrak.drawingfun.sdk.utils.SecureLogUtils;

public class BackstageAdDetector extends Detector {
    @NonNull
    private final BackstageManager backstageManager;
    @NonNull
    private final Validator[] validators;

    public BackstageAdDetector(@NonNull Config config, @NonNull Settings settings, @NonNull BackstageManager backstageManager2) {
        super(config, settings);
        this.backstageManager = backstageManager2;
        this.validators = new Validator[]{new DuplicateServiceValidator(), new InitValidator(config, settings), new ConfigStateValidator(settings), new InitialDelayValidator(config, settings), new NetworkStateValidator(config), new BackstageAdEnableStateValidator(config), new BackstageAdUrlStateValidator(config), new BackstageAdPerDayLimitValidator(config, settings), new BackstageAdDelayStateValidator(settings)};
    }

    public void tick(Detector.Delegate delegate) {
    }

    public boolean detect(Detector.Delegate delegate) {
        for (Validator validator : this.validators) {
            if (!validator.validate(delegate.getCurrentTime())) {
                SecureLogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validator.getReason());
                return true;
            }
        }
        this.backstageManager.openUrl(UrlBuilder.fromString(getConfig().getBackstageAdUrl()).addParameter("uid", getSettings().getClientId()).toString());
        getSettings().setNextBackstageAdTime(delegate.getCurrentTime() + getConfig().getBackstageAdDelay());
        getSettings().incCurrentBackstageAdCount();
        getSettings().save();
        return true;
    }
}
