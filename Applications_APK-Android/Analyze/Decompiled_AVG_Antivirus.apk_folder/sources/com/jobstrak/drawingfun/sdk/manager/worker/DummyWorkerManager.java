package com.jobstrak.drawingfun.sdk.manager.worker;

import java.util.UUID;
import kotlin.Metadata;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001a\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\t\u001a\u0004\u0018\u00010\u0004H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u000b\u001a\u00020\bH\u0016J\b\u0010\f\u001a\u00020\bH\u0016¨\u0006\r"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/manager/worker/DummyWorkerManager;", "Lcom/jobstrak/drawingfun/sdk/manager/worker/WorkerManager;", "()V", "getActiveId", "Ljava/util/UUID;", "isPeriodic", "", "setActiveId", "", "id", "startWorker", "startWorkersIfNotRunning", "stopWorkersIfRunning", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: DummyWorkerManager.kt */
public final class DummyWorkerManager implements WorkerManager {
    @NotNull
    public UUID getActiveId(boolean isPeriodic) {
        return new UUID(0, 0);
    }

    public void setActiveId(boolean isPeriodic, @Nullable UUID id) {
    }

    public void startWorker(boolean isPeriodic) {
    }

    public void startWorkersIfNotRunning() {
    }

    public void stopWorkersIfRunning() {
    }
}
