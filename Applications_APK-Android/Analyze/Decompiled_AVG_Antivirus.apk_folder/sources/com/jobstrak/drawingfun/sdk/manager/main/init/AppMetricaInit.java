package com.jobstrak.drawingfun.sdk.manager.main.init;

import android.content.Context;
import com.jobstrak.drawingfun.sdk.BuildConfig;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.YandexMetrica;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0014R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/manager/main/init/AppMetricaInit;", "Lcom/jobstrak/drawingfun/sdk/manager/main/init/Init;", "context", "Landroid/content/Context;", "appMetricaKey", "", "(Landroid/content/Context;Ljava/lang/String;)V", "doExecute", "", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: AppMetricaInit.kt */
public final class AppMetricaInit extends Init {
    private final String appMetricaKey;
    private final Context context;

    public AppMetricaInit(@NotNull Context context2, @Nullable String appMetricaKey2) {
        Intrinsics.checkParameterIsNotNull(context2, "context");
        this.context = context2;
        this.appMetricaKey = appMetricaKey2;
    }

    /* access modifiers changed from: protected */
    public void doExecute() {
        String str = this.appMetricaKey;
        if (str != null) {
            ReporterConfig.Builder configBuilder = ReporterConfig.newConfigBuilder(str);
            Intrinsics.checkExpressionValueIsNotNull(configBuilder, "ReporterConfig.newConfigBuilder(appMetricaKey)");
            if (BuildConfig.DEBUG) {
                configBuilder.withLogs();
            }
            YandexMetrica.activateReporter(this.context, configBuilder.build());
            IReporter reporter = YandexMetrica.getReporter(this.context, this.appMetricaKey);
            IReporter $this$apply = reporter;
            $this$apply.resumeSession();
            $this$apply.sendEventsBuffer();
            Intrinsics.checkExpressionValueIsNotNull(reporter, "YandexMetrica.getReporte…ntsBuffer()\n            }");
            return;
        }
        LogUtils.debug("AppMetrica key is null", new Object[0]);
    }
}
