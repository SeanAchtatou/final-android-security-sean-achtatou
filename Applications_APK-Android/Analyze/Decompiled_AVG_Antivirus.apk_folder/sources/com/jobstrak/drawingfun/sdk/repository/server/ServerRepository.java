package com.jobstrak.drawingfun.sdk.repository.server;

import com.jobstrak.drawingfun.sdk.data.Config;

public interface ServerRepository {
    Config getConfig() throws Exception;

    String registerClient() throws Exception;
}
