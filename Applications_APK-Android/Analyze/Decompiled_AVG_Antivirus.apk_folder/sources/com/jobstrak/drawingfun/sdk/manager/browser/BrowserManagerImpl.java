package com.jobstrak.drawingfun.sdk.manager.browser;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.manager.browser.BrowserManager;
import com.jobstrak.drawingfun.sdk.manager.main.NotInitializedException;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.util.LinkedList;
import java.util.List;

public class BrowserManagerImpl implements BrowserManager, BrowserManager.Updater {
    private static final Intent BROWSER_INTENT = new Intent("android.intent.action.VIEW", Uri.parse("http://"));
    private static final String CHROME_PACKAGE_NAME = "com.android.chrome";
    @NonNull
    private final AndroidManager androidManager;
    @NonNull
    private final List<String> browsers = new LinkedList();
    private boolean browsersResolved;

    public BrowserManagerImpl(@NonNull AndroidManager androidManager2) {
        this.androidManager = androidManager2;
    }

    private void resolveBrowsers() {
        if (!this.browsersResolved) {
            try {
                this.browsers.clear();
                for (ResolveInfo info : this.androidManager.queryIntentActivities(BROWSER_INTENT)) {
                    this.browsers.add(info.activityInfo.packageName);
                }
                LogUtils.debug("Installed browsers: %s", TextUtils.join(", ", this.browsers));
                this.browsersResolved = true;
            } catch (NotInitializedException e) {
                LogUtils.error("Can't resolve browsers", e, new Object[0]);
            }
        }
    }

    @Nullable
    private ActivityInfo resolveDefaultBrowserInfo() {
        try {
            ResolveInfo resolveInfo = this.androidManager.resolveActivity(BROWSER_INTENT);
            if (resolveInfo != null) {
                if (resolveInfo.activityInfo != null) {
                    if (resolveInfo.activityInfo.exported && this.browsers.contains(resolveInfo.activityInfo.packageName)) {
                        return resolveInfo.activityInfo;
                    }
                    return null;
                }
            }
            return null;
        } catch (NotInitializedException e) {
            LogUtils.error("Can't resolve default browser", e, new Object[0]);
            return null;
        }
    }

    public void openUrlInBrowser(@NonNull String url) {
        resolveBrowsers();
        ActivityInfo defaultBrowserInfo = resolveDefaultBrowserInfo();
        if (this.browsers.contains(CHROME_PACKAGE_NAME)) {
            LogUtils.debug("Opening %s in chrome browser...", url);
            openUrlInBrowser(url, CHROME_PACKAGE_NAME);
        } else if (defaultBrowserInfo != null) {
            LogUtils.debug("Opening %s in default browser...", url);
            openUrlInBrowser(url, defaultBrowserInfo.packageName);
        } else if (!this.browsers.isEmpty()) {
            LogUtils.debug("Opening %s in the best picked browser...", url);
            openUrlInBrowser(url, this.browsers.get(0));
        } else {
            LogUtils.debug("Opening %s...", new Object[0]);
            openUrlInBrowser(url, null);
        }
    }

    public void openUrlInBrowser(@NonNull String url, @Nullable String browserPackageName) {
        try {
            LogUtils.debug("Opening %s in %s browser...", url, browserPackageName);
            this.androidManager.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)).setFlags(268435456).setPackage(browserPackageName));
        } catch (NotInitializedException e) {
            LogUtils.error("Can't open %s in browser", e, url);
        } catch (ActivityNotFoundException e2) {
            if (browserPackageName != null) {
                LogUtils.error("Can't find %s browser", e2, browserPackageName);
                openUrlInBrowser(url, null);
                return;
            }
            LogUtils.warning("There are no browsers installed", e2);
        }
    }

    public boolean isBrowser(@NonNull String packageName) {
        resolveBrowsers();
        return this.browsers.contains(packageName);
    }

    public void updateBrowsers() {
        LogUtils.debug("Updating browsers list...", new Object[0]);
        this.browsersResolved = false;
        resolveBrowsers();
    }
}
