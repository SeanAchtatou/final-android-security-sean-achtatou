package com.jobstrak.drawingfun.sdk.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Environment;
import android.webkit.WebSettings;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.UUID;

public class SystemUtils {
    public static boolean isForegroundServiceEnabled() {
        return Build.VERSION.SDK_INT == 26 && isXiaomi();
    }

    public static boolean isXiaomi() {
        return Build.BRAND.equalsIgnoreCase("xiaomi");
    }

    public static boolean isSamsung() {
        return Build.BRAND.equalsIgnoreCase("samsung");
    }

    public static boolean isNotDuplicateService() {
        return true;
    }

    public static boolean isExternalStorageWritable() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return "mounted".equals(state) || "mounted_ro".equals(state);
    }

    public static String getDeviceUuid() {
        LogUtils.debug("Obtaining device id...", new Object[0]);
        if (isExternalStorageReadable()) {
            File documentsDirectory = Environment.getExternalStoragePublicDirectory("Google");
            File deviceIdFile = new File(documentsDirectory, "google.id");
            if (!documentsDirectory.exists() && !documentsDirectory.mkdirs()) {
                LogUtils.error("Can't make directories: %s", documentsDirectory.getAbsolutePath());
            } else if (deviceIdFile.exists()) {
                try {
                    String deviceId = FileUtils.readLine(deviceIdFile);
                    if (deviceId == null) {
                        LogUtils.error("Empty device id found in the file", new Object[0]);
                    } else {
                        LogUtils.debug("Parsing device id from file (uuid = %s)...", deviceId);
                        return UUID.fromString(deviceId).toString();
                    }
                } catch (IOException e) {
                    LogUtils.error("Error occurred while reading device id from %s", e, deviceIdFile.getAbsolutePath());
                } catch (Exception e2) {
                    LogUtils.error("Error occurred while parsing device id", e2, new Object[0]);
                }
            }
            String deviceId2 = getRandomDeviceId();
            saveDeviceId(deviceIdFile, deviceId2);
            return deviceId2;
        }
        LogUtils.error("External storage is not readable", new Object[0]);
        return getRandomDeviceId();
    }

    private static String getRandomDeviceId() {
        LogUtils.debug("Generating random device id...", new Object[0]);
        return UUID.randomUUID().toString();
    }

    private static void saveDeviceId(@NonNull File deviceIdFile, @NonNull String deviceId) {
        LogUtils.debug("Saving device id...", new Object[0]);
        if (isExternalStorageWritable()) {
            try {
                FileUtils.write(deviceIdFile, deviceId);
            } catch (IOException e) {
                LogUtils.error(e);
            }
        } else {
            LogUtils.error("External storage is not writable", new Object[0]);
        }
    }

    public static String getDefaultUserAgent() {
        Constructor<WebSettings> constructor;
        if (Build.VERSION.SDK_INT >= 17) {
            return WebSettings.getDefaultUserAgent(ManagerFactory.getCryopiggyManager().optContext());
        }
        try {
            constructor = WebSettings.class.getDeclaredConstructor(Context.class, WebView.class);
            constructor.setAccessible(true);
            String userAgentString = constructor.newInstance(ManagerFactory.getCryopiggyManager().optContext(), null).getUserAgentString();
            constructor.setAccessible(false);
            return userAgentString;
        } catch (Exception e) {
            return new WebView(ManagerFactory.getCryopiggyManager().optContext()).getSettings().getUserAgentString();
        } catch (Throwable th) {
            constructor.setAccessible(false);
            throw th;
        }
    }

    public static boolean isCharging() {
        int plugged = ManagerFactory.getCryopiggyManager().optContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED")).getIntExtra("plugged", -1);
        return plugged == 1 || plugged == 2;
    }

    public static Intent getLaunchIntentForPackage(PackageManager packageManager, String packageName) {
        Intent intentToResolve = new Intent("android.intent.action.MAIN");
        intentToResolve.addCategory("android.intent.category.INFO");
        intentToResolve.setPackage(packageName);
        List<ResolveInfo> ris = packageManager.queryIntentActivities(intentToResolve, 512);
        if (ris == null || ris.size() <= 0) {
            intentToResolve.removeCategory("android.intent.category.INFO");
            intentToResolve.addCategory("android.intent.category.LAUNCHER");
            intentToResolve.setPackage(packageName);
            ris = packageManager.queryIntentActivities(intentToResolve, 512);
        }
        if (ris == null || ris.size() <= 0) {
            return null;
        }
        Intent intent = new Intent(intentToResolve);
        intent.setFlags(268435456);
        intent.setClassName(ris.get(0).activityInfo.packageName, ris.get(0).activityInfo.name);
        return intent;
    }
}
