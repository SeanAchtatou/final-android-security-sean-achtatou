package com.jobstrak.drawingfun.sdk.repository.server;

import android.content.Context;
import android.util.Base64;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.okhttp3.RequestBody;
import com.jobstrak.drawingfun.lib.okhttp3.Response;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.manager.request.exception.HttpException;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.utils.Crypter;
import com.jobstrak.drawingfun.sdk.utils.JsonUtils;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import javax.crypto.SecretKey;
import org.json.JSONObject;

public class CryopiggyServerRepository implements ServerRepository {
    @NonNull
    private final CryopiggyManager cryopiggyManager;
    @NonNull
    private final RequestManager requestManager;
    @NonNull
    private final SecretKey secretKey;
    @NonNull
    private final UrlManager urlManager;

    public CryopiggyServerRepository(@NonNull UrlManager urlManager2, @NonNull RequestManager requestManager2, @NonNull CryopiggyManager cryopiggyManager2, @NonNull SecretKey secretKey2) {
        this.urlManager = urlManager2;
        this.requestManager = requestManager2;
        this.cryopiggyManager = cryopiggyManager2;
        this.secretKey = secretKey2;
    }

    public String registerClient() throws Exception {
        String url = this.urlManager.create("/v2/client").toString();
        UrlManager.Builder<RequestBody> createBody = this.urlManager.createBody();
        String[] strArr = {UrlManager.Parameter.APP_ID, UrlManager.Parameter.DEVICE_ID, UrlManager.Parameter.SDK_VER, UrlManager.Parameter.A_VER, UrlManager.Parameter.BRAND, UrlManager.Parameter.MODEL, "light", UrlManager.Parameter.IMEI};
        return ((JSONObject) this.requestManager.sendRequest(url, JSONObject.class, "POST", createBody.addParameters(strArr).build())).getString("id");
    }

    public Config getConfig() throws Exception {
        Response response = null;
        try {
            response = (Response) this.requestManager.sendRequest(this.urlManager.create("/v2/settings").addParameters(UrlManager.Parameter.CLIENT_ID, UrlManager.Parameter.SDK_VER, UrlManager.Parameter.A_VER, UrlManager.Parameter.TS).build(), Response.class);
            String responseBody = response.body().string();
            String ivHeader = response.header("iv");
            String json = responseBody;
            if (ivHeader != null) {
                try {
                    json = new String(Crypter.decrypt(this.secretKey, Base64.decode(ivHeader, 2), Base64.decode(responseBody, 2)));
                } catch (IllegalArgumentException e) {
                    LogUtils.info("Config is received without encryption", new Object[0]);
                }
            } else {
                LogUtils.info("Config is received without iv", new Object[0]);
            }
            Config config = (Config) JsonUtils.fromJSON(json, Config.class);
            if (!(response == null || response.body() == null)) {
                response.body().close();
            }
            return config;
        } catch (Exception e2) {
            if (e2 instanceof HttpException) {
                Context context = this.cryopiggyManager.optContext();
                int httpCode = ((HttpException) e2).getHttpCode();
                if (context != null && httpCode == 403) {
                    LogUtils.error("httpCode == 403", new Object[0]);
                    this.cryopiggyManager.clear();
                    this.cryopiggyManager.init(context);
                }
            }
            throw e2;
        } catch (Throwable th) {
            if (!(response == null || response.body() == null)) {
                response.body().close();
            }
            throw th;
        }
    }
}
