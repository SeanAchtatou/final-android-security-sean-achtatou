package com.jobstrak.drawingfun.sdk.manager.backstage.js;

import android.webkit.JavascriptInterface;

public interface BackstageJsInterface {
    @JavascriptInterface
    void onError();

    @JavascriptInterface
    void onSuccess();
}
