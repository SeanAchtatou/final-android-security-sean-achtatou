package com.jobstrak.drawingfun.sdk.repository.system;

import android.app.ActivityManager;
import android.content.Context;
import androidx.annotation.NonNull;

public class PreLollipopSystemRepository implements SystemRepository {
    @NonNull
    public String getForegroundPackageName(@NonNull Context context) {
        return ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName();
    }
}
