package com.jobstrak.drawingfun.sdk.manager.request;

import com.jobstrak.drawingfun.lib.okhttp3.OkHttpClient;
import com.jobstrak.drawingfun.lib.okhttp3.Request;
import com.jobstrak.drawingfun.lib.okhttp3.RequestBody;
import com.jobstrak.drawingfun.lib.okhttp3.Response;
import com.jobstrak.drawingfun.lib.okio.Buffer;
import com.jobstrak.drawingfun.sdk.manager.request.exception.HttpException;
import com.jobstrak.drawingfun.sdk.util.UnsafeOkHttpClientProvider;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.lang.reflect.Constructor;

public class RequestManagerImpl implements RequestManager {
    private final OkHttpClient client = UnsafeOkHttpClientProvider.provide();

    public boolean sendRequest(String url) throws Exception {
        return sendRequest(url, "GET");
    }

    public boolean sendRequest(String url, String method) throws Exception {
        return sendRequest(url, method, (RequestBody) null);
    }

    public boolean sendRequest(String url, String method, RequestBody body) throws Exception {
        return ((Boolean) sendRequest(url, Boolean.class, method, body)).booleanValue();
    }

    public <T> T sendRequest(String url, Class<T> clazz) throws Exception {
        return sendRequest(url, clazz, "GET");
    }

    public <T> T sendRequest(String url, Class<T> clazz, String method) throws Exception {
        RequestBody requestBody;
        if (method.equals("GET")) {
            requestBody = null;
        } else {
            requestBody = RequestBody.create(JSON, new byte[0]);
        }
        return sendRequest(url, clazz, method, requestBody);
    }

    public <T> T sendRequest(String url, Class<T> clazz, String method, RequestBody body) throws Exception {
        LogUtils.debug("Sending %s request to %s", method, url);
        if (body != null) {
            LogUtils.debug("body = %s", requestBodyToString(body));
        }
        Response response = null;
        try {
            response = this.client.newCall(new Request.Builder().url(url).method(method, body).build()).execute();
            if (!response.isSuccessful()) {
                throw new HttpException(response.code());
            } else if (clazz.equals(Response.class)) {
                if (!(clazz.equals(Response.class) || response == null || response.body() == null)) {
                    response.body().close();
                }
                return response;
            } else if (clazz.equals(Boolean.class)) {
                T bool = new Boolean(true);
                if (!(clazz.equals(Response.class) || response == null || response.body() == null)) {
                    response.body().close();
                }
                return bool;
            } else {
                Constructor<?>[] constructors = clazz.getConstructors();
                int length = constructors.length;
                int i = 0;
                while (i < length) {
                    Constructor<?> c = constructors[i];
                    Class<?>[] parameterTypes = c.getParameterTypes();
                    if (parameterTypes.length != 1 || !String.class.isAssignableFrom(parameterTypes[0])) {
                        i++;
                    } else {
                        Object newInstance = c.newInstance(parameterTypes[0].getConstructor(String.class).newInstance(response.body().string()));
                        if (!(clazz.equals(Response.class) || response == null || response.body() == null)) {
                            response.body().close();
                        }
                        return newInstance;
                    }
                }
                throw new IllegalArgumentException(String.format("No String one-arg constructor found in %s class", clazz.getSimpleName()));
            }
        } catch (Exception e) {
            LogUtils.error("Failed to send request to %s", url);
            throw e;
        } catch (Throwable th) {
            if (!(clazz.equals(Response.class) || response == null || response.body() == null)) {
                response.body().close();
            }
            throw th;
        }
    }

    private String requestBodyToString(RequestBody body) {
        try {
            Buffer buffer = new Buffer();
            body.writeTo(buffer);
            return buffer.readUtf8();
        } catch (Exception e) {
            LogUtils.error(e);
            return null;
        }
    }
}
