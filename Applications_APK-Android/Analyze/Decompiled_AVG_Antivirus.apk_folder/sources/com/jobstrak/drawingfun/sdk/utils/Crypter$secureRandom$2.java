package com.jobstrak.drawingfun.sdk.utils;

import java.security.SecureRandom;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Ljava/security/SecureRandom;", "invoke"}, k = 3, mv = {1, 1, 15})
/* compiled from: Crypter.kt */
final class Crypter$secureRandom$2 extends Lambda implements Function0<SecureRandom> {
    public static final Crypter$secureRandom$2 INSTANCE = new Crypter$secureRandom$2();

    Crypter$secureRandom$2() {
        super(0);
    }

    @NotNull
    public final SecureRandom invoke() {
        return new SecureRandom();
    }
}
