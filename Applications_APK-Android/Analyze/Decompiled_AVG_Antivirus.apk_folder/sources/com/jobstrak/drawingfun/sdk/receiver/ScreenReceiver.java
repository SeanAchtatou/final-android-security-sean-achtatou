package com.jobstrak.drawingfun.sdk.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.PowerManager;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.data.Stats;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.WakeLockManager;
import com.jobstrak.drawingfun.sdk.manager.lockscreen.LockscreenManager;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.model.LinkAd;
import com.jobstrak.drawingfun.sdk.repository.RepositoryFactory;
import com.jobstrak.drawingfun.sdk.service.MainTimer;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.detector.LockscreenAdDetector;
import com.jobstrak.drawingfun.sdk.service.detector.UnlockAdDetector;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.ad.ShowLinkAdTask;
import com.jobstrak.drawingfun.sdk.task.ad.ShowLockscreenAdTask;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.util.concurrent.atomic.AtomicBoolean;

public class ScreenReceiver extends BroadcastReceiver {
    public static final AtomicBoolean isInteractive = new AtomicBoolean(true);
    @NonNull
    private final LockscreenAdDetector lockscreenAdDetector;
    @NonNull
    private final LockscreenManager lockscreenManager;
    @NonNull
    private final UnlockAdDetector unlockAdDetector;

    public static void init(Context context) {
        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.intent.action.SCREEN_OFF");
            filter.addAction("android.intent.action.USER_PRESENT");
            Config config = Config.getInstance();
            Settings settings = Settings.getInstance();
            context.registerReceiver(new ScreenReceiver(new UnlockAdDetector(config, settings, buildShowUnlockAdTaskFactory(config, settings)), ManagerFactory.getLockscreenManager(), new LockscreenAdDetector(config, settings, buildShowLockscreenAdTaskFactory(config, settings))), filter);
            updateScreenState(context);
        } catch (Exception e) {
            LogUtils.error(e);
        }
    }

    public ScreenReceiver(@NonNull UnlockAdDetector unlockAdDetector2, @NonNull LockscreenManager lockscreenManager2, @NonNull LockscreenAdDetector lockscreenAdDetector2) {
        this.unlockAdDetector = unlockAdDetector2;
        this.lockscreenManager = lockscreenManager2;
        this.lockscreenAdDetector = lockscreenAdDetector2;
    }

    public void onReceive(Context context, Intent intent) {
        if (ManagerFactory.getCryopiggyManager().init(context)) {
            String action = intent.getAction();
            char c = 65535;
            int hashCode = action.hashCode();
            if (hashCode != -2128145023) {
                if (hashCode == 823795052 && action.equals("android.intent.action.USER_PRESENT")) {
                    c = 1;
                }
            } else if (action.equals("android.intent.action.SCREEN_OFF")) {
                c = 0;
            }
            if (c == 0) {
                updateScreenState(context);
                this.lockscreenAdDetector.detect(new Detector.SimpleDelegate());
                if (Build.VERSION.SDK_INT < 26) {
                    WakeLockManager.INSTANCE.release();
                }
            } else if (c == 1) {
                updateScreenState(context);
                if (this.lockscreenManager.isBannerVisible()) {
                    this.lockscreenManager.hideBanner();
                } else if (this.lockscreenManager.isBannerClicked()) {
                    this.lockscreenManager.openClickedBanner();
                }
                this.unlockAdDetector.detect(new Detector.SimpleDelegate());
                if (Build.VERSION.SDK_INT < 26) {
                    WakeLockManager.INSTANCE.acquire(context);
                }
                MainTimer.getInstance().startIfNotRunning();
            }
        }
    }

    private static void updateScreenState(@NonNull Context context) {
        boolean isScreenOn = ((PowerManager) context.getSystemService("power")).isScreenOn();
        if (isInteractive.compareAndSet(!isScreenOn, isScreenOn)) {
            LogUtils.debug("App interactive state is switched to %s", Boolean.valueOf(isScreenOn));
        }
    }

    @NonNull
    private static TaskFactory<ShowLockscreenAdTask> buildShowLockscreenAdTaskFactory(@NonNull Config config, @NonNull Settings settings) {
        return new ShowLockscreenAdTask.Factory(settings, RepositoryFactory.getAdRepository(ManagerFactory.createUrlManager(config, settings), ManagerFactory.createRequestManager()), ManagerFactory.createDownloadManager(), ManagerFactory.getLockscreenManager());
    }

    @NonNull
    private static TaskFactory<ShowLinkAdTask> buildShowUnlockAdTaskFactory(@NonNull Config config, @NonNull Settings settings) {
        UrlManager urlManager = ManagerFactory.createUrlManager(config, settings);
        RequestManager requestManager = ManagerFactory.createRequestManager();
        return new ShowLinkAdTask.Factory(RepositoryFactory.getAdRepository(urlManager, requestManager), ManagerFactory.getBrowserManager(), LinkAd.Type.UNLOCK, Stats.getInstance());
    }
}
