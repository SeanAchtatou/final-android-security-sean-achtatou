package com.jobstrak.drawingfun.sdk.manager.android;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.data.meta.MetaData;
import com.jobstrak.drawingfun.sdk.manager.main.NotInitializedException;
import java.util.List;

public interface AndroidManager {
    void createOverlayView();

    @NonNull
    String getForegroundPackageName();

    @NonNull
    String getImei();

    @NonNull
    String getInstallerPackageName();

    @NonNull
    String getPackageName();

    @NonNull
    Point getScreenSize();

    boolean hasPermission(@NonNull String str);

    boolean isTablet();

    @Nullable
    List<ResolveInfo> queryIntentActivities(Intent intent) throws NotInitializedException;

    @NonNull
    String readFileFromAssets(@NonNull String str);

    @Nullable
    MetaData readMetaData();

    void removeOverlayView();

    @Nullable
    ResolveInfo resolveActivity(Intent intent) throws NotInitializedException;

    void setLauncherIconVisibility(boolean z);

    void setLauncherIconVisibility(boolean z, boolean z2);

    void startActivity(@NonNull Intent intent) throws NotInitializedException;

    void startActivity(@NonNull Class<? extends Activity> cls);

    void startActivity(@NonNull Class<? extends Activity> cls, @Nullable Bundle bundle);

    void startScreenReceiver();

    void startService();

    void stopScreenReceiver();

    void stopService();
}
