package com.jobstrak.drawingfun.sdk.utils;

import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.PropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KProperty;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\nH\u0007J\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\nH\u0007J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0015"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/utils/Crypter;", "", "()V", "secureRandom", "Ljava/security/SecureRandom;", "getSecureRandom", "()Ljava/security/SecureRandom;", "secureRandom$delegate", "Lkotlin/Lazy;", "decrypt", "", "key", "Ljava/security/Key;", "iv", "input", "encrypt", "Lcom/jobstrak/drawingfun/sdk/utils/Crypter$Result;", "generateIv", "blockSize", "", "Result", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: Crypter.kt */
public final class Crypter {
    static final /* synthetic */ KProperty[] $$delegatedProperties = {Reflection.property1(new PropertyReference1Impl(Reflection.getOrCreateKotlinClass(Crypter.class), "secureRandom", "getSecureRandom()Ljava/security/SecureRandom;"))};
    public static final Crypter INSTANCE = new Crypter();
    private static final Lazy secureRandom$delegate = LazyKt.lazy(Crypter$secureRandom$2.INSTANCE);

    private final SecureRandom getSecureRandom() {
        Lazy lazy = secureRandom$delegate;
        KProperty kProperty = $$delegatedProperties[0];
        return (SecureRandom) lazy.getValue();
    }

    private Crypter() {
    }

    private final byte[] generateIv(int blockSize) {
        byte[] iv = new byte[blockSize];
        getSecureRandom().nextBytes(iv);
        return iv;
    }

    @JvmStatic
    @NotNull
    public static final Result encrypt(@NotNull Key key, @NotNull byte[] input) throws GeneralSecurityException {
        Intrinsics.checkParameterIsNotNull(key, "key");
        Intrinsics.checkParameterIsNotNull(input, "input");
        Cipher cipher = Cipher.getInstance(key.getAlgorithm());
        Crypter crypter = INSTANCE;
        Intrinsics.checkExpressionValueIsNotNull(cipher, "cipher");
        byte[] iv = crypter.generateIv(cipher.getBlockSize());
        cipher.init(1, key, new IvParameterSpec(iv));
        byte[] doFinal = cipher.doFinal(input);
        Intrinsics.checkExpressionValueIsNotNull(doFinal, "cipher.doFinal(input)");
        return new Result(doFinal, iv);
    }

    @JvmStatic
    @NotNull
    public static final byte[] decrypt(@NotNull Key key, @NotNull byte[] iv, @NotNull byte[] input) throws GeneralSecurityException {
        Intrinsics.checkParameterIsNotNull(key, "key");
        Intrinsics.checkParameterIsNotNull(iv, "iv");
        Intrinsics.checkParameterIsNotNull(input, "input");
        Cipher cipher = Cipher.getInstance(key.getAlgorithm());
        cipher.init(2, key, new IvParameterSpec(iv));
        byte[] doFinal = cipher.doFinal(input);
        Intrinsics.checkExpressionValueIsNotNull(doFinal, "cipher.doFinal(input)");
        return doFinal;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\t"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/utils/Crypter$Result;", "", "output", "", "iv", "([B[B)V", "getIv", "()[B", "getOutput", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Crypter.kt */
    public static final class Result {
        @NotNull
        private final byte[] iv;
        @NotNull
        private final byte[] output;

        public Result(@NotNull byte[] output2, @NotNull byte[] iv2) {
            Intrinsics.checkParameterIsNotNull(output2, "output");
            Intrinsics.checkParameterIsNotNull(iv2, "iv");
            this.output = output2;
            this.iv = iv2;
        }

        @NotNull
        public final byte[] getIv() {
            return this.iv;
        }

        @NotNull
        public final byte[] getOutput() {
            return this.output;
        }
    }
}
