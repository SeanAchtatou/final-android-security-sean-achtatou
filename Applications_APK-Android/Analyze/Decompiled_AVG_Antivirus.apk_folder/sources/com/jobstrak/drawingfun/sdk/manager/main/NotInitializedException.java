package com.jobstrak.drawingfun.sdk.manager.main;

public class NotInitializedException extends Exception {
    public NotInitializedException() {
        super("SDK is not initialized");
    }
}
