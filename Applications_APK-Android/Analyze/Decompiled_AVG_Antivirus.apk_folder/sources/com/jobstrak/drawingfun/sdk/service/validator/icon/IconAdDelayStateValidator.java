package com.jobstrak.drawingfun.sdk.service.validator.icon;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class IconAdDelayStateValidator extends Validator {
    private static final int ONE_DAY = 86400000;
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;

    public IconAdDelayStateValidator(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        long currentNextIconLap = this.settings.getNextIconLap();
        if (currentNextIconLap <= 0) {
            this.settings.setNextIconLap(86400000 + currentTime);
            this.settings.save();
        } else if (currentTime > currentNextIconLap) {
            long nextIconAdTime = currentTime + this.config.getIconAdLap();
            long nextIconLap = 86400000 + nextIconAdTime;
            LogUtils.debug("Icon ad lap is passed! [nextIconAdTime = %s, nextIconLap = %s]", Long.valueOf(nextIconAdTime), Long.valueOf(nextIconLap));
            this.settings.setNextIconAdTime(nextIconAdTime);
            this.settings.setNextIconLap(nextIconLap);
            this.settings.save();
        }
        if (this.settings.getNextIconAdTime() <= currentTime) {
            return true;
        }
        return false;
    }

    public String getReason() {
        return String.format("icon ad delayed (%s left)", TimeUtils.parseTime(this.settings.getNextIconAdTime() - TimeUtils.getCurrentTime()));
    }
}
