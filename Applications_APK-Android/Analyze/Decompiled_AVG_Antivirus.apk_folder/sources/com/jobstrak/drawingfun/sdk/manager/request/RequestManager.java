package com.jobstrak.drawingfun.sdk.manager.request;

import com.jobstrak.drawingfun.lib.okhttp3.MediaType;
import com.jobstrak.drawingfun.lib.okhttp3.RequestBody;

public interface RequestManager {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    <T> T sendRequest(String str, Class<T> cls) throws Exception;

    <T> T sendRequest(String str, Class<T> cls, String str2) throws Exception;

    <T> T sendRequest(String str, Class<T> cls, String str2, RequestBody requestBody) throws Exception;

    boolean sendRequest(String str) throws Exception;

    boolean sendRequest(String str, String str2) throws Exception;

    boolean sendRequest(String str, String str2, RequestBody requestBody) throws Exception;
}
