package com.jobstrak.drawingfun.sdk.service.validator.testage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class TestageItemsStateValidator extends Validator {
    @NonNull
    private Config config;

    public TestageItemsStateValidator(@NonNull Config config2) {
        this.config = config2;
    }

    public boolean validate(long currentTime) {
        return !this.config.getTestageItems().isEmpty();
    }

    public String getReason() {
        return "no testage items";
    }
}
