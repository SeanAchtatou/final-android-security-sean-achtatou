package com.jobstrak.drawingfun.sdk.repository.stat;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.model.Stat;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.util.List;

public class CompositeStatRepository implements StatRepository {
    @NonNull
    private final Config config;
    @NonNull
    private final StatRepository repository;
    @NonNull
    private final StatRepository reservedRepository;

    public CompositeStatRepository(@NonNull Config config2, @NonNull StatRepository repository2, @NonNull StatRepository reservedRepository2) {
        this.config = config2;
        this.repository = repository2;
        this.reservedRepository = reservedRepository2;
    }

    public void addEvent(@NonNull String type) throws Exception {
        this.repository.addEvent(type);
        if (!TextUtils.isEmpty(this.config.getReservedServer())) {
            try {
                this.reservedRepository.addEvent(type);
            } catch (Exception e) {
                LogUtils.error("Unable to add event to the reserved server", e, new Object[0]);
            }
        }
    }

    public void addStatsFromArray(@NonNull List<Stat> stats) throws Exception {
        this.repository.addStatsFromArray(stats);
        if (!TextUtils.isEmpty(this.config.getReservedServer())) {
            try {
                this.reservedRepository.addStatsFromArray(stats);
            } catch (Exception e) {
                LogUtils.error("Unable to add stats to the reserved server", e, new Object[0]);
            }
        }
    }
}
