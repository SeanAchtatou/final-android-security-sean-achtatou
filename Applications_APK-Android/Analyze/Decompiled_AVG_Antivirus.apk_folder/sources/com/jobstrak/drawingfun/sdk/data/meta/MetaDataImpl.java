package com.jobstrak.drawingfun.sdk.data.meta;

import androidx.annotation.Nullable;

public class MetaDataImpl extends MetaData {
    @Nullable
    String appId;
    boolean debug;
    @Nullable
    String flurryKey;
    @Nullable
    String server;

    MetaDataImpl() {
    }

    @Nullable
    public String getAppId() {
        return this.appId;
    }

    @Nullable
    public String getServer() {
        return this.server;
    }

    @Nullable
    public String getFlurryKey() {
        return this.flurryKey;
    }

    public boolean isDebug() {
        return this.debug;
    }
}
