package com.jobstrak.drawingfun.sdk.manager.main.init;

public class InitException extends Exception {
    public InitException() {
    }

    public InitException(String detailMessage) {
        super(detailMessage);
    }

    public InitException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public InitException(Throwable throwable) {
        super(throwable);
    }
}
