package com.jobstrak.drawingfun.sdk.task.stat;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.repository.stat.StatRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class UpdatedEventSendTask extends BaseTask<Void> {
    @NonNull
    private StatRepository statRepository;

    private UpdatedEventSendTask(@NonNull StatRepository statRepository2) {
        this.statRepository = statRepository2;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground() {
        try {
            LogUtils.debug("Sending 'updated' event...", new Object[0]);
            this.statRepository.addEvent("updated");
            return null;
        } catch (Exception e) {
            LogUtils.error("Error occurred while sending 'updated' event", e, new Object[0]);
            return null;
        }
    }

    public static class Factory implements TaskFactory<UpdatedEventSendTask> {
        @NonNull
        private StatRepository statRepository;

        public Factory(@NonNull StatRepository statRepository2) {
            this.statRepository = statRepository2;
        }

        @NonNull
        public UpdatedEventSendTask create() {
            return new UpdatedEventSendTask(this.statRepository);
        }
    }
}
