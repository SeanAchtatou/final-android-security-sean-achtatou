package com.jobstrak.drawingfun.sdk.task;

import android.os.AsyncTask;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public abstract class BaseTask<T> extends AsyncTask<Void, Void, T> {
    /* access modifiers changed from: protected */
    public abstract T doInBackground();

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        if (needBeRegistered() && Settings.getInstance().getClientId() == null) {
            LogUtils.debug("Task %s not started due to lack of registration.", getClass().getSimpleName());
            cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public final T doInBackground(Void... voids) {
        LogUtils.debug("Task %s started.", getClass().getSimpleName());
        return doInBackground();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object obj) {
        super.onPostExecute(obj);
        LogUtils.debug("Task %s executed.", getClass().getSimpleName());
    }

    /* access modifiers changed from: protected */
    public boolean needBeRegistered() {
        return true;
    }
}
