package com.jobstrak.drawingfun.sdk.manager.backstage.js;

import android.webkit.JavascriptInterface;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.utils.SecureLogUtils;

public class BackstageJsInterfaceImpl implements BackstageJsInterface {
    @NonNull
    private Settings settings;

    public BackstageJsInterfaceImpl(@NonNull Settings settings2) {
        this.settings = settings2;
    }

    @JavascriptInterface
    public void onSuccess() {
        SecureLogUtils.debug();
        this.settings.incCurrentBackstageAdClicksCount();
        this.settings.save();
    }

    @JavascriptInterface
    public void onError() {
        SecureLogUtils.debug();
    }
}
