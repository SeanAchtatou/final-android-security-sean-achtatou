package com.jobstrak.drawingfun.sdk.service.validator.banner;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BannerAdFirstShowValidator extends Validator {
    @NonNull
    private Settings settings;

    public BannerAdFirstShowValidator(@NonNull Settings settings2) {
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        return this.settings.getNextBannerAdTime() > 0;
    }

    public String getReason() {
        return null;
    }
}
