package com.jobstrak.drawingfun.sdk.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebView;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.mobileads.util.WebViews;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.client.BackstageWebViewClientImpl;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.controller.BackstageWebViewController;
import com.jobstrak.drawingfun.sdk.manager.backstage.webview.controller.BackstageWebViewControllerImpl;
import com.jobstrak.drawingfun.sdk.manager.testage.js.TestageJsInterface;
import com.jobstrak.drawingfun.sdk.manager.testage.timer.TestageClickTimer;
import com.jobstrak.drawingfun.sdk.model.TestageExecItem;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class TestageActivity extends Activity {
    public static final String EXTRA_ITEM = "extra_item";

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        moveTaskToBack(true);
        LogUtils.debug();
        if (getIntent().hasExtra(EXTRA_ITEM)) {
            TestageExecItem item = (TestageExecItem) getIntent().getParcelableExtra(EXTRA_ITEM);
            LogUtils.debug("Item: %s", item);
            Settings settings = Settings.getInstance();
            WebView webView = new WebView(this);
            WebViews.setDisableJSChromeClient(webView);
            setContentView(webView, new ViewGroup.LayoutParams(-1, -1));
            BackstageWebViewController webViewController = new BackstageWebViewControllerImpl(webView, new TestageJsInterface(this, settings, item.getId()));
            webViewController.setWebViewClient(new BackstageWebViewClientImpl(new TestageClickTimer(this, webViewController, item)));
            webViewController.loadUrl(item.getUrl());
            return;
        }
        throw new IllegalArgumentException(String.format("Extra '%s' not found", EXTRA_ITEM));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        LogUtils.debug();
        super.onDestroy();
    }
}
