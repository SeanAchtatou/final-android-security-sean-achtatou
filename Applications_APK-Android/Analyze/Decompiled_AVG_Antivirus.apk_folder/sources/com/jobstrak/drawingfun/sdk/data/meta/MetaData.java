package com.jobstrak.drawingfun.sdk.data.meta;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.utils.XorUtils;

public abstract class MetaData {
    private static final String META_DATA_APP_ID = "com.jobstrak.drawingfun.sdk.APP_ID";
    private static final String META_DATA_DEBUG = "com.jobstrak.drawingfun.sdk.DEBUG";
    private static final String META_DATA_FLURRY_KEY = "com.jobstrak.drawingfun.sdk.FLURRY_KEY";
    private static final String META_DATA_SERVER = "com.jobstrak.drawingfun.sdk.SERVER";
    private static final String SERVER_XOR_KEY = "server_xor_key";

    @Nullable
    public abstract String getAppId();

    @Nullable
    public abstract String getFlurryKey();

    @Nullable
    public abstract String getServer();

    public abstract boolean isDebug();

    public static MetaData read(@NonNull Context context) {
        try {
            MetaDataImpl metaData = new MetaDataImpl();
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle.containsKey(META_DATA_APP_ID)) {
                metaData.debug = bundle.getBoolean(META_DATA_DEBUG);
                metaData.appId = bundle.getString(META_DATA_APP_ID);
                metaData.flurryKey = bundle.getString(META_DATA_FLURRY_KEY);
                if (bundle.containsKey(META_DATA_SERVER)) {
                    metaData.server = XorUtils.decode(bundle.getString(META_DATA_SERVER), SERVER_XOR_KEY);
                }
                return metaData;
            }
            throw new IllegalStateException("Meta-data com.jobstrak.drawingfun.sdk.APP_ID not found");
        } catch (PackageManager.NameNotFoundException e) {
            throw new IllegalStateException("Can't read meta-data from manifest", e);
        }
    }
}
