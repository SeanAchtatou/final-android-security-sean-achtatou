package com.jobstrak.drawingfun.sdk.utils;

import androidx.annotation.NonNull;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileUtils {
    public static byte[] readBytes(File file) {
        LogUtils.verbose("Reading bytes from %s...", file.getAbsolutePath());
        byte[] bytes = new byte[((int) file.length())];
        try {
            DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
            dis.readFully(bytes);
            dis.close();
        } catch (IOException e) {
            LogUtils.error("Can't read bytes from %s", e, file.getAbsolutePath());
        }
        return bytes;
    }

    public static String read(@NonNull String path) throws IOException {
        LogUtils.verbose("Reading from %s...", path);
        StringBuilder output = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(path));
        output.append(reader.readLine());
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            output.append(10);
            output.append(line);
        }
        reader.close();
        return output.toString();
    }

    public static String readLine(@NonNull String path) throws IOException {
        LogUtils.verbose("Reading line from %s...", path);
        BufferedReader reader = new BufferedReader(new FileReader(path));
        try {
            return reader.readLine();
        } finally {
            reader.close();
        }
    }

    public static String readLine(@NonNull File file) throws IOException {
        return readLine(file.getAbsolutePath());
    }

    public static void write(@NonNull File file, @NonNull String s) throws IOException {
        LogUtils.verbose("Writing '%s' to %s...", s, file.getAbsolutePath());
        Writer writer = new BufferedWriter(new FileWriter(file));
        writer.write(s);
        writer.flush();
    }
}
