package com.jobstrak.drawingfun.sdk.task.server;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class SendPostbackTask extends BaseTask<Void> {
    @NonNull
    private final Config config;
    @NonNull
    private final RequestManager requestManager;

    private SendPostbackTask(@NonNull Config config2, @NonNull RequestManager requestManager2) {
        this.config = config2;
        this.requestManager = requestManager2;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground() {
        String postbackUrl = this.config.getPostbackUrl();
        if (TextUtils.isEmpty(postbackUrl)) {
            return null;
        }
        try {
            LogUtils.debug("Sending postback...", new Object[0]);
            this.requestManager.sendRequest(postbackUrl);
            return null;
        } catch (Exception e) {
            LogUtils.error("Error occurred while sending postback", e, new Object[0]);
            return null;
        }
    }

    public static class Factory implements TaskFactory<SendPostbackTask> {
        @NonNull
        private final Config config;
        @NonNull
        private final RequestManager requestManager;

        public Factory(@NonNull Config config2, @NonNull RequestManager requestManager2) {
            this.config = config2;
            this.requestManager = requestManager2;
        }

        @NonNull
        public SendPostbackTask create() {
            return new SendPostbackTask(this.config, this.requestManager);
        }
    }
}
