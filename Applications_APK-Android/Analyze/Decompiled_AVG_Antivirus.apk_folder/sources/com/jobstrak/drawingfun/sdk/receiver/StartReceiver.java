package com.jobstrak.drawingfun.sdk.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class StartReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (ManagerFactory.getCryopiggyManager().init(context)) {
            LogUtils.debug("Restarting...", new Object[0]);
        }
    }
}
