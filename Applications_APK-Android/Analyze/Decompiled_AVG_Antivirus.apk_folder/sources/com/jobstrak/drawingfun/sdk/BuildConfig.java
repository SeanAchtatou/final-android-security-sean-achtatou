package com.jobstrak.drawingfun.sdk;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.jobstrak.drawingfun.sdk";
    public static final String APPMETRICA_KEY = "33d68e8b-7186-454f-8c2a-eebb526ae5e9";
    public static final String BUILD_TYPE = "debug";
    public static final boolean DEBUG = Boolean.parseBoolean("true");
    public static final String DEFAULT_HOST = "api.birbira.xyz";
    public static final String FLAVOR = "light";
    public static final boolean LIGHT = true;
    public static final String LOG_FILENAME = "ZPkFS.log";
    public static final String REFERRER_PARAMS_FILEPATH = "google/gaid.dat";
    public static final String SCHEME = "https";
    public static final String SECRET_KEY = "d5R6MmX9tUhlBfRk1T/lF0scUABiJYulYmybEVVN1yI=";
    public static final boolean STATS_DISABLED = false;
    public static final int VERSION_CODE = 82;
    public static final String VERSION_NAME = "2.9.0";
}
