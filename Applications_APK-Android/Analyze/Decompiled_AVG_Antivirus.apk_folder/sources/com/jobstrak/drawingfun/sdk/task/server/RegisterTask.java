package com.jobstrak.drawingfun.sdk.task.server;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.repository.server.ServerRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class RegisterTask extends BaseTask<Void> {
    @NonNull
    private final ServerRepository serverRepository;
    @NonNull
    private final Settings settings;

    private RegisterTask(@NonNull Settings settings2, @NonNull ServerRepository serverRepository2) {
        this.settings = settings2;
        this.serverRepository = serverRepository2;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground() {
        try {
            LogUtils.debug("Registering...", new Object[0]);
            String clientId = this.serverRepository.registerClient();
            if (!TextUtils.isEmpty(clientId)) {
                long registerTime = System.currentTimeMillis();
                LogUtils.debug("clientId == %s, registerTime = %s", clientId, Long.valueOf(registerTime));
                this.settings.setRegisterTime(registerTime);
                this.settings.setClientId(clientId);
                this.settings.save();
                return null;
            }
            LogUtils.error("clientId == null", new Object[0]);
            return null;
        } catch (Exception e) {
            LogUtils.error("Error occurred while registering", e, new Object[0]);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean needBeRegistered() {
        return false;
    }

    public static class Factory implements TaskFactory<RegisterTask> {
        @NonNull
        private final ServerRepository serverRepository;
        @NonNull
        private final Settings settings;

        public Factory(@NonNull Settings settings2, @NonNull ServerRepository serverRepository2) {
            this.settings = settings2;
            this.serverRepository = serverRepository2;
        }

        @NonNull
        public RegisterTask create() {
            return new RegisterTask(this.settings, this.serverRepository);
        }
    }
}
