package com.jobstrak.drawingfun.sdk.manager.backstage.timer;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.service.detector.BackstageAdClicksDetector;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.utils.SecureLogUtils;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class BackstageClickTimerImpl implements BackstageClickTimer {
    private static final int MAX_TIMER_DELAY = 30000;
    private static final int MIN_TIMER_DELAY = 15000;
    @NonNull
    private static final Random RANDOM = new Random();
    /* access modifiers changed from: private */
    @NonNull
    public final BackstageAdClicksDetector clicksDetector;
    /* access modifiers changed from: private */
    @NonNull
    public final Handler handler = new Handler(Looper.getMainLooper());
    private Timer timer;

    public BackstageClickTimerImpl(@NonNull BackstageAdClicksDetector clicksDetector2) {
        this.clicksDetector = clicksDetector2;
    }

    public void startTimer() {
        SecureLogUtils.debug();
        Timer timer2 = this.timer;
        if (timer2 != null) {
            timer2.cancel();
        }
        this.timer = new Timer();
        int delay = randDelay();
        this.timer.schedule(new TimerTask() {
            public void run() {
                BackstageClickTimerImpl.this.handler.post(new Runnable() {
                    public void run() {
                        BackstageClickTimerImpl.this.clicksDetector.detect(new Detector.SimpleDelegate());
                    }
                });
            }
        }, (long) delay);
        SecureLogUtils.debug("Started with delay of %dms", Integer.valueOf(delay));
    }

    public void stopTimer() {
        SecureLogUtils.debug();
        Timer timer2 = this.timer;
        if (timer2 != null) {
            timer2.cancel();
            this.timer = null;
        }
    }

    private static int randDelay() {
        return rand(MIN_TIMER_DELAY, 30000);
    }

    private static int rand(int min, int max) {
        return RANDOM.nextInt((max - min) + 1) + min;
    }
}
