package com.jobstrak.drawingfun.sdk.repository.system;

import android.content.Context;
import androidx.annotation.NonNull;

public class NougatSystemRepository implements SystemRepository {
    @NonNull
    public String getForegroundPackageName(@NonNull Context context) {
        return context.getPackageName();
    }
}
