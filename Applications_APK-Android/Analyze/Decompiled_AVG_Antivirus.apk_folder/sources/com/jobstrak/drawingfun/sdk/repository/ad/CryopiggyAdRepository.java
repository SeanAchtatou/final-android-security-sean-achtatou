package com.jobstrak.drawingfun.sdk.repository.ad;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.model.HTMLAd;
import com.jobstrak.drawingfun.sdk.model.IconAd;
import com.jobstrak.drawingfun.sdk.model.LinkAd;
import com.jobstrak.drawingfun.sdk.model.LockscreenAd;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;
import com.jobstrak.drawingfun.sdk.utils.JsonUtils;

public class CryopiggyAdRepository implements AdRepository {
    @NonNull
    private final RequestManager requestManager;
    @NonNull
    private final UrlManager urlManager;

    public CryopiggyAdRepository(@NonNull UrlManager urlManager2, @NonNull RequestManager requestManager2) {
        this.urlManager = urlManager2;
        this.requestManager = requestManager2;
    }

    @NonNull
    public LinkAd getLinkAd(LinkAd.Type type) throws Exception {
        UrlManager.Builder<String> create = this.urlManager.create("/v2/ad/link");
        String[] strArr = {UrlManager.Parameter.CLIENT_ID, UrlManager.Parameter.SDK_VER, UrlManager.Parameter.A_VER, UrlManager.Parameter.GAID, UrlManager.Parameter.IMEI, UrlManager.Parameter.IS_TABLET, UrlManager.Parameter.CONNECTION_TYPE};
        return (LinkAd) JsonUtils.fromJSON((String) this.requestManager.sendRequest(create.addParameters(strArr).addCustomParameter("type", type.toString()).build(), String.class), LinkAd.class);
    }

    @NonNull
    public HTMLAd getHTMLAd(@NonNull HTMLAd.Type type) throws Exception {
        UrlManager urlManager2 = this.urlManager;
        UrlManager.Builder<String> create = urlManager2.create("/v2/ad/" + type.toString());
        String[] strArr = {UrlManager.Parameter.CLIENT_ID, UrlManager.Parameter.SDK_VER, UrlManager.Parameter.A_VER, UrlManager.Parameter.GAID, UrlManager.Parameter.IS_WIFI, UrlManager.Parameter.WIDTH, UrlManager.Parameter.HEIGHT, UrlManager.Parameter.UA, UrlManager.Parameter.IMEI, UrlManager.Parameter.IS_TABLET, UrlManager.Parameter.CONNECTION_TYPE};
        return (HTMLAd) JsonUtils.fromJSON((String) this.requestManager.sendRequest(create.addParameters(strArr).build(), String.class), HTMLAd.class);
    }

    @NonNull
    public IconAd getIconAd() throws Exception {
        UrlManager.Builder<String> create = this.urlManager.create("/v2/ad/icon");
        String[] strArr = {UrlManager.Parameter.CLIENT_ID, UrlManager.Parameter.SDK_VER, UrlManager.Parameter.A_VER, UrlManager.Parameter.GAID, UrlManager.Parameter.UA, UrlManager.Parameter.IMEI, UrlManager.Parameter.IS_TABLET, UrlManager.Parameter.CONNECTION_TYPE};
        return (IconAd) JsonUtils.fromJSON((String) this.requestManager.sendRequest(create.addParameters(strArr).build(), String.class), IconAd.class);
    }

    @NonNull
    public NotificationAd getNotificationAd() throws Exception {
        UrlManager.Builder<String> create = this.urlManager.create("/v2/ad/notification");
        String[] strArr = {UrlManager.Parameter.CLIENT_ID, UrlManager.Parameter.SDK_VER, UrlManager.Parameter.A_VER, UrlManager.Parameter.GAID, UrlManager.Parameter.UA, UrlManager.Parameter.IMEI, UrlManager.Parameter.IS_TABLET, UrlManager.Parameter.CONNECTION_TYPE};
        return (NotificationAd) JsonUtils.fromJSON((String) this.requestManager.sendRequest(create.addParameters(strArr).build(), String.class), NotificationAd.class);
    }

    @NonNull
    public LockscreenAd getLockscreenAd() throws Exception {
        UrlManager.Builder<String> create = this.urlManager.create("/v2/ad/lock");
        String[] strArr = {UrlManager.Parameter.CLIENT_ID, UrlManager.Parameter.SDK_VER, UrlManager.Parameter.A_VER, UrlManager.Parameter.GAID, UrlManager.Parameter.UA, UrlManager.Parameter.IMEI, UrlManager.Parameter.IS_TABLET, UrlManager.Parameter.CONNECTION_TYPE};
        return (LockscreenAd) JsonUtils.fromJSON((String) this.requestManager.sendRequest(create.addParameters(strArr).build(), String.class), LockscreenAd.class);
    }
}
