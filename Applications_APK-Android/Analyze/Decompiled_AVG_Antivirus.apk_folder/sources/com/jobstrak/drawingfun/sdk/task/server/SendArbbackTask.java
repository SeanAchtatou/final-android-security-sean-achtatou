package com.jobstrak.drawingfun.sdk.task.server;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.gson.JsonParser;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.apk.ApkManager;
import com.jobstrak.drawingfun.sdk.manager.request.RequestManager;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.StringUtils;
import com.jobstrak.drawingfun.sdk.utils.XorUtils;

public class SendArbbackTask extends BaseTask<Boolean> {
    @NonNull
    private final ApkManager apkManager;
    @NonNull
    private final Config config;
    @Nullable
    private OnResultListener onResultListener;
    @NonNull
    private final RequestManager requestManager;
    @NonNull
    private final Settings settings;

    public interface OnResultListener {
        void onResult(boolean z);
    }

    private SendArbbackTask(@NonNull Config config2, @NonNull Settings settings2, @NonNull RequestManager requestManager2, @NonNull ApkManager apkManager2) {
        this.config = config2;
        this.settings = settings2;
        this.requestManager = requestManager2;
        this.apkManager = apkManager2;
    }

    public void execute(@Nullable OnResultListener onResultListener2) {
        this.onResultListener = onResultListener2;
        execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground() {
        String arbbackUrl = this.config.getArbbackUrl();
        if (!TextUtils.isEmpty(arbbackUrl)) {
            try {
                LogUtils.debug("Sending arbback...", new Object[0]);
                return Boolean.valueOf(this.requestManager.sendRequest(arbbackUrl.toLowerCase().replace("{click_id}", getClickId()).replace("{app_id}", this.settings.getAppId()).replace("{client_id}", this.settings.getClientId())));
            } catch (Exception e) {
                LogUtils.error("Error occurred while sending arbback", e, new Object[0]);
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean result) {
        super.onPostExecute((Object) result);
        OnResultListener onResultListener2 = this.onResultListener;
        if (onResultListener2 != null) {
            onResultListener2.onResult(result.booleanValue());
        }
    }

    private String getClickId() {
        String apkComment = this.apkManager.getApkFileComment();
        if (StringUtils.isEmpty(apkComment)) {
            return "null";
        }
        try {
            return new JsonParser().parse(XorUtils.decode(apkComment, "2wDubezdi0")).getAsJsonObject().get("click_id").getAsString();
        } catch (Exception e) {
            return apkComment;
        }
    }

    public static class Factory implements TaskFactory<SendArbbackTask> {
        @NonNull
        private final ApkManager apkManager;
        @NonNull
        private final Config config;
        @NonNull
        private final RequestManager requestManager;
        @NonNull
        private final Settings settings;

        public Factory(@NonNull Config config2, @NonNull Settings settings2, @NonNull RequestManager requestManager2, @NonNull ApkManager apkManager2) {
            this.config = config2;
            this.settings = settings2;
            this.requestManager = requestManager2;
            this.apkManager = apkManager2;
        }

        @NonNull
        public SendArbbackTask create() {
            return new SendArbbackTask(this.config, this.settings, this.requestManager, this.apkManager);
        }
    }
}
