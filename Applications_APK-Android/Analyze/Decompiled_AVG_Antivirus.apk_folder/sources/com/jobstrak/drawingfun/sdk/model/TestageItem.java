package com.jobstrak.drawingfun.sdk.model;

import com.google.android.gms.common.internal.ImagesContract;
import com.jobstrak.drawingfun.lib.gson.annotations.SerializedName;

public class TestageItem {
    public static final String CHECKBOX_ACTION_CHECK = "check";
    public static final String CHECKBOX_ACTION_NONE = "none";
    public static final String CHECKBOX_ACTION_UNCHECK = "uncheck";
    @SerializedName("button_type")
    private String buttonAttrName;
    @SerializedName("button_id")
    private String buttonAttrValue;
    @SerializedName("button_num")
    private int buttonNum;
    @SerializedName("checkbox_action")
    private String checkboxAction;
    @SerializedName("checkbox_type")
    private String checkboxAttrName;
    @SerializedName("checkbox_id")
    private String checkboxAttrValue;
    @SerializedName("clicks_count")
    private int clicksCount;
    @SerializedName("clicks_delay")
    private int clicksDelay;
    @SerializedName("clicks_enabled")
    private boolean clicksEnabled;
    @SerializedName("count")
    private int count;
    @SerializedName("delay")
    private int delay;
    @SerializedName("id")
    private int id;
    @SerializedName(ImagesContract.URL)
    private String url;

    public int getId() {
        return this.id;
    }

    public String getUrl() {
        return this.url;
    }

    public String getButtonAttrValue() {
        return this.buttonAttrValue;
    }

    public String getButtonAttrName() {
        return this.buttonAttrName;
    }

    public int getButtonNum() {
        return this.buttonNum;
    }

    public String getCheckboxAttrName() {
        return this.checkboxAttrName;
    }

    public String getCheckboxAttrValue() {
        return this.checkboxAttrValue;
    }

    public String getCheckboxAction() {
        return this.checkboxAction;
    }

    public int getDelay() {
        return this.delay;
    }

    public int getCount() {
        return this.count;
    }

    public boolean isClicksEnabled() {
        return this.clicksEnabled;
    }

    public int getClicksDelay() {
        return this.clicksDelay;
    }

    public int getClicksCount() {
        return this.clicksCount;
    }

    public String toString() {
        return "TestageItem{id=" + this.id + ", url='" + this.url + '\'' + ", buttonAttrName='" + this.buttonAttrName + '\'' + ", buttonAttrValue='" + this.buttonAttrValue + '\'' + ", buttonNum=" + this.buttonNum + ", checkboxAttrName='" + this.checkboxAttrName + '\'' + ", checkboxAttrValue='" + this.checkboxAttrValue + '\'' + ", checkboxAction='" + this.checkboxAction + '\'' + ", delay=" + this.delay + ", count=" + this.count + ", clicksEnabled=" + this.clicksEnabled + ", clicksDelay=" + this.clicksDelay + ", clicksCount=" + this.clicksCount + '}';
    }
}
