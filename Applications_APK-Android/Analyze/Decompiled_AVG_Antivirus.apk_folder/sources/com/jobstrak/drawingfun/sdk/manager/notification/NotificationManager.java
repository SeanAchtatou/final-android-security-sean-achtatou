package com.jobstrak.drawingfun.sdk.manager.notification;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;

public interface NotificationManager {
    void showNotification(@NonNull NotificationAd notificationAd);
}
