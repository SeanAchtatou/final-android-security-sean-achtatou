package com.jobstrak.drawingfun.sdk.service.validator.banner;

import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BannerAdPerDayLimitValidator extends Validator {
    private int bannerAdCount;
    private int currentBannerAdCount;

    public boolean validate(long currentTime) {
        this.bannerAdCount = Config.getInstance().getAdCount();
        this.currentBannerAdCount = Settings.getInstance().getCurrentBannerAdCount();
        int i = this.bannerAdCount;
        return i == 0 || this.currentBannerAdCount < i;
    }

    public String getReason() {
        return String.format("banner ad count per day reached (%s/%s)", Integer.valueOf(this.currentBannerAdCount), Integer.valueOf(this.bannerAdCount));
    }
}
