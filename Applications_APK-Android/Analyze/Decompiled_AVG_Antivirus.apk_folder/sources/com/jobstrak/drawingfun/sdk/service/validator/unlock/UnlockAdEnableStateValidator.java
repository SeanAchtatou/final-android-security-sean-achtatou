package com.jobstrak.drawingfun.sdk.service.validator.unlock;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class UnlockAdEnableStateValidator extends Validator {
    @NonNull
    private final Config config;

    public UnlockAdEnableStateValidator(@NonNull Config config2) {
        this.config = config2;
    }

    public boolean validate(long currentTime) {
        return this.config.isUnlockAdEnabled();
    }

    public String getReason() {
        return "unlock ad is disabled";
    }
}
