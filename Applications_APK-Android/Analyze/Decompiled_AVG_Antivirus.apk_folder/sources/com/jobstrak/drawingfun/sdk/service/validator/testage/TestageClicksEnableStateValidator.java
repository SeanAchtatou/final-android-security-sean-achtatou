package com.jobstrak.drawingfun.sdk.service.validator.testage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.TestageItem;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageValidator;

public class TestageClicksEnableStateValidator implements TestageValidator {
    public TestageValidator.Result validate(long currentTime, @NonNull TestageItem item) {
        return new TestageValidator.Result(item.isClicksEnabled()) {
            public String reason() {
                return "clicks are disabled";
            }
        };
    }
}
