package com.jobstrak.drawingfun.sdk.model;

import com.google.android.gms.common.internal.ImagesContract;
import com.jobstrak.drawingfun.lib.gson.annotations.SerializedName;

public class LinkAd {
    @SerializedName(ImagesContract.URL)
    private String url;

    public enum Type {
        BROWSER(HTMLAd.TYPE_BROWSER),
        UNLOCK("unlock");
        
        private String s;

        private Type(String s2) {
            this.s = s2;
        }

        public String toString() {
            return this.s;
        }
    }

    public LinkAd() {
    }

    public LinkAd(String url2) {
        this.url = url2;
    }

    public String getUrl() {
        return this.url;
    }
}
