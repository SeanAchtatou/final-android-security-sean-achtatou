package com.jobstrak.drawingfun.sdk.manager.google;

import androidx.annotation.NonNull;

public interface GoogleManager {
    @NonNull
    String getAdvId();
}
