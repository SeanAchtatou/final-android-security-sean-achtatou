package com.jobstrak.drawingfun.sdk.manager.backstage;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.activity.BackstageActivity2;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.utils.SecureLogUtils;

public class BackstageManagerImpl implements BackstageManager {
    @NonNull
    private final CryopiggyManager cryopiggyManager;

    public BackstageManagerImpl(@NonNull CryopiggyManager cryopiggyManager2) {
        this.cryopiggyManager = cryopiggyManager2;
    }

    public void openUrl(@NonNull String url) {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            SecureLogUtils.debug("Opening url in backstage: %s", url);
            context.startActivity(new Intent(context, BackstageActivity2.class).putExtra(BackstageActivity2.EXTRA_URL, url).addFlags(268435456));
            return;
        }
        SecureLogUtils.error("context == null", new Object[0]);
    }
}
