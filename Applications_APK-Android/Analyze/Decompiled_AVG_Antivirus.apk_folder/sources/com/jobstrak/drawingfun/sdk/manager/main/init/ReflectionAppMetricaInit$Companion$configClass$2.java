package com.jobstrak.drawingfun.sdk.manager.main.init;

import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u0012\u0012\u0002\b\u0003 \u0002*\b\u0012\u0002\b\u0003\u0018\u00010\u00010\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Ljava/lang/Class;", "kotlin.jvm.PlatformType", "invoke"}, k = 3, mv = {1, 1, 15})
/* compiled from: ReflectionAppMetricaInit.kt */
final class ReflectionAppMetricaInit$Companion$configClass$2 extends Lambda implements Function0<Class<?>> {
    public static final ReflectionAppMetricaInit$Companion$configClass$2 INSTANCE = new ReflectionAppMetricaInit$Companion$configClass$2();

    ReflectionAppMetricaInit$Companion$configClass$2() {
        super(0);
    }

    public final Class<?> invoke() {
        return Class.forName("com.yandex.metrica.YandexMetricaConfig");
    }
}
