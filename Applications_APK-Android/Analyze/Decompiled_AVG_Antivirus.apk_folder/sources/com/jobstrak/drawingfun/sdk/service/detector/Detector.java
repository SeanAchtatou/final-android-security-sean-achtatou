package com.jobstrak.drawingfun.sdk.service.detector;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public abstract class Detector {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;

    public interface Delegate {
        long getCurrentTime();

        String getForegroundPackage();

        boolean isBannersPrePaused();

        void prePause();

        void unPrePause();
    }

    public abstract boolean detect(@Nullable Delegate delegate);

    public abstract void tick(@Nullable Delegate delegate);

    public Detector(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Config getConfig() {
        return this.config;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Settings getSettings() {
        return this.settings;
    }

    public static class SimpleDelegate implements Delegate {
        private long currentTime = TimeUtils.getCurrentTime();

        public void prePause() {
        }

        public void unPrePause() {
        }

        public long getCurrentTime() {
            return this.currentTime;
        }

        public String getForegroundPackage() {
            return null;
        }

        public boolean isBannersPrePaused() {
            return false;
        }
    }
}
