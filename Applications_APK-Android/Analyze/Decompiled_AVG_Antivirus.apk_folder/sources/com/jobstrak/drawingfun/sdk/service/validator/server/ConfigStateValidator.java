package com.jobstrak.drawingfun.sdk.service.validator.server;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class ConfigStateValidator extends Validator {
    @NonNull
    private Settings settings;

    public ConfigStateValidator(@NonNull Settings settings2) {
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        return this.settings.getConfigTimestamp() > 0;
    }

    public String getReason() {
        return "config is not retrieved now";
    }
}
