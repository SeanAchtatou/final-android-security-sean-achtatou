package com.jobstrak.drawingfun.sdk.manager.google;

import android.content.Context;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class GoogleManagerImpl implements GoogleManager {
    @NonNull
    private final CryopiggyManager cryopiggyManager;

    public GoogleManagerImpl(@NonNull CryopiggyManager cryopiggyManager2) {
        this.cryopiggyManager = cryopiggyManager2;
    }

    @NonNull
    public String getAdvId() {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            LogUtils.debug("Obtaining gaid...", new Object[0]);
            try {
                Class<?> adInfoClass = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
                return adInfoClass.getMethod("getId", new Class[0]).invoke(Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", Context.class).invoke(null, context), new Object[0]).toString();
            } catch (Exception e) {
                LogUtils.error(e);
                LogUtils.warning("gaid == null", new Object[0]);
                return "";
            }
        } else {
            LogUtils.error("context == null", new Object[0]);
            return "";
        }
    }
}
