package com.jobstrak.drawingfun.sdk.service.validator.device;

import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.SystemUtils;

public class DuplicateServiceValidator extends Validator {
    public boolean validate(long currentTime) {
        return SystemUtils.isNotDuplicateService();
    }

    public String getReason() {
        return "duplicate service";
    }
}
