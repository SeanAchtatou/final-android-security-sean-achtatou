package com.jobstrak.drawingfun.sdk.service.validator.device;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.SystemUtils;

public class ChargingStateValidator extends Validator {
    @NonNull
    private final Config config;

    public ChargingStateValidator(@NonNull Config config2) {
        this.config = config2;
    }

    public boolean validate(long currentTime) {
        return this.config.isEnableOnCharging() || !SystemUtils.isCharging();
    }

    public String getReason() {
        return "disabled on charging";
    }
}
