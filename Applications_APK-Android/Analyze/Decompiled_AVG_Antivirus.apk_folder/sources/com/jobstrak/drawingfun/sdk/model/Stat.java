package com.jobstrak.drawingfun.sdk.model;

import com.jobstrak.drawingfun.lib.gson.annotations.SerializedName;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.utils.JsonUtils;
import com.jobstrak.drawingfun.sdk.utils.XorUtils;

public class Stat {
    public static final transient String TYPE_CLICK = "click";
    public static final transient String TYPE_IMPRESSION = "impression";
    public static final transient String TYPE_REQUEST = "request";
    @SerializedName("banner_id")
    private String bannerId;
    @SerializedName(UrlManager.Parameter.CLIENT_ID)
    private String clientId;
    @SerializedName(UrlManager.Parameter.EXTRA)
    private String extra;
    @SerializedName(UrlManager.Parameter.SDK_VER)
    private int sdkVer;
    @SerializedName("timestamp")
    private long timestamp;
    @SerializedName("type")
    private String type;

    public Stat(String bannerId2, String type2) {
        this(Settings.getInstance().getClientId(), bannerId2, type2, System.currentTimeMillis(), 82);
    }

    public Stat(String clientId2, String bannerId2, String type2, long timestamp2, int sdkVer2) {
        this.clientId = clientId2;
        this.bannerId = bannerId2;
        this.type = type2;
        this.timestamp = timestamp2;
        this.sdkVer = sdkVer2;
        this.extra = XorUtils.decode(ManagerFactory.getApkManager().getApkFileComment(), "2wDubezdi0");
        if (this.extra == null) {
            this.extra = "";
        }
    }

    public String getType() {
        return this.type;
    }

    public String getClientId() {
        return this.clientId;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public String getBannerId() {
        return this.bannerId;
    }

    public int getSdkVer() {
        return this.sdkVer;
    }

    public String toString() {
        return JsonUtils.toJSON(this);
    }
}
