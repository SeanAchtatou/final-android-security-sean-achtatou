package com.jobstrak.drawingfun.sdk.service.validator.backstage.clicks;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BackstageAdClicksPerDayLimitValidator extends Validator {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;

    public BackstageAdClicksPerDayLimitValidator(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        double d;
        double clicksCount = (double) this.config.getBackstageAdClicksCount();
        double clicksDelay = (double) this.config.getBackstageAdClicksDelay();
        double currentClicksCount = (double) this.settings.getCurrentBackstageAdClicksCount();
        if (clicksCount > 0.0d) {
            if (clicksDelay > 0.0d) {
                Double.isNaN(currentClicksCount);
                Double.isNaN(clicksDelay);
                d = currentClicksCount / clicksDelay;
            } else {
                d = currentClicksCount;
            }
            return d < clicksCount;
        }
    }

    public String getReason() {
        return String.format("daily backstage ad clicks limit exceeded (%d)", Integer.valueOf(this.config.getBackstageAdClicksCount()));
    }
}
