package com.jobstrak.drawingfun.sdk.task.stat;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.repository.stat.StatRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class WorkingEventSendTask extends BaseTask<Void> {
    @NonNull
    private final Settings settings;
    @NonNull
    private final StatRepository statRepository;

    private WorkingEventSendTask(@NonNull Settings settings2, @NonNull StatRepository statRepository2) {
        this.settings = settings2;
        this.statRepository = statRepository2;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground() {
        try {
            if (this.settings.isStartedEventSent()) {
                sendEvent("working");
            } else {
                sendEvent("started");
                this.settings.setStartedEventSent(true);
            }
            this.settings.setNextWorkingEventSendTime(TimeUtils.getCurrentTime() + 86400000);
            this.settings.save();
            return null;
        } catch (Exception e) {
            LogUtils.error("Error occurred while sending working event", e, new Object[0]);
            return null;
        }
    }

    private void sendEvent(String type) throws Exception {
        LogUtils.debug("Sending %s event...", type);
        this.statRepository.addEvent(type);
    }

    public static class Factory implements TaskFactory<WorkingEventSendTask> {
        @NonNull
        private final Settings settings;
        @NonNull
        private final StatRepository statRepository;

        public Factory(@NonNull Settings settings2, @NonNull StatRepository statRepository2) {
            this.settings = settings2;
            this.statRepository = statRepository2;
        }

        @NonNull
        public WorkingEventSendTask create() {
            return new WorkingEventSendTask(this.settings, this.statRepository);
        }
    }
}
