package com.jobstrak.drawingfun.sdk.model;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

public class TestageExecItem implements Parcelable {
    public static final Parcelable.Creator<TestageExecItem> CREATOR = new Parcelable.Creator<TestageExecItem>() {
        public TestageExecItem createFromParcel(Parcel source) {
            return new TestageExecItem(source);
        }

        public TestageExecItem[] newArray(int size) {
            return new TestageExecItem[size];
        }
    };
    private String attrName;
    private String attrValue;
    private String checkboxAction;
    private String checkboxAttrName;
    private String checkboxAttrValue;
    private int id;
    private int num;
    private String url;

    public TestageExecItem(int id2, String url2) {
        this.id = id2;
        this.url = url2;
    }

    public TestageExecItem(@NonNull TestageItem testageItem) {
        this.id = testageItem.getId();
        this.url = testageItem.getUrl();
        this.attrName = testageItem.getButtonAttrName();
        this.attrValue = testageItem.getButtonAttrValue();
        this.num = testageItem.getButtonNum();
        this.checkboxAction = testageItem.getCheckboxAction();
        this.checkboxAttrName = testageItem.getCheckboxAttrName();
        this.checkboxAttrValue = testageItem.getCheckboxAttrValue();
    }

    public int getId() {
        return this.id;
    }

    public String getUrl() {
        return this.url;
    }

    public String getAttrName() {
        return this.attrName;
    }

    public String getAttrValue() {
        return this.attrValue;
    }

    public int getNum() {
        return this.num;
    }

    public String getCheckboxAttrName() {
        return this.checkboxAttrName;
    }

    public String getCheckboxAttrValue() {
        return this.checkboxAttrValue;
    }

    public String getCheckboxAction() {
        return this.checkboxAction;
    }

    public String toString() {
        return "TestageExecItem{id=" + this.id + ", url='" + this.url + '\'' + ", attrName='" + this.attrName + '\'' + ", attrValue='" + this.attrValue + '\'' + ", num=" + this.num + ", checkboxAction='" + this.checkboxAction + '\'' + ", checkboxAttrName='" + this.checkboxAttrName + '\'' + ", checkboxAttrValue='" + this.checkboxAttrValue + '\'' + '}';
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.url);
        dest.writeString(this.attrName);
        dest.writeString(this.attrValue);
        dest.writeInt(this.num);
        dest.writeString(this.checkboxAction);
        dest.writeString(this.checkboxAttrName);
        dest.writeString(this.checkboxAttrValue);
    }

    protected TestageExecItem(Parcel in) {
        this.id = in.readInt();
        this.url = in.readString();
        this.attrName = in.readString();
        this.attrValue = in.readString();
        this.num = in.readInt();
        this.checkboxAction = in.readString();
        this.checkboxAttrName = in.readString();
        this.checkboxAttrValue = in.readString();
    }
}
