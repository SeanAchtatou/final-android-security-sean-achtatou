package com.jobstrak.drawingfun.sdk.manager.testage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.TestageExecItem;

public interface TestageManager {
    void execute(@NonNull TestageExecItem testageExecItem);
}
