package com.jobstrak.drawingfun.sdk.service.validator.backstage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.StringUtils;

public class BackstageAdUrlStateValidator extends Validator {
    @NonNull
    private final Config config;

    public BackstageAdUrlStateValidator(@NonNull Config config2) {
        this.config = config2;
    }

    public boolean validate(long currentTime) {
        return !StringUtils.isEmpty(this.config.getBackstageAdUrl());
    }

    public String getReason() {
        return "backstage ad url is empty";
    }
}
