package com.jobstrak.drawingfun.sdk.service.detector;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.service.validator.device.DuplicateServiceValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.NetworkStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.lockscreen.LockscreenAdDelayStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.lockscreen.LockscreenAdEnableStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.lockscreen.LockscreenAdPerDayLimitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.lockscreen.LockscreenAdShowingStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.ConfigStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitialDelayValidator;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.ad.ShowLockscreenAdTask;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class LockscreenAdDetector extends Detector {
    @NonNull
    private TaskFactory<ShowLockscreenAdTask> showLockscreenAdTaskFactory;
    @NonNull
    private final Validator[] validators;

    public LockscreenAdDetector(@NonNull Config config, @NonNull Settings settings, @NonNull TaskFactory<ShowLockscreenAdTask> showLockscreenAdTaskFactory2) {
        super(config, settings);
        this.validators = new Validator[]{new DuplicateServiceValidator(), new InitValidator(config, settings), new ConfigStateValidator(settings), new InitialDelayValidator(config, settings), new NetworkStateValidator(config), new LockscreenAdEnableStateValidator(config), new LockscreenAdPerDayLimitValidator(config, settings), new LockscreenAdDelayStateValidator(config, settings), new LockscreenAdShowingStateValidator()};
        this.showLockscreenAdTaskFactory = showLockscreenAdTaskFactory2;
    }

    public void tick(Detector.Delegate delegate) {
    }

    public boolean detect(Detector.Delegate delegate) {
        LogUtils.debug("Lock detected", new Object[0]);
        getSettings().incCurrentLockscreenLockCount();
        getSettings().save();
        for (Validator validator : this.validators) {
            if (!validator.validate(delegate.getCurrentTime())) {
                LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validator.getReason());
                return true;
            }
        }
        this.showLockscreenAdTaskFactory.create().execute(new Void[0]);
        return true;
    }
}
