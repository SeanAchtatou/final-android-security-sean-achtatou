package com.jobstrak.drawingfun.sdk.task.server;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.repository.server.ServerRepository;
import com.jobstrak.drawingfun.sdk.task.BaseTask;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class UpdateConfigTask extends BaseTask<Void> {
    @NonNull
    private final Config config;
    @NonNull
    private final TaskFactory<SendPostbackTask> sendPostbackTask;
    @NonNull
    private final ServerRepository serverRepository;
    @NonNull
    private final Settings settings;

    public UpdateConfigTask(@NonNull Config config2, @NonNull Settings settings2, @NonNull ServerRepository serverRepository2, @NonNull TaskFactory<SendPostbackTask> sendPostbackTask2) {
        this.config = config2;
        this.settings = settings2;
        this.serverRepository = serverRepository2;
        this.sendPostbackTask = sendPostbackTask2;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground() {
        try {
            Config config2 = retrieveConfig();
            if (config2 != null) {
                updateLocalConfig(config2);
                sendPostbackIfNeeded();
                updateSettings(config2);
                return null;
            }
            LogUtils.error("config == null", new Object[0]);
            return null;
        } catch (Exception e) {
            LogUtils.error("Error occurred while retrieving config", e, new Object[0]);
            return null;
        }
    }

    private void updateSettings(@NonNull Config config2) {
        this.settings.setNextConfigUpdateTime(TimeUtils.getCurrentTime() + config2.getSettingsDelay());
        this.settings.setConfigTimestamp(System.currentTimeMillis() / 1000);
        this.settings.save();
    }

    private void sendPostbackIfNeeded() {
        if (!this.settings.getPostbackSent()) {
            this.sendPostbackTask.create().execute(new Void[0]);
            this.settings.setPostbackSent(true);
            this.settings.save();
        }
    }

    private Config retrieveConfig() throws Exception {
        LogUtils.debug("Retrieving config...", new Object[0]);
        return this.serverRepository.getConfig();
    }

    private void updateLocalConfig(@NonNull Config config2) {
        try {
            LogUtils.debug("Updating local config...", new Object[0]);
            this.config.update(config2);
        } catch (Exception e) {
            LogUtils.error("Error occurred while updating local config", e, new Object[0]);
        }
    }

    public static class Factory implements TaskFactory<UpdateConfigTask> {
        @NonNull
        private final Config config;
        @NonNull
        private final TaskFactory<SendPostbackTask> sendPostbackTask;
        @NonNull
        private final ServerRepository serverRepository;
        @NonNull
        private final Settings settings;

        public Factory(@NonNull Config config2, @NonNull Settings settings2, @NonNull ServerRepository serverRepository2, @NonNull TaskFactory<SendPostbackTask> sendPostbackTask2) {
            this.config = config2;
            this.settings = settings2;
            this.serverRepository = serverRepository2;
            this.sendPostbackTask = sendPostbackTask2;
        }

        @NonNull
        public UpdateConfigTask create() {
            return new UpdateConfigTask(this.config, this.settings, this.serverRepository, this.sendPostbackTask);
        }
    }
}
