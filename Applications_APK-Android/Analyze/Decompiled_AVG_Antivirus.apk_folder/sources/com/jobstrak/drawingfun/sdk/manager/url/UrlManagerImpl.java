package com.jobstrak.drawingfun.sdk.manager.url;

import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.okhttp3.FormBody;
import com.jobstrak.drawingfun.lib.okhttp3.RequestBody;
import com.jobstrak.drawingfun.lib.urlbuilder.UrlBuilder;
import com.jobstrak.drawingfun.sdk.BuildConfig;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.manager.android.AndroidManager;
import com.jobstrak.drawingfun.sdk.manager.apk.ApkManager;
import com.jobstrak.drawingfun.sdk.manager.google.GoogleManager;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.util.Supplier;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.NetworkUtils;
import com.jobstrak.drawingfun.sdk.utils.SystemUtils;
import com.jobstrak.drawingfun.sdk.utils.XorUtils;
import java.util.HashMap;
import java.util.Map;

public class UrlManagerImpl implements UrlManager {
    @NonNull
    private final AndroidManager androidManager;
    @NonNull
    private final ApkManager apkManager;
    @NonNull
    private final GoogleManager googleManager;
    @NonNull
    private final Settings settings;
    @NonNull
    private final StoreFactory storeFactory;

    public interface StoreFactory {
        UrlManager.Store create(@NonNull String str);
    }

    public UrlManagerImpl(@NonNull Settings settings2, @NonNull StoreFactory storeFactory2, @NonNull GoogleManager googleManager2, @NonNull AndroidManager androidManager2, @NonNull ApkManager apkManager2) {
        this.settings = settings2;
        this.storeFactory = storeFactory2;
        this.googleManager = googleManager2;
        this.androidManager = androidManager2;
        this.apkManager = apkManager2;
    }

    @NonNull
    public UrlManager.Builder create(@NonNull String path) {
        return new LinkBuilderImpl(this.androidManager, this.storeFactory.create(path), new ParameterStoreImpl(this.settings, this.googleManager, this.androidManager, this.apkManager));
    }

    @NonNull
    public UrlManager.Builder<RequestBody> createBody() {
        AndroidManager androidManager2 = this.androidManager;
        return new BodyBuilderImpl(androidManager2, new ParameterStoreImpl(this.settings, this.googleManager, androidManager2, this.apkManager));
    }

    public static class ParameterStoreImpl implements UrlManager.Store {
        @NonNull
        private final AndroidManager androidManager;
        @NonNull
        private final ApkManager apkManager;
        @NonNull
        private final GoogleManager googleManager;
        @NonNull
        private final Settings settings;

        public ParameterStoreImpl(@NonNull Settings settings2, @NonNull GoogleManager googleManager2, @NonNull AndroidManager androidManager2, @NonNull ApkManager apkManager2) {
            this.settings = settings2;
            this.googleManager = googleManager2;
            this.androidManager = androidManager2;
            this.apkManager = apkManager2;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        @Nullable
        public String getValue(@NonNull String key) {
            char c;
            switch (key.hashCode()) {
                case -1904089585:
                    if (key.equals(UrlManager.Parameter.CLIENT_ID)) {
                        c = 0;
                        break;
                    }
                    c = 65535;
                    break;
                case -1411074055:
                    if (key.equals(UrlManager.Parameter.APP_ID)) {
                        c = 8;
                        break;
                    }
                    c = 65535;
                    break;
                case -1221029593:
                    if (key.equals(UrlManager.Parameter.HEIGHT)) {
                        c = 6;
                        break;
                    }
                    c = 65535;
                    break;
                case -417338597:
                    if (key.equals(UrlManager.Parameter.IS_TABLET)) {
                        c = 15;
                        break;
                    }
                    c = 65535;
                    break;
                case 3711:
                    if (key.equals(UrlManager.Parameter.TS)) {
                        c = 14;
                        break;
                    }
                    c = 65535;
                    break;
                case 3724:
                    if (key.equals(UrlManager.Parameter.UA)) {
                        c = 7;
                        break;
                    }
                    c = 65535;
                    break;
                case 3165045:
                    if (key.equals(UrlManager.Parameter.GAID)) {
                        c = 3;
                        break;
                    }
                    c = 65535;
                    break;
                case 3236040:
                    if (key.equals(UrlManager.Parameter.IMEI)) {
                        c = 10;
                        break;
                    }
                    c = 65535;
                    break;
                case 25209764:
                    if (key.equals(UrlManager.Parameter.DEVICE_ID)) {
                        c = 9;
                        break;
                    }
                    c = 65535;
                    break;
                case 92528325:
                    if (key.equals(UrlManager.Parameter.A_VER)) {
                        c = 2;
                        break;
                    }
                    c = 65535;
                    break;
                case 93997959:
                    if (key.equals(UrlManager.Parameter.BRAND)) {
                        c = 11;
                        break;
                    }
                    c = 65535;
                    break;
                case 96965648:
                    if (key.equals(UrlManager.Parameter.EXTRA)) {
                        c = 17;
                        break;
                    }
                    c = 65535;
                    break;
                case 102970646:
                    if (key.equals("light")) {
                        c = 13;
                        break;
                    }
                    c = 65535;
                    break;
                case 104069929:
                    if (key.equals(UrlManager.Parameter.MODEL)) {
                        c = 12;
                        break;
                    }
                    c = 65535;
                    break;
                case 113126854:
                    if (key.equals(UrlManager.Parameter.WIDTH)) {
                        c = 5;
                        break;
                    }
                    c = 65535;
                    break;
                case 731866107:
                    if (key.equals(UrlManager.Parameter.CONNECTION_TYPE)) {
                        c = 16;
                        break;
                    }
                    c = 65535;
                    break;
                case 1948386846:
                    if (key.equals(UrlManager.Parameter.SDK_VER)) {
                        c = 1;
                        break;
                    }
                    c = 65535;
                    break;
                case 2082342154:
                    if (key.equals(UrlManager.Parameter.IS_WIFI)) {
                        c = 4;
                        break;
                    }
                    c = 65535;
                    break;
                default:
                    c = 65535;
                    break;
            }
            switch (c) {
                case 0:
                    return this.settings.getClientId();
                case 1:
                    return String.valueOf(82);
                case 2:
                    return String.valueOf(Build.VERSION.SDK_INT);
                case 3:
                    return this.googleManager.getAdvId();
                case 4:
                    return String.valueOf(NetworkUtils.isConnectedWifi());
                case 5:
                    return String.valueOf(this.androidManager.getScreenSize().x);
                case 6:
                    return String.valueOf(this.androidManager.getScreenSize().y);
                case 7:
                    return SystemUtils.getDefaultUserAgent();
                case 8:
                    return this.settings.getAppId();
                case 9:
                    return SystemUtils.getDeviceUuid();
                case 10:
                    return this.androidManager.getImei();
                case 11:
                    return Build.BRAND;
                case 12:
                    return Build.MODEL;
                case 13:
                    return String.valueOf(this.settings.isLightMode());
                case 14:
                    return String.valueOf(this.settings.getConfigTimestamp());
                case 15:
                    return String.valueOf(this.androidManager.isTablet());
                case 16:
                    return String.valueOf(NetworkUtils.getConnectionType());
                case 17:
                    return XorUtils.decode(this.apkManager.getApkFileComment(), "2wDubezdi0");
                default:
                    throw new IllegalArgumentException("Unknown key: " + key);
            }
        }
    }

    public static class StoreImpl implements UrlManager.Store {
        @NonNull
        private final String host;
        @NonNull
        private final String path;

        private StoreImpl(@NonNull String host2, @NonNull String path2) {
            this.host = host2;
            this.path = path2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0058 A[RETURN] */
        @androidx.annotation.Nullable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String getValue(@androidx.annotation.NonNull java.lang.String r5) {
            /*
                r4 = this;
                int r0 = r5.hashCode()
                r1 = -907987547(0xffffffffc9e135a5, float:-1844916.6)
                r2 = 2
                r3 = 1
                if (r0 == r1) goto L_0x002a
                r1 = 3208616(0x30f5a8, float:4.496229E-39)
                if (r0 == r1) goto L_0x0020
                r1 = 3433509(0x346425, float:4.811371E-39)
                if (r0 == r1) goto L_0x0016
            L_0x0015:
                goto L_0x0034
            L_0x0016:
                java.lang.String r0 = "path"
                boolean r0 = r5.equals(r0)
                if (r0 == 0) goto L_0x0015
                r0 = 2
                goto L_0x0035
            L_0x0020:
                java.lang.String r0 = "host"
                boolean r0 = r5.equals(r0)
                if (r0 == 0) goto L_0x0015
                r0 = 1
                goto L_0x0035
            L_0x002a:
                java.lang.String r0 = "scheme"
                boolean r0 = r5.equals(r0)
                if (r0 == 0) goto L_0x0015
                r0 = 0
                goto L_0x0035
            L_0x0034:
                r0 = -1
            L_0x0035:
                if (r0 == 0) goto L_0x0058
                if (r0 == r3) goto L_0x0055
                if (r0 != r2) goto L_0x003e
                java.lang.String r0 = r4.path
                return r0
            L_0x003e:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Unknown key: "
                r1.append(r2)
                r1.append(r5)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x0055:
                java.lang.String r0 = r4.host
                return r0
            L_0x0058:
                java.lang.String r0 = "https"
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.sdk.manager.url.UrlManagerImpl.StoreImpl.getValue(java.lang.String):java.lang.String");
        }
    }

    public static class StoreFactoryImpl implements StoreFactory {
        @NonNull
        private final Supplier<String> host;

        public StoreFactoryImpl(@NonNull Supplier<String> host2) {
            this.host = host2;
        }

        public UrlManager.Store create(@NonNull String path) {
            return new StoreImpl(this.host.get(), path);
        }
    }

    private static abstract class BaseBuilderImpl<T> implements UrlManager.Builder<T> {
        private final Map<String, String> referrerParams = new HashMap();

        public abstract T performBuild();

        public BaseBuilderImpl(@NonNull AndroidManager androidManager) {
            try {
                for (String param : androidManager.readFileFromAssets(BuildConfig.REFERRER_PARAMS_FILEPATH).split("&")) {
                    String[] keyValue = param.split("=");
                    this.referrerParams.put(keyValue[0], keyValue[1]);
                }
            } catch (Exception e) {
            }
        }

        @NonNull
        public T build() {
            for (Map.Entry<String, String> param : this.referrerParams.entrySet()) {
                addCustomParameter((String) param.getKey(), (String) param.getValue());
            }
            return performBuild();
        }
    }

    public static class LinkBuilderImpl extends BaseBuilderImpl<String> {
        @NonNull
        private final UrlManager.Store parameterStore;
        @NonNull
        private UrlBuilder urlBuilder;

        public LinkBuilderImpl(@NonNull AndroidManager androidManager, @NonNull UrlManager.Store linkStore, @NonNull UrlManager.Store parameterStore2) {
            super(androidManager);
            this.urlBuilder = UrlBuilder.empty().withScheme(linkStore.getValue(UrlManager.Link.SCHEME)).withHost(linkStore.getValue(UrlManager.Link.HOST)).withPath(linkStore.getValue(UrlManager.Link.PATH));
            this.parameterStore = parameterStore2;
        }

        @NonNull
        public UrlManager.Builder<String> addParameters(@NonNull String... params) {
            for (String param : params) {
                this.urlBuilder = this.urlBuilder.addParameter(param, this.parameterStore.getValue(param));
            }
            return this;
        }

        @NonNull
        public UrlManager.Builder<String> addCustomParameter(@NonNull String key, @NonNull String value) {
            this.urlBuilder = this.urlBuilder.addParameter(key, value);
            return this;
        }

        public String performBuild() {
            return this.urlBuilder.toString();
        }

        public String toString() {
            return performBuild();
        }
    }

    public static class BodyBuilderImpl extends BaseBuilderImpl<RequestBody> {
        @NonNull
        private final FormBody.Builder bodyBuilder = new FormBody.Builder();
        @NonNull
        private final UrlManager.Store parameterStore;

        public BodyBuilderImpl(@NonNull AndroidManager androidManager, @NonNull UrlManager.Store parameterStore2) {
            super(androidManager);
            this.parameterStore = parameterStore2;
        }

        @NonNull
        public UrlManager.Builder<RequestBody> addParameters(@NonNull String... params) {
            for (String param : params) {
                String value = this.parameterStore.getValue(param);
                this.bodyBuilder.add(param, value != null ? value : "");
                if (value == null) {
                    LogUtils.warning("Value for key '%s' is null", param);
                }
            }
            return this;
        }

        @NonNull
        public UrlManager.Builder<RequestBody> addCustomParameter(@NonNull String key, @NonNull String value) {
            this.bodyBuilder.add(key, value);
            return this;
        }

        public RequestBody performBuild() {
            return this.bodyBuilder.build();
        }
    }
}
