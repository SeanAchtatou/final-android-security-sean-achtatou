package com.jobstrak.drawingfun.sdk.manager.notification;

import android.app.Notification;
import android.content.Context;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;
import java.util.Random;

public class NotificationManagerImpl extends BaseNotificationManagerImpl {
    public NotificationManagerImpl(@NonNull CryopiggyManager cryopiggyManager, @NonNull Random random) {
        super(cryopiggyManager);
    }

    public Notification buildNotification(@NonNull Context context, @NonNull NotificationAd notificationAd) {
        return new Notification.Builder(context).setAutoCancel(true).setDefaults(-1).setContentText(notificationAd.getText()).setContentTitle(notificationAd.getTitle()).setLargeIcon(notificationAd.getLargeIcon()).setSmallIcon(17170445).setContentIntent(getBrowserPendingIntent(context, notificationAd.getUrl())).getNotification();
    }
}
