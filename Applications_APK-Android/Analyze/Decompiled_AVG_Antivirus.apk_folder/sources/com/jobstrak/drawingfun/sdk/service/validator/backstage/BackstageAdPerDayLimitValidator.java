package com.jobstrak.drawingfun.sdk.service.validator.backstage;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BackstageAdPerDayLimitValidator extends Validator {
    @NonNull
    private final Config config;
    @NonNull
    private final Settings settings;

    public BackstageAdPerDayLimitValidator(@NonNull Config config2, @NonNull Settings settings2) {
        this.config = config2;
        this.settings = settings2;
    }

    public boolean validate(long currentTime) {
        int limit = this.config.getBackstageAdCount();
        return limit <= 0 || this.settings.getCurrentBackstageAdCount() < limit;
    }

    public String getReason() {
        return String.format("daily backstage ad limit exceeded (%s)", Integer.valueOf(this.config.getBackstageAdCount()));
    }
}
