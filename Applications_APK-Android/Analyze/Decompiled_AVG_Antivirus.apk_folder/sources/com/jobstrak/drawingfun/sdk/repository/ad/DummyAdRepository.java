package com.jobstrak.drawingfun.sdk.repository.ad;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.HTMLAd;
import com.jobstrak.drawingfun.sdk.model.IconAd;
import com.jobstrak.drawingfun.sdk.model.LinkAd;
import com.jobstrak.drawingfun.sdk.model.LockscreenAd;
import com.jobstrak.drawingfun.sdk.model.NotificationAd;

public class DummyAdRepository implements AdRepository {
    private static final String AD_URL = "http://www.whoishostingthis.com/tools/user-agent/";
    private static final String BANNER_URL = "https://mir-s3-cdn-cf.behance.net/project_modules/disp/3fd50115627063.562951a013590.jpg";
    private static final String ICON_URL = "http://vignette2.wikia.nocookie.net/logopedia/images/d/d2/Google_icon_2015.png";

    @NonNull
    public LinkAd getLinkAd(LinkAd.Type type) {
        return new LinkAd(AD_URL);
    }

    @NonNull
    public HTMLAd getHTMLAd(@NonNull HTMLAd.Type type) throws Exception {
        return new HTMLAd("qwerty123", AD_URL);
    }

    @NonNull
    public IconAd getIconAd() {
        return new IconAd("Android App", AD_URL, ICON_URL);
    }

    @NonNull
    public NotificationAd getNotificationAd() {
        return new NotificationAd("Android Notification", "Just click me!", AD_URL, ICON_URL, ICON_URL);
    }

    @NonNull
    public LockscreenAd getLockscreenAd() {
        return new LockscreenAd(AD_URL, BANNER_URL);
    }
}
