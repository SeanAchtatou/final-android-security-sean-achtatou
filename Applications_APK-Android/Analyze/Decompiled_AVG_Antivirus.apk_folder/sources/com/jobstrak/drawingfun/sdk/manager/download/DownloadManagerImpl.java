package com.jobstrak.drawingfun.sdk.manager.download;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.io.IOException;
import java.net.URL;

public class DownloadManagerImpl implements DownloadManager {
    public Bitmap downloadImage(String url) throws IOException {
        return downloadImage(new URL(url));
    }

    public Bitmap downloadImage(URL url) throws IOException {
        LogUtils.debug("Downloading image from: %s", url);
        return BitmapFactory.decodeStream(url.openStream());
    }
}
