package com.jobstrak.drawingfun.sdk.service.detector;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.detector.Detector;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.service.validator.device.DuplicateServiceValidator;
import com.jobstrak.drawingfun.sdk.service.validator.device.NetworkStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.notification.NotificationAdDelayStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.notification.NotificationAdEnableStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.notification.NotificationAdPerDayLimitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.ConfigStateValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitValidator;
import com.jobstrak.drawingfun.sdk.service.validator.server.InitialDelayValidator;
import com.jobstrak.drawingfun.sdk.task.TaskFactory;
import com.jobstrak.drawingfun.sdk.task.ad.ShowNotificationAdTask2;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class NotificationAdDetector extends Detector {
    @NonNull
    private TaskFactory<ShowNotificationAdTask2> showNotificationAdTaskTaskFactory;
    @NonNull
    private final Validator[] validators;

    public NotificationAdDetector(@NonNull Config config, @NonNull Settings settings, @NonNull TaskFactory<ShowNotificationAdTask2> showNotificationAdTaskTaskFactory2) {
        super(config, settings);
        this.validators = new Validator[]{new DuplicateServiceValidator(), new InitValidator(config, settings), new ConfigStateValidator(settings), new InitialDelayValidator(config, settings), new NetworkStateValidator(config), new NotificationAdEnableStateValidator(config), new NotificationAdPerDayLimitValidator(config, settings), new NotificationAdDelayStateValidator(settings)};
        this.showNotificationAdTaskTaskFactory = showNotificationAdTaskTaskFactory2;
    }

    public void tick(Detector.Delegate delegate) {
    }

    public boolean detect(Detector.Delegate delegate) {
        for (Validator validator : this.validators) {
            if (!validator.validate(delegate.getCurrentTime())) {
                LogUtils.debug("Validator %s failed with reason: %s", validator.getClass().getSimpleName(), validator.getReason());
                return true;
            }
        }
        this.showNotificationAdTaskTaskFactory.create().execute(new Void[0]);
        getSettings().setNextNotificationAdTime(delegate.getCurrentTime() + getConfig().getNotificationAdDelay());
        getSettings().save();
        return true;
    }
}
