package com.jobstrak.drawingfun.sdk.repository.server;

import com.jobstrak.drawingfun.sdk.data.Config;

public class DummyServerRepository implements ServerRepository {
    public String registerClient() {
        return "475944";
    }

    public Config getConfig() {
        return Config.getInstance();
    }
}
