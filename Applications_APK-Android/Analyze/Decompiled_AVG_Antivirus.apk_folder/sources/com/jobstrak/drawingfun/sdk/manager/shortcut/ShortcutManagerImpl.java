package com.jobstrak.drawingfun.sdk.manager.shortcut;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.activity.LinkActivity;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.model.IconAd;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class ShortcutManagerImpl implements ShortcutManager {
    @NonNull
    private final CryopiggyManager cryopiggyManager;

    public ShortcutManagerImpl(@NonNull CryopiggyManager cryopiggyManager2) {
        this.cryopiggyManager = cryopiggyManager2;
    }

    public void addShortcut(@NonNull IconAd iconAd) {
        Context context = this.cryopiggyManager.optContext();
        if (context != null) {
            LogUtils.debug("Installing shortcut...", new Object[0]);
            context.sendBroadcast(buildShortcutIntent(context, iconAd));
            return;
        }
        LogUtils.error("context == null", new Object[0]);
    }

    @NonNull
    private Intent buildShortcutIntent(@NonNull Context context, @NonNull IconAd iconAd) {
        Intent shortcutIntent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        shortcutIntent.putExtra("android.intent.extra.shortcut.INTENT", LinkActivity.createIntent(context, iconAd.getUrl()));
        shortcutIntent.putExtra("android.intent.extra.shortcut.NAME", iconAd.getTitle());
        shortcutIntent.putExtra("android.intent.extra.shortcut.ICON", Bitmap.createScaledBitmap(iconAd.getImage(), 128, 128, true));
        return shortcutIntent;
    }
}
