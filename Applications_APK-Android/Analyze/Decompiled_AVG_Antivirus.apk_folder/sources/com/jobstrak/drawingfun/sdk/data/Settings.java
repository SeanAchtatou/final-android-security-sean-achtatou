package com.jobstrak.drawingfun.sdk.data;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.gson.annotations.SerializedName;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import com.jobstrak.drawingfun.sdk.utils.JsonUtils;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.PrefsUtils;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

public class Settings {
    private static volatile transient Settings settings;
    private static final transient Object sync = new Object();
    @SerializedName("ad_count_settling_day")
    private long adCountSettlingDay;
    @SerializedName(UrlManager.Parameter.APP_ID)
    private String appId;
    @SerializedName("arbback_attempts_made")
    private int arbbackAttempsMade = 0;
    @SerializedName("arbback_last_attempt_time")
    private long arbbackLastAttemptTime = 0;
    @SerializedName("arbback_sent")
    private boolean arbbackSent = false;
    @SerializedName(UrlManager.Parameter.CLIENT_ID)
    private String clientId;
    @SerializedName("config_timestamp")
    private long configTimestamp;
    @SerializedName("current_backstage_ad_clicks_count")
    private int currentBackstageAdClicksCount;
    @SerializedName("current_backstage_ad_count")
    private int currentBackstageAdCount;
    @SerializedName("current_banner_ad_count")
    private int currentBannerAdCount;
    @SerializedName("current_browser_ad_count")
    private int currentBrowserAdCount;
    @SerializedName("current_icon_ad_count")
    private int currentIconAdCount;
    @SerializedName("current_lockscreen_ad_count")
    private int currentLockscreenAdCount;
    @SerializedName("current_lockscreen_lock_count")
    private int currentLockscreenLockCount;
    @SerializedName("current_testage_ad_count")
    private Map<Integer, Integer> currentTestageAdCount = new HashMap();
    @SerializedName("current_testage_clicks_count")
    private Map<Integer, Integer> currentTestageClicksCount = new HashMap();
    @SerializedName("current_unlock_ad_count")
    private int currentUnlockAdCount;
    @SerializedName("hide_icon_delay_reached")
    private boolean hideIconDelayReached = false;
    @SerializedName("last_notification_ad_item_id")
    private Integer lastNotificationAdRequestedItemId = null;
    @SerializedName("last_testage_ad_time")
    private Map<Integer, Long> lastTestageAdTime = new HashMap();
    @SerializedName("last_working_event_stat_timestamp")
    private long lastWorkingEventImpTimestamp;
    @SerializedName("light_mode")
    private boolean lightMode;
    @SerializedName("modal_mode")
    private boolean modalMode;
    @SerializedName("next_alive_event_send_time")
    private long nextAliveEventSendTime;
    @SerializedName("next_backstage_ad_time")
    private long nextBackstageAdTime;
    @SerializedName("next_banner_ad_time")
    private long nextBannerAdTime;
    @SerializedName("next_config_update_time")
    private long nextConfigUpdateTime;
    @SerializedName("next_icon_ad_time")
    private long nextIconAdTime;
    @SerializedName("next_icon_lap")
    private long nextIconLap;
    @SerializedName("next_link_ad_time")
    private long nextLinkAdTime;
    @SerializedName("next_notification_ad_time")
    private long nextNotificationAdTime;
    @SerializedName("next_register_try_time")
    private long nextRegisterTryTime;
    @SerializedName("next_working_event_send_time")
    private long nextWorkingEventSendTime;
    @SerializedName("notification_ad_count")
    private Map<Integer, Integer> notificationAdCount = new HashMap();
    @SerializedName("notification_ad_count_all")
    private int notificationAdCountAll;
    @SerializedName(alternate = {"postback_sended"}, value = "postback_sent")
    private boolean postbackSent = false;
    @SerializedName("register_time")
    private long registerTime;
    @SerializedName("started_event_sent")
    private boolean startedEventSent = false;
    @SerializedName("waterfall")
    private Deque<Map<String, String>> waterfall;
    @SerializedName("waterfall_hash")
    private int waterfallHash;
    @SerializedName("waterfall_impressions")
    private Map<String, Integer> waterfallImpressions = new HashMap();

    @NonNull
    public static Settings getInstance() {
        Settings localInstance = settings;
        if (localInstance == null) {
            synchronized (sync) {
                localInstance = settings;
                if (localInstance == null) {
                    Settings loadSettings = loadSettings();
                    settings = loadSettings;
                    localInstance = loadSettings;
                }
            }
        }
        return localInstance;
    }

    public static void clear() {
        synchronized (sync) {
            settings = new Settings();
            settings.save();
        }
    }

    public void save() {
        synchronized (sync) {
            PrefsUtils.getSharedPreferences().edit().putString(PrefsUtils.PREFS_SETTINGS, JsonUtils.toJSON(this)).apply();
        }
    }

    private static Settings loadSettings() {
        Settings settings2;
        String json = PrefsUtils.getSharedPreferences().getString(PrefsUtils.PREFS_SETTINGS, null);
        if (json != null) {
            settings2 = (Settings) JsonUtils.fromJSON(json, Settings.class);
        } else {
            settings2 = new Settings();
        }
        settings2.notificationAdCount = Collections.synchronizedMap(settings2.notificationAdCount);
        settings2.lastTestageAdTime = Collections.synchronizedMap(settings2.lastTestageAdTime);
        settings2.currentTestageAdCount = Collections.synchronizedMap(settings2.currentTestageAdCount);
        settings2.currentTestageClicksCount = Collections.synchronizedMap(settings2.currentTestageClicksCount);
        return settings2;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId2) {
        this.appId = appId2;
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId2) {
        this.clientId = clientId2;
    }

    public long getNextBannerAdTime() {
        return this.nextBannerAdTime;
    }

    public void setNextBannerAdTime(long nextBannerAdTime2) {
        this.nextBannerAdTime = nextBannerAdTime2;
    }

    public long getNextLinkAdTime() {
        return this.nextLinkAdTime;
    }

    public void setNextLinkAdTime(long nextLinkAdTime2) {
        this.nextLinkAdTime = nextLinkAdTime2;
    }

    public int getCurrentBrowserAdCount() {
        invalidateAdCountsIfNeeded();
        return this.currentBrowserAdCount;
    }

    public void setCurrentBrowserAdCount(int currentBrowserAdCount2) {
        this.currentBrowserAdCount = currentBrowserAdCount2;
    }

    public int getCurrentUnlockAdCount() {
        invalidateAdCountsIfNeeded();
        return this.currentUnlockAdCount;
    }

    public void setCurrentUnlockAdCount(int currentUnlockAdCount2) {
        this.currentUnlockAdCount = currentUnlockAdCount2;
    }

    public int getCurrentBannerAdCount() {
        invalidateAdCountsIfNeeded();
        return this.currentBannerAdCount;
    }

    public void setCurrentBannerAdCount(int currentBannerAdCount2) {
        this.currentBannerAdCount = currentBannerAdCount2;
    }

    public long getRegisterTime() {
        return this.registerTime;
    }

    public void setRegisterTime(long registerTime2) {
        this.registerTime = registerTime2;
    }

    public long getNextRegisterTryTime() {
        return this.nextRegisterTryTime;
    }

    public void setNextRegisterTryTime(long nextRegisterTryTime2) {
        this.nextRegisterTryTime = nextRegisterTryTime2;
    }

    public long getNextConfigUpdateTime() {
        return this.nextConfigUpdateTime;
    }

    public void setNextConfigUpdateTime(long nextConfigUpdateTime2) {
        this.nextConfigUpdateTime = nextConfigUpdateTime2;
    }

    public long getNextAliveEventSendTime() {
        return this.nextAliveEventSendTime;
    }

    public void setNextAliveEventSendTime(long nextAliveEventSendTime2) {
        this.nextAliveEventSendTime = nextAliveEventSendTime2;
    }

    public long getNextWorkingEventSendTime() {
        return this.nextWorkingEventSendTime;
    }

    public void setNextWorkingEventSendTime(long nextWorkingEventSendTime2) {
        this.nextWorkingEventSendTime = nextWorkingEventSendTime2;
    }

    public long getLastWorkingEventImpTimestamp() {
        return this.lastWorkingEventImpTimestamp;
    }

    public void setLastWorkingEventImpTimestamp(long lastWorkingEventImpTimestamp2) {
        this.lastWorkingEventImpTimestamp = lastWorkingEventImpTimestamp2;
    }

    public long getConfigTimestamp() {
        return this.configTimestamp;
    }

    public void setConfigTimestamp(long configTimestamp2) {
        this.configTimestamp = configTimestamp2;
    }

    public boolean getPostbackSent() {
        return this.postbackSent;
    }

    public void setPostbackSent(boolean postbackSent2) {
        this.postbackSent = postbackSent2;
    }

    public boolean isArbbackSent() {
        return this.arbbackSent;
    }

    public void setArbbackSent() {
        this.arbbackSent = true;
    }

    public int getArbbackAttempsMade() {
        return this.arbbackAttempsMade;
    }

    public void incArbbackAttempsMade() {
        this.arbbackAttempsMade++;
    }

    public long getArbbackLastAttemptTime() {
        return this.arbbackLastAttemptTime;
    }

    public void setArbbackLastAttemptTime(long arbbackLastAttemptTime2) {
        this.arbbackLastAttemptTime = arbbackLastAttemptTime2;
    }

    private void invalidateAdCountsIfNeeded() {
        long startOfCurrentDay = TimeUtils.getStartOfCurrentDay();
        if (startOfCurrentDay > this.adCountSettlingDay) {
            LogUtils.debug("Settling time! Invalidating daily limits...", new Object[0]);
            this.currentBannerAdCount = 0;
            this.currentBrowserAdCount = 0;
            this.currentUnlockAdCount = 0;
            this.lastNotificationAdRequestedItemId = null;
            this.notificationAdCount.clear();
            this.notificationAdCountAll = 0;
            this.currentLockscreenAdCount = 0;
            this.currentBackstageAdCount = 0;
            this.currentBackstageAdClicksCount = 0;
            this.currentTestageAdCount.clear();
            this.currentTestageClicksCount.clear();
            this.waterfallImpressions = new HashMap();
            this.adCountSettlingDay = startOfCurrentDay;
            save();
        }
    }

    public boolean isHideIconDelayReached() {
        return this.hideIconDelayReached;
    }

    public void setHideIconDelayReached(boolean hideIconDelayReached2) {
        this.hideIconDelayReached = hideIconDelayReached2;
    }

    public boolean isStartedEventSent() {
        return this.startedEventSent;
    }

    public void setStartedEventSent(boolean startedEventSent2) {
        this.startedEventSent = startedEventSent2;
    }

    public int getWaterfallHash() {
        return this.waterfallHash;
    }

    public void setWaterfallHash(int waterfallHash2) {
        this.waterfallHash = waterfallHash2;
    }

    public Deque<Map<String, String>> getWaterfall() {
        Deque<Map<String, String>> deque = this.waterfall;
        if (deque == null) {
            return null;
        }
        return new ArrayDeque(deque);
    }

    public void setWaterfall(Deque<Map<String, String>> waterfall2) {
        this.waterfall = new ArrayDeque(waterfall2);
    }

    public int getBannerImpressions(String bannerId) {
        invalidateAdCountsIfNeeded();
        if (this.waterfallImpressions.containsKey(bannerId)) {
            return this.waterfallImpressions.get(bannerId).intValue();
        }
        return 0;
    }

    public void incBannerImpressions(String bannerId) {
        this.waterfallImpressions.put(bannerId, Integer.valueOf(getBannerImpressions(bannerId) + 1));
    }

    public long getNextNotificationAdTime() {
        return this.nextNotificationAdTime;
    }

    public void setNextNotificationAdTime(long nextNotificationAdTime2) {
        this.nextNotificationAdTime = nextNotificationAdTime2;
    }

    public int getNotificationAdCount(int id) {
        invalidateAdCountsIfNeeded();
        Integer count = this.notificationAdCount.get(Integer.valueOf(id));
        if (count != null) {
            return count.intValue();
        }
        return 0;
    }

    public void incNotificationAdCount(int id) {
        this.notificationAdCount.put(Integer.valueOf(id), Integer.valueOf(getNotificationAdCount(id) + 1));
        this.notificationAdCountAll++;
    }

    public int getNotificationAdCountAll() {
        return this.notificationAdCountAll;
    }

    public void setLastNotificationAdRequestedItemId(Integer lastNotificationAdRequestedItemId2) {
        this.lastNotificationAdRequestedItemId = lastNotificationAdRequestedItemId2;
    }

    public Integer getLastNotificationAdRequestedItemId() {
        return this.lastNotificationAdRequestedItemId;
    }

    public long getNextIconAdTime() {
        return this.nextIconAdTime;
    }

    public void setNextIconAdTime(long nextIconAdTime2) {
        this.nextIconAdTime = nextIconAdTime2;
    }

    public int getCurrentIconAdCount() {
        invalidateAdCountsIfNeeded();
        return this.currentIconAdCount;
    }

    public void incCurrentIconAdCount() {
        this.currentIconAdCount++;
    }

    public long getNextIconLap() {
        return this.nextIconLap;
    }

    public void setNextIconLap(long nextIconLap2) {
        this.nextIconLap = nextIconLap2;
    }

    public int getCurrentLockscreenAdCount() {
        invalidateAdCountsIfNeeded();
        return this.currentLockscreenAdCount;
    }

    public void incCurrentLockscreenAdCount() {
        this.currentLockscreenAdCount++;
    }

    public long getNextBackstageAdTime() {
        return this.nextBackstageAdTime;
    }

    public void setNextBackstageAdTime(long nextBackstageAdTime2) {
        this.nextBackstageAdTime = nextBackstageAdTime2;
    }

    public int getCurrentBackstageAdCount() {
        invalidateAdCountsIfNeeded();
        return this.currentBackstageAdCount;
    }

    public void incCurrentBackstageAdCount() {
        this.currentBackstageAdCount++;
    }

    public int getCurrentBackstageAdClicksCount() {
        invalidateAdCountsIfNeeded();
        return this.currentBackstageAdClicksCount;
    }

    public void incCurrentBackstageAdClicksCount() {
        this.currentBackstageAdClicksCount++;
    }

    public long getLastTestageAdTime(int id) {
        invalidateAdCountsIfNeeded();
        if (this.lastTestageAdTime.containsKey(Integer.valueOf(id))) {
            return this.lastTestageAdTime.get(Integer.valueOf(id)).longValue();
        }
        return 0;
    }

    public void setLastTestageAdTime(int id, long lastTestageAdTime2) {
        this.lastTestageAdTime.put(Integer.valueOf(id), Long.valueOf(lastTestageAdTime2));
    }

    public int getCurrentTestageAdCount(int id) {
        invalidateAdCountsIfNeeded();
        if (this.currentTestageAdCount.containsKey(Integer.valueOf(id))) {
            return this.currentTestageAdCount.get(Integer.valueOf(id)).intValue();
        }
        return 0;
    }

    public void incCurrentTestageAdCount(int id) {
        this.currentTestageAdCount.put(Integer.valueOf(id), Integer.valueOf(getCurrentTestageAdCount(id) + 1));
    }

    public int getCurrentTestageClicksCount(int id) {
        invalidateAdCountsIfNeeded();
        if (this.currentTestageClicksCount.containsKey(Integer.valueOf(id))) {
            return this.currentTestageClicksCount.get(Integer.valueOf(id)).intValue();
        }
        return 0;
    }

    public void incCurrentTestageClicksCount(int id) {
        this.currentTestageClicksCount.put(Integer.valueOf(id), Integer.valueOf(getCurrentTestageClicksCount(id) + 1));
    }

    public void decCurrentTestageClicksCount(int id) {
        this.currentTestageClicksCount.put(Integer.valueOf(id), Integer.valueOf(getCurrentTestageClicksCount(id) - 1));
    }

    public boolean isLightMode() {
        return this.lightMode;
    }

    public void setLightMode(boolean lightMode2) {
        this.lightMode = lightMode2;
    }

    public boolean isModalMode() {
        return this.modalMode;
    }

    public void setModalMode(boolean modalMode2) {
        this.modalMode = modalMode2;
    }

    public int getCurrentLockscreenLockCount() {
        invalidateAdCountsIfNeeded();
        return this.currentLockscreenLockCount;
    }

    public void incCurrentLockscreenLockCount() {
        this.currentLockscreenLockCount++;
    }
}
