package com.jobstrak.drawingfun.sdk.service.validator.browser;

import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class BrowserAdPerDayLimitValidator extends Validator {
    private int browserAdCount;
    private int currentBrowserAdCount;

    public boolean validate(long currentTime) {
        this.browserAdCount = Config.getInstance().getBrowserAdCount();
        this.currentBrowserAdCount = Settings.getInstance().getCurrentBrowserAdCount();
        int i = this.browserAdCount;
        return i <= 0 || this.currentBrowserAdCount < i;
    }

    public String getReason() {
        return String.format("browser ad count per day reached (%s/%s)", Integer.valueOf(this.currentBrowserAdCount), Integer.valueOf(this.browserAdCount));
    }
}
