package com.jobstrak.drawingfun.sdk.permissions;

import android.app.Activity;
import com.jobstrak.drawingfun.sdk.activity.PermissionsActivity;

public class Permissions {
    public Permissions(Activity activity) {
        PermissionsActivity.checkPermissions(activity);
    }
}
