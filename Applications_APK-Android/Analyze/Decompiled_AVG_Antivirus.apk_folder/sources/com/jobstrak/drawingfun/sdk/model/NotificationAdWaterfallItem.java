package com.jobstrak.drawingfun.sdk.model;

import com.google.android.gms.common.internal.ImagesContract;
import com.jobstrak.drawingfun.lib.gson.annotations.SerializedName;

public class NotificationAdWaterfallItem {
    @SerializedName("daily_limit")
    private int dailyLimit;
    @SerializedName(ImagesContract.URL)
    private String url;

    public NotificationAdWaterfallItem() {
    }

    public NotificationAdWaterfallItem(String url2, int dailyLimit2) {
        this.url = url2;
        this.dailyLimit = dailyLimit2;
    }

    public String getUrl() {
        return this.url;
    }

    public int getDailyLimit() {
        return this.dailyLimit;
    }
}
