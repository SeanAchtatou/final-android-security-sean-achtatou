package com.jobstrak.drawingfun.sdk.service.validator.banner;

import com.jobstrak.drawingfun.sdk.data.Settings;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;
import com.jobstrak.drawingfun.sdk.utils.TimeUtils;

public class BannerAdPauseStateValidator extends Validator {
    private long timeLeft;

    public boolean validate(long currentTime) {
        this.timeLeft = Settings.getInstance().getNextBannerAdTime() - currentTime;
        return this.timeLeft <= 0;
    }

    public String getReason() {
        return String.format("banner ad is [pre?]paused (%s left)", TimeUtils.parseTime(this.timeLeft));
    }
}
