package com.jobstrak.drawingfun.sdk.service.validator.testage;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.model.TestageItem;
import com.jobstrak.drawingfun.sdk.service.validator.testage.TestageValidator;

public class TestageClicksButtonStateValidator implements TestageValidator {
    public TestageValidator.Result validate(long currentTime, @NonNull TestageItem item) {
        return new TestageValidator.Result(!TextUtils.isEmpty(item.getButtonAttrName()) && !TextUtils.isEmpty(item.getButtonAttrValue())) {
            public String reason() {
                return "button_id or button_type is empty";
            }
        };
    }
}
