package com.jobstrak.drawingfun.sdk.util;

public interface Supplier<T> {
    T get();
}
