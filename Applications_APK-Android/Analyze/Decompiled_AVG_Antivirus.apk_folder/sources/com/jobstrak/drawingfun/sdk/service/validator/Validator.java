package com.jobstrak.drawingfun.sdk.service.validator;

public abstract class Validator {
    public abstract String getReason();

    public abstract boolean validate(long j);
}
