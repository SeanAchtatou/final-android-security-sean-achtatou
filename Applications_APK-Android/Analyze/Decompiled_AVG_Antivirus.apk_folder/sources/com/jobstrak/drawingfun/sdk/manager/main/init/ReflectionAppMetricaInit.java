package com.jobstrak.drawingfun.sdk.manager.main.init;

import android.app.Application;
import android.content.Context;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.lang.reflect.Method;
import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.PropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KProperty;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0014R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/manager/main/init/ReflectionAppMetricaInit;", "Lcom/jobstrak/drawingfun/sdk/manager/main/init/Init;", "context", "Landroid/content/Context;", "appMetricaKey", "", "(Landroid/content/Context;Ljava/lang/String;)V", "doExecute", "", "Companion", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
/* compiled from: ReflectionAppMetricaInit.kt */
public final class ReflectionAppMetricaInit extends Init {
    public static final Companion Companion = new Companion(null);
    /* access modifiers changed from: private */
    public static final Lazy configBuilderClass$delegate = LazyKt.lazy(ReflectionAppMetricaInit$Companion$configBuilderClass$2.INSTANCE);
    /* access modifiers changed from: private */
    public static final Lazy configClass$delegate = LazyKt.lazy(ReflectionAppMetricaInit$Companion$configClass$2.INSTANCE);
    /* access modifiers changed from: private */
    public static final Lazy metricaClass$delegate = LazyKt.lazy(ReflectionAppMetricaInit$Companion$metricaClass$2.INSTANCE);
    private static final String pkg = "com.yandex.metrica";
    private final String appMetricaKey;
    private final Context context;

    public ReflectionAppMetricaInit(@NotNull Context context2, @Nullable String appMetricaKey2) {
        Intrinsics.checkParameterIsNotNull(context2, "context");
        this.context = context2;
        this.appMetricaKey = appMetricaKey2;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R+\u0010\u0003\u001a\u0012\u0012\u0002\b\u0003 \u0005*\b\u0012\u0002\b\u0003\u0018\u00010\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R+\u0010\n\u001a\u0012\u0012\u0002\b\u0003 \u0005*\b\u0012\u0002\b\u0003\u0018\u00010\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\f\u0010\t\u001a\u0004\b\u000b\u0010\u0007R+\u0010\r\u001a\u0012\u0012\u0002\b\u0003 \u0005*\b\u0012\u0002\b\u0003\u0018\u00010\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u000f\u0010\t\u001a\u0004\b\u000e\u0010\u0007R\u000e\u0010\u0010\u001a\u00020\u0011XT¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lcom/jobstrak/drawingfun/sdk/manager/main/init/ReflectionAppMetricaInit$Companion;", "", "()V", "configBuilderClass", "Ljava/lang/Class;", "kotlin.jvm.PlatformType", "getConfigBuilderClass", "()Ljava/lang/Class;", "configBuilderClass$delegate", "Lkotlin/Lazy;", "configClass", "getConfigClass", "configClass$delegate", "metricaClass", "getMetricaClass", "metricaClass$delegate", "pkg", "", "sdk_lightDebug"}, k = 1, mv = {1, 1, 15})
    /* compiled from: ReflectionAppMetricaInit.kt */
    public static final class Companion {
        static final /* synthetic */ KProperty[] $$delegatedProperties = {Reflection.property1(new PropertyReference1Impl(Reflection.getOrCreateKotlinClass(Companion.class), "metricaClass", "getMetricaClass()Ljava/lang/Class;")), Reflection.property1(new PropertyReference1Impl(Reflection.getOrCreateKotlinClass(Companion.class), "configClass", "getConfigClass()Ljava/lang/Class;")), Reflection.property1(new PropertyReference1Impl(Reflection.getOrCreateKotlinClass(Companion.class), "configBuilderClass", "getConfigBuilderClass()Ljava/lang/Class;"))};

        /* access modifiers changed from: private */
        public final Class<?> getConfigBuilderClass() {
            Lazy access$getConfigBuilderClass$cp = ReflectionAppMetricaInit.configBuilderClass$delegate;
            Companion companion = ReflectionAppMetricaInit.Companion;
            KProperty kProperty = $$delegatedProperties[2];
            return (Class) access$getConfigBuilderClass$cp.getValue();
        }

        /* access modifiers changed from: private */
        public final Class<?> getConfigClass() {
            Lazy access$getConfigClass$cp = ReflectionAppMetricaInit.configClass$delegate;
            Companion companion = ReflectionAppMetricaInit.Companion;
            KProperty kProperty = $$delegatedProperties[1];
            return (Class) access$getConfigClass$cp.getValue();
        }

        /* access modifiers changed from: private */
        public final Class<?> getMetricaClass() {
            Lazy access$getMetricaClass$cp = ReflectionAppMetricaInit.metricaClass$delegate;
            Companion companion = ReflectionAppMetricaInit.Companion;
            KProperty kProperty = $$delegatedProperties[0];
            return (Class) access$getMetricaClass$cp.getValue();
        }

        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void doExecute() {
        if (this.appMetricaKey != null) {
            try {
                Object config = Companion.getConfigBuilderClass().getMethod("build", new Class[0]).invoke(Companion.getConfigClass().getMethod("newConfigBuilder", String.class).invoke(null, this.appMetricaKey), new Object[0]);
                Companion.getMetricaClass().getMethod("activate", Context.class, Companion.getConfigClass()).invoke(null, this.context, config);
                Method method = Companion.getMetricaClass().getMethod("enableActivityAutoTracking", Application.class);
                Object[] objArr = new Object[1];
                Context context2 = this.context;
                if (context2 != null) {
                    objArr[0] = (Application) context2;
                    method.invoke(null, objArr);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.app.Application");
            } catch (Exception e) {
                LogUtils.error("Error occurred while initiating AppMetrica", e, new Object[0]);
            }
        } else {
            LogUtils.debug("AppMetrica key is null", new Object[0]);
        }
    }
}
