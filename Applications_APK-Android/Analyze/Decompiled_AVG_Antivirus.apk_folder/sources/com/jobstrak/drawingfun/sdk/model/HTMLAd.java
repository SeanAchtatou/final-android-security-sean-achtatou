package com.jobstrak.drawingfun.sdk.model;

import com.google.android.gms.common.internal.ImagesContract;
import com.jobstrak.drawingfun.lib.gson.annotations.SerializedName;
import java.io.Serializable;

public class HTMLAd implements Serializable {
    public static final String TYPE_BROWSER = "browser";
    private static final String TYPE_DEFAULT = "webview";
    public static final String TYPE_MRAID = "mraid";
    public static final String TYPE_WEBVIEW = "webview";
    private static final long serialVersionUID = -5673951098012253524L;
    @SerializedName("bundle")
    private String bundle;
    @SerializedName("html_data")
    private String htmlData;
    @SerializedName("request_id")
    private String requestId;
    @SerializedName("type")
    private String type;
    @SerializedName(ImagesContract.URL)
    private String url;

    public enum Type {
        HTML("html"),
        LINKVIEW("linkview"),
        CUSTOMTABS("customtabs");
        
        private String s;

        private Type(String s2) {
            this.s = s2;
        }

        public String toString() {
            return this.s;
        }
    }

    public HTMLAd(String requestId2, String url2) {
        this(requestId2, url2, "webview", null);
    }

    public HTMLAd(String requestId2, String url2, String type2, String bundle2) {
        this.requestId = requestId2;
        this.url = url2;
        this.type = type2;
        this.bundle = bundle2;
    }

    public String getRequestId() {
        return this.requestId;
    }

    public String getUrl() {
        return this.url;
    }

    public String getHtmlData() {
        return this.htmlData;
    }

    public void setHtmlData(String htmlData2) {
        this.htmlData = htmlData2;
    }

    public String getType() {
        String str = this.type;
        return str != null ? str : "webview";
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getBundle() {
        return this.bundle;
    }

    public void setBundle(String bundle2) {
        this.bundle = bundle2;
    }
}
