package com.jobstrak.drawingfun.sdk;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.IBinder;
import android.view.WindowManager;
import android.widget.ImageView;
import androidx.annotation.RequiresApi;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;
import com.jobstrak.drawingfun.sdk.service.MainTimer;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import com.jobstrak.drawingfun.sdk.utils.ScreenUtils;

public class BackupService extends Service {
    public static final String ACT_CREATE_OVERLAY_VIEW = "com.jobstrak.drawingfun.sdkintent.action.CREATE_OVERLAY_VIEW";
    public static final String ACT_REMOVE_OVERLAY_VIEW = "com.jobstrak.drawingfun.sdkintent.action.REMOVE_OVERLAY_VIEW";
    public static final String ACT_START = "com.jobstrak.drawingfun.sdkintent.action.START";
    public static final String ACT_STOP = "com.jobstrak.drawingfun.sdkintent.action.STOP";
    private WindowManager.LayoutParams overlayParams;
    private ImageView overlayView;
    private WindowManager windowManager;

    private void createOverlayView() {
        removeOverlayView();
        this.windowManager.addView(this.overlayView, this.overlayParams);
    }

    private void removeOverlayView() {
        if (this.overlayView.getParent() != null) {
            this.windowManager.removeView(this.overlayView);
        }
    }

    private static Drawable createDrawable() {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setSize((ManagerFactory.getAndroidManager().getScreenSize().x / 2) - ScreenUtils.dp(50), ScreenUtils.dp(50));
        drawable.setColor(0);
        drawable.setShape(0);
        return drawable;
    }

    public void onCreate() {
        LogUtils.debug("Creating service...", new Object[0]);
        Cryopiggy.init(this);
        ManagerFactory.createFlurryManager().onStartSession(this);
        this.overlayView = new ImageView(this);
        this.overlayView.setImageDrawable(createDrawable());
        this.windowManager = (WindowManager) getSystemService("window");
        this.overlayParams = new WindowManager.LayoutParams(-2, -2, 2002, 32, -3);
        this.overlayParams.gravity = 8388691;
        int i = Build.VERSION.SDK_INT;
    }

    @RequiresApi(26)
    private String createNotificationChannel(NotificationManager notificationManager) {
        NotificationChannel channel = new NotificationChannel("cid1234", "name", 0);
        channel.setLockscreenVisibility(-1);
        notificationManager.createNotificationChannel(channel);
        return "cid1234";
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (action.equals(ACT_START)) {
                LogUtils.debug("Starting service...", new Object[0]);
                MainTimer.getInstance().startIfNotRunning();
            } else if (action.equals(ACT_STOP)) {
                LogUtils.debug("Stopping service...", new Object[0]);
                stopSelf();
            } else if (action.equals(ACT_CREATE_OVERLAY_VIEW)) {
                LogUtils.debug("Creating overlay view...", new Object[0]);
                createOverlayView();
            } else if (action.equals(ACT_REMOVE_OVERLAY_VIEW)) {
                LogUtils.debug("Removing overlay view...", new Object[0]);
                removeOverlayView();
            } else {
                LogUtils.warning("Unknown action: %s", intent.getAction());
            }
        } else {
            LogUtils.warning("intent == null", new Object[0]);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        LogUtils.debug("Service destroyed", new Object[0]);
        ManagerFactory.createFlurryManager().onEndSession(this);
        sendBroadcast(new Intent(ACT_START));
    }
}
