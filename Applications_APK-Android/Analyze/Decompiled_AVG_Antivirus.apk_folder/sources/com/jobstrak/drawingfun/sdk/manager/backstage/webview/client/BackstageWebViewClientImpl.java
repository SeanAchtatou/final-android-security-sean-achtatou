package com.jobstrak.drawingfun.sdk.manager.backstage.webview.client;

import android.graphics.Bitmap;
import android.webkit.WebView;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.manager.backstage.timer.BackstageClickTimer;
import com.jobstrak.drawingfun.sdk.utils.SecureLogUtils;

public class BackstageWebViewClientImpl extends BackstageWebViewClient {
    @Nullable
    private final BackstageClickTimer clickTimer;
    private boolean enabled = true;

    public BackstageWebViewClientImpl(@Nullable BackstageClickTimer clickTimer2) {
        this.clickTimer = clickTimer2;
    }

    public void setEnabled(boolean enabled2) {
        SecureLogUtils.debug(String.valueOf(enabled2), new Object[0]);
        this.enabled = enabled2;
    }

    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        BackstageClickTimer backstageClickTimer;
        SecureLogUtils.debug("Loading URL in backstage: %s", url);
        if (this.enabled && (backstageClickTimer = this.clickTimer) != null) {
            backstageClickTimer.stopTimer();
        }
    }

    public void onPageFinished(WebView view, String url) {
        BackstageClickTimer backstageClickTimer;
        SecureLogUtils.debug("Loaded URL in backstage: %s", url);
        if (this.enabled && (backstageClickTimer = this.clickTimer) != null) {
            backstageClickTimer.startTimer();
        }
    }
}
