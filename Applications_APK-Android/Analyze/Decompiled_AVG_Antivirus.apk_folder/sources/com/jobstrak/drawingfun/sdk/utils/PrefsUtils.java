package com.jobstrak.drawingfun.sdk.utils;

import android.content.SharedPreferences;
import com.jobstrak.drawingfun.sdk.manager.ManagerFactory;

public class PrefsUtils {
    public static final String PREFS_CONFIG = "config";
    public static final String PREFS_CURRENT_TIME = "current_time";
    public static final String PREFS_SETTINGS = "settings";
    public static final String PREFS_STATS = "stats";
    private static volatile SharedPreferences sharedPreferences;

    public static SharedPreferences getSharedPreferences() {
        SharedPreferences localInstance = sharedPreferences;
        if (localInstance == null) {
            synchronized (SharedPreferences.class) {
                localInstance = sharedPreferences;
                if (localInstance == null) {
                    SharedPreferences sharedPreferences2 = ManagerFactory.getCryopiggyManager().optContext().getSharedPreferences("yteSlT7daYsGlbVGqXfR", 0);
                    sharedPreferences = sharedPreferences2;
                    localInstance = sharedPreferences2;
                }
            }
        }
        return localInstance;
    }
}
