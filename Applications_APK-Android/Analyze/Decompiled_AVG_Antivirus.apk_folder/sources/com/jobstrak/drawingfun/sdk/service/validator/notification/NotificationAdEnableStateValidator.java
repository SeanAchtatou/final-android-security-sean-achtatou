package com.jobstrak.drawingfun.sdk.service.validator.notification;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.sdk.data.Config;
import com.jobstrak.drawingfun.sdk.service.validator.Validator;

public class NotificationAdEnableStateValidator extends Validator {
    @NonNull
    private final Config config;

    public NotificationAdEnableStateValidator(@NonNull Config config2) {
        this.config = config2;
    }

    public boolean validate(long currentTime) {
        return this.config.isNotificationAdEnabled();
    }

    public String getReason() {
        return "notification ad is disabled";
    }
}
