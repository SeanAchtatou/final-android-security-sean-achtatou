package com.jobstrak.drawingfun.sdk.manager.apk;

import android.content.Context;
import android.content.pm.PackageManager;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.sdk.manager.main.CryopiggyManager;
import com.jobstrak.drawingfun.sdk.utils.FileUtils;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;
import java.io.File;
import kotlin.UByte;

public class ApkManagerImpl implements ApkManager {
    private final CryopiggyManager cryopiggyManager;

    public ApkManagerImpl(CryopiggyManager cryopiggyManager2) {
        this.cryopiggyManager = cryopiggyManager2;
    }

    @Nullable
    public File getApkFile() {
        if (this.cryopiggyManager.isInitialized()) {
            try {
                Context context = this.cryopiggyManager.optContext();
                return new File(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.publicSourceDir);
            } catch (PackageManager.NameNotFoundException e) {
                LogUtils.wtf("Can't retrieve the apk file of the app", e, new Object[0]);
                return null;
            }
        } else {
            LogUtils.error("isn't initialized", new Object[0]);
            return null;
        }
    }

    @Nullable
    public String getApkFileComment() {
        LogUtils.debug();
        File apkFile = getApkFile();
        if (apkFile != null) {
            return getApkFileComment(FileUtils.readBytes(apkFile));
        }
        LogUtils.error("Apk file is null", new Object[0]);
        return null;
    }

    private String getApkFileComment(byte[] buffer) {
        LogUtils.debug();
        byte[] magicDirEnd = {80, 75, 5, 6};
        for (int i = (buffer.length - magicDirEnd.length) - 22; i >= 0; i--) {
            boolean isMagicStart = true;
            int k = 0;
            while (true) {
                if (k >= magicDirEnd.length) {
                    break;
                } else if (buffer[i + k] != magicDirEnd[k]) {
                    isMagicStart = false;
                    break;
                } else {
                    k++;
                }
            }
            if (isMagicStart) {
                int commentLen = buffer[i + 20] + (buffer[i + 21] * UByte.MIN_VALUE);
                int realLen = (buffer.length - i) - 22;
                if (commentLen != realLen) {
                    LogUtils.warning("Comment size mismatch: directory says len is %d, but file ends after %d bytes", Integer.valueOf(commentLen), Integer.valueOf(realLen));
                }
                String comment = new String(buffer, i + 22, Math.min(commentLen, realLen));
                LogUtils.debug("Apk file comment is %s", comment);
                return comment;
            }
        }
        LogUtils.debug("Apk file comment doesn't exist", new Object[0]);
        return null;
    }
}
