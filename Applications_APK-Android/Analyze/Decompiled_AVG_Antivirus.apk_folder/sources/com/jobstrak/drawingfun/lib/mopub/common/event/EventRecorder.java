package com.jobstrak.drawingfun.lib.mopub.common.event;

public interface EventRecorder {
    void record(BaseEvent baseEvent);
}
