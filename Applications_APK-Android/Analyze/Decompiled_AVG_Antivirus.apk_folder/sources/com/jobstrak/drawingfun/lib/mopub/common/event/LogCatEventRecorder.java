package com.jobstrak.drawingfun.lib.mopub.common.event;

import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;

class LogCatEventRecorder implements EventRecorder {
    LogCatEventRecorder() {
    }

    public void record(BaseEvent baseEvent) {
        MoPubLog.d(baseEvent.toString());
    }
}
