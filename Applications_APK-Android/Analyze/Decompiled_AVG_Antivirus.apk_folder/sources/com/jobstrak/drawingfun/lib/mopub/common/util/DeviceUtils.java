package com.jobstrak.drawingfun.lib.mopub.common.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Process;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.CreativeOrientation;
import com.jobstrak.drawingfun.lib.mopub.common.Preconditions;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;
import com.jobstrak.drawingfun.lib.mopub.common.util.Reflection;
import java.io.File;
import java.net.SocketException;

public class DeviceUtils {
    private static final int MAX_DISK_CACHE_SIZE = 104857600;
    private static final int MAX_MEMORY_CACHE_SIZE = 31457280;
    private static final int MIN_DISK_CACHE_SIZE = 31457280;

    @Deprecated
    public enum IP {
        IPv4,
        IPv6
    }

    private DeviceUtils() {
    }

    public enum ForceOrientation {
        FORCE_PORTRAIT("portrait"),
        FORCE_LANDSCAPE("landscape"),
        DEVICE_ORIENTATION("device"),
        UNDEFINED("");
        
        @NonNull
        private final String mKey;

        private ForceOrientation(@NonNull String key) {
            this.mKey = key;
        }

        @NonNull
        public static ForceOrientation getForceOrientation(@Nullable String key) {
            for (ForceOrientation orientation : values()) {
                if (orientation.mKey.equalsIgnoreCase(key)) {
                    return orientation;
                }
            }
            return UNDEFINED;
        }
    }

    public static boolean isNetworkAvailable(@Nullable Context context) {
        if (context == null || !isPermissionGranted(context, "android.permission.INTERNET")) {
            return false;
        }
        if (!isPermissionGranted(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return true;
        }
        try {
            return ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo().isConnected();
        } catch (NullPointerException e) {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public static int memoryCacheSizeBytes(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        long memoryClass = (long) activityManager.getMemoryClass();
        try {
            if (Utils.bitMaskContainsFlag(context.getApplicationInfo().flags, ApplicationInfo.class.getDeclaredField("FLAG_LARGE_HEAP").getInt(null))) {
                memoryClass = (long) ((Integer) new Reflection.MethodBuilder(activityManager, "getLargeMemoryClass").execute()).intValue();
            }
        } catch (Exception e) {
            MoPubLog.d("Unable to reflectively determine large heap size.");
        }
        return (int) Math.min(31457280L, (memoryClass / 8) * 1024 * 1024);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public static long diskCacheSizeBytes(File dir, long minSize) {
        long size = minSize;
        try {
            StatFs statFs = new StatFs(dir.getAbsolutePath());
            size = (((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 50;
        } catch (IllegalArgumentException e) {
            MoPubLog.d("Unable to calculate 2% of available disk space, defaulting to minimum");
        }
        return Math.max(Math.min(size, 104857600L), 31457280L);
    }

    public static long diskCacheSizeBytes(File dir) {
        return diskCacheSizeBytes(dir, 31457280);
    }

    public static int getScreenOrientation(@NonNull Activity activity) {
        return getScreenOrientationFromRotationAndOrientation(activity.getWindowManager().getDefaultDisplay().getRotation(), activity.getResources().getConfiguration().orientation);
    }

    static int getScreenOrientationFromRotationAndOrientation(int rotation, int orientation) {
        if (1 == orientation) {
            return (rotation == 1 || rotation == 2) ? 9 : 1;
        }
        if (2 != orientation) {
            MoPubLog.d("Unknown screen orientation. Defaulting to portrait.");
            return 9;
        } else if (rotation == 2 || rotation == 3) {
            return 8;
        } else {
            return 0;
        }
    }

    public static void lockOrientation(@NonNull Activity activity, @NonNull CreativeOrientation creativeOrientation) {
        int requestedOrientation;
        if (Preconditions.NoThrow.checkNotNull(creativeOrientation) && Preconditions.NoThrow.checkNotNull(activity)) {
            int currentOrientation = getScreenOrientationFromRotationAndOrientation(((WindowManager) activity.getSystemService("window")).getDefaultDisplay().getRotation(), activity.getResources().getConfiguration().orientation);
            if (CreativeOrientation.PORTRAIT == creativeOrientation) {
                if (9 == currentOrientation) {
                    requestedOrientation = 9;
                } else {
                    requestedOrientation = 1;
                }
            } else if (CreativeOrientation.LANDSCAPE != creativeOrientation) {
                return;
            } else {
                if (8 == currentOrientation) {
                    requestedOrientation = 8;
                } else {
                    requestedOrientation = 0;
                }
            }
            activity.setRequestedOrientation(requestedOrientation);
        }
    }

    @TargetApi(17)
    public static Point getDeviceDimensions(@NonNull Context context) {
        Integer bestWidthPixels = null;
        Integer bestHeightPixels = null;
        Display display = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= 17) {
            Point screenSize = new Point();
            display.getRealSize(screenSize);
            bestWidthPixels = Integer.valueOf(screenSize.x);
            bestHeightPixels = Integer.valueOf(screenSize.y);
        } else {
            try {
                bestWidthPixels = (Integer) new Reflection.MethodBuilder(display, "getRawWidth").execute();
                bestHeightPixels = (Integer) new Reflection.MethodBuilder(display, "getRawHeight").execute();
            } catch (Exception e) {
                MoPubLog.v("Display#getRawWidth/Height failed.", e);
            }
        }
        if (bestWidthPixels == null || bestHeightPixels == null) {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            bestWidthPixels = Integer.valueOf(displayMetrics.widthPixels);
            bestHeightPixels = Integer.valueOf(displayMetrics.heightPixels);
        }
        return new Point(bestWidthPixels.intValue(), bestHeightPixels.intValue());
    }

    public static boolean isPermissionGranted(@NonNull Context context, @NonNull String permission) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(permission);
        return context.checkPermission(permission, Process.myPid(), Process.myUid()) == 0;
    }

    @Deprecated
    @Nullable
    public static String getIpAddress(IP ip) throws SocketException {
        return null;
    }

    @Deprecated
    @Nullable
    public static String getHashedUdid(Context context) {
        return null;
    }
}
