package com.jobstrak.drawingfun.lib.processes.models;

import java.io.IOException;

public final class Statm extends ProcFile {
    public final String[] fields = this.content.split("\\s+");

    public static Statm get(int pid) throws IOException {
        return new Statm(String.format("/proc/%d/statm", Integer.valueOf(pid)));
    }

    private Statm(String path) throws IOException {
        super(path);
    }

    public long getSize() {
        return Long.parseLong(this.fields[0]) * 1024;
    }

    public long getResidentSetSize() {
        return Long.parseLong(this.fields[1]) * 1024;
    }
}
