package com.jobstrak.drawingfun.lib.gson.internal.bind;

import com.jobstrak.drawingfun.lib.gson.TypeAdapterFactory;
import com.jobstrak.drawingfun.lib.gson.internal.ConstructorConstructor;

public final class JsonAdapterAnnotationTypeAdapterFactory implements TypeAdapterFactory {
    private final ConstructorConstructor constructorConstructor;

    public JsonAdapterAnnotationTypeAdapterFactory(ConstructorConstructor constructorConstructor2) {
        this.constructorConstructor = constructorConstructor2;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [com.jobstrak.drawingfun.lib.gson.reflect.TypeToken<T>, com.jobstrak.drawingfun.lib.gson.reflect.TypeToken] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> com.jobstrak.drawingfun.lib.gson.TypeAdapter<T> create(com.jobstrak.drawingfun.lib.gson.Gson r3, com.jobstrak.drawingfun.lib.gson.reflect.TypeToken<T> r4) {
        /*
            r2 = this;
            java.lang.Class r0 = r4.getRawType()
            java.lang.Class<com.jobstrak.drawingfun.lib.gson.annotations.JsonAdapter> r1 = com.jobstrak.drawingfun.lib.gson.annotations.JsonAdapter.class
            java.lang.annotation.Annotation r0 = r0.getAnnotation(r1)
            com.jobstrak.drawingfun.lib.gson.annotations.JsonAdapter r0 = (com.jobstrak.drawingfun.lib.gson.annotations.JsonAdapter) r0
            if (r0 != 0) goto L_0x0010
            r1 = 0
            return r1
        L_0x0010:
            com.jobstrak.drawingfun.lib.gson.internal.ConstructorConstructor r1 = r2.constructorConstructor
            com.jobstrak.drawingfun.lib.gson.TypeAdapter r1 = getTypeAdapter(r1, r3, r4, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.gson.internal.bind.JsonAdapterAnnotationTypeAdapterFactory.create(com.jobstrak.drawingfun.lib.gson.Gson, com.jobstrak.drawingfun.lib.gson.reflect.TypeToken):com.jobstrak.drawingfun.lib.gson.TypeAdapter");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: com.jobstrak.drawingfun.lib.gson.TypeAdapter} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.jobstrak.drawingfun.lib.gson.TypeAdapter<?> getTypeAdapter(com.jobstrak.drawingfun.lib.gson.internal.ConstructorConstructor r3, com.jobstrak.drawingfun.lib.gson.Gson r4, com.jobstrak.drawingfun.lib.gson.reflect.TypeToken<?> r5, com.jobstrak.drawingfun.lib.gson.annotations.JsonAdapter r6) {
        /*
            java.lang.Class r0 = r6.value()
            java.lang.Class<com.jobstrak.drawingfun.lib.gson.TypeAdapter> r1 = com.jobstrak.drawingfun.lib.gson.TypeAdapter.class
            boolean r1 = r1.isAssignableFrom(r0)
            if (r1 == 0) goto L_0x001d
            r1 = r0
            com.jobstrak.drawingfun.lib.gson.reflect.TypeToken r2 = com.jobstrak.drawingfun.lib.gson.reflect.TypeToken.get(r1)
            com.jobstrak.drawingfun.lib.gson.internal.ObjectConstructor r2 = r3.get(r2)
            java.lang.Object r2 = r2.construct()
            r1 = r2
            com.jobstrak.drawingfun.lib.gson.TypeAdapter r1 = (com.jobstrak.drawingfun.lib.gson.TypeAdapter) r1
            goto L_0x0039
        L_0x001d:
            java.lang.Class<com.jobstrak.drawingfun.lib.gson.TypeAdapterFactory> r1 = com.jobstrak.drawingfun.lib.gson.TypeAdapterFactory.class
            boolean r1 = r1.isAssignableFrom(r0)
            if (r1 == 0) goto L_0x003e
            r1 = r0
            com.jobstrak.drawingfun.lib.gson.reflect.TypeToken r2 = com.jobstrak.drawingfun.lib.gson.reflect.TypeToken.get(r1)
            com.jobstrak.drawingfun.lib.gson.internal.ObjectConstructor r2 = r3.get(r2)
            java.lang.Object r2 = r2.construct()
            com.jobstrak.drawingfun.lib.gson.TypeAdapterFactory r2 = (com.jobstrak.drawingfun.lib.gson.TypeAdapterFactory) r2
            com.jobstrak.drawingfun.lib.gson.TypeAdapter r1 = r2.create(r4, r5)
        L_0x0039:
            com.jobstrak.drawingfun.lib.gson.TypeAdapter r2 = r1.nullSafe()
            return r2
        L_0x003e:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "@JsonAdapter value must be TypeAdapter or TypeAdapterFactory reference."
            r1.<init>(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.gson.internal.bind.JsonAdapterAnnotationTypeAdapterFactory.getTypeAdapter(com.jobstrak.drawingfun.lib.gson.internal.ConstructorConstructor, com.jobstrak.drawingfun.lib.gson.Gson, com.jobstrak.drawingfun.lib.gson.reflect.TypeToken, com.jobstrak.drawingfun.lib.gson.annotations.JsonAdapter):com.jobstrak.drawingfun.lib.gson.TypeAdapter");
    }
}
