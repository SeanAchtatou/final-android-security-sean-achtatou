package com.jobstrak.drawingfun.lib.mopub.common;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.MoPub;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;
import com.jobstrak.drawingfun.lib.mopub.common.util.DeviceUtils;
import java.math.BigDecimal;

public class LocationService {
    private static volatile LocationService sInstance;
    @VisibleForTesting
    @Nullable
    Location mLastKnownLocation;
    @VisibleForTesting
    long mLocationLastUpdatedMillis;

    public enum LocationAwareness {
        NORMAL,
        TRUNCATED,
        DISABLED;

        @Deprecated
        public MoPub.LocationAwareness getNewLocationAwareness() {
            if (this == TRUNCATED) {
                return MoPub.LocationAwareness.TRUNCATED;
            }
            if (this == DISABLED) {
                return MoPub.LocationAwareness.DISABLED;
            }
            return MoPub.LocationAwareness.NORMAL;
        }

        @Deprecated
        public static LocationAwareness fromMoPubLocationAwareness(MoPub.LocationAwareness awareness) {
            if (awareness == MoPub.LocationAwareness.DISABLED) {
                return DISABLED;
            }
            if (awareness == MoPub.LocationAwareness.TRUNCATED) {
                return TRUNCATED;
            }
            return NORMAL;
        }
    }

    private LocationService() {
    }

    @NonNull
    @VisibleForTesting
    static LocationService getInstance() {
        LocationService locationService = sInstance;
        if (locationService == null) {
            synchronized (LocationService.class) {
                locationService = sInstance;
                if (locationService == null) {
                    locationService = new LocationService();
                    sInstance = locationService;
                }
            }
        }
        return locationService;
    }

    public enum ValidLocationProvider {
        NETWORK("network"),
        GPS("gps");
        
        @NonNull
        final String name;

        private ValidLocationProvider(@NonNull String name2) {
            this.name = name2;
        }

        public String toString() {
            return this.name;
        }

        private boolean hasRequiredPermissions(@NonNull Context context) {
            int i = AnonymousClass1.$SwitchMap$com$jobstrak$drawingfun$lib$mopub$common$LocationService$ValidLocationProvider[ordinal()];
            if (i != 1) {
                if (i != 2) {
                    return false;
                }
                return DeviceUtils.isPermissionGranted(context, "android.permission.ACCESS_FINE_LOCATION");
            } else if (DeviceUtils.isPermissionGranted(context, "android.permission.ACCESS_FINE_LOCATION") || DeviceUtils.isPermissionGranted(context, "android.permission.ACCESS_COARSE_LOCATION")) {
                return true;
            } else {
                return false;
            }
        }
    }

    /* renamed from: com.jobstrak.drawingfun.lib.mopub.common.LocationService$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$jobstrak$drawingfun$lib$mopub$common$LocationService$ValidLocationProvider = new int[ValidLocationProvider.values().length];

        static {
            try {
                $SwitchMap$com$jobstrak$drawingfun$lib$mopub$common$LocationService$ValidLocationProvider[ValidLocationProvider.NETWORK.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$jobstrak$drawingfun$lib$mopub$common$LocationService$ValidLocationProvider[ValidLocationProvider.GPS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    @Nullable
    public static Location getLastKnownLocation(@NonNull Context context, int locationPrecision, @NonNull MoPub.LocationAwareness locationAwareness) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(locationAwareness);
        if (locationAwareness == MoPub.LocationAwareness.DISABLED) {
            return null;
        }
        LocationService locationService = getInstance();
        if (isLocationFreshEnough()) {
            return locationService.mLastKnownLocation;
        }
        Location result = getMostRecentValidLocation(getLocationFromProvider(context, ValidLocationProvider.GPS), getLocationFromProvider(context, ValidLocationProvider.NETWORK));
        if (locationAwareness == MoPub.LocationAwareness.TRUNCATED) {
            truncateLocationLatLon(result, locationPrecision);
        }
        locationService.mLastKnownLocation = result;
        locationService.mLocationLastUpdatedMillis = SystemClock.elapsedRealtime();
        return result;
    }

    @VisibleForTesting
    @Nullable
    static Location getLocationFromProvider(@NonNull Context context, @NonNull ValidLocationProvider provider) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(provider);
        if (!ValidLocationProvider.access$000(provider, context)) {
            return null;
        }
        try {
            return ((LocationManager) context.getSystemService("location")).getLastKnownLocation(provider.toString());
        } catch (SecurityException e) {
            MoPubLog.d("Failed to retrieve location from " + provider.toString() + " provider: access appears to be disabled.");
            return null;
        } catch (IllegalArgumentException e2) {
            MoPubLog.d("Failed to retrieve location: device has no " + provider.toString() + " location provider.");
            return null;
        } catch (NullPointerException e3) {
            MoPubLog.d("Failed to retrieve location: device has no " + provider.toString() + " location provider.");
            return null;
        }
    }

    @VisibleForTesting
    @Nullable
    static Location getMostRecentValidLocation(@Nullable Location a, @Nullable Location b) {
        if (a == null) {
            return b;
        }
        if (b == null) {
            return a;
        }
        return a.getTime() > b.getTime() ? a : b;
    }

    @VisibleForTesting
    static void truncateLocationLatLon(@Nullable Location location, int precision) {
        if (location != null && precision >= 0) {
            location.setLatitude(BigDecimal.valueOf(location.getLatitude()).setScale(precision, 5).doubleValue());
            location.setLongitude(BigDecimal.valueOf(location.getLongitude()).setScale(precision, 5).doubleValue());
        }
    }

    private static boolean isLocationFreshEnough() {
        LocationService locationService = getInstance();
        if (locationService.mLastKnownLocation != null && SystemClock.elapsedRealtime() - locationService.mLocationLastUpdatedMillis <= MoPub.getMinimumLocationRefreshTimeMillis()) {
            return true;
        }
        return false;
    }

    @VisibleForTesting
    @Deprecated
    public static void clearLastKnownLocation() {
        getInstance().mLastKnownLocation = null;
    }
}
