package com.jobstrak.drawingfun.lib.mopub.mraid;

import android.net.Uri;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.mopub.common.VisibleForTesting;
import com.jobstrak.drawingfun.lib.mopub.mobileads.resource.MraidJavascript;
import java.io.ByteArrayInputStream;
import java.util.Locale;

public class MraidWebViewClient extends WebViewClient {
    private static final String MRAID_INJECTION_JAVASCRIPT = ("javascript:" + MraidJavascript.JAVASCRIPT_SOURCE);
    private static final String MRAID_JS = "mraid.js";

    public WebResourceResponse shouldInterceptRequest(@NonNull WebView view, @NonNull String url) {
        if (matchesInjectionUrl(url)) {
            return createMraidInjectionResponse();
        }
        return super.shouldInterceptRequest(view, url);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean matchesInjectionUrl(@NonNull String url) {
        return MRAID_JS.equals(Uri.parse(url.toLowerCase(Locale.US)).getLastPathSegment());
    }

    private WebResourceResponse createMraidInjectionResponse() {
        return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(MRAID_INJECTION_JAVASCRIPT.getBytes()));
    }
}
