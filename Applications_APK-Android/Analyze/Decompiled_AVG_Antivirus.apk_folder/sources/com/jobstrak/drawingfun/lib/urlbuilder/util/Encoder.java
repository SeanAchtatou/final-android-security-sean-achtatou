package com.jobstrak.drawingfun.lib.urlbuilder.util;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import kotlin.text.Typography;

public class Encoder {
    protected static final boolean IS_FRAGMENT = true;
    protected static final boolean IS_NOT_FRAGMENT = false;
    protected static final boolean IS_NOT_PATH = false;
    protected static final boolean IS_NOT_USERINFO = false;
    protected static final boolean IS_PATH = true;
    protected static final boolean IS_USERINFO = true;
    protected final Charset outputEncoding;

    public Encoder(Charset outputEncoding2) {
        this.outputEncoding = outputEncoding2;
    }

    public String encodeUserInfo(String input) {
        if (input == null || input.isEmpty()) {
            return "";
        }
        return urlEncode(input, false, false, true);
    }

    public String encodePath(String input) {
        if (input == null || input.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        StringTokenizer st = new StringTokenizer(input, "/", true);
        while (st.hasMoreElements()) {
            String element = st.nextToken();
            if ("/".equals(element)) {
                sb.append(element);
            } else if (!element.isEmpty()) {
                sb.append(urlEncode(element, true, false, false));
            }
        }
        return sb.toString();
    }

    public String encodeQueryParameters(UrlParameterMultimap queryParametersMultimap) {
        if (queryParametersMultimap != null) {
            StringBuilder sb = new StringBuilder();
            for (Map.Entry<String, String> e : queryParametersMultimap.flatEntryList()) {
                sb.append(encodeQueryElement((String) e.getKey()));
                if (e.getValue() != null) {
                    sb.append('=');
                    sb.append(encodeQueryElement((String) e.getValue()));
                }
                sb.append((char) Typography.amp);
            }
            sb.deleteCharAt(sb.length() - 1);
            return sb.toString();
        }
        throw new IllegalArgumentException("queryParametersMultimap is required to not be null.");
    }

    /* access modifiers changed from: protected */
    public String encodeQueryElement(String input) {
        return urlEncode(input, false, false, false);
    }

    public String encodeFragment(String input) {
        if (input == null || input.isEmpty()) {
            return input;
        }
        return urlEncode(input, false, true, false);
    }

    /* access modifiers changed from: protected */
    public String urlEncode(String input, boolean isPath, boolean isFragment, boolean isUserInfo) {
        CharBuffer cb;
        StringBuilder sb = new StringBuilder();
        char[] inputChars = input.toCharArray();
        for (int i = 0; i < Character.codePointCount(inputChars, 0, inputChars.length); i++) {
            int codePoint = Character.codePointAt(inputChars, i);
            if (Character.isBmpCodePoint(codePoint)) {
                char c = Character.toChars(codePoint)[0];
                if ((!isPath || !Rfc3986Util.isPChar(c) || c == '+') && ((!isFragment || !Rfc3986Util.isFragmentSafe(c)) && ((!isUserInfo || c != ':') && !Rfc3986Util.isUnreserved(c)))) {
                    cb = CharBuffer.allocate(1);
                    cb.append(c);
                } else {
                    sb.append(c);
                }
            } else {
                cb = CharBuffer.allocate(2);
                cb.append(Character.highSurrogate(codePoint));
                cb.append(Character.lowSurrogate(codePoint));
            }
            cb.rewind();
            ByteBuffer bb = this.outputEncoding.encode(cb);
            for (int j = 0; j < bb.limit(); j++) {
                sb.append('%');
                sb.append(String.format(Locale.US, "%1$02X", Byte.valueOf(bb.get(j))));
            }
        }
        return sb.toString();
    }
}
