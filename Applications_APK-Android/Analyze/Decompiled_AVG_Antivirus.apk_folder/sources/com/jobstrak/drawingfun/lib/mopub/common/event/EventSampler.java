package com.jobstrak.drawingfun.lib.mopub.common.event;

import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.mopub.common.Preconditions;
import com.jobstrak.drawingfun.lib.mopub.common.VisibleForTesting;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class EventSampler {
    private static final int CAPACITY = 135;
    private static final float LOAD_FACTOR = 0.75f;
    @VisibleForTesting
    static final int MAX_SIZE = 100;
    @NonNull
    private Random mRandom;
    @NonNull
    private LinkedHashMap<String, Boolean> mSampleDecisionsCache;

    public EventSampler() {
        this(new Random());
    }

    @VisibleForTesting
    public EventSampler(@NonNull Random random) {
        this.mRandom = random;
        this.mSampleDecisionsCache = new LinkedHashMap<String, Boolean>(CAPACITY, LOAD_FACTOR, true) {
            /* access modifiers changed from: protected */
            public boolean removeEldestEntry(Map.Entry<String, Boolean> entry) {
                return size() > 100;
            }
        };
    }

    /* access modifiers changed from: package-private */
    public boolean sample(@NonNull BaseEvent baseEvent) {
        Preconditions.checkNotNull(baseEvent);
        String requestId = baseEvent.getRequestId();
        boolean newSample = true;
        if (requestId != null) {
            Boolean existingSample = this.mSampleDecisionsCache.get(requestId);
            if (existingSample != null) {
                return existingSample.booleanValue();
            }
            if (this.mRandom.nextDouble() >= baseEvent.getSamplingRate()) {
                newSample = false;
            }
            this.mSampleDecisionsCache.put(requestId, Boolean.valueOf(newSample));
            return newSample;
        } else if (this.mRandom.nextDouble() < baseEvent.getSamplingRate()) {
            return true;
        } else {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public int getCacheSize() {
        return this.mSampleDecisionsCache.size();
    }
}
