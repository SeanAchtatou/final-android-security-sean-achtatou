package com.jobstrak.drawingfun.lib.gson.internal.bind;

import com.jobstrak.drawingfun.lib.gson.Gson;
import com.jobstrak.drawingfun.lib.gson.JsonSyntaxException;
import com.jobstrak.drawingfun.lib.gson.TypeAdapter;
import com.jobstrak.drawingfun.lib.gson.TypeAdapterFactory;
import com.jobstrak.drawingfun.lib.gson.internal.bind.util.ISO8601Utils;
import com.jobstrak.drawingfun.lib.gson.reflect.TypeToken;
import com.jobstrak.drawingfun.lib.gson.stream.JsonReader;
import com.jobstrak.drawingfun.lib.gson.stream.JsonToken;
import com.jobstrak.drawingfun.lib.gson.stream.JsonWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;
import java.util.Locale;

public final class DateTypeAdapter extends TypeAdapter<Date> {
    public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            if (typeToken.getRawType() == Date.class) {
                return new DateTypeAdapter();
            }
            return null;
        }
    };
    private final DateFormat enUsFormat = DateFormat.getDateTimeInstance(2, 2, Locale.US);
    private final DateFormat localFormat = DateFormat.getDateTimeInstance(2, 2);

    public Date read(JsonReader in) throws IOException {
        if (in.peek() != JsonToken.NULL) {
            return deserializeToDate(in.nextString());
        }
        in.nextNull();
        return null;
    }

    private synchronized Date deserializeToDate(String json) {
        try {
        } catch (ParseException e) {
            try {
                return this.enUsFormat.parse(json);
            } catch (ParseException e2) {
                try {
                    return ISO8601Utils.parse(json, new ParsePosition(0));
                } catch (ParseException e3) {
                    throw new JsonSyntaxException(json, e3);
                }
            }
        }
        return this.localFormat.parse(json);
    }

    public synchronized void write(JsonWriter out, Date value) throws IOException {
        if (value == null) {
            out.nullValue();
        } else {
            out.value(this.enUsFormat.format(value));
        }
    }
}
