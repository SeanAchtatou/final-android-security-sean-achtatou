package com.jobstrak.drawingfun.lib.homewatcher;

public interface OnHomePressedListener {
    void onHomePressed();

    void onRecentAppsPressed();
}
