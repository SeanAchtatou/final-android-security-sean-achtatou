package com.jobstrak.drawingfun.lib.mopub.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddressUtils {
    @NonNull
    public static InetAddress getInetAddressByName(@Nullable String host) throws UnknownHostException {
        return InetAddress.getByName(host);
    }

    private InetAddressUtils() {
    }
}
