package com.jobstrak.drawingfun.lib.mopub.common;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.UrlResolutionTask;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;
import java.util.EnumSet;

public class UrlHandler {
    /* access modifiers changed from: private */
    public static final ResultActions EMPTY_CLICK_LISTENER = new ResultActions() {
        public void urlHandlingSucceeded(@NonNull String url, @NonNull UrlAction urlAction) {
        }

        public void urlHandlingFailed(@NonNull String url, @NonNull UrlAction lastFailedUrlAction) {
        }
    };
    /* access modifiers changed from: private */
    public static final MoPubSchemeListener EMPTY_MOPUB_SCHEME_LISTENER = new MoPubSchemeListener() {
        public void onFinishLoad() {
        }

        public void onClose() {
        }

        public void onFailLoad() {
        }
    };
    private boolean mAlreadySucceeded;
    @Nullable
    private String mDspCreativeId;
    @NonNull
    private MoPubSchemeListener mMoPubSchemeListener;
    @NonNull
    private ResultActions mResultActions;
    private boolean mSkipShowMoPubBrowser;
    @NonNull
    private EnumSet<UrlAction> mSupportedUrlActions;
    /* access modifiers changed from: private */
    public boolean mTaskPending;

    public interface MoPubSchemeListener {
        void onClose();

        void onFailLoad();

        void onFinishLoad();
    }

    public interface ResultActions {
        void urlHandlingFailed(@NonNull String str, @NonNull UrlAction urlAction);

        void urlHandlingSucceeded(@NonNull String str, @NonNull UrlAction urlAction);
    }

    public static class Builder {
        @Nullable
        private String creativeId;
        @NonNull
        private MoPubSchemeListener moPubSchemeListener = UrlHandler.EMPTY_MOPUB_SCHEME_LISTENER;
        @NonNull
        private ResultActions resultActions = UrlHandler.EMPTY_CLICK_LISTENER;
        private boolean skipShowMoPubBrowser = false;
        @NonNull
        private EnumSet<UrlAction> supportedUrlActions = EnumSet.of(UrlAction.NOOP);

        public Builder withSupportedUrlActions(@NonNull UrlAction first, @Nullable UrlAction... others) {
            this.supportedUrlActions = EnumSet.of(first, others);
            return this;
        }

        public Builder withSupportedUrlActions(@NonNull EnumSet<UrlAction> supportedUrlActions2) {
            this.supportedUrlActions = EnumSet.copyOf((EnumSet) supportedUrlActions2);
            return this;
        }

        public Builder withResultActions(@NonNull ResultActions resultActions2) {
            this.resultActions = resultActions2;
            return this;
        }

        public Builder withMoPubSchemeListener(@NonNull MoPubSchemeListener moPubSchemeListener2) {
            this.moPubSchemeListener = moPubSchemeListener2;
            return this;
        }

        public Builder withoutMoPubBrowser() {
            this.skipShowMoPubBrowser = true;
            return this;
        }

        public Builder withDspCreativeId(@Nullable String creativeId2) {
            this.creativeId = creativeId2;
            return this;
        }

        public UrlHandler build() {
            return new UrlHandler(this.supportedUrlActions, this.resultActions, this.moPubSchemeListener, this.skipShowMoPubBrowser, this.creativeId);
        }
    }

    private UrlHandler(@NonNull EnumSet<UrlAction> supportedUrlActions, @NonNull ResultActions resultActions, @NonNull MoPubSchemeListener moPubSchemeListener, boolean skipShowMoPubBrowser, @Nullable String dspCreativeId) {
        this.mSupportedUrlActions = EnumSet.copyOf((EnumSet) supportedUrlActions);
        this.mResultActions = resultActions;
        this.mMoPubSchemeListener = moPubSchemeListener;
        this.mSkipShowMoPubBrowser = skipShowMoPubBrowser;
        this.mDspCreativeId = dspCreativeId;
        this.mAlreadySucceeded = false;
        this.mTaskPending = false;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public EnumSet<UrlAction> getSupportedUrlActions() {
        return EnumSet.copyOf((EnumSet) this.mSupportedUrlActions);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public ResultActions getResultActions() {
        return this.mResultActions;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public MoPubSchemeListener getMoPubSchemeListener() {
        return this.mMoPubSchemeListener;
    }

    /* access modifiers changed from: package-private */
    public boolean shouldSkipShowMoPubBrowser() {
        return this.mSkipShowMoPubBrowser;
    }

    public void handleUrl(@NonNull Context context, @NonNull String destinationUrl) {
        Preconditions.checkNotNull(context);
        handleUrl(context, destinationUrl, true);
    }

    public void handleUrl(@NonNull Context context, @NonNull String destinationUrl, boolean fromUserInteraction) {
        Preconditions.checkNotNull(context);
        handleUrl(context, destinationUrl, fromUserInteraction, null);
    }

    public void handleUrl(@NonNull Context context, @NonNull String destinationUrl, boolean fromUserInteraction, @Nullable Iterable<String> trackingUrls) {
        Preconditions.checkNotNull(context);
        if (TextUtils.isEmpty(destinationUrl)) {
            failUrlHandling(destinationUrl, null, "Attempted to handle empty url.", null);
            return;
        }
        final Context context2 = context;
        final boolean z = fromUserInteraction;
        final Iterable<String> iterable = trackingUrls;
        final String str = destinationUrl;
        UrlResolutionTask.getResolvedUrl(destinationUrl, new UrlResolutionTask.UrlResolutionListener() {
            public void onSuccess(@NonNull String resolvedUrl) {
                boolean unused = UrlHandler.this.mTaskPending = false;
                UrlHandler.this.handleResolvedUrl(context2, resolvedUrl, z, iterable);
            }

            public void onFailure(@NonNull String message, @Nullable Throwable throwable) {
                boolean unused = UrlHandler.this.mTaskPending = false;
                UrlHandler.this.failUrlHandling(str, null, message, throwable);
            }
        });
        this.mTaskPending = true;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v0, resolved type: com.jobstrak.drawingfun.lib.mopub.common.UrlAction} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean handleResolvedUrl(@androidx.annotation.NonNull android.content.Context r16, @androidx.annotation.NonNull java.lang.String r17, boolean r18, @androidx.annotation.Nullable java.lang.Iterable<java.lang.String> r19) {
        /*
            r15 = this;
            r7 = r15
            r8 = r17
            boolean r0 = android.text.TextUtils.isEmpty(r17)
            r9 = 0
            r10 = 0
            if (r0 == 0) goto L_0x0011
            java.lang.String r0 = "Attempted to handle empty url."
            r15.failUrlHandling(r8, r10, r0, r10)
            return r9
        L_0x0011:
            com.jobstrak.drawingfun.lib.mopub.common.UrlAction r0 = com.jobstrak.drawingfun.lib.mopub.common.UrlAction.NOOP
            android.net.Uri r11 = android.net.Uri.parse(r17)
            java.util.EnumSet<com.jobstrak.drawingfun.lib.mopub.common.UrlAction> r1 = r7.mSupportedUrlActions
            java.util.Iterator r12 = r1.iterator()
            r13 = r0
        L_0x001e:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x0090
            java.lang.Object r0 = r12.next()
            r14 = r0
            com.jobstrak.drawingfun.lib.mopub.common.UrlAction r14 = (com.jobstrak.drawingfun.lib.mopub.common.UrlAction) r14
            boolean r0 = r14.shouldTryHandlingUrl(r11)
            if (r0 == 0) goto L_0x008b
            java.lang.String r6 = r7.mDspCreativeId     // Catch:{ IntentNotResolvableException -> 0x007c }
            r1 = r14
            r2 = r15
            r3 = r16
            r4 = r11
            r5 = r18
            r1.handleUrl(r2, r3, r4, r5, r6)     // Catch:{ IntentNotResolvableException -> 0x007c }
            boolean r0 = r7.mAlreadySucceeded     // Catch:{ IntentNotResolvableException -> 0x007c }
            r1 = 1
            if (r0 != 0) goto L_0x0077
            boolean r0 = r7.mTaskPending     // Catch:{ IntentNotResolvableException -> 0x007c }
            if (r0 != 0) goto L_0x0077
            com.jobstrak.drawingfun.lib.mopub.common.UrlAction r0 = com.jobstrak.drawingfun.lib.mopub.common.UrlAction.IGNORE_ABOUT_SCHEME     // Catch:{ IntentNotResolvableException -> 0x007c }
            boolean r0 = r0.equals(r14)     // Catch:{ IntentNotResolvableException -> 0x007c }
            if (r0 != 0) goto L_0x0072
            com.jobstrak.drawingfun.lib.mopub.common.UrlAction r0 = com.jobstrak.drawingfun.lib.mopub.common.UrlAction.HANDLE_MOPUB_SCHEME     // Catch:{ IntentNotResolvableException -> 0x007c }
            boolean r0 = r0.equals(r14)     // Catch:{ IntentNotResolvableException -> 0x007c }
            if (r0 != 0) goto L_0x006d
            com.jobstrak.drawingfun.lib.mopub.common.event.BaseEvent$Name r0 = com.jobstrak.drawingfun.lib.mopub.common.event.BaseEvent.Name.CLICK_REQUEST     // Catch:{ IntentNotResolvableException -> 0x007c }
            r2 = r16
            r3 = r19
            com.jobstrak.drawingfun.lib.mopub.network.TrackingRequest.makeTrackingHttpRequest(r3, r2, r0)     // Catch:{ IntentNotResolvableException -> 0x006b }
            com.jobstrak.drawingfun.lib.mopub.common.UrlHandler$ResultActions r0 = r7.mResultActions     // Catch:{ IntentNotResolvableException -> 0x006b }
            java.lang.String r4 = r11.toString()     // Catch:{ IntentNotResolvableException -> 0x006b }
            r0.urlHandlingSucceeded(r4, r14)     // Catch:{ IntentNotResolvableException -> 0x006b }
            r7.mAlreadySucceeded = r1     // Catch:{ IntentNotResolvableException -> 0x006b }
            goto L_0x007b
        L_0x006b:
            r0 = move-exception
            goto L_0x0081
        L_0x006d:
            r2 = r16
            r3 = r19
            goto L_0x007b
        L_0x0072:
            r2 = r16
            r3 = r19
            goto L_0x007b
        L_0x0077:
            r2 = r16
            r3 = r19
        L_0x007b:
            return r1
        L_0x007c:
            r0 = move-exception
            r2 = r16
            r3 = r19
        L_0x0081:
            java.lang.String r1 = r0.getMessage()
            com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog.d(r1, r0)
            r1 = r14
            r13 = r1
            goto L_0x008f
        L_0x008b:
            r2 = r16
            r3 = r19
        L_0x008f:
            goto L_0x001e
        L_0x0090:
            r2 = r16
            r3 = r19
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Link ignored. Unable to handle url: "
            r0.append(r1)
            r0.append(r8)
            java.lang.String r0 = r0.toString()
            r15.failUrlHandling(r8, r13, r0, r10)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.mopub.common.UrlHandler.handleResolvedUrl(android.content.Context, java.lang.String, boolean, java.lang.Iterable):boolean");
    }

    /* access modifiers changed from: private */
    public void failUrlHandling(@Nullable String url, @Nullable UrlAction urlAction, @NonNull String message, @Nullable Throwable throwable) {
        Preconditions.checkNotNull(message);
        if (urlAction == null) {
            urlAction = UrlAction.NOOP;
        }
        MoPubLog.d(message, throwable);
        this.mResultActions.urlHandlingFailed(url, urlAction);
    }
}
