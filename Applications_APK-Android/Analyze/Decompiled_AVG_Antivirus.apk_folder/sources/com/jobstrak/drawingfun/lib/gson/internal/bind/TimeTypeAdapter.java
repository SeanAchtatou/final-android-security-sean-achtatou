package com.jobstrak.drawingfun.lib.gson.internal.bind;

import com.jobstrak.drawingfun.lib.gson.Gson;
import com.jobstrak.drawingfun.lib.gson.JsonSyntaxException;
import com.jobstrak.drawingfun.lib.gson.TypeAdapter;
import com.jobstrak.drawingfun.lib.gson.TypeAdapterFactory;
import com.jobstrak.drawingfun.lib.gson.reflect.TypeToken;
import com.jobstrak.drawingfun.lib.gson.stream.JsonReader;
import com.jobstrak.drawingfun.lib.gson.stream.JsonToken;
import com.jobstrak.drawingfun.lib.gson.stream.JsonWriter;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class TimeTypeAdapter extends TypeAdapter<Time> {
    public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            if (typeToken.getRawType() == Time.class) {
                return new TimeTypeAdapter();
            }
            return null;
        }
    };
    private final DateFormat format = new SimpleDateFormat("hh:mm:ss a");

    public synchronized Time read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        }
        try {
            return new Time(this.format.parse(in.nextString()).getTime());
        } catch (ParseException e) {
            throw new JsonSyntaxException(e);
        }
    }

    public synchronized void write(JsonWriter out, Time value) throws IOException {
        out.value(value == null ? null : this.format.format(value));
    }
}
