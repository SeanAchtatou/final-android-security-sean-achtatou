package com.jobstrak.drawingfun.lib.mopub.mraid;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.DataKeys;
import com.jobstrak.drawingfun.lib.mopub.mobileads.CustomEventInterstitial;
import com.jobstrak.drawingfun.lib.mopub.mobileads.MraidActivity;
import com.jobstrak.drawingfun.lib.mopub.mobileads.ResponseBodyInterstitial;
import java.util.Map;

public class MraidInterstitial extends ResponseBodyInterstitial {
    @Nullable
    protected String mHtmlData;

    /* access modifiers changed from: protected */
    public void extractExtras(Map<String, String> serverExtras) {
        this.mHtmlData = serverExtras.get(DataKeys.HTML_RESPONSE_BODY_KEY);
    }

    /* access modifiers changed from: protected */
    public void preRenderHtml(@NonNull CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener) {
        MraidActivity.preRenderHtml(this.mContext, customEventInterstitialListener, this.mHtmlData);
    }

    public void showInterstitial() {
        MraidActivity.start(this.mContext, this.mHtmlData, this.mBroadcastIdentifier);
    }
}
