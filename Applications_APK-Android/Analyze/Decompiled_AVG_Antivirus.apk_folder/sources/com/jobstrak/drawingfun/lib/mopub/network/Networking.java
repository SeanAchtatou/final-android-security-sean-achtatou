package com.jobstrak.drawingfun.lib.mopub.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Looper;
import android.webkit.WebSettings;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.ClientMetadata;
import com.jobstrak.drawingfun.lib.mopub.common.Constants;
import com.jobstrak.drawingfun.lib.mopub.common.LruCache;
import com.jobstrak.drawingfun.lib.mopub.common.Preconditions;
import com.jobstrak.drawingfun.lib.mopub.common.VisibleForTesting;
import com.jobstrak.drawingfun.lib.mopub.common.util.DeviceUtils;
import com.mopub.volley.Network;
import com.mopub.volley.RequestQueue;
import com.mopub.volley.toolbox.BasicNetwork;
import com.mopub.volley.toolbox.DiskBasedCache;
import com.mopub.volley.toolbox.ImageLoader;
import java.io.File;

public class Networking {
    @VisibleForTesting
    static final String CACHE_DIRECTORY_NAME = "mopub-volley-cache";
    private static final String DEFAULT_USER_AGENT = System.getProperty("http.agent");
    private static volatile MaxWidthImageLoader sMaxWidthImageLoader;
    private static volatile MoPubRequestQueue sRequestQueue;
    private static boolean sUseHttps = false;
    private static volatile String sUserAgent;

    @Nullable
    public static MoPubRequestQueue getRequestQueue() {
        return sRequestQueue;
    }

    @NonNull
    public static MoPubRequestQueue getRequestQueue(@NonNull Context context) {
        MoPubRequestQueue requestQueue = sRequestQueue;
        if (requestQueue == null) {
            synchronized (Networking.class) {
                requestQueue = sRequestQueue;
                if (requestQueue == null) {
                    Network network = new BasicNetwork(new RequestQueueHttpStack(getUserAgent(context.getApplicationContext()), new PlayServicesUrlRewriter(ClientMetadata.getInstance(context).getDeviceId(), context), CustomSSLSocketFactory.getDefault(10000)));
                    File volleyCacheDir = new File(context.getCacheDir().getPath() + File.separator + CACHE_DIRECTORY_NAME);
                    requestQueue = new MoPubRequestQueue(new DiskBasedCache(volleyCacheDir, (int) DeviceUtils.diskCacheSizeBytes(volleyCacheDir, 10485760)), network);
                    sRequestQueue = requestQueue;
                    requestQueue.start();
                }
            }
        }
        return requestQueue;
    }

    @NonNull
    public static ImageLoader getImageLoader(@NonNull Context context) {
        MaxWidthImageLoader imageLoader = sMaxWidthImageLoader;
        if (imageLoader == null) {
            synchronized (Networking.class) {
                imageLoader = sMaxWidthImageLoader;
                if (imageLoader == null) {
                    RequestQueue queue = getRequestQueue(context);
                    final LruCache<String, Bitmap> imageCache = new LruCache<String, Bitmap>(DeviceUtils.memoryCacheSizeBytes(context)) {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.jobstrak.drawingfun.lib.mopub.common.LruCache.sizeOf(java.lang.Object, java.lang.Object):int
                         arg types: [java.lang.String, android.graphics.Bitmap]
                         candidates:
                          com.jobstrak.drawingfun.lib.mopub.network.Networking.1.sizeOf(java.lang.String, android.graphics.Bitmap):int
                          com.jobstrak.drawingfun.lib.mopub.common.LruCache.sizeOf(java.lang.Object, java.lang.Object):int */
                        /* access modifiers changed from: protected */
                        public int sizeOf(String key, Bitmap value) {
                            if (value != null) {
                                return value.getRowBytes() * value.getHeight();
                            }
                            return super.sizeOf((Object) key, (Object) value);
                        }
                    };
                    imageLoader = new MaxWidthImageLoader(queue, context, new ImageLoader.ImageCache() {
                        public Bitmap getBitmap(String key) {
                            return (Bitmap) imageCache.get(key);
                        }

                        public void putBitmap(String key, Bitmap bitmap) {
                            imageCache.put(key, bitmap);
                        }
                    });
                    sMaxWidthImageLoader = imageLoader;
                }
            }
        }
        return imageLoader;
    }

    @NonNull
    public static String getUserAgent(@NonNull Context context) {
        Preconditions.checkNotNull(context);
        String userAgent = sUserAgent;
        if (userAgent == null) {
            synchronized (Networking.class) {
                userAgent = sUserAgent;
                if (userAgent == null) {
                    if (Build.VERSION.SDK_INT >= 17) {
                        userAgent = WebSettings.getDefaultUserAgent(context);
                    } else if (Looper.myLooper() == Looper.getMainLooper()) {
                        try {
                            userAgent = new WebView(context).getSettings().getUserAgentString();
                        } catch (Exception e) {
                            userAgent = DEFAULT_USER_AGENT;
                        }
                    } else {
                        userAgent = DEFAULT_USER_AGENT;
                    }
                    sUserAgent = userAgent;
                }
            }
        }
        return userAgent;
    }

    @NonNull
    public static String getCachedUserAgent() {
        String userAgent = sUserAgent;
        if (userAgent == null) {
            return DEFAULT_USER_AGENT;
        }
        return userAgent;
    }

    @VisibleForTesting
    public static synchronized void clearForTesting() {
        synchronized (Networking.class) {
            sRequestQueue = null;
            sMaxWidthImageLoader = null;
            sUserAgent = null;
        }
    }

    @VisibleForTesting
    public static synchronized void setRequestQueueForTesting(MoPubRequestQueue queue) {
        synchronized (Networking.class) {
            sRequestQueue = queue;
        }
    }

    @VisibleForTesting
    public static synchronized void setImageLoaderForTesting(MaxWidthImageLoader imageLoader) {
        synchronized (Networking.class) {
            sMaxWidthImageLoader = imageLoader;
        }
    }

    @VisibleForTesting
    public static synchronized void setUserAgentForTesting(String userAgent) {
        synchronized (Networking.class) {
            sUserAgent = userAgent;
        }
    }

    public static void useHttps(boolean useHttps) {
        sUseHttps = useHttps;
    }

    public static boolean useHttps() {
        return sUseHttps;
    }

    public static String getScheme() {
        return useHttps() ? "https" : Constants.HTTP;
    }

    public static String getBaseUrlScheme() {
        return Constants.HTTP;
    }
}
