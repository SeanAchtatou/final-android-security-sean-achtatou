package com.jobstrak.drawingfun.lib.urlbuilder.util;

import java.util.Arrays;
import kotlin.text.Typography;

final class Rfc3986Util {
    private static final char[] SUB_DELIMITERS = {'!', Typography.dollar, Typography.amp, '\'', '(', ')', '*', '+', ',', ';', '='};

    Rfc3986Util() {
    }

    static {
        Arrays.sort(SUB_DELIMITERS);
    }

    static boolean isFragmentSafe(char c) {
        return isPChar(c) || c == '/' || c == '?';
    }

    static boolean isPChar(char c) {
        return isUnreserved(c) || isSubDelimeter(c) || c == ':' || c == '@';
    }

    static boolean isUnreserved(char c) {
        return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || (('0' <= c && c <= '9') || c == '-' || c == '.' || c == '_' || c == '~');
    }

    private static boolean isSubDelimeter(char c) {
        return Arrays.binarySearch(SUB_DELIMITERS, c) >= 0;
    }
}
