package com.jobstrak.drawingfun.lib.urlbuilder;

import com.jobstrak.drawingfun.lib.urlbuilder.util.Decoder;
import com.jobstrak.drawingfun.lib.urlbuilder.util.Encoder;
import com.jobstrak.drawingfun.lib.urlbuilder.util.RuntimeMalformedURLException;
import com.jobstrak.drawingfun.lib.urlbuilder.util.RuntimeURISyntaxException;
import com.jobstrak.drawingfun.lib.urlbuilder.util.UrlParameterMultimap;
import java.io.IOException;
import java.net.IDN;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class UrlBuilder {
    private static final Pattern AUTHORITY_PATTERN = Pattern.compile("((.*)@)?([^:]*)(:(\\d+))?");
    private static final Charset DEFAULT_ENCODING = Charset.forName("UTF-8");
    private static final Pattern URI_PATTERN = Pattern.compile("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?");
    private final Decoder decoder;
    private final Encoder encoder;
    public final String fragment;
    public final String hostName;
    public final String path;
    public final Integer port;
    public final Map<String, List<String>> queryParameters;
    private final UrlParameterMultimap.Immutable queryParametersMultimap;
    public final String scheme;
    public final String userInfo;

    private UrlBuilder() {
        this(null, null, null, null, null, null, null, null, null);
    }

    private UrlBuilder(Decoder decoder2, Encoder encoder2, String scheme2, String userInfo2, String hostName2, Integer port2, String path2, UrlParameterMultimap queryParametersMultimap2, String fragment2) {
        if (decoder2 == null) {
            this.decoder = new Decoder(DEFAULT_ENCODING);
        } else {
            this.decoder = decoder2;
        }
        if (encoder2 == null) {
            this.encoder = new Encoder(DEFAULT_ENCODING);
        } else {
            this.encoder = encoder2;
        }
        this.scheme = scheme2;
        this.userInfo = userInfo2;
        this.hostName = hostName2;
        this.port = port2;
        this.path = path2;
        if (queryParametersMultimap2 == null) {
            this.queryParametersMultimap = UrlParameterMultimap.newMultimap().immutable();
        } else {
            this.queryParametersMultimap = queryParametersMultimap2.immutable();
        }
        this.queryParameters = this.queryParametersMultimap;
        this.fragment = fragment2;
    }

    public static UrlBuilder empty() {
        return new UrlBuilder();
    }

    protected static UrlBuilder of(Decoder decoder2, Encoder encoder2, String scheme2, String userInfo2, String hostName2, Integer port2, String path2, UrlParameterMultimap queryParameters2, String fragment2) {
        return new UrlBuilder(decoder2, encoder2, scheme2, userInfo2, hostName2, port2, path2, queryParameters2, fragment2);
    }

    public static UrlBuilder fromString(String url) {
        return fromString(url, DEFAULT_ENCODING);
    }

    public static UrlBuilder fromString(String url, String inputEncoding) {
        return fromString(url, Charset.forName(inputEncoding));
    }

    public static UrlBuilder fromString(String url, Charset inputEncoding) {
        return fromString(url, new Decoder(inputEncoding));
    }

    public static UrlBuilder fromString(String url, Decoder decoder2) {
        String fragment2;
        UrlParameterMultimap queryParametersMultimap2;
        String path2;
        Integer port2;
        String hostName2;
        String userInfo2;
        String scheme2;
        String str = url;
        Decoder decoder3 = decoder2;
        if (str == null || url.isEmpty()) {
            return new UrlBuilder();
        }
        Matcher m = URI_PATTERN.matcher(str);
        String userInfo3 = null;
        String hostName3 = null;
        Integer port3 = null;
        if (m.find()) {
            String scheme3 = m.group(2);
            if (m.group(4) != null) {
                Matcher n = AUTHORITY_PATTERN.matcher(m.group(4));
                if (n.find()) {
                    if (n.group(2) != null) {
                        userInfo3 = decoder3.decodeUserInfo(n.group(2));
                    }
                    if (n.group(3) != null) {
                        hostName3 = IDN.toUnicode(n.group(3));
                    }
                    if (n.group(5) != null) {
                        port3 = Integer.valueOf(Integer.parseInt(n.group(5)));
                    }
                }
            }
            String path3 = decoder3.decodePath(m.group(5));
            UrlParameterMultimap queryParametersMultimap3 = decoder3.parseQueryString(m.group(7));
            scheme2 = scheme3;
            userInfo2 = userInfo3;
            hostName2 = hostName3;
            path2 = path3;
            fragment2 = decoder3.decodeFragment(m.group(9));
            port2 = port3;
            queryParametersMultimap2 = queryParametersMultimap3;
        } else {
            scheme2 = null;
            userInfo2 = null;
            hostName2 = null;
            path2 = null;
            fragment2 = null;
            port2 = null;
            queryParametersMultimap2 = UrlParameterMultimap.newMultimap();
        }
        return of(decoder2, new Encoder(DEFAULT_ENCODING), scheme2, userInfo2, hostName2, port2, path2, queryParametersMultimap2, fragment2);
    }

    public static UrlBuilder fromUri(URI uri) {
        Decoder decoder2 = new Decoder(DEFAULT_ENCODING);
        return of(decoder2, new Encoder(DEFAULT_ENCODING), uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort() == -1 ? null : Integer.valueOf(uri.getPort()), decoder2.decodePath(uri.getRawPath()), decoder2.parseQueryString(uri.getRawQuery()), decoder2.decodeFragment(uri.getFragment()));
    }

    public static UrlBuilder fromUrl(URL url) {
        Decoder decoder2 = new Decoder(DEFAULT_ENCODING);
        return of(decoder2, new Encoder(DEFAULT_ENCODING), url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort() == -1 ? null : Integer.valueOf(url.getPort()), decoder2.decodePath(url.getPath()), decoder2.parseQueryString(url.getQuery()), decoder2.decodeFragment(url.getRef()));
    }

    public void toString(Appendable out) throws IOException {
        String str = this.scheme;
        if (str != null) {
            out.append(str);
            out.append(':');
        }
        if (this.hostName != null) {
            out.append("//");
            String str2 = this.userInfo;
            if (str2 != null) {
                out.append(this.encoder.encodeUserInfo(str2));
                out.append('@');
            }
            out.append(IDN.toASCII(this.hostName));
        }
        if (this.port != null) {
            out.append(':');
            out.append(Integer.toString(this.port.intValue()));
        }
        String str3 = this.path;
        if (str3 != null) {
            out.append(this.encoder.encodePath(str3));
        }
        UrlParameterMultimap.Immutable immutable = this.queryParametersMultimap;
        if (immutable != null && !immutable.isEmpty()) {
            out.append('?');
            out.append(this.encoder.encodeQueryParameters(this.queryParametersMultimap));
        }
        if (this.fragment != null) {
            out.append('#');
            out.append(this.encoder.encodeFragment(this.fragment));
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        try {
            toString(sb);
        } catch (IOException e) {
        }
        return sb.toString();
    }

    public URI toUriWithException() throws URISyntaxException {
        return new URI(toString());
    }

    public URI toUri() throws RuntimeURISyntaxException {
        try {
            return toUriWithException();
        } catch (URISyntaxException e) {
            throw new RuntimeURISyntaxException(e);
        }
    }

    public URL toUrlWithException() throws MalformedURLException {
        return new URL(toString());
    }

    public URL toUrl() throws RuntimeMalformedURLException {
        try {
            return toUrlWithException();
        } catch (MalformedURLException e) {
            throw new RuntimeMalformedURLException(e);
        }
    }

    public UrlBuilder withDecoder(Decoder decoder2) {
        return of(decoder2, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, this.path, this.queryParametersMultimap, this.fragment);
    }

    public UrlBuilder withEncoder(Encoder encoder2) {
        return of(this.decoder, encoder2, this.scheme, this.userInfo, this.hostName, this.port, this.path, this.queryParametersMultimap, this.fragment);
    }

    public UrlBuilder encodeAs(Charset charset) {
        return of(this.decoder, new Encoder(charset), this.scheme, this.userInfo, this.hostName, this.port, this.path, this.queryParametersMultimap, this.fragment);
    }

    public UrlBuilder encodeAs(String charsetName) {
        return of(this.decoder, new Encoder(Charset.forName(charsetName)), this.scheme, this.userInfo, this.hostName, this.port, this.path, this.queryParametersMultimap, this.fragment);
    }

    public UrlBuilder withScheme(String scheme2) {
        return of(this.decoder, this.encoder, scheme2, this.userInfo, this.hostName, this.port, this.path, this.queryParametersMultimap, this.fragment);
    }

    public UrlBuilder withUserInfo(String userInfo2) {
        return of(this.decoder, this.encoder, this.scheme, userInfo2, this.hostName, this.port, this.path, this.queryParametersMultimap, this.fragment);
    }

    public UrlBuilder withHost(String name) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, IDN.toUnicode(name), this.port, this.path, this.queryParametersMultimap, this.fragment);
    }

    public UrlBuilder withPort(Integer port2) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, port2, this.path, this.queryParametersMultimap, this.fragment);
    }

    public UrlBuilder withPath(String path2) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, path2, this.queryParametersMultimap, this.fragment);
    }

    public UrlBuilder withPath(String path2, Charset encoding) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, new Decoder(encoding).decodePath(path2), this.queryParametersMultimap, this.fragment);
    }

    public UrlBuilder withPath(String path2, String encoding) {
        return withPath(path2, Charset.forName(encoding));
    }

    public UrlBuilder withQuery(UrlParameterMultimap query) {
        UrlParameterMultimap q;
        if (query == null) {
            q = UrlParameterMultimap.newMultimap();
        } else {
            q = query.deepCopy();
        }
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, this.path, q, this.fragment);
    }

    public UrlBuilder withQuery(String query) {
        Decoder decoder2 = this.decoder;
        return of(decoder2, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, this.path, decoder2.parseQueryString(query), this.fragment);
    }

    public UrlBuilder withQuery(String query, Charset encoding) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, this.path, new Decoder(encoding).parseQueryString(query), this.fragment);
    }

    public UrlBuilder withParameters(UrlParameterMultimap parameters) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, this.path, parameters, this.fragment);
    }

    public UrlBuilder addParameter(String key, String value) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, this.path, this.queryParametersMultimap.deepCopy().add(key, value), this.fragment);
    }

    public UrlBuilder setParameter(String key, String value) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, this.path, this.queryParametersMultimap.deepCopy().replaceValues(key, value), this.fragment);
    }

    public UrlBuilder removeParameter(String key, String value) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, this.path, this.queryParametersMultimap.deepCopy().remove(key, value), this.fragment);
    }

    public UrlBuilder removeParameters(String key) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, this.path, this.queryParametersMultimap.deepCopy().removeAllValues(key), this.fragment);
    }

    public UrlBuilder withFragment(String fragment2) {
        return of(this.decoder, this.encoder, this.scheme, this.userInfo, this.hostName, this.port, this.path, this.queryParametersMultimap, fragment2);
    }
}
