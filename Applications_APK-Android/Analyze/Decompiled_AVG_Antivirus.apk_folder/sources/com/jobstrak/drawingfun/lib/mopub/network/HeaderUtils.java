package com.jobstrak.drawingfun.lib.mopub.network;

import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.util.ResponseHeader;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

public class HeaderUtils {
    @Nullable
    public static String extractHeader(Map<String, String> headers, ResponseHeader responseHeader) {
        return headers.get(responseHeader.getKey());
    }

    public static Integer extractIntegerHeader(Map<String, String> headers, ResponseHeader responseHeader) {
        return formatIntHeader(extractHeader(headers, responseHeader));
    }

    public static boolean extractBooleanHeader(Map<String, String> headers, ResponseHeader responseHeader, boolean defaultValue) {
        return formatBooleanHeader(extractHeader(headers, responseHeader), defaultValue);
    }

    public static Integer extractPercentHeader(Map<String, String> headers, ResponseHeader responseHeader) {
        return formatPercentHeader(extractHeader(headers, responseHeader));
    }

    @Nullable
    public static String extractPercentHeaderString(Map<String, String> headers, ResponseHeader responseHeader) {
        Integer percentHeaderValue = extractPercentHeader(headers, responseHeader);
        if (percentHeaderValue != null) {
            return percentHeaderValue.toString();
        }
        return null;
    }

    public static String extractHeader(HttpResponse response, ResponseHeader responseHeader) {
        Header header = response.getFirstHeader(responseHeader.getKey());
        if (header != null) {
            return header.getValue();
        }
        return null;
    }

    public static boolean extractBooleanHeader(HttpResponse response, ResponseHeader responseHeader, boolean defaultValue) {
        return formatBooleanHeader(extractHeader(response, responseHeader), defaultValue);
    }

    public static Integer extractIntegerHeader(HttpResponse response, ResponseHeader responseHeader) {
        return formatIntHeader(extractHeader(response, responseHeader));
    }

    public static int extractIntHeader(HttpResponse response, ResponseHeader responseHeader, int defaultValue) {
        Integer headerValue = extractIntegerHeader(response, responseHeader);
        if (headerValue == null) {
            return defaultValue;
        }
        return headerValue.intValue();
    }

    private static boolean formatBooleanHeader(@Nullable String headerValue, boolean defaultValue) {
        if (headerValue == null) {
            return defaultValue;
        }
        return headerValue.equals("1");
    }

    private static Integer formatIntHeader(String headerValue) {
        try {
            return Integer.valueOf(Integer.parseInt(headerValue));
        } catch (Exception e) {
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.US);
            numberFormat.setParseIntegerOnly(true);
            try {
                return Integer.valueOf(numberFormat.parse(headerValue.trim()).intValue());
            } catch (Exception e2) {
                return null;
            }
        }
    }

    @Nullable
    private static Integer formatPercentHeader(@Nullable String headerValue) {
        Integer percentValue;
        if (headerValue != null && (percentValue = formatIntHeader(headerValue.replace("%", ""))) != null && percentValue.intValue() >= 0 && percentValue.intValue() <= 100) {
            return percentValue;
        }
        return null;
    }
}
