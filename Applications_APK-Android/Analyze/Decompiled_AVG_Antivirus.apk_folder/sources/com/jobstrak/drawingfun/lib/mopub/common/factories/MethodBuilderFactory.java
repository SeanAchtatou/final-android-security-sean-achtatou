package com.jobstrak.drawingfun.lib.mopub.common.factories;

import com.jobstrak.drawingfun.lib.mopub.common.util.Reflection;

public class MethodBuilderFactory {
    protected static MethodBuilderFactory instance = new MethodBuilderFactory();

    @Deprecated
    public static void setInstance(MethodBuilderFactory factory) {
        instance = factory;
    }

    public static Reflection.MethodBuilder create(Object object, String methodName) {
        return instance.internalCreate(object, methodName);
    }

    /* access modifiers changed from: protected */
    public Reflection.MethodBuilder internalCreate(Object object, String methodName) {
        return new Reflection.MethodBuilder(object, methodName);
    }
}
