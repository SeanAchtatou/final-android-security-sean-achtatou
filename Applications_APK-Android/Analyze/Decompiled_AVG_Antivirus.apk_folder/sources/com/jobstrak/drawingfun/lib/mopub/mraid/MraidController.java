package com.jobstrak.drawingfun.lib.mopub.mraid;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.CloseableLayout;
import com.jobstrak.drawingfun.lib.mopub.common.Preconditions;
import com.jobstrak.drawingfun.lib.mopub.common.UrlAction;
import com.jobstrak.drawingfun.lib.mopub.common.UrlHandler;
import com.jobstrak.drawingfun.lib.mopub.common.VisibleForTesting;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;
import com.jobstrak.drawingfun.lib.mopub.common.util.DeviceUtils;
import com.jobstrak.drawingfun.lib.mopub.common.util.Dips;
import com.jobstrak.drawingfun.lib.mopub.common.util.Utils;
import com.jobstrak.drawingfun.lib.mopub.common.util.Views;
import com.jobstrak.drawingfun.lib.mopub.mobileads.MraidVideoPlayerActivity;
import com.jobstrak.drawingfun.lib.mopub.mobileads.util.WebViews;
import com.jobstrak.drawingfun.lib.mopub.mraid.MraidBridge;
import java.lang.ref.WeakReference;
import java.net.URI;

public class MraidController {
    private boolean mAllowOrientationChange;
    @NonNull
    private final CloseableLayout mCloseableAdContainer;
    /* access modifiers changed from: private */
    @NonNull
    public final Context mContext;
    @Nullable
    private MraidWebViewDebugListener mDebugListener;
    /* access modifiers changed from: private */
    @NonNull
    public final FrameLayout mDefaultAdContainer;
    private MraidOrientation mForceOrientation;
    private boolean mIsPaused;
    /* access modifiers changed from: private */
    @NonNull
    public final MraidBridge mMraidBridge;
    private final MraidBridge.MraidBridgeListener mMraidBridgeListener;
    /* access modifiers changed from: private */
    @Nullable
    public MraidListener mMraidListener;
    /* access modifiers changed from: private */
    public final MraidNativeCommandHandler mMraidNativeCommandHandler;
    @Nullable
    private MraidBridge.MraidWebView mMraidWebView;
    @Nullable
    private UseCustomCloseListener mOnCloseButtonListener;
    @NonNull
    private OrientationBroadcastReceiver mOrientationBroadcastReceiver;
    @Nullable
    private Integer mOriginalActivityOrientation;
    /* access modifiers changed from: private */
    @NonNull
    public final PlacementType mPlacementType;
    @Nullable
    private ViewGroup mRootView;
    /* access modifiers changed from: private */
    @NonNull
    public final MraidScreenMetrics mScreenMetrics;
    @NonNull
    private final ScreenMetricsWaiter mScreenMetricsWaiter;
    /* access modifiers changed from: private */
    @NonNull
    public final MraidBridge mTwoPartBridge;
    private final MraidBridge.MraidBridgeListener mTwoPartBridgeListener;
    @Nullable
    private MraidBridge.MraidWebView mTwoPartWebView;
    /* access modifiers changed from: private */
    @NonNull
    public ViewState mViewState;
    @NonNull
    private final WeakReference<Activity> mWeakActivity;

    public interface MraidListener {
        void onClose();

        void onExpand();

        void onFailedToLoad();

        void onLoaded(View view);

        void onOpen();
    }

    public interface UseCustomCloseListener {
        void useCustomCloseChanged(boolean z);
    }

    public MraidController(@NonNull Context context, @NonNull PlacementType placementType) {
        this(context, placementType, new MraidBridge(placementType), new MraidBridge(PlacementType.INTERSTITIAL), new ScreenMetricsWaiter());
    }

    @VisibleForTesting
    MraidController(@NonNull Context context, @NonNull PlacementType placementType, @NonNull MraidBridge bridge, @NonNull MraidBridge twoPartBridge, @NonNull ScreenMetricsWaiter screenMetricsWaiter) {
        this.mViewState = ViewState.LOADING;
        this.mOrientationBroadcastReceiver = new OrientationBroadcastReceiver();
        this.mAllowOrientationChange = true;
        this.mForceOrientation = MraidOrientation.NONE;
        this.mMraidBridgeListener = new MraidBridge.MraidBridgeListener() {
            public void onPageLoaded() {
                MraidController.this.handlePageLoad();
            }

            public void onPageFailedToLoad() {
                if (MraidController.this.mMraidListener != null) {
                    MraidController.this.mMraidListener.onFailedToLoad();
                }
            }

            public void onVisibilityChanged(boolean isVisible) {
                if (!MraidController.this.mTwoPartBridge.isAttached()) {
                    MraidController.this.mMraidBridge.notifyViewability(isVisible);
                }
            }

            public boolean onJsAlert(@NonNull String message, @NonNull JsResult result) {
                return MraidController.this.handleJsAlert(message, result);
            }

            public boolean onConsoleMessage(@NonNull ConsoleMessage consoleMessage) {
                return MraidController.this.handleConsoleMessage(consoleMessage);
            }

            public void onClose() {
                MraidController.this.handleClose();
            }

            public void onResize(int width, int height, int offsetX, int offsetY, @NonNull CloseableLayout.ClosePosition closePosition, boolean allowOffscreen) throws MraidCommandException {
                MraidController.this.handleResize(width, height, offsetX, offsetY, closePosition, allowOffscreen);
            }

            public void onExpand(@Nullable URI uri, boolean shouldUseCustomClose) throws MraidCommandException {
                MraidController.this.handleExpand(uri, shouldUseCustomClose);
            }

            public void onUseCustomClose(boolean shouldUseCustomClose) {
                MraidController.this.handleCustomClose(shouldUseCustomClose);
            }

            public void onSetOrientationProperties(boolean allowOrientationChange, MraidOrientation forceOrientation) throws MraidCommandException {
                MraidController.this.handleSetOrientationProperties(allowOrientationChange, forceOrientation);
            }

            public void onOpen(@NonNull URI uri) {
                MraidController.this.handleOpen(uri.toString());
            }

            public void onPlayVideo(@NonNull URI uri) {
                MraidController.this.handleShowVideo(uri.toString());
            }
        };
        this.mTwoPartBridgeListener = new MraidBridge.MraidBridgeListener() {
            public void onPageLoaded() {
                MraidController.this.handleTwoPartPageLoad();
            }

            public void onPageFailedToLoad() {
            }

            public void onVisibilityChanged(boolean isVisible) {
                MraidController.this.mMraidBridge.notifyViewability(isVisible);
                MraidController.this.mTwoPartBridge.notifyViewability(isVisible);
            }

            public boolean onJsAlert(@NonNull String message, @NonNull JsResult result) {
                return MraidController.this.handleJsAlert(message, result);
            }

            public boolean onConsoleMessage(@NonNull ConsoleMessage consoleMessage) {
                return MraidController.this.handleConsoleMessage(consoleMessage);
            }

            public void onResize(int width, int height, int offsetX, int offsetY, @NonNull CloseableLayout.ClosePosition closePosition, boolean allowOffscreen) throws MraidCommandException {
                throw new MraidCommandException("Not allowed to resize from an expanded state");
            }

            public void onExpand(@Nullable URI uri, boolean shouldUseCustomClose) {
            }

            public void onClose() {
                MraidController.this.handleClose();
            }

            public void onUseCustomClose(boolean shouldUseCustomClose) {
                MraidController.this.handleCustomClose(shouldUseCustomClose);
            }

            public void onSetOrientationProperties(boolean allowOrientationChange, MraidOrientation forceOrientation) throws MraidCommandException {
                MraidController.this.handleSetOrientationProperties(allowOrientationChange, forceOrientation);
            }

            public void onOpen(URI uri) {
                MraidController.this.handleOpen(uri.toString());
            }

            public void onPlayVideo(@NonNull URI uri) {
                MraidController.this.handleShowVideo(uri.toString());
            }
        };
        this.mContext = context.getApplicationContext();
        Preconditions.checkNotNull(this.mContext);
        if (context instanceof Activity) {
            this.mWeakActivity = new WeakReference<>((Activity) context);
        } else {
            this.mWeakActivity = new WeakReference<>(null);
        }
        this.mPlacementType = placementType;
        this.mMraidBridge = bridge;
        this.mTwoPartBridge = twoPartBridge;
        this.mScreenMetricsWaiter = screenMetricsWaiter;
        this.mViewState = ViewState.LOADING;
        this.mScreenMetrics = new MraidScreenMetrics(this.mContext, this.mContext.getResources().getDisplayMetrics().density);
        this.mDefaultAdContainer = new FrameLayout(this.mContext);
        this.mCloseableAdContainer = new CloseableLayout(this.mContext);
        this.mCloseableAdContainer.setOnCloseListener(new CloseableLayout.OnCloseListener() {
            public void onClose() {
                MraidController.this.handleClose();
            }
        });
        View dimmingView = new View(this.mContext);
        dimmingView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        this.mCloseableAdContainer.addView(dimmingView, new FrameLayout.LayoutParams(-1, -1));
        this.mOrientationBroadcastReceiver.register(this.mContext);
        this.mMraidBridge.setMraidBridgeListener(this.mMraidBridgeListener);
        this.mTwoPartBridge.setMraidBridgeListener(this.mTwoPartBridgeListener);
        this.mMraidNativeCommandHandler = new MraidNativeCommandHandler();
    }

    public void setMraidListener(@Nullable MraidListener mraidListener) {
        this.mMraidListener = mraidListener;
    }

    public void setUseCustomCloseListener(@Nullable UseCustomCloseListener listener) {
        this.mOnCloseButtonListener = listener;
    }

    public void setDebugListener(@Nullable MraidWebViewDebugListener debugListener) {
        this.mDebugListener = debugListener;
    }

    public void loadContent(@NonNull String htmlData) {
        Preconditions.checkState(this.mMraidWebView == null, "loadContent should only be called once");
        this.mMraidWebView = new MraidBridge.MraidWebView(this.mContext);
        this.mMraidBridge.attachView(this.mMraidWebView);
        this.mDefaultAdContainer.addView(this.mMraidWebView, new FrameLayout.LayoutParams(-1, -1));
        this.mMraidBridge.setContentHtml(htmlData);
    }

    /* access modifiers changed from: private */
    public int getDisplayRotation() {
        return ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay().getRotation();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean handleConsoleMessage(@NonNull ConsoleMessage consoleMessage) {
        MraidWebViewDebugListener mraidWebViewDebugListener = this.mDebugListener;
        if (mraidWebViewDebugListener != null) {
            return mraidWebViewDebugListener.onConsoleMessage(consoleMessage);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean handleJsAlert(@NonNull String message, @NonNull JsResult result) {
        MraidWebViewDebugListener mraidWebViewDebugListener = this.mDebugListener;
        if (mraidWebViewDebugListener != null) {
            return mraidWebViewDebugListener.onJsAlert(message, result);
        }
        result.confirm();
        return true;
    }

    @VisibleForTesting
    static class ScreenMetricsWaiter {
        @NonNull
        private final Handler mHandler = new Handler();
        @Nullable
        private WaitRequest mLastWaitRequest;

        ScreenMetricsWaiter() {
        }

        static class WaitRequest {
            @NonNull
            private final Handler mHandler;
            @Nullable
            private Runnable mSuccessRunnable;
            /* access modifiers changed from: private */
            @NonNull
            public final View[] mViews;
            int mWaitCount;
            private final Runnable mWaitingRunnable;

            private WaitRequest(@NonNull Handler handler, @NonNull View[] views) {
                this.mWaitingRunnable = new Runnable() {
                    public void run() {
                        for (final View view : WaitRequest.this.mViews) {
                            if (view.getHeight() > 0 || view.getWidth() > 0) {
                                WaitRequest.this.countDown();
                            } else {
                                view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                                    public boolean onPreDraw() {
                                        view.getViewTreeObserver().removeOnPreDrawListener(this);
                                        WaitRequest.this.countDown();
                                        return true;
                                    }
                                });
                            }
                        }
                    }
                };
                this.mHandler = handler;
                this.mViews = views;
            }

            /* access modifiers changed from: private */
            public void countDown() {
                Runnable runnable;
                this.mWaitCount--;
                if (this.mWaitCount == 0 && (runnable = this.mSuccessRunnable) != null) {
                    runnable.run();
                    this.mSuccessRunnable = null;
                }
            }

            /* access modifiers changed from: package-private */
            public void start(@NonNull Runnable successRunnable) {
                this.mSuccessRunnable = successRunnable;
                this.mWaitCount = this.mViews.length;
                this.mHandler.post(this.mWaitingRunnable);
            }

            /* access modifiers changed from: package-private */
            public void cancel() {
                this.mHandler.removeCallbacks(this.mWaitingRunnable);
                this.mSuccessRunnable = null;
            }
        }

        /* access modifiers changed from: package-private */
        public WaitRequest waitFor(@NonNull View... views) {
            this.mLastWaitRequest = new WaitRequest(this.mHandler, views);
            return this.mLastWaitRequest;
        }

        /* access modifiers changed from: package-private */
        public void cancelLastRequest() {
            WaitRequest waitRequest = this.mLastWaitRequest;
            if (waitRequest != null) {
                waitRequest.cancel();
                this.mLastWaitRequest = null;
            }
        }
    }

    @Nullable
    private View getCurrentWebView() {
        return this.mTwoPartBridge.isAttached() ? this.mTwoPartWebView : this.mMraidWebView;
    }

    /* access modifiers changed from: private */
    public boolean isInlineVideoAvailable() {
        Activity activity = this.mWeakActivity.get();
        if (activity == null || getCurrentWebView() == null) {
            return false;
        }
        return this.mMraidNativeCommandHandler.isInlineVideoAvailable(activity, getCurrentWebView());
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handlePageLoad() {
        setViewState(ViewState.DEFAULT, new Runnable() {
            public void run() {
                MraidController.this.mMraidBridge.notifySupports(MraidController.this.mMraidNativeCommandHandler.isSmsAvailable(MraidController.this.mContext), MraidController.this.mMraidNativeCommandHandler.isTelAvailable(MraidController.this.mContext), MraidNativeCommandHandler.isCalendarAvailable(MraidController.this.mContext), MraidNativeCommandHandler.isStorePictureSupported(MraidController.this.mContext), MraidController.this.isInlineVideoAvailable());
                MraidController.this.mMraidBridge.notifyPlacementType(MraidController.this.mPlacementType);
                MraidController.this.mMraidBridge.notifyViewability(MraidController.this.mMraidBridge.isVisible());
                MraidController.this.mMraidBridge.notifyReady();
            }
        });
        MraidListener mraidListener = this.mMraidListener;
        if (mraidListener != null) {
            mraidListener.onLoaded(this.mDefaultAdContainer);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handleTwoPartPageLoad() {
        updateScreenMetricsAsync(new Runnable() {
            public void run() {
                MraidBridge access$100 = MraidController.this.mTwoPartBridge;
                boolean isSmsAvailable = MraidController.this.mMraidNativeCommandHandler.isSmsAvailable(MraidController.this.mContext);
                boolean isTelAvailable = MraidController.this.mMraidNativeCommandHandler.isTelAvailable(MraidController.this.mContext);
                MraidNativeCommandHandler unused = MraidController.this.mMraidNativeCommandHandler;
                boolean isCalendarAvailable = MraidNativeCommandHandler.isCalendarAvailable(MraidController.this.mContext);
                MraidNativeCommandHandler unused2 = MraidController.this.mMraidNativeCommandHandler;
                access$100.notifySupports(isSmsAvailable, isTelAvailable, isCalendarAvailable, MraidNativeCommandHandler.isStorePictureSupported(MraidController.this.mContext), MraidController.this.isInlineVideoAvailable());
                MraidController.this.mTwoPartBridge.notifyViewState(MraidController.this.mViewState);
                MraidController.this.mTwoPartBridge.notifyPlacementType(MraidController.this.mPlacementType);
                MraidController.this.mTwoPartBridge.notifyViewability(MraidController.this.mTwoPartBridge.isVisible());
                MraidController.this.mTwoPartBridge.notifyReady();
            }
        });
    }

    private void updateScreenMetricsAsync(@Nullable final Runnable successRunnable) {
        this.mScreenMetricsWaiter.cancelLastRequest();
        final View currentWebView = getCurrentWebView();
        if (currentWebView != null) {
            this.mScreenMetricsWaiter.waitFor(this.mDefaultAdContainer, currentWebView).start(new Runnable() {
                public void run() {
                    DisplayMetrics displayMetrics = MraidController.this.mContext.getResources().getDisplayMetrics();
                    MraidController.this.mScreenMetrics.setScreenSize(displayMetrics.widthPixels, displayMetrics.heightPixels);
                    int[] location = new int[2];
                    View rootView = MraidController.this.getRootView();
                    rootView.getLocationOnScreen(location);
                    MraidController.this.mScreenMetrics.setRootViewPosition(location[0], location[1], rootView.getWidth(), rootView.getHeight());
                    MraidController.this.mDefaultAdContainer.getLocationOnScreen(location);
                    MraidController.this.mScreenMetrics.setDefaultAdPosition(location[0], location[1], MraidController.this.mDefaultAdContainer.getWidth(), MraidController.this.mDefaultAdContainer.getHeight());
                    currentWebView.getLocationOnScreen(location);
                    MraidController.this.mScreenMetrics.setCurrentAdPosition(location[0], location[1], currentWebView.getWidth(), currentWebView.getHeight());
                    MraidController.this.mMraidBridge.notifyScreenMetrics(MraidController.this.mScreenMetrics);
                    if (MraidController.this.mTwoPartBridge.isAttached()) {
                        MraidController.this.mTwoPartBridge.notifyScreenMetrics(MraidController.this.mScreenMetrics);
                    }
                    Runnable runnable = successRunnable;
                    if (runnable != null) {
                        runnable.run();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void handleOrientationChange(int currentRotation) {
        updateScreenMetricsAsync(null);
    }

    public void pause(boolean isFinishing) {
        this.mIsPaused = true;
        MraidBridge.MraidWebView mraidWebView = this.mMraidWebView;
        if (mraidWebView != null) {
            WebViews.onPause(mraidWebView, isFinishing);
        }
        MraidBridge.MraidWebView mraidWebView2 = this.mTwoPartWebView;
        if (mraidWebView2 != null) {
            WebViews.onPause(mraidWebView2, isFinishing);
        }
    }

    public void resume() {
        this.mIsPaused = false;
        MraidBridge.MraidWebView mraidWebView = this.mMraidWebView;
        if (mraidWebView != null) {
            mraidWebView.onResume();
        }
        MraidBridge.MraidWebView mraidWebView2 = this.mTwoPartWebView;
        if (mraidWebView2 != null) {
            mraidWebView2.onResume();
        }
    }

    public void destroy() {
        this.mScreenMetricsWaiter.cancelLastRequest();
        try {
            this.mOrientationBroadcastReceiver.unregister();
        } catch (IllegalArgumentException e) {
            if (!e.getMessage().contains("Receiver not registered")) {
                throw e;
            }
        }
        if (!this.mIsPaused) {
            pause(true);
        }
        Views.removeFromParent(this.mCloseableAdContainer);
        this.mMraidBridge.detach();
        MraidBridge.MraidWebView mraidWebView = this.mMraidWebView;
        if (mraidWebView != null) {
            mraidWebView.destroy();
            this.mMraidWebView = null;
        }
        this.mTwoPartBridge.detach();
        MraidBridge.MraidWebView mraidWebView2 = this.mTwoPartWebView;
        if (mraidWebView2 != null) {
            mraidWebView2.destroy();
            this.mTwoPartWebView = null;
        }
    }

    private void setViewState(@NonNull ViewState viewState) {
        setViewState(viewState, null);
    }

    private void setViewState(@NonNull ViewState viewState, @Nullable Runnable successRunnable) {
        MoPubLog.d("MRAID state set to " + viewState);
        ViewState previousViewState = this.mViewState;
        this.mViewState = viewState;
        this.mMraidBridge.notifyViewState(viewState);
        if (this.mTwoPartBridge.isLoaded()) {
            this.mTwoPartBridge.notifyViewState(viewState);
        }
        if (this.mMraidListener != null) {
            if (viewState == ViewState.EXPANDED) {
                this.mMraidListener.onExpand();
            } else if (previousViewState == ViewState.EXPANDED && viewState == ViewState.DEFAULT) {
                this.mMraidListener.onClose();
            } else if (viewState == ViewState.HIDDEN) {
                this.mMraidListener.onClose();
            }
        }
        updateScreenMetricsAsync(successRunnable);
    }

    /* access modifiers changed from: package-private */
    public int clampInt(int min, int target, int max) {
        return Math.max(min, Math.min(target, max));
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handleResize(int widthDips, int heightDips, int offsetXDips, int offsetYDips, @NonNull CloseableLayout.ClosePosition closePosition, boolean allowOffscreen) throws MraidCommandException {
        int height;
        int i = widthDips;
        int i2 = heightDips;
        int i3 = offsetXDips;
        int i4 = offsetYDips;
        CloseableLayout.ClosePosition closePosition2 = closePosition;
        if (this.mMraidWebView == null) {
            throw new MraidCommandException("Unable to resize after the WebView is destroyed");
        } else if (this.mViewState != ViewState.LOADING && this.mViewState != ViewState.HIDDEN) {
            if (this.mViewState == ViewState.EXPANDED) {
                throw new MraidCommandException("Not allowed to resize from an already expanded ad");
            } else if (this.mPlacementType != PlacementType.INTERSTITIAL) {
                int width = Dips.dipsToIntPixels((float) i, this.mContext);
                int height2 = Dips.dipsToIntPixels((float) i2, this.mContext);
                int offsetX = Dips.dipsToIntPixels((float) i3, this.mContext);
                int offsetY = Dips.dipsToIntPixels((float) i4, this.mContext);
                int left = this.mScreenMetrics.getDefaultAdRect().left + offsetX;
                int top = this.mScreenMetrics.getDefaultAdRect().top + offsetY;
                Rect resizeRect = new Rect(left, top, left + width, top + height2);
                if (!allowOffscreen) {
                    Rect bounds = this.mScreenMetrics.getRootViewRect();
                    if (resizeRect.width() > bounds.width() || resizeRect.height() > bounds.height()) {
                        throw new MraidCommandException("resizeProperties specified a size (" + i + ", " + i2 + ") and offset (" + i3 + ", " + i4 + ") that doesn't allow the ad to" + " appear within the max allowed size (" + this.mScreenMetrics.getRootViewRectDips().width() + ", " + this.mScreenMetrics.getRootViewRectDips().height() + ")");
                    }
                    height = height2;
                    resizeRect.offsetTo(clampInt(bounds.left, resizeRect.left, bounds.right - resizeRect.width()), clampInt(bounds.top, resizeRect.top, bounds.bottom - resizeRect.height()));
                } else {
                    height = height2;
                }
                Rect closeRect = new Rect();
                this.mCloseableAdContainer.applyCloseRegionBounds(closePosition2, resizeRect, closeRect);
                if (!this.mScreenMetrics.getRootViewRect().contains(closeRect)) {
                    throw new MraidCommandException("resizeProperties specified a size (" + i + ", " + i2 + ") and offset (" + i3 + ", " + i4 + ") that doesn't allow the close" + " region to appear within the max allowed size (" + this.mScreenMetrics.getRootViewRectDips().width() + ", " + this.mScreenMetrics.getRootViewRectDips().height() + ")");
                } else if (resizeRect.contains(closeRect)) {
                    this.mCloseableAdContainer.setCloseVisible(false);
                    this.mCloseableAdContainer.setClosePosition(closePosition2);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(resizeRect.width(), resizeRect.height());
                    layoutParams.leftMargin = resizeRect.left - this.mScreenMetrics.getRootViewRect().left;
                    layoutParams.topMargin = resizeRect.top - this.mScreenMetrics.getRootViewRect().top;
                    if (this.mViewState == ViewState.DEFAULT) {
                        this.mDefaultAdContainer.removeView(this.mMraidWebView);
                        this.mDefaultAdContainer.setVisibility(4);
                        this.mCloseableAdContainer.addView(this.mMraidWebView, new FrameLayout.LayoutParams(-1, -1));
                        getAndMemoizeRootView().addView(this.mCloseableAdContainer, layoutParams);
                    } else if (this.mViewState == ViewState.RESIZED) {
                        this.mCloseableAdContainer.setLayoutParams(layoutParams);
                    }
                    this.mCloseableAdContainer.setClosePosition(closePosition2);
                    setViewState(ViewState.RESIZED);
                } else {
                    throw new MraidCommandException("resizeProperties specified a size (" + i + ", " + height + ") and offset (" + i3 + ", " + i4 + ") that don't allow the close region to appear " + "within the resized ad.");
                }
            } else {
                throw new MraidCommandException("Not allowed to resize from an interstitial ad");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void handleExpand(@Nullable URI uri, boolean shouldUseCustomClose) throws MraidCommandException {
        if (this.mMraidWebView == null) {
            throw new MraidCommandException("Unable to expand after the WebView is destroyed");
        } else if (this.mPlacementType != PlacementType.INTERSTITIAL) {
            if (this.mViewState == ViewState.DEFAULT || this.mViewState == ViewState.RESIZED) {
                applyOrientation();
                boolean isTwoPart = uri != null;
                if (isTwoPart) {
                    this.mTwoPartWebView = new MraidBridge.MraidWebView(this.mContext);
                    this.mTwoPartBridge.attachView(this.mTwoPartWebView);
                    this.mTwoPartBridge.setContentUrl(uri.toString());
                }
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
                if (this.mViewState == ViewState.DEFAULT) {
                    if (isTwoPart) {
                        this.mCloseableAdContainer.addView(this.mTwoPartWebView, layoutParams);
                    } else {
                        this.mDefaultAdContainer.removeView(this.mMraidWebView);
                        this.mDefaultAdContainer.setVisibility(4);
                        this.mCloseableAdContainer.addView(this.mMraidWebView, layoutParams);
                    }
                    getAndMemoizeRootView().addView(this.mCloseableAdContainer, new FrameLayout.LayoutParams(-1, -1));
                } else if (this.mViewState == ViewState.RESIZED && isTwoPart) {
                    this.mCloseableAdContainer.removeView(this.mMraidWebView);
                    this.mDefaultAdContainer.addView(this.mMraidWebView, layoutParams);
                    this.mDefaultAdContainer.setVisibility(4);
                    this.mCloseableAdContainer.addView(this.mTwoPartWebView, layoutParams);
                }
                this.mCloseableAdContainer.setLayoutParams(layoutParams);
                handleCustomClose(shouldUseCustomClose);
                setViewState(ViewState.EXPANDED);
            }
        }
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public void handleClose() {
        MraidBridge.MraidWebView mraidWebView;
        if (this.mMraidWebView != null && this.mViewState != ViewState.LOADING && this.mViewState != ViewState.HIDDEN) {
            if (this.mViewState == ViewState.EXPANDED || this.mPlacementType == PlacementType.INTERSTITIAL) {
                unApplyOrientation();
            }
            if (this.mViewState == ViewState.RESIZED || this.mViewState == ViewState.EXPANDED) {
                if (!this.mTwoPartBridge.isAttached() || (mraidWebView = this.mTwoPartWebView) == null) {
                    this.mCloseableAdContainer.removeView(this.mMraidWebView);
                    this.mDefaultAdContainer.addView(this.mMraidWebView, new FrameLayout.LayoutParams(-1, -1));
                    this.mDefaultAdContainer.setVisibility(0);
                } else {
                    this.mCloseableAdContainer.removeView(mraidWebView);
                    this.mTwoPartBridge.detach();
                }
                Views.removeFromParent(this.mCloseableAdContainer);
                setViewState(ViewState.DEFAULT);
            } else if (this.mViewState == ViewState.DEFAULT) {
                this.mDefaultAdContainer.setVisibility(4);
                setViewState(ViewState.HIDDEN);
            }
        }
    }

    /* access modifiers changed from: private */
    @NonNull
    public ViewGroup getRootView() {
        ViewGroup viewGroup = this.mRootView;
        if (viewGroup != null) {
            return viewGroup;
        }
        View bestRootView = Views.getTopmostView(this.mWeakActivity.get(), this.mDefaultAdContainer);
        return bestRootView instanceof ViewGroup ? (ViewGroup) bestRootView : this.mDefaultAdContainer;
    }

    @NonNull
    private ViewGroup getAndMemoizeRootView() {
        if (this.mRootView == null) {
            this.mRootView = getRootView();
        }
        return this.mRootView;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handleShowVideo(@NonNull String videoUrl) {
        MraidVideoPlayerActivity.startMraid(this.mContext, videoUrl);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void lockOrientation(int screenOrientation) throws MraidCommandException {
        Activity activity = this.mWeakActivity.get();
        if (activity == null || !shouldAllowForceOrientation(this.mForceOrientation)) {
            throw new MraidCommandException("Attempted to lock orientation to unsupported value: " + this.mForceOrientation.name());
        }
        if (this.mOriginalActivityOrientation == null) {
            this.mOriginalActivityOrientation = Integer.valueOf(activity.getRequestedOrientation());
        }
        activity.setRequestedOrientation(screenOrientation);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void applyOrientation() throws MraidCommandException {
        if (this.mForceOrientation != MraidOrientation.NONE) {
            lockOrientation(this.mForceOrientation.getActivityInfoOrientation());
        } else if (this.mAllowOrientationChange) {
            unApplyOrientation();
        } else {
            Activity activity = this.mWeakActivity.get();
            if (activity != null) {
                lockOrientation(DeviceUtils.getScreenOrientation(activity));
                return;
            }
            throw new MraidCommandException("Unable to set MRAID expand orientation to 'none'; expected passed in Activity Context.");
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void unApplyOrientation() {
        Integer num;
        Activity activity = this.mWeakActivity.get();
        if (!(activity == null || (num = this.mOriginalActivityOrientation) == null)) {
            activity.setRequestedOrientation(num.intValue());
        }
        this.mOriginalActivityOrientation = null;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean shouldAllowForceOrientation(MraidOrientation newOrientation) {
        if (newOrientation == MraidOrientation.NONE) {
            return true;
        }
        Activity activity = this.mWeakActivity.get();
        if (activity == null) {
            return false;
        }
        try {
            ActivityInfo activityInfo = activity.getPackageManager().getActivityInfo(new ComponentName(activity, activity.getClass()), 0);
            int activityOrientation = activityInfo.screenOrientation;
            if (activityOrientation != -1) {
                if (activityOrientation == newOrientation.getActivityInfoOrientation()) {
                    return true;
                }
                return false;
            } else if (!Utils.bitMaskContainsFlag(activityInfo.configChanges, 128) || !Utils.bitMaskContainsFlag(activityInfo.configChanges, 1024)) {
                return false;
            } else {
                return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public void handleCustomClose(boolean useCustomClose) {
        if (useCustomClose != (!this.mCloseableAdContainer.isCloseVisible())) {
            this.mCloseableAdContainer.setCloseVisible(!useCustomClose);
            UseCustomCloseListener useCustomCloseListener = this.mOnCloseButtonListener;
            if (useCustomCloseListener != null) {
                useCustomCloseListener.useCustomCloseChanged(useCustomClose);
            }
        }
    }

    @NonNull
    public FrameLayout getAdContainer() {
        return this.mDefaultAdContainer;
    }

    public void loadJavascript(@NonNull String javascript) {
        this.mMraidBridge.injectJavaScript(javascript);
    }

    @VisibleForTesting
    class OrientationBroadcastReceiver extends BroadcastReceiver {
        @Nullable
        private Context mContext;
        private int mLastRotation = -1;

        OrientationBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            int orientation;
            if (this.mContext != null && "android.intent.action.CONFIGURATION_CHANGED".equals(intent.getAction()) && (orientation = MraidController.this.getDisplayRotation()) != this.mLastRotation) {
                this.mLastRotation = orientation;
                MraidController.this.handleOrientationChange(this.mLastRotation);
            }
        }

        public void register(@NonNull Context context) {
            Preconditions.checkNotNull(context);
            this.mContext = context.getApplicationContext();
            Context context2 = this.mContext;
            if (context2 != null) {
                context2.registerReceiver(this, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"));
            }
        }

        public void unregister() {
            Context context = this.mContext;
            if (context != null) {
                context.unregisterReceiver(this);
                this.mContext = null;
            }
        }
    }

    @NonNull
    public Context getContext() {
        return this.mContext;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handleSetOrientationProperties(boolean allowOrientationChange, MraidOrientation forceOrientation) throws MraidCommandException {
        if (shouldAllowForceOrientation(forceOrientation)) {
            this.mAllowOrientationChange = allowOrientationChange;
            this.mForceOrientation = forceOrientation;
            if (this.mViewState == ViewState.EXPANDED || this.mPlacementType == PlacementType.INTERSTITIAL) {
                applyOrientation();
                return;
            }
            return;
        }
        throw new MraidCommandException("Unable to force orientation to " + forceOrientation);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handleOpen(@NonNull String url) {
        MraidListener mraidListener = this.mMraidListener;
        if (mraidListener != null) {
            mraidListener.onOpen();
        }
        new UrlHandler.Builder().withSupportedUrlActions(UrlAction.IGNORE_ABOUT_SCHEME, UrlAction.OPEN_NATIVE_BROWSER, UrlAction.OPEN_IN_APP_BROWSER, UrlAction.HANDLE_SHARE_TWEET, UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK, UrlAction.FOLLOW_DEEP_LINK).build().handleUrl(this.mContext, url);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    @VisibleForTesting
    @Deprecated
    public ViewState getViewState() {
        return this.mViewState;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Deprecated
    public void setViewStateForTesting(@NonNull ViewState viewState) {
        this.mViewState = viewState;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    @VisibleForTesting
    @Deprecated
    public CloseableLayout getExpandedAdContainer() {
        return this.mCloseableAdContainer;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Deprecated
    public void setRootView(FrameLayout rootView) {
        this.mRootView = rootView;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Deprecated
    public void setRootViewSize(int width, int height) {
        this.mScreenMetrics.setRootViewPosition(0, 0, width, height);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Deprecated
    public Integer getOriginalActivityOrientation() {
        return this.mOriginalActivityOrientation;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Deprecated
    public boolean getAllowOrientationChange() {
        return this.mAllowOrientationChange;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Deprecated
    public MraidOrientation getForceOrientation() {
        return this.mForceOrientation;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Deprecated
    public void setOrientationBroadcastReceiver(OrientationBroadcastReceiver receiver) {
        this.mOrientationBroadcastReceiver = receiver;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Deprecated
    public MraidBridge.MraidWebView getMraidWebView() {
        return this.mMraidWebView;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Deprecated
    public MraidBridge.MraidWebView getTwoPartWebView() {
        return this.mTwoPartWebView;
    }
}
