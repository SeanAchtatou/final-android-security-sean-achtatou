package com.jobstrak.drawingfun.lib.mopub.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.StateListDrawable;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.util.Dips;
import com.jobstrak.drawingfun.lib.mopub.common.util.Drawables;

public class CloseableLayout extends FrameLayout {
    @VisibleForTesting
    static final float CLOSE_BUTTON_PADDING_DP = 8.0f;
    @VisibleForTesting
    static final float CLOSE_BUTTON_SIZE_DP = 30.0f;
    static final float CLOSE_REGION_SIZE_DP = 50.0f;
    private final Rect mClosableLayoutRect = new Rect();
    private boolean mCloseAlwaysInteractable;
    private boolean mCloseBoundChanged;
    private final Rect mCloseButtonBounds = new Rect();
    private final int mCloseButtonPadding;
    private final int mCloseButtonSize;
    @NonNull
    private final StateListDrawable mCloseDrawable = new StateListDrawable();
    @NonNull
    private ClosePosition mClosePosition = ClosePosition.TOP_RIGHT;
    private final Rect mCloseRegionBounds = new Rect();
    private final int mCloseRegionSize;
    private final Rect mInsetCloseRegionBounds = new Rect();
    @Nullable
    private OnCloseListener mOnCloseListener;
    private final int mTouchSlop;
    @Nullable
    private UnsetPressedState mUnsetPressedState;

    public interface OnCloseListener {
        void onClose();
    }

    public enum ClosePosition {
        TOP_LEFT(51),
        TOP_CENTER(49),
        TOP_RIGHT(53),
        CENTER(17),
        BOTTOM_LEFT(83),
        BOTTOM_CENTER(81),
        BOTTOM_RIGHT(85);
        
        private final int mGravity;

        private ClosePosition(int mGravity2) {
            this.mGravity = mGravity2;
        }

        /* access modifiers changed from: package-private */
        public int getGravity() {
            return this.mGravity;
        }
    }

    public CloseableLayout(@NonNull Context context) {
        super(context);
        this.mCloseDrawable.addState(SELECTED_STATE_SET, Drawables.INTERSTITIAL_CLOSE_BUTTON_PRESSED.createDrawable(context));
        this.mCloseDrawable.addState(EMPTY_STATE_SET, Drawables.INTERSTITIAL_CLOSE_BUTTON_NORMAL.createDrawable(context));
        this.mCloseDrawable.setState(EMPTY_STATE_SET);
        this.mCloseDrawable.setCallback(this);
        this.mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        this.mCloseRegionSize = Dips.asIntPixels(CLOSE_REGION_SIZE_DP, context);
        this.mCloseButtonSize = Dips.asIntPixels(CLOSE_BUTTON_SIZE_DP, context);
        this.mCloseButtonPadding = Dips.asIntPixels(8.0f, context);
        setWillNotDraw(false);
        this.mCloseAlwaysInteractable = true;
    }

    public void setOnCloseListener(@Nullable OnCloseListener onCloseListener) {
        this.mOnCloseListener = onCloseListener;
    }

    public void setClosePosition(@NonNull ClosePosition closePosition) {
        Preconditions.checkNotNull(closePosition);
        this.mClosePosition = closePosition;
        this.mCloseBoundChanged = true;
        invalidate();
    }

    public void setCloseVisible(boolean visible) {
        if (this.mCloseDrawable.setVisible(visible, false)) {
            invalidate(this.mCloseRegionBounds);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        this.mCloseBoundChanged = true;
    }

    public void draw(@NonNull Canvas canvas) {
        super.draw(canvas);
        if (this.mCloseBoundChanged) {
            this.mCloseBoundChanged = false;
            this.mClosableLayoutRect.set(0, 0, getWidth(), getHeight());
            applyCloseRegionBounds(this.mClosePosition, this.mClosableLayoutRect, this.mCloseRegionBounds);
            this.mInsetCloseRegionBounds.set(this.mCloseRegionBounds);
            Rect rect = this.mInsetCloseRegionBounds;
            int i = this.mCloseButtonPadding;
            rect.inset(i, i);
            applyCloseButtonBounds(this.mClosePosition, this.mInsetCloseRegionBounds, this.mCloseButtonBounds);
            this.mCloseDrawable.setBounds(this.mCloseButtonBounds);
        }
        if (this.mCloseDrawable.isVisible()) {
            this.mCloseDrawable.draw(canvas);
        }
    }

    public void applyCloseRegionBounds(ClosePosition closePosition, Rect bounds, Rect closeBounds) {
        applyCloseBoundsWithSize(closePosition, this.mCloseRegionSize, bounds, closeBounds);
    }

    private void applyCloseButtonBounds(ClosePosition closePosition, Rect bounds, Rect outBounds) {
        applyCloseBoundsWithSize(closePosition, this.mCloseButtonSize, bounds, outBounds);
    }

    private void applyCloseBoundsWithSize(ClosePosition closePosition, int size, Rect bounds, Rect outBounds) {
        Gravity.apply(closePosition.getGravity(), size, size, bounds, outBounds);
    }

    public boolean onInterceptTouchEvent(@NonNull MotionEvent event) {
        if (event.getAction() != 0) {
            return false;
        }
        return pointInCloseBounds((int) event.getX(), (int) event.getY(), 0);
    }

    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (!pointInCloseBounds((int) event.getX(), (int) event.getY(), this.mTouchSlop) || !shouldAllowPress()) {
            setClosePressed(false);
            super.onTouchEvent(event);
            return false;
        }
        int action = event.getAction();
        if (action == 0) {
            setClosePressed(true);
        } else if (action != 1) {
            if (action == 3) {
                setClosePressed(false);
            }
        } else if (isClosePressed()) {
            if (this.mUnsetPressedState == null) {
                this.mUnsetPressedState = new UnsetPressedState();
            }
            postDelayed(this.mUnsetPressedState, (long) ViewConfiguration.getPressedStateDuration());
            performClose();
        }
        return true;
    }

    public void setCloseAlwaysInteractable(boolean closeAlwaysInteractable) {
        this.mCloseAlwaysInteractable = closeAlwaysInteractable;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean shouldAllowPress() {
        return this.mCloseAlwaysInteractable || this.mCloseDrawable.isVisible();
    }

    /* access modifiers changed from: private */
    public void setClosePressed(boolean pressed) {
        if (pressed != isClosePressed()) {
            this.mCloseDrawable.setState(pressed ? SELECTED_STATE_SET : EMPTY_STATE_SET);
            invalidate(this.mCloseRegionBounds);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean isClosePressed() {
        return this.mCloseDrawable.getState() == SELECTED_STATE_SET;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean pointInCloseBounds(int x, int y, int slop) {
        return x >= this.mCloseRegionBounds.left - slop && y >= this.mCloseRegionBounds.top - slop && x < this.mCloseRegionBounds.right + slop && y < this.mCloseRegionBounds.bottom + slop;
    }

    private void performClose() {
        playSoundEffect(0);
        OnCloseListener onCloseListener = this.mOnCloseListener;
        if (onCloseListener != null) {
            onCloseListener.onClose();
        }
    }

    private final class UnsetPressedState implements Runnable {
        private UnsetPressedState() {
        }

        public void run() {
            CloseableLayout.this.setClosePressed(false);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void setCloseBounds(Rect closeBounds) {
        this.mCloseRegionBounds.set(closeBounds);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public Rect getCloseBounds() {
        return this.mCloseRegionBounds;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void setCloseBoundChanged(boolean changed) {
        this.mCloseBoundChanged = changed;
    }

    @VisibleForTesting
    public boolean isCloseVisible() {
        return this.mCloseDrawable.isVisible();
    }
}
