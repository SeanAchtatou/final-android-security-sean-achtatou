package com.jobstrak.drawingfun.lib.okhttp3.internal.tls;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.GeneralSecurityException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.X509TrustManager;

public abstract class CertificateChainCleaner {
    public abstract List<Certificate> clean(List<Certificate> list, String str) throws SSLPeerUnverifiedException;

    public static CertificateChainCleaner get(X509TrustManager trustManager) {
        try {
            Class<?> extensionsClass = Class.forName("android.net.http.X509TrustManagerExtensions");
            return new AndroidCertificateChainCleaner(extensionsClass.getConstructor(X509TrustManager.class).newInstance(trustManager), extensionsClass.getMethod("checkServerTrusted", X509Certificate[].class, String.class, String.class));
        } catch (Exception e) {
            return new BasicCertificateChainCleaner(TrustRootIndex.get(trustManager));
        }
    }

    public static CertificateChainCleaner get(X509Certificate... caCerts) {
        return new BasicCertificateChainCleaner(TrustRootIndex.get(caCerts));
    }

    static final class BasicCertificateChainCleaner extends CertificateChainCleaner {
        private static final int MAX_SIGNERS = 9;
        private final TrustRootIndex trustRootIndex;

        public BasicCertificateChainCleaner(TrustRootIndex trustRootIndex2) {
            this.trustRootIndex = trustRootIndex2;
        }

        public List<Certificate> clean(List<Certificate> chain, String hostname) throws SSLPeerUnverifiedException {
            Deque<Certificate> queue = new ArrayDeque<>(chain);
            List<Certificate> result = new ArrayList<>();
            result.add(queue.removeFirst());
            boolean foundTrustedCertificate = false;
            for (int c = 0; c < 9; c++) {
                X509Certificate toVerify = (X509Certificate) result.get(result.size() - 1);
                X509Certificate trustedCert = this.trustRootIndex.findByIssuerAndSignature(toVerify);
                if (trustedCert != null) {
                    if (result.size() > 1 || !toVerify.equals(trustedCert)) {
                        result.add(trustedCert);
                    }
                    if (verifySignature(trustedCert, trustedCert)) {
                        return result;
                    }
                    foundTrustedCertificate = true;
                } else {
                    Iterator<Certificate> i = queue.iterator();
                    while (i.hasNext()) {
                        X509Certificate signingCert = (X509Certificate) i.next();
                        if (verifySignature(toVerify, signingCert)) {
                            i.remove();
                            result.add(signingCert);
                        }
                    }
                    if (foundTrustedCertificate) {
                        return result;
                    }
                    throw new SSLPeerUnverifiedException("Failed to find a trusted cert that signed " + toVerify);
                }
            }
            throw new SSLPeerUnverifiedException("Certificate chain too long: " + result);
        }

        private boolean verifySignature(X509Certificate toVerify, X509Certificate signingCert) {
            if (!toVerify.getIssuerDN().equals(signingCert.getSubjectDN())) {
                return false;
            }
            try {
                toVerify.verify(signingCert.getPublicKey());
                return true;
            } catch (GeneralSecurityException e) {
                return false;
            }
        }
    }

    static final class AndroidCertificateChainCleaner extends CertificateChainCleaner {
        private final Method checkServerTrusted;
        private final Object x509TrustManagerExtensions;

        AndroidCertificateChainCleaner(Object x509TrustManagerExtensions2, Method checkServerTrusted2) {
            this.x509TrustManagerExtensions = x509TrustManagerExtensions2;
            this.checkServerTrusted = checkServerTrusted2;
        }

        public List<Certificate> clean(List<Certificate> chain, String hostname) throws SSLPeerUnverifiedException {
            try {
                return (List) this.checkServerTrusted.invoke(this.x509TrustManagerExtensions, (X509Certificate[]) chain.toArray(new X509Certificate[chain.size()]), "RSA", hostname);
            } catch (InvocationTargetException e) {
                SSLPeerUnverifiedException exception = new SSLPeerUnverifiedException(e.getMessage());
                exception.initCause(e);
                throw exception;
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }
    }
}
