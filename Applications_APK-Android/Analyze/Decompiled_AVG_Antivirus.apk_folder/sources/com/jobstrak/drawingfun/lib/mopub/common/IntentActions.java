package com.jobstrak.drawingfun.lib.mopub.common;

public class IntentActions {
    public static final String ACTION_INTERSTITIAL_CLICK = "com.jobstrak.drawingfun.lib.mopub.action.interstitial.click";
    public static final String ACTION_INTERSTITIAL_DISMISS = "com.jobstrak.drawingfun.lib.mopub.action.interstitial.dismiss";
    public static final String ACTION_INTERSTITIAL_FAIL = "com.jobstrak.drawingfun.lib.mopub.action.interstitial.fail";
    public static final String ACTION_INTERSTITIAL_SHOW = "com.jobstrak.drawingfun.lib.mopub.action.interstitial.show";
    public static final String ACTION_REWARDED_PLAYABLE_COMPLETE = "com.jobstrak.drawingfun.lib.mopub.action.rewardedplayable.complete";
    public static final String ACTION_REWARDED_VIDEO_COMPLETE = "com.jobstrak.drawingfun.lib.mopub.action.rewardedvideo.complete";

    private IntentActions() {
    }
}
