package com.jobstrak.drawingfun.lib.mopub.common.event;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.jobstrak.drawingfun.lib.mopub.common.VisibleForTesting;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;

public class EventDispatcher {
    /* access modifiers changed from: private */
    public final Iterable<EventRecorder> mEventRecorders;
    private final Handler.Callback mHandlerCallback = new Handler.Callback() {
        public boolean handleMessage(Message msg) {
            if (msg.obj instanceof BaseEvent) {
                for (EventRecorder recorder : EventDispatcher.this.mEventRecorders) {
                    recorder.record((BaseEvent) msg.obj);
                }
                return true;
            }
            MoPubLog.d("EventDispatcher received non-BaseEvent message type.");
            return true;
        }
    };
    private final Looper mLooper;
    private final Handler mMessageHandler = new Handler(this.mLooper, this.mHandlerCallback);

    @VisibleForTesting
    EventDispatcher(Iterable<EventRecorder> recorders, Looper looper) {
        this.mEventRecorders = recorders;
        this.mLooper = looper;
    }

    public void dispatch(BaseEvent event) {
        Message.obtain(this.mMessageHandler, 0, event).sendToTarget();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public Iterable<EventRecorder> getEventRecorders() {
        return this.mEventRecorders;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public Handler.Callback getHandlerCallback() {
        return this.mHandlerCallback;
    }
}
