package com.jobstrak.drawingfun.lib.gson.internal.bind;

import com.jobstrak.drawingfun.lib.gson.Gson;
import com.jobstrak.drawingfun.lib.gson.JsonSyntaxException;
import com.jobstrak.drawingfun.lib.gson.TypeAdapter;
import com.jobstrak.drawingfun.lib.gson.TypeAdapterFactory;
import com.jobstrak.drawingfun.lib.gson.reflect.TypeToken;
import com.jobstrak.drawingfun.lib.gson.stream.JsonReader;
import com.jobstrak.drawingfun.lib.gson.stream.JsonToken;
import com.jobstrak.drawingfun.lib.gson.stream.JsonWriter;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class SqlDateTypeAdapter extends TypeAdapter<Date> {
    public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            if (typeToken.getRawType() == Date.class) {
                return new SqlDateTypeAdapter();
            }
            return null;
        }
    };
    private final DateFormat format = new SimpleDateFormat("MMM d, yyyy");

    public synchronized Date read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        }
        try {
            return new Date(this.format.parse(in.nextString()).getTime());
        } catch (ParseException e) {
            throw new JsonSyntaxException(e);
        }
    }

    public synchronized void write(JsonWriter out, Date value) throws IOException {
        out.value(value == null ? null : this.format.format(value));
    }
}
