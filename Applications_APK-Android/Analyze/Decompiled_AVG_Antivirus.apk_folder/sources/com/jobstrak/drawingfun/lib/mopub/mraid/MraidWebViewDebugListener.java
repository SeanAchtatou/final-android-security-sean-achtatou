package com.jobstrak.drawingfun.lib.mopub.mraid;

import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.mopub.common.VisibleForTesting;

@VisibleForTesting
public interface MraidWebViewDebugListener {
    boolean onConsoleMessage(@NonNull ConsoleMessage consoleMessage);

    boolean onJsAlert(@NonNull String str, @NonNull JsResult jsResult);
}
