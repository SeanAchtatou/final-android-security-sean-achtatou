package com.jobstrak.drawingfun.lib.mopub.common.event;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.Preconditions;
import com.jobstrak.drawingfun.lib.mopub.common.event.BaseEvent;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;

public class Event extends BaseEvent {
    private Event(@NonNull Builder builder) {
        super(builder);
    }

    public static class Builder extends BaseEvent.Builder {
        public Builder(@NonNull BaseEvent.Name name, @NonNull BaseEvent.Category category, double samplingRate) {
            super(BaseEvent.ScribeCategory.EXCHANGE_CLIENT_EVENT, name, category, samplingRate);
        }

        @NonNull
        public Event build() {
            return new Event(this);
        }
    }

    @Nullable
    public static BaseEvent createEventFromDetails(@NonNull BaseEvent.Name name, @NonNull BaseEvent.Category category, @NonNull BaseEvent.SamplingRate samplingRate, @Nullable EventDetails eventDetails) {
        Preconditions.checkNotNull(name);
        Preconditions.checkNotNull(category);
        Preconditions.checkNotNull(samplingRate);
        if (eventDetails != null) {
            return new Builder(name, category, samplingRate.getSamplingRate()).withAdUnitId(eventDetails.getAdUnitId()).withAdCreativeId(eventDetails.getDspCreativeId()).withAdType(eventDetails.getAdType()).withAdNetworkType(eventDetails.getAdNetworkType()).withAdWidthPx(eventDetails.getAdWidthPx()).withAdHeightPx(eventDetails.getAdHeightPx()).withGeoLat(eventDetails.getGeoLatitude()).withGeoLon(eventDetails.getGeoLongitude()).withGeoAccuracy(eventDetails.getGeoAccuracy()).withPerformanceDurationMs(eventDetails.getPerformanceDurationMs()).withRequestId(eventDetails.getRequestId()).withRequestStatusCode(eventDetails.getRequestStatusCode()).withRequestUri(eventDetails.getRequestUri()).build();
        }
        MoPubLog.d("Unable to log event due to no details present");
        return null;
    }
}
