package com.jobstrak.drawingfun.lib.processes.models;

public class ControlGroup {
    public final String group;
    public final int id;
    public final String subsystems;

    protected ControlGroup(String line) throws NumberFormatException, IndexOutOfBoundsException {
        String[] fields = line.split(":");
        this.id = Integer.parseInt(fields[0]);
        this.subsystems = fields[1];
        this.group = fields[2];
    }
}
