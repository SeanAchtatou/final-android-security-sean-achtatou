package com.jobstrak.drawingfun.lib.okhttp3.internal.framed;

import androidx.core.internal.view.SupportMenu;
import com.jobstrak.drawingfun.lib.okhttp3.Protocol;
import com.jobstrak.drawingfun.lib.okhttp3.internal.NamedRunnable;
import com.jobstrak.drawingfun.lib.okhttp3.internal.Util;
import com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FrameReader;
import com.jobstrak.drawingfun.lib.okio.Buffer;
import com.jobstrak.drawingfun.lib.okio.BufferedSink;
import com.jobstrak.drawingfun.lib.okio.BufferedSource;
import com.jobstrak.drawingfun.lib.okio.ByteString;
import com.jobstrak.drawingfun.lib.okio.Okio;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.LongCompanionObject;

public final class FramedConnection implements Closeable {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final int OKHTTP_CLIENT_WINDOW_SIZE = 16777216;
    /* access modifiers changed from: private */
    public static final ExecutorService executor = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), Util.threadFactory("OkHttp FramedConnection", true));
    long bytesLeftInWriteWindow;
    final boolean client;
    /* access modifiers changed from: private */
    public final Set<Integer> currentPushRequests;
    final FrameWriter frameWriter;
    /* access modifiers changed from: private */
    public final String hostname;
    private long idleStartTimeNs;
    /* access modifiers changed from: private */
    public int lastGoodStreamId;
    /* access modifiers changed from: private */
    public final Listener listener;
    private int nextPingId;
    /* access modifiers changed from: private */
    public int nextStreamId;
    Settings okHttpSettings;
    final Settings peerSettings;
    private Map<Integer, Ping> pings;
    final Protocol protocol;
    private final ExecutorService pushExecutor;
    /* access modifiers changed from: private */
    public final PushObserver pushObserver;
    final Reader readerRunnable;
    /* access modifiers changed from: private */
    public boolean receivedInitialPeerSettings;
    /* access modifiers changed from: private */
    public boolean shutdown;
    final Socket socket;
    /* access modifiers changed from: private */
    public final Map<Integer, FramedStream> streams;
    long unacknowledgedBytesRead;
    final Variant variant;

    private FramedConnection(Builder builder) throws IOException {
        this.streams = new HashMap();
        this.idleStartTimeNs = System.nanoTime();
        this.unacknowledgedBytesRead = 0;
        this.okHttpSettings = new Settings();
        this.peerSettings = new Settings();
        this.receivedInitialPeerSettings = false;
        this.currentPushRequests = new LinkedHashSet();
        this.protocol = builder.protocol;
        this.pushObserver = builder.pushObserver;
        this.client = builder.client;
        this.listener = builder.listener;
        int i = 2;
        this.nextStreamId = builder.client ? 1 : 2;
        if (builder.client && this.protocol == Protocol.HTTP_2) {
            this.nextStreamId += 2;
        }
        this.nextPingId = builder.client ? 1 : i;
        if (builder.client) {
            this.okHttpSettings.set(7, 0, 16777216);
        }
        this.hostname = builder.hostname;
        if (this.protocol == Protocol.HTTP_2) {
            this.variant = new Http2();
            this.pushExecutor = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), Util.threadFactory(String.format("OkHttp %s Push Observer", this.hostname), true));
            this.peerSettings.set(7, 0, SupportMenu.USER_MASK);
            this.peerSettings.set(5, 0, 16384);
        } else if (this.protocol == Protocol.SPDY_3) {
            this.variant = new Spdy3();
            this.pushExecutor = null;
        } else {
            throw new AssertionError(this.protocol);
        }
        this.bytesLeftInWriteWindow = (long) this.peerSettings.getInitialWindowSize(65536);
        this.socket = builder.socket;
        this.frameWriter = this.variant.newWriter(builder.sink, this.client);
        this.readerRunnable = new Reader(this.variant.newReader(builder.source, this.client));
        new Thread(this.readerRunnable).start();
    }

    public Protocol getProtocol() {
        return this.protocol;
    }

    public synchronized int openStreamCount() {
        return this.streams.size();
    }

    /* access modifiers changed from: package-private */
    public synchronized FramedStream getStream(int id) {
        return this.streams.get(Integer.valueOf(id));
    }

    /* access modifiers changed from: package-private */
    public synchronized FramedStream removeStream(int streamId) {
        FramedStream stream;
        stream = this.streams.remove(Integer.valueOf(streamId));
        if (stream != null && this.streams.isEmpty()) {
            setIdle(true);
        }
        notifyAll();
        return stream;
    }

    private synchronized void setIdle(boolean value) {
        long j;
        if (value) {
            try {
                j = System.nanoTime();
            } catch (Throwable th) {
                throw th;
            }
        } else {
            j = LongCompanionObject.MAX_VALUE;
        }
        this.idleStartTimeNs = j;
    }

    public synchronized boolean isIdle() {
        return this.idleStartTimeNs != LongCompanionObject.MAX_VALUE;
    }

    public synchronized int maxConcurrentStreams() {
        return this.peerSettings.getMaxConcurrentStreams(Integer.MAX_VALUE);
    }

    public synchronized long getIdleStartTimeNs() {
        return this.idleStartTimeNs;
    }

    public FramedStream pushStream(int associatedStreamId, List<Header> requestHeaders, boolean out) throws IOException {
        if (this.client) {
            throw new IllegalStateException("Client cannot push requests.");
        } else if (this.protocol == Protocol.HTTP_2) {
            return newStream(associatedStreamId, requestHeaders, out, false);
        } else {
            throw new IllegalStateException("protocol != HTTP_2");
        }
    }

    public FramedStream newStream(List<Header> requestHeaders, boolean out, boolean in) throws IOException {
        return newStream(0, requestHeaders, out, in);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0038, code lost:
        if (r14 != 0) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r7.frameWriter.synStream(r4, r5, r0, r17, r18);
        r3 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004b, code lost:
        if (r7.client != false) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r7.frameWriter.pushPromise(r14, r0, r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0055, code lost:
        if (r19 != false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0057, code lost:
        r7.frameWriter.flush();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005c, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0066, code lost:
        throw new java.lang.IllegalArgumentException("client streams shouldn't have associated stream IDs");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream newStream(int r17, java.util.List<com.jobstrak.drawingfun.lib.okhttp3.internal.framed.Header> r18, boolean r19, boolean r20) throws java.io.IOException {
        /*
            r16 = this;
            r7 = r16
            r14 = r17
            r4 = r19 ^ 1
            r5 = r20 ^ 1
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FrameWriter r15 = r7.frameWriter
            monitor-enter(r15)
            monitor-enter(r16)     // Catch:{ all -> 0x0078 }
            boolean r0 = r7.shutdown     // Catch:{ all -> 0x0071 }
            if (r0 != 0) goto L_0x0067
            int r0 = r7.nextStreamId     // Catch:{ all -> 0x0071 }
            int r1 = r7.nextStreamId     // Catch:{ all -> 0x0071 }
            int r1 = r1 + 2
            r7.nextStreamId = r1     // Catch:{ all -> 0x0071 }
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r8 = new com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream     // Catch:{ all -> 0x0071 }
            r1 = r8
            r2 = r0
            r3 = r16
            r6 = r18
            r1.<init>(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0071 }
            r1 = r8
            boolean r2 = r1.isOpen()     // Catch:{ all -> 0x0071 }
            if (r2 == 0) goto L_0x0037
            java.util.Map<java.lang.Integer, com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream> r2 = r7.streams     // Catch:{ all -> 0x0071 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0071 }
            r2.put(r3, r1)     // Catch:{ all -> 0x0071 }
            r2 = 0
            r7.setIdle(r2)     // Catch:{ all -> 0x0071 }
        L_0x0037:
            monitor-exit(r16)     // Catch:{ all -> 0x0071 }
            if (r14 != 0) goto L_0x0049
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FrameWriter r8 = r7.frameWriter     // Catch:{ all -> 0x0078 }
            r9 = r4
            r10 = r5
            r11 = r0
            r12 = r17
            r13 = r18
            r8.synStream(r9, r10, r11, r12, r13)     // Catch:{ all -> 0x0078 }
            r3 = r18
            goto L_0x0054
        L_0x0049:
            boolean r2 = r7.client     // Catch:{ all -> 0x0078 }
            if (r2 != 0) goto L_0x005d
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FrameWriter r2 = r7.frameWriter     // Catch:{ all -> 0x0078 }
            r3 = r18
            r2.pushPromise(r14, r0, r3)     // Catch:{ all -> 0x007d }
        L_0x0054:
            monitor-exit(r15)     // Catch:{ all -> 0x007d }
            if (r19 != 0) goto L_0x005c
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FrameWriter r2 = r7.frameWriter
            r2.flush()
        L_0x005c:
            return r1
        L_0x005d:
            r3 = r18
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x007d }
            java.lang.String r6 = "client streams shouldn't have associated stream IDs"
            r2.<init>(r6)     // Catch:{ all -> 0x007d }
            throw r2     // Catch:{ all -> 0x007d }
        L_0x0067:
            r3 = r18
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0076 }
            java.lang.String r1 = "shutdown"
            r0.<init>(r1)     // Catch:{ all -> 0x0076 }
            throw r0     // Catch:{ all -> 0x0076 }
        L_0x0071:
            r0 = move-exception
            r3 = r18
        L_0x0074:
            monitor-exit(r16)     // Catch:{ all -> 0x0076 }
            throw r0     // Catch:{ all -> 0x007d }
        L_0x0076:
            r0 = move-exception
            goto L_0x0074
        L_0x0078:
            r0 = move-exception
            r3 = r18
        L_0x007b:
            monitor-exit(r15)     // Catch:{ all -> 0x007d }
            throw r0
        L_0x007d:
            r0 = move-exception
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.newStream(int, java.util.List, boolean, boolean):com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream");
    }

    /* access modifiers changed from: package-private */
    public void writeSynReply(int streamId, boolean outFinished, List<Header> alternating) throws IOException {
        this.frameWriter.synReply(outFinished, streamId, alternating);
    }

    public void writeData(int streamId, boolean outFinished, Buffer buffer, long byteCount) throws IOException {
        int toWrite;
        if (byteCount == 0) {
            this.frameWriter.data(outFinished, streamId, buffer, 0);
            return;
        }
        while (byteCount > 0) {
            synchronized (this) {
                while (this.bytesLeftInWriteWindow <= 0) {
                    try {
                        if (this.streams.containsKey(Integer.valueOf(streamId))) {
                            wait();
                        } else {
                            throw new IOException("stream closed");
                        }
                    } catch (InterruptedException e) {
                        throw new InterruptedIOException();
                    }
                }
                toWrite = Math.min((int) Math.min(byteCount, this.bytesLeftInWriteWindow), this.frameWriter.maxDataLength());
                this.bytesLeftInWriteWindow -= (long) toWrite;
            }
            byteCount -= (long) toWrite;
            this.frameWriter.data(outFinished && byteCount == 0, streamId, buffer, toWrite);
        }
    }

    /* access modifiers changed from: package-private */
    public void addBytesToWriteWindow(long delta) {
        this.bytesLeftInWriteWindow += delta;
        if (delta > 0) {
            notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    public void writeSynResetLater(int streamId, ErrorCode errorCode) {
        final int i = streamId;
        final ErrorCode errorCode2 = errorCode;
        executor.submit(new NamedRunnable("OkHttp %s stream %d", new Object[]{this.hostname, Integer.valueOf(streamId)}) {
            public void execute() {
                try {
                    FramedConnection.this.writeSynReset(i, errorCode2);
                } catch (IOException e) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void writeSynReset(int streamId, ErrorCode statusCode) throws IOException {
        this.frameWriter.rstStream(streamId, statusCode);
    }

    /* access modifiers changed from: package-private */
    public void writeWindowUpdateLater(int streamId, long unacknowledgedBytesRead2) {
        final int i = streamId;
        final long j = unacknowledgedBytesRead2;
        executor.execute(new NamedRunnable("OkHttp Window Update %s stream %d", new Object[]{this.hostname, Integer.valueOf(streamId)}) {
            public void execute() {
                try {
                    FramedConnection.this.frameWriter.windowUpdate(i, j);
                } catch (IOException e) {
                }
            }
        });
    }

    public Ping ping() throws IOException {
        int pingId;
        Ping ping = new Ping();
        synchronized (this) {
            if (!this.shutdown) {
                pingId = this.nextPingId;
                this.nextPingId += 2;
                if (this.pings == null) {
                    this.pings = new HashMap();
                }
                this.pings.put(Integer.valueOf(pingId), ping);
            } else {
                throw new IOException("shutdown");
            }
        }
        writePing(false, pingId, 1330343787, ping);
        return ping;
    }

    /* access modifiers changed from: private */
    public void writePingLater(boolean reply, int payload1, int payload2, Ping ping) {
        final boolean z = reply;
        final int i = payload1;
        final int i2 = payload2;
        final Ping ping2 = ping;
        executor.execute(new NamedRunnable("OkHttp %s ping %08x%08x", new Object[]{this.hostname, Integer.valueOf(payload1), Integer.valueOf(payload2)}) {
            public void execute() {
                try {
                    FramedConnection.this.writePing(z, i, i2, ping2);
                } catch (IOException e) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void writePing(boolean reply, int payload1, int payload2, Ping ping) throws IOException {
        synchronized (this.frameWriter) {
            if (ping != null) {
                ping.send();
            }
            this.frameWriter.ping(reply, payload1, payload2);
        }
    }

    /* access modifiers changed from: private */
    public synchronized Ping removePing(int id) {
        return this.pings != null ? this.pings.remove(Integer.valueOf(id)) : null;
    }

    public void flush() throws IOException {
        this.frameWriter.flush();
    }

    public void shutdown(ErrorCode statusCode) throws IOException {
        synchronized (this.frameWriter) {
            synchronized (this) {
                if (!this.shutdown) {
                    this.shutdown = true;
                    int lastGoodStreamId2 = this.lastGoodStreamId;
                    this.frameWriter.goAway(lastGoodStreamId2, statusCode, Util.EMPTY_BYTE_ARRAY);
                }
            }
        }
    }

    public void close() throws IOException {
        close(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
    }

    /* access modifiers changed from: private */
    public void close(ErrorCode connectionCode, ErrorCode streamCode) throws IOException {
        int i;
        IOException thrown = null;
        try {
            shutdown(connectionCode);
        } catch (IOException e) {
            thrown = e;
        }
        FramedStream[] streamsToClose = null;
        Ping[] pingsToCancel = null;
        synchronized (this) {
            if (!this.streams.isEmpty()) {
                streamsToClose = (FramedStream[]) this.streams.values().toArray(new FramedStream[this.streams.size()]);
                this.streams.clear();
                setIdle(false);
            }
            if (this.pings != null) {
                pingsToCancel = (Ping[]) this.pings.values().toArray(new Ping[this.pings.size()]);
                this.pings = null;
            }
        }
        if (streamsToClose != null) {
            IOException thrown2 = thrown;
            for (FramedStream stream : streamsToClose) {
                try {
                    stream.close(streamCode);
                } catch (IOException e2) {
                    if (thrown2 != null) {
                        thrown2 = e2;
                    }
                }
            }
            thrown = thrown2;
        }
        if (pingsToCancel != null) {
            for (Ping ping : pingsToCancel) {
                ping.cancel();
            }
        }
        try {
            this.frameWriter.close();
        } catch (IOException e3) {
            if (thrown == null) {
                thrown = e3;
            }
        }
        try {
            this.socket.close();
        } catch (IOException e4) {
            thrown = e4;
        }
        if (thrown != null) {
            throw thrown;
        }
    }

    public void sendConnectionPreface() throws IOException {
        this.frameWriter.connectionPreface();
        this.frameWriter.settings(this.okHttpSettings);
        int windowSize = this.okHttpSettings.getInitialWindowSize(65536);
        if (windowSize != 65536) {
            this.frameWriter.windowUpdate(0, (long) (windowSize - 65536));
        }
    }

    public void setSettings(Settings settings) throws IOException {
        synchronized (this.frameWriter) {
            synchronized (this) {
                if (!this.shutdown) {
                    this.okHttpSettings.merge(settings);
                    this.frameWriter.settings(settings);
                } else {
                    throw new IOException("shutdown");
                }
            }
        }
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public boolean client;
        /* access modifiers changed from: private */
        public String hostname;
        /* access modifiers changed from: private */
        public Listener listener = Listener.REFUSE_INCOMING_STREAMS;
        /* access modifiers changed from: private */
        public Protocol protocol = Protocol.SPDY_3;
        /* access modifiers changed from: private */
        public PushObserver pushObserver = PushObserver.CANCEL;
        /* access modifiers changed from: private */
        public BufferedSink sink;
        /* access modifiers changed from: private */
        public Socket socket;
        /* access modifiers changed from: private */
        public BufferedSource source;

        public Builder(boolean client2) throws IOException {
            this.client = client2;
        }

        public Builder socket(Socket socket2) throws IOException {
            return socket(socket2, ((InetSocketAddress) socket2.getRemoteSocketAddress()).getHostName(), Okio.buffer(Okio.source(socket2)), Okio.buffer(Okio.sink(socket2)));
        }

        public Builder socket(Socket socket2, String hostname2, BufferedSource source2, BufferedSink sink2) {
            this.socket = socket2;
            this.hostname = hostname2;
            this.source = source2;
            this.sink = sink2;
            return this;
        }

        public Builder listener(Listener listener2) {
            this.listener = listener2;
            return this;
        }

        public Builder protocol(Protocol protocol2) {
            this.protocol = protocol2;
            return this;
        }

        public Builder pushObserver(PushObserver pushObserver2) {
            this.pushObserver = pushObserver2;
            return this;
        }

        public FramedConnection build() throws IOException {
            return new FramedConnection(this);
        }
    }

    class Reader extends NamedRunnable implements FrameReader.Handler {
        final FrameReader frameReader;

        private Reader(FrameReader frameReader2) {
            super("OkHttp %s", FramedConnection.this.hostname);
            this.frameReader = frameReader2;
        }

        /* access modifiers changed from: protected */
        public void execute() {
            ErrorCode connectionErrorCode = ErrorCode.INTERNAL_ERROR;
            ErrorCode streamErrorCode = ErrorCode.INTERNAL_ERROR;
            try {
                if (!FramedConnection.this.client) {
                    this.frameReader.readConnectionPreface();
                }
                while (this.frameReader.nextFrame(this)) {
                }
                try {
                    FramedConnection.this.close(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
                } catch (IOException e) {
                }
            } catch (IOException e2) {
                connectionErrorCode = ErrorCode.PROTOCOL_ERROR;
                try {
                    FramedConnection.this.close(connectionErrorCode, ErrorCode.PROTOCOL_ERROR);
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    FramedConnection.this.close(connectionErrorCode, streamErrorCode);
                } catch (IOException e4) {
                }
                Util.closeQuietly(this.frameReader);
                throw th;
            }
            Util.closeQuietly(this.frameReader);
        }

        public void data(boolean inFinished, int streamId, BufferedSource source, int length) throws IOException {
            if (FramedConnection.this.pushedStream(streamId)) {
                FramedConnection.this.pushDataLater(streamId, source, length, inFinished);
                return;
            }
            FramedStream dataStream = FramedConnection.this.getStream(streamId);
            if (dataStream == null) {
                FramedConnection.this.writeSynResetLater(streamId, ErrorCode.INVALID_STREAM);
                source.skip((long) length);
                return;
            }
            dataStream.receiveData(source, length);
            if (inFinished) {
                dataStream.receiveFin();
            }
        }

        /* JADX INFO: finally extract failed */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x009a, code lost:
            if (r20.failIfStreamPresent() == false) goto L_0x00a7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x009c, code lost:
            r0.closeLater(com.jobstrak.drawingfun.lib.okhttp3.internal.framed.ErrorCode.PROTOCOL_ERROR);
            r1.this$0.removeStream(r9);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a6, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a7, code lost:
            r0.receiveHeaders(r10, r20);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ac, code lost:
            if (r8 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ae, code lost:
            r0.receiveFin();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b7, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void headers(boolean r15, boolean r16, int r17, int r18, java.util.List<com.jobstrak.drawingfun.lib.okhttp3.internal.framed.Header> r19, com.jobstrak.drawingfun.lib.okhttp3.internal.framed.HeadersMode r20) {
            /*
                r14 = this;
                r1 = r14
                r8 = r16
                r9 = r17
                r10 = r19
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this
                boolean r0 = r0.pushedStream(r9)
                if (r0 == 0) goto L_0x0015
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this
                r0.pushHeadersLater(r9, r10, r8)
                return
            L_0x0015:
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r11 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this
                monitor-enter(r11)
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this     // Catch:{ all -> 0x00b2 }
                boolean r0 = r0.shutdown     // Catch:{ all -> 0x00b2 }
                if (r0 == 0) goto L_0x0022
                monitor-exit(r11)     // Catch:{ all -> 0x00b2 }
                return
            L_0x0022:
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this     // Catch:{ all -> 0x00b2 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r0 = r0.getStream(r9)     // Catch:{ all -> 0x00b2 }
                if (r0 != 0) goto L_0x0095
                boolean r2 = r20.failIfStreamAbsent()     // Catch:{ all -> 0x00b2 }
                if (r2 == 0) goto L_0x0039
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this     // Catch:{ all -> 0x00b2 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.ErrorCode r3 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.ErrorCode.INVALID_STREAM     // Catch:{ all -> 0x00b2 }
                r2.writeSynResetLater(r9, r3)     // Catch:{ all -> 0x00b2 }
                monitor-exit(r11)     // Catch:{ all -> 0x00b2 }
                return
            L_0x0039:
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this     // Catch:{ all -> 0x00b2 }
                int r2 = r2.lastGoodStreamId     // Catch:{ all -> 0x00b2 }
                if (r9 > r2) goto L_0x0043
                monitor-exit(r11)     // Catch:{ all -> 0x00b2 }
                return
            L_0x0043:
                int r2 = r9 % 2
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r3 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this     // Catch:{ all -> 0x00b2 }
                int r3 = r3.nextStreamId     // Catch:{ all -> 0x00b2 }
                r12 = 2
                int r3 = r3 % r12
                if (r2 != r3) goto L_0x0051
                monitor-exit(r11)     // Catch:{ all -> 0x00b2 }
                return
            L_0x0051:
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r13 = new com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream     // Catch:{ all -> 0x00b2 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r4 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this     // Catch:{ all -> 0x00b2 }
                r2 = r13
                r3 = r17
                r5 = r15
                r6 = r16
                r7 = r19
                r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00b2 }
                r2 = r13
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r3 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this     // Catch:{ all -> 0x00b2 }
                int unused = r3.lastGoodStreamId = r9     // Catch:{ all -> 0x00b2 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r3 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this     // Catch:{ all -> 0x00b2 }
                java.util.Map r3 = r3.streams     // Catch:{ all -> 0x00b2 }
                java.lang.Integer r4 = java.lang.Integer.valueOf(r17)     // Catch:{ all -> 0x00b2 }
                r3.put(r4, r2)     // Catch:{ all -> 0x00b2 }
                java.util.concurrent.ExecutorService r3 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.executor     // Catch:{ all -> 0x00b2 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection$Reader$1 r4 = new com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection$Reader$1     // Catch:{ all -> 0x00b2 }
                java.lang.String r5 = "OkHttp %s stream %d"
                java.lang.Object[] r6 = new java.lang.Object[r12]     // Catch:{ all -> 0x00b2 }
                r7 = 0
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r12 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this     // Catch:{ all -> 0x00b2 }
                java.lang.String r12 = r12.hostname     // Catch:{ all -> 0x00b2 }
                r6[r7] = r12     // Catch:{ all -> 0x00b2 }
                r7 = 1
                java.lang.Integer r12 = java.lang.Integer.valueOf(r17)     // Catch:{ all -> 0x00b2 }
                r6[r7] = r12     // Catch:{ all -> 0x00b2 }
                r4.<init>(r5, r6, r2)     // Catch:{ all -> 0x00b2 }
                r3.execute(r4)     // Catch:{ all -> 0x00b2 }
                monitor-exit(r11)     // Catch:{ all -> 0x00b2 }
                return
            L_0x0095:
                monitor-exit(r11)     // Catch:{ all -> 0x00b2 }
                boolean r2 = r20.failIfStreamPresent()
                if (r2 == 0) goto L_0x00a7
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.ErrorCode r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.ErrorCode.PROTOCOL_ERROR
                r0.closeLater(r2)
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.this
                r2.removeStream(r9)
                return
            L_0x00a7:
                r2 = r20
                r0.receiveHeaders(r10, r2)
                if (r8 == 0) goto L_0x00b1
                r0.receiveFin()
            L_0x00b1:
                return
            L_0x00b2:
                r0 = move-exception
                r2 = r20
            L_0x00b5:
                monitor-exit(r11)     // Catch:{ all -> 0x00b7 }
                throw r0
            L_0x00b7:
                r0 = move-exception
                goto L_0x00b5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection.Reader.headers(boolean, boolean, int, int, java.util.List, com.jobstrak.drawingfun.lib.okhttp3.internal.framed.HeadersMode):void");
        }

        public void rstStream(int streamId, ErrorCode errorCode) {
            if (FramedConnection.this.pushedStream(streamId)) {
                FramedConnection.this.pushResetLater(streamId, errorCode);
                return;
            }
            FramedStream rstStream = FramedConnection.this.removeStream(streamId);
            if (rstStream != null) {
                rstStream.receiveRstStream(errorCode);
            }
        }

        public void settings(boolean clearPrevious, Settings newSettings) {
            int i;
            long delta = 0;
            FramedStream[] streamsToNotify = null;
            synchronized (FramedConnection.this) {
                int priorWriteWindowSize = FramedConnection.this.peerSettings.getInitialWindowSize(65536);
                if (clearPrevious) {
                    FramedConnection.this.peerSettings.clear();
                }
                FramedConnection.this.peerSettings.merge(newSettings);
                if (FramedConnection.this.getProtocol() == Protocol.HTTP_2) {
                    ackSettingsLater(newSettings);
                }
                int peerInitialWindowSize = FramedConnection.this.peerSettings.getInitialWindowSize(65536);
                if (!(peerInitialWindowSize == -1 || peerInitialWindowSize == priorWriteWindowSize)) {
                    delta = (long) (peerInitialWindowSize - priorWriteWindowSize);
                    if (!FramedConnection.this.receivedInitialPeerSettings) {
                        FramedConnection.this.addBytesToWriteWindow(delta);
                        boolean unused = FramedConnection.this.receivedInitialPeerSettings = true;
                    }
                    if (!FramedConnection.this.streams.isEmpty()) {
                        streamsToNotify = (FramedStream[]) FramedConnection.this.streams.values().toArray(new FramedStream[FramedConnection.this.streams.size()]);
                    }
                }
                FramedConnection.executor.execute(new NamedRunnable("OkHttp %s settings", FramedConnection.this.hostname) {
                    public void execute() {
                        FramedConnection.this.listener.onSettings(FramedConnection.this);
                    }
                });
            }
            if (streamsToNotify != null && delta != 0) {
                for (FramedStream stream : streamsToNotify) {
                    synchronized (stream) {
                        stream.addBytesToWriteWindow(delta);
                    }
                }
            }
        }

        private void ackSettingsLater(final Settings peerSettings) {
            FramedConnection.executor.execute(new NamedRunnable("OkHttp %s ACK Settings", new Object[]{FramedConnection.this.hostname}) {
                public void execute() {
                    try {
                        FramedConnection.this.frameWriter.ackSettings(peerSettings);
                    } catch (IOException e) {
                    }
                }
            });
        }

        public void ackSettings() {
        }

        public void ping(boolean reply, int payload1, int payload2) {
            if (reply) {
                Ping ping = FramedConnection.this.removePing(payload1);
                if (ping != null) {
                    ping.receive();
                    return;
                }
                return;
            }
            FramedConnection.this.writePingLater(true, payload1, payload2, null);
        }

        public void goAway(int lastGoodStreamId, ErrorCode errorCode, ByteString debugData) {
            FramedStream[] streamsCopy;
            debugData.size();
            synchronized (FramedConnection.this) {
                streamsCopy = (FramedStream[]) FramedConnection.this.streams.values().toArray(new FramedStream[FramedConnection.this.streams.size()]);
                boolean unused = FramedConnection.this.shutdown = true;
            }
            for (FramedStream framedStream : streamsCopy) {
                if (framedStream.getId() > lastGoodStreamId && framedStream.isLocallyInitiated()) {
                    framedStream.receiveRstStream(ErrorCode.REFUSED_STREAM);
                    FramedConnection.this.removeStream(framedStream.getId());
                }
            }
        }

        public void windowUpdate(int streamId, long windowSizeIncrement) {
            if (streamId == 0) {
                synchronized (FramedConnection.this) {
                    FramedConnection.this.bytesLeftInWriteWindow += windowSizeIncrement;
                    FramedConnection.this.notifyAll();
                }
                return;
            }
            FramedStream stream = FramedConnection.this.getStream(streamId);
            if (stream != null) {
                synchronized (stream) {
                    stream.addBytesToWriteWindow(windowSizeIncrement);
                }
            }
        }

        public void priority(int streamId, int streamDependency, int weight, boolean exclusive) {
        }

        public void pushPromise(int streamId, int promisedStreamId, List<Header> requestHeaders) {
            FramedConnection.this.pushRequestLater(promisedStreamId, requestHeaders);
        }

        public void alternateService(int streamId, String origin, ByteString protocol, String host, int port, long maxAge) {
        }
    }

    /* access modifiers changed from: private */
    public boolean pushedStream(int streamId) {
        return this.protocol == Protocol.HTTP_2 && streamId != 0 && (streamId & 1) == 0;
    }

    /* access modifiers changed from: private */
    public void pushRequestLater(int streamId, List<Header> requestHeaders) {
        synchronized (this) {
            if (this.currentPushRequests.contains(Integer.valueOf(streamId))) {
                writeSynResetLater(streamId, ErrorCode.PROTOCOL_ERROR);
                return;
            }
            this.currentPushRequests.add(Integer.valueOf(streamId));
            final int i = streamId;
            final List<Header> list = requestHeaders;
            this.pushExecutor.execute(new NamedRunnable("OkHttp %s Push Request[%s]", new Object[]{this.hostname, Integer.valueOf(streamId)}) {
                public void execute() {
                    if (FramedConnection.this.pushObserver.onRequest(i, list)) {
                        try {
                            FramedConnection.this.frameWriter.rstStream(i, ErrorCode.CANCEL);
                            synchronized (FramedConnection.this) {
                                FramedConnection.this.currentPushRequests.remove(Integer.valueOf(i));
                            }
                        } catch (IOException e) {
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void pushHeadersLater(int streamId, List<Header> requestHeaders, boolean inFinished) {
        final int i = streamId;
        final List<Header> list = requestHeaders;
        final boolean z = inFinished;
        this.pushExecutor.execute(new NamedRunnable("OkHttp %s Push Headers[%s]", new Object[]{this.hostname, Integer.valueOf(streamId)}) {
            public void execute() {
                boolean cancel = FramedConnection.this.pushObserver.onHeaders(i, list, z);
                if (cancel) {
                    try {
                        FramedConnection.this.frameWriter.rstStream(i, ErrorCode.CANCEL);
                    } catch (IOException e) {
                        return;
                    }
                }
                if (cancel || z) {
                    synchronized (FramedConnection.this) {
                        FramedConnection.this.currentPushRequests.remove(Integer.valueOf(i));
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void pushDataLater(int streamId, BufferedSource source, int byteCount, boolean inFinished) throws IOException {
        Buffer buffer = new Buffer();
        source.require((long) byteCount);
        source.read(buffer, (long) byteCount);
        if (buffer.size() == ((long) byteCount)) {
            final int i = streamId;
            final Buffer buffer2 = buffer;
            final int i2 = byteCount;
            final boolean z = inFinished;
            this.pushExecutor.execute(new NamedRunnable("OkHttp %s Push Data[%s]", new Object[]{this.hostname, Integer.valueOf(streamId)}) {
                public void execute() {
                    try {
                        boolean cancel = FramedConnection.this.pushObserver.onData(i, buffer2, i2, z);
                        if (cancel) {
                            FramedConnection.this.frameWriter.rstStream(i, ErrorCode.CANCEL);
                        }
                        if (cancel || z) {
                            synchronized (FramedConnection.this) {
                                FramedConnection.this.currentPushRequests.remove(Integer.valueOf(i));
                            }
                        }
                    } catch (IOException e) {
                    }
                }
            });
            return;
        }
        throw new IOException(buffer.size() + " != " + byteCount);
    }

    /* access modifiers changed from: private */
    public void pushResetLater(int streamId, ErrorCode errorCode) {
        final int i = streamId;
        final ErrorCode errorCode2 = errorCode;
        this.pushExecutor.execute(new NamedRunnable("OkHttp %s Push Reset[%s]", new Object[]{this.hostname, Integer.valueOf(streamId)}) {
            public void execute() {
                FramedConnection.this.pushObserver.onReset(i, errorCode2);
                synchronized (FramedConnection.this) {
                    FramedConnection.this.currentPushRequests.remove(Integer.valueOf(i));
                }
            }
        });
    }

    public static abstract class Listener {
        public static final Listener REFUSE_INCOMING_STREAMS = new Listener() {
            public void onStream(FramedStream stream) throws IOException {
                stream.close(ErrorCode.REFUSED_STREAM);
            }
        };

        public abstract void onStream(FramedStream framedStream) throws IOException;

        public void onSettings(FramedConnection connection) {
        }
    }
}
