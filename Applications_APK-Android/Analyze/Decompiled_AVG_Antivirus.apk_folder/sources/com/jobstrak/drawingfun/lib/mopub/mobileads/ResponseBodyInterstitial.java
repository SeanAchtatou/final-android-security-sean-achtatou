package com.jobstrak.drawingfun.lib.mopub.mobileads;

import android.content.Context;
import com.jobstrak.drawingfun.lib.mopub.common.DataKeys;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;
import com.jobstrak.drawingfun.lib.mopub.mobileads.CustomEventInterstitial;
import java.util.Map;

public abstract class ResponseBodyInterstitial extends CustomEventInterstitial {
    protected long mBroadcastIdentifier;
    private EventForwardingBroadcastReceiver mBroadcastReceiver;
    protected Context mContext;

    /* access modifiers changed from: protected */
    public abstract void extractExtras(Map<String, String> map);

    /* access modifiers changed from: protected */
    public abstract void preRenderHtml(CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener);

    public abstract void showInterstitial();

    public void loadInterstitial(Context context, CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, Map<String, Object> localExtras, Map<String, String> serverExtras) {
        this.mContext = context;
        if (extrasAreValid(serverExtras)) {
            extractExtras(serverExtras);
            try {
                Long boxedBroadcastId = (Long) localExtras.get(DataKeys.BROADCAST_IDENTIFIER_KEY);
                if (boxedBroadcastId == null) {
                    MoPubLog.e("Broadcast Identifier was not set in localExtras");
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
                    return;
                }
                this.mBroadcastIdentifier = boxedBroadcastId.longValue();
                this.mBroadcastReceiver = new EventForwardingBroadcastReceiver(customEventInterstitialListener, this.mBroadcastIdentifier);
                EventForwardingBroadcastReceiver eventForwardingBroadcastReceiver = this.mBroadcastReceiver;
                eventForwardingBroadcastReceiver.register(eventForwardingBroadcastReceiver, context);
                customEventInterstitialListener.onInterstitialLoaded();
            } catch (ClassCastException e) {
                MoPubLog.e("LocalExtras contained an incorrect type.");
                customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
            }
        } else {
            customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.NETWORK_INVALID_STATE);
        }
    }

    public void onInvalidate() {
        EventForwardingBroadcastReceiver eventForwardingBroadcastReceiver = this.mBroadcastReceiver;
        if (eventForwardingBroadcastReceiver != null) {
            eventForwardingBroadcastReceiver.unregister(eventForwardingBroadcastReceiver);
        }
    }

    private boolean extrasAreValid(Map<String, String> serverExtras) {
        return serverExtras.containsKey(DataKeys.HTML_RESPONSE_BODY_KEY);
    }
}
