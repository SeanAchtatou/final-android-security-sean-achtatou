package com.jobstrak.drawingfun.lib.urlbuilder.util;

public class RuntimeMalformedURLException extends RuntimeException {
    public RuntimeMalformedURLException(Throwable cause) {
        super(cause);
    }

    public Throwable fillInStackTrace() {
        return null;
    }
}
