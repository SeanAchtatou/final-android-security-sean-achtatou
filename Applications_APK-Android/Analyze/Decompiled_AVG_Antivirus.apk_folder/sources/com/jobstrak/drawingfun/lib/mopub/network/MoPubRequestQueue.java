package com.jobstrak.drawingfun.lib.mopub.network;

import android.os.Handler;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.mopub.common.Preconditions;
import com.jobstrak.drawingfun.lib.mopub.common.VisibleForTesting;
import com.mopub.volley.Cache;
import com.mopub.volley.Network;
import com.mopub.volley.Request;
import com.mopub.volley.RequestQueue;
import com.mopub.volley.ResponseDelivery;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MoPubRequestQueue extends RequestQueue {
    private static final int CAPACITY = 10;
    /* access modifiers changed from: private */
    @NonNull
    public final Map<Request<?>, DelayedRequestHelper> mDelayedRequests = new HashMap(10);

    MoPubRequestQueue(Cache cache, Network network, int threadPoolSize, ResponseDelivery delivery) {
        super(cache, network, threadPoolSize, delivery);
    }

    MoPubRequestQueue(Cache cache, Network network, int threadPoolSize) {
        super(cache, network, threadPoolSize);
    }

    MoPubRequestQueue(Cache cache, Network network) {
        super(cache, network);
    }

    public void addDelayedRequest(@NonNull Request<?> request, int delayMs) {
        Preconditions.checkNotNull(request);
        addDelayedRequest(request, new DelayedRequestHelper(this, request, delayMs));
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void addDelayedRequest(@NonNull Request<?> request, @NonNull DelayedRequestHelper delayedRequestHelper) {
        Preconditions.checkNotNull(delayedRequestHelper);
        if (this.mDelayedRequests.containsKey(request)) {
            cancel(request);
        }
        delayedRequestHelper.start();
        this.mDelayedRequests.put(request, delayedRequestHelper);
    }

    public void cancelAll(@NonNull RequestQueue.RequestFilter filter) {
        Preconditions.checkNotNull(filter);
        super.cancelAll(filter);
        Iterator<Map.Entry<Request<?>, DelayedRequestHelper>> iterator = this.mDelayedRequests.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Request<?>, DelayedRequestHelper> entry = iterator.next();
            if (filter.apply((Request) entry.getKey())) {
                ((Request) entry.getKey()).cancel();
                ((DelayedRequestHelper) entry.getValue()).cancel();
                iterator.remove();
            }
        }
    }

    public void cancelAll(@NonNull final Object tag) {
        Preconditions.checkNotNull(tag);
        super.cancelAll(tag);
        cancelAll((RequestQueue.RequestFilter) new RequestQueue.RequestFilter() {
            public boolean apply(Request<?> request) {
                return request.getTag() == tag;
            }
        });
    }

    public void cancel(@NonNull final Request<?> request) {
        Preconditions.checkNotNull(request);
        cancelAll((RequestQueue.RequestFilter) new RequestQueue.RequestFilter() {
            public boolean apply(Request<?> _request) {
                return request == _request;
            }
        });
    }

    class DelayedRequestHelper {
        final int mDelayMs;
        @NonNull
        final Runnable mDelayedRunnable;
        @NonNull
        final Handler mHandler;

        DelayedRequestHelper(@NonNull MoPubRequestQueue this$02, Request<?> request, int delayMs) {
            this(request, delayMs, new Handler());
        }

        @VisibleForTesting
        DelayedRequestHelper(@NonNull final Request<?> request, int delayMs, @NonNull Handler handler) {
            this.mDelayMs = delayMs;
            this.mHandler = handler;
            this.mDelayedRunnable = new Runnable(MoPubRequestQueue.this) {
                public void run() {
                    MoPubRequestQueue.this.mDelayedRequests.remove(request);
                    MoPubRequestQueue.this.add(request);
                }
            };
        }

        /* access modifiers changed from: package-private */
        public void start() {
            this.mHandler.postDelayed(this.mDelayedRunnable, (long) this.mDelayMs);
        }

        /* access modifiers changed from: package-private */
        public void cancel() {
            this.mHandler.removeCallbacks(this.mDelayedRunnable);
        }
    }

    /* access modifiers changed from: package-private */
    @NonNull
    @VisibleForTesting
    @Deprecated
    public Map<Request<?>, DelayedRequestHelper> getDelayedRequests() {
        return this.mDelayedRequests;
    }
}
