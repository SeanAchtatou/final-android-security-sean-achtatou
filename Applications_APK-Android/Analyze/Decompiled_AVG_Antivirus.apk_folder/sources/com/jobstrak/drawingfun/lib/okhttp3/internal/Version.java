package com.jobstrak.drawingfun.lib.okhttp3.internal;

public final class Version {
    public static String userAgent() {
        return "okhttp/3";
    }

    private Version() {
    }
}
