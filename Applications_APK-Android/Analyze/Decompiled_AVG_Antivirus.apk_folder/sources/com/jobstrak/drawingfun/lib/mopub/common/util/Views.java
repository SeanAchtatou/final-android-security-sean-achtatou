package com.jobstrak.drawingfun.lib.mopub.common.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;

public class Views {
    public static void removeFromParent(@Nullable View view) {
        if (view != null && view.getParent() != null && (view.getParent() instanceof ViewGroup)) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }

    @Nullable
    public static View getTopmostView(@Nullable Context context, @Nullable View view) {
        View rootViewFromActivity = getRootViewFromActivity(context);
        return rootViewFromActivity != null ? rootViewFromActivity : getRootViewFromView(view);
    }

    @Nullable
    private static View getRootViewFromActivity(@Nullable Context context) {
        if (!(context instanceof Activity)) {
            return null;
        }
        return ((Activity) context).getWindow().getDecorView().findViewById(16908290);
    }

    @Nullable
    private static View getRootViewFromView(@Nullable View view) {
        View rootView;
        if (view == null || (rootView = view.getRootView()) == null) {
            return null;
        }
        View rootContentView = rootView.findViewById(16908290);
        return rootContentView != null ? rootContentView : rootView;
    }
}
