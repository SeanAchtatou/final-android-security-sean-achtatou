package com.jobstrak.drawingfun.lib.mopub.mobileads;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.Constants;
import com.jobstrak.drawingfun.lib.mopub.common.DataKeys;
import com.jobstrak.drawingfun.lib.mopub.common.IntentActions;
import com.jobstrak.drawingfun.lib.mopub.common.VisibleForTesting;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;
import com.jobstrak.drawingfun.lib.mopub.mobileads.BaseInterstitialActivity;
import com.jobstrak.drawingfun.lib.mopub.mobileads.CustomEventInterstitial;
import com.jobstrak.drawingfun.lib.mopub.mraid.MraidController;
import com.jobstrak.drawingfun.lib.mopub.mraid.MraidWebViewClient;
import com.jobstrak.drawingfun.lib.mopub.mraid.MraidWebViewDebugListener;
import com.jobstrak.drawingfun.lib.mopub.mraid.PlacementType;
import com.jobstrak.drawingfun.lib.mopub.network.Networking;

public class MraidActivity extends BaseInterstitialActivity {
    @Nullable
    private MraidWebViewDebugListener mDebugListener;
    /* access modifiers changed from: private */
    @Nullable
    public MraidController mMraidController;

    public static void preRenderHtml(@NonNull Context context, @NonNull CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, @Nullable String htmlData) {
        preRenderHtml(customEventInterstitialListener, htmlData, new BaseWebView(context));
    }

    @VisibleForTesting
    static void preRenderHtml(@NonNull final CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, @Nullable String htmlData, @NonNull final BaseWebView dummyWebView) {
        dummyWebView.enablePlugins(false);
        dummyWebView.enableJavascriptCaching();
        dummyWebView.setWebViewClient(new MraidWebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                customEventInterstitialListener.onInterstitialLoaded();
                dummyWebView.loadUrl("javascript:mraidbridge.setState('default');");
                dummyWebView.loadUrl("javascript:mraidbridge.notifyReadyEvent();");
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.MRAID_LOAD_ERROR);
            }
        });
        dummyWebView.loadDataWithBaseURL(Networking.getBaseUrlScheme() + "://" + Constants.HOST + "/", htmlData, "text/html", "UTF-8", null);
    }

    public static void start(@NonNull Context context, @Nullable String htmlData, long broadcastIdentifier) {
        try {
            context.startActivity(createIntent(context, htmlData, broadcastIdentifier));
        } catch (ActivityNotFoundException e) {
            Log.d("MraidInterstitial", "MraidActivity.class not found. Did you declare MraidActivity in your manifest?");
        }
    }

    @VisibleForTesting
    protected static Intent createIntent(@NonNull Context context, @Nullable String htmlData, long broadcastIdentifier) {
        Intent intent = new Intent(context, MraidActivity.class);
        intent.putExtra(DataKeys.HTML_RESPONSE_BODY_KEY, htmlData);
        intent.putExtra(DataKeys.BROADCAST_IDENTIFIER_KEY, broadcastIdentifier);
        intent.addFlags(268435456);
        return intent;
    }

    public View getAdView() {
        String htmlData = getIntent().getStringExtra(DataKeys.HTML_RESPONSE_BODY_KEY);
        if (htmlData == null) {
            MoPubLog.w("MraidActivity received a null HTML body. Finishing the activity.");
            finish();
            return new View(this);
        }
        this.mMraidController = new MraidController(this, PlacementType.INTERSTITIAL);
        this.mMraidController.setDebugListener(this.mDebugListener);
        this.mMraidController.setMraidListener(new MraidController.MraidListener() {
            public void onLoaded(View view) {
                MraidActivity.this.mMraidController.loadJavascript(BaseInterstitialActivity.JavaScriptWebViewCallbacks.WEB_VIEW_DID_APPEAR.getJavascript());
            }

            public void onFailedToLoad() {
                MoPubLog.d("MraidActivity failed to load. Finishing the activity");
                if (MraidActivity.this.getBroadcastIdentifier() != null) {
                    MraidActivity mraidActivity = MraidActivity.this;
                    EventForwardingBroadcastReceiver.broadcastAction(mraidActivity, mraidActivity.getBroadcastIdentifier().longValue(), IntentActions.ACTION_INTERSTITIAL_FAIL);
                }
                MraidActivity.this.finish();
            }

            public void onClose() {
                MraidActivity.this.mMraidController.loadJavascript(BaseInterstitialActivity.JavaScriptWebViewCallbacks.WEB_VIEW_DID_CLOSE.getJavascript());
                MraidActivity.this.finish();
            }

            public void onExpand() {
            }

            public void onOpen() {
                if (MraidActivity.this.getBroadcastIdentifier() != null) {
                    MraidActivity mraidActivity = MraidActivity.this;
                    EventForwardingBroadcastReceiver.broadcastAction(mraidActivity, mraidActivity.getBroadcastIdentifier().longValue(), IntentActions.ACTION_INTERSTITIAL_CLICK);
                }
            }
        });
        this.mMraidController.setUseCustomCloseListener(new MraidController.UseCustomCloseListener() {
            public void useCustomCloseChanged(boolean useCustomClose) {
                if (useCustomClose) {
                    MraidActivity.this.hideInterstitialCloseButton();
                } else {
                    MraidActivity.this.showInterstitialCloseButton();
                }
            }
        });
        this.mMraidController.loadContent(htmlData);
        return this.mMraidController.getAdContainer();
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getBroadcastIdentifier() != null) {
            EventForwardingBroadcastReceiver.broadcastAction(this, getBroadcastIdentifier().longValue(), IntentActions.ACTION_INTERSTITIAL_SHOW);
        }
        getWindow().setFlags(16777216, 16777216);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        MraidController mraidController = this.mMraidController;
        if (mraidController != null) {
            mraidController.pause(isFinishing());
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MraidController mraidController = this.mMraidController;
        if (mraidController != null) {
            mraidController.resume();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        MraidController mraidController = this.mMraidController;
        if (mraidController != null) {
            mraidController.destroy();
        }
        if (getBroadcastIdentifier() != null) {
            EventForwardingBroadcastReceiver.broadcastAction(this, getBroadcastIdentifier().longValue(), IntentActions.ACTION_INTERSTITIAL_DISMISS);
        }
        super.onDestroy();
    }

    @VisibleForTesting
    public void setDebugListener(@Nullable MraidWebViewDebugListener debugListener) {
        this.mDebugListener = debugListener;
        MraidController mraidController = this.mMraidController;
        if (mraidController != null) {
            mraidController.setDebugListener(debugListener);
        }
    }
}
