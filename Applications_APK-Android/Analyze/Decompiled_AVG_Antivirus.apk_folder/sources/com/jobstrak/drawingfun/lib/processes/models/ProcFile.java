package com.jobstrak.drawingfun.lib.processes.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ProcFile extends File {
    public final String content;

    protected static String readFile(String path) throws IOException {
        BufferedReader reader = null;
        try {
            StringBuilder output = new StringBuilder();
            BufferedReader reader2 = new BufferedReader(new FileReader(path));
            String newLine = "";
            for (String line = reader2.readLine(); line != null; line = reader2.readLine()) {
                output.append(newLine);
                output.append(line);
                newLine = "\n";
            }
            String line2 = output.toString();
            reader2.close();
            return line2;
        } catch (Throwable th) {
            if (reader != null) {
                reader.close();
            }
            throw th;
        }
    }

    protected ProcFile(String path) throws IOException {
        super(path);
        this.content = readFile(path);
    }

    public long length() {
        return (long) this.content.length();
    }
}
