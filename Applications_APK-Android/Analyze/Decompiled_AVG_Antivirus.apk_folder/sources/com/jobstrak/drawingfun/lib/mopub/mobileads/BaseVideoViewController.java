package com.jobstrak.drawingfun.lib.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.IntentActions;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;

public abstract class BaseVideoViewController {
    private final BaseVideoViewControllerListener mBaseVideoViewControllerListener;
    @Nullable
    private Long mBroadcastIdentifier;
    private final Context mContext;
    private final RelativeLayout mLayout = new RelativeLayout(this.mContext);

    public interface BaseVideoViewControllerListener {
        void onFinish();

        void onSetContentView(View view);

        void onSetRequestedOrientation(int i);

        void onStartActivityForResult(Class<? extends Activity> cls, int i, Bundle bundle);
    }

    /* access modifiers changed from: protected */
    public abstract VideoView getVideoView();

    /* access modifiers changed from: protected */
    public abstract void onBackPressed();

    /* access modifiers changed from: protected */
    public abstract void onConfigurationChanged(Configuration configuration);

    /* access modifiers changed from: protected */
    public abstract void onDestroy();

    /* access modifiers changed from: protected */
    public abstract void onPause();

    /* access modifiers changed from: protected */
    public abstract void onResume();

    /* access modifiers changed from: protected */
    public abstract void onSaveInstanceState(@NonNull Bundle bundle);

    protected BaseVideoViewController(Context context, @Nullable Long broadcastIdentifier, BaseVideoViewControllerListener baseVideoViewControllerListener) {
        this.mContext = context;
        this.mBroadcastIdentifier = broadcastIdentifier;
        this.mBaseVideoViewControllerListener = baseVideoViewControllerListener;
    }

    /* access modifiers changed from: protected */
    public void onCreate() {
        RelativeLayout.LayoutParams adViewLayout = new RelativeLayout.LayoutParams(-1, -2);
        adViewLayout.addRule(13);
        this.mLayout.addView(getVideoView(), 0, adViewLayout);
        this.mBaseVideoViewControllerListener.onSetContentView(this.mLayout);
    }

    public boolean backButtonEnabled() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    /* access modifiers changed from: protected */
    public BaseVideoViewControllerListener getBaseVideoViewControllerListener() {
        return this.mBaseVideoViewControllerListener;
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return this.mContext;
    }

    public ViewGroup getLayout() {
        return this.mLayout;
    }

    /* access modifiers changed from: protected */
    public void videoError(boolean shouldFinish) {
        MoPubLog.e("Video cannot be played.");
        broadcastAction(IntentActions.ACTION_INTERSTITIAL_FAIL);
        if (shouldFinish) {
            this.mBaseVideoViewControllerListener.onFinish();
        }
    }

    /* access modifiers changed from: protected */
    public void videoCompleted(boolean shouldFinish) {
        if (shouldFinish) {
            this.mBaseVideoViewControllerListener.onFinish();
        }
    }

    /* access modifiers changed from: package-private */
    public void broadcastAction(String action) {
        Long l = this.mBroadcastIdentifier;
        if (l != null) {
            BaseBroadcastReceiver.broadcastAction(this.mContext, l.longValue(), action);
        } else {
            MoPubLog.w("Tried to broadcast a video event without a broadcast identifier to send to.");
        }
    }
}
