package com.jobstrak.drawingfun.lib.gson.internal;

import com.jobstrak.drawingfun.lib.gson.JsonElement;
import com.jobstrak.drawingfun.lib.gson.JsonIOException;
import com.jobstrak.drawingfun.lib.gson.JsonNull;
import com.jobstrak.drawingfun.lib.gson.JsonParseException;
import com.jobstrak.drawingfun.lib.gson.JsonSyntaxException;
import com.jobstrak.drawingfun.lib.gson.internal.bind.TypeAdapters;
import com.jobstrak.drawingfun.lib.gson.stream.JsonReader;
import com.jobstrak.drawingfun.lib.gson.stream.JsonWriter;
import com.jobstrak.drawingfun.lib.gson.stream.MalformedJsonException;
import java.io.EOFException;
import java.io.IOException;
import java.io.Writer;

public final class Streams {
    private Streams() {
        throw new UnsupportedOperationException();
    }

    public static JsonElement parse(JsonReader reader) throws JsonParseException {
        boolean isEmpty = true;
        try {
            reader.peek();
            isEmpty = false;
            return TypeAdapters.JSON_ELEMENT.read(reader);
        } catch (EOFException e) {
            if (isEmpty) {
                return JsonNull.INSTANCE;
            }
            throw new JsonSyntaxException(e);
        } catch (MalformedJsonException e2) {
            throw new JsonSyntaxException(e2);
        } catch (IOException e3) {
            throw new JsonIOException(e3);
        } catch (NumberFormatException e4) {
            throw new JsonSyntaxException(e4);
        }
    }

    public static void write(JsonElement element, JsonWriter writer) throws IOException {
        TypeAdapters.JSON_ELEMENT.write(writer, element);
    }

    public static Writer writerForAppendable(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new AppendableWriter(appendable);
    }

    private static final class AppendableWriter extends Writer {
        private final Appendable appendable;
        private final CurrentWrite currentWrite = new CurrentWrite();

        AppendableWriter(Appendable appendable2) {
            this.appendable = appendable2;
        }

        public void write(char[] chars, int offset, int length) throws IOException {
            CurrentWrite currentWrite2 = this.currentWrite;
            currentWrite2.chars = chars;
            this.appendable.append(currentWrite2, offset, offset + length);
        }

        public void write(int i) throws IOException {
            this.appendable.append((char) i);
        }

        public void flush() {
        }

        public void close() {
        }

        static class CurrentWrite implements CharSequence {
            char[] chars;

            CurrentWrite() {
            }

            public int length() {
                return this.chars.length;
            }

            public char charAt(int i) {
                return this.chars[i];
            }

            public CharSequence subSequence(int start, int end) {
                return new String(this.chars, start, end - start);
            }
        }
    }
}
