package com.jobstrak.drawingfun.lib.okhttp3.internal.framed;

import com.jobstrak.drawingfun.lib.okio.AsyncTimeout;
import com.jobstrak.drawingfun.lib.okio.Buffer;
import com.jobstrak.drawingfun.lib.okio.BufferedSource;
import com.jobstrak.drawingfun.lib.okio.Sink;
import com.jobstrak.drawingfun.lib.okio.Source;
import com.jobstrak.drawingfun.lib.okio.Timeout;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public final class FramedStream {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    long bytesLeftInWriteWindow;
    /* access modifiers changed from: private */
    public final FramedConnection connection;
    /* access modifiers changed from: private */
    public ErrorCode errorCode = null;
    /* access modifiers changed from: private */
    public final int id;
    /* access modifiers changed from: private */
    public final StreamTimeout readTimeout = new StreamTimeout();
    private final List<Header> requestHeaders;
    private List<Header> responseHeaders;
    final FramedDataSink sink;
    private final FramedDataSource source;
    long unacknowledgedBytesRead = 0;
    /* access modifiers changed from: private */
    public final StreamTimeout writeTimeout = new StreamTimeout();

    FramedStream(int id2, FramedConnection connection2, boolean outFinished, boolean inFinished, List<Header> requestHeaders2) {
        if (connection2 == null) {
            throw new NullPointerException("connection == null");
        } else if (requestHeaders2 != null) {
            this.id = id2;
            this.connection = connection2;
            this.bytesLeftInWriteWindow = (long) connection2.peerSettings.getInitialWindowSize(65536);
            this.source = new FramedDataSource((long) connection2.okHttpSettings.getInitialWindowSize(65536));
            this.sink = new FramedDataSink();
            boolean unused = this.source.finished = inFinished;
            boolean unused2 = this.sink.finished = outFinished;
            this.requestHeaders = requestHeaders2;
        } else {
            throw new NullPointerException("requestHeaders == null");
        }
    }

    public int getId() {
        return this.id;
    }

    public synchronized boolean isOpen() {
        if (this.errorCode != null) {
            return false;
        }
        if ((this.source.finished || this.source.closed) && ((this.sink.finished || this.sink.closed) && this.responseHeaders != null)) {
            return false;
        }
        return true;
    }

    public boolean isLocallyInitiated() {
        if (this.connection.client == ((this.id & 1) == 1)) {
            return true;
        }
        return false;
    }

    public FramedConnection getConnection() {
        return this.connection;
    }

    public List<Header> getRequestHeaders() {
        return this.requestHeaders;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.jobstrak.drawingfun.lib.okhttp3.internal.framed.Header> getResponseHeaders() throws java.io.IOException {
        /*
            r3 = this;
            monitor-enter(r3)
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream$StreamTimeout r0 = r3.readTimeout     // Catch:{ all -> 0x0042 }
            r0.enter()     // Catch:{ all -> 0x0042 }
        L_0x0006:
            java.util.List<com.jobstrak.drawingfun.lib.okhttp3.internal.framed.Header> r0 = r3.responseHeaders     // Catch:{ all -> 0x003b }
            if (r0 != 0) goto L_0x0014
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.ErrorCode r0 = r3.errorCode     // Catch:{ all -> 0x0012 }
            if (r0 != 0) goto L_0x0014
            r3.waitForIo()     // Catch:{ all -> 0x0012 }
            goto L_0x0006
        L_0x0012:
            r0 = move-exception
            goto L_0x003c
        L_0x0014:
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream$StreamTimeout r0 = r3.readTimeout     // Catch:{ all -> 0x0042 }
            r0.exitAndThrowIfTimedOut()     // Catch:{ all -> 0x0042 }
            java.util.List<com.jobstrak.drawingfun.lib.okhttp3.internal.framed.Header> r0 = r3.responseHeaders     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x0022
            java.util.List<com.jobstrak.drawingfun.lib.okhttp3.internal.framed.Header> r0 = r3.responseHeaders     // Catch:{ all -> 0x0042 }
            monitor-exit(r3)
            return r0
        L_0x0022:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0042 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0042 }
            r1.<init>()     // Catch:{ all -> 0x0042 }
            java.lang.String r2 = "stream was reset: "
            r1.append(r2)     // Catch:{ all -> 0x0042 }
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.ErrorCode r2 = r3.errorCode     // Catch:{ all -> 0x0042 }
            r1.append(r2)     // Catch:{ all -> 0x0042 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0042 }
            r0.<init>(r1)     // Catch:{ all -> 0x0042 }
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x003b:
            r0 = move-exception
        L_0x003c:
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream$StreamTimeout r1 = r3.readTimeout     // Catch:{ all -> 0x0042 }
            r1.exitAndThrowIfTimedOut()     // Catch:{ all -> 0x0042 }
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x0042:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.getResponseHeaders():java.util.List");
    }

    public synchronized ErrorCode getErrorCode() {
        return this.errorCode;
    }

    public void reply(List<Header> responseHeaders2, boolean out) throws IOException {
        boolean outFinished = false;
        synchronized (this) {
            if (responseHeaders2 != null) {
                try {
                    if (this.responseHeaders == null) {
                        this.responseHeaders = responseHeaders2;
                        if (!out) {
                            boolean unused = this.sink.finished = true;
                            outFinished = true;
                        }
                    } else {
                        throw new IllegalStateException("reply already sent");
                    }
                } catch (Throwable th) {
                    throw th;
                }
            } else {
                throw new NullPointerException("responseHeaders == null");
            }
        }
        this.connection.writeSynReply(this.id, outFinished, responseHeaders2);
        if (outFinished) {
            this.connection.flush();
        }
    }

    public Timeout readTimeout() {
        return this.readTimeout;
    }

    public Timeout writeTimeout() {
        return this.writeTimeout;
    }

    public Source getSource() {
        return this.source;
    }

    public Sink getSink() {
        synchronized (this) {
            if (this.responseHeaders == null) {
                if (!isLocallyInitiated()) {
                    throw new IllegalStateException("reply before requesting the sink");
                }
            }
        }
        return this.sink;
    }

    public void close(ErrorCode rstStatusCode) throws IOException {
        if (closeInternal(rstStatusCode)) {
            this.connection.writeSynReset(this.id, rstStatusCode);
        }
    }

    public void closeLater(ErrorCode errorCode2) {
        if (closeInternal(errorCode2)) {
            this.connection.writeSynResetLater(this.id, errorCode2);
        }
    }

    private boolean closeInternal(ErrorCode errorCode2) {
        synchronized (this) {
            if (this.errorCode != null) {
                return false;
            }
            if (this.source.finished && this.sink.finished) {
                return false;
            }
            this.errorCode = errorCode2;
            notifyAll();
            this.connection.removeStream(this.id);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void receiveHeaders(List<Header> headers, HeadersMode headersMode) {
        ErrorCode errorCode2 = null;
        boolean open = true;
        synchronized (this) {
            if (this.responseHeaders == null) {
                if (headersMode.failIfHeadersAbsent()) {
                    errorCode2 = ErrorCode.PROTOCOL_ERROR;
                } else {
                    this.responseHeaders = headers;
                    open = isOpen();
                    notifyAll();
                }
            } else if (headersMode.failIfHeadersPresent()) {
                errorCode2 = ErrorCode.STREAM_IN_USE;
            } else {
                List<Header> newHeaders = new ArrayList<>();
                newHeaders.addAll(this.responseHeaders);
                newHeaders.addAll(headers);
                this.responseHeaders = newHeaders;
            }
        }
        if (errorCode2 != null) {
            closeLater(errorCode2);
        } else if (!open) {
            this.connection.removeStream(this.id);
        }
    }

    /* access modifiers changed from: package-private */
    public void receiveData(BufferedSource in, int length) throws IOException {
        this.source.receive(in, (long) length);
    }

    /* access modifiers changed from: package-private */
    public void receiveFin() {
        boolean open;
        synchronized (this) {
            boolean unused = this.source.finished = true;
            open = isOpen();
            notifyAll();
        }
        if (!open) {
            this.connection.removeStream(this.id);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void receiveRstStream(ErrorCode errorCode2) {
        if (this.errorCode == null) {
            this.errorCode = errorCode2;
            notifyAll();
        }
    }

    private final class FramedDataSource implements Source {
        static final /* synthetic */ boolean $assertionsDisabled = false;
        /* access modifiers changed from: private */
        public boolean closed;
        /* access modifiers changed from: private */
        public boolean finished;
        private final long maxByteCount;
        private final Buffer readBuffer;
        private final Buffer receiveBuffer;

        private FramedDataSource(long maxByteCount2) {
            this.receiveBuffer = new Buffer();
            this.readBuffer = new Buffer();
            this.maxByteCount = maxByteCount2;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0065, code lost:
            r5 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$500(r11.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x006b, code lost:
            monitor-enter(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$500(r11.this$0).unacknowledgedBytesRead += r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0090, code lost:
            if (com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$500(r11.this$0).unacknowledgedBytesRead < ((long) (com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$500(r11.this$0).okHttpSettings.getInitialWindowSize(65536) / 2))) goto L_0x00ac;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0092, code lost:
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$500(r11.this$0).writeWindowUpdateLater(0, com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$500(r11.this$0).unacknowledgedBytesRead);
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$500(r11.this$0).unacknowledgedBytesRead = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x00ac, code lost:
            monitor-exit(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00ad, code lost:
            return r3;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public long read(com.jobstrak.drawingfun.lib.okio.Buffer r12, long r13) throws java.io.IOException {
            /*
                r11 = this;
                r0 = 0
                int r2 = (r13 > r0 ? 1 : (r13 == r0 ? 0 : -1))
                if (r2 < 0) goto L_0x00b4
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this
                monitor-enter(r2)
                r11.waitUntilReadable()     // Catch:{ all -> 0x00b1 }
                r11.checkNotClosed()     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okio.Buffer r3 = r11.readBuffer     // Catch:{ all -> 0x00b1 }
                long r3 = r3.size()     // Catch:{ all -> 0x00b1 }
                int r5 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
                if (r5 != 0) goto L_0x001d
                r0 = -1
                monitor-exit(r2)     // Catch:{ all -> 0x00b1 }
                return r0
            L_0x001d:
                com.jobstrak.drawingfun.lib.okio.Buffer r3 = r11.readBuffer     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okio.Buffer r4 = r11.readBuffer     // Catch:{ all -> 0x00b1 }
                long r4 = r4.size()     // Catch:{ all -> 0x00b1 }
                long r4 = java.lang.Math.min(r13, r4)     // Catch:{ all -> 0x00b1 }
                long r3 = r3.read(r12, r4)     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r5 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00b1 }
                long r6 = r5.unacknowledgedBytesRead     // Catch:{ all -> 0x00b1 }
                long r6 = r6 + r3
                r5.unacknowledgedBytesRead = r6     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r5 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00b1 }
                long r5 = r5.unacknowledgedBytesRead     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r7 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r7 = r7.connection     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.Settings r7 = r7.okHttpSettings     // Catch:{ all -> 0x00b1 }
                r8 = 65536(0x10000, float:9.18355E-41)
                int r7 = r7.getInitialWindowSize(r8)     // Catch:{ all -> 0x00b1 }
                int r7 = r7 / 2
                long r9 = (long) r7     // Catch:{ all -> 0x00b1 }
                int r7 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
                if (r7 < 0) goto L_0x0064
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r5 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r5 = r5.connection     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r6 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00b1 }
                int r6 = r6.id     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r7 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00b1 }
                long r9 = r7.unacknowledgedBytesRead     // Catch:{ all -> 0x00b1 }
                r5.writeWindowUpdateLater(r6, r9)     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r5 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00b1 }
                r5.unacknowledgedBytesRead = r0     // Catch:{ all -> 0x00b1 }
            L_0x0064:
                monitor-exit(r2)     // Catch:{ all -> 0x00b1 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r5 = r2.connection
                monitor-enter(r5)
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00ae }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r2 = r2.connection     // Catch:{ all -> 0x00ae }
                long r6 = r2.unacknowledgedBytesRead     // Catch:{ all -> 0x00ae }
                long r6 = r6 + r3
                r2.unacknowledgedBytesRead = r6     // Catch:{ all -> 0x00ae }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00ae }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r2 = r2.connection     // Catch:{ all -> 0x00ae }
                long r6 = r2.unacknowledgedBytesRead     // Catch:{ all -> 0x00ae }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00ae }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r2 = r2.connection     // Catch:{ all -> 0x00ae }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.Settings r2 = r2.okHttpSettings     // Catch:{ all -> 0x00ae }
                int r2 = r2.getInitialWindowSize(r8)     // Catch:{ all -> 0x00ae }
                int r2 = r2 / 2
                long r8 = (long) r2     // Catch:{ all -> 0x00ae }
                int r2 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
                if (r2 < 0) goto L_0x00ac
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00ae }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r2 = r2.connection     // Catch:{ all -> 0x00ae }
                r6 = 0
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r7 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00ae }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r7 = r7.connection     // Catch:{ all -> 0x00ae }
                long r7 = r7.unacknowledgedBytesRead     // Catch:{ all -> 0x00ae }
                r2.writeWindowUpdateLater(r6, r7)     // Catch:{ all -> 0x00ae }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this     // Catch:{ all -> 0x00ae }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r2 = r2.connection     // Catch:{ all -> 0x00ae }
                r2.unacknowledgedBytesRead = r0     // Catch:{ all -> 0x00ae }
            L_0x00ac:
                monitor-exit(r5)     // Catch:{ all -> 0x00ae }
                return r3
            L_0x00ae:
                r0 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x00ae }
                throw r0
            L_0x00b1:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x00b1 }
                throw r0
            L_0x00b4:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "byteCount < 0: "
                r1.append(r2)
                r1.append(r13)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.FramedDataSource.read(com.jobstrak.drawingfun.lib.okio.Buffer, long):long");
        }

        private void waitUntilReadable() throws IOException {
            FramedStream.this.readTimeout.enter();
            while (this.readBuffer.size() == 0 && !this.finished && !this.closed && FramedStream.this.errorCode == null) {
                try {
                    FramedStream.this.waitForIo();
                } finally {
                    FramedStream.this.readTimeout.exitAndThrowIfTimedOut();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void receive(BufferedSource in, long byteCount) throws IOException {
            boolean finished2;
            boolean z;
            boolean flowControlError;
            while (byteCount > 0) {
                synchronized (FramedStream.this) {
                    finished2 = this.finished;
                    z = true;
                    flowControlError = this.readBuffer.size() + byteCount > this.maxByteCount;
                }
                if (flowControlError) {
                    in.skip(byteCount);
                    FramedStream.this.closeLater(ErrorCode.FLOW_CONTROL_ERROR);
                    return;
                } else if (finished2) {
                    in.skip(byteCount);
                    return;
                } else {
                    long read = in.read(this.receiveBuffer, byteCount);
                    if (read != -1) {
                        long byteCount2 = byteCount - read;
                        synchronized (FramedStream.this) {
                            if (this.readBuffer.size() != 0) {
                                z = false;
                            }
                            boolean wasEmpty = z;
                            this.readBuffer.writeAll(this.receiveBuffer);
                            if (wasEmpty) {
                                FramedStream.this.notifyAll();
                            }
                        }
                        byteCount = byteCount2;
                    } else {
                        throw new EOFException();
                    }
                }
            }
        }

        public Timeout timeout() {
            return FramedStream.this.readTimeout;
        }

        public void close() throws IOException {
            synchronized (FramedStream.this) {
                this.closed = true;
                this.readBuffer.clear();
                FramedStream.this.notifyAll();
            }
            FramedStream.this.cancelStreamIfNecessary();
        }

        private void checkNotClosed() throws IOException {
            if (this.closed) {
                throw new IOException("stream closed");
            } else if (FramedStream.this.errorCode != null) {
                throw new IOException("stream was reset: " + FramedStream.this.errorCode);
            }
        }
    }

    /* access modifiers changed from: private */
    public void cancelStreamIfNecessary() throws IOException {
        boolean cancel;
        boolean open;
        synchronized (this) {
            cancel = !this.source.finished && this.source.closed && (this.sink.finished || this.sink.closed);
            open = isOpen();
        }
        if (cancel) {
            close(ErrorCode.CANCEL);
        } else if (!open) {
            this.connection.removeStream(this.id);
        }
    }

    final class FramedDataSink implements Sink {
        static final /* synthetic */ boolean $assertionsDisabled = false;
        private static final long EMIT_BUFFER_SIZE = 16384;
        /* access modifiers changed from: private */
        public boolean closed;
        /* access modifiers changed from: private */
        public boolean finished;
        private final Buffer sendBuffer = new Buffer();

        FramedDataSink() {
        }

        public void write(Buffer source, long byteCount) throws IOException {
            this.sendBuffer.write(source, byteCount);
            while (this.sendBuffer.size() >= EMIT_BUFFER_SIZE) {
                emitDataFrame(false);
            }
        }

        /* JADX INFO: finally extract failed */
        private void emitDataFrame(boolean outFinished) throws IOException {
            long toWrite;
            synchronized (FramedStream.this) {
                FramedStream.this.writeTimeout.enter();
                while (FramedStream.this.bytesLeftInWriteWindow <= 0 && !this.finished && !this.closed && FramedStream.this.errorCode == null) {
                    try {
                        FramedStream.this.waitForIo();
                    } catch (Throwable th) {
                        FramedStream.this.writeTimeout.exitAndThrowIfTimedOut();
                        throw th;
                    }
                }
                FramedStream.this.writeTimeout.exitAndThrowIfTimedOut();
                FramedStream.this.checkOutNotClosed();
                toWrite = Math.min(FramedStream.this.bytesLeftInWriteWindow, this.sendBuffer.size());
                FramedStream.this.bytesLeftInWriteWindow -= toWrite;
            }
            FramedStream.this.writeTimeout.enter();
            try {
                FramedStream.this.connection.writeData(FramedStream.this.id, outFinished && toWrite == this.sendBuffer.size(), this.sendBuffer, toWrite);
            } finally {
                FramedStream.this.writeTimeout.exitAndThrowIfTimedOut();
            }
        }

        public void flush() throws IOException {
            synchronized (FramedStream.this) {
                FramedStream.this.checkOutNotClosed();
            }
            while (this.sendBuffer.size() > 0) {
                emitDataFrame(false);
                FramedStream.this.connection.flush();
            }
        }

        public Timeout timeout() {
            return FramedStream.this.writeTimeout;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
            if (r8.sendBuffer.size() <= 0) goto L_0x002e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
            if (r8.sendBuffer.size() <= 0) goto L_0x0041;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x002a, code lost:
            emitDataFrame(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$500(r8.this$0).writeData(com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$600(r8.this$0), true, null, 0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0041, code lost:
            r2 = r8.this$0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0043, code lost:
            monitor-enter(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            r8.closed = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0046, code lost:
            monitor-exit(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0047, code lost:
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$500(r8.this$0).flush();
            com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.access$1000(r8.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0055, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
            if (r8.this$0.sink.finished != false) goto L_0x0041;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() throws java.io.IOException {
            /*
                r8 = this;
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this
                monitor-enter(r0)
                boolean r1 = r8.closed     // Catch:{ all -> 0x0059 }
                if (r1 == 0) goto L_0x000a
                monitor-exit(r0)     // Catch:{ all -> 0x0059 }
                return
            L_0x000a:
                monitor-exit(r0)     // Catch:{ all -> 0x0059 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream$FramedDataSink r0 = r0.sink
                boolean r0 = r0.finished
                r1 = 1
                if (r0 != 0) goto L_0x0041
                com.jobstrak.drawingfun.lib.okio.Buffer r0 = r8.sendBuffer
                long r2 = r0.size()
                r4 = 0
                int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x002e
            L_0x0020:
                com.jobstrak.drawingfun.lib.okio.Buffer r0 = r8.sendBuffer
                long r2 = r0.size()
                int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0041
                r8.emitDataFrame(r1)
                goto L_0x0020
            L_0x002e:
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r2 = r0.connection
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this
                int r3 = r0.id
                r4 = 1
                r5 = 0
                r6 = 0
                r2.writeData(r3, r4, r5, r6)
            L_0x0041:
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this
                monitor-enter(r2)
                r8.closed = r1     // Catch:{ all -> 0x0056 }
                monitor-exit(r2)     // Catch:{ all -> 0x0056 }
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedConnection r0 = r0.connection
                r0.flush()
                com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.this
                r0.cancelStreamIfNecessary()
                return
            L_0x0056:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0056 }
                throw r0
            L_0x0059:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0059 }
                throw r1
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.okhttp3.internal.framed.FramedStream.FramedDataSink.close():void");
        }
    }

    /* access modifiers changed from: package-private */
    public void addBytesToWriteWindow(long delta) {
        this.bytesLeftInWriteWindow += delta;
        if (delta > 0) {
            notifyAll();
        }
    }

    /* access modifiers changed from: private */
    public void checkOutNotClosed() throws IOException {
        if (this.sink.closed) {
            throw new IOException("stream closed");
        } else if (this.sink.finished) {
            throw new IOException("stream finished");
        } else if (this.errorCode != null) {
            throw new IOException("stream was reset: " + this.errorCode);
        }
    }

    /* access modifiers changed from: private */
    public void waitForIo() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException e) {
            throw new InterruptedIOException();
        }
    }

    class StreamTimeout extends AsyncTimeout {
        StreamTimeout() {
        }

        /* access modifiers changed from: protected */
        public void timedOut() {
            FramedStream.this.closeLater(ErrorCode.CANCEL);
        }

        /* access modifiers changed from: protected */
        public IOException newTimeoutException(IOException cause) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (cause != null) {
                socketTimeoutException.initCause(cause);
            }
            return socketTimeoutException;
        }

        public void exitAndThrowIfTimedOut() throws IOException {
            if (exit()) {
                throw newTimeoutException(null);
            }
        }
    }
}
