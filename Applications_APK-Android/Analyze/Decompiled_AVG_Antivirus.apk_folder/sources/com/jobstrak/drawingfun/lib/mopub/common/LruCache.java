package com.jobstrak.drawingfun.lib.mopub.common;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<K, V> {
    private int createCount;
    private int evictionCount;
    private int hitCount;
    private final LinkedHashMap<K, V> map;
    private int maxSize;
    private int missCount;
    private int putCount;
    private int size;

    public LruCache(int maxSize2) {
        if (maxSize2 > 0) {
            this.maxSize = maxSize2;
            this.map = new LinkedHashMap<>(0, 0.75f, true);
            return;
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003a, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized V get(K r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            if (r4 == 0) goto L_0x003d
            java.util.LinkedHashMap<K, V> r0 = r3.map     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x0013
            int r1 = r3.hitCount     // Catch:{ all -> 0x003b }
            int r1 = r1 + 1
            r3.hitCount = r1     // Catch:{ all -> 0x003b }
            monitor-exit(r3)
            return r0
        L_0x0013:
            int r1 = r3.missCount     // Catch:{ all -> 0x003b }
            int r1 = r1 + 1
            r3.missCount = r1     // Catch:{ all -> 0x003b }
            java.lang.Object r1 = r3.create(r4)     // Catch:{ all -> 0x003b }
            r0 = r1
            if (r0 == 0) goto L_0x0039
            int r1 = r3.createCount     // Catch:{ all -> 0x003b }
            int r1 = r1 + 1
            r3.createCount = r1     // Catch:{ all -> 0x003b }
            int r1 = r3.size     // Catch:{ all -> 0x003b }
            int r2 = r3.safeSizeOf(r4, r0)     // Catch:{ all -> 0x003b }
            int r1 = r1 + r2
            r3.size = r1     // Catch:{ all -> 0x003b }
            java.util.LinkedHashMap<K, V> r1 = r3.map     // Catch:{ all -> 0x003b }
            r1.put(r4, r0)     // Catch:{ all -> 0x003b }
            int r1 = r3.maxSize     // Catch:{ all -> 0x003b }
            r3.trimToSize(r1)     // Catch:{ all -> 0x003b }
        L_0x0039:
            monitor-exit(r3)
            return r0
        L_0x003b:
            r4 = move-exception
            goto L_0x0045
        L_0x003d:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ all -> 0x003b }
            java.lang.String r1 = "key == null"
            r0.<init>(r1)     // Catch:{ all -> 0x003b }
            throw r0     // Catch:{ all -> 0x003b }
        L_0x0045:
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.mopub.common.LruCache.get(java.lang.Object):java.lang.Object");
    }

    public final synchronized V put(K key, V value) {
        V previous;
        if (key == null || value == null) {
            throw new NullPointerException("key == null || value == null");
        }
        this.putCount++;
        this.size += safeSizeOf(key, value);
        previous = this.map.put(key, value);
        if (previous != null) {
            this.size -= safeSizeOf(key, previous);
        }
        trimToSize(this.maxSize);
        return previous;
    }

    private void trimToSize(int maxSize2) {
        Map.Entry<K, V> toEvict;
        while (this.size > maxSize2 && !this.map.isEmpty() && (toEvict = this.map.entrySet().iterator().next()) != null) {
            K key = toEvict.getKey();
            V value = toEvict.getValue();
            this.map.remove(key);
            this.size -= safeSizeOf(key, value);
            this.evictionCount++;
            entryEvicted(key, value);
        }
        if (this.size < 0 || (this.map.isEmpty() && this.size != 0)) {
            throw new IllegalStateException(getClass().getName() + ".sizeOf() is reporting inconsistent results!");
        }
    }

    public final synchronized V remove(K key) {
        V previous;
        if (key != null) {
            previous = this.map.remove(key);
            if (previous != null) {
                this.size -= safeSizeOf(key, previous);
            }
        } else {
            throw new NullPointerException("key == null");
        }
        return previous;
    }

    /* access modifiers changed from: protected */
    public void entryEvicted(K k, V v) {
    }

    /* access modifiers changed from: protected */
    public V create(K k) {
        return null;
    }

    private int safeSizeOf(K key, V value) {
        int result = sizeOf(key, value);
        if (result >= 0) {
            return result;
        }
        throw new IllegalStateException("Negative size: " + ((Object) key) + "=" + ((Object) value));
    }

    /* access modifiers changed from: protected */
    public int sizeOf(K k, V v) {
        return 1;
    }

    public final synchronized void evictAll() {
        trimToSize(-1);
    }

    public final synchronized int size() {
        return this.size;
    }

    public final synchronized int maxSize() {
        return this.maxSize;
    }

    public final synchronized int hitCount() {
        return this.hitCount;
    }

    public final synchronized int missCount() {
        return this.missCount;
    }

    public final synchronized int createCount() {
        return this.createCount;
    }

    public final synchronized int putCount() {
        return this.putCount;
    }

    public final synchronized int evictionCount() {
        return this.evictionCount;
    }

    public final synchronized Map<K, V> snapshot() {
        return new LinkedHashMap(this.map);
    }

    public final synchronized String toString() {
        int accesses;
        accesses = this.hitCount + this.missCount;
        return String.format("LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", Integer.valueOf(this.maxSize), Integer.valueOf(this.hitCount), Integer.valueOf(this.missCount), Integer.valueOf(accesses != 0 ? (this.hitCount * 100) / accesses : 0));
    }
}
