package com.jobstrak.drawingfun.lib.urlbuilder.util;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

public class UrlParameterMultimap implements Map<String, List<String>> {
    private final List<Map.Entry<String, String>> data;

    public /* bridge */ /* synthetic */ Object put(Object obj, Object obj2) {
        return put((String) obj, (List<String>) ((List) obj2));
    }

    public static final class Immutable extends UrlParameterMultimap {
        public /* bridge */ /* synthetic */ Object get(Object obj) {
            return UrlParameterMultimap.super.get(obj);
        }

        public /* bridge */ /* synthetic */ Object put(Object obj, Object obj2) {
            return UrlParameterMultimap.super.put((String) obj, (List<String>) ((List) obj2));
        }

        public /* bridge */ /* synthetic */ Object remove(Object obj) {
            return UrlParameterMultimap.super.remove(obj);
        }

        Immutable(List<Map.Entry<String, String>> data) {
            super(Collections.unmodifiableList(new LinkedList(data)));
        }
    }

    private UrlParameterMultimap(List<Map.Entry<String, String>> data2) {
        this.data = data2;
    }

    public int size() {
        return this.data.size();
    }

    public boolean isEmpty() {
        return this.data.isEmpty();
    }

    private static Map.Entry<String, String> newEntry(String key, String value) {
        return new AbstractMap.SimpleImmutableEntry(key, value);
    }

    public static UrlParameterMultimap newMultimap() {
        return new UrlParameterMultimap(new LinkedList());
    }

    public UrlParameterMultimap deepCopy() {
        return new UrlParameterMultimap(new LinkedList(this.data));
    }

    public Immutable immutable() {
        if (this instanceof Immutable) {
            return (Immutable) this;
        }
        return new Immutable(this.data);
    }

    public boolean containsKey(Object key) {
        if (key != null) {
            for (Map.Entry<String, String> e : this.data) {
                if (key.equals(e.getKey())) {
                    return true;
                }
            }
            return false;
        }
        throw new IllegalArgumentException("key can't be null");
    }

    public boolean containsValue(Object value) {
        if (value != null) {
            for (Map.Entry<String, String> e : this.data) {
                if (value.equals(e.getValue())) {
                    return true;
                }
            }
            return false;
        }
        throw new IllegalArgumentException("value can't be null");
    }

    public List<String> get(Object key) {
        List<String> ret = new ArrayList<>();
        for (Map.Entry<String, String> e : this.data) {
            if (key.equals(e.getKey())) {
                ret.add(e.getValue());
            }
        }
        if (ret.isEmpty()) {
            return null;
        }
        return ret;
    }

    public UrlParameterMultimap add(String key, String value) {
        this.data.add(newEntry(key, value));
        return this;
    }

    public UrlParameterMultimap replaceValues(String key, String value) {
        remove((Object) key);
        add(key, value);
        return this;
    }

    public List<String> put(String key, List<String> value) {
        List<String> overflow = new ArrayList<>(value);
        ListIterator<Map.Entry<String, String>> it = this.data.listIterator();
        while (it.hasNext()) {
            Map.Entry<String, String> e = it.next();
            if (key.equals(e.getKey()) && value.contains(e.getValue())) {
                overflow.remove(e.getValue());
            } else if (key.equals(e.getKey())) {
                it.remove();
            }
        }
        for (String v : overflow) {
            add(key, v);
        }
        return null;
    }

    public List<String> remove(Object key) {
        if (key != null) {
            List<String> ret = new ArrayList<>();
            ListIterator<Map.Entry<String, String>> it = this.data.listIterator();
            while (it.hasNext()) {
                Map.Entry<String, String> e = it.next();
                if (key.equals(e.getKey())) {
                    ret.add(e.getValue());
                    it.remove();
                }
            }
            return ret;
        }
        throw new IllegalArgumentException("can't remove null");
    }

    public UrlParameterMultimap removeAllValues(String key) {
        remove((Object) key);
        return this;
    }

    public UrlParameterMultimap remove(String key, String value) {
        if (key == null || value == null) {
            throw new IllegalArgumentException("can't remove null");
        }
        ListIterator<Map.Entry<String, String>> it = this.data.listIterator();
        while (it.hasNext()) {
            Map.Entry<String, String> e = it.next();
            if (key.equals(e.getKey()) && value.equals(e.getValue())) {
                it.remove();
            }
        }
        return this;
    }

    public void putAll(Map<? extends String, ? extends List<String>> m) {
        for (Map.Entry<? extends String, ? extends List<String>> e : m.entrySet()) {
            put((String) e.getKey(), (List<String>) ((List) e.getValue()));
        }
    }

    public void clear() {
        this.data.clear();
    }

    public Set<String> keySet() {
        Set<String> ret = new HashSet<>();
        for (Map.Entry<String, String> e : this.data) {
            ret.add(e.getKey());
        }
        return ret;
    }

    public List<Map.Entry<String, String>> flatEntryList() {
        return this.data;
    }

    public Set<Map.Entry<String, List<String>>> entrySet() {
        LinkedHashMap<String, List<String>> entries = new LinkedHashMap<>();
        for (Map.Entry<String, String> e : this.data) {
            if (!entries.containsKey(e.getKey())) {
                entries.put(e.getKey(), new LinkedList());
            }
            ((List) entries.get(e.getKey())).add(e.getValue());
        }
        for (Map.Entry<String, List<String>> e2 : entries.entrySet()) {
            e2.setValue(Collections.unmodifiableList((List) e2.getValue()));
        }
        return Collections.unmodifiableSet(entries.entrySet());
    }

    public Collection<List<String>> values() {
        List<List<String>> ret = new LinkedList<>();
        for (Map.Entry<String, List<String>> e : entrySet()) {
            ret.add(e.getValue());
        }
        return Collections.unmodifiableList(ret);
    }

    public boolean equals(Object other) {
        if (!(other instanceof UrlParameterMultimap)) {
            return false;
        }
        return this.data.equals(((UrlParameterMultimap) other).data);
    }

    public int hashCode() {
        return this.data.hashCode();
    }
}
