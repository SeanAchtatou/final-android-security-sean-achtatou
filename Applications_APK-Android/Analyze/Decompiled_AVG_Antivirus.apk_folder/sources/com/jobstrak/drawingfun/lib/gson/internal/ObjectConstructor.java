package com.jobstrak.drawingfun.lib.gson.internal;

public interface ObjectConstructor<T> {
    T construct();
}
