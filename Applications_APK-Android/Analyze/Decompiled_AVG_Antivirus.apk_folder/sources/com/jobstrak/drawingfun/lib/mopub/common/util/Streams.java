package com.jobstrak.drawingfun.lib.mopub.common.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Streams {
    public static void copyContent(InputStream inputStream, OutputStream outputStream) throws IOException {
        if (inputStream == null || outputStream == null) {
            throw new IOException("Unable to copy from or to a null stream.");
        }
        byte[] buffer = new byte[16384];
        while (true) {
            int read = inputStream.read(buffer);
            int length = read;
            if (read != -1) {
                outputStream.write(buffer, 0, length);
            } else {
                return;
            }
        }
    }

    public static void copyContent(InputStream inputStream, OutputStream outputStream, long maxBytes) throws IOException {
        if (inputStream == null || outputStream == null) {
            throw new IOException("Unable to copy from or to a null stream.");
        }
        byte[] buffer = new byte[16384];
        long totalRead = 0;
        while (true) {
            int read = inputStream.read(buffer);
            int length = read;
            if (read != -1) {
                totalRead += (long) length;
                if (totalRead < maxBytes) {
                    outputStream.write(buffer, 0, length);
                } else {
                    throw new IOException("Error copying content: attempted to copy " + totalRead + " bytes, with " + maxBytes + " maximum.");
                }
            } else {
                return;
            }
        }
    }

    public static void readStream(InputStream inputStream, byte[] buffer) throws IOException {
        int offset = 0;
        int maxBytes = buffer.length;
        do {
            int read = inputStream.read(buffer, offset, maxBytes);
            int bytesRead = read;
            if (read != -1) {
                offset += bytesRead;
                maxBytes -= bytesRead;
            } else {
                return;
            }
        } while (maxBytes > 0);
    }

    public static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }
}
