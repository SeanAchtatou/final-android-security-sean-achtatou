package com.jobstrak.drawingfun.lib.mopub.mobileads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.DataKeys;
import com.jobstrak.drawingfun.lib.mopub.common.IntentActions;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;
import com.jobstrak.drawingfun.lib.mopub.common.util.Intents;
import com.jobstrak.drawingfun.lib.mopub.common.util.Reflection;
import com.jobstrak.drawingfun.lib.mopub.mobileads.BaseVideoViewController;
import com.jobstrak.drawingfun.lib.mopub.mraid.MraidVideoViewController;
import com.jobstrak.drawingfun.sdk.model.HTMLAd;

public class MraidVideoPlayerActivity extends BaseVideoPlayerActivity implements BaseVideoViewController.BaseVideoViewControllerListener {
    private static final String NATIVE_VIDEO_VIEW_CONTROLLER = "com.jobstrak.drawingfun.lib.mopub.nativeads.NativeVideoViewController";
    @Nullable
    private BaseVideoViewController mBaseVideoController;
    private long mBroadcastIdentifier;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        this.mBroadcastIdentifier = getBroadcastIdentifierFromIntent(getIntent());
        try {
            this.mBaseVideoController = createVideoViewController(savedInstanceState);
            this.mBaseVideoController.onCreate();
        } catch (IllegalStateException e) {
            BaseBroadcastReceiver.broadcastAction(this, this.mBroadcastIdentifier, IntentActions.ACTION_INTERSTITIAL_FAIL);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        BaseVideoViewController baseVideoViewController = this.mBaseVideoController;
        if (baseVideoViewController != null) {
            baseVideoViewController.onPause();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        BaseVideoViewController baseVideoViewController = this.mBaseVideoController;
        if (baseVideoViewController != null) {
            baseVideoViewController.onResume();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        BaseVideoViewController baseVideoViewController = this.mBaseVideoController;
        if (baseVideoViewController != null) {
            baseVideoViewController.onDestroy();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        BaseVideoViewController baseVideoViewController = this.mBaseVideoController;
        if (baseVideoViewController != null) {
            baseVideoViewController.onSaveInstanceState(outState);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        BaseVideoViewController baseVideoViewController = this.mBaseVideoController;
        if (baseVideoViewController != null) {
            baseVideoViewController.onConfigurationChanged(newConfig);
        }
    }

    public void onBackPressed() {
        BaseVideoViewController baseVideoViewController = this.mBaseVideoController;
        if (baseVideoViewController != null && baseVideoViewController.backButtonEnabled()) {
            super.onBackPressed();
            this.mBaseVideoController.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        BaseVideoViewController baseVideoViewController = this.mBaseVideoController;
        if (baseVideoViewController != null) {
            baseVideoViewController.onActivityResult(requestCode, resultCode, data);
        }
    }

    private BaseVideoViewController createVideoViewController(Bundle savedInstanceState) throws IllegalStateException {
        String clazz = getIntent().getStringExtra(BaseVideoPlayerActivity.VIDEO_CLASS_EXTRAS_KEY);
        if (HTMLAd.TYPE_MRAID.equals(clazz)) {
            return new MraidVideoViewController(this, getIntent().getExtras(), savedInstanceState, this);
        }
        if ("native".equals(clazz)) {
            Class[] constructorParameterClasses = {Context.class, Bundle.class, Bundle.class, BaseVideoViewController.BaseVideoViewControllerListener.class};
            Object[] constructorParameterValues = {this, getIntent().getExtras(), savedInstanceState, this};
            if (Reflection.classFound(NATIVE_VIDEO_VIEW_CONTROLLER)) {
                try {
                    return (BaseVideoViewController) Reflection.instantiateClassWithConstructor(NATIVE_VIDEO_VIEW_CONTROLLER, BaseVideoViewController.class, constructorParameterClasses, constructorParameterValues);
                } catch (Exception e) {
                    throw new IllegalStateException("Missing native video module");
                }
            } else {
                throw new IllegalStateException("Missing native video module");
            }
        } else {
            throw new IllegalStateException("Unsupported video type: " + clazz);
        }
    }

    public void onSetContentView(View view) {
        setContentView(view);
    }

    public void onSetRequestedOrientation(int requestedOrientation) {
        setRequestedOrientation(requestedOrientation);
    }

    public void onFinish() {
        finish();
    }

    public void onStartActivityForResult(Class<? extends Activity> clazz, int requestCode, Bundle extras) {
        if (clazz != null) {
            try {
                startActivityForResult(Intents.getStartActivityIntent(this, clazz, extras), requestCode);
            } catch (ActivityNotFoundException e) {
                MoPubLog.d("Activity " + clazz.getName() + " not found. Did you declare it in your AndroidManifest.xml?");
            }
        }
    }

    protected static long getBroadcastIdentifierFromIntent(Intent intent) {
        return intent.getLongExtra(DataKeys.BROADCAST_IDENTIFIER_KEY, -1);
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    public BaseVideoViewController getBaseVideoViewController() {
        return this.mBaseVideoController;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    public void setBaseVideoViewController(BaseVideoViewController baseVideoViewController) {
        this.mBaseVideoController = baseVideoViewController;
    }
}
