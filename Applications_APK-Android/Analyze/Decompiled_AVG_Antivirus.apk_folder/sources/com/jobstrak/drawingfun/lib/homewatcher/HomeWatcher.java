package com.jobstrak.drawingfun.lib.homewatcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.jobstrak.drawingfun.sdk.utils.LogUtils;

public class HomeWatcher {
    private Context context;
    private IntentFilter filter;
    /* access modifiers changed from: private */
    public OnHomePressedListener listener;
    private InnerRecevier recevier;

    public HomeWatcher(Context context2) {
        this(context2, null);
    }

    public HomeWatcher(Context context2, OnHomePressedListener listener2) {
        this.context = context2;
        this.recevier = new InnerRecevier();
        this.filter = new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        setOnHomePressedListener(listener2);
    }

    public void setOnHomePressedListener(OnHomePressedListener listener2) {
        this.listener = listener2;
    }

    public void startWatch() {
        InnerRecevier innerRecevier = this.recevier;
        if (innerRecevier != null) {
            this.context.registerReceiver(innerRecevier, this.filter);
        }
    }

    public void stopWatch() {
        InnerRecevier innerRecevier = this.recevier;
        if (innerRecevier != null) {
            this.context.unregisterReceiver(innerRecevier);
        }
    }

    class InnerRecevier extends BroadcastReceiver {
        final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";
        final String SYSTEM_DIALOG_REASON_KEY = "reason";
        final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";

        InnerRecevier() {
        }

        public void onReceive(Context context, Intent intent) {
            String reason;
            String action = intent.getAction();
            if (action.equals("android.intent.action.CLOSE_SYSTEM_DIALOGS") && (reason = intent.getStringExtra("reason")) != null) {
                LogUtils.debug("System button pressed [action = %s, reason = %s]", action, reason);
                if (HomeWatcher.this.listener == null) {
                    return;
                }
                if (reason.equals("homekey")) {
                    HomeWatcher.this.listener.onHomePressed();
                } else if (reason.equals("recentapps")) {
                    HomeWatcher.this.listener.onRecentAppsPressed();
                }
            }
        }
    }
}
