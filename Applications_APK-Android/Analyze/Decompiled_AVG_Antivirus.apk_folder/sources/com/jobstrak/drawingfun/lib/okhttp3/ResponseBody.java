package com.jobstrak.drawingfun.lib.okhttp3;

import com.jobstrak.drawingfun.lib.okhttp3.internal.Util;
import com.jobstrak.drawingfun.lib.okio.Buffer;
import com.jobstrak.drawingfun.lib.okio.BufferedSource;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

public abstract class ResponseBody implements Closeable {
    private Reader reader;

    public abstract long contentLength();

    public abstract MediaType contentType();

    public abstract BufferedSource source();

    public final InputStream byteStream() {
        return source().inputStream();
    }

    /* JADX INFO: finally extract failed */
    public final byte[] bytes() throws IOException {
        long contentLength = contentLength();
        if (contentLength <= 2147483647L) {
            BufferedSource source = source();
            try {
                byte[] bytes = source.readByteArray();
                Util.closeQuietly(source);
                if (contentLength == -1 || contentLength == ((long) bytes.length)) {
                    return bytes;
                }
                throw new IOException("Content-Length and stream length disagree");
            } catch (Throwable th) {
                Util.closeQuietly(source);
                throw th;
            }
        } else {
            throw new IOException("Cannot buffer entire body for content length: " + contentLength);
        }
    }

    public final Reader charStream() {
        Reader r = this.reader;
        if (r != null) {
            return r;
        }
        InputStreamReader inputStreamReader = new InputStreamReader(byteStream(), charset());
        this.reader = inputStreamReader;
        return inputStreamReader;
    }

    public final String string() throws IOException {
        return new String(bytes(), charset().name());
    }

    private Charset charset() {
        MediaType contentType = contentType();
        return contentType != null ? contentType.charset(Util.UTF_8) : Util.UTF_8;
    }

    public void close() {
        Util.closeQuietly(source());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.jobstrak.drawingfun.lib.okio.Buffer.writeString(java.lang.String, java.nio.charset.Charset):com.jobstrak.drawingfun.lib.okio.Buffer
     arg types: [java.lang.String, java.nio.charset.Charset]
     candidates:
      com.jobstrak.drawingfun.lib.okio.Buffer.writeString(java.lang.String, java.nio.charset.Charset):com.jobstrak.drawingfun.lib.okio.BufferedSink
      com.jobstrak.drawingfun.lib.okio.BufferedSink.writeString(java.lang.String, java.nio.charset.Charset):com.jobstrak.drawingfun.lib.okio.BufferedSink
      com.jobstrak.drawingfun.lib.okio.Buffer.writeString(java.lang.String, java.nio.charset.Charset):com.jobstrak.drawingfun.lib.okio.Buffer */
    public static ResponseBody create(MediaType contentType, String content) {
        Charset charset = Util.UTF_8;
        if (contentType != null && (charset = contentType.charset()) == null) {
            charset = Util.UTF_8;
            contentType = MediaType.parse(contentType + "; charset=utf-8");
        }
        Buffer buffer = new Buffer().writeString(content, charset);
        return create(contentType, buffer.size(), buffer);
    }

    public static ResponseBody create(MediaType contentType, byte[] content) {
        return create(contentType, (long) content.length, new Buffer().write(content));
    }

    public static ResponseBody create(final MediaType contentType, final long contentLength, final BufferedSource content) {
        if (content != null) {
            return new ResponseBody() {
                public MediaType contentType() {
                    return contentType;
                }

                public long contentLength() {
                    return contentLength;
                }

                public BufferedSource source() {
                    return content;
                }
            };
        }
        throw new NullPointerException("source == null");
    }
}
