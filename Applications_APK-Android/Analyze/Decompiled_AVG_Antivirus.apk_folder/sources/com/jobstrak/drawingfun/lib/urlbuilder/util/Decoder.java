package com.jobstrak.drawingfun.lib.urlbuilder.util;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Decoder {
    protected static final boolean DECODE_PLUS_AS_SPACE = true;
    protected static final boolean DO_NOT_DECODE_PLUS_AS_SPACE = false;
    protected final Charset inputEncoding;

    public Decoder(Charset inputEncoding2) {
        this.inputEncoding = inputEncoding2;
    }

    public String decodeUserInfo(String userInfo) {
        if (userInfo == null || userInfo.isEmpty()) {
            return userInfo;
        }
        return urlDecode(userInfo, true);
    }

    public String decodeFragment(String fragment) {
        if (fragment == null || fragment.isEmpty()) {
            return fragment;
        }
        return urlDecode(fragment, false);
    }

    public UrlParameterMultimap parseQueryString(String query) {
        String value;
        UrlParameterMultimap ret = UrlParameterMultimap.newMultimap();
        if (query == null || query.isEmpty()) {
            return ret;
        }
        for (String part : query.split("&")) {
            String[] kvp = part.split("=", 2);
            String key = urlDecode(kvp[0], true);
            if (kvp.length == 2) {
                value = urlDecode(kvp[1], true);
            } else {
                value = null;
            }
            ret.add(key, value);
        }
        return ret;
    }

    /* access modifiers changed from: protected */
    public byte[] nextDecodeableSequence(String input, int position) {
        int len = input.length();
        byte[] data = new byte[len];
        int j = 0;
        int i = position;
        while (i < len) {
            if (input.charAt(i) != '%' || len < i + 3) {
                return Arrays.copyOfRange(data, 0, j);
            }
            data[j] = (byte) Integer.parseInt(input.substring(i + 1, i + 3), 16);
            i = i + 2 + 1;
            j++;
        }
        return Arrays.copyOfRange(data, 0, j);
    }

    public String decodePath(String input) {
        if (input == null || input.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        StringTokenizer st = new StringTokenizer(input, "/", true);
        while (st.hasMoreElements()) {
            String element = st.nextToken();
            if ("/".equals(element)) {
                sb.append(element);
            } else if (!element.isEmpty()) {
                sb.append(urlDecode(element, false));
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String urlDecode(String input, boolean decodePlusAsSpace) {
        StringBuilder sb = new StringBuilder();
        int len = input.length();
        int i = 0;
        while (i < len) {
            char c0 = input.charAt(i);
            if (c0 == '+' && decodePlusAsSpace) {
                sb.append(' ');
            } else if (c0 != '%') {
                sb.append(c0);
            } else if (len < i + 3) {
                sb.append(input.substring(i, Math.min(input.length(), i + 2)));
                i += 3;
            } else {
                byte[] bytes = nextDecodeableSequence(input, i);
                sb.append((CharSequence) this.inputEncoding.decode(ByteBuffer.wrap(bytes)));
                i += (bytes.length * 3) - 1;
            }
            i++;
        }
        return sb.toString();
    }
}
