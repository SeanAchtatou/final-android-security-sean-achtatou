package com.jobstrak.drawingfun.lib.mopub.mraid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.MoPubHttpUrlConnection;
import com.jobstrak.drawingfun.lib.mopub.common.Preconditions;
import com.jobstrak.drawingfun.lib.mopub.common.VisibleForTesting;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;
import com.jobstrak.drawingfun.lib.mopub.common.util.AsyncTasks;
import com.jobstrak.drawingfun.lib.mopub.common.util.DeviceUtils;
import com.jobstrak.drawingfun.lib.mopub.common.util.Intents;
import com.jobstrak.drawingfun.lib.mopub.common.util.ResponseHeader;
import com.jobstrak.drawingfun.lib.mopub.common.util.Streams;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MraidNativeCommandHandler {
    public static final String ANDROID_CALENDAR_CONTENT_TYPE = "vnd.android.cursor.item/event";
    private static final String[] DATE_FORMATS = {"yyyy-MM-dd'T'HH:mm:ssZZZZZ", "yyyy-MM-dd'T'HH:mmZZZZZ"};
    private static final int MAX_NUMBER_DAYS_IN_MONTH = 31;
    @VisibleForTesting
    static final String MIME_TYPE_HEADER = "Content-Type";

    interface MraidCommandFailureListener {
        void onFailure(MraidCommandException mraidCommandException);
    }

    /* access modifiers changed from: package-private */
    public void createCalendarEvent(Context context, Map<String, String> params) throws MraidCommandException {
        if (isCalendarAvailable(context)) {
            try {
                Map<String, Object> calendarParams = translateJSParamsToAndroidCalendarEventMapping(params);
                Intent intent = new Intent("android.intent.action.INSERT").setType(ANDROID_CALENDAR_CONTENT_TYPE);
                for (String key : calendarParams.keySet()) {
                    Object value = calendarParams.get(key);
                    if (value instanceof Long) {
                        intent.putExtra(key, ((Long) value).longValue());
                    } else if (value instanceof Integer) {
                        intent.putExtra(key, ((Integer) value).intValue());
                    } else {
                        intent.putExtra(key, (String) value);
                    }
                }
                intent.setFlags(268435456);
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                MoPubLog.d("no calendar app installed");
                throw new MraidCommandException("Action is unsupported on this device - no calendar app installed");
            } catch (IllegalArgumentException e2) {
                MoPubLog.d("create calendar: invalid parameters " + e2.getMessage());
                throw new MraidCommandException(e2);
            } catch (Exception e3) {
                MoPubLog.d("could not create calendar event");
                throw new MraidCommandException(e3);
            }
        } else {
            MoPubLog.d("unsupported action createCalendarEvent for devices pre-ICS");
            throw new MraidCommandException("Action is unsupported on this device (need Android version Ice Cream Sandwich or above)");
        }
    }

    /* access modifiers changed from: package-private */
    public void storePicture(@NonNull Context context, @NonNull String imageUrl, @NonNull MraidCommandFailureListener failureListener) throws MraidCommandException {
        if (!isStorePictureSupported(context)) {
            MoPubLog.d("Error downloading file - the device does not have an SD card mounted, or the Android permission is not granted.");
            throw new MraidCommandException("Error downloading file  - the device does not have an SD card mounted, or the Android permission is not granted.");
        } else if (context instanceof Activity) {
            showUserDialog(context, imageUrl, failureListener);
        } else {
            Toast.makeText(context, "Downloading image to Picture gallery...", 0).show();
            downloadImage(context, imageUrl, failureListener);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isTelAvailable(Context context) {
        Intent telIntent = new Intent("android.intent.action.DIAL");
        telIntent.setData(Uri.parse("tel:"));
        return Intents.deviceCanHandleIntent(context, telIntent);
    }

    /* access modifiers changed from: package-private */
    public boolean isSmsAvailable(Context context) {
        Intent smsIntent = new Intent("android.intent.action.VIEW");
        smsIntent.setData(Uri.parse("sms:"));
        return Intents.deviceCanHandleIntent(context, smsIntent);
    }

    public static boolean isStorePictureSupported(Context context) {
        return "mounted".equals(Environment.getExternalStorageState()) && DeviceUtils.isPermissionGranted(context, "android.permission.WRITE_EXTERNAL_STORAGE");
    }

    static boolean isCalendarAvailable(Context context) {
        return Intents.deviceCanHandleIntent(context, new Intent("android.intent.action.INSERT").setType(ANDROID_CALENDAR_CONTENT_TYPE));
    }

    /* JADX WARN: Type inference failed for: r1v6, types: [android.view.ViewParent] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isInlineVideoAvailable(@androidx.annotation.NonNull android.app.Activity r7, @androidx.annotation.NonNull android.view.View r8) {
        /*
            r6 = this;
            r0 = r8
        L_0x0001:
            boolean r1 = r0.isHardwareAccelerated()
            r2 = 0
            if (r1 == 0) goto L_0x003b
            int r1 = r0.getLayerType()
            r3 = 1
            boolean r1 = com.jobstrak.drawingfun.lib.mopub.common.util.Utils.bitMaskContainsFlag(r1, r3)
            if (r1 == 0) goto L_0x0014
            goto L_0x003b
        L_0x0014:
            android.view.ViewParent r1 = r0.getParent()
            boolean r1 = r1 instanceof android.view.View
            if (r1 != 0) goto L_0x0033
            android.view.Window r1 = r7.getWindow()
            if (r1 == 0) goto L_0x0032
            android.view.WindowManager$LayoutParams r4 = r1.getAttributes()
            int r4 = r4.flags
            r5 = 16777216(0x1000000, float:2.3509887E-38)
            boolean r4 = com.jobstrak.drawingfun.lib.mopub.common.util.Utils.bitMaskContainsFlag(r4, r5)
            if (r4 == 0) goto L_0x0032
            return r3
        L_0x0032:
            return r2
        L_0x0033:
            android.view.ViewParent r1 = r0.getParent()
            r0 = r1
            android.view.View r0 = (android.view.View) r0
            goto L_0x0001
        L_0x003b:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.mopub.mraid.MraidNativeCommandHandler.isInlineVideoAvailable(android.app.Activity, android.view.View):boolean");
    }

    private Map<String, Object> translateJSParamsToAndroidCalendarEventMapping(Map<String, String> params) {
        Map<String, Object> validatedParamsMapping = new HashMap<>();
        if (!params.containsKey("description") || !params.containsKey("start")) {
            throw new IllegalArgumentException("Missing start and description fields");
        }
        validatedParamsMapping.put("title", params.get("description"));
        if (!params.containsKey("start") || params.get("start") == null) {
            throw new IllegalArgumentException("Invalid calendar event: start is null.");
        }
        Date startDateTime = parseDate(params.get("start"));
        if (startDateTime != null) {
            validatedParamsMapping.put("beginTime", Long.valueOf(startDateTime.getTime()));
            if (params.containsKey("end") && params.get("end") != null) {
                Date endDateTime = parseDate(params.get("end"));
                if (endDateTime != null) {
                    validatedParamsMapping.put("endTime", Long.valueOf(endDateTime.getTime()));
                } else {
                    throw new IllegalArgumentException("Invalid calendar event: end time is malformed. Date format expecting (yyyy-MM-DDTHH:MM:SS-xx:xx) or (yyyy-MM-DDTHH:MM-xx:xx) i.e. 2013-08-14T09:00:01-08:00");
                }
            }
            if (params.containsKey("location")) {
                validatedParamsMapping.put("eventLocation", params.get("location"));
            }
            if (params.containsKey("summary")) {
                validatedParamsMapping.put("description", params.get("summary"));
            }
            if (params.containsKey("transparency")) {
                validatedParamsMapping.put("availability", Integer.valueOf(params.get("transparency").equals("transparent") ? 1 : 0));
            }
            validatedParamsMapping.put("rrule", parseRecurrenceRule(params));
            return validatedParamsMapping;
        }
        throw new IllegalArgumentException("Invalid calendar event: start time is malformed. Date format expecting (yyyy-MM-DDTHH:MM:SS-xx:xx) or (yyyy-MM-DDTHH:MM-xx:xx) i.e. 2013-08-14T09:00:01-08:00");
    }

    private Date parseDate(String dateTime) {
        Date result = null;
        String[] strArr = DATE_FORMATS;
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            try {
                result = new SimpleDateFormat(strArr[i], Locale.US).parse(dateTime);
                if (result != null) {
                    break;
                }
                i++;
            } catch (ParseException e) {
            }
        }
        return result;
    }

    private String parseRecurrenceRule(Map<String, String> params) throws IllegalArgumentException {
        StringBuilder rule = new StringBuilder();
        if (params.containsKey("frequency")) {
            String frequency = params.get("frequency");
            int interval = -1;
            if (params.containsKey("interval")) {
                interval = Integer.parseInt(params.get("interval"));
            }
            if ("daily".equals(frequency)) {
                rule.append("FREQ=DAILY;");
                if (interval != -1) {
                    rule.append("INTERVAL=" + interval + ";");
                }
            } else if ("weekly".equals(frequency)) {
                rule.append("FREQ=WEEKLY;");
                if (interval != -1) {
                    rule.append("INTERVAL=" + interval + ";");
                }
                if (params.containsKey("daysInWeek")) {
                    String weekdays = translateWeekIntegersToDays(params.get("daysInWeek"));
                    if (weekdays != null) {
                        rule.append("BYDAY=" + weekdays + ";");
                    } else {
                        throw new IllegalArgumentException("invalid ");
                    }
                }
            } else if ("monthly".equals(frequency)) {
                rule.append("FREQ=MONTHLY;");
                if (interval != -1) {
                    rule.append("INTERVAL=" + interval + ";");
                }
                if (params.containsKey("daysInMonth")) {
                    String monthDays = translateMonthIntegersToDays(params.get("daysInMonth"));
                    if (monthDays != null) {
                        rule.append("BYMONTHDAY=" + monthDays + ";");
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
            } else {
                throw new IllegalArgumentException("frequency is only supported for daily, weekly, and monthly.");
            }
        }
        return rule.toString();
    }

    private String translateWeekIntegersToDays(String expression) throws IllegalArgumentException {
        StringBuilder daysResult = new StringBuilder();
        boolean[] daysAlreadyCounted = new boolean[7];
        String[] days = expression.split(",");
        for (String day : days) {
            int dayNumber = Integer.parseInt(day);
            int dayNumber2 = dayNumber == 7 ? 0 : dayNumber;
            if (!daysAlreadyCounted[dayNumber2]) {
                daysResult.append(dayNumberToDayOfWeekString(dayNumber2) + ",");
                daysAlreadyCounted[dayNumber2] = true;
            }
        }
        if (days.length != 0) {
            daysResult.deleteCharAt(daysResult.length() - 1);
            return daysResult.toString();
        }
        throw new IllegalArgumentException("must have at least 1 day of the week if specifying repeating weekly");
    }

    private String translateMonthIntegersToDays(String expression) throws IllegalArgumentException {
        StringBuilder daysResult = new StringBuilder();
        boolean[] daysAlreadyCounted = new boolean[63];
        String[] days = expression.split(",");
        for (String day : days) {
            int dayNumber = Integer.parseInt(day);
            if (!daysAlreadyCounted[dayNumber + MAX_NUMBER_DAYS_IN_MONTH]) {
                daysResult.append(dayNumberToDayOfMonthString(dayNumber) + ",");
                daysAlreadyCounted[dayNumber + MAX_NUMBER_DAYS_IN_MONTH] = true;
            }
        }
        if (days.length != 0) {
            daysResult.deleteCharAt(daysResult.length() - 1);
            return daysResult.toString();
        }
        throw new IllegalArgumentException("must have at least 1 day of the month if specifying repeating weekly");
    }

    private String dayNumberToDayOfWeekString(int number) throws IllegalArgumentException {
        switch (number) {
            case 0:
                return "SU";
            case 1:
                return "MO";
            case 2:
                return "TU";
            case 3:
                return "WE";
            case 4:
                return "TH";
            case 5:
                return "FR";
            case 6:
                return "SA";
            default:
                throw new IllegalArgumentException("invalid day of week " + number);
        }
    }

    private String dayNumberToDayOfMonthString(int number) throws IllegalArgumentException {
        if (number == 0 || number < -31 || number > MAX_NUMBER_DAYS_IN_MONTH) {
            throw new IllegalArgumentException("invalid day of month " + number);
        }
        return "" + number;
    }

    /* access modifiers changed from: package-private */
    public void downloadImage(final Context context, String uriString, final MraidCommandFailureListener failureListener) {
        AsyncTasks.safeExecuteOnExecutor(new DownloadImageAsyncTask(context, new DownloadImageAsyncTask.DownloadImageAsyncTaskListener() {
            public void onSuccess() {
                MoPubLog.d("Image successfully saved.");
            }

            public void onFailure() {
                Toast.makeText(context, "Image failed to download.", 0).show();
                MoPubLog.d("Error downloading and saving image file.");
                failureListener.onFailure(new MraidCommandException("Error downloading and saving image file."));
            }
        }), uriString);
    }

    private void showUserDialog(final Context context, final String imageUrl, final MraidCommandFailureListener failureListener) {
        new AlertDialog.Builder(context).setTitle("Save Image").setMessage("Download image to Picture gallery?").setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MraidNativeCommandHandler.this.downloadImage(context, imageUrl, failureListener);
            }
        }).setCancelable(true).show();
    }

    @VisibleForTesting
    static class DownloadImageAsyncTask extends AsyncTask<String, Void, Boolean> {
        private final Context mContext;
        private final DownloadImageAsyncTaskListener mListener;

        interface DownloadImageAsyncTaskListener {
            void onFailure();

            void onSuccess();
        }

        public DownloadImageAsyncTask(@NonNull Context context, @NonNull DownloadImageAsyncTaskListener listener) {
            this.mContext = context.getApplicationContext();
            this.mListener = listener;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(@NonNull String[] params) {
            if (params == null || params.length == 0 || params[0] == null) {
                return false;
            }
            File pictureStoragePath = getPictureStoragePath();
            pictureStoragePath.mkdirs();
            String uriString = params[0];
            URI uri = URI.create(uriString);
            InputStream pictureInputStream = null;
            OutputStream pictureOutputStream = null;
            try {
                HttpURLConnection urlConnection = MoPubHttpUrlConnection.getHttpUrlConnection(uriString);
                pictureInputStream = new BufferedInputStream(urlConnection.getInputStream());
                String redirectLocation = urlConnection.getHeaderField(ResponseHeader.LOCATION.getKey());
                if (!TextUtils.isEmpty(redirectLocation)) {
                    uri = URI.create(redirectLocation);
                }
                File pictureFile = new File(pictureStoragePath, getFileNameForUriAndHeaders(uri, urlConnection.getHeaderFields()));
                pictureOutputStream = new FileOutputStream(pictureFile);
                Streams.copyContent(pictureInputStream, pictureOutputStream);
                loadPictureIntoGalleryApp(pictureFile.toString());
                return true;
            } catch (Exception e) {
                return false;
            } finally {
                Streams.closeStream(pictureInputStream);
                Streams.closeStream(pictureOutputStream);
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean success) {
            if (success == null || !success.booleanValue()) {
                this.mListener.onFailure();
            } else {
                this.mListener.onSuccess();
            }
        }

        @Nullable
        private String getFileNameForUriAndHeaders(@NonNull URI uri, @Nullable Map<String, List<String>> headers) {
            Preconditions.checkNotNull(uri);
            String path = uri.getPath();
            if (path == null || headers == null) {
                return null;
            }
            String filename = new File(path).getName();
            List<String> mimeTypeHeaders = headers.get(MraidNativeCommandHandler.MIME_TYPE_HEADER);
            if (mimeTypeHeaders != null && !mimeTypeHeaders.isEmpty()) {
                if (mimeTypeHeaders.get(0) != null) {
                    for (String field : ((String) mimeTypeHeaders.get(0)).split(";")) {
                        if (field.contains("image/")) {
                            String extension = "." + field.split("/")[1];
                            if (filename.endsWith(extension)) {
                                return filename;
                            }
                            return filename + extension;
                        }
                    }
                    return filename;
                }
            }
            return filename;
        }

        private File getPictureStoragePath() {
            return new File(Environment.getExternalStorageDirectory(), "Pictures");
        }

        private void loadPictureIntoGalleryApp(String filename) {
            MoPubMediaScannerConnectionClient mediaScannerConnectionClient = new MoPubMediaScannerConnectionClient(filename, null);
            MediaScannerConnection mediaScannerConnection = new MediaScannerConnection(this.mContext, mediaScannerConnectionClient);
            mediaScannerConnectionClient.setMediaScannerConnection(mediaScannerConnection);
            mediaScannerConnection.connect();
        }

        /* access modifiers changed from: package-private */
        @VisibleForTesting
        @Deprecated
        public DownloadImageAsyncTaskListener getListener() {
            return this.mListener;
        }
    }

    private static class MoPubMediaScannerConnectionClient implements MediaScannerConnection.MediaScannerConnectionClient {
        private final String mFilename;
        private MediaScannerConnection mMediaScannerConnection;
        private final String mMimeType;

        private MoPubMediaScannerConnectionClient(String filename, String mimeType) {
            this.mFilename = filename;
            this.mMimeType = mimeType;
        }

        /* access modifiers changed from: private */
        public void setMediaScannerConnection(MediaScannerConnection connection) {
            this.mMediaScannerConnection = connection;
        }

        public void onMediaScannerConnected() {
            MediaScannerConnection mediaScannerConnection = this.mMediaScannerConnection;
            if (mediaScannerConnection != null) {
                mediaScannerConnection.scanFile(this.mFilename, this.mMimeType);
            }
        }

        public void onScanCompleted(String path, Uri uri) {
            MediaScannerConnection mediaScannerConnection = this.mMediaScannerConnection;
            if (mediaScannerConnection != null) {
                mediaScannerConnection.disconnect();
            }
        }
    }
}
