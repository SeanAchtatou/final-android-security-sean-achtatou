package com.jobstrak.drawingfun.lib.gson;

import com.jobstrak.drawingfun.lib.gson.reflect.TypeToken;

public interface TypeAdapterFactory {
    <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken);
}
