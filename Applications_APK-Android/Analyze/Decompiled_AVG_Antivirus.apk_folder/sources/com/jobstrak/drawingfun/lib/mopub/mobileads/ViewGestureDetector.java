package com.jobstrak.drawingfun.lib.mopub.mobileads;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.annotation.NonNull;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;

public class ViewGestureDetector extends GestureDetector {
    private UserClickListener mUserClickListener;
    private final View mView;

    public interface UserClickListener {
        void onResetUserClick();

        void onUserClick();

        boolean wasClicked();
    }

    public ViewGestureDetector(@NonNull Context context, @NonNull View view) {
        super(context, new GestureDetector.SimpleOnGestureListener());
        this.mView = view;
        setIsLongpressEnabled(false);
    }

    public void sendTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            onTouchEvent(motionEvent);
        } else if (action == 1) {
            UserClickListener userClickListener = this.mUserClickListener;
            if (userClickListener != null) {
                userClickListener.onUserClick();
            } else {
                MoPubLog.d("View's onUserClick() is not registered.");
            }
        } else if (action == 2) {
            if (isMotionEventInView(motionEvent, this.mView)) {
                onTouchEvent(motionEvent);
            } else {
                resetAdFlaggingGesture();
            }
        }
    }

    public void setUserClickListener(UserClickListener listener) {
        this.mUserClickListener = listener;
    }

    /* access modifiers changed from: package-private */
    public void resetAdFlaggingGesture() {
    }

    private boolean isMotionEventInView(MotionEvent motionEvent, View view) {
        if (motionEvent == null || view == null) {
            return false;
        }
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        if (x < 0.0f || x > ((float) view.getWidth()) || y < 0.0f || y > ((float) view.getHeight())) {
            return false;
        }
        return true;
    }
}
