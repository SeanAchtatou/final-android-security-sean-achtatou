package com.jobstrak.drawingfun.lib.okhttp3.internal.http;

import com.jobstrak.drawingfun.lib.okio.Sink;
import java.io.IOException;

public interface CacheRequest {
    void abort();

    Sink body() throws IOException;
}
