package com.jobstrak.drawingfun.lib.mopub.mobileads;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.CloseableLayout;
import com.jobstrak.drawingfun.lib.mopub.common.DataKeys;

abstract class BaseInterstitialActivity extends Activity {
    @Nullable
    private Long mBroadcastIdentifier;
    @Nullable
    private CloseableLayout mCloseableLayout;

    public abstract View getAdView();

    BaseInterstitialActivity() {
    }

    enum JavaScriptWebViewCallbacks {
        WEB_VIEW_DID_APPEAR("webviewDidAppear();"),
        WEB_VIEW_DID_CLOSE("webviewDidClose();");
        
        private String mJavascript;

        private JavaScriptWebViewCallbacks(String javascript) {
            this.mJavascript = javascript;
        }

        /* access modifiers changed from: protected */
        public String getJavascript() {
            return this.mJavascript;
        }

        /* access modifiers changed from: protected */
        public String getUrl() {
            return "javascript:" + this.mJavascript;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mBroadcastIdentifier = getBroadcastIdentifierFromIntent(getIntent());
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        View adView = getAdView();
        this.mCloseableLayout = new CloseableLayout(this);
        this.mCloseableLayout.setOnCloseListener(new CloseableLayout.OnCloseListener() {
            public void onClose() {
                BaseInterstitialActivity.this.finish();
            }
        });
        this.mCloseableLayout.addView(adView, new FrameLayout.LayoutParams(-1, -1));
        setContentView(this.mCloseableLayout);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        CloseableLayout closeableLayout = this.mCloseableLayout;
        if (closeableLayout != null) {
            closeableLayout.removeAllViews();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    @Nullable
    public CloseableLayout getCloseableLayout() {
        return this.mCloseableLayout;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public Long getBroadcastIdentifier() {
        return this.mBroadcastIdentifier;
    }

    /* access modifiers changed from: protected */
    public void showInterstitialCloseButton() {
        CloseableLayout closeableLayout = this.mCloseableLayout;
        if (closeableLayout != null) {
            closeableLayout.setCloseVisible(true);
        }
    }

    /* access modifiers changed from: protected */
    public void hideInterstitialCloseButton() {
        CloseableLayout closeableLayout = this.mCloseableLayout;
        if (closeableLayout != null) {
            closeableLayout.setCloseVisible(false);
        }
    }

    protected static Long getBroadcastIdentifierFromIntent(Intent intent) {
        if (intent.hasExtra(DataKeys.BROADCAST_IDENTIFIER_KEY)) {
            return Long.valueOf(intent.getLongExtra(DataKeys.BROADCAST_IDENTIFIER_KEY, -1));
        }
        return null;
    }
}
