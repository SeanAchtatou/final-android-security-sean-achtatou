package com.jobstrak.drawingfun.lib.urlbuilder.util;

public class RuntimeURISyntaxException extends RuntimeException {
    public RuntimeURISyntaxException(Throwable cause) {
        super(cause);
    }

    public Throwable fillInStackTrace() {
        return null;
    }
}
