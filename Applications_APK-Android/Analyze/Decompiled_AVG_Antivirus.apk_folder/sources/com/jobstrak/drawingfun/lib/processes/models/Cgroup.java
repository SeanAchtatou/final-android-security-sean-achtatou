package com.jobstrak.drawingfun.lib.processes.models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public final class Cgroup extends ProcFile {
    public final ArrayList<ControlGroup> groups = new ArrayList<>();

    public static Cgroup get(int pid) throws IOException {
        return new Cgroup(String.format("/proc/%d/cgroup", Integer.valueOf(pid)));
    }

    private Cgroup(String path) throws IOException {
        super(path);
        for (String line : this.content.split("\n")) {
            try {
                this.groups.add(new ControlGroup(line));
            } catch (Exception e) {
            }
        }
    }

    public ControlGroup getGroup(String subsystem) {
        Iterator<ControlGroup> it = this.groups.iterator();
        while (it.hasNext()) {
            ControlGroup group = it.next();
            String[] systems = group.subsystems.split(",");
            int length = systems.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    if (systems[i].equals(subsystem)) {
                        return group;
                    }
                    i++;
                }
            }
        }
        return null;
    }
}
