package com.jobstrak.drawingfun.lib.mopub.mobileads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;
import com.jobstrak.drawingfun.sdk.model.HTMLAd;

public class BaseVideoPlayerActivity extends Activity {
    public static final String VIDEO_CLASS_EXTRAS_KEY = "video_view_class_name";
    public static final String VIDEO_URL = "video_url";

    public static void startMraid(Context context, String videoUrl) {
        try {
            context.startActivity(createIntentMraid(context, videoUrl));
        } catch (ActivityNotFoundException e) {
            MoPubLog.d("Activity MraidVideoPlayerActivity not found. Did you declare it in your AndroidManifest.xml?");
        }
    }

    static Intent createIntentMraid(Context context, String videoUrl) {
        Intent intentVideoPlayerActivity = new Intent(context, MraidVideoPlayerActivity.class);
        intentVideoPlayerActivity.setFlags(268435456);
        intentVideoPlayerActivity.putExtra(VIDEO_CLASS_EXTRAS_KEY, HTMLAd.TYPE_MRAID);
        intentVideoPlayerActivity.putExtra(VIDEO_URL, videoUrl);
        return intentVideoPlayerActivity;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AudioManager am = (AudioManager) getSystemService("audio");
        if (am != null) {
            am.abandonAudioFocus(null);
        }
    }
}
