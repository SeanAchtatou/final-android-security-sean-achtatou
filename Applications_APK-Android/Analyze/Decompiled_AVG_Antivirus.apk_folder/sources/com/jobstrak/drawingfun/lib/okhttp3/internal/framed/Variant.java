package com.jobstrak.drawingfun.lib.okhttp3.internal.framed;

import com.jobstrak.drawingfun.lib.okhttp3.Protocol;
import com.jobstrak.drawingfun.lib.okio.BufferedSink;
import com.jobstrak.drawingfun.lib.okio.BufferedSource;

public interface Variant {
    Protocol getProtocol();

    FrameReader newReader(BufferedSource bufferedSource, boolean z);

    FrameWriter newWriter(BufferedSink bufferedSink, boolean z);
}
