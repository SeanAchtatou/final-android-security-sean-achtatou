package com.jobstrak.drawingfun.lib.okhttp3.internal.http;

import com.jobstrak.drawingfun.lib.okhttp3.Address;
import com.jobstrak.drawingfun.lib.okhttp3.ConnectionPool;
import com.jobstrak.drawingfun.lib.okhttp3.Route;
import com.jobstrak.drawingfun.lib.okhttp3.internal.Internal;
import com.jobstrak.drawingfun.lib.okhttp3.internal.RouteDatabase;
import com.jobstrak.drawingfun.lib.okhttp3.internal.Util;
import com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection;
import com.jobstrak.drawingfun.lib.okio.Sink;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.lang.ref.WeakReference;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class StreamAllocation {
    public final Address address;
    private boolean canceled;
    private RealConnection connection;
    private final ConnectionPool connectionPool;
    private boolean released;
    private Route route;
    private RouteSelector routeSelector;
    private HttpStream stream;

    public StreamAllocation(ConnectionPool connectionPool2, Address address2) {
        this.connectionPool = connectionPool2;
        this.address = address2;
        this.routeSelector = new RouteSelector(address2, routeDatabase());
    }

    public HttpStream newStream(int connectTimeout, int readTimeout, int writeTimeout, boolean connectionRetryEnabled, boolean doExtensiveHealthChecks) throws RouteException, IOException {
        HttpStream resultStream;
        try {
            RealConnection resultConnection = findHealthyConnection(connectTimeout, readTimeout, writeTimeout, connectionRetryEnabled, doExtensiveHealthChecks);
            if (resultConnection.framedConnection != null) {
                resultStream = new Http2xStream(this, resultConnection.framedConnection);
            } else {
                resultConnection.socket().setSoTimeout(readTimeout);
                resultConnection.source.timeout().timeout((long) readTimeout, TimeUnit.MILLISECONDS);
                resultConnection.sink.timeout().timeout((long) writeTimeout, TimeUnit.MILLISECONDS);
                resultStream = new Http1xStream(this, resultConnection.source, resultConnection.sink);
            }
            synchronized (this.connectionPool) {
                this.stream = resultStream;
            }
            return resultStream;
        } catch (IOException e) {
            throw new RouteException(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0014, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r0.isHealthy(r8) == false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection findHealthyConnection(int r4, int r5, int r6, boolean r7, boolean r8) throws java.io.IOException, com.jobstrak.drawingfun.lib.okhttp3.internal.http.RouteException {
        /*
            r3 = this;
        L_0x0000:
            com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection r0 = r3.findConnection(r4, r5, r6, r7)
            com.jobstrak.drawingfun.lib.okhttp3.ConnectionPool r1 = r3.connectionPool
            monitor-enter(r1)
            int r2 = r0.successCount     // Catch:{ all -> 0x001e }
            if (r2 != 0) goto L_0x000d
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            return r0
        L_0x000d:
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            boolean r1 = r0.isHealthy(r8)
            if (r1 == 0) goto L_0x0015
            return r0
        L_0x0015:
            java.io.IOException r1 = new java.io.IOException
            r1.<init>()
            r3.connectionFailed(r1)
            goto L_0x0000
        L_0x001e:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            throw r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.okhttp3.internal.http.StreamAllocation.findHealthyConnection(int, int, int, boolean, boolean):com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection");
    }

    /* JADX INFO: Multiple debug info for r1v8 com.jobstrak.drawingfun.lib.okhttp3.Route: [D('allocatedConnection' com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection), D('selectedRoute' com.jobstrak.drawingfun.lib.okhttp3.Route)] */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002d, code lost:
        if (r1 != null) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002f, code lost:
        r0 = r9.routeSelector.next();
        r2 = r9.connectionPool;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0037, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r9.route = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003a, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003b, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0040, code lost:
        r8 = new com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection(r1);
        acquire(r8);
        r2 = r9.connectionPool;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x004b, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        com.jobstrak.drawingfun.lib.okhttp3.internal.Internal.instance.put(r9.connectionPool, r8);
        r9.connection = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0057, code lost:
        if (r9.canceled != false) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0059, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x005a, code lost:
        r8.connect(r10, r11, r12, r9.address.connectionSpecs(), r13);
        routeDatabase().connected(r8.route());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0073, code lost:
        return r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x007b, code lost:
        throw new java.io.IOException("Canceled");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection findConnection(int r10, int r11, int r12, boolean r13) throws java.io.IOException, com.jobstrak.drawingfun.lib.okhttp3.internal.http.RouteException {
        /*
            r9 = this;
            com.jobstrak.drawingfun.lib.okhttp3.ConnectionPool r0 = r9.connectionPool
            monitor-enter(r0)
            boolean r1 = r9.released     // Catch:{ all -> 0x0097 }
            if (r1 != 0) goto L_0x008f
            com.jobstrak.drawingfun.lib.okhttp3.internal.http.HttpStream r1 = r9.stream     // Catch:{ all -> 0x0097 }
            if (r1 != 0) goto L_0x0087
            boolean r1 = r9.canceled     // Catch:{ all -> 0x0097 }
            if (r1 != 0) goto L_0x007f
            com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection r1 = r9.connection     // Catch:{ all -> 0x0097 }
            if (r1 == 0) goto L_0x0019
            boolean r2 = r1.noNewStreams     // Catch:{ all -> 0x0097 }
            if (r2 != 0) goto L_0x0019
            monitor-exit(r0)     // Catch:{ all -> 0x0097 }
            return r1
        L_0x0019:
            com.jobstrak.drawingfun.lib.okhttp3.internal.Internal r2 = com.jobstrak.drawingfun.lib.okhttp3.internal.Internal.instance     // Catch:{ all -> 0x0097 }
            com.jobstrak.drawingfun.lib.okhttp3.ConnectionPool r3 = r9.connectionPool     // Catch:{ all -> 0x0097 }
            com.jobstrak.drawingfun.lib.okhttp3.Address r4 = r9.address     // Catch:{ all -> 0x0097 }
            com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection r2 = r2.get(r3, r4, r9)     // Catch:{ all -> 0x0097 }
            if (r2 == 0) goto L_0x0029
            r9.connection = r2     // Catch:{ all -> 0x0097 }
            monitor-exit(r0)     // Catch:{ all -> 0x0097 }
            return r2
        L_0x0029:
            com.jobstrak.drawingfun.lib.okhttp3.Route r3 = r9.route     // Catch:{ all -> 0x0097 }
            r1 = r3
            monitor-exit(r0)     // Catch:{ all -> 0x0097 }
            if (r1 != 0) goto L_0x0040
            com.jobstrak.drawingfun.lib.okhttp3.internal.http.RouteSelector r0 = r9.routeSelector
            com.jobstrak.drawingfun.lib.okhttp3.Route r0 = r0.next()
            com.jobstrak.drawingfun.lib.okhttp3.ConnectionPool r2 = r9.connectionPool
            monitor-enter(r2)
            r9.route = r0     // Catch:{ all -> 0x003d }
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
            r1 = r0
            goto L_0x0040
        L_0x003d:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
            throw r1
        L_0x0040:
            com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection r0 = new com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection
            r0.<init>(r1)
            r8 = r0
            r9.acquire(r8)
            com.jobstrak.drawingfun.lib.okhttp3.ConnectionPool r2 = r9.connectionPool
            monitor-enter(r2)
            com.jobstrak.drawingfun.lib.okhttp3.internal.Internal r0 = com.jobstrak.drawingfun.lib.okhttp3.internal.Internal.instance     // Catch:{ all -> 0x007c }
            com.jobstrak.drawingfun.lib.okhttp3.ConnectionPool r3 = r9.connectionPool     // Catch:{ all -> 0x007c }
            r0.put(r3, r8)     // Catch:{ all -> 0x007c }
            r9.connection = r8     // Catch:{ all -> 0x007c }
            boolean r0 = r9.canceled     // Catch:{ all -> 0x007c }
            if (r0 != 0) goto L_0x0074
            monitor-exit(r2)     // Catch:{ all -> 0x007c }
            com.jobstrak.drawingfun.lib.okhttp3.Address r0 = r9.address
            java.util.List r6 = r0.connectionSpecs()
            r2 = r8
            r3 = r10
            r4 = r11
            r5 = r12
            r7 = r13
            r2.connect(r3, r4, r5, r6, r7)
            com.jobstrak.drawingfun.lib.okhttp3.internal.RouteDatabase r0 = r9.routeDatabase()
            com.jobstrak.drawingfun.lib.okhttp3.Route r2 = r8.route()
            r0.connected(r2)
            return r8
        L_0x0074:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x007c }
            java.lang.String r3 = "Canceled"
            r0.<init>(r3)     // Catch:{ all -> 0x007c }
            throw r0     // Catch:{ all -> 0x007c }
        L_0x007c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x007c }
            throw r0
        L_0x007f:
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "Canceled"
            r1.<init>(r2)     // Catch:{ all -> 0x0097 }
            throw r1     // Catch:{ all -> 0x0097 }
        L_0x0087:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "stream != null"
            r1.<init>(r2)     // Catch:{ all -> 0x0097 }
            throw r1     // Catch:{ all -> 0x0097 }
        L_0x008f:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "released"
            r1.<init>(r2)     // Catch:{ all -> 0x0097 }
            throw r1     // Catch:{ all -> 0x0097 }
        L_0x0097:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0097 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jobstrak.drawingfun.lib.okhttp3.internal.http.StreamAllocation.findConnection(int, int, int, boolean):com.jobstrak.drawingfun.lib.okhttp3.internal.io.RealConnection");
    }

    public void streamFinished(boolean noNewStreams, HttpStream stream2) {
        synchronized (this.connectionPool) {
            if (stream2 != null) {
                if (stream2 == this.stream) {
                    if (!noNewStreams) {
                        this.connection.successCount++;
                    }
                }
            }
            throw new IllegalStateException("expected " + this.stream + " but was " + stream2);
        }
        deallocate(noNewStreams, false, true);
    }

    public HttpStream stream() {
        HttpStream httpStream;
        synchronized (this.connectionPool) {
            httpStream = this.stream;
        }
        return httpStream;
    }

    private RouteDatabase routeDatabase() {
        return Internal.instance.routeDatabase(this.connectionPool);
    }

    public synchronized RealConnection connection() {
        return this.connection;
    }

    public void release() {
        deallocate(false, true, false);
    }

    public void noNewStreams() {
        deallocate(true, false, false);
    }

    private void deallocate(boolean noNewStreams, boolean released2, boolean streamFinished) {
        RealConnection connectionToClose = null;
        synchronized (this.connectionPool) {
            if (streamFinished) {
                try {
                    this.stream = null;
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
            if (released2) {
                this.released = true;
            }
            if (this.connection != null) {
                if (noNewStreams) {
                    this.connection.noNewStreams = true;
                }
                if (this.stream == null && (this.released || this.connection.noNewStreams)) {
                    release(this.connection);
                    if (this.connection.allocations.isEmpty()) {
                        this.connection.idleAtNanos = System.nanoTime();
                        if (Internal.instance.connectionBecameIdle(this.connectionPool, this.connection)) {
                            connectionToClose = this.connection;
                        }
                    }
                    this.connection = null;
                }
            }
        }
        if (connectionToClose != null) {
            Util.closeQuietly(connectionToClose.socket());
        }
    }

    public void cancel() {
        HttpStream streamToCancel;
        RealConnection connectionToCancel;
        synchronized (this.connectionPool) {
            this.canceled = true;
            streamToCancel = this.stream;
            connectionToCancel = this.connection;
        }
        if (streamToCancel != null) {
            streamToCancel.cancel();
        } else if (connectionToCancel != null) {
            connectionToCancel.cancel();
        }
    }

    public void connectionFailed(IOException e) {
        synchronized (this.connectionPool) {
            if (this.connection != null && this.connection.successCount == 0) {
                if (!(this.route == null || e == null)) {
                    this.routeSelector.connectFailed(this.route, e);
                }
                this.route = null;
            }
        }
        deallocate(true, false, true);
    }

    public void acquire(RealConnection connection2) {
        connection2.allocations.add(new WeakReference(this));
    }

    private void release(RealConnection connection2) {
        int size = connection2.allocations.size();
        for (int i = 0; i < size; i++) {
            if (connection2.allocations.get(i).get() == this) {
                connection2.allocations.remove(i);
                return;
            }
        }
        throw new IllegalStateException();
    }

    public boolean recover(IOException e, Sink requestBodyOut) {
        if (this.connection != null) {
            connectionFailed(e);
        }
        boolean canRetryRequestBody = requestBodyOut == null || (requestBodyOut instanceof RetryableSink);
        RouteSelector routeSelector2 = this.routeSelector;
        return (routeSelector2 == null || routeSelector2.hasNext()) && isRecoverable(e) && canRetryRequestBody;
    }

    private boolean isRecoverable(IOException e) {
        if (e instanceof ProtocolException) {
            return false;
        }
        if (e instanceof InterruptedIOException) {
            return e instanceof SocketTimeoutException;
        }
        if ((!(e instanceof SSLHandshakeException) || !(e.getCause() instanceof CertificateException)) && !(e instanceof SSLPeerUnverifiedException)) {
            return true;
        }
        return false;
    }

    public String toString() {
        return this.address.toString();
    }
}
