package com.jobstrak.drawingfun.lib.mopub.network;

import android.net.SSLCertificateSocketFactory;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jobstrak.drawingfun.lib.mopub.common.Preconditions;
import com.jobstrak.drawingfun.lib.mopub.common.VisibleForTesting;
import com.jobstrak.drawingfun.lib.mopub.common.logging.MoPubLog;
import com.jobstrak.drawingfun.lib.mopub.common.util.Reflection;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class CustomSSLSocketFactory extends SSLSocketFactory {
    @Nullable
    private SSLSocketFactory mCertificateSocketFactory;

    private CustomSSLSocketFactory() {
    }

    @NonNull
    public static CustomSSLSocketFactory getDefault(int handshakeTimeoutMillis) {
        CustomSSLSocketFactory factory = new CustomSSLSocketFactory();
        factory.mCertificateSocketFactory = SSLCertificateSocketFactory.getDefault(handshakeTimeoutMillis, null);
        return factory;
    }

    public Socket createSocket() throws IOException {
        SSLSocketFactory sSLSocketFactory = this.mCertificateSocketFactory;
        if (sSLSocketFactory != null) {
            Socket socket = sSLSocketFactory.createSocket();
            enableTlsIfAvailable(socket);
            return socket;
        }
        throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
    }

    public Socket createSocket(String host, int i) throws IOException, UnknownHostException {
        SSLSocketFactory sSLSocketFactory = this.mCertificateSocketFactory;
        if (sSLSocketFactory != null) {
            Socket socket = sSLSocketFactory.createSocket(host, i);
            enableTlsIfAvailable(socket);
            return socket;
        }
        throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
    }

    public Socket createSocket(String host, int port, InetAddress localhost, int localPort) throws IOException, UnknownHostException {
        SSLSocketFactory sSLSocketFactory = this.mCertificateSocketFactory;
        if (sSLSocketFactory != null) {
            Socket socket = sSLSocketFactory.createSocket(host, port, localhost, localPort);
            enableTlsIfAvailable(socket);
            return socket;
        }
        throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
    }

    public Socket createSocket(InetAddress address, int port) throws IOException {
        SSLSocketFactory sSLSocketFactory = this.mCertificateSocketFactory;
        if (sSLSocketFactory != null) {
            Socket socket = sSLSocketFactory.createSocket(address, port);
            enableTlsIfAvailable(socket);
            return socket;
        }
        throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
    }

    public Socket createSocket(InetAddress address, int port, InetAddress localhost, int localPort) throws IOException {
        SSLSocketFactory sSLSocketFactory = this.mCertificateSocketFactory;
        if (sSLSocketFactory != null) {
            Socket socket = sSLSocketFactory.createSocket(address, port, localhost, localPort);
            enableTlsIfAvailable(socket);
            return socket;
        }
        throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
    }

    public String[] getDefaultCipherSuites() {
        SSLSocketFactory sSLSocketFactory = this.mCertificateSocketFactory;
        if (sSLSocketFactory == null) {
            return new String[0];
        }
        return sSLSocketFactory.getDefaultCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        SSLSocketFactory sSLSocketFactory = this.mCertificateSocketFactory;
        if (sSLSocketFactory == null) {
            return new String[0];
        }
        return sSLSocketFactory.getSupportedCipherSuites();
    }

    public Socket createSocket(Socket socketParam, String host, int port, boolean autoClose) throws IOException {
        if (this.mCertificateSocketFactory == null) {
            throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
        } else if (Build.VERSION.SDK_INT < 23) {
            if (autoClose && socketParam != null) {
                socketParam.close();
            }
            Socket socket = this.mCertificateSocketFactory.createSocket(InetAddressUtils.getInetAddressByName(host), port);
            enableTlsIfAvailable(socket);
            doManualServerNameIdentification(socket, host);
            return socket;
        } else {
            Socket socket2 = this.mCertificateSocketFactory.createSocket(socketParam, host, port, autoClose);
            enableTlsIfAvailable(socket2);
            return socket2;
        }
    }

    private void doManualServerNameIdentification(@NonNull Socket socket, @Nullable String host) throws IOException {
        Preconditions.checkNotNull(socket);
        SSLSocketFactory sSLSocketFactory = this.mCertificateSocketFactory;
        if (sSLSocketFactory == null) {
            throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
        } else if (socket instanceof SSLSocket) {
            SSLSocket sslSocket = (SSLSocket) socket;
            setHostnameOnSocket((SSLCertificateSocketFactory) sSLSocketFactory, sslSocket, host);
            verifyServerName(sslSocket, host);
        }
    }

    @VisibleForTesting
    static void setHostnameOnSocket(@NonNull SSLCertificateSocketFactory certificateSocketFactory, @NonNull SSLSocket sslSocket, @Nullable String host) {
        Preconditions.checkNotNull(certificateSocketFactory);
        Preconditions.checkNotNull(sslSocket);
        if (Build.VERSION.SDK_INT >= 17) {
            certificateSocketFactory.setHostname(sslSocket, host);
            return;
        }
        try {
            new Reflection.MethodBuilder(sslSocket, "setHostname").addParam(String.class, host).execute();
        } catch (Exception e) {
            MoPubLog.d("Unable to call setHostname() on the socket");
        }
    }

    @VisibleForTesting
    static void verifyServerName(@NonNull SSLSocket sslSocket, @Nullable String host) throws IOException {
        Preconditions.checkNotNull(sslSocket);
        sslSocket.startHandshake();
        if (!HttpsURLConnection.getDefaultHostnameVerifier().verify(host, sslSocket.getSession())) {
            throw new SSLHandshakeException("Server Name Identification failed.");
        }
    }

    private void enableTlsIfAvailable(@Nullable Socket socket) {
        if (socket instanceof SSLSocket) {
            SSLSocket sslSocket = (SSLSocket) socket;
            sslSocket.setEnabledProtocols(sslSocket.getSupportedProtocols());
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    @Deprecated
    public void setCertificateSocketFactory(@NonNull SSLSocketFactory sslSocketFactory) {
        this.mCertificateSocketFactory = sslSocketFactory;
    }
}
