package org.jsoup.nodes;

import java.util.Arrays;
import java.util.Map;
import kotlin.text.Typography;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;

public class Attribute implements Cloneable, Map.Entry {
    private static final String[] a = {"allowfullscreen", "async", "autofocus", "checked", "compact", "declare", "default", "defer", "disabled", "formnovalidate", "hidden", "inert", "ismap", "itemscope", "multiple", "muted", "nohref", "noresize", "noshade", "novalidate", "nowrap", "open", "readonly", "required", "reversed", "seamless", "selected", "sortable", "truespeed", "typemustmatch"};
    private String b;
    private String c;

    public Attribute(String str, String str2) {
        Validate.notEmpty(str);
        Validate.notNull(str2);
        this.b = str.trim().toLowerCase();
        this.c = str2;
    }

    public static Attribute createFromEncoded(String str, String str2) {
        return new Attribute(str, Entities.b(str2));
    }

    /* access modifiers changed from: protected */
    public final void a(StringBuilder sb, Document.OutputSettings outputSettings) {
        sb.append(this.b);
        if (!(("".equals(this.c) || this.c.equalsIgnoreCase(this.b)) && outputSettings.syntax() == Document.OutputSettings.Syntax.html && Arrays.binarySearch(a, this.b) >= 0)) {
            sb.append("=\"");
            Entities.a(sb, this.c, outputSettings, true, false);
            sb.append((char) Typography.quote);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return this.b.startsWith("data-") && this.b.length() > 5;
    }

    public Attribute clone() {
        try {
            return (Attribute) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Attribute)) {
            return false;
        }
        Attribute attribute = (Attribute) obj;
        if (this.b == null ? attribute.b != null : !this.b.equals(attribute.b)) {
            return false;
        }
        if (this.c != null) {
            if (this.c.equals(attribute.c)) {
                return true;
            }
        } else if (attribute.c == null) {
            return true;
        }
        return false;
    }

    public String getKey() {
        return this.b;
    }

    public String getValue() {
        return this.c;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.b != null ? this.b.hashCode() : 0) * 31;
        if (this.c != null) {
            i = this.c.hashCode();
        }
        return hashCode + i;
    }

    public String html() {
        StringBuilder sb = new StringBuilder();
        a(sb, new Document("").outputSettings());
        return sb.toString();
    }

    public void setKey(String str) {
        Validate.notEmpty(str);
        this.b = str.trim().toLowerCase();
    }

    public String setValue(String str) {
        Validate.notNull(str);
        String str2 = this.c;
        this.c = str;
        return str2;
    }

    public String toString() {
        return html();
    }
}
