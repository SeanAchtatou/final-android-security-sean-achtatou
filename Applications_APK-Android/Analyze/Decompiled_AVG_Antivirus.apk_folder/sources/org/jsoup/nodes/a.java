package org.jsoup.nodes;

import java.util.AbstractMap;
import java.util.LinkedHashMap;
import java.util.Set;

final class a extends AbstractMap {
    final /* synthetic */ Attributes a;

    private a(Attributes attributes) {
        this.a = attributes;
        if (attributes.a == null) {
            LinkedHashMap unused = attributes.a = new LinkedHashMap(2);
        }
    }

    /* synthetic */ a(Attributes attributes, byte b) {
        this(attributes);
    }

    public final Set entrySet() {
        return new c(this, (byte) 0);
    }

    public final /* synthetic */ Object put(Object obj, Object obj2) {
        String str = (String) obj2;
        String a2 = Attributes.a((String) obj);
        String value = this.a.hasKey(a2) ? ((Attribute) this.a.a.get(a2)).getValue() : null;
        this.a.a.put(a2, new Attribute(a2, str));
        return value;
    }
}
