package org.jsoup.nodes;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

public class Document extends Element {
    private OutputSettings f = new OutputSettings();
    private QuirksMode g = QuirksMode.noQuirks;
    private String h;

    public class OutputSettings implements Cloneable {
        private Entities.EscapeMode a = Entities.EscapeMode.base;
        private Charset b = Charset.forName("UTF-8");
        private CharsetEncoder c = this.b.newEncoder();
        private boolean d = true;
        private boolean e = false;
        private int f = 1;
        private Syntax g = Syntax.html;

        public enum Syntax {
            html,
            xml
        }

        /* access modifiers changed from: package-private */
        public final CharsetEncoder a() {
            return this.c;
        }

        public Charset charset() {
            return this.b;
        }

        public OutputSettings charset(String str) {
            charset(Charset.forName(str));
            return this;
        }

        public OutputSettings charset(Charset charset) {
            this.b = charset;
            this.c = charset.newEncoder();
            return this;
        }

        public OutputSettings clone() {
            try {
                OutputSettings outputSettings = (OutputSettings) super.clone();
                outputSettings.charset(this.b.name());
                outputSettings.a = Entities.EscapeMode.valueOf(this.a.name());
                return outputSettings;
            } catch (CloneNotSupportedException e2) {
                throw new RuntimeException(e2);
            }
        }

        public OutputSettings escapeMode(Entities.EscapeMode escapeMode) {
            this.a = escapeMode;
            return this;
        }

        public Entities.EscapeMode escapeMode() {
            return this.a;
        }

        public int indentAmount() {
            return this.f;
        }

        public OutputSettings indentAmount(int i) {
            Validate.isTrue(i >= 0);
            this.f = i;
            return this;
        }

        public OutputSettings outline(boolean z) {
            this.e = z;
            return this;
        }

        public boolean outline() {
            return this.e;
        }

        public OutputSettings prettyPrint(boolean z) {
            this.d = z;
            return this;
        }

        public boolean prettyPrint() {
            return this.d;
        }

        public Syntax syntax() {
            return this.g;
        }

        public OutputSettings syntax(Syntax syntax) {
            this.g = syntax;
            return this;
        }
    }

    public enum QuirksMode {
        noQuirks,
        quirks,
        limitedQuirks
    }

    public Document(String str) {
        super(Tag.valueOf("#root"), str);
        this.h = str;
    }

    private Element a(String str, Node node) {
        if (node.nodeName().equals(str)) {
            return (Element) node;
        }
        for (Node a : node.b) {
            Element a2 = a(str, a);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    private void a(String str, Element element) {
        int i = 1;
        Elements elementsByTag = getElementsByTag(str);
        Element first = elementsByTag.first();
        if (elementsByTag.size() > 1) {
            ArrayList<Node> arrayList = new ArrayList<>();
            while (true) {
                int i2 = i;
                if (i2 >= elementsByTag.size()) {
                    break;
                }
                Element element2 = elementsByTag.get(i2);
                for (Node add : element2.b) {
                    arrayList.add(add);
                }
                element2.remove();
                i = i2 + 1;
            }
            for (Node appendChild : arrayList) {
                first.appendChild(appendChild);
            }
        }
        if (!first.parent().equals(element)) {
            element.appendChild(first);
        }
    }

    private void b(Element element) {
        ArrayList arrayList = new ArrayList();
        for (Node node : element.b) {
            if (node instanceof TextNode) {
                TextNode textNode = (TextNode) node;
                if (!textNode.isBlank()) {
                    arrayList.add(textNode);
                }
            }
        }
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            Node node2 = (Node) arrayList.get(size);
            element.b(node2);
            body().prependChild(new TextNode(" ", ""));
            body().prependChild(node2);
        }
    }

    public static Document createShell(String str) {
        Validate.notNull(str);
        Document document = new Document(str);
        Element appendElement = document.appendElement("html");
        appendElement.appendElement("head");
        appendElement.appendElement("body");
        return document;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.nodes.Document.a(java.lang.String, org.jsoup.nodes.Node):org.jsoup.nodes.Element
     arg types: [java.lang.String, org.jsoup.nodes.Document]
     candidates:
      org.jsoup.nodes.Document.a(java.lang.String, org.jsoup.nodes.Element):void
      org.jsoup.nodes.Element.a(org.jsoup.nodes.Element, java.util.List):java.lang.Integer
      org.jsoup.nodes.Element.a(java.lang.StringBuilder, org.jsoup.nodes.TextNode):void
      org.jsoup.nodes.Node.a(int, java.lang.String):void
      org.jsoup.nodes.Node.a(org.jsoup.nodes.Node, org.jsoup.nodes.Node):void
      org.jsoup.nodes.Node.a(int, org.jsoup.nodes.Node[]):void
      org.jsoup.nodes.Document.a(java.lang.String, org.jsoup.nodes.Node):org.jsoup.nodes.Element */
    public Element body() {
        return a("body", (Node) this);
    }

    public Document clone() {
        Document document = (Document) super.clone();
        document.f = this.f.clone();
        return document;
    }

    public Element createElement(String str) {
        return new Element(Tag.valueOf(str), baseUri());
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.nodes.Document.a(java.lang.String, org.jsoup.nodes.Node):org.jsoup.nodes.Element
     arg types: [java.lang.String, org.jsoup.nodes.Document]
     candidates:
      org.jsoup.nodes.Document.a(java.lang.String, org.jsoup.nodes.Element):void
      org.jsoup.nodes.Element.a(org.jsoup.nodes.Element, java.util.List):java.lang.Integer
      org.jsoup.nodes.Element.a(java.lang.StringBuilder, org.jsoup.nodes.TextNode):void
      org.jsoup.nodes.Node.a(int, java.lang.String):void
      org.jsoup.nodes.Node.a(org.jsoup.nodes.Node, org.jsoup.nodes.Node):void
      org.jsoup.nodes.Node.a(int, org.jsoup.nodes.Node[]):void
      org.jsoup.nodes.Document.a(java.lang.String, org.jsoup.nodes.Node):org.jsoup.nodes.Element */
    public Element head() {
        return a("head", (Node) this);
    }

    public String location() {
        return this.h;
    }

    public String nodeName() {
        return "#document";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.nodes.Document.a(java.lang.String, org.jsoup.nodes.Node):org.jsoup.nodes.Element
     arg types: [java.lang.String, org.jsoup.nodes.Document]
     candidates:
      org.jsoup.nodes.Document.a(java.lang.String, org.jsoup.nodes.Element):void
      org.jsoup.nodes.Element.a(org.jsoup.nodes.Element, java.util.List):java.lang.Integer
      org.jsoup.nodes.Element.a(java.lang.StringBuilder, org.jsoup.nodes.TextNode):void
      org.jsoup.nodes.Node.a(int, java.lang.String):void
      org.jsoup.nodes.Node.a(org.jsoup.nodes.Node, org.jsoup.nodes.Node):void
      org.jsoup.nodes.Node.a(int, org.jsoup.nodes.Node[]):void
      org.jsoup.nodes.Document.a(java.lang.String, org.jsoup.nodes.Node):org.jsoup.nodes.Element */
    public Document normalise() {
        Element a = a("html", (Node) this);
        if (a == null) {
            a = appendElement("html");
        }
        if (head() == null) {
            a.prependElement("head");
        }
        if (body() == null) {
            a.appendElement("body");
        }
        b(head());
        b(a);
        b((Element) this);
        a("head", a);
        a("body", a);
        return this;
    }

    public String outerHtml() {
        return super.html();
    }

    public OutputSettings outputSettings() {
        return this.f;
    }

    public Document outputSettings(OutputSettings outputSettings) {
        Validate.notNull(outputSettings);
        this.f = outputSettings;
        return this;
    }

    public QuirksMode quirksMode() {
        return this.g;
    }

    public Document quirksMode(QuirksMode quirksMode) {
        this.g = quirksMode;
        return this;
    }

    public Element text(String str) {
        body().text(str);
        return this;
    }

    public String title() {
        Element first = getElementsByTag("title").first();
        return first != null ? StringUtil.normaliseWhitespace(first.text()).trim() : "";
    }

    public void title(String str) {
        Validate.notNull(str);
        Element first = getElementsByTag("title").first();
        if (first == null) {
            head().appendElement("title").text(str);
        } else {
            first.text(str);
        }
    }
}
