package org.jsoup.nodes;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

public abstract class Node implements Cloneable {
    Node a;
    List b;
    Attributes c;
    String d;
    int e;

    protected Node() {
        this.b = Collections.emptyList();
        this.c = null;
    }

    protected Node(String str) {
        this(str, new Attributes());
    }

    protected Node(String str, Attributes attributes) {
        Validate.notNull(str);
        Validate.notNull(attributes);
        this.b = new ArrayList(4);
        this.d = str.trim();
        this.c = attributes;
    }

    private void a(int i, String str) {
        Validate.notNull(str);
        Validate.notNull(this.a);
        List parseFragment = Parser.parseFragment(str, parent() instanceof Element ? (Element) parent() : null, baseUri());
        this.a.a(i, (Node[]) parseFragment.toArray(new Node[parseFragment.size()]));
    }

    private void a(Node node) {
        if (node.a != null) {
            node.a.b(node);
        }
        if (node.a != null) {
            node.a.b(node);
        }
        node.a = this;
    }

    private void a(Node node, Node node2) {
        Validate.isTrue(node.a == this);
        Validate.notNull(node2);
        if (node2.a != null) {
            node2.a.b(node2);
        }
        Integer valueOf = Integer.valueOf(node.siblingIndex());
        this.b.set(valueOf.intValue(), node2);
        node2.a = this;
        node2.e = valueOf.intValue();
        node.a = null;
    }

    private void b() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.b.size()) {
                ((Node) this.b.get(i2)).e = i2;
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private Node c(Node node) {
        try {
            Node node2 = (Node) super.clone();
            node2.a = node;
            node2.e = node == null ? 0 : this.e;
            node2.c = this.c != null ? this.c.clone() : null;
            node2.d = this.d;
            node2.b = new ArrayList(this.b.size());
            for (Node add : this.b) {
                node2.b.add(add);
            }
            return node2;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    protected static void c(StringBuilder sb, int i, Document.OutputSettings outputSettings) {
        sb.append("\n").append(StringUtil.padding(outputSettings.indentAmount() * i));
    }

    /* access modifiers changed from: package-private */
    public final Document.OutputSettings a() {
        return ownerDocument() != null ? ownerDocument().outputSettings() : new Document("").outputSettings();
    }

    /* access modifiers changed from: protected */
    public final void a(int i, Node... nodeArr) {
        Validate.noNullElements(nodeArr);
        for (int length = nodeArr.length - 1; length >= 0; length--) {
            Node node = nodeArr[length];
            a(node);
            this.b.add(i, node);
        }
        b();
    }

    /* access modifiers changed from: protected */
    public final void a(StringBuilder sb) {
        new NodeTraversor(new f(sb, a())).traverse(this);
    }

    /* access modifiers changed from: package-private */
    public abstract void a(StringBuilder sb, int i, Document.OutputSettings outputSettings);

    /* access modifiers changed from: protected */
    public final void a(Node... nodeArr) {
        for (Node node : nodeArr) {
            a(node);
            this.b.add(node);
            node.e = this.b.size() - 1;
        }
    }

    public String absUrl(String str) {
        Validate.notEmpty(str);
        String attr = attr(str);
        if (!hasAttr(str)) {
            return "";
        }
        try {
            URL url = new URL(this.d);
            try {
                if (attr.startsWith("?")) {
                    attr = url.getPath() + attr;
                }
                return new URL(url, attr).toExternalForm();
            } catch (MalformedURLException e2) {
                return "";
            }
        } catch (MalformedURLException e3) {
            return new URL(attr).toExternalForm();
        }
    }

    public Node after(String str) {
        a(siblingIndex() + 1, str);
        return this;
    }

    public Node after(Node node) {
        Validate.notNull(node);
        Validate.notNull(this.a);
        this.a.a(siblingIndex() + 1, node);
        return this;
    }

    public String attr(String str) {
        Validate.notNull(str);
        return this.c.hasKey(str) ? this.c.get(str) : str.toLowerCase().startsWith("abs:") ? absUrl(str.substring(4)) : "";
    }

    public Node attr(String str, String str2) {
        this.c.put(str, str2);
        return this;
    }

    public Attributes attributes() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public abstract void b(StringBuilder sb, int i, Document.OutputSettings outputSettings);

    /* access modifiers changed from: protected */
    public final void b(Node node) {
        Validate.isTrue(node.a == this);
        this.b.remove(node.siblingIndex());
        b();
        node.a = null;
    }

    public String baseUri() {
        return this.d;
    }

    public Node before(String str) {
        a(siblingIndex(), str);
        return this;
    }

    public Node before(Node node) {
        Validate.notNull(node);
        Validate.notNull(this.a);
        this.a.a(siblingIndex(), node);
        return this;
    }

    public Node childNode(int i) {
        return (Node) this.b.get(i);
    }

    public final int childNodeSize() {
        return this.b.size();
    }

    public List childNodes() {
        return Collections.unmodifiableList(this.b);
    }

    public List childNodesCopy() {
        ArrayList arrayList = new ArrayList(this.b.size());
        for (Node clone : this.b) {
            arrayList.add(clone.clone());
        }
        return arrayList;
    }

    public Node clone() {
        Node c2 = c(null);
        LinkedList linkedList = new LinkedList();
        linkedList.add(c2);
        while (!linkedList.isEmpty()) {
            Node node = (Node) linkedList.remove();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < node.b.size()) {
                    Node c3 = ((Node) node.b.get(i2)).c(node);
                    node.b.set(i2, c3);
                    linkedList.add(c3);
                    i = i2 + 1;
                }
            }
        }
        return c2;
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    public boolean hasAttr(String str) {
        Validate.notNull(str);
        if (str.startsWith("abs:")) {
            String substring = str.substring(4);
            if (this.c.hasKey(substring) && !absUrl(substring).equals("")) {
                return true;
            }
        }
        return this.c.hasKey(str);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.a != null ? this.a.hashCode() : 0) * 31;
        if (this.c != null) {
            i = this.c.hashCode();
        }
        return hashCode + i;
    }

    public Node nextSibling() {
        if (this.a == null) {
            return null;
        }
        List list = this.a.b;
        Integer valueOf = Integer.valueOf(siblingIndex());
        Validate.notNull(valueOf);
        if (list.size() > valueOf.intValue() + 1) {
            return (Node) list.get(valueOf.intValue() + 1);
        }
        return null;
    }

    public abstract String nodeName();

    public String outerHtml() {
        StringBuilder sb = new StringBuilder(128);
        a(sb);
        return sb.toString();
    }

    public Document ownerDocument() {
        if (this instanceof Document) {
            return (Document) this;
        }
        if (this.a == null) {
            return null;
        }
        return this.a.ownerDocument();
    }

    public Node parent() {
        return this.a;
    }

    public final Node parentNode() {
        return this.a;
    }

    public Node previousSibling() {
        if (this.a == null) {
            return null;
        }
        List list = this.a.b;
        Integer valueOf = Integer.valueOf(siblingIndex());
        Validate.notNull(valueOf);
        if (valueOf.intValue() > 0) {
            return (Node) list.get(valueOf.intValue() - 1);
        }
        return null;
    }

    public void remove() {
        Validate.notNull(this.a);
        this.a.b(this);
    }

    public Node removeAttr(String str) {
        Validate.notNull(str);
        this.c.remove(str);
        return this;
    }

    public void replaceWith(Node node) {
        Validate.notNull(node);
        Validate.notNull(this.a);
        this.a.a(this, node);
    }

    public void setBaseUri(String str) {
        Validate.notNull(str);
        traverse(new e(this, str));
    }

    public int siblingIndex() {
        return this.e;
    }

    public List siblingNodes() {
        if (this.a == null) {
            return Collections.emptyList();
        }
        List<Node> list = this.a.b;
        ArrayList arrayList = new ArrayList(list.size() - 1);
        for (Node node : list) {
            if (node != this) {
                arrayList.add(node);
            }
        }
        return arrayList;
    }

    public String toString() {
        return outerHtml();
    }

    public Node traverse(NodeVisitor nodeVisitor) {
        Validate.notNull(nodeVisitor);
        new NodeTraversor(nodeVisitor).traverse(this);
        return this;
    }

    public Node unwrap() {
        Validate.notNull(this.a);
        int i = this.e;
        Node node = this.b.size() > 0 ? (Node) this.b.get(0) : null;
        this.a.a(i, (Node[]) this.b.toArray(new Node[childNodeSize()]));
        remove();
        return node;
    }

    public Node wrap(String str) {
        Validate.notEmpty(str);
        List parseFragment = Parser.parseFragment(str, parent() instanceof Element ? (Element) parent() : null, baseUri());
        Node node = (Node) parseFragment.get(0);
        if (node == null || !(node instanceof Element)) {
            return null;
        }
        Element element = (Element) node;
        Element element2 = element;
        while (true) {
            Elements children = element2.children();
            if (children.size() <= 0) {
                break;
            }
            element2 = (Element) children.get(0);
        }
        this.a.a(this, element);
        element2.a(this);
        if (parseFragment.size() <= 0) {
            return this;
        }
        for (int i = 0; i < parseFragment.size(); i++) {
            Node node2 = (Node) parseFragment.get(i);
            node2.a.b(node2);
            element.appendChild(node2);
        }
        return this;
    }
}
