package org.jsoup.nodes;

import org.jsoup.nodes.Document;
import org.jsoup.select.NodeVisitor;

final class f implements NodeVisitor {
    private StringBuilder a;
    private Document.OutputSettings b;

    f(StringBuilder sb, Document.OutputSettings outputSettings) {
        this.a = sb;
        this.b = outputSettings;
    }

    public final void head(Node node, int i) {
        node.a(this.a, i, this.b);
    }

    public final void tail(Node node, int i) {
        if (!node.nodeName().equals("#text")) {
            node.b(this.a, i, this.b);
        }
    }
}
