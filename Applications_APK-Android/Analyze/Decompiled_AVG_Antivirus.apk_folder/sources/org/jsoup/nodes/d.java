package org.jsoup.nodes;

import org.jsoup.select.NodeVisitor;

final class d implements NodeVisitor {
    final /* synthetic */ StringBuilder a;
    final /* synthetic */ Element b;

    d(Element element, StringBuilder sb) {
        this.b = element;
        this.a = sb;
    }

    public final void head(Node node, int i) {
        if (node instanceof TextNode) {
            Element.b(this.a, (TextNode) node);
        } else if (node instanceof Element) {
            Element element = (Element) node;
            if (this.a.length() <= 0) {
                return;
            }
            if ((element.isBlock() || element.f.getName().equals("br")) && !TextNode.b(this.a)) {
                this.a.append(" ");
            }
        }
    }

    public final void tail(Node node, int i) {
    }
}
