package org.jsoup.nodes;

import org.jsoup.nodes.Document;

public class Comment extends Node {
    public Comment(String str, String str2) {
        super(str2);
        this.c.put("comment", str);
    }

    /* access modifiers changed from: package-private */
    public final void a(StringBuilder sb, int i, Document.OutputSettings outputSettings) {
        if (outputSettings.prettyPrint()) {
            c(sb, i, outputSettings);
        }
        sb.append("<!--").append(getData()).append("-->");
    }

    /* access modifiers changed from: package-private */
    public final void b(StringBuilder sb, int i, Document.OutputSettings outputSettings) {
    }

    public String getData() {
        return this.c.get("comment");
    }

    public String nodeName() {
        return "#comment";
    }

    public String toString() {
        return outerHtml();
    }
}
