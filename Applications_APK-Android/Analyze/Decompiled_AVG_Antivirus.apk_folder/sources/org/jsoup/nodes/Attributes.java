package org.jsoup.nodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;

public class Attributes implements Cloneable, Iterable {
    /* access modifiers changed from: private */
    public LinkedHashMap a = null;

    static /* synthetic */ String a(String str) {
        return "data-" + str;
    }

    /* access modifiers changed from: package-private */
    public final void a(StringBuilder sb, Document.OutputSettings outputSettings) {
        if (this.a != null) {
            for (Map.Entry value : this.a.entrySet()) {
                sb.append(" ");
                ((Attribute) value.getValue()).a(sb, outputSettings);
            }
        }
    }

    public void addAll(Attributes attributes) {
        if (attributes.size() != 0) {
            if (this.a == null) {
                this.a = new LinkedHashMap(attributes.size());
            }
            this.a.putAll(attributes.a);
        }
    }

    public List asList() {
        if (this.a == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(this.a.size());
        for (Map.Entry value : this.a.entrySet()) {
            arrayList.add(value.getValue());
        }
        return Collections.unmodifiableList(arrayList);
    }

    public Attributes clone() {
        if (this.a == null) {
            return new Attributes();
        }
        try {
            Attributes attributes = (Attributes) super.clone();
            attributes.a = new LinkedHashMap(this.a.size());
            Iterator it = iterator();
            while (it.hasNext()) {
                Attribute attribute = (Attribute) it.next();
                attributes.a.put(attribute.getKey(), attribute.clone());
            }
            return attributes;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public Map dataset() {
        return new a(this, (byte) 0);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Attributes)) {
            return false;
        }
        Attributes attributes = (Attributes) obj;
        if (this.a != null) {
            if (this.a.equals(attributes.a)) {
                return true;
            }
        } else if (attributes.a == null) {
            return true;
        }
        return false;
    }

    public String get(String str) {
        Attribute attribute;
        Validate.notEmpty(str);
        return (this.a == null || (attribute = (Attribute) this.a.get(str.toLowerCase())) == null) ? "" : attribute.getValue();
    }

    public boolean hasKey(String str) {
        return this.a != null && this.a.containsKey(str.toLowerCase());
    }

    public int hashCode() {
        if (this.a != null) {
            return this.a.hashCode();
        }
        return 0;
    }

    public String html() {
        StringBuilder sb = new StringBuilder();
        a(sb, new Document("").outputSettings());
        return sb.toString();
    }

    public Iterator iterator() {
        return asList().iterator();
    }

    public void put(String str, String str2) {
        put(new Attribute(str, str2));
    }

    public void put(Attribute attribute) {
        Validate.notNull(attribute);
        if (this.a == null) {
            this.a = new LinkedHashMap(2);
        }
        this.a.put(attribute.getKey(), attribute);
    }

    public void remove(String str) {
        Validate.notEmpty(str);
        if (this.a != null) {
            this.a.remove(str.toLowerCase());
        }
    }

    public int size() {
        if (this.a == null) {
            return 0;
        }
        return this.a.size();
    }

    public String toString() {
        return html();
    }
}
