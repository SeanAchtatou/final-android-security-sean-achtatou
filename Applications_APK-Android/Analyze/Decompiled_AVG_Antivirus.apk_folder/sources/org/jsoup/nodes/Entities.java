package org.jsoup.nodes;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import org.jsoup.parser.Parser;

public class Entities {
    private static final Map a;
    /* access modifiers changed from: private */
    public static final Map b = new HashMap();
    private static final Map c;
    /* access modifiers changed from: private */
    public static final Map d;
    /* access modifiers changed from: private */
    public static final Map e;
    private static final Object[][] f = {new Object[]{"quot", 34}, new Object[]{"amp", 38}, new Object[]{"lt", 60}, new Object[]{"gt", 62}};

    public enum EscapeMode {
        xhtml(Entities.b),
        base(Entities.d),
        extended(Entities.e);
        
        private Map a;

        private EscapeMode(Map map) {
            this.a = map;
        }

        public final Map getMap() {
            return this.a;
        }
    }

    static {
        Map c2 = c("entities-base.properties");
        c = c2;
        d = a(c2);
        Map c3 = c("entities-full.properties");
        a = c3;
        e = a(c3);
        for (Object[] objArr : f) {
            b.put(Character.valueOf((char) ((Integer) objArr[1]).intValue()), (String) objArr[0]);
        }
    }

    private Entities() {
    }

    static String a(String str) {
        return Parser.unescapeEntities(str, false);
    }

    private static Map a(Map map) {
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : map.entrySet()) {
            Character ch = (Character) entry.getValue();
            String str = (String) entry.getKey();
            if (!hashMap.containsKey(ch)) {
                hashMap.put(ch, str);
            } else if (str.toLowerCase().equals(str)) {
                hashMap.put(ch, str);
            }
        }
        return hashMap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00be  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(java.lang.StringBuilder r11, java.lang.String r12, org.jsoup.nodes.Document.OutputSettings r13, boolean r14, boolean r15) {
        /*
            r10 = 59
            r2 = 0
            org.jsoup.nodes.Entities$EscapeMode r4 = r13.escapeMode()
            java.nio.charset.CharsetEncoder r5 = r13.a()
            java.util.Map r6 = r4.getMap()
            int r7 = r12.length()
            r3 = r2
            r0 = r2
        L_0x0015:
            if (r3 >= r7) goto L_0x00e7
            int r8 = r12.codePointAt(r3)
            if (r15 == 0) goto L_0x00e8
            boolean r1 = org.jsoup.helper.StringUtil.isWhitespace(r8)
            if (r1 == 0) goto L_0x0032
            if (r0 != 0) goto L_0x002b
            r0 = 32
            r11.append(r0)
            r0 = 1
        L_0x002b:
            int r1 = java.lang.Character.charCount(r8)
            int r1 = r1 + r3
            r3 = r1
            goto L_0x0015
        L_0x0032:
            r1 = r2
        L_0x0033:
            r0 = 65536(0x10000, float:9.18355E-41)
            if (r8 >= r0) goto L_0x00be
            char r0 = (char) r8
            switch(r0) {
                case 34: goto L_0x0079;
                case 38: goto L_0x0046;
                case 60: goto L_0x005d;
                case 62: goto L_0x006b;
                case 160: goto L_0x004d;
                default: goto L_0x003b;
            }
        L_0x003b:
            boolean r9 = r5.canEncode(r0)
            if (r9 == 0) goto L_0x0087
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x0046:
            java.lang.String r0 = "&amp;"
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x004d:
            org.jsoup.nodes.Entities$EscapeMode r9 = org.jsoup.nodes.Entities.EscapeMode.xhtml
            if (r4 == r9) goto L_0x0058
            java.lang.String r0 = "&nbsp;"
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x0058:
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x005d:
            if (r14 != 0) goto L_0x0066
            java.lang.String r0 = "&lt;"
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x0066:
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x006b:
            if (r14 != 0) goto L_0x0074
            java.lang.String r0 = "&gt;"
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x0074:
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x0079:
            if (r14 == 0) goto L_0x0082
            java.lang.String r0 = "&quot;"
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x0082:
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x0087:
            java.lang.Character r9 = java.lang.Character.valueOf(r0)
            boolean r9 = r6.containsKey(r9)
            if (r9 == 0) goto L_0x00aa
            r9 = 38
            java.lang.StringBuilder r9 = r11.append(r9)
            java.lang.Character r0 = java.lang.Character.valueOf(r0)
            java.lang.Object r0 = r6.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r9.append(r0)
            r0.append(r10)
            r0 = r1
            goto L_0x002b
        L_0x00aa:
            java.lang.String r0 = "&#x"
            java.lang.StringBuilder r0 = r11.append(r0)
            java.lang.String r9 = java.lang.Integer.toHexString(r8)
            java.lang.StringBuilder r0 = r0.append(r9)
            r0.append(r10)
            r0 = r1
            goto L_0x002b
        L_0x00be:
            java.lang.String r0 = new java.lang.String
            char[] r9 = java.lang.Character.toChars(r8)
            r0.<init>(r9)
            boolean r9 = r5.canEncode(r0)
            if (r9 == 0) goto L_0x00d3
            r11.append(r0)
            r0 = r1
            goto L_0x002b
        L_0x00d3:
            java.lang.String r0 = "&#x"
            java.lang.StringBuilder r0 = r11.append(r0)
            java.lang.String r9 = java.lang.Integer.toHexString(r8)
            java.lang.StringBuilder r0 = r0.append(r9)
            r0.append(r10)
            r0 = r1
            goto L_0x002b
        L_0x00e7:
            return
        L_0x00e8:
            r1 = r0
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.nodes.Entities.a(java.lang.StringBuilder, java.lang.String, org.jsoup.nodes.Document$OutputSettings, boolean, boolean):void");
    }

    static String b(String str) {
        return Parser.unescapeEntities(str, true);
    }

    private static Map c(String str) {
        Properties properties = new Properties();
        HashMap hashMap = new HashMap();
        try {
            InputStream resourceAsStream = Entities.class.getResourceAsStream(str);
            properties.load(resourceAsStream);
            resourceAsStream.close();
            for (Map.Entry entry : properties.entrySet()) {
                hashMap.put((String) entry.getKey(), Character.valueOf((char) Integer.parseInt((String) entry.getValue(), 16)));
            }
            return hashMap;
        } catch (IOException e2) {
            throw new MissingResourceException("Error loading entities resource: " + e2.getMessage(), "Entities", str);
        }
    }

    public static Character getCharacterByName(String str) {
        return (Character) a.get(str);
    }

    public static boolean isBaseNamedEntity(String str) {
        return c.containsKey(str);
    }

    public static boolean isNamedEntity(String str) {
        return a.containsKey(str);
    }
}
