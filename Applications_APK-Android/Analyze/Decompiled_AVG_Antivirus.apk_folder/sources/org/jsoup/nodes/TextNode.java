package org.jsoup.nodes;

import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;

public class TextNode extends Node {
    String f;

    public TextNode(String str, String str2) {
        this.d = str2;
        this.f = str;
    }

    private void b() {
        if (this.c == null) {
            this.c = new Attributes();
            this.c.put("text", this.f);
        }
    }

    static boolean b(StringBuilder sb) {
        return sb.length() != 0 && sb.charAt(sb.length() + -1) == ' ';
    }

    public static TextNode createFromEncoded(String str, String str2) {
        return new TextNode(Entities.a(str), str2);
    }

    /* access modifiers changed from: package-private */
    public final void a(StringBuilder sb, int i, Document.OutputSettings outputSettings) {
        if (outputSettings.prettyPrint() && ((siblingIndex() == 0 && (this.a instanceof Element) && ((Element) this.a).tag().formatAsBlock() && !isBlank()) || (outputSettings.outline() && siblingNodes().size() > 0 && !isBlank()))) {
            c(sb, i, outputSettings);
        }
        Entities.a(sb, getWholeText(), outputSettings, false, outputSettings.prettyPrint() && (parent() instanceof Element) && !Element.a((Element) parent()));
    }

    public String absUrl(String str) {
        b();
        return super.absUrl(str);
    }

    public String attr(String str) {
        b();
        return super.attr(str);
    }

    public Node attr(String str, String str2) {
        b();
        return super.attr(str, str2);
    }

    public Attributes attributes() {
        b();
        return super.attributes();
    }

    /* access modifiers changed from: package-private */
    public final void b(StringBuilder sb, int i, Document.OutputSettings outputSettings) {
    }

    public String getWholeText() {
        return this.c == null ? this.f : this.c.get("text");
    }

    public boolean hasAttr(String str) {
        b();
        return super.hasAttr(str);
    }

    public boolean isBlank() {
        return StringUtil.isBlank(getWholeText());
    }

    public String nodeName() {
        return "#text";
    }

    public Node removeAttr(String str) {
        b();
        return super.removeAttr(str);
    }

    public TextNode splitText(int i) {
        Validate.isTrue(i >= 0, "Split offset must be not be negative");
        Validate.isTrue(i < this.f.length(), "Split offset must not be greater than current text length");
        String substring = getWholeText().substring(0, i);
        String substring2 = getWholeText().substring(i);
        text(substring);
        TextNode textNode = new TextNode(substring2, baseUri());
        if (parent() != null) {
            parent().a(siblingIndex() + 1, textNode);
        }
        return textNode;
    }

    public String text() {
        return StringUtil.normaliseWhitespace(getWholeText());
    }

    public TextNode text(String str) {
        this.f = str;
        if (this.c != null) {
            this.c.put("text", str);
        }
        return this;
    }

    public String toString() {
        return outerHtml();
    }
}
