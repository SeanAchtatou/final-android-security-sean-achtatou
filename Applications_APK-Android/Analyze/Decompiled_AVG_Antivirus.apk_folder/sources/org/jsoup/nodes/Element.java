package org.jsoup.nodes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import kotlin.text.Typography;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.parser.Tag;
import org.jsoup.select.Collector;
import org.jsoup.select.Elements;
import org.jsoup.select.Evaluator;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.Selector;

public class Element extends Node {
    /* access modifiers changed from: private */
    public Tag f;
    private Set g;

    public Element(Tag tag, String str) {
        this(tag, str, new Attributes());
    }

    public Element(Tag tag, String str, Attributes attributes) {
        super(str, attributes);
        Validate.notNull(tag);
        this.f = tag;
    }

    private static Integer a(Element element, List list) {
        Validate.notNull(element);
        Validate.notNull(list);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return null;
            }
            if (((Element) list.get(i2)).equals(element)) {
                return Integer.valueOf(i2);
            }
            i = i2 + 1;
        }
    }

    static boolean a(Node node) {
        if (node == null || !(node instanceof Element)) {
            return false;
        }
        Element element = (Element) node;
        return element.f.preserveWhitespace() || (element.parent() != null && element.parent().f.preserveWhitespace());
    }

    private void b(StringBuilder sb) {
        for (Node node : this.b) {
            if (node instanceof TextNode) {
                b(sb, (TextNode) node);
            } else if ((node instanceof Element) && ((Element) node).f.getName().equals("br") && !TextNode.b(sb)) {
                sb.append(" ");
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(StringBuilder sb, TextNode textNode) {
        String wholeText = textNode.getWholeText();
        if (a(textNode.a)) {
            sb.append(wholeText);
        } else {
            StringUtil.appendNormalisedWhitespace(sb, wholeText, TextNode.b(sb));
        }
    }

    private void c(StringBuilder sb) {
        for (Node a : this.b) {
            a.a(sb);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(StringBuilder sb, int i, Document.OutputSettings outputSettings) {
        if (sb.length() > 0 && outputSettings.prettyPrint() && (this.f.formatAsBlock() || ((parent() != null && parent().tag().formatAsBlock()) || outputSettings.outline()))) {
            c(sb, i, outputSettings);
        }
        sb.append("<").append(tagName());
        this.c.a(sb, outputSettings);
        if (!this.b.isEmpty() || !this.f.isSelfClosing()) {
            sb.append(">");
        } else if (outputSettings.syntax() != Document.OutputSettings.Syntax.html || !this.f.isEmpty()) {
            sb.append(" />");
        } else {
            sb.append((char) Typography.greater);
        }
    }

    public Element addClass(String str) {
        Validate.notNull(str);
        Set classNames = classNames();
        classNames.add(str);
        classNames(classNames);
        return this;
    }

    public Element after(String str) {
        return (Element) super.after(str);
    }

    public Element after(Node node) {
        return (Element) super.after(node);
    }

    public Element append(String str) {
        Validate.notNull(str);
        List parseFragment = Parser.parseFragment(str, this, baseUri());
        a((Node[]) parseFragment.toArray(new Node[parseFragment.size()]));
        return this;
    }

    public Element appendChild(Node node) {
        Validate.notNull(node);
        a(node);
        return this;
    }

    public Element appendElement(String str) {
        Element element = new Element(Tag.valueOf(str), baseUri());
        appendChild(element);
        return element;
    }

    public Element appendText(String str) {
        appendChild(new TextNode(str, baseUri()));
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.nodes.Node.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element
      org.jsoup.nodes.Node.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node */
    public Element attr(String str, String str2) {
        super.attr(str, str2);
        return this;
    }

    /* access modifiers changed from: package-private */
    public final void b(StringBuilder sb, int i, Document.OutputSettings outputSettings) {
        if (!this.b.isEmpty() || !this.f.isSelfClosing()) {
            if (outputSettings.prettyPrint() && !this.b.isEmpty() && (this.f.formatAsBlock() || (outputSettings.outline() && (this.b.size() > 1 || (this.b.size() == 1 && !(this.b.get(0) instanceof TextNode)))))) {
                c(sb, i, outputSettings);
            }
            sb.append("</").append(tagName()).append(">");
        }
    }

    public Element before(String str) {
        return (Element) super.before(str);
    }

    public Element before(Node node) {
        return (Element) super.before(node);
    }

    public Element child(int i) {
        return children().get(i);
    }

    public Elements children() {
        ArrayList arrayList = new ArrayList(this.b.size());
        for (Node node : this.b) {
            if (node instanceof Element) {
                arrayList.add((Element) node);
            }
        }
        return new Elements((List) arrayList);
    }

    public String className() {
        return attr("class");
    }

    public Set classNames() {
        if (this.g == null) {
            this.g = new LinkedHashSet(Arrays.asList(className().split("\\s+")));
        }
        return this.g;
    }

    public Element classNames(Set set) {
        Validate.notNull(set);
        this.c.put("class", StringUtil.join(set, " "));
        return this;
    }

    public Element clone() {
        Element element = (Element) super.clone();
        element.g = null;
        return element;
    }

    public String cssSelector() {
        if (id().length() > 0) {
            return "#" + id();
        }
        StringBuilder sb = new StringBuilder(tagName());
        String join = StringUtil.join(classNames(), ".");
        if (join.length() > 0) {
            sb.append('.').append(join);
        }
        if (parent() == null || (parent() instanceof Document)) {
            return sb.toString();
        }
        sb.insert(0, " > ");
        if (parent().select(sb.toString()).size() > 1) {
            sb.append(String.format(":nth-child(%d)", Integer.valueOf(elementSiblingIndex().intValue() + 1)));
        }
        return parent().cssSelector() + sb.toString();
    }

    public String data() {
        StringBuilder sb = new StringBuilder();
        for (Node node : this.b) {
            if (node instanceof DataNode) {
                sb.append(((DataNode) node).getWholeData());
            } else if (node instanceof Element) {
                sb.append(((Element) node).data());
            }
        }
        return sb.toString();
    }

    public List dataNodes() {
        ArrayList arrayList = new ArrayList();
        for (Node node : this.b) {
            if (node instanceof DataNode) {
                arrayList.add((DataNode) node);
            }
        }
        return Collections.unmodifiableList(arrayList);
    }

    public Map dataset() {
        return this.c.dataset();
    }

    public Integer elementSiblingIndex() {
        if (parent() == null) {
            return 0;
        }
        return a(this, parent().children());
    }

    public Element empty() {
        this.b.clear();
        return this;
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    public Element firstElementSibling() {
        Elements children = parent().children();
        if (children.size() > 1) {
            return (Element) children.get(0);
        }
        return null;
    }

    public Elements getAllElements() {
        return Collector.collect(new Evaluator.AllElements(), this);
    }

    public Element getElementById(String str) {
        Validate.notEmpty(str);
        Elements collect = Collector.collect(new Evaluator.Id(str), this);
        if (collect.size() > 0) {
            return collect.get(0);
        }
        return null;
    }

    public Elements getElementsByAttribute(String str) {
        Validate.notEmpty(str);
        return Collector.collect(new Evaluator.Attribute(str.trim().toLowerCase()), this);
    }

    public Elements getElementsByAttributeStarting(String str) {
        Validate.notEmpty(str);
        return Collector.collect(new Evaluator.AttributeStarting(str.trim().toLowerCase()), this);
    }

    public Elements getElementsByAttributeValue(String str, String str2) {
        return Collector.collect(new Evaluator.AttributeWithValue(str, str2), this);
    }

    public Elements getElementsByAttributeValueContaining(String str, String str2) {
        return Collector.collect(new Evaluator.AttributeWithValueContaining(str, str2), this);
    }

    public Elements getElementsByAttributeValueEnding(String str, String str2) {
        return Collector.collect(new Evaluator.AttributeWithValueEnding(str, str2), this);
    }

    public Elements getElementsByAttributeValueMatching(String str, String str2) {
        try {
            return getElementsByAttributeValueMatching(str, Pattern.compile(str2));
        } catch (PatternSyntaxException e) {
            throw new IllegalArgumentException("Pattern syntax error: " + str2, e);
        }
    }

    public Elements getElementsByAttributeValueMatching(String str, Pattern pattern) {
        return Collector.collect(new Evaluator.AttributeWithValueMatching(str, pattern), this);
    }

    public Elements getElementsByAttributeValueNot(String str, String str2) {
        return Collector.collect(new Evaluator.AttributeWithValueNot(str, str2), this);
    }

    public Elements getElementsByAttributeValueStarting(String str, String str2) {
        return Collector.collect(new Evaluator.AttributeWithValueStarting(str, str2), this);
    }

    public Elements getElementsByClass(String str) {
        Validate.notEmpty(str);
        return Collector.collect(new Evaluator.Class(str), this);
    }

    public Elements getElementsByIndexEquals(int i) {
        return Collector.collect(new Evaluator.IndexEquals(i), this);
    }

    public Elements getElementsByIndexGreaterThan(int i) {
        return Collector.collect(new Evaluator.IndexGreaterThan(i), this);
    }

    public Elements getElementsByIndexLessThan(int i) {
        return Collector.collect(new Evaluator.IndexLessThan(i), this);
    }

    public Elements getElementsByTag(String str) {
        Validate.notEmpty(str);
        return Collector.collect(new Evaluator.Tag(str.toLowerCase().trim()), this);
    }

    public Elements getElementsContainingOwnText(String str) {
        return Collector.collect(new Evaluator.ContainsOwnText(str), this);
    }

    public Elements getElementsContainingText(String str) {
        return Collector.collect(new Evaluator.ContainsText(str), this);
    }

    public Elements getElementsMatchingOwnText(String str) {
        try {
            return getElementsMatchingOwnText(Pattern.compile(str));
        } catch (PatternSyntaxException e) {
            throw new IllegalArgumentException("Pattern syntax error: " + str, e);
        }
    }

    public Elements getElementsMatchingOwnText(Pattern pattern) {
        return Collector.collect(new Evaluator.MatchesOwn(pattern), this);
    }

    public Elements getElementsMatchingText(String str) {
        try {
            return getElementsMatchingText(Pattern.compile(str));
        } catch (PatternSyntaxException e) {
            throw new IllegalArgumentException("Pattern syntax error: " + str, e);
        }
    }

    public Elements getElementsMatchingText(Pattern pattern) {
        return Collector.collect(new Evaluator.Matches(pattern), this);
    }

    public boolean hasClass(String str) {
        for (String equalsIgnoreCase : classNames()) {
            if (str.equalsIgnoreCase(equalsIgnoreCase)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasText() {
        for (Node node : this.b) {
            if (node instanceof TextNode) {
                if (!((TextNode) node).isBlank()) {
                    return true;
                }
            } else if ((node instanceof Element) && ((Element) node).hasText()) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return (this.f != null ? this.f.hashCode() : 0) + (super.hashCode() * 31);
    }

    public String html() {
        StringBuilder sb = new StringBuilder();
        c(sb);
        return a().prettyPrint() ? sb.toString().trim() : sb.toString();
    }

    public Element html(String str) {
        empty();
        append(str);
        return this;
    }

    public String id() {
        String attr = attr("id");
        return attr == null ? "" : attr;
    }

    public Element insertChildren(int i, Collection collection) {
        Validate.notNull(collection, "Children collection to be inserted must not be null.");
        int childNodeSize = childNodeSize();
        if (i < 0) {
            i += childNodeSize + 1;
        }
        Validate.isTrue(i >= 0 && i <= childNodeSize, "Insert position out of bounds.");
        ArrayList arrayList = new ArrayList(collection);
        a(i, (Node[]) arrayList.toArray(new Node[arrayList.size()]));
        return this;
    }

    public boolean isBlock() {
        return this.f.isBlock();
    }

    public Element lastElementSibling() {
        Elements children = parent().children();
        if (children.size() > 1) {
            return (Element) children.get(children.size() - 1);
        }
        return null;
    }

    public Element nextElementSibling() {
        if (this.a == null) {
            return null;
        }
        Elements children = parent().children();
        Integer a = a(this, children);
        Validate.notNull(a);
        if (children.size() > a.intValue() + 1) {
            return (Element) children.get(a.intValue() + 1);
        }
        return null;
    }

    public String nodeName() {
        return this.f.getName();
    }

    public String ownText() {
        StringBuilder sb = new StringBuilder();
        b(sb);
        return sb.toString().trim();
    }

    public final Element parent() {
        return (Element) this.a;
    }

    public Elements parents() {
        Elements elements = new Elements();
        while (true) {
            this = this.parent();
            if (this == null || this.tagName().equals("#root")) {
                return elements;
            }
            elements.add(this);
        }
        return elements;
    }

    public Element prepend(String str) {
        Validate.notNull(str);
        List parseFragment = Parser.parseFragment(str, this, baseUri());
        a(0, (Node[]) parseFragment.toArray(new Node[parseFragment.size()]));
        return this;
    }

    public Element prependChild(Node node) {
        Validate.notNull(node);
        a(0, node);
        return this;
    }

    public Element prependElement(String str) {
        Element element = new Element(Tag.valueOf(str), baseUri());
        prependChild(element);
        return element;
    }

    public Element prependText(String str) {
        prependChild(new TextNode(str, baseUri()));
        return this;
    }

    public Element previousElementSibling() {
        if (this.a == null) {
            return null;
        }
        Elements children = parent().children();
        Integer a = a(this, children);
        Validate.notNull(a);
        if (a.intValue() > 0) {
            return (Element) children.get(a.intValue() - 1);
        }
        return null;
    }

    public Element removeClass(String str) {
        Validate.notNull(str);
        Set classNames = classNames();
        classNames.remove(str);
        classNames(classNames);
        return this;
    }

    public Elements select(String str) {
        return Selector.select(str, this);
    }

    public Elements siblingElements() {
        if (this.a == null) {
            return new Elements(0);
        }
        Elements<Element> children = parent().children();
        Elements elements = new Elements(children.size() - 1);
        for (Element element : children) {
            if (element != this) {
                elements.add(element);
            }
        }
        return elements;
    }

    public Tag tag() {
        return this.f;
    }

    public String tagName() {
        return this.f.getName();
    }

    public Element tagName(String str) {
        Validate.notEmpty(str, "Tag name must not be empty.");
        this.f = Tag.valueOf(str);
        return this;
    }

    public String text() {
        StringBuilder sb = new StringBuilder();
        new NodeTraversor(new d(this, sb)).traverse(this);
        return sb.toString().trim();
    }

    public Element text(String str) {
        Validate.notNull(str);
        empty();
        appendChild(new TextNode(str, this.d));
        return this;
    }

    public List textNodes() {
        ArrayList arrayList = new ArrayList();
        for (Node node : this.b) {
            if (node instanceof TextNode) {
                arrayList.add((TextNode) node);
            }
        }
        return Collections.unmodifiableList(arrayList);
    }

    public String toString() {
        return outerHtml();
    }

    public Element toggleClass(String str) {
        Validate.notNull(str);
        Set classNames = classNames();
        if (classNames.contains(str)) {
            classNames.remove(str);
        } else {
            classNames.add(str);
        }
        classNames(classNames);
        return this;
    }

    public String val() {
        return tagName().equals("textarea") ? text() : attr("value");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
      org.jsoup.nodes.Node.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element */
    public Element val(String str) {
        if (tagName().equals("textarea")) {
            text(str);
        } else {
            attr("value", str);
        }
        return this;
    }

    public Element wrap(String str) {
        return (Element) super.wrap(str);
    }
}
