package org.jsoup.nodes;

import java.util.AbstractSet;
import java.util.Iterator;

final class c extends AbstractSet {
    final /* synthetic */ a a;

    private c(a aVar) {
        this.a = aVar;
    }

    /* synthetic */ c(a aVar, byte b) {
        this(aVar);
    }

    public final Iterator iterator() {
        return new b(this.a, (byte) 0);
    }

    public final int size() {
        int i = 0;
        while (new b(this.a, (byte) 0).hasNext()) {
            i++;
        }
        return i;
    }
}
