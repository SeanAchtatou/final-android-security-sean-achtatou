package org.jsoup.nodes;

import org.jsoup.nodes.Document;

public class XmlDeclaration extends Node {
    private final boolean f;

    public XmlDeclaration(String str, String str2, boolean z) {
        super(str2);
        this.c.put("declaration", str);
        this.f = z;
    }

    /* access modifiers changed from: package-private */
    public final void a(StringBuilder sb, int i, Document.OutputSettings outputSettings) {
        sb.append("<").append(this.f ? "!" : "?").append(getWholeDeclaration()).append(">");
    }

    /* access modifiers changed from: package-private */
    public final void b(StringBuilder sb, int i, Document.OutputSettings outputSettings) {
    }

    public String getWholeDeclaration() {
        return this.c.get("declaration");
    }

    public String nodeName() {
        return "#declaration";
    }

    public String toString() {
        return outerHtml();
    }
}
