package org.jsoup.safety;

import org.jsoup.helper.Validate;

abstract class g {
    private String a;

    g(String str) {
        Validate.notNull(str);
        this.a = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        return this.a == null ? gVar.a == null : this.a.equals(gVar.a);
    }

    public int hashCode() {
        return (this.a == null ? 0 : this.a.hashCode()) + 31;
    }

    public String toString() {
        return this.a;
    }
}
