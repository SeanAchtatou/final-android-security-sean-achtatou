package org.jsoup.safety;

import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeVisitor;

final class a implements NodeVisitor {
    final /* synthetic */ Cleaner a;
    /* access modifiers changed from: private */
    public int b;
    private final Element c;
    private Element d;

    private a(Cleaner cleaner, Element element, Element element2) {
        this.a = cleaner;
        this.b = 0;
        this.c = element;
        this.d = element2;
    }

    /* synthetic */ a(Cleaner cleaner, Element element, Element element2, byte b2) {
        this(cleaner, element, element2);
    }

    public final void head(Node node, int i) {
        if (node instanceof Element) {
            Element element = (Element) node;
            if (this.a.a.a(element.tagName())) {
                b a2 = Cleaner.a(this.a, element);
                Element element2 = a2.a;
                this.d.appendChild(element2);
                this.b = a2.b + this.b;
                this.d = element2;
            } else if (node != this.c) {
                this.b++;
            }
        } else if (node instanceof TextNode) {
            this.d.appendChild(new TextNode(((TextNode) node).getWholeText(), node.baseUri()));
        } else if (!(node instanceof DataNode) || !this.a.a.a(node.parent().nodeName())) {
            this.b++;
        } else {
            this.d.appendChild(new DataNode(((DataNode) node).getWholeData(), node.baseUri()));
        }
    }

    public final void tail(Node node, int i) {
        if ((node instanceof Element) && this.a.a.a(node.nodeName())) {
            this.d = this.d.parent();
        }
    }
}
