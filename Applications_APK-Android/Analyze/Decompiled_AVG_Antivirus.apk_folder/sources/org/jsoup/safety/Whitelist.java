package org.jsoup.safety;

import com.jobstrak.drawingfun.lib.mopub.common.Constants;
import com.jobstrak.drawingfun.sdk.manager.url.UrlManager;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;

public class Whitelist {
    private Set a = new HashSet();
    private Map b = new HashMap();
    private Map c = new HashMap();
    private Map d = new HashMap();
    private boolean e = false;

    public static Whitelist basic() {
        return new Whitelist().addTags("a", "b", "blockquote", "br", "cite", "code", "dd", "dl", "dt", "em", "i", "li", "ol", "p", "pre", "q", "small", "span", "strike", "strong", "sub", "sup", "u", "ul").addAttributes("a", "href").addAttributes("blockquote", "cite").addAttributes("q", "cite").addProtocols("a", "href", "ftp", Constants.HTTP, "https", "mailto").addProtocols("blockquote", "cite", Constants.HTTP, "https").addProtocols("cite", "cite", Constants.HTTP, "https").addEnforcedAttribute("a", "rel", "nofollow");
    }

    public static Whitelist basicWithImages() {
        return basic().addTags("img").addAttributes("img", "align", "alt", UrlManager.Parameter.HEIGHT, "src", "title", UrlManager.Parameter.WIDTH).addProtocols("img", "src", Constants.HTTP, "https");
    }

    public static Whitelist none() {
        return new Whitelist();
    }

    public static Whitelist relaxed() {
        return new Whitelist().addTags("a", "b", "blockquote", "br", "caption", "cite", "code", "col", "colgroup", "dd", "div", "dl", "dt", "em", "h1", "h2", "h3", "h4", "h5", "h6", "i", "img", "li", "ol", "p", "pre", "q", "small", "span", "strike", "strong", "sub", "sup", "table", "tbody", "td", "tfoot", "th", "thead", "tr", "u", "ul").addAttributes("a", "href", "title").addAttributes("blockquote", "cite").addAttributes("col", "span", UrlManager.Parameter.WIDTH).addAttributes("colgroup", "span", UrlManager.Parameter.WIDTH).addAttributes("img", "align", "alt", UrlManager.Parameter.HEIGHT, "src", "title", UrlManager.Parameter.WIDTH).addAttributes("ol", "start", "type").addAttributes("q", "cite").addAttributes("table", "summary", UrlManager.Parameter.WIDTH).addAttributes("td", "abbr", "axis", "colspan", "rowspan", UrlManager.Parameter.WIDTH).addAttributes("th", "abbr", "axis", "colspan", "rowspan", "scope", UrlManager.Parameter.WIDTH).addAttributes("ul", "type").addProtocols("a", "href", "ftp", Constants.HTTP, "https", "mailto").addProtocols("blockquote", "cite", Constants.HTTP, "https").addProtocols("cite", "cite", Constants.HTTP, "https").addProtocols("img", "src", Constants.HTTP, "https").addProtocols("q", "cite", Constants.HTTP, "https");
    }

    public static Whitelist simpleText() {
        return new Whitelist().addTags("b", "em", "i", "strong", "u");
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str) {
        return this.a.contains(f.a(str));
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str, Element element, Attribute attribute) {
        boolean z;
        f a2 = f.a(str);
        c a3 = c.a(attribute.getKey());
        if (!this.b.containsKey(a2) || !((Set) this.b.get(a2)).contains(a3)) {
            return !str.equals(":all") && a(":all", element, attribute);
        }
        if (!this.d.containsKey(a2)) {
            return true;
        }
        Map map = (Map) this.d.get(a2);
        if (map.containsKey(a3)) {
            Set set = (Set) map.get(a3);
            String absUrl = element.absUrl(attribute.getKey());
            if (absUrl.length() == 0) {
                absUrl = attribute.getValue();
            }
            if (!this.e) {
                attribute.setValue(absUrl);
            }
            Iterator it = set.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                if (absUrl.toLowerCase().startsWith(((e) it.next()).toString() + ":")) {
                    z = true;
                    break;
                }
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }

    public Whitelist addAttributes(String str, String... strArr) {
        Validate.notEmpty(str);
        Validate.notNull(strArr);
        Validate.isTrue(strArr.length > 0, "No attributes supplied.");
        f a2 = f.a(str);
        if (!this.a.contains(a2)) {
            this.a.add(a2);
        }
        HashSet hashSet = new HashSet();
        for (String str2 : strArr) {
            Validate.notEmpty(str2);
            hashSet.add(c.a(str2));
        }
        if (this.b.containsKey(a2)) {
            ((Set) this.b.get(a2)).addAll(hashSet);
        } else {
            this.b.put(a2, hashSet);
        }
        return this;
    }

    public Whitelist addEnforcedAttribute(String str, String str2, String str3) {
        Validate.notEmpty(str);
        Validate.notEmpty(str2);
        Validate.notEmpty(str3);
        f a2 = f.a(str);
        if (!this.a.contains(a2)) {
            this.a.add(a2);
        }
        c a3 = c.a(str2);
        d dVar = new d(str3);
        if (this.c.containsKey(a2)) {
            ((Map) this.c.get(a2)).put(a3, dVar);
        } else {
            HashMap hashMap = new HashMap();
            hashMap.put(a3, dVar);
            this.c.put(a2, hashMap);
        }
        return this;
    }

    public Whitelist addProtocols(String str, String str2, String... strArr) {
        Map hashMap;
        Set set;
        Validate.notEmpty(str);
        Validate.notEmpty(str2);
        Validate.notNull(strArr);
        f a2 = f.a(str);
        c a3 = c.a(str2);
        if (this.d.containsKey(a2)) {
            hashMap = (Map) this.d.get(a2);
        } else {
            hashMap = new HashMap();
            this.d.put(a2, hashMap);
        }
        if (hashMap.containsKey(a3)) {
            set = (Set) hashMap.get(a3);
        } else {
            HashSet hashSet = new HashSet();
            hashMap.put(a3, hashSet);
            set = hashSet;
        }
        for (String str3 : strArr) {
            Validate.notEmpty(str3);
            set.add(new e(str3));
        }
        return this;
    }

    public Whitelist addTags(String... strArr) {
        Validate.notNull(strArr);
        for (String str : strArr) {
            Validate.notEmpty(str);
            this.a.add(f.a(str));
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public final Attributes b(String str) {
        Attributes attributes = new Attributes();
        f a2 = f.a(str);
        if (this.c.containsKey(a2)) {
            for (Map.Entry entry : ((Map) this.c.get(a2)).entrySet()) {
                attributes.put(((c) entry.getKey()).toString(), ((d) entry.getValue()).toString());
            }
        }
        return attributes;
    }

    public Whitelist preserveRelativeLinks(boolean z) {
        this.e = z;
        return this;
    }
}
