package org.jsoup.safety;

import java.util.Iterator;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.NodeTraversor;

public class Cleaner {
    /* access modifiers changed from: private */
    public Whitelist a;

    public Cleaner(Whitelist whitelist) {
        Validate.notNull(whitelist);
        this.a = whitelist;
    }

    private int a(Element element, Element element2) {
        a aVar = new a(this, element, element2, (byte) 0);
        new NodeTraversor(aVar).traverse(element);
        return aVar.b;
    }

    static /* synthetic */ b a(Cleaner cleaner, Element element) {
        String tagName = element.tagName();
        Attributes attributes = new Attributes();
        Element element2 = new Element(Tag.valueOf(tagName), element.baseUri(), attributes);
        Iterator it = element.attributes().iterator();
        int i = 0;
        while (it.hasNext()) {
            Attribute attribute = (Attribute) it.next();
            if (cleaner.a.a(tagName, element, attribute)) {
                attributes.put(attribute);
            } else {
                i++;
            }
        }
        attributes.addAll(cleaner.a.b(tagName));
        return new b(element2, i);
    }

    public Document clean(Document document) {
        Validate.notNull(document);
        Document createShell = Document.createShell(document.baseUri());
        if (document.body() != null) {
            a(document.body(), createShell.body());
        }
        return createShell;
    }

    public boolean isValid(Document document) {
        Validate.notNull(document);
        return a(document.body(), Document.createShell(document.baseUri()).body()) == 0;
    }
}
