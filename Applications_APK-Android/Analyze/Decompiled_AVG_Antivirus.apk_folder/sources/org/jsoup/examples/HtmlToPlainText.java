package org.jsoup.examples;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;
import org.jsoup.select.NodeTraversor;

public class HtmlToPlainText {
    public static void main(String... strArr) {
        boolean z = true;
        if (strArr.length != 1) {
            z = false;
        }
        Validate.isTrue(z, "usage: supply url to fetch");
        System.out.println(new HtmlToPlainText().getPlainText(Jsoup.connect(strArr[0]).get()));
    }

    public String getPlainText(Element element) {
        a aVar = new a(this, (byte) 0);
        new NodeTraversor(aVar).traverse(element);
        return aVar.toString();
    }
}
