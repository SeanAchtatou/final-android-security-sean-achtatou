package org.jsoup.examples;

import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeVisitor;

final class a implements NodeVisitor {
    final /* synthetic */ HtmlToPlainText a;
    private int b;
    private StringBuilder c;

    private a(HtmlToPlainText htmlToPlainText) {
        this.a = htmlToPlainText;
        this.b = 0;
        this.c = new StringBuilder();
    }

    /* synthetic */ a(HtmlToPlainText htmlToPlainText, byte b2) {
        this(htmlToPlainText);
    }

    private void a(String str) {
        if (str.startsWith("\n")) {
            this.b = 0;
        }
        if (str.equals(" ")) {
            if (this.c.length() != 0) {
                if (StringUtil.in(this.c.substring(this.c.length() - 1), " ", "\n")) {
                    return;
                }
            } else {
                return;
            }
        }
        if (str.length() + this.b > 80) {
            String[] split = str.split("\\s+");
            int i = 0;
            while (i < split.length) {
                String str2 = split[i];
                if (!(i == split.length + -1)) {
                    str2 = str2 + " ";
                }
                if (str2.length() + this.b > 80) {
                    this.c.append("\n").append(str2);
                    this.b = str2.length();
                } else {
                    this.c.append(str2);
                    this.b = str2.length() + this.b;
                }
                i++;
            }
            return;
        }
        this.c.append(str);
        this.b += str.length();
    }

    public final void head(Node node, int i) {
        String nodeName = node.nodeName();
        if (node instanceof TextNode) {
            a(((TextNode) node).text());
        } else if (nodeName.equals("li")) {
            a("\n * ");
        }
    }

    public final void tail(Node node, int i) {
        String nodeName = node.nodeName();
        if (nodeName.equals("br")) {
            a("\n");
            return;
        }
        if (StringUtil.in(nodeName, "p", "h1", "h2", "h3", "h4", "h5")) {
            a("\n\n");
        } else if (nodeName.equals("a")) {
            a(String.format(" <%s>", node.absUrl("href")));
        }
    }

    public final String toString() {
        return this.c.toString();
    }
}
