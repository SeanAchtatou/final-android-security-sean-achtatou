package org.jsoup.helper;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import kotlin.text.Typography;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.parser.TokenQueue;

public class HttpConnection implements Connection {
    private Connection.Request a = new Request((byte) 0);
    private Connection.Response b = new Response();

    public class KeyVal implements Connection.KeyVal {
        private String a;
        private String b;

        private KeyVal(String str, String str2) {
            this.a = str;
            this.b = str2;
        }

        public static KeyVal create(String str, String str2) {
            Validate.notEmpty(str, "Data key must not be empty");
            Validate.notNull(str2, "Data value must not be null");
            return new KeyVal(str, str2);
        }

        public String key() {
            return this.a;
        }

        public KeyVal key(String str) {
            Validate.notEmpty(str, "Data key must not be empty");
            this.a = str;
            return this;
        }

        public String toString() {
            return this.a + "=" + this.b;
        }

        public String value() {
            return this.b;
        }

        public KeyVal value(String str) {
            Validate.notNull(str, "Data value must not be null");
            this.b = str;
            return this;
        }
    }

    public class Request extends b implements Connection.Request {
        private int e;
        private int f;
        private boolean g;
        private Collection h;
        private boolean i;
        private boolean j;
        private Parser k;

        private Request() {
            super((byte) 0);
            this.i = false;
            this.j = false;
            this.e = 3000;
            this.f = 1048576;
            this.g = true;
            this.h = new ArrayList();
            this.b = Connection.Method.GET;
            this.c.put("Accept-Encoding", "gzip");
            this.k = Parser.htmlParser();
        }

        /* synthetic */ Request(byte b) {
            this();
        }

        public /* bridge */ /* synthetic */ String cookie(String str) {
            return super.cookie(str);
        }

        public /* bridge */ /* synthetic */ Map cookies() {
            return super.cookies();
        }

        public Collection data() {
            return this.h;
        }

        public Request data(Connection.KeyVal keyVal) {
            Validate.notNull(keyVal, "Key val must not be null");
            this.h.add(keyVal);
            return this;
        }

        public Connection.Request followRedirects(boolean z) {
            this.g = z;
            return this;
        }

        public boolean followRedirects() {
            return this.g;
        }

        public /* bridge */ /* synthetic */ boolean hasCookie(String str) {
            return super.hasCookie(str);
        }

        public /* bridge */ /* synthetic */ boolean hasHeader(String str) {
            return super.hasHeader(str);
        }

        public /* bridge */ /* synthetic */ String header(String str) {
            return super.header(str);
        }

        public /* bridge */ /* synthetic */ Map headers() {
            return super.headers();
        }

        public Connection.Request ignoreContentType(boolean z) {
            this.j = z;
            return this;
        }

        public boolean ignoreContentType() {
            return this.j;
        }

        public Connection.Request ignoreHttpErrors(boolean z) {
            this.i = z;
            return this;
        }

        public boolean ignoreHttpErrors() {
            return this.i;
        }

        public int maxBodySize() {
            return this.f;
        }

        public Connection.Request maxBodySize(int i2) {
            Validate.isTrue(i2 >= 0, "maxSize must be 0 (unlimited) or larger");
            this.f = i2;
            return this;
        }

        public /* bridge */ /* synthetic */ Connection.Method method() {
            return super.method();
        }

        public Request parser(Parser parser) {
            this.k = parser;
            return this;
        }

        public Parser parser() {
            return this.k;
        }

        public int timeout() {
            return this.e;
        }

        public Request timeout(int i2) {
            Validate.isTrue(i2 >= 0, "Timeout milliseconds must be 0 (infinite) or greater");
            this.e = i2;
            return this;
        }

        public /* bridge */ /* synthetic */ URL url() {
            return super.url();
        }
    }

    public class Response extends b implements Connection.Response {
        private static final Pattern m = Pattern.compile("application/\\w+\\+xml.*");
        private int e;
        private String f;
        private ByteBuffer g;
        private String h;
        private String i;
        private boolean j = false;
        private int k = 0;
        private Connection.Request l;

        Response() {
            super((byte) 0);
        }

        private Response(Response response) {
            super((byte) 0);
            if (response != null) {
                this.k = response.k + 1;
                if (this.k >= 20) {
                    throw new IOException(String.format("Too many redirects occurred trying to load URL %s", response.url()));
                }
            }
        }

        static Response a(Connection.Request request) {
            return a(request, (Response) null);
        }

        /* JADX WARNING: Removed duplicated region for block: B:92:0x019c A[SYNTHETIC, Splitter:B:92:0x019c] */
        /* JADX WARNING: Removed duplicated region for block: B:95:0x01a1 A[Catch:{ all -> 0x00e2 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static org.jsoup.helper.HttpConnection.Response a(org.jsoup.Connection.Request r8, org.jsoup.helper.HttpConnection.Response r9) {
            /*
                r2 = 0
                r5 = 1
                java.lang.String r1 = "Request must not be null"
                org.jsoup.helper.Validate.notNull(r8, r1)
                java.net.URL r1 = r8.url()
                java.lang.String r1 = r1.getProtocol()
                java.lang.String r3 = "http"
                boolean r3 = r1.equals(r3)
                if (r3 != 0) goto L_0x0027
                java.lang.String r3 = "https"
                boolean r1 = r1.equals(r3)
                if (r1 != 0) goto L_0x0027
                java.net.MalformedURLException r1 = new java.net.MalformedURLException
                java.lang.String r2 = "Only http & https protocols supported"
                r1.<init>(r2)
                throw r1
            L_0x0027:
                org.jsoup.Connection$Method r1 = r8.method()
                org.jsoup.Connection$Method r3 = org.jsoup.Connection.Method.GET
                if (r1 != r3) goto L_0x003c
                java.util.Collection r1 = r8.data()
                int r1 = r1.size()
                if (r1 <= 0) goto L_0x003c
                c(r8)
            L_0x003c:
                java.net.HttpURLConnection r6 = b(r8)
                r6.connect()     // Catch:{ all -> 0x00e2 }
                org.jsoup.Connection$Method r1 = r8.method()     // Catch:{ all -> 0x00e2 }
                org.jsoup.Connection$Method r3 = org.jsoup.Connection.Method.POST     // Catch:{ all -> 0x00e2 }
                if (r1 != r3) goto L_0x0056
                java.util.Collection r1 = r8.data()     // Catch:{ all -> 0x00e2 }
                java.io.OutputStream r3 = r6.getOutputStream()     // Catch:{ all -> 0x00e2 }
                a(r1, r3)     // Catch:{ all -> 0x00e2 }
            L_0x0056:
                int r3 = r6.getResponseCode()     // Catch:{ all -> 0x00e2 }
                r1 = 0
                r4 = 200(0xc8, float:2.8E-43)
                if (r3 == r4) goto L_0x0070
                r4 = 302(0x12e, float:4.23E-43)
                if (r3 == r4) goto L_0x006f
                r4 = 301(0x12d, float:4.22E-43)
                if (r3 == r4) goto L_0x006f
                r4 = 303(0x12f, float:4.25E-43)
                if (r3 == r4) goto L_0x006f
                r4 = 307(0x133, float:4.3E-43)
                if (r3 != r4) goto L_0x00e7
            L_0x006f:
                r1 = r5
            L_0x0070:
                org.jsoup.helper.HttpConnection$Response r3 = new org.jsoup.helper.HttpConnection$Response     // Catch:{ all -> 0x00e2 }
                r3.<init>(r9)     // Catch:{ all -> 0x00e2 }
                r3.a(r6, r9)     // Catch:{ all -> 0x00e2 }
                if (r1 == 0) goto L_0x0105
                boolean r1 = r8.followRedirects()     // Catch:{ all -> 0x00e2 }
                if (r1 == 0) goto L_0x0105
                org.jsoup.Connection$Method r1 = org.jsoup.Connection.Method.GET     // Catch:{ all -> 0x00e2 }
                r8.method(r1)     // Catch:{ all -> 0x00e2 }
                java.util.Collection r1 = r8.data()     // Catch:{ all -> 0x00e2 }
                r1.clear()     // Catch:{ all -> 0x00e2 }
                java.lang.String r1 = "Location"
                java.lang.String r1 = r3.header(r1)     // Catch:{ all -> 0x00e2 }
                if (r1 == 0) goto L_0x00aa
                java.lang.String r2 = "http:/"
                boolean r2 = r1.startsWith(r2)     // Catch:{ all -> 0x00e2 }
                if (r2 == 0) goto L_0x00aa
                r2 = 6
                char r2 = r1.charAt(r2)     // Catch:{ all -> 0x00e2 }
                r4 = 47
                if (r2 == r4) goto L_0x00aa
                r2 = 6
                java.lang.String r1 = r1.substring(r2)     // Catch:{ all -> 0x00e2 }
            L_0x00aa:
                java.net.URL r2 = new java.net.URL     // Catch:{ all -> 0x00e2 }
                java.net.URL r4 = r8.url()     // Catch:{ all -> 0x00e2 }
                java.lang.String r1 = org.jsoup.helper.HttpConnection.b(r1)     // Catch:{ all -> 0x00e2 }
                r2.<init>(r4, r1)     // Catch:{ all -> 0x00e2 }
                r8.url(r2)     // Catch:{ all -> 0x00e2 }
                java.util.Map r1 = r3.d     // Catch:{ all -> 0x00e2 }
                java.util.Set r1 = r1.entrySet()     // Catch:{ all -> 0x00e2 }
                java.util.Iterator r4 = r1.iterator()     // Catch:{ all -> 0x00e2 }
            L_0x00c4:
                boolean r1 = r4.hasNext()     // Catch:{ all -> 0x00e2 }
                if (r1 == 0) goto L_0x00fd
                java.lang.Object r1 = r4.next()     // Catch:{ all -> 0x00e2 }
                r0 = r1
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x00e2 }
                r2 = r0
                java.lang.Object r1 = r2.getKey()     // Catch:{ all -> 0x00e2 }
                java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x00e2 }
                java.lang.Object r2 = r2.getValue()     // Catch:{ all -> 0x00e2 }
                java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x00e2 }
                r8.cookie(r1, r2)     // Catch:{ all -> 0x00e2 }
                goto L_0x00c4
            L_0x00e2:
                r1 = move-exception
                r6.disconnect()
                throw r1
            L_0x00e7:
                boolean r4 = r8.ignoreHttpErrors()     // Catch:{ all -> 0x00e2 }
                if (r4 != 0) goto L_0x0070
                org.jsoup.HttpStatusException r1 = new org.jsoup.HttpStatusException     // Catch:{ all -> 0x00e2 }
                java.lang.String r2 = "HTTP error fetching URL"
                java.net.URL r4 = r8.url()     // Catch:{ all -> 0x00e2 }
                java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00e2 }
                r1.<init>(r2, r3, r4)     // Catch:{ all -> 0x00e2 }
                throw r1     // Catch:{ all -> 0x00e2 }
            L_0x00fd:
                org.jsoup.helper.HttpConnection$Response r1 = a(r8, r3)     // Catch:{ all -> 0x00e2 }
                r6.disconnect()
            L_0x0104:
                return r1
            L_0x0105:
                r3.l = r8     // Catch:{ all -> 0x00e2 }
                java.lang.String r1 = r3.contentType()     // Catch:{ all -> 0x00e2 }
                if (r1 == 0) goto L_0x013f
                boolean r4 = r8.ignoreContentType()     // Catch:{ all -> 0x00e2 }
                if (r4 != 0) goto L_0x013f
                java.lang.String r4 = "text/"
                boolean r4 = r1.startsWith(r4)     // Catch:{ all -> 0x00e2 }
                if (r4 != 0) goto L_0x013f
                java.lang.String r4 = "application/xml"
                boolean r4 = r1.startsWith(r4)     // Catch:{ all -> 0x00e2 }
                if (r4 != 0) goto L_0x013f
                java.util.regex.Pattern r4 = org.jsoup.helper.HttpConnection.Response.m     // Catch:{ all -> 0x00e2 }
                java.util.regex.Matcher r4 = r4.matcher(r1)     // Catch:{ all -> 0x00e2 }
                boolean r4 = r4.matches()     // Catch:{ all -> 0x00e2 }
                if (r4 != 0) goto L_0x013f
                org.jsoup.UnsupportedMimeTypeException r2 = new org.jsoup.UnsupportedMimeTypeException     // Catch:{ all -> 0x00e2 }
                java.lang.String r3 = "Unhandled content type. Must be text/*, application/xml, or application/xhtml+xml"
                java.net.URL r4 = r8.url()     // Catch:{ all -> 0x00e2 }
                java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00e2 }
                r2.<init>(r3, r1, r4)     // Catch:{ all -> 0x00e2 }
                throw r2     // Catch:{ all -> 0x00e2 }
            L_0x013f:
                java.io.InputStream r1 = r6.getErrorStream()     // Catch:{ all -> 0x0198 }
                if (r1 == 0) goto L_0x018c
                java.io.InputStream r4 = r6.getErrorStream()     // Catch:{ all -> 0x0198 }
            L_0x0149:
                java.lang.String r1 = "Content-Encoding"
                boolean r1 = r3.hasHeader(r1)     // Catch:{ all -> 0x01a5 }
                if (r1 == 0) goto L_0x0191
                java.lang.String r1 = "Content-Encoding"
                java.lang.String r1 = r3.header(r1)     // Catch:{ all -> 0x01a5 }
                java.lang.String r7 = "gzip"
                boolean r1 = r1.equalsIgnoreCase(r7)     // Catch:{ all -> 0x01a5 }
                if (r1 == 0) goto L_0x0191
                java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x01a5 }
                java.util.zip.GZIPInputStream r7 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x01a5 }
                r7.<init>(r4)     // Catch:{ all -> 0x01a5 }
                r1.<init>(r7)     // Catch:{ all -> 0x01a5 }
                r2 = r1
            L_0x016a:
                int r1 = r8.maxBodySize()     // Catch:{ all -> 0x01a9 }
                java.nio.ByteBuffer r1 = org.jsoup.helper.DataUtil.a(r2, r1)     // Catch:{ all -> 0x01a9 }
                r3.g = r1     // Catch:{ all -> 0x01a9 }
                java.lang.String r1 = r3.i     // Catch:{ all -> 0x01a9 }
                java.lang.String r1 = org.jsoup.helper.DataUtil.a(r1)     // Catch:{ all -> 0x01a9 }
                r3.h = r1     // Catch:{ all -> 0x01a9 }
                r2.close()     // Catch:{ all -> 0x00e2 }
                if (r4 == 0) goto L_0x0184
                r4.close()     // Catch:{ all -> 0x00e2 }
            L_0x0184:
                r6.disconnect()
                r3.j = r5
                r1 = r3
                goto L_0x0104
            L_0x018c:
                java.io.InputStream r4 = r6.getInputStream()     // Catch:{ all -> 0x0198 }
                goto L_0x0149
            L_0x0191:
                java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x01a5 }
                r1.<init>(r4)     // Catch:{ all -> 0x01a5 }
                r2 = r1
                goto L_0x016a
            L_0x0198:
                r1 = move-exception
                r3 = r2
            L_0x019a:
                if (r3 == 0) goto L_0x019f
                r3.close()     // Catch:{ all -> 0x00e2 }
            L_0x019f:
                if (r2 == 0) goto L_0x01a4
                r2.close()     // Catch:{ all -> 0x00e2 }
            L_0x01a4:
                throw r1     // Catch:{ all -> 0x00e2 }
            L_0x01a5:
                r1 = move-exception
                r3 = r2
                r2 = r4
                goto L_0x019a
            L_0x01a9:
                r1 = move-exception
                r3 = r2
                r2 = r4
                goto L_0x019a
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.helper.HttpConnection.Response.a(org.jsoup.Connection$Request, org.jsoup.helper.HttpConnection$Response):org.jsoup.helper.HttpConnection$Response");
        }

        private void a(HttpURLConnection httpURLConnection, Connection.Response response) {
            this.b = Connection.Method.valueOf(httpURLConnection.getRequestMethod());
            this.a = httpURLConnection.getURL();
            this.e = httpURLConnection.getResponseCode();
            this.f = httpURLConnection.getResponseMessage();
            this.i = httpURLConnection.getContentType();
            for (Map.Entry next : httpURLConnection.getHeaderFields().entrySet()) {
                String str = (String) next.getKey();
                if (str != null) {
                    List<String> list = (List) next.getValue();
                    if (str.equalsIgnoreCase("Set-Cookie")) {
                        for (String str2 : list) {
                            if (str2 != null) {
                                TokenQueue tokenQueue = new TokenQueue(str2);
                                String trim = tokenQueue.chompTo("=").trim();
                                String trim2 = tokenQueue.consumeTo(";").trim();
                                if (trim2 == null) {
                                    trim2 = "";
                                }
                                if (trim != null && trim.length() > 0) {
                                    cookie(trim, trim2);
                                }
                            }
                        }
                    } else if (!list.isEmpty()) {
                        header(str, (String) list.get(0));
                    }
                }
            }
            if (response != null) {
                for (Map.Entry entry : response.cookies().entrySet()) {
                    if (!hasCookie((String) entry.getKey())) {
                        cookie((String) entry.getKey(), (String) entry.getValue());
                    }
                }
            }
        }

        private static void a(Collection collection, OutputStream outputStream) {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
            Iterator it = collection.iterator();
            boolean z = true;
            while (it.hasNext()) {
                Connection.KeyVal keyVal = (Connection.KeyVal) it.next();
                if (!z) {
                    outputStreamWriter.append((char) Typography.amp);
                } else {
                    z = false;
                }
                outputStreamWriter.write(URLEncoder.encode(keyVal.key(), "UTF-8"));
                outputStreamWriter.write(61);
                outputStreamWriter.write(URLEncoder.encode(keyVal.value(), "UTF-8"));
            }
            outputStreamWriter.close();
        }

        private static HttpURLConnection b(Connection.Request request) {
            boolean z;
            HttpURLConnection httpURLConnection = (HttpURLConnection) request.url().openConnection();
            httpURLConnection.setRequestMethod(request.method().name());
            httpURLConnection.setInstanceFollowRedirects(false);
            httpURLConnection.setConnectTimeout(request.timeout());
            httpURLConnection.setReadTimeout(request.timeout());
            if (request.method() == Connection.Method.POST) {
                httpURLConnection.setDoOutput(true);
            }
            if (request.cookies().size() > 0) {
                StringBuilder sb = new StringBuilder();
                boolean z2 = true;
                for (Map.Entry entry : request.cookies().entrySet()) {
                    if (!z2) {
                        sb.append("; ");
                        z = z2;
                    } else {
                        z = false;
                    }
                    sb.append((String) entry.getKey()).append('=').append((String) entry.getValue());
                    z2 = z;
                }
                httpURLConnection.addRequestProperty("Cookie", sb.toString());
            }
            for (Map.Entry entry2 : request.headers().entrySet()) {
                httpURLConnection.addRequestProperty((String) entry2.getKey(), (String) entry2.getValue());
            }
            return httpURLConnection;
        }

        private static void c(Connection.Request request) {
            URL url = request.url();
            StringBuilder sb = new StringBuilder();
            boolean z = true;
            sb.append(url.getProtocol()).append("://").append(url.getAuthority()).append(url.getPath()).append("?");
            if (url.getQuery() != null) {
                sb.append(url.getQuery());
                z = false;
            }
            boolean z2 = z;
            for (Connection.KeyVal keyVal : request.data()) {
                if (!z2) {
                    sb.append((char) Typography.amp);
                } else {
                    z2 = false;
                }
                sb.append(URLEncoder.encode(keyVal.key(), "UTF-8")).append('=').append(URLEncoder.encode(keyVal.value(), "UTF-8"));
            }
            request.url(new URL(sb.toString()));
            request.data().clear();
        }

        public String body() {
            Validate.isTrue(this.j, "Request must be executed (with .execute(), .get(), or .post() before getting response body");
            String charBuffer = this.h == null ? Charset.forName("UTF-8").decode(this.g).toString() : Charset.forName(this.h).decode(this.g).toString();
            this.g.rewind();
            return charBuffer;
        }

        public byte[] bodyAsBytes() {
            Validate.isTrue(this.j, "Request must be executed (with .execute(), .get(), or .post() before getting response body");
            return this.g.array();
        }

        public String charset() {
            return this.h;
        }

        public String contentType() {
            return this.i;
        }

        public /* bridge */ /* synthetic */ String cookie(String str) {
            return super.cookie(str);
        }

        public /* bridge */ /* synthetic */ Map cookies() {
            return super.cookies();
        }

        public /* bridge */ /* synthetic */ boolean hasCookie(String str) {
            return super.hasCookie(str);
        }

        public /* bridge */ /* synthetic */ boolean hasHeader(String str) {
            return super.hasHeader(str);
        }

        public /* bridge */ /* synthetic */ String header(String str) {
            return super.header(str);
        }

        public /* bridge */ /* synthetic */ Map headers() {
            return super.headers();
        }

        public /* bridge */ /* synthetic */ Connection.Method method() {
            return super.method();
        }

        public Document parse() {
            Validate.isTrue(this.j, "Request must be executed (with .execute(), .get(), or .post() before parsing response");
            Document a = DataUtil.a(this.g, this.h, this.a.toExternalForm(), this.l.parser());
            this.g.rewind();
            this.h = a.outputSettings().charset().name();
            return a;
        }

        public int statusCode() {
            return this.e;
        }

        public String statusMessage() {
            return this.f;
        }

        public /* bridge */ /* synthetic */ URL url() {
            return super.url();
        }
    }

    private HttpConnection() {
    }

    /* access modifiers changed from: private */
    public static String b(String str) {
        if (str == null) {
            return null;
        }
        return str.replaceAll(" ", "%20");
    }

    public static Connection connect(String str) {
        HttpConnection httpConnection = new HttpConnection();
        httpConnection.url(str);
        return httpConnection;
    }

    public static Connection connect(URL url) {
        HttpConnection httpConnection = new HttpConnection();
        httpConnection.url(url);
        return httpConnection;
    }

    public Connection cookie(String str, String str2) {
        this.a.cookie(str, str2);
        return this;
    }

    public Connection cookies(Map map) {
        Validate.notNull(map, "Cookie map must not be null");
        for (Map.Entry entry : map.entrySet()) {
            this.a.cookie((String) entry.getKey(), (String) entry.getValue());
        }
        return this;
    }

    public Connection data(String str, String str2) {
        this.a.data(KeyVal.create(str, str2));
        return this;
    }

    public Connection data(Collection collection) {
        Validate.notNull(collection, "Data collection must not be null");
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            this.a.data((Connection.KeyVal) it.next());
        }
        return this;
    }

    public Connection data(Map map) {
        Validate.notNull(map, "Data map must not be null");
        for (Map.Entry entry : map.entrySet()) {
            this.a.data(KeyVal.create((String) entry.getKey(), (String) entry.getValue()));
        }
        return this;
    }

    public Connection data(String... strArr) {
        Validate.notNull(strArr, "Data key value pairs must not be null");
        Validate.isTrue(strArr.length % 2 == 0, "Must supply an even number of key value pairs");
        for (int i = 0; i < strArr.length; i += 2) {
            String str = strArr[i];
            String str2 = strArr[i + 1];
            Validate.notEmpty(str, "Data key must not be empty");
            Validate.notNull(str2, "Data value must not be null");
            this.a.data(KeyVal.create(str, str2));
        }
        return this;
    }

    public Connection.Response execute() {
        this.b = Response.a(this.a);
        return this.b;
    }

    public Connection followRedirects(boolean z) {
        this.a.followRedirects(z);
        return this;
    }

    public Document get() {
        this.a.method(Connection.Method.GET);
        execute();
        return this.b.parse();
    }

    public Connection header(String str, String str2) {
        this.a.header(str, str2);
        return this;
    }

    public Connection ignoreContentType(boolean z) {
        this.a.ignoreContentType(z);
        return this;
    }

    public Connection ignoreHttpErrors(boolean z) {
        this.a.ignoreHttpErrors(z);
        return this;
    }

    public Connection maxBodySize(int i) {
        this.a.maxBodySize(i);
        return this;
    }

    public Connection method(Connection.Method method) {
        this.a.method(method);
        return this;
    }

    public Connection parser(Parser parser) {
        this.a.parser(parser);
        return this;
    }

    public Document post() {
        this.a.method(Connection.Method.POST);
        execute();
        return this.b.parse();
    }

    public Connection referrer(String str) {
        Validate.notNull(str, "Referrer must not be null");
        this.a.header("Referer", str);
        return this;
    }

    public Connection.Request request() {
        return this.a;
    }

    public Connection request(Connection.Request request) {
        this.a = request;
        return this;
    }

    public Connection.Response response() {
        return this.b;
    }

    public Connection response(Connection.Response response) {
        this.b = response;
        return this;
    }

    public Connection timeout(int i) {
        this.a.timeout(i);
        return this;
    }

    public Connection url(String str) {
        Validate.notEmpty(str, "Must supply a valid URL");
        try {
            this.a.url(new URL(b(str)));
            return this;
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Malformed URL: " + str, e);
        }
    }

    public Connection url(URL url) {
        this.a.url(url);
        return this;
    }

    public Connection userAgent(String str) {
        Validate.notNull(str, "User agent must not be null");
        this.a.header("User-Agent", str);
        return this;
    }
}
