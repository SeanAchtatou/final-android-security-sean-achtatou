package org.jsoup.helper;

import java.util.Iterator;
import java.util.LinkedList;

public class DescendableLinkedList extends LinkedList {
    public Iterator descendingIterator() {
        return new a(this, size(), (byte) 0);
    }

    public Object peekLast() {
        if (size() == 0) {
            return null;
        }
        return getLast();
    }

    public Object pollLast() {
        if (size() == 0) {
            return null;
        }
        return removeLast();
    }

    public void push(Object obj) {
        addFirst(obj);
    }
}
