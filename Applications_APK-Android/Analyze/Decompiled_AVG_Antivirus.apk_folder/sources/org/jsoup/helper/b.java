package org.jsoup.helper;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import org.jsoup.Connection;

abstract class b implements Connection.Base {
    URL a;
    Connection.Method b;
    Map c;
    Map d;

    private b() {
        this.c = new LinkedHashMap();
        this.d = new LinkedHashMap();
    }

    /* synthetic */ b(byte b2) {
        this();
    }

    private String a(String str) {
        Map.Entry b2;
        Validate.notNull(str, "Header name must not be null");
        String str2 = (String) this.c.get(str);
        if (str2 == null) {
            str2 = (String) this.c.get(str.toLowerCase());
        }
        return (str2 != null || (b2 = b(str)) == null) ? str2 : (String) b2.getValue();
    }

    private Map.Entry b(String str) {
        String lowerCase = str.toLowerCase();
        for (Map.Entry entry : this.c.entrySet()) {
            if (((String) entry.getKey()).toLowerCase().equals(lowerCase)) {
                return entry;
            }
        }
        return null;
    }

    public String cookie(String str) {
        Validate.notNull(str, "Cookie name must not be null");
        return (String) this.d.get(str);
    }

    public Connection.Base cookie(String str, String str2) {
        Validate.notEmpty(str, "Cookie name must not be empty");
        Validate.notNull(str2, "Cookie value must not be null");
        this.d.put(str, str2);
        return this;
    }

    public Map cookies() {
        return this.d;
    }

    public boolean hasCookie(String str) {
        Validate.notEmpty("Cookie name must not be empty");
        return this.d.containsKey(str);
    }

    public boolean hasHeader(String str) {
        Validate.notEmpty(str, "Header name must not be empty");
        return a(str) != null;
    }

    public String header(String str) {
        Validate.notNull(str, "Header name must not be null");
        return a(str);
    }

    public Connection.Base header(String str, String str2) {
        Validate.notEmpty(str, "Header name must not be empty");
        Validate.notNull(str2, "Header value must not be null");
        removeHeader(str);
        this.c.put(str, str2);
        return this;
    }

    public Map headers() {
        return this.c;
    }

    public Connection.Base method(Connection.Method method) {
        Validate.notNull(method, "Method must not be null");
        this.b = method;
        return this;
    }

    public Connection.Method method() {
        return this.b;
    }

    public Connection.Base removeCookie(String str) {
        Validate.notEmpty("Cookie name must not be empty");
        this.d.remove(str);
        return this;
    }

    public Connection.Base removeHeader(String str) {
        Validate.notEmpty(str, "Header name must not be empty");
        Map.Entry b2 = b(str);
        if (b2 != null) {
            this.c.remove(b2.getKey());
        }
        return this;
    }

    public URL url() {
        return this.a;
    }

    public Connection.Base url(URL url) {
        Validate.notNull(url, "URL must not be null");
        this.a = url;
        return this;
    }
}
