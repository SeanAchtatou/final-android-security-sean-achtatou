package org.jsoup.helper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

public class DataUtil {
    private static final Pattern a = Pattern.compile("(?i)\\bcharset=\\s*(?:\"|')?([^\\s,;\"']*)");

    private DataUtil() {
    }

    static String a(String str) {
        if (str == null) {
            return null;
        }
        Matcher matcher = a.matcher(str);
        if (!matcher.find()) {
            return null;
        }
        String replace = matcher.group(1).trim().replace("charset=", "");
        if (replace.length() == 0) {
            return null;
        }
        try {
            if (Charset.isSupported(replace)) {
                return replace;
            }
            String upperCase = replace.toUpperCase(Locale.ENGLISH);
            if (Charset.isSupported(upperCase)) {
                return upperCase;
            }
            return null;
        } catch (IllegalCharsetNameException e) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.nio.ByteBuffer a(java.io.File r4) {
        /*
            r2 = 0
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ all -> 0x001a }
            java.lang.String r0 = "r"
            r1.<init>(r4, r0)     // Catch:{ all -> 0x001a }
            long r2 = r1.length()     // Catch:{ all -> 0x0022 }
            int r0 = (int) r2     // Catch:{ all -> 0x0022 }
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0022 }
            r1.readFully(r0)     // Catch:{ all -> 0x0022 }
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.wrap(r0)     // Catch:{ all -> 0x0022 }
            r1.close()
            return r0
        L_0x001a:
            r0 = move-exception
            r1 = r2
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()
        L_0x0021:
            throw r0
        L_0x0022:
            r0 = move-exception
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.helper.DataUtil.a(java.io.File):java.nio.ByteBuffer");
    }

    static ByteBuffer a(InputStream inputStream, int i) {
        boolean z = true;
        Validate.isTrue(i >= 0, "maxSize must be 0 (unlimited) or larger");
        if (i <= 0) {
            z = false;
        }
        byte[] bArr = new byte[131072];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(131072);
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                break;
            }
            if (z) {
                if (read > i) {
                    byteArrayOutputStream.write(bArr, 0, i);
                    break;
                }
                i -= read;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
        return ByteBuffer.wrap(byteArrayOutputStream.toByteArray());
    }

    static Document a(ByteBuffer byteBuffer, String str, String str2, Parser parser) {
        String charBuffer;
        Document document;
        String attr;
        if (str == null) {
            String charBuffer2 = Charset.forName("UTF-8").decode(byteBuffer).toString();
            Document parseInput = parser.parseInput(charBuffer2, str2);
            Element first = parseInput.select("meta[http-equiv=content-type], meta[charset]").first();
            if (first != null) {
                if (first.hasAttr("http-equiv")) {
                    attr = a(first.attr("content"));
                    if (attr == null && first.hasAttr("charset")) {
                        try {
                            if (Charset.isSupported(first.attr("charset"))) {
                                attr = first.attr("charset");
                            }
                        } catch (IllegalCharsetNameException e) {
                            attr = null;
                        }
                    }
                } else {
                    attr = first.attr("charset");
                }
                if (!(attr == null || attr.length() == 0 || attr.equals("UTF-8"))) {
                    str = attr.trim().replaceAll("[\"']", "");
                    byteBuffer.rewind();
                    charBuffer = Charset.forName(str).decode(byteBuffer).toString();
                    document = null;
                }
            }
            document = parseInput;
            charBuffer = charBuffer2;
        } else {
            Validate.notEmpty(str, "Must set charset arg to character set of file to parse. Set to null to attempt to detect from HTML");
            charBuffer = Charset.forName(str).decode(byteBuffer).toString();
            document = null;
        }
        if (charBuffer.length() > 0 && charBuffer.charAt(0) == 65279) {
            byteBuffer.rewind();
            charBuffer = Charset.forName("UTF-8").decode(byteBuffer).toString().substring(1);
            str = "UTF-8";
            document = null;
        }
        if (document != null) {
            return document;
        }
        Document parseInput2 = parser.parseInput(charBuffer, str2);
        parseInput2.outputSettings().charset(str);
        return parseInput2;
    }

    public static Document load(File file, String str, String str2) {
        return a(a(file), str, str2, Parser.htmlParser());
    }

    public static Document load(InputStream inputStream, String str, String str2) {
        return a(a(inputStream, 0), str, str2, Parser.htmlParser());
    }

    public static Document load(InputStream inputStream, String str, String str2, Parser parser) {
        return a(a(inputStream, 0), str, str2, parser);
    }
}
