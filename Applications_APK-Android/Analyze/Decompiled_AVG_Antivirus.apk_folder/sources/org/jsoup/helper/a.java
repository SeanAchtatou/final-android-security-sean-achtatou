package org.jsoup.helper;

import java.util.Iterator;
import java.util.ListIterator;

final class a implements Iterator {
    final /* synthetic */ DescendableLinkedList a;
    private final ListIterator b;

    private a(DescendableLinkedList descendableLinkedList, int i) {
        this.a = descendableLinkedList;
        this.b = descendableLinkedList.listIterator(i);
    }

    /* synthetic */ a(DescendableLinkedList descendableLinkedList, int i, byte b2) {
        this(descendableLinkedList, i);
    }

    public final boolean hasNext() {
        return this.b.hasPrevious();
    }

    public final Object next() {
        return this.b.previous();
    }

    public final void remove() {
        this.b.remove();
    }
}
