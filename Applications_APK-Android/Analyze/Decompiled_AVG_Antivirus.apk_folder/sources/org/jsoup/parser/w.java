package org.jsoup.parser;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class w extends c {
    w(String str) {
        super(str, 4, (byte) 0);
    }

    private boolean b(ad adVar, b bVar) {
        bVar.b(this);
        bVar.a(new ai("noscript"));
        return bVar.a(adVar);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0083, code lost:
        if (org.jsoup.helper.StringUtil.in(((org.jsoup.parser.aj) r8).i(), "basefont", "bgsound", "link", "meta", "noframes", "style") != false) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00c2, code lost:
        if (org.jsoup.helper.StringUtil.in(((org.jsoup.parser.aj) r8).i(), "head", "noscript") == false) goto L_0x00c4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(org.jsoup.parser.ad r8, org.jsoup.parser.b r9) {
        /*
            r7 = this;
            r6 = 2
            r2 = 1
            r1 = 0
            boolean r0 = r8.a()
            if (r0 == 0) goto L_0x000e
            r9.b(r7)
        L_0x000c:
            r0 = r2
        L_0x000d:
            return r0
        L_0x000e:
            boolean r0 = r8.b()
            if (r0 == 0) goto L_0x002a
            r0 = r8
            org.jsoup.parser.aj r0 = (org.jsoup.parser.aj) r0
            java.lang.String r0 = r0.i()
            java.lang.String r3 = "html"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x002a
            org.jsoup.parser.c r0 = org.jsoup.parser.w.g
            boolean r0 = r9.a(r8, r0)
            goto L_0x000d
        L_0x002a:
            boolean r0 = r8.c()
            if (r0 == 0) goto L_0x0048
            r0 = r8
            org.jsoup.parser.ai r0 = (org.jsoup.parser.ai) r0
            java.lang.String r0 = r0.i()
            java.lang.String r3 = "noscript"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x0048
            r9.h()
            org.jsoup.parser.c r0 = org.jsoup.parser.w.d
            r9.a(r0)
            goto L_0x000c
        L_0x0048:
            boolean r0 = org.jsoup.parser.c.a(r8)
            if (r0 != 0) goto L_0x0085
            boolean r0 = r8.d()
            if (r0 != 0) goto L_0x0085
            boolean r0 = r8.b()
            if (r0 == 0) goto L_0x008c
            r0 = r8
            org.jsoup.parser.aj r0 = (org.jsoup.parser.aj) r0
            java.lang.String r0 = r0.i()
            r3 = 6
            java.lang.String[] r3 = new java.lang.String[r3]
            java.lang.String r4 = "basefont"
            r3[r1] = r4
            java.lang.String r4 = "bgsound"
            r3[r2] = r4
            java.lang.String r4 = "link"
            r3[r6] = r4
            r4 = 3
            java.lang.String r5 = "meta"
            r3[r4] = r5
            r4 = 4
            java.lang.String r5 = "noframes"
            r3[r4] = r5
            r4 = 5
            java.lang.String r5 = "style"
            r3[r4] = r5
            boolean r0 = org.jsoup.helper.StringUtil.in(r0, r3)
            if (r0 == 0) goto L_0x008c
        L_0x0085:
            org.jsoup.parser.c r0 = org.jsoup.parser.w.d
            boolean r0 = r9.a(r8, r0)
            goto L_0x000d
        L_0x008c:
            boolean r0 = r8.c()
            if (r0 == 0) goto L_0x00a7
            r0 = r8
            org.jsoup.parser.ai r0 = (org.jsoup.parser.ai) r0
            java.lang.String r0 = r0.i()
            java.lang.String r3 = "br"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x00a7
            boolean r0 = r7.b(r8, r9)
            goto L_0x000d
        L_0x00a7:
            boolean r0 = r8.b()
            if (r0 == 0) goto L_0x00c4
            r0 = r8
            org.jsoup.parser.aj r0 = (org.jsoup.parser.aj) r0
            java.lang.String r0 = r0.i()
            java.lang.String[] r3 = new java.lang.String[r6]
            java.lang.String r4 = "head"
            r3[r1] = r4
            java.lang.String r4 = "noscript"
            r3[r2] = r4
            boolean r0 = org.jsoup.helper.StringUtil.in(r0, r3)
            if (r0 != 0) goto L_0x00ca
        L_0x00c4:
            boolean r0 = r8.c()
            if (r0 == 0) goto L_0x00d0
        L_0x00ca:
            r9.b(r7)
            r0 = r1
            goto L_0x000d
        L_0x00d0:
            boolean r0 = r7.b(r8, r9)
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.w.a(org.jsoup.parser.ad, org.jsoup.parser.b):boolean");
    }
}
