package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import androidx.core.view.MotionEventCompat;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bx extends an {
    bx(String str) {
        super(str, 40, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        switch (aVar.d()) {
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
                amVar.a(BeforeAttributeName);
                return;
            case MotionEventCompat.AXIS_GENERIC_16 /*47*/:
                amVar.a(SelfClosingStartTag);
                return;
            case '>':
                amVar.c();
                amVar.a(Data);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.a(Data);
                return;
            default:
                amVar.c(this);
                aVar.e();
                amVar.a(BeforeAttributeName);
                return;
        }
    }
}
