package org.jsoup.parser;

import kotlinx.coroutines.internal.LockFreeTaskQueueCore;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class be extends an {
    be(String str) {
        super(str, 23, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        if (aVar.b()) {
            amVar.d(this);
            amVar.a(Data);
            return;
        }
        char d = aVar.d();
        switch (d) {
            case 0:
                amVar.c(this);
                amVar.a(65533);
                amVar.a(ScriptDataEscaped);
                return;
            case '-':
                amVar.a(d);
                return;
            case LockFreeTaskQueueCore.FROZEN_SHIFT /*60*/:
                amVar.a(ScriptDataEscapedLessthanSign);
                return;
            case '>':
                amVar.a(d);
                amVar.a(ScriptData);
                return;
            default:
                amVar.a(d);
                amVar.a(ScriptDataEscaped);
                return;
        }
    }
}
