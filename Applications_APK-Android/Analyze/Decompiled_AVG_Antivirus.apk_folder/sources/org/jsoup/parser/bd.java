package org.jsoup.parser;

import kotlinx.coroutines.internal.LockFreeTaskQueueCore;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bd extends an {
    bd(String str) {
        super(str, 22, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        if (aVar.b()) {
            amVar.d(this);
            amVar.a(Data);
            return;
        }
        char d = aVar.d();
        switch (d) {
            case 0:
                amVar.c(this);
                amVar.a(65533);
                amVar.a(ScriptDataEscaped);
                return;
            case '-':
                amVar.a(d);
                amVar.a(ScriptDataEscapedDashDash);
                return;
            case LockFreeTaskQueueCore.FROZEN_SHIFT /*60*/:
                amVar.a(ScriptDataEscapedLessthanSign);
                return;
            default:
                amVar.a(d);
                amVar.a(ScriptDataEscaped);
                return;
        }
    }
}
