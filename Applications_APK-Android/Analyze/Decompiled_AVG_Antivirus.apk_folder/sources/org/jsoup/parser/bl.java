package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bl extends an {
    bl(String str) {
        super(str, 29, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        char d = aVar.d();
        switch (d) {
            case 0:
                amVar.c(this);
                amVar.a(65533);
                amVar.a(ScriptDataDoubleEscaped);
                return;
            case '-':
                amVar.a(d);
                amVar.a(ScriptDataDoubleEscapedDashDash);
                return;
            case LockFreeTaskQueueCore.FROZEN_SHIFT /*60*/:
                amVar.a(d);
                amVar.a(ScriptDataDoubleEscapedLessthanSign);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.a(Data);
                return;
            default:
                amVar.a(d);
                amVar.a(ScriptDataDoubleEscaped);
                return;
        }
    }
}
