package org.jsoup.parser;

import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Element;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class v extends c {
    v(String str) {
        super(str, 3, (byte) 0);
    }

    private static boolean a(ad adVar, dd ddVar) {
        ddVar.a(new ai("head"));
        return ddVar.a(adVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.parser.v.a(org.jsoup.parser.ad, org.jsoup.parser.dd):boolean
     arg types: [org.jsoup.parser.ad, org.jsoup.parser.b]
     candidates:
      org.jsoup.parser.v.a(org.jsoup.parser.ad, org.jsoup.parser.b):boolean
      org.jsoup.parser.c.a(org.jsoup.parser.aj, org.jsoup.parser.b):void
      org.jsoup.parser.c.a(org.jsoup.parser.ad, org.jsoup.parser.b):boolean
      org.jsoup.parser.v.a(org.jsoup.parser.ad, org.jsoup.parser.dd):boolean */
    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (c.a(adVar)) {
            bVar.a((ae) adVar);
            return true;
        }
        switch (t.a[adVar.a.ordinal()]) {
            case 1:
                bVar.a((af) adVar);
                break;
            case 2:
                bVar.b(this);
                return false;
            case 3:
                aj ajVar = (aj) adVar;
                String i = ajVar.i();
                if (i.equals("html")) {
                    return g.a(adVar, bVar);
                }
                if (StringUtil.in(i, "base", "basefont", "bgsound", "command", "link")) {
                    Element b = bVar.b(ajVar);
                    if (i.equals("base") && b.hasAttr("href")) {
                        bVar.a(b);
                        break;
                    }
                } else if (i.equals("meta")) {
                    bVar.b(ajVar);
                    break;
                } else if (i.equals("title")) {
                    bVar.a(ajVar);
                    bVar.d.a(an.Rcdata);
                    bVar.b();
                    bVar.a(c.h);
                    break;
                } else {
                    if (StringUtil.in(i, "noframes", "style")) {
                        c.a(ajVar, bVar);
                        break;
                    } else if (i.equals("noscript")) {
                        bVar.a(ajVar);
                        bVar.a(e);
                        break;
                    } else if (i.equals("script")) {
                        bVar.d.a(an.ScriptData);
                        bVar.b();
                        bVar.a(h);
                        bVar.a(ajVar);
                        break;
                    } else if (!i.equals("head")) {
                        return a(adVar, (dd) bVar);
                    } else {
                        bVar.b(this);
                        return false;
                    }
                }
            case 4:
                String i2 = ((ai) adVar).i();
                if (i2.equals("head")) {
                    bVar.h();
                    bVar.a(f);
                    break;
                } else {
                    if (StringUtil.in(i2, "body", "html", "br")) {
                        return a(adVar, (dd) bVar);
                    }
                    bVar.b(this);
                    return false;
                }
            default:
                return a(adVar, (dd) bVar);
        }
        return true;
    }
}
