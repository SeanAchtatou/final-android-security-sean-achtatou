package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import androidx.core.view.MotionEventCompat;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class cn extends an {
    cn(String str) {
        super(str, 55, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        switch (aVar.d()) {
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
                return;
            case MotionEventCompat.AXIS_GENERIC_3 /*34*/:
                amVar.a(DoctypePublicIdentifier_doubleQuoted);
                return;
            case MotionEventCompat.AXIS_GENERIC_8 /*39*/:
                amVar.a(DoctypePublicIdentifier_singleQuoted);
                return;
            case '>':
                amVar.c(this);
                amVar.c.e = true;
                amVar.f();
                amVar.a(Data);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.c.e = true;
                amVar.f();
                amVar.a(Data);
                return;
            default:
                amVar.c(this);
                amVar.c.e = true;
                amVar.a(BogusDoctype);
                return;
        }
    }
}
