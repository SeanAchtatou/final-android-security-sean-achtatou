package org.jsoup.parser;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class g extends c {
    g(String str) {
        super(str, 11, (byte) 0);
    }

    private static boolean a(ad adVar, dd ddVar) {
        if (ddVar.a(new ai("colgroup"))) {
            return ddVar.a(adVar);
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.parser.g.a(org.jsoup.parser.ad, org.jsoup.parser.dd):boolean
     arg types: [org.jsoup.parser.ad, org.jsoup.parser.b]
     candidates:
      org.jsoup.parser.g.a(org.jsoup.parser.ad, org.jsoup.parser.b):boolean
      org.jsoup.parser.c.a(org.jsoup.parser.aj, org.jsoup.parser.b):void
      org.jsoup.parser.c.a(org.jsoup.parser.ad, org.jsoup.parser.b):boolean
      org.jsoup.parser.g.a(org.jsoup.parser.ad, org.jsoup.parser.dd):boolean */
    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (c.a(adVar)) {
            bVar.a((ae) adVar);
            return true;
        }
        switch (t.a[adVar.a.ordinal()]) {
            case 1:
                bVar.a((af) adVar);
                break;
            case 2:
                bVar.b(this);
                break;
            case 3:
                aj ajVar = (aj) adVar;
                String i = ajVar.i();
                if (i.equals("html")) {
                    return bVar.a(adVar, g);
                }
                if (i.equals("col")) {
                    bVar.b(ajVar);
                    break;
                } else {
                    return a(adVar, (dd) bVar);
                }
            case 4:
                if (((ai) adVar).i().equals("colgroup")) {
                    if (!bVar.x().nodeName().equals("html")) {
                        bVar.h();
                        bVar.a(i);
                        break;
                    } else {
                        bVar.b(this);
                        return false;
                    }
                } else {
                    return a(adVar, (dd) bVar);
                }
            case 5:
            default:
                return a(adVar, (dd) bVar);
            case 6:
                if (bVar.x().nodeName().equals("html")) {
                    return true;
                }
                return a(adVar, (dd) bVar);
        }
        return true;
    }
}
