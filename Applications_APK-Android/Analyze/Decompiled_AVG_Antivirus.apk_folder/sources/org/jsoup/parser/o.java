package org.jsoup.parser;

import org.jsoup.helper.StringUtil;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class o extends c {
    o(String str) {
        super(str, 1, (byte) 0);
    }

    private static boolean b(ad adVar, b bVar) {
        bVar.a("html");
        bVar.a(c);
        return bVar.a(adVar);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (adVar.a()) {
            bVar.b(this);
            return false;
        }
        if (adVar.d()) {
            bVar.a((af) adVar);
        } else if (c.a(adVar)) {
            return true;
        } else {
            if (!adVar.b() || !((aj) adVar).i().equals("html")) {
                if (adVar.c()) {
                    if (StringUtil.in(((ai) adVar).i(), "head", "body", "html", "br")) {
                        return b(adVar, bVar);
                    }
                }
                if (!adVar.c()) {
                    return b(adVar, bVar);
                }
                bVar.b(this);
                return false;
            }
            bVar.a((aj) adVar);
            bVar.a(c);
        }
        return true;
    }
}
