package org.jsoup.parser;

import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Element;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class x extends c {
    x(String str) {
        super(str, 5, (byte) 0);
    }

    private static boolean b(ad adVar, b bVar) {
        bVar.a((ad) new aj("body"));
        bVar.a(true);
        return bVar.a(adVar);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (c.a(adVar)) {
            bVar.a((ae) adVar);
        } else if (adVar.d()) {
            bVar.a((af) adVar);
        } else if (adVar.a()) {
            bVar.b(this);
        } else if (adVar.b()) {
            aj ajVar = (aj) adVar;
            String i = ajVar.i();
            if (i.equals("html")) {
                return bVar.a(adVar, g);
            }
            if (i.equals("body")) {
                bVar.a(ajVar);
                bVar.a(false);
                bVar.a(g);
            } else if (i.equals("frameset")) {
                bVar.a(ajVar);
                bVar.a(s);
            } else {
                if (StringUtil.in(i, "base", "basefont", "bgsound", "link", "meta", "noframes", "script", "style", "title")) {
                    bVar.b(this);
                    Element n = bVar.n();
                    bVar.b(n);
                    bVar.a(adVar, d);
                    bVar.d(n);
                } else if (i.equals("head")) {
                    bVar.b(this);
                    return false;
                } else {
                    b(adVar, bVar);
                }
            }
        } else if (adVar.c()) {
            if (StringUtil.in(((ai) adVar).i(), "body", "html")) {
                b(adVar, bVar);
            } else {
                bVar.b(this);
                return false;
            }
        } else {
            b(adVar, bVar);
        }
        return true;
    }
}
