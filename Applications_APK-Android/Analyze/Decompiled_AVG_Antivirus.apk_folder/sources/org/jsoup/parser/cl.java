package org.jsoup.parser;

import kotlin.text.Typography;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class cl extends an {
    cl(String str) {
        super(str, 53, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        if (aVar.b()) {
            amVar.d(this);
            amVar.c.e = true;
            amVar.f();
            amVar.a(Data);
        } else if (aVar.b(9, 10, 13, 12, ' ')) {
            aVar.f();
        } else if (aVar.b((char) Typography.greater)) {
            amVar.f();
            amVar.b(Data);
        } else if (aVar.c("PUBLIC")) {
            amVar.a(AfterDoctypePublicKeyword);
        } else if (aVar.c("SYSTEM")) {
            amVar.a(AfterDoctypeSystemKeyword);
        } else {
            amVar.c(this);
            amVar.c.e = true;
            amVar.b(BogusDoctype);
        }
    }
}
