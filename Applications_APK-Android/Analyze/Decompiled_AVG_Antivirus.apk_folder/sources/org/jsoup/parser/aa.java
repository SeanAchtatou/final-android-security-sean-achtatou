package org.jsoup.parser;

import org.jsoup.helper.StringUtil;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class aa extends c {
    aa(String str) {
        super(str, 8, (byte) 0);
    }

    private boolean b(ad adVar, b bVar) {
        bVar.b(this);
        if (!StringUtil.in(bVar.x().nodeName(), "table", "tbody", "tfoot", "thead", "tr")) {
            return bVar.a(adVar, g);
        }
        bVar.b(true);
        boolean a = bVar.a(adVar, g);
        bVar.b(false);
        return a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.parser.b.a(org.jsoup.parser.aj, boolean):org.jsoup.nodes.FormElement
     arg types: [org.jsoup.parser.aj, int]
     candidates:
      org.jsoup.parser.b.a(java.lang.String, java.lang.String[]):boolean
      org.jsoup.parser.b.a(org.jsoup.helper.DescendableLinkedList, org.jsoup.nodes.Element):boolean
      org.jsoup.parser.b.a(org.jsoup.nodes.Element, org.jsoup.nodes.Element):void
      org.jsoup.parser.b.a(org.jsoup.parser.ad, org.jsoup.parser.c):boolean
      org.jsoup.parser.b.a(org.jsoup.parser.aj, boolean):org.jsoup.nodes.FormElement */
    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (adVar.e()) {
            bVar.q();
            bVar.b();
            bVar.a(j);
            return bVar.a(adVar);
        } else if (adVar.d()) {
            bVar.a((af) adVar);
            return true;
        } else if (adVar.a()) {
            bVar.b(this);
            return false;
        } else if (adVar.b()) {
            aj ajVar = (aj) adVar;
            String i = ajVar.i();
            if (i.equals("caption")) {
                bVar.j();
                bVar.v();
                bVar.a(ajVar);
                bVar.a(k);
            } else if (i.equals("colgroup")) {
                bVar.j();
                bVar.a(ajVar);
                bVar.a(l);
            } else if (i.equals("col")) {
                bVar.a((ad) new aj("colgroup"));
                return bVar.a(adVar);
            } else {
                if (StringUtil.in(i, "tbody", "tfoot", "thead")) {
                    bVar.j();
                    bVar.a(ajVar);
                    bVar.a(m);
                } else {
                    if (StringUtil.in(i, "td", "th", "tr")) {
                        bVar.a((ad) new aj("tbody"));
                        return bVar.a(adVar);
                    } else if (i.equals("table")) {
                        bVar.b(this);
                        if (bVar.a(new ai("table"))) {
                            return bVar.a(adVar);
                        }
                    } else {
                        if (StringUtil.in(i, "style", "script")) {
                            return bVar.a(adVar, d);
                        }
                        if (i.equals("input")) {
                            if (!ajVar.d.get("type").equalsIgnoreCase("hidden")) {
                                return b(adVar, bVar);
                            }
                            bVar.b(ajVar);
                        } else if (!i.equals("form")) {
                            return b(adVar, bVar);
                        } else {
                            bVar.b(this);
                            if (bVar.o() != null) {
                                return false;
                            }
                            bVar.a(ajVar, false);
                        }
                    }
                }
            }
            return true;
        } else if (adVar.c()) {
            String i2 = ((ai) adVar).i();
            if (!i2.equals("table")) {
                if (!StringUtil.in(i2, "body", "caption", "col", "colgroup", "html", "tbody", "td", "tfoot", "th", "thead", "tr")) {
                    return b(adVar, bVar);
                }
                bVar.b(this);
                return false;
            } else if (!bVar.h(i2)) {
                bVar.b(this);
                return false;
            } else {
                bVar.c("table");
                bVar.m();
                return true;
            }
        } else if (!adVar.f()) {
            return b(adVar, bVar);
        } else {
            if (bVar.x().nodeName().equals("html")) {
                bVar.b(this);
            }
            return true;
        }
    }
}
