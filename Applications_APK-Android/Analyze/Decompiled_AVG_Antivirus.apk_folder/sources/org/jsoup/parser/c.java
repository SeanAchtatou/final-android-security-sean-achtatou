package org.jsoup.parser;

import org.jsoup.helper.StringUtil;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
abstract class c extends Enum {
    public static final c a = new d("Initial");
    public static final c b = new o("BeforeHtml");
    public static final c c = new u("BeforeHead");
    public static final c d = new v("InHead");
    public static final c e = new w("InHeadNoscript");
    public static final c f = new x("AfterHead");
    public static final c g = new y("InBody");
    public static final c h = new z("Text");
    public static final c i = new aa("InTable");
    public static final c j = new e("InTableText");
    public static final c k = new f("InCaption");
    public static final c l = new g("InColumnGroup");
    public static final c m = new h("InTableBody");
    public static final c n = new i("InRow");
    public static final c o = new j("InCell");
    public static final c p = new k("InSelect");
    public static final c q = new l("InSelectInTable");
    public static final c r = new m("AfterBody");
    public static final c s = new n("InFrameset");
    public static final c t = new p("AfterFrameset");
    public static final c u = new q("AfterAfterBody");
    public static final c v = new r("AfterAfterFrameset");
    public static final c w = new s("ForeignContent");
    /* access modifiers changed from: private */
    public static String x = "\u0000";
    private static final /* synthetic */ c[] y = {a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w};

    private c(String str, int i2) {
    }

    /* synthetic */ c(String str, int i2, byte b2) {
        this(str, i2);
    }

    static /* synthetic */ void a(aj ajVar, b bVar) {
        bVar.a(ajVar);
        bVar.d.a(an.Rawtext);
        bVar.b();
        bVar.a(h);
    }

    static /* synthetic */ boolean a(ad adVar) {
        if (!adVar.e()) {
            return false;
        }
        String g2 = ((ae) adVar).g();
        for (int i2 = 0; i2 < g2.length(); i2++) {
            if (!StringUtil.isWhitespace(g2.charAt(i2))) {
                return false;
            }
        }
        return true;
    }

    public static c valueOf(String str) {
        return (c) Enum.valueOf(c.class, str);
    }

    public static c[] values() {
        return (c[]) y.clone();
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(ad adVar, b bVar);
}
