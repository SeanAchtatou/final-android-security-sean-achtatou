package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import androidx.core.view.MotionEventCompat;
import kotlin.text.Typography;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bw extends an {
    bw(String str) {
        super(str, 39, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        String a = aVar.a(9, 10, 13, 12, ' ', Typography.amp, Typography.greater, 0, Typography.quote, '\'', Typography.less, '=', '`');
        if (a.length() > 0) {
            amVar.b.d(a);
        }
        char d = aVar.d();
        switch (d) {
            case 0:
                amVar.c(this);
                amVar.b.c(65533);
                return;
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
                amVar.a(BeforeAttributeName);
                return;
            case MotionEventCompat.AXIS_GENERIC_3 /*34*/:
            case MotionEventCompat.AXIS_GENERIC_8 /*39*/:
            case LockFreeTaskQueueCore.FROZEN_SHIFT /*60*/:
            case LockFreeTaskQueueCore.CLOSED_SHIFT /*61*/:
            case '`':
                amVar.c(this);
                amVar.b.c(d);
                return;
            case '&':
                char[] a2 = amVar.a(Character.valueOf(Typography.greater), true);
                if (a2 != null) {
                    amVar.b.a(a2);
                    return;
                } else {
                    amVar.b.c((char) Typography.amp);
                    return;
                }
            case '>':
                amVar.c();
                amVar.a(Data);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.a(Data);
                return;
            default:
                return;
        }
    }
}
