package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import androidx.core.view.MotionEventCompat;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bs extends an {
    bs(String str) {
        super(str, 36, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        char d = aVar.d();
        switch (d) {
            case 0:
                amVar.c(this);
                amVar.b.c(65533);
                amVar.a(AttributeValue_unquoted);
                return;
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
                return;
            case MotionEventCompat.AXIS_GENERIC_3 /*34*/:
                amVar.a(AttributeValue_doubleQuoted);
                return;
            case '&':
                aVar.e();
                amVar.a(AttributeValue_unquoted);
                return;
            case MotionEventCompat.AXIS_GENERIC_8 /*39*/:
                amVar.a(AttributeValue_singleQuoted);
                return;
            case LockFreeTaskQueueCore.FROZEN_SHIFT /*60*/:
            case LockFreeTaskQueueCore.CLOSED_SHIFT /*61*/:
            case '`':
                amVar.c(this);
                amVar.b.c(d);
                amVar.a(AttributeValue_unquoted);
                return;
            case '>':
                amVar.c(this);
                amVar.c();
                amVar.a(Data);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.a(Data);
                return;
            default:
                aVar.e();
                amVar.a(AttributeValue_unquoted);
                return;
        }
    }
}
