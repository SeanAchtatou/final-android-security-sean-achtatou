package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class cj extends an {
    cj(String str) {
        super(str, 51, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        if (aVar.m()) {
            amVar.e();
            amVar.a(DoctypeName);
            return;
        }
        char d = aVar.d();
        switch (d) {
            case 0:
                amVar.c(this);
                amVar.e();
                amVar.c.b.append(65533);
                amVar.a(DoctypeName);
                return;
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.e();
                amVar.c.e = true;
                amVar.f();
                amVar.a(Data);
                return;
            default:
                amVar.e();
                amVar.c.b.append(d);
                amVar.a(DoctypeName);
                return;
        }
    }
}
