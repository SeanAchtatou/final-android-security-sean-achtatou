package org.jsoup.parser;

import androidx.core.app.NotificationCompat;
import java.util.HashMap;
import java.util.Map;
import org.jsoup.helper.Validate;

public class Tag {
    private static final Map a = new HashMap();
    private static final String[] l = {"html", "head", "body", "frameset", "script", "noscript", "style", "meta", "link", "title", "frame", "noframes", "section", "nav", "aside", "hgroup", "header", "footer", "p", "h1", "h2", "h3", "h4", "h5", "h6", "ul", "ol", "pre", "div", "blockquote", "hr", "address", "figure", "figcaption", "form", "fieldset", "ins", "del", "s", "dl", "dt", "dd", "li", "table", "caption", "thead", "tfoot", "tbody", "colgroup", "col", "tr", "th", "td", "video", "audio", "canvas", "details", "menu", "plaintext"};
    private static final String[] m = {"object", "base", "font", "tt", "i", "b", "u", "big", "small", "em", "strong", "dfn", "code", "samp", "kbd", "var", "cite", "abbr", "time", "acronym", "mark", "ruby", "rt", "rp", "a", "img", "br", "wbr", "map", "q", "sub", "sup", "bdo", "iframe", "embed", "span", "input", "select", "textarea", "label", "button", "optgroup", "option", "legend", "datalist", "keygen", "output", NotificationCompat.CATEGORY_PROGRESS, "meter", "area", "param", "source", "track", "summary", "command", "device", "area", "basefont", "bgsound", "menuitem", "param", "source", "track"};
    private static final String[] n = {"meta", "link", "base", "frame", "img", "br", "wbr", "embed", "hr", "input", "keygen", "col", "command", "device", "area", "basefont", "bgsound", "menuitem", "param", "source", "track"};
    private static final String[] o = {"title", "a", "p", "h1", "h2", "h3", "h4", "h5", "h6", "pre", "address", "li", "th", "td", "script", "style", "ins", "del", "s"};
    private static final String[] p = {"pre", "plaintext", "title", "textarea"};
    private static final String[] q = {"button", "fieldset", "input", "keygen", "object", "output", "select", "textarea"};
    private static final String[] r = {"input", "keygen", "object", "select", "textarea"};
    private String b;
    private boolean c = true;
    private boolean d = true;
    private boolean e = true;
    private boolean f = true;
    private boolean g = false;
    private boolean h = false;
    private boolean i = false;
    private boolean j = false;
    private boolean k = false;

    static {
        for (String tag : l) {
            a(new Tag(tag));
        }
        for (String tag2 : m) {
            Tag tag3 = new Tag(tag2);
            tag3.c = false;
            tag3.e = false;
            tag3.d = false;
            a(tag3);
        }
        for (String str : n) {
            Tag tag4 = (Tag) a.get(str);
            Validate.notNull(tag4);
            tag4.e = false;
            tag4.f = false;
            tag4.g = true;
        }
        for (String str2 : o) {
            Tag tag5 = (Tag) a.get(str2);
            Validate.notNull(tag5);
            tag5.d = false;
        }
        for (String str3 : p) {
            Tag tag6 = (Tag) a.get(str3);
            Validate.notNull(tag6);
            tag6.i = true;
        }
        for (String str4 : q) {
            Tag tag7 = (Tag) a.get(str4);
            Validate.notNull(tag7);
            tag7.j = true;
        }
        for (String str5 : r) {
            Tag tag8 = (Tag) a.get(str5);
            Validate.notNull(tag8);
            tag8.k = true;
        }
    }

    private Tag(String str) {
        this.b = str.toLowerCase();
    }

    private static void a(Tag tag) {
        a.put(tag.b, tag);
    }

    public static boolean isKnownTag(String str) {
        return a.containsKey(str);
    }

    public static Tag valueOf(String str) {
        Validate.notNull(str);
        Tag tag = (Tag) a.get(str);
        if (tag != null) {
            return tag;
        }
        String lowerCase = str.trim().toLowerCase();
        Validate.notEmpty(lowerCase);
        Tag tag2 = (Tag) a.get(lowerCase);
        if (tag2 != null) {
            return tag2;
        }
        Tag tag3 = new Tag(lowerCase);
        tag3.c = false;
        tag3.e = true;
        return tag3;
    }

    /* access modifiers changed from: package-private */
    public final Tag a() {
        this.h = true;
        return this;
    }

    public boolean canContainBlock() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Tag)) {
            return false;
        }
        Tag tag = (Tag) obj;
        if (this.e != tag.e) {
            return false;
        }
        if (this.f != tag.f) {
            return false;
        }
        if (this.g != tag.g) {
            return false;
        }
        if (this.d != tag.d) {
            return false;
        }
        if (this.c != tag.c) {
            return false;
        }
        if (this.i != tag.i) {
            return false;
        }
        if (this.h != tag.h) {
            return false;
        }
        if (this.j != tag.j) {
            return false;
        }
        if (this.k != tag.k) {
            return false;
        }
        return this.b.equals(tag.b);
    }

    public boolean formatAsBlock() {
        return this.d;
    }

    public String getName() {
        return this.b;
    }

    public int hashCode() {
        int i2 = 1;
        int hashCode = ((this.j ? 1 : 0) + (((this.i ? 1 : 0) + (((this.h ? 1 : 0) + (((this.g ? 1 : 0) + (((this.f ? 1 : 0) + (((this.e ? 1 : 0) + (((this.d ? 1 : 0) + (((this.c ? 1 : 0) + (this.b.hashCode() * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (!this.k) {
            i2 = 0;
        }
        return hashCode + i2;
    }

    public boolean isBlock() {
        return this.c;
    }

    public boolean isData() {
        return !this.f && !isEmpty();
    }

    public boolean isEmpty() {
        return this.g;
    }

    public boolean isFormListed() {
        return this.j;
    }

    public boolean isFormSubmittable() {
        return this.k;
    }

    public boolean isInline() {
        return !this.c;
    }

    public boolean isKnownTag() {
        return a.containsKey(this.b);
    }

    public boolean isSelfClosing() {
        return this.g || this.h;
    }

    public boolean preserveWhitespace() {
        return this.i;
    }

    public String toString() {
        return this.b;
    }
}
