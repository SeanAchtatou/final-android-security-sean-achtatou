package org.jsoup.parser;

abstract class ad {
    al a;

    private ad() {
    }

    /* synthetic */ ad(byte b) {
        this();
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.a == al.Doctype;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.a == al.StartTag;
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        return this.a == al.EndTag;
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return this.a == al.Comment;
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        return this.a == al.Character;
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return this.a == al.EOF;
    }
}
