package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import androidx.core.view.MotionEventCompat;
import kotlin.text.Typography;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bt extends an {
    bt(String str) {
        super(str, 37, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        String a = aVar.a(Typography.quote, Typography.amp, 0);
        if (a.length() > 0) {
            amVar.b.d(a);
        }
        switch (aVar.d()) {
            case 0:
                amVar.c(this);
                amVar.b.c(65533);
                return;
            case MotionEventCompat.AXIS_GENERIC_3 /*34*/:
                amVar.a(AfterAttributeValue_quoted);
                return;
            case '&':
                char[] a2 = amVar.a(Character.valueOf(Typography.quote), true);
                if (a2 != null) {
                    amVar.b.a(a2);
                    return;
                } else {
                    amVar.b.c((char) Typography.amp);
                    return;
                }
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.a(Data);
                return;
            default:
                return;
        }
    }
}
