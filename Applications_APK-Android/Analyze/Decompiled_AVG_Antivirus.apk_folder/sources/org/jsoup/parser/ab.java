package org.jsoup.parser;

final class ab {
    /* access modifiers changed from: private */
    public static final String[] a = {"base", "basefont", "bgsound", "command", "link", "meta", "noframes", "script", "style", "title"};
    /* access modifiers changed from: private */
    public static final String[] b = {"address", "article", "aside", "blockquote", "center", "details", "dir", "div", "dl", "fieldset", "figcaption", "figure", "footer", "header", "hgroup", "menu", "nav", "ol", "p", "section", "summary", "ul"};
    /* access modifiers changed from: private */
    public static final String[] c = {"h1", "h2", "h3", "h4", "h5", "h6"};
    /* access modifiers changed from: private */
    public static final String[] d = {"pre", "listing"};
    /* access modifiers changed from: private */
    public static final String[] e = {"address", "div", "p"};
    /* access modifiers changed from: private */
    public static final String[] f = {"dd", "dt"};
    /* access modifiers changed from: private */
    public static final String[] g = {"b", "big", "code", "em", "font", "i", "s", "small", "strike", "strong", "tt", "u"};
    /* access modifiers changed from: private */
    public static final String[] h = {"applet", "marquee", "object"};
    /* access modifiers changed from: private */
    public static final String[] i = {"area", "br", "embed", "img", "keygen", "wbr"};
    /* access modifiers changed from: private */
    public static final String[] j = {"param", "source", "track"};
    /* access modifiers changed from: private */
    public static final String[] k = {"name", "action", "prompt"};
    /* access modifiers changed from: private */
    public static final String[] l = {"optgroup", "option"};
    /* access modifiers changed from: private */
    public static final String[] m = {"rp", "rt"};
    /* access modifiers changed from: private */
    public static final String[] n = {"caption", "col", "colgroup", "frame", "head", "tbody", "td", "tfoot", "th", "thead", "tr"};
    /* access modifiers changed from: private */
    public static final String[] o = {"address", "article", "aside", "blockquote", "button", "center", "details", "dir", "div", "dl", "fieldset", "figcaption", "figure", "footer", "header", "hgroup", "listing", "menu", "nav", "ol", "pre", "section", "summary", "ul"};
    /* access modifiers changed from: private */
    public static final String[] p = {"a", "b", "big", "code", "em", "font", "i", "nobr", "s", "small", "strike", "strong", "tt", "u"};
    /* access modifiers changed from: private */
    public static final String[] q = {"table", "tbody", "tfoot", "thead", "tr"};
}
