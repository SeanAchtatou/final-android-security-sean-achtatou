package org.jsoup.parser;

import org.jsoup.helper.StringUtil;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class i extends c {
    i(String str) {
        super(str, 13, (byte) 0);
    }

    private static boolean a(ad adVar, dd ddVar) {
        if (ddVar.a(new ai("tr"))) {
            return ddVar.a(adVar);
        }
        return false;
    }

    private static boolean b(ad adVar, b bVar) {
        return bVar.a(adVar, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.parser.i.a(org.jsoup.parser.ad, org.jsoup.parser.dd):boolean
     arg types: [org.jsoup.parser.ad, org.jsoup.parser.b]
     candidates:
      org.jsoup.parser.i.a(org.jsoup.parser.ad, org.jsoup.parser.b):boolean
      org.jsoup.parser.c.a(org.jsoup.parser.aj, org.jsoup.parser.b):void
      org.jsoup.parser.c.a(org.jsoup.parser.ad, org.jsoup.parser.b):boolean
      org.jsoup.parser.i.a(org.jsoup.parser.ad, org.jsoup.parser.dd):boolean */
    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (adVar.b()) {
            aj ajVar = (aj) adVar;
            String i = ajVar.i();
            if (StringUtil.in(i, "th", "td")) {
                bVar.l();
                bVar.a(ajVar);
                bVar.a(o);
                bVar.v();
            } else {
                return StringUtil.in(i, new String[]{"caption", "col", "colgroup", "tbody", "tfoot", "thead", "tr"}) ? a(adVar, (dd) bVar) : b(adVar, bVar);
            }
        } else if (!adVar.c()) {
            return b(adVar, bVar);
        } else {
            String i2 = ((ai) adVar).i();
            if (i2.equals("tr")) {
                if (!bVar.h(i2)) {
                    bVar.b(this);
                    return false;
                }
                bVar.l();
                bVar.h();
                bVar.a(m);
            } else if (i2.equals("table")) {
                return a(adVar, (dd) bVar);
            } else {
                if (!StringUtil.in(i2, "tbody", "tfoot", "thead")) {
                    if (!StringUtil.in(i2, "body", "caption", "col", "colgroup", "html", "td", "th")) {
                        return b(adVar, bVar);
                    }
                    bVar.b(this);
                    return false;
                } else if (!bVar.h(i2)) {
                    bVar.b(this);
                    return false;
                } else {
                    bVar.a(new ai("tr"));
                    return bVar.a(adVar);
                }
            }
        }
        return true;
    }
}
