package org.jsoup.parser;

import androidx.core.view.MotionEventCompat;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class as extends an {
    as(String str) {
        super(str, 12, (byte) 0);
    }

    private static void b(am amVar, a aVar) {
        amVar.a("</" + amVar.a.toString());
        aVar.e();
        amVar.a(Rcdata);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        if (aVar.m()) {
            String i = aVar.i();
            amVar.b.b(i.toLowerCase());
            amVar.a.append(i);
            return;
        }
        switch (aVar.d()) {
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
                if (amVar.h()) {
                    amVar.a(BeforeAttributeName);
                    return;
                } else {
                    b(amVar, aVar);
                    return;
                }
            case MotionEventCompat.AXIS_GENERIC_16 /*47*/:
                if (amVar.h()) {
                    amVar.a(SelfClosingStartTag);
                    return;
                } else {
                    b(amVar, aVar);
                    return;
                }
            case '>':
                if (amVar.h()) {
                    amVar.c();
                    amVar.a(Data);
                    return;
                }
                b(amVar, aVar);
                return;
            default:
                b(amVar, aVar);
                return;
        }
    }
}
