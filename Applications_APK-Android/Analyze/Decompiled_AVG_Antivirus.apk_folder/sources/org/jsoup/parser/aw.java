package org.jsoup.parser;

import androidx.core.view.MotionEventCompat;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class aw extends an {
    aw(String str) {
        super(str, 16, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        switch (aVar.d()) {
            case MotionEventCompat.AXIS_GENERIC_2 /*33*/:
                amVar.a("<!");
                amVar.a(ScriptDataEscapeStart);
                return;
            case MotionEventCompat.AXIS_GENERIC_16 /*47*/:
                amVar.g();
                amVar.a(ScriptDataEndTagOpen);
                return;
            default:
                amVar.a("<");
                aVar.e();
                amVar.a(ScriptData);
                return;
        }
    }
}
