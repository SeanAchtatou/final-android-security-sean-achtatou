package org.jsoup.parser;

import org.jsoup.helper.StringUtil;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class k extends c {
    k(String str) {
        super(str, 15, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        switch (adVar.a) {
            case Comment:
                bVar.a((af) adVar);
                break;
            case Doctype:
                bVar.b(this);
                return false;
            case StartTag:
                aj ajVar = (aj) adVar;
                String i = ajVar.i();
                if (i.equals("html")) {
                    return bVar.a(ajVar, g);
                }
                if (i.equals("option")) {
                    bVar.a(new ai("option"));
                    bVar.a(ajVar);
                    break;
                } else if (i.equals("optgroup")) {
                    if (bVar.x().nodeName().equals("option")) {
                        bVar.a(new ai("option"));
                    } else if (bVar.x().nodeName().equals("optgroup")) {
                        bVar.a(new ai("optgroup"));
                    }
                    bVar.a(ajVar);
                    break;
                } else if (i.equals("select")) {
                    bVar.b(this);
                    return bVar.a(new ai("select"));
                } else {
                    if (StringUtil.in(i, "input", "keygen", "textarea")) {
                        bVar.b(this);
                        if (!bVar.i("select")) {
                            return false;
                        }
                        bVar.a(new ai("select"));
                        return bVar.a((ad) ajVar);
                    } else if (i.equals("script")) {
                        return bVar.a(adVar, d);
                    } else {
                        bVar.b(this);
                        return false;
                    }
                }
            case EndTag:
                String i2 = ((ai) adVar).i();
                if (i2.equals("optgroup")) {
                    if (bVar.x().nodeName().equals("option") && bVar.e(bVar.x()) != null && bVar.e(bVar.x()).nodeName().equals("optgroup")) {
                        bVar.a(new ai("option"));
                    }
                    if (!bVar.x().nodeName().equals("optgroup")) {
                        bVar.b(this);
                        break;
                    } else {
                        bVar.h();
                        break;
                    }
                } else if (i2.equals("option")) {
                    if (!bVar.x().nodeName().equals("option")) {
                        bVar.b(this);
                        break;
                    } else {
                        bVar.h();
                        break;
                    }
                } else if (i2.equals("select")) {
                    if (bVar.i(i2)) {
                        bVar.c(i2);
                        bVar.m();
                        break;
                    } else {
                        bVar.b(this);
                        return false;
                    }
                } else {
                    bVar.b(this);
                    return false;
                }
            case Character:
                ae aeVar = (ae) adVar;
                if (!aeVar.g().equals(c.x)) {
                    bVar.a(aeVar);
                    break;
                } else {
                    bVar.b(this);
                    return false;
                }
            case EOF:
                if (!bVar.x().nodeName().equals("html")) {
                    bVar.b(this);
                    break;
                }
                break;
            default:
                bVar.b(this);
                return false;
        }
        return true;
    }
}
