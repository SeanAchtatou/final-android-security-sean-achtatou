package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import androidx.core.view.MotionEventCompat;
import kotlin.text.Typography;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class ap extends an {
    ap(String str) {
        super(str, 9, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        amVar.b.b(aVar.a(9, 10, 13, 12, ' ', '/', Typography.greater, 0).toLowerCase());
        switch (aVar.d()) {
            case 0:
                amVar.b.b(an.a());
                return;
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
                amVar.a(BeforeAttributeName);
                return;
            case MotionEventCompat.AXIS_GENERIC_16 /*47*/:
                amVar.a(SelfClosingStartTag);
                return;
            case '>':
                amVar.c();
                amVar.a(Data);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.a(Data);
                return;
            default:
                return;
        }
    }
}
