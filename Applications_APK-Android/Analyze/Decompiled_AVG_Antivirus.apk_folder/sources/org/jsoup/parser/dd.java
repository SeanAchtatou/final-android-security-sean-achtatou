package org.jsoup.parser;

import org.jsoup.helper.DescendableLinkedList;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

abstract class dd {
    a c;
    am d;
    protected Document e;
    protected DescendableLinkedList f;
    protected String g;
    protected ad h;
    protected ac i;

    dd() {
    }

    /* access modifiers changed from: package-private */
    public Document a(String str, String str2, ac acVar) {
        b(str, str2, acVar);
        w();
        return this.e;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(ad adVar);

    /* access modifiers changed from: protected */
    public void b(String str, String str2, ac acVar) {
        Validate.notNull(str, "String input must not be null");
        Validate.notNull(str2, "BaseURI must not be null");
        this.e = new Document(str2);
        this.c = new a(str);
        this.i = acVar;
        this.d = new am(this.c, acVar);
        this.f = new DescendableLinkedList();
        this.g = str2;
    }

    /* access modifiers changed from: protected */
    public final void w() {
        ad a;
        do {
            a = this.d.a();
            a(a);
        } while (a.a != al.EOF);
    }

    /* access modifiers changed from: protected */
    public final Element x() {
        return (Element) this.f.getLast();
    }
}
