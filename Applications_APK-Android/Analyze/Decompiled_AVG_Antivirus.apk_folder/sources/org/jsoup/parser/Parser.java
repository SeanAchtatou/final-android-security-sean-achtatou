package org.jsoup.parser;

import java.util.List;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

public class Parser {
    private dd a;
    private int b = 0;
    private ac c;

    public Parser(dd ddVar) {
        this.a = ddVar;
    }

    public static Parser htmlParser() {
        return new Parser(new b());
    }

    public static Document parse(String str, String str2) {
        return new b().a(str, str2, ac.b());
    }

    public static Document parseBodyFragment(String str, String str2) {
        Document createShell = Document.createShell(str2);
        Element body = createShell.body();
        List parseFragment = parseFragment(str, body, str2);
        for (Node appendChild : (Node[]) parseFragment.toArray(new Node[parseFragment.size()])) {
            body.appendChild(appendChild);
        }
        return createShell;
    }

    public static Document parseBodyFragmentRelaxed(String str, String str2) {
        return parse(str, str2);
    }

    public static List parseFragment(String str, Element element, String str2) {
        return new b().a(str, element, str2, ac.b());
    }

    public static List parseXmlFragment(String str, String str2) {
        return new XmlTreeBuilder().c(str, str2, ac.b());
    }

    public static String unescapeEntities(String str, boolean z) {
        return new am(new a(str), ac.b()).b(z);
    }

    public static Parser xmlParser() {
        return new Parser(new XmlTreeBuilder());
    }

    public List getErrors() {
        return this.c;
    }

    public dd getTreeBuilder() {
        return this.a;
    }

    public boolean isTrackErrors() {
        return this.b > 0;
    }

    public Document parseInput(String str, String str2) {
        this.c = isTrackErrors() ? ac.a(this.b) : ac.b();
        return this.a.a(str, str2, this.c);
    }

    public Parser setTrackErrors(int i) {
        this.b = i;
        return this;
    }

    public Parser setTreeBuilder(dd ddVar) {
        this.a = ddVar;
        return this;
    }
}
