package org.jsoup.parser;

import org.jsoup.helper.StringUtil;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class e extends c {
    e(String str) {
        super(str, 9, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        switch (t.a[adVar.a.ordinal()]) {
            case 5:
                ae aeVar = (ae) adVar;
                if (aeVar.g().equals(c.x)) {
                    bVar.b(this);
                    return false;
                }
                bVar.r().add(aeVar);
                return true;
            default:
                if (bVar.r().size() > 0) {
                    for (ae aeVar2 : bVar.r()) {
                        if (!c.a(aeVar2)) {
                            bVar.b(this);
                            if (StringUtil.in(bVar.x().nodeName(), "table", "tbody", "tfoot", "thead", "tr")) {
                                bVar.b(true);
                                bVar.a(aeVar2, g);
                                bVar.b(false);
                            } else {
                                bVar.a(aeVar2, g);
                            }
                        } else {
                            bVar.a(aeVar2);
                        }
                    }
                    bVar.q();
                }
                bVar.a(bVar.c());
                return bVar.a(adVar);
        }
    }
}
