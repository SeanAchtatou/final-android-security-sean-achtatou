package org.jsoup.parser;

import kotlin.text.Typography;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bf extends an {
    bf(String str) {
        super(str, 24, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        if (aVar.m()) {
            amVar.g();
            amVar.a.append(Character.toLowerCase(aVar.c()));
            amVar.a("<" + aVar.c());
            amVar.b(ScriptDataDoubleEscapeStart);
        } else if (aVar.b('/')) {
            amVar.g();
            amVar.b(ScriptDataEscapedEndTagOpen);
        } else {
            amVar.a((char) Typography.less);
            amVar.a(ScriptDataEscaped);
        }
    }
}
