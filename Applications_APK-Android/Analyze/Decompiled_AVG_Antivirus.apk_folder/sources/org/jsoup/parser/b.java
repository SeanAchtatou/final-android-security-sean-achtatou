package org.jsoup.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.helper.DescendableLinkedList;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

class b extends dd {
    public static final String[] a = {"applet", "caption", "html", "table", "td", "th", "marquee", "object"};
    static final /* synthetic */ boolean b = (!b.class.desiredAssertionStatus());
    private static final String[] j = {"script", "style"};
    private static final String[] k = {"ol", "ul"};
    private static final String[] l = {"button"};
    private static final String[] m = {"html", "table"};
    private static final String[] n = {"optgroup", "option"};
    private static final String[] o = {"dd", "dt", "li", "option", "optgroup", "p", "rp", "rt"};
    private static final String[] p = {"address", "applet", "area", "article", "aside", "base", "basefont", "bgsound", "blockquote", "body", "br", "button", "caption", "center", "col", "colgroup", "command", "dd", "details", "dir", "div", "dl", "dt", "embed", "fieldset", "figcaption", "figure", "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr", "html", "iframe", "img", "input", "isindex", "li", "link", "listing", "marquee", "menu", "meta", "nav", "noembed", "noframes", "noscript", "object", "ol", "p", "param", "plaintext", "pre", "script", "section", "select", "style", "summary", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "title", "tr", "ul", "wbr", "xmp"};
    private boolean A = false;
    private c q;
    private c r;
    private boolean s = false;
    private Element t;
    private FormElement u;
    private Element v;
    private DescendableLinkedList w = new DescendableLinkedList();
    private List x = new ArrayList();
    private boolean y = true;
    private boolean z = false;

    b() {
    }

    private static void a(LinkedList linkedList, Element element, Element element2) {
        int lastIndexOf = linkedList.lastIndexOf(element);
        Validate.isTrue(lastIndexOf != -1);
        linkedList.remove(lastIndexOf);
        linkedList.add(lastIndexOf, element2);
    }

    private boolean a(String str, String[] strArr) {
        return a(str, a, strArr);
    }

    private boolean a(String str, String[] strArr, String[] strArr2) {
        return a(new String[]{str}, strArr, strArr2);
    }

    private static boolean a(DescendableLinkedList descendableLinkedList, Element element) {
        Iterator descendingIterator = descendableLinkedList.descendingIterator();
        while (descendingIterator.hasNext()) {
            if (((Element) descendingIterator.next()) == element) {
                return true;
            }
        }
        return false;
    }

    private boolean a(String[] strArr, String[] strArr2, String[] strArr3) {
        Iterator descendingIterator = this.f.descendingIterator();
        while (descendingIterator.hasNext()) {
            String nodeName = ((Element) descendingIterator.next()).nodeName();
            if (StringUtil.in(nodeName, strArr)) {
                return true;
            }
            if (StringUtil.in(nodeName, strArr2)) {
                return false;
            }
            if (strArr3 != null && StringUtil.in(nodeName, strArr3)) {
                return false;
            }
        }
        Validate.fail("Should not be reachable");
        return false;
    }

    private void b(Node node) {
        if (this.f.size() == 0) {
            this.e.appendChild(node);
        } else if (this.z) {
            a(node);
        } else {
            x().appendChild(node);
        }
        if ((node instanceof Element) && ((Element) node).tag().isFormListed() && this.u != null) {
            this.u.addElement((Element) node);
        }
    }

    private void c(String... strArr) {
        Iterator descendingIterator = this.f.descendingIterator();
        while (descendingIterator.hasNext()) {
            Element element = (Element) descendingIterator.next();
            if (!StringUtil.in(element.nodeName(), strArr) && !element.nodeName().equals("html")) {
                descendingIterator.remove();
            } else {
                return;
            }
        }
    }

    static boolean g(Element element) {
        return StringUtil.in(element.nodeName(), p);
    }

    private void k(Element element) {
        b((Node) element);
        this.f.add(element);
    }

    /* access modifiers changed from: package-private */
    public final List a(String str, Element element, String str2, ac acVar) {
        this.q = c.a;
        b(str, str2, acVar);
        this.v = element;
        this.A = true;
        Element element2 = null;
        if (element != null) {
            if (element.ownerDocument() != null) {
                this.e.quirksMode(element.ownerDocument().quirksMode());
            }
            String tagName = element.tagName();
            if (StringUtil.in(tagName, "title", "textarea")) {
                this.d.a(an.Rcdata);
            } else {
                if (StringUtil.in(tagName, "iframe", "noembed", "noframes", "style", "xmp")) {
                    this.d.a(an.Rawtext);
                } else if (tagName.equals("script")) {
                    this.d.a(an.ScriptData);
                } else {
                    if (!tagName.equals("noscript")) {
                        tagName.equals("plaintext");
                    }
                    this.d.a(an.Data);
                }
            }
            Element element3 = new Element(Tag.valueOf("html"), str2);
            this.e.appendChild(element3);
            this.f.push(element3);
            m();
            Elements parents = element.parents();
            parents.add(0, element);
            Iterator it = parents.iterator();
            while (true) {
                if (!it.hasNext()) {
                    element2 = element3;
                    break;
                }
                Element element4 = (Element) it.next();
                if (element4 instanceof FormElement) {
                    this.u = (FormElement) element4;
                    element2 = element3;
                    break;
                }
            }
        }
        w();
        return element != null ? element2.childNodes() : this.e.childNodes();
    }

    /* access modifiers changed from: package-private */
    public final Document a(String str, String str2, ac acVar) {
        this.q = c.a;
        this.s = false;
        return super.a(str, str2, acVar);
    }

    /* access modifiers changed from: package-private */
    public final Element a(String str) {
        Element element = new Element(Tag.valueOf(str), this.g);
        k(element);
        return element;
    }

    /* access modifiers changed from: package-private */
    public final Element a(aj ajVar) {
        if (ajVar.c) {
            Element b2 = b(ajVar);
            this.f.add(b2);
            this.d.a(an.Data);
            this.d.a(new ai(b2.tagName()));
            return b2;
        }
        Element element = new Element(Tag.valueOf(ajVar.i()), this.g, ajVar.d);
        k(element);
        return element;
    }

    /* access modifiers changed from: package-private */
    public final FormElement a(aj ajVar, boolean z2) {
        FormElement formElement = new FormElement(Tag.valueOf(ajVar.i()), this.g, ajVar.d);
        this.u = formElement;
        b((Node) formElement);
        if (z2) {
            this.f.add(formElement);
        }
        return formElement;
    }

    /* access modifiers changed from: package-private */
    public final c a() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public final void a(Element element) {
        if (!this.s) {
            String absUrl = element.absUrl("href");
            if (absUrl.length() != 0) {
                this.g = absUrl;
                this.s = true;
                this.e.setBaseUri(absUrl);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Element element, Element element2) {
        int lastIndexOf = this.f.lastIndexOf(element);
        Validate.isTrue(lastIndexOf != -1);
        this.f.add(lastIndexOf + 1, element2);
    }

    /* access modifiers changed from: package-private */
    public final void a(Node node) {
        Element element;
        boolean z2;
        Element b2 = b("table");
        if (b2 == null) {
            element = (Element) this.f.get(0);
            z2 = false;
        } else if (b2.parent() != null) {
            b2.parent();
            element = null;
            z2 = true;
        } else {
            element = e(b2);
            z2 = false;
        }
        if (z2) {
            Validate.notNull(b2);
            b2.before(node);
            return;
        }
        element.appendChild(node);
    }

    /* access modifiers changed from: package-private */
    public final void a(ae aeVar) {
        String tagName = x().tagName();
        x().appendChild((tagName.equals("script") || tagName.equals("style")) ? new DataNode(aeVar.g(), this.g) : new TextNode(aeVar.g(), this.g));
    }

    /* access modifiers changed from: package-private */
    public final void a(af afVar) {
        b(new Comment(afVar.b.toString(), this.g));
    }

    /* access modifiers changed from: package-private */
    public final void a(c cVar) {
        this.q = cVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z2) {
        this.y = z2;
    }

    /* access modifiers changed from: package-private */
    public final void a(String... strArr) {
        Iterator descendingIterator = this.f.descendingIterator();
        while (descendingIterator.hasNext()) {
            if (StringUtil.in(((Element) descendingIterator.next()).nodeName(), strArr)) {
                descendingIterator.remove();
                return;
            }
            descendingIterator.remove();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(ad adVar) {
        this.h = adVar;
        return this.q.a(adVar, this);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, c cVar) {
        this.h = adVar;
        return cVar.a(adVar, this);
    }

    /* access modifiers changed from: package-private */
    public final Element b(String str) {
        Iterator descendingIterator = this.f.descendingIterator();
        while (descendingIterator.hasNext()) {
            Element element = (Element) descendingIterator.next();
            if (element.nodeName().equals(str)) {
                return element;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final Element b(aj ajVar) {
        Tag valueOf = Tag.valueOf(ajVar.i());
        Element element = new Element(valueOf, this.g, ajVar.d);
        b((Node) element);
        if (ajVar.c) {
            if (!valueOf.isKnownTag()) {
                valueOf.a();
                this.d.b();
            } else if (valueOf.isSelfClosing()) {
                this.d.b();
            }
        }
        return element;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        this.r = this.q;
    }

    /* access modifiers changed from: package-private */
    public final void b(Element element) {
        this.f.add(element);
    }

    /* access modifiers changed from: package-private */
    public final void b(Element element, Element element2) {
        a(this.f, element, element2);
    }

    /* access modifiers changed from: package-private */
    public final void b(c cVar) {
        if (this.i.a()) {
            this.i.add(new ParseError(this.c.a(), "Unexpected token [%s] when in state [%s]", this.h.getClass().getSimpleName(), cVar));
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(boolean z2) {
        this.z = z2;
    }

    /* access modifiers changed from: package-private */
    public final boolean b(String[] strArr) {
        return a(strArr, a, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    public final c c() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public final void c(String str) {
        Iterator descendingIterator = this.f.descendingIterator();
        while (descendingIterator.hasNext()) {
            if (((Element) descendingIterator.next()).nodeName().equals(str)) {
                descendingIterator.remove();
                return;
            }
            descendingIterator.remove();
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(Element element, Element element2) {
        a(this.w, element, element2);
    }

    /* access modifiers changed from: package-private */
    public final boolean c(Element element) {
        return a(this.f, element);
    }

    /* access modifiers changed from: package-private */
    public final void d(String str) {
        Iterator descendingIterator = this.f.descendingIterator();
        while (descendingIterator.hasNext() && !((Element) descendingIterator.next()).nodeName().equals(str)) {
            descendingIterator.remove();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return this.y;
    }

    /* access modifiers changed from: package-private */
    public final boolean d(Element element) {
        Iterator descendingIterator = this.f.descendingIterator();
        while (descendingIterator.hasNext()) {
            if (((Element) descendingIterator.next()) == element) {
                descendingIterator.remove();
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final Document e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final Element e(Element element) {
        if (b || c(element)) {
            Iterator descendingIterator = this.f.descendingIterator();
            while (descendingIterator.hasNext()) {
                if (((Element) descendingIterator.next()) == element) {
                    return (Element) descendingIterator.next();
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public final boolean e(String str) {
        return a(str, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final void f(Element element) {
        this.t = element;
    }

    /* access modifiers changed from: package-private */
    public final boolean f(String str) {
        return a(str, k);
    }

    /* access modifiers changed from: package-private */
    public final boolean g() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public final boolean g(String str) {
        return a(str, l);
    }

    /* access modifiers changed from: package-private */
    public final Element h() {
        if (((Element) this.f.peekLast()).nodeName().equals("td") && !this.q.name().equals("InCell")) {
            Validate.isFalse(true, "pop td not in cell");
        }
        if (((Element) this.f.peekLast()).nodeName().equals("html")) {
            Validate.isFalse(true, "popping html!");
        }
        return (Element) this.f.pollLast();
    }

    /* access modifiers changed from: package-private */
    public final void h(Element element) {
        Element element2;
        Iterator descendingIterator = this.w.descendingIterator();
        int i = 0;
        while (true) {
            if (!descendingIterator.hasNext() || (element2 = (Element) descendingIterator.next()) == null) {
                break;
            }
            int i2 = element.nodeName().equals(element2.nodeName()) && element.attributes().equals(element2.attributes()) ? i + 1 : i;
            if (i2 == 3) {
                descendingIterator.remove();
                break;
            }
            i = i2;
        }
        this.w.add(element);
    }

    /* access modifiers changed from: package-private */
    public final boolean h(String str) {
        return a(str, m, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    public final DescendableLinkedList i() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final void i(Element element) {
        Iterator descendingIterator = this.w.descendingIterator();
        while (descendingIterator.hasNext()) {
            if (((Element) descendingIterator.next()) == element) {
                descendingIterator.remove();
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean i(String str) {
        Iterator descendingIterator = this.f.descendingIterator();
        while (descendingIterator.hasNext()) {
            String nodeName = ((Element) descendingIterator.next()).nodeName();
            if (nodeName.equals(str)) {
                return true;
            }
            if (!StringUtil.in(nodeName, n)) {
                return false;
            }
        }
        Validate.fail("Should not be reachable");
        return false;
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        c("table");
    }

    /* access modifiers changed from: package-private */
    public final void j(String str) {
        while (str != null && !x().nodeName().equals(str) && StringUtil.in(x().nodeName(), o)) {
            h();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean j(Element element) {
        return a(this.w, element);
    }

    /* access modifiers changed from: package-private */
    public final Element k(String str) {
        Element element;
        Iterator descendingIterator = this.w.descendingIterator();
        while (descendingIterator.hasNext() && (element = (Element) descendingIterator.next()) != null) {
            if (element.nodeName().equals(str)) {
                return element;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void k() {
        c("tbody", "tfoot", "thead");
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        c("tr");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m() {
        /*
            r5 = this;
            r0 = 0
            org.jsoup.helper.DescendableLinkedList r1 = r5.f
            java.util.Iterator r2 = r1.descendingIterator()
            r1 = r0
        L_0x0008:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0030
            java.lang.Object r0 = r2.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            boolean r3 = r2.hasNext()
            if (r3 != 0) goto L_0x00db
            r1 = 1
            org.jsoup.nodes.Element r0 = r5.v
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x0020:
            java.lang.String r1 = r1.nodeName()
            java.lang.String r3 = "select"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x0031
            org.jsoup.parser.c r0 = org.jsoup.parser.c.p
            r5.q = r0
        L_0x0030:
            return
        L_0x0031:
            java.lang.String r3 = "td"
            boolean r3 = r3.equals(r1)
            if (r3 != 0) goto L_0x0043
            java.lang.String r3 = "td"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x0048
            if (r0 != 0) goto L_0x0048
        L_0x0043:
            org.jsoup.parser.c r0 = org.jsoup.parser.c.o
            r5.q = r0
            goto L_0x0030
        L_0x0048:
            java.lang.String r3 = "tr"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x0055
            org.jsoup.parser.c r0 = org.jsoup.parser.c.n
            r5.q = r0
            goto L_0x0030
        L_0x0055:
            java.lang.String r3 = "tbody"
            boolean r3 = r3.equals(r1)
            if (r3 != 0) goto L_0x006d
            java.lang.String r3 = "thead"
            boolean r3 = r3.equals(r1)
            if (r3 != 0) goto L_0x006d
            java.lang.String r3 = "tfoot"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x0072
        L_0x006d:
            org.jsoup.parser.c r0 = org.jsoup.parser.c.m
            r5.q = r0
            goto L_0x0030
        L_0x0072:
            java.lang.String r3 = "caption"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x007f
            org.jsoup.parser.c r0 = org.jsoup.parser.c.k
            r5.q = r0
            goto L_0x0030
        L_0x007f:
            java.lang.String r3 = "colgroup"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x008c
            org.jsoup.parser.c r0 = org.jsoup.parser.c.l
            r5.q = r0
            goto L_0x0030
        L_0x008c:
            java.lang.String r3 = "table"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x0099
            org.jsoup.parser.c r0 = org.jsoup.parser.c.i
            r5.q = r0
            goto L_0x0030
        L_0x0099:
            java.lang.String r3 = "head"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x00a6
            org.jsoup.parser.c r0 = org.jsoup.parser.c.g
            r5.q = r0
            goto L_0x0030
        L_0x00a6:
            java.lang.String r3 = "body"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x00b4
            org.jsoup.parser.c r0 = org.jsoup.parser.c.g
            r5.q = r0
            goto L_0x0030
        L_0x00b4:
            java.lang.String r3 = "frameset"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x00c2
            org.jsoup.parser.c r0 = org.jsoup.parser.c.s
            r5.q = r0
            goto L_0x0030
        L_0x00c2:
            java.lang.String r3 = "html"
            boolean r1 = r3.equals(r1)
            if (r1 == 0) goto L_0x00d0
            org.jsoup.parser.c r0 = org.jsoup.parser.c.c
            r5.q = r0
            goto L_0x0030
        L_0x00d0:
            if (r0 == 0) goto L_0x00d8
            org.jsoup.parser.c r0 = org.jsoup.parser.c.g
            r5.q = r0
            goto L_0x0030
        L_0x00d8:
            r1 = r0
            goto L_0x0008
        L_0x00db:
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.b.m():void");
    }

    /* access modifiers changed from: package-private */
    public final Element n() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public final FormElement o() {
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public final void p() {
        this.u = null;
    }

    /* access modifiers changed from: package-private */
    public final void q() {
        this.x = new ArrayList();
    }

    /* access modifiers changed from: package-private */
    public final List r() {
        return this.x;
    }

    /* access modifiers changed from: package-private */
    public final void s() {
        j((String) null);
    }

    /* access modifiers changed from: package-private */
    public final void t() {
        boolean z2;
        Element element;
        int i;
        int size = this.w.size();
        if (size != 0 && this.w.getLast() != null && !c((Element) this.w.getLast())) {
            int i2 = size - 1;
            Element element2 = (Element) this.w.getLast();
            while (true) {
                if (i2 == 0) {
                    i = i2;
                    element = element2;
                    z2 = true;
                    break;
                }
                i2--;
                Element element3 = (Element) this.w.get(i2);
                if (element3 == null || c(element3)) {
                    z2 = false;
                    int i3 = i2;
                    element = element3;
                    i = i3;
                } else {
                    element2 = element3;
                }
            }
            while (true) {
                if (!z2) {
                    int i4 = i + 1;
                    int i5 = i4;
                    element = (Element) this.w.get(i4);
                    i = i5;
                }
                Validate.notNull(element);
                Element a2 = a(element.nodeName());
                a2.attributes().addAll(element.attributes());
                this.w.add(i, a2);
                this.w.remove(i + 1);
                if (i != size - 1) {
                    z2 = false;
                } else {
                    return;
                }
            }
        }
    }

    public String toString() {
        return "TreeBuilder{currentToken=" + this.h + ", state=" + this.q + ", currentElement=" + x() + '}';
    }

    /* access modifiers changed from: package-private */
    public final void u() {
        while (!this.w.isEmpty()) {
            Element element = (Element) this.w.peekLast();
            this.w.removeLast();
            if (element == null) {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void v() {
        this.w.add(null);
    }
}
