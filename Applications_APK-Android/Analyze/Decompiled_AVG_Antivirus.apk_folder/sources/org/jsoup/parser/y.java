package org.jsoup.parser;

import java.util.Iterator;
import org.jsoup.nodes.Element;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class y extends c {
    y(String str) {
        super(str, 6, (byte) 0);
    }

    private boolean b(ad adVar, b bVar) {
        Element element;
        String i = ((ai) adVar).i();
        Iterator descendingIterator = bVar.i().descendingIterator();
        do {
            if (descendingIterator.hasNext()) {
                element = (Element) descendingIterator.next();
                if (element.nodeName().equals(i)) {
                    bVar.j(i);
                    if (!i.equals(bVar.x().nodeName())) {
                        bVar.b(this);
                    }
                    bVar.c(i);
                }
            }
            return true;
        } while (!b.g(element));
        bVar.b(this);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.parser.b.a(org.jsoup.parser.aj, boolean):org.jsoup.nodes.FormElement
     arg types: [org.jsoup.parser.aj, int]
     candidates:
      org.jsoup.parser.b.a(java.lang.String, java.lang.String[]):boolean
      org.jsoup.parser.b.a(org.jsoup.helper.DescendableLinkedList, org.jsoup.nodes.Element):boolean
      org.jsoup.parser.b.a(org.jsoup.nodes.Element, org.jsoup.nodes.Element):void
      org.jsoup.parser.b.a(org.jsoup.parser.ad, org.jsoup.parser.c):boolean
      org.jsoup.parser.b.a(org.jsoup.parser.aj, boolean):org.jsoup.nodes.FormElement */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
      org.jsoup.nodes.Node.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:374:0x08f5  */
    /* JADX WARNING: Removed duplicated region for block: B:380:0x092d A[LOOP:9: B:379:0x092b->B:380:0x092d, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:381:0x0935  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(org.jsoup.parser.ad r13, org.jsoup.parser.b r14) {
        /*
            r12 = this;
            int[] r0 = org.jsoup.parser.t.a
            org.jsoup.parser.al r1 = r13.a
            int r1 = r1.ordinal()
            r0 = r0[r1]
            switch(r0) {
                case 1: goto L_0x0042;
                case 2: goto L_0x0048;
                case 3: goto L_0x004d;
                case 4: goto L_0x069a;
                case 5: goto L_0x000f;
                default: goto L_0x000d;
            }
        L_0x000d:
            r0 = 1
        L_0x000e:
            return r0
        L_0x000f:
            org.jsoup.parser.ae r13 = (org.jsoup.parser.ae) r13
            java.lang.String r0 = r13.g()
            java.lang.String r1 = org.jsoup.parser.c.x
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0024
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x0024:
            boolean r0 = r14.d()
            if (r0 == 0) goto L_0x0037
            boolean r0 = org.jsoup.parser.c.a(r13)
            if (r0 == 0) goto L_0x0037
            r14.t()
            r14.a(r13)
            goto L_0x000d
        L_0x0037:
            r14.t()
            r14.a(r13)
            r0 = 0
            r14.a(r0)
            goto L_0x000d
        L_0x0042:
            org.jsoup.parser.af r13 = (org.jsoup.parser.af) r13
            r14.a(r13)
            goto L_0x000d
        L_0x0048:
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x004d:
            r0 = r13
            org.jsoup.parser.aj r0 = (org.jsoup.parser.aj) r0
            java.lang.String r1 = r0.i()
            java.lang.String r2 = "html"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x008d
            r14.b(r12)
            org.jsoup.helper.DescendableLinkedList r1 = r14.i()
            java.lang.Object r1 = r1.getFirst()
            org.jsoup.nodes.Element r1 = (org.jsoup.nodes.Element) r1
            org.jsoup.nodes.Attributes r0 = r0.d
            java.util.Iterator r2 = r0.iterator()
        L_0x006f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x000d
            java.lang.Object r0 = r2.next()
            org.jsoup.nodes.Attribute r0 = (org.jsoup.nodes.Attribute) r0
            java.lang.String r3 = r0.getKey()
            boolean r3 = r1.hasAttr(r3)
            if (r3 != 0) goto L_0x006f
            org.jsoup.nodes.Attributes r3 = r1.attributes()
            r3.put(r0)
            goto L_0x006f
        L_0x008d:
            java.lang.String[] r2 = org.jsoup.parser.ab.a
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x009f
            org.jsoup.parser.c r0 = org.jsoup.parser.y.d
            boolean r0 = r14.a(r13, r0)
            goto L_0x000e
        L_0x009f:
            java.lang.String r2 = "body"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0101
            r14.b(r12)
            org.jsoup.helper.DescendableLinkedList r2 = r14.i()
            int r1 = r2.size()
            r3 = 1
            if (r1 == r3) goto L_0x00cf
            int r1 = r2.size()
            r3 = 2
            if (r1 <= r3) goto L_0x00d2
            r1 = 1
            java.lang.Object r1 = r2.get(r1)
            org.jsoup.nodes.Element r1 = (org.jsoup.nodes.Element) r1
            java.lang.String r1 = r1.nodeName()
            java.lang.String r3 = "body"
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x00d2
        L_0x00cf:
            r0 = 0
            goto L_0x000e
        L_0x00d2:
            r1 = 0
            r14.a(r1)
            r1 = 1
            java.lang.Object r1 = r2.get(r1)
            org.jsoup.nodes.Element r1 = (org.jsoup.nodes.Element) r1
            org.jsoup.nodes.Attributes r0 = r0.d
            java.util.Iterator r2 = r0.iterator()
        L_0x00e3:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x000d
            java.lang.Object r0 = r2.next()
            org.jsoup.nodes.Attribute r0 = (org.jsoup.nodes.Attribute) r0
            java.lang.String r3 = r0.getKey()
            boolean r3 = r1.hasAttr(r3)
            if (r3 != 0) goto L_0x00e3
            org.jsoup.nodes.Attributes r3 = r1.attributes()
            r3.put(r0)
            goto L_0x00e3
        L_0x0101:
            java.lang.String r2 = "frameset"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0162
            r14.b(r12)
            org.jsoup.helper.DescendableLinkedList r2 = r14.i()
            int r1 = r2.size()
            r3 = 1
            if (r1 == r3) goto L_0x0131
            int r1 = r2.size()
            r3 = 2
            if (r1 <= r3) goto L_0x0134
            r1 = 1
            java.lang.Object r1 = r2.get(r1)
            org.jsoup.nodes.Element r1 = (org.jsoup.nodes.Element) r1
            java.lang.String r1 = r1.nodeName()
            java.lang.String r3 = "body"
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0134
        L_0x0131:
            r0 = 0
            goto L_0x000e
        L_0x0134:
            boolean r1 = r14.d()
            if (r1 != 0) goto L_0x013d
            r0 = 0
            goto L_0x000e
        L_0x013d:
            r1 = 1
            java.lang.Object r1 = r2.get(r1)
            org.jsoup.nodes.Element r1 = (org.jsoup.nodes.Element) r1
            org.jsoup.nodes.Element r3 = r1.parent()
            if (r3 == 0) goto L_0x014d
            r1.remove()
        L_0x014d:
            int r1 = r2.size()
            r3 = 1
            if (r1 <= r3) goto L_0x0158
            r2.removeLast()
            goto L_0x014d
        L_0x0158:
            r14.a(r0)
            org.jsoup.parser.c r0 = org.jsoup.parser.y.s
            r14.a(r0)
            goto L_0x000d
        L_0x0162:
            java.lang.String[] r2 = org.jsoup.parser.ab.b
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x0183
            java.lang.String r1 = "p"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x017e
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "p"
            r1.<init>(r2)
            r14.a(r1)
        L_0x017e:
            r14.a(r0)
            goto L_0x000d
        L_0x0183:
            java.lang.String[] r2 = org.jsoup.parser.ab.c
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x01bc
            java.lang.String r1 = "p"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x019f
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "p"
            r1.<init>(r2)
            r14.a(r1)
        L_0x019f:
            org.jsoup.nodes.Element r1 = r14.x()
            java.lang.String r1 = r1.nodeName()
            java.lang.String[] r2 = org.jsoup.parser.ab.c
            boolean r1 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r1 == 0) goto L_0x01b7
            r14.b(r12)
            r14.h()
        L_0x01b7:
            r14.a(r0)
            goto L_0x000d
        L_0x01bc:
            java.lang.String[] r2 = org.jsoup.parser.ab.d
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x01e1
            java.lang.String r1 = "p"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x01d8
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "p"
            r1.<init>(r2)
            r14.a(r1)
        L_0x01d8:
            r14.a(r0)
            r0 = 0
            r14.a(r0)
            goto L_0x000d
        L_0x01e1:
            java.lang.String r2 = "form"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x020d
            org.jsoup.nodes.FormElement r1 = r14.o()
            if (r1 == 0) goto L_0x01f5
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x01f5:
            java.lang.String r1 = "p"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x0207
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "p"
            r1.<init>(r2)
            r14.a(r1)
        L_0x0207:
            r1 = 1
            r14.a(r0, r1)
            goto L_0x000d
        L_0x020d:
            java.lang.String r2 = "li"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0271
            r1 = 0
            r14.a(r1)
            org.jsoup.helper.DescendableLinkedList r3 = r14.i()
            int r1 = r3.size()
            int r1 = r1 + -1
            r2 = r1
        L_0x0224:
            if (r2 <= 0) goto L_0x0242
            java.lang.Object r1 = r3.get(r2)
            org.jsoup.nodes.Element r1 = (org.jsoup.nodes.Element) r1
            java.lang.String r4 = r1.nodeName()
            java.lang.String r5 = "li"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0259
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "li"
            r1.<init>(r2)
            r14.a(r1)
        L_0x0242:
            java.lang.String r1 = "p"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x0254
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "p"
            r1.<init>(r2)
            r14.a(r1)
        L_0x0254:
            r14.a(r0)
            goto L_0x000d
        L_0x0259:
            boolean r4 = org.jsoup.parser.b.g(r1)
            if (r4 == 0) goto L_0x026d
            java.lang.String r1 = r1.nodeName()
            java.lang.String[] r4 = org.jsoup.parser.ab.e
            boolean r1 = org.jsoup.helper.StringUtil.in(r1, r4)
            if (r1 == 0) goto L_0x0242
        L_0x026d:
            int r1 = r2 + -1
            r2 = r1
            goto L_0x0224
        L_0x0271:
            java.lang.String[] r2 = org.jsoup.parser.ab.f
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x02db
            r1 = 0
            r14.a(r1)
            org.jsoup.helper.DescendableLinkedList r3 = r14.i()
            int r1 = r3.size()
            int r1 = r1 + -1
            r2 = r1
        L_0x028a:
            if (r2 <= 0) goto L_0x02ac
            java.lang.Object r1 = r3.get(r2)
            org.jsoup.nodes.Element r1 = (org.jsoup.nodes.Element) r1
            java.lang.String r4 = r1.nodeName()
            java.lang.String[] r5 = org.jsoup.parser.ab.f
            boolean r4 = org.jsoup.helper.StringUtil.in(r4, r5)
            if (r4 == 0) goto L_0x02c3
            org.jsoup.parser.ai r2 = new org.jsoup.parser.ai
            java.lang.String r1 = r1.nodeName()
            r2.<init>(r1)
            r14.a(r2)
        L_0x02ac:
            java.lang.String r1 = "p"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x02be
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "p"
            r1.<init>(r2)
            r14.a(r1)
        L_0x02be:
            r14.a(r0)
            goto L_0x000d
        L_0x02c3:
            boolean r4 = org.jsoup.parser.b.g(r1)
            if (r4 == 0) goto L_0x02d7
            java.lang.String r1 = r1.nodeName()
            java.lang.String[] r4 = org.jsoup.parser.ab.e
            boolean r1 = org.jsoup.helper.StringUtil.in(r1, r4)
            if (r1 == 0) goto L_0x02ac
        L_0x02d7:
            int r1 = r2 + -1
            r2 = r1
            goto L_0x028a
        L_0x02db:
            java.lang.String r2 = "plaintext"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0301
            java.lang.String r1 = "p"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x02f5
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "p"
            r1.<init>(r2)
            r14.a(r1)
        L_0x02f5:
            r14.a(r0)
            org.jsoup.parser.am r0 = r14.d
            org.jsoup.parser.an r1 = org.jsoup.parser.an.PLAINTEXT
            r0.a(r1)
            goto L_0x000d
        L_0x0301:
            java.lang.String r2 = "button"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x032f
            java.lang.String r1 = "button"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x0323
            r14.b(r12)
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "button"
            r1.<init>(r2)
            r14.a(r1)
            r14.a(r0)
            goto L_0x000d
        L_0x0323:
            r14.t()
            r14.a(r0)
            r0 = 0
            r14.a(r0)
            goto L_0x000d
        L_0x032f:
            java.lang.String r2 = "a"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0366
            java.lang.String r1 = "a"
            org.jsoup.nodes.Element r1 = r14.k(r1)
            if (r1 == 0) goto L_0x035a
            r14.b(r12)
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "a"
            r1.<init>(r2)
            r14.a(r1)
            java.lang.String r1 = "a"
            org.jsoup.nodes.Element r1 = r14.b(r1)
            if (r1 == 0) goto L_0x035a
            r14.i(r1)
            r14.d(r1)
        L_0x035a:
            r14.t()
            org.jsoup.nodes.Element r0 = r14.a(r0)
            r14.h(r0)
            goto L_0x000d
        L_0x0366:
            java.lang.String[] r2 = org.jsoup.parser.ab.g
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x037c
            r14.t()
            org.jsoup.nodes.Element r0 = r14.a(r0)
            r14.h(r0)
            goto L_0x000d
        L_0x037c:
            java.lang.String r2 = "nobr"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x03a8
            r14.t()
            java.lang.String r1 = "nobr"
            boolean r1 = r14.e(r1)
            if (r1 == 0) goto L_0x039f
            r14.b(r12)
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "nobr"
            r1.<init>(r2)
            r14.a(r1)
            r14.t()
        L_0x039f:
            org.jsoup.nodes.Element r0 = r14.a(r0)
            r14.h(r0)
            goto L_0x000d
        L_0x03a8:
            java.lang.String[] r2 = org.jsoup.parser.ab.h
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x03c1
            r14.t()
            r14.a(r0)
            r14.v()
            r0 = 0
            r14.a(r0)
            goto L_0x000d
        L_0x03c1:
            java.lang.String r2 = "table"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x03f5
            org.jsoup.nodes.Document r1 = r14.e()
            org.jsoup.nodes.Document$QuirksMode r1 = r1.quirksMode()
            org.jsoup.nodes.Document$QuirksMode r2 = org.jsoup.nodes.Document.QuirksMode.quirks
            if (r1 == r2) goto L_0x03e7
            java.lang.String r1 = "p"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x03e7
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "p"
            r1.<init>(r2)
            r14.a(r1)
        L_0x03e7:
            r14.a(r0)
            r0 = 0
            r14.a(r0)
            org.jsoup.parser.c r0 = org.jsoup.parser.y.i
            r14.a(r0)
            goto L_0x000d
        L_0x03f5:
            java.lang.String[] r2 = org.jsoup.parser.ab.i
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x040b
            r14.t()
            r14.b(r0)
            r0 = 0
            r14.a(r0)
            goto L_0x000d
        L_0x040b:
            java.lang.String r2 = "input"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x042e
            r14.t()
            org.jsoup.nodes.Element r0 = r14.b(r0)
            java.lang.String r1 = "type"
            java.lang.String r0 = r0.attr(r1)
            java.lang.String r1 = "hidden"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 != 0) goto L_0x000d
            r0 = 0
            r14.a(r0)
            goto L_0x000d
        L_0x042e:
            java.lang.String[] r2 = org.jsoup.parser.ab.j
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x043d
            r14.b(r0)
            goto L_0x000d
        L_0x043d:
            java.lang.String r2 = "hr"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0460
            java.lang.String r1 = "p"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x0457
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "p"
            r1.<init>(r2)
            r14.a(r1)
        L_0x0457:
            r14.b(r0)
            r0 = 0
            r14.a(r0)
            goto L_0x000d
        L_0x0460:
            java.lang.String r2 = "image"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0481
            java.lang.String r1 = "svg"
            org.jsoup.nodes.Element r1 = r14.b(r1)
            if (r1 != 0) goto L_0x047c
            java.lang.String r1 = "img"
            org.jsoup.parser.ak r0 = r0.a(r1)
            boolean r0 = r14.a(r0)
            goto L_0x000e
        L_0x047c:
            r14.a(r0)
            goto L_0x000d
        L_0x0481:
            java.lang.String r2 = "isindex"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x054a
            r14.b(r12)
            org.jsoup.nodes.FormElement r1 = r14.o()
            if (r1 == 0) goto L_0x0495
            r0 = 0
            goto L_0x000e
        L_0x0495:
            org.jsoup.parser.am r1 = r14.d
            r1.b()
            org.jsoup.parser.aj r1 = new org.jsoup.parser.aj
            java.lang.String r2 = "form"
            r1.<init>(r2)
            r14.a(r1)
            org.jsoup.nodes.Attributes r1 = r0.d
            java.lang.String r2 = "action"
            boolean r1 = r1.hasKey(r2)
            if (r1 == 0) goto L_0x04bf
            org.jsoup.nodes.FormElement r1 = r14.o()
            java.lang.String r2 = "action"
            org.jsoup.nodes.Attributes r3 = r0.d
            java.lang.String r4 = "action"
            java.lang.String r3 = r3.get(r4)
            r1.attr(r2, r3)
        L_0x04bf:
            org.jsoup.parser.aj r1 = new org.jsoup.parser.aj
            java.lang.String r2 = "hr"
            r1.<init>(r2)
            r14.a(r1)
            org.jsoup.parser.aj r1 = new org.jsoup.parser.aj
            java.lang.String r2 = "label"
            r1.<init>(r2)
            r14.a(r1)
            org.jsoup.nodes.Attributes r1 = r0.d
            java.lang.String r2 = "prompt"
            boolean r1 = r1.hasKey(r2)
            if (r1 == 0) goto L_0x0516
            org.jsoup.nodes.Attributes r1 = r0.d
            java.lang.String r2 = "prompt"
            java.lang.String r1 = r1.get(r2)
        L_0x04e5:
            org.jsoup.parser.ae r2 = new org.jsoup.parser.ae
            r2.<init>(r1)
            r14.a(r2)
            org.jsoup.nodes.Attributes r1 = new org.jsoup.nodes.Attributes
            r1.<init>()
            org.jsoup.nodes.Attributes r0 = r0.d
            java.util.Iterator r2 = r0.iterator()
        L_0x04f8:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0519
            java.lang.Object r0 = r2.next()
            org.jsoup.nodes.Attribute r0 = (org.jsoup.nodes.Attribute) r0
            java.lang.String r3 = r0.getKey()
            java.lang.String[] r4 = org.jsoup.parser.ab.k
            boolean r3 = org.jsoup.helper.StringUtil.in(r3, r4)
            if (r3 != 0) goto L_0x04f8
            r1.put(r0)
            goto L_0x04f8
        L_0x0516:
            java.lang.String r1 = "This is a searchable index. Enter search keywords: "
            goto L_0x04e5
        L_0x0519:
            java.lang.String r0 = "name"
            java.lang.String r2 = "isindex"
            r1.put(r0, r2)
            org.jsoup.parser.aj r0 = new org.jsoup.parser.aj
            java.lang.String r2 = "input"
            r0.<init>(r2, r1)
            r14.a(r0)
            org.jsoup.parser.ai r0 = new org.jsoup.parser.ai
            java.lang.String r1 = "label"
            r0.<init>(r1)
            r14.a(r0)
            org.jsoup.parser.aj r0 = new org.jsoup.parser.aj
            java.lang.String r1 = "hr"
            r0.<init>(r1)
            r14.a(r0)
            org.jsoup.parser.ai r0 = new org.jsoup.parser.ai
            java.lang.String r1 = "form"
            r0.<init>(r1)
            r14.a(r0)
            goto L_0x000d
        L_0x054a:
            java.lang.String r2 = "textarea"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x056a
            r14.a(r0)
            org.jsoup.parser.am r0 = r14.d
            org.jsoup.parser.an r1 = org.jsoup.parser.an.Rcdata
            r0.a(r1)
            r14.b()
            r0 = 0
            r14.a(r0)
            org.jsoup.parser.c r0 = org.jsoup.parser.y.h
            r14.a(r0)
            goto L_0x000d
        L_0x056a:
            java.lang.String r2 = "xmp"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0590
            java.lang.String r1 = "p"
            boolean r1 = r14.g(r1)
            if (r1 == 0) goto L_0x0584
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "p"
            r1.<init>(r2)
            r14.a(r1)
        L_0x0584:
            r14.t()
            r1 = 0
            r14.a(r1)
            org.jsoup.parser.c.a(r0, r14)
            goto L_0x000d
        L_0x0590:
            java.lang.String r2 = "iframe"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x05a1
            r1 = 0
            r14.a(r1)
            org.jsoup.parser.c.a(r0, r14)
            goto L_0x000d
        L_0x05a1:
            java.lang.String r2 = "noembed"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x05ae
            org.jsoup.parser.c.a(r0, r14)
            goto L_0x000d
        L_0x05ae:
            java.lang.String r2 = "select"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x05fa
            r14.t()
            r14.a(r0)
            r0 = 0
            r14.a(r0)
            org.jsoup.parser.c r0 = r14.a()
            org.jsoup.parser.c r1 = org.jsoup.parser.y.i
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x05ec
            org.jsoup.parser.c r1 = org.jsoup.parser.y.k
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x05ec
            org.jsoup.parser.c r1 = org.jsoup.parser.y.m
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x05ec
            org.jsoup.parser.c r1 = org.jsoup.parser.y.n
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x05ec
            org.jsoup.parser.c r1 = org.jsoup.parser.y.o
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x05f3
        L_0x05ec:
            org.jsoup.parser.c r0 = org.jsoup.parser.y.q
            r14.a(r0)
            goto L_0x000d
        L_0x05f3:
            org.jsoup.parser.c r0 = org.jsoup.parser.y.p
            r14.a(r0)
            goto L_0x000d
        L_0x05fa:
            java.lang.String[] r2 = org.jsoup.parser.ab.l
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x0626
            org.jsoup.nodes.Element r1 = r14.x()
            java.lang.String r1 = r1.nodeName()
            java.lang.String r2 = "option"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x061e
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "option"
            r1.<init>(r2)
            r14.a(r1)
        L_0x061e:
            r14.t()
            r14.a(r0)
            goto L_0x000d
        L_0x0626:
            java.lang.String[] r2 = org.jsoup.parser.ab.m
            boolean r2 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r2 == 0) goto L_0x0658
            java.lang.String r1 = "ruby"
            boolean r1 = r14.e(r1)
            if (r1 == 0) goto L_0x000d
            r14.s()
            org.jsoup.nodes.Element r1 = r14.x()
            java.lang.String r1 = r1.nodeName()
            java.lang.String r2 = "ruby"
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x0653
            r14.b(r12)
            java.lang.String r1 = "ruby"
            r14.d(r1)
        L_0x0653:
            r14.a(r0)
            goto L_0x000d
        L_0x0658:
            java.lang.String r2 = "math"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x066d
            r14.t()
            r14.a(r0)
            org.jsoup.parser.am r0 = r14.d
            r0.b()
            goto L_0x000d
        L_0x066d:
            java.lang.String r2 = "svg"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0682
            r14.t()
            r14.a(r0)
            org.jsoup.parser.am r0 = r14.d
            r0.b()
            goto L_0x000d
        L_0x0682:
            java.lang.String[] r2 = org.jsoup.parser.ab.n
            boolean r1 = org.jsoup.helper.StringUtil.in(r1, r2)
            if (r1 == 0) goto L_0x0692
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x0692:
            r14.t()
            r14.a(r0)
            goto L_0x000d
        L_0x069a:
            r0 = r13
            org.jsoup.parser.ai r0 = (org.jsoup.parser.ai) r0
            java.lang.String r7 = r0.i()
            java.lang.String r1 = "body"
            boolean r1 = r7.equals(r1)
            if (r1 == 0) goto L_0x06be
            java.lang.String r0 = "body"
            boolean r0 = r14.e(r0)
            if (r0 != 0) goto L_0x06b7
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x06b7:
            org.jsoup.parser.c r0 = org.jsoup.parser.y.r
            r14.a(r0)
            goto L_0x000d
        L_0x06be:
            java.lang.String r1 = "html"
            boolean r1 = r7.equals(r1)
            if (r1 == 0) goto L_0x06d9
            org.jsoup.parser.ai r1 = new org.jsoup.parser.ai
            java.lang.String r2 = "body"
            r1.<init>(r2)
            boolean r1 = r14.a(r1)
            if (r1 == 0) goto L_0x000d
            boolean r0 = r14.a(r0)
            goto L_0x000e
        L_0x06d9:
            java.lang.String[] r1 = org.jsoup.parser.ab.o
            boolean r1 = org.jsoup.helper.StringUtil.in(r7, r1)
            if (r1 == 0) goto L_0x0708
            boolean r0 = r14.e(r7)
            if (r0 != 0) goto L_0x06ef
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x06ef:
            r14.s()
            org.jsoup.nodes.Element r0 = r14.x()
            java.lang.String r0 = r0.nodeName()
            boolean r0 = r0.equals(r7)
            if (r0 != 0) goto L_0x0703
            r14.b(r12)
        L_0x0703:
            r14.c(r7)
            goto L_0x000d
        L_0x0708:
            java.lang.String r1 = "form"
            boolean r1 = r7.equals(r1)
            if (r1 == 0) goto L_0x073e
            org.jsoup.nodes.FormElement r0 = r14.o()
            r14.p()
            if (r0 == 0) goto L_0x071f
            boolean r1 = r14.e(r7)
            if (r1 != 0) goto L_0x0725
        L_0x071f:
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x0725:
            r14.s()
            org.jsoup.nodes.Element r1 = r14.x()
            java.lang.String r1 = r1.nodeName()
            boolean r1 = r1.equals(r7)
            if (r1 != 0) goto L_0x0739
            r14.b(r12)
        L_0x0739:
            r14.d(r0)
            goto L_0x000d
        L_0x073e:
            java.lang.String r1 = "p"
            boolean r1 = r7.equals(r1)
            if (r1 == 0) goto L_0x0776
            boolean r1 = r14.g(r7)
            if (r1 != 0) goto L_0x075d
            r14.b(r12)
            org.jsoup.parser.aj r1 = new org.jsoup.parser.aj
            r1.<init>(r7)
            r14.a(r1)
            boolean r0 = r14.a(r0)
            goto L_0x000e
        L_0x075d:
            r14.j(r7)
            org.jsoup.nodes.Element r0 = r14.x()
            java.lang.String r0 = r0.nodeName()
            boolean r0 = r0.equals(r7)
            if (r0 != 0) goto L_0x0771
            r14.b(r12)
        L_0x0771:
            r14.c(r7)
            goto L_0x000d
        L_0x0776:
            java.lang.String r0 = "li"
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x07a3
            boolean r0 = r14.f(r7)
            if (r0 != 0) goto L_0x078a
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x078a:
            r14.j(r7)
            org.jsoup.nodes.Element r0 = r14.x()
            java.lang.String r0 = r0.nodeName()
            boolean r0 = r0.equals(r7)
            if (r0 != 0) goto L_0x079e
            r14.b(r12)
        L_0x079e:
            r14.c(r7)
            goto L_0x000d
        L_0x07a3:
            java.lang.String[] r0 = org.jsoup.parser.ab.f
            boolean r0 = org.jsoup.helper.StringUtil.in(r7, r0)
            if (r0 == 0) goto L_0x07d2
            boolean r0 = r14.e(r7)
            if (r0 != 0) goto L_0x07b9
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x07b9:
            r14.j(r7)
            org.jsoup.nodes.Element r0 = r14.x()
            java.lang.String r0 = r0.nodeName()
            boolean r0 = r0.equals(r7)
            if (r0 != 0) goto L_0x07cd
            r14.b(r12)
        L_0x07cd:
            r14.c(r7)
            goto L_0x000d
        L_0x07d2:
            java.lang.String[] r0 = org.jsoup.parser.ab.c
            boolean r0 = org.jsoup.helper.StringUtil.in(r7, r0)
            if (r0 == 0) goto L_0x0809
            java.lang.String[] r0 = org.jsoup.parser.ab.c
            boolean r0 = r14.b(r0)
            if (r0 != 0) goto L_0x07ec
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x07ec:
            r14.j(r7)
            org.jsoup.nodes.Element r0 = r14.x()
            java.lang.String r0 = r0.nodeName()
            boolean r0 = r0.equals(r7)
            if (r0 != 0) goto L_0x0800
            r14.b(r12)
        L_0x0800:
            java.lang.String[] r0 = org.jsoup.parser.ab.c
            r14.a(r0)
            goto L_0x000d
        L_0x0809:
            java.lang.String r0 = "sarcasm"
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x0817
            boolean r0 = r12.b(r13, r14)
            goto L_0x000e
        L_0x0817:
            java.lang.String[] r0 = org.jsoup.parser.ab.p
            boolean r0 = org.jsoup.helper.StringUtil.in(r7, r0)
            if (r0 == 0) goto L_0x0953
            r0 = 0
            r4 = r0
        L_0x0823:
            r0 = 8
            if (r4 >= r0) goto L_0x000d
            org.jsoup.nodes.Element r8 = r14.k(r7)
            if (r8 != 0) goto L_0x0833
            boolean r0 = r12.b(r13, r14)
            goto L_0x000e
        L_0x0833:
            boolean r0 = r14.c(r8)
            if (r0 != 0) goto L_0x0842
            r14.b(r12)
            r14.i(r8)
            r0 = 1
            goto L_0x000e
        L_0x0842:
            java.lang.String r0 = r8.nodeName()
            boolean r0 = r14.e(r0)
            if (r0 != 0) goto L_0x0852
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x0852:
            org.jsoup.nodes.Element r0 = r14.x()
            if (r0 == r8) goto L_0x085b
            r14.b(r12)
        L_0x085b:
            r5 = 0
            r2 = 0
            r1 = 0
            org.jsoup.helper.DescendableLinkedList r6 = r14.i()
            int r9 = r6.size()
            r0 = 0
            r3 = r0
        L_0x0868:
            if (r3 >= r9) goto L_0x0891
            r0 = 64
            if (r3 >= r0) goto L_0x0891
            java.lang.Object r0 = r6.get(r3)
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            if (r0 != r8) goto L_0x0888
            int r0 = r3 + -1
            java.lang.Object r0 = r6.get(r0)
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            r1 = 1
            r11 = r1
            r1 = r0
            r0 = r11
        L_0x0882:
            int r2 = r3 + 1
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x0868
        L_0x0888:
            if (r1 == 0) goto L_0x09ab
            boolean r10 = org.jsoup.parser.b.g(r0)
            if (r10 == 0) goto L_0x09ab
            r5 = r0
        L_0x0891:
            if (r5 != 0) goto L_0x08a0
            java.lang.String r0 = r8.nodeName()
            r14.c(r0)
            r14.i(r8)
            r0 = 1
            goto L_0x000e
        L_0x08a0:
            r0 = 0
            r3 = r5
            r6 = r0
            r0 = r5
        L_0x08a4:
            r1 = 3
            if (r6 >= r1) goto L_0x08e7
            boolean r1 = r14.c(r3)
            if (r1 == 0) goto L_0x08b1
            org.jsoup.nodes.Element r3 = r14.e(r3)
        L_0x08b1:
            boolean r1 = r14.j(r3)
            if (r1 != 0) goto L_0x08c0
            r14.d(r3)
            r1 = r3
        L_0x08bb:
            int r3 = r6 + 1
            r6 = r3
            r3 = r1
            goto L_0x08a4
        L_0x08c0:
            if (r3 == r8) goto L_0x08e7
            org.jsoup.nodes.Element r1 = new org.jsoup.nodes.Element
            java.lang.String r9 = r3.nodeName()
            org.jsoup.parser.Tag r9 = org.jsoup.parser.Tag.valueOf(r9)
            java.lang.String r10 = r14.f()
            r1.<init>(r9, r10)
            r14.c(r3, r1)
            r14.b(r3, r1)
            org.jsoup.nodes.Element r3 = r0.parent()
            if (r3 == 0) goto L_0x08e2
            r0.remove()
        L_0x08e2:
            r1.appendChild(r0)
            r0 = r1
            goto L_0x08bb
        L_0x08e7:
            java.lang.String r1 = r2.nodeName()
            java.lang.String[] r3 = org.jsoup.parser.ab.q
            boolean r1 = org.jsoup.helper.StringUtil.in(r1, r3)
            if (r1 == 0) goto L_0x0935
            org.jsoup.nodes.Element r1 = r0.parent()
            if (r1 == 0) goto L_0x08fe
            r0.remove()
        L_0x08fe:
            r14.a(r0)
        L_0x0901:
            org.jsoup.nodes.Element r2 = new org.jsoup.nodes.Element
            org.jsoup.parser.Tag r0 = r8.tag()
            java.lang.String r1 = r14.f()
            r2.<init>(r0, r1)
            org.jsoup.nodes.Attributes r0 = r2.attributes()
            org.jsoup.nodes.Attributes r1 = r8.attributes()
            r0.addAll(r1)
            java.util.List r0 = r5.childNodes()
            int r1 = r5.childNodeSize()
            org.jsoup.nodes.Node[] r1 = new org.jsoup.nodes.Node[r1]
            java.lang.Object[] r0 = r0.toArray(r1)
            org.jsoup.nodes.Node[] r0 = (org.jsoup.nodes.Node[]) r0
            int r3 = r0.length
            r1 = 0
        L_0x092b:
            if (r1 >= r3) goto L_0x0942
            r6 = r0[r1]
            r2.appendChild(r6)
            int r1 = r1 + 1
            goto L_0x092b
        L_0x0935:
            org.jsoup.nodes.Element r1 = r0.parent()
            if (r1 == 0) goto L_0x093e
            r0.remove()
        L_0x093e:
            r2.appendChild(r0)
            goto L_0x0901
        L_0x0942:
            r5.appendChild(r2)
            r14.i(r8)
            r14.d(r8)
            r14.a(r5, r2)
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0823
        L_0x0953:
            java.lang.String[] r0 = org.jsoup.parser.ab.h
            boolean r0 = org.jsoup.helper.StringUtil.in(r7, r0)
            if (r0 == 0) goto L_0x098d
            java.lang.String r0 = "name"
            boolean r0 = r14.e(r0)
            if (r0 != 0) goto L_0x000d
            boolean r0 = r14.e(r7)
            if (r0 != 0) goto L_0x0971
            r14.b(r12)
            r0 = 0
            goto L_0x000e
        L_0x0971:
            r14.s()
            org.jsoup.nodes.Element r0 = r14.x()
            java.lang.String r0 = r0.nodeName()
            boolean r0 = r0.equals(r7)
            if (r0 != 0) goto L_0x0985
            r14.b(r12)
        L_0x0985:
            r14.c(r7)
            r14.u()
            goto L_0x000d
        L_0x098d:
            java.lang.String r0 = "br"
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x09a5
            r14.b(r12)
            org.jsoup.parser.aj r0 = new org.jsoup.parser.aj
            java.lang.String r1 = "br"
            r0.<init>(r1)
            r14.a(r0)
            r0 = 0
            goto L_0x000e
        L_0x09a5:
            boolean r0 = r12.b(r13, r14)
            goto L_0x000e
        L_0x09ab:
            r0 = r1
            r1 = r2
            goto L_0x0882
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.y.a(org.jsoup.parser.ad, org.jsoup.parser.b):boolean");
    }
}
