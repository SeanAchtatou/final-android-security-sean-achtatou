package org.jsoup.parser;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class f extends c {
    f(String str) {
        super(str, 10, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x008e, code lost:
        if (org.jsoup.helper.StringUtil.in(((org.jsoup.parser.aj) r10).i(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr") == false) goto L_0x0090;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(org.jsoup.parser.ad r10, org.jsoup.parser.b r11) {
        /*
            r9 = this;
            r8 = 4
            r7 = 3
            r6 = 2
            r2 = 1
            r1 = 0
            boolean r0 = r10.c()
            if (r0 == 0) goto L_0x0050
            r0 = r10
            org.jsoup.parser.ai r0 = (org.jsoup.parser.ai) r0
            java.lang.String r0 = r0.i()
            java.lang.String r3 = "caption"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x0050
            org.jsoup.parser.ai r10 = (org.jsoup.parser.ai) r10
            java.lang.String r0 = r10.i()
            boolean r0 = r11.h(r0)
            if (r0 != 0) goto L_0x002b
            r11.b(r9)
            r0 = r1
        L_0x002a:
            return r0
        L_0x002b:
            r11.s()
            org.jsoup.nodes.Element r0 = r11.x()
            java.lang.String r0 = r0.nodeName()
            java.lang.String r1 = "caption"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0041
            r11.b(r9)
        L_0x0041:
            java.lang.String r0 = "caption"
            r11.c(r0)
            r11.u()
            org.jsoup.parser.c r0 = org.jsoup.parser.f.i
            r11.a(r0)
        L_0x004e:
            r0 = r2
            goto L_0x002a
        L_0x0050:
            boolean r0 = r10.b()
            if (r0 == 0) goto L_0x0090
            r0 = r10
            org.jsoup.parser.aj r0 = (org.jsoup.parser.aj) r0
            java.lang.String r0 = r0.i()
            r3 = 9
            java.lang.String[] r3 = new java.lang.String[r3]
            java.lang.String r4 = "caption"
            r3[r1] = r4
            java.lang.String r4 = "col"
            r3[r2] = r4
            java.lang.String r4 = "colgroup"
            r3[r6] = r4
            java.lang.String r4 = "tbody"
            r3[r7] = r4
            java.lang.String r4 = "td"
            r3[r8] = r4
            r4 = 5
            java.lang.String r5 = "tfoot"
            r3[r4] = r5
            r4 = 6
            java.lang.String r5 = "th"
            r3[r4] = r5
            r4 = 7
            java.lang.String r5 = "thead"
            r3[r4] = r5
            r4 = 8
            java.lang.String r5 = "tr"
            r3[r4] = r5
            boolean r0 = org.jsoup.helper.StringUtil.in(r0, r3)
            if (r0 != 0) goto L_0x00a5
        L_0x0090:
            boolean r0 = r10.c()
            if (r0 == 0) goto L_0x00bb
            r0 = r10
            org.jsoup.parser.ai r0 = (org.jsoup.parser.ai) r0
            java.lang.String r0 = r0.i()
            java.lang.String r3 = "table"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x00bb
        L_0x00a5:
            r11.b(r9)
            org.jsoup.parser.ai r0 = new org.jsoup.parser.ai
            java.lang.String r1 = "caption"
            r0.<init>(r1)
            boolean r0 = r11.a(r0)
            if (r0 == 0) goto L_0x004e
            boolean r0 = r11.a(r10)
            goto L_0x002a
        L_0x00bb:
            boolean r0 = r10.c()
            if (r0 == 0) goto L_0x0107
            r0 = r10
            org.jsoup.parser.ai r0 = (org.jsoup.parser.ai) r0
            java.lang.String r0 = r0.i()
            r3 = 10
            java.lang.String[] r3 = new java.lang.String[r3]
            java.lang.String r4 = "body"
            r3[r1] = r4
            java.lang.String r4 = "col"
            r3[r2] = r4
            java.lang.String r2 = "colgroup"
            r3[r6] = r2
            java.lang.String r2 = "html"
            r3[r7] = r2
            java.lang.String r2 = "tbody"
            r3[r8] = r2
            r2 = 5
            java.lang.String r4 = "td"
            r3[r2] = r4
            r2 = 6
            java.lang.String r4 = "tfoot"
            r3[r2] = r4
            r2 = 7
            java.lang.String r4 = "th"
            r3[r2] = r4
            r2 = 8
            java.lang.String r4 = "thead"
            r3[r2] = r4
            r2 = 9
            java.lang.String r4 = "tr"
            r3[r2] = r4
            boolean r0 = org.jsoup.helper.StringUtil.in(r0, r3)
            if (r0 == 0) goto L_0x0107
            r11.b(r9)
            r0 = r1
            goto L_0x002a
        L_0x0107:
            org.jsoup.parser.c r0 = org.jsoup.parser.f.g
            boolean r0 = r11.a(r10, r0)
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.f.a(org.jsoup.parser.ad, org.jsoup.parser.b):boolean");
    }
}
