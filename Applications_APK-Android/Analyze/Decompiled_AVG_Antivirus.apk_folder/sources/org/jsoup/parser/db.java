package org.jsoup.parser;

import androidx.core.text.HtmlCompat;
import androidx.core.view.MotionEventCompat;
import kotlin.text.Typography;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class db extends an {
    db(String str) {
        super(str, 7, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        switch (aVar.c()) {
            case MotionEventCompat.AXIS_GENERIC_2 /*33*/:
                amVar.b(MarkupDeclarationOpen);
                return;
            case MotionEventCompat.AXIS_GENERIC_16 /*47*/:
                amVar.b(EndTagOpen);
                return;
            case HtmlCompat.FROM_HTML_MODE_COMPACT /*63*/:
                amVar.b(BogusComment);
                return;
            default:
                if (aVar.m()) {
                    amVar.a(true);
                    amVar.a(TagName);
                    return;
                }
                amVar.c(this);
                amVar.a((char) Typography.less);
                amVar.a(Data);
                return;
        }
    }
}
