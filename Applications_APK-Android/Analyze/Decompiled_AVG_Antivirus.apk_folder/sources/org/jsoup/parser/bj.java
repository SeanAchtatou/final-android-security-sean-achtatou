package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import kotlin.text.Typography;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bj extends an {
    bj(String str) {
        super(str, 28, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        char c = aVar.c();
        switch (c) {
            case 0:
                amVar.c(this);
                aVar.f();
                amVar.a(65533);
                return;
            case '-':
                amVar.a(c);
                amVar.b(ScriptDataDoubleEscapedDash);
                return;
            case LockFreeTaskQueueCore.FROZEN_SHIFT /*60*/:
                amVar.a(c);
                amVar.b(ScriptDataDoubleEscapedLessthanSign);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.a(Data);
                return;
            default:
                amVar.a(aVar.a('-', Typography.less, 0));
                return;
        }
    }
}
