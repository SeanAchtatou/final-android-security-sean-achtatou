package org.jsoup.parser;

import java.util.ArrayList;

final class ac extends ArrayList {
    private final int a;

    private ac(int i, int i2) {
        super(i);
        this.a = i2;
    }

    static ac a(int i) {
        return new ac(16, i);
    }

    static ac b() {
        return new ac(0, 0);
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return size() < this.a;
    }
}
