package org.jsoup.parser;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class d extends c {
    d(String str) {
        super(str, 0, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (c.a(adVar)) {
            return true;
        }
        if (adVar.d()) {
            bVar.a((af) adVar);
            return true;
        } else if (adVar.a()) {
            ag agVar = (ag) adVar;
            bVar.e().appendChild(new DocumentType(agVar.b.toString(), agVar.c.toString(), agVar.d.toString(), bVar.f()));
            if (agVar.e) {
                bVar.e().quirksMode(Document.QuirksMode.quirks);
            }
            bVar.a(b);
            return true;
        } else {
            bVar.a(b);
            return bVar.a(adVar);
        }
    }
}
