package org.jsoup.parser;

import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;

abstract class ak extends ad {
    protected String b;
    boolean c = false;
    Attributes d;
    private String e;
    private StringBuilder f;

    ak() {
        super((byte) 0);
    }

    private final void j() {
        if (this.f == null) {
            this.f = new StringBuilder();
        }
    }

    /* access modifiers changed from: package-private */
    public final ak a(String str) {
        this.b = str;
        return this;
    }

    /* access modifiers changed from: package-private */
    public final void a(char c2) {
        b(String.valueOf(c2));
    }

    /* access modifiers changed from: package-private */
    public final void a(char[] cArr) {
        j();
        this.f.append(cArr);
    }

    /* access modifiers changed from: package-private */
    public final void b(char c2) {
        c(String.valueOf(c2));
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        if (this.b != null) {
            str = this.b.concat(str);
        }
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public final void c(char c2) {
        j();
        this.f.append(c2);
    }

    /* access modifiers changed from: package-private */
    public final void c(String str) {
        if (this.e != null) {
            str = this.e.concat(str);
        }
        this.e = str;
    }

    /* access modifiers changed from: package-private */
    public final void d(String str) {
        j();
        this.f.append(str);
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        if (this.d == null) {
            this.d = new Attributes();
        }
        if (this.e != null) {
            this.d.put(this.f == null ? new Attribute(this.e, "") : new Attribute(this.e, this.f.toString()));
        }
        this.e = null;
        if (this.f != null) {
            this.f.delete(0, this.f.length());
        }
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        if (this.e != null) {
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public final String i() {
        Validate.isFalse(this.b == null || this.b.length() == 0);
        return this.b;
    }
}
