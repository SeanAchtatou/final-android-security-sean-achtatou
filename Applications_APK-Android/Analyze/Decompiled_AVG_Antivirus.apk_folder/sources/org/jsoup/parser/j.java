package org.jsoup.parser;

import org.jsoup.helper.StringUtil;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class j extends c {
    j(String str) {
        super(str, 14, (byte) 0);
    }

    private static void a(b bVar) {
        if (bVar.h("td")) {
            bVar.a(new ai("td"));
        } else {
            bVar.a(new ai("th"));
        }
    }

    private static boolean b(ad adVar, b bVar) {
        return bVar.a(adVar, g);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (adVar.c()) {
            String i = ((ai) adVar).i();
            if (!StringUtil.in(i, "td", "th")) {
                if (StringUtil.in(i, "body", "caption", "col", "colgroup", "html")) {
                    bVar.b(this);
                    return false;
                }
                if (!StringUtil.in(i, "table", "tbody", "tfoot", "thead", "tr")) {
                    return b(adVar, bVar);
                }
                if (!bVar.h(i)) {
                    bVar.b(this);
                    return false;
                }
                a(bVar);
                return bVar.a(adVar);
            } else if (!bVar.h(i)) {
                bVar.b(this);
                bVar.a(n);
                return false;
            } else {
                bVar.s();
                if (!bVar.x().nodeName().equals(i)) {
                    bVar.b(this);
                }
                bVar.c(i);
                bVar.u();
                bVar.a(n);
                return true;
            }
        } else {
            if (adVar.b()) {
                if (StringUtil.in(((aj) adVar).i(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr")) {
                    if (bVar.h("td") || bVar.h("th")) {
                        a(bVar);
                        return bVar.a(adVar);
                    }
                    bVar.b(this);
                    return false;
                }
            }
            return b(adVar, bVar);
        }
    }
}
