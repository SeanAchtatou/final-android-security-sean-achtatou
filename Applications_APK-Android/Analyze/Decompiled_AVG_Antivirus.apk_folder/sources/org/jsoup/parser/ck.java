package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class ck extends an {
    ck(String str) {
        super(str, 52, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        if (aVar.m()) {
            amVar.c.b.append(aVar.i().toLowerCase());
            return;
        }
        char d = aVar.d();
        switch (d) {
            case 0:
                amVar.c(this);
                amVar.c.b.append(65533);
                return;
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
                amVar.a(AfterDoctypeName);
                return;
            case '>':
                amVar.f();
                amVar.a(Data);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.c.e = true;
                amVar.f();
                amVar.a(Data);
                return;
            default:
                amVar.c.b.append(d);
                return;
        }
    }
}
