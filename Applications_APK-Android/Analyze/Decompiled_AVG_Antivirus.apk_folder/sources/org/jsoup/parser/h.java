package org.jsoup.parser;

import org.jsoup.helper.StringUtil;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class h extends c {
    h(String str) {
        super(str, 12, (byte) 0);
    }

    private boolean b(ad adVar, b bVar) {
        if (bVar.h("tbody") || bVar.h("thead") || bVar.e("tfoot")) {
            bVar.k();
            bVar.a(new ai(bVar.x().nodeName()));
            return bVar.a(adVar);
        }
        bVar.b(this);
        return false;
    }

    private static boolean c(ad adVar, b bVar) {
        return bVar.a(adVar, i);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        switch (adVar.a) {
            case StartTag:
                aj ajVar = (aj) adVar;
                String i = ajVar.i();
                if (i.equals("tr")) {
                    bVar.k();
                    bVar.a(ajVar);
                    bVar.a(n);
                    break;
                } else {
                    if (StringUtil.in(i, "th", "td")) {
                        bVar.b(this);
                        bVar.a((ad) new aj("tr"));
                        return bVar.a((ad) ajVar);
                    }
                    return StringUtil.in(i, new String[]{"caption", "col", "colgroup", "tbody", "tfoot", "thead"}) ? b(adVar, bVar) : c(adVar, bVar);
                }
            case EndTag:
                String i2 = ((ai) adVar).i();
                if (StringUtil.in(i2, "tbody", "tfoot", "thead")) {
                    if (bVar.h(i2)) {
                        bVar.k();
                        bVar.h();
                        bVar.a(i);
                        break;
                    } else {
                        bVar.b(this);
                        return false;
                    }
                } else if (i2.equals("table")) {
                    return b(adVar, bVar);
                } else {
                    if (!StringUtil.in(i2, "body", "caption", "col", "colgroup", "html", "td", "th", "tr")) {
                        return c(adVar, bVar);
                    }
                    bVar.b(this);
                    return false;
                }
            default:
                return c(adVar, bVar);
        }
        return true;
    }
}
