package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import androidx.core.view.MotionEventCompat;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class br extends an {
    br(String str) {
        super(str, 35, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        char d = aVar.d();
        switch (d) {
            case 0:
                amVar.c(this);
                amVar.b.b(65533);
                amVar.a(AttributeName);
                return;
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
                return;
            case MotionEventCompat.AXIS_GENERIC_3 /*34*/:
            case MotionEventCompat.AXIS_GENERIC_8 /*39*/:
            case LockFreeTaskQueueCore.FROZEN_SHIFT /*60*/:
                amVar.c(this);
                amVar.b.g();
                amVar.b.b(d);
                amVar.a(AttributeName);
                return;
            case MotionEventCompat.AXIS_GENERIC_16 /*47*/:
                amVar.a(SelfClosingStartTag);
                return;
            case LockFreeTaskQueueCore.CLOSED_SHIFT /*61*/:
                amVar.a(BeforeAttributeValue);
                return;
            case '>':
                amVar.c();
                amVar.a(Data);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.a(Data);
                return;
            default:
                amVar.b.g();
                aVar.e();
                amVar.a(AttributeName);
                return;
        }
    }
}
