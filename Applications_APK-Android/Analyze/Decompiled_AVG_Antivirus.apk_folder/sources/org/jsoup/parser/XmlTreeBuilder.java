package org.jsoup.parser;

import java.util.List;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

public class XmlTreeBuilder extends dd {
    private void a(Node node) {
        x().appendChild(node);
    }

    /* JADX WARN: Type inference failed for: r1v11, types: [org.jsoup.nodes.XmlDeclaration] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(org.jsoup.parser.ad r7) {
        /*
            r6 = this;
            r5 = 1
            int[] r0 = org.jsoup.parser.de.a
            org.jsoup.parser.al r1 = r7.a
            int r1 = r1.ordinal()
            r0 = r0[r1]
            switch(r0) {
                case 1: goto L_0x0023;
                case 2: goto L_0x0052;
                case 3: goto L_0x0094;
                case 4: goto L_0x00da;
                case 5: goto L_0x00ec;
                case 6: goto L_0x0022;
                default: goto L_0x000e;
            }
        L_0x000e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "Unexpected token type: "
            r0.<init>(r1)
            org.jsoup.parser.al r1 = r7.a
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            org.jsoup.helper.Validate.fail(r0)
        L_0x0022:
            return r5
        L_0x0023:
            org.jsoup.parser.aj r7 = (org.jsoup.parser.aj) r7
            java.lang.String r0 = r7.i()
            org.jsoup.parser.Tag r0 = org.jsoup.parser.Tag.valueOf(r0)
            org.jsoup.nodes.Element r1 = new org.jsoup.nodes.Element
            java.lang.String r2 = r6.g
            org.jsoup.nodes.Attributes r3 = r7.d
            r1.<init>(r0, r2, r3)
            r6.a(r1)
            boolean r2 = r7.c
            if (r2 == 0) goto L_0x004c
            org.jsoup.parser.am r1 = r6.d
            r1.b()
            boolean r1 = r0.isKnownTag()
            if (r1 != 0) goto L_0x0022
            r0.a()
            goto L_0x0022
        L_0x004c:
            org.jsoup.helper.DescendableLinkedList r0 = r6.f
            r0.add(r1)
            goto L_0x0022
        L_0x0052:
            org.jsoup.parser.ai r7 = (org.jsoup.parser.ai) r7
            java.lang.String r2 = r7.i()
            r1 = 0
            org.jsoup.helper.DescendableLinkedList r0 = r6.f
            java.util.Iterator r3 = r0.descendingIterator()
        L_0x005f:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0076
            java.lang.Object r0 = r3.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r4 = r0.nodeName()
            boolean r4 = r4.equals(r2)
            if (r4 == 0) goto L_0x005f
            r1 = r0
        L_0x0076:
            if (r1 == 0) goto L_0x0022
            org.jsoup.helper.DescendableLinkedList r0 = r6.f
            java.util.Iterator r2 = r0.descendingIterator()
        L_0x007e:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0022
            java.lang.Object r0 = r2.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            if (r0 != r1) goto L_0x0090
            r2.remove()
            goto L_0x0022
        L_0x0090:
            r2.remove()
            goto L_0x007e
        L_0x0094:
            org.jsoup.parser.af r7 = (org.jsoup.parser.af) r7
            org.jsoup.nodes.Comment r0 = new org.jsoup.nodes.Comment
            java.lang.StringBuilder r1 = r7.b
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = r6.g
            r0.<init>(r1, r2)
            boolean r1 = r7.c
            if (r1 == 0) goto L_0x00d5
            java.lang.String r2 = r0.getData()
            int r1 = r2.length()
            if (r1 <= r5) goto L_0x00d5
            java.lang.String r1 = "!"
            boolean r1 = r2.startsWith(r1)
            if (r1 != 0) goto L_0x00c1
            java.lang.String r1 = "?"
            boolean r1 = r2.startsWith(r1)
            if (r1 == 0) goto L_0x00d5
        L_0x00c1:
            java.lang.String r3 = r2.substring(r5)
            org.jsoup.nodes.XmlDeclaration r1 = new org.jsoup.nodes.XmlDeclaration
            java.lang.String r0 = r0.baseUri()
            java.lang.String r4 = "!"
            boolean r2 = r2.startsWith(r4)
            r1.<init>(r3, r0, r2)
            r0 = r1
        L_0x00d5:
            r6.a(r0)
            goto L_0x0022
        L_0x00da:
            org.jsoup.parser.ae r7 = (org.jsoup.parser.ae) r7
            org.jsoup.nodes.TextNode r0 = new org.jsoup.nodes.TextNode
            java.lang.String r1 = r7.g()
            java.lang.String r2 = r6.g
            r0.<init>(r1, r2)
            r6.a(r0)
            goto L_0x0022
        L_0x00ec:
            org.jsoup.parser.ag r7 = (org.jsoup.parser.ag) r7
            org.jsoup.nodes.DocumentType r0 = new org.jsoup.nodes.DocumentType
            java.lang.StringBuilder r1 = r7.b
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r2 = r7.c
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r3 = r7.d
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = r6.g
            r0.<init>(r1, r2, r3, r4)
            r6.a(r0)
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.XmlTreeBuilder.a(org.jsoup.parser.ad):boolean");
    }

    /* access modifiers changed from: protected */
    public final void b(String str, String str2, ac acVar) {
        super.b(str, str2, acVar);
        this.f.add(this.e);
        this.e.outputSettings().syntax(Document.OutputSettings.Syntax.xml);
    }

    /* access modifiers changed from: package-private */
    public final List c(String str, String str2, ac acVar) {
        b(str, str2, acVar);
        w();
        return this.e.childNodes();
    }
}
