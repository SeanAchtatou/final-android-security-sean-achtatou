package org.jsoup.parser;

import kotlin.text.Typography;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bc extends an {
    bc(String str) {
        super(str, 21, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        if (aVar.b()) {
            amVar.d(this);
            amVar.a(Data);
            return;
        }
        switch (aVar.c()) {
            case 0:
                amVar.c(this);
                aVar.f();
                amVar.a(65533);
                return;
            case '-':
                amVar.a('-');
                amVar.b(ScriptDataEscapedDash);
                return;
            case LockFreeTaskQueueCore.FROZEN_SHIFT /*60*/:
                amVar.b(ScriptDataEscapedLessthanSign);
                return;
            default:
                amVar.a(aVar.a('-', Typography.less, 0));
                return;
        }
    }
}
