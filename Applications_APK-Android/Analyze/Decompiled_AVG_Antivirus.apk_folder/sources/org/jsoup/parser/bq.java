package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import androidx.core.view.MotionEventCompat;
import kotlin.text.Typography;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bq extends an {
    bq(String str) {
        super(str, 34, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        amVar.b.c(aVar.a(9, 10, 13, 12, ' ', '/', '=', Typography.greater, 0, Typography.quote, '\'', Typography.less).toLowerCase());
        char d = aVar.d();
        switch (d) {
            case 0:
                amVar.c(this);
                amVar.b.b(65533);
                return;
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
                amVar.a(AfterAttributeName);
                return;
            case MotionEventCompat.AXIS_GENERIC_3 /*34*/:
            case MotionEventCompat.AXIS_GENERIC_8 /*39*/:
            case LockFreeTaskQueueCore.FROZEN_SHIFT /*60*/:
                amVar.c(this);
                amVar.b.b(d);
                return;
            case MotionEventCompat.AXIS_GENERIC_16 /*47*/:
                amVar.a(SelfClosingStartTag);
                return;
            case LockFreeTaskQueueCore.CLOSED_SHIFT /*61*/:
                amVar.a(BeforeAttributeValue);
                return;
            case '>':
                amVar.c();
                amVar.a(Data);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.d(this);
                amVar.a(Data);
                return;
            default:
                return;
        }
    }
}
