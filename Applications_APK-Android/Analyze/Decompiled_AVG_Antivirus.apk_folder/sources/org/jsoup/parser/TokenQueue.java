package org.jsoup.parser;

import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;

public class TokenQueue {
    private String a;
    private int b = 0;

    public TokenQueue(String str) {
        Validate.notNull(str);
        this.a = str;
    }

    private int a() {
        return this.a.length() - this.b;
    }

    public static String unescape(String str) {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        char c = 0;
        while (i < length) {
            char c2 = charArray[i];
            if (c2 != '\\') {
                sb.append(c2);
            } else if (c != 0 && c == '\\') {
                sb.append(c2);
            }
            i++;
            c = c2;
        }
        return sb.toString();
    }

    public void addFirst(Character ch) {
        addFirst(ch.toString());
    }

    public void addFirst(String str) {
        this.a = str + this.a.substring(this.b);
        this.b = 0;
    }

    public void advance() {
        if (!isEmpty()) {
            this.b++;
        }
    }

    public String chompBalanced(char c, char c2) {
        char c3 = 0;
        int i = 0;
        int i2 = -1;
        int i3 = -1;
        while (!isEmpty()) {
            Character valueOf = Character.valueOf(consume());
            if (c3 == 0 || c3 != '\\') {
                if (valueOf.equals(Character.valueOf(c))) {
                    i++;
                    if (i3 == -1) {
                        i3 = this.b;
                    }
                } else if (valueOf.equals(Character.valueOf(c2))) {
                    i--;
                }
            }
            if (i > 0 && c3 != 0) {
                i2 = this.b;
            }
            c3 = valueOf.charValue();
            if (i <= 0) {
                break;
            }
        }
        return i2 >= 0 ? this.a.substring(i3, i2) : "";
    }

    public String chompTo(String str) {
        String consumeTo = consumeTo(str);
        matchChomp(str);
        return consumeTo;
    }

    public String chompToIgnoreCase(String str) {
        String consumeToIgnoreCase = consumeToIgnoreCase(str);
        matchChomp(str);
        return consumeToIgnoreCase;
    }

    public char consume() {
        String str = this.a;
        int i = this.b;
        this.b = i + 1;
        return str.charAt(i);
    }

    public void consume(String str) {
        if (!matches(str)) {
            throw new IllegalStateException("Queue did not match expected sequence");
        }
        int length = str.length();
        if (length > a()) {
            throw new IllegalStateException("Queue not long enough to consume sequence");
        }
        this.b = length + this.b;
    }

    public String consumeAttributeKey() {
        int i = this.b;
        while (!isEmpty() && (matchesWord() || matchesAny('-', '_', ':'))) {
            this.b++;
        }
        return this.a.substring(i, this.b);
    }

    public String consumeCssIdentifier() {
        int i = this.b;
        while (!isEmpty() && (matchesWord() || matchesAny('-', '_'))) {
            this.b++;
        }
        return this.a.substring(i, this.b);
    }

    public String consumeElementSelector() {
        int i = this.b;
        while (!isEmpty() && (matchesWord() || matchesAny('|', '_', '-'))) {
            this.b++;
        }
        return this.a.substring(i, this.b);
    }

    public String consumeTagName() {
        int i = this.b;
        while (!isEmpty() && (matchesWord() || matchesAny(':', '_', '-'))) {
            this.b++;
        }
        return this.a.substring(i, this.b);
    }

    public String consumeTo(String str) {
        int indexOf = this.a.indexOf(str, this.b);
        if (indexOf == -1) {
            return remainder();
        }
        String substring = this.a.substring(this.b, indexOf);
        this.b += substring.length();
        return substring;
    }

    public String consumeToAny(String... strArr) {
        int i = this.b;
        while (!isEmpty() && !matchesAny(strArr)) {
            this.b++;
        }
        return this.a.substring(i, this.b);
    }

    public String consumeToIgnoreCase(String str) {
        int i = this.b;
        String substring = str.substring(0, 1);
        boolean equals = substring.toLowerCase().equals(substring.toUpperCase());
        while (!isEmpty() && !matches(str)) {
            if (equals) {
                int indexOf = this.a.indexOf(substring, this.b) - this.b;
                if (indexOf == 0) {
                    this.b++;
                } else if (indexOf < 0) {
                    this.b = this.a.length();
                } else {
                    this.b = indexOf + this.b;
                }
            } else {
                this.b++;
            }
        }
        return this.a.substring(i, this.b);
    }

    public boolean consumeWhitespace() {
        boolean z = false;
        while (matchesWhitespace()) {
            this.b++;
            z = true;
        }
        return z;
    }

    public String consumeWord() {
        int i = this.b;
        while (matchesWord()) {
            this.b++;
        }
        return this.a.substring(i, this.b);
    }

    public boolean isEmpty() {
        return a() == 0;
    }

    public boolean matchChomp(String str) {
        if (!matches(str)) {
            return false;
        }
        this.b += str.length();
        return true;
    }

    public boolean matches(String str) {
        return this.a.regionMatches(true, this.b, str, 0, str.length());
    }

    public boolean matchesAny(char... cArr) {
        if (isEmpty()) {
            return false;
        }
        for (char c : cArr) {
            if (this.a.charAt(this.b) == c) {
                return true;
            }
        }
        return false;
    }

    public boolean matchesAny(String... strArr) {
        for (String matches : strArr) {
            if (matches(matches)) {
                return true;
            }
        }
        return false;
    }

    public boolean matchesCS(String str) {
        return this.a.startsWith(str, this.b);
    }

    public boolean matchesStartTag() {
        return a() >= 2 && this.a.charAt(this.b) == '<' && Character.isLetter(this.a.charAt(this.b + 1));
    }

    public boolean matchesWhitespace() {
        return !isEmpty() && StringUtil.isWhitespace(this.a.charAt(this.b));
    }

    public boolean matchesWord() {
        return !isEmpty() && Character.isLetterOrDigit(this.a.charAt(this.b));
    }

    public char peek() {
        if (isEmpty()) {
            return 0;
        }
        return this.a.charAt(this.b);
    }

    public String remainder() {
        String substring = this.a.substring(this.b, this.a.length());
        this.b = this.a.length();
        return substring;
    }

    public String toString() {
        return this.a.substring(this.b);
    }
}
