package org.jsoup.parser;

import androidx.core.internal.view.SupportMenu;
import kotlin.text.Typography;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class bk extends an {
    bk(String str) {
        super(str, 2, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar, a aVar) {
        switch (aVar.c()) {
            case 0:
                amVar.c(this);
                aVar.f();
                amVar.a(65533);
                return;
            case '&':
                amVar.b(CharacterReferenceInRcdata);
                return;
            case LockFreeTaskQueueCore.FROZEN_SHIFT /*60*/:
                amVar.b(RcdataLessthanSign);
                return;
            case SupportMenu.USER_MASK /*65535*/:
                amVar.a(new ah());
                return;
            default:
                amVar.a(aVar.a(Typography.amp, Typography.less, 0));
                return;
        }
    }
}
