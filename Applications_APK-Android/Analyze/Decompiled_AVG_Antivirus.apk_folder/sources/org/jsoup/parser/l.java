package org.jsoup.parser;

import org.jsoup.helper.StringUtil;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class l extends c {
    l(String str) {
        super(str, 16, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (adVar.b()) {
            if (StringUtil.in(((aj) adVar).i(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                bVar.b(this);
                bVar.a(new ai("select"));
                return bVar.a(adVar);
            }
        }
        if (adVar.c()) {
            if (StringUtil.in(((ai) adVar).i(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                bVar.b(this);
                if (!bVar.h(((ai) adVar).i())) {
                    return false;
                }
                bVar.a(new ai("select"));
                return bVar.a(adVar);
            }
        }
        return bVar.a(adVar, p);
    }
}
