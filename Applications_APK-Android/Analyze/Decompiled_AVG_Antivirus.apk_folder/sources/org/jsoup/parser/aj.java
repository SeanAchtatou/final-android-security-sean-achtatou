package org.jsoup.parser;

import org.jsoup.nodes.Attributes;

final class aj extends ak {
    aj() {
        this.d = new Attributes();
        this.a = al.StartTag;
    }

    aj(String str) {
        this();
        this.b = str;
    }

    aj(String str, Attributes attributes) {
        this();
        this.b = str;
        this.d = attributes;
    }

    public final String toString() {
        return (this.d == null || this.d.size() <= 0) ? "<" + i() + ">" : "<" + i() + " " + this.d.toString() + ">";
    }
}
