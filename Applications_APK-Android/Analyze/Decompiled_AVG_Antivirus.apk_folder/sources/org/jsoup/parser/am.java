package org.jsoup.parser;

import kotlin.text.Typography;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Entities;

final class am {
    StringBuilder a;
    ak b;
    ag c;
    af d;
    private a e;
    private ac f;
    private an g = an.Data;
    private ad h;
    private boolean i = false;
    private StringBuilder j = new StringBuilder();
    private aj k;
    private boolean l = true;

    am(a aVar, ac acVar) {
        this.e = aVar;
        this.f = acVar;
    }

    private void b(String str) {
        if (this.f.a()) {
            this.f.add(new ParseError(this.e.a(), "Invalid character reference: %s", str));
        }
    }

    private void c(String str) {
        if (this.f.a()) {
            this.f.add(new ParseError(this.e.a(), str));
        }
    }

    /* access modifiers changed from: package-private */
    public final ad a() {
        if (!this.l) {
            c("Self closing flag not acknowledged");
            this.l = true;
        }
        while (!this.i) {
            this.g.a(this, this.e);
        }
        if (this.j.length() > 0) {
            String sb = this.j.toString();
            this.j.delete(0, this.j.length());
            return new ae(sb);
        }
        this.i = false;
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public final ak a(boolean z) {
        this.b = z ? new aj() : new ai();
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void a(char c2) {
        this.j.append(c2);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.j.append(str);
    }

    /* access modifiers changed from: package-private */
    public final void a(ad adVar) {
        Validate.isFalse(this.i, "There is an unread token pending!");
        this.h = adVar;
        this.i = true;
        if (adVar.a == al.StartTag) {
            aj ajVar = (aj) adVar;
            this.k = ajVar;
            if (ajVar.c) {
                this.l = false;
            }
        } else if (adVar.a == al.EndTag && ((ai) adVar).d != null) {
            c("Attributes incorrectly present on end tag");
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(an anVar) {
        this.g = anVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(char[] cArr) {
        this.j.append(cArr);
    }

    /* access modifiers changed from: package-private */
    public final char[] a(Character ch, boolean z) {
        int i2;
        if (this.e.b()) {
            return null;
        }
        if (ch != null && ch.charValue() == this.e.c()) {
            return null;
        }
        if (this.e.b(9, 10, 13, 12, ' ', Typography.less, Typography.amp)) {
            return null;
        }
        this.e.g();
        if (this.e.b("#")) {
            boolean c2 = this.e.c("X");
            String k2 = c2 ? this.e.k() : this.e.l();
            if (k2.length() == 0) {
                b("numeric reference with no numerals");
                this.e.h();
                return null;
            }
            if (!this.e.b(";")) {
                b("missing semicolon");
            }
            try {
                i2 = Integer.valueOf(k2, c2 ? 16 : 10).intValue();
            } catch (NumberFormatException e2) {
                i2 = -1;
            }
            if (i2 != -1 && ((i2 < 55296 || i2 > 57343) && i2 <= 1114111)) {
                return Character.toChars(i2);
            }
            b("character outside of valid range");
            return new char[]{65533};
        }
        String j2 = this.e.j();
        boolean b2 = this.e.b(';');
        if (!(Entities.isBaseNamedEntity(j2) || (Entities.isNamedEntity(j2) && b2))) {
            this.e.h();
            if (b2) {
                b(String.format("invalid named referenece '%s'", j2));
            }
            return null;
        } else if (!z || (!this.e.m() && !this.e.n() && !this.e.b('=', '-', '_'))) {
            if (!this.e.b(";")) {
                b("missing semicolon");
            }
            return new char[]{Entities.getCharacterByName(j2).charValue()};
        } else {
            this.e.h();
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final String b(boolean z) {
        StringBuilder sb = new StringBuilder();
        while (!this.e.b()) {
            sb.append(this.e.a((char) Typography.amp));
            if (this.e.b((char) Typography.amp)) {
                this.e.d();
                char[] a2 = a(null, z);
                if (a2 == null || a2.length == 0) {
                    sb.append((char) Typography.amp);
                } else {
                    sb.append(a2);
                }
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        this.l = true;
    }

    /* access modifiers changed from: package-private */
    public final void b(an anVar) {
        this.e.f();
        this.g = anVar;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.b.h();
        a(this.b);
    }

    /* access modifiers changed from: package-private */
    public final void c(an anVar) {
        if (this.f.a()) {
            this.f.add(new ParseError(this.e.a(), "Unexpected character '%s' in input state [%s]", Character.valueOf(this.e.c()), anVar));
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        a(this.d);
    }

    /* access modifiers changed from: package-private */
    public final void d(an anVar) {
        if (this.f.a()) {
            this.f.add(new ParseError(this.e.a(), "Unexpectedly reached end of file (EOF) in input state [%s]", anVar));
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        this.c = new ag();
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        a(this.c);
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        this.a = new StringBuilder();
    }

    /* access modifiers changed from: package-private */
    public final boolean h() {
        if (this.k == null) {
            return false;
        }
        return this.b.b.equals(this.k.b);
    }

    /* access modifiers changed from: package-private */
    public final String i() {
        if (this.k == null) {
            return null;
        }
        return this.k.b;
    }
}
