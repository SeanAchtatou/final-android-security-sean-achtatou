package org.jsoup.parser;

import org.jsoup.helper.StringUtil;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class u extends c {
    u(String str) {
        super(str, 2, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (c.a(adVar)) {
            return true;
        }
        if (adVar.d()) {
            bVar.a((af) adVar);
        } else if (adVar.a()) {
            bVar.b(this);
            return false;
        } else if (adVar.b() && ((aj) adVar).i().equals("html")) {
            return g.a(adVar, bVar);
        } else {
            if (!adVar.b() || !((aj) adVar).i().equals("head")) {
                if (adVar.c()) {
                    if (StringUtil.in(((ai) adVar).i(), "head", "body", "html", "br")) {
                        bVar.a((ad) new aj("head"));
                        return bVar.a(adVar);
                    }
                }
                if (adVar.c()) {
                    bVar.b(this);
                    return false;
                }
                bVar.a((ad) new aj("head"));
                return bVar.a(adVar);
            }
            bVar.f(bVar.a((aj) adVar));
            bVar.a(d);
        }
        return true;
    }
}
