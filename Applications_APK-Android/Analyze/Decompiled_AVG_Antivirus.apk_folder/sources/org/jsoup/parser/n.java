package org.jsoup.parser;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class n extends c {
    n(String str) {
        super(str, 18, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ad adVar, b bVar) {
        if (c.a(adVar)) {
            bVar.a((ae) adVar);
        } else if (adVar.d()) {
            bVar.a((af) adVar);
        } else if (adVar.a()) {
            bVar.b(this);
            return false;
        } else if (adVar.b()) {
            aj ajVar = (aj) adVar;
            String i = ajVar.i();
            if (i.equals("html")) {
                return bVar.a(ajVar, g);
            }
            if (i.equals("frameset")) {
                bVar.a(ajVar);
            } else if (i.equals("frame")) {
                bVar.b(ajVar);
            } else if (i.equals("noframes")) {
                return bVar.a(ajVar, d);
            } else {
                bVar.b(this);
                return false;
            }
        } else if (!adVar.c() || !((ai) adVar).i().equals("frameset")) {
            if (!adVar.f()) {
                bVar.b(this);
                return false;
            } else if (!bVar.x().nodeName().equals("html")) {
                bVar.b(this);
                return true;
            }
        } else if (bVar.x().nodeName().equals("html")) {
            bVar.b(this);
            return false;
        } else {
            bVar.h();
            if (!bVar.g() && !bVar.x().nodeName().equals("frameset")) {
                bVar.a(t);
            }
        }
        return true;
    }
}
