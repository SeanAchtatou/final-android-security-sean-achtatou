package org.jsoup.parser;

import java.util.Locale;
import kotlin.jvm.internal.CharCompanionObject;
import org.jsoup.helper.Validate;

final class a {
    private final char[] a;
    private final int b;
    private int c = 0;
    private int d = 0;

    a(String str) {
        Validate.notNull(str);
        this.a = str.toCharArray();
        this.b = this.a.length;
    }

    private int a(CharSequence charSequence) {
        char charAt = charSequence.charAt(0);
        int i = this.c;
        while (i < this.b) {
            if (charAt != this.a[i]) {
                do {
                    i++;
                    if (i >= this.b) {
                        break;
                    }
                } while (charAt != this.a[i]);
            }
            int i2 = i + 1;
            int length = (charSequence.length() + i2) - 1;
            if (i < this.b && length <= this.b) {
                int i3 = 1;
                while (i2 < length && charSequence.charAt(i3) == this.a[i2]) {
                    i2++;
                    i3++;
                }
                if (i2 == length) {
                    return i - this.c;
                }
            }
            i++;
        }
        return -1;
    }

    private String o() {
        String str = new String(this.a, this.c, this.b - this.c);
        this.c = this.b;
        return str;
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final String a(char c2) {
        int i;
        int i2 = this.c;
        while (true) {
            if (i2 >= this.b) {
                i = -1;
                break;
            } else if (c2 == this.a[i2]) {
                i = i2 - this.c;
                break;
            } else {
                i2++;
            }
        }
        if (i == -1) {
            return o();
        }
        String str = new String(this.a, this.c, i);
        this.c = i + this.c;
        return str;
    }

    /* access modifiers changed from: package-private */
    public final String a(String str) {
        int a2 = a((CharSequence) str);
        if (a2 == -1) {
            return o();
        }
        String str2 = new String(this.a, this.c, a2);
        this.c = a2 + this.c;
        return str2;
    }

    /* access modifiers changed from: package-private */
    public final String a(char... cArr) {
        int i = this.c;
        loop0:
        while (this.c < this.b) {
            for (char c2 : cArr) {
                if (this.a[this.c] == c2) {
                    break loop0;
                }
            }
            this.c++;
        }
        return this.c > i ? new String(this.a, i, this.c - i) : "";
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.c >= this.b;
    }

    /* access modifiers changed from: package-private */
    public final boolean b(char c2) {
        return !b() && this.a[this.c] == c2;
    }

    /* access modifiers changed from: package-private */
    public final boolean b(String str) {
        boolean z;
        int length = str.length();
        if (length <= this.b - this.c) {
            int i = 0;
            while (true) {
                if (i >= length) {
                    z = true;
                    break;
                } else if (str.charAt(i) != this.a[this.c + i]) {
                    z = false;
                    break;
                } else {
                    i++;
                }
            }
        } else {
            z = false;
        }
        if (!z) {
            return false;
        }
        this.c += str.length();
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean b(char... cArr) {
        if (b()) {
            return false;
        }
        char c2 = this.a[this.c];
        for (char c3 : cArr) {
            if (c3 == c2) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final char c() {
        return this.c >= this.b ? CharCompanionObject.MAX_VALUE : this.a[this.c];
    }

    /* access modifiers changed from: package-private */
    public final boolean c(String str) {
        boolean z;
        int length = str.length();
        if (length <= this.b - this.c) {
            int i = 0;
            while (true) {
                if (i >= length) {
                    z = true;
                    break;
                } else if (Character.toUpperCase(str.charAt(i)) != Character.toUpperCase(this.a[this.c + i])) {
                    z = false;
                    break;
                } else {
                    i++;
                }
            }
        } else {
            z = false;
        }
        if (!z) {
            return false;
        }
        this.c += str.length();
        return true;
    }

    /* access modifiers changed from: package-private */
    public final char d() {
        char c2 = this.c >= this.b ? CharCompanionObject.MAX_VALUE : this.a[this.c];
        this.c++;
        return c2;
    }

    /* access modifiers changed from: package-private */
    public final boolean d(String str) {
        return a(str.toLowerCase(Locale.ENGLISH)) >= 0 || a(str.toUpperCase(Locale.ENGLISH)) >= 0;
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        this.c--;
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.c++;
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        this.d = this.c;
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        this.c = this.d;
    }

    /* access modifiers changed from: package-private */
    public final String i() {
        int i = this.c;
        while (this.c < this.b && (((r1 = this.a[this.c]) >= 'A' && r1 <= 'Z') || (r1 >= 'a' && r1 <= 'z'))) {
            this.c++;
        }
        return new String(this.a, i, this.c - i);
    }

    /* access modifiers changed from: package-private */
    public final String j() {
        int i = this.c;
        while (this.c < this.b && (((r1 = this.a[this.c]) >= 'A' && r1 <= 'Z') || (r1 >= 'a' && r1 <= 'z'))) {
            this.c++;
        }
        while (!b() && (r1 = this.a[this.c]) >= '0' && r1 <= '9') {
            this.c++;
        }
        return new String(this.a, i, this.c - i);
    }

    /* access modifiers changed from: package-private */
    public final String k() {
        int i = this.c;
        while (this.c < this.b && (((r1 = this.a[this.c]) >= '0' && r1 <= '9') || ((r1 >= 'A' && r1 <= 'F') || (r1 >= 'a' && r1 <= 'f')))) {
            this.c++;
        }
        return new String(this.a, i, this.c - i);
    }

    /* access modifiers changed from: package-private */
    public final String l() {
        int i = this.c;
        while (this.c < this.b && (r1 = this.a[this.c]) >= '0' && r1 <= '9') {
            this.c++;
        }
        return new String(this.a, i, this.c - i);
    }

    /* access modifiers changed from: package-private */
    public final boolean m() {
        if (b()) {
            return false;
        }
        char c2 = this.a[this.c];
        return (c2 >= 'A' && c2 <= 'Z') || (c2 >= 'a' && c2 <= 'z');
    }

    /* access modifiers changed from: package-private */
    public final boolean n() {
        char c2;
        return !b() && (c2 = this.a[this.c]) >= '0' && c2 <= '9';
    }

    public final String toString() {
        return new String(this.a, this.c, this.b - this.c);
    }
}
