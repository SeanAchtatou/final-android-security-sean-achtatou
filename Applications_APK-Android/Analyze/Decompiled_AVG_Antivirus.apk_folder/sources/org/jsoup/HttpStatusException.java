package org.jsoup;

import java.io.IOException;

public class HttpStatusException extends IOException {
    private int a;
    private String b;

    public HttpStatusException(String str, int i, String str2) {
        super(str);
        this.a = i;
        this.b = str2;
    }

    public int getStatusCode() {
        return this.a;
    }

    public String getUrl() {
        return this.b;
    }

    public String toString() {
        return super.toString() + ". Status=" + this.a + ", URL=" + this.b;
    }
}
