package org.jsoup.select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;

public class Elements implements Cloneable, List {
    private List a;

    public Elements() {
        this.a = new ArrayList();
    }

    public Elements(int i) {
        this.a = new ArrayList(i);
    }

    public Elements(Collection collection) {
        this.a = new ArrayList(collection);
    }

    public Elements(List list) {
        this.a = list;
    }

    public Elements(Element... elementArr) {
        this(Arrays.asList(elementArr));
    }

    public void add(int i, Element element) {
        this.a.add(i, element);
    }

    public boolean add(Element element) {
        return this.a.add(element);
    }

    public boolean addAll(int i, Collection collection) {
        return this.a.addAll(i, collection);
    }

    public boolean addAll(Collection collection) {
        return this.a.addAll(collection);
    }

    public Elements addClass(String str) {
        for (Element addClass : this.a) {
            addClass.addClass(str);
        }
        return this;
    }

    public Elements after(String str) {
        for (Element after : this.a) {
            after.after(str);
        }
        return this;
    }

    public Elements append(String str) {
        for (Element append : this.a) {
            append.append(str);
        }
        return this;
    }

    public String attr(String str) {
        for (Element element : this.a) {
            if (element.hasAttr(str)) {
                return element.attr(str);
            }
        }
        return "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
      org.jsoup.nodes.Node.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
      org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element */
    public Elements attr(String str, String str2) {
        for (Element attr : this.a) {
            attr.attr(str, str2);
        }
        return this;
    }

    public Elements before(String str) {
        for (Element before : this.a) {
            before.before(str);
        }
        return this;
    }

    public void clear() {
        this.a.clear();
    }

    public Elements clone() {
        try {
            Elements elements = (Elements) super.clone();
            ArrayList arrayList = new ArrayList();
            elements.a = arrayList;
            for (Element clone : this.a) {
                arrayList.add(clone.clone());
            }
            return elements;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean contains(Object obj) {
        return this.a.contains(obj);
    }

    public boolean containsAll(Collection collection) {
        return this.a.containsAll(collection);
    }

    public Elements empty() {
        for (Element empty : this.a) {
            empty.empty();
        }
        return this;
    }

    public Elements eq(int i) {
        if (this.a.size() <= i) {
            return new Elements();
        }
        return new Elements(get(i));
    }

    public boolean equals(Object obj) {
        return this.a.equals(obj);
    }

    public Element first() {
        if (this.a.isEmpty()) {
            return null;
        }
        return (Element) this.a.get(0);
    }

    public List forms() {
        ArrayList arrayList = new ArrayList();
        for (Element element : this.a) {
            if (element instanceof FormElement) {
                arrayList.add((FormElement) element);
            }
        }
        return arrayList;
    }

    public Element get(int i) {
        return (Element) this.a.get(i);
    }

    public boolean hasAttr(String str) {
        for (Element hasAttr : this.a) {
            if (hasAttr.hasAttr(str)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasClass(String str) {
        for (Element hasClass : this.a) {
            if (hasClass.hasClass(str)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasText() {
        for (Element hasText : this.a) {
            if (hasText.hasText()) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String html() {
        StringBuilder sb = new StringBuilder();
        for (Element element : this.a) {
            if (sb.length() != 0) {
                sb.append("\n");
            }
            sb.append(element.html());
        }
        return sb.toString();
    }

    public Elements html(String str) {
        for (Element html : this.a) {
            html.html(str);
        }
        return this;
    }

    public int indexOf(Object obj) {
        return this.a.indexOf(obj);
    }

    public boolean is(String str) {
        return !select(str).isEmpty();
    }

    public boolean isEmpty() {
        return this.a.isEmpty();
    }

    public Iterator iterator() {
        return this.a.iterator();
    }

    public Element last() {
        if (this.a.isEmpty()) {
            return null;
        }
        return (Element) this.a.get(this.a.size() - 1);
    }

    public int lastIndexOf(Object obj) {
        return this.a.lastIndexOf(obj);
    }

    public ListIterator listIterator() {
        return this.a.listIterator();
    }

    public ListIterator listIterator(int i) {
        return this.a.listIterator(i);
    }

    public Elements not(String str) {
        return Selector.a(this, Selector.select(str, this));
    }

    public String outerHtml() {
        StringBuilder sb = new StringBuilder();
        for (Element element : this.a) {
            if (sb.length() != 0) {
                sb.append("\n");
            }
            sb.append(element.outerHtml());
        }
        return sb.toString();
    }

    public Elements parents() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (Element parents : this.a) {
            linkedHashSet.addAll(parents.parents());
        }
        return new Elements(linkedHashSet);
    }

    public Elements prepend(String str) {
        for (Element prepend : this.a) {
            prepend.prepend(str);
        }
        return this;
    }

    public Element remove(int i) {
        return (Element) this.a.remove(i);
    }

    public Elements remove() {
        for (Element remove : this.a) {
            remove.remove();
        }
        return this;
    }

    public boolean remove(Object obj) {
        return this.a.remove(obj);
    }

    public boolean removeAll(Collection collection) {
        return this.a.removeAll(collection);
    }

    public Elements removeAttr(String str) {
        for (Element removeAttr : this.a) {
            removeAttr.removeAttr(str);
        }
        return this;
    }

    public Elements removeClass(String str) {
        for (Element removeClass : this.a) {
            removeClass.removeClass(str);
        }
        return this;
    }

    public boolean retainAll(Collection collection) {
        return this.a.retainAll(collection);
    }

    public Elements select(String str) {
        return Selector.select(str, this);
    }

    public Element set(int i, Element element) {
        return (Element) this.a.set(i, element);
    }

    public int size() {
        return this.a.size();
    }

    public List subList(int i, int i2) {
        return this.a.subList(i, i2);
    }

    public Elements tagName(String str) {
        for (Element tagName : this.a) {
            tagName.tagName(str);
        }
        return this;
    }

    public String text() {
        StringBuilder sb = new StringBuilder();
        for (Element element : this.a) {
            if (sb.length() != 0) {
                sb.append(" ");
            }
            sb.append(element.text());
        }
        return sb.toString();
    }

    public Object[] toArray() {
        return this.a.toArray();
    }

    public Object[] toArray(Object[] objArr) {
        return this.a.toArray(objArr);
    }

    public String toString() {
        return outerHtml();
    }

    public Elements toggleClass(String str) {
        for (Element element : this.a) {
            element.toggleClass(str);
        }
        return this;
    }

    public Elements traverse(NodeVisitor nodeVisitor) {
        Validate.notNull(nodeVisitor);
        NodeTraversor nodeTraversor = new NodeTraversor(nodeVisitor);
        for (Element traverse : this.a) {
            nodeTraversor.traverse(traverse);
        }
        return this;
    }

    public Elements unwrap() {
        for (Element unwrap : this.a) {
            unwrap.unwrap();
        }
        return this;
    }

    public String val() {
        return size() > 0 ? first().val() : "";
    }

    public Elements val(String str) {
        for (Element val : this.a) {
            val.val(str);
        }
        return this;
    }

    public Elements wrap(String str) {
        Validate.notEmpty(str);
        for (Element wrap : this.a) {
            wrap.wrap(str);
        }
        return this;
    }
}
