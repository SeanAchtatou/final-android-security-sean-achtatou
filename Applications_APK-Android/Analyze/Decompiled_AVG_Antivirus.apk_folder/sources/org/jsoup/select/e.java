package org.jsoup.select;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.parser.TokenQueue;
import org.jsoup.select.Evaluator;
import org.jsoup.select.Selector;

final class e {
    private static final String[] a = {",", ">", "+", "~", " "};
    private static final String[] b = {"=", "!=", "^=", "$=", "*=", "~="};
    private static final Pattern f = Pattern.compile("((\\+|-)?(\\d+)?)n(\\s*(\\+|-)?\\s*\\d+)?", 2);
    private static final Pattern g = Pattern.compile("(\\+|-)?(\\d+)");
    private TokenQueue c;
    private String d;
    private List e = new ArrayList();

    private e(String str) {
        this.d = str;
        this.c = new TokenQueue(str);
    }

    public static Evaluator a(String str) {
        e eVar = new e(str);
        eVar.c.consumeWhitespace();
        if (eVar.c.matchesAny(a)) {
            eVar.e.add(new m());
            eVar.a(eVar.c.consume());
        } else {
            eVar.a();
        }
        while (!eVar.c.isEmpty()) {
            boolean consumeWhitespace = eVar.c.consumeWhitespace();
            if (eVar.c.matchesAny(a)) {
                eVar.a(eVar.c.consume());
            } else if (consumeWhitespace) {
                eVar.a(' ');
            } else {
                eVar.a();
            }
        }
        return eVar.e.size() == 1 ? (Evaluator) eVar.e.get(0) : new c(eVar.e);
    }

    private void a() {
        if (this.c.matchChomp("#")) {
            String consumeCssIdentifier = this.c.consumeCssIdentifier();
            Validate.notEmpty(consumeCssIdentifier);
            this.e.add(new Evaluator.Id(consumeCssIdentifier));
        } else if (this.c.matchChomp(".")) {
            String consumeCssIdentifier2 = this.c.consumeCssIdentifier();
            Validate.notEmpty(consumeCssIdentifier2);
            this.e.add(new Evaluator.Class(consumeCssIdentifier2.trim().toLowerCase()));
        } else if (this.c.matchesWord()) {
            String consumeElementSelector = this.c.consumeElementSelector();
            Validate.notEmpty(consumeElementSelector);
            if (consumeElementSelector.contains("|")) {
                consumeElementSelector = consumeElementSelector.replace("|", ":");
            }
            this.e.add(new Evaluator.Tag(consumeElementSelector.trim().toLowerCase()));
        } else if (this.c.matches("[")) {
            TokenQueue tokenQueue = new TokenQueue(this.c.chompBalanced('[', ']'));
            String consumeToAny = tokenQueue.consumeToAny(b);
            Validate.notEmpty(consumeToAny);
            tokenQueue.consumeWhitespace();
            if (tokenQueue.isEmpty()) {
                if (consumeToAny.startsWith("^")) {
                    this.e.add(new Evaluator.AttributeStarting(consumeToAny.substring(1)));
                } else {
                    this.e.add(new Evaluator.Attribute(consumeToAny));
                }
            } else if (tokenQueue.matchChomp("=")) {
                this.e.add(new Evaluator.AttributeWithValue(consumeToAny, tokenQueue.remainder()));
            } else if (tokenQueue.matchChomp("!=")) {
                this.e.add(new Evaluator.AttributeWithValueNot(consumeToAny, tokenQueue.remainder()));
            } else if (tokenQueue.matchChomp("^=")) {
                this.e.add(new Evaluator.AttributeWithValueStarting(consumeToAny, tokenQueue.remainder()));
            } else if (tokenQueue.matchChomp("$=")) {
                this.e.add(new Evaluator.AttributeWithValueEnding(consumeToAny, tokenQueue.remainder()));
            } else if (tokenQueue.matchChomp("*=")) {
                this.e.add(new Evaluator.AttributeWithValueContaining(consumeToAny, tokenQueue.remainder()));
            } else if (tokenQueue.matchChomp("~=")) {
                this.e.add(new Evaluator.AttributeWithValueMatching(consumeToAny, Pattern.compile(tokenQueue.remainder())));
            } else {
                throw new Selector.SelectorParseException("Could not parse attribute query '%s': unexpected token at '%s'", this.d, tokenQueue.remainder());
            }
        } else if (this.c.matchChomp("*")) {
            this.e.add(new Evaluator.AllElements());
        } else if (this.c.matchChomp(":lt(")) {
            this.e.add(new Evaluator.IndexLessThan(b()));
        } else if (this.c.matchChomp(":gt(")) {
            this.e.add(new Evaluator.IndexGreaterThan(b()));
        } else if (this.c.matchChomp(":eq(")) {
            this.e.add(new Evaluator.IndexEquals(b()));
        } else if (this.c.matches(":has(")) {
            this.c.consume(":has");
            String chompBalanced = this.c.chompBalanced('(', ')');
            Validate.notEmpty(chompBalanced, ":has(el) subselect must not be empty");
            this.e.add(new g(a(chompBalanced)));
        } else if (this.c.matches(":contains(")) {
            a(false);
        } else if (this.c.matches(":containsOwn(")) {
            a(true);
        } else if (this.c.matches(":matches(")) {
            b(false);
        } else if (this.c.matches(":matchesOwn(")) {
            b(true);
        } else if (this.c.matches(":not(")) {
            this.c.consume(":not");
            String chompBalanced2 = this.c.chompBalanced('(', ')');
            Validate.notEmpty(chompBalanced2, ":not(selector) subselect must not be empty");
            this.e.add(new j(a(chompBalanced2)));
        } else if (this.c.matchChomp(":nth-child(")) {
            a(false, false);
        } else if (this.c.matchChomp(":nth-last-child(")) {
            a(true, false);
        } else if (this.c.matchChomp(":nth-of-type(")) {
            a(false, true);
        } else if (this.c.matchChomp(":nth-last-of-type(")) {
            a(true, true);
        } else if (this.c.matchChomp(":first-child")) {
            this.e.add(new Evaluator.IsFirstChild());
        } else if (this.c.matchChomp(":last-child")) {
            this.e.add(new Evaluator.IsLastChild());
        } else if (this.c.matchChomp(":first-of-type")) {
            this.e.add(new Evaluator.IsFirstOfType());
        } else if (this.c.matchChomp(":last-of-type")) {
            this.e.add(new Evaluator.IsLastOfType());
        } else if (this.c.matchChomp(":only-child")) {
            this.e.add(new Evaluator.IsOnlyChild());
        } else if (this.c.matchChomp(":only-of-type")) {
            this.e.add(new Evaluator.IsOnlyOfType());
        } else if (this.c.matchChomp(":empty")) {
            this.e.add(new Evaluator.IsEmpty());
        } else if (this.c.matchChomp(":root")) {
            this.e.add(new Evaluator.IsRoot());
        } else {
            throw new Selector.SelectorParseException("Could not parse query '%s': unexpected token at '%s'", this.d, this.c.remainder());
        }
    }

    private void a(char c2) {
        Evaluator cVar;
        boolean z;
        Evaluator evaluator;
        d dVar;
        c cVar2;
        this.c.consumeWhitespace();
        StringBuilder sb = new StringBuilder();
        while (!this.c.isEmpty()) {
            if (!this.c.matches("(")) {
                if (!this.c.matches("[")) {
                    if (this.c.matchesAny(a)) {
                        break;
                    }
                    sb.append(this.c.consume());
                } else {
                    sb.append("[").append(this.c.chompBalanced('[', ']')).append("]");
                }
            } else {
                sb.append("(").append(this.c.chompBalanced('(', ')')).append(")");
            }
        }
        Evaluator a2 = a(sb.toString());
        if (this.e.size() == 1) {
            cVar = (Evaluator) this.e.get(0);
            if (!(cVar instanceof d) || c2 == ',') {
                z = false;
                evaluator = cVar;
            } else {
                d dVar2 = (d) cVar;
                z = true;
                Evaluator evaluator2 = dVar2.b > 0 ? (Evaluator) dVar2.a.get(dVar2.b - 1) : null;
                evaluator = cVar;
                cVar = evaluator2;
            }
        } else {
            cVar = new c(this.e);
            z = false;
            evaluator = cVar;
        }
        this.e.clear();
        if (c2 == '>') {
            cVar2 = new c(a2, new h(cVar));
        } else if (c2 == ' ') {
            cVar2 = new c(a2, new k(cVar));
        } else if (c2 == '+') {
            cVar2 = new c(a2, new i(cVar));
        } else if (c2 == '~') {
            cVar2 = new c(a2, new l(cVar));
        } else if (c2 == ',') {
            if (cVar instanceof d) {
                dVar = (d) cVar;
                dVar.a(a2);
            } else {
                d dVar3 = new d();
                dVar3.a(cVar);
                dVar3.a(a2);
                dVar = dVar3;
            }
            cVar2 = dVar;
        } else {
            throw new Selector.SelectorParseException("Unknown combinator: " + c2, new Object[0]);
        }
        if (z) {
            d dVar4 = (d) evaluator;
            dVar4.a.set(dVar4.b - 1, cVar2);
        } else {
            evaluator = cVar2;
        }
        this.e.add(evaluator);
    }

    private void a(boolean z) {
        this.c.consume(z ? ":containsOwn" : ":contains");
        String unescape = TokenQueue.unescape(this.c.chompBalanced('(', ')'));
        Validate.notEmpty(unescape, ":contains(text) query must not be empty");
        if (z) {
            this.e.add(new Evaluator.ContainsOwnText(unescape));
        } else {
            this.e.add(new Evaluator.ContainsText(unescape));
        }
    }

    private void a(boolean z, boolean z2) {
        int i = 1;
        int i2 = 0;
        String lowerCase = this.c.chompTo(")").trim().toLowerCase();
        Matcher matcher = f.matcher(lowerCase);
        Matcher matcher2 = g.matcher(lowerCase);
        if ("odd".equals(lowerCase)) {
            i2 = 1;
            i = 2;
        } else if ("even".equals(lowerCase)) {
            i = 2;
        } else if (matcher.matches()) {
            if (matcher.group(3) != null) {
                i = Integer.parseInt(matcher.group(1).replaceFirst("^\\+", ""));
            }
            if (matcher.group(4) != null) {
                i2 = Integer.parseInt(matcher.group(4).replaceFirst("^\\+", ""));
            }
        } else if (matcher2.matches()) {
            i = 0;
            i2 = Integer.parseInt(matcher2.group().replaceFirst("^\\+", ""));
        } else {
            throw new Selector.SelectorParseException("Could not parse nth-index '%s': unexpected format", lowerCase);
        }
        if (z2) {
            if (z) {
                this.e.add(new Evaluator.IsNthLastOfType(i, i2));
            } else {
                this.e.add(new Evaluator.IsNthOfType(i, i2));
            }
        } else if (z) {
            this.e.add(new Evaluator.IsNthLastChild(i, i2));
        } else {
            this.e.add(new Evaluator.IsNthChild(i, i2));
        }
    }

    private int b() {
        String trim = this.c.chompTo(")").trim();
        Validate.isTrue(StringUtil.isNumeric(trim), "Index must be numeric");
        return Integer.parseInt(trim);
    }

    private void b(boolean z) {
        this.c.consume(z ? ":matchesOwn" : ":matches");
        String chompBalanced = this.c.chompBalanced('(', ')');
        Validate.notEmpty(chompBalanced, ":matches(regex) query must not be empty");
        if (z) {
            this.e.add(new Evaluator.MatchesOwn(Pattern.compile(chompBalanced)));
        } else {
            this.e.add(new Evaluator.Matches(Pattern.compile(chompBalanced)));
        }
    }
}
