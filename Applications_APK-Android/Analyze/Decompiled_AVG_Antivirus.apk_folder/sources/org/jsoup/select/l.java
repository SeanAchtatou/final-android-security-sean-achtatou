package org.jsoup.select;

import org.jsoup.nodes.Element;

final class l extends f {
    public l(Evaluator evaluator) {
        this.a = evaluator;
    }

    public final boolean matches(Element element, Element element2) {
        if (element == element2) {
            return false;
        }
        for (Element previousElementSibling = element2.previousElementSibling(); previousElementSibling != null; previousElementSibling = previousElementSibling.previousElementSibling()) {
            if (this.a.matches(element, previousElementSibling)) {
                return true;
            }
        }
        return false;
    }

    public final String toString() {
        return String.format(":prev*%s", this.a);
    }
}
