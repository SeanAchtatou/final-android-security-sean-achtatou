package org.jsoup.select;

import org.jsoup.nodes.Element;

final class h extends f {
    public h(Evaluator evaluator) {
        this.a = evaluator;
    }

    public final boolean matches(Element element, Element element2) {
        Element parent;
        return (element == element2 || (parent = element2.parent()) == null || !this.a.matches(element, parent)) ? false : true;
    }

    public final String toString() {
        return String.format(":ImmediateParent%s", this.a);
    }
}
