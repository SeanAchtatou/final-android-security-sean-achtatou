package org.jsoup.select;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;

public class Selector {
    private final Evaluator a;
    private final Element b;

    public class SelectorParseException extends IllegalStateException {
        public SelectorParseException(String str, Object... objArr) {
            super(String.format(str, objArr));
        }
    }

    private Selector(String str, Element element) {
        Validate.notNull(str);
        String trim = str.trim();
        Validate.notEmpty(trim);
        Validate.notNull(element);
        this.a = e.a(trim);
        this.b = element;
    }

    static Elements a(Collection collection, Collection collection2) {
        boolean z;
        Elements elements = new Elements();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Element element = (Element) it.next();
            Iterator it2 = collection2.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (element.equals((Element) it2.next())) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                elements.add(element);
            }
        }
        return elements;
    }

    public static Elements select(String str, Iterable iterable) {
        Validate.notEmpty(str);
        Validate.notNull(iterable);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            linkedHashSet.addAll(select(str, (Element) it.next()));
        }
        return new Elements(linkedHashSet);
    }

    public static Elements select(String str, Element element) {
        Selector selector = new Selector(str, element);
        return Collector.collect(selector.a, selector.b);
    }
}
