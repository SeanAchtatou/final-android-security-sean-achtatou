package org.jsoup.select;

import org.jsoup.nodes.Element;

final class d extends b {
    d() {
    }

    public final void a(Evaluator evaluator) {
        this.a.add(evaluator);
        a();
    }

    public final boolean matches(Element element, Element element2) {
        for (int i = 0; i < this.b; i++) {
            if (((Evaluator) this.a.get(i)).matches(element, element2)) {
                return true;
            }
        }
        return false;
    }

    public final String toString() {
        return String.format(":or%s", this.a);
    }
}
