package org.jsoup.select;

import org.jsoup.nodes.Element;

final class i extends f {
    public i(Evaluator evaluator) {
        this.a = evaluator;
    }

    public final boolean matches(Element element, Element element2) {
        Element previousElementSibling;
        return (element == element2 || (previousElementSibling = element2.previousElementSibling()) == null || !this.a.matches(element, previousElementSibling)) ? false : true;
    }

    public final String toString() {
        return String.format(":prev%s", this.a);
    }
}
