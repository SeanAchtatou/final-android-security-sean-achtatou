package org.jsoup.select;

import org.jsoup.nodes.Element;

final class k extends f {
    public k(Evaluator evaluator) {
        this.a = evaluator;
    }

    public final boolean matches(Element element, Element element2) {
        if (element == element2) {
            return false;
        }
        for (Element parent = element2.parent(); parent != element; parent = parent.parent()) {
            if (this.a.matches(element, parent)) {
                return true;
            }
        }
        return false;
    }

    public final String toString() {
        return String.format(":parent%s", this.a);
    }
}
