package org.jsoup.select;

import java.util.ArrayList;
import java.util.Collection;

abstract class b extends Evaluator {
    final ArrayList a;
    int b;

    b() {
        this.b = 0;
        this.a = new ArrayList();
    }

    b(Collection collection) {
        this();
        this.a.addAll(collection);
        a();
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.b = this.a.size();
    }
}
