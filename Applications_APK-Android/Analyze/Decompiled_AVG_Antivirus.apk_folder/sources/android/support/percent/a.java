package android.support.percent;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.ao;
import android.support.v4.view.bx;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public final class a {
    private final ViewGroup a;

    public a(ViewGroup viewGroup) {
        this.a = viewGroup;
    }

    public static b a(Context context, AttributeSet attributeSet) {
        b bVar = null;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, e.a);
        float fraction = obtainStyledAttributes.getFraction(e.j, 1, 1, -1.0f);
        if (fraction != -1.0f) {
            if (Log.isLoggable("PercentLayout", 2)) {
                Log.v("PercentLayout", "percent width: " + fraction);
            }
            bVar = new b();
            bVar.a = fraction;
        }
        float fraction2 = obtainStyledAttributes.getFraction(e.b, 1, 1, -1.0f);
        if (fraction2 != -1.0f) {
            if (Log.isLoggable("PercentLayout", 2)) {
                Log.v("PercentLayout", "percent height: " + fraction2);
            }
            if (bVar == null) {
                bVar = new b();
            }
            bVar.b = fraction2;
        }
        float fraction3 = obtainStyledAttributes.getFraction(e.f, 1, 1, -1.0f);
        if (fraction3 != -1.0f) {
            if (Log.isLoggable("PercentLayout", 2)) {
                Log.v("PercentLayout", "percent margin: " + fraction3);
            }
            if (bVar == null) {
                bVar = new b();
            }
            bVar.c = fraction3;
            bVar.d = fraction3;
            bVar.e = fraction3;
            bVar.f = fraction3;
        }
        float fraction4 = obtainStyledAttributes.getFraction(e.e, 1, 1, -1.0f);
        if (fraction4 != -1.0f) {
            if (Log.isLoggable("PercentLayout", 2)) {
                Log.v("PercentLayout", "percent left margin: " + fraction4);
            }
            if (bVar == null) {
                bVar = new b();
            }
            bVar.c = fraction4;
        }
        float fraction5 = obtainStyledAttributes.getFraction(e.i, 1, 1, -1.0f);
        if (fraction5 != -1.0f) {
            if (Log.isLoggable("PercentLayout", 2)) {
                Log.v("PercentLayout", "percent top margin: " + fraction5);
            }
            if (bVar == null) {
                bVar = new b();
            }
            bVar.d = fraction5;
        }
        float fraction6 = obtainStyledAttributes.getFraction(e.g, 1, 1, -1.0f);
        if (fraction6 != -1.0f) {
            if (Log.isLoggable("PercentLayout", 2)) {
                Log.v("PercentLayout", "percent right margin: " + fraction6);
            }
            if (bVar == null) {
                bVar = new b();
            }
            bVar.e = fraction6;
        }
        float fraction7 = obtainStyledAttributes.getFraction(e.c, 1, 1, -1.0f);
        if (fraction7 != -1.0f) {
            if (Log.isLoggable("PercentLayout", 2)) {
                Log.v("PercentLayout", "percent bottom margin: " + fraction7);
            }
            if (bVar == null) {
                bVar = new b();
            }
            bVar.f = fraction7;
        }
        float fraction8 = obtainStyledAttributes.getFraction(e.h, 1, 1, -1.0f);
        if (fraction8 != -1.0f) {
            if (Log.isLoggable("PercentLayout", 2)) {
                Log.v("PercentLayout", "percent start margin: " + fraction8);
            }
            if (bVar == null) {
                bVar = new b();
            }
            bVar.g = fraction8;
        }
        float fraction9 = obtainStyledAttributes.getFraction(e.d, 1, 1, -1.0f);
        if (fraction9 != -1.0f) {
            if (Log.isLoggable("PercentLayout", 2)) {
                Log.v("PercentLayout", "percent end margin: " + fraction9);
            }
            if (bVar == null) {
                bVar = new b();
            }
            bVar.h = fraction9;
        }
        obtainStyledAttributes.recycle();
        if (Log.isLoggable("PercentLayout", 3)) {
            Log.d("PercentLayout", "constructed: " + bVar);
        }
        return bVar;
    }

    public static void a(ViewGroup.LayoutParams layoutParams, TypedArray typedArray, int i, int i2) {
        layoutParams.width = typedArray.getLayoutDimension(i, 0);
        layoutParams.height = typedArray.getLayoutDimension(i2, 0);
    }

    public final void a() {
        int childCount = this.a.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.a.getChildAt(i);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if (Log.isLoggable("PercentLayout", 3)) {
                Log.d("PercentLayout", "should restore " + childAt + " " + layoutParams);
            }
            if (layoutParams instanceof c) {
                b a2 = ((c) layoutParams).a();
                if (Log.isLoggable("PercentLayout", 3)) {
                    Log.d("PercentLayout", "using " + a2);
                }
                if (a2 != null) {
                    if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                        a2.a(marginLayoutParams);
                        marginLayoutParams.leftMargin = a2.i.leftMargin;
                        marginLayoutParams.topMargin = a2.i.topMargin;
                        marginLayoutParams.rightMargin = a2.i.rightMargin;
                        marginLayoutParams.bottomMargin = a2.i.bottomMargin;
                        ao.a(marginLayoutParams, ao.a(a2.i));
                        ao.b(marginLayoutParams, ao.b(a2.i));
                    } else {
                        a2.a(layoutParams);
                    }
                }
            }
        }
    }

    public final void a(int i, int i2) {
        if (Log.isLoggable("PercentLayout", 3)) {
            Log.d("PercentLayout", "adjustChildren: " + this.a + " widthMeasureSpec: " + View.MeasureSpec.toString(i) + " heightMeasureSpec: " + View.MeasureSpec.toString(i2));
        }
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        int childCount = this.a.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.a.getChildAt(i3);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if (Log.isLoggable("PercentLayout", 3)) {
                Log.d("PercentLayout", "should adjust " + childAt + " " + layoutParams);
            }
            if (layoutParams instanceof c) {
                b a2 = ((c) layoutParams).a();
                if (Log.isLoggable("PercentLayout", 3)) {
                    Log.d("PercentLayout", "using " + a2);
                }
                if (a2 != null) {
                    if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                        a2.a(marginLayoutParams, size, size2);
                        a2.i.leftMargin = marginLayoutParams.leftMargin;
                        a2.i.topMargin = marginLayoutParams.topMargin;
                        a2.i.rightMargin = marginLayoutParams.rightMargin;
                        a2.i.bottomMargin = marginLayoutParams.bottomMargin;
                        ao.a(a2.i, ao.a(marginLayoutParams));
                        ao.b(a2.i, ao.b(marginLayoutParams));
                        if (a2.c >= 0.0f) {
                            marginLayoutParams.leftMargin = (int) (((float) size) * a2.c);
                        }
                        if (a2.d >= 0.0f) {
                            marginLayoutParams.topMargin = (int) (((float) size2) * a2.d);
                        }
                        if (a2.e >= 0.0f) {
                            marginLayoutParams.rightMargin = (int) (((float) size) * a2.e);
                        }
                        if (a2.f >= 0.0f) {
                            marginLayoutParams.bottomMargin = (int) (((float) size2) * a2.f);
                        }
                        if (a2.g >= 0.0f) {
                            ao.a(marginLayoutParams, (int) (((float) size) * a2.g));
                        }
                        if (a2.h >= 0.0f) {
                            ao.b(marginLayoutParams, (int) (a2.h * ((float) size)));
                        }
                        if (Log.isLoggable("PercentLayout", 3)) {
                            Log.d("PercentLayout", "after fillMarginLayoutParams: (" + marginLayoutParams.width + ", " + marginLayoutParams.height + ")");
                        }
                    } else {
                        a2.a(layoutParams, size, size2);
                    }
                }
            }
        }
    }

    public final boolean b() {
        b a2;
        boolean z;
        int childCount = this.a.getChildCount();
        boolean z2 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = this.a.getChildAt(i);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if (Log.isLoggable("PercentLayout", 3)) {
                Log.d("PercentLayout", "should handle measured state too small " + childAt + " " + layoutParams);
            }
            if ((layoutParams instanceof c) && (a2 = ((c) layoutParams).a()) != null) {
                if ((bx.k(childAt) & -16777216) == 16777216 && a2.a >= 0.0f && a2.i.width == -2) {
                    layoutParams.width = -2;
                    z = true;
                } else {
                    z = z2;
                }
                if ((bx.l(childAt) & -16777216) == 16777216 && a2.b >= 0.0f && a2.i.height == -2) {
                    layoutParams.height = -2;
                    z2 = true;
                } else {
                    z2 = z;
                }
            }
        }
        if (Log.isLoggable("PercentLayout", 3)) {
            Log.d("PercentLayout", "should trigger second measure pass: " + z2);
        }
        return z2;
    }
}
