package android.support.percent;

import android.util.Log;
import android.view.ViewGroup;

public final class b {
    public float a = -1.0f;
    public float b = -1.0f;
    public float c = -1.0f;
    public float d = -1.0f;
    public float e = -1.0f;
    public float f = -1.0f;
    public float g = -1.0f;
    public float h = -1.0f;
    final ViewGroup.MarginLayoutParams i = new ViewGroup.MarginLayoutParams(0, 0);

    public final void a(ViewGroup.LayoutParams layoutParams) {
        layoutParams.width = this.i.width;
        layoutParams.height = this.i.height;
    }

    public final void a(ViewGroup.LayoutParams layoutParams, int i2, int i3) {
        this.i.width = layoutParams.width;
        this.i.height = layoutParams.height;
        if (this.a >= 0.0f) {
            layoutParams.width = (int) (((float) i2) * this.a);
        }
        if (this.b >= 0.0f) {
            layoutParams.height = (int) (((float) i3) * this.b);
        }
        if (Log.isLoggable("PercentLayout", 3)) {
            Log.d("PercentLayout", "after fillLayoutParams: (" + layoutParams.width + ", " + layoutParams.height + ")");
        }
    }

    public final String toString() {
        return String.format("PercentLayoutInformation width: %f height %f, margins (%f, %f,  %f, %f, %f, %f)", Float.valueOf(this.a), Float.valueOf(this.b), Float.valueOf(this.c), Float.valueOf(this.d), Float.valueOf(this.e), Float.valueOf(this.f), Float.valueOf(this.g), Float.valueOf(this.h));
    }
}
