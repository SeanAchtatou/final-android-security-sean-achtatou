package android.support.v4.content;

import android.support.v4.e.d;
import java.io.PrintWriter;

public final class i {
    int a;
    k b;
    j c;
    boolean d;
    boolean e;
    boolean f;
    boolean g;
    boolean h;

    public final void a() {
        this.d = true;
        this.f = false;
        this.e = false;
    }

    public final void a(int i, k kVar) {
        if (this.b != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.b = kVar;
        this.a = i;
    }

    public final void a(j jVar) {
        if (this.c != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.c = jVar;
    }

    public final void a(k kVar) {
        if (this.b == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.b != kVar) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.b = null;
        }
    }

    public final void a(String str, PrintWriter printWriter) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.a);
        printWriter.print(" mListener=");
        printWriter.println(this.b);
        if (this.d || this.g || this.h) {
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.d);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.g);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.h);
        }
        if (this.e || this.f) {
            printWriter.print(str);
            printWriter.print("mAbandoned=");
            printWriter.print(this.e);
            printWriter.print(" mReset=");
            printWriter.println(this.f);
        }
    }

    public final void b() {
        this.d = false;
    }

    public final void b(j jVar) {
        if (this.c == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.c != jVar) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.c = null;
        }
    }

    public final void c() {
        this.f = true;
        this.d = false;
        this.e = false;
        this.g = false;
        this.h = false;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(64);
        d.a(this, sb);
        sb.append(" id=");
        sb.append(this.a);
        sb.append("}");
        return sb.toString();
    }
}
