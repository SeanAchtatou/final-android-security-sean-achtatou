package android.support.v4.os;

import android.os.Build;
import android.os.Parcelable;

public final class d {
    public static Parcelable.Creator a(f fVar) {
        return Build.VERSION.SDK_INT >= 13 ? new g(fVar) : new e(fVar);
    }
}
