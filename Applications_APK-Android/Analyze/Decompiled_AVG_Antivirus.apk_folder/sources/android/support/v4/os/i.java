package android.support.v4.os;

import android.os.Bundle;

final class i extends b {
    final /* synthetic */ C0020ResultReceiver a;

    i(C0020ResultReceiver resultReceiver) {
        this.a = resultReceiver;
    }

    public final void a(int i, Bundle bundle) {
        if (this.a.b != null) {
            this.a.b.post(new j(this.a, i, bundle));
        } else {
            this.a.a(i, bundle);
        }
    }
}
