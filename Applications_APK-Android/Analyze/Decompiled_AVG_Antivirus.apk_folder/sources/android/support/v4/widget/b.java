package android.support.v4.widget;

import android.view.animation.AnimationUtils;

final class b {
    private int a;
    private int b;
    private float c;
    private float d;
    private long e = Long.MIN_VALUE;
    private long f = 0;
    private int g = 0;
    private int h = 0;
    private long i = -1;
    private float j;
    private int k;

    private float a(long j2) {
        if (j2 < this.e) {
            return 0.0f;
        }
        if (this.i < 0 || j2 < this.i) {
            return a.a(((float) (j2 - this.e)) / ((float) this.a), 0.0f, 1.0f) * 0.5f;
        }
        return (a.a(((float) (j2 - this.i)) / ((float) this.k), 0.0f, 1.0f) * this.j) + (1.0f - this.j);
    }

    public final void a() {
        this.a = 500;
    }

    public final void a(float f2, float f3) {
        this.c = f2;
        this.d = f3;
    }

    public final void b() {
        this.b = 500;
    }

    public final void c() {
        this.e = AnimationUtils.currentAnimationTimeMillis();
        this.i = -1;
        this.f = this.e;
        this.j = 0.5f;
        this.g = 0;
        this.h = 0;
    }

    public final void d() {
        long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
        this.k = a.a((int) (currentAnimationTimeMillis - this.e), this.b);
        this.j = a(currentAnimationTimeMillis);
        this.i = currentAnimationTimeMillis;
    }

    public final boolean e() {
        return this.i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.i + ((long) this.k);
    }

    public final void f() {
        if (this.f == 0) {
            throw new RuntimeException("Cannot compute scroll delta before calling start()");
        }
        long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
        float a2 = a(currentAnimationTimeMillis);
        float f2 = (a2 * 4.0f) + (-4.0f * a2 * a2);
        long j2 = currentAnimationTimeMillis - this.f;
        this.f = currentAnimationTimeMillis;
        this.g = (int) (((float) j2) * f2 * this.c);
        this.h = (int) (((float) j2) * f2 * this.d);
    }

    public final int g() {
        return (int) (this.c / Math.abs(this.c));
    }

    public final int h() {
        return (int) (this.d / Math.abs(this.d));
    }

    public final int i() {
        return this.h;
    }
}
