package android.support.v4.widget;

import android.os.Build;
import android.view.View;
import android.widget.PopupWindow;

public final class al {
    static final ar a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            a = new an();
        } else if (i >= 21) {
            a = new am();
        } else if (i >= 19) {
            a = new aq();
        } else if (i >= 9) {
            a = new ap();
        } else {
            a = new ao();
        }
    }

    public static void a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
        a.a(popupWindow, view, i, i2, i3);
    }
}
