package android.support.v4.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.widget.NestedScrollView;

final class ak implements Parcelable.Creator {
    ak() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new NestedScrollView.SavedState(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new NestedScrollView.SavedState[i];
    }
}
