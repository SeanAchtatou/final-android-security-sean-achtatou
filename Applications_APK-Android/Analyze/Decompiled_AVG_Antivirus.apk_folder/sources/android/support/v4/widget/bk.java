package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class bk extends Animation {
    final /* synthetic */ SwipeRefreshLayout a;

    bk(SwipeRefreshLayout swipeRefreshLayout) {
        this.a = swipeRefreshLayout;
    }

    public final void applyTransformation(float f, Transformation transformation) {
        this.a.a((((int) (((float) ((!this.a.L ? (int) (this.a.H - ((float) Math.abs(this.a.b))) : (int) this.a.H) - this.a.a)) * f)) + this.a.a) - this.a.z.getTop(), false);
        this.a.C.a(1.0f - f);
    }
}
