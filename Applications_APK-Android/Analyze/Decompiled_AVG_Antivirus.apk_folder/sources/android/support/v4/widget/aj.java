package android.support.v4.widget;

import android.os.Bundle;
import android.support.v4.view.a;
import android.support.v4.view.a.af;
import android.support.v4.view.a.f;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ScrollView;

final class aj extends a {
    aj() {
    }

    public final void a(View view, f fVar) {
        int a;
        super.a(view, fVar);
        NestedScrollView nestedScrollView = (NestedScrollView) view;
        fVar.b((CharSequence) ScrollView.class.getName());
        if (nestedScrollView.isEnabled() && (a = nestedScrollView.c()) > 0) {
            fVar.i(true);
            if (nestedScrollView.getScrollY() > 0) {
                fVar.a(8192);
            }
            if (nestedScrollView.getScrollY() < a) {
                fVar.a(4096);
            }
        }
    }

    public final void a(View view, AccessibilityEvent accessibilityEvent) {
        super.a(view, accessibilityEvent);
        NestedScrollView nestedScrollView = (NestedScrollView) view;
        accessibilityEvent.setClassName(ScrollView.class.getName());
        af a = android.support.v4.view.a.a.a(accessibilityEvent);
        a.a(nestedScrollView.c() > 0);
        a.d(nestedScrollView.getScrollX());
        a.e(nestedScrollView.getScrollY());
        a.f(nestedScrollView.getScrollX());
        a.g(nestedScrollView.c());
    }

    public final boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        NestedScrollView nestedScrollView = (NestedScrollView) view;
        if (!nestedScrollView.isEnabled()) {
            return false;
        }
        switch (i) {
            case 4096:
                int min = Math.min(((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()) + nestedScrollView.getScrollY(), nestedScrollView.c());
                if (min == nestedScrollView.getScrollY()) {
                    return false;
                }
                nestedScrollView.a(min);
                return true;
            case 8192:
                int max = Math.max(nestedScrollView.getScrollY() - ((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()), 0);
                if (max == nestedScrollView.getScrollY()) {
                    return false;
                }
                nestedScrollView.a(max);
                return true;
            default:
                return false;
        }
    }
}
