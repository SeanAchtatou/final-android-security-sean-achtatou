package android.support.v4.widget;

import android.graphics.Paint;
import android.support.v4.view.bx;
import android.view.View;

final class az implements Runnable {
    final View a;
    final /* synthetic */ SlidingPaneLayout b;

    az(SlidingPaneLayout slidingPaneLayout, View view) {
        this.b = slidingPaneLayout;
        this.a = view;
    }

    public final void run() {
        if (this.a.getParent() == this.b) {
            bx.a(this.a, 0, (Paint) null);
            this.b.c(this.a);
        }
        this.b.t.remove(this);
    }
}
