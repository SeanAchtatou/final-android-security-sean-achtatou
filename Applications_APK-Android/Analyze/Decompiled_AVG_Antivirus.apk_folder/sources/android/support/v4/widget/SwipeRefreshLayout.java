package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.ay;
import android.support.v4.view.be;
import android.support.v4.view.bf;
import android.support.v4.view.bg;
import android.support.v4.view.bh;
import android.support.v4.view.bx;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;

public class SwipeRefreshLayout extends ViewGroup implements be, bg {
    private static final String c = SwipeRefreshLayout.class.getSimpleName();
    private static final int[] y = {16842766};
    private int A = -1;
    /* access modifiers changed from: private */
    public float B;
    /* access modifiers changed from: private */
    public ae C;
    private Animation D;
    private Animation E;
    private Animation F;
    private Animation G;
    /* access modifiers changed from: private */
    public float H;
    /* access modifiers changed from: private */
    public boolean I;
    private int J;
    private int K;
    /* access modifiers changed from: private */
    public boolean L;
    private Animation.AnimationListener M = new bg(this);
    private final Animation N = new bk(this);
    private final Animation O = new bl(this);
    protected int a;
    protected int b;
    private View d;
    /* access modifiers changed from: private */
    public bn e;
    /* access modifiers changed from: private */
    public boolean f = false;
    private int g;
    private float h = -1.0f;
    private float i;
    private final bh j;
    private final bf k;
    private final int[] l = new int[2];
    private final int[] m = new int[2];
    private boolean n;
    private int o;
    /* access modifiers changed from: private */
    public int p;
    private boolean q = false;
    private float r;
    private float s;
    private boolean t;
    private int u = -1;
    /* access modifiers changed from: private */
    public boolean v;
    private boolean w;
    private final DecelerateInterpolator x;
    /* access modifiers changed from: private */
    public d z;

    public SwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g = ViewConfiguration.get(context).getScaledTouchSlop();
        this.o = getResources().getInteger(17694721);
        setWillNotDraw(false);
        this.x = new DecelerateInterpolator(2.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, y);
        setEnabled(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.J = (int) (displayMetrics.density * 40.0f);
        this.K = (int) (displayMetrics.density * 40.0f);
        this.z = new d(getContext());
        this.C = new ae(getContext(), this);
        this.C.a();
        this.z.setImageDrawable(this.C);
        this.z.setVisibility(8);
        addView(this.z);
        bx.a((ViewGroup) this);
        this.H = displayMetrics.density * 64.0f;
        this.h = this.H;
        this.j = new bh(this);
        this.k = new bf(this);
        setNestedScrollingEnabled(true);
    }

    private static float a(MotionEvent motionEvent, int i2) {
        int a2 = ay.a(motionEvent, i2);
        if (a2 < 0) {
            return -1.0f;
        }
        return ay.d(motionEvent, a2);
    }

    private Animation a(int i2, int i3) {
        if (this.v && a()) {
            return null;
        }
        bi biVar = new bi(this, i2, i3);
        biVar.setDuration(300);
        this.z.a((Animation.AnimationListener) null);
        this.z.clearAnimation();
        this.z.startAnimation(biVar);
        return biVar;
    }

    /* access modifiers changed from: private */
    public void a(float f2) {
        if (a()) {
            a((int) (255.0f * f2));
            return;
        }
        bx.d(this.z, f2);
        bx.e(this.z, f2);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.z.getBackground().setAlpha(i2);
        this.C.setAlpha(i2);
    }

    /* access modifiers changed from: private */
    public void a(int i2, boolean z2) {
        this.z.bringToFront();
        this.z.offsetTopAndBottom(i2);
        this.p = this.z.getTop();
        if (z2 && Build.VERSION.SDK_INT < 11) {
            invalidate();
        }
    }

    private void a(MotionEvent motionEvent) {
        int b2 = ay.b(motionEvent);
        if (ay.b(motionEvent, b2) == this.u) {
            this.u = ay.b(motionEvent, b2 == 0 ? 1 : 0);
        }
    }

    /* access modifiers changed from: private */
    public void a(Animation.AnimationListener animationListener) {
        this.D = new bh(this);
        this.D.setDuration(150);
        this.z.a(animationListener);
        this.z.clearAnimation();
        this.z.startAnimation(this.D);
    }

    private static boolean a() {
        return Build.VERSION.SDK_INT < 11;
    }

    private static boolean a(Animation animation) {
        return animation != null && animation.hasStarted() && !animation.hasEnded();
    }

    private void b() {
        if (this.d == null) {
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                View childAt = getChildAt(i2);
                if (!childAt.equals(this.z)) {
                    this.d = childAt;
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.d(android.view.View, float):void
     arg types: [android.support.v4.widget.d, int]
     candidates:
      android.support.v4.view.bx.d(android.view.View, int):void
      android.support.v4.view.bx.d(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.e(android.view.View, float):void
     arg types: [android.support.v4.widget.d, int]
     candidates:
      android.support.v4.view.bx.e(android.view.View, int):void
      android.support.v4.view.bx.e(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(android.view.MotionEvent, int):float
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, int):int
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    private void b(float f2) {
        this.C.a(true);
        float min = Math.min(1.0f, Math.abs(f2 / this.h));
        float max = (((float) Math.max(((double) min) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
        float abs = Math.abs(f2) - this.h;
        float f3 = this.L ? this.H - ((float) this.b) : this.H;
        float max2 = Math.max(0.0f, Math.min(abs, f3 * 2.0f) / f3);
        float pow = ((float) (((double) (max2 / 4.0f)) - Math.pow((double) (max2 / 4.0f), 2.0d))) * 2.0f;
        int i2 = ((int) ((f3 * min) + (f3 * pow * 2.0f))) + this.b;
        if (this.z.getVisibility() != 0) {
            this.z.setVisibility(0);
        }
        if (!this.v) {
            bx.d((View) this.z, 1.0f);
            bx.e((View) this.z, 1.0f);
        }
        if (f2 < this.h) {
            if (this.v) {
                a(f2 / this.h);
            }
            if (this.C.getAlpha() > 76 && !a(this.E)) {
                this.E = a(this.C.getAlpha(), 76);
            }
            this.C.b(Math.min(0.8f, max * 0.8f));
            this.C.a(Math.min(1.0f, max));
        } else if (this.C.getAlpha() < 255 && !a(this.F)) {
            this.F = a(this.C.getAlpha(), 255);
        }
        this.C.c((-0.25f + (max * 0.4f) + (pow * 2.0f)) * 0.5f);
        a(i2 - this.p, true);
    }

    private void c(float f2) {
        if (f2 <= this.h) {
            this.f = false;
            this.C.b(0.0f);
            bj bjVar = null;
            if (!this.v) {
                bjVar = new bj(this);
            }
            int i2 = this.p;
            if (this.v) {
                this.a = i2;
                if (a()) {
                    this.B = (float) this.C.getAlpha();
                } else {
                    this.B = bx.u(this.z);
                }
                this.G = new bm(this);
                this.G.setDuration(150);
                if (bjVar != null) {
                    this.z.a(bjVar);
                }
                this.z.clearAnimation();
                this.z.startAnimation(this.G);
            } else {
                this.a = i2;
                this.O.reset();
                this.O.setDuration(200);
                this.O.setInterpolator(this.x);
                if (bjVar != null) {
                    this.z.a(bjVar);
                }
                this.z.clearAnimation();
                this.z.startAnimation(this.O);
            }
            this.C.a(false);
        } else if (!this.f) {
            this.I = true;
            b();
            this.f = true;
            if (this.f) {
                int i3 = this.p;
                Animation.AnimationListener animationListener = this.M;
                this.a = i3;
                this.N.reset();
                this.N.setDuration(200);
                this.N.setInterpolator(this.x);
                if (animationListener != null) {
                    this.z.a(animationListener);
                }
                this.z.clearAnimation();
                this.z.startAnimation(this.N);
                return;
            }
            a(this.M);
        }
    }

    private boolean c() {
        if (Build.VERSION.SDK_INT >= 14) {
            return bx.b(this.d, -1);
        }
        if (!(this.d instanceof AbsListView)) {
            return bx.b(this.d, -1) || this.d.getScrollY() > 0;
        }
        AbsListView absListView = (AbsListView) this.d;
        return absListView.getChildCount() > 0 && (absListView.getFirstVisiblePosition() > 0 || absListView.getChildAt(0).getTop() < absListView.getPaddingTop());
    }

    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return this.k.a(f2, f3, z2);
    }

    public boolean dispatchNestedPreFling(float f2, float f3) {
        return this.k.a(f2, f3);
    }

    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return this.k.a(i2, i3, iArr, iArr2);
    }

    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return this.k.a(i2, i3, i4, i5, iArr);
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        return this.A < 0 ? i3 : i3 == i2 + -1 ? this.A : i3 >= this.A ? i3 + 1 : i3;
    }

    public int getNestedScrollAxes() {
        return this.j.a();
    }

    public boolean hasNestedScrollingParent() {
        return this.k.b();
    }

    public boolean isNestedScrollingEnabled() {
        return this.k.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(android.view.MotionEvent, int):float
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, int):int
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        b();
        int a2 = ay.a(motionEvent);
        if (this.w && a2 == 0) {
            this.w = false;
        }
        if (!isEnabled() || this.w || c() || this.f || this.n) {
            return false;
        }
        switch (a2) {
            case 0:
                a(this.b - this.z.getTop(), true);
                this.u = ay.b(motionEvent, 0);
                this.t = false;
                float a3 = a(motionEvent, this.u);
                if (a3 != -1.0f) {
                    this.s = a3;
                    break;
                } else {
                    return false;
                }
            case 1:
            case 3:
                this.t = false;
                this.u = -1;
                break;
            case 2:
                if (this.u == -1) {
                    Log.e(c, "Got ACTION_MOVE event but don't have an active pointer id.");
                    return false;
                }
                float a4 = a(motionEvent, this.u);
                if (a4 != -1.0f) {
                    if (a4 - this.s > ((float) this.g) && !this.t) {
                        this.r = this.s + ((float) this.g);
                        this.t = true;
                        this.C.setAlpha(76);
                        break;
                    }
                } else {
                    return false;
                }
            case 6:
                a(motionEvent);
                break;
        }
        return this.t;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() != 0) {
            if (this.d == null) {
                b();
            }
            if (this.d != null) {
                View view = this.d;
                int paddingLeft = getPaddingLeft();
                int paddingTop = getPaddingTop();
                view.layout(paddingLeft, paddingTop, ((measuredWidth - getPaddingLeft()) - getPaddingRight()) + paddingLeft, ((measuredHeight - getPaddingTop()) - getPaddingBottom()) + paddingTop);
                int measuredWidth2 = this.z.getMeasuredWidth();
                this.z.layout((measuredWidth / 2) - (measuredWidth2 / 2), this.p, (measuredWidth / 2) + (measuredWidth2 / 2), this.p + this.z.getMeasuredHeight());
            }
        }
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.d == null) {
            b();
        }
        if (this.d != null) {
            this.d.measure(View.MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
            this.z.measure(View.MeasureSpec.makeMeasureSpec(this.J, 1073741824), View.MeasureSpec.makeMeasureSpec(this.K, 1073741824));
            if (!this.L && !this.q) {
                this.q = true;
                int i4 = -this.z.getMeasuredHeight();
                this.b = i4;
                this.p = i4;
            }
            this.A = -1;
            for (int i5 = 0; i5 < getChildCount(); i5++) {
                if (getChildAt(i5) == this.z) {
                    this.A = i5;
                    return;
                }
            }
        }
    }

    public boolean onNestedFling(View view, float f2, float f3, boolean z2) {
        return dispatchNestedFling(f2, f3, z2);
    }

    public boolean onNestedPreFling(View view, float f2, float f3) {
        return dispatchNestedPreFling(f2, f3);
    }

    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
        if (i3 > 0 && this.i > 0.0f) {
            if (((float) i3) > this.i) {
                iArr[1] = i3 - ((int) this.i);
                this.i = 0.0f;
            } else {
                this.i -= (float) i3;
                iArr[1] = i3;
            }
            b(this.i);
        }
        if (this.L && i3 > 0 && this.i == 0.0f && Math.abs(i3 - iArr[1]) > 0) {
            this.z.setVisibility(8);
        }
        int[] iArr2 = this.l;
        if (dispatchNestedPreScroll(i2 - iArr[0], i3 - iArr[1], iArr2, null)) {
            iArr[0] = iArr[0] + iArr2[0];
            iArr[1] = iArr2[1] + iArr[1];
        }
    }

    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        dispatchNestedScroll(i2, i3, i4, i5, this.m);
        int i6 = this.m[1] + i5;
        if (i6 < 0) {
            this.i = ((float) Math.abs(i6)) + this.i;
            b(this.i);
        }
    }

    public void onNestedScrollAccepted(View view, View view2, int i2) {
        this.j.a(i2);
        startNestedScroll(i2 & 2);
        this.i = 0.0f;
        this.n = true;
    }

    public boolean onStartNestedScroll(View view, View view2, int i2) {
        return isEnabled() && !this.w && !this.f && (i2 & 2) != 0;
    }

    public void onStopNestedScroll(View view) {
        this.j.b();
        this.n = false;
        if (this.i > 0.0f) {
            c(this.i);
            this.i = 0.0f;
        }
        stopNestedScroll();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a2 = ay.a(motionEvent);
        if (this.w && a2 == 0) {
            this.w = false;
        }
        if (!isEnabled() || this.w || c() || this.n) {
            return false;
        }
        switch (a2) {
            case 0:
                this.u = ay.b(motionEvent, 0);
                this.t = false;
                break;
            case 1:
                int a3 = ay.a(motionEvent, this.u);
                if (a3 < 0) {
                    Log.e(c, "Got ACTION_UP event but don't have an active pointer id.");
                    return false;
                }
                this.t = false;
                c((ay.d(motionEvent, a3) - this.r) * 0.5f);
                this.u = -1;
                return false;
            case 2:
                int a4 = ay.a(motionEvent, this.u);
                if (a4 < 0) {
                    Log.e(c, "Got ACTION_MOVE event but have an invalid active pointer id.");
                    return false;
                }
                float d2 = (ay.d(motionEvent, a4) - this.r) * 0.5f;
                if (this.t) {
                    if (d2 > 0.0f) {
                        b(d2);
                        break;
                    } else {
                        return false;
                    }
                }
                break;
            case 3:
                return false;
            case 5:
                int b2 = ay.b(motionEvent);
                if (b2 >= 0) {
                    this.u = ay.b(motionEvent, b2);
                    break;
                } else {
                    Log.e(c, "Got ACTION_POINTER_DOWN event but have an invalid action index.");
                    return false;
                }
            case 6:
                a(motionEvent);
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        if (Build.VERSION.SDK_INT < 21 && (this.d instanceof AbsListView)) {
            return;
        }
        if (this.d == null || bx.D(this.d)) {
            super.requestDisallowInterceptTouchEvent(z2);
        }
    }

    public void setNestedScrollingEnabled(boolean z2) {
        this.k.a(z2);
    }

    public boolean startNestedScroll(int i2) {
        return this.k.a(i2);
    }

    public void stopNestedScroll() {
        this.k.c();
    }
}
