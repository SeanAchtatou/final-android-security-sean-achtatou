package android.support.v4.widget;

import android.graphics.drawable.Drawable;
import android.widget.TextView;

class bq extends bs {
    bq() {
    }

    public void a(TextView textView, Drawable drawable) {
        boolean z = textView.getLayoutDirection() == 1;
        Drawable drawable2 = z ? null : drawable;
        if (!z) {
            drawable = null;
        }
        textView.setCompoundDrawables(drawable2, null, drawable, null);
    }
}
