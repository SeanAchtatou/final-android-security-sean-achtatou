package android.support.v4.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

final class ai {
    private final RectF a = new RectF();
    private final Paint b = new Paint();
    private final Paint c = new Paint();
    private final Drawable.Callback d;
    private float e = 0.0f;
    private float f = 0.0f;
    private float g = 0.0f;
    private float h = 5.0f;
    private float i = 2.5f;
    private int[] j;
    private int k;
    private float l;
    private float m;
    private float n;
    private boolean o;
    private Path p;
    private float q;
    private double r;
    private int s;
    private int t;
    private int u;
    private final Paint v = new Paint(1);
    private int w;
    private int x;

    public ai(Drawable.Callback callback) {
        this.d = callback;
        this.b.setStrokeCap(Paint.Cap.SQUARE);
        this.b.setAntiAlias(true);
        this.b.setStyle(Paint.Style.STROKE);
        this.c.setStyle(Paint.Style.FILL);
        this.c.setAntiAlias(true);
    }

    private int o() {
        return (this.k + 1) % this.j.length;
    }

    private void p() {
        this.d.invalidateDrawable(null);
    }

    public final void a() {
        this.w = -328966;
    }

    public final void a(double d2) {
        this.r = d2;
    }

    public final void a(float f2) {
        this.h = f2;
        this.b.setStrokeWidth(f2);
        p();
    }

    public final void a(float f2, float f3) {
        this.s = (int) f2;
        this.t = (int) f3;
    }

    public final void a(int i2) {
        this.x = i2;
    }

    public final void a(int i2, int i3) {
        float min = (float) Math.min(i2, i3);
        this.i = (this.r <= 0.0d || min < 0.0f) ? (float) Math.ceil((double) (this.h / 2.0f)) : (float) (((double) (min / 2.0f)) - this.r);
    }

    public final void a(Canvas canvas, Rect rect) {
        RectF rectF = this.a;
        rectF.set(rect);
        rectF.inset(this.i, this.i);
        float f2 = (this.e + this.g) * 360.0f;
        float f3 = ((this.f + this.g) * 360.0f) - f2;
        this.b.setColor(this.x);
        canvas.drawArc(rectF, f2, f3, false, this.b);
        if (this.o) {
            if (this.p == null) {
                this.p = new Path();
                this.p.setFillType(Path.FillType.EVEN_ODD);
            } else {
                this.p.reset();
            }
            float f4 = ((float) (((int) this.i) / 2)) * this.q;
            float cos = (float) ((this.r * Math.cos(0.0d)) + ((double) rect.exactCenterX()));
            this.p.moveTo(0.0f, 0.0f);
            this.p.lineTo(((float) this.s) * this.q, 0.0f);
            this.p.lineTo((((float) this.s) * this.q) / 2.0f, ((float) this.t) * this.q);
            this.p.offset(cos - f4, (float) ((this.r * Math.sin(0.0d)) + ((double) rect.exactCenterY())));
            this.p.close();
            this.c.setColor(this.x);
            canvas.rotate((f2 + f3) - 5.0f, rect.exactCenterX(), rect.exactCenterY());
            canvas.drawPath(this.p, this.c);
        }
        if (this.u < 255) {
            this.v.setColor(this.w);
            this.v.setAlpha(255 - this.u);
            canvas.drawCircle(rect.exactCenterX(), rect.exactCenterY(), (float) (rect.width() / 2), this.v);
        }
    }

    public final void a(ColorFilter colorFilter) {
        this.b.setColorFilter(colorFilter);
        p();
    }

    public final void a(boolean z) {
        if (this.o != z) {
            this.o = z;
            p();
        }
    }

    public final void a(int[] iArr) {
        this.j = iArr;
        b(0);
    }

    public final int b() {
        return this.j[o()];
    }

    public final void b(float f2) {
        this.e = f2;
        p();
    }

    public final void b(int i2) {
        this.k = i2;
        this.x = this.j[this.k];
    }

    public final void c() {
        b(o());
    }

    public final void c(float f2) {
        this.f = f2;
        p();
    }

    public final void c(int i2) {
        this.u = i2;
    }

    public final int d() {
        return this.u;
    }

    public final void d(float f2) {
        this.g = f2;
        p();
    }

    public final float e() {
        return this.h;
    }

    public final void e(float f2) {
        if (f2 != this.q) {
            this.q = f2;
            p();
        }
    }

    public final float f() {
        return this.e;
    }

    public final float g() {
        return this.l;
    }

    public final float h() {
        return this.m;
    }

    public final int i() {
        return this.j[this.k];
    }

    public final float j() {
        return this.f;
    }

    public final double k() {
        return this.r;
    }

    public final float l() {
        return this.n;
    }

    public final void m() {
        this.l = this.e;
        this.m = this.f;
        this.n = this.g;
    }

    public final void n() {
        this.l = 0.0f;
        this.m = 0.0f;
        this.n = 0.0f;
        b(0.0f);
        c(0.0f);
        d(0.0f);
    }
}
