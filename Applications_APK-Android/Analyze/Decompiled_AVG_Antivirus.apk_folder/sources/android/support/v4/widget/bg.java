package android.support.v4.widget;

import android.view.animation.Animation;

final class bg implements Animation.AnimationListener {
    final /* synthetic */ SwipeRefreshLayout a;

    bg(SwipeRefreshLayout swipeRefreshLayout) {
        this.a = swipeRefreshLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void
     arg types: [android.support.v4.widget.SwipeRefreshLayout, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(android.view.MotionEvent, int):float
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, int):int
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void */
    public final void onAnimationEnd(Animation animation) {
        if (this.a.f) {
            this.a.C.setAlpha(255);
            this.a.C.start();
            if (this.a.I && this.a.e != null) {
                bn unused = this.a.e;
            }
        } else {
            this.a.C.stop();
            this.a.z.setVisibility(8);
            this.a.a(255);
            if (this.a.v) {
                this.a.a(0.0f);
            } else {
                this.a.a(this.a.b - this.a.p, true);
            }
        }
        int unused2 = this.a.p = this.a.z.getTop();
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
