package android.support.v4.widget;

import android.content.Context;
import android.support.v4.view.ay;
import android.support.v4.view.bt;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import java.util.Arrays;

public final class bu {
    private static final Interpolator v = new bv();
    private int a;
    private int b;
    private int c = -1;
    private float[] d;
    private float[] e;
    private float[] f;
    private float[] g;
    private int[] h;
    private int[] i;
    private int[] j;
    private int k;
    private VelocityTracker l;
    private float m;
    private float n;
    private int o;
    private int p;
    private at q;
    private final bx r;
    private View s;
    private boolean t;
    private final ViewGroup u;
    private final Runnable w = new bw(this);

    private bu(Context context, ViewGroup viewGroup, bx bxVar) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Parent view may not be null");
        } else if (bxVar == null) {
            throw new IllegalArgumentException("Callback may not be null");
        } else {
            this.u = viewGroup;
            this.r = bxVar;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.o = (int) ((context.getResources().getDisplayMetrics().density * 20.0f) + 0.5f);
            this.b = viewConfiguration.getScaledTouchSlop();
            this.m = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.n = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.q = at.a(context, v);
        }
    }

    private static float a(float f2, float f3, float f4) {
        float abs = Math.abs(f2);
        if (abs < f3) {
            return 0.0f;
        }
        return abs > f4 ? f2 <= 0.0f ? -f4 : f4 : f2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private int a(int i2, int i3, int i4) {
        if (i2 == 0) {
            return 0;
        }
        int width = this.u.getWidth();
        int i5 = width / 2;
        float sin = (((float) Math.sin((double) ((float) (((double) (Math.min(1.0f, ((float) Math.abs(i2)) / ((float) width)) - 0.5f)) * 0.4712389167638204d)))) * ((float) i5)) + ((float) i5);
        int abs = Math.abs(i3);
        return Math.min(abs > 0 ? Math.round(Math.abs(sin / ((float) abs)) * 1000.0f) * 4 : (int) (((((float) Math.abs(i2)) / ((float) i4)) + 1.0f) * 256.0f), 600);
    }

    public static bu a(ViewGroup viewGroup, float f2, bx bxVar) {
        bu a2 = a(viewGroup, bxVar);
        a2.b = (int) (((float) a2.b) * (1.0f / f2));
        return a2;
    }

    public static bu a(ViewGroup viewGroup, bx bxVar) {
        return new bu(viewGroup.getContext(), viewGroup, bxVar);
    }

    private void a(float f2, float f3, int i2) {
        int i3 = 0;
        if (this.d == null || this.d.length <= i2) {
            float[] fArr = new float[(i2 + 1)];
            float[] fArr2 = new float[(i2 + 1)];
            float[] fArr3 = new float[(i2 + 1)];
            float[] fArr4 = new float[(i2 + 1)];
            int[] iArr = new int[(i2 + 1)];
            int[] iArr2 = new int[(i2 + 1)];
            int[] iArr3 = new int[(i2 + 1)];
            if (this.d != null) {
                System.arraycopy(this.d, 0, fArr, 0, this.d.length);
                System.arraycopy(this.e, 0, fArr2, 0, this.e.length);
                System.arraycopy(this.f, 0, fArr3, 0, this.f.length);
                System.arraycopy(this.g, 0, fArr4, 0, this.g.length);
                System.arraycopy(this.h, 0, iArr, 0, this.h.length);
                System.arraycopy(this.i, 0, iArr2, 0, this.i.length);
                System.arraycopy(this.j, 0, iArr3, 0, this.j.length);
            }
            this.d = fArr;
            this.e = fArr2;
            this.f = fArr3;
            this.g = fArr4;
            this.h = iArr;
            this.i = iArr2;
            this.j = iArr3;
        }
        float[] fArr5 = this.d;
        this.f[i2] = f2;
        fArr5[i2] = f2;
        float[] fArr6 = this.e;
        this.g[i2] = f3;
        fArr6[i2] = f3;
        int[] iArr4 = this.h;
        int i4 = (int) f2;
        int i5 = (int) f3;
        if (i4 < this.u.getLeft() + this.o) {
            i3 = 1;
        }
        if (i5 < this.u.getTop() + this.o) {
            i3 |= 4;
        }
        if (i4 > this.u.getRight() - this.o) {
            i3 |= 2;
        }
        if (i5 > this.u.getBottom() - this.o) {
            i3 |= 8;
        }
        iArr4[i2] = i3;
        this.k |= 1 << i2;
    }

    private boolean a(float f2, float f3, int i2, int i3) {
        float abs = Math.abs(f2);
        float abs2 = Math.abs(f3);
        if ((this.h[i2] & i3) != i3 || (this.p & i3) == 0 || (this.j[i2] & i3) == i3 || (this.i[i2] & i3) == i3) {
            return false;
        }
        return (abs > ((float) this.b) || abs2 > ((float) this.b)) && (this.i[i2] & i3) == 0 && abs > ((float) this.b);
    }

    private boolean a(int i2, int i3, int i4, int i5) {
        int left = this.s.getLeft();
        int top = this.s.getTop();
        int i6 = i2 - left;
        int i7 = i3 - top;
        if (i6 == 0 && i7 == 0) {
            this.q.h();
            b(0);
            return false;
        }
        View view = this.s;
        int b2 = b(i4, (int) this.n, (int) this.m);
        int b3 = b(i5, (int) this.n, (int) this.m);
        int abs = Math.abs(i6);
        int abs2 = Math.abs(i7);
        int abs3 = Math.abs(b2);
        int abs4 = Math.abs(b3);
        int i8 = abs3 + abs4;
        int i9 = abs + abs2;
        this.q.a(left, top, i6, i7, (int) (((b3 != 0 ? ((float) abs4) / ((float) i8) : ((float) abs2) / ((float) i9)) * ((float) a(i7, b3, 0))) + ((b2 != 0 ? ((float) abs3) / ((float) i8) : ((float) abs) / ((float) i9)) * ((float) a(i6, b2, this.r.b(view))))));
        b(2);
        return true;
    }

    private boolean a(View view, float f2) {
        if (view == null) {
            return false;
        }
        return (this.r.b(view) > 0) && Math.abs(f2) > ((float) this.b);
    }

    private static int b(int i2, int i3, int i4) {
        int abs = Math.abs(i2);
        if (abs < i3) {
            return 0;
        }
        return abs > i4 ? i2 <= 0 ? -i4 : i4 : i2;
    }

    private void b(float f2) {
        this.t = true;
        this.r.a(this.s, f2);
        this.t = false;
        if (this.a == 1) {
            b(0);
        }
    }

    private void b(float f2, float f3, int i2) {
        int i3 = 1;
        if (!a(f2, f3, i2, 1)) {
            i3 = 0;
        }
        if (a(f3, f2, i2, 4)) {
            i3 |= 4;
        }
        if (a(f2, f3, i2, 2)) {
            i3 |= 2;
        }
        if (a(f3, f2, i2, 8)) {
            i3 |= 8;
        }
        if (i3 != 0) {
            int[] iArr = this.i;
            iArr[i2] = iArr[i2] | i3;
            this.r.a(i3, i2);
        }
    }

    private boolean b(View view, int i2) {
        if (view == this.s && this.c == i2) {
            return true;
        }
        if (view == null || !this.r.a(view)) {
            return false;
        }
        this.c = i2;
        a(view, i2);
        return true;
    }

    public static boolean b(View view, int i2, int i3) {
        return view != null && i2 >= view.getLeft() && i2 < view.getRight() && i3 >= view.getTop() && i3 < view.getBottom();
    }

    private void c(int i2) {
        if (this.d != null) {
            this.d[i2] = 0.0f;
            this.e[i2] = 0.0f;
            this.f[i2] = 0.0f;
            this.g[i2] = 0.0f;
            this.h[i2] = 0;
            this.i[i2] = 0;
            this.j[i2] = 0;
            this.k &= (1 << i2) ^ -1;
        }
    }

    private void c(MotionEvent motionEvent) {
        int c2 = ay.c(motionEvent);
        for (int i2 = 0; i2 < c2; i2++) {
            int b2 = ay.b(motionEvent, i2);
            float c3 = ay.c(motionEvent, i2);
            float d2 = ay.d(motionEvent, i2);
            this.f[b2] = c3;
            this.g[b2] = d2;
        }
    }

    private void i() {
        this.l.computeCurrentVelocity(1000, this.m);
        float a2 = a(bt.a(this.l, this.c), this.n, this.m);
        a(bt.b(this.l, this.c), this.n, this.m);
        b(a2);
    }

    public final int a() {
        return this.a;
    }

    public final void a(float f2) {
        this.n = f2;
    }

    public final void a(int i2) {
        this.p = i2;
    }

    public final void a(View view, int i2) {
        if (view.getParent() != this.u) {
            throw new IllegalArgumentException("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (" + this.u + ")");
        }
        this.s = view;
        this.c = i2;
        this.r.d(view);
        b(1);
    }

    public final boolean a(int i2, int i3) {
        if (this.t) {
            return a(i2, i3, (int) bt.a(this.l, this.c), (int) bt.b(this.l, this.c));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    public final boolean a(MotionEvent motionEvent) {
        View b2;
        int a2 = ay.a(motionEvent);
        int b3 = ay.b(motionEvent);
        if (a2 == 0) {
            e();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        switch (a2) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int b4 = ay.b(motionEvent, 0);
                a(x, y, b4);
                View b5 = b((int) x, (int) y);
                if (b5 == this.s && this.a == 2) {
                    b(b5, b4);
                }
                if ((this.h[b4] & this.p) != 0) {
                    this.r.b();
                    break;
                }
                break;
            case 1:
            case 3:
                e();
                break;
            case 2:
                if (!(this.d == null || this.e == null)) {
                    int c2 = ay.c(motionEvent);
                    int i2 = 0;
                    while (i2 < c2) {
                        int b6 = ay.b(motionEvent, i2);
                        float c3 = ay.c(motionEvent, i2);
                        float d2 = ay.d(motionEvent, i2);
                        float f2 = c3 - this.d[b6];
                        float f3 = d2 - this.e[b6];
                        View b7 = b((int) c3, (int) d2);
                        boolean z = b7 != null && a(b7, f2);
                        if (z) {
                            int left = b7.getLeft();
                            int a3 = this.r.a(b7, ((int) f2) + left);
                            b7.getTop();
                            this.r.c(b7);
                            int b8 = this.r.b(b7);
                            if (b8 != 0) {
                                if (b8 > 0 && a3 == left) {
                                }
                            }
                            c(motionEvent);
                            break;
                        }
                        b(f2, f3, b6);
                        if (this.a != 1 && (!z || !b(b7, b6))) {
                            i2++;
                        }
                        c(motionEvent);
                    }
                    c(motionEvent);
                }
                break;
            case 5:
                int b9 = ay.b(motionEvent, b3);
                float c4 = ay.c(motionEvent, b3);
                float d3 = ay.d(motionEvent, b3);
                a(c4, d3, b9);
                if (this.a != 0) {
                    if (this.a == 2 && (b2 = b((int) c4, (int) d3)) == this.s) {
                        b(b2, b9);
                        break;
                    }
                } else if ((this.h[b9] & this.p) != 0) {
                    this.r.b();
                    break;
                }
                break;
            case 6:
                c(ay.b(motionEvent, b3));
                break;
        }
        return this.a == 1;
    }

    public final boolean a(View view, int i2, int i3) {
        this.s = view;
        this.c = -1;
        boolean a2 = a(i2, i3, 0, 0);
        if (!a2 && this.a == 0 && this.s != null) {
            this.s = null;
        }
        return a2;
    }

    public final int b() {
        return this.o;
    }

    public final View b(int i2, int i3) {
        for (int childCount = this.u.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = this.u.getChildAt(childCount);
            if (i2 >= childAt.getLeft() && i2 < childAt.getRight() && i3 >= childAt.getTop() && i3 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2) {
        this.u.removeCallbacks(this.w);
        if (this.a != i2) {
            this.a = i2;
            this.r.a(i2);
            if (this.a == 0) {
                this.s = null;
            }
        }
    }

    public final void b(MotionEvent motionEvent) {
        int i2;
        int i3 = 0;
        int a2 = ay.a(motionEvent);
        int b2 = ay.b(motionEvent);
        if (a2 == 0) {
            e();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        switch (a2) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int b3 = ay.b(motionEvent, 0);
                View b4 = b((int) x, (int) y);
                a(x, y, b3);
                b(b4, b3);
                if ((this.h[b3] & this.p) != 0) {
                    this.r.b();
                    return;
                }
                return;
            case 1:
                if (this.a == 1) {
                    i();
                }
                e();
                return;
            case 2:
                if (this.a == 1) {
                    int a3 = ay.a(motionEvent, this.c);
                    float c2 = ay.c(motionEvent, a3);
                    float d2 = ay.d(motionEvent, a3);
                    int i4 = (int) (c2 - this.f[this.c]);
                    int i5 = (int) (d2 - this.g[this.c]);
                    int left = this.s.getLeft() + i4;
                    this.s.getTop();
                    int left2 = this.s.getLeft();
                    int top = this.s.getTop();
                    if (i4 != 0) {
                        left = this.r.a(this.s, left);
                        this.s.offsetLeftAndRight(left - left2);
                    }
                    if (i5 != 0) {
                        this.s.offsetTopAndBottom(this.r.c(this.s) - top);
                    }
                    if (!(i4 == 0 && i5 == 0)) {
                        this.r.b(this.s, left);
                    }
                    c(motionEvent);
                    return;
                }
                int c3 = ay.c(motionEvent);
                while (i3 < c3) {
                    int b5 = ay.b(motionEvent, i3);
                    float c4 = ay.c(motionEvent, i3);
                    float d3 = ay.d(motionEvent, i3);
                    float f2 = c4 - this.d[b5];
                    b(f2, d3 - this.e[b5], b5);
                    if (this.a != 1) {
                        View b6 = b((int) c4, (int) d3);
                        if (!a(b6, f2) || !b(b6, b5)) {
                            i3++;
                        }
                    }
                    c(motionEvent);
                    return;
                }
                c(motionEvent);
                return;
            case 3:
                if (this.a == 1) {
                    b(0.0f);
                }
                e();
                return;
            case 4:
            default:
                return;
            case 5:
                int b7 = ay.b(motionEvent, b2);
                float c5 = ay.c(motionEvent, b2);
                float d4 = ay.d(motionEvent, b2);
                a(c5, d4, b7);
                if (this.a == 0) {
                    b(b((int) c5, (int) d4), b7);
                    if ((this.h[b7] & this.p) != 0) {
                        this.r.b();
                        return;
                    }
                    return;
                }
                if (b(this.s, (int) c5, (int) d4)) {
                    b(this.s, b7);
                    return;
                }
                return;
            case 6:
                int b8 = ay.b(motionEvent, b2);
                if (this.a == 1 && b8 == this.c) {
                    int c6 = ay.c(motionEvent);
                    while (true) {
                        if (i3 >= c6) {
                            i2 = -1;
                        } else {
                            int b9 = ay.b(motionEvent, i3);
                            if (b9 != this.c) {
                                if (b((int) ay.c(motionEvent, i3), (int) ay.d(motionEvent, i3)) == this.s && b(this.s, b9)) {
                                    i2 = this.c;
                                }
                            }
                            i3++;
                        }
                    }
                    if (i2 == -1) {
                        i();
                    }
                }
                c(b8);
                return;
        }
    }

    public final View c() {
        return this.s;
    }

    public final int d() {
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(float[], float):void}
     arg types: [float[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void} */
    public final void e() {
        this.c = -1;
        if (this.d != null) {
            Arrays.fill(this.d, 0.0f);
            Arrays.fill(this.e, 0.0f);
            Arrays.fill(this.f, 0.0f);
            Arrays.fill(this.g, 0.0f);
            Arrays.fill(this.h, 0);
            Arrays.fill(this.i, 0);
            Arrays.fill(this.j, 0);
            this.k = 0;
        }
        if (this.l != null) {
            this.l.recycle();
            this.l = null;
        }
    }

    public final void f() {
        e();
        if (this.a == 2) {
            this.q.b();
            this.q.c();
            this.q.h();
            int b2 = this.q.b();
            this.q.c();
            this.r.b(this.s, b2);
        }
        b(0);
    }

    public final boolean g() {
        if (this.a == 2) {
            boolean g2 = this.q.g();
            int b2 = this.q.b();
            int c2 = this.q.c();
            int left = b2 - this.s.getLeft();
            int top = c2 - this.s.getTop();
            if (left != 0) {
                this.s.offsetLeftAndRight(left);
            }
            if (top != 0) {
                this.s.offsetTopAndBottom(top);
            }
            if (!(left == 0 && top == 0)) {
                this.r.b(this.s, b2);
            }
            if (g2 && b2 == this.q.d() && c2 == this.q.e()) {
                this.q.h();
                g2 = false;
            }
            if (!g2) {
                this.u.post(this.w);
            }
        }
        return this.a == 2;
    }

    public final boolean h() {
        boolean z;
        int length = this.d.length;
        for (int i2 = 0; i2 < length; i2++) {
            if ((this.k & (1 << i2)) != 0) {
                float f2 = this.f[i2] - this.d[i2];
                float f3 = this.g[i2] - this.e[i2];
                z = (f2 * f2) + (f3 * f3) > ((float) (this.b * this.b));
            } else {
                z = false;
            }
            if (z) {
                return true;
            }
        }
        return false;
    }
}
