package android.support.v4.widget;

import android.graphics.Rect;
import android.support.v4.view.a;
import android.support.v4.view.a.f;
import android.support.v4.view.a.g;
import android.support.v4.view.bx;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

final class l extends a {
    final /* synthetic */ DrawerLayout a;
    private final Rect c = new Rect();

    l(DrawerLayout drawerLayout) {
        this.a = drawerLayout;
    }

    public final void a(View view, f fVar) {
        if (DrawerLayout.c) {
            super.a(view, fVar);
        } else {
            f a2 = f.a(fVar);
            super.a(view, a2);
            fVar.a(view);
            ViewParent i = bx.i(view);
            if (i instanceof View) {
                fVar.c((View) i);
            }
            Rect rect = this.c;
            a2.a(rect);
            fVar.b(rect);
            a2.c(rect);
            fVar.d(rect);
            fVar.c(a2.f());
            fVar.a(a2.l());
            fVar.b(a2.m());
            fVar.d(a2.n());
            fVar.h(a2.k());
            fVar.f(a2.i());
            fVar.a(a2.d());
            fVar.b(a2.e());
            fVar.d(a2.g());
            fVar.e(a2.h());
            fVar.g(a2.j());
            fVar.a(a2.b());
            a2.o();
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = viewGroup.getChildAt(i2);
                if (DrawerLayout.h(childAt)) {
                    fVar.b(childAt);
                }
            }
        }
        fVar.b((CharSequence) DrawerLayout.class.getName());
        fVar.a(false);
        fVar.b(false);
        fVar.a(g.a);
        fVar.a(g.b);
    }

    public final void a(View view, AccessibilityEvent accessibilityEvent) {
        super.a(view, accessibilityEvent);
        accessibilityEvent.setClassName(DrawerLayout.class.getName());
    }

    public final boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        if (DrawerLayout.c || DrawerLayout.h(view)) {
            return super.a(viewGroup, view, accessibilityEvent);
        }
        return false;
    }

    public final boolean d(View view, AccessibilityEvent accessibilityEvent) {
        CharSequence a2;
        if (accessibilityEvent.getEventType() != 32) {
            return super.d(view, accessibilityEvent);
        }
        List<CharSequence> text = accessibilityEvent.getText();
        View a3 = this.a.h();
        if (!(a3 == null || (a2 = this.a.a(this.a.c(a3))) == null)) {
            text.add(a2);
        }
        return true;
    }
}
