package android.support.v4.widget;

import android.view.animation.Animation;

final class ag implements Animation.AnimationListener {
    final /* synthetic */ ai a;
    final /* synthetic */ ae b;

    ag(ae aeVar, ai aiVar) {
        this.b = aeVar;
        this.a = aiVar;
    }

    public final void onAnimationEnd(Animation animation) {
    }

    public final void onAnimationRepeat(Animation animation) {
        this.a.m();
        this.a.c();
        this.a.b(this.a.j());
        if (this.b.a) {
            this.b.a = false;
            animation.setDuration(1332);
            this.a.a(false);
            return;
        }
        float unused = this.b.k = (this.b.k + 1.0f) % 5.0f;
    }

    public final void onAnimationStart(Animation animation) {
        float unused = this.b.k = 0.0f;
    }
}
