package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ay;
import android.support.v4.view.bx;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import java.util.ArrayList;

public class SlidingPaneLayout extends ViewGroup {
    static final bc a;
    private int b;
    private int c;
    private Drawable d;
    private Drawable e;
    private final int f;
    private boolean g;
    /* access modifiers changed from: private */
    public View h;
    /* access modifiers changed from: private */
    public float i;
    private float j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public boolean l;
    private int m;
    private float n;
    private float o;
    /* access modifiers changed from: private */
    public final bu p;
    /* access modifiers changed from: private */
    public boolean q;
    private boolean r;
    private final Rect s;
    /* access modifiers changed from: private */
    public final ArrayList t;

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        private static final int[] e = {16843137};
        public float a = 0.0f;
        boolean b;
        boolean c;
        Paint d;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, e);
            this.a = obtainStyledAttributes.getFloat(0, 0.0f);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new bb();
        boolean a;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readInt() != 0;
        }

        /* synthetic */ SavedState(Parcel parcel, byte b) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a ? 1 : 0);
        }
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 17) {
            a = new bf();
        } else if (i2 >= 16) {
            a = new be();
        } else {
            a = new bd();
        }
    }

    public SlidingPaneLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, int):void
     arg types: [android.support.v4.widget.SlidingPaneLayout, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, float):void
      android.support.v4.view.bx.c(android.view.View, int):void */
    public SlidingPaneLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = -858993460;
        this.r = true;
        this.s = new Rect();
        this.t = new ArrayList();
        float f2 = context.getResources().getDisplayMetrics().density;
        this.f = (int) ((32.0f * f2) + 0.5f);
        ViewConfiguration.get(context);
        setWillNotDraw(false);
        bx.a(this, new ay(this));
        bx.c((View) this, 1);
        this.p = bu.a(this, 0.5f, new ba(this, (byte) 0));
        this.p.a(f2 * 400.0f);
    }

    static /* synthetic */ void a(SlidingPaneLayout slidingPaneLayout, int i2) {
        if (slidingPaneLayout.h == null) {
            slidingPaneLayout.i = 0.0f;
            return;
        }
        boolean c2 = slidingPaneLayout.c();
        LayoutParams layoutParams = (LayoutParams) slidingPaneLayout.h.getLayoutParams();
        int width = slidingPaneLayout.h.getWidth();
        if (c2) {
            i2 = (slidingPaneLayout.getWidth() - i2) - width;
        }
        slidingPaneLayout.i = ((float) (i2 - ((c2 ? layoutParams.rightMargin : layoutParams.leftMargin) + (c2 ? slidingPaneLayout.getPaddingRight() : slidingPaneLayout.getPaddingLeft())))) / ((float) slidingPaneLayout.k);
        if (slidingPaneLayout.m != 0) {
            slidingPaneLayout.b(slidingPaneLayout.i);
        }
        if (layoutParams.c) {
            slidingPaneLayout.a(slidingPaneLayout.h, slidingPaneLayout.i, slidingPaneLayout.b);
        }
    }

    private void a(View view, float f2, int i2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f2 > 0.0f && i2 != 0) {
            int i3 = (((int) (((float) ((-16777216 & i2) >>> 24)) * f2)) << 24) | (16777215 & i2);
            if (layoutParams.d == null) {
                layoutParams.d = new Paint();
            }
            layoutParams.d.setColorFilter(new PorterDuffColorFilter(i3, PorterDuff.Mode.SRC_OVER));
            if (bx.g(view) != 2) {
                bx.a(view, 2, layoutParams.d);
            }
            c(view);
        } else if (bx.g(view) != 0) {
            if (layoutParams.d != null) {
                layoutParams.d.setColorFilter(null);
            }
            az azVar = new az(this, view);
            this.t.add(azVar);
            bx.a(this, azVar);
        }
    }

    private boolean a(float f2) {
        int paddingLeft;
        if (!this.g) {
            return false;
        }
        boolean c2 = c();
        LayoutParams layoutParams = (LayoutParams) this.h.getLayoutParams();
        if (c2) {
            paddingLeft = (int) (((float) getWidth()) - ((((float) (layoutParams.rightMargin + getPaddingRight())) + (((float) this.k) * f2)) + ((float) this.h.getWidth())));
        } else {
            paddingLeft = (int) (((float) (layoutParams.leftMargin + getPaddingLeft())) + (((float) this.k) * f2));
        }
        if (!this.p.a(this.h, paddingLeft, this.h.getTop())) {
            return false;
        }
        a();
        bx.d(this);
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(float r10) {
        /*
            r9 = this;
            r1 = 0
            r8 = 1065353216(0x3f800000, float:1.0)
            boolean r3 = r9.c()
            android.view.View r0 = r9.h
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            android.support.v4.widget.SlidingPaneLayout$LayoutParams r0 = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) r0
            boolean r2 = r0.c
            if (r2 == 0) goto L_0x0055
            if (r3 == 0) goto L_0x0052
            int r0 = r0.rightMargin
        L_0x0017:
            if (r0 > 0) goto L_0x0055
            r0 = 1
        L_0x001a:
            int r4 = r9.getChildCount()
            r2 = r1
        L_0x001f:
            if (r2 >= r4) goto L_0x005c
            android.view.View r5 = r9.getChildAt(r2)
            android.view.View r1 = r9.h
            if (r5 == r1) goto L_0x004e
            float r1 = r9.j
            float r1 = r8 - r1
            int r6 = r9.m
            float r6 = (float) r6
            float r1 = r1 * r6
            int r1 = (int) r1
            r9.j = r10
            float r6 = r8 - r10
            int r7 = r9.m
            float r7 = (float) r7
            float r6 = r6 * r7
            int r6 = (int) r6
            int r1 = r1 - r6
            if (r3 == 0) goto L_0x003f
            int r1 = -r1
        L_0x003f:
            r5.offsetLeftAndRight(r1)
            if (r0 == 0) goto L_0x004e
            if (r3 == 0) goto L_0x0057
            float r1 = r9.j
            float r1 = r1 - r8
        L_0x0049:
            int r6 = r9.c
            r9.a(r5, r1, r6)
        L_0x004e:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x001f
        L_0x0052:
            int r0 = r0.leftMargin
            goto L_0x0017
        L_0x0055:
            r0 = r1
            goto L_0x001a
        L_0x0057:
            float r1 = r9.j
            float r1 = r8 - r1
            goto L_0x0049
        L_0x005c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.SlidingPaneLayout.b(float):void");
    }

    private boolean b() {
        if (!this.r && !a(0.0f)) {
            return false;
        }
        this.q = false;
        return true;
    }

    /* access modifiers changed from: private */
    public void c(View view) {
        a.a(this, view);
    }

    /* access modifiers changed from: private */
    public boolean c() {
        return bx.h(this) == 1;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 4) {
                childAt.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.view.View r18) {
        /*
            r17 = this;
            boolean r9 = r17.c()
            if (r9 == 0) goto L_0x008d
            int r1 = r17.getWidth()
            int r2 = r17.getPaddingRight()
            int r7 = r1 - r2
        L_0x0010:
            if (r9 == 0) goto L_0x0093
            int r1 = r17.getPaddingLeft()
        L_0x0016:
            int r10 = r17.getPaddingTop()
            int r2 = r17.getHeight()
            int r3 = r17.getPaddingBottom()
            int r11 = r2 - r3
            if (r18 == 0) goto L_0x00ba
            boolean r2 = android.support.v4.view.bx.j(r18)
            if (r2 == 0) goto L_0x009e
            r2 = 1
        L_0x002d:
            if (r2 == 0) goto L_0x00ba
            int r5 = r18.getLeft()
            int r4 = r18.getRight()
            int r3 = r18.getTop()
            int r2 = r18.getBottom()
        L_0x003f:
            r6 = 0
            int r12 = r17.getChildCount()
            r8 = r6
        L_0x0045:
            if (r8 >= r12) goto L_0x00c5
            r0 = r17
            android.view.View r13 = r0.getChildAt(r8)
            r0 = r18
            if (r13 == r0) goto L_0x00c5
            if (r9 == 0) goto L_0x00bf
            r6 = r1
        L_0x0054:
            int r14 = r13.getLeft()
            int r14 = java.lang.Math.max(r6, r14)
            int r6 = r13.getTop()
            int r15 = java.lang.Math.max(r10, r6)
            if (r9 == 0) goto L_0x00c1
            r6 = r7
        L_0x0067:
            int r16 = r13.getRight()
            r0 = r16
            int r6 = java.lang.Math.min(r6, r0)
            int r16 = r13.getBottom()
            r0 = r16
            int r16 = java.lang.Math.min(r11, r0)
            if (r14 < r5) goto L_0x00c3
            if (r15 < r3) goto L_0x00c3
            if (r6 > r4) goto L_0x00c3
            r0 = r16
            if (r0 > r2) goto L_0x00c3
            r6 = 4
        L_0x0086:
            r13.setVisibility(r6)
            int r6 = r8 + 1
            r8 = r6
            goto L_0x0045
        L_0x008d:
            int r7 = r17.getPaddingLeft()
            goto L_0x0010
        L_0x0093:
            int r1 = r17.getWidth()
            int r2 = r17.getPaddingRight()
            int r1 = r1 - r2
            goto L_0x0016
        L_0x009e:
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 18
            if (r2 >= r3) goto L_0x00b7
            android.graphics.drawable.Drawable r2 = r18.getBackground()
            if (r2 == 0) goto L_0x00b7
            int r2 = r2.getOpacity()
            r3 = -1
            if (r2 != r3) goto L_0x00b4
            r2 = 1
            goto L_0x002d
        L_0x00b4:
            r2 = 0
            goto L_0x002d
        L_0x00b7:
            r2 = 0
            goto L_0x002d
        L_0x00ba:
            r2 = 0
            r3 = r2
            r4 = r2
            r5 = r2
            goto L_0x003f
        L_0x00bf:
            r6 = r7
            goto L_0x0054
        L_0x00c1:
            r6 = r1
            goto L_0x0067
        L_0x00c3:
            r6 = 0
            goto L_0x0086
        L_0x00c5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.SlidingPaneLayout.a(android.view.View):void");
    }

    /* access modifiers changed from: package-private */
    public final boolean b(View view) {
        if (view == null) {
            return false;
        }
        return this.g && ((LayoutParams) view.getLayoutParams()).c && this.i > 0.0f;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        if (!this.p.g()) {
            return;
        }
        if (!this.g) {
            this.p.f();
        } else {
            bx.d(this);
        }
    }

    public void draw(Canvas canvas) {
        int left;
        int i2;
        super.draw(canvas);
        Drawable drawable = c() ? this.e : this.d;
        View childAt = getChildCount() > 1 ? getChildAt(1) : null;
        if (childAt != null && drawable != null) {
            int top = childAt.getTop();
            int bottom = childAt.getBottom();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            if (c()) {
                i2 = childAt.getRight();
                left = i2 + intrinsicWidth;
            } else {
                left = childAt.getLeft();
                i2 = left - intrinsicWidth;
            }
            drawable.setBounds(i2, top, left, bottom);
            drawable.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        boolean drawChild;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int save = canvas.save(2);
        if (this.g && !layoutParams.b && this.h != null) {
            canvas.getClipBounds(this.s);
            if (c()) {
                this.s.left = Math.max(this.s.left, this.h.getRight());
            } else {
                this.s.right = Math.min(this.s.right, this.h.getLeft());
            }
            canvas.clipRect(this.s);
        }
        if (Build.VERSION.SDK_INT < 11) {
            if (layoutParams.c && this.i > 0.0f) {
                if (!view.isDrawingCacheEnabled()) {
                    view.setDrawingCacheEnabled(true);
                }
                Bitmap drawingCache = view.getDrawingCache();
                if (drawingCache != null) {
                    canvas.drawBitmap(drawingCache, (float) view.getLeft(), (float) view.getTop(), layoutParams.d);
                    drawChild = false;
                    canvas.restoreToCount(save);
                    return drawChild;
                }
                Log.e("SlidingPaneLayout", "drawChild: child view " + view + " returned null drawing cache");
            } else if (view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(false);
            }
        }
        drawChild = super.drawChild(canvas, view, j2);
        canvas.restoreToCount(save);
        return drawChild;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.r = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.r = true;
        int size = this.t.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((az) this.t.get(i2)).run();
        }
        this.t.clear();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View childAt;
        int a2 = ay.a(motionEvent);
        if (!this.g && a2 == 0 && getChildCount() > 1 && (childAt = getChildAt(1)) != null) {
            this.q = !bu.b(childAt, (int) motionEvent.getX(), (int) motionEvent.getY());
        }
        if (!this.g || (this.l && a2 != 0)) {
            this.p.e();
            return super.onInterceptTouchEvent(motionEvent);
        } else if (a2 == 3 || a2 == 1) {
            this.p.e();
            return false;
        } else {
            switch (a2) {
                case 0:
                    this.l = false;
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    this.n = x;
                    this.o = y;
                    if (bu.b(this.h, (int) x, (int) y) && b(this.h)) {
                        z = true;
                        break;
                    }
                    z = false;
                    break;
                case 1:
                default:
                    z = false;
                    break;
                case 2:
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float abs = Math.abs(x2 - this.n);
                    float abs2 = Math.abs(y2 - this.o);
                    if (abs > ((float) this.p.d()) && abs2 > abs) {
                        this.p.e();
                        this.l = true;
                        return false;
                    }
                    z = false;
                    break;
            }
            return this.p.a(motionEvent) || z;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        boolean c2 = c();
        if (c2) {
            this.p.a(2);
        } else {
            this.p.a(1);
        }
        int i11 = i4 - i2;
        int paddingRight = c2 ? getPaddingRight() : getPaddingLeft();
        int paddingLeft = c2 ? getPaddingLeft() : getPaddingRight();
        int paddingTop = getPaddingTop();
        int childCount = getChildCount();
        if (this.r) {
            this.i = (!this.g || !this.q) ? 0.0f : 1.0f;
        }
        int i12 = 0;
        int i13 = paddingRight;
        while (i12 < childCount) {
            View childAt = getChildAt(i12);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                int i14 = 0;
                if (layoutParams.b) {
                    int min = (Math.min(paddingRight, (i11 - paddingLeft) - this.f) - i13) - (layoutParams.leftMargin + layoutParams.rightMargin);
                    this.k = min;
                    int i15 = c2 ? layoutParams.rightMargin : layoutParams.leftMargin;
                    layoutParams.c = ((i13 + i15) + min) + (measuredWidth / 2) > i11 - paddingLeft;
                    int i16 = (int) (((float) min) * this.i);
                    i8 = i13 + i15 + i16;
                    this.i = ((float) i16) / ((float) this.k);
                } else {
                    i14 = (!this.g || this.m == 0) ? 0 : (int) ((1.0f - this.i) * ((float) this.m));
                    i8 = paddingRight;
                }
                if (c2) {
                    i10 = (i11 - i8) + i14;
                    i9 = i10 - measuredWidth;
                } else {
                    i9 = i8 - i14;
                    i10 = i9 + measuredWidth;
                }
                childAt.layout(i9, paddingTop, i10, childAt.getMeasuredHeight() + paddingTop);
                i6 = childAt.getWidth() + paddingRight;
                i7 = i8;
            } else {
                i6 = paddingRight;
                i7 = i13;
            }
            i12++;
            paddingRight = i6;
            i13 = i7;
        }
        if (this.r) {
            if (this.g) {
                if (this.m != 0) {
                    b(this.i);
                }
                if (((LayoutParams) this.h.getLayoutParams()).c) {
                    a(this.h, this.i, this.b);
                }
            } else {
                for (int i17 = 0; i17 < childCount; i17++) {
                    a(getChildAt(i17), 0.0f, this.b);
                }
            }
            a(this.h);
        }
        this.r = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        int paddingTop;
        int i8;
        float f2;
        boolean z;
        int i9;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode == 1073741824) {
            if (mode2 == 0) {
                if (!isInEditMode()) {
                    throw new IllegalStateException("Height must not be UNSPECIFIED");
                } else if (mode2 == 0) {
                    i4 = Integer.MIN_VALUE;
                    i5 = size;
                    i6 = 300;
                }
            }
            i4 = mode2;
            i5 = size;
            i6 = size2;
        } else if (isInEditMode()) {
            if (mode != Integer.MIN_VALUE && mode == 0) {
                i4 = mode2;
                i5 = 300;
                i6 = size2;
            }
            i4 = mode2;
            i5 = size;
            i6 = size2;
        } else {
            throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
        }
        switch (i4) {
            case Integer.MIN_VALUE:
                i7 = 0;
                paddingTop = (i6 - getPaddingTop()) - getPaddingBottom();
                break;
            case 1073741824:
                i7 = (i6 - getPaddingTop()) - getPaddingBottom();
                paddingTop = i7;
                break;
            default:
                i7 = 0;
                paddingTop = -1;
                break;
        }
        boolean z2 = false;
        int paddingLeft = (i5 - getPaddingLeft()) - getPaddingRight();
        int childCount = getChildCount();
        if (childCount > 2) {
            Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.h = null;
        int i10 = 0;
        int i11 = paddingLeft;
        int i12 = i7;
        float f3 = 0.0f;
        while (i10 < childCount) {
            View childAt = getChildAt(i10);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (childAt.getVisibility() == 8) {
                layoutParams.c = false;
                i8 = i11;
                i9 = i12;
                f2 = f3;
                z = z2;
            } else {
                if (layoutParams.a > 0.0f) {
                    f3 += layoutParams.a;
                    if (layoutParams.width == 0) {
                        i8 = i11;
                        i9 = i12;
                        f2 = f3;
                        z = z2;
                    }
                }
                int i13 = layoutParams.leftMargin + layoutParams.rightMargin;
                childAt.measure(layoutParams.width == -2 ? View.MeasureSpec.makeMeasureSpec(paddingLeft - i13, Integer.MIN_VALUE) : layoutParams.width == -1 ? View.MeasureSpec.makeMeasureSpec(paddingLeft - i13, 1073741824) : View.MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824), layoutParams.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : layoutParams.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824));
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                if (i4 == Integer.MIN_VALUE && measuredHeight > i12) {
                    i12 = Math.min(measuredHeight, paddingTop);
                }
                int i14 = i11 - measuredWidth;
                boolean z3 = i14 < 0;
                layoutParams.b = z3;
                boolean z4 = z3 | z2;
                if (layoutParams.b) {
                    this.h = childAt;
                }
                i8 = i14;
                f2 = f3;
                z = z4;
                i9 = i12;
            }
            i10++;
            z2 = z;
            i12 = i9;
            i11 = i8;
            f3 = f2;
        }
        if (z2 || f3 > 0.0f) {
            int i15 = paddingLeft - this.f;
            for (int i16 = 0; i16 < childCount; i16++) {
                View childAt2 = getChildAt(i16);
                if (childAt2.getVisibility() != 8) {
                    LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                    if (childAt2.getVisibility() != 8) {
                        boolean z5 = layoutParams2.width == 0 && layoutParams2.a > 0.0f;
                        int measuredWidth2 = z5 ? 0 : childAt2.getMeasuredWidth();
                        if (!z2 || childAt2 == this.h) {
                            if (layoutParams2.a > 0.0f) {
                                int makeMeasureSpec = layoutParams2.width == 0 ? layoutParams2.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : layoutParams2.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(layoutParams2.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                                if (z2) {
                                    int i17 = paddingLeft - (layoutParams2.rightMargin + layoutParams2.leftMargin);
                                    int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i17, 1073741824);
                                    if (measuredWidth2 != i17) {
                                        childAt2.measure(makeMeasureSpec2, makeMeasureSpec);
                                    }
                                } else {
                                    childAt2.measure(View.MeasureSpec.makeMeasureSpec(((int) ((layoutParams2.a * ((float) Math.max(0, i11))) / f3)) + measuredWidth2, 1073741824), makeMeasureSpec);
                                }
                            }
                        } else if (layoutParams2.width < 0 && (measuredWidth2 > i15 || layoutParams2.a > 0.0f)) {
                            childAt2.measure(View.MeasureSpec.makeMeasureSpec(i15, 1073741824), z5 ? layoutParams2.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : layoutParams2.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(layoutParams2.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824));
                        }
                    }
                }
            }
        }
        setMeasuredDimension(i5, getPaddingTop() + i12 + getPaddingBottom());
        this.g = z2;
        if (this.p.a() != 0 && !z2) {
            this.p.f();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!savedState.a) {
            b();
        } else if (this.r || a(1.0f)) {
            this.q = true;
        }
        this.q = savedState.a;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = this.g ? !this.g || this.i == 1.0f : this.q;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            this.r = true;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.g) {
            return super.onTouchEvent(motionEvent);
        }
        this.p.b(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.n = x;
                this.o = y;
                break;
            case 1:
                if (b(this.h)) {
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float f2 = x2 - this.n;
                    float f3 = y2 - this.o;
                    int d2 = this.p.d();
                    if ((f2 * f2) + (f3 * f3) < ((float) (d2 * d2)) && bu.b(this.h, (int) x2, (int) y2)) {
                        b();
                        break;
                    }
                }
                break;
        }
        return true;
    }

    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        if (!isInTouchMode() && !this.g) {
            this.q = view == this.h;
        }
    }
}
