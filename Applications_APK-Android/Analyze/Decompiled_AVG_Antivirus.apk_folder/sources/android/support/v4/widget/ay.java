package android.support.v4.widget;

import android.graphics.Rect;
import android.support.v4.view.a;
import android.support.v4.view.a.f;
import android.support.v4.view.bx;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;

final class ay extends a {
    final /* synthetic */ SlidingPaneLayout a;
    private final Rect c = new Rect();

    ay(SlidingPaneLayout slidingPaneLayout) {
        this.a = slidingPaneLayout;
    }

    private boolean b(View view) {
        return this.a.b(view);
    }

    public final void a(View view, f fVar) {
        f a2 = f.a(fVar);
        super.a(view, a2);
        Rect rect = this.c;
        a2.a(rect);
        fVar.b(rect);
        a2.c(rect);
        fVar.d(rect);
        fVar.c(a2.f());
        fVar.a(a2.l());
        fVar.b(a2.m());
        fVar.d(a2.n());
        fVar.h(a2.k());
        fVar.f(a2.i());
        fVar.a(a2.d());
        fVar.b(a2.e());
        fVar.d(a2.g());
        fVar.e(a2.h());
        fVar.g(a2.j());
        fVar.a(a2.b());
        fVar.b(a2.c());
        a2.o();
        fVar.b((CharSequence) SlidingPaneLayout.class.getName());
        fVar.a(view);
        ViewParent i = bx.i(view);
        if (i instanceof View) {
            fVar.c((View) i);
        }
        int childCount = this.a.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = this.a.getChildAt(i2);
            if (!b(childAt) && childAt.getVisibility() == 0) {
                bx.c(childAt, 1);
                fVar.b(childAt);
            }
        }
    }

    public final void a(View view, AccessibilityEvent accessibilityEvent) {
        super.a(view, accessibilityEvent);
        accessibilityEvent.setClassName(SlidingPaneLayout.class.getName());
    }

    public final boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        if (!b(view)) {
            return super.a(viewGroup, view, accessibilityEvent);
        }
        return false;
    }
}
