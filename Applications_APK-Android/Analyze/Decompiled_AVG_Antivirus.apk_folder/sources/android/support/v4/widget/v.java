package android.support.v4.widget;

import android.view.View;
import android.view.WindowInsets;

final class v implements View.OnApplyWindowInsetsListener {
    v() {
    }

    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        ((w) view).a(windowInsets, windowInsets.getSystemWindowInsetTop() > 0);
        return windowInsets.consumeSystemWindowInsets();
    }
}
