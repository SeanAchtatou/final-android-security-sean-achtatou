package android.support.v4.widget;

import android.support.v4.view.bx;

final class c implements Runnable {
    final /* synthetic */ a a;

    private c(a aVar) {
        this.a = aVar;
    }

    /* synthetic */ c(a aVar, byte b) {
        this(aVar);
    }

    public final void run() {
        if (this.a.o) {
            if (this.a.m) {
                boolean unused = this.a.m = false;
                this.a.a.c();
            }
            b d = this.a.a;
            if (d.e() || !this.a.a()) {
                boolean unused2 = this.a.o = false;
                return;
            }
            if (this.a.n) {
                boolean unused3 = this.a.n = false;
                a.i(this.a);
            }
            d.f();
            this.a.a(d.i());
            bx.a(this.a.c, this);
        }
    }
}
