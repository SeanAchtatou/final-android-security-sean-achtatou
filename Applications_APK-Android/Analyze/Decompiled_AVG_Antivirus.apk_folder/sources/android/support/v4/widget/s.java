package android.support.v4.widget;

import android.support.v4.widget.DrawerLayout;
import android.view.View;

final class s extends bx {
    final /* synthetic */ DrawerLayout a;
    private final int b;
    private bu c;
    private final Runnable d = new t(this);

    public s(DrawerLayout drawerLayout, int i) {
        this.a = drawerLayout;
        this.b = i;
    }

    static /* synthetic */ void a(s sVar) {
        View view;
        int i;
        int i2 = 0;
        int b2 = sVar.c.b();
        boolean z = sVar.b == 3;
        if (z) {
            View b3 = sVar.a.b(3);
            if (b3 != null) {
                i2 = -b3.getWidth();
            }
            int i3 = i2 + b2;
            view = b3;
            i = i3;
        } else {
            View b4 = sVar.a.b(5);
            int width = sVar.a.getWidth() - b2;
            view = b4;
            i = width;
        }
        if (view == null) {
            return;
        }
        if (((z && view.getLeft() < i) || (!z && view.getLeft() > i)) && sVar.a.a(view) == 0) {
            sVar.c.a(view, i, view.getTop());
            ((DrawerLayout.LayoutParams) view.getLayoutParams()).c = true;
            sVar.a.invalidate();
            sVar.c();
            sVar.a.c();
        }
    }

    private void c() {
        int i = 3;
        if (this.b == 3) {
            i = 5;
        }
        View b2 = this.a.b(i);
        if (b2 != null) {
            this.a.f(b2);
        }
    }

    public final int a(View view, int i) {
        if (this.a.a(view, 3)) {
            return Math.max(-view.getWidth(), Math.min(i, 0));
        }
        int width = this.a.getWidth();
        return Math.max(width - view.getWidth(), Math.min(i, width));
    }

    public final void a() {
        this.a.removeCallbacks(this.d);
    }

    public final void a(int i) {
        this.a.b(i, this.c.c());
    }

    public final void a(int i, int i2) {
        View b2 = (i & 1) == 1 ? this.a.b(3) : this.a.b(5);
        if (b2 != null && this.a.a(b2) == 0) {
            this.c.a(b2, i2);
        }
    }

    public final void a(bu buVar) {
        this.c = buVar;
    }

    public final void a(View view, float f) {
        int width;
        float b2 = DrawerLayout.b(view);
        int width2 = view.getWidth();
        if (this.a.a(view, 3)) {
            width = (f > 0.0f || (f == 0.0f && b2 > 0.5f)) ? 0 : -width2;
        } else {
            width = this.a.getWidth();
            if (f < 0.0f || (f == 0.0f && b2 > 0.5f)) {
                width -= width2;
            }
        }
        this.c.a(width, view.getTop());
        this.a.invalidate();
    }

    public final boolean a(View view) {
        return DrawerLayout.d(view) && this.a.a(view, this.b) && this.a.a(view) == 0;
    }

    public final int b(View view) {
        if (DrawerLayout.d(view)) {
            return view.getWidth();
        }
        return 0;
    }

    public final void b() {
        this.a.postDelayed(this.d, 160);
    }

    public final void b(View view, int i) {
        int width = view.getWidth();
        float width2 = this.a.a(view, 3) ? ((float) (width + i)) / ((float) width) : ((float) (this.a.getWidth() - i)) / ((float) width);
        DrawerLayout.a(view, width2);
        view.setVisibility(width2 == 0.0f ? 4 : 0);
        this.a.invalidate();
    }

    public final int c(View view) {
        return view.getTop();
    }

    public final void d(View view) {
        ((DrawerLayout.LayoutParams) view.getLayoutParams()).c = false;
        c();
    }
}
