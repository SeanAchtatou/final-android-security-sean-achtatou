package android.support.v4.widget;

import android.support.v4.widget.SlidingPaneLayout;
import android.view.View;

final class ba extends bx {
    final /* synthetic */ SlidingPaneLayout a;

    private ba(SlidingPaneLayout slidingPaneLayout) {
        this.a = slidingPaneLayout;
    }

    /* synthetic */ ba(SlidingPaneLayout slidingPaneLayout, byte b) {
        this(slidingPaneLayout);
    }

    public final int a(View view, int i) {
        SlidingPaneLayout.LayoutParams layoutParams = (SlidingPaneLayout.LayoutParams) this.a.h.getLayoutParams();
        if (this.a.c()) {
            int width = this.a.getWidth() - ((layoutParams.rightMargin + this.a.getPaddingRight()) + this.a.h.getWidth());
            return Math.max(Math.min(i, width), width - this.a.k);
        }
        int paddingLeft = layoutParams.leftMargin + this.a.getPaddingLeft();
        return Math.min(Math.max(i, paddingLeft), this.a.k + paddingLeft);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, boolean):boolean
     arg types: [android.support.v4.widget.SlidingPaneLayout, int]
     candidates:
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, int):void
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, android.view.View):void
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, boolean):boolean */
    public final void a(int i) {
        if (this.a.p.a() != 0) {
            return;
        }
        if (this.a.i == 0.0f) {
            this.a.a(this.a.h);
            SlidingPaneLayout slidingPaneLayout = this.a;
            View unused = this.a.h;
            slidingPaneLayout.sendAccessibilityEvent(32);
            boolean unused2 = this.a.q = false;
            return;
        }
        SlidingPaneLayout slidingPaneLayout2 = this.a;
        View unused3 = this.a.h;
        slidingPaneLayout2.sendAccessibilityEvent(32);
        boolean unused4 = this.a.q = true;
    }

    public final void a(int i, int i2) {
        this.a.p.a(this.a.h, i2);
    }

    public final void a(View view, float f) {
        int paddingLeft;
        SlidingPaneLayout.LayoutParams layoutParams = (SlidingPaneLayout.LayoutParams) view.getLayoutParams();
        if (this.a.c()) {
            int paddingRight = layoutParams.rightMargin + this.a.getPaddingRight();
            if (f < 0.0f || (f == 0.0f && this.a.i > 0.5f)) {
                paddingRight += this.a.k;
            }
            paddingLeft = (this.a.getWidth() - paddingRight) - this.a.h.getWidth();
        } else {
            paddingLeft = layoutParams.leftMargin + this.a.getPaddingLeft();
            if (f > 0.0f || (f == 0.0f && this.a.i > 0.5f)) {
                paddingLeft += this.a.k;
            }
        }
        this.a.p.a(paddingLeft, view.getTop());
        this.a.invalidate();
    }

    public final boolean a(View view) {
        if (this.a.l) {
            return false;
        }
        return ((SlidingPaneLayout.LayoutParams) view.getLayoutParams()).b;
    }

    public final int b(View view) {
        return this.a.k;
    }

    public final void b(View view, int i) {
        SlidingPaneLayout.a(this.a, i);
        this.a.invalidate();
    }

    public final int c(View view) {
        return view.getTop();
    }

    public final void d(View view) {
        this.a.a();
    }
}
