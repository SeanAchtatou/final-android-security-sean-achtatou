package android.support.v4.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;

final class o implements n {
    o() {
    }

    public final int a(Object obj) {
        return u.a(obj);
    }

    public final Drawable a(Context context) {
        return u.a(context);
    }

    public final void a(View view) {
        u.a(view);
    }

    public final void a(View view, Object obj, int i) {
        u.a(view, obj, i);
    }

    public final void a(ViewGroup.MarginLayoutParams marginLayoutParams, Object obj, int i) {
        u.a(marginLayoutParams, obj, i);
    }
}
