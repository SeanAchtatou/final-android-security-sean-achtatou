package android.support.v4.widget;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.TextView;

public final class bo {
    static final bt a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 18) {
            a = new br();
        } else if (i >= 17) {
            a = new bq();
        } else if (i >= 16) {
            a = new bs();
        } else {
            a = new bp();
        }
    }

    public static void a(TextView textView, Drawable drawable) {
        a.a(textView, drawable);
    }
}
