package android.support.v4.widget;

import android.view.animation.Animation;

final class bj implements Animation.AnimationListener {
    final /* synthetic */ SwipeRefreshLayout a;

    bj(SwipeRefreshLayout swipeRefreshLayout) {
        this.a = swipeRefreshLayout;
    }

    public final void onAnimationEnd(Animation animation) {
        if (!this.a.v) {
            this.a.a((Animation.AnimationListener) null);
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
