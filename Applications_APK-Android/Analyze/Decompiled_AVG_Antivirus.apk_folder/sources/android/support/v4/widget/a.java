package android.support.v4.widget;

import android.content.res.Resources;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;

public abstract class a implements View.OnTouchListener {
    private static final int r = ViewConfiguration.getTapTimeout();
    /* access modifiers changed from: private */
    public final b a = new b();
    private final Interpolator b = new AccelerateInterpolator();
    /* access modifiers changed from: private */
    public final View c;
    private Runnable d;
    private float[] e = {0.0f, 0.0f};
    private float[] f = {Float.MAX_VALUE, Float.MAX_VALUE};
    private int g;
    private int h;
    private float[] i = {0.0f, 0.0f};
    private float[] j = {0.0f, 0.0f};
    private float[] k = {Float.MAX_VALUE, Float.MAX_VALUE};
    private boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    private boolean p;
    private boolean q;

    public a(View view) {
        this.c = view;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        float f2 = (float) ((int) ((1575.0f * displayMetrics.density) + 0.5f));
        this.k[0] = f2 / 1000.0f;
        this.k[1] = f2 / 1000.0f;
        float f3 = (float) ((int) ((displayMetrics.density * 315.0f) + 0.5f));
        this.j[0] = f3 / 1000.0f;
        this.j[1] = f3 / 1000.0f;
        this.g = 1;
        this.f[0] = Float.MAX_VALUE;
        this.f[1] = Float.MAX_VALUE;
        this.e[0] = 0.2f;
        this.e[1] = 0.2f;
        this.i[0] = 0.001f;
        this.i[1] = 0.001f;
        this.h = r;
        this.a.a();
        this.a.b();
    }

    private float a(float f2, float f3) {
        if (f3 == 0.0f) {
            return 0.0f;
        }
        switch (this.g) {
            case 0:
            case 1:
                if (f2 < f3) {
                    return f2 >= 0.0f ? 1.0f - (f2 / f3) : (!this.o || this.g != 1) ? 0.0f : 1.0f;
                }
                return 0.0f;
            case 2:
                if (f2 < 0.0f) {
                    return f2 / (-f3);
                }
                return 0.0f;
            default:
                return 0.0f;
        }
    }

    /* access modifiers changed from: private */
    public static float a(float f2, float f3, float f4) {
        return f2 > f4 ? f4 : f2 < f3 ? f3 : f2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private float a(int r6, float r7, float r8, float r9) {
        /*
            r5 = this;
            r1 = 0
            float[] r0 = r5.e
            r0 = r0[r6]
            float[] r2 = r5.f
            r2 = r2[r6]
            float r0 = r0 * r8
            float r0 = a(r0, r1, r2)
            float r2 = r5.a(r7, r0)
            float r3 = r8 - r7
            float r0 = r5.a(r3, r0)
            float r0 = r0 - r2
            int r2 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r2 >= 0) goto L_0x0033
            android.view.animation.Interpolator r2 = r5.b
            float r0 = -r0
            float r0 = r2.getInterpolation(r0)
            float r0 = -r0
        L_0x0025:
            r2 = -1082130432(0xffffffffbf800000, float:-1.0)
            r3 = 1065353216(0x3f800000, float:1.0)
            float r0 = a(r0, r2, r3)
        L_0x002d:
            int r2 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r2 != 0) goto L_0x0040
            r0 = r1
        L_0x0032:
            return r0
        L_0x0033:
            int r2 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r2 <= 0) goto L_0x003e
            android.view.animation.Interpolator r2 = r5.b
            float r0 = r2.getInterpolation(r0)
            goto L_0x0025
        L_0x003e:
            r0 = r1
            goto L_0x002d
        L_0x0040:
            float[] r2 = r5.i
            r2 = r2[r6]
            float[] r3 = r5.j
            r3 = r3[r6]
            float[] r4 = r5.k
            r4 = r4[r6]
            float r2 = r2 * r9
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x0057
            float r0 = r0 * r2
            float r0 = a(r0, r3, r4)
            goto L_0x0032
        L_0x0057:
            float r0 = -r0
            float r0 = r0 * r2
            float r0 = a(r0, r3, r4)
            float r0 = -r0
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.a.a(int, float, float, float):float");
    }

    static /* synthetic */ int a(int i2, int i3) {
        if (i2 > i3) {
            return i3;
        }
        if (i2 < 0) {
            return 0;
        }
        return i2;
    }

    /* access modifiers changed from: private */
    public boolean a() {
        b bVar = this.a;
        int h2 = bVar.h();
        int g2 = bVar.g();
        if (h2 != 0 && b(h2)) {
            return true;
        }
        if (g2 != 0) {
        }
        return false;
    }

    private void b() {
        if (this.m) {
            this.o = false;
        } else {
            this.a.d();
        }
    }

    static /* synthetic */ void i(a aVar) {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
        aVar.c.onTouchEvent(obtain);
        obtain.recycle();
    }

    public final a a(boolean z) {
        if (this.p && !z) {
            b();
        }
        this.p = z;
        return this;
    }

    public abstract void a(int i2);

    public abstract boolean b(int i2);

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0057  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r7, android.view.MotionEvent r8) {
        /*
            r6 = this;
            r0 = 0
            r1 = 1
            boolean r2 = r6.p
            if (r2 != 0) goto L_0x0007
        L_0x0006:
            return r0
        L_0x0007:
            int r2 = android.support.v4.view.ay.a(r8)
            switch(r2) {
                case 0: goto L_0x0018;
                case 1: goto L_0x007d;
                case 2: goto L_0x001c;
                case 3: goto L_0x007d;
                default: goto L_0x000e;
            }
        L_0x000e:
            boolean r2 = r6.q
            if (r2 == 0) goto L_0x0006
            boolean r2 = r6.o
            if (r2 == 0) goto L_0x0006
            r0 = r1
            goto L_0x0006
        L_0x0018:
            r6.n = r1
            r6.l = r0
        L_0x001c:
            float r2 = r8.getX()
            int r3 = r7.getWidth()
            float r3 = (float) r3
            android.view.View r4 = r6.c
            int r4 = r4.getWidth()
            float r4 = (float) r4
            float r2 = r6.a(r0, r2, r3, r4)
            float r3 = r8.getY()
            int r4 = r7.getHeight()
            float r4 = (float) r4
            android.view.View r5 = r6.c
            int r5 = r5.getHeight()
            float r5 = (float) r5
            float r3 = r6.a(r1, r3, r4, r5)
            android.support.v4.widget.b r4 = r6.a
            r4.a(r2, r3)
            boolean r2 = r6.o
            if (r2 != 0) goto L_0x000e
            boolean r2 = r6.a()
            if (r2 == 0) goto L_0x000e
            java.lang.Runnable r2 = r6.d
            if (r2 != 0) goto L_0x005e
            android.support.v4.widget.c r2 = new android.support.v4.widget.c
            r2.<init>(r6, r0)
            r6.d = r2
        L_0x005e:
            r6.o = r1
            r6.m = r1
            boolean r2 = r6.l
            if (r2 != 0) goto L_0x0077
            int r2 = r6.h
            if (r2 <= 0) goto L_0x0077
            android.view.View r2 = r6.c
            java.lang.Runnable r3 = r6.d
            int r4 = r6.h
            long r4 = (long) r4
            android.support.v4.view.bx.a(r2, r3, r4)
        L_0x0074:
            r6.l = r1
            goto L_0x000e
        L_0x0077:
            java.lang.Runnable r2 = r6.d
            r2.run()
            goto L_0x0074
        L_0x007d:
            r6.b()
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.a.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }
}
