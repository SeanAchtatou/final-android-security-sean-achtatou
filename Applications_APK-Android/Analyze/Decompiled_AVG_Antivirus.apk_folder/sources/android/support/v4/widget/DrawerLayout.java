package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.b.a.a;
import android.support.v4.view.ay;
import android.support.v4.view.bx;
import android.support.v4.view.cv;
import android.support.v4.view.v;
import android.support.v4.view.z;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import java.util.ArrayList;

public class DrawerLayout extends ViewGroup implements w {
    static final n a;
    /* access modifiers changed from: private */
    public static final int[] b = {16842931};
    /* access modifiers changed from: private */
    public static final boolean c = (Build.VERSION.SDK_INT >= 19);
    private static final boolean d;
    private Drawable A;
    private CharSequence B;
    private CharSequence C;
    private Object D;
    private boolean E;
    private Drawable F;
    private Drawable G;
    private Drawable H;
    private Drawable I;
    private final ArrayList J;
    private final m e;
    private float f;
    private int g;
    private int h;
    private float i;
    private Paint j;
    private final bu k;
    private final bu l;
    private final s m;
    private final s n;
    private int o;
    private boolean p;
    private boolean q;
    private int r;
    private int s;
    private boolean t;
    private boolean u;
    private q v;
    private float w;
    private float x;
    private Drawable y;
    private Drawable z;

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int a = 0;
        float b;
        boolean c;
        boolean d;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.b);
            this.a = obtainStyledAttributes.getInt(0, 0);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.MarginLayoutParams) layoutParams);
            this.a = layoutParams.a;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }
    }

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new r();
        int a = 0;
        int b = 0;
        int c = 0;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readInt();
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
        }
    }

    static {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT < 21) {
            z2 = false;
        }
        d = z2;
        if (Build.VERSION.SDK_INT >= 21) {
            a = new o();
        } else {
            a = new p();
        }
    }

    public DrawerLayout(Context context) {
        this(context, null);
    }

    public DrawerLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, int):void
     arg types: [android.support.v4.widget.DrawerLayout, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, float):void
      android.support.v4.view.bx.c(android.view.View, int):void */
    public DrawerLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.e = new m(this);
        this.h = -1728053248;
        this.j = new Paint();
        this.q = true;
        this.F = null;
        this.G = null;
        this.H = null;
        this.I = null;
        setDescendantFocusability(262144);
        float f2 = getResources().getDisplayMetrics().density;
        this.g = (int) ((64.0f * f2) + 0.5f);
        float f3 = 400.0f * f2;
        this.m = new s(this, 3);
        this.n = new s(this, 5);
        this.k = bu.a(this, 1.0f, this.m);
        this.k.a(1);
        this.k.a(f3);
        this.m.a(this.k);
        this.l = bu.a(this, 1.0f, this.n);
        this.l.a(2);
        this.l.a(f3);
        this.n.a(this.l);
        setFocusableInTouchMode(true);
        bx.c((View) this, 1);
        bx.a(this, new l(this));
        cv.a(this);
        if (bx.y(this)) {
            a.a((View) this);
            this.y = a.a(context);
        }
        this.f = f2 * 10.0f;
        this.J = new ArrayList();
    }

    static void a(View view, float f2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f2 != layoutParams.b) {
            layoutParams.b = f2;
        }
    }

    private void a(View view, boolean z2) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((z2 || d(childAt)) && (!z2 || childAt != view)) {
                bx.c(childAt, 4);
            } else {
                bx.c(childAt, 1);
            }
        }
    }

    private void a(boolean z2) {
        int childCount = getChildCount();
        boolean z3 = false;
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (d(childAt) && (!z2 || layoutParams.c)) {
                z3 = a(childAt, 3) ? z3 | this.k.a(childAt, -childAt.getWidth(), childAt.getTop()) : z3 | this.l.a(childAt, getWidth(), childAt.getTop());
                layoutParams.c = false;
            }
        }
        this.m.a();
        this.n.a();
        if (z3) {
            invalidate();
        }
    }

    private static boolean a(Drawable drawable, int i2) {
        if (drawable == null || !a.b(drawable)) {
            return false;
        }
        a.b(drawable, i2);
        return true;
    }

    static float b(View view) {
        return ((LayoutParams) view.getLayoutParams()).b;
    }

    private void b(int i2, int i3) {
        int a2 = v.a(i3, bx.h(this));
        if (a2 == 3) {
            this.r = i2;
        } else if (a2 == 5) {
            this.s = i2;
        }
        if (i2 != 0) {
            (a2 == 3 ? this.k : this.l).e();
        }
        switch (i2) {
            case 1:
                View b2 = b(a2);
                if (b2 != null) {
                    f(b2);
                    return;
                }
                return;
            case 2:
                View b3 = b(a2);
                if (b3 != null) {
                    e(b3);
                    return;
                }
                return;
            default:
                return;
        }
    }

    static boolean d(View view) {
        return (v.a(((LayoutParams) view.getLayoutParams()).a, bx.h(view)) & 7) != 0;
    }

    private View f() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (((LayoutParams) childAt.getLayoutParams()).d) {
                return childAt;
            }
        }
        return null;
    }

    private void g() {
        Drawable drawable;
        Drawable drawable2;
        if (!d) {
            int h2 = bx.h(this);
            if (h2 == 0) {
                if (this.F != null) {
                    a(this.F, h2);
                    drawable = this.F;
                }
                drawable = this.H;
            } else {
                if (this.G != null) {
                    a(this.G, h2);
                    drawable = this.G;
                }
                drawable = this.H;
            }
            this.z = drawable;
            int h3 = bx.h(this);
            if (h3 == 0) {
                if (this.G != null) {
                    a(this.G, h3);
                    drawable2 = this.G;
                }
                drawable2 = this.I;
            } else {
                if (this.F != null) {
                    a(this.F, h3);
                    drawable2 = this.F;
                }
                drawable2 = this.I;
            }
            this.A = drawable2;
        }
    }

    public static boolean g(View view) {
        if (d(view)) {
            return ((LayoutParams) view.getLayoutParams()).d;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    /* access modifiers changed from: private */
    public View h() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (d(childAt)) {
                if (!d(childAt)) {
                    throw new IllegalArgumentException("View " + childAt + " is not a drawer");
                }
                if (((LayoutParams) childAt.getLayoutParams()).b > 0.0f) {
                    return childAt;
                }
            }
        }
        return null;
    }

    static /* synthetic */ boolean h(View view) {
        return (bx.e(view) == 4 || bx.e(view) == 2) ? false : true;
    }

    private static boolean i(View view) {
        return ((LayoutParams) view.getLayoutParams()).a == 0;
    }

    public final int a(View view) {
        int c2 = c(view);
        if (c2 == 3) {
            return this.r;
        }
        if (c2 == 5) {
            return this.s;
        }
        return 0;
    }

    public final CharSequence a(int i2) {
        int a2 = v.a(i2, bx.h(this));
        if (a2 == 3) {
            return this.B;
        }
        if (a2 == 5) {
            return this.C;
        }
        return null;
    }

    public final void a() {
        this.h = 0;
        invalidate();
    }

    public final void a(int i2, int i3) {
        Drawable drawable = getResources().getDrawable(i2);
        if (!d) {
            if ((i3 & GravityCompat.START) == 8388611) {
                this.F = drawable;
            } else if ((i3 & GravityCompat.END) == 8388613) {
                this.G = drawable;
            } else if ((i3 & 3) == 3) {
                this.H = drawable;
            } else if ((i3 & 5) == 5) {
                this.I = drawable;
            } else {
                return;
            }
            g();
            invalidate();
        }
    }

    public final void a(int i2, View view) {
        if (!d(view)) {
            throw new IllegalArgumentException("View " + view + " is not a drawer with appropriate layout_gravity");
        }
        b(i2, ((LayoutParams) view.getLayoutParams()).a);
    }

    public final void a(q qVar) {
        this.v = qVar;
    }

    public final void a(Object obj, boolean z2) {
        this.D = obj;
        this.E = z2;
        setWillNotDraw(!z2 && getBackground() == null);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public final boolean a(View view, int i2) {
        return (c(view) & i2) == i2;
    }

    public void addFocusables(ArrayList arrayList, int i2, int i3) {
        if (getDescendantFocusability() != 393216) {
            int childCount = getChildCount();
            boolean z2 = false;
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = getChildAt(i4);
                if (!d(childAt)) {
                    this.J.add(childAt);
                } else if (g(childAt)) {
                    z2 = true;
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
            if (!z2) {
                int size = this.J.size();
                for (int i5 = 0; i5 < size; i5++) {
                    View view = (View) this.J.get(i5);
                    if (view.getVisibility() == 0) {
                        view.addFocusables(arrayList, i2, i3);
                    }
                }
            }
            this.J.clear();
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i2, layoutParams);
        if (f() != null || d(view)) {
            bx.c(view, 4);
        } else {
            bx.c(view, 1);
        }
        if (!c) {
            bx.a(view, this.e);
        }
    }

    /* access modifiers changed from: package-private */
    public final View b(int i2) {
        int a2 = v.a(i2, bx.h(this)) & 7;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if ((c(childAt) & 7) == a2) {
                return childAt;
            }
        }
        return null;
    }

    public final void b() {
        a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.a(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.a(android.graphics.drawable.Drawable, int):boolean
      android.support.v4.widget.DrawerLayout.a(int, int):void
      android.support.v4.widget.DrawerLayout.a(int, android.view.View):void
      android.support.v4.widget.DrawerLayout.a(java.lang.Object, boolean):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, int):boolean
      android.support.v4.widget.w.a(java.lang.Object, boolean):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public final void b(int i2, View view) {
        View rootView;
        int a2 = this.k.a();
        int a3 = this.l.a();
        int i3 = (a2 == 1 || a3 == 1) ? 1 : (a2 == 2 || a3 == 2) ? 2 : 0;
        if (view != null && i2 == 0) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (layoutParams.b == 0.0f) {
                LayoutParams layoutParams2 = (LayoutParams) view.getLayoutParams();
                if (layoutParams2.d) {
                    layoutParams2.d = false;
                    if (this.v != null) {
                        this.v.b(view);
                    }
                    a(view, false);
                    if (hasWindowFocus() && (rootView = getRootView()) != null) {
                        rootView.sendAccessibilityEvent(32);
                    }
                }
            } else if (layoutParams.b == 1.0f) {
                LayoutParams layoutParams3 = (LayoutParams) view.getLayoutParams();
                if (!layoutParams3.d) {
                    layoutParams3.d = true;
                    if (this.v != null) {
                        this.v.a(view);
                    }
                    a(view, true);
                    if (hasWindowFocus()) {
                        sendAccessibilityEvent(32);
                    }
                    view.requestFocus();
                }
            }
        }
        if (i3 != this.o) {
            this.o = i3;
        }
    }

    /* access modifiers changed from: package-private */
    public final int c(View view) {
        return v.a(((LayoutParams) view.getLayoutParams()).a, bx.h(this));
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (!this.u) {
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                getChildAt(i2).dispatchTouchEvent(obtain);
            }
            obtain.recycle();
            this.u = true;
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f2 = 0.0f;
        for (int i2 = 0; i2 < childCount; i2++) {
            f2 = Math.max(f2, ((LayoutParams) getChildAt(i2).getLayoutParams()).b);
        }
        this.i = f2;
        if (this.k.g() || this.l.g()) {
            bx.d(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        int i2;
        int height = getHeight();
        boolean i3 = i(view);
        int i4 = 0;
        int width = getWidth();
        int save = canvas.save();
        if (i3) {
            int childCount = getChildCount();
            int i5 = 0;
            while (i5 < childCount) {
                View childAt = getChildAt(i5);
                if (childAt != view && childAt.getVisibility() == 0) {
                    Drawable background = childAt.getBackground();
                    if ((background != null ? background.getOpacity() == -1 : false) && d(childAt) && childAt.getHeight() >= height) {
                        if (a(childAt, 3)) {
                            int right = childAt.getRight();
                            if (right <= i4) {
                                right = i4;
                            }
                            i4 = right;
                            i2 = width;
                        } else {
                            i2 = childAt.getLeft();
                            if (i2 < width) {
                            }
                        }
                        i5++;
                        width = i2;
                    }
                }
                i2 = width;
                i5++;
                width = i2;
            }
            canvas.clipRect(i4, 0, width, getHeight());
        }
        int i6 = width;
        boolean drawChild = super.drawChild(canvas, view, j2);
        canvas.restoreToCount(save);
        if (this.i > 0.0f && i3) {
            this.j.setColor((((int) (((float) ((this.h & -16777216) >>> 24)) * this.i)) << 24) | (this.h & ViewCompat.MEASURED_SIZE_MASK));
            canvas.drawRect((float) i4, 0.0f, (float) i6, (float) getHeight(), this.j);
        } else if (this.z != null && a(view, 3)) {
            int intrinsicWidth = this.z.getIntrinsicWidth();
            int right2 = view.getRight();
            float max = Math.max(0.0f, Math.min(((float) right2) / ((float) this.k.b()), 1.0f));
            this.z.setBounds(right2, view.getTop(), intrinsicWidth + right2, view.getBottom());
            this.z.setAlpha((int) (255.0f * max));
            this.z.draw(canvas);
        } else if (this.A != null && a(view, 5)) {
            int intrinsicWidth2 = this.A.getIntrinsicWidth();
            int left = view.getLeft();
            float max2 = Math.max(0.0f, Math.min(((float) (getWidth() - left)) / ((float) this.l.b()), 1.0f));
            this.A.setBounds(left - intrinsicWidth2, view.getTop(), left, view.getBottom());
            this.A.setAlpha((int) (255.0f * max2));
            this.A.draw(canvas);
        }
        return drawChild;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.a(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.a(android.graphics.drawable.Drawable, int):boolean
      android.support.v4.widget.DrawerLayout.a(int, int):void
      android.support.v4.widget.DrawerLayout.a(int, android.view.View):void
      android.support.v4.widget.DrawerLayout.a(java.lang.Object, boolean):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, int):boolean
      android.support.v4.widget.w.a(java.lang.Object, boolean):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void */
    public final void e(View view) {
        if (!d(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.q) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            layoutParams.b = 1.0f;
            layoutParams.d = true;
            a(view, true);
        } else if (a(view, 3)) {
            this.k.a(view, 0, view.getTop());
        } else {
            this.l.a(view, getWidth() - view.getWidth(), view.getTop());
        }
        invalidate();
    }

    public final void f(View view) {
        if (!d(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.q) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            layoutParams.b = 0.0f;
            layoutParams.d = false;
        } else if (a(view, 3)) {
            this.k.a(view, -view.getWidth(), view.getTop());
        } else {
            this.l.a(view, getWidth(), view.getTop());
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams ? new LayoutParams((LayoutParams) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.q = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.q = true;
    }

    public void onDraw(Canvas canvas) {
        int a2;
        super.onDraw(canvas);
        if (this.E && this.y != null && (a2 = a.a(this.D)) > 0) {
            this.y.setBounds(0, 0, getWidth(), a2);
            this.y.draw(canvas);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        View b2;
        boolean z3;
        int a2 = ay.a(motionEvent);
        boolean a3 = this.k.a(motionEvent) | this.l.a(motionEvent);
        switch (a2) {
            case 0:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                this.w = x2;
                this.x = y2;
                z2 = this.i > 0.0f && (b2 = this.k.b((int) x2, (int) y2)) != null && i(b2);
                this.t = false;
                this.u = false;
                break;
            case 1:
            case 3:
                a(true);
                this.t = false;
                this.u = false;
                z2 = false;
                break;
            case 2:
                if (this.k.h()) {
                    this.m.a();
                    this.n.a();
                    z2 = false;
                    break;
                }
                z2 = false;
                break;
            default:
                z2 = false;
                break;
        }
        if (!a3 && !z2) {
            int childCount = getChildCount();
            int i2 = 0;
            while (true) {
                if (i2 >= childCount) {
                    z3 = false;
                } else if (((LayoutParams) getChildAt(i2).getLayoutParams()).c) {
                    z3 = true;
                } else {
                    i2++;
                }
            }
            if (!z3 && !this.u) {
                return false;
            }
        }
        return true;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            if (h() != null) {
                z.c(keyEvent);
                return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyUp(i2, keyEvent);
        }
        View h2 = h();
        if (h2 != null && a(h2) == 0) {
            a(false);
        }
        return h2 != null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        float f2;
        this.p = true;
        int i7 = i4 - i2;
        int childCount = getChildCount();
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (i(childAt)) {
                    childAt.layout(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.leftMargin + childAt.getMeasuredWidth(), layoutParams.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (a(childAt, 3)) {
                        i6 = ((int) (((float) measuredWidth) * layoutParams.b)) + (-measuredWidth);
                        f2 = ((float) (measuredWidth + i6)) / ((float) measuredWidth);
                    } else {
                        i6 = i7 - ((int) (((float) measuredWidth) * layoutParams.b));
                        f2 = ((float) (i7 - i6)) / ((float) measuredWidth);
                    }
                    boolean z3 = f2 != layoutParams.b;
                    switch (layoutParams.a & 112) {
                        case 16:
                            int i9 = i5 - i3;
                            int i10 = (i9 - measuredHeight) / 2;
                            if (i10 < layoutParams.topMargin) {
                                i10 = layoutParams.topMargin;
                            } else if (i10 + measuredHeight > i9 - layoutParams.bottomMargin) {
                                i10 = (i9 - layoutParams.bottomMargin) - measuredHeight;
                            }
                            childAt.layout(i6, i10, measuredWidth + i6, measuredHeight + i10);
                            break;
                        case 80:
                            int i11 = i5 - i3;
                            childAt.layout(i6, (i11 - layoutParams.bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i6, i11 - layoutParams.bottomMargin);
                            break;
                        default:
                            childAt.layout(i6, layoutParams.topMargin, measuredWidth + i6, measuredHeight + layoutParams.topMargin);
                            break;
                    }
                    if (z3) {
                        a(childAt, f2);
                    }
                    int i12 = layoutParams.b > 0.0f ? 0 : 4;
                    if (childAt.getVisibility() != i12) {
                        childAt.setVisibility(i12);
                    }
                }
            }
        }
        this.p = false;
        this.q = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0028, code lost:
        if (r5 == 0) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r13, int r14) {
        /*
            r12 = this;
            r1 = 300(0x12c, float:4.2E-43)
            r4 = 0
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            r11 = 1073741824(0x40000000, float:2.0)
            int r3 = android.view.View.MeasureSpec.getMode(r13)
            int r5 = android.view.View.MeasureSpec.getMode(r14)
            int r2 = android.view.View.MeasureSpec.getSize(r13)
            int r0 = android.view.View.MeasureSpec.getSize(r14)
            if (r3 != r11) goto L_0x001b
            if (r5 == r11) goto L_0x0135
        L_0x001b:
            boolean r6 = r12.isInEditMode()
            if (r6 == 0) goto L_0x008d
            if (r3 == r7) goto L_0x0026
            if (r3 != 0) goto L_0x0026
            r2 = r1
        L_0x0026:
            if (r5 == r7) goto L_0x0135
            if (r5 != 0) goto L_0x0135
        L_0x002a:
            r12.setMeasuredDimension(r2, r1)
            java.lang.Object r0 = r12.D
            if (r0 == 0) goto L_0x0095
            boolean r0 = android.support.v4.view.bx.y(r12)
            if (r0 == 0) goto L_0x0095
            r0 = 1
            r3 = r0
        L_0x0039:
            int r5 = android.support.v4.view.bx.h(r12)
            int r6 = r12.getChildCount()
        L_0x0041:
            if (r4 >= r6) goto L_0x0134
            android.view.View r7 = r12.getChildAt(r4)
            int r0 = r7.getVisibility()
            r8 = 8
            if (r0 == r8) goto L_0x008a
            android.view.ViewGroup$LayoutParams r0 = r7.getLayoutParams()
            android.support.v4.widget.DrawerLayout$LayoutParams r0 = (android.support.v4.widget.DrawerLayout.LayoutParams) r0
            if (r3 == 0) goto L_0x006a
            int r8 = r0.a
            int r8 = android.support.v4.view.v.a(r8, r5)
            boolean r9 = android.support.v4.view.bx.y(r7)
            if (r9 == 0) goto L_0x0097
            android.support.v4.widget.n r9 = android.support.v4.widget.DrawerLayout.a
            java.lang.Object r10 = r12.D
            r9.a(r7, r10, r8)
        L_0x006a:
            boolean r8 = i(r7)
            if (r8 == 0) goto L_0x009f
            int r8 = r0.leftMargin
            int r8 = r2 - r8
            int r9 = r0.rightMargin
            int r8 = r8 - r9
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r8, r11)
            int r9 = r0.topMargin
            int r9 = r1 - r9
            int r0 = r0.bottomMargin
            int r0 = r9 - r0
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r11)
            r7.measure(r8, r0)
        L_0x008a:
            int r4 = r4 + 1
            goto L_0x0041
        L_0x008d:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "DrawerLayout must be measured with MeasureSpec.EXACTLY."
            r0.<init>(r1)
            throw r0
        L_0x0095:
            r3 = r4
            goto L_0x0039
        L_0x0097:
            android.support.v4.widget.n r9 = android.support.v4.widget.DrawerLayout.a
            java.lang.Object r10 = r12.D
            r9.a(r0, r10, r8)
            goto L_0x006a
        L_0x009f:
            boolean r8 = d(r7)
            if (r8 == 0) goto L_0x010f
            boolean r8 = android.support.v4.widget.DrawerLayout.d
            if (r8 == 0) goto L_0x00b8
            float r8 = android.support.v4.view.bx.v(r7)
            float r9 = r12.f
            int r8 = (r8 > r9 ? 1 : (r8 == r9 ? 0 : -1))
            if (r8 == 0) goto L_0x00b8
            float r8 = r12.f
            android.support.v4.view.bx.f(r7, r8)
        L_0x00b8:
            int r8 = r12.c(r7)
            r8 = r8 & 7
            r9 = r8 & 0
            if (r9 == 0) goto L_0x00f1
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "Child drawer has absolute gravity "
            r2.<init>(r0)
            r0 = r8 & 3
            r3 = 3
            if (r0 != r3) goto L_0x00e4
            java.lang.String r0 = "LEFT"
        L_0x00d2:
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = " but this DrawerLayout already has a drawer view along that edge"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x00e4:
            r0 = r8 & 5
            r3 = 5
            if (r0 != r3) goto L_0x00ec
            java.lang.String r0 = "RIGHT"
            goto L_0x00d2
        L_0x00ec:
            java.lang.String r0 = java.lang.Integer.toHexString(r8)
            goto L_0x00d2
        L_0x00f1:
            int r8 = r12.g
            int r9 = r0.leftMargin
            int r8 = r8 + r9
            int r9 = r0.rightMargin
            int r8 = r8 + r9
            int r9 = r0.width
            int r8 = getChildMeasureSpec(r13, r8, r9)
            int r9 = r0.topMargin
            int r10 = r0.bottomMargin
            int r9 = r9 + r10
            int r0 = r0.height
            int r0 = getChildMeasureSpec(r14, r9, r0)
            r7.measure(r8, r0)
            goto L_0x008a
        L_0x010f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Child "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r2 = " at index "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r2 = " does not have a valid layout_gravity - must be Gravity.LEFT, Gravity.RIGHT or Gravity.NO_GRAVITY"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0134:
            return
        L_0x0135:
            r1 = r0
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        View b2;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.a == 0 || (b2 = b(savedState.a)) == null)) {
            e(b2);
        }
        b(savedState.b, 3);
        b(savedState.c, 5);
    }

    public void onRtlPropertiesChanged(int i2) {
        g();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        View f2 = f();
        if (f2 != null) {
            savedState.a = ((LayoutParams) f2.getLayoutParams()).a;
        }
        savedState.b = this.r;
        savedState.c = this.s;
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        View f2;
        this.k.b(motionEvent);
        this.l.b(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                this.w = x2;
                this.x = y2;
                this.t = false;
                this.u = false;
                break;
            case 1:
                float x3 = motionEvent.getX();
                float y3 = motionEvent.getY();
                View b2 = this.k.b((int) x3, (int) y3);
                if (b2 != null && i(b2)) {
                    float f3 = x3 - this.w;
                    float f4 = y3 - this.x;
                    int d2 = this.k.d();
                    if ((f3 * f3) + (f4 * f4) < ((float) (d2 * d2)) && (f2 = f()) != null) {
                        z2 = a(f2) == 2;
                        a(z2);
                        this.t = false;
                        break;
                    }
                }
                z2 = true;
                a(z2);
                this.t = false;
            case 3:
                a(true);
                this.t = false;
                this.u = false;
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        super.requestDisallowInterceptTouchEvent(z2);
        this.t = z2;
        if (z2) {
            a(true);
        }
    }

    public void requestLayout() {
        if (!this.p) {
            super.requestLayout();
        }
    }
}
