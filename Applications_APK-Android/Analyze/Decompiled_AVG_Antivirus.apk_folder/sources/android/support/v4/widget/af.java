package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class af extends Animation {
    final /* synthetic */ ai a;
    final /* synthetic */ ae b;

    af(ae aeVar, ai aiVar) {
        this.b = aeVar;
        this.a = aiVar;
    }

    public final void applyTransformation(float f, Transformation transformation) {
        if (this.b.a) {
            ae.a(f, this.a);
            return;
        }
        float a2 = ae.b(this.a);
        float h = this.a.h();
        float g = this.a.g();
        float l = this.a.l();
        ae.c(f, this.a);
        if (f <= 0.5f) {
            this.a.b(g + (ae.c.getInterpolation(f / 0.5f) * (0.8f - a2)));
        }
        if (f > 0.5f) {
            this.a.c(((0.8f - a2) * ae.c.getInterpolation((f - 0.5f) / 0.5f)) + h);
        }
        this.a.d((0.25f * f) + l);
        this.b.d((216.0f * f) + (1080.0f * (this.b.k / 5.0f)));
    }
}
