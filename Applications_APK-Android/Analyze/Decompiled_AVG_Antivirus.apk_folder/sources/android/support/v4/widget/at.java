package android.support.v4.widget;

import android.content.Context;
import android.os.Build;
import android.view.animation.Interpolator;

public final class at {
    Object a;
    au b;

    private at(int i, Context context, Interpolator interpolator) {
        if (i >= 14) {
            this.b = new ax();
        } else if (i >= 9) {
            this.b = new aw();
        } else {
            this.b = new av();
        }
        this.a = this.b.a(context, interpolator);
    }

    at(Context context, Interpolator interpolator) {
        this(Build.VERSION.SDK_INT, context, interpolator);
    }

    public static at a(Context context) {
        return a(context, null);
    }

    public static at a(Context context, Interpolator interpolator) {
        return new at(context, interpolator);
    }

    public final void a(int i, int i2, int i3, int i4, int i5) {
        this.b.a(this.a, i, i2, i3, i4, i5);
    }

    public final void a(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this.b.a(this.a, i, i2, i3, i4, i5, i6, i7);
    }

    public final boolean a() {
        return this.b.a(this.a);
    }

    public final boolean a(int i, int i2, int i3) {
        return this.b.b(this.a, i, i2, i3);
    }

    public final int b() {
        return this.b.b(this.a);
    }

    public final int c() {
        return this.b.c(this.a);
    }

    public final int d() {
        return this.b.g(this.a);
    }

    public final int e() {
        return this.b.h(this.a);
    }

    public final float f() {
        return this.b.d(this.a);
    }

    public final boolean g() {
        return this.b.e(this.a);
    }

    public final void h() {
        this.b.f(this.a);
    }
}
