package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class bh extends Animation {
    final /* synthetic */ SwipeRefreshLayout a;

    bh(SwipeRefreshLayout swipeRefreshLayout) {
        this.a = swipeRefreshLayout;
    }

    public final void applyTransformation(float f, Transformation transformation) {
        this.a.a(1.0f - f);
    }
}
