package android.support.v4.media;

import android.media.MediaDescription;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

final class c implements Parcelable.Creator {
    c() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return Build.VERSION.SDK_INT < 21 ? new MediaDescriptionCompat(parcel, (byte) 0) : MediaDescriptionCompat.a(MediaDescription.CREATOR.createFromParcel(parcel));
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new MediaDescriptionCompat[i];
    }
}
