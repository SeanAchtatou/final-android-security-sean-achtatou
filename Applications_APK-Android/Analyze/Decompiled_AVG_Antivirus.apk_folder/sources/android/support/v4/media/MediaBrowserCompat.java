package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

public final class MediaBrowserCompat {

    public class MediaItem implements Parcelable {
        public static final Parcelable.Creator CREATOR = new b();
        private final int a;
        private final MediaDescriptionCompat b;

        private MediaItem(Parcel parcel) {
            this.a = parcel.readInt();
            this.b = (MediaDescriptionCompat) MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
        }

        /* synthetic */ MediaItem(Parcel parcel, byte b2) {
            this(parcel);
        }

        public int describeContents() {
            return 0;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("MediaItem{");
            sb.append("mFlags=").append(this.a);
            sb.append(", mDescription=").append(this.b);
            sb.append('}');
            return sb.toString();
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            this.b.writeToParcel(parcel, i);
        }
    }
}
