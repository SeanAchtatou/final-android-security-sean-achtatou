package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.session.MediaSessionCompat;

final class b implements Parcelable.Creator {
    b() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new MediaSessionCompat.ResultReceiverWrapper(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new MediaSessionCompat.ResultReceiverWrapper[i];
    }
}
