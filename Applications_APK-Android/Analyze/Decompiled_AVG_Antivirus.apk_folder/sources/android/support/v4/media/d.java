package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

public final class d {
    private String a;
    private CharSequence b;
    private CharSequence c;
    private CharSequence d;
    private Bitmap e;
    private Uri f;
    private Bundle g;
    private Uri h;

    public final MediaDescriptionCompat a() {
        return new MediaDescriptionCompat(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, (byte) 0);
    }

    public final d a(Bitmap bitmap) {
        this.e = bitmap;
        return this;
    }

    public final d a(Uri uri) {
        this.f = uri;
        return this;
    }

    public final d a(Bundle bundle) {
        this.g = bundle;
        return this;
    }

    public final d a(CharSequence charSequence) {
        this.b = charSequence;
        return this;
    }

    public final d a(String str) {
        this.a = str;
        return this;
    }

    public final d b(Uri uri) {
        this.h = uri;
        return this;
    }

    public final d b(CharSequence charSequence) {
        this.c = charSequence;
        return this;
    }

    public final d c(CharSequence charSequence) {
        this.d = charSequence;
        return this;
    }
}
