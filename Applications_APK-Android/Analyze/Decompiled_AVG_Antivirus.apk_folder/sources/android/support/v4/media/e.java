package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

final class e implements Parcelable.Creator {
    e() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new MediaMetadataCompat(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new MediaMetadataCompat[i];
    }
}
