package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.MediaBrowserCompat;

final class b implements Parcelable.Creator {
    b() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new MediaBrowserCompat.MediaItem(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new MediaBrowserCompat.MediaItem[i];
    }
}
