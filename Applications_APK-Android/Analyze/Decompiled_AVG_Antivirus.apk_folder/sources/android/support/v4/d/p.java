package android.support.v4.d;

import android.os.Build;
import java.util.Locale;

public final class p {
    public static final Locale a = new Locale("", "");
    private static final q b;
    /* access modifiers changed from: private */
    public static String c = "Arab";
    /* access modifiers changed from: private */
    public static String d = "Hebr";

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            b = new r((byte) 0);
        } else {
            b = new q((byte) 0);
        }
    }

    public static int a(Locale locale) {
        return b.a(locale);
    }
}
