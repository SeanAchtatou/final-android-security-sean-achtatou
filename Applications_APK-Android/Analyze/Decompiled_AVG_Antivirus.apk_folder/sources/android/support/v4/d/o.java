package android.support.v4.d;

import java.util.Locale;

final class o extends m {
    public static final o a = new o();

    public o() {
        super(null);
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return p.a(Locale.getDefault()) == 1;
    }
}
