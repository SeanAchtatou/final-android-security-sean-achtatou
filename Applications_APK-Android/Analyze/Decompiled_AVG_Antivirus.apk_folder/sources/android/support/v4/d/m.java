package android.support.v4.d;

abstract class m implements h {
    private final l a;

    public m(l lVar) {
        this.a = lVar;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a();

    public final boolean a(CharSequence charSequence, int i) {
        if (charSequence == null || i < 0 || charSequence.length() - i < 0) {
            throw new IllegalArgumentException();
        } else if (this.a == null) {
            return a();
        } else {
            switch (this.a.a(charSequence, i)) {
                case 0:
                    return true;
                case 1:
                    return false;
                default:
                    return a();
            }
        }
    }
}
