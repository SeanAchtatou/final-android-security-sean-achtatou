package android.support.v4.d;

import java.util.Locale;

class q {
    private q() {
    }

    /* synthetic */ q(byte b) {
        this();
    }

    public int a(Locale locale) {
        if (locale != null && !locale.equals(p.a)) {
            String a = a.a(locale);
            if (a == null) {
                switch (Character.getDirectionality(locale.getDisplayName(locale).charAt(0))) {
                    case 1:
                    case 2:
                        return 1;
                    default:
                        return 0;
                }
            } else if (a.equalsIgnoreCase(p.c) || a.equalsIgnoreCase(p.d)) {
                return 1;
            }
        }
        return 0;
    }
}
