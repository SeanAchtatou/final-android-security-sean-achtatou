package android.support.v4.d;

import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

public final class f {
    private static Method a;

    static {
        try {
            a = Class.forName("libcore.icu.ICU").getMethod("addLikelySubtags", Locale.class);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public static String a(Locale locale) {
        try {
            return ((Locale) a.invoke(null, locale)).getScript();
        } catch (InvocationTargetException e) {
            Log.w("ICUCompatIcs", e);
        } catch (IllegalAccessException e2) {
            Log.w("ICUCompatIcs", e2);
        }
        return locale.getScript();
    }
}
