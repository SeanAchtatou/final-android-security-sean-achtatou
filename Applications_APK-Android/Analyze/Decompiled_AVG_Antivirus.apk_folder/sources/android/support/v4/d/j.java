package android.support.v4.d;

final class j implements l {
    public static final j a = new j(true);
    public static final j b = new j(false);
    private final boolean c;

    private j(boolean z) {
        this.c = z;
    }

    public final int a(CharSequence charSequence, int i) {
        int i2 = i + 0;
        boolean z = false;
        for (int i3 = 0; i3 < i2; i3++) {
            switch (i.b(Character.getDirectionality(charSequence.charAt(i3)))) {
                case 0:
                    if (!this.c) {
                        z = true;
                        break;
                    } else {
                        return 0;
                    }
                case 1:
                    if (this.c) {
                        z = true;
                        break;
                    } else {
                        return 1;
                    }
            }
        }
        if (z) {
            return !this.c ? 0 : 1;
        }
        return 2;
    }
}
