package android.support.v4.d;

import android.text.TextUtils;
import java.util.Locale;

final class r extends q {
    private r() {
        super((byte) 0);
    }

    /* synthetic */ r(byte b) {
        this();
    }

    public final int a(Locale locale) {
        return TextUtils.getLayoutDirectionFromLocale(locale);
    }
}
