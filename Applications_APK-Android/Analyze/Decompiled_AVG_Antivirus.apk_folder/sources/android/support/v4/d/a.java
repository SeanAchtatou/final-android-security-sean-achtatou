package android.support.v4.d;

import android.os.Build;
import java.util.Locale;

public final class a {
    private static final b a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            a = new e();
        } else if (i >= 14) {
            a = new d();
        } else {
            a = new c();
        }
    }

    public static String a(Locale locale) {
        return a.a(locale);
    }
}
