package android.support.v4.d;

final class k implements l {
    public static final k a = new k();

    private k() {
    }

    public final int a(CharSequence charSequence, int i) {
        int i2 = i + 0;
        int i3 = 2;
        for (int i4 = 0; i4 < i2 && i3 == 2; i4++) {
            i3 = i.a(Character.getDirectionality(charSequence.charAt(i4)));
        }
        return i3;
    }
}
