package android.support.v4.view;

import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;

public final class dv {
    static final ee a;
    private WeakReference b;
    /* access modifiers changed from: private */
    public Runnable c = null;
    /* access modifiers changed from: private */
    public Runnable d = null;
    /* access modifiers changed from: private */
    public int e = -1;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            a = new ed();
        } else if (i >= 19) {
            a = new ec();
        } else if (i >= 18) {
            a = new ea();
        } else if (i >= 16) {
            a = new eb();
        } else if (i >= 14) {
            a = new dy();
        } else {
            a = new dw();
        }
    }

    dv(View view) {
        this.b = new WeakReference(view);
    }

    public final dv a() {
        View view = (View) this.b.get();
        if (view != null) {
            a.a(this, view);
        }
        return this;
    }

    public final dv a(float f) {
        View view = (View) this.b.get();
        if (view != null) {
            a.a(this, view, f);
        }
        return this;
    }

    public final dv a(long j) {
        View view = (View) this.b.get();
        if (view != null) {
            a.a(view, j);
        }
        return this;
    }

    public final dv a(el elVar) {
        View view = (View) this.b.get();
        if (view != null) {
            a.a(this, view, elVar);
        }
        return this;
    }

    public final dv a(en enVar) {
        View view = (View) this.b.get();
        if (view != null) {
            a.a(view, enVar);
        }
        return this;
    }

    public final dv a(Interpolator interpolator) {
        View view = (View) this.b.get();
        if (view != null) {
            a.a(view, interpolator);
        }
        return this;
    }

    public final dv b(float f) {
        View view = (View) this.b.get();
        if (view != null) {
            a.b(this, view, f);
        }
        return this;
    }

    public final dv b(long j) {
        View view = (View) this.b.get();
        if (view != null) {
            a.b(view, j);
        }
        return this;
    }

    public final void b() {
        View view = (View) this.b.get();
        if (view != null) {
            a.b(this, view);
        }
    }

    public final dv c(float f) {
        View view = (View) this.b.get();
        if (view != null) {
            a.c(this, view, f);
        }
        return this;
    }

    public final void c() {
        View view = (View) this.b.get();
        if (view != null) {
            a.c(this, view);
        }
    }
}
