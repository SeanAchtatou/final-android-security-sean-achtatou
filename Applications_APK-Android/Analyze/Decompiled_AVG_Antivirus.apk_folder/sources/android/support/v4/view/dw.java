package android.support.v4.view;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.WeakHashMap;

class dw implements ee {
    WeakHashMap a = null;

    dw() {
    }

    /* access modifiers changed from: private */
    public void d(dv dvVar, View view) {
        Object tag = view.getTag(2113929216);
        el elVar = tag instanceof el ? (el) tag : null;
        Runnable a2 = dvVar.c;
        Runnable b = dvVar.d;
        if (a2 != null) {
            a2.run();
        }
        if (elVar != null) {
            elVar.a(view);
            elVar.b(view);
        }
        if (b != null) {
            b.run();
        }
        if (this.a != null) {
            this.a.remove(view);
        }
    }

    private void e(dv dvVar, View view) {
        Runnable runnable = null;
        if (this.a != null) {
            runnable = (Runnable) this.a.get(view);
        }
        if (runnable == null) {
            runnable = new dx(this, dvVar, view, (byte) 0);
            if (this.a == null) {
                this.a = new WeakHashMap();
            }
            this.a.put(view, runnable);
        }
        view.removeCallbacks(runnable);
        view.post(runnable);
    }

    public void a(dv dvVar, View view) {
        e(dvVar, view);
    }

    public void a(dv dvVar, View view, float f) {
        e(dvVar, view);
    }

    public void a(dv dvVar, View view, el elVar) {
        view.setTag(2113929216, elVar);
    }

    public void a(View view, long j) {
    }

    public void a(View view, en enVar) {
    }

    public void a(View view, Interpolator interpolator) {
    }

    public void b(dv dvVar, View view) {
        e(dvVar, view);
    }

    public void b(dv dvVar, View view, float f) {
        e(dvVar, view);
    }

    public void b(View view, long j) {
    }

    public void c(dv dvVar, View view) {
        Runnable runnable;
        if (!(this.a == null || (runnable = (Runnable) this.a.get(view)) == null)) {
            view.removeCallbacks(runnable);
        }
        d(dvVar, view);
    }

    public void c(dv dvVar, View view, float f) {
        e(dvVar, view);
    }
}
