package android.support.v4.view;

import android.view.View;

class ch extends cg {
    ch() {
    }

    public final boolean G(View view) {
        return view.isLaidOut();
    }

    public final boolean I(View view) {
        return view.isAttachedToWindow();
    }

    public final void c(View view, int i) {
        view.setImportantForAccessibility(i);
    }
}
