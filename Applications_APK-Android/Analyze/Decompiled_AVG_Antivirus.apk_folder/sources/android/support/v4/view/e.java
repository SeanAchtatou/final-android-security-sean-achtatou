package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.s;
import android.view.View;
import android.view.accessibility.AccessibilityNodeProvider;

final class e extends b {
    e() {
    }

    public final s a(Object obj, View view) {
        AccessibilityNodeProvider accessibilityNodeProvider = ((View.AccessibilityDelegate) obj).getAccessibilityNodeProvider(view);
        if (accessibilityNodeProvider != null) {
            return new s(accessibilityNodeProvider);
        }
        return null;
    }

    public final Object a(a aVar) {
        return new l(new f(this, aVar));
    }

    public final boolean a(Object obj, View view, int i, Bundle bundle) {
        return ((View.AccessibilityDelegate) obj).performAccessibilityAction(view, i, bundle);
    }
}
