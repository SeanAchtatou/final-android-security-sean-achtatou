package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

interface ck {
    boolean A(View view);

    void B(View view);

    void C(View view);

    boolean D(View view);

    boolean E(View view);

    void F(View view);

    boolean G(View view);

    float H(View view);

    boolean I(View view);

    int a(int i, int i2);

    int a(int i, int i2, int i3);

    int a(View view);

    eo a(View view, eo eoVar);

    void a(View view, float f);

    void a(View view, int i, int i2, int i3, int i4);

    void a(View view, int i, Paint paint);

    void a(View view, Paint paint);

    void a(View view, a aVar);

    void a(View view, bi biVar);

    void a(View view, Runnable runnable);

    void a(View view, Runnable runnable, long j);

    void a(View view, boolean z);

    void a(ViewGroup viewGroup);

    boolean a(View view, int i);

    eo b(View view, eo eoVar);

    void b(View view, float f);

    void b(View view, int i, int i2, int i3, int i4);

    void b(View view, boolean z);

    boolean b(View view);

    boolean b(View view, int i);

    void c(View view, float f);

    void c(View view, int i);

    boolean c(View view);

    void d(View view);

    void d(View view, float f);

    int e(View view);

    void e(View view, float f);

    float f(View view);

    void f(View view, float f);

    int g(View view);

    int h(View view);

    ViewParent i(View view);

    boolean j(View view);

    int k(View view);

    int l(View view);

    int m(View view);

    int n(View view);

    int o(View view);

    boolean p(View view);

    float q(View view);

    float r(View view);

    float s(View view);

    int t(View view);

    int u(View view);

    dv v(View view);

    int w(View view);

    void x(View view);

    float y(View view);
}
