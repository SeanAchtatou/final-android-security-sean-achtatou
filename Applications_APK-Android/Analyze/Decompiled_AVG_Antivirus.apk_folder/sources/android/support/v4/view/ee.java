package android.support.v4.view;

import android.view.View;
import android.view.animation.Interpolator;

interface ee {
    void a(dv dvVar, View view);

    void a(dv dvVar, View view, float f);

    void a(dv dvVar, View view, el elVar);

    void a(View view, long j);

    void a(View view, en enVar);

    void a(View view, Interpolator interpolator);

    void b(dv dvVar, View view);

    void b(dv dvVar, View view, float f);

    void b(View view, long j);

    void c(dv dvVar, View view);

    void c(dv dvVar, View view, float f);
}
