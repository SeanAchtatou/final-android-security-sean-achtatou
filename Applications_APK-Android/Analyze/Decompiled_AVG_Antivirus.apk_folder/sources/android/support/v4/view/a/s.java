package android.support.v4.view.a;

import android.os.Build;
import java.util.List;

public final class s {
    private static final t a;
    private final Object b;

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            a = new w();
        } else if (Build.VERSION.SDK_INT >= 16) {
            a = new u();
        } else {
            a = new y();
        }
    }

    public s() {
        this.b = a.a(this);
    }

    public s(Object obj) {
        this.b = obj;
    }

    public static f b() {
        return null;
    }

    public static boolean c() {
        return false;
    }

    public static List d() {
        return null;
    }

    public static f e() {
        return null;
    }

    public final Object a() {
        return this.b;
    }
}
