package android.support.v4.view;

import android.database.DataSetObserver;

final class dl extends DataSetObserver {
    final /* synthetic */ ViewPager a;

    private dl(ViewPager viewPager) {
        this.a = viewPager;
    }

    /* synthetic */ dl(ViewPager viewPager, byte b) {
        this(viewPager);
    }

    public final void onChanged() {
        this.a.c();
    }

    public final void onInvalidated() {
        this.a.c();
    }
}
