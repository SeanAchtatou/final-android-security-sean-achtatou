package android.support.v4.view;

import android.content.Context;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

final class s implements r {
    private static final int e = ViewConfiguration.getLongPressTimeout();
    private static final int f = ViewConfiguration.getTapTimeout();
    private static final int g = ViewConfiguration.getDoubleTapTimeout();
    private int a;
    private int b;
    private int c;
    private int d;
    private final Handler h = new t(this);
    /* access modifiers changed from: private */
    public final GestureDetector.OnGestureListener i;
    /* access modifiers changed from: private */
    public GestureDetector.OnDoubleTapListener j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    private boolean m;
    private boolean n;
    private boolean o;
    /* access modifiers changed from: private */
    public MotionEvent p;
    private MotionEvent q;
    private boolean r;
    private float s;
    private float t;
    private float u;
    private float v;
    private boolean w;
    private VelocityTracker x;

    public s(Context context, GestureDetector.OnGestureListener onGestureListener) {
        this.i = onGestureListener;
        if (onGestureListener instanceof GestureDetector.OnDoubleTapListener) {
            this.j = (GestureDetector.OnDoubleTapListener) onGestureListener;
        }
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null");
        } else if (this.i == null) {
            throw new IllegalArgumentException("OnGestureListener must not be null");
        } else {
            this.w = true;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
            int scaledDoubleTapSlop = viewConfiguration.getScaledDoubleTapSlop();
            this.c = viewConfiguration.getScaledMinimumFlingVelocity();
            this.d = viewConfiguration.getScaledMaximumFlingVelocity();
            this.a = scaledTouchSlop * scaledTouchSlop;
            this.b = scaledDoubleTapSlop * scaledDoubleTapSlop;
        }
    }

    static /* synthetic */ void c(s sVar) {
        sVar.h.removeMessages(3);
        sVar.l = false;
        sVar.m = true;
        sVar.i.onLongPress(sVar.p);
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x014c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.view.MotionEvent r14) {
        /*
            r13 = this;
            r7 = 0
            r12 = 2
            r11 = 3
            r4 = 1
            r3 = 0
            int r9 = r14.getAction()
            android.view.VelocityTracker r0 = r13.x
            if (r0 != 0) goto L_0x0013
            android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
            r13.x = r0
        L_0x0013:
            android.view.VelocityTracker r0 = r13.x
            r0.addMovement(r14)
            r0 = r9 & 255(0xff, float:3.57E-43)
            r1 = 6
            if (r0 != r1) goto L_0x003c
            r8 = r4
        L_0x001e:
            if (r8 == 0) goto L_0x003e
            int r0 = android.support.v4.view.ay.b(r14)
        L_0x0024:
            int r5 = android.support.v4.view.ay.c(r14)
            r6 = r3
            r1 = r7
            r2 = r7
        L_0x002b:
            if (r6 >= r5) goto L_0x0040
            if (r0 == r6) goto L_0x0039
            float r10 = android.support.v4.view.ay.c(r14, r6)
            float r2 = r2 + r10
            float r10 = android.support.v4.view.ay.d(r14, r6)
            float r1 = r1 + r10
        L_0x0039:
            int r6 = r6 + 1
            goto L_0x002b
        L_0x003c:
            r8 = r3
            goto L_0x001e
        L_0x003e:
            r0 = -1
            goto L_0x0024
        L_0x0040:
            if (r8 == 0) goto L_0x004e
            int r0 = r5 + -1
        L_0x0044:
            float r6 = (float) r0
            float r2 = r2 / r6
            float r0 = (float) r0
            float r1 = r1 / r0
            r0 = r9 & 255(0xff, float:3.57E-43)
            switch(r0) {
                case 0: goto L_0x00c1;
                case 1: goto L_0x01fe;
                case 2: goto L_0x0189;
                case 3: goto L_0x0293;
                case 4: goto L_0x004d;
                case 5: goto L_0x0050;
                case 6: goto L_0x0076;
                default: goto L_0x004d;
            }
        L_0x004d:
            return r3
        L_0x004e:
            r0 = r5
            goto L_0x0044
        L_0x0050:
            r13.s = r2
            r13.u = r2
            r13.t = r1
            r13.v = r1
            android.os.Handler r0 = r13.h
            r0.removeMessages(r4)
            android.os.Handler r0 = r13.h
            r0.removeMessages(r12)
            android.os.Handler r0 = r13.h
            r0.removeMessages(r11)
            r13.r = r3
            r13.n = r3
            r13.o = r3
            r13.l = r3
            boolean r0 = r13.m
            if (r0 == 0) goto L_0x004d
            r13.m = r3
            goto L_0x004d
        L_0x0076:
            r13.s = r2
            r13.u = r2
            r13.t = r1
            r13.v = r1
            android.view.VelocityTracker r0 = r13.x
            r1 = 1000(0x3e8, float:1.401E-42)
            int r2 = r13.d
            float r2 = (float) r2
            r0.computeCurrentVelocity(r1, r2)
            int r1 = android.support.v4.view.ay.b(r14)
            int r0 = android.support.v4.view.ay.b(r14, r1)
            android.view.VelocityTracker r2 = r13.x
            float r2 = android.support.v4.view.bt.a(r2, r0)
            android.view.VelocityTracker r4 = r13.x
            float r4 = android.support.v4.view.bt.b(r4, r0)
            r0 = r3
        L_0x009d:
            if (r0 >= r5) goto L_0x004d
            if (r0 == r1) goto L_0x00be
            int r6 = android.support.v4.view.ay.b(r14, r0)
            android.view.VelocityTracker r8 = r13.x
            float r8 = android.support.v4.view.bt.a(r8, r6)
            float r8 = r8 * r2
            android.view.VelocityTracker r9 = r13.x
            float r6 = android.support.v4.view.bt.b(r9, r6)
            float r6 = r6 * r4
            float r6 = r6 + r8
            int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r6 >= 0) goto L_0x00be
            android.view.VelocityTracker r0 = r13.x
            r0.clear()
            goto L_0x004d
        L_0x00be:
            int r0 = r0 + 1
            goto L_0x009d
        L_0x00c1:
            android.view.GestureDetector$OnDoubleTapListener r0 = r13.j
            if (r0 == 0) goto L_0x0187
            android.os.Handler r0 = r13.h
            boolean r0 = r0.hasMessages(r11)
            if (r0 == 0) goto L_0x00d2
            android.os.Handler r5 = r13.h
            r5.removeMessages(r11)
        L_0x00d2:
            android.view.MotionEvent r5 = r13.p
            if (r5 == 0) goto L_0x017f
            android.view.MotionEvent r5 = r13.q
            if (r5 == 0) goto L_0x017f
            if (r0 == 0) goto L_0x017f
            android.view.MotionEvent r0 = r13.p
            android.view.MotionEvent r5 = r13.q
            boolean r6 = r13.o
            if (r6 == 0) goto L_0x017d
            long r6 = r14.getEventTime()
            long r8 = r5.getEventTime()
            long r6 = r6 - r8
            int r5 = android.support.v4.view.s.g
            long r8 = (long) r5
            int r5 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r5 > 0) goto L_0x017d
            float r5 = r0.getX()
            int r5 = (int) r5
            float r6 = r14.getX()
            int r6 = (int) r6
            int r5 = r5 - r6
            float r0 = r0.getY()
            int r0 = (int) r0
            float r6 = r14.getY()
            int r6 = (int) r6
            int r0 = r0 - r6
            int r5 = r5 * r5
            int r0 = r0 * r0
            int r0 = r0 + r5
            int r5 = r13.b
            if (r0 >= r5) goto L_0x017d
            r0 = r4
        L_0x0112:
            if (r0 == 0) goto L_0x017f
            r13.r = r4
            android.view.GestureDetector$OnDoubleTapListener r0 = r13.j
            android.view.MotionEvent r5 = r13.p
            boolean r0 = r0.onDoubleTap(r5)
            r0 = r0 | 0
            android.view.GestureDetector$OnDoubleTapListener r5 = r13.j
            boolean r5 = r5.onDoubleTapEvent(r14)
            r0 = r0 | r5
        L_0x0127:
            r13.s = r2
            r13.u = r2
            r13.t = r1
            r13.v = r1
            android.view.MotionEvent r1 = r13.p
            if (r1 == 0) goto L_0x0138
            android.view.MotionEvent r1 = r13.p
            r1.recycle()
        L_0x0138:
            android.view.MotionEvent r1 = android.view.MotionEvent.obtain(r14)
            r13.p = r1
            r13.n = r4
            r13.o = r4
            r13.k = r4
            r13.m = r3
            r13.l = r3
            boolean r1 = r13.w
            if (r1 == 0) goto L_0x0164
            android.os.Handler r1 = r13.h
            r1.removeMessages(r12)
            android.os.Handler r1 = r13.h
            android.view.MotionEvent r2 = r13.p
            long r2 = r2.getDownTime()
            int r5 = android.support.v4.view.s.f
            long r6 = (long) r5
            long r2 = r2 + r6
            int r5 = android.support.v4.view.s.e
            long r6 = (long) r5
            long r2 = r2 + r6
            r1.sendEmptyMessageAtTime(r12, r2)
        L_0x0164:
            android.os.Handler r1 = r13.h
            android.view.MotionEvent r2 = r13.p
            long r2 = r2.getDownTime()
            int r5 = android.support.v4.view.s.f
            long r6 = (long) r5
            long r2 = r2 + r6
            r1.sendEmptyMessageAtTime(r4, r2)
            android.view.GestureDetector$OnGestureListener r1 = r13.i
            boolean r1 = r1.onDown(r14)
            r3 = r0 | r1
            goto L_0x004d
        L_0x017d:
            r0 = r3
            goto L_0x0112
        L_0x017f:
            android.os.Handler r0 = r13.h
            int r5 = android.support.v4.view.s.g
            long r6 = (long) r5
            r0.sendEmptyMessageDelayed(r11, r6)
        L_0x0187:
            r0 = r3
            goto L_0x0127
        L_0x0189:
            boolean r0 = r13.m
            if (r0 != 0) goto L_0x004d
            float r0 = r13.s
            float r0 = r0 - r2
            float r5 = r13.t
            float r5 = r5 - r1
            boolean r6 = r13.r
            if (r6 == 0) goto L_0x01a1
            android.view.GestureDetector$OnDoubleTapListener r0 = r13.j
            boolean r0 = r0.onDoubleTapEvent(r14)
            r3 = r0 | 0
            goto L_0x004d
        L_0x01a1:
            boolean r6 = r13.n
            if (r6 == 0) goto L_0x01dc
            float r6 = r13.u
            float r6 = r2 - r6
            int r6 = (int) r6
            float r7 = r13.v
            float r7 = r1 - r7
            int r7 = (int) r7
            int r6 = r6 * r6
            int r7 = r7 * r7
            int r6 = r6 + r7
            int r7 = r13.a
            if (r6 <= r7) goto L_0x02bf
            android.view.GestureDetector$OnGestureListener r7 = r13.i
            android.view.MotionEvent r8 = r13.p
            boolean r0 = r7.onScroll(r8, r14, r0, r5)
            r13.s = r2
            r13.t = r1
            r13.n = r3
            android.os.Handler r1 = r13.h
            r1.removeMessages(r11)
            android.os.Handler r1 = r13.h
            r1.removeMessages(r4)
            android.os.Handler r1 = r13.h
            r1.removeMessages(r12)
        L_0x01d3:
            int r1 = r13.a
            if (r6 <= r1) goto L_0x01d9
            r13.o = r3
        L_0x01d9:
            r3 = r0
            goto L_0x004d
        L_0x01dc:
            float r4 = java.lang.Math.abs(r0)
            r6 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 >= 0) goto L_0x01f0
            float r4 = java.lang.Math.abs(r5)
            r6 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 < 0) goto L_0x004d
        L_0x01f0:
            android.view.GestureDetector$OnGestureListener r3 = r13.i
            android.view.MotionEvent r4 = r13.p
            boolean r3 = r3.onScroll(r4, r14, r0, r5)
            r13.s = r2
            r13.t = r1
            goto L_0x004d
        L_0x01fe:
            r13.k = r3
            android.view.MotionEvent r1 = android.view.MotionEvent.obtain(r14)
            boolean r0 = r13.r
            if (r0 == 0) goto L_0x0238
            android.view.GestureDetector$OnDoubleTapListener r0 = r13.j
            boolean r0 = r0.onDoubleTapEvent(r14)
            r0 = r0 | 0
        L_0x0210:
            android.view.MotionEvent r2 = r13.q
            if (r2 == 0) goto L_0x0219
            android.view.MotionEvent r2 = r13.q
            r2.recycle()
        L_0x0219:
            r13.q = r1
            android.view.VelocityTracker r1 = r13.x
            if (r1 == 0) goto L_0x0227
            android.view.VelocityTracker r1 = r13.x
            r1.recycle()
            r1 = 0
            r13.x = r1
        L_0x0227:
            r13.r = r3
            r13.l = r3
            android.os.Handler r1 = r13.h
            r1.removeMessages(r4)
            android.os.Handler r1 = r13.h
            r1.removeMessages(r12)
            r3 = r0
            goto L_0x004d
        L_0x0238:
            boolean r0 = r13.m
            if (r0 == 0) goto L_0x0245
            android.os.Handler r0 = r13.h
            r0.removeMessages(r11)
            r13.m = r3
            r0 = r3
            goto L_0x0210
        L_0x0245:
            boolean r0 = r13.n
            if (r0 == 0) goto L_0x025d
            android.view.GestureDetector$OnGestureListener r0 = r13.i
            boolean r0 = r0.onSingleTapUp(r14)
            boolean r2 = r13.l
            if (r2 == 0) goto L_0x0210
            android.view.GestureDetector$OnDoubleTapListener r2 = r13.j
            if (r2 == 0) goto L_0x0210
            android.view.GestureDetector$OnDoubleTapListener r2 = r13.j
            r2.onSingleTapConfirmed(r14)
            goto L_0x0210
        L_0x025d:
            android.view.VelocityTracker r0 = r13.x
            int r2 = android.support.v4.view.ay.b(r14, r3)
            r5 = 1000(0x3e8, float:1.401E-42)
            int r6 = r13.d
            float r6 = (float) r6
            r0.computeCurrentVelocity(r5, r6)
            float r5 = android.support.v4.view.bt.b(r0, r2)
            float r0 = android.support.v4.view.bt.a(r0, r2)
            float r2 = java.lang.Math.abs(r5)
            int r6 = r13.c
            float r6 = (float) r6
            int r2 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r2 > 0) goto L_0x0289
            float r2 = java.lang.Math.abs(r0)
            int r6 = r13.c
            float r6 = (float) r6
            int r2 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r2 <= 0) goto L_0x02bc
        L_0x0289:
            android.view.GestureDetector$OnGestureListener r2 = r13.i
            android.view.MotionEvent r6 = r13.p
            boolean r0 = r2.onFling(r6, r14, r0, r5)
            goto L_0x0210
        L_0x0293:
            android.os.Handler r0 = r13.h
            r0.removeMessages(r4)
            android.os.Handler r0 = r13.h
            r0.removeMessages(r12)
            android.os.Handler r0 = r13.h
            r0.removeMessages(r11)
            android.view.VelocityTracker r0 = r13.x
            r0.recycle()
            r0 = 0
            r13.x = r0
            r13.r = r3
            r13.k = r3
            r13.n = r3
            r13.o = r3
            r13.l = r3
            boolean r0 = r13.m
            if (r0 == 0) goto L_0x004d
            r13.m = r3
            goto L_0x004d
        L_0x02bc:
            r0 = r3
            goto L_0x0210
        L_0x02bf:
            r0 = r3
            goto L_0x01d3
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.s.a(android.view.MotionEvent):boolean");
    }
}
