package android.support.v4.view.a;

import android.os.Build;
import android.view.accessibility.AccessibilityEvent;

public final class a {
    private static final e a;

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            a = new c();
        } else if (Build.VERSION.SDK_INT >= 14) {
            a = new b();
        } else {
            a = new d();
        }
    }

    public static af a(AccessibilityEvent accessibilityEvent) {
        return new af(accessibilityEvent);
    }

    public static void a(AccessibilityEvent accessibilityEvent, int i) {
        a.a(accessibilityEvent, i);
    }

    public static int b(AccessibilityEvent accessibilityEvent) {
        return a.a(accessibilityEvent);
    }
}
