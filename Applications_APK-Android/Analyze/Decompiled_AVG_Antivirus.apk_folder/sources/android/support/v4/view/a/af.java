package android.support.v4.view.a;

import android.os.Build;

public final class af {
    private static final ai a;
    private final Object b;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            a = new aj();
        } else if (Build.VERSION.SDK_INT >= 15) {
            a = new ah();
        } else if (Build.VERSION.SDK_INT >= 14) {
            a = new ag();
        } else {
            a = new ak();
        }
    }

    public af(Object obj) {
        this.b = obj;
    }

    public static af a() {
        return new af(a.a());
    }

    public final void a(int i) {
        a.b(this.b, i);
    }

    public final void a(boolean z) {
        a.a(this.b, z);
    }

    public final void b(int i) {
        a.a(this.b, i);
    }

    public final void c(int i) {
        a.e(this.b, i);
    }

    public final void d(int i) {
        a.c(this.b, i);
    }

    public final void e(int i) {
        a.d(this.b, i);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        af afVar = (af) obj;
        return this.b == null ? afVar.b == null : this.b.equals(afVar.b);
    }

    public final void f(int i) {
        a.f(this.b, i);
    }

    public final void g(int i) {
        a.g(this.b, i);
    }

    public final int hashCode() {
        if (this.b == null) {
            return 0;
        }
        return this.b.hashCode();
    }
}
