package android.support.v4.view;

import android.view.View;
import android.view.WindowInsets;

class ci extends ch {
    ci() {
    }

    public final boolean E(View view) {
        return view.isNestedScrollingEnabled();
    }

    public final void F(View view) {
        view.stopNestedScroll();
    }

    public final float H(View view) {
        return view.getZ();
    }

    public final eo a(View view, eo eoVar) {
        WindowInsets g;
        WindowInsets onApplyWindowInsets;
        return (!(eoVar instanceof ep) || (onApplyWindowInsets = view.onApplyWindowInsets((g = ((ep) eoVar).g()))) == g) ? eoVar : new ep(onApplyWindowInsets);
    }

    public final void a(View view, bi biVar) {
        view.setOnApplyWindowInsetsListener(new co(biVar));
    }

    public final eo b(View view, eo eoVar) {
        WindowInsets g;
        WindowInsets dispatchApplyWindowInsets;
        return (!(eoVar instanceof ep) || (dispatchApplyWindowInsets = view.dispatchApplyWindowInsets((g = ((ep) eoVar).g()))) == g) ? eoVar : new ep(dispatchApplyWindowInsets);
    }

    public final void f(View view, float f) {
        view.setElevation(f);
    }

    public final void x(View view) {
        view.requestApplyInsets();
    }

    public final float y(View view) {
        return view.getElevation();
    }

    public final float z(View view) {
        return view.getTranslationZ();
    }
}
