package android.support.v4.view;

import android.database.DataSetObserver;

final class bm extends DataSetObserver implements di, dj {
    final /* synthetic */ PagerTitleStrip a;
    private int b;

    private bm(PagerTitleStrip pagerTitleStrip) {
        this.a = pagerTitleStrip;
    }

    /* synthetic */ bm(PagerTitleStrip pagerTitleStrip, byte b2) {
        this(pagerTitleStrip);
    }

    public final void a() {
        float f = 0.0f;
        if (this.b == 0) {
            this.a.a(this.a.a.b(), this.a.a.a());
            if (this.a.g >= 0.0f) {
                f = this.a.g;
            }
            this.a.a(this.a.a.b(), f, true);
        }
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void a(int i, float f) {
        if (f > 0.5f) {
            i++;
        }
        this.a.a(i, f, false);
    }

    public final void a(bj bjVar, bj bjVar2) {
        this.a.a(bjVar, bjVar2);
    }

    public final void onChanged() {
        float f = 0.0f;
        this.a.a(this.a.a.b(), this.a.a.a());
        if (this.a.g >= 0.0f) {
            f = this.a.g;
        }
        this.a.a(this.a.a.b(), f, true);
    }
}
