package android.support.v4.view;

import android.os.Build;
import android.view.ViewGroup;

public final class cv {
    static final cy a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            a = new da();
        } else if (i >= 18) {
            a = new cz();
        } else if (i >= 14) {
            a = new cx();
        } else if (i >= 11) {
            a = new cw();
        } else {
            a = new db();
        }
    }

    public static void a(ViewGroup viewGroup) {
        a.a(viewGroup);
    }
}
