package android.support.v4.view;

import android.view.View;
import java.lang.ref.WeakReference;

final class dx implements Runnable {
    WeakReference a;
    dv b;
    final /* synthetic */ dw c;

    private dx(dw dwVar, dv dvVar, View view) {
        this.c = dwVar;
        this.a = new WeakReference(view);
        this.b = dvVar;
    }

    /* synthetic */ dx(dw dwVar, dv dvVar, View view, byte b2) {
        this(dwVar, dvVar, view);
    }

    public final void run() {
        View view = (View) this.a.get();
        if (view != null) {
            this.c.d(this.b, view);
        }
    }
}
