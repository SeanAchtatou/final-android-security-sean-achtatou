package android.support.v4.view;

import android.view.ViewGroup;

public final class bh {
    private final ViewGroup a;
    private int b;

    public bh(ViewGroup viewGroup) {
        this.a = viewGroup;
    }

    public final int a() {
        return this.b;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void b() {
        this.b = 0;
    }
}
