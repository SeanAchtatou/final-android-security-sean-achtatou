package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

final class dz implements el {
    dv a;

    dz(dv dvVar) {
        this.a = dvVar;
    }

    public final void a(View view) {
        if (this.a.e >= 0) {
            bx.a(view, 2, (Paint) null);
        }
        if (this.a.c != null) {
            this.a.c.run();
        }
        Object tag = view.getTag(2113929216);
        el elVar = tag instanceof el ? (el) tag : null;
        if (elVar != null) {
            elVar.a(view);
        }
    }

    public final void b(View view) {
        if (this.a.e >= 0) {
            bx.a(view, this.a.e, (Paint) null);
            int unused = this.a.e = -1;
        }
        if (this.a.d != null) {
            this.a.d.run();
        }
        Object tag = view.getTag(2113929216);
        el elVar = tag instanceof el ? (el) tag : null;
        if (elVar != null) {
            elVar.b(view);
        }
    }

    public final void c(View view) {
        Object tag = view.getTag(2113929216);
        el elVar = tag instanceof el ? (el) tag : null;
        if (elVar != null) {
            elVar.c(view);
        }
    }
}
