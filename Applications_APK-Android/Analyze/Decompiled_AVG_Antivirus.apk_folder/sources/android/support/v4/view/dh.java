package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.af;
import android.support.v4.view.a.f;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

final class dh extends a {
    final /* synthetic */ ViewPager a;

    dh(ViewPager viewPager) {
        this.a = viewPager;
    }

    private boolean a() {
        return this.a.h != null && this.a.h.b() > 1;
    }

    public final void a(View view, f fVar) {
        super.a(view, fVar);
        fVar.b((CharSequence) ViewPager.class.getName());
        fVar.i(a());
        if (this.a.canScrollHorizontally(1)) {
            fVar.a(4096);
        }
        if (this.a.canScrollHorizontally(-1)) {
            fVar.a(8192);
        }
    }

    public final void a(View view, AccessibilityEvent accessibilityEvent) {
        super.a(view, accessibilityEvent);
        accessibilityEvent.setClassName(ViewPager.class.getName());
        af a2 = af.a();
        a2.a(a());
        if (accessibilityEvent.getEventType() == 4096 && this.a.h != null) {
            a2.a(this.a.h.b());
            a2.b(this.a.i);
            a2.c(this.a.i);
        }
    }

    public final boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        switch (i) {
            case 4096:
                if (!this.a.canScrollHorizontally(1)) {
                    return false;
                }
                this.a.a(this.a.i + 1);
                return true;
            case 8192:
                if (!this.a.canScrollHorizontally(-1)) {
                    return false;
                }
                this.a.a(this.a.i - 1);
                return true;
            default:
                return false;
        }
    }
}
