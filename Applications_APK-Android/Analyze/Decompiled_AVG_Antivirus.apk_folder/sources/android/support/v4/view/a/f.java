package android.support.v4.view.a;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;

public final class f {
    /* access modifiers changed from: private */
    public static final k a;
    private final Object b;

    static {
        if (Build.VERSION.SDK_INT >= 22) {
            a = new i();
        } else if (Build.VERSION.SDK_INT >= 21) {
            a = new h();
        } else if (Build.VERSION.SDK_INT >= 19) {
            a = new o();
        } else if (Build.VERSION.SDK_INT >= 18) {
            a = new n();
        } else if (Build.VERSION.SDK_INT >= 17) {
            a = new m();
        } else if (Build.VERSION.SDK_INT >= 16) {
            a = new l();
        } else if (Build.VERSION.SDK_INT >= 14) {
            a = new j();
        } else {
            a = new p();
        }
    }

    public f(Object obj) {
        this.b = obj;
    }

    public static f a(f fVar) {
        Object a2 = a.a(fVar.b);
        if (a2 != null) {
            return new f(a2);
        }
        return null;
    }

    public final Object a() {
        return this.b;
    }

    public final void a(int i) {
        a.a(this.b, i);
    }

    public final void a(Rect rect) {
        a.a(this.b, rect);
    }

    public final void a(View view) {
        a.c(this.b, view);
    }

    public final void a(CharSequence charSequence) {
        a.d(this.b, charSequence);
    }

    public final void a(Object obj) {
        a.b(this.b, ((q) obj).a);
    }

    public final void a(boolean z) {
        a.c(this.b, z);
    }

    public final boolean a(g gVar) {
        return a.a(this.b, gVar.w);
    }

    public final int b() {
        return a.b(this.b);
    }

    public final void b(int i) {
        a.b(this.b, i);
    }

    public final void b(Rect rect) {
        a.c(this.b, rect);
    }

    public final void b(View view) {
        a.a(this.b, view);
    }

    public final void b(CharSequence charSequence) {
        a.b(this.b, charSequence);
    }

    public final void b(Object obj) {
        a.c(this.b, ((r) obj).a);
    }

    public final void b(boolean z) {
        a.d(this.b, z);
    }

    public final int c() {
        return a.r(this.b);
    }

    public final void c(Rect rect) {
        a.b(this.b, rect);
    }

    public final void c(View view) {
        a.b(this.b, view);
    }

    public final void c(CharSequence charSequence) {
        a.e(this.b, charSequence);
    }

    public final void c(boolean z) {
        a.h(this.b, z);
    }

    public final void d(Rect rect) {
        a.d(this.b, rect);
    }

    public final void d(View view) {
        a.d(this.b, view);
    }

    public final void d(CharSequence charSequence) {
        a.c(this.b, charSequence);
    }

    public final void d(boolean z) {
        a.i(this.b, z);
    }

    public final boolean d() {
        return a.k(this.b);
    }

    public final void e(CharSequence charSequence) {
        a.a(this.b, charSequence);
    }

    public final void e(boolean z) {
        a.g(this.b, z);
    }

    public final boolean e() {
        return a.l(this.b);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        return this.b == null ? fVar.b == null : this.b.equals(fVar.b);
    }

    public final void f(boolean z) {
        a.a(this.b, z);
    }

    public final boolean f() {
        return a.s(this.b);
    }

    public final void g(boolean z) {
        a.e(this.b, z);
    }

    public final boolean g() {
        return a.t(this.b);
    }

    public final void h(boolean z) {
        a.b(this.b, z);
    }

    public final boolean h() {
        return a.p(this.b);
    }

    public final int hashCode() {
        if (this.b == null) {
            return 0;
        }
        return this.b.hashCode();
    }

    public final void i(boolean z) {
        a.f(this.b, z);
    }

    public final boolean i() {
        return a.i(this.b);
    }

    public final boolean j() {
        return a.m(this.b);
    }

    public final boolean k() {
        return a.j(this.b);
    }

    public final CharSequence l() {
        return a.e(this.b);
    }

    public final CharSequence m() {
        return a.c(this.b);
    }

    public final CharSequence n() {
        return a.d(this.b);
    }

    public final void o() {
        a.q(this.b);
    }

    public final void p() {
        a.v(this.b);
    }

    public final String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        a(rect);
        sb.append("; boundsInParent: " + rect);
        c(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ").append(l());
        sb.append("; className: ").append(m());
        sb.append("; text: ").append(a.f(this.b));
        sb.append("; contentDescription: ").append(n());
        sb.append("; viewId: ").append(a.u(this.b));
        sb.append("; checkable: ").append(a.g(this.b));
        sb.append("; checked: ").append(a.h(this.b));
        sb.append("; focusable: ").append(d());
        sb.append("; focused: ").append(e());
        sb.append("; selected: ").append(h());
        sb.append("; clickable: ").append(i());
        sb.append("; longClickable: ").append(j());
        sb.append("; enabled: ").append(k());
        sb.append("; password: ").append(a.n(this.b));
        sb.append("; scrollable: " + a.o(this.b));
        sb.append("; [");
        int b2 = b();
        while (b2 != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(b2);
            int i = (numberOfTrailingZeros ^ -1) & b2;
            switch (numberOfTrailingZeros) {
                case 1:
                    str = "ACTION_FOCUS";
                    break;
                case 2:
                    str = "ACTION_CLEAR_FOCUS";
                    break;
                case 4:
                    str = "ACTION_SELECT";
                    break;
                case 8:
                    str = "ACTION_CLEAR_SELECTION";
                    break;
                case 16:
                    str = "ACTION_CLICK";
                    break;
                case 32:
                    str = "ACTION_LONG_CLICK";
                    break;
                case 64:
                    str = "ACTION_ACCESSIBILITY_FOCUS";
                    break;
                case 128:
                    str = "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
                    break;
                case 256:
                    str = "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
                    break;
                case 512:
                    str = "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
                    break;
                case 1024:
                    str = "ACTION_NEXT_HTML_ELEMENT";
                    break;
                case 2048:
                    str = "ACTION_PREVIOUS_HTML_ELEMENT";
                    break;
                case 4096:
                    str = "ACTION_SCROLL_FORWARD";
                    break;
                case 8192:
                    str = "ACTION_SCROLL_BACKWARD";
                    break;
                case 16384:
                    str = "ACTION_COPY";
                    break;
                case 32768:
                    str = "ACTION_PASTE";
                    break;
                case 65536:
                    str = "ACTION_CUT";
                    break;
                case 131072:
                    str = "ACTION_SET_SELECTION";
                    break;
                default:
                    str = "ACTION_UNKNOWN";
                    break;
            }
            sb.append(str);
            if (i != 0) {
                sb.append(", ");
            }
            b2 = i;
        }
        sb.append("]");
        return sb.toString();
    }
}
