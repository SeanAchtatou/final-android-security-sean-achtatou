package android.support.v4.view.a;

import android.view.accessibility.AccessibilityNodeInfo;

class h extends o {
    h() {
    }

    public final Object a(int i) {
        return new AccessibilityNodeInfo.AccessibilityAction(i, null);
    }

    public final Object a(int i, int i2) {
        return AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, false, 0);
    }

    public final Object a(int i, int i2, int i3, int i4, boolean z) {
        return AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z, false);
    }

    public final void a(Object obj, CharSequence charSequence) {
        ((AccessibilityNodeInfo) obj).setError(charSequence);
    }

    public final boolean a(Object obj, Object obj2) {
        return ((AccessibilityNodeInfo) obj).removeAction((AccessibilityNodeInfo.AccessibilityAction) obj2);
    }
}
