package android.support.v4.view;

import android.view.View;
import android.view.ViewParent;

class ce extends cc {
    ce() {
    }

    public final boolean A(View view) {
        return view.getFitsSystemWindows();
    }

    public final void a(View view, int i, int i2, int i3, int i4) {
        view.postInvalidate(i, i2, i3, i4);
    }

    public final void a(View view, Runnable runnable) {
        view.postOnAnimation(runnable);
    }

    public final void a(View view, Runnable runnable, long j) {
        view.postOnAnimationDelayed(runnable, j);
    }

    public void c(View view, int i) {
        if (i == 4) {
            i = 2;
        }
        view.setImportantForAccessibility(i);
    }

    public final boolean c(View view) {
        return view.hasTransientState();
    }

    public final void d(View view) {
        view.postInvalidateOnAnimation();
    }

    public final int e(View view) {
        return view.getImportantForAccessibility();
    }

    public final ViewParent i(View view) {
        return view.getParentForAccessibility();
    }

    public final boolean p(View view) {
        return view.hasOverlappingRendering();
    }

    public final int t(View view) {
        return view.getMinimumWidth();
    }

    public final int u(View view) {
        return view.getMinimumHeight();
    }

    public void x(View view) {
        view.requestFitSystemWindows();
    }
}
