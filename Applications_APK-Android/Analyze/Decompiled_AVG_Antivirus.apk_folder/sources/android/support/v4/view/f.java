package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.s;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

final class f implements m {
    final /* synthetic */ a a;
    final /* synthetic */ e b;

    f(e eVar, a aVar) {
        this.b = eVar;
        this.a = aVar;
    }

    public final Object a(View view) {
        s a2 = a.a(view);
        if (a2 != null) {
            return a2.a();
        }
        return null;
    }

    public final void a(View view, int i) {
        a.a(view, i);
    }

    public final void a(View view, Object obj) {
        this.a.a(view, new android.support.v4.view.a.f(obj));
    }

    public final boolean a(View view, int i, Bundle bundle) {
        return this.a.a(view, i, bundle);
    }

    public final boolean a(View view, AccessibilityEvent accessibilityEvent) {
        return this.a.d(view, accessibilityEvent);
    }

    public final boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.a.a(viewGroup, view, accessibilityEvent);
    }

    public final void b(View view, AccessibilityEvent accessibilityEvent) {
        this.a.a(view, accessibilityEvent);
    }

    public final void c(View view, AccessibilityEvent accessibilityEvent) {
        this.a.b(view, accessibilityEvent);
    }

    public final void d(View view, AccessibilityEvent accessibilityEvent) {
        a.c(view, accessibilityEvent);
    }
}
