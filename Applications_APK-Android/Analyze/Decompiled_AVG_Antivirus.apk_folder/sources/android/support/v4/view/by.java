package android.support.v4.view;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.util.WeakHashMap;

class by implements ck {
    WeakHashMap a = null;

    by() {
    }

    public boolean A(View view) {
        return false;
    }

    public void B(View view) {
    }

    public void C(View view) {
    }

    public boolean D(View view) {
        return false;
    }

    public boolean E(View view) {
        if (view instanceof be) {
            return ((be) view).isNestedScrollingEnabled();
        }
        return false;
    }

    public void F(View view) {
        if (view instanceof be) {
            ((be) view).stopNestedScroll();
        }
    }

    public boolean G(View view) {
        return view.getWidth() > 0 && view.getHeight() > 0;
    }

    public float H(View view) {
        return z(view) + y(view);
    }

    public boolean I(View view) {
        return view.getWindowToken() != null;
    }

    public int a(int i, int i2) {
        return i | i2;
    }

    public int a(int i, int i2, int i3) {
        return View.resolveSize(i, i2);
    }

    public int a(View view) {
        return 2;
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return 10;
    }

    public eo a(View view, eo eoVar) {
        return eoVar;
    }

    public void a(View view, float f) {
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        view.invalidate(i, i2, i3, i4);
    }

    public void a(View view, int i, Paint paint) {
    }

    public void a(View view, Paint paint) {
    }

    public void a(View view, a aVar) {
    }

    public void a(View view, bi biVar) {
    }

    public void a(View view, Runnable runnable) {
        view.postDelayed(runnable, a());
    }

    public void a(View view, Runnable runnable, long j) {
        view.postDelayed(runnable, a() + j);
    }

    public void a(View view, boolean z) {
    }

    public void a(ViewGroup viewGroup) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.view.View r6, int r7) {
        /*
            r5 = this;
            r0 = 1
            r1 = 0
            boolean r2 = r6 instanceof android.support.v4.view.bs
            if (r2 == 0) goto L_0x0029
            android.support.v4.view.bs r6 = (android.support.v4.view.bs) r6
            int r2 = r6.computeHorizontalScrollOffset()
            int r3 = r6.computeHorizontalScrollRange()
            int r4 = r6.computeHorizontalScrollExtent()
            int r3 = r3 - r4
            if (r3 == 0) goto L_0x0027
            if (r7 >= 0) goto L_0x0021
            if (r2 <= 0) goto L_0x001f
            r2 = r0
        L_0x001c:
            if (r2 == 0) goto L_0x0029
        L_0x001e:
            return r0
        L_0x001f:
            r2 = r1
            goto L_0x001c
        L_0x0021:
            int r3 = r3 + -1
            if (r2 >= r3) goto L_0x0027
            r2 = r0
            goto L_0x001c
        L_0x0027:
            r2 = r1
            goto L_0x001c
        L_0x0029:
            r0 = r1
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.by.a(android.view.View, int):boolean");
    }

    public eo b(View view, eo eoVar) {
        return eoVar;
    }

    public void b(View view, float f) {
    }

    public void b(View view, int i, int i2, int i3, int i4) {
        view.setPadding(i, i2, i3, i4);
    }

    public void b(View view, boolean z) {
    }

    public boolean b(View view) {
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(android.view.View r6, int r7) {
        /*
            r5 = this;
            r0 = 1
            r1 = 0
            boolean r2 = r6 instanceof android.support.v4.view.bs
            if (r2 == 0) goto L_0x0029
            android.support.v4.view.bs r6 = (android.support.v4.view.bs) r6
            int r2 = r6.computeVerticalScrollOffset()
            int r3 = r6.computeVerticalScrollRange()
            int r4 = r6.computeVerticalScrollExtent()
            int r3 = r3 - r4
            if (r3 == 0) goto L_0x0027
            if (r7 >= 0) goto L_0x0021
            if (r2 <= 0) goto L_0x001f
            r2 = r0
        L_0x001c:
            if (r2 == 0) goto L_0x0029
        L_0x001e:
            return r0
        L_0x001f:
            r2 = r1
            goto L_0x001c
        L_0x0021:
            int r3 = r3 + -1
            if (r2 >= r3) goto L_0x0027
            r2 = r0
            goto L_0x001c
        L_0x0027:
            r2 = r1
            goto L_0x001c
        L_0x0029:
            r0 = r1
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.by.b(android.view.View, int):boolean");
    }

    public void c(View view, float f) {
    }

    public void c(View view, int i) {
    }

    public boolean c(View view) {
        return false;
    }

    public void d(View view) {
        view.invalidate();
    }

    public void d(View view, float f) {
    }

    public int e(View view) {
        return 0;
    }

    public void e(View view, float f) {
    }

    public float f(View view) {
        return 1.0f;
    }

    public void f(View view, float f) {
    }

    public int g(View view) {
        return 0;
    }

    public int h(View view) {
        return 0;
    }

    public ViewParent i(View view) {
        return view.getParent();
    }

    public boolean j(View view) {
        Drawable background = view.getBackground();
        return background != null && background.getOpacity() == -1;
    }

    public int k(View view) {
        return view.getMeasuredWidth();
    }

    public int l(View view) {
        return view.getMeasuredHeight();
    }

    public int m(View view) {
        return 0;
    }

    public int n(View view) {
        return view.getPaddingLeft();
    }

    public int o(View view) {
        return view.getPaddingRight();
    }

    public boolean p(View view) {
        return true;
    }

    public float q(View view) {
        return 0.0f;
    }

    public float r(View view) {
        return 0.0f;
    }

    public float s(View view) {
        return 0.0f;
    }

    public int t(View view) {
        return cl.a(view);
    }

    public int u(View view) {
        return cl.b(view);
    }

    public dv v(View view) {
        return new dv(view);
    }

    public int w(View view) {
        return 0;
    }

    public void x(View view) {
    }

    public float y(View view) {
        return 0.0f;
    }

    public float z(View view) {
        return 0.0f;
    }
}
