package android.support.v4.view;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.a.f;
import android.support.v4.view.a.s;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

public class a {
    private static final d a;
    private static final Object c = a.a();
    final Object b = a.a(this);

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            a = new e();
        } else if (Build.VERSION.SDK_INT >= 14) {
            a = new b();
        } else {
            a = new g();
        }
    }

    public static s a(View view) {
        return a.a(c, view);
    }

    public static void a(View view, int i) {
        a.a(c, view, i);
    }

    public static void c(View view, AccessibilityEvent accessibilityEvent) {
        a.d(c, view, accessibilityEvent);
    }

    public void a(View view, f fVar) {
        a.a(c, view, fVar);
    }

    public void a(View view, AccessibilityEvent accessibilityEvent) {
        a.b(c, view, accessibilityEvent);
    }

    public boolean a(View view, int i, Bundle bundle) {
        return a.a(c, view, i, bundle);
    }

    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return a.a(c, viewGroup, view, accessibilityEvent);
    }

    public void b(View view, AccessibilityEvent accessibilityEvent) {
        a.c(c, view, accessibilityEvent);
    }

    public boolean d(View view, AccessibilityEvent accessibilityEvent) {
        return a.a(c, view, accessibilityEvent);
    }
}
