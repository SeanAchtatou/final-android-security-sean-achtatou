package android.support.v4.view.a;

import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.List;

final class ad extends AccessibilityNodeProvider {
    final /* synthetic */ ae a;

    ad(ae aeVar) {
        this.a = aeVar;
    }

    public final AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
        this.a.c();
        return null;
    }

    public final List findAccessibilityNodeInfosByText(String str, int i) {
        return this.a.b();
    }

    public final AccessibilityNodeInfo findFocus(int i) {
        this.a.d();
        return null;
    }

    public final boolean performAction(int i, int i2, Bundle bundle) {
        return this.a.a();
    }
}
