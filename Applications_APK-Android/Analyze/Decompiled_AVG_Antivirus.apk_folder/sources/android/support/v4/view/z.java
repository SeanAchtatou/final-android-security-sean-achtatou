package android.support.v4.view;

import android.os.Build;
import android.view.KeyEvent;

public final class z {
    static final ad a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            a = new ac();
        } else {
            a = new aa();
        }
    }

    public static boolean a(KeyEvent keyEvent) {
        return a.b(keyEvent.getMetaState());
    }

    public static boolean b(KeyEvent keyEvent) {
        return a.c(keyEvent.getMetaState());
    }

    public static void c(KeyEvent keyEvent) {
        a.a(keyEvent);
    }
}
