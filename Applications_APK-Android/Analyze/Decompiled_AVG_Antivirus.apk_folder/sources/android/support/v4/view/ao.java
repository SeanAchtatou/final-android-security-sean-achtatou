package android.support.v4.view;

import android.os.Build;
import android.view.ViewGroup;

public final class ao {
    static final ap a;

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            a = new ar();
        } else {
            a = new aq();
        }
    }

    public static int a(ViewGroup.MarginLayoutParams marginLayoutParams) {
        return a.a(marginLayoutParams);
    }

    public static void a(ViewGroup.MarginLayoutParams marginLayoutParams, int i) {
        a.a(marginLayoutParams, i);
    }

    public static int b(ViewGroup.MarginLayoutParams marginLayoutParams) {
        return a.b(marginLayoutParams);
    }

    public static void b(ViewGroup.MarginLayoutParams marginLayoutParams, int i) {
        a.b(marginLayoutParams, i);
    }
}
