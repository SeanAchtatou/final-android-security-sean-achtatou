package android.support.v4.view;

import android.os.Parcel;
import android.support.v4.os.f;
import android.support.v4.view.ViewPager;

final class dm implements f {
    dm() {
    }

    public final /* synthetic */ Object a(Parcel parcel, ClassLoader classLoader) {
        return new ViewPager.SavedState(parcel, classLoader);
    }

    public final /* bridge */ /* synthetic */ Object[] a(int i) {
        return new ViewPager.SavedState[i];
    }
}
