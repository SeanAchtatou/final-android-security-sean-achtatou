package android.support.v4.view;

import android.view.View;
import java.lang.reflect.Field;
import java.util.WeakHashMap;

class cd extends cb {
    static Field b;
    static boolean c = false;

    cd() {
    }

    public final void a(View view, a aVar) {
        view.setAccessibilityDelegate((View.AccessibilityDelegate) (aVar == null ? null : aVar.b));
    }

    public final void a(View view, boolean z) {
        view.setFitsSystemWindows(z);
    }

    public final boolean a(View view, int i) {
        return view.canScrollHorizontally(i);
    }

    public final boolean b(View view) {
        if (c) {
            return false;
        }
        if (b == null) {
            try {
                Field declaredField = View.class.getDeclaredField("mAccessibilityDelegate");
                b = declaredField;
                declaredField.setAccessible(true);
            } catch (Throwable th) {
                c = true;
                return false;
            }
        }
        try {
            return b.get(view) != null;
        } catch (Throwable th2) {
            c = true;
            return false;
        }
    }

    public final boolean b(View view, int i) {
        return view.canScrollVertically(i);
    }

    public final dv v(View view) {
        if (this.a == null) {
            this.a = new WeakHashMap();
        }
        dv dvVar = (dv) this.a.get(view);
        if (dvVar != null) {
            return dvVar;
        }
        dv dvVar2 = new dv(view);
        this.a.put(view, dvVar2);
        return dvVar2;
    }
}
