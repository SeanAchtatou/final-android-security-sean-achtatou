package android.support.v4.view;

import android.graphics.Paint;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public final class bx {
    static final ck a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            a = new cj();
        } else if (i >= 21) {
            a = new ci();
        } else if (i >= 19) {
            a = new ch();
        } else if (i >= 17) {
            a = new cf();
        } else if (i >= 16) {
            a = new ce();
        } else if (i >= 15) {
            a = new cc();
        } else if (i >= 14) {
            a = new cd();
        } else if (i >= 11) {
            a = new cb();
        } else if (i >= 9) {
            a = new ca();
        } else if (i >= 7) {
            a = new bz();
        } else {
            a = new by();
        }
    }

    public static void A(View view) {
        a.C(view);
    }

    public static boolean B(View view) {
        return a.p(view);
    }

    public static boolean C(View view) {
        return a.D(view);
    }

    public static boolean D(View view) {
        return a.E(view);
    }

    public static void E(View view) {
        a.F(view);
    }

    public static boolean F(View view) {
        return a.G(view);
    }

    public static float G(View view) {
        return a.H(view);
    }

    public static boolean H(View view) {
        return a.I(view);
    }

    public static int a(int i, int i2) {
        return a.a(i, i2);
    }

    public static int a(int i, int i2, int i3) {
        return a.a(i, i2, i3);
    }

    public static int a(View view) {
        return a.a(view);
    }

    public static eo a(View view, eo eoVar) {
        return a.a(view, eoVar);
    }

    public static void a(View view, float f) {
        a.a(view, f);
    }

    public static void a(View view, int i, int i2, int i3, int i4) {
        a.a(view, i, i2, i3, i4);
    }

    public static void a(View view, int i, Paint paint) {
        a.a(view, i, paint);
    }

    public static void a(View view, Paint paint) {
        a.a(view, paint);
    }

    public static void a(View view, a aVar) {
        a.a(view, aVar);
    }

    public static void a(View view, bi biVar) {
        a.a(view, biVar);
    }

    public static void a(View view, Runnable runnable) {
        a.a(view, runnable);
    }

    public static void a(View view, Runnable runnable, long j) {
        a.a(view, runnable, j);
    }

    public static void a(View view, boolean z) {
        a.a(view, z);
    }

    public static void a(ViewGroup viewGroup) {
        a.a(viewGroup);
    }

    public static boolean a(View view, int i) {
        return a.a(view, i);
    }

    public static eo b(View view, eo eoVar) {
        return a.b(view, eoVar);
    }

    public static void b(View view, float f) {
        a.b(view, f);
    }

    public static void b(View view, int i, int i2, int i3, int i4) {
        a.b(view, i, i2, i3, i4);
    }

    public static void b(View view, boolean z) {
        a.b(view, z);
    }

    public static boolean b(View view) {
        return a.b(view);
    }

    public static boolean b(View view, int i) {
        return a.b(view, i);
    }

    public static void c(View view, float f) {
        a.c(view, f);
    }

    public static void c(View view, int i) {
        a.c(view, i);
    }

    public static boolean c(View view) {
        return a.c(view);
    }

    public static void d(View view) {
        a.d(view);
    }

    public static void d(View view, float f) {
        a.d(view, f);
    }

    public static void d(View view, int i) {
        view.offsetTopAndBottom(i);
        if (i != 0 && Build.VERSION.SDK_INT < 11) {
            view.invalidate();
        }
    }

    public static int e(View view) {
        return a.e(view);
    }

    public static void e(View view, float f) {
        a.e(view, f);
    }

    public static void e(View view, int i) {
        view.offsetLeftAndRight(i);
        if (i != 0 && Build.VERSION.SDK_INT < 11) {
            view.invalidate();
        }
    }

    public static float f(View view) {
        return a.f(view);
    }

    public static void f(View view, float f) {
        a.f(view, f);
    }

    public static int g(View view) {
        return a.g(view);
    }

    public static int h(View view) {
        return a.h(view);
    }

    public static ViewParent i(View view) {
        return a.i(view);
    }

    public static boolean j(View view) {
        return a.j(view);
    }

    public static int k(View view) {
        return a.k(view);
    }

    public static int l(View view) {
        return a.l(view);
    }

    public static int m(View view) {
        return a.m(view);
    }

    public static int n(View view) {
        return a.n(view);
    }

    public static int o(View view) {
        return a.o(view);
    }

    public static float p(View view) {
        return a.q(view);
    }

    public static float q(View view) {
        return a.r(view);
    }

    public static int r(View view) {
        return a.t(view);
    }

    public static int s(View view) {
        return a.u(view);
    }

    public static dv t(View view) {
        return a.v(view);
    }

    public static float u(View view) {
        return a.s(view);
    }

    public static float v(View view) {
        return a.y(view);
    }

    public static int w(View view) {
        return a.w(view);
    }

    public static void x(View view) {
        a.x(view);
    }

    public static boolean y(View view) {
        return a.A(view);
    }

    public static void z(View view) {
        a.B(view);
    }
}
