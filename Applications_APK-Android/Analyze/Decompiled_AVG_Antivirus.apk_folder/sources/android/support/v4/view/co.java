package android.support.v4.view;

import android.view.View;
import android.view.WindowInsets;

final class co implements View.OnApplyWindowInsetsListener {
    final /* synthetic */ bi a;

    co(bi biVar) {
        this.a = biVar;
    }

    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return ((ep) this.a.a(view, new ep(windowInsets))).g();
    }
}
