package android.support.v4.view;

import android.os.Build;
import android.view.ViewConfiguration;

public final class cp {
    static final cu a;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            a = new ct();
        } else if (Build.VERSION.SDK_INT >= 11) {
            a = new cs();
        } else if (Build.VERSION.SDK_INT >= 8) {
            a = new cr();
        } else {
            a = new cq();
        }
    }

    public static int a(ViewConfiguration viewConfiguration) {
        return a.a(viewConfiguration);
    }

    public static boolean b(ViewConfiguration viewConfiguration) {
        return a.b(viewConfiguration);
    }
}
