package android.support.v4.view;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.WeakHashMap;

class dy extends dw {
    WeakHashMap b = null;

    dy() {
    }

    public final void a(dv dvVar, View view) {
        view.animate().scaleY(1.0f);
    }

    public final void a(dv dvVar, View view, float f) {
        view.animate().alpha(f);
    }

    public void a(dv dvVar, View view, el elVar) {
        view.setTag(2113929216, elVar);
        view.animate().setListener(new eg(new dz(dvVar), view));
    }

    public final void a(View view, long j) {
        view.animate().setDuration(j);
    }

    public final void a(View view, Interpolator interpolator) {
        view.animate().setInterpolator(interpolator);
    }

    public final void b(dv dvVar, View view) {
        view.animate().cancel();
    }

    public final void b(dv dvVar, View view, float f) {
        view.animate().translationX(f);
    }

    public final void b(View view, long j) {
        view.animate().setStartDelay(j);
    }

    public final void c(dv dvVar, View view) {
        view.animate().start();
    }

    public final void c(dv dvVar, View view, float f) {
        view.animate().translationY(f);
    }
}
