package android.support.v4.view;

import android.view.WindowInsets;

final class ep extends eo {
    private final WindowInsets a;

    ep(WindowInsets windowInsets) {
        this.a = windowInsets;
    }

    public final int a() {
        return this.a.getSystemWindowInsetLeft();
    }

    public final eo a(int i, int i2, int i3, int i4) {
        return new ep(this.a.replaceSystemWindowInsets(i, i2, i3, i4));
    }

    public final int b() {
        return this.a.getSystemWindowInsetTop();
    }

    public final int c() {
        return this.a.getSystemWindowInsetRight();
    }

    public final int d() {
        return this.a.getSystemWindowInsetBottom();
    }

    public final boolean e() {
        return this.a.isConsumed();
    }

    public final eo f() {
        return new ep(this.a.consumeSystemWindowInsets());
    }

    /* access modifiers changed from: package-private */
    public final WindowInsets g() {
        return this.a;
    }
}
