package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

class cf extends ce {
    cf() {
    }

    public final boolean D(View view) {
        return view.isPaddingRelative();
    }

    public final void a(View view, Paint paint) {
        view.setLayerPaint(paint);
    }

    public final void b(View view, int i, int i2, int i3, int i4) {
        view.setPaddingRelative(i, i2, i3, i4);
    }

    public final int h(View view) {
        return view.getLayoutDirection();
    }

    public final int n(View view) {
        return view.getPaddingStart();
    }

    public final int o(View view) {
        return view.getPaddingEnd();
    }

    public final int w(View view) {
        return view.getWindowSystemUiVisibility();
    }
}
