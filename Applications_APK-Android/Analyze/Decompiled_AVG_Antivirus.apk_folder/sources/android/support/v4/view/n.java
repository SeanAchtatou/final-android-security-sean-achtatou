package android.support.v4.view;

import android.content.Context;
import android.util.Log;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public abstract class n {
    private final Context a;
    private o b;
    private p c;

    public n(Context context) {
        this.a = context;
    }

    public abstract View a();

    public View a(MenuItem menuItem) {
        return a();
    }

    public final void a(o oVar) {
        this.b = oVar;
    }

    public void a(p pVar) {
        if (!(this.c == null || pVar == null)) {
            Log.w("ActionProvider(support)", "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this " + getClass().getSimpleName() + " instance while it is still in use somewhere else?");
        }
        this.c = pVar;
    }

    public void a(SubMenu subMenu) {
    }

    public final void a(boolean z) {
        if (this.b != null) {
            this.b.b(z);
        }
    }

    public boolean b() {
        return false;
    }

    public boolean c() {
        return true;
    }

    public boolean d() {
        return false;
    }

    public boolean e() {
        return false;
    }
}
