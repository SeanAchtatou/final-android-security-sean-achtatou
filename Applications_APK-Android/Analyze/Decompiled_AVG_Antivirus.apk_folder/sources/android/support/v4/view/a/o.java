package android.support.v4.view.a;

import android.view.accessibility.AccessibilityNodeInfo;

class o extends n {
    o() {
    }

    public Object a(int i, int i2) {
        return AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, false);
    }

    public Object a(int i, int i2, int i3, int i4, boolean z) {
        return AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z);
    }

    public final void b(Object obj, Object obj2) {
        ((AccessibilityNodeInfo) obj).setCollectionInfo((AccessibilityNodeInfo.CollectionInfo) obj2);
    }

    public final void c(Object obj, Object obj2) {
        ((AccessibilityNodeInfo) obj).setCollectionItemInfo((AccessibilityNodeInfo.CollectionItemInfo) obj2);
    }

    public final void v(Object obj) {
        ((AccessibilityNodeInfo) obj).setContentInvalid(true);
    }
}
