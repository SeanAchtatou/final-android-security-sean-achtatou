package android.support.v4.b.a;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

class b implements c {
    b() {
    }

    public void a(Drawable drawable) {
    }

    public void a(Drawable drawable, float f, float f2) {
    }

    public void a(Drawable drawable, int i) {
        j.a(drawable, i);
    }

    public void a(Drawable drawable, int i, int i2, int i3, int i4) {
    }

    public void a(Drawable drawable, ColorStateList colorStateList) {
        j.a(drawable, colorStateList);
    }

    public void a(Drawable drawable, PorterDuff.Mode mode) {
        j.a(drawable, mode);
    }

    public void a(Drawable drawable, boolean z) {
    }

    public void b(Drawable drawable, int i) {
    }

    public boolean b(Drawable drawable) {
        return false;
    }

    public Drawable c(Drawable drawable) {
        return !(drawable instanceof m) ? new m(drawable) : drawable;
    }
}
