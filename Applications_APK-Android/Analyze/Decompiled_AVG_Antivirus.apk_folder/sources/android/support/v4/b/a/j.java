package android.support.v4.b.a;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

final class j {
    public static void a(Drawable drawable, int i) {
        if (drawable instanceof l) {
            ((l) drawable).setTint(i);
        }
    }

    public static void a(Drawable drawable, ColorStateList colorStateList) {
        if (drawable instanceof l) {
            ((l) drawable).setTintList(colorStateList);
        }
    }

    public static void a(Drawable drawable, PorterDuff.Mode mode) {
        if (drawable instanceof l) {
            ((l) drawable).setTintMode(mode);
        }
    }
}
