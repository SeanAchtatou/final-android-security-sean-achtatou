package android.support.v4.b.a;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;

class g extends f {
    g() {
    }

    public final void a(Drawable drawable, float f, float f2) {
        drawable.setHotspot(f, f2);
    }

    public final void a(Drawable drawable, int i) {
        if (drawable instanceof p) {
            j.a(drawable, i);
        } else {
            drawable.setTint(i);
        }
    }

    public final void a(Drawable drawable, int i, int i2, int i3, int i4) {
        drawable.setHotspotBounds(i, i2, i3, i4);
    }

    public final void a(Drawable drawable, ColorStateList colorStateList) {
        if (drawable instanceof p) {
            j.a(drawable, colorStateList);
        } else {
            drawable.setTintList(colorStateList);
        }
    }

    public final void a(Drawable drawable, PorterDuff.Mode mode) {
        if (drawable instanceof p) {
            j.a(drawable, mode);
        } else {
            drawable.setTintMode(mode);
        }
    }

    public Drawable c(Drawable drawable) {
        return ((drawable instanceof GradientDrawable) || (drawable instanceof DrawableContainer)) ? new p(drawable) : drawable;
    }
}
