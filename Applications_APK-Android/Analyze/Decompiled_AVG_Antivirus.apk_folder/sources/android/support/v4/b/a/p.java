package android.support.v4.b.a;

import android.content.res.Resources;
import android.graphics.Outline;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

final class p extends o {
    p(Drawable drawable) {
        super(drawable);
    }

    public final void applyTheme(Resources.Theme theme) {
        this.b.applyTheme(theme);
    }

    public final boolean canApplyTheme() {
        return this.b.canApplyTheme();
    }

    public final Rect getDirtyBounds() {
        return this.b.getDirtyBounds();
    }

    public final void getOutline(Outline outline) {
        this.b.getOutline(outline);
    }

    public final void setHotspot(float f, float f2) {
        this.b.setHotspot(f, f2);
    }

    public final void setHotspotBounds(int i, int i2, int i3, int i4) {
        this.b.setHotspotBounds(i, i2, i3, i4);
    }
}
