package android.support.v4.b.a;

import android.graphics.drawable.Drawable;

class d extends b {
    d() {
    }

    public final void a(Drawable drawable) {
        drawable.jumpToCurrentState();
    }

    public Drawable c(Drawable drawable) {
        return !(drawable instanceof n) ? new n(drawable) : drawable;
    }
}
