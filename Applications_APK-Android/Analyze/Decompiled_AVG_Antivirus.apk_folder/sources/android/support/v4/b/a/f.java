package android.support.v4.b.a;

import android.graphics.drawable.Drawable;

class f extends e {
    f() {
    }

    public final void a(Drawable drawable, boolean z) {
        drawable.setAutoMirrored(z);
    }

    public final boolean b(Drawable drawable) {
        return drawable.isAutoMirrored();
    }

    public Drawable c(Drawable drawable) {
        return !(drawable instanceof o) ? new o(drawable) : drawable;
    }
}
