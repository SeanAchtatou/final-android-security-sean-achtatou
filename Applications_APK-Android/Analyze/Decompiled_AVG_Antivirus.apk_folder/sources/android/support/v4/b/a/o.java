package android.support.v4.b.a;

import android.graphics.drawable.Drawable;

class o extends n {
    o(Drawable drawable) {
        super(drawable);
    }

    public boolean isAutoMirrored() {
        return this.b.isAutoMirrored();
    }

    public void setAutoMirrored(boolean z) {
        this.b.setAutoMirrored(z);
    }
}
