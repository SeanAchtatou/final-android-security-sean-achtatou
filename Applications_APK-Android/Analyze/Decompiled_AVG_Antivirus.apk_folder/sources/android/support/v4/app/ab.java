package android.support.v4.app;

import android.support.v4.view.bj;
import android.view.View;
import android.view.ViewGroup;

public abstract class ab extends bj {
    private final s a;
    private af b = null;
    private Fragment c = null;

    public ab(s sVar) {
        this.a = sVar;
    }

    private static String a(int i, long j) {
        return "android:switcher:" + i + ":" + j;
    }

    public abstract Fragment a(int i);

    public final Object a(ViewGroup viewGroup, int i) {
        if (this.b == null) {
            this.b = this.a.a();
        }
        long j = (long) i;
        Fragment a2 = this.a.a(a(viewGroup.getId(), j));
        if (a2 != null) {
            this.b.b(a2);
        } else {
            a2 = a(i);
            this.b.a(viewGroup.getId(), a2, a(viewGroup.getId(), j));
        }
        if (a2 != this.c) {
            a2.a(false);
            a2.b(false);
        }
        return a2;
    }

    public final void a() {
        if (this.b != null) {
            this.b.b();
            this.b = null;
            this.a.b();
        }
    }

    public final void a(Object obj) {
        if (this.b == null) {
            this.b = this.a.a();
        }
        this.b.a((Fragment) obj);
    }

    public final boolean a(View view, Object obj) {
        return ((Fragment) obj).J == view;
    }

    public final void b(Object obj) {
        Fragment fragment = (Fragment) obj;
        if (fragment != this.c) {
            if (this.c != null) {
                this.c.a(false);
                this.c.b(false);
            }
            if (fragment != null) {
                fragment.a(true);
                fragment.b(true);
            }
            this.c = fragment;
        }
    }
}
