package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.SparseArray;
import android.widget.RemoteViews;
import androidx.core.app.NotificationCompatExtras;
import androidx.core.app.NotificationManagerCompat;
import java.util.ArrayList;
import java.util.List;

public final class ca implements aw, ax {
    private Notification.Builder a;
    private final Bundle b;
    private List c = new ArrayList();

    public ca(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, int i4, CharSequence charSequence4, boolean z3, Bundle bundle, String str, boolean z4, String str2) {
        this.a = new Notification.Builder(context).setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, remoteViews).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(pendingIntent2, (notification.flags & 128) != 0).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z2).setPriority(i4).setProgress(i2, i3, z);
        this.b = new Bundle();
        if (bundle != null) {
            this.b.putAll(bundle);
        }
        if (z3) {
            this.b.putBoolean(NotificationCompatExtras.EXTRA_LOCAL_ONLY, true);
        }
        if (str != null) {
            this.b.putString(NotificationCompatExtras.EXTRA_GROUP_KEY, str);
            if (z4) {
                this.b.putBoolean(NotificationCompatExtras.EXTRA_GROUP_SUMMARY, true);
            } else {
                this.b.putBoolean(NotificationManagerCompat.EXTRA_USE_SIDE_CHANNEL, true);
            }
        }
        if (str2 != null) {
            this.b.putString(NotificationCompatExtras.EXTRA_SORT_KEY, str2);
        }
    }

    public final Notification.Builder a() {
        return this.a;
    }

    public final void a(bv bvVar) {
        this.c.add(bz.a(this.a, bvVar));
    }

    public final Notification b() {
        Notification build = this.a.build();
        Bundle a2 = bz.a(build);
        Bundle bundle = new Bundle(this.b);
        for (String next : this.b.keySet()) {
            if (a2.containsKey(next)) {
                bundle.remove(next);
            }
        }
        a2.putAll(bundle);
        SparseArray a3 = bz.a(this.c);
        if (a3 != null) {
            bz.a(build).putSparseParcelableArray(NotificationCompatExtras.EXTRA_ACTION_EXTRAS, a3);
        }
        return build;
    }
}
