package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public final class cq implements Iterable {
    private static final cs a;
    private final ArrayList b = new ArrayList();
    private final Context c;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            a = new cu();
        } else {
            a = new ct();
        }
    }

    private cq(Context context) {
        this.c = context;
    }

    private cq a(ComponentName componentName) {
        int size = this.b.size();
        try {
            Intent a2 = ar.a(this.c, componentName);
            while (a2 != null) {
                this.b.add(size, a2);
                a2 = ar.a(this.c, a2.getComponent());
            }
            return this;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(e);
        }
    }

    public static cq a(Context context) {
        return new cq(context);
    }

    public final cq a(Activity activity) {
        Intent intent = null;
        if (activity instanceof cr) {
            intent = ((cr) activity).a_();
        }
        Intent a2 = intent == null ? ar.a(activity) : intent;
        if (a2 != null) {
            ComponentName component = a2.getComponent();
            if (component == null) {
                component = a2.resolveActivity(this.c.getPackageManager());
            }
            a(component);
            this.b.add(a2);
        }
        return this;
    }

    public final void a() {
        boolean z = true;
        if (this.b.isEmpty()) {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
        }
        Intent[] intentArr = (Intent[]) this.b.toArray(new Intent[this.b.size()]);
        intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
        Context context = this.c;
        int i = Build.VERSION.SDK_INT;
        if (i >= 16) {
            context.startActivities(intentArr, null);
        } else if (i >= 11) {
            context.startActivities(intentArr);
        } else {
            z = false;
        }
        if (!z) {
            Intent intent = new Intent(intentArr[intentArr.length - 1]);
            intent.addFlags(268435456);
            this.c.startActivity(intent);
        }
    }

    public final Iterator iterator() {
        return this.b.iterator();
    }
}
