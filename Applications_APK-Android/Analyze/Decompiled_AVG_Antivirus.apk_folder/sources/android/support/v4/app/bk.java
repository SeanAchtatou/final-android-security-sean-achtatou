package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;

final class bk extends bj {
    bk() {
    }

    public final Notification a(bd bdVar) {
        Notification notification = bdVar.B;
        notification.setLatestEventInfo(bdVar.a, bdVar.b, bdVar.c, bdVar.d);
        Context context = bdVar.a;
        CharSequence charSequence = bdVar.b;
        CharSequence charSequence2 = bdVar.c;
        PendingIntent pendingIntent = bdVar.d;
        PendingIntent pendingIntent2 = bdVar.e;
        notification.setLatestEventInfo(context, charSequence, charSequence2, pendingIntent);
        notification.fullScreenIntent = pendingIntent2;
        if (bdVar.j > 0) {
            notification.flags |= 128;
        }
        return notification;
    }
}
