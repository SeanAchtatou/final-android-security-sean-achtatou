package android.support.v4.app;

import android.transition.Transition;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Map;

final class ai implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ View a;
    final /* synthetic */ Transition b;
    final /* synthetic */ View c;
    final /* synthetic */ am d;
    final /* synthetic */ Map e;
    final /* synthetic */ Map f;
    final /* synthetic */ ArrayList g;

    ai(View view, Transition transition, View view2, am amVar, Map map, Map map2, ArrayList arrayList) {
        this.a = view;
        this.b = transition;
        this.c = view2;
        this.d = amVar;
        this.e = map;
        this.f = map2;
        this.g = arrayList;
    }

    public final boolean onPreDraw() {
        this.a.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.b != null) {
            this.b.removeTarget(this.c);
        }
        View a2 = this.d.a();
        if (a2 == null) {
            return true;
        }
        if (!this.e.isEmpty()) {
            ag.a(this.f, a2);
            this.f.keySet().retainAll(this.e.values());
            for (Map.Entry entry : this.e.entrySet()) {
                View view = (View) this.f.get((String) entry.getValue());
                if (view != null) {
                    view.setTransitionName((String) entry.getKey());
                }
            }
        }
        if (this.b == null) {
            return true;
        }
        ag.b(this.g, a2);
        this.g.removeAll(this.f.values());
        this.g.add(this.c);
        ag.b(this.b, this.g);
        return true;
    }
}
