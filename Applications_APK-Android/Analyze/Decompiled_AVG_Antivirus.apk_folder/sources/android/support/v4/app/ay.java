package android.support.v4.app;

import android.os.Build;
import java.util.ArrayList;
import java.util.Iterator;

public final class ay {
    /* access modifiers changed from: private */
    public static final bg a;

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            a = new bi();
        } else if (Build.VERSION.SDK_INT >= 20) {
            a = new bh();
        } else if (Build.VERSION.SDK_INT >= 19) {
            a = new bo();
        } else if (Build.VERSION.SDK_INT >= 16) {
            a = new bn();
        } else if (Build.VERSION.SDK_INT >= 14) {
            a = new bm();
        } else if (Build.VERSION.SDK_INT >= 11) {
            a = new bl();
        } else if (Build.VERSION.SDK_INT >= 9) {
            a = new bk();
        } else {
            a = new bj();
        }
    }

    static /* synthetic */ void a(aw awVar, ArrayList arrayList) {
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            awVar.a((az) it.next());
        }
    }

    static /* synthetic */ void a(ax axVar, bp bpVar) {
        if (bpVar == null) {
            return;
        }
        if (bpVar instanceof bc) {
            bc bcVar = (bc) bpVar;
            bz.a(axVar, bcVar.d, bcVar.f, bcVar.e, bcVar.a);
        } else if (bpVar instanceof bf) {
            bf bfVar = (bf) bpVar;
            bz.a(axVar, bfVar.d, bfVar.f, bfVar.e, bfVar.a);
        } else if (bpVar instanceof bb) {
            bb bbVar = (bb) bpVar;
            bz.a(axVar, bbVar.d, bbVar.f, bbVar.e, bbVar.a, bbVar.b, bbVar.c);
        }
    }
}
