package android.support.v4.app;

import android.app.PendingIntent;
import android.os.Bundle;

public final class az extends bv {
    public static final bw d = new ba();
    public int a;
    public CharSequence b;
    public PendingIntent c;
    private final Bundle e;
    private final cd[] f;

    public final int a() {
        return this.a;
    }

    public final CharSequence b() {
        return this.b;
    }

    public final PendingIntent c() {
        return this.c;
    }

    public final Bundle d() {
        return this.e;
    }

    public final /* bridge */ /* synthetic */ cl[] e() {
        return this.f;
    }
}
