package android.support.v4.app;

import android.app.PendingIntent;
import android.os.Bundle;

public abstract class bv {
    public abstract int a();

    public abstract CharSequence b();

    public abstract PendingIntent c();

    public abstract Bundle d();

    public abstract cl[] e();
}
