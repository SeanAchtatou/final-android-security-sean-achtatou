package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.RemoteViews;
import java.util.ArrayList;

public final class bd {
    Notification A;
    public Notification B = new Notification();
    public ArrayList C;
    public Context a;
    public CharSequence b;
    public CharSequence c;
    PendingIntent d;
    PendingIntent e;
    RemoteViews f;
    public Bitmap g;
    public CharSequence h;
    public int i;
    int j;
    boolean k = true;
    public boolean l;
    public bp m;
    public CharSequence n;
    int o;
    int p;
    boolean q;
    String r;
    boolean s;
    String t;
    public ArrayList u = new ArrayList();
    boolean v = false;
    String w;
    Bundle x;
    int y = 0;
    int z = 0;

    public bd(Context context) {
        this.a = context;
        this.B.when = System.currentTimeMillis();
        this.B.audioStreamType = -1;
        this.j = 0;
        this.C = new ArrayList();
    }

    private void a(int i2, boolean z2) {
        if (z2) {
            this.B.flags |= i2;
            return;
        }
        this.B.flags &= i2 ^ -1;
    }

    public final bd a() {
        a(2, true);
        return this;
    }

    public final bd b() {
        a(16, false);
        return this;
    }

    public final Notification c() {
        bg a2 = ay.a;
        new be();
        return a2.a(this);
    }
}
