package android.support.v4.app;

import android.support.v4.e.a;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;

final class c implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ View a;
    final /* synthetic */ Object b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ f d;
    final /* synthetic */ boolean e;
    final /* synthetic */ Fragment f;
    final /* synthetic */ Fragment g;
    final /* synthetic */ a h;

    c(a aVar, View view, Object obj, ArrayList arrayList, f fVar, boolean z, Fragment fragment, Fragment fragment2) {
        this.h = aVar;
        this.a = view;
        this.b = obj;
        this.c = arrayList;
        this.d = fVar;
        this.e = z;
        this.f = fragment;
        this.g = fragment2;
    }

    public final boolean onPreDraw() {
        this.a.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.b == null) {
            return true;
        }
        ag.a(this.b, this.c);
        this.c.clear();
        a a2 = a.a(this.h, this.d, this.e, this.f);
        ag.a(this.b, this.d.d, a2, this.c);
        a.a(this.h, a2, this.d);
        a.a(this.f, this.g, this.e, a2);
        return true;
    }
}
