package android.support.v4.app;

import android.view.View;

final class j extends p {
    final /* synthetic */ Fragment a;

    j(Fragment fragment) {
        this.a = fragment;
    }

    public final View a(int i) {
        if (this.a.J != null) {
            return this.a.J.findViewById(i);
        }
        throw new IllegalStateException("Fragment does not have a view");
    }

    public final boolean a() {
        return this.a.J != null;
    }
}
