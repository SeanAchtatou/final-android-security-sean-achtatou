package android.support.v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.e.d;
import android.support.v4.e.e;
import android.support.v4.view.an;
import android.support.v4.view.bx;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import androidx.fragment.app.FragmentTransaction;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

final class t extends s implements an {
    static final Interpolator A = new DecelerateInterpolator(2.5f);
    static final Interpolator B = new DecelerateInterpolator(1.5f);
    static final Interpolator C = new AccelerateInterpolator(2.5f);
    static final Interpolator D = new AccelerateInterpolator(1.5f);
    static boolean a = false;
    static final boolean b;
    static Field r = null;
    ArrayList c;
    Runnable[] d;
    boolean e;
    ArrayList f;
    ArrayList g;
    ArrayList h;
    ArrayList i;
    ArrayList j;
    ArrayList k;
    ArrayList l;
    ArrayList m;
    int n = 0;
    r o;
    p p;
    Fragment q;
    boolean s;
    boolean t;
    boolean u;
    String v;
    boolean w;
    Bundle x = null;
    SparseArray y = null;
    Runnable z = new u(this);

    static {
        boolean z2 = false;
        if (Build.VERSION.SDK_INT >= 11) {
            z2 = true;
        }
        b = z2;
    }

    t() {
    }

    private Fragment a(Bundle bundle, String str) {
        int i2 = bundle.getInt(str, -1);
        if (i2 == -1) {
            return null;
        }
        if (i2 >= this.f.size()) {
            a(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i2));
        }
        Fragment fragment = (Fragment) this.f.get(i2);
        if (fragment != null) {
            return fragment;
        }
        a(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i2));
        return fragment;
    }

    private static Animation a(float f2, float f3) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f3);
        alphaAnimation.setInterpolator(B);
        alphaAnimation.setDuration(220);
        return alphaAnimation;
    }

    private static Animation a(float f2, float f3, float f4, float f5) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, f2, f3, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(A);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f4, f5);
        alphaAnimation.setInterpolator(B);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    private Animation a(Fragment fragment, int i2, boolean z2, int i3) {
        Animation loadAnimation;
        int i4 = fragment.H;
        Fragment.g();
        if (fragment.H != 0 && (loadAnimation = AnimationUtils.loadAnimation(this.o.b, fragment.H)) != null) {
            return loadAnimation;
        }
        if (i2 == 0) {
            return null;
        }
        char c2 = 65535;
        switch (i2) {
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*4097*/:
                if (!z2) {
                    c2 = 2;
                    break;
                } else {
                    c2 = 1;
                    break;
                }
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*4099*/:
                if (!z2) {
                    c2 = 6;
                    break;
                } else {
                    c2 = 5;
                    break;
                }
            case 8194:
                if (!z2) {
                    c2 = 4;
                    break;
                } else {
                    c2 = 3;
                    break;
                }
        }
        if (c2 < 0) {
            return null;
        }
        switch (c2) {
            case 1:
                return a(1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return a(1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return a(0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                return a(1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                return a(0.0f, 1.0f);
            case 6:
                return a(1.0f, 0.0f);
            default:
                if (i3 == 0 && this.o.e()) {
                    i3 = this.o.f();
                }
                return i3 == 0 ? null : null;
        }
    }

    private void a(int i2, a aVar) {
        synchronized (this) {
            if (this.k == null) {
                this.k = new ArrayList();
            }
            int size = this.k.size();
            if (i2 < size) {
                if (a) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + aVar);
                }
                this.k.set(i2, aVar);
            } else {
                while (size < i2) {
                    this.k.add(null);
                    if (this.l == null) {
                        this.l = new ArrayList();
                    }
                    if (a) {
                        Log.v("FragmentManager", "Adding available back stack index " + size);
                    }
                    this.l.add(Integer.valueOf(size));
                    size++;
                }
                if (a) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + aVar);
                }
                this.k.add(aVar);
            }
        }
    }

    private void a(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new e("FragmentManager"));
        if (this.o != null) {
            try {
                this.o.a("  ", printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        } else {
            try {
                a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e3) {
                Log.e("FragmentManager", "Failed dumping state", e3);
            }
        }
        throw runtimeException;
    }

    static boolean a(View view, Animation animation) {
        boolean z2;
        if (Build.VERSION.SDK_INT < 19 || bx.g(view) != 0 || !bx.B(view)) {
            return false;
        }
        if (animation instanceof AlphaAnimation) {
            z2 = true;
        } else {
            if (animation instanceof AnimationSet) {
                List<Animation> animations = ((AnimationSet) animation).getAnimations();
                int i2 = 0;
                while (true) {
                    if (i2 >= animations.size()) {
                        break;
                    } else if (animations.get(i2) instanceof AlphaAnimation) {
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            z2 = false;
        }
        return z2;
    }

    public static int b(int i2) {
        switch (i2) {
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN /*4097*/:
                return 8194;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE /*4099*/:
                return FragmentTransaction.TRANSIT_FRAGMENT_FADE;
            case 8194:
                return FragmentTransaction.TRANSIT_FRAGMENT_OPEN;
            default:
                return 0;
        }
    }

    private void b(Fragment fragment) {
        a(fragment, this.n, 0, 0, false);
    }

    private static void b(View view, Animation animation) {
        Animation.AnimationListener animationListener;
        if (view != null && animation != null && a(view, animation)) {
            try {
                if (r == null) {
                    Field declaredField = Animation.class.getDeclaredField("mListener");
                    r = declaredField;
                    declaredField.setAccessible(true);
                }
                animationListener = (Animation.AnimationListener) r.get(animation);
            } catch (NoSuchFieldException e2) {
                Log.e("FragmentManager", "No field with the name mListener is found in Animation class", e2);
                animationListener = null;
            } catch (IllegalAccessException e3) {
                Log.e("FragmentManager", "Cannot access Animation's mListener field", e3);
                animationListener = null;
            }
            animation.setAnimationListener(new w(view, animation, animationListener));
        }
    }

    private Fragment c(int i2) {
        if (this.g != null) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.g.get(size);
                if (fragment != null && fragment.x == i2) {
                    return fragment;
                }
            }
        }
        if (this.f != null) {
            for (int size2 = this.f.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = (Fragment) this.f.get(size2);
                if (fragment2 != null && fragment2.x == i2) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    private void c(Fragment fragment) {
        if (fragment.K != null) {
            if (this.y == null) {
                this.y = new SparseArray();
            } else {
                this.y.clear();
            }
            fragment.K.saveHierarchyState(this.y);
            if (this.y.size() > 0) {
                fragment.f = this.y;
                this.y = null;
            }
        }
    }

    private void n() {
        if (this.f != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f.size()) {
                    Fragment fragment = (Fragment) this.f.get(i3);
                    if (fragment != null) {
                        a(fragment);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private void o() {
        if (this.t) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        } else if (this.v != null) {
            throw new IllegalStateException("Can not perform this action inside of " + this.v);
        }
    }

    public final int a(a aVar) {
        int i2;
        synchronized (this) {
            if (this.l == null || this.l.size() <= 0) {
                if (this.k == null) {
                    this.k = new ArrayList();
                }
                i2 = this.k.size();
                if (a) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + aVar);
                }
                this.k.add(aVar);
            } else {
                i2 = ((Integer) this.l.remove(this.l.size() - 1)).intValue();
                if (a) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + aVar);
                }
                this.k.set(i2, aVar);
            }
        }
        return i2;
    }

    public final Fragment a(String str) {
        if (!(this.g == null || str == null)) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.g.get(size);
                if (fragment != null && str.equals(fragment.z)) {
                    return fragment;
                }
            }
        }
        if (!(this.f == null || str == null)) {
            for (int size2 = this.f.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = (Fragment) this.f.get(size2);
                if (fragment2 != null && str.equals(fragment2.z)) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public final af a() {
        return new a(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(float, float):android.view.animation.Animation
      android.support.v4.app.t.a(int, android.support.v4.app.a):void
      android.support.v4.app.t.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.t.a(android.os.Parcelable, java.util.List):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void */
    public final View a(View view, String str, Context context, AttributeSet attributeSet) {
        Fragment fragment;
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, z.a);
        String string = attributeValue == null ? obtainStyledAttributes.getString(0) : attributeValue;
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string2 = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (!Fragment.b(this.o.b, string)) {
            return null;
        }
        int id = view != null ? view.getId() : 0;
        if (id == -1 && resourceId == -1 && string2 == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + string);
        }
        Fragment c2 = resourceId != -1 ? c(resourceId) : null;
        if (c2 == null && string2 != null) {
            c2 = a(string2);
        }
        if (c2 == null && id != -1) {
            c2 = c(id);
        }
        if (a) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(resourceId) + " fname=" + string + " existing=" + c2);
        }
        if (c2 == null) {
            Fragment a2 = Fragment.a(context, string);
            a2.p = true;
            a2.x = resourceId != 0 ? resourceId : id;
            a2.y = id;
            a2.z = string2;
            a2.q = true;
            a2.t = this;
            a2.u = this.o;
            Bundle bundle = a2.e;
            a2.f();
            a(a2, true);
            fragment = a2;
        } else if (c2.q) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string2 + ", or parent id 0x" + Integer.toHexString(id) + " with another fragment for " + string);
        } else {
            c2.q = true;
            if (!c2.D) {
                Bundle bundle2 = c2.e;
                c2.f();
            }
            fragment = c2;
        }
        if (this.n > 0 || !fragment.p) {
            b(fragment);
        } else {
            a(fragment, 1, 0, 0, false);
        }
        if (fragment.J == null) {
            throw new IllegalStateException("Fragment " + string + " did not create a view.");
        }
        if (resourceId != 0) {
            fragment.J.setId(resourceId);
        }
        if (fragment.J.getTag() == null) {
            fragment.J.setTag(string2);
        }
        return fragment.J;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.t.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.t.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.s.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.view.an.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        a(i2, 0, 0, false);
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3, int i4, boolean z2) {
        if (this.o == null && i2 != 0) {
            throw new IllegalStateException("No host");
        } else if (z2 || this.n != i2) {
            this.n = i2;
            if (this.f != null) {
                int i5 = 0;
                boolean z3 = false;
                while (i5 < this.f.size()) {
                    Fragment fragment = (Fragment) this.f.get(i5);
                    if (fragment != null) {
                        a(fragment, i2, i3, i4, false);
                        if (fragment.N != null) {
                            z3 |= fragment.N.a();
                        }
                    }
                    i5++;
                    z3 = z3;
                }
                if (!z3) {
                    n();
                }
                if (this.s && this.o != null && this.n == 5) {
                    this.o.d();
                    this.s = false;
                }
            }
        }
    }

    public final void a(Configuration configuration) {
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.g.size()) {
                    Fragment fragment = (Fragment) this.g.get(i3);
                    if (fragment != null) {
                        fragment.onConfigurationChanged(configuration);
                        if (fragment.v != null) {
                            fragment.v.a(configuration);
                        }
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.a.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.f
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.app.Fragment, boolean):android.support.v4.e.a
      android.support.v4.app.a.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.e.a):android.support.v4.e.a
      android.support.v4.app.a.a(android.support.v4.app.a, android.support.v4.e.a, android.support.v4.app.f):void
      android.support.v4.app.a.a(android.support.v4.app.f, int, java.lang.Object):void
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.e.a, boolean):void
      android.support.v4.app.a.a(android.support.v4.e.a, java.lang.String, java.lang.String):void
      android.support.v4.app.a.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(android.support.v4.app.f, android.util.SparseArray, android.util.SparseArray):android.support.v4.app.f
      android.support.v4.app.af.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(Parcelable parcelable, List list) {
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.a != null) {
                if (list != null) {
                    for (int i2 = 0; i2 < list.size(); i2++) {
                        Fragment fragment = (Fragment) list.get(i2);
                        if (a) {
                            Log.v("FragmentManager", "restoreAllState: re-attaching retained " + fragment);
                        }
                        FragmentState fragmentState = fragmentManagerState.a[fragment.g];
                        fragmentState.k = fragment;
                        fragment.f = null;
                        fragment.s = 0;
                        fragment.q = false;
                        fragment.m = false;
                        fragment.j = null;
                        if (fragmentState.j != null) {
                            fragmentState.j.setClassLoader(this.o.b.getClassLoader());
                            fragment.f = fragmentState.j.getSparseParcelableArray("android:view_state");
                            fragment.e = fragmentState.j;
                        }
                    }
                }
                this.f = new ArrayList(fragmentManagerState.a.length);
                if (this.h != null) {
                    this.h.clear();
                }
                for (int i3 = 0; i3 < fragmentManagerState.a.length; i3++) {
                    FragmentState fragmentState2 = fragmentManagerState.a[i3];
                    if (fragmentState2 != null) {
                        r rVar = this.o;
                        Fragment fragment2 = this.q;
                        if (fragmentState2.k == null) {
                            Context context = rVar.b;
                            if (fragmentState2.i != null) {
                                fragmentState2.i.setClassLoader(context.getClassLoader());
                            }
                            fragmentState2.k = Fragment.a(context, fragmentState2.a, fragmentState2.i);
                            if (fragmentState2.j != null) {
                                fragmentState2.j.setClassLoader(context.getClassLoader());
                                fragmentState2.k.e = fragmentState2.j;
                            }
                            fragmentState2.k.a(fragmentState2.b, fragment2);
                            fragmentState2.k.p = fragmentState2.c;
                            fragmentState2.k.r = true;
                            fragmentState2.k.x = fragmentState2.d;
                            fragmentState2.k.y = fragmentState2.e;
                            fragmentState2.k.z = fragmentState2.f;
                            fragmentState2.k.C = fragmentState2.g;
                            fragmentState2.k.B = fragmentState2.h;
                            fragmentState2.k.t = rVar.d;
                            if (a) {
                                Log.v("FragmentManager", "Instantiated fragment " + fragmentState2.k);
                            }
                        }
                        Fragment fragment3 = fragmentState2.k;
                        if (a) {
                            Log.v("FragmentManager", "restoreAllState: active #" + i3 + ": " + fragment3);
                        }
                        this.f.add(fragment3);
                        fragmentState2.k = null;
                    } else {
                        this.f.add(null);
                        if (this.h == null) {
                            this.h = new ArrayList();
                        }
                        if (a) {
                            Log.v("FragmentManager", "restoreAllState: avail #" + i3);
                        }
                        this.h.add(Integer.valueOf(i3));
                    }
                }
                if (list != null) {
                    for (int i4 = 0; i4 < list.size(); i4++) {
                        Fragment fragment4 = (Fragment) list.get(i4);
                        if (fragment4.k >= 0) {
                            if (fragment4.k < this.f.size()) {
                                fragment4.j = (Fragment) this.f.get(fragment4.k);
                            } else {
                                Log.w("FragmentManager", "Re-attaching retained fragment " + fragment4 + " target no longer exists: " + fragment4.k);
                                fragment4.j = null;
                            }
                        }
                    }
                }
                if (fragmentManagerState.b != null) {
                    this.g = new ArrayList(fragmentManagerState.b.length);
                    for (int i5 = 0; i5 < fragmentManagerState.b.length; i5++) {
                        Fragment fragment5 = (Fragment) this.f.get(fragmentManagerState.b[i5]);
                        if (fragment5 == null) {
                            a(new IllegalStateException("No instantiated fragment for index #" + fragmentManagerState.b[i5]));
                        }
                        fragment5.m = true;
                        if (a) {
                            Log.v("FragmentManager", "restoreAllState: added #" + i5 + ": " + fragment5);
                        }
                        if (this.g.contains(fragment5)) {
                            throw new IllegalStateException("Already added!");
                        }
                        this.g.add(fragment5);
                    }
                } else {
                    this.g = null;
                }
                if (fragmentManagerState.c != null) {
                    this.i = new ArrayList(fragmentManagerState.c.length);
                    for (int i6 = 0; i6 < fragmentManagerState.c.length; i6++) {
                        a a2 = fragmentManagerState.c[i6].a(this);
                        if (a) {
                            Log.v("FragmentManager", "restoreAllState: back stack #" + i6 + " (index " + a2.p + "): " + a2);
                            a2.a("  ", new PrintWriter(new e("FragmentManager")), false);
                        }
                        this.i.add(a2);
                        if (a2.p >= 0) {
                            a(a2.p, a2);
                        }
                    }
                    return;
                }
                this.i = null;
            }
        }
    }

    public final void a(Fragment fragment) {
        if (!fragment.L) {
            return;
        }
        if (this.e) {
            this.w = true;
            return;
        }
        fragment.L = false;
        a(fragment, this.n, 0, 0, false);
    }

    public final void a(Fragment fragment, int i2, int i3) {
        if (a) {
            Log.v("FragmentManager", "remove: " + fragment + " nesting=" + fragment.s);
        }
        boolean z2 = !(fragment.s > 0);
        if (!fragment.B || z2) {
            if (this.g != null) {
                this.g.remove(fragment);
            }
            if (fragment.E && fragment.F) {
                this.s = true;
            }
            fragment.m = false;
            fragment.n = true;
            a(fragment, z2 ? 0 : 1, i2, i3, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.t.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(int, int, int, boolean):void
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.s.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.view.an.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    /* access modifiers changed from: package-private */
    public final void a(Fragment fragment, int i2, int i3, int i4, boolean z2) {
        ViewGroup viewGroup;
        Parcelable parcelable;
        if ((!fragment.m || fragment.B) && i2 > 1) {
            i2 = 1;
        }
        if (fragment.n && i2 > fragment.b) {
            i2 = fragment.b;
        }
        if (fragment.L && fragment.b < 4 && i2 > 3) {
            i2 = 3;
        }
        if (fragment.b >= i2) {
            if (fragment.b > i2) {
                switch (fragment.b) {
                    case 5:
                        if (i2 < 5) {
                            if (a) {
                                Log.v("FragmentManager", "movefrom RESUMED: " + fragment);
                            }
                            if (fragment.v != null) {
                                fragment.v.a(4);
                            }
                            fragment.G = false;
                            fragment.G = true;
                            if (!fragment.G) {
                                throw new cp("Fragment " + fragment + " did not call through to super.onPause()");
                            }
                            fragment.o = false;
                        }
                    case 4:
                        if (i2 < 4) {
                            if (a) {
                                Log.v("FragmentManager", "movefrom STARTED: " + fragment);
                            }
                            if (fragment.v != null) {
                                fragment.v.k();
                            }
                            fragment.G = false;
                            fragment.G = true;
                            if (!fragment.G) {
                                throw new cp("Fragment " + fragment + " did not call through to super.onStop()");
                            }
                        }
                    case 3:
                        if (i2 < 3) {
                            if (a) {
                                Log.v("FragmentManager", "movefrom STOPPED: " + fragment);
                            }
                            fragment.l();
                        }
                    case 2:
                        if (i2 < 2) {
                            if (a) {
                                Log.v("FragmentManager", "movefrom ACTIVITY_CREATED: " + fragment);
                            }
                            if (fragment.J != null && this.o.b() && fragment.f == null) {
                                c(fragment);
                            }
                            if (fragment.v != null) {
                                fragment.v.a(1);
                            }
                            fragment.G = false;
                            fragment.G = true;
                            if (!fragment.G) {
                                throw new cp("Fragment " + fragment + " did not call through to super.onDestroyView()");
                            }
                            if (fragment.N != null) {
                                fragment.N.e();
                            }
                            if (!(fragment.J == null || fragment.I == null)) {
                                Animation a2 = (this.n <= 0 || this.u) ? null : a(fragment, i3, false, i4);
                                if (a2 != null) {
                                    fragment.c = fragment.J;
                                    fragment.d = i2;
                                    a2.setAnimationListener(new v(this, fragment.J, a2, fragment));
                                    fragment.J.startAnimation(a2);
                                }
                                fragment.I.removeView(fragment.J);
                            }
                            fragment.I = null;
                            fragment.J = null;
                            fragment.K = null;
                        }
                        break;
                    case 1:
                        if (i2 <= 0) {
                            if (this.u && fragment.c != null) {
                                View view = fragment.c;
                                fragment.c = null;
                                view.clearAnimation();
                            }
                            if (fragment.c == null) {
                                if (a) {
                                    Log.v("FragmentManager", "movefrom CREATED: " + fragment);
                                }
                                if (!fragment.D) {
                                    fragment.m();
                                }
                                fragment.G = false;
                                fragment.G = true;
                                if (fragment.G) {
                                    if (!z2) {
                                        if (!fragment.D) {
                                            if (fragment.g >= 0) {
                                                if (a) {
                                                    Log.v("FragmentManager", "Freeing fragment index " + fragment);
                                                }
                                                this.f.set(fragment.g, null);
                                                if (this.h == null) {
                                                    this.h = new ArrayList();
                                                }
                                                this.h.add(Integer.valueOf(fragment.g));
                                                this.o.a(fragment.h);
                                                fragment.g = -1;
                                                fragment.h = null;
                                                fragment.m = false;
                                                fragment.n = false;
                                                fragment.o = false;
                                                fragment.p = false;
                                                fragment.q = false;
                                                fragment.r = false;
                                                fragment.s = 0;
                                                fragment.t = null;
                                                fragment.v = null;
                                                fragment.u = null;
                                                fragment.x = 0;
                                                fragment.y = 0;
                                                fragment.z = null;
                                                fragment.A = false;
                                                fragment.B = false;
                                                fragment.D = false;
                                                fragment.N = null;
                                                fragment.O = false;
                                                fragment.P = false;
                                                break;
                                            }
                                        } else {
                                            fragment.u = null;
                                            fragment.w = null;
                                            fragment.t = null;
                                            fragment.v = null;
                                            break;
                                        }
                                    }
                                } else {
                                    throw new cp("Fragment " + fragment + " did not call through to super.onDetach()");
                                }
                            } else {
                                fragment.d = i2;
                                i2 = 1;
                                break;
                            }
                        }
                        break;
                }
            }
        } else if (!fragment.p || fragment.q) {
            if (fragment.c != null) {
                fragment.c = null;
                a(fragment, fragment.d, 0, 0, true);
            }
            switch (fragment.b) {
                case 0:
                    if (a) {
                        Log.v("FragmentManager", "moveto CREATED: " + fragment);
                    }
                    if (fragment.e != null) {
                        fragment.e.setClassLoader(this.o.b.getClassLoader());
                        fragment.f = fragment.e.getSparseParcelableArray("android:view_state");
                        fragment.j = a(fragment.e, "android:target_state");
                        if (fragment.j != null) {
                            fragment.l = fragment.e.getInt("android:target_req_state", 0);
                        }
                        fragment.M = fragment.e.getBoolean("android:user_visible_hint", true);
                        if (!fragment.M) {
                            fragment.L = true;
                            if (i2 > 3) {
                                i2 = 3;
                            }
                        }
                    }
                    fragment.u = this.o;
                    fragment.w = this.q;
                    fragment.t = this.q != null ? this.q.v : this.o.d;
                    fragment.G = false;
                    fragment.G = true;
                    if ((fragment.u == null ? null : fragment.u.g()) != null) {
                        fragment.G = false;
                        fragment.G = true;
                    }
                    if (!fragment.G) {
                        throw new cp("Fragment " + fragment + " did not call through to super.onAttach()");
                    }
                    Fragment fragment2 = fragment.w;
                    if (!fragment.D) {
                        Bundle bundle = fragment.e;
                        if (fragment.v != null) {
                            fragment.v.t = false;
                        }
                        fragment.G = false;
                        fragment.a(bundle);
                        if (!fragment.G) {
                            throw new cp("Fragment " + fragment + " did not call through to super.onCreate()");
                        } else if (!(bundle == null || (parcelable = bundle.getParcelable("android:support:fragments")) == null)) {
                            if (fragment.v == null) {
                                fragment.j();
                            }
                            fragment.v.a(parcelable, (List) null);
                            fragment.v.g();
                        }
                    }
                    fragment.D = false;
                    if (fragment.p) {
                        Bundle bundle2 = fragment.e;
                        LayoutInflater e2 = fragment.e();
                        Bundle bundle3 = fragment.e;
                        fragment.J = fragment.b(e2, (ViewGroup) null);
                        if (fragment.J != null) {
                            fragment.K = fragment.J;
                            if (Build.VERSION.SDK_INT >= 11) {
                                bx.A(fragment.J);
                            } else {
                                fragment.J = av.a(fragment.J);
                            }
                            if (fragment.A) {
                                fragment.J.setVisibility(8);
                            }
                            View view2 = fragment.J;
                            Bundle bundle4 = fragment.e;
                            Fragment.h();
                        } else {
                            fragment.K = null;
                        }
                    }
                case 1:
                    if (i2 > 1) {
                        if (a) {
                            Log.v("FragmentManager", "moveto ACTIVITY_CREATED: " + fragment);
                        }
                        if (!fragment.p) {
                            if (fragment.y != 0) {
                                viewGroup = (ViewGroup) this.p.a(fragment.y);
                                if (viewGroup == null && !fragment.r) {
                                    StringBuilder append = new StringBuilder("No view found for id 0x").append(Integer.toHexString(fragment.y)).append(" (");
                                    if (fragment.u == null) {
                                        throw new IllegalStateException("Fragment " + fragment + " not attached to Activity");
                                    }
                                    a(new IllegalArgumentException(append.append(fragment.u.b.getResources().getResourceName(fragment.y)).append(") for fragment ").append(fragment).toString()));
                                }
                            } else {
                                viewGroup = null;
                            }
                            fragment.I = viewGroup;
                            Bundle bundle5 = fragment.e;
                            LayoutInflater e3 = fragment.e();
                            Bundle bundle6 = fragment.e;
                            fragment.J = fragment.b(e3, viewGroup);
                            if (fragment.J != null) {
                                fragment.K = fragment.J;
                                if (Build.VERSION.SDK_INT >= 11) {
                                    bx.A(fragment.J);
                                } else {
                                    fragment.J = av.a(fragment.J);
                                }
                                if (viewGroup != null) {
                                    Animation a3 = a(fragment, i3, true, i4);
                                    if (a3 != null) {
                                        b(fragment.J, a3);
                                        fragment.J.startAnimation(a3);
                                    }
                                    viewGroup.addView(fragment.J);
                                }
                                if (fragment.A) {
                                    fragment.J.setVisibility(8);
                                }
                                View view3 = fragment.J;
                                Bundle bundle7 = fragment.e;
                                Fragment.h();
                            } else {
                                fragment.K = null;
                            }
                        }
                        Bundle bundle8 = fragment.e;
                        if (fragment.v != null) {
                            fragment.v.t = false;
                        }
                        fragment.G = false;
                        fragment.G = true;
                        if (!fragment.G) {
                            throw new cp("Fragment " + fragment + " did not call through to super.onActivityCreated()");
                        }
                        if (fragment.v != null) {
                            fragment.v.h();
                        }
                        if (fragment.J != null) {
                            Bundle bundle9 = fragment.e;
                            if (fragment.f != null) {
                                fragment.K.restoreHierarchyState(fragment.f);
                                fragment.f = null;
                            }
                            fragment.G = false;
                            fragment.G = true;
                            if (!fragment.G) {
                                throw new cp("Fragment " + fragment + " did not call through to super.onViewStateRestored()");
                            }
                        }
                        fragment.e = null;
                    }
                case 2:
                case 3:
                    if (i2 > 3) {
                        if (a) {
                            Log.v("FragmentManager", "moveto STARTED: " + fragment);
                        }
                        fragment.k();
                    }
                case 4:
                    if (i2 > 4) {
                        if (a) {
                            Log.v("FragmentManager", "moveto RESUMED: " + fragment);
                        }
                        fragment.o = true;
                        if (fragment.v != null) {
                            fragment.v.t = false;
                            fragment.v.d();
                        }
                        fragment.G = false;
                        fragment.G = true;
                        if (fragment.G) {
                            if (fragment.v != null) {
                                fragment.v.j();
                                fragment.v.d();
                            }
                            fragment.e = null;
                            fragment.f = null;
                            break;
                        } else {
                            throw new cp("Fragment " + fragment + " did not call through to super.onResume()");
                        }
                    }
                    break;
            }
        } else {
            return;
        }
        fragment.b = i2;
    }

    public final void a(Fragment fragment, boolean z2) {
        if (this.g == null) {
            this.g = new ArrayList();
        }
        if (a) {
            Log.v("FragmentManager", "add: " + fragment);
        }
        if (fragment.g < 0) {
            if (this.h == null || this.h.size() <= 0) {
                if (this.f == null) {
                    this.f = new ArrayList();
                }
                fragment.a(this.f.size(), this.q);
                this.f.add(fragment);
            } else {
                fragment.a(((Integer) this.h.remove(this.h.size() - 1)).intValue(), this.q);
                this.f.set(fragment.g, fragment);
            }
            if (a) {
                Log.v("FragmentManager", "Allocated fragment index " + fragment);
            }
        }
        if (fragment.B) {
            return;
        }
        if (this.g.contains(fragment)) {
            throw new IllegalStateException("Fragment already added: " + fragment);
        }
        this.g.add(fragment);
        fragment.m = true;
        fragment.n = false;
        if (fragment.E && fragment.F) {
            this.s = true;
        }
        if (z2) {
            b(fragment);
        }
    }

    public final void a(r rVar, p pVar, Fragment fragment) {
        if (this.o != null) {
            throw new IllegalStateException("Already attached");
        }
        this.o = rVar;
        this.p = pVar;
        this.q = fragment;
    }

    public final void a(Runnable runnable, boolean z2) {
        if (!z2) {
            o();
        }
        synchronized (this) {
            if (this.u || this.o == null) {
                throw new IllegalStateException("Activity has been destroyed");
            }
            if (this.c == null) {
                this.c = new ArrayList();
            }
            this.c.add(runnable);
            if (this.c.size() == 1) {
                this.o.h().removeCallbacks(this.z);
                this.o.h().post(this.z);
            }
        }
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        int size3;
        int size4;
        int size5;
        int size6;
        String str2 = str + "    ";
        if (this.f != null && (size6 = this.f.size()) > 0) {
            printWriter.print(str);
            printWriter.print("Active Fragments in ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this)));
            printWriter.println(":");
            for (int i2 = 0; i2 < size6; i2++) {
                Fragment fragment = (Fragment) this.f.get(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(fragment);
                if (fragment != null) {
                    printWriter.print(str2);
                    printWriter.print("mFragmentId=#");
                    printWriter.print(Integer.toHexString(fragment.x));
                    printWriter.print(" mContainerId=#");
                    printWriter.print(Integer.toHexString(fragment.y));
                    printWriter.print(" mTag=");
                    printWriter.println(fragment.z);
                    printWriter.print(str2);
                    printWriter.print("mState=");
                    printWriter.print(fragment.b);
                    printWriter.print(" mIndex=");
                    printWriter.print(fragment.g);
                    printWriter.print(" mWho=");
                    printWriter.print(fragment.h);
                    printWriter.print(" mBackStackNesting=");
                    printWriter.println(fragment.s);
                    printWriter.print(str2);
                    printWriter.print("mAdded=");
                    printWriter.print(fragment.m);
                    printWriter.print(" mRemoving=");
                    printWriter.print(fragment.n);
                    printWriter.print(" mResumed=");
                    printWriter.print(fragment.o);
                    printWriter.print(" mFromLayout=");
                    printWriter.print(fragment.p);
                    printWriter.print(" mInLayout=");
                    printWriter.println(fragment.q);
                    printWriter.print(str2);
                    printWriter.print("mHidden=");
                    printWriter.print(fragment.A);
                    printWriter.print(" mDetached=");
                    printWriter.print(fragment.B);
                    printWriter.print(" mMenuVisible=");
                    printWriter.print(fragment.F);
                    printWriter.print(" mHasMenu=");
                    printWriter.println(fragment.E);
                    printWriter.print(str2);
                    printWriter.print("mRetainInstance=");
                    printWriter.print(fragment.C);
                    printWriter.print(" mRetaining=");
                    printWriter.print(fragment.D);
                    printWriter.print(" mUserVisibleHint=");
                    printWriter.println(fragment.M);
                    if (fragment.t != null) {
                        printWriter.print(str2);
                        printWriter.print("mFragmentManager=");
                        printWriter.println(fragment.t);
                    }
                    if (fragment.u != null) {
                        printWriter.print(str2);
                        printWriter.print("mHost=");
                        printWriter.println(fragment.u);
                    }
                    if (fragment.w != null) {
                        printWriter.print(str2);
                        printWriter.print("mParentFragment=");
                        printWriter.println(fragment.w);
                    }
                    if (fragment.i != null) {
                        printWriter.print(str2);
                        printWriter.print("mArguments=");
                        printWriter.println(fragment.i);
                    }
                    if (fragment.e != null) {
                        printWriter.print(str2);
                        printWriter.print("mSavedFragmentState=");
                        printWriter.println(fragment.e);
                    }
                    if (fragment.f != null) {
                        printWriter.print(str2);
                        printWriter.print("mSavedViewState=");
                        printWriter.println(fragment.f);
                    }
                    if (fragment.j != null) {
                        printWriter.print(str2);
                        printWriter.print("mTarget=");
                        printWriter.print(fragment.j);
                        printWriter.print(" mTargetRequestCode=");
                        printWriter.println(fragment.l);
                    }
                    if (fragment.H != 0) {
                        printWriter.print(str2);
                        printWriter.print("mNextAnim=");
                        printWriter.println(fragment.H);
                    }
                    if (fragment.I != null) {
                        printWriter.print(str2);
                        printWriter.print("mContainer=");
                        printWriter.println(fragment.I);
                    }
                    if (fragment.J != null) {
                        printWriter.print(str2);
                        printWriter.print("mView=");
                        printWriter.println(fragment.J);
                    }
                    if (fragment.K != null) {
                        printWriter.print(str2);
                        printWriter.print("mInnerView=");
                        printWriter.println(fragment.J);
                    }
                    if (fragment.c != null) {
                        printWriter.print(str2);
                        printWriter.print("mAnimatingAway=");
                        printWriter.println(fragment.c);
                        printWriter.print(str2);
                        printWriter.print("mStateAfterAnimating=");
                        printWriter.println(fragment.d);
                    }
                    if (fragment.N != null) {
                        printWriter.print(str2);
                        printWriter.println("Loader Manager:");
                        fragment.N.a(str2 + "  ", printWriter);
                    }
                    if (fragment.v != null) {
                        printWriter.print(str2);
                        printWriter.println("Child " + fragment.v + ":");
                        fragment.v.a(str2 + "  ", fileDescriptor, printWriter, strArr);
                    }
                }
            }
        }
        if (this.g != null && (size5 = this.g.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i3 = 0; i3 < size5; i3++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(((Fragment) this.g.get(i3)).toString());
            }
        }
        if (this.j != null && (size4 = this.j.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i4 = 0; i4 < size4; i4++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i4);
                printWriter.print(": ");
                printWriter.println(((Fragment) this.j.get(i4)).toString());
            }
        }
        if (this.i != null && (size3 = this.i.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i5 = 0; i5 < size3; i5++) {
                a aVar = (a) this.i.get(i5);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i5);
                printWriter.print(": ");
                printWriter.println(aVar.toString());
                aVar.a(str2, printWriter);
            }
        }
        synchronized (this) {
            if (this.k != null && (size2 = this.k.size()) > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack Indices:");
                for (int i6 = 0; i6 < size2; i6++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i6);
                    printWriter.print(": ");
                    printWriter.println((a) this.k.get(i6));
                }
            }
            if (this.l != null && this.l.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.l.toArray()));
            }
        }
        if (this.c != null && (size = this.c.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Pending Actions:");
            for (int i7 = 0; i7 < size; i7++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i7);
                printWriter.print(": ");
                printWriter.println((Runnable) this.c.get(i7));
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.o);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.p);
        if (this.q != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.q);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.n);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.t);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.u);
        if (this.s) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.s);
        }
        if (this.v != null) {
            printWriter.print(str);
            printWriter.print("  mNoTransactionsBecause=");
            printWriter.println(this.v);
        }
        if (this.h != null && this.h.size() > 0) {
            printWriter.print(str);
            printWriter.print("  mAvailIndices: ");
            printWriter.println(Arrays.toString(this.h.toArray()));
        }
    }

    public final boolean a(Menu menu) {
        boolean z2;
        if (this.g == null) {
            return false;
        }
        boolean z3 = false;
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = (Fragment) this.g.get(i2);
            if (fragment != null) {
                if (!fragment.A) {
                    z2 = fragment.E && fragment.F;
                    if (fragment.v != null) {
                        z2 |= fragment.v.a(menu);
                    }
                } else {
                    z2 = false;
                }
                if (z2) {
                    z3 = true;
                }
            }
        }
        return z3;
    }

    public final boolean a(Menu menu, MenuInflater menuInflater) {
        boolean z2;
        boolean z3;
        boolean z4;
        ArrayList arrayList = null;
        if (this.g != null) {
            int i2 = 0;
            z2 = false;
            while (i2 < this.g.size()) {
                Fragment fragment = (Fragment) this.g.get(i2);
                if (fragment != null) {
                    if (!fragment.A) {
                        z4 = fragment.E && fragment.F;
                        if (fragment.v != null) {
                            z4 |= fragment.v.a(menu, menuInflater);
                        }
                    } else {
                        z4 = false;
                    }
                    if (z4) {
                        if (arrayList == null) {
                            arrayList = new ArrayList();
                        }
                        arrayList.add(fragment);
                        z3 = true;
                        i2++;
                        z2 = z3;
                    }
                }
                z3 = z2;
                i2++;
                z2 = z3;
            }
        } else {
            z2 = false;
        }
        if (this.j != null) {
            for (int i3 = 0; i3 < this.j.size(); i3++) {
                Fragment fragment2 = (Fragment) this.j.get(i3);
                if (arrayList == null || !arrayList.contains(fragment2)) {
                    Fragment.i();
                }
            }
        }
        this.j = arrayList;
        return z2;
    }

    public final boolean a(MenuItem menuItem) {
        if (this.g == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = (Fragment) this.g.get(i2);
            if (fragment != null) {
                if (!fragment.A && fragment.v != null && fragment.v.a(menuItem)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.t.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(int, int, int, boolean):void
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.s.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.view.an.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    public final void b(Fragment fragment, int i2, int i3) {
        if (a) {
            Log.v("FragmentManager", "hide: " + fragment);
        }
        if (!fragment.A) {
            fragment.A = true;
            if (fragment.J != null) {
                Animation a2 = a(fragment, i2, false, i3);
                if (a2 != null) {
                    b(fragment.J, a2);
                    fragment.J.startAnimation(a2);
                }
                fragment.J.setVisibility(8);
            }
            if (fragment.m && fragment.E && fragment.F) {
                this.s = true;
            }
            Fragment.c();
        }
    }

    public final void b(Menu menu) {
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.g.size()) {
                    Fragment fragment = (Fragment) this.g.get(i3);
                    if (!(fragment == null || fragment.A || fragment.v == null)) {
                        fragment.v.b(menu);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final boolean b() {
        return d();
    }

    public final boolean b(MenuItem menuItem) {
        if (this.g == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = (Fragment) this.g.get(i2);
            if (fragment != null) {
                if (!fragment.A && fragment.v != null && fragment.v.b(menuItem)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.t.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(int, int, int, boolean):void
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.s.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.view.an.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    public final void c(Fragment fragment, int i2, int i3) {
        if (a) {
            Log.v("FragmentManager", "show: " + fragment);
        }
        if (fragment.A) {
            fragment.A = false;
            if (fragment.J != null) {
                Animation a2 = a(fragment, i2, true, i3);
                if (a2 != null) {
                    b(fragment.J, a2);
                    fragment.J.startAnimation(a2);
                }
                fragment.J.setVisibility(0);
            }
            if (fragment.m && fragment.E && fragment.F) {
                this.s = true;
            }
            Fragment.c();
        }
    }

    public final boolean c() {
        int size;
        o();
        d();
        if (this.i == null || this.i.size() - 1 < 0) {
            return false;
        }
        a aVar = (a) this.i.remove(size);
        SparseArray sparseArray = new SparseArray();
        SparseArray sparseArray2 = new SparseArray();
        aVar.a(sparseArray, sparseArray2);
        aVar.a((f) null, sparseArray, sparseArray2);
        e();
        return true;
    }

    public final void d(Fragment fragment, int i2, int i3) {
        if (a) {
            Log.v("FragmentManager", "detach: " + fragment);
        }
        if (!fragment.B) {
            fragment.B = true;
            if (fragment.m) {
                if (this.g != null) {
                    if (a) {
                        Log.v("FragmentManager", "remove from detach: " + fragment);
                    }
                    this.g.remove(fragment);
                }
                if (fragment.E && fragment.F) {
                    this.s = true;
                }
                fragment.m = false;
                a(fragment, 1, i2, i3, false);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0087, code lost:
        r6.e = true;
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x008a, code lost:
        if (r1 >= r3) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008c, code lost:
        r6.d[r1].run();
        r6.d[r1] = null;
        r1 = r1 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean d() {
        /*
            r6 = this;
            r0 = 1
            r2 = 0
            boolean r1 = r6.e
            if (r1 == 0) goto L_0x000e
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Recursive entry to executePendingTransactions"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            android.os.Looper r1 = android.os.Looper.myLooper()
            android.support.v4.app.r r3 = r6.o
            android.os.Handler r3 = r3.h()
            android.os.Looper r3 = r3.getLooper()
            if (r1 == r3) goto L_0x0026
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Must be called from main thread of process"
            r0.<init>(r1)
            throw r0
        L_0x0026:
            r1 = r2
        L_0x0027:
            monitor-enter(r6)
            java.util.ArrayList r3 = r6.c     // Catch:{ all -> 0x009b }
            if (r3 == 0) goto L_0x0034
            java.util.ArrayList r3 = r6.c     // Catch:{ all -> 0x009b }
            int r3 = r3.size()     // Catch:{ all -> 0x009b }
            if (r3 != 0) goto L_0x005c
        L_0x0034:
            monitor-exit(r6)     // Catch:{ all -> 0x009b }
            boolean r0 = r6.w
            if (r0 == 0) goto L_0x00a9
            r3 = r2
            r4 = r2
        L_0x003b:
            java.util.ArrayList r0 = r6.f
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x00a2
            java.util.ArrayList r0 = r6.f
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
            if (r0 == 0) goto L_0x0058
            android.support.v4.app.ap r5 = r0.N
            if (r5 == 0) goto L_0x0058
            android.support.v4.app.ap r0 = r0.N
            boolean r0 = r0.a()
            r4 = r4 | r0
        L_0x0058:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x003b
        L_0x005c:
            java.util.ArrayList r1 = r6.c     // Catch:{ all -> 0x009b }
            int r3 = r1.size()     // Catch:{ all -> 0x009b }
            java.lang.Runnable[] r1 = r6.d     // Catch:{ all -> 0x009b }
            if (r1 == 0) goto L_0x006b
            java.lang.Runnable[] r1 = r6.d     // Catch:{ all -> 0x009b }
            int r1 = r1.length     // Catch:{ all -> 0x009b }
            if (r1 >= r3) goto L_0x006f
        L_0x006b:
            java.lang.Runnable[] r1 = new java.lang.Runnable[r3]     // Catch:{ all -> 0x009b }
            r6.d = r1     // Catch:{ all -> 0x009b }
        L_0x006f:
            java.util.ArrayList r1 = r6.c     // Catch:{ all -> 0x009b }
            java.lang.Runnable[] r4 = r6.d     // Catch:{ all -> 0x009b }
            r1.toArray(r4)     // Catch:{ all -> 0x009b }
            java.util.ArrayList r1 = r6.c     // Catch:{ all -> 0x009b }
            r1.clear()     // Catch:{ all -> 0x009b }
            android.support.v4.app.r r1 = r6.o     // Catch:{ all -> 0x009b }
            android.os.Handler r1 = r1.h()     // Catch:{ all -> 0x009b }
            java.lang.Runnable r4 = r6.z     // Catch:{ all -> 0x009b }
            r1.removeCallbacks(r4)     // Catch:{ all -> 0x009b }
            monitor-exit(r6)     // Catch:{ all -> 0x009b }
            r6.e = r0
            r1 = r2
        L_0x008a:
            if (r1 >= r3) goto L_0x009e
            java.lang.Runnable[] r4 = r6.d
            r4 = r4[r1]
            r4.run()
            java.lang.Runnable[] r4 = r6.d
            r5 = 0
            r4[r1] = r5
            int r1 = r1 + 1
            goto L_0x008a
        L_0x009b:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x009b }
            throw r0
        L_0x009e:
            r6.e = r2
            r1 = r0
            goto L_0x0027
        L_0x00a2:
            if (r4 != 0) goto L_0x00a9
            r6.w = r2
            r6.n()
        L_0x00a9:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.t.d():boolean");
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        if (this.m != null) {
            for (int i2 = 0; i2 < this.m.size(); i2++) {
                this.m.get(i2);
            }
        }
    }

    public final void e(Fragment fragment, int i2, int i3) {
        if (a) {
            Log.v("FragmentManager", "attach: " + fragment);
        }
        if (fragment.B) {
            fragment.B = false;
            if (!fragment.m) {
                if (this.g == null) {
                    this.g = new ArrayList();
                }
                if (this.g.contains(fragment)) {
                    throw new IllegalStateException("Fragment already added: " + fragment);
                }
                if (a) {
                    Log.v("FragmentManager", "add from attach: " + fragment);
                }
                this.g.add(fragment);
                fragment.m = true;
                if (fragment.E && fragment.F) {
                    this.s = true;
                }
                a(fragment, this.n, i2, i3, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final Parcelable f() {
        int[] iArr;
        int size;
        int size2;
        boolean z2;
        Bundle bundle;
        Parcelable f2;
        BackStackState[] backStackStateArr = null;
        d();
        if (b) {
            this.t = true;
        }
        if (this.f == null || this.f.size() <= 0) {
            return null;
        }
        int size3 = this.f.size();
        FragmentState[] fragmentStateArr = new FragmentState[size3];
        int i2 = 0;
        boolean z3 = false;
        while (i2 < size3) {
            Fragment fragment = (Fragment) this.f.get(i2);
            if (fragment != null) {
                if (fragment.g < 0) {
                    a(new IllegalStateException("Failure saving state: active " + fragment + " has cleared index: " + fragment.g));
                }
                FragmentState fragmentState = new FragmentState(fragment);
                fragmentStateArr[i2] = fragmentState;
                if (fragment.b <= 0 || fragmentState.j != null) {
                    fragmentState.j = fragment.e;
                } else {
                    if (this.x == null) {
                        this.x = new Bundle();
                    }
                    Bundle bundle2 = this.x;
                    if (!(fragment.v == null || (f2 = fragment.v.f()) == null)) {
                        bundle2.putParcelable("android:support:fragments", f2);
                    }
                    if (!this.x.isEmpty()) {
                        bundle = this.x;
                        this.x = null;
                    } else {
                        bundle = null;
                    }
                    if (fragment.J != null) {
                        c(fragment);
                    }
                    if (fragment.f != null) {
                        if (bundle == null) {
                            bundle = new Bundle();
                        }
                        bundle.putSparseParcelableArray("android:view_state", fragment.f);
                    }
                    if (!fragment.M) {
                        if (bundle == null) {
                            bundle = new Bundle();
                        }
                        bundle.putBoolean("android:user_visible_hint", fragment.M);
                    }
                    fragmentState.j = bundle;
                    if (fragment.j != null) {
                        if (fragment.j.g < 0) {
                            a(new IllegalStateException("Failure saving state: " + fragment + " has target not in fragment manager: " + fragment.j));
                        }
                        if (fragmentState.j == null) {
                            fragmentState.j = new Bundle();
                        }
                        Bundle bundle3 = fragmentState.j;
                        Fragment fragment2 = fragment.j;
                        if (fragment2.g < 0) {
                            a(new IllegalStateException("Fragment " + fragment2 + " is not currently in the FragmentManager"));
                        }
                        bundle3.putInt("android:target_state", fragment2.g);
                        if (fragment.l != 0) {
                            fragmentState.j.putInt("android:target_req_state", fragment.l);
                        }
                    }
                }
                if (a) {
                    Log.v("FragmentManager", "Saved state of " + fragment + ": " + fragmentState.j);
                }
                z2 = true;
            } else {
                z2 = z3;
            }
            i2++;
            z3 = z2;
        }
        if (z3) {
            if (this.g == null || (size2 = this.g.size()) <= 0) {
                iArr = null;
            } else {
                iArr = new int[size2];
                for (int i3 = 0; i3 < size2; i3++) {
                    iArr[i3] = ((Fragment) this.g.get(i3)).g;
                    if (iArr[i3] < 0) {
                        a(new IllegalStateException("Failure saving state: active " + this.g.get(i3) + " has cleared index: " + iArr[i3]));
                    }
                    if (a) {
                        Log.v("FragmentManager", "saveAllState: adding fragment #" + i3 + ": " + this.g.get(i3));
                    }
                }
            }
            if (this.i != null && (size = this.i.size()) > 0) {
                backStackStateArr = new BackStackState[size];
                for (int i4 = 0; i4 < size; i4++) {
                    backStackStateArr[i4] = new BackStackState((a) this.i.get(i4));
                    if (a) {
                        Log.v("FragmentManager", "saveAllState: adding back stack #" + i4 + ": " + this.i.get(i4));
                    }
                }
            }
            FragmentManagerState fragmentManagerState = new FragmentManagerState();
            fragmentManagerState.a = fragmentStateArr;
            fragmentManagerState.b = iArr;
            fragmentManagerState.c = backStackStateArr;
            return fragmentManagerState;
        } else if (!a) {
            return null;
        } else {
            Log.v("FragmentManager", "saveAllState: no fragments!");
            return null;
        }
    }

    public final void g() {
        this.t = false;
        a(1);
    }

    public final void h() {
        this.t = false;
        a(2);
    }

    public final void i() {
        this.t = false;
        a(4);
    }

    public final void j() {
        this.t = false;
        a(5);
    }

    public final void k() {
        this.t = true;
        a(3);
    }

    public final void l() {
        this.u = true;
        d();
        a(0);
        this.o = null;
        this.p = null;
        this.q = null;
    }

    public final void m() {
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.g.size()) {
                    Fragment fragment = (Fragment) this.g.get(i3);
                    if (fragment != null) {
                        fragment.onLowMemory();
                        if (fragment.v != null) {
                            fragment.v.m();
                        }
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        if (this.q != null) {
            d.a(this.q, sb);
        } else {
            d.a(this.o, sb);
        }
        sb.append("}}");
        return sb.toString();
    }
}
