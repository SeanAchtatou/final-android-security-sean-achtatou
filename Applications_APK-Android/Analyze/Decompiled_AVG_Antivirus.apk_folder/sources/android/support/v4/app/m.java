package android.support.v4.app;

import android.os.Handler;
import android.os.Message;

final class m extends Handler {
    final /* synthetic */ FragmentActivity a;

    m(FragmentActivity fragmentActivity) {
        this.a = fragmentActivity;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.a.e) {
                    this.a.a(false);
                    return;
                }
                return;
            case 2:
                this.a.b.j();
                this.a.b.p();
                return;
            default:
                super.handleMessage(message);
                return;
        }
    }
}
