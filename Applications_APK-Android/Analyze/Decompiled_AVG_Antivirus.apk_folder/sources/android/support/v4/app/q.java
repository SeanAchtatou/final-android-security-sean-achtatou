package android.support.v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public final class q {
    private final r a;

    private q(r rVar) {
        this.a = rVar;
    }

    public static final q a(r rVar) {
        return new q(rVar);
    }

    public final s a() {
        return this.a.d;
    }

    public final View a(View view, String str, Context context, AttributeSet attributeSet) {
        return this.a.d.a(view, str, context, attributeSet);
    }

    public final List a(List list) {
        if (this.a.d.f == null) {
            return null;
        }
        list.addAll(this.a.d.f);
        return list;
    }

    public final void a(Configuration configuration) {
        this.a.d.a(configuration);
    }

    public final void a(Parcelable parcelable, List list) {
        this.a.d.a(parcelable, list);
    }

    public final void a(android.support.v4.e.q qVar) {
        this.a.a(qVar);
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter) {
        this.a.a(str, printWriter);
    }

    public final void a(boolean z) {
        this.a.a(z);
    }

    public final boolean a(Menu menu) {
        return this.a.d.a(menu);
    }

    public final boolean a(Menu menu, MenuInflater menuInflater) {
        return this.a.d.a(menu, menuInflater);
    }

    public final boolean a(MenuItem menuItem) {
        return this.a.d.a(menuItem);
    }

    public final int b() {
        ArrayList arrayList = this.a.d.f;
        if (arrayList == null) {
            return 0;
        }
        return arrayList.size();
    }

    public final void b(Menu menu) {
        this.a.d.b(menu);
    }

    public final boolean b(MenuItem menuItem) {
        return this.a.d.b(menuItem);
    }

    public final void c() {
        this.a.d.a(this.a, this.a, (Fragment) null);
    }

    public final void d() {
        this.a.d.t = false;
    }

    public final Parcelable e() {
        return this.a.d.f();
    }

    public final List f() {
        t tVar = this.a.d;
        ArrayList arrayList = null;
        if (tVar.f != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= tVar.f.size()) {
                    break;
                }
                Fragment fragment = (Fragment) tVar.f.get(i2);
                if (fragment != null && fragment.C) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(fragment);
                    fragment.D = true;
                    fragment.k = fragment.j != null ? fragment.j.g : -1;
                    if (t.a) {
                        Log.v("FragmentManager", "retainNonConfig: keeping retained " + fragment);
                    }
                }
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    public final void g() {
        this.a.d.g();
    }

    public final void h() {
        this.a.d.h();
    }

    public final void i() {
        this.a.d.i();
    }

    public final void j() {
        this.a.d.j();
    }

    public final void k() {
        this.a.d.a(4);
    }

    public final void l() {
        this.a.d.k();
    }

    public final void m() {
        this.a.d.a(2);
    }

    public final void n() {
        this.a.d.l();
    }

    public final void o() {
        this.a.d.m();
    }

    public final boolean p() {
        return this.a.d.d();
    }

    public final void q() {
        this.a.j();
    }

    public final void r() {
        this.a.k();
    }

    public final void s() {
        this.a.l();
    }

    public final android.support.v4.e.q t() {
        return this.a.m();
    }
}
