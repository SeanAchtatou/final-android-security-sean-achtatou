package android.support.v4.app;

import android.app.RemoteInput;

final class cj {
    static RemoteInput[] a(cl[] clVarArr) {
        if (clVarArr == null) {
            return null;
        }
        RemoteInput[] remoteInputArr = new RemoteInput[clVarArr.length];
        for (int i = 0; i < clVarArr.length; i++) {
            cl clVar = clVarArr[i];
            remoteInputArr[i] = new RemoteInput.Builder(clVar.a()).setLabel(clVar.b()).setChoices(clVar.c()).setAllowFreeFormInput(clVar.d()).addExtras(clVar.e()).build();
        }
        return remoteInputArr;
    }
}
