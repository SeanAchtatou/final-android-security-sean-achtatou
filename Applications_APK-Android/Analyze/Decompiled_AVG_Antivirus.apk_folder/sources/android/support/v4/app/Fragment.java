package android.support.v4.app;

import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.e.d;
import android.support.v4.e.q;
import android.support.v4.view.ae;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

public class Fragment implements ComponentCallbacks, View.OnCreateContextMenuListener {
    static final Object a = new Object();
    private static final q aa = new q();
    boolean A;
    boolean B;
    boolean C;
    boolean D;
    boolean E;
    boolean F = true;
    boolean G;
    int H;
    ViewGroup I;
    View J;
    View K;
    boolean L;
    boolean M = true;
    ap N;
    boolean O;
    boolean P;
    Object Q = null;
    Object R = a;
    Object S = null;
    Object T = a;
    Object U = null;
    Object V = a;
    Boolean W;
    Boolean X;
    co Y = null;
    co Z = null;
    int b = 0;
    View c;
    int d;
    Bundle e;
    SparseArray f;
    int g = -1;
    String h;
    Bundle i;
    Fragment j;
    int k = -1;
    int l;
    boolean m;
    boolean n;
    boolean o;
    boolean p;
    boolean q;
    boolean r;
    int s;
    t t;
    r u;
    t v;
    Fragment w;
    int x;
    int y;
    String z;

    public class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new l();
        final Bundle a;

        SavedState(Parcel parcel) {
            this.a = parcel.readBundle();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeBundle(this.a);
        }
    }

    public static Fragment a(Context context, String str) {
        return a(context, str, null);
    }

    public static Fragment a(Context context, String str, Bundle bundle) {
        try {
            Class<?> cls = (Class) aa.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                aa.put(str, cls);
            }
            Fragment fragment = (Fragment) cls.newInstance();
            if (bundle != null) {
                bundle.setClassLoader(fragment.getClass().getClassLoader());
                fragment.i = bundle;
            }
            return fragment;
        } catch (ClassNotFoundException e2) {
            throw new k("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an empty constructor that is public", e2);
        } catch (InstantiationException e3) {
            throw new k("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an empty constructor that is public", e3);
        } catch (IllegalAccessException e4) {
            throw new k("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an empty constructor that is public", e4);
        }
    }

    static boolean b(Context context, String str) {
        try {
            Class<?> cls = (Class) aa.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                aa.put(str, cls);
            }
            return Fragment.class.isAssignableFrom(cls);
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    public static void c() {
    }

    public static void d() {
    }

    public static Animation g() {
        return null;
    }

    public static void h() {
    }

    public static void i() {
    }

    public final FragmentActivity a() {
        if (this.u == null) {
            return null;
        }
        return (FragmentActivity) this.u.g();
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, Fragment fragment) {
        this.g = i2;
        if (fragment != null) {
            this.h = fragment.h + ":" + this.g;
        } else {
            this.h = "android:fragment:" + this.g;
        }
    }

    public void a(Bundle bundle) {
        this.G = true;
    }

    public final void a(boolean z2) {
        if (this.F != z2) {
            this.F = z2;
            if (this.E && b() && !this.A) {
                this.u.d();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final View b(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        if (this.v != null) {
            this.v.t = false;
        }
        return a(layoutInflater, viewGroup);
    }

    public final void b(boolean z2) {
        if (!this.M && z2 && this.b < 4) {
            this.t.a(this);
        }
        this.M = z2;
        this.L = !z2;
    }

    public final boolean b() {
        return this.u != null && this.m;
    }

    public final LayoutInflater e() {
        LayoutInflater c2 = this.u.c();
        if (this.v == null) {
            j();
            if (this.b >= 5) {
                this.v.j();
            } else if (this.b >= 4) {
                this.v.i();
            } else if (this.b >= 2) {
                this.v.h();
            } else if (this.b > 0) {
                this.v.g();
            }
        }
        ae.a(c2, this.v);
        return c2;
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    public final void f() {
        this.G = true;
        if ((this.u == null ? null : this.u.g()) != null) {
            this.G = false;
            this.G = true;
        }
    }

    public final int hashCode() {
        return super.hashCode();
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        this.v = new t();
        this.v.a(this.u, new j(this), this);
    }

    /* access modifiers changed from: package-private */
    public final void k() {
        if (this.v != null) {
            this.v.t = false;
            this.v.d();
        }
        this.G = false;
        this.G = true;
        if (!this.O) {
            this.O = true;
            if (!this.P) {
                this.P = true;
                this.N = this.u.b(this.h);
            }
            if (this.N != null) {
                this.N.b();
            }
        }
        if (!this.G) {
            throw new cp("Fragment " + this + " did not call through to super.onStart()");
        }
        if (this.v != null) {
            this.v.i();
        }
        if (this.N != null) {
            this.N.f();
        }
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        if (this.v != null) {
            this.v.a(2);
        }
        if (this.O) {
            this.O = false;
            if (!this.P) {
                this.P = true;
                this.N = this.u.b(this.h);
            }
            if (this.N == null) {
                return;
            }
            if (this.u.i()) {
                this.N.d();
            } else {
                this.N.c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void m() {
        if (this.v != null) {
            this.v.l();
        }
        this.G = false;
        this.G = true;
        if (!this.P) {
            this.P = true;
            this.N = this.u.b(this.h);
        }
        if (this.N != null) {
            this.N.g();
        }
        if (!this.G) {
            throw new cp("Fragment " + this + " did not call through to super.onDestroy()");
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.G = true;
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        a().onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public void onLowMemory() {
        this.G = true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        d.a(this, sb);
        if (this.g >= 0) {
            sb.append(" #");
            sb.append(this.g);
        }
        if (this.x != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.x));
        }
        if (this.z != null) {
            sb.append(" ");
            sb.append(this.z);
        }
        sb.append('}');
        return sb.toString();
    }
}
