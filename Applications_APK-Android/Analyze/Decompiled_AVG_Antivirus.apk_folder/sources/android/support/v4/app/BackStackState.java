package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

final class BackStackState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new g();
    final int[] a;
    final int b;
    final int c;
    final String d;
    final int e;
    final int f;
    final CharSequence g;
    final int h;
    final CharSequence i;
    final ArrayList j;
    final ArrayList k;

    public BackStackState(Parcel parcel) {
        this.a = parcel.createIntArray();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readString();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.h = parcel.readInt();
        this.i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.j = parcel.createStringArrayList();
        this.k = parcel.createStringArrayList();
    }

    public BackStackState(a aVar) {
        int i2 = 0;
        for (e eVar = aVar.c; eVar != null; eVar = eVar.a) {
            if (eVar.i != null) {
                i2 += eVar.i.size();
            }
        }
        this.a = new int[(i2 + (aVar.e * 7))];
        if (!aVar.l) {
            throw new IllegalStateException("Not on back stack");
        }
        int i3 = 0;
        for (e eVar2 = aVar.c; eVar2 != null; eVar2 = eVar2.a) {
            int i4 = i3 + 1;
            this.a[i3] = eVar2.c;
            int i5 = i4 + 1;
            this.a[i4] = eVar2.d != null ? eVar2.d.g : -1;
            int i6 = i5 + 1;
            this.a[i5] = eVar2.e;
            int i7 = i6 + 1;
            this.a[i6] = eVar2.f;
            int i8 = i7 + 1;
            this.a[i7] = eVar2.g;
            int i9 = i8 + 1;
            this.a[i8] = eVar2.h;
            if (eVar2.i != null) {
                int size = eVar2.i.size();
                int i10 = i9 + 1;
                this.a[i9] = size;
                int i11 = 0;
                while (i11 < size) {
                    this.a[i10] = ((Fragment) eVar2.i.get(i11)).g;
                    i11++;
                    i10++;
                }
                i3 = i10;
            } else {
                i3 = i9 + 1;
                this.a[i9] = 0;
            }
        }
        this.b = aVar.j;
        this.c = aVar.k;
        this.d = aVar.n;
        this.e = aVar.p;
        this.f = aVar.q;
        this.g = aVar.r;
        this.h = aVar.s;
        this.i = aVar.t;
        this.j = aVar.u;
        this.k = aVar.v;
    }

    public final a a(t tVar) {
        a aVar = new a(tVar);
        int i2 = 0;
        int i3 = 0;
        while (i3 < this.a.length) {
            e eVar = new e();
            int i4 = i3 + 1;
            eVar.c = this.a[i3];
            if (t.a) {
                Log.v("FragmentManager", "Instantiate " + aVar + " op #" + i2 + " base fragment #" + this.a[i4]);
            }
            int i5 = i4 + 1;
            int i6 = this.a[i4];
            if (i6 >= 0) {
                eVar.d = (Fragment) tVar.f.get(i6);
            } else {
                eVar.d = null;
            }
            int i7 = i5 + 1;
            eVar.e = this.a[i5];
            int i8 = i7 + 1;
            eVar.f = this.a[i7];
            int i9 = i8 + 1;
            eVar.g = this.a[i8];
            int i10 = i9 + 1;
            eVar.h = this.a[i9];
            int i11 = i10 + 1;
            int i12 = this.a[i10];
            if (i12 > 0) {
                eVar.i = new ArrayList(i12);
                int i13 = 0;
                while (i13 < i12) {
                    if (t.a) {
                        Log.v("FragmentManager", "Instantiate " + aVar + " set remove fragment #" + this.a[i11]);
                    }
                    eVar.i.add((Fragment) tVar.f.get(this.a[i11]));
                    i13++;
                    i11++;
                }
            }
            aVar.a(eVar);
            i2++;
            i3 = i11;
        }
        aVar.j = this.b;
        aVar.k = this.c;
        aVar.n = this.d;
        aVar.p = this.e;
        aVar.l = true;
        aVar.q = this.f;
        aVar.r = this.g;
        aVar.s = this.h;
        aVar.t = this.i;
        aVar.u = this.j;
        aVar.v = this.k;
        aVar.a(1);
        return aVar;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        TextUtils.writeToParcel(this.g, parcel, 0);
        parcel.writeInt(this.h);
        TextUtils.writeToParcel(this.i, parcel, 0);
        parcel.writeStringList(this.j);
        parcel.writeStringList(this.k);
    }
}
