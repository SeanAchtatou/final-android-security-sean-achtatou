package android.support.v4.app;

import android.graphics.Rect;
import android.transition.Transition;

final class ah extends Transition.EpicenterCallback {
    final /* synthetic */ Rect a;

    ah(Rect rect) {
        this.a = rect;
    }

    public final Rect onGetEpicenter(Transition transition) {
        return this.a;
    }
}
