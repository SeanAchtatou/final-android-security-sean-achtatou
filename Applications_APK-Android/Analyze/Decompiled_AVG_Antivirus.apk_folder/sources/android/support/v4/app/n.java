package android.support.v4.app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import java.io.PrintWriter;

final class n extends r {
    final /* synthetic */ FragmentActivity a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(FragmentActivity fragmentActivity) {
        super(fragmentActivity);
        this.a = fragmentActivity;
    }

    public final View a(int i) {
        return this.a.findViewById(i);
    }

    public final void a(String str, PrintWriter printWriter, String[] strArr) {
        this.a.dump(str, null, printWriter, strArr);
    }

    public final boolean a() {
        Window window = this.a.getWindow();
        return (window == null || window.peekDecorView() == null) ? false : true;
    }

    public final boolean b() {
        return !this.a.isFinishing();
    }

    public final LayoutInflater c() {
        return this.a.getLayoutInflater().cloneInContext(this.a);
    }

    public final void d() {
        this.a.a();
    }

    public final boolean e() {
        return this.a.getWindow() != null;
    }

    public final int f() {
        Window window = this.a.getWindow();
        if (window == null) {
            return 0;
        }
        return window.getAttributes().windowAnimations;
    }
}
