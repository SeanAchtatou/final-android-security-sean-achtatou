package android.support.v4.app;

import android.view.View;
import android.view.animation.Animation;

final class v extends w {
    final /* synthetic */ Fragment a;
    final /* synthetic */ t b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    v(t tVar, View view, Animation animation, Fragment fragment) {
        super(view, animation);
        this.b = tVar;
        this.a = fragment;
    }

    public final void onAnimationEnd(Animation animation) {
        super.onAnimationEnd(animation);
        if (this.a.c != null) {
            this.a.c = null;
            this.b.a(this.a, this.a.d, 0, 0, false);
        }
    }
}
