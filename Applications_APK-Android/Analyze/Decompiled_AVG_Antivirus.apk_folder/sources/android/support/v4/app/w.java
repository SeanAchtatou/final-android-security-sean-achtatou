package android.support.v4.app;

import android.view.View;
import android.view.animation.Animation;

class w implements Animation.AnimationListener {
    private Animation.AnimationListener a = null;
    private boolean b = false;
    /* access modifiers changed from: private */
    public View c = null;

    public w(View view, Animation animation) {
        if (view != null && animation != null) {
            this.c = view;
        }
    }

    public w(View view, Animation animation, Animation.AnimationListener animationListener) {
        if (view != null && animation != null) {
            this.a = animationListener;
            this.c = view;
        }
    }

    public void onAnimationEnd(Animation animation) {
        if (this.c != null && this.b) {
            this.c.post(new y(this));
        }
        if (this.a != null) {
            this.a.onAnimationEnd(animation);
        }
    }

    public void onAnimationRepeat(Animation animation) {
        if (this.a != null) {
            this.a.onAnimationRepeat(animation);
        }
    }

    public void onAnimationStart(Animation animation) {
        if (this.c != null) {
            this.b = t.a(this.c, animation);
            if (this.b) {
                this.c.post(new x(this));
            }
        }
        if (this.a != null) {
            this.a.onAnimationStart(animation);
        }
    }
}
