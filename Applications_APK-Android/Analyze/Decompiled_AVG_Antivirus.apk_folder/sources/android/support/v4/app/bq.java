package android.support.v4.app;

import android.app.Notification;
import android.app.RemoteInput;

final class bq {
    public static void a(Notification.Builder builder, bv bvVar) {
        Notification.Action.Builder builder2 = new Notification.Action.Builder(bvVar.a(), bvVar.b(), bvVar.c());
        if (bvVar.e() != null) {
            for (RemoteInput addRemoteInput : cj.a(bvVar.e())) {
                builder2.addRemoteInput(addRemoteInput);
            }
        }
        if (bvVar.d() != null) {
            builder2.addExtras(bvVar.d());
        }
        builder.addAction(builder2.build());
    }
}
