package android.support.v4.app;

import android.view.View;
import android.view.ViewTreeObserver;

final class d implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ View a;
    final /* synthetic */ f b;
    final /* synthetic */ int c;
    final /* synthetic */ Object d;
    final /* synthetic */ a e;

    d(a aVar, View view, f fVar, int i, Object obj) {
        this.e = aVar;
        this.a = view;
        this.b = fVar;
        this.c = i;
        this.d = obj;
    }

    public final boolean onPreDraw() {
        this.a.getViewTreeObserver().removeOnPreDrawListener(this);
        this.e.a(this.b, this.c, this.d);
        return true;
    }
}
