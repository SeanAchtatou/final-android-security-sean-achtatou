package android.support.v4.app;

import android.os.Bundle;
import android.support.v4.content.i;
import android.support.v4.content.j;
import android.support.v4.content.k;
import android.support.v4.e.d;
import android.util.Log;
import java.io.PrintWriter;

final class aq implements j, k {
    final int a;
    final Bundle b;
    ao c;
    i d;
    boolean e;
    boolean f;
    Object g;
    boolean h;
    boolean i;
    boolean j;
    boolean k;
    boolean l;
    boolean m;
    aq n;
    final /* synthetic */ ap o;

    /* access modifiers changed from: package-private */
    public final void a() {
        if (ap.a) {
            Log.v("LoaderManager", "  Stopping: " + this);
        }
        this.h = false;
        if (!this.i && this.d != null && this.m) {
            this.m = false;
            this.d.a((k) this);
            this.d.b(this);
            this.d.b();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(i iVar, Object obj) {
        String str;
        if (this.c != null) {
            if (this.o.g != null) {
                String str2 = this.o.g.d.v;
                this.o.g.d.v = "onLoadFinished";
                str = str2;
            } else {
                str = null;
            }
            try {
                if (ap.a) {
                    StringBuilder append = new StringBuilder("  onLoadFinished in ").append(iVar).append(": ");
                    StringBuilder sb = new StringBuilder(64);
                    d.a(obj, sb);
                    sb.append("}");
                    Log.v("LoaderManager", append.append(sb.toString()).toString());
                }
                this.f = true;
            } finally {
                if (this.o.g != null) {
                    this.o.g.d.v = str;
                }
            }
        }
    }

    public final void a(String str, PrintWriter printWriter) {
        while (true) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.a);
            printWriter.print(" mArgs=");
            printWriter.println(this.b);
            printWriter.print(str);
            printWriter.print("mCallbacks=");
            printWriter.println(this.c);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.d);
            if (this.d != null) {
                this.d.a(str + "  ", printWriter);
            }
            if (this.e || this.f) {
                printWriter.print(str);
                printWriter.print("mHaveData=");
                printWriter.print(this.e);
                printWriter.print("  mDeliveredData=");
                printWriter.println(this.f);
                printWriter.print(str);
                printWriter.print("mData=");
                printWriter.println(this.g);
            }
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.h);
            printWriter.print(" mReportNextStart=");
            printWriter.print(this.k);
            printWriter.print(" mDestroyed=");
            printWriter.println(this.l);
            printWriter.print(str);
            printWriter.print("mRetaining=");
            printWriter.print(this.i);
            printWriter.print(" mRetainingStarted=");
            printWriter.print(this.j);
            printWriter.print(" mListenerRegistered=");
            printWriter.println(this.m);
            if (this.n != null) {
                printWriter.print(str);
                printWriter.println("Pending Loader ");
                printWriter.print(this.n);
                printWriter.println(":");
                this = this.n;
                str = str + "  ";
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        String str;
        while (true) {
            if (ap.a) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.l = true;
            boolean z = this.f;
            this.f = false;
            if (this.c != null && this.d != null && this.e && z) {
                if (ap.a) {
                    Log.v("LoaderManager", "  Reseting: " + this);
                }
                if (this.o.g != null) {
                    str = this.o.g.d.v;
                    this.o.g.d.v = "onLoaderReset";
                } else {
                    str = null;
                }
                if (this.o.g != null) {
                    this.o.g.d.v = str;
                }
            }
            this.c = null;
            this.g = null;
            this.e = false;
            if (this.d != null) {
                if (this.m) {
                    this.m = false;
                    this.d.a((k) this);
                    this.d.b(this);
                }
                this.d.c();
            }
            if (this.n != null) {
                this = this.n;
            } else {
                return;
            }
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(64);
        sb.append("LoaderInfo{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" #");
        sb.append(this.a);
        sb.append(" : ");
        d.a(this.d, sb);
        sb.append("}}");
        return sb.toString();
    }
}
