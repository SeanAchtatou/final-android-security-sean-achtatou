package android.support.v4.app;

import android.os.Build;
import android.os.Bundle;

public final class cd extends cl {
    public static final cm a = new ce();
    private static final cf g;
    private final String b;
    private final CharSequence c;
    private final CharSequence[] d;
    private final boolean e;
    private final Bundle f;

    static {
        if (Build.VERSION.SDK_INT >= 20) {
            g = new cg();
        } else if (Build.VERSION.SDK_INT >= 16) {
            g = new ci();
        } else {
            g = new ch();
        }
    }

    public final String a() {
        return this.b;
    }

    public final CharSequence b() {
        return this.c;
    }

    public final CharSequence[] c() {
        return this.d;
    }

    public final boolean d() {
        return this.e;
    }

    public final Bundle e() {
        return this.f;
    }
}
