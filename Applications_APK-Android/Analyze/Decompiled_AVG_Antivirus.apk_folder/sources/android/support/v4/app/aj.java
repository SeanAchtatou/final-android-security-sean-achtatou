package android.support.v4.app;

import android.graphics.Rect;
import android.transition.Transition;

final class aj extends Transition.EpicenterCallback {
    final /* synthetic */ al a;
    private Rect b;

    aj(al alVar) {
        this.a = alVar;
    }

    public final Rect onGetEpicenter(Transition transition) {
        if (this.b == null && this.a.a != null) {
            this.b = ag.b(this.a.a);
        }
        return this.b;
    }
}
