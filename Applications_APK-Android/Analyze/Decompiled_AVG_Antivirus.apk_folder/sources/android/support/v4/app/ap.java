package android.support.v4.app;

import android.support.v4.content.j;
import android.support.v4.e.d;
import android.support.v4.e.r;
import android.util.Log;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

final class ap extends an {
    static boolean a = false;
    final r b;
    final r c;
    final String d;
    boolean e;
    boolean f;
    /* access modifiers changed from: private */
    public r g;

    /* access modifiers changed from: package-private */
    public final void a(r rVar) {
        this.g = rVar;
    }

    public final void a(String str, PrintWriter printWriter) {
        if (this.b.a() > 0) {
            printWriter.print(str);
            printWriter.println("Active Loaders:");
            String str2 = str + "    ";
            for (int i = 0; i < this.b.a(); i++) {
                aq aqVar = (aq) this.b.b(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.b.a(i));
                printWriter.print(": ");
                printWriter.println(aqVar.toString());
                aqVar.a(str2, printWriter);
            }
        }
        if (this.c.a() > 0) {
            printWriter.print(str);
            printWriter.println("Inactive Loaders:");
            String str3 = str + "    ";
            for (int i2 = 0; i2 < this.c.a(); i2++) {
                aq aqVar2 = (aq) this.c.b(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.c.a(i2));
                printWriter.print(": ");
                printWriter.println(aqVar2.toString());
                aqVar2.a(str3, printWriter);
            }
        }
    }

    public final boolean a() {
        int a2 = this.b.a();
        boolean z = false;
        for (int i = 0; i < a2; i++) {
            aq aqVar = (aq) this.b.b(i);
            z |= aqVar.h && !aqVar.f;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (a) {
            Log.v("LoaderManager", "Starting in " + this);
        }
        if (this.e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.e = true;
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            aq aqVar = (aq) this.b.b(a2);
            if (aqVar.i && aqVar.j) {
                aqVar.h = true;
            } else if (!aqVar.h) {
                aqVar.h = true;
                if (a) {
                    Log.v("LoaderManager", "  Starting: " + aqVar);
                }
                if (aqVar.d == null && aqVar.c != null) {
                    aqVar.d = aqVar.c.a();
                }
                if (aqVar.d == null) {
                    continue;
                } else if (!aqVar.d.getClass().isMemberClass() || Modifier.isStatic(aqVar.d.getClass().getModifiers())) {
                    if (!aqVar.m) {
                        aqVar.d.a(aqVar.a, aqVar);
                        aqVar.d.a((j) aqVar);
                        aqVar.m = true;
                    }
                    aqVar.d.a();
                } else {
                    throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + aqVar.d);
                }
            } else {
                continue;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (a) {
            Log.v("LoaderManager", "Stopping in " + this);
        }
        if (!this.e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            ((aq) this.b.b(a2)).a();
        }
        this.e = false;
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (a) {
            Log.v("LoaderManager", "Retaining in " + this);
        }
        if (!this.e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.f = true;
        this.e = false;
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            aq aqVar = (aq) this.b.b(a2);
            if (a) {
                Log.v("LoaderManager", "  Retaining: " + aqVar);
            }
            aqVar.i = true;
            aqVar.j = aqVar.h;
            aqVar.h = false;
            aqVar.c = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            ((aq) this.b.b(a2)).k = true;
        }
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            aq aqVar = (aq) this.b.b(a2);
            if (aqVar.h && aqVar.k) {
                aqVar.k = false;
                if (aqVar.e) {
                    aqVar.a(aqVar.d, aqVar.g);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        if (!this.f) {
            if (a) {
                Log.v("LoaderManager", "Destroying Active in " + this);
            }
            for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
                ((aq) this.b.b(a2)).b();
            }
            this.b.b();
        }
        if (a) {
            Log.v("LoaderManager", "Destroying Inactive in " + this);
        }
        for (int a3 = this.c.a() - 1; a3 >= 0; a3--) {
            ((aq) this.c.b(a3)).b();
        }
        this.c.b();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        d.a(this.g, sb);
        sb.append("}}");
        return sb.toString();
    }
}
