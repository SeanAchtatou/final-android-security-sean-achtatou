package android.support.v4.app;

import android.os.Bundle;

final class cn {
    static Bundle[] a(cl[] clVarArr) {
        if (clVarArr == null) {
            return null;
        }
        Bundle[] bundleArr = new Bundle[clVarArr.length];
        for (int i = 0; i < clVarArr.length; i++) {
            cl clVar = clVarArr[i];
            Bundle bundle = new Bundle();
            bundle.putString("resultKey", clVar.a());
            bundle.putCharSequence("label", clVar.b());
            bundle.putCharSequenceArray("choices", clVar.c());
            bundle.putBoolean("allowFreeFormInput", clVar.d());
            bundle.putBundle("extras", clVar.e());
            bundleArr[i] = bundle;
        }
        return bundleArr;
    }
}
