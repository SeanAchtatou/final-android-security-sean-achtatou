package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v4.e.q;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import java.io.PrintWriter;

public abstract class r extends p {
    private final Activity a;
    final Context b;
    final int c;
    final t d;
    private final Handler e;
    private q f;
    private boolean g;
    private ap h;
    private boolean i;
    private boolean j;

    private r(Activity activity, Context context, Handler handler) {
        this.d = new t();
        this.a = activity;
        this.b = context;
        this.e = handler;
        this.c = 0;
    }

    r(FragmentActivity fragmentActivity) {
        this(fragmentActivity, fragmentActivity, fragmentActivity.a);
    }

    public View a(int i2) {
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a(q qVar) {
        this.f = qVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        ap apVar;
        if (this.f != null && (apVar = (ap) this.f.get(str)) != null && !apVar.f) {
            apVar.g();
            this.f.remove(str);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, PrintWriter printWriter) {
        printWriter.print(str);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.j);
        if (this.h != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.h)));
            printWriter.println(":");
            this.h.a(str + "  ", printWriter);
        }
    }

    public void a(String str, PrintWriter printWriter, String[] strArr) {
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.g = z;
        if (this.h != null && this.j) {
            this.j = false;
            if (z) {
                this.h.d();
            } else {
                this.h.c();
            }
        }
    }

    public boolean a() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public final ap b(String str) {
        if (this.f == null) {
            this.f = new q();
        }
        ap apVar = (ap) this.f.get(str);
        if (apVar != null) {
            apVar.a(this);
        }
        return apVar;
    }

    public boolean b() {
        return true;
    }

    public LayoutInflater c() {
        return (LayoutInflater) this.b.getSystemService("layout_inflater");
    }

    public void d() {
    }

    public boolean e() {
        return true;
    }

    public int f() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final Activity g() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final Handler h() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final boolean i() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        if (!this.j) {
            this.j = true;
            if (this.h != null) {
                this.h.b();
            } else if (!this.i) {
                this.h = b("(root)");
                if (this.h != null && !this.h.e) {
                    this.h.b();
                }
            }
            this.i = true;
        }
    }

    /* access modifiers changed from: package-private */
    public final void k() {
        if (this.h != null) {
            this.h.g();
        }
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        if (this.f != null) {
            int size = this.f.size();
            ap[] apVarArr = new ap[size];
            for (int i2 = size - 1; i2 >= 0; i2--) {
                apVarArr[i2] = (ap) this.f.c(i2);
            }
            for (int i3 = 0; i3 < size; i3++) {
                ap apVar = apVarArr[i3];
                if (apVar.f) {
                    if (ap.a) {
                        Log.v("LoaderManager", "Finished Retaining in " + apVar);
                    }
                    apVar.f = false;
                    for (int a2 = apVar.b.a() - 1; a2 >= 0; a2--) {
                        aq aqVar = (aq) apVar.b.b(a2);
                        if (aqVar.i) {
                            if (ap.a) {
                                Log.v("LoaderManager", "  Finished Retaining: " + aqVar);
                            }
                            aqVar.i = false;
                            if (aqVar.h != aqVar.j && !aqVar.h) {
                                aqVar.a();
                            }
                        }
                        if (aqVar.h && aqVar.e && !aqVar.k) {
                            aqVar.a(aqVar.d, aqVar.g);
                        }
                    }
                }
                apVar.f();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final q m() {
        boolean z;
        if (this.f != null) {
            int size = this.f.size();
            ap[] apVarArr = new ap[size];
            for (int i2 = size - 1; i2 >= 0; i2--) {
                apVarArr[i2] = (ap) this.f.c(i2);
            }
            z = false;
            for (int i3 = 0; i3 < size; i3++) {
                ap apVar = apVarArr[i3];
                if (apVar.f) {
                    z = true;
                } else {
                    apVar.g();
                    this.f.remove(apVar.d);
                }
            }
        } else {
            z = false;
        }
        if (z) {
            return this.f;
        }
        return null;
    }
}
