package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.widget.RemoteViews;

final class bl extends bj {
    bl() {
    }

    public final Notification a(bd bdVar) {
        Context context = bdVar.a;
        Notification notification = bdVar.B;
        CharSequence charSequence = bdVar.b;
        CharSequence charSequence2 = bdVar.c;
        CharSequence charSequence3 = bdVar.h;
        RemoteViews remoteViews = bdVar.f;
        int i = bdVar.i;
        PendingIntent pendingIntent = bdVar.d;
        return new Notification.Builder(context).setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, remoteViews).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(charSequence).setContentText(charSequence2).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(bdVar.e, (notification.flags & 128) != 0).setLargeIcon(bdVar.g).setNumber(i).getNotification();
    }
}
