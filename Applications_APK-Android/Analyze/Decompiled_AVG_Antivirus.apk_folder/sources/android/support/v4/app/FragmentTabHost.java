package android.support.v4.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TabHost;
import java.util.ArrayList;

public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {
    private final ArrayList a = new ArrayList();
    private Context b;
    private s c;
    private int d;
    private TabHost.OnTabChangeListener e;
    private ae f;
    private boolean g;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new ad();
        String a;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readString();
        }

        /* synthetic */ SavedState(Parcel parcel, byte b) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.a);
        }
    }

    public FragmentTabHost(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842995}, 0, 0);
        this.d = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        super.setOnTabChangedListener(this);
    }

    private af a(String str, af afVar) {
        ae aeVar = null;
        int i = 0;
        while (i < this.a.size()) {
            ae aeVar2 = (ae) this.a.get(i);
            if (!aeVar2.a.equals(str)) {
                aeVar2 = aeVar;
            }
            i++;
            aeVar = aeVar2;
        }
        if (aeVar == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.f != aeVar) {
            if (afVar == null) {
                afVar = this.c.a();
            }
            if (!(this.f == null || this.f.d == null)) {
                afVar.a(this.f.d);
            }
            if (aeVar != null) {
                if (aeVar.d == null) {
                    Fragment unused = aeVar.d = Fragment.a(this.b, aeVar.b.getName(), aeVar.c);
                    afVar.a(this.d, aeVar.d, aeVar.a);
                } else {
                    afVar.b(aeVar.d);
                }
            }
            this.f = aeVar;
        }
        return afVar;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        af afVar = null;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.a.size()) {
                break;
            }
            ae aeVar = (ae) this.a.get(i2);
            Fragment unused = aeVar.d = this.c.a(aeVar.a);
            if (aeVar.d != null && !aeVar.d.B) {
                if (aeVar.a.equals(currentTabTag)) {
                    this.f = aeVar;
                } else {
                    if (afVar == null) {
                        afVar = this.c.a();
                    }
                    afVar.a(aeVar.d);
                }
            }
            i = i2 + 1;
        }
        this.g = true;
        af a2 = a(currentTabTag, afVar);
        if (a2 != null) {
            a2.a();
            this.c.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.g = false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.a);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = getCurrentTabTag();
        return savedState;
    }

    public void onTabChanged(String str) {
        af a2;
        if (this.g && (a2 = a(str, null)) != null) {
            a2.a();
        }
        if (this.e != null) {
            this.e.onTabChanged(str);
        }
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.e = onTabChangeListener;
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }
}
