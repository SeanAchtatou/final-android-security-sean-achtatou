package android.support.v4.app;

import android.os.Build;
import android.support.v4.e.e;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

final class a extends af implements Runnable {
    static final boolean a = (Build.VERSION.SDK_INT >= 21);
    final t b;
    e c;
    e d;
    int e;
    int f;
    int g;
    int h;
    int i;
    int j;
    int k;
    boolean l;
    boolean m = true;
    String n;
    boolean o;
    int p = -1;
    int q;
    CharSequence r;
    int s;
    CharSequence t;
    ArrayList u;
    ArrayList v;

    public a(t tVar) {
        this.b = tVar;
    }

    private int a(boolean z) {
        if (this.o) {
            throw new IllegalStateException("commit already called");
        }
        if (t.a) {
            Log.v("FragmentManager", "Commit: " + this);
            a("  ", new PrintWriter(new e("FragmentManager")));
        }
        this.o = true;
        if (this.l) {
            this.p = this.b.a(this);
        } else {
            this.p = -1;
        }
        this.b.a(this, z);
        return this.p;
    }

    private f a(SparseArray sparseArray, SparseArray sparseArray2, boolean z) {
        f fVar = new f(this);
        fVar.d = new View(this.b.o.b);
        int i2 = 0;
        boolean z2 = false;
        while (i2 < sparseArray.size()) {
            boolean z3 = a(sparseArray.keyAt(i2), fVar, z, sparseArray, sparseArray2) ? true : z2;
            i2++;
            z2 = z3;
        }
        for (int i3 = 0; i3 < sparseArray2.size(); i3++) {
            int keyAt = sparseArray2.keyAt(i3);
            if (sparseArray.get(keyAt) == null && a(keyAt, fVar, z, sparseArray, sparseArray2)) {
                z2 = true;
            }
        }
        if (!z2) {
            return null;
        }
        return fVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.ag.a(java.util.Map, android.view.View):void
     arg types: [android.support.v4.e.a, android.view.View]
     candidates:
      android.support.v4.app.ag.a(java.lang.Object, android.view.View):void
      android.support.v4.app.ag.a(java.lang.Object, java.util.ArrayList):void
      android.support.v4.app.ag.a(java.util.ArrayList, android.view.View):void
      android.support.v4.app.ag.a(java.util.Map, android.view.View):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.e.a, boolean):void
     arg types: [android.support.v4.app.f, android.support.v4.e.a, int]
     candidates:
      android.support.v4.app.a.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.f
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.app.Fragment, boolean):android.support.v4.e.a
      android.support.v4.app.a.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.e.a):android.support.v4.e.a
      android.support.v4.app.a.a(android.support.v4.app.a, android.support.v4.e.a, android.support.v4.app.f):void
      android.support.v4.app.a.a(android.support.v4.app.f, int, java.lang.Object):void
      android.support.v4.app.a.a(android.support.v4.e.a, java.lang.String, java.lang.String):void
      android.support.v4.app.a.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(android.support.v4.app.f, android.util.SparseArray, android.util.SparseArray):android.support.v4.app.f
      android.support.v4.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.af.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.e.a, boolean):void */
    static /* synthetic */ android.support.v4.e.a a(a aVar, f fVar, boolean z, Fragment fragment) {
        android.support.v4.e.a aVar2 = new android.support.v4.e.a();
        View view = fragment.J;
        if (!(view == null || aVar.u == null)) {
            ag.a((Map) aVar2, view);
            if (z) {
                aVar2 = a(aVar.u, aVar.v, aVar2);
            } else {
                aVar2.a((Collection) aVar.v);
            }
        }
        if (z) {
            if (fragment.Z != null) {
                co coVar = fragment.Z;
            }
            aVar.a(fVar, aVar2, true);
        } else {
            if (fragment.Y != null) {
                co coVar2 = fragment.Y;
            }
            b(fVar, aVar2, true);
        }
        return aVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.ag.a(java.util.Map, android.view.View):void
     arg types: [android.support.v4.e.a, android.view.View]
     candidates:
      android.support.v4.app.ag.a(java.lang.Object, android.view.View):void
      android.support.v4.app.ag.a(java.lang.Object, java.util.ArrayList):void
      android.support.v4.app.ag.a(java.util.ArrayList, android.view.View):void
      android.support.v4.app.ag.a(java.util.Map, android.view.View):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.e.a, boolean):void
     arg types: [android.support.v4.app.f, android.support.v4.e.a, int]
     candidates:
      android.support.v4.app.a.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.f
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.app.Fragment, boolean):android.support.v4.e.a
      android.support.v4.app.a.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.e.a):android.support.v4.e.a
      android.support.v4.app.a.a(android.support.v4.app.a, android.support.v4.e.a, android.support.v4.app.f):void
      android.support.v4.app.a.a(android.support.v4.app.f, int, java.lang.Object):void
      android.support.v4.app.a.a(android.support.v4.e.a, java.lang.String, java.lang.String):void
      android.support.v4.app.a.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(android.support.v4.app.f, android.util.SparseArray, android.util.SparseArray):android.support.v4.app.f
      android.support.v4.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.af.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.e.a, boolean):void */
    private android.support.v4.e.a a(f fVar, Fragment fragment, boolean z) {
        android.support.v4.e.a aVar = new android.support.v4.e.a();
        if (this.u != null) {
            ag.a((Map) aVar, fragment.J);
            if (z) {
                aVar.a((Collection) this.v);
            } else {
                aVar = a(this.u, this.v, aVar);
            }
        }
        if (z) {
            if (fragment.Y != null) {
                co coVar = fragment.Y;
            }
            a(fVar, aVar, false);
        } else {
            if (fragment.Z != null) {
                co coVar2 = fragment.Z;
            }
            b(fVar, aVar, false);
        }
        return aVar;
    }

    private static android.support.v4.e.a a(ArrayList arrayList, ArrayList arrayList2, android.support.v4.e.a aVar) {
        if (aVar.isEmpty()) {
            return aVar;
        }
        android.support.v4.e.a aVar2 = new android.support.v4.e.a();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = (View) aVar.get(arrayList.get(i2));
            if (view != null) {
                aVar2.put(arrayList2.get(i2), view);
            }
        }
        return aVar2;
    }

    static /* synthetic */ void a(Fragment fragment, Fragment fragment2, boolean z, android.support.v4.e.a aVar) {
        if ((z ? fragment2.Y : fragment.Y) != null) {
            new ArrayList(aVar.keySet());
            new ArrayList(aVar.values());
        }
    }

    static /* synthetic */ void a(a aVar, android.support.v4.e.a aVar2, f fVar) {
        View view;
        if (aVar.v != null && !aVar2.isEmpty() && (view = (View) aVar2.get(aVar.v.get(0))) != null) {
            fVar.c.a = view;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.ag.a(java.lang.Object, android.view.View, boolean):void
     arg types: [java.lang.Object, android.view.View, int]
     candidates:
      android.support.v4.app.ag.a(java.util.List, android.view.View, int):boolean
      android.support.v4.app.ag.a(java.lang.Object, android.view.View, boolean):void */
    /* access modifiers changed from: private */
    public void a(f fVar, int i2, Object obj) {
        if (this.b.g != null) {
            for (int i3 = 0; i3 < this.b.g.size(); i3++) {
                Fragment fragment = (Fragment) this.b.g.get(i3);
                if (!(fragment.J == null || fragment.I == null || fragment.y != i2)) {
                    if (!fragment.A) {
                        ag.a(obj, fragment.J, false);
                        fVar.b.remove(fragment.J);
                    } else if (!fVar.b.contains(fragment.J)) {
                        ag.a(obj, fragment.J, true);
                        fVar.b.add(fragment.J);
                    }
                }
            }
        }
    }

    private void a(f fVar, android.support.v4.e.a aVar, boolean z) {
        int size = this.v == null ? 0 : this.v.size();
        for (int i2 = 0; i2 < size; i2++) {
            String str = (String) this.u.get(i2);
            View view = (View) aVar.get((String) this.v.get(i2));
            if (view != null) {
                String transitionName = view.getTransitionName();
                if (z) {
                    a(fVar.a, str, transitionName);
                } else {
                    a(fVar.a, transitionName, str);
                }
            }
        }
    }

    private static void a(android.support.v4.e.a aVar, String str, String str2) {
        if (str != null && str2 != null) {
            for (int i2 = 0; i2 < aVar.size(); i2++) {
                if (str.equals(aVar.c(i2))) {
                    aVar.a(i2, str2);
                    return;
                }
            }
            aVar.put(str, str2);
        }
    }

    private static void a(SparseArray sparseArray, Fragment fragment) {
        int i2;
        if (fragment != null && (i2 = fragment.y) != 0 && !fragment.A && fragment.b() && fragment.J != null && sparseArray.get(i2) == null) {
            sparseArray.put(i2, fragment);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.ag.a(java.lang.Object, android.view.View, boolean):void
     arg types: [android.transition.Transition, android.view.View, int]
     candidates:
      android.support.v4.app.ag.a(java.util.List, android.view.View, int):boolean
      android.support.v4.app.ag.a(java.lang.Object, android.view.View, boolean):void */
    private boolean a(int i2, f fVar, boolean z, SparseArray sparseArray, SparseArray sparseArray2) {
        Object a2;
        TransitionSet transitionSet;
        Object a3;
        android.support.v4.e.a aVar;
        TransitionSet transitionSet2;
        boolean z2;
        TransitionSet transitionSet3;
        ViewGroup viewGroup = (ViewGroup) this.b.p.a(i2);
        if (viewGroup == null) {
            return false;
        }
        Fragment fragment = (Fragment) sparseArray2.get(i2);
        Fragment fragment2 = (Fragment) sparseArray.get(i2);
        if (fragment == null) {
            a2 = null;
        } else {
            a2 = ag.a(z ? fragment.T == Fragment.a ? fragment.S : fragment.T : fragment.Q);
        }
        if (fragment == null || fragment2 == null) {
            transitionSet = null;
        } else {
            Object obj = z ? fragment2.V == Fragment.a ? fragment2.U : fragment2.V : fragment.U;
            if (obj == null) {
                transitionSet = null;
            } else {
                Transition transition = (Transition) obj;
                if (transition == null) {
                    transitionSet = null;
                } else {
                    transitionSet = new TransitionSet();
                    transitionSet.addTransition(transition);
                }
            }
        }
        if (fragment2 == null) {
            a3 = null;
        } else {
            a3 = ag.a(z ? fragment2.R == Fragment.a ? fragment2.Q : fragment2.R : fragment2.S);
        }
        ArrayList arrayList = new ArrayList();
        if (transitionSet != null) {
            android.support.v4.e.a a4 = a(fVar, fragment2, z);
            if (a4.isEmpty()) {
                aVar = null;
                transitionSet2 = null;
            } else {
                if ((z ? fragment2.Y : fragment.Y) != null) {
                    new ArrayList(a4.keySet());
                    new ArrayList(a4.values());
                }
                viewGroup.getViewTreeObserver().addOnPreDrawListener(new c(this, viewGroup, transitionSet, arrayList, fVar, z, fragment, fragment2));
                aVar = a4;
                transitionSet2 = transitionSet;
            }
        } else {
            aVar = null;
            transitionSet2 = transitionSet;
        }
        if (a2 == null && transitionSet2 == null && a3 == null) {
            return false;
        }
        ArrayList arrayList2 = new ArrayList();
        Object a5 = a3 != null ? ag.a(a3, fragment2.J, arrayList2, aVar, fVar.d) : a3;
        if (!(this.v == null || aVar == null)) {
            View view = (View) aVar.get(this.v.get(0));
            if (view != null) {
                if (a5 != null) {
                    ag.a(a5, view);
                }
                if (transitionSet2 != null) {
                    ag.a(transitionSet2, view);
                }
            }
        }
        b bVar = new b(this, fragment);
        ArrayList arrayList3 = new ArrayList();
        android.support.v4.e.a aVar2 = new android.support.v4.e.a();
        if (fragment != null) {
            z2 = z ? fragment.W == null ? true : fragment.W.booleanValue() : fragment.X == null ? true : fragment.X.booleanValue();
        } else {
            z2 = true;
        }
        Transition transition2 = (Transition) a2;
        Transition transition3 = (Transition) a5;
        Transition transition4 = transitionSet2;
        if (transition2 == null || transition3 == null) {
            z2 = true;
        }
        if (z2) {
            TransitionSet transitionSet4 = new TransitionSet();
            if (transition2 != null) {
                transitionSet4.addTransition(transition2);
            }
            if (transition3 != null) {
                transitionSet4.addTransition(transition3);
            }
            if (transition4 != null) {
                transitionSet4.addTransition(transition4);
            }
            transitionSet3 = transitionSet4;
        } else {
            if (transition3 != null && transition2 != null) {
                transition2 = new TransitionSet().addTransition(transition3).addTransition(transition2).setOrdering(1);
            } else if (transition3 != null) {
                transition2 = transition3;
            } else if (transition2 == null) {
                transition2 = null;
            }
            if (transition4 != null) {
                TransitionSet transitionSet5 = new TransitionSet();
                if (transition2 != null) {
                    transitionSet5.addTransition(transition2);
                }
                transitionSet5.addTransition(transition4);
                transitionSet3 = transitionSet5;
            } else {
                transitionSet3 = transition2;
            }
        }
        if (transitionSet3 != null) {
            View view2 = fVar.d;
            al alVar = fVar.c;
            android.support.v4.e.a aVar3 = fVar.a;
            if (!(a2 == null && transitionSet2 == null)) {
                Transition transition5 = (Transition) a2;
                if (transition5 != null) {
                    transition5.addTarget(view2);
                }
                if (transitionSet2 != null) {
                    ag.a(transitionSet2, view2, aVar, arrayList);
                }
                viewGroup.getViewTreeObserver().addOnPreDrawListener(new ai(viewGroup, transition5, view2, bVar, aVar3, aVar2, arrayList3));
                if (transition5 != null) {
                    transition5.setEpicenterCallback(new aj(alVar));
                }
            }
            viewGroup.getViewTreeObserver().addOnPreDrawListener(new d(this, viewGroup, fVar, i2, transitionSet3));
            ag.a((Object) transitionSet3, fVar.d, true);
            a(fVar, i2, transitionSet3);
            TransitionManager.beginDelayedTransition(viewGroup, transitionSet3);
            View view3 = fVar.d;
            ArrayList arrayList4 = fVar.b;
            Transition transition6 = (Transition) a2;
            Transition transition7 = (Transition) a5;
            Transition transition8 = transitionSet2;
            Transition transition9 = transitionSet3;
            if (transition9 != null) {
                viewGroup.getViewTreeObserver().addOnPreDrawListener(new ak(viewGroup, transition6, arrayList3, transition7, arrayList2, transition8, arrayList, aVar2, arrayList4, transition9, view3));
            }
        }
        return transitionSet3 != null;
    }

    private static void b(f fVar, android.support.v4.e.a aVar, boolean z) {
        int size = aVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            String str = (String) aVar.b(i2);
            String transitionName = ((View) aVar.c(i2)).getTransitionName();
            if (z) {
                a(fVar.a, str, transitionName);
            } else {
                a(fVar.a, transitionName, str);
            }
        }
    }

    private static void b(SparseArray sparseArray, Fragment fragment) {
        int i2;
        if (fragment != null && (i2 = fragment.y) != 0) {
            sparseArray.put(i2, fragment);
        }
    }

    private void b(SparseArray sparseArray, SparseArray sparseArray2) {
        Fragment fragment;
        if (this.b.p.a()) {
            for (e eVar = this.c; eVar != null; eVar = eVar.a) {
                switch (eVar.c) {
                    case 1:
                        b(sparseArray2, eVar.d);
                        break;
                    case 2:
                        Fragment fragment2 = eVar.d;
                        if (this.b.g != null) {
                            int i2 = 0;
                            fragment = fragment2;
                            while (true) {
                                int i3 = i2;
                                if (i3 < this.b.g.size()) {
                                    Fragment fragment3 = (Fragment) this.b.g.get(i3);
                                    if (fragment == null || fragment3.y == fragment.y) {
                                        if (fragment3 == fragment) {
                                            fragment = null;
                                        } else {
                                            a(sparseArray, fragment3);
                                        }
                                    }
                                    i2 = i3 + 1;
                                }
                            }
                        } else {
                            fragment = fragment2;
                        }
                        b(sparseArray2, fragment);
                        break;
                    case 3:
                        a(sparseArray, eVar.d);
                        break;
                    case 4:
                        a(sparseArray, eVar.d);
                        break;
                    case 5:
                        b(sparseArray2, eVar.d);
                        break;
                    case 6:
                        a(sparseArray, eVar.d);
                        break;
                    case 7:
                        b(sparseArray2, eVar.d);
                        break;
                }
            }
        }
    }

    public final int a() {
        return a(false);
    }

    public final af a(int i2, Fragment fragment, String str) {
        fragment.t = this.b;
        if (str != null) {
            if (fragment.z == null || str.equals(fragment.z)) {
                fragment.z = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.z + " now " + str);
            }
        }
        if (i2 != 0) {
            if (fragment.x == 0 || fragment.x == i2) {
                fragment.x = i2;
                fragment.y = i2;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.x + " now " + i2);
            }
        }
        e eVar = new e();
        eVar.c = 1;
        eVar.d = fragment;
        a(eVar);
        return this;
    }

    public final af a(Fragment fragment) {
        e eVar = new e();
        eVar.c = 6;
        eVar.d = fragment;
        a(eVar);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.a.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.f
     arg types: [android.util.SparseArray, android.util.SparseArray, int]
     candidates:
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.app.Fragment, boolean):android.support.v4.e.a
      android.support.v4.app.a.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.e.a):android.support.v4.e.a
      android.support.v4.app.a.a(android.support.v4.app.a, android.support.v4.e.a, android.support.v4.app.f):void
      android.support.v4.app.a.a(android.support.v4.app.f, int, java.lang.Object):void
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.e.a, boolean):void
      android.support.v4.app.a.a(android.support.v4.e.a, java.lang.String, java.lang.String):void
      android.support.v4.app.a.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(android.support.v4.app.f, android.util.SparseArray, android.util.SparseArray):android.support.v4.app.f
      android.support.v4.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.af.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.f */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(float, float):android.view.animation.Animation
      android.support.v4.app.t.a(int, android.support.v4.app.a):void
      android.support.v4.app.t.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.t.a(android.os.Parcelable, java.util.List):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.t.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.t.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.s.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.view.an.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(int, int, int, boolean):void */
    public final f a(f fVar, SparseArray sparseArray, SparseArray sparseArray2) {
        if (t.a) {
            Log.v("FragmentManager", "popFromBackStack: " + this);
            a("  ", new PrintWriter(new e("FragmentManager")));
        }
        if (a && !(sparseArray.size() == 0 && sparseArray2.size() == 0)) {
            fVar = a(sparseArray, sparseArray2, true);
        }
        a(-1);
        int i2 = fVar != null ? 0 : this.k;
        int i3 = fVar != null ? 0 : this.j;
        for (e eVar = this.d; eVar != null; eVar = eVar.b) {
            int i4 = fVar != null ? 0 : eVar.g;
            int i5 = fVar != null ? 0 : eVar.h;
            switch (eVar.c) {
                case 1:
                    Fragment fragment = eVar.d;
                    fragment.H = i5;
                    this.b.a(fragment, t.b(i3), i2);
                    break;
                case 2:
                    Fragment fragment2 = eVar.d;
                    if (fragment2 != null) {
                        fragment2.H = i5;
                        this.b.a(fragment2, t.b(i3), i2);
                    }
                    if (eVar.i == null) {
                        break;
                    } else {
                        for (int i6 = 0; i6 < eVar.i.size(); i6++) {
                            Fragment fragment3 = (Fragment) eVar.i.get(i6);
                            fragment3.H = i4;
                            this.b.a(fragment3, false);
                        }
                        break;
                    }
                case 3:
                    Fragment fragment4 = eVar.d;
                    fragment4.H = i4;
                    this.b.a(fragment4, false);
                    break;
                case 4:
                    Fragment fragment5 = eVar.d;
                    fragment5.H = i4;
                    this.b.c(fragment5, t.b(i3), i2);
                    break;
                case 5:
                    Fragment fragment6 = eVar.d;
                    fragment6.H = i5;
                    this.b.b(fragment6, t.b(i3), i2);
                    break;
                case 6:
                    Fragment fragment7 = eVar.d;
                    fragment7.H = i4;
                    this.b.e(fragment7, t.b(i3), i2);
                    break;
                case 7:
                    Fragment fragment8 = eVar.d;
                    fragment8.H = i4;
                    this.b.d(fragment8, t.b(i3), i2);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + eVar.c);
            }
        }
        this.b.a(this.b.n, t.b(i3), i2, true);
        if (this.p >= 0) {
            t tVar = this.b;
            int i7 = this.p;
            synchronized (tVar) {
                tVar.k.set(i7, null);
                if (tVar.l == null) {
                    tVar.l = new ArrayList();
                }
                if (t.a) {
                    Log.v("FragmentManager", "Freeing back stack index " + i7);
                }
                tVar.l.add(Integer.valueOf(i7));
            }
            this.p = -1;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        if (this.l) {
            if (t.a) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i2);
            }
            for (e eVar = this.c; eVar != null; eVar = eVar.a) {
                if (eVar.d != null) {
                    eVar.d.s += i2;
                    if (t.a) {
                        Log.v("FragmentManager", "Bump nesting of " + eVar.d + " to " + eVar.d.s);
                    }
                }
                if (eVar.i != null) {
                    for (int size = eVar.i.size() - 1; size >= 0; size--) {
                        Fragment fragment = (Fragment) eVar.i.get(size);
                        fragment.s += i2;
                        if (t.a) {
                            Log.v("FragmentManager", "Bump nesting of " + fragment + " to " + fragment.s);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(e eVar) {
        if (this.c == null) {
            this.d = eVar;
            this.c = eVar;
        } else {
            eVar.b = this.d;
            this.d.a = eVar;
            this.d = eVar;
        }
        eVar.e = this.f;
        eVar.f = this.g;
        eVar.g = this.h;
        eVar.h = this.i;
        this.e++;
    }

    public final void a(SparseArray sparseArray, SparseArray sparseArray2) {
        if (this.b.p.a()) {
            for (e eVar = this.c; eVar != null; eVar = eVar.a) {
                switch (eVar.c) {
                    case 1:
                        a(sparseArray, eVar.d);
                        break;
                    case 2:
                        if (eVar.i != null) {
                            for (int size = eVar.i.size() - 1; size >= 0; size--) {
                                b(sparseArray2, (Fragment) eVar.i.get(size));
                            }
                        }
                        a(sparseArray, eVar.d);
                        break;
                    case 3:
                        b(sparseArray2, eVar.d);
                        break;
                    case 4:
                        b(sparseArray2, eVar.d);
                        break;
                    case 5:
                        a(sparseArray, eVar.d);
                        break;
                    case 6:
                        b(sparseArray2, eVar.d);
                        break;
                    case 7:
                        a(sparseArray, eVar.d);
                        break;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.a.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.f
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.app.Fragment, boolean):android.support.v4.e.a
      android.support.v4.app.a.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.e.a):android.support.v4.e.a
      android.support.v4.app.a.a(android.support.v4.app.a, android.support.v4.e.a, android.support.v4.app.f):void
      android.support.v4.app.a.a(android.support.v4.app.f, int, java.lang.Object):void
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.e.a, boolean):void
      android.support.v4.app.a.a(android.support.v4.e.a, java.lang.String, java.lang.String):void
      android.support.v4.app.a.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(android.support.v4.app.f, android.util.SparseArray, android.util.SparseArray):android.support.v4.app.f
      android.support.v4.app.af.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void */
    public final void a(String str, PrintWriter printWriter) {
        a(str, printWriter, true);
    }

    public final void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.n);
            printWriter.print(" mIndex=");
            printWriter.print(this.p);
            printWriter.print(" mCommitted=");
            printWriter.println(this.o);
            if (this.j != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.j));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.k));
            }
            if (!(this.f == 0 && this.g == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.g));
            }
            if (!(this.h == 0 && this.i == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.h));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.i));
            }
            if (!(this.q == 0 && this.r == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.q));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.r);
            }
            if (!(this.s == 0 && this.t == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.s));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.t);
            }
        }
        if (this.c != null) {
            printWriter.print(str);
            printWriter.println("Operations:");
            String str3 = str + "    ";
            int i2 = 0;
            e eVar = this.c;
            while (eVar != null) {
                switch (eVar.c) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    default:
                        str2 = "cmd=" + eVar.c;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(eVar.d);
                if (z) {
                    if (!(eVar.e == 0 && eVar.f == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(eVar.e));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(eVar.f));
                    }
                    if (!(eVar.g == 0 && eVar.h == 0)) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(eVar.g));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(eVar.h));
                    }
                }
                if (eVar.i != null && eVar.i.size() > 0) {
                    for (int i3 = 0; i3 < eVar.i.size(); i3++) {
                        printWriter.print(str3);
                        if (eVar.i.size() == 1) {
                            printWriter.print("Removed: ");
                        } else {
                            if (i3 == 0) {
                                printWriter.println("Removed:");
                            }
                            printWriter.print(str3);
                            printWriter.print("  #");
                            printWriter.print(i3);
                            printWriter.print(": ");
                        }
                        printWriter.println(eVar.i.get(i3));
                    }
                }
                eVar = eVar.a;
                i2++;
            }
        }
    }

    public final int b() {
        return a(true);
    }

    public final af b(Fragment fragment) {
        e eVar = new e();
        eVar.c = 7;
        eVar.d = fragment;
        a(eVar);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.a.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.f
     arg types: [android.util.SparseArray, android.util.SparseArray, int]
     candidates:
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.app.Fragment, boolean):android.support.v4.e.a
      android.support.v4.app.a.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.e.a):android.support.v4.e.a
      android.support.v4.app.a.a(android.support.v4.app.a, android.support.v4.e.a, android.support.v4.app.f):void
      android.support.v4.app.a.a(android.support.v4.app.f, int, java.lang.Object):void
      android.support.v4.app.a.a(android.support.v4.app.f, android.support.v4.e.a, boolean):void
      android.support.v4.app.a.a(android.support.v4.e.a, java.lang.String, java.lang.String):void
      android.support.v4.app.a.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(android.support.v4.app.f, android.util.SparseArray, android.util.SparseArray):android.support.v4.app.f
      android.support.v4.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.af.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.af
      android.support.v4.app.a.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.f */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(float, float):android.view.animation.Animation
      android.support.v4.app.t.a(int, android.support.v4.app.a):void
      android.support.v4.app.t.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.t.a(android.os.Parcelable, java.util.List):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.t.a(float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.t.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.s.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.view.an.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.t.a(int, int, int, boolean):void */
    public final void run() {
        f fVar;
        Fragment fragment;
        if (t.a) {
            Log.v("FragmentManager", "Run: " + this);
        }
        if (!this.l || this.p >= 0) {
            a(1);
            if (a) {
                SparseArray sparseArray = new SparseArray();
                SparseArray sparseArray2 = new SparseArray();
                b(sparseArray, sparseArray2);
                fVar = a(sparseArray, sparseArray2, false);
            } else {
                fVar = null;
            }
            int i2 = fVar != null ? 0 : this.k;
            int i3 = fVar != null ? 0 : this.j;
            for (e eVar = this.c; eVar != null; eVar = eVar.a) {
                int i4 = fVar != null ? 0 : eVar.e;
                int i5 = fVar != null ? 0 : eVar.f;
                switch (eVar.c) {
                    case 1:
                        Fragment fragment2 = eVar.d;
                        fragment2.H = i4;
                        this.b.a(fragment2, false);
                        break;
                    case 2:
                        Fragment fragment3 = eVar.d;
                        int i6 = fragment3.y;
                        if (this.b.g != null) {
                            int i7 = 0;
                            fragment = fragment3;
                            while (true) {
                                int i8 = i7;
                                if (i8 < this.b.g.size()) {
                                    Fragment fragment4 = (Fragment) this.b.g.get(i8);
                                    if (t.a) {
                                        Log.v("FragmentManager", "OP_REPLACE: adding=" + fragment + " old=" + fragment4);
                                    }
                                    if (fragment4.y == i6) {
                                        if (fragment4 == fragment) {
                                            fragment = null;
                                            eVar.d = null;
                                        } else {
                                            if (eVar.i == null) {
                                                eVar.i = new ArrayList();
                                            }
                                            eVar.i.add(fragment4);
                                            fragment4.H = i5;
                                            if (this.l) {
                                                fragment4.s++;
                                                if (t.a) {
                                                    Log.v("FragmentManager", "Bump nesting of " + fragment4 + " to " + fragment4.s);
                                                }
                                            }
                                            this.b.a(fragment4, i3, i2);
                                        }
                                    }
                                    i7 = i8 + 1;
                                }
                            }
                        } else {
                            fragment = fragment3;
                        }
                        if (fragment == null) {
                            break;
                        } else {
                            fragment.H = i4;
                            this.b.a(fragment, false);
                            break;
                        }
                    case 3:
                        Fragment fragment5 = eVar.d;
                        fragment5.H = i5;
                        this.b.a(fragment5, i3, i2);
                        break;
                    case 4:
                        Fragment fragment6 = eVar.d;
                        fragment6.H = i5;
                        this.b.b(fragment6, i3, i2);
                        break;
                    case 5:
                        Fragment fragment7 = eVar.d;
                        fragment7.H = i4;
                        this.b.c(fragment7, i3, i2);
                        break;
                    case 6:
                        Fragment fragment8 = eVar.d;
                        fragment8.H = i5;
                        this.b.d(fragment8, i3, i2);
                        break;
                    case 7:
                        Fragment fragment9 = eVar.d;
                        fragment9.H = i4;
                        this.b.e(fragment9, i3, i2);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown cmd: " + eVar.c);
                }
            }
            this.b.a(this.b.n, i3, i2, true);
            if (this.l) {
                t tVar = this.b;
                if (tVar.i == null) {
                    tVar.i = new ArrayList();
                }
                tVar.i.add(this);
                tVar.e();
                return;
            }
            return;
        }
        throw new IllegalStateException("addToBackStack() called after commit()");
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.p >= 0) {
            sb.append(" #");
            sb.append(this.p);
        }
        if (this.n != null) {
            sb.append(" ");
            sb.append(this.n);
        }
        sb.append("}");
        return sb.toString();
    }
}
