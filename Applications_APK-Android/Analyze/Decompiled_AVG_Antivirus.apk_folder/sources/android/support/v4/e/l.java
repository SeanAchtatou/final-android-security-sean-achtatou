package android.support.v4.e;

import java.util.Iterator;
import java.util.Map;

final class l implements Iterator, Map.Entry {
    int a;
    int b;
    boolean c = false;
    final /* synthetic */ h d;

    l(h hVar) {
        this.d = hVar;
        this.a = hVar.a() - 1;
        this.b = -1;
    }

    public final boolean equals(Object obj) {
        if (!this.c) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        } else if (!(obj instanceof Map.Entry)) {
            return false;
        } else {
            Map.Entry entry = (Map.Entry) obj;
            return c.a(entry.getKey(), this.d.a(this.b, 0)) && c.a(entry.getValue(), this.d.a(this.b, 1));
        }
    }

    public final Object getKey() {
        if (this.c) {
            return this.d.a(this.b, 0);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public final Object getValue() {
        if (this.c) {
            return this.d.a(this.b, 1);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public final boolean hasNext() {
        return this.b < this.a;
    }

    public final int hashCode() {
        int i = 0;
        if (!this.c) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        Object a2 = this.d.a(this.b, 0);
        Object a3 = this.d.a(this.b, 1);
        int hashCode = a2 == null ? 0 : a2.hashCode();
        if (a3 != null) {
            i = a3.hashCode();
        }
        return i ^ hashCode;
    }

    public final /* bridge */ /* synthetic */ Object next() {
        this.b++;
        this.c = true;
        return this;
    }

    public final void remove() {
        if (!this.c) {
            throw new IllegalStateException();
        }
        this.d.a(this.b);
        this.b--;
        this.a--;
        this.c = false;
    }

    public final Object setValue(Object obj) {
        if (this.c) {
            return this.d.a(this.b, obj);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public final String toString() {
        return getKey() + "=" + getValue();
    }
}
