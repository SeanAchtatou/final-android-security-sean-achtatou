package android.support.v4.e;

public final class f implements Cloneable {
    private static final Object a = new Object();
    private boolean b;
    private long[] c;
    private Object[] d;
    private int e;

    public f() {
        this((byte) 0);
    }

    private f(byte b2) {
        this.b = false;
        int a2 = c.a(10);
        this.c = new long[a2];
        this.d = new Object[a2];
        this.e = 0;
    }

    private long c(int i) {
        if (this.b) {
            d();
        }
        return this.c[i];
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public f clone() {
        try {
            f fVar = (f) super.clone();
            try {
                fVar.c = (long[]) this.c.clone();
                fVar.d = (Object[]) this.d.clone();
                return fVar;
            } catch (CloneNotSupportedException e2) {
                return fVar;
            }
        } catch (CloneNotSupportedException e3) {
            return null;
        }
    }

    private void d() {
        int i = this.e;
        long[] jArr = this.c;
        Object[] objArr = this.d;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != a) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.b = false;
        this.e = i2;
    }

    public final int a() {
        if (this.b) {
            d();
        }
        return this.e;
    }

    public final Object a(long j) {
        int a2 = c.a(this.c, this.e, j);
        if (a2 < 0 || this.d[a2] == a) {
            return null;
        }
        return this.d[a2];
    }

    public final void a(int i) {
        if (this.d[i] != a) {
            this.d[i] = a;
            this.b = true;
        }
    }

    public final void a(long j, Object obj) {
        int a2 = c.a(this.c, this.e, j);
        if (a2 >= 0) {
            this.d[a2] = obj;
            return;
        }
        int i = a2 ^ -1;
        if (i >= this.e || this.d[i] != a) {
            if (this.b && this.e >= this.c.length) {
                d();
                i = c.a(this.c, this.e, j) ^ -1;
            }
            if (this.e >= this.c.length) {
                int a3 = c.a(this.e + 1);
                long[] jArr = new long[a3];
                Object[] objArr = new Object[a3];
                System.arraycopy(this.c, 0, jArr, 0, this.c.length);
                System.arraycopy(this.d, 0, objArr, 0, this.d.length);
                this.c = jArr;
                this.d = objArr;
            }
            if (this.e - i != 0) {
                System.arraycopy(this.c, i, this.c, i + 1, this.e - i);
                System.arraycopy(this.d, i, this.d, i + 1, this.e - i);
            }
            this.c[i] = j;
            this.d[i] = obj;
            this.e++;
            return;
        }
        this.c[i] = j;
        this.d[i] = obj;
    }

    public final Object b(int i) {
        if (this.b) {
            d();
        }
        return this.d[i];
    }

    public final void b() {
        int i = this.e;
        Object[] objArr = this.d;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.e = 0;
        this.b = false;
    }

    public final String toString() {
        if (a() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.e * 28);
        sb.append('{');
        for (int i = 0; i < this.e; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(c(i));
            sb.append('=');
            Object b2 = b(i);
            if (b2 != this) {
                sb.append(b2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
