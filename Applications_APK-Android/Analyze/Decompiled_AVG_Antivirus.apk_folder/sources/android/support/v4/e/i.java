package android.support.v4.e;

import java.util.Iterator;

final class i implements Iterator {
    final int a;
    int b;
    int c;
    boolean d = false;
    final /* synthetic */ h e;

    i(h hVar, int i) {
        this.e = hVar;
        this.a = i;
        this.b = hVar.a();
    }

    public final boolean hasNext() {
        return this.c < this.b;
    }

    public final Object next() {
        Object a2 = this.e.a(this.c, this.a);
        this.c++;
        this.d = true;
        return a2;
    }

    public final void remove() {
        if (!this.d) {
            throw new IllegalStateException();
        }
        this.c--;
        this.b--;
        this.d = false;
        this.e.a(this.c);
    }
}
