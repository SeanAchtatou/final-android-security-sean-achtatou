package android.support.v4.e;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class a extends q implements Map {
    h a;

    private h a() {
        if (this.a == null) {
            this.a = new b(this);
        }
        return this.a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.e.h.a(java.util.Map, java.util.Collection):boolean
     arg types: [android.support.v4.e.a, java.util.Collection]
     candidates:
      android.support.v4.e.h.a(java.util.Set, java.lang.Object):boolean
      android.support.v4.e.h.a(int, int):java.lang.Object
      android.support.v4.e.h.a(int, java.lang.Object):java.lang.Object
      android.support.v4.e.h.a(java.lang.Object, java.lang.Object):void
      android.support.v4.e.h.a(java.lang.Object[], int):java.lang.Object[]
      android.support.v4.e.h.a(java.util.Map, java.util.Collection):boolean */
    public final boolean a(Collection collection) {
        return h.a((Map) this, collection);
    }

    public Set entrySet() {
        h a2 = a();
        if (a2.b == null) {
            a2.b = new j(a2);
        }
        return a2.b;
    }

    public Set keySet() {
        h a2 = a();
        if (a2.c == null) {
            a2.c = new k(a2);
        }
        return a2.c;
    }

    public void putAll(Map map) {
        a(this.h + map.size());
        for (Map.Entry entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    public Collection values() {
        h a2 = a();
        if (a2.d == null) {
            a2.d = new m(a2);
        }
        return a2.d;
    }
}
