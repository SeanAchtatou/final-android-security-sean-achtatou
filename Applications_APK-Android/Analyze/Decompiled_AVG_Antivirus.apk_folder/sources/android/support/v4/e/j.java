package android.support.v4.e;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class j implements Set {
    final /* synthetic */ h a;

    j(h hVar) {
        this.a = hVar;
    }

    public final /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(Collection collection) {
        int a2 = this.a.a();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            this.a.a(entry.getKey(), entry.getValue());
        }
        return a2 != this.a.a();
    }

    public final void clear() {
        this.a.c();
    }

    public final boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        int a2 = this.a.a(entry.getKey());
        if (a2 >= 0) {
            return c.a(this.a.a(a2, 1), entry.getValue());
        }
        return false;
    }

    public final boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.e.h.a(java.util.Set, java.lang.Object):boolean
     arg types: [android.support.v4.e.j, java.lang.Object]
     candidates:
      android.support.v4.e.h.a(java.util.Map, java.util.Collection):boolean
      android.support.v4.e.h.a(int, int):java.lang.Object
      android.support.v4.e.h.a(int, java.lang.Object):java.lang.Object
      android.support.v4.e.h.a(java.lang.Object, java.lang.Object):void
      android.support.v4.e.h.a(java.lang.Object[], int):java.lang.Object[]
      android.support.v4.e.h.a(java.util.Set, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        return h.a((Set) this, obj);
    }

    public final int hashCode() {
        int a2 = this.a.a() - 1;
        int i = 0;
        while (a2 >= 0) {
            Object a3 = this.a.a(a2, 0);
            Object a4 = this.a.a(a2, 1);
            a2--;
            i += (a4 == null ? 0 : a4.hashCode()) ^ (a3 == null ? 0 : a3.hashCode());
        }
        return i;
    }

    public final boolean isEmpty() {
        return this.a.a() == 0;
    }

    public final Iterator iterator() {
        return new l(this.a);
    }

    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public final boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public final int size() {
        return this.a.a();
    }

    public final Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    public final Object[] toArray(Object[] objArr) {
        throw new UnsupportedOperationException();
    }
}
