package android.support.v4.e;

import java.util.Map;

final class b extends h {
    final /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return this.a.h;
    }

    /* access modifiers changed from: protected */
    public final int a(Object obj) {
        return this.a.a(obj);
    }

    /* access modifiers changed from: protected */
    public final Object a(int i, int i2) {
        return this.a.g[(i << 1) + i2];
    }

    /* access modifiers changed from: protected */
    public final Object a(int i, Object obj) {
        return this.a.a(i, obj);
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        this.a.d(i);
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj, Object obj2) {
        this.a.put(obj, obj2);
    }

    /* access modifiers changed from: protected */
    public final int b(Object obj) {
        return this.a.b(obj);
    }

    /* access modifiers changed from: protected */
    public final Map b() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.a.clear();
    }
}
