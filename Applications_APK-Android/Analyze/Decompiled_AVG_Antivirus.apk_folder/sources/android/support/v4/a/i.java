package android.support.v4.a;

import android.animation.Animator;

final class i implements Animator.AnimatorListener {
    final b a;
    final l b;

    public i(b bVar, l lVar) {
        this.a = bVar;
        this.b = lVar;
    }

    public final void onAnimationCancel(Animator animator) {
        this.a.a();
    }

    public final void onAnimationEnd(Animator animator) {
        this.a.a(this.b);
    }

    public final void onAnimationRepeat(Animator animator) {
    }

    public final void onAnimationStart(Animator animator) {
    }
}
