package android.support.v4.a;

import android.view.View;
import java.util.ArrayList;
import java.util.List;

final class f implements l {
    List a = new ArrayList();
    List b = new ArrayList();
    View c;
    /* access modifiers changed from: private */
    public long d;
    /* access modifiers changed from: private */
    public long e = 200;
    /* access modifiers changed from: private */
    public float f = 0.0f;
    private boolean g = false;
    private boolean h = false;
    /* access modifiers changed from: private */
    public Runnable i = new g(this);

    /* access modifiers changed from: private */
    public void d() {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            ((b) this.a.get(size)).a(this);
        }
    }

    public final void a() {
        if (!this.g) {
            this.g = true;
            for (int size = this.a.size() - 1; size >= 0; size--) {
                this.a.get(size);
            }
            this.f = 0.0f;
            this.d = this.c.getDrawingTime();
            this.c.postDelayed(this.i, 16);
        }
    }

    public final void a(long j) {
        if (!this.g) {
            this.e = j;
        }
    }

    public final void a(b bVar) {
        this.a.add(bVar);
    }

    public final void a(d dVar) {
        this.b.add(dVar);
    }

    public final void a(View view) {
        this.c = view;
    }

    public final void b() {
        if (!this.h) {
            this.h = true;
            if (this.g) {
                for (int size = this.a.size() - 1; size >= 0; size--) {
                    ((b) this.a.get(size)).a();
                }
            }
            d();
        }
    }

    public final float c() {
        return this.f;
    }
}
