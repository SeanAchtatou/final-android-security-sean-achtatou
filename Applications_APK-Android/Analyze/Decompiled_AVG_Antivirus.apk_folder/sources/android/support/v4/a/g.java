package android.support.v4.a;

final class g implements Runnable {
    final /* synthetic */ f a;

    g(f fVar) {
        this.a = fVar;
    }

    public final void run() {
        float drawingTime = (((float) (this.a.c.getDrawingTime() - this.a.d)) * 1.0f) / ((float) this.a.e);
        if (drawingTime > 1.0f || this.a.c.getParent() == null) {
            drawingTime = 1.0f;
        }
        float unused = this.a.f = drawingTime;
        f fVar = this.a;
        for (int size = fVar.b.size() - 1; size >= 0; size--) {
            ((d) fVar.b.get(size)).a(fVar);
        }
        if (this.a.f >= 1.0f) {
            this.a.d();
        } else {
            this.a.c.postDelayed(this.a.i, 16);
        }
    }
}
