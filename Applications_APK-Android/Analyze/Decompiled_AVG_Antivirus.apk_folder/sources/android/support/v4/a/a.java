package android.support.v4.a;

import android.os.Build;
import android.view.View;

public abstract class a {
    static c a;

    static {
        if (Build.VERSION.SDK_INT >= 12) {
            a = new h();
        } else {
            a = new e();
        }
    }

    public static l a() {
        return a.a();
    }

    public static void a(View view) {
        a.a(view);
    }
}
