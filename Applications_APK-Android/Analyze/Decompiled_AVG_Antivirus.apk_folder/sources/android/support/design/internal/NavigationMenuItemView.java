package android.support.design.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.design.c;
import android.support.design.e;
import android.support.v4.b.a.a;
import android.support.v4.widget.bo;
import android.support.v7.internal.view.menu.aa;
import android.support.v7.internal.view.menu.m;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

public class NavigationMenuItemView extends TextView implements aa {
    private static final int[] a = {16842912};
    private int b;
    private m c;
    private ColorStateList d;

    public NavigationMenuItemView(Context context) {
        this(context, null);
    }

    public NavigationMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NavigationMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.b = context.getResources().getDimensionPixelSize(e.navigation_icon_size);
    }

    private void a(Drawable drawable) {
        if (drawable != null) {
            drawable = a.c(drawable.getConstantState().newDrawable()).mutate();
            drawable.setBounds(0, 0, this.b, this.b);
            a.a(drawable, this.d);
        }
        bo.a(this, drawable);
    }

    public final m a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final void a(ColorStateList colorStateList) {
        this.d = colorStateList;
        if (this.c != null) {
            a(this.c.getIcon());
        }
    }

    public final void a(m mVar) {
        StateListDrawable stateListDrawable;
        this.c = mVar;
        setVisibility(mVar.isVisible() ? 0 : 8);
        if (getBackground() == null) {
            TypedValue typedValue = new TypedValue();
            if (getContext().getTheme().resolveAttribute(c.a, typedValue, true)) {
                stateListDrawable = new StateListDrawable();
                stateListDrawable.addState(a, new ColorDrawable(typedValue.data));
                stateListDrawable.addState(EMPTY_STATE_SET, new ColorDrawable(0));
            } else {
                stateListDrawable = null;
            }
            setBackgroundDrawable(stateListDrawable);
        }
        mVar.isCheckable();
        refreshDrawableState();
        mVar.isChecked();
        refreshDrawableState();
        setEnabled(mVar.isEnabled());
        setText(mVar.getTitle());
        a(mVar.getIcon());
    }

    public final boolean b() {
        return false;
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (this.c != null && this.c.isCheckable() && this.c.isChecked()) {
            mergeDrawableStates(onCreateDrawableState, a);
        }
        return onCreateDrawableState;
    }
}
