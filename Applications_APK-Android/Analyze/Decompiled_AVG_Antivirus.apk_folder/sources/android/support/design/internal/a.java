package android.support.design.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.e;
import android.support.design.g;
import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.x;
import android.support.v7.internal.view.menu.y;
import android.support.v7.internal.view.menu.z;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

public final class a implements x, AdapterView.OnItemClickListener {
    private NavigationMenuView a;
    private LinearLayout b;
    private y c;
    /* access modifiers changed from: private */
    public i d;
    private int e;
    private b f;
    /* access modifiers changed from: private */
    public LayoutInflater g;
    /* access modifiers changed from: private */
    public ColorStateList h;
    /* access modifiers changed from: private */
    public ColorStateList i;
    /* access modifiers changed from: private */
    public Drawable j;
    private int k;
    /* access modifiers changed from: private */
    public int l;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.design.internal.NavigationMenuView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final z a(ViewGroup viewGroup) {
        if (this.a == null) {
            this.a = (NavigationMenuView) this.g.inflate(g.design_navigation_menu, viewGroup, false);
            if (this.f == null) {
                this.f = new b(this);
            }
            this.b = (LinearLayout) this.g.inflate(g.design_navigation_item_header, (ViewGroup) this.a, false);
            this.a.addHeaderView(this.b);
            this.a.setAdapter((ListAdapter) this.f);
            this.a.setOnItemClickListener(this);
        }
        return this.a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View a(int i2) {
        View inflate = this.g.inflate(i2, (ViewGroup) this.b, false);
        this.b.addView(inflate);
        this.a.setPadding(0, 0, 0, this.a.getPaddingBottom());
        return inflate;
    }

    public final void a(Context context, i iVar) {
        this.g = LayoutInflater.from(context);
        this.d = iVar;
        Resources resources = context.getResources();
        this.k = resources.getDimensionPixelOffset(e.navigation_padding_top_default);
        this.l = resources.getDimensionPixelOffset(e.navigation_separator_vertical_padding);
    }

    public final void a(ColorStateList colorStateList) {
        this.i = colorStateList;
    }

    public final void a(Drawable drawable) {
        this.j = drawable;
    }

    public final void a(Parcelable parcelable) {
        Bundle bundle = (Bundle) parcelable;
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.a.restoreHierarchyState(sparseParcelableArray);
        }
        Bundle bundle2 = bundle.getBundle("android:menu:adapter");
        if (bundle2 != null) {
            this.f.a(bundle2);
        }
    }

    public final void a(i iVar, boolean z) {
        if (this.c != null) {
            this.c.a(iVar, z);
        }
    }

    public final void a(boolean z) {
        if (this.f != null) {
            this.f.notifyDataSetChanged();
        }
    }

    public final boolean a() {
        return false;
    }

    public final boolean a(ad adVar) {
        return false;
    }

    public final boolean a(m mVar) {
        return false;
    }

    public final int b() {
        return this.e;
    }

    public final void b(ColorStateList colorStateList) {
        this.h = colorStateList;
    }

    public final void b(boolean z) {
        if (this.f != null) {
            this.f.a(z);
        }
    }

    public final boolean b(m mVar) {
        return false;
    }

    public final void c() {
        this.e = 1;
    }

    public final Parcelable d() {
        Bundle bundle = new Bundle();
        if (this.a != null) {
            SparseArray sparseArray = new SparseArray();
            this.a.saveHierarchyState(sparseArray);
            bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        }
        if (this.f != null) {
            bundle.putBundle("android:menu:adapter", this.f.a());
        }
        return bundle;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        int headerViewsCount = i2 - this.a.getHeaderViewsCount();
        if (headerViewsCount >= 0) {
            this.d.a(this.f.getItem(headerViewsCount).d(), this, 0);
        }
    }
}
