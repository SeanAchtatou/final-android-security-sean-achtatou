package android.support.design.internal;

import android.graphics.Rect;
import android.support.v4.view.bi;
import android.support.v4.view.bx;
import android.support.v4.view.eo;
import android.view.View;

final class d implements bi {
    final /* synthetic */ ScrimInsetsFrameLayout a;

    d(ScrimInsetsFrameLayout scrimInsetsFrameLayout) {
        this.a = scrimInsetsFrameLayout;
    }

    public final eo a(View view, eo eoVar) {
        if (this.a.b == null) {
            Rect unused = this.a.b = new Rect();
        }
        this.a.b.set(eoVar.a(), eoVar.b(), eoVar.c(), eoVar.d());
        this.a.setWillNotDraw(this.a.b.isEmpty() || this.a.a == null);
        bx.d(this.a);
        return eoVar.f();
    }
}
