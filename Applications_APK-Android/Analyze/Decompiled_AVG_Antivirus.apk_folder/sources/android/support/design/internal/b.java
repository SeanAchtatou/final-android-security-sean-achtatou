package android.support.design.internal;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.g;
import android.support.v7.internal.view.menu.m;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;

final class b extends BaseAdapter {
    final /* synthetic */ a a;
    private final ArrayList b = new ArrayList();
    private ColorDrawable c;
    private boolean d;

    b(a aVar) {
        this.a = aVar;
        b();
    }

    private void a(int i, int i2) {
        while (i < i2) {
            m d2 = ((c) this.b.get(i)).d();
            if (d2.getIcon() == null) {
                if (this.c == null) {
                    this.c = new ColorDrawable(17170445);
                }
                d2.setIcon(this.c);
            }
            i++;
        }
    }

    private void b() {
        boolean z;
        boolean z2;
        int i;
        boolean z3;
        int i2;
        int i3;
        if (!this.d) {
            this.b.clear();
            int i4 = -1;
            int size = this.a.d.k().size();
            int i5 = 0;
            boolean z4 = false;
            int i6 = 0;
            while (i5 < size) {
                m mVar = (m) this.a.d.k().get(i5);
                if (mVar.hasSubMenu()) {
                    SubMenu subMenu = mVar.getSubMenu();
                    if (subMenu.hasVisibleItems()) {
                        if (i5 != 0) {
                            this.b.add(c.a(this.a.l, 0));
                        }
                        this.b.add(c.a(mVar));
                        int size2 = this.b.size();
                        int size3 = subMenu.size();
                        boolean z5 = false;
                        for (int i7 = 0; i7 < size3; i7++) {
                            MenuItem item = subMenu.getItem(i7);
                            if (item.isVisible()) {
                                if (!z5 && item.getIcon() != null) {
                                    z5 = true;
                                }
                                this.b.add(c.a((m) item));
                            }
                        }
                        if (z5) {
                            a(size2, this.b.size());
                        }
                    }
                    z3 = z;
                    i2 = i6;
                    i3 = i4;
                } else {
                    int groupId = mVar.getGroupId();
                    if (groupId != i4) {
                        i6 = this.b.size();
                        z = mVar.getIcon() != null;
                        if (i5 != 0) {
                            this.b.add(c.a(this.a.l, this.a.l));
                            z2 = z;
                            i = i6 + 1;
                        }
                        z2 = z;
                        i = i6;
                    } else {
                        if (!z && mVar.getIcon() != null) {
                            a(i6, this.b.size());
                            z2 = true;
                            i = i6;
                        }
                        z2 = z;
                        i = i6;
                    }
                    if (z2 && mVar.getIcon() == null) {
                        mVar.setIcon(17170445);
                    }
                    this.b.add(c.a(mVar));
                    z3 = z2;
                    i2 = i;
                    i3 = groupId;
                }
                i5++;
                i4 = i3;
                i6 = i2;
                z4 = z3;
            }
        }
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        ArrayList arrayList = new ArrayList();
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            m d2 = ((c) it.next()).d();
            if (d2 != null && d2.isChecked()) {
                arrayList.add(Integer.valueOf(d2.getItemId()));
            }
        }
        bundle.putIntegerArrayList("android:menu:checked", arrayList);
        return bundle;
    }

    /* renamed from: a */
    public final c getItem(int i) {
        return (c) this.b.get(i);
    }

    public final void a(Bundle bundle) {
        ArrayList<Integer> integerArrayList = bundle.getIntegerArrayList("android:menu:checked");
        if (integerArrayList != null) {
            this.d = true;
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                m d2 = ((c) it.next()).d();
                if (d2 != null && integerArrayList.contains(Integer.valueOf(d2.getItemId()))) {
                    d2.setChecked(true);
                }
            }
            this.d = false;
            b();
        }
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public final boolean areAllItemsEnabled() {
        return false;
    }

    public final int getCount() {
        return this.b.size();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final int getItemViewType(int i) {
        c a2 = getItem(i);
        if (a2.a()) {
            return 2;
        }
        return a2.d().hasSubMenu() ? 1 : 0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        c a2 = getItem(i);
        switch (getItemViewType(i)) {
            case 0:
                View inflate = view == null ? this.a.g.inflate(g.design_navigation_item, viewGroup, false) : view;
                NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) inflate;
                navigationMenuItemView.a(this.a.i);
                navigationMenuItemView.setTextColor(this.a.h);
                navigationMenuItemView.setBackgroundDrawable(this.a.j != null ? this.a.j.getConstantState().newDrawable() : null);
                navigationMenuItemView.a(a2.d());
                return inflate;
            case 1:
                View inflate2 = view == null ? this.a.g.inflate(g.design_navigation_item_subheader, viewGroup, false) : view;
                ((TextView) inflate2).setText(a2.d().getTitle());
                return inflate2;
            case 2:
                if (view == null) {
                    view = this.a.g.inflate(g.design_navigation_item_separator, viewGroup, false);
                }
                view.setPadding(0, a2.b(), 0, a2.c());
                break;
        }
        return view;
    }

    public final int getViewTypeCount() {
        return 3;
    }

    public final boolean isEnabled(int i) {
        return getItem(i).e();
    }

    public final void notifyDataSetChanged() {
        b();
        super.notifyDataSetChanged();
    }
}
