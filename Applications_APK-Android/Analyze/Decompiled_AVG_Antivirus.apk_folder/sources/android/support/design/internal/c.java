package android.support.design.internal;

import android.support.v7.internal.view.menu.m;

final class c {
    private final m a;
    private final int b;
    private final int c;

    private c(m mVar, int i, int i2) {
        this.a = mVar;
        this.b = i;
        this.c = i2;
    }

    public static c a(int i, int i2) {
        return new c(null, i, i2);
    }

    public static c a(m mVar) {
        return new c(mVar, 0, 0);
    }

    public final boolean a() {
        return this.a == null;
    }

    public final int b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }

    public final m d() {
        return this.a;
    }

    public final boolean e() {
        return this.a != null && !this.a.hasSubMenu() && this.a.isEnabled();
    }
}
