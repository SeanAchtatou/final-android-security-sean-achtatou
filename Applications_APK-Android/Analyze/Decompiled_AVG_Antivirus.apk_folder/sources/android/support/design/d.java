package android.support.design;

public final class d {

    /* renamed from: a */
    public static final int fab_stroke_end_inner_color = 2131492894;

    /* renamed from: b */
    public static final int fab_stroke_end_outer_color = 2131492895;

    /* renamed from: c */
    public static final int fab_stroke_top_inner_color = 2131492896;

    /* renamed from: d */
    public static final int fab_stroke_top_outer_color = 2131492897;

    /* renamed from: e */
    public static final int shadow_end_color = 2131492942;

    /* renamed from: f */
    public static final int shadow_mid_color = 2131492943;

    /* renamed from: g */
    public static final int shadow_start_color = 2131492944;
}
