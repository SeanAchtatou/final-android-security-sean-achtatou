package android.support.design;

public final class e {

    /* renamed from: a */
    public static final int fab_content_size = 2131230798;

    /* renamed from: b */
    public static final int fab_size_mini = 2131230800;

    /* renamed from: c */
    public static final int fab_size_normal = 2131230801;

    /* renamed from: d */
    public static final int navigation_icon_size = 2131230806;

    /* renamed from: e */
    public static final int navigation_padding_top_default = 2131230743;

    /* renamed from: f */
    public static final int navigation_separator_vertical_padding = 2131230809;

    /* renamed from: g */
    public static final int snackbar_padding_vertical = 2131230817;

    /* renamed from: h */
    public static final int snackbar_padding_vertical_2lines = 2131230740;
}
