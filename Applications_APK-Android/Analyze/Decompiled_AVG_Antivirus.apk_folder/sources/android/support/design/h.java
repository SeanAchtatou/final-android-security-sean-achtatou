package android.support.design;

public final class h {

    /* renamed from: a */
    public static final int TextAppearance_AppCompat_Title = 2131296452;

    /* renamed from: b */
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Title = 2131296457;

    /* renamed from: c */
    public static final int TextAppearance_Design_Tab = 2131296473;

    /* renamed from: d */
    public static final int Widget_Design_AppBarLayout = 2131296561;

    /* renamed from: e */
    public static final int Widget_Design_CollapsingToolbar = 2131296562;

    /* renamed from: f */
    public static final int Widget_Design_CoordinatorLayout = 2131296563;

    /* renamed from: g */
    public static final int Widget_Design_FloatingActionButton = 2131296564;

    /* renamed from: h */
    public static final int Widget_Design_NavigationView = 2131296565;

    /* renamed from: i */
    public static final int Widget_Design_ScrimInsetsFrameLayout = 2131296566;

    /* renamed from: j */
    public static final int Widget_Design_TabLayout = 2131296257;

    /* renamed from: k */
    public static final int Widget_Design_TextInputLayout = 2131296568;
}
