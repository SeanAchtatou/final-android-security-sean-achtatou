package android.support.design.widget;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

final class bt extends br {
    private static final Handler a = new Handler(Looper.getMainLooper());
    private long b;
    private boolean c;
    private final int[] d = new int[2];
    private final float[] e = new float[2];
    private int f = 200;
    private Interpolator g;
    private bs h;
    private float i;
    private final Runnable j = new bu(this);

    bt() {
    }

    static /* synthetic */ void a(bt btVar) {
        if (btVar.c) {
            float uptimeMillis = ((float) (SystemClock.uptimeMillis() - btVar.b)) / ((float) btVar.f);
            if (btVar.g != null) {
                uptimeMillis = btVar.g.getInterpolation(uptimeMillis);
            }
            btVar.i = uptimeMillis;
            if (btVar.h != null) {
                btVar.h.a();
            }
            if (SystemClock.uptimeMillis() >= btVar.b + ((long) btVar.f)) {
                btVar.c = false;
            }
        }
        if (btVar.c) {
            a.postDelayed(btVar.j, 10);
        }
    }

    public final void a() {
        if (!this.c) {
            if (this.g == null) {
                this.g = new AccelerateDecelerateInterpolator();
            }
            this.b = SystemClock.uptimeMillis();
            this.c = true;
            a.postDelayed(this.j, 10);
        }
    }

    public final void a(float f2, float f3) {
        this.e[0] = f2;
        this.e[1] = f3;
    }

    public final void a(int i2) {
        this.f = i2;
    }

    public final void a(int i2, int i3) {
        this.d[0] = i2;
        this.d[1] = i3;
    }

    public final void a(bs bsVar) {
        this.h = bsVar;
    }

    public final void a(Interpolator interpolator) {
        this.g = interpolator;
    }

    public final boolean b() {
        return this.c;
    }

    public final int c() {
        return a.a(this.d[0], this.d[1], this.i);
    }

    public final float d() {
        return a.a(this.e[0], this.e[1], this.i);
    }

    public final void e() {
        this.c = false;
        a.removeCallbacks(this.j);
    }
}
