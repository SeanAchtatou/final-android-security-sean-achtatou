package android.support.design.widget;

import android.support.v4.widget.bx;
import android.view.View;

final class be extends bx {
    final /* synthetic */ bd a;
    private int b;

    be(bd bdVar) {
        this.a = bdVar;
    }

    public final int a(View view, int i) {
        int width;
        int width2;
        boolean z = android.support.v4.view.bx.h(view) == 1;
        if (this.a.f == 0) {
            if (z) {
                width = this.b - view.getWidth();
                width2 = this.b;
            } else {
                width = this.b;
                width2 = this.b + view.getWidth();
            }
        } else if (this.a.f != 1) {
            width = this.b - view.getWidth();
            width2 = this.b + view.getWidth();
        } else if (z) {
            width = this.b;
            width2 = this.b + view.getWidth();
        } else {
            width = this.b - view.getWidth();
            width2 = this.b;
        }
        return Math.min(Math.max(width, i), width2);
    }

    public final void a(int i) {
        if (this.a.b != null) {
            this.a.b.a(i);
        }
    }

    public final void a(View view, float f) {
        boolean z;
        int i;
        boolean z2 = true;
        int width = view.getWidth();
        if (f != 0.0f) {
            boolean z3 = android.support.v4.view.bx.h(view) == 1;
            z = this.a.f == 2 ? true : this.a.f == 0 ? z3 ? f < 0.0f : f > 0.0f : this.a.f == 1 ? z3 ? f > 0.0f : f < 0.0f : false;
        } else {
            z = Math.abs(view.getLeft() - this.b) >= Math.round(((float) view.getWidth()) * this.a.g);
        }
        if (z) {
            i = view.getLeft() < this.b ? this.b - width : this.b + width;
        } else {
            i = this.b;
            z2 = false;
        }
        if (this.a.a.a(i, view.getTop())) {
            android.support.v4.view.bx.a(view, new bg(this.a, view, z2));
        } else if (z2 && this.a.b != null) {
            this.a.b.a();
        }
    }

    public final boolean a(View view) {
        this.b = view.getLeft();
        return true;
    }

    public final int b(View view) {
        return view.getWidth();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, int):void
      android.support.v4.view.bx.c(android.view.View, float):void */
    public final void b(View view, int i) {
        float width = ((float) view.getWidth()) * this.a.h;
        float width2 = ((float) view.getWidth()) * this.a.i;
        if (((float) i) <= width) {
            android.support.v4.view.bx.c(view, 1.0f);
        } else if (((float) i) >= width2) {
            android.support.v4.view.bx.c(view, 0.0f);
        } else {
            android.support.v4.view.bx.c(view, bd.b(1.0f - bd.a(width, width2, (float) i)));
        }
    }

    public final int c(View view) {
        return view.getTop();
    }
}
