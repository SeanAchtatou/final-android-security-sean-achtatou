package android.support.design.widget;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

class ViewOffsetBehavior extends CoordinatorLayout.Behavior {
    private cc a;
    private int b = 0;
    private int c = 0;

    public ViewOffsetBehavior() {
    }

    public ViewOffsetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean a(int i) {
        if (this.a != null) {
            return this.a.a(i);
        }
        this.b = i;
        return false;
    }

    public boolean a(CoordinatorLayout coordinatorLayout, View view, int i) {
        coordinatorLayout.a(view, i);
        if (this.a == null) {
            this.a = new cc(view);
        }
        this.a.a();
        if (this.b != 0) {
            this.a.a(this.b);
            this.b = 0;
        }
        if (this.c == 0) {
            return true;
        }
        this.a.b(this.c);
        this.c = 0;
        return true;
    }

    public int b() {
        if (this.a != null) {
            return this.a.b();
        }
        return 0;
    }
}
