package android.support.design.widget;

import android.view.View;
import java.util.Comparator;

final class n implements Comparator {
    final /* synthetic */ CoordinatorLayout a;

    n(CoordinatorLayout coordinatorLayout) {
        this.a = coordinatorLayout;
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        View view = (View) obj;
        View view2 = (View) obj2;
        if (view != view2) {
            if (((r) view.getLayoutParams()).a(view2)) {
                return 1;
            }
            if (((r) view2.getLayoutParams()).a(view)) {
                return -1;
            }
        }
        return 0;
    }
}
