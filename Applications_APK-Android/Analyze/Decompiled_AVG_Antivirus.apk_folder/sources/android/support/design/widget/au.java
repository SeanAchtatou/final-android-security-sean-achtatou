package android.support.design.widget;

import android.support.design.widget.Snackbar;
import android.view.MotionEvent;
import android.view.View;

final class au extends bd {
    final /* synthetic */ Snackbar a;

    au(Snackbar snackbar) {
        this.a = snackbar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.bd.a(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
     arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.Snackbar$SnackbarLayout, android.view.MotionEvent]
     candidates:
      android.support.design.widget.bd.a(float, float, float):float
      android.support.design.widget.bd.a(int, int, int):int
      android.support.design.widget.CoordinatorLayout.Behavior.a(android.support.design.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
      android.support.design.widget.CoordinatorLayout.Behavior.a(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean
      android.support.design.widget.CoordinatorLayout.Behavior.a(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View):boolean
      android.support.design.widget.bd.a(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean */
    public final /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) view;
        if (coordinatorLayout.a(snackbarLayout, (int) motionEvent.getX(), (int) motionEvent.getY())) {
            switch (motionEvent.getActionMasked()) {
                case 0:
                    aw.a().d(this.a.d);
                    break;
                case 1:
                case 3:
                    aw.a().e(this.a.d);
                    break;
            }
        }
        return super.a(coordinatorLayout, (View) snackbarLayout, motionEvent);
    }
}
