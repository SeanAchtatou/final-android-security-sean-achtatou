package android.support.design.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.e;
import android.support.design.h;
import android.support.design.i;
import android.support.v4.view.bx;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

@p(a = y.class)
public class FloatingActionButton extends ImageView {
    private ColorStateList a;
    private PorterDuff.Mode b;
    private int c;
    private int d;
    private int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public final Rect g;
    private final ag h;

    public FloatingActionButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public FloatingActionButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        PorterDuff.Mode mode;
        this.g = new Rect();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.M, i, h.Widget_Design_FloatingActionButton);
        Drawable drawable = obtainStyledAttributes.getDrawable(i.N);
        this.a = obtainStyledAttributes.getColorStateList(i.O);
        switch (obtainStyledAttributes.getInt(i.P, -1)) {
            case 3:
                mode = PorterDuff.Mode.SRC_OVER;
                break;
            case 5:
                mode = PorterDuff.Mode.SRC_IN;
                break;
            case 9:
                mode = PorterDuff.Mode.SRC_ATOP;
                break;
            case 14:
                mode = PorterDuff.Mode.MULTIPLY;
                break;
            case 15:
                mode = PorterDuff.Mode.SCREEN;
                break;
            default:
                mode = null;
                break;
        }
        this.b = mode;
        this.d = obtainStyledAttributes.getColor(i.U, 0);
        this.e = obtainStyledAttributes.getInt(i.S, 0);
        this.c = obtainStyledAttributes.getDimensionPixelSize(i.Q, 0);
        float dimension = obtainStyledAttributes.getDimension(i.R, 0.0f);
        float dimension2 = obtainStyledAttributes.getDimension(i.T, 0.0f);
        obtainStyledAttributes.recycle();
        x xVar = new x(this);
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21) {
            this.h = new ah(this, xVar);
        } else if (i2 >= 12) {
            this.h = new ae(this, xVar);
        } else {
            this.h = new z(this, xVar);
        }
        this.f = (c() - ((int) getResources().getDimension(e.fab_content_size))) / 2;
        this.h.a(drawable, this.a, this.b, this.d, this.c);
        this.h.a(dimension);
        this.h.b(dimension2);
        setClickable(true);
    }

    private static int a(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                return Math.min(i, size);
            case 0:
            default:
                return i;
            case 1073741824:
                return size;
        }
    }

    public final void a() {
        if (getVisibility() != 0) {
            setVisibility(0);
            if (bx.F(this)) {
                this.h.c();
            }
        }
    }

    public final void b() {
        if (getVisibility() == 0) {
            if (!bx.F(this) || isInEditMode()) {
                setVisibility(8);
            } else {
                this.h.b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        switch (this.e) {
            case 1:
                return getResources().getDimensionPixelSize(e.fab_size_mini);
            default:
                return getResources().getDimensionPixelSize(e.fab_size_normal);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        this.h.a(getDrawableState());
    }

    public ColorStateList getBackgroundTintList() {
        return this.a;
    }

    public PorterDuff.Mode getBackgroundTintMode() {
        return this.b;
    }

    @TargetApi(11)
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        this.h.a();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int c2 = c();
        int min = Math.min(a(c2, i), a(c2, i2));
        setMeasuredDimension(this.g.left + min + this.g.right, min + this.g.top + this.g.bottom);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        if (this.h != null) {
            this.h.a(drawable, this.a, this.b, this.d, this.c);
        }
    }

    public void setBackgroundTintList(ColorStateList colorStateList) {
        this.h.a(colorStateList);
    }

    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        this.h.a(mode);
    }
}
