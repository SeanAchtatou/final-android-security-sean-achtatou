package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.support.design.h;
import android.support.design.i;
import android.support.v4.view.bx;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TextInputLayout extends LinearLayout {
    /* access modifiers changed from: private */
    public EditText a;
    private CharSequence b;
    private boolean c;
    /* access modifiers changed from: private */
    public TextView d;
    private int e;
    private int f;
    private int g;
    /* access modifiers changed from: private */
    public final j h = new j(this);
    /* access modifiers changed from: private */
    public final Handler i = new Handler(new bi(this));
    private bn j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, int):void
     arg types: [android.support.design.widget.TextInputLayout, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, float):void
      android.support.v4.view.bx.c(android.view.View, int):void */
    public TextInputLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOrientation(1);
        setWillNotDraw(false);
        this.h.a(a.b);
        this.h.b(new AccelerateInterpolator());
        this.h.b();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.aS, 0, h.Widget_Design_TextInputLayout);
        this.b = obtainStyledAttributes.getText(i.aT);
        int resourceId = obtainStyledAttributes.getResourceId(i.aW, -1);
        if (resourceId != -1) {
            this.h.c(resourceId);
        }
        this.e = obtainStyledAttributes.getResourceId(i.aV, 0);
        boolean z = obtainStyledAttributes.getBoolean(i.aU, false);
        TypedValue typedValue = new TypedValue();
        this.f = getContext().getTheme().resolveAttribute(16842906, typedValue, true) ? typedValue.data : -65281;
        this.g = this.h.g();
        this.h.a(this.f);
        this.h.b(this.f);
        obtainStyledAttributes.recycle();
        if (z && !this.c) {
            this.d = new TextView(getContext());
            this.d.setTextAppearance(getContext(), this.e);
            this.d.setVisibility(4);
            addView(this.d);
            if (this.a != null) {
                bx.b(this.d, bx.n(this.a), 0, bx.o(this.a), this.a.getPaddingBottom());
            }
            this.c = true;
        }
        if (bx.e(this) == 0) {
            bx.c((View) this, 1);
        }
        bx.a(this, new bm(this, (byte) 0));
    }

    private void a(float f2) {
        if (this.j == null) {
            this.j = cd.a();
            this.j.a(a.a);
            this.j.a(200);
            this.j.a(new bl(this));
        } else if (this.j.b()) {
            this.j.e();
        }
        this.j.a(this.h.c(), f2);
        this.j.a();
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        boolean z2 = !TextUtils.isEmpty(this.a.getText());
        boolean isFocused = this.a.isFocused();
        this.h.b(this.f);
        this.h.a(isFocused ? this.g : this.f);
        if (z2 || isFocused) {
            if (z) {
                a(1.0f);
            } else {
                this.h.b(1.0f);
            }
        } else if (z) {
            a(0.0f);
        } else {
            this.h.b(0.0f);
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (view instanceof EditText) {
            EditText editText = (EditText) view;
            if (this.a != null) {
                throw new IllegalArgumentException("We already have an EditText, can only have one");
            }
            this.a = editText;
            this.h.a(this.a.getTextSize());
            this.a.addTextChangedListener(new bj(this));
            this.f = this.a.getHintTextColors().getDefaultColor();
            this.a.setOnFocusChangeListener(new bk(this));
            if (TextUtils.isEmpty(this.b)) {
                CharSequence hint = this.a.getHint();
                this.b = hint;
                this.h.a(hint);
                sendAccessibilityEvent(2048);
                this.a.setHint((CharSequence) null);
            }
            if (this.d != null) {
                bx.b(this.d, bx.n(this.a), 0, bx.o(this.a), this.a.getPaddingBottom());
            }
            a(false);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(layoutParams);
            Paint paint = new Paint();
            paint.setTextSize(this.h.d());
            layoutParams2.topMargin = (int) (-paint.ascent());
            super.addView(view, 0, layoutParams2);
            return;
        }
        super.addView(view, i2, layoutParams);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        this.h.a(canvas);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (this.a != null) {
            int left = this.a.getLeft() + this.a.getCompoundPaddingLeft();
            int right = this.a.getRight() - this.a.getCompoundPaddingRight();
            this.h.a(left, this.a.getTop() + this.a.getCompoundPaddingTop(), right, this.a.getBottom() - this.a.getCompoundPaddingBottom());
            this.h.b(left, getPaddingTop(), right, (i5 - i3) - getPaddingBottom());
            this.h.e();
        }
    }
}
