package android.support.design.widget;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.design.d;
import android.support.v7.b.a.a;

final class al extends a {
    static final double a = Math.cos(Math.toRadians(45.0d));
    final Paint b;
    final Paint c;
    final RectF d;
    float e;
    Path f;
    float g;
    float h;
    float i;
    float j;
    private boolean k = true;
    private final int l;
    private final int m;
    private final int n;
    private boolean o = true;
    private boolean p = false;

    public al(Resources resources, Drawable drawable, float f2, float f3, float f4) {
        super(drawable);
        this.l = resources.getColor(d.shadow_start_color);
        this.m = resources.getColor(d.shadow_mid_color);
        this.n = resources.getColor(d.shadow_end_color);
        this.b = new Paint(5);
        this.b.setStyle(Paint.Style.FILL);
        this.e = (float) Math.round(f2);
        this.d = new RectF();
        this.c = new Paint(this.b);
        this.c.setAntiAlias(false);
        a(f3, f4);
    }

    private static int a(float f2) {
        int round = Math.round(f2);
        return round % 2 == 1 ? round - 1 : round;
    }

    public final void a() {
        this.o = false;
        invalidateSelf();
    }

    /* access modifiers changed from: package-private */
    public final void a(float f2, float f3) {
        if (f2 < 0.0f || f3 < 0.0f) {
            throw new IllegalArgumentException("invalid shadow size");
        }
        float a2 = (float) a(f2);
        float a3 = (float) a(f3);
        if (a2 > a3) {
            if (!this.p) {
                this.p = true;
            }
            a2 = a3;
        }
        if (this.j != a2 || this.h != a3) {
            this.j = a2;
            this.h = a3;
            this.i = (float) Math.round(a2 * 1.5f);
            this.g = a3;
            this.k = true;
            invalidateSelf();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    public final void draw(Canvas canvas) {
        if (this.k) {
            Rect bounds = getBounds();
            float f2 = this.h * 1.5f;
            this.d.set(((float) bounds.left) + this.h, ((float) bounds.top) + f2, ((float) bounds.right) - this.h, ((float) bounds.bottom) - f2);
            b().setBounds((int) this.d.left, (int) this.d.top, (int) this.d.right, (int) this.d.bottom);
            RectF rectF = new RectF(-this.e, -this.e, this.e, this.e);
            RectF rectF2 = new RectF(rectF);
            rectF2.inset(-this.i, -this.i);
            if (this.f == null) {
                this.f = new Path();
            } else {
                this.f.reset();
            }
            this.f.setFillType(Path.FillType.EVEN_ODD);
            this.f.moveTo(-this.e, 0.0f);
            this.f.rLineTo(-this.i, 0.0f);
            this.f.arcTo(rectF2, 180.0f, 90.0f, false);
            this.f.arcTo(rectF, 270.0f, -90.0f, false);
            this.f.close();
            float f3 = -rectF2.top;
            if (f3 > 0.0f) {
                float f4 = this.e / f3;
                this.b.setShader(new RadialGradient(0.0f, 0.0f, f3, new int[]{0, this.l, this.m, this.n}, new float[]{0.0f, f4, f4 + ((1.0f - f4) / 2.0f), 1.0f}, Shader.TileMode.CLAMP));
            }
            this.c.setShader(new LinearGradient(0.0f, rectF.top, 0.0f, rectF2.top, new int[]{this.l, this.m, this.n}, new float[]{0.0f, 0.5f, 1.0f}, Shader.TileMode.CLAMP));
            this.c.setAntiAlias(false);
            this.k = false;
        }
        float f5 = (-this.e) - this.i;
        float f6 = this.e;
        boolean z = this.d.width() - (2.0f * f6) > 0.0f;
        boolean z2 = this.d.height() - (2.0f * f6) > 0.0f;
        float f7 = this.j - (this.j * 0.25f);
        float f8 = f6 / ((this.j - (this.j * 0.5f)) + f6);
        float f9 = f6 / (f7 + f6);
        float f10 = f6 / (f6 + (this.j - (this.j * 1.0f)));
        int save = canvas.save();
        canvas.translate(this.d.left + f6, this.d.top + f6);
        canvas.scale(f8, f9);
        canvas.drawPath(this.f, this.b);
        if (z) {
            canvas.scale(1.0f / f8, 1.0f);
            canvas.drawRect(0.0f, f5, this.d.width() - (2.0f * f6), -this.e, this.c);
        }
        canvas.restoreToCount(save);
        int save2 = canvas.save();
        canvas.translate(this.d.right - f6, this.d.bottom - f6);
        canvas.scale(f8, f10);
        canvas.rotate(180.0f);
        canvas.drawPath(this.f, this.b);
        if (z) {
            canvas.scale(1.0f / f8, 1.0f);
            canvas.drawRect(0.0f, f5, this.d.width() - (2.0f * f6), this.i + (-this.e), this.c);
        }
        canvas.restoreToCount(save2);
        int save3 = canvas.save();
        canvas.translate(this.d.left + f6, this.d.bottom - f6);
        canvas.scale(f8, f10);
        canvas.rotate(270.0f);
        canvas.drawPath(this.f, this.b);
        if (z2) {
            canvas.scale(1.0f / f10, 1.0f);
            canvas.drawRect(0.0f, f5, this.d.height() - (2.0f * f6), -this.e, this.c);
        }
        canvas.restoreToCount(save3);
        int save4 = canvas.save();
        canvas.translate(this.d.right - f6, this.d.top + f6);
        canvas.scale(f8, f9);
        canvas.rotate(90.0f);
        canvas.drawPath(this.f, this.b);
        if (z2) {
            canvas.scale(1.0f / f9, 1.0f);
            canvas.drawRect(0.0f, f5, this.d.height() - (2.0f * f6), -this.e, this.c);
        }
        canvas.restoreToCount(save4);
        super.draw(canvas);
    }

    public final int getOpacity() {
        return -3;
    }

    public final boolean getPadding(Rect rect) {
        float f2;
        float f3 = this.h;
        float f4 = this.e;
        if (this.o) {
            f2 = (float) ((((double) f4) * (1.0d - a)) + ((double) (f3 * 1.5f)));
        } else {
            f2 = f3 * 1.5f;
        }
        int ceil = (int) Math.ceil((double) f2);
        float f5 = this.h;
        float f6 = this.e;
        if (this.o) {
            f5 = (float) ((((double) f6) * (1.0d - a)) + ((double) f5));
        }
        int ceil2 = (int) Math.ceil((double) f5);
        rect.set(ceil2, ceil, ceil2, ceil);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void onBoundsChange(Rect rect) {
        this.k = true;
    }

    public final void setAlpha(int i2) {
        super.setAlpha(i2);
        this.b.setAlpha(i2);
        this.c.setAlpha(i2);
    }
}
