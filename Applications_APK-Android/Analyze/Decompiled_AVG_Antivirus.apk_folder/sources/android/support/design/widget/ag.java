package android.support.design.widget;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.design.d;
import android.view.View;

abstract class ag {
    static final int[] b = {16842919, 16842910};
    static final int[] c = {16842908, 16842910};
    static final int[] d = new int[0];
    final View e;
    final am f;

    ag(View view, am amVar) {
        this.e = view;
        this.f = amVar;
    }

    /* access modifiers changed from: package-private */
    public final Drawable a(int i, ColorStateList colorStateList) {
        Resources resources = this.e.getResources();
        h d2 = d();
        d2.a(resources.getColor(d.fab_stroke_top_outer_color), resources.getColor(d.fab_stroke_top_inner_color), resources.getColor(d.fab_stroke_end_inner_color), resources.getColor(d.fab_stroke_end_outer_color));
        d2.a((float) i);
        d2.a(colorStateList.getDefaultColor());
        return d2;
    }

    /* access modifiers changed from: package-private */
    public abstract void a();

    /* access modifiers changed from: package-private */
    public abstract void a(float f2);

    /* access modifiers changed from: package-private */
    public abstract void a(ColorStateList colorStateList);

    /* access modifiers changed from: package-private */
    public abstract void a(PorterDuff.Mode mode);

    /* access modifiers changed from: package-private */
    public abstract void a(Drawable drawable, ColorStateList colorStateList, PorterDuff.Mode mode, int i, int i2);

    /* access modifiers changed from: package-private */
    public abstract void a(int[] iArr);

    /* access modifiers changed from: package-private */
    public abstract void b();

    /* access modifiers changed from: package-private */
    public abstract void b(float f2);

    /* access modifiers changed from: package-private */
    public abstract void c();

    /* access modifiers changed from: package-private */
    public h d() {
        return new h();
    }
}
