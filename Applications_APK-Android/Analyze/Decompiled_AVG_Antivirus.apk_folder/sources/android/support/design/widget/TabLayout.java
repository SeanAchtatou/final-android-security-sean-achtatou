package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.support.design.h;
import android.support.design.i;
import android.support.v4.view.bx;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import androidx.core.view.GravityCompat;
import java.util.ArrayList;

public class TabLayout extends HorizontalScrollView {
    private final ArrayList a;
    private final bh b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private ColorStateList h;
    private final int i;
    private final int j;
    private int k;
    private final int l;
    private int m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public int o;

    public TabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TabLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = new ArrayList();
        setHorizontalScrollBarEnabled(false);
        setFillViewport(true);
        this.b = new bh(this, context);
        addView(this.b, -2, -1);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.ay, i2, h.Widget_Design_TabLayout);
        this.b.b(obtainStyledAttributes.getDimensionPixelSize(i.aD, 0));
        this.b.a(obtainStyledAttributes.getColor(i.aC, 0));
        this.g = obtainStyledAttributes.getResourceId(i.aN, h.TextAppearance_Design_Tab);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(i.aH, 0);
        this.f = dimensionPixelSize;
        this.e = dimensionPixelSize;
        this.d = dimensionPixelSize;
        this.c = dimensionPixelSize;
        this.c = obtainStyledAttributes.getDimensionPixelSize(i.aK, this.c);
        this.d = obtainStyledAttributes.getDimensionPixelSize(i.aL, this.d);
        this.e = obtainStyledAttributes.getDimensionPixelSize(i.aJ, this.e);
        this.f = obtainStyledAttributes.getDimensionPixelSize(i.aI, this.f);
        this.h = b(this.g);
        if (obtainStyledAttributes.hasValue(i.aO)) {
            this.h = obtainStyledAttributes.getColorStateList(i.aO);
        }
        if (obtainStyledAttributes.hasValue(i.aM)) {
            this.h = new ColorStateList(new int[][]{SELECTED_STATE_SET, EMPTY_STATE_SET}, new int[]{obtainStyledAttributes.getColor(i.aM, 0), this.h.getDefaultColor()});
        }
        this.j = obtainStyledAttributes.getDimensionPixelSize(i.aF, 0);
        this.l = obtainStyledAttributes.getDimensionPixelSize(i.aE, 0);
        this.i = obtainStyledAttributes.getResourceId(i.az, 0);
        this.m = obtainStyledAttributes.getDimensionPixelSize(i.aA, 0);
        this.o = obtainStyledAttributes.getInt(i.aG, 1);
        this.n = obtainStyledAttributes.getInt(i.aB, 0);
        obtainStyledAttributes.recycle();
        bx.b(this.b, this.o == 0 ? Math.max(0, this.m - this.c) : 0, 0, 0, 0);
        switch (this.o) {
            case 0:
                this.b.setGravity(GravityCompat.START);
                break;
            case 1:
                this.b.setGravity(1);
                break;
        }
        a();
    }

    /* access modifiers changed from: private */
    public int a(int i2) {
        return Math.round(getResources().getDisplayMetrics().density * ((float) i2));
    }

    /* access modifiers changed from: private */
    public void a() {
        for (int i2 = 0; i2 < this.b.getChildCount(); i2++) {
            View childAt = this.b.getChildAt(i2);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) childAt.getLayoutParams();
            if (this.o == 1 && this.n == 0) {
                layoutParams.width = 0;
                layoutParams.weight = 1.0f;
            } else {
                layoutParams.width = -2;
                layoutParams.weight = 0.0f;
            }
            childAt.requestLayout();
        }
    }

    private ColorStateList b(int i2) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(i2, i.aP);
        try {
            return obtainStyledAttributes.getColorStateList(i.aQ);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int a2 = a(48) + getPaddingTop() + getPaddingBottom();
        switch (View.MeasureSpec.getMode(i3)) {
            case Integer.MIN_VALUE:
                i3 = View.MeasureSpec.makeMeasureSpec(Math.min(a2, View.MeasureSpec.getSize(i3)), 1073741824);
                break;
            case 0:
                i3 = View.MeasureSpec.makeMeasureSpec(a2, 1073741824);
                break;
        }
        super.onMeasure(i2, i3);
        if (this.o == 1 && getChildCount() == 1) {
            View childAt = getChildAt(0);
            int measuredWidth = getMeasuredWidth();
            if (childAt.getMeasuredWidth() > measuredWidth) {
                childAt.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824), getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom(), childAt.getLayoutParams().height));
            }
        }
        int i4 = this.l;
        int measuredWidth2 = getMeasuredWidth() - a(56);
        if (i4 == 0 || i4 > measuredWidth2) {
            i4 = measuredWidth2;
        }
        this.k = i4;
    }
}
