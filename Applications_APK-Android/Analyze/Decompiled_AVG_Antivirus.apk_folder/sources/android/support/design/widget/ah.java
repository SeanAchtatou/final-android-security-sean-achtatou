package android.support.design.widget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.support.v4.b.a.a;
import android.support.v4.view.bx;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

@TargetApi(21)
final class ah extends ae {
    private Drawable g;
    private RippleDrawable h;
    private Drawable i;
    private Interpolator j;

    ah(View view, am amVar) {
        super(view, amVar);
        if (!view.isInEditMode()) {
            this.j = AnimationUtils.loadInterpolator(this.e.getContext(), 17563661);
        }
    }

    private Animator a(Animator animator) {
        animator.setInterpolator(this.j);
        return animator;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
    }

    public final void a(float f) {
        bx.f(this.e, f);
    }

    /* access modifiers changed from: package-private */
    public final void a(ColorStateList colorStateList) {
        a.a(this.g, colorStateList);
        if (this.i != null) {
            a.a(this.i, colorStateList);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(PorterDuff.Mode mode) {
        a.a(this.g, mode);
    }

    /* access modifiers changed from: package-private */
    public final void a(Drawable drawable, ColorStateList colorStateList, PorterDuff.Mode mode, int i2, int i3) {
        Drawable drawable2;
        this.g = a.c(drawable);
        a.a(this.g, colorStateList);
        if (mode != null) {
            a.a(this.g, mode);
        }
        if (i3 > 0) {
            this.i = a(i3, colorStateList);
            drawable2 = new LayerDrawable(new Drawable[]{this.i, this.g});
        } else {
            this.i = null;
            drawable2 = this.g;
        }
        this.h = new RippleDrawable(ColorStateList.valueOf(i2), drawable2, null);
        this.f.a(this.h);
        this.f.a(0, 0, 0, 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(int[] iArr) {
    }

    /* access modifiers changed from: package-private */
    public final void b(float f) {
        StateListAnimator stateListAnimator = new StateListAnimator();
        stateListAnimator.addState(b, a(ObjectAnimator.ofFloat(this.e, "translationZ", f)));
        stateListAnimator.addState(c, a(ObjectAnimator.ofFloat(this.e, "translationZ", f)));
        stateListAnimator.addState(d, a(ObjectAnimator.ofFloat(this.e, "translationZ", 0.0f)));
        this.e.setStateListAnimator(stateListAnimator);
    }

    /* access modifiers changed from: package-private */
    public final h d() {
        return new i();
    }
}
