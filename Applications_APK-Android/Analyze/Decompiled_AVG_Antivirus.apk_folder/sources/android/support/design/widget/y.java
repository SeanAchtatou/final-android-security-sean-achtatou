package android.support.design.widget;

import android.graphics.Rect;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.bx;
import android.support.v4.view.el;
import android.view.View;
import java.util.List;

public final class y extends CoordinatorLayout.Behavior {
    private static final boolean a = (Build.VERSION.SDK_INT >= 11);
    private Rect b;
    private float c;

    private boolean a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, FloatingActionButton floatingActionButton) {
        if (((r) floatingActionButton.getLayoutParams()).f != appBarLayout.getId()) {
            return false;
        }
        if (this.b == null) {
            this.b = new Rect();
        }
        Rect rect = this.b;
        bx.a(coordinatorLayout, appBarLayout, rect);
        if (rect.bottom <= appBarLayout.f()) {
            floatingActionButton.b();
        } else {
            floatingActionButton.a();
        }
        return true;
    }

    public final /* synthetic */ void a(View view, View view2) {
        FloatingActionButton floatingActionButton = (FloatingActionButton) view;
        if (view2 instanceof Snackbar.SnackbarLayout) {
            bx.t(floatingActionButton).c(0.0f).a(a.b).a((el) null);
        }
    }

    public final /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, int i) {
        int i2 = 0;
        FloatingActionButton floatingActionButton = (FloatingActionButton) view;
        List b2 = coordinatorLayout.b(floatingActionButton);
        int size = b2.size();
        for (int i3 = 0; i3 < size; i3++) {
            View view2 = (View) b2.get(i3);
            if ((view2 instanceof AppBarLayout) && a(coordinatorLayout, (AppBarLayout) view2, floatingActionButton)) {
                break;
            }
        }
        coordinatorLayout.a(floatingActionButton, i);
        Rect a2 = floatingActionButton.g;
        if (a2 == null || a2.centerX() <= 0 || a2.centerY() <= 0) {
            return true;
        }
        r rVar = (r) floatingActionButton.getLayoutParams();
        int i4 = floatingActionButton.getRight() >= coordinatorLayout.getWidth() - rVar.rightMargin ? a2.right : floatingActionButton.getLeft() <= rVar.leftMargin ? -a2.left : 0;
        if (floatingActionButton.getBottom() >= coordinatorLayout.getBottom() - rVar.bottomMargin) {
            i2 = a2.bottom;
        } else if (floatingActionButton.getTop() <= rVar.topMargin) {
            i2 = -a2.top;
        }
        floatingActionButton.offsetTopAndBottom(i2);
        floatingActionButton.offsetLeftAndRight(i4);
        return true;
    }

    public final /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, View view2) {
        FloatingActionButton floatingActionButton = (FloatingActionButton) view;
        if (view2 instanceof Snackbar.SnackbarLayout) {
            if (floatingActionButton.getVisibility() == 0) {
                float f = 0.0f;
                List b2 = coordinatorLayout.b(floatingActionButton);
                int size = b2.size();
                int i = 0;
                while (i < size) {
                    View view3 = (View) b2.get(i);
                    i++;
                    f = (!(view3 instanceof Snackbar.SnackbarLayout) || !coordinatorLayout.a(floatingActionButton, view3)) ? f : Math.min(f, bx.q(view3) - ((float) view3.getHeight()));
                }
                if (f != this.c) {
                    bx.t(floatingActionButton).b();
                    bx.b(floatingActionButton, f);
                    this.c = f;
                }
            }
        } else if (view2 instanceof AppBarLayout) {
            a(coordinatorLayout, (AppBarLayout) view2, floatingActionButton);
        }
        return false;
    }

    public final /* bridge */ /* synthetic */ boolean a(View view) {
        return a && (view instanceof Snackbar.SnackbarLayout);
    }
}
