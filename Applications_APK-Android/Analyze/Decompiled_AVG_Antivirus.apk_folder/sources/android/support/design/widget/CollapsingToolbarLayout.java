package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.design.f;
import android.support.design.h;
import android.support.design.i;
import android.support.v4.view.bx;
import android.support.v4.view.eo;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;

public class CollapsingToolbarLayout extends FrameLayout {
    private boolean a;
    private int b;
    private Toolbar c;
    private View d;
    private int e;
    private int f;
    private int g;
    private int h;
    private final Rect i;
    /* access modifiers changed from: private */
    public final j j;
    /* access modifiers changed from: private */
    public Drawable k;
    /* access modifiers changed from: private */
    public Drawable l;
    private int m;
    private boolean n;
    private bn o;
    private g p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public eo r;

    public class LayoutParams extends FrameLayout.LayoutParams {
        int a = 0;
        float b = 0.5f;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.o);
            this.a = obtainStyledAttributes.getInt(i.p, 0);
            this.b = obtainStyledAttributes.getFloat(i.q, 0.5f);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(FrameLayout.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public CollapsingToolbarLayout(Context context) {
        this(context, null);
    }

    public CollapsingToolbarLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CollapsingToolbarLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        boolean z = true;
        this.a = true;
        this.i = new Rect();
        this.j = new j(this);
        this.j.a();
        this.j.a(a.c);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.r, i2, h.Widget_Design_CollapsingToolbar);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(i.u, 0);
        this.h = dimensionPixelSize;
        this.g = dimensionPixelSize;
        this.f = dimensionPixelSize;
        this.e = dimensionPixelSize;
        z = bx.h(this) != 1 ? false : z;
        if (obtainStyledAttributes.hasValue(i.x)) {
            int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(i.x, 0);
            if (z) {
                this.g = dimensionPixelSize2;
            } else {
                this.e = dimensionPixelSize2;
            }
        }
        if (obtainStyledAttributes.hasValue(i.w)) {
            int dimensionPixelSize3 = obtainStyledAttributes.getDimensionPixelSize(i.w, 0);
            if (z) {
                this.e = dimensionPixelSize3;
            } else {
                this.g = dimensionPixelSize3;
            }
        }
        if (obtainStyledAttributes.hasValue(i.y)) {
            this.f = obtainStyledAttributes.getDimensionPixelSize(i.y, 0);
        }
        if (obtainStyledAttributes.hasValue(i.v)) {
            this.h = obtainStyledAttributes.getDimensionPixelSize(i.v, 0);
        }
        this.j.d(obtainStyledAttributes.getResourceId(i.z, h.TextAppearance_AppCompat_Title));
        this.j.c(obtainStyledAttributes.getResourceId(i.s, h.TextAppearance_AppCompat_Widget_ActionBar_Title));
        Drawable drawable = obtainStyledAttributes.getDrawable(i.t);
        if (this.k != drawable) {
            if (this.k != null) {
                this.k.setCallback(null);
            }
            this.k = drawable;
            drawable.setBounds(0, 0, getWidth(), getHeight());
            drawable.setCallback(this);
            drawable.mutate().setAlpha(this.m);
            bx.d(this);
        }
        Drawable drawable2 = obtainStyledAttributes.getDrawable(i.A);
        if (this.l != drawable2) {
            if (this.l != null) {
                this.l.setCallback(null);
            }
            this.l = drawable2;
            drawable2.setCallback(this);
            drawable2.mutate().setAlpha(this.m);
            bx.d(this);
        }
        this.b = obtainStyledAttributes.getResourceId(i.B, -1);
        obtainStyledAttributes.recycle();
        setWillNotDraw(false);
        bx.a(this, new k(this));
    }

    private void a() {
        Toolbar toolbar;
        Toolbar toolbar2;
        if (this.a) {
            int childCount = getChildCount();
            int i2 = 0;
            Toolbar toolbar3 = null;
            while (true) {
                if (i2 >= childCount) {
                    toolbar = null;
                    break;
                }
                View childAt = getChildAt(i2);
                if (childAt instanceof Toolbar) {
                    if (this.b == -1) {
                        toolbar = (Toolbar) childAt;
                        break;
                    } else if (this.b == childAt.getId()) {
                        toolbar = (Toolbar) childAt;
                        break;
                    } else if (toolbar3 == null) {
                        toolbar2 = (Toolbar) childAt;
                        i2++;
                        toolbar3 = toolbar2;
                    }
                }
                toolbar2 = toolbar3;
                i2++;
                toolbar3 = toolbar2;
            }
            if (toolbar != null) {
                toolbar3 = toolbar;
            }
            if (toolbar3 != null) {
                this.c = toolbar3;
                this.d = new View(getContext());
                this.c.addView(this.d, -1, -1);
            } else {
                this.c = null;
                this.d = null;
            }
            this.a = false;
        }
    }

    private void a(int i2) {
        a();
        if (this.o == null) {
            this.o = cd.a();
            this.o.a(600);
            this.o.a(a.b);
            this.o.a(new l(this));
        } else if (this.o.b()) {
            this.o.e();
        }
        this.o.a(this.m, i2);
        this.o.a();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(super.generateDefaultLayoutParams());
    }

    /* access modifiers changed from: private */
    public static cc b(View view) {
        cc ccVar = (cc) view.getTag(f.view_offset_helper);
        if (ccVar != null) {
            return ccVar;
        }
        cc ccVar2 = new cc(view);
        view.setTag(f.view_offset_helper, ccVar2);
        return ccVar2;
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        if (i2 != this.m) {
            if (!(this.k == null || this.c == null)) {
                bx.d(this.c);
            }
            this.m = i2;
            bx.d(this);
        }
    }

    static /* synthetic */ void d(CollapsingToolbarLayout collapsingToolbarLayout) {
        if (!collapsingToolbarLayout.n) {
            if (!bx.F(collapsingToolbarLayout) || collapsingToolbarLayout.isInEditMode()) {
                collapsingToolbarLayout.b(255);
            } else {
                collapsingToolbarLayout.a(255);
            }
            collapsingToolbarLayout.n = true;
        }
    }

    static /* synthetic */ void e(CollapsingToolbarLayout collapsingToolbarLayout) {
        if (collapsingToolbarLayout.n) {
            if (!bx.F(collapsingToolbarLayout) || collapsingToolbarLayout.isInEditMode()) {
                collapsingToolbarLayout.b(0);
            } else {
                collapsingToolbarLayout.a(0);
            }
            collapsingToolbarLayout.n = false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        a();
        if (this.c == null && this.k != null && this.m > 0) {
            this.k.mutate().setAlpha(this.m);
            this.k.draw(canvas);
        }
        this.j.a(canvas);
        if (this.l != null && this.m > 0) {
            int b2 = this.r != null ? this.r.b() : 0;
            if (b2 > 0) {
                this.l.setBounds(0, -this.q, getWidth(), b2 - this.q);
                this.l.mutate().setAlpha(this.m);
                this.l.draw(canvas);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        a();
        if (view == this.c && this.k != null && this.m > 0) {
            this.k.mutate().setAlpha(this.m);
            this.k.draw(canvas);
        }
        return super.drawChild(canvas, view, j2);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (parent instanceof AppBarLayout) {
            if (this.p == null) {
                this.p = new m(this, (byte) 0);
            }
            ((AppBarLayout) parent).a(this.p);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        ViewParent parent = getParent();
        if (this.p != null && (parent instanceof AppBarLayout)) {
            ((AppBarLayout) parent).b(this.p);
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int b2;
        super.onLayout(z, i2, i3, i4, i5);
        int childCount = getChildCount();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (this.r != null && !bx.y(childAt) && childAt.getTop() < (b2 = this.r.b())) {
                childAt.offsetTopAndBottom(b2);
            }
            b(childAt).a();
        }
        if (this.d != null) {
            bx.a(this, this.d, this.i);
            this.j.b(this.i.left, i5 - this.i.height(), this.i.right, i5);
            this.j.a(this.e + i2, this.i.bottom + this.f, i4 - this.g, i5 - this.h);
            this.j.e();
        }
        if (this.c != null) {
            setMinimumHeight(this.c.getHeight());
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        a();
        super.onMeasure(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (this.k != null) {
            this.k.setBounds(0, 0, i2, i3);
        }
    }
}
