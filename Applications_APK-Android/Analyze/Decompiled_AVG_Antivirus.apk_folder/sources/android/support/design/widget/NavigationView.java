package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.c;
import android.support.design.h;
import android.support.design.internal.ScrimInsetsFrameLayout;
import android.support.design.internal.a;
import android.support.v4.view.bx;
import android.support.v7.internal.view.f;
import android.support.v7.internal.view.menu.i;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MenuInflater;
import android.view.View;

public class NavigationView extends ScrimInsetsFrameLayout {
    private static final int[] a = {16842912};
    private static final int[] b = {-16842910};
    private final i c;
    private final a d;
    /* access modifiers changed from: private */
    public aj e;
    private int f;
    private MenuInflater g;

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new ak();
        public Bundle a;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readBundle();
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeBundle(this.a);
        }
    }

    public NavigationView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.a(android.view.View, boolean):void
     arg types: [android.support.design.widget.NavigationView, boolean]
     candidates:
      android.support.v4.view.bx.a(int, int):int
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.a(android.view.View, float):void
      android.support.v4.view.bx.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.bi):void
      android.support.v4.view.bx.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.bx.a(android.view.View, int):boolean
      android.support.v4.view.bx.a(android.view.View, boolean):void */
    public NavigationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.c = new i(context);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, android.support.design.i.ab, i, h.Widget_Design_NavigationView);
        setBackgroundDrawable(obtainStyledAttributes.getDrawable(android.support.design.i.ac));
        if (obtainStyledAttributes.hasValue(android.support.design.i.af)) {
            bx.f(this, (float) obtainStyledAttributes.getDimensionPixelSize(android.support.design.i.af, 0));
        }
        bx.a((View) this, obtainStyledAttributes.getBoolean(android.support.design.i.ad, false));
        this.f = obtainStyledAttributes.getDimensionPixelSize(android.support.design.i.ae, 0);
        ColorStateList colorStateList = obtainStyledAttributes.hasValue(android.support.design.i.ai) ? obtainStyledAttributes.getColorStateList(android.support.design.i.ai) : a(16842808);
        ColorStateList colorStateList2 = obtainStyledAttributes.hasValue(android.support.design.i.aj) ? obtainStyledAttributes.getColorStateList(android.support.design.i.aj) : a(16842806);
        Drawable drawable = obtainStyledAttributes.getDrawable(android.support.design.i.ah);
        if (obtainStyledAttributes.hasValue(android.support.design.i.ak)) {
            int resourceId = obtainStyledAttributes.getResourceId(android.support.design.i.ak, 0);
            if (this.d != null) {
                this.d.b(true);
            }
            if (this.g == null) {
                this.g = new f(getContext());
            }
            this.g.inflate(resourceId, this.c);
            if (this.d != null) {
                this.d.b(false);
                this.d.a(false);
            }
        }
        this.c.a(new ai(this));
        this.d = new a();
        this.d.c();
        this.d.a(context, this.c);
        this.d.a(colorStateList);
        this.d.b(colorStateList2);
        this.d.a(drawable);
        this.c.a(this.d);
        addView((View) this.d.a(this));
        if (obtainStyledAttributes.hasValue(android.support.design.i.ag)) {
            this.d.a(obtainStyledAttributes.getResourceId(android.support.design.i.ag, 0));
        }
        obtainStyledAttributes.recycle();
    }

    private ColorStateList a(int i) {
        TypedValue typedValue = new TypedValue();
        if (!getContext().getTheme().resolveAttribute(i, typedValue, true)) {
            return null;
        }
        ColorStateList colorStateList = getResources().getColorStateList(typedValue.resourceId);
        if (!getContext().getTheme().resolveAttribute(c.colorPrimary, typedValue, true)) {
            return null;
        }
        int i2 = typedValue.data;
        int defaultColor = colorStateList.getDefaultColor();
        return new ColorStateList(new int[][]{b, a, EMPTY_STATE_SET}, new int[]{colorStateList.getColorForState(b, defaultColor), i2, defaultColor});
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        switch (View.MeasureSpec.getMode(i)) {
            case Integer.MIN_VALUE:
                i = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(i), this.f), 1073741824);
                break;
            case 0:
                i = View.MeasureSpec.makeMeasureSpec(this.f, 1073741824);
                break;
        }
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.c.b(savedState.a);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = new Bundle();
        this.c.a(savedState.a);
        return savedState;
    }
}
