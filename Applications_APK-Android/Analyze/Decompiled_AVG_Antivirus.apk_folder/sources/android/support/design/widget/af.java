package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class af extends AnimatorListenerAdapter {
    final /* synthetic */ ae a;

    af(ae aeVar) {
        this.a = aeVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.ae.a(android.support.design.widget.ae, boolean):boolean
     arg types: [android.support.design.widget.ae, int]
     candidates:
      android.support.design.widget.ag.a(int, android.content.res.ColorStateList):android.graphics.drawable.Drawable
      android.support.design.widget.ae.a(android.support.design.widget.ae, boolean):boolean */
    public final void onAnimationCancel(Animator animator) {
        boolean unused = this.a.g = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.ae.a(android.support.design.widget.ae, boolean):boolean
     arg types: [android.support.design.widget.ae, int]
     candidates:
      android.support.design.widget.ag.a(int, android.content.res.ColorStateList):android.graphics.drawable.Drawable
      android.support.design.widget.ae.a(android.support.design.widget.ae, boolean):boolean */
    public final void onAnimationEnd(Animator animator) {
        boolean unused = this.a.g = false;
        this.a.e.setVisibility(8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.ae.a(android.support.design.widget.ae, boolean):boolean
     arg types: [android.support.design.widget.ae, int]
     candidates:
      android.support.design.widget.ag.a(int, android.content.res.ColorStateList):android.graphics.drawable.Drawable
      android.support.design.widget.ae.a(android.support.design.widget.ae, boolean):boolean */
    public final void onAnimationStart(Animator animator) {
        boolean unused = this.a.g = true;
    }
}
