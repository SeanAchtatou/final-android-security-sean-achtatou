package android.support.design.widget;

import android.support.v4.view.bi;
import android.support.v4.view.eo;
import android.view.View;

final class k implements bi {
    final /* synthetic */ CollapsingToolbarLayout a;

    k(CollapsingToolbarLayout collapsingToolbarLayout) {
        this.a = collapsingToolbarLayout;
    }

    public final eo a(View view, eo eoVar) {
        eo unused = this.a.r = eoVar;
        this.a.requestLayout();
        return eoVar.f();
    }
}
