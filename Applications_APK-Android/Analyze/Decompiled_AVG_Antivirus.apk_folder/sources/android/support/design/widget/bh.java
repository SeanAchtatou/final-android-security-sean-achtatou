package android.support.design.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.view.bx;
import android.view.View;
import android.widget.LinearLayout;

final class bh extends LinearLayout {
    final /* synthetic */ TabLayout a;
    private int b;
    private final Paint c;
    private int d = -1;
    private float e;
    private int f = -1;
    private int g = -1;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bh(TabLayout tabLayout, Context context) {
        super(context);
        this.a = tabLayout;
        setWillNotDraw(false);
        this.c = new Paint();
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        this.c.setColor(i);
        bx.d(this);
    }

    /* access modifiers changed from: package-private */
    public final void b(int i) {
        this.b = i;
        bx.d(this);
    }

    public final void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f >= 0 && this.g > this.f) {
            canvas.drawRect((float) this.f, (float) (getHeight() - this.b), (float) this.g, (float) getHeight(), this.c);
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        super.onLayout(z, i, i2, i3, i4);
        View childAt = getChildAt(this.d);
        if (childAt == null || childAt.getWidth() <= 0) {
            i5 = -1;
            i6 = -1;
        } else {
            i5 = childAt.getLeft();
            i6 = childAt.getRight();
            if (this.e > 0.0f && this.d < getChildCount() - 1) {
                View childAt2 = getChildAt(this.d + 1);
                i5 = (int) ((((float) i5) * (1.0f - this.e)) + (this.e * ((float) childAt2.getLeft())));
                i6 = (int) ((((float) i6) * (1.0f - this.e)) + (((float) childAt2.getRight()) * this.e));
            }
        }
        if (i5 != this.f || i6 != this.g) {
            this.f = i5;
            this.g = i6;
            bx.d(this);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (View.MeasureSpec.getMode(i) == 1073741824 && this.a.o == 1 && this.a.n == 1) {
            int childCount = getChildCount();
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
            int i3 = 0;
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = getChildAt(i4);
                childAt.measure(makeMeasureSpec, i2);
                i3 = Math.max(i3, childAt.getMeasuredWidth());
            }
            if (i3 > 0) {
                if (i3 * childCount <= getMeasuredWidth() - (this.a.a(16) * 2)) {
                    for (int i5 = 0; i5 < childCount; i5++) {
                        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) getChildAt(i5).getLayoutParams();
                        layoutParams.width = i3;
                        layoutParams.weight = 0.0f;
                    }
                } else {
                    int unused = this.a.n = 0;
                    this.a.a();
                }
                super.onMeasure(i, i2);
            }
        }
    }
}
