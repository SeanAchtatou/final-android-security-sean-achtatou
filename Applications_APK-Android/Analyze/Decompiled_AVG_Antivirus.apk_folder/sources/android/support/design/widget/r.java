package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.support.design.i;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public final class r extends ViewGroup.MarginLayoutParams {
    CoordinatorLayout.Behavior a;
    boolean b = false;
    public int c = 0;
    public int d = 0;
    public int e = -1;
    int f = -1;
    View g;
    View h;
    final Rect i = new Rect();
    Object j;
    private boolean k;
    private boolean l;
    private boolean m;

    public r() {
        super(-2, -2);
    }

    r(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.D);
        this.c = obtainStyledAttributes.getInteger(i.E, 0);
        this.f = obtainStyledAttributes.getResourceId(i.F, -1);
        this.d = obtainStyledAttributes.getInteger(i.G, 0);
        this.e = obtainStyledAttributes.getInteger(i.I, -1);
        this.b = obtainStyledAttributes.hasValue(i.H);
        if (this.b) {
            this.a = CoordinatorLayout.a(context, attributeSet, obtainStyledAttributes.getString(i.H));
        }
        obtainStyledAttributes.recycle();
    }

    public r(r rVar) {
        super((ViewGroup.MarginLayoutParams) rVar);
    }

    public r(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public r(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }

    public final void a(CoordinatorLayout.Behavior behavior) {
        if (this.a != behavior) {
            this.a = behavior;
            this.j = null;
            this.b = true;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.l = z;
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        if (this.a == null) {
            this.k = false;
        }
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(View view) {
        return view == this.h || (this.a != null && this.a.a(view));
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        if (this.k) {
            return true;
        }
        boolean z = this.k | false;
        this.k = z;
        return z;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.k = false;
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        this.l = false;
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        this.m = false;
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        this.m = false;
    }
}
