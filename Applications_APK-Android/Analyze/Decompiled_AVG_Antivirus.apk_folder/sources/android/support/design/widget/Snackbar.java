package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.design.b;
import android.support.design.e;
import android.support.design.f;
import android.support.design.g;
import android.support.design.i;
import android.support.v4.view.bx;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

public final class Snackbar {
    private static final Handler a = new Handler(Looper.getMainLooper(), new an());
    private final ViewGroup b;
    /* access modifiers changed from: private */
    public final SnackbarLayout c;
    /* access modifiers changed from: private */
    public final ay d;

    public class SnackbarLayout extends LinearLayout {
        private TextView a;
        private TextView b;
        private int c;
        private int d;
        private av e;

        public SnackbarLayout(Context context) {
            this(context, null);
        }

        public SnackbarLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.as);
            this.c = obtainStyledAttributes.getDimensionPixelSize(i.at, -1);
            this.d = obtainStyledAttributes.getDimensionPixelSize(i.av, -1);
            if (obtainStyledAttributes.hasValue(i.au)) {
                bx.f(this, (float) obtainStyledAttributes.getDimensionPixelSize(i.au, 0));
            }
            obtainStyledAttributes.recycle();
            setClickable(true);
            LayoutInflater.from(context).inflate(g.layout_snackbar_include, this);
        }

        private boolean a(int i, int i2, int i3) {
            boolean z = false;
            if (i != getOrientation()) {
                setOrientation(i);
                z = true;
            }
            if (this.a.getPaddingTop() == i2 && this.a.getPaddingBottom() == i3) {
                return z;
            }
            TextView textView = this.a;
            if (bx.C(textView)) {
                bx.b(textView, bx.n(textView), i2, bx.o(textView), i3);
            } else {
                textView.setPadding(textView.getPaddingLeft(), i2, textView.getPaddingRight(), i3);
            }
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.bx.c(android.view.View, float):void
         arg types: [android.widget.TextView, int]
         candidates:
          android.support.v4.view.bx.c(android.view.View, int):void
          android.support.v4.view.bx.c(android.view.View, float):void */
        /* access modifiers changed from: package-private */
        public final void a() {
            bx.c((View) this.a, 0.0f);
            bx.t(this.a).a(1.0f).a(180L).b(70L).c();
            if (this.b.getVisibility() == 0) {
                bx.c((View) this.b, 0.0f);
                bx.t(this.b).a(1.0f).a(180L).b(70L).c();
            }
        }

        /* access modifiers changed from: package-private */
        public final void a(av avVar) {
            this.e = avVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.bx.c(android.view.View, float):void
         arg types: [android.widget.TextView, int]
         candidates:
          android.support.v4.view.bx.c(android.view.View, int):void
          android.support.v4.view.bx.c(android.view.View, float):void */
        /* access modifiers changed from: package-private */
        public final void b() {
            bx.c((View) this.a, 1.0f);
            bx.t(this.a).a(0.0f).a(180L).b(0L).c();
            if (this.b.getVisibility() == 0) {
                bx.c((View) this.b, 1.0f);
                bx.t(this.b).a(0.0f).a(180L).b(0L).c();
            }
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            super.onFinishInflate();
            this.a = (TextView) findViewById(f.snackbar_text);
            this.b = (TextView) findViewById(f.snackbar_action);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            if (z && this.e != null) {
                this.e.a();
            }
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            boolean z;
            super.onMeasure(i, i2);
            if (this.c > 0 && getMeasuredWidth() > this.c) {
                i = View.MeasureSpec.makeMeasureSpec(this.c, 1073741824);
                super.onMeasure(i, i2);
            }
            int dimensionPixelSize = getResources().getDimensionPixelSize(e.snackbar_padding_vertical_2lines);
            int dimensionPixelSize2 = getResources().getDimensionPixelSize(e.snackbar_padding_vertical);
            boolean z2 = this.a.getLayout().getLineCount() > 1;
            if (!z2 || this.d <= 0 || this.b.getMeasuredWidth() <= this.d) {
                if (!z2) {
                    dimensionPixelSize = dimensionPixelSize2;
                }
                if (a(0, dimensionPixelSize, dimensionPixelSize)) {
                    z = true;
                }
                z = false;
            } else {
                if (a(1, dimensionPixelSize, dimensionPixelSize - dimensionPixelSize2)) {
                    z = true;
                }
                z = false;
            }
            if (z) {
                super.onMeasure(i, i2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (Build.VERSION.SDK_INT >= 14) {
            bx.b(this.c, (float) this.c.getHeight());
            bx.t(this.c).c(0.0f).a(a.b).a(250L).a(new aq(this)).c();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.c.getContext(), b.c);
        loadAnimation.setInterpolator(a.b);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new ar(this));
        this.c.startAnimation(loadAnimation);
    }

    /* access modifiers changed from: private */
    public void e() {
        this.b.removeView(this.c);
        aw.a().b(this.d);
    }

    public final void a() {
        aw.a().a(this.d);
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.c.getParent() == null) {
            ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
            if (layoutParams instanceof r) {
                au auVar = new au(this);
                auVar.b();
                auVar.d();
                auVar.a();
                auVar.a(new ao(this));
                ((r) layoutParams).a(auVar);
            }
            this.b.addView(this.c);
        }
        if (bx.F(this.c)) {
            d();
        } else {
            this.c.a(new ap(this));
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c() {
        /*
            r6 = this;
            r4 = 250(0xfa, double:1.235E-321)
            r1 = 0
            android.support.design.widget.Snackbar$SnackbarLayout r0 = r6.c
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L_0x0028
            android.support.design.widget.Snackbar$SnackbarLayout r0 = r6.c
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            boolean r2 = r0 instanceof android.support.design.widget.r
            if (r2 == 0) goto L_0x002e
            android.support.design.widget.r r0 = (android.support.design.widget.r) r0
            android.support.design.widget.CoordinatorLayout$Behavior r0 = r0.a
            boolean r2 = r0 instanceof android.support.design.widget.bd
            if (r2 == 0) goto L_0x002e
            android.support.design.widget.bd r0 = (android.support.design.widget.bd) r0
            int r0 = r0.e()
            if (r0 == 0) goto L_0x002c
            r0 = 1
        L_0x0026:
            if (r0 == 0) goto L_0x0030
        L_0x0028:
            r6.e()
        L_0x002b:
            return
        L_0x002c:
            r0 = r1
            goto L_0x0026
        L_0x002e:
            r0 = r1
            goto L_0x0026
        L_0x0030:
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 14
            if (r0 < r1) goto L_0x005e
            android.support.design.widget.Snackbar$SnackbarLayout r0 = r6.c
            android.support.v4.view.dv r0 = android.support.v4.view.bx.t(r0)
            android.support.design.widget.Snackbar$SnackbarLayout r1 = r6.c
            int r1 = r1.getHeight()
            float r1 = (float) r1
            android.support.v4.view.dv r0 = r0.c(r1)
            android.view.animation.Interpolator r1 = android.support.design.widget.a.b
            android.support.v4.view.dv r0 = r0.a(r1)
            android.support.v4.view.dv r0 = r0.a(r4)
            android.support.design.widget.as r1 = new android.support.design.widget.as
            r1.<init>(r6)
            android.support.v4.view.dv r0 = r0.a(r1)
            r0.c()
            goto L_0x002b
        L_0x005e:
            android.support.design.widget.Snackbar$SnackbarLayout r0 = r6.c
            android.content.Context r0 = r0.getContext()
            int r1 = android.support.design.b.d
            android.view.animation.Animation r0 = android.view.animation.AnimationUtils.loadAnimation(r0, r1)
            android.view.animation.Interpolator r1 = android.support.design.widget.a.b
            r0.setInterpolator(r1)
            r0.setDuration(r4)
            android.support.design.widget.at r1 = new android.support.design.widget.at
            r1.<init>(r6)
            r0.setAnimationListener(r1)
            android.support.design.widget.Snackbar$SnackbarLayout r1 = r6.c
            r1.startAnimation(r0)
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.Snackbar.c():void");
    }
}
