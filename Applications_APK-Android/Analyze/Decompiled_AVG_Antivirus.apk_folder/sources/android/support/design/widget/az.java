package android.support.design.widget;

import java.lang.ref.WeakReference;

final class az {
    /* access modifiers changed from: private */
    public final WeakReference a;
    /* access modifiers changed from: private */
    public int b;

    /* access modifiers changed from: package-private */
    public final boolean a(ay ayVar) {
        return ayVar != null && this.a.get() == ayVar;
    }
}
