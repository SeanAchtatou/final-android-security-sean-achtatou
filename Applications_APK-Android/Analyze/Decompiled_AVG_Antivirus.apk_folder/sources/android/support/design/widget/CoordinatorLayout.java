package android.support.design.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.design.h;
import android.support.design.i;
import android.support.v4.view.ay;
import android.support.v4.view.bg;
import android.support.v4.view.bh;
import android.support.v4.view.bx;
import android.support.v4.view.eo;
import android.support.v4.view.v;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoordinatorLayout extends ViewGroup implements bg {
    static final String a = CoordinatorLayout.class.getPackage().getName();
    static final Class[] b = {Context.class, AttributeSet.class};
    static final ThreadLocal c = new ThreadLocal();
    static final Comparator e;
    static final v f;
    final Comparator d;
    private final List g;
    private final List h;
    private final List i;
    private final Rect j;
    private final Rect k;
    private final Rect l;
    private final int[] m;
    private boolean n;
    private int[] o;
    private View p;
    private View q;
    private View r;
    private s s;
    private boolean t;
    private eo u;
    private boolean v;
    private Drawable w;
    /* access modifiers changed from: private */
    public ViewGroup.OnHierarchyChangeListener x;
    private final bh y;

    public abstract class Behavior {
        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
        }

        public static eo a(eo eoVar) {
            return eoVar;
        }

        public Parcelable a(CoordinatorLayout coordinatorLayout, View view) {
            return View.BaseSavedState.EMPTY_STATE;
        }

        public void a(CoordinatorLayout coordinatorLayout, View view, int i, int[] iArr) {
        }

        public void a(CoordinatorLayout coordinatorLayout, View view, Parcelable parcelable) {
        }

        public void a(View view, View view2) {
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, float f, boolean z) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, int i) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, View view2, int i) {
            return false;
        }

        public boolean a(MotionEvent motionEvent) {
            return false;
        }

        public boolean a(View view) {
            return false;
        }

        public void b(CoordinatorLayout coordinatorLayout, View view, int i) {
        }

        public void c() {
        }
    }

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new t();
        SparseArray a;

        public SavedState(Parcel parcel) {
            super(parcel);
            int readInt = parcel.readInt();
            int[] iArr = new int[readInt];
            parcel.readIntArray(iArr);
            Parcelable[] readParcelableArray = parcel.readParcelableArray(CoordinatorLayout.class.getClassLoader());
            this.a = new SparseArray(readInt);
            for (int i = 0; i < readInt; i++) {
                this.a.append(iArr[i], readParcelableArray[i]);
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            int size = this.a != null ? this.a.size() : 0;
            parcel.writeInt(size);
            int[] iArr = new int[size];
            Parcelable[] parcelableArr = new Parcelable[size];
            for (int i2 = 0; i2 < size; i2++) {
                iArr[i2] = this.a.keyAt(i2);
                parcelableArr[i2] = (Parcelable) this.a.valueAt(i2);
            }
            parcel.writeIntArray(iArr);
            parcel.writeParcelableArray(parcelableArr, i);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            e = new u();
            f = new w();
        } else {
            e = null;
            f = null;
        }
    }

    public CoordinatorLayout(Context context) {
        this(context, null);
    }

    public CoordinatorLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CoordinatorLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = new n(this);
        this.g = new ArrayList();
        this.h = new ArrayList();
        this.i = new ArrayList();
        this.j = new Rect();
        this.k = new Rect();
        this.l = new Rect();
        this.m = new int[2];
        this.y = new bh(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.C, i2, h.Widget_Design_CoordinatorLayout);
        int resourceId = obtainStyledAttributes.getResourceId(i.J, 0);
        if (resourceId != 0) {
            Resources resources = context.getResources();
            this.o = resources.getIntArray(resourceId);
            float f2 = resources.getDisplayMetrics().density;
            int length = this.o.length;
            for (int i3 = 0; i3 < length; i3++) {
                int[] iArr = this.o;
                iArr[i3] = (int) (((float) iArr[i3]) * f2);
            }
        }
        this.w = obtainStyledAttributes.getDrawable(i.K);
        obtainStyledAttributes.recycle();
        if (f != null) {
            f.a(this, new o(this));
        }
        super.setOnHierarchyChangeListener(new q(this));
    }

    private int a(int i2) {
        if (this.o == null) {
            Log.e("CoordinatorLayout", "No keylines defined for " + this + " - attempted index lookup " + i2);
            return 0;
        } else if (i2 >= 0 && i2 < this.o.length) {
            return this.o[i2];
        } else {
            Log.e("CoordinatorLayout", "Keyline index " + i2 + " out of range for " + this);
            return 0;
        }
    }

    static Behavior a(Context context, AttributeSet attributeSet, String str) {
        HashMap hashMap;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.startsWith(".")) {
            str = context.getPackageName() + str;
        } else if (str.indexOf(46) < 0) {
            str = a + '.' + str;
        }
        try {
            Map map = (Map) c.get();
            if (map == null) {
                HashMap hashMap2 = new HashMap();
                c.set(hashMap2);
                hashMap = hashMap2;
            } else {
                hashMap = map;
            }
            Constructor<?> constructor = (Constructor) hashMap.get(str);
            if (constructor == null) {
                constructor = Class.forName(str, true, context.getClassLoader()).getConstructor(b);
                hashMap.put(str, constructor);
            }
            return (Behavior) constructor.newInstance(context, attributeSet);
        } catch (Exception e2) {
            throw new RuntimeException("Could not inflate Behavior subclass " + str, e2);
        }
    }

    private void a() {
        if (this.p != null) {
            Behavior behavior = ((r) this.p.getLayoutParams()).a;
            if (behavior != null) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                behavior.a(obtain);
                obtain.recycle();
            }
            this.p = null;
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            ((r) getChildAt(i2).getLayoutParams()).c();
        }
    }

    static /* synthetic */ void a(CoordinatorLayout coordinatorLayout, eo eoVar) {
        eo eoVar2;
        boolean z = true;
        if (coordinatorLayout.u != eoVar) {
            coordinatorLayout.u = eoVar;
            coordinatorLayout.v = eoVar != null && eoVar.b() > 0;
            if (coordinatorLayout.v || coordinatorLayout.getBackground() != null) {
                z = false;
            }
            coordinatorLayout.setWillNotDraw(z);
            if (!eoVar.e()) {
                int childCount = coordinatorLayout.getChildCount();
                eo eoVar3 = eoVar;
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = coordinatorLayout.getChildAt(i2);
                    if (bx.y(childAt)) {
                        if (((r) childAt.getLayoutParams()).a != null) {
                            eoVar2 = Behavior.a(eoVar3);
                            if (eoVar2.e()) {
                                break;
                            }
                        } else {
                            eoVar2 = eoVar3;
                        }
                        eoVar3 = bx.b(childAt, eoVar2);
                        if (eoVar3.e()) {
                            break;
                        }
                    }
                }
            }
            coordinatorLayout.requestLayout();
        }
    }

    private void a(View view, int i2, Rect rect, Rect rect2) {
        int width;
        int height;
        r rVar = (r) view.getLayoutParams();
        int i3 = rVar.c;
        if (i3 == 0) {
            i3 = 17;
        }
        int a2 = v.a(i3, i2);
        int a3 = v.a(b(rVar.d), i2);
        int i4 = a2 & 7;
        int i5 = a2 & 112;
        int i6 = a3 & 7;
        int i7 = a3 & 112;
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        switch (i6) {
            case 1:
                width = (rect.width() / 2) + rect.left;
                break;
            case 5:
                width = rect.right;
                break;
            default:
                width = rect.left;
                break;
        }
        switch (i7) {
            case 16:
                height = rect.top + (rect.height() / 2);
                break;
            case 80:
                height = rect.bottom;
                break;
            default:
                height = rect.top;
                break;
        }
        switch (i4) {
            case 1:
                width -= measuredWidth / 2;
                break;
            case 5:
                break;
            default:
                width -= measuredWidth;
                break;
        }
        switch (i5) {
            case 16:
                height -= measuredHeight / 2;
                break;
            case 80:
                break;
            default:
                height -= measuredHeight;
                break;
        }
        int width2 = getWidth();
        int height2 = getHeight();
        int max = Math.max(getPaddingLeft() + rVar.leftMargin, Math.min(width, ((width2 - getPaddingRight()) - measuredWidth) - rVar.rightMargin));
        int max2 = Math.max(getPaddingTop() + rVar.topMargin, Math.min(height, ((height2 - getPaddingBottom()) - measuredHeight) - rVar.bottomMargin));
        rect2.set(max, max2, max + measuredWidth, max2 + measuredHeight);
    }

    private void a(View view, boolean z, Rect rect) {
        if (view.isLayoutRequested() || view.getVisibility() == 8) {
            rect.set(0, 0, 0, 0);
        } else if (z) {
            bx.a(this, view, rect);
        } else {
            rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        }
    }

    private boolean a(MotionEvent motionEvent) {
        boolean z;
        boolean z2;
        int a2 = ay.a(motionEvent);
        List list = this.h;
        list.clear();
        boolean isChildrenDrawingOrderEnabled = isChildrenDrawingOrderEnabled();
        int childCount = getChildCount();
        for (int i2 = childCount - 1; i2 >= 0; i2--) {
            list.add(getChildAt(isChildrenDrawingOrderEnabled ? getChildDrawingOrder(childCount, i2) : i2));
        }
        if (e != null) {
            Collections.sort(list, e);
        }
        int size = list.size();
        int i3 = 0;
        boolean z3 = false;
        boolean z4 = false;
        while (true) {
            if (i3 >= size) {
                z = z4;
                break;
            }
            View view = (View) list.get(i3);
            r rVar = (r) view.getLayoutParams();
            Behavior behavior = rVar.a;
            if ((!z4 && !z3) || a2 == 0) {
                if (z4 || behavior == null) {
                    z = z4;
                } else {
                    boolean a3 = behavior.a(this, view, motionEvent);
                    if (a3) {
                        this.p = view;
                    }
                    z = a3;
                }
                boolean a4 = rVar.a();
                boolean b2 = rVar.b();
                boolean z5 = b2 && !a4;
                if (b2 && !z5) {
                    break;
                }
                z4 = z;
                z2 = z5;
            } else if (behavior != null) {
                behavior.a(this, view, (MotionEvent) null);
                z2 = z3;
            } else {
                z2 = z3;
            }
            i3++;
            z3 = z2;
        }
        list.clear();
        return z;
    }

    private static int b(int i2) {
        if (i2 == 0) {
            return 8388659;
        }
        return i2;
    }

    private static int c(int i2) {
        if (i2 == 0) {
            return 8388661;
        }
        return i2;
    }

    private static r c(View view) {
        r rVar = (r) view.getLayoutParams();
        if (!rVar.b) {
            p pVar = null;
            for (Class<?> cls = view.getClass(); cls != null; cls = cls.getSuperclass()) {
                pVar = (p) cls.getAnnotation(p.class);
                if (pVar != null) {
                    break;
                }
            }
            p pVar2 = pVar;
            if (pVar2 != null) {
                try {
                    rVar.a((Behavior) pVar2.a().newInstance());
                } catch (Exception e2) {
                    Log.e("CoordinatorLayout", "Default behavior class " + pVar2.a().getName() + " could not be instantiated. Did you forget a default constructor?", e2);
                }
            }
            rVar.b = true;
        }
        return rVar;
    }

    public final void a(View view) {
        boolean z;
        r rVar;
        Behavior behavior;
        int size = this.g.size();
        int i2 = 0;
        boolean z2 = false;
        while (i2 < size) {
            View view2 = (View) this.g.get(i2);
            if (view2 == view) {
                z = true;
            } else {
                if (z2 && (behavior = (rVar = (r) view2.getLayoutParams()).a) != null && rVar.a(view)) {
                    behavior.a(this, view2, view);
                }
                z = z2;
            }
            i2++;
            z2 = z;
        }
    }

    public final void a(View view, int i2) {
        int i3;
        int i4;
        r rVar = (r) view.getLayoutParams();
        if (rVar.g == null && rVar.f != -1) {
            throw new IllegalStateException("An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete.");
        } else if (rVar.g != null) {
            View view2 = rVar.g;
            view.getLayoutParams();
            Rect rect = this.j;
            Rect rect2 = this.k;
            bx.a(this, view2, rect);
            a(view, i2, rect, rect2);
            view.layout(rect2.left, rect2.top, rect2.right, rect2.bottom);
        } else if (rVar.e >= 0) {
            int i5 = rVar.e;
            r rVar2 = (r) view.getLayoutParams();
            int a2 = v.a(c(rVar2.c), i2);
            int i6 = a2 & 7;
            int i7 = a2 & 112;
            int width = getWidth();
            int height = getHeight();
            int measuredWidth = view.getMeasuredWidth();
            int measuredHeight = view.getMeasuredHeight();
            if (i2 == 1) {
                i5 = width - i5;
            }
            int a3 = a(i5) - measuredWidth;
            switch (i6) {
                case 1:
                    i3 = a3 + (measuredWidth / 2);
                    break;
                case 5:
                    i3 = a3 + measuredWidth;
                    break;
                default:
                    i3 = a3;
                    break;
            }
            switch (i7) {
                case 16:
                    i4 = (measuredHeight / 2) + 0;
                    break;
                case 80:
                    i4 = measuredHeight + 0;
                    break;
                default:
                    i4 = 0;
                    break;
            }
            int max = Math.max(getPaddingLeft() + rVar2.leftMargin, Math.min(i3, ((width - getPaddingRight()) - measuredWidth) - rVar2.rightMargin));
            int max2 = Math.max(getPaddingTop() + rVar2.topMargin, Math.min(i4, ((height - getPaddingBottom()) - measuredHeight) - rVar2.bottomMargin));
            view.layout(max, max2, max + measuredWidth, max2 + measuredHeight);
        } else {
            r rVar3 = (r) view.getLayoutParams();
            Rect rect3 = this.j;
            rect3.set(getPaddingLeft() + rVar3.leftMargin, getPaddingTop() + rVar3.topMargin, (getWidth() - getPaddingRight()) - rVar3.rightMargin, (getHeight() - getPaddingBottom()) - rVar3.bottomMargin);
            if (this.u != null && bx.y(this) && !bx.y(view)) {
                rect3.left += this.u.a();
                rect3.top += this.u.b();
                rect3.right -= this.u.c();
                rect3.bottom -= this.u.d();
            }
            Rect rect4 = this.k;
            v.a(b(rVar3.c), view.getMeasuredWidth(), view.getMeasuredHeight(), rect3, rect4, i2);
            view.layout(rect4.left, rect4.top, rect4.right, rect4.bottom);
        }
    }

    public final void a(View view, int i2, int i3, int i4) {
        measureChildWithMargins(view, i2, i3, i4, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.CoordinatorLayout.a(android.view.View, boolean, android.graphics.Rect):void
     arg types: [android.view.View, int, android.graphics.Rect]
     candidates:
      android.support.design.widget.CoordinatorLayout.a(android.content.Context, android.util.AttributeSet, java.lang.String):android.support.design.widget.CoordinatorLayout$Behavior
      android.support.design.widget.CoordinatorLayout.a(android.view.View, int, int):boolean
      android.support.design.widget.CoordinatorLayout.a(android.view.View, boolean, android.graphics.Rect):void */
    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        Behavior behavior;
        int h2 = bx.h(this);
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = (View) this.g.get(i2);
            r rVar = (r) view.getLayoutParams();
            for (int i3 = 0; i3 < i2; i3++) {
                if (rVar.h == ((View) this.g.get(i3))) {
                    r rVar2 = (r) view.getLayoutParams();
                    if (rVar2.g != null) {
                        Rect rect = this.j;
                        Rect rect2 = this.k;
                        Rect rect3 = this.l;
                        bx.a(this, rVar2.g, rect);
                        a(view, false, rect2);
                        a(view, h2, rect, rect3);
                        int i4 = rect3.left - rect2.left;
                        int i5 = rect3.top - rect2.top;
                        if (i4 != 0) {
                            view.offsetLeftAndRight(i4);
                        }
                        if (i5 != 0) {
                            view.offsetTopAndBottom(i5);
                        }
                        if (!((i4 == 0 && i5 == 0) || (behavior = rVar2.a) == null)) {
                            behavior.a(this, view, rVar2.g);
                        }
                    }
                }
            }
            Rect rect4 = this.j;
            Rect rect5 = this.k;
            rect4.set(((r) view.getLayoutParams()).i);
            a(view, true, rect5);
            if (!rect4.equals(rect5)) {
                ((r) view.getLayoutParams()).i.set(rect5);
                for (int i6 = i2 + 1; i6 < size; i6++) {
                    View view2 = (View) this.g.get(i6);
                    r rVar3 = (r) view2.getLayoutParams();
                    Behavior behavior2 = rVar3.a;
                    if (behavior2 != null && behavior2.a(view)) {
                        if (z || !rVar3.f()) {
                            behavior2.a(this, view2, view);
                            if (z) {
                                rVar3.g();
                            }
                        } else {
                            rVar3.h();
                        }
                    }
                }
            }
        }
    }

    public final boolean a(View view, int i2, int i3) {
        Rect rect = this.j;
        bx.a(this, view, rect);
        return rect.contains(i2, i3);
    }

    public final boolean a(View view, View view2) {
        if (view.getVisibility() != 0 || view2.getVisibility() != 0) {
            return false;
        }
        Rect rect = this.j;
        a(view, view.getParent() != this, rect);
        Rect rect2 = this.k;
        a(view2, view2.getParent() != this, rect2);
        return rect.left <= rect2.right && rect.top <= rect2.bottom && rect.right >= rect2.left && rect.bottom >= rect2.top;
    }

    public final List b(View view) {
        r rVar = (r) view.getLayoutParams();
        List list = this.i;
        list.clear();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt != view && rVar.a(childAt)) {
                list.add(childAt);
            }
        }
        return list;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof r) && super.checkLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        view.getLayoutParams();
        return super.drawChild(canvas, view, j2);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new r();
    }

    public /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new r(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof r ? new r((r) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new r((ViewGroup.MarginLayoutParams) layoutParams) : new r(layoutParams);
    }

    public int getNestedScrollAxes() {
        return this.y.a();
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumHeight() {
        return Math.max(super.getSuggestedMinimumHeight(), getPaddingTop() + getPaddingBottom());
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumWidth() {
        return Math.max(super.getSuggestedMinimumWidth(), getPaddingLeft() + getPaddingRight());
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a();
        if (this.t) {
            if (this.s == null) {
                this.s = new s(this);
            }
            getViewTreeObserver().addOnPreDrawListener(this.s);
        }
        this.n = true;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        a();
        if (this.t && this.s != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.s);
        }
        if (this.r != null) {
            onStopNestedScroll(this.r);
        }
        this.n = false;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.v && this.w != null) {
            int b2 = this.u != null ? this.u.b() : 0;
            if (b2 > 0) {
                this.w.setBounds(0, 0, getWidth(), b2);
                this.w.draw(canvas);
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int a2 = ay.a(motionEvent);
        if (a2 == 0) {
            a();
        }
        boolean a3 = a(motionEvent);
        if (a2 == 1 || a2 == 3) {
            a();
        }
        return a3;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int h2 = bx.h(this);
        int size = this.g.size();
        for (int i6 = 0; i6 < size; i6++) {
            View view = (View) this.g.get(i6);
            Behavior behavior = ((r) view.getLayoutParams()).a;
            if (behavior == null || !behavior.a(this, view, h2)) {
                a(view, h2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        boolean z;
        int i4;
        int i5;
        boolean z2;
        boolean z3;
        int childCount = getChildCount();
        boolean z4 = this.g.size() != childCount;
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            r c2 = c(childAt);
            if (c2.f == -1) {
                c2.h = null;
                c2.g = null;
            } else {
                if (c2.g != null) {
                    if (c2.g.getId() != c2.f) {
                        z3 = false;
                    } else {
                        View view = c2.g;
                        ViewParent parent = c2.g.getParent();
                        while (true) {
                            if (parent == this) {
                                c2.h = view;
                                z3 = true;
                                break;
                            } else if (parent == null || parent == childAt) {
                                c2.h = null;
                                c2.g = null;
                                z3 = false;
                            } else {
                                if (parent instanceof View) {
                                    view = (View) parent;
                                }
                                parent = parent.getParent();
                            }
                        }
                        c2.h = null;
                        c2.g = null;
                        z3 = false;
                    }
                    if (z3) {
                        continue;
                    }
                }
                c2.g = findViewById(c2.f);
                if (c2.g != null) {
                    View view2 = c2.g;
                    ViewParent parent2 = c2.g.getParent();
                    while (true) {
                        if (parent2 == this || parent2 == null) {
                            c2.h = view2;
                        } else if (parent2 != childAt) {
                            if (parent2 instanceof View) {
                                view2 = (View) parent2;
                            }
                            parent2 = parent2.getParent();
                        } else if (isInEditMode()) {
                            c2.h = null;
                            c2.g = null;
                        } else {
                            throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                        }
                    }
                } else if (isInEditMode()) {
                    c2.h = null;
                    c2.g = null;
                } else {
                    throw new IllegalStateException("Could not find CoordinatorLayout descendant view with id " + getResources().getResourceName(c2.f) + " to anchor view " + childAt);
                }
            }
        }
        if (z4) {
            this.g.clear();
            for (int i7 = 0; i7 < childCount; i7++) {
                this.g.add(getChildAt(i7));
            }
            Collections.sort(this.g, this.d);
        }
        int childCount2 = getChildCount();
        int i8 = 0;
        while (true) {
            if (i8 >= childCount2) {
                z = false;
                break;
            }
            View childAt2 = getChildAt(i8);
            r rVar = (r) childAt2.getLayoutParams();
            if (rVar.g == null) {
                int childCount3 = getChildCount();
                int i9 = 0;
                while (true) {
                    if (i9 < childCount3) {
                        View childAt3 = getChildAt(i9);
                        if (childAt3 != childAt2 && rVar.a(childAt3)) {
                            z2 = true;
                            break;
                        }
                        i9++;
                    } else {
                        z2 = false;
                        break;
                    }
                }
            } else {
                z2 = true;
            }
            if (z2) {
                z = true;
                break;
            }
            i8++;
        }
        if (z != this.t) {
            if (z) {
                if (this.n) {
                    if (this.s == null) {
                        this.s = new s(this);
                    }
                    getViewTreeObserver().addOnPreDrawListener(this.s);
                }
                this.t = true;
            } else {
                if (this.n && this.s != null) {
                    getViewTreeObserver().removeOnPreDrawListener(this.s);
                }
                this.t = false;
            }
        }
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int h2 = bx.h(this);
        boolean z5 = h2 == 1;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        int i10 = paddingLeft + paddingRight;
        int i11 = paddingTop + paddingBottom;
        int suggestedMinimumWidth = getSuggestedMinimumWidth();
        int suggestedMinimumHeight = getSuggestedMinimumHeight();
        boolean z6 = this.u != null && bx.y(this);
        int size3 = this.g.size();
        int i12 = 0;
        int i13 = 0;
        int i14 = suggestedMinimumHeight;
        int i15 = suggestedMinimumWidth;
        while (i12 < size3) {
            View view3 = (View) this.g.get(i12);
            r rVar2 = (r) view3.getLayoutParams();
            int i16 = 0;
            if (rVar2.e >= 0 && mode != 0) {
                int a2 = a(rVar2.e);
                int a3 = v.a(c(rVar2.c), h2) & 7;
                if ((a3 == 3 && !z5) || (a3 == 5 && z5)) {
                    i16 = Math.max(0, (size - paddingRight) - a2);
                } else if ((a3 == 5 && !z5) || (a3 == 3 && z5)) {
                    i16 = Math.max(0, a2 - paddingLeft);
                }
            }
            if (!z6 || bx.y(view3)) {
                i4 = i3;
                i5 = i2;
            } else {
                int a4 = this.u.a() + this.u.c();
                int b2 = this.u.b() + this.u.d();
                i5 = View.MeasureSpec.makeMeasureSpec(size - a4, mode);
                i4 = View.MeasureSpec.makeMeasureSpec(size2 - b2, mode2);
            }
            Behavior behavior = rVar2.a;
            if (behavior == null || !behavior.a(this, view3, i5, i16, i4)) {
                a(view3, i5, i16, i4);
            }
            int max = Math.max(i15, view3.getMeasuredWidth() + i10 + rVar2.leftMargin + rVar2.rightMargin);
            int max2 = Math.max(i14, view3.getMeasuredHeight() + i11 + rVar2.topMargin + rVar2.bottomMargin);
            i12++;
            i13 = bx.a(i13, bx.m(view3));
            i14 = max2;
            i15 = max;
        }
        setMeasuredDimension(bx.a(i15, i2, -16777216 & i13), bx.a(i14, i3, i13 << 16));
    }

    public boolean onNestedFling(View view, float f2, float f3, boolean z) {
        Behavior behavior;
        int childCount = getChildCount();
        int i2 = 0;
        boolean z2 = false;
        while (i2 < childCount) {
            View childAt = getChildAt(i2);
            r rVar = (r) childAt.getLayoutParams();
            i2++;
            z2 = (!rVar.e() || (behavior = rVar.a) == null) ? z2 : behavior.a(this, childAt, f3, z) | z2;
        }
        if (z2) {
            a(true);
        }
        return z2;
    }

    public boolean onNestedPreFling(View view, float f2, float f3) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            getChildAt(i2).getLayoutParams();
        }
        return false;
    }

    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
        boolean z;
        int i4;
        int i5;
        Behavior behavior;
        int childCount = getChildCount();
        int i6 = 0;
        boolean z2 = false;
        int i7 = 0;
        int i8 = 0;
        while (i6 < childCount) {
            View childAt = getChildAt(i6);
            r rVar = (r) childAt.getLayoutParams();
            if (!rVar.e() || (behavior = rVar.a) == null) {
                z = z2;
                i4 = i7;
                i5 = i8;
            } else {
                int[] iArr2 = this.m;
                this.m[1] = 0;
                iArr2[0] = 0;
                behavior.a(this, childAt, i3, this.m);
                int max = i2 > 0 ? Math.max(i8, this.m[0]) : Math.min(i8, this.m[0]);
                int max2 = i3 > 0 ? Math.max(i7, this.m[1]) : Math.min(i7, this.m[1]);
                i5 = max;
                i4 = max2;
                z = true;
            }
            i6++;
            i8 = i5;
            i7 = i4;
            z2 = z;
        }
        iArr[0] = i8;
        iArr[1] = i7;
        if (z2) {
            a(true);
        }
    }

    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        boolean z;
        Behavior behavior;
        int childCount = getChildCount();
        int i6 = 0;
        boolean z2 = false;
        while (i6 < childCount) {
            View childAt = getChildAt(i6);
            r rVar = (r) childAt.getLayoutParams();
            if (!rVar.e() || (behavior = rVar.a) == null) {
                z = z2;
            } else {
                behavior.b(this, childAt, i5);
                z = true;
            }
            i6++;
            z2 = z;
        }
        if (z2) {
            a(true);
        }
    }

    public void onNestedScrollAccepted(View view, View view2, int i2) {
        this.y.a(i2);
        this.q = view;
        this.r = view2;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            getChildAt(i3).getLayoutParams();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        SparseArray sparseArray = savedState.a;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            int id = childAt.getId();
            Behavior behavior = c(childAt).a;
            if (!(id == -1 || behavior == null || (parcelable2 = (Parcelable) sparseArray.get(id)) == null)) {
                behavior.a(this, childAt, parcelable2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Parcelable a2;
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        SparseArray sparseArray = new SparseArray();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            int id = childAt.getId();
            Behavior behavior = ((r) childAt.getLayoutParams()).a;
            if (!(id == -1 || behavior == null || (a2 = behavior.a(this, childAt)) == null)) {
                sparseArray.append(id, a2);
            }
        }
        savedState.a = sparseArray;
        return savedState;
    }

    public boolean onStartNestedScroll(View view, View view2, int i2) {
        int childCount = getChildCount();
        int i3 = 0;
        boolean z = false;
        while (i3 < childCount) {
            View childAt = getChildAt(i3);
            r rVar = (r) childAt.getLayoutParams();
            Behavior behavior = rVar.a;
            if (behavior != null) {
                boolean a2 = behavior.a(this, childAt, view2, i2);
                z |= a2;
                rVar.a(a2);
            } else {
                rVar.a(false);
            }
            i3++;
            z = z;
        }
        return z;
    }

    public void onStopNestedScroll(View view) {
        this.y.b();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            r rVar = (r) getChildAt(i2).getLayoutParams();
            if (rVar.e()) {
                Behavior behavior = rVar.a;
                if (behavior != null) {
                    behavior.c();
                }
                rVar.d();
                rVar.h();
            }
        }
        this.q = null;
        this.r = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0033  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r5) {
        /*
            r4 = this;
            r2 = 0
            int r3 = android.support.v4.view.ay.a(r5)
            android.view.View r0 = r4.p
            if (r0 != 0) goto L_0x003c
            boolean r0 = r4.a(r5)
            if (r0 == 0) goto L_0x003a
            r1 = r0
        L_0x0010:
            android.view.View r0 = r4.p
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            android.support.design.widget.r r0 = (android.support.design.widget.r) r0
            android.support.design.widget.CoordinatorLayout$Behavior r0 = r0.a
            if (r0 == 0) goto L_0x001f
            r0.a(r5)
        L_0x001f:
            android.view.View r0 = r4.p
            if (r0 != 0) goto L_0x0033
            boolean r0 = super.onTouchEvent(r5)
            r2 = r0 | 0
        L_0x0029:
            r0 = 1
            if (r3 == r0) goto L_0x002f
            r0 = 3
            if (r3 != r0) goto L_0x0032
        L_0x002f:
            r4.a()
        L_0x0032:
            return r2
        L_0x0033:
            if (r1 == 0) goto L_0x0029
            r0 = 0
            super.onTouchEvent(r0)
            goto L_0x0029
        L_0x003a:
            r1 = r0
            goto L_0x001f
        L_0x003c:
            r1 = r2
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CoordinatorLayout.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        if (z) {
            a();
        }
    }

    public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener) {
        this.x = onHierarchyChangeListener;
    }
}
