package android.support.design.widget;

import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ay;
import android.support.v4.widget.bu;
import android.support.v4.widget.bx;
import android.view.MotionEvent;
import android.view.View;

public class bd extends CoordinatorLayout.Behavior {
    /* access modifiers changed from: private */
    public bu a;
    /* access modifiers changed from: private */
    public bf b;
    private boolean c;
    private float d = 0.0f;
    private boolean e;
    /* access modifiers changed from: private */
    public int f = 2;
    /* access modifiers changed from: private */
    public float g = 0.5f;
    /* access modifiers changed from: private */
    public float h = 0.0f;
    /* access modifiers changed from: private */
    public float i = 0.5f;
    private final bx j = new be(this);

    static float a(float f2, float f3, float f4) {
        return (f4 - f2) / (f3 - f2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: private */
    public static float b(float f2) {
        return Math.min(Math.max(0.0f, f2), 1.0f);
    }

    public final void a() {
        this.f = 0;
    }

    public final void a(bf bfVar) {
        this.b = bfVar;
    }

    public boolean a(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
        switch (ay.a(motionEvent)) {
            case 1:
            case 3:
                if (this.c) {
                    this.c = false;
                    return false;
                }
                break;
            case 2:
            default:
                this.c = !coordinatorLayout.a(view, (int) motionEvent.getX(), (int) motionEvent.getY());
                break;
        }
        if (this.c) {
            return false;
        }
        if (this.a == null) {
            this.a = this.e ? bu.a(coordinatorLayout, this.d, this.j) : bu.a(coordinatorLayout, this.j);
        }
        return this.a.a(motionEvent);
    }

    public final boolean a(MotionEvent motionEvent) {
        if (this.a == null) {
            return false;
        }
        this.a.b(motionEvent);
        return true;
    }

    public final void b() {
        this.h = b(0.1f);
    }

    public final void d() {
        this.i = b(0.6f);
    }

    public final int e() {
        if (this.a != null) {
            return this.a.a();
        }
        return 0;
    }
}
