package android.support.design.widget;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

final class bz implements by {
    private bz() {
    }

    /* synthetic */ bz(byte b) {
        this();
    }

    public final void a(ViewGroup viewGroup, View view, Rect rect) {
        viewGroup.offsetDescendantRectToMyCoords(view, rect);
    }
}
