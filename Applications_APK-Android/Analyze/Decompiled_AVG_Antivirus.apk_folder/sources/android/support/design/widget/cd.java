package android.support.design.widget;

import android.os.Build;
import android.view.View;

final class cd {
    static final bq a = new ce();
    private static final cf b;

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            b = new ch((byte) 0);
        } else {
            b = new cg((byte) 0);
        }
    }

    static bn a() {
        return a.a();
    }

    static void a(View view) {
        b.a(view);
    }
}
