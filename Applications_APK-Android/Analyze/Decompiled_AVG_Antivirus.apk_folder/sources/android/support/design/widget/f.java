package android.support.design.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;

final class f implements Parcelable.Creator {
    f() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new AppBarLayout.Behavior.SavedState(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new AppBarLayout.Behavior.SavedState[i];
    }
}
