package android.support.design.widget;

import android.support.design.widget.AppBarLayout;
import android.support.v4.view.bx;

final class e implements Runnable {
    final /* synthetic */ AppBarLayout.Behavior a;
    private final CoordinatorLayout b;
    private final AppBarLayout c;

    e(AppBarLayout.Behavior behavior, CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
        this.a = behavior;
        this.b = coordinatorLayout;
        this.c = appBarLayout;
    }

    public final void run() {
        if (this.c != null && this.a.d != null && this.a.d.g()) {
            this.a.a(this.b, this.c, this.a.d.c());
            bx.a(this.c, this);
        }
    }
}
