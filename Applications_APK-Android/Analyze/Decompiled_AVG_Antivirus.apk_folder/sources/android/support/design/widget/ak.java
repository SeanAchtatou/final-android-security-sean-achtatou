package android.support.design.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.NavigationView;

final class ak implements Parcelable.Creator {
    ak() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new NavigationView.SavedState(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new NavigationView.SavedState[i];
    }
}
