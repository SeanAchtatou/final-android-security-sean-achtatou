package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.h;
import android.support.design.i;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.bx;
import android.support.v4.view.eo;
import android.support.v4.widget.at;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@p(a = Behavior.class)
public class AppBarLayout extends LinearLayout {
    boolean a;
    private int b;
    private int c;
    private int d;
    private float e;
    private eo f;
    /* access modifiers changed from: private */
    public final List g;

    public class Behavior extends ViewOffsetBehavior {
        private int a;
        private boolean b;
        private Runnable c;
        /* access modifiers changed from: private */
        public at d;
        private bn e;
        private int f = -1;
        private boolean g;
        private float h;

        public class SavedState extends View.BaseSavedState {
            public static final Parcelable.Creator CREATOR = new f();
            int a;
            float b;
            boolean c;

            public SavedState(Parcel parcel) {
                super(parcel);
                this.a = parcel.readInt();
                this.b = parcel.readFloat();
                this.c = parcel.readByte() != 0;
            }

            public SavedState(Parcelable parcelable) {
                super(parcelable);
            }

            public void writeToParcel(Parcel parcel, int i) {
                super.writeToParcel(parcel, i);
                parcel.writeInt(this.a);
                parcel.writeFloat(this.b);
                parcel.writeByte((byte) (this.c ? 1 : 0));
            }
        }

        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        private int a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, int i3) {
            return b(coordinatorLayout, appBarLayout, a() - i, i2, i3);
        }

        private void a(AppBarLayout appBarLayout) {
            List a2 = appBarLayout.g;
            int size = a2.size();
            for (int i = 0; i < size; i++) {
                WeakReference weakReference = (WeakReference) a2.get(i);
                g gVar = weakReference != null ? (g) weakReference.get() : null;
                if (gVar != null) {
                    gVar.a(appBarLayout, super.b());
                }
            }
        }

        private int b(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, int i3) {
            int i4;
            int i5 = 0;
            int a2 = a();
            if (i2 != 0 && a2 >= i2 && a2 <= i3) {
                int i6 = i < i2 ? i2 : i > i3 ? i3 : i;
                if (a2 != i6) {
                    if (appBarLayout.a) {
                        int abs = Math.abs(i6);
                        int childCount = appBarLayout.getChildCount();
                        int i7 = 0;
                        while (true) {
                            if (i7 >= childCount) {
                                break;
                            }
                            View childAt = appBarLayout.getChildAt(i7);
                            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                            Interpolator interpolator = layoutParams.b;
                            if (abs < childAt.getTop() || abs > childAt.getBottom()) {
                                i7++;
                            } else if (interpolator != null) {
                                int i8 = layoutParams.a;
                                if ((i8 & 1) != 0) {
                                    i5 = childAt.getHeight() + 0;
                                    if ((i8 & 2) != 0) {
                                        i5 -= bx.s(childAt);
                                    }
                                }
                                if (i5 > 0) {
                                    i4 = (Math.round(interpolator.getInterpolation(((float) (abs - childAt.getTop())) / ((float) i5)) * ((float) i5)) + childAt.getTop()) * Integer.signum(i6);
                                }
                            }
                        }
                        i4 = i6;
                    } else {
                        i4 = i6;
                    }
                    boolean a3 = super.a(i4);
                    i5 = a2 - i6;
                    this.a = i6 - i4;
                    if (!a3 && appBarLayout.a) {
                        coordinatorLayout.a(appBarLayout);
                    }
                    a(appBarLayout);
                }
            }
            return i5;
        }

        /* access modifiers changed from: package-private */
        public final int a() {
            return super.b() + this.a;
        }

        /* access modifiers changed from: package-private */
        public final int a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i) {
            return b(coordinatorLayout, appBarLayout, i, Integer.MIN_VALUE, Integer.MAX_VALUE);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.CoordinatorLayout.Behavior.a(android.support.design.widget.CoordinatorLayout, android.view.View):android.os.Parcelable
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout]
         candidates:
          android.support.design.widget.CoordinatorLayout.Behavior.a(android.view.View, android.view.View):void
          android.support.design.widget.CoordinatorLayout.Behavior.a(android.support.design.widget.CoordinatorLayout, android.view.View):android.os.Parcelable */
        public final /* synthetic */ Parcelable a(CoordinatorLayout coordinatorLayout, View view) {
            boolean z = false;
            AppBarLayout appBarLayout = (AppBarLayout) view;
            Parcelable a2 = super.a(coordinatorLayout, (View) appBarLayout);
            int b2 = super.b();
            int childCount = appBarLayout.getChildCount();
            int i = 0;
            while (i < childCount) {
                View childAt = appBarLayout.getChildAt(i);
                int bottom = childAt.getBottom() + b2;
                if (childAt.getTop() + b2 > 0 || bottom < 0) {
                    i++;
                } else {
                    SavedState savedState = new SavedState(a2);
                    savedState.a = i;
                    if (bottom == bx.s(childAt)) {
                        z = true;
                    }
                    savedState.c = z;
                    savedState.b = ((float) bottom) / ((float) childAt.getHeight());
                    return savedState;
                }
            }
            return a2;
        }

        public final /* synthetic */ void a(CoordinatorLayout coordinatorLayout, View view, int i, int[] iArr) {
            int i2;
            int i3;
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (i != 0 && !this.b) {
                if (i < 0) {
                    i2 = -appBarLayout.a();
                    i3 = i2 + appBarLayout.d();
                } else {
                    i2 = -appBarLayout.c();
                    i3 = 0;
                }
                iArr[1] = a(coordinatorLayout, appBarLayout, i, i2, i3);
            }
        }

        public final /* synthetic */ void a(CoordinatorLayout coordinatorLayout, View view, Parcelable parcelable) {
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (parcelable instanceof SavedState) {
                SavedState savedState = (SavedState) parcelable;
                super.a(coordinatorLayout, appBarLayout, savedState.getSuperState());
                this.f = savedState.a;
                this.h = savedState.b;
                this.g = savedState.c;
                return;
            }
            super.a(coordinatorLayout, appBarLayout, parcelable);
            this.f = -1;
        }

        public final /* bridge */ /* synthetic */ boolean a(int i) {
            return super.a(i);
        }

        public final /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, float f2, boolean z) {
            int i;
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (!z) {
                int i2 = -appBarLayout.a();
                float f3 = -f2;
                if (this.c != null) {
                    appBarLayout.removeCallbacks(this.c);
                }
                if (this.d == null) {
                    this.d = at.a(appBarLayout.getContext());
                }
                this.d.a(a(), 0, Math.round(f3), 0, 0, i2, 0);
                if (this.d.g()) {
                    this.c = new e(this, coordinatorLayout, appBarLayout);
                    bx.a(appBarLayout, this.c);
                    return true;
                }
                this.c = null;
                return false;
            }
            if (f2 < 0.0f) {
                i = (-appBarLayout.a()) + appBarLayout.d();
                if (a() > i) {
                    return false;
                }
            } else {
                i = -appBarLayout.c();
                if (a() < i) {
                    return false;
                }
            }
            if (a() == i) {
                return false;
            }
            if (this.e == null) {
                this.e = cd.a();
                this.e.a(a.c);
                this.e.a(new d(this, coordinatorLayout, appBarLayout));
            } else {
                this.e.e();
            }
            this.e.a(a(), i);
            this.e.a();
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.ViewOffsetBehavior.a(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.a(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int):int
          android.support.design.widget.AppBarLayout.Behavior.a(android.support.design.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
          android.support.design.widget.CoordinatorLayout.Behavior.a(android.support.design.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
          android.support.design.widget.CoordinatorLayout.Behavior.a(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
          android.support.design.widget.CoordinatorLayout.Behavior.a(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View):boolean
          android.support.design.widget.ViewOffsetBehavior.a(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean */
        public final /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, int i) {
            AppBarLayout appBarLayout = (AppBarLayout) view;
            boolean a2 = super.a(coordinatorLayout, (View) appBarLayout, i);
            if (this.f >= 0) {
                View childAt = appBarLayout.getChildAt(this.f);
                int i2 = -childAt.getBottom();
                super.a(this.g ? bx.s(childAt) + i2 : Math.round(((float) childAt.getHeight()) * this.h) + i2);
                this.f = -1;
            }
            a(appBarLayout);
            return a2;
        }

        public final /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, View view2, int i) {
            AppBarLayout appBarLayout = (AppBarLayout) view;
            boolean z = (i & 2) != 0 && appBarLayout.b() && coordinatorLayout.getHeight() - view2.getHeight() <= appBarLayout.getHeight();
            if (z && this.e != null) {
                this.e.e();
            }
            return z;
        }

        public final /* bridge */ /* synthetic */ int b() {
            return super.b();
        }

        public final /* synthetic */ void b(CoordinatorLayout coordinatorLayout, View view, int i) {
            AppBarLayout appBarLayout = (AppBarLayout) view;
            if (i < 0) {
                a(coordinatorLayout, appBarLayout, i, -appBarLayout.e(), 0);
                this.b = true;
                return;
            }
            this.b = false;
        }

        public final /* bridge */ /* synthetic */ void c() {
            this.b = false;
        }
    }

    public class LayoutParams extends LinearLayout.LayoutParams {
        int a = 1;
        Interpolator b;

        public LayoutParams() {
            super(-1, -2);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.i);
            this.a = obtainStyledAttributes.getInt(i.j, 0);
            if (obtainStyledAttributes.hasValue(i.k)) {
                this.b = AnimationUtils.loadInterpolator(context, obtainStyledAttributes.getResourceId(i.k, 0));
            }
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(LinearLayout.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public class ScrollingViewBehavior extends ViewOffsetBehavior {
        private int a;

        public ScrollingViewBehavior() {
        }

        public ScrollingViewBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.ap);
            this.a = obtainStyledAttributes.getDimensionPixelSize(i.aq, 0);
            obtainStyledAttributes.recycle();
        }

        public final /* bridge */ /* synthetic */ boolean a(int i) {
            return super.a(i);
        }

        public final /* bridge */ /* synthetic */ boolean a(CoordinatorLayout coordinatorLayout, View view, int i) {
            return super.a(coordinatorLayout, view, i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.bx.a(android.view.View, boolean):void
         arg types: [android.view.View, int]
         candidates:
          android.support.v4.view.bx.a(int, int):int
          android.support.v4.view.bx.a(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
          android.support.v4.view.bx.a(android.view.View, float):void
          android.support.v4.view.bx.a(android.view.View, android.graphics.Paint):void
          android.support.v4.view.bx.a(android.view.View, android.support.v4.view.a):void
          android.support.v4.view.bx.a(android.view.View, android.support.v4.view.bi):void
          android.support.v4.view.bx.a(android.view.View, java.lang.Runnable):void
          android.support.v4.view.bx.a(android.view.View, int):boolean
          android.support.v4.view.bx.a(android.view.View, boolean):void */
        public final boolean a(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3) {
            AppBarLayout appBarLayout;
            if (view.getLayoutParams().height == -1) {
                List b = coordinatorLayout.b(view);
                if (b.isEmpty()) {
                    return false;
                }
                int size = b.size();
                int i4 = 0;
                while (true) {
                    if (i4 >= size) {
                        appBarLayout = null;
                        break;
                    }
                    View view2 = (View) b.get(i4);
                    if (view2 instanceof AppBarLayout) {
                        appBarLayout = (AppBarLayout) view2;
                        break;
                    }
                    i4++;
                }
                if (appBarLayout != null && bx.F(appBarLayout)) {
                    if (bx.y(appBarLayout)) {
                        bx.a(view, true);
                    }
                    int size2 = View.MeasureSpec.getSize(i3);
                    if (size2 == 0) {
                        size2 = coordinatorLayout.getHeight();
                    }
                    coordinatorLayout.a(view, i, i2, View.MeasureSpec.makeMeasureSpec((size2 - appBarLayout.getMeasuredHeight()) + appBarLayout.a(), Integer.MIN_VALUE));
                    return true;
                }
            }
            return false;
        }

        public final boolean a(CoordinatorLayout coordinatorLayout, View view, View view2) {
            CoordinatorLayout.Behavior behavior = ((r) view2.getLayoutParams()).a;
            if (!(behavior instanceof Behavior)) {
                return false;
            }
            int a2 = ((Behavior) behavior).a();
            int height = view2.getHeight() - this.a;
            int height2 = coordinatorLayout.getHeight() - view.getHeight();
            if (this.a == 0 || !(view2 instanceof AppBarLayout)) {
                super.a(a2 + (view2.getHeight() - this.a));
                return false;
            }
            super.a(a.a(height, height2, ((float) Math.abs(a2)) / ((float) ((AppBarLayout) view2).a())));
            return false;
        }

        public final boolean a(View view) {
            return view instanceof AppBarLayout;
        }

        public final /* bridge */ /* synthetic */ int b() {
            return super.b();
        }
    }

    public AppBarLayout(Context context) {
        this(context, null);
    }

    public AppBarLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = -1;
        this.c = -1;
        this.d = -1;
        setOrientation(1);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, i.h, 0, h.Widget_Design_AppBarLayout);
        this.e = (float) obtainStyledAttributes.getDimensionPixelSize(i.m, 0);
        setBackgroundDrawable(obtainStyledAttributes.getDrawable(i.l));
        obtainStyledAttributes.recycle();
        cd.a(this);
        this.g = new ArrayList();
        bx.f(this, this.e);
        bx.a(this, new c(this));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    private static LayoutParams a(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LinearLayout.LayoutParams ? new LayoutParams((LinearLayout.LayoutParams) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    static /* synthetic */ void a(AppBarLayout appBarLayout, eo eoVar) {
        appBarLayout.b = -1;
        appBarLayout.f = eoVar;
        int i = 0;
        int childCount = appBarLayout.getChildCount();
        while (i < childCount) {
            eoVar = bx.b(appBarLayout.getChildAt(i), eoVar);
            if (!eoVar.e()) {
                i++;
            } else {
                return;
            }
        }
    }

    public final int a() {
        int i;
        if (this.b != -1) {
            return this.b;
        }
        int childCount = getChildCount();
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            int height = bx.F(childAt) ? childAt.getHeight() : childAt.getMeasuredHeight();
            int i4 = layoutParams.a;
            if ((i4 & 1) == 0) {
                break;
            }
            i3 += height;
            if ((i4 & 2) != 0) {
                i = i3 - bx.s(childAt);
                break;
            }
            i2++;
        }
        i = i3;
        int b2 = i - (this.f != null ? this.f.b() : 0);
        this.b = b2;
        return b2;
    }

    public final void a(g gVar) {
        int size = this.g.size();
        int i = 0;
        while (i < size) {
            WeakReference weakReference = (WeakReference) this.g.get(i);
            if (weakReference == null || weakReference.get() != gVar) {
                i++;
            } else {
                return;
            }
        }
        this.g.add(new WeakReference(gVar));
    }

    public final void b(g gVar) {
        Iterator it = this.g.iterator();
        while (it.hasNext()) {
            g gVar2 = (g) ((WeakReference) it.next()).get();
            if (gVar2 == gVar || gVar2 == null) {
                it.remove();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return a() != 0;
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        return a();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* access modifiers changed from: package-private */
    public final int d() {
        int i;
        if (this.c != -1) {
            return this.c;
        }
        int i2 = 0;
        int childCount = getChildCount() - 1;
        while (childCount >= 0) {
            View childAt = getChildAt(childCount);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            int height = bx.F(childAt) ? childAt.getHeight() : childAt.getMeasuredHeight();
            int i3 = layoutParams.a;
            if ((i3 & 5) != 5) {
                if (i2 > 0) {
                    break;
                }
                i = i2;
            } else {
                i = (i3 & 8) != 0 ? bx.s(childAt) + i2 : i2 + height;
            }
            childCount--;
            i2 = i;
        }
        this.c = i2;
        return i2;
    }

    /* access modifiers changed from: package-private */
    public final int e() {
        if (this.d != -1) {
            return this.d;
        }
        int childCount = getChildCount();
        int i = 0;
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            int height = bx.F(childAt) ? childAt.getHeight() : childAt.getMeasuredHeight();
            int i3 = layoutParams.a;
            if ((i3 & 1) == 0) {
                break;
            }
            i += height;
            if ((i3 & 2) != 0) {
                return i - bx.s(childAt);
            }
        }
        this.d = i;
        return i;
    }

    /* access modifiers changed from: package-private */
    public final int f() {
        int b2 = this.f != null ? this.f.b() : 0;
        int s = bx.s(this);
        if (s != 0) {
            return (s * 2) + b2;
        }
        int childCount = getChildCount();
        if (childCount > 0) {
            return (bx.s(getChildAt(childCount - 1)) * 2) + b2;
        }
        return 0;
    }

    public final float g() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.b = -1;
        this.c = -1;
        this.c = -1;
        this.a = false;
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            if (((LayoutParams) getChildAt(i5).getLayoutParams()).b != null) {
                this.a = true;
                return;
            }
        }
    }

    public void setOrientation(int i) {
        if (i != 1) {
            throw new IllegalArgumentException("AppBarLayout is always vertical and does not support horizontal orientation");
        }
        super.setOrientation(i);
    }
}
