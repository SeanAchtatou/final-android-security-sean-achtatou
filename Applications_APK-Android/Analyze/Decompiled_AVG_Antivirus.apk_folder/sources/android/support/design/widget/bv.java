package android.support.design.widget;

import android.animation.ValueAnimator;
import android.view.animation.Interpolator;

final class bv extends br {
    final ValueAnimator a = new ValueAnimator();

    bv() {
    }

    public final void a() {
        this.a.start();
    }

    public final void a(float f, float f2) {
        this.a.setFloatValues(f, f2);
    }

    public final void a(int i) {
        this.a.setDuration((long) i);
    }

    public final void a(int i, int i2) {
        this.a.setIntValues(i, i2);
    }

    public final void a(bs bsVar) {
        this.a.addUpdateListener(new bw(this, bsVar));
    }

    public final void a(Interpolator interpolator) {
        this.a.setInterpolator(interpolator);
    }

    public final boolean b() {
        return this.a.isRunning();
    }

    public final int c() {
        return ((Integer) this.a.getAnimatedValue()).intValue();
    }

    public final float d() {
        return ((Float) this.a.getAnimatedValue()).floatValue();
    }

    public final void e() {
        this.a.cancel();
    }
}
