package android.support.design.widget;

import android.support.v4.view.bx;
import android.view.View;
import android.view.ViewParent;

final class cc {
    private final View a;
    private int b;
    private int c;
    private int d;
    private int e;

    public cc(View view) {
        this.a = view;
    }

    private void c() {
        bx.d(this.a, this.d - (this.a.getTop() - this.b));
        bx.e(this.a, this.e - (this.a.getLeft() - this.c));
        ViewParent parent = this.a.getParent();
        if (parent instanceof View) {
            ((View) parent).invalidate();
        }
    }

    public final void a() {
        this.b = this.a.getTop();
        this.c = this.a.getLeft();
        c();
    }

    public final boolean a(int i) {
        if (this.d == i) {
            return false;
        }
        this.d = i;
        c();
        return true;
    }

    public final int b() {
        return this.d;
    }

    public final boolean b(int i) {
        if (this.e == i) {
            return false;
        }
        this.e = i;
        c();
        return true;
    }
}
