package android.support.design.widget;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.bx;
import android.view.View;

final class m implements g {
    final /* synthetic */ CollapsingToolbarLayout a;

    private m(CollapsingToolbarLayout collapsingToolbarLayout) {
        this.a = collapsingToolbarLayout;
    }

    /* synthetic */ m(CollapsingToolbarLayout collapsingToolbarLayout, byte b) {
        this(collapsingToolbarLayout);
    }

    public final void a(AppBarLayout appBarLayout, int i) {
        int unused = this.a.q = i;
        int b = this.a.r != null ? this.a.r.b() : 0;
        int a2 = appBarLayout.a();
        int childCount = this.a.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = this.a.getChildAt(i2);
            CollapsingToolbarLayout.LayoutParams layoutParams = (CollapsingToolbarLayout.LayoutParams) childAt.getLayoutParams();
            cc a3 = CollapsingToolbarLayout.b(childAt);
            switch (layoutParams.a) {
                case 1:
                    if ((this.a.getHeight() - b) + i < childAt.getHeight()) {
                        break;
                    } else {
                        a3.a(-i);
                        break;
                    }
                case 2:
                    a3.a(Math.round(layoutParams.b * ((float) (-i))));
                    break;
            }
        }
        if (!(this.a.k == null && this.a.l == null)) {
            if (this.a.getHeight() + i < (bx.s(this.a) * 2) + b) {
                CollapsingToolbarLayout.d(this.a);
            } else {
                CollapsingToolbarLayout.e(this.a);
            }
        }
        if (this.a.l != null && b > 0) {
            bx.d(this.a);
        }
        this.a.j.b(((float) Math.abs(i)) / ((float) ((this.a.getHeight() - bx.s(this.a)) - b)));
        if (Math.abs(i) == a2) {
            bx.f(appBarLayout, appBarLayout.g());
        } else {
            bx.f(appBarLayout, 0.0f);
        }
    }
}
