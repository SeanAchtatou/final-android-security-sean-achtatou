package android.support.design.widget;

import android.view.animation.Interpolator;

abstract class br {
    br() {
    }

    /* access modifiers changed from: package-private */
    public abstract void a();

    /* access modifiers changed from: package-private */
    public abstract void a(float f, float f2);

    /* access modifiers changed from: package-private */
    public abstract void a(int i);

    /* access modifiers changed from: package-private */
    public abstract void a(int i, int i2);

    /* access modifiers changed from: package-private */
    public abstract void a(bs bsVar);

    /* access modifiers changed from: package-private */
    public abstract void a(Interpolator interpolator);

    /* access modifiers changed from: package-private */
    public abstract boolean b();

    /* access modifiers changed from: package-private */
    public abstract int c();

    /* access modifiers changed from: package-private */
    public abstract float d();

    /* access modifiers changed from: package-private */
    public abstract void e();
}
