package android.support.design.widget;

import android.support.v4.view.bx;
import android.view.View;
import java.util.Comparator;

final class u implements Comparator {
    u() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        float G = bx.G((View) obj);
        float G2 = bx.G((View) obj2);
        if (G > G2) {
            return -1;
        }
        return G < G2 ? 1 : 0;
    }
}
