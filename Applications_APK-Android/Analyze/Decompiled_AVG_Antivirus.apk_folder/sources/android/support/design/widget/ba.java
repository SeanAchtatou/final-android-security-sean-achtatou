package android.support.design.widget;

import android.util.StateSet;
import android.view.View;
import android.view.animation.Animation;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

final class ba {
    private final ArrayList a = new ArrayList();
    private bc b = null;
    /* access modifiers changed from: private */
    public Animation c = null;
    private WeakReference d;
    private Animation.AnimationListener e = new bb(this);

    ba() {
    }

    private View b() {
        if (this.d == null) {
            return null;
        }
        return (View) this.d.get();
    }

    public final void a() {
        View b2;
        if (this.c != null && (b2 = b()) != null && b2.getAnimation() == this.c) {
            b2.clearAnimation();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(View view) {
        View b2 = b();
        if (b2 != view) {
            if (b2 != null) {
                View b3 = b();
                int size = this.a.size();
                for (int i = 0; i < size; i++) {
                    if (b3.getAnimation() == ((bc) this.a.get(i)).b) {
                        b3.clearAnimation();
                    }
                }
                this.d = null;
                this.b = null;
                this.c = null;
            }
            if (view != null) {
                this.d = new WeakReference(view);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int[] iArr) {
        bc bcVar;
        int size = this.a.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                bcVar = null;
                break;
            }
            bcVar = (bc) this.a.get(i);
            if (StateSet.stateSetMatches(bcVar.a, iArr)) {
                break;
            }
            i++;
        }
        if (bcVar != this.b) {
            if (!(this.b == null || this.c == null)) {
                View b2 = b();
                if (b2 != null && b2.getAnimation() == this.c) {
                    b2.clearAnimation();
                }
                this.c = null;
            }
            this.b = bcVar;
            if (bcVar != null) {
                this.c = bcVar.b;
                View b3 = b();
                if (b3 != null) {
                    b3.startAnimation(this.c);
                }
            }
        }
    }

    public final void a(int[] iArr, Animation animation) {
        bc bcVar = new bc(iArr, animation, (byte) 0);
        animation.setAnimationListener(this.e);
        this.a.add(bcVar);
    }
}
