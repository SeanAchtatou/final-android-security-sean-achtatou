package android.support.design.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.design.b;
import android.support.v4.b.a.a;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

class z extends ag {
    al a;
    private Drawable g;
    private Drawable h;
    private Drawable i;
    /* access modifiers changed from: private */
    public float j;
    /* access modifiers changed from: private */
    public float k;
    private int l;
    private ba m = new ba();
    private boolean n;

    z(View view, am amVar) {
        super(view, amVar);
        this.l = view.getResources().getInteger(17694720);
        this.m.a(view);
        this.m.a(b, a(new ac(this, (byte) 0)));
        this.m.a(c, a(new ac(this, (byte) 0)));
        this.m.a(d, a(new ad(this, (byte) 0)));
    }

    private Animation a(Animation animation) {
        animation.setInterpolator(a.b);
        animation.setDuration((long) this.l);
        return animation;
    }

    private void e() {
        Rect rect = new Rect();
        this.a.getPadding(rect);
        this.f.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.m.a();
    }

    /* access modifiers changed from: package-private */
    public void a(float f) {
        if (this.j != f && this.a != null) {
            this.a.a(f, this.k + f);
            this.j = f;
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        a.a(this.g, colorStateList);
        if (this.i != null) {
            a.a(this.i, colorStateList);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        a.a(this.g, mode);
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable, ColorStateList colorStateList, PorterDuff.Mode mode, int i2, int i3) {
        Drawable[] drawableArr;
        this.g = a.c(drawable);
        a.a(this.g, colorStateList);
        if (mode != null) {
            a.a(this.g, mode);
        }
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(1);
        gradientDrawable.setColor(-1);
        gradientDrawable.setCornerRadius(this.f.a());
        this.h = a.c(gradientDrawable);
        a.a(this.h, new ColorStateList(new int[][]{c, b, new int[0]}, new int[]{i2, i2, 0}));
        a.a(this.h, PorterDuff.Mode.MULTIPLY);
        if (i3 > 0) {
            this.i = a(i3, colorStateList);
            drawableArr = new Drawable[]{this.i, this.g, this.h};
        } else {
            this.i = null;
            drawableArr = new Drawable[]{this.g, this.h};
        }
        this.a = new al(this.e.getResources(), new LayerDrawable(drawableArr), this.f.a(), this.j, this.j + this.k);
        this.a.a();
        this.f.a(this.a);
        e();
    }

    /* access modifiers changed from: package-private */
    public void a(int[] iArr) {
        this.m.a(iArr);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [android.support.design.widget.aa, android.view.animation.Animation$AnimationListener] */
    /* access modifiers changed from: package-private */
    public void b() {
        if (!this.n) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.e.getContext(), b.b);
            loadAnimation.setInterpolator(a.b);
            loadAnimation.setDuration(200);
            loadAnimation.setAnimationListener(new aa(this));
            this.e.startAnimation(loadAnimation);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(float f) {
        if (this.k != f && this.a != null) {
            this.k = f;
            al alVar = this.a;
            alVar.a(alVar.j, this.j + f);
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Animation loadAnimation = AnimationUtils.loadAnimation(this.e.getContext(), b.a);
        loadAnimation.setDuration(200);
        loadAnimation.setInterpolator(a.b);
        this.e.startAnimation(loadAnimation);
    }
}
