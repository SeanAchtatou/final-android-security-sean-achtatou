package android.support.design.widget;

import android.support.design.widget.AppBarLayout;

final class d implements bp {
    final /* synthetic */ CoordinatorLayout a;
    final /* synthetic */ AppBarLayout b;
    final /* synthetic */ AppBarLayout.Behavior c;

    d(AppBarLayout.Behavior behavior, CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
        this.c = behavior;
        this.a = coordinatorLayout;
        this.b = appBarLayout;
    }

    public final void a(bn bnVar) {
        this.c.a(this.a, this.b, bnVar.c());
    }
}
