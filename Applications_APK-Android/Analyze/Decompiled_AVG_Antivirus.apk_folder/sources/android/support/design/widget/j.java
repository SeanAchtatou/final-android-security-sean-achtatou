package android.support.design.widget;

import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.d.i;
import android.support.v4.view.bx;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Interpolator;

final class j {
    private static final boolean a = (Build.VERSION.SDK_INT < 18);
    private static final Paint b = null;
    private float A;
    private float B;
    private final TextPaint C;
    private Interpolator D;
    private Interpolator E;
    private final View c;
    private float d;
    private final Rect e;
    private final Rect f;
    private int g = 16;
    private int h = 16;
    private float i;
    private float j;
    private int k;
    private int l;
    private float m;
    private float n;
    private CharSequence o;
    private CharSequence p;
    private float q;
    private boolean r;
    private boolean s;
    private Bitmap t;
    private Paint u;
    private float v;
    private float w;
    private float x;
    private float y;
    private float z;

    public j(View view) {
        this.c = view;
        this.C = new TextPaint();
        this.C.setAntiAlias(true);
        this.f = new Rect();
        this.e = new Rect();
    }

    private static float a(float f2, float f3, float f4, Interpolator interpolator) {
        if (interpolator != null) {
            f4 = interpolator.getInterpolation(f4);
        }
        return a.a(f2, f3, f4);
    }

    private static boolean a(float f2, float f3) {
        return Math.abs(f2 - f3) < 0.001f;
    }

    private void h() {
        float f2;
        float f3;
        boolean z2;
        float f4 = this.d;
        this.x = a((float) this.e.left, (float) this.f.left, f4, this.D);
        this.z = a(this.m, this.n, f4, this.D);
        this.y = a((float) this.e.right, (float) this.f.right, f4, this.D);
        float a2 = a(this.i, this.j, f4, this.E);
        if (this.o != null) {
            if (a(a2, this.j)) {
                float width = (float) this.f.width();
                float f5 = this.j;
                this.A = 1.0f;
                f2 = width;
                f3 = f5;
            } else {
                float width2 = (float) this.e.width();
                float f6 = this.i;
                if (a(a2, this.i)) {
                    this.A = 1.0f;
                    f2 = width2;
                    f3 = f6;
                } else {
                    this.A = a2 / this.i;
                    f2 = width2;
                    f3 = f6;
                }
            }
            if (f2 > 0.0f) {
                z2 = this.B != f3;
                this.B = f3;
            } else {
                z2 = false;
            }
            if (this.p == null || z2) {
                this.C.setTextSize(this.B);
                CharSequence ellipsize = TextUtils.ellipsize(this.o, this.C, f2, TextUtils.TruncateAt.END);
                if (this.p == null || !this.p.equals(ellipsize)) {
                    this.p = ellipsize;
                }
                CharSequence charSequence = this.p;
                this.r = (bx.h(this.c) == 1 ? i.d : i.c).a(charSequence, charSequence.length());
                this.q = this.C.measureText(this.p, 0, this.p.length());
            }
            this.s = a && this.A != 1.0f;
            if (this.s && this.t == null && !this.e.isEmpty() && !TextUtils.isEmpty(this.p)) {
                this.C.setTextSize(this.i);
                this.C.setColor(this.k);
                int round = Math.round(this.C.measureText(this.p, 0, this.p.length()));
                int round2 = Math.round(this.C.descent() - this.C.ascent());
                this.q = (float) round;
                if (round > 0 || round2 > 0) {
                    this.t = Bitmap.createBitmap(round, round2, Bitmap.Config.ARGB_8888);
                    new Canvas(this.t).drawText(this.p, 0, this.p.length(), 0.0f, ((float) round2) - this.C.descent(), this.C);
                    if (this.u == null) {
                        this.u = new Paint();
                        this.u.setAntiAlias(true);
                        this.u.setFilterBitmap(true);
                    }
                }
            }
            bx.d(this.c);
        }
        if (this.l != this.k) {
            TextPaint textPaint = this.C;
            int i2 = this.k;
            int i3 = this.l;
            float f7 = 1.0f - f4;
            textPaint.setColor(Color.argb((int) ((((float) Color.alpha(i2)) * f7) + (((float) Color.alpha(i3)) * f4)), (int) ((((float) Color.red(i2)) * f7) + (((float) Color.red(i3)) * f4)), (int) ((((float) Color.green(i2)) * f7) + (((float) Color.green(i3)) * f4)), (int) ((((float) Color.blue(i2)) * f7) + (((float) Color.blue(i3)) * f4))));
        } else {
            this.C.setColor(this.l);
        }
        bx.d(this.c);
    }

    private void i() {
        if (this.t != null) {
            this.t.recycle();
            this.t = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.g != 80) {
            this.g = 80;
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(float f2) {
        if (this.i != f2) {
            this.i = f2;
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        if (this.l != i2) {
            this.l = i2;
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3, int i4, int i5) {
        this.e.set(i2, i3, i4, i5);
    }

    public final void a(Canvas canvas) {
        float f2;
        int save = canvas.save();
        if (this.p != null) {
            boolean z2 = this.r;
            float f3 = z2 ? this.y : this.x;
            float f4 = this.z;
            boolean z3 = this.s && this.t != null;
            this.C.setTextSize(this.B);
            if (z3) {
                f2 = this.v * this.A;
            } else {
                this.C.ascent();
                f2 = 0.0f;
                this.C.descent();
            }
            if (z3) {
                f4 += f2;
            }
            if (this.A != 1.0f) {
                canvas.scale(this.A, this.A, f3, f4);
            }
            float f5 = z2 ? f3 - this.q : f3;
            if (z3) {
                canvas.drawBitmap(this.t, f5, f4, this.u);
            } else {
                canvas.drawText(this.p, 0, this.p.length(), f5, f4, this.C);
            }
        }
        canvas.restoreToCount(save);
    }

    /* access modifiers changed from: package-private */
    public final void a(Interpolator interpolator) {
        this.E = interpolator;
        e();
    }

    /* access modifiers changed from: package-private */
    public final void a(CharSequence charSequence) {
        if (charSequence == null || !charSequence.equals(this.o)) {
            this.o = charSequence;
            this.p = null;
            i();
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.h != 48) {
            this.h = 48;
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(float f2) {
        if (f2 < 0.0f) {
            f2 = 0.0f;
        } else if (f2 > 1.0f) {
            f2 = 1.0f;
        }
        if (f2 != this.d) {
            this.d = f2;
            h();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2) {
        if (this.k != i2) {
            this.k = i2;
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2, int i3, int i4, int i5) {
        this.f.set(i2, i3, i4, i5);
    }

    /* access modifiers changed from: package-private */
    public final void b(Interpolator interpolator) {
        this.D = interpolator;
        e();
    }

    /* access modifiers changed from: package-private */
    public final float c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final void c(int i2) {
        TypedArray obtainStyledAttributes = this.c.getContext().obtainStyledAttributes(i2, android.support.design.i.aP);
        if (obtainStyledAttributes.hasValue(android.support.design.i.aQ)) {
            this.l = obtainStyledAttributes.getColor(android.support.design.i.aQ, 0);
        }
        if (obtainStyledAttributes.hasValue(android.support.design.i.aR)) {
            this.j = (float) obtainStyledAttributes.getDimensionPixelSize(android.support.design.i.aR, 0);
        }
        obtainStyledAttributes.recycle();
        e();
    }

    /* access modifiers changed from: package-private */
    public final float d() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public final void d(int i2) {
        TypedArray obtainStyledAttributes = this.c.getContext().obtainStyledAttributes(i2, android.support.design.i.aP);
        if (obtainStyledAttributes.hasValue(android.support.design.i.aQ)) {
            this.k = obtainStyledAttributes.getColor(android.support.design.i.aQ, 0);
        }
        if (obtainStyledAttributes.hasValue(android.support.design.i.aR)) {
            this.i = (float) obtainStyledAttributes.getDimensionPixelSize(android.support.design.i.aR, 0);
        }
        obtainStyledAttributes.recycle();
        e();
    }

    public final void e() {
        if (this.c.getHeight() > 0 && this.c.getWidth() > 0) {
            this.C.setTextSize(this.j);
            switch (this.h) {
                case 48:
                    this.n = ((float) this.f.top) - this.C.ascent();
                    break;
                case 80:
                    this.n = (float) this.f.bottom;
                    break;
                default:
                    this.n = (((this.C.descent() - this.C.ascent()) / 2.0f) - this.C.descent()) + ((float) this.f.centerY());
                    break;
            }
            this.C.setTextSize(this.i);
            switch (this.g) {
                case 48:
                    this.m = ((float) this.e.top) - this.C.ascent();
                    break;
                case 80:
                    this.m = (float) this.e.bottom;
                    break;
                default:
                    this.m = (((this.C.descent() - this.C.ascent()) / 2.0f) - this.C.descent()) + ((float) this.e.centerY());
                    break;
            }
            this.v = this.C.ascent();
            this.w = this.C.descent();
            i();
            h();
        }
    }

    /* access modifiers changed from: package-private */
    public final CharSequence f() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final int g() {
        return this.l;
    }
}
