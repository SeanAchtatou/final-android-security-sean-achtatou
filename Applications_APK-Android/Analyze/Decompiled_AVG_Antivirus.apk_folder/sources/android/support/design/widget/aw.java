package android.support.design.widget;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.android.gms.common.ConnectionResult;

final class aw {
    private static aw a;
    private final Object b = new Object();
    private final Handler c = new Handler(Looper.getMainLooper(), new ax(this));
    private az d;
    private az e;

    private aw() {
    }

    static aw a() {
        if (a == null) {
            a = new aw();
        }
        return a;
    }

    static /* synthetic */ void a(aw awVar, az azVar) {
        synchronized (awVar.b) {
            if (awVar.d == azVar || awVar.e == azVar) {
                a(azVar);
            }
        }
    }

    private static boolean a(az azVar) {
        return ((ay) azVar.a.get()) != null;
    }

    private void b(az azVar) {
        if (azVar.b != -2) {
            int i = 2750;
            if (azVar.b > 0) {
                i = azVar.b;
            } else if (azVar.b == -1) {
                i = ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED;
            }
            this.c.removeCallbacksAndMessages(azVar);
            this.c.sendMessageDelayed(Message.obtain(this.c, 0, azVar), (long) i);
        }
    }

    private boolean f(ay ayVar) {
        return this.d != null && this.d.a(ayVar);
    }

    public final void a(ay ayVar) {
        synchronized (this.b) {
            if (f(ayVar)) {
                a(this.d);
            }
            if (this.e != null && this.e.a(ayVar)) {
                a(this.e);
            }
        }
    }

    public final void b(ay ayVar) {
        synchronized (this.b) {
            if (f(ayVar)) {
                this.d = null;
                if (!(this.e == null || this.e == null)) {
                    this.d = this.e;
                    this.e = null;
                    if (((ay) this.d.a.get()) == null) {
                        this.d = null;
                    }
                }
            }
        }
    }

    public final void c(ay ayVar) {
        synchronized (this.b) {
            if (f(ayVar)) {
                b(this.d);
            }
        }
    }

    public final void d(ay ayVar) {
        synchronized (this.b) {
            if (f(ayVar)) {
                this.c.removeCallbacksAndMessages(this.d);
            }
        }
    }

    public final void e(ay ayVar) {
        synchronized (this.b) {
            if (f(ayVar)) {
                b(this.d);
            }
        }
    }
}
