package android.support.design.widget;

import android.graphics.drawable.Drawable;

final class x implements am {
    final /* synthetic */ FloatingActionButton a;

    x(FloatingActionButton floatingActionButton) {
        this.a = floatingActionButton;
    }

    public final float a() {
        return ((float) this.a.c()) / 2.0f;
    }

    public final void a(int i, int i2, int i3, int i4) {
        this.a.g.set(i, i2, i3, i4);
        this.a.setPadding(this.a.f + i, this.a.f + i2, this.a.f + i3, this.a.f + i4);
    }

    public final void a(Drawable drawable) {
        x.super.setBackgroundDrawable(drawable);
    }
}
