package android.support.design.widget;

import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.view.ViewGroup;

final class q implements ViewGroup.OnHierarchyChangeListener {
    final /* synthetic */ CoordinatorLayout a;

    q(CoordinatorLayout coordinatorLayout) {
        this.a = coordinatorLayout;
    }

    public final void onChildViewAdded(View view, View view2) {
        if (this.a.x != null) {
            this.a.x.onChildViewAdded(view, view2);
        }
    }

    public final void onChildViewRemoved(View view, View view2) {
        CoordinatorLayout coordinatorLayout = this.a;
        int childCount = coordinatorLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = coordinatorLayout.getChildAt(i);
            CoordinatorLayout.Behavior behavior = ((r) childAt.getLayoutParams()).a;
            if (behavior != null && behavior.a(view2)) {
                behavior.a(childAt, view2);
            }
        }
        if (this.a.x != null) {
            this.a.x.onChildViewRemoved(view, view2);
        }
    }
}
