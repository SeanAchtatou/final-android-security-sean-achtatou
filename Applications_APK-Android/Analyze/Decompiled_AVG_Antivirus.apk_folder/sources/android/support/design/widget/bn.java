package android.support.design.widget;

import android.view.animation.Interpolator;

final class bn {
    private final br a;

    bn(br brVar) {
        this.a = brVar;
    }

    public final void a() {
        this.a.a();
    }

    public final void a(float f, float f2) {
        this.a.a(f, f2);
    }

    public final void a(int i) {
        this.a.a(i);
    }

    public final void a(int i, int i2) {
        this.a.a(i, i2);
    }

    public final void a(bp bpVar) {
        this.a.a(new bo(this, bpVar));
    }

    public final void a(Interpolator interpolator) {
        this.a.a(interpolator);
    }

    public final boolean b() {
        return this.a.b();
    }

    public final int c() {
        return this.a.c();
    }

    public final float d() {
        return this.a.d();
    }

    public final void e() {
        this.a.e();
    }
}
