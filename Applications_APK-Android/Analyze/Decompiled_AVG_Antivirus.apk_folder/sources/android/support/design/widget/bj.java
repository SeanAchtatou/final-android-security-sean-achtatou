package android.support.design.widget;

import android.text.Editable;
import android.text.TextWatcher;

final class bj implements TextWatcher {
    final /* synthetic */ TextInputLayout a;

    bj(TextInputLayout textInputLayout) {
        this.a = textInputLayout;
    }

    public final void afterTextChanged(Editable editable) {
        this.a.i.sendEmptyMessage(0);
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
