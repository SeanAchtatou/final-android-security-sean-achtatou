package android.support.design.widget;

import android.support.v4.view.a;
import android.support.v4.view.a.f;
import android.text.TextUtils;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

final class bm extends a {
    final /* synthetic */ TextInputLayout a;

    private bm(TextInputLayout textInputLayout) {
        this.a = textInputLayout;
    }

    /* synthetic */ bm(TextInputLayout textInputLayout, byte b) {
        this(textInputLayout);
    }

    public final void a(View view, f fVar) {
        super.a(view, fVar);
        fVar.b((CharSequence) TextInputLayout.class.getSimpleName());
        CharSequence f = this.a.h.f();
        if (!TextUtils.isEmpty(f)) {
            fVar.c(f);
        }
        if (this.a.a != null) {
            fVar.d(this.a.a);
        }
        CharSequence text = this.a.d != null ? this.a.d.getText() : null;
        if (!TextUtils.isEmpty(text)) {
            fVar.p();
            fVar.e(text);
        }
    }

    public final void a(View view, AccessibilityEvent accessibilityEvent) {
        super.a(view, accessibilityEvent);
        accessibilityEvent.setClassName(TextInputLayout.class.getSimpleName());
    }

    public final void b(View view, AccessibilityEvent accessibilityEvent) {
        super.b(view, accessibilityEvent);
        CharSequence f = this.a.h.f();
        if (!TextUtils.isEmpty(f)) {
            accessibilityEvent.getText().add(f);
        }
    }
}
