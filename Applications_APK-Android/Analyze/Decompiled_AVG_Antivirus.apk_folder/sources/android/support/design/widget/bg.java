package android.support.design.widget;

import android.support.v4.view.bx;
import android.view.View;

final class bg implements Runnable {
    final /* synthetic */ bd a;
    private final View b;
    private final boolean c;

    bg(bd bdVar, View view, boolean z) {
        this.a = bdVar;
        this.b = view;
        this.c = z;
    }

    public final void run() {
        if (this.a.a != null && this.a.a.g()) {
            bx.a(this.b, this);
        } else if (this.c && this.a.b != null) {
            this.a.b.a();
        }
    }
}
