package android.support.design.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;

final class i extends h {
    private ColorStateList e;
    private PorterDuff.Mode f = PorterDuff.Mode.SRC_IN;
    private PorterDuffColorFilter g;

    i() {
    }

    private PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    public final void draw(Canvas canvas) {
        boolean z;
        if (this.g == null || this.a.getColorFilter() != null) {
            z = false;
        } else {
            this.a.setColorFilter(this.g);
            z = true;
        }
        super.draw(canvas);
        if (z) {
            this.a.setColorFilter(null);
        }
    }

    public final void getOutline(Outline outline) {
        copyBounds(this.b);
        outline.setOval(this.b);
    }

    public final void setTintList(ColorStateList colorStateList) {
        this.e = colorStateList;
        this.g = a(colorStateList, this.f);
        invalidateSelf();
    }

    public final void setTintMode(PorterDuff.Mode mode) {
        this.f = mode;
        this.g = a(this.e, mode);
        invalidateSelf();
    }
}
