package android.support.design.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

abstract class ab extends Animation {
    final /* synthetic */ z a;
    private float b;
    private float c;

    private ab(z zVar) {
        this.a = zVar;
    }

    /* synthetic */ ab(z zVar, byte b2) {
        this(zVar);
    }

    /* access modifiers changed from: protected */
    public abstract float a();

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        al alVar = this.a.a;
        alVar.a(this.b + (this.c * f), alVar.h);
    }

    public void reset() {
        super.reset();
        this.b = this.a.a.j;
        this.c = a() - this.b;
    }
}
