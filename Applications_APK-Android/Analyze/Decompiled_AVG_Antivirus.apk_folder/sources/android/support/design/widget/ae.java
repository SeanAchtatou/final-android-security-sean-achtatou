package android.support.design.widget;

import android.view.View;

class ae extends z {
    /* access modifiers changed from: private */
    public boolean g;

    ae(View view, am amVar) {
        super(view, amVar);
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (!this.g) {
            this.e.animate().scaleX(0.0f).scaleY(0.0f).alpha(0.0f).setDuration(200).setInterpolator(a.b).setListener(new af(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.e.animate().scaleX(1.0f).scaleY(1.0f).alpha(1.0f).setDuration(200).setInterpolator(a.b).setListener(null);
    }
}
