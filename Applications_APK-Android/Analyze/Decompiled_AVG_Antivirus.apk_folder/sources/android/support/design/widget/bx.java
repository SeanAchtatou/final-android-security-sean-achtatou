package android.support.design.widget;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;

final class bx {
    private static final by a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            a = new ca((byte) 0);
        } else {
            a = new bz((byte) 0);
        }
    }

    static void a(ViewGroup viewGroup, View view, Rect rect) {
        rect.set(0, 0, view.getWidth(), view.getHeight());
        a.a(viewGroup, view, rect);
    }
}
