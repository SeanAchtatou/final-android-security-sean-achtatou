package android.support.v7.a;

public final class f {

    /* renamed from: A */
    public static final int abc_spinner_textfield_background_material = 2130837552;

    /* renamed from: B */
    public static final int abc_switch_thumb_material = 2130837553;

    /* renamed from: C */
    public static final int abc_switch_track_mtrl_alpha = 2130837554;

    /* renamed from: D */
    public static final int abc_tab_indicator_material = 2130837555;

    /* renamed from: E */
    public static final int abc_text_cursor_mtrl_alpha = 2130837557;

    /* renamed from: F */
    public static final int abc_textfield_activated_mtrl_alpha = 2130837558;

    /* renamed from: G */
    public static final int abc_textfield_default_mtrl_alpha = 2130837559;

    /* renamed from: H */
    public static final int abc_textfield_search_activated_mtrl_alpha = 2130837560;

    /* renamed from: I */
    public static final int abc_textfield_search_default_mtrl_alpha = 2130837561;

    /* renamed from: J */
    public static final int abc_textfield_search_material = 2130837562;

    /* renamed from: a */
    public static final int abc_ab_share_pack_mtrl_alpha = 2130837504;

    /* renamed from: b */
    public static final int abc_btn_borderless_material = 2130837505;

    /* renamed from: c */
    public static final int abc_btn_check_material = 2130837506;
    public static final int d = 2130837509;
    public static final int e = 2130837510;

    /* renamed from: f */
    public static final int abc_cab_background_internal_bg = 2130837517;

    /* renamed from: g */
    public static final int abc_cab_background_top_material = 2130837518;

    /* renamed from: h */
    public static final int abc_cab_background_top_mtrl_alpha = 2130837519;

    /* renamed from: i */
    public static final int abc_edit_text_material = 2130837522;

    /* renamed from: j */
    public static final int abc_ic_ab_back_mtrl_am_alpha = 2130837523;

    /* renamed from: k */
    public static final int abc_ic_clear_mtrl_alpha = 2130837524;

    /* renamed from: l */
    public static final int abc_ic_commit_search_api_mtrl_alpha = 2130837525;

    /* renamed from: m */
    public static final int abc_ic_go_search_api_mtrl_alpha = 2130837526;

    /* renamed from: n */
    public static final int abc_ic_menu_copy_mtrl_am_alpha = 2130837527;

    /* renamed from: o */
    public static final int abc_ic_menu_cut_mtrl_alpha = 2130837528;

    /* renamed from: p */
    public static final int abc_ic_menu_moreoverflow_mtrl_alpha = 2130837529;

    /* renamed from: q */
    public static final int abc_ic_menu_paste_mtrl_am_alpha = 2130837530;

    /* renamed from: r */
    public static final int abc_ic_menu_selectall_mtrl_alpha = 2130837531;

    /* renamed from: s */
    public static final int abc_ic_menu_share_mtrl_alpha = 2130837532;

    /* renamed from: t */
    public static final int abc_ic_search_api_mtrl_alpha = 2130837533;

    /* renamed from: u */
    public static final int abc_ic_voice_search_api_mtrl_alpha = 2130837534;

    /* renamed from: v */
    public static final int abc_list_divider_mtrl_alpha = 2130837537;

    /* renamed from: w */
    public static final int abc_menu_hardkey_panel_mtrl_mult = 2130837548;

    /* renamed from: x */
    public static final int abc_popup_background_mtrl_mult = 2130837549;

    /* renamed from: y */
    public static final int abc_ratingbar_full_material = 2130837550;

    /* renamed from: z */
    public static final int abc_spinner_mtrl_am_alpha = 2130837551;
}
