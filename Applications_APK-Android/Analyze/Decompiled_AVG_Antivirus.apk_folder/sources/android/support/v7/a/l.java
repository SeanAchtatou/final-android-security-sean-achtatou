package android.support.v7.a;

import com.fastnet.browseralam.R;

public final class l {
    public static final int A = 4;
    public static final int B = 5;
    public static final int C = 0;
    public static final int D = 2;
    public static final int E = 1;
    public static final int[] F = {R.attr.initialActivityCount, R.attr.expandActivityOverflowButtonDrawable};
    public static final int G = 1;
    public static final int H = 0;
    public static final int[] I = {16842994, R.attr.buttonPanelSideLayout, R.attr.listLayout, R.attr.multiChoiceItemLayout, R.attr.singleChoiceItemLayout, R.attr.listItemLayout};
    public static final int J = 0;
    public static final int K = 1;
    public static final int L = 5;
    public static final int M = 2;
    public static final int N = 3;
    public static final int O = 4;
    public static final int[] P = {16842804, R.attr.textAllCaps};
    public static final int Q = 0;
    public static final int R = 1;
    public static final int[] S = {R.attr.color, R.attr.spinBars, R.attr.drawableSize, R.attr.gapBetweenBars, R.attr.topBottomBarArrowSize, R.attr.middleBarArrowSize, R.attr.barSize, R.attr.thickness};
    public static final int[] T = {16842927, 16842948, 16843046, 16843047, 16843048, R.attr.divider, R.attr.measureWithLargestChild, R.attr.showDividers, R.attr.dividerPadding};
    public static final int[] U = {16842931, 16842996, 16842997, 16843137};
    public static final int V = 0;
    public static final int W = 3;
    public static final int X = 2;
    public static final int Y = 3;
    public static final int Z = 0;
    public static final int[] a = {R.attr.height, R.attr.title, R.attr.navigationMode, R.attr.displayOptions, R.attr.subtitle, R.attr.titleTextStyle, R.attr.subtitleTextStyle, R.attr.icon, R.attr.logo, R.attr.divider, R.attr.background, R.attr.backgroundStacked, R.attr.backgroundSplit, R.attr.customNavigationLayout, R.attr.homeLayout, R.attr.progressBarStyle, R.attr.indeterminateProgressStyle, R.attr.progressBarPadding, R.attr.itemPadding, R.attr.hideOnContentScroll, R.attr.contentInsetStart, R.attr.contentInsetEnd, R.attr.contentInsetLeft, R.attr.contentInsetRight, R.attr.elevation, R.attr.popupTheme, R.attr.homeAsUpIndicator};
    public static final int aA = 5;
    public static final int aB = 10;
    public static final int aC = 12;
    public static final int aD = 6;
    public static final int aE = 7;
    public static final int aF = 8;
    public static final int aG = 4;
    public static final int aH = 13;
    public static final int[] aI = {16842926, 16843052, 16843053, 16843054, 16843055, 16843056, 16843057, R.attr.preserveIconSpacing};
    public static final int aJ = 5;
    public static final int aK = 1;
    public static final int aL = 7;
    public static final int[] aM = {16843126, R.attr.overlapAnchor};
    public static final int[] aN = {R.attr.state_above_anchor};
    public static final int aO = 0;
    public static final int aP = 1;
    public static final int[] aQ = {16842970, 16843039, 16843296, 16843364, R.attr.layout, R.attr.iconifiedByDefault, R.attr.queryHint, R.attr.defaultQueryHint, R.attr.closeIcon, R.attr.goIcon, R.attr.searchIcon, R.attr.searchHintIcon, R.attr.voiceIcon, R.attr.commitIcon, R.attr.suggestionRowLayout, R.attr.queryBackground, R.attr.submitBackground};
    public static final int aR = 0;
    public static final int aS = 3;
    public static final int aT = 2;
    public static final int aU = 1;
    public static final int aV = 8;
    public static final int aW = 13;
    public static final int aX = 7;
    public static final int aY = 9;
    public static final int aZ = 5;
    public static final int aa = 1;
    public static final int ab = 4;
    public static final int ac = 5;
    public static final int ad = 8;
    public static final int ae = 6;
    public static final int af = 7;
    public static final int[] ag = {16843436, 16843437};
    public static final int ah = 0;
    public static final int ai = 1;
    public static final int[] aj = {16842766, 16842960, 16843156, 16843230, 16843231, 16843232};
    public static final int ak = 5;
    public static final int al = 0;
    public static final int am = 1;
    public static final int an = 3;
    public static final int ao = 4;
    public static final int ap = 2;
    public static final int[] aq = {16842754, 16842766, 16842960, 16843014, 16843156, 16843230, 16843231, 16843233, 16843234, 16843235, 16843236, 16843237, 16843375, R.attr.showAsAction, R.attr.actionLayout, R.attr.actionViewClass, R.attr.actionProviderClass};
    public static final int ar = 14;
    public static final int as = 16;
    public static final int at = 15;
    public static final int au = 9;
    public static final int av = 11;
    public static final int aw = 3;
    public static final int ax = 1;
    public static final int ay = 0;
    public static final int az = 2;
    public static final int[] b = {16842931};
    public static final int bA = 3;
    public static final int[] bB = {16842901, 16842902, 16842903, 16842904, R.attr.textAllCaps};
    public static final int bC = 3;
    public static final int bD = 0;
    public static final int bE = 2;
    public static final int bF = 1;
    public static final int bG = 4;
    public static final int[] bH = {16842839, 16842926, R.attr.windowActionBar, R.attr.windowNoTitle, R.attr.windowActionBarOverlay, R.attr.windowActionModeOverlay, R.attr.windowFixedWidthMajor, R.attr.windowFixedHeightMinor, R.attr.windowFixedWidthMinor, R.attr.windowFixedHeightMajor, R.attr.windowMinWidthMajor, R.attr.windowMinWidthMinor, R.attr.actionBarTabStyle, R.attr.actionBarTabBarStyle, R.attr.actionBarTabTextStyle, R.attr.actionOverflowButtonStyle, R.attr.actionOverflowMenuStyle, R.attr.actionBarPopupTheme, R.attr.actionBarStyle, R.attr.actionBarSplitStyle, R.attr.actionBarTheme, R.attr.actionBarWidgetTheme, R.attr.actionBarSize, R.attr.actionBarDivider, R.attr.actionBarItemBackground, R.attr.actionMenuTextAppearance, R.attr.actionMenuTextColor, R.attr.actionModeStyle, R.attr.actionModeCloseButtonStyle, R.attr.actionModeBackground, R.attr.actionModeSplitBackground, R.attr.actionModeCloseDrawable, R.attr.actionModeCutDrawable, R.attr.actionModeCopyDrawable, R.attr.actionModePasteDrawable, R.attr.actionModeSelectAllDrawable, R.attr.actionModeShareDrawable, R.attr.actionModeFindDrawable, R.attr.actionModeWebSearchDrawable, R.attr.actionModePopupWindowStyle, R.attr.textAppearanceLargePopupMenu, R.attr.textAppearanceSmallPopupMenu, R.attr.dialogTheme, R.attr.dialogPreferredPadding, R.attr.listDividerAlertDialog, R.attr.actionDropDownStyle, R.attr.dropdownListPreferredItemHeight, R.attr.spinnerDropDownItemStyle, R.attr.homeAsUpIndicator, R.attr.actionButtonStyle, R.attr.buttonBarStyle, R.attr.buttonBarButtonStyle, R.attr.selectableItemBackground, R.attr.selectableItemBackgroundBorderless, R.attr.borderlessButtonStyle, R.attr.dividerVertical, R.attr.dividerHorizontal, R.attr.activityChooserViewStyle, R.attr.toolbarStyle, R.attr.toolbarNavigationButtonStyle, R.attr.popupMenuStyle, R.attr.popupWindowStyle, R.attr.editTextColor, R.attr.editTextBackground, R.attr.textAppearanceSearchResultTitle, R.attr.textAppearanceSearchResultSubtitle, R.attr.textColorSearchUrl, R.attr.searchViewStyle, R.attr.listPreferredItemHeight, R.attr.listPreferredItemHeightSmall, R.attr.listPreferredItemHeightLarge, R.attr.listPreferredItemPaddingLeft, R.attr.listPreferredItemPaddingRight, R.attr.dropDownListViewStyle, R.attr.listPopupWindowStyle, R.attr.textAppearanceListItem, R.attr.textAppearanceListItemSmall, R.attr.panelBackground, R.attr.panelMenuListWidth, R.attr.panelMenuListTheme, R.attr.listChoiceBackgroundIndicator, R.attr.colorPrimary, R.attr.colorPrimaryDark, R.attr.colorAccent, R.attr.colorControlNormal, R.attr.colorControlActivated, R.attr.colorControlHighlight, R.attr.colorButtonNormal, R.attr.colorSwitchThumbNormal, R.attr.alertDialogStyle, R.attr.alertDialogButtonGroupStyle, R.attr.alertDialogCenterButtons, R.attr.alertDialogTheme, R.attr.textColorAlertDialogListItem, R.attr.buttonBarPositiveButtonStyle, R.attr.buttonBarNegativeButtonStyle, R.attr.buttonBarNeutralButtonStyle, R.attr.autoCompleteTextViewStyle, R.attr.buttonStyle, R.attr.buttonStyleSmall, R.attr.checkboxStyle, R.attr.checkedTextViewStyle, R.attr.editTextStyle, R.attr.radioButtonStyle, R.attr.ratingBarStyle, R.attr.spinnerStyle, R.attr.switchStyle};
    public static final int bI = 1;
    public static final int bJ = 0;
    public static final int bK = 77;
    public static final int bL = 2;
    public static final int bM = 4;
    public static final int bN = 5;
    public static final int bO = 9;
    public static final int bP = 7;
    public static final int bQ = 6;
    public static final int bR = 8;
    public static final int bS = 10;
    public static final int bT = 11;
    public static final int bU = 3;
    public static final int[] bV = {16842927, 16843072, R.attr.title, R.attr.subtitle, R.attr.contentInsetStart, R.attr.contentInsetEnd, R.attr.contentInsetLeft, R.attr.contentInsetRight, R.attr.popupTheme, R.attr.titleTextAppearance, R.attr.subtitleTextAppearance, R.attr.titleMargins, R.attr.titleMarginStart, R.attr.titleMarginEnd, R.attr.titleMarginTop, R.attr.titleMarginBottom, R.attr.maxButtonHeight, R.attr.collapseIcon, R.attr.collapseContentDescription, R.attr.navigationIcon, R.attr.navigationContentDescription};
    public static final int bW = 0;
    public static final int bX = 18;
    public static final int bY = 17;
    public static final int bZ = 5;
    public static final int ba = 4;
    public static final int bb = 15;
    public static final int bc = 6;
    public static final int bd = 11;
    public static final int be = 10;
    public static final int bf = 16;
    public static final int bg = 14;
    public static final int bh = 12;
    public static final int[] bi = {16842927, 16842964, 16843125, 16843126, 16843362, 16843436, 16843437, R.attr.prompt, R.attr.spinnerMode, R.attr.popupPromptView, R.attr.disableChildrenWhenDisabled};
    public static final int bj = 1;
    public static final int bk = 4;
    public static final int bl = 0;
    public static final int bm = 3;
    public static final int bn = 10;
    public static final int bo = 7;
    public static final int bp = 8;
    public static final int[] bq = {16843044, 16843045, 16843074, R.attr.track, R.attr.thumbTextPadding, R.attr.switchTextAppearance, R.attr.switchMinWidth, R.attr.switchPadding, R.attr.splitTrack, R.attr.showText};
    public static final int br = 1;
    public static final int bs = 0;
    public static final int bt = 2;
    public static final int bu = 9;
    public static final int bv = 8;
    public static final int bw = 6;
    public static final int bx = 7;
    public static final int by = 5;
    public static final int bz = 4;
    public static final int c = 0;
    public static final int ca = 6;
    public static final int cb = 7;
    public static final int cc = 4;
    public static final int cd = 16;
    public static final int ce = 20;
    public static final int cf = 19;
    public static final int cg = 8;
    public static final int ch = 3;
    public static final int ci = 10;
    public static final int cj = 2;
    public static final int ck = 15;
    public static final int cl = 13;
    public static final int cm = 12;
    public static final int cn = 14;
    public static final int co = 11;
    public static final int cp = 9;
    public static final int[] cq = {16842752, 16842970, R.attr.paddingStart, R.attr.paddingEnd, R.attr.theme, R.attr.backgroundTint, R.attr.backgroundTintMode};
    public static final int[] cr = {16842960, 16842994, 16842995};
    public static final int cs = 0;
    public static final int ct = 2;
    public static final int cu = 1;
    public static final int cv = 0;
    public static final int cw = 4;
    public static final int d = 10;
    public static final int e = 12;
    public static final int f = 11;
    public static final int g = 21;
    public static final int h = 20;
    public static final int i = 13;
    public static final int j = 3;
    public static final int k = 24;
    public static final int l = 0;
    public static final int m = 19;
    public static final int n = 26;
    public static final int o = 7;
    public static final int p = 8;
    public static final int q = 25;
    public static final int r = 4;
    public static final int s = 6;
    public static final int t = 1;
    public static final int u = 5;
    public static final int[] v = {16843071};
    public static final int w = 0;
    public static final int[] x = new int[0];
    public static final int[] y = {R.attr.height, R.attr.titleTextStyle, R.attr.subtitleTextStyle, R.attr.background, R.attr.backgroundSplit, R.attr.closeItemLayout};
    public static final int z = 3;
}
