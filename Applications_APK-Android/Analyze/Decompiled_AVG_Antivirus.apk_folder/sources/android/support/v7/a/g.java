package android.support.v7.a;

public final class g {

    /* renamed from: A */
    public static final int search_go_btn = 2131558500;

    /* renamed from: B */
    public static final int search_mag_icon = 2131558495;

    /* renamed from: C */
    public static final int search_plate = 2131558496;

    /* renamed from: D */
    public static final int search_src_text = 2131558497;

    /* renamed from: E */
    public static final int search_voice_btn = 2131558501;

    /* renamed from: F */
    public static final int shortcut = 2131558481;

    /* renamed from: G */
    public static final int split_action_bar = 2131558408;

    /* renamed from: H */
    public static final int submit_area = 2131558499;

    /* renamed from: I */
    public static final int textSpacerNoButtons = 2131558475;

    /* renamed from: J */
    public static final int title = 2131558468;

    /* renamed from: K */
    public static final int title_template = 2131558471;

    /* renamed from: L */
    public static final int topPanel = 2131558470;

    /* renamed from: a */
    public static final int action_bar = 2131558488;

    /* renamed from: b */
    public static final int action_bar_activity_content = 2131558400;

    /* renamed from: c */
    public static final int action_bar_container = 2131558487;

    /* renamed from: d */
    public static final int action_bar_subtitle = 2131558460;

    /* renamed from: e */
    public static final int action_bar_title = 2131558459;

    /* renamed from: f */
    public static final int action_context_bar = 2131558489;

    /* renamed from: g */
    public static final int action_menu_presenter = 2131558403;

    /* renamed from: h */
    public static final int action_mode_bar_stub = 2131558484;

    /* renamed from: i */
    public static final int action_mode_close_button = 2131558461;

    /* renamed from: j */
    public static final int activity_chooser_view_content = 2131558462;

    /* renamed from: k */
    public static final int alertTitle = 2131558472;

    /* renamed from: l */
    public static final int buttonPanel = 2131558478;

    /* renamed from: m */
    public static final int contentPanel = 2131558473;

    /* renamed from: n */
    public static final int custom = 2131558477;

    /* renamed from: o */
    public static final int customPanel = 2131558476;

    /* renamed from: p */
    public static final int decor_content_parent = 2131558486;

    /* renamed from: q */
    public static final int default_activity_button = 2131558465;

    /* renamed from: r */
    public static final int edit_query = 2131558490;

    /* renamed from: s */
    public static final int expand_activities_button = 2131558463;

    /* renamed from: t */
    public static final int icon = 2131558467;

    /* renamed from: u */
    public static final int image = 2131558464;

    /* renamed from: v */
    public static final int list_item = 2131558466;

    /* renamed from: w */
    public static final int scrollView = 2131558474;

    /* renamed from: x */
    public static final int search_button = 2131558493;

    /* renamed from: y */
    public static final int search_close_btn = 2131558498;

    /* renamed from: z */
    public static final int search_edit_frame = 2131558494;
}
