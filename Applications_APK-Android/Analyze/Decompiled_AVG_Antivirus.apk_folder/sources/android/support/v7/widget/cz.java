package android.support.v7.widget;

import android.view.View;

public abstract class cz extends bm {
    boolean a = true;

    public abstract boolean a(cf cfVar);

    public abstract boolean a(cf cfVar, int i, int i2, int i3, int i4);

    public final boolean a(cf cfVar, bo boVar, bo boVar2) {
        int i = boVar.a;
        int i2 = boVar.b;
        View view = cfVar.a;
        int left = boVar2 == null ? view.getLeft() : boVar2.a;
        int top = boVar2 == null ? view.getTop() : boVar2.b;
        if (cfVar.m() || (i == left && i2 == top)) {
            return a(cfVar);
        }
        view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
        return a(cfVar, i, i2, left, top);
    }

    public abstract boolean a(cf cfVar, cf cfVar2, int i, int i2, int i3, int i4);

    public final boolean a(cf cfVar, cf cfVar2, bo boVar, bo boVar2) {
        int i;
        int i2;
        int i3 = boVar.a;
        int i4 = boVar.b;
        if (cfVar2.d_()) {
            i = boVar.a;
            i2 = boVar.b;
        } else {
            i = boVar2.a;
            i2 = boVar2.b;
        }
        return a(cfVar, cfVar2, i3, i4, i, i2);
    }

    public abstract boolean b(cf cfVar);

    public final boolean b(cf cfVar, bo boVar, bo boVar2) {
        if (boVar == null || (boVar.a == boVar2.a && boVar.b == boVar2.b)) {
            return b(cfVar);
        }
        return a(cfVar, boVar.a, boVar.b, boVar2.a, boVar2.b);
    }

    public final boolean c(cf cfVar, bo boVar, bo boVar2) {
        if (boVar.a == boVar2.a && boVar.b == boVar2.b) {
            e(cfVar);
            return false;
        }
        return a(cfVar, boVar.a, boVar.b, boVar2.a, boVar2.b);
    }

    public final boolean f(cf cfVar) {
        return !this.a || cfVar.j();
    }

    public void g(cf cfVar) {
    }

    public final void k() {
        this.a = false;
    }
}
