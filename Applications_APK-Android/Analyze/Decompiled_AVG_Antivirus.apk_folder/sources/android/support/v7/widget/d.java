package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.b.a.a;
import android.support.v7.a.b;
import android.support.v7.internal.widget.TintImageView;

final class d extends TintImageView implements i {
    final /* synthetic */ ActionMenuPresenter a;
    private final float[] b = new float[2];

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(ActionMenuPresenter actionMenuPresenter, Context context) {
        super(context, null, b.actionOverflowButtonStyle);
        this.a = actionMenuPresenter;
        setClickable(true);
        setFocusable(true);
        setVisibility(0);
        setEnabled(true);
        setOnTouchListener(new e(this, this, actionMenuPresenter));
    }

    public final boolean d() {
        return false;
    }

    public final boolean e() {
        return false;
    }

    public final boolean performClick() {
        if (!super.performClick()) {
            playSoundEffect(0);
            this.a.i();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean setFrame(int i, int i2, int i3, int i4) {
        boolean frame = super.setFrame(i, i2, i3, i4);
        Drawable drawable = getDrawable();
        Drawable background = getBackground();
        if (!(drawable == null || background == null)) {
            int width = getWidth();
            int height = getHeight();
            int max = Math.max(width, height) / 2;
            int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
            int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
            a.a(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
        }
        return frame;
    }
}
