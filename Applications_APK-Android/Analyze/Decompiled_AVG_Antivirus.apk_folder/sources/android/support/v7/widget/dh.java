package android.support.v7.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

final class dh extends Animation {
    final /* synthetic */ float a;
    final /* synthetic */ float b;
    final /* synthetic */ SwitchCompat c;

    dh(SwitchCompat switchCompat, float f, float f2) {
        this.c = switchCompat;
        this.a = f;
        this.b = f2;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        this.c.a(this.a + (this.b * f));
    }
}
