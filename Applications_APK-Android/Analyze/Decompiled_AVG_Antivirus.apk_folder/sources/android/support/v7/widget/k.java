package android.support.v7.widget;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.view.MenuItem;

final class k implements j {
    final /* synthetic */ ActionMenuView a;

    private k(ActionMenuView actionMenuView) {
        this.a = actionMenuView;
    }

    /* synthetic */ k(ActionMenuView actionMenuView, byte b) {
        this(actionMenuView);
    }

    public final void a(i iVar) {
        if (this.a.h != null) {
            this.a.h.a(iVar);
        }
    }

    public final boolean a(i iVar, MenuItem menuItem) {
        return this.a.m != null && this.a.m.a(menuItem);
    }
}
