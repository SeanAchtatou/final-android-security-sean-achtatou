package android.support.v7.widget;

import android.app.PendingIntent;
import android.app.SearchableInfo;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.z;
import android.support.v4.widget.h;
import android.support.v7.a.b;
import android.support.v7.a.e;
import android.support.v7.a.g;
import android.support.v7.a.i;
import android.support.v7.a.l;
import android.support.v7.d.c;
import android.support.v7.internal.widget.bc;
import android.support.v7.internal.widget.be;
import android.support.v7.internal.widget.bj;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.actions.SearchIntents;
import java.util.WeakHashMap;

public class SearchView extends LinearLayoutCompat implements c {
    static final cv a = new cv();
    private static final boolean c = (Build.VERSION.SDK_INT >= 8);
    /* access modifiers changed from: private */
    public h A;
    private boolean B;
    private CharSequence C;
    private boolean D;
    private int E;
    private boolean F;
    private CharSequence G;
    private CharSequence H;
    private boolean I;
    private int J;
    /* access modifiers changed from: private */
    public SearchableInfo K;
    private Bundle L;
    private final bc M;
    private Runnable N;
    private final Runnable O;
    private Runnable P;
    private final WeakHashMap Q;
    private final View.OnClickListener R;
    private final TextView.OnEditorActionListener S;
    private final AdapterView.OnItemClickListener T;
    private final AdapterView.OnItemSelectedListener U;
    private TextWatcher V;
    View.OnKeyListener b;
    /* access modifiers changed from: private */
    public final SearchAutoComplete d;
    private final View e;
    private final View f;
    private final View g;
    /* access modifiers changed from: private */
    public final ImageView h;
    /* access modifiers changed from: private */
    public final ImageView i;
    /* access modifiers changed from: private */
    public final ImageView j;
    /* access modifiers changed from: private */
    public final ImageView k;
    private final View l;
    private final ImageView m;
    private final Drawable n;
    private final int o;
    private final int p;
    private final Intent q;
    private final Intent r;
    private final CharSequence s;
    private cx t;
    private cw u;
    /* access modifiers changed from: private */
    public View.OnFocusChangeListener v;
    private cy w;
    private View.OnClickListener x;
    private boolean y;
    private boolean z;

    public class SearchAutoComplete extends AppCompatAutoCompleteTextView {
        private int a;
        private SearchView b;

        public SearchAutoComplete(Context context) {
            this(context, null);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            this(context, attributeSet, b.autoCompleteTextViewStyle);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            this.a = getThreshold();
        }

        static /* synthetic */ boolean a(SearchAutoComplete searchAutoComplete) {
            return TextUtils.getTrimmedLength(searchAutoComplete.getText()) == 0;
        }

        /* access modifiers changed from: package-private */
        public final void a(SearchView searchView) {
            this.b = searchView;
        }

        public boolean enoughToFilter() {
            return this.a <= 0 || super.enoughToFilter();
        }

        /* access modifiers changed from: protected */
        public void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            this.b.c();
        }

        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            if (i == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    KeyEvent.DispatcherState keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState == null) {
                        return true;
                    }
                    keyDispatcherState.startTracking(keyEvent, this);
                    return true;
                } else if (keyEvent.getAction() == 1) {
                    KeyEvent.DispatcherState keyDispatcherState2 = getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.b.clearFocus();
                        this.b.c(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i, keyEvent);
        }

        public void onWindowFocusChanged(boolean z) {
            super.onWindowFocusChanged(z);
            if (z && this.b.hasFocus() && getVisibility() == 0) {
                ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0);
                if (SearchView.a(getContext())) {
                    SearchView.a.c(this);
                }
            }
        }

        public void performCompletion() {
        }

        /* access modifiers changed from: protected */
        public void replaceText(CharSequence charSequence) {
        }

        public void setThreshold(int i) {
            super.setThreshold(i);
            this.a = i;
        }
    }

    public SearchView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.searchViewStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.SearchView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.be.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.be.a(int, int):int
      android.support.v7.internal.widget.be.a(int, boolean):boolean */
    public SearchView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.N = new cj(this);
        this.O = new cn(this);
        this.P = new co(this);
        this.Q = new WeakHashMap();
        this.R = new cs(this);
        this.b = new ct(this);
        this.S = new cu(this);
        this.T = new ck(this);
        this.U = new cl(this);
        this.V = new cm(this);
        be a2 = be.a(context, attributeSet, l.aQ, i2);
        this.M = a2.c();
        LayoutInflater.from(context).inflate(a2.f(l.ba, i.abc_search_view), (ViewGroup) this, true);
        this.d = (SearchAutoComplete) findViewById(g.search_src_text);
        this.d.a(this);
        this.e = findViewById(g.search_edit_frame);
        this.f = findViewById(g.search_plate);
        this.g = findViewById(g.submit_area);
        this.h = (ImageView) findViewById(g.search_button);
        this.i = (ImageView) findViewById(g.search_go_btn);
        this.j = (ImageView) findViewById(g.search_close_btn);
        this.k = (ImageView) findViewById(g.search_voice_btn);
        this.m = (ImageView) findViewById(g.search_mag_icon);
        this.f.setBackgroundDrawable(a2.a(l.bb));
        this.g.setBackgroundDrawable(a2.a(l.bf));
        this.h.setImageDrawable(a2.a(l.be));
        this.i.setImageDrawable(a2.a(l.aY));
        this.j.setImageDrawable(a2.a(l.aV));
        this.k.setImageDrawable(a2.a(l.bh));
        this.m.setImageDrawable(a2.a(l.be));
        this.n = a2.a(l.bd);
        this.o = a2.f(l.bg, i.abc_search_dropdown_item_icons_2line);
        this.p = a2.f(l.aW, 0);
        this.h.setOnClickListener(this.R);
        this.j.setOnClickListener(this.R);
        this.i.setOnClickListener(this.R);
        this.k.setOnClickListener(this.R);
        this.d.setOnClickListener(this.R);
        this.d.addTextChangedListener(this.V);
        this.d.setOnEditorActionListener(this.S);
        this.d.setOnItemClickListener(this.T);
        this.d.setOnItemSelectedListener(this.U);
        this.d.setOnKeyListener(this.b);
        this.d.setOnFocusChangeListener(new cp(this));
        boolean a3 = a2.a(l.aZ, true);
        if (this.y != a3) {
            this.y = a3;
            a(a3);
            i();
        }
        int d2 = a2.d(l.aU, -1);
        if (d2 != -1) {
            this.E = d2;
            requestLayout();
        }
        this.s = a2.c(l.aX);
        this.C = a2.c(l.bc);
        int a4 = a2.a(l.aS, -1);
        if (a4 != -1) {
            this.d.setImeOptions(a4);
        }
        int a5 = a2.a(l.aT, -1);
        if (a5 != -1) {
            this.d.setInputType(a5);
        }
        setFocusable(a2.a(l.aR, true));
        a2.b();
        this.q = new Intent("android.speech.action.WEB_SEARCH");
        this.q.addFlags(268435456);
        this.q.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
        this.r = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        this.r.addFlags(268435456);
        this.l = findViewById(this.d.getDropDownAnchor());
        if (this.l != null) {
            if (Build.VERSION.SDK_INT >= 11) {
                this.l.addOnLayoutChangeListener(new cq(this));
            } else {
                this.l.getViewTreeObserver().addOnGlobalLayoutListener(new cr(this));
            }
        }
        a(this.y);
        i();
    }

    private Intent a(Cursor cursor) {
        int i2;
        String a2;
        try {
            String a3 = df.a(cursor, "suggest_intent_action");
            if (a3 == null && Build.VERSION.SDK_INT >= 8) {
                a3 = this.K.getSuggestIntentAction();
            }
            String str = a3 == null ? "android.intent.action.SEARCH" : a3;
            String a4 = df.a(cursor, "suggest_intent_data");
            if (c && a4 == null) {
                a4 = this.K.getSuggestIntentData();
            }
            if (!(a4 == null || (a2 = df.a(cursor, "suggest_intent_data_id")) == null)) {
                a4 = a4 + "/" + Uri.encode(a2);
            }
            return a(str, a4 == null ? null : Uri.parse(a4), df.a(cursor, "suggest_intent_extra_data"), df.a(cursor, "suggest_intent_query"));
        } catch (RuntimeException e2) {
            RuntimeException runtimeException = e2;
            try {
                i2 = cursor.getPosition();
            } catch (RuntimeException e3) {
                i2 = -1;
            }
            Log.w("SearchView", "Search suggestions cursor at row " + i2 + " returned exception.", runtimeException);
            return null;
        }
    }

    private Intent a(String str, Uri uri, String str2, String str3) {
        Intent intent = new Intent(str);
        intent.addFlags(268435456);
        if (uri != null) {
            intent.setData(uri);
        }
        intent.putExtra("user_query", this.H);
        if (str3 != null) {
            intent.putExtra(SearchIntents.EXTRA_QUERY, str3);
        }
        if (str2 != null) {
            intent.putExtra("intent_extra_data_key", str2);
        }
        if (this.L != null) {
            intent.putExtra("app_data", this.L);
        }
        if (c) {
            intent.setComponent(this.K.getSearchActivity());
        }
        return intent;
    }

    static /* synthetic */ void a(SearchView searchView) {
        int[] iArr = searchView.d.hasFocus() ? FOCUSED_STATE_SET : EMPTY_STATE_SET;
        Drawable background = searchView.f.getBackground();
        if (background != null) {
            background.setState(iArr);
        }
        Drawable background2 = searchView.g.getBackground();
        if (background2 != null) {
            background2.setState(iArr);
        }
        searchView.invalidate();
    }

    static /* synthetic */ void a(SearchView searchView, CharSequence charSequence) {
        boolean z2 = true;
        Editable text = searchView.d.getText();
        searchView.H = text;
        boolean z3 = !TextUtils.isEmpty(text);
        searchView.b(z3);
        if (z3) {
            z2 = false;
        }
        searchView.d(z2);
        searchView.g();
        searchView.f();
        if (searchView.t != null && !TextUtils.equals(charSequence, searchView.G)) {
            charSequence.toString();
        }
        searchView.G = charSequence.toString();
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        getContext().startActivity(a("android.intent.action.SEARCH", null, null, str));
    }

    private void a(boolean z2) {
        boolean z3 = true;
        int i2 = 8;
        this.z = z2;
        int i3 = z2 ? 0 : 8;
        boolean z4 = !TextUtils.isEmpty(this.d.getText());
        this.h.setVisibility(i3);
        b(z4);
        this.e.setVisibility(z2 ? 8 : 0);
        ImageView imageView = this.m;
        if (!this.y) {
            i2 = 0;
        }
        imageView.setVisibility(i2);
        g();
        if (z4) {
            z3 = false;
        }
        d(z3);
        f();
    }

    /* access modifiers changed from: private */
    public boolean a(int i2) {
        Intent a2;
        if (this.w != null && this.w.b()) {
            return false;
        }
        Cursor a3 = this.A.a();
        if (!(a3 == null || !a3.moveToPosition(i2) || (a2 = a(a3)) == null)) {
            try {
                getContext().startActivity(a2);
            } catch (RuntimeException e2) {
                Log.e("SearchView", "Failed launch activity: " + a2, e2);
            }
        }
        c(false);
        this.d.dismissDropDown();
        return true;
    }

    static boolean a(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }

    static /* synthetic */ boolean a(SearchView searchView, int i2, KeyEvent keyEvent) {
        if (searchView.K == null || searchView.A == null || keyEvent.getAction() != 0 || !z.b(keyEvent)) {
            return false;
        }
        if (i2 == 66 || i2 == 84 || i2 == 61) {
            return searchView.a(searchView.d.getListSelection());
        }
        if (i2 == 21 || i2 == 22) {
            searchView.d.setSelection(i2 == 21 ? 0 : searchView.d.length());
            searchView.d.setListSelection(0);
            searchView.d.clearListSelection();
            a.c(searchView.d);
            return true;
        } else if (i2 != 19) {
            return false;
        } else {
            searchView.d.getListSelection();
            return false;
        }
    }

    private void b(CharSequence charSequence) {
        this.d.setText(charSequence);
        this.d.setSelection(TextUtils.isEmpty(charSequence) ? 0 : charSequence.length());
    }

    private void b(boolean z2) {
        int i2 = 8;
        if (this.B && e() && hasFocus() && (z2 || !this.F)) {
            i2 = 0;
        }
        this.i.setVisibility(i2);
    }

    static /* synthetic */ boolean b(SearchView searchView, int i2) {
        if (searchView.w != null && searchView.w.a()) {
            return false;
        }
        Editable text = searchView.d.getText();
        Cursor a2 = searchView.A.a();
        if (a2 != null) {
            if (a2.moveToPosition(i2)) {
                CharSequence b2 = searchView.A.b(a2);
                if (b2 != null) {
                    searchView.b(b2);
                } else {
                    searchView.b(text);
                }
            } else {
                searchView.b(text);
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        if (z2) {
            post(this.N);
            return;
        }
        removeCallbacks(this.N);
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
    }

    private int d() {
        return getContext().getResources().getDimensionPixelSize(e.abc_search_view_preferred_width);
    }

    static /* synthetic */ void d(SearchView searchView) {
        if (searchView.l.getWidth() > 1) {
            Resources resources = searchView.getContext().getResources();
            int paddingLeft = searchView.f.getPaddingLeft();
            Rect rect = new Rect();
            boolean a2 = bj.a(searchView);
            int dimensionPixelSize = searchView.y ? resources.getDimensionPixelSize(e.abc_dropdownitem_text_padding_left) + resources.getDimensionPixelSize(e.abc_dropdownitem_icon_width) : 0;
            searchView.d.getDropDownBackground().getPadding(rect);
            searchView.d.setDropDownHorizontalOffset(a2 ? -rect.left : paddingLeft - (rect.left + dimensionPixelSize));
            searchView.d.setDropDownWidth((dimensionPixelSize + ((searchView.l.getWidth() + rect.left) + rect.right)) - paddingLeft);
        }
    }

    private void d(boolean z2) {
        int i2;
        if (!this.F || this.z || !z2) {
            i2 = 8;
        } else {
            i2 = 0;
            this.i.setVisibility(8);
        }
        this.k.setVisibility(i2);
    }

    private boolean e() {
        return (this.B || this.F) && !this.z;
    }

    private void f() {
        int i2 = 8;
        if (e() && (this.i.getVisibility() == 0 || this.k.getVisibility() == 0)) {
            i2 = 0;
        }
        this.g.setVisibility(i2);
    }

    private void g() {
        boolean z2 = true;
        int i2 = 0;
        boolean z3 = !TextUtils.isEmpty(this.d.getText());
        if (!z3 && (!this.y || this.I)) {
            z2 = false;
        }
        ImageView imageView = this.j;
        if (!z2) {
            i2 = 8;
        }
        imageView.setVisibility(i2);
        Drawable drawable = this.j.getDrawable();
        if (drawable != null) {
            drawable.setState(z3 ? ENABLED_STATE_SET : EMPTY_STATE_SET);
        }
    }

    private void h() {
        post(this.O);
    }

    static /* synthetic */ void h(SearchView searchView) {
        if (!TextUtils.isEmpty(searchView.d.getText())) {
            searchView.d.setText("");
            searchView.d.requestFocus();
            searchView.c(true);
        } else if (!searchView.y) {
        } else {
            if (searchView.u == null || !searchView.u.a()) {
                searchView.clearFocus();
                searchView.a(true);
            }
        }
    }

    private void i() {
        SpannableStringBuilder text = this.C != null ? this.C : (!c || this.K == null || this.K.getHintId() == 0) ? this.s : getContext().getText(this.K.getHintId());
        SearchAutoComplete searchAutoComplete = this.d;
        if (text == null) {
            text = "";
        }
        if (this.y && this.n != null) {
            int textSize = (int) (((double) this.d.getTextSize()) * 1.25d);
            this.n.setBounds(0, 0, textSize, textSize);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
            spannableStringBuilder.setSpan(new ImageSpan(this.n), 1, 2, 33);
            spannableStringBuilder.append(text);
            text = spannableStringBuilder;
        }
        searchAutoComplete.setHint(text);
    }

    /* access modifiers changed from: private */
    public void j() {
        a(false);
        this.d.requestFocus();
        c(true);
        if (this.x != null) {
            this.x.onClick(this);
        }
    }

    static /* synthetic */ void j(SearchView searchView) {
        Editable text = searchView.d.getText();
        if (text != null && TextUtils.getTrimmedLength(text) > 0) {
            if (searchView.t != null) {
                cx cxVar = searchView.t;
                text.toString();
                if (cxVar.a()) {
                    return;
                }
            }
            if (searchView.K != null) {
                searchView.a(text.toString());
            }
            searchView.c(false);
            searchView.d.dismissDropDown();
        }
    }

    static /* synthetic */ void l(SearchView searchView) {
        String str;
        String str2;
        String str3;
        String str4 = null;
        if (searchView.K != null) {
            SearchableInfo searchableInfo = searchView.K;
            try {
                if (searchableInfo.getVoiceSearchLaunchWebSearch()) {
                    Intent intent = new Intent(searchView.q);
                    ComponentName searchActivity = searchableInfo.getSearchActivity();
                    if (searchActivity != null) {
                        str4 = searchActivity.flattenToShortString();
                    }
                    intent.putExtra("calling_package", str4);
                    searchView.getContext().startActivity(intent);
                } else if (searchableInfo.getVoiceSearchLaunchRecognizer()) {
                    Intent intent2 = searchView.r;
                    ComponentName searchActivity2 = searchableInfo.getSearchActivity();
                    Intent intent3 = new Intent("android.intent.action.SEARCH");
                    intent3.setComponent(searchActivity2);
                    PendingIntent activity = PendingIntent.getActivity(searchView.getContext(), 0, intent3, 1073741824);
                    Bundle bundle = new Bundle();
                    if (searchView.L != null) {
                        bundle.putParcelable("app_data", searchView.L);
                    }
                    Intent intent4 = new Intent(intent2);
                    int i2 = 1;
                    if (Build.VERSION.SDK_INT >= 8) {
                        Resources resources = searchView.getResources();
                        str2 = searchableInfo.getVoiceLanguageModeId() != 0 ? resources.getString(searchableInfo.getVoiceLanguageModeId()) : "free_form";
                        str = searchableInfo.getVoicePromptTextId() != 0 ? resources.getString(searchableInfo.getVoicePromptTextId()) : null;
                        str3 = searchableInfo.getVoiceLanguageId() != 0 ? resources.getString(searchableInfo.getVoiceLanguageId()) : null;
                        if (searchableInfo.getVoiceMaxResults() != 0) {
                            i2 = searchableInfo.getVoiceMaxResults();
                        }
                    } else {
                        str = null;
                        str2 = "free_form";
                        str3 = null;
                    }
                    intent4.putExtra("android.speech.extra.LANGUAGE_MODEL", str2);
                    intent4.putExtra("android.speech.extra.PROMPT", str);
                    intent4.putExtra("android.speech.extra.LANGUAGE", str3);
                    intent4.putExtra("android.speech.extra.MAX_RESULTS", i2);
                    if (searchActivity2 != null) {
                        str4 = searchActivity2.flattenToShortString();
                    }
                    intent4.putExtra("calling_package", str4);
                    intent4.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", activity);
                    intent4.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", bundle);
                    searchView.getContext().startActivity(intent4);
                }
            } catch (ActivityNotFoundException e2) {
                Log.w("SearchView", "Could not find voice search activity");
            }
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        a.a(this.d);
        a.b(this.d);
    }

    public final void a() {
        if (!this.I) {
            this.I = true;
            this.J = this.d.getImeOptions();
            this.d.setImeOptions(this.J | 33554432);
            this.d.setText("");
            j();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(CharSequence charSequence) {
        b(charSequence);
    }

    public final void b() {
        this.d.setText("");
        this.d.setSelection(this.d.length());
        this.H = "";
        clearFocus();
        a(true);
        this.d.setImeOptions(this.J);
        this.I = false;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        a(this.z);
        h();
        if (this.d.hasFocus()) {
            n();
        }
    }

    public void clearFocus() {
        this.D = true;
        c(false);
        super.clearFocus();
        this.d.clearFocus();
        this.D = false;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.O);
        post(this.P);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.z) {
            super.onMeasure(i2, i3);
            return;
        }
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (this.E <= 0) {
                    size = Math.min(d(), size);
                    break;
                } else {
                    size = Math.min(this.E, size);
                    break;
                }
            case 0:
                if (this.E <= 0) {
                    size = d();
                    break;
                } else {
                    size = this.E;
                    break;
                }
            case 1073741824:
                if (this.E > 0) {
                    size = Math.min(this.E, size);
                    break;
                }
                break;
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), i3);
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        h();
    }

    public boolean requestFocus(int i2, Rect rect) {
        if (this.D || !isFocusable()) {
            return false;
        }
        if (this.z) {
            return super.requestFocus(i2, rect);
        }
        boolean requestFocus = this.d.requestFocus(i2, rect);
        if (requestFocus) {
            a(false);
        }
        return requestFocus;
    }
}
