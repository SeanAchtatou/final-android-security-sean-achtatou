package android.support.v7.widget;

import android.os.Bundle;
import android.support.v4.view.a;
import android.support.v4.view.a.f;
import android.support.v4.view.a.q;
import android.support.v4.view.bx;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

public final class cg extends a {
    final RecyclerView a;
    final a c = new ch(this);

    public cg(RecyclerView recyclerView) {
        this.a = recyclerView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, int):boolean
     arg types: [android.support.v7.widget.RecyclerView, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, float):void
      android.support.v4.view.bx.b(android.view.View, boolean):void
      android.support.v4.view.bx.b(android.view.View, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.a(android.view.View, int):boolean
     arg types: [android.support.v7.widget.RecyclerView, int]
     candidates:
      android.support.v4.view.bx.a(int, int):int
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.a(android.view.View, float):void
      android.support.v4.view.bx.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.bi):void
      android.support.v4.view.bx.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.bx.a(android.view.View, boolean):void
      android.support.v4.view.bx.a(android.view.View, int):boolean */
    public final void a(View view, f fVar) {
        super.a(view, fVar);
        fVar.b((CharSequence) RecyclerView.class.getName());
        if (!this.a.j() && this.a.b() != null) {
            br b = this.a.b();
            bw bwVar = b.r.a;
            cc ccVar = b.r.f;
            if (bx.b((View) b.r, -1) || bx.a((View) b.r, -1)) {
                fVar.a(8192);
                fVar.i(true);
            }
            if (bx.b((View) b.r, 1) || bx.a((View) b.r, 1)) {
                fVar.a(4096);
                fVar.i(true);
            }
            fVar.a(q.a(b.a(bwVar, ccVar), b.b(bwVar, ccVar)));
        }
    }

    public final void a(View view, AccessibilityEvent accessibilityEvent) {
        super.a(view, accessibilityEvent);
        accessibilityEvent.setClassName(RecyclerView.class.getName());
        if ((view instanceof RecyclerView) && !this.a.j()) {
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.b() != null) {
                recyclerView.b().a(accessibilityEvent);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.a(android.view.View, int):boolean
     arg types: [android.support.v7.widget.RecyclerView, int]
     candidates:
      android.support.v4.view.bx.a(int, int):int
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.a(android.view.View, float):void
      android.support.v4.view.bx.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.bi):void
      android.support.v4.view.bx.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.bx.a(android.view.View, boolean):void
      android.support.v4.view.bx.a(android.view.View, int):boolean */
    public final boolean a(View view, int i, Bundle bundle) {
        int i2;
        int i3;
        int i4;
        if (super.a(view, i, bundle)) {
            return true;
        }
        if (this.a.j() || this.a.b() == null) {
            return false;
        }
        br b = this.a.b();
        bw bwVar = b.r.a;
        cc ccVar = b.r.f;
        if (b.r == null) {
            return false;
        }
        switch (i) {
            case 4096:
                i2 = bx.b(b.r, 1) ? (b.q() - b.s()) - b.u() : 0;
                if (bx.a((View) b.r, 1)) {
                    i3 = i2;
                    i4 = (b.p() - b.r()) - b.t();
                    break;
                }
                i3 = i2;
                i4 = 0;
                break;
            case 8192:
                i2 = bx.b(b.r, -1) ? -((b.q() - b.s()) - b.u()) : 0;
                if (bx.a((View) b.r, -1)) {
                    i3 = i2;
                    i4 = -((b.p() - b.r()) - b.t());
                    break;
                }
                i3 = i2;
                i4 = 0;
                break;
            default:
                i4 = 0;
                i3 = 0;
                break;
        }
        if (i3 == 0 && i4 == 0) {
            return false;
        }
        b.r.scrollBy(i4, i3);
        return true;
    }
}
