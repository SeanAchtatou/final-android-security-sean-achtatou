package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

final class ba extends az {
    ba(br brVar) {
        super(brVar, (byte) 0);
    }

    public final int a(View view) {
        return br.h(view) - ((RecyclerView.LayoutParams) view.getLayoutParams()).leftMargin;
    }

    public final void a(int i) {
        this.a.e(i);
    }

    public final int b(View view) {
        return ((RecyclerView.LayoutParams) view.getLayoutParams()).rightMargin + br.j(view);
    }

    public final int c() {
        return this.a.r();
    }

    public final int c(View view) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        return layoutParams.rightMargin + br.f(view) + layoutParams.leftMargin;
    }

    public final int d() {
        return this.a.p() - this.a.t();
    }

    public final int d(View view) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        return layoutParams.bottomMargin + br.g(view) + layoutParams.topMargin;
    }

    public final int e() {
        return this.a.p();
    }

    public final int f() {
        return (this.a.p() - this.a.r()) - this.a.t();
    }

    public final int g() {
        return this.a.t();
    }
}
