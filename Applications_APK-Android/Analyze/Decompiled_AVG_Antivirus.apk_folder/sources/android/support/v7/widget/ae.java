package android.support.v7.widget;

import android.util.SparseIntArray;

public abstract class ae {
    final SparseIntArray a;
    private boolean b;

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int c(int r7, int r8) {
        /*
            r6 = this;
            r1 = 0
            int r4 = r6.a()
            if (r4 != r8) goto L_0x0008
        L_0x0007:
            return r1
        L_0x0008:
            boolean r0 = r6.b
            if (r0 == 0) goto L_0x006e
            android.util.SparseIntArray r0 = r6.a
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x006e
            android.util.SparseIntArray r0 = r6.a
            int r0 = r0.size()
            int r0 = r0 + -1
            r2 = r1
        L_0x001d:
            if (r2 > r0) goto L_0x0031
            int r3 = r2 + r0
            int r3 = r3 >>> 1
            android.util.SparseIntArray r5 = r6.a
            int r5 = r5.keyAt(r3)
            if (r5 >= r7) goto L_0x002e
            int r2 = r3 + 1
            goto L_0x001d
        L_0x002e:
            int r0 = r3 + -1
            goto L_0x001d
        L_0x0031:
            int r0 = r2 + -1
            if (r0 < 0) goto L_0x0062
            android.util.SparseIntArray r2 = r6.a
            int r2 = r2.size()
            if (r0 >= r2) goto L_0x0062
            android.util.SparseIntArray r2 = r6.a
            int r0 = r2.keyAt(r0)
        L_0x0043:
            if (r0 < 0) goto L_0x006e
            android.util.SparseIntArray r2 = r6.a
            int r2 = r2.get(r0)
            int r3 = r6.a()
            int r2 = r2 + r3
            int r0 = r0 + 1
        L_0x0052:
            r3 = r0
        L_0x0053:
            if (r3 >= r7) goto L_0x0068
            int r0 = r6.a()
            int r2 = r2 + r0
            if (r2 != r8) goto L_0x0064
            r0 = r1
        L_0x005d:
            int r2 = r3 + 1
            r3 = r2
            r2 = r0
            goto L_0x0053
        L_0x0062:
            r0 = -1
            goto L_0x0043
        L_0x0064:
            if (r2 > r8) goto L_0x005d
            r0 = r2
            goto L_0x005d
        L_0x0068:
            int r0 = r2 + r4
            if (r0 > r8) goto L_0x0007
            r1 = r2
            goto L_0x0007
        L_0x006e:
            r0 = r1
            r2 = r1
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ae.c(int, int):int");
    }

    public abstract int a();

    /* access modifiers changed from: package-private */
    public final int a(int i, int i2) {
        if (!this.b) {
            return c(i, i2);
        }
        int i3 = this.a.get(i, -1);
        if (i3 != -1) {
            return i3;
        }
        int c = c(i, i2);
        this.a.put(i, c);
        return c;
    }

    public final int b(int i, int i2) {
        int a2 = a();
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 < i) {
            int a3 = a();
            int i6 = i5 + a3;
            if (i6 == i2) {
                i4++;
                a3 = 0;
            } else if (i6 > i2) {
                i4++;
            } else {
                a3 = i6;
            }
            i3++;
            i5 = a3;
        }
        return i5 + a2 > i2 ? i4 + 1 : i4;
    }
}
