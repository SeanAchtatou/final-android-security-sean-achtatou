package android.support.v7.widget;

import java.util.ArrayList;
import java.util.Iterator;

final class u implements Runnable {
    final /* synthetic */ ArrayList a;
    final /* synthetic */ s b;

    u(s sVar, ArrayList arrayList) {
        this.b = sVar;
        this.a = arrayList;
    }

    public final void run() {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            s.a(this.b, (ab) it.next());
        }
        this.a.clear();
        this.b.h.remove(this.a);
    }
}
