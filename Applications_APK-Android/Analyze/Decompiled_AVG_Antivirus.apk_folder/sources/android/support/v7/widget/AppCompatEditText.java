package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v7.a.b;
import android.support.v7.internal.widget.az;
import android.support.v7.internal.widget.bb;
import android.support.v7.internal.widget.bc;
import android.support.v7.internal.widget.be;
import android.util.AttributeSet;
import android.widget.EditText;

public class AppCompatEditText extends EditText {
    private static final int[] a = {16842964};
    private bb b;
    private bb c;
    private bc d;

    public AppCompatEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.editTextStyle);
    }

    public AppCompatEditText(Context context, AttributeSet attributeSet, int i) {
        super(az.a(context), attributeSet, i);
        ColorStateList b2;
        if (bc.a) {
            be a2 = be.a(getContext(), attributeSet, a, i);
            if (a2.f(0) && (b2 = a2.c().b(a2.f(0, -1))) != null) {
                a(b2);
            }
            this.d = a2.c();
            a2.b();
        }
    }

    private void a() {
        if (getBackground() == null) {
            return;
        }
        if (this.c != null) {
            bc.a(this, this.c);
        } else if (this.b != null) {
            bc.a(this, this.b);
        }
    }

    private void a(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.b == null) {
                this.b = new bb();
            }
            this.b.a = colorStateList;
            this.b.d = true;
        } else {
            this.b = null;
        }
        a();
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        a();
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        a(null);
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        a(this.d != null ? this.d.b(i) : null);
    }
}
