package android.support.v7.widget;

import android.support.v4.view.bx;
import android.support.v4.view.dv;
import android.support.v4.view.el;
import android.view.View;

final class w extends ad {
    final /* synthetic */ cf a;
    final /* synthetic */ dv b;
    final /* synthetic */ s c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    w(s sVar, cf cfVar, dv dvVar) {
        super((byte) 0);
        this.c = sVar;
        this.a = cfVar;
        this.b = dvVar;
    }

    public final void a(View view) {
        this.c.g(this.a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, int):void
      android.support.v4.view.bx.c(android.view.View, float):void */
    public final void b(View view) {
        this.b.a((el) null);
        bx.c(view, 1.0f);
        this.c.e(this.a);
        this.c.k.remove(this.a);
        this.c.l();
    }
}
