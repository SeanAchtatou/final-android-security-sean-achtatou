package android.support.v7.widget;

import android.support.v4.view.a;
import android.support.v4.view.bx;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class bw {
    final ArrayList a = new ArrayList();
    final ArrayList b = new ArrayList();
    final /* synthetic */ RecyclerView c;
    /* access modifiers changed from: private */
    public ArrayList d = null;
    private final List e = Collections.unmodifiableList(this.a);
    private int f = 2;
    private bv g;
    private cd h;

    public bw(RecyclerView recyclerView) {
        this.c = recyclerView;
    }

    private void a(ViewGroup viewGroup, boolean z) {
        for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = viewGroup.getChildAt(childCount);
            if (childAt instanceof ViewGroup) {
                a((ViewGroup) childAt, true);
            }
        }
        if (z) {
            if (viewGroup.getVisibility() == 4) {
                viewGroup.setVisibility(0);
                viewGroup.setVisibility(4);
                return;
            }
            int visibility = viewGroup.getVisibility();
            viewGroup.setVisibility(4);
            viewGroup.setVisibility(visibility);
        }
    }

    private void c(cf cfVar) {
        bx.a(cfVar.a, (a) null);
        if (this.c.r != null) {
            bx unused = this.c.r;
        }
        if (this.c.p != null) {
            bi unused2 = this.c.p;
        }
        if (this.c.f != null) {
            this.c.d.c(cfVar);
        }
        cfVar.k = null;
        e().a(cfVar);
    }

    private cf d(int i) {
        int size;
        int a2;
        int i2 = 0;
        if (this.d == null || (size = this.d.size()) == 0) {
            return null;
        }
        int i3 = 0;
        while (i3 < size) {
            cf cfVar = (cf) this.d.get(i3);
            if (cfVar.g() || cfVar.e_() != i) {
                i3++;
            } else {
                cfVar.b(32);
                return cfVar;
            }
        }
        if (this.c.p.b_() && (a2 = this.c.b.a(i, 0)) > 0 && a2 < this.c.p.a()) {
            bi unused = this.c.p;
            while (i2 < size) {
                cf cfVar2 = (cf) this.d.get(i2);
                if (cfVar2.g() || cfVar2.d != -1) {
                    i2++;
                } else {
                    cfVar2.b(32);
                    return cfVar2;
                }
            }
        }
        return null;
    }

    private cf e(int i) {
        View view;
        int i2 = 0;
        int size = this.a.size();
        int i3 = 0;
        while (i3 < size) {
            cf cfVar = (cf) this.a.get(i3);
            if (cfVar.g() || cfVar.e_() != i || cfVar.j() || (!this.c.f.g && cfVar.m())) {
                i3++;
            } else {
                cfVar.b(32);
                return cfVar;
            }
        }
        p pVar = this.c.c;
        int size2 = pVar.c.size();
        int i4 = 0;
        while (true) {
            if (i4 >= size2) {
                view = null;
                break;
            }
            View view2 = (View) pVar.c.get(i4);
            cf b2 = pVar.a.b(view2);
            if (b2.e_() == i && !b2.j() && !b2.m()) {
                view = view2;
                break;
            }
            i4++;
        }
        if (view != null) {
            cf b3 = RecyclerView.b(view);
            this.c.c.f(view);
            int c2 = this.c.c.c(view);
            if (c2 == -1) {
                throw new IllegalStateException("layout index should not be -1 after unhiding a view:" + b3);
            }
            this.c.c.d(c2);
            c(view);
            b3.b(8224);
            return b3;
        }
        int size3 = this.b.size();
        while (i2 < size3) {
            cf cfVar2 = (cf) this.b.get(i2);
            if (cfVar2.j() || cfVar2.e_() != i) {
                i2++;
            } else {
                this.b.remove(i2);
                return cfVar2;
            }
        }
        return null;
    }

    private cf f(int i) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            cf cfVar = (cf) this.a.get(size);
            if (cfVar.d == -1 && !cfVar.g()) {
                if (i == cfVar.e) {
                    cfVar.b(32);
                    if (!cfVar.m() || this.c.f.a()) {
                        return cfVar;
                    }
                    cfVar.a(2, 14);
                    return cfVar;
                }
                this.a.remove(size);
                this.c.removeDetachedView(cfVar.a, false);
                b(cfVar.a);
            }
        }
        for (int size2 = this.b.size() - 1; size2 >= 0; size2--) {
            cf cfVar2 = (cf) this.b.get(size2);
            if (cfVar2.d == -1) {
                if (i == cfVar2.e) {
                    this.b.remove(size2);
                    return cfVar2;
                }
                c(size2);
            }
        }
        return null;
    }

    public final int a(int i) {
        if (i >= 0 && i < this.c.f.e()) {
            return !this.c.f.a() ? i : this.c.b.b(i);
        }
        throw new IndexOutOfBoundsException("invalid position " + i + ". State item count is " + this.c.f.e());
    }

    public final void a() {
        this.a.clear();
        c();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.support.v7.widget.cf r6) {
        /*
            r5 = this;
            r0 = 1
            r1 = 0
            boolean r2 = r6.e()
            if (r2 != 0) goto L_0x0010
            android.view.View r2 = r6.a
            android.view.ViewParent r2 = r2.getParent()
            if (r2 == 0) goto L_0x003d
        L_0x0010:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Scrapped or attached views may not be recycled. isScrap:"
            r3.<init>(r4)
            boolean r4 = r6.e()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = " isAttached:"
            java.lang.StringBuilder r3 = r3.append(r4)
            android.view.View r4 = r6.a
            android.view.ViewParent r4 = r4.getParent()
            if (r4 == 0) goto L_0x003b
        L_0x002f:
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            throw r2
        L_0x003b:
            r0 = r1
            goto L_0x002f
        L_0x003d:
            boolean r2 = r6.n()
            if (r2 == 0) goto L_0x0058
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Tmp detached view should be removed from RecyclerView before it can be recycled: "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0058:
            boolean r2 = r6.d_()
            if (r2 == 0) goto L_0x0066
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle."
            r0.<init>(r1)
            throw r0
        L_0x0066:
            boolean r3 = android.support.v7.widget.cf.c(r6)
            android.support.v7.widget.RecyclerView r2 = r5.c
            android.support.v7.widget.bi r2 = r2.p
            if (r2 == 0) goto L_0x0079
            if (r3 == 0) goto L_0x0079
            android.support.v7.widget.RecyclerView r2 = r5.c
            android.support.v7.widget.bi unused = r2.p
        L_0x0079:
            boolean r2 = r6.r()
            if (r2 == 0) goto L_0x00bc
            r2 = 14
            boolean r2 = r6.a(r2)
            if (r2 != 0) goto L_0x00ba
            java.util.ArrayList r2 = r5.b
            int r2 = r2.size()
            int r4 = r5.f
            if (r2 != r4) goto L_0x0096
            if (r2 <= 0) goto L_0x0096
            r5.c(r1)
        L_0x0096:
            int r4 = r5.f
            if (r2 >= r4) goto L_0x00ba
            java.util.ArrayList r2 = r5.b
            r2.add(r6)
            r2 = r0
        L_0x00a0:
            if (r2 != 0) goto L_0x00b8
            r5.c(r6)
            r1 = r0
            r0 = r2
        L_0x00a7:
            android.support.v7.widget.RecyclerView r2 = r5.c
            android.support.v7.widget.do r2 = r2.d
            r2.c(r6)
            if (r0 != 0) goto L_0x00b7
            if (r1 != 0) goto L_0x00b7
            if (r3 == 0) goto L_0x00b7
            r0 = 0
            r6.k = r0
        L_0x00b7:
            return
        L_0x00b8:
            r0 = r2
            goto L_0x00a7
        L_0x00ba:
            r2 = r1
            goto L_0x00a0
        L_0x00bc:
            r0 = r1
            goto L_0x00a7
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.bw.a(android.support.v7.widget.cf):void");
    }

    public final void a(View view) {
        cf b2 = RecyclerView.b(view);
        if (b2.n()) {
            this.c.removeDetachedView(view, false);
        }
        if (b2.e()) {
            b2.f();
        } else if (b2.g()) {
            b2.h();
        }
        a(b2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:110:0x0271  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0295  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0299  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01f1  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0228  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.view.View b(int r13) {
        /*
            r12 = this;
            r3 = 0
            r10 = 8192(0x2000, float:1.14794E-41)
            r1 = 1
            r2 = 0
            if (r13 < 0) goto L_0x0011
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.cc r0 = r0.f
            int r0 = r0.e()
            if (r13 < r0) goto L_0x0042
        L_0x0011:
            java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Invalid item position "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r13)
            java.lang.String r2 = "("
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r13)
            java.lang.String r2 = "). Item count:"
            java.lang.StringBuilder r1 = r1.append(r2)
            android.support.v7.widget.RecyclerView r2 = r12.c
            android.support.v7.widget.cc r2 = r2.f
            int r2 = r2.e()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0042:
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.cc r0 = r0.f
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x02be
            android.support.v7.widget.cf r4 = r12.d(r13)
            if (r4 == 0) goto L_0x00d0
            r0 = r1
        L_0x0053:
            r11 = r4
            r4 = r0
            r0 = r11
        L_0x0056:
            if (r0 != 0) goto L_0x02bb
            android.support.v7.widget.cf r0 = r12.e(r13)
            if (r0 == 0) goto L_0x02bb
            boolean r5 = r0.m()
            if (r5 == 0) goto L_0x00d2
            android.support.v7.widget.RecyclerView r5 = r12.c
            android.support.v7.widget.cc r5 = r5.f
            boolean r5 = r5.a()
        L_0x006c:
            if (r5 != 0) goto L_0x0142
            r5 = 4
            r0.b(r5)
            boolean r5 = r0.e()
            if (r5 == 0) goto L_0x0137
            android.support.v7.widget.RecyclerView r5 = r12.c
            android.view.View r6 = r0.a
            r5.removeDetachedView(r6, r2)
            r0.f()
        L_0x0082:
            r12.a(r0)
            r0 = r3
            r3 = r4
        L_0x0087:
            if (r0 != 0) goto L_0x02b7
            android.support.v7.widget.RecyclerView r4 = r12.c
            android.support.v7.widget.m r4 = r4.b
            int r4 = r4.b(r13)
            if (r4 < 0) goto L_0x009f
            android.support.v7.widget.RecyclerView r5 = r12.c
            android.support.v7.widget.bi r5 = r5.p
            int r5 = r5.a()
            if (r4 < r5) goto L_0x0145
        L_0x009f:
            java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Inconsistency detected. Invalid item position "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r13)
            java.lang.String r2 = "(offset:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r2 = ").state:"
            java.lang.StringBuilder r1 = r1.append(r2)
            android.support.v7.widget.RecyclerView r2 = r12.c
            android.support.v7.widget.cc r2 = r2.f
            int r2 = r2.e()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00d0:
            r0 = r2
            goto L_0x0053
        L_0x00d2:
            int r5 = r0.b
            if (r5 < 0) goto L_0x00e4
            int r5 = r0.b
            android.support.v7.widget.RecyclerView r6 = r12.c
            android.support.v7.widget.bi r6 = r6.p
            int r6 = r6.a()
            if (r5 < r6) goto L_0x00f9
        L_0x00e4:
            java.lang.IndexOutOfBoundsException r1 = new java.lang.IndexOutOfBoundsException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Inconsistency detected. Invalid view holder adapter position"
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x00f9:
            android.support.v7.widget.RecyclerView r5 = r12.c
            android.support.v7.widget.cc r5 = r5.f
            boolean r5 = r5.a()
            if (r5 != 0) goto L_0x0116
            android.support.v7.widget.RecyclerView r5 = r12.c
            android.support.v7.widget.bi r5 = r5.p
            int r6 = r0.b
            int r5 = r5.a(r6)
            int r6 = r0.e
            if (r5 == r6) goto L_0x0116
            r5 = r2
            goto L_0x006c
        L_0x0116:
            android.support.v7.widget.RecyclerView r5 = r12.c
            android.support.v7.widget.bi r5 = r5.p
            boolean r5 = r5.b_()
            if (r5 == 0) goto L_0x0134
            long r6 = r0.d
            android.support.v7.widget.RecyclerView r5 = r12.c
            android.support.v7.widget.bi unused = r5.p
            int r5 = r0.b
            r8 = -1
            int r5 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r5 == 0) goto L_0x0134
            r5 = r2
            goto L_0x006c
        L_0x0134:
            r5 = r1
            goto L_0x006c
        L_0x0137:
            boolean r5 = r0.g()
            if (r5 == 0) goto L_0x0082
            r0.h()
            goto L_0x0082
        L_0x0142:
            r3 = r1
            goto L_0x0087
        L_0x0145:
            android.support.v7.widget.RecyclerView r5 = r12.c
            android.support.v7.widget.bi r5 = r5.p
            int r5 = r5.a(r4)
            android.support.v7.widget.RecyclerView r6 = r12.c
            android.support.v7.widget.bi r6 = r6.p
            boolean r6 = r6.b_()
            if (r6 == 0) goto L_0x0169
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.bi unused = r0.p
            android.support.v7.widget.cf r0 = r12.f(r5)
            if (r0 == 0) goto L_0x0169
            r0.b = r4
            r3 = r1
        L_0x0169:
            if (r0 != 0) goto L_0x0195
            android.support.v7.widget.cd r4 = r12.h
            if (r4 == 0) goto L_0x0195
            android.support.v7.widget.cd r4 = r12.h
            android.view.View r4 = r4.a()
            if (r4 == 0) goto L_0x0195
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.cf r0 = r0.a(r4)
            if (r0 != 0) goto L_0x0187
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "getViewForPositionAndType returned a view which does not have a ViewHolder"
            r0.<init>(r1)
            throw r0
        L_0x0187:
            boolean r4 = r0.d_()
            if (r4 == 0) goto L_0x0195
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view."
            r0.<init>(r1)
            throw r0
        L_0x0195:
            if (r0 != 0) goto L_0x01b8
            android.support.v7.widget.bv r0 = r12.e()
            android.support.v7.widget.cf r4 = r0.a(r5)
            if (r4 == 0) goto L_0x01b7
            r4.q()
            boolean r0 = android.support.v7.widget.RecyclerView.i
            if (r0 == 0) goto L_0x01b7
            android.view.View r0 = r4.a
            boolean r0 = r0 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x01b7
            android.view.View r0 = r4.a
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            r12.a(r0, r2)
        L_0x01b7:
            r0 = r4
        L_0x01b8:
            if (r0 != 0) goto L_0x02b7
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.bi r0 = r0.p
            android.support.v7.widget.RecyclerView r4 = r12.c
            java.lang.String r6 = "RV CreateView"
            android.support.v4.os.k.a(r6)
            android.support.v7.widget.cf r0 = r0.a(r4, r5)
            r0.e = r5
            r4 = r0
            r5 = r3
            android.support.v4.os.k.a()
        L_0x01d2:
            if (r5 == 0) goto L_0x020d
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.cc r0 = r0.f
            boolean r0 = r0.a()
            if (r0 != 0) goto L_0x020d
            boolean r0 = r4.a(r10)
            if (r0 == 0) goto L_0x020d
            r4.a(r2, r10)
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.cc r0 = r0.f
            boolean r0 = r0.h
            if (r0 == 0) goto L_0x020d
            android.support.v7.widget.bm.d(r4)
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.bm r0 = r0.e
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.cc r0 = r0.f
            r4.p()
            android.support.v7.widget.bo r0 = new android.support.v7.widget.bo
            r0.<init>()
            android.support.v7.widget.bo r0 = r0.a(r4)
            android.support.v7.widget.RecyclerView r3 = r12.c
            r3.a(r4, r0)
        L_0x020d:
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.cc r0 = r0.f
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x0240
            boolean r0 = r4.l()
            if (r0 == 0) goto L_0x0240
            r4.f = r13
            r3 = r2
        L_0x0220:
            android.view.View r0 = r4.a
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            if (r0 != 0) goto L_0x0299
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.view.ViewGroup$LayoutParams r0 = r0.generateDefaultLayoutParams()
            android.support.v7.widget.RecyclerView$LayoutParams r0 = (android.support.v7.widget.RecyclerView.LayoutParams) r0
            android.view.View r6 = r4.a
            r6.setLayoutParams(r0)
        L_0x0235:
            r0.a = r4
            if (r5 == 0) goto L_0x02b2
            if (r3 == 0) goto L_0x02b2
        L_0x023b:
            r0.d = r1
            android.view.View r0 = r4.a
            return r0
        L_0x0240:
            boolean r0 = r4.l()
            if (r0 == 0) goto L_0x0252
            boolean r0 = r4.k()
            if (r0 != 0) goto L_0x0252
            boolean r0 = r4.j()
            if (r0 == 0) goto L_0x02b4
        L_0x0252:
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.m r0 = r0.b
            int r0 = r0.b(r13)
            android.support.v7.widget.RecyclerView r3 = r12.c
            r4.k = r3
            android.support.v7.widget.RecyclerView r3 = r12.c
            android.support.v7.widget.bi r3 = r3.p
            r3.b(r4, r0)
            android.view.View r0 = r4.a
            android.support.v7.widget.RecyclerView r3 = r12.c
            boolean r3 = r3.g()
            if (r3 == 0) goto L_0x028b
            int r3 = android.support.v4.view.bx.e(r0)
            if (r3 != 0) goto L_0x027a
            android.support.v4.view.bx.c(r0, r1)
        L_0x027a:
            boolean r3 = android.support.v4.view.bx.b(r0)
            if (r3 != 0) goto L_0x028b
            android.support.v7.widget.RecyclerView r3 = r12.c
            android.support.v7.widget.cg r3 = r3.ag
            android.support.v4.view.a r3 = r3.c
            android.support.v4.view.bx.a(r0, r3)
        L_0x028b:
            android.support.v7.widget.RecyclerView r0 = r12.c
            android.support.v7.widget.cc r0 = r0.f
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x0297
            r4.f = r13
        L_0x0297:
            r3 = r1
            goto L_0x0220
        L_0x0299:
            android.support.v7.widget.RecyclerView r6 = r12.c
            boolean r6 = r6.checkLayoutParams(r0)
            if (r6 != 0) goto L_0x02af
            android.support.v7.widget.RecyclerView r6 = r12.c
            android.view.ViewGroup$LayoutParams r0 = r6.generateLayoutParams(r0)
            android.support.v7.widget.RecyclerView$LayoutParams r0 = (android.support.v7.widget.RecyclerView.LayoutParams) r0
            android.view.View r6 = r4.a
            r6.setLayoutParams(r0)
            goto L_0x0235
        L_0x02af:
            android.support.v7.widget.RecyclerView$LayoutParams r0 = (android.support.v7.widget.RecyclerView.LayoutParams) r0
            goto L_0x0235
        L_0x02b2:
            r1 = r2
            goto L_0x023b
        L_0x02b4:
            r3 = r2
            goto L_0x0220
        L_0x02b7:
            r4 = r0
            r5 = r3
            goto L_0x01d2
        L_0x02bb:
            r3 = r4
            goto L_0x0087
        L_0x02be:
            r0 = r3
            r4 = r2
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.bw.b(int):android.view.View");
    }

    public final List b() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final void b(cf cfVar) {
        if (cfVar.p) {
            this.d.remove(cfVar);
        } else {
            this.a.remove(cfVar);
        }
        bw unused = cfVar.o = null;
        boolean unused2 = cfVar.p = false;
        cfVar.h();
    }

    /* access modifiers changed from: package-private */
    public final void b(View view) {
        cf b2 = RecyclerView.b(view);
        bw unused = b2.o = null;
        boolean unused2 = b2.p = false;
        b2.h();
        a(b2);
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        for (int size = this.b.size() - 1; size >= 0; size--) {
            c(size);
        }
        this.b.clear();
    }

    /* access modifiers changed from: package-private */
    public final void c(int i) {
        c((cf) this.b.get(i));
        this.b.remove(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cf.a(android.support.v7.widget.bw, boolean):void
     arg types: [android.support.v7.widget.bw, int]
     candidates:
      android.support.v7.widget.cf.a(int, int):void
      android.support.v7.widget.cf.a(int, boolean):void
      android.support.v7.widget.cf.a(android.support.v7.widget.bw, boolean):void */
    /* access modifiers changed from: package-private */
    public final void c(View view) {
        cf b2 = RecyclerView.b(view);
        if (!b2.a(12) && b2.s() && !RecyclerView.a(this.c, b2)) {
            if (this.d == null) {
                this.d = new ArrayList();
            }
            b2.a(this, true);
            this.d.add(b2);
        } else if (!b2.j() || b2.m() || this.c.p.b_()) {
            b2.a(this, false);
            this.a.add(b2);
        } else {
            throw new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool.");
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        this.a.clear();
        if (this.d != null) {
            this.d.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public final bv e() {
        if (this.g == null) {
            this.g = new bv();
        }
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            ((cf) this.b.get(i)).a();
        }
        int size2 = this.a.size();
        for (int i2 = 0; i2 < size2; i2++) {
            ((cf) this.a.get(i2)).a();
        }
        if (this.d != null) {
            int size3 = this.d.size();
            for (int i3 = 0; i3 < size3; i3++) {
                ((cf) this.d.get(i3)).a();
            }
        }
    }
}
