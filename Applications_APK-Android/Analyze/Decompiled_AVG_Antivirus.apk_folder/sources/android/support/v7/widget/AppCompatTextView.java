package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.a.l;
import android.support.v7.internal.b.a;
import android.util.AttributeSet;
import android.widget.TextView;

public class AppCompatTextView extends TextView {
    public AppCompatTextView(Context context) {
        this(context, null);
    }

    public AppCompatTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842884);
    }

    public AppCompatTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.P, i, 0);
        int resourceId = obtainStyledAttributes.getResourceId(l.Q, -1);
        obtainStyledAttributes.recycle();
        if (resourceId != -1) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(resourceId, l.bB);
            if (obtainStyledAttributes2.hasValue(l.bG)) {
                setAllCaps(obtainStyledAttributes2.getBoolean(l.bG, false));
            }
            obtainStyledAttributes2.recycle();
        }
        TypedArray obtainStyledAttributes3 = context.obtainStyledAttributes(attributeSet, l.P, i, 0);
        if (obtainStyledAttributes3.hasValue(l.R)) {
            setAllCaps(obtainStyledAttributes3.getBoolean(l.R, false));
        }
        obtainStyledAttributes3.recycle();
    }

    public void setAllCaps(boolean z) {
        setTransformationMethod(z ? new a(getContext()) : null);
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, l.bB);
        if (obtainStyledAttributes.hasValue(l.bG)) {
            setAllCaps(obtainStyledAttributes.getBoolean(l.bG, false));
        }
        obtainStyledAttributes.recycle();
    }
}
