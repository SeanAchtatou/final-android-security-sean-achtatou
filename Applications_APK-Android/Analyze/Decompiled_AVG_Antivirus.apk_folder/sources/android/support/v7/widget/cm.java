package android.support.v7.widget;

import android.text.Editable;
import android.text.TextWatcher;

final class cm implements TextWatcher {
    final /* synthetic */ SearchView a;

    cm(SearchView searchView) {
        this.a = searchView;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        SearchView.a(this.a, charSequence);
    }
}
