package android.support.v7.widget.a;

import android.graphics.Canvas;
import android.support.v4.view.bx;
import android.support.v7.c.c;
import android.support.v7.widget.RecyclerView;
import android.view.View;

final class r extends q {
    r() {
    }

    public final void a(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
        float f3;
        if (z && view.getTag(c.item_touch_helper_previous_elevation) == null) {
            Float valueOf = Float.valueOf(bx.v(view));
            int childCount = recyclerView.getChildCount();
            float f4 = 0.0f;
            int i2 = 0;
            while (i2 < childCount) {
                View childAt = recyclerView.getChildAt(i2);
                if (childAt != view) {
                    f3 = bx.v(childAt);
                    if (f3 > f4) {
                        i2++;
                        f4 = f3;
                    }
                }
                f3 = f4;
                i2++;
                f4 = f3;
            }
            bx.f(view, 1.0f + f4);
            view.setTag(c.item_touch_helper_previous_elevation, valueOf);
        }
        super.a(canvas, recyclerView, view, f, f2, i, z);
    }

    public final void a(View view) {
        Object tag = view.getTag(c.item_touch_helper_previous_elevation);
        if (tag != null && (tag instanceof Float)) {
            bx.f(view, ((Float) tag).floatValue());
        }
        view.setTag(c.item_touch_helper_previous_elevation, null);
        super.a(view);
    }
}
