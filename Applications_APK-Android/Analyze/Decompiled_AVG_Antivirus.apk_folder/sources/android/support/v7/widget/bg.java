package android.support.v7.widget;

import android.view.View;
import android.view.ViewGroup;

final class bg implements r {
    final /* synthetic */ RecyclerView a;

    bg(RecyclerView recyclerView) {
        this.a = recyclerView;
    }

    public final int a() {
        return this.a.getChildCount();
    }

    public final int a(View view) {
        return this.a.indexOfChild(view);
    }

    public final void a(int i) {
        View childAt = this.a.getChildAt(i);
        if (childAt != null) {
            this.a.f(childAt);
        }
        this.a.removeViewAt(i);
    }

    public final void a(View view, int i) {
        this.a.addView(view, i);
        RecyclerView.a(this.a, view);
    }

    public final void a(View view, int i, ViewGroup.LayoutParams layoutParams) {
        cf b = RecyclerView.b(view);
        if (b != null) {
            if (b.n() || b.d_()) {
                b.i();
            } else {
                throw new IllegalArgumentException("Called attach on a child which is not detached: " + b);
            }
        }
        this.a.attachViewToParent(view, i, layoutParams);
    }

    public final cf b(View view) {
        return RecyclerView.b(view);
    }

    public final View b(int i) {
        return this.a.getChildAt(i);
    }

    public final void b() {
        int childCount = this.a.getChildCount();
        for (int i = 0; i < childCount; i++) {
            this.a.f(b(i));
        }
        this.a.removeAllViews();
    }

    public final void c(int i) {
        cf b;
        View b2 = b(i);
        if (!(b2 == null || (b = RecyclerView.b(b2)) == null)) {
            if (!b.n() || b.d_()) {
                b.b(256);
            } else {
                throw new IllegalArgumentException("called detach on an already detached child " + b);
            }
        }
        this.a.detachViewFromParent(i);
    }

    public final void c(View view) {
        cf b = RecyclerView.b(view);
        if (b != null) {
            cf.a(b);
        }
    }

    public final void d(View view) {
        cf b = RecyclerView.b(view);
        if (b != null) {
            cf.b(b);
        }
    }
}
