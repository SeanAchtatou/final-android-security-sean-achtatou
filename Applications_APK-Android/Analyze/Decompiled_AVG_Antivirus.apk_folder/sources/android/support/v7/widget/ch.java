package android.support.v7.widget;

import android.os.Bundle;
import android.support.v4.view.a;
import android.support.v4.view.a.f;
import android.view.View;

final class ch extends a {
    final /* synthetic */ cg a;

    ch(cg cgVar) {
        this.a = cgVar;
    }

    public final void a(View view, f fVar) {
        super.a(view, fVar);
        if (!this.a.a.j() && this.a.a.b() != null) {
            this.a.a.b().a(view, fVar);
        }
    }

    public final boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        if (this.a.a.j() || this.a.a.b() == null) {
            return false;
        }
        br b = this.a.a.b();
        bw bwVar = b.r.a;
        cc ccVar = b.r.f;
        return false;
    }
}
