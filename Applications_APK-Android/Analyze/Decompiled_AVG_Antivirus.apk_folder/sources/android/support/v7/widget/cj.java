package android.support.v7.widget;

import android.view.inputmethod.InputMethodManager;

final class cj implements Runnable {
    final /* synthetic */ SearchView a;

    cj(SearchView searchView) {
        this.a = searchView;
    }

    public final void run() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.a.getContext().getSystemService("input_method");
        if (inputMethodManager != null) {
            SearchView.a.a(inputMethodManager, this.a);
        }
    }
}
