package android.support.v7.widget;

import android.support.v7.internal.view.menu.c;

final class b extends c {
    final /* synthetic */ ActionMenuPresenter a;

    private b(ActionMenuPresenter actionMenuPresenter) {
        this.a = actionMenuPresenter;
    }

    /* synthetic */ b(ActionMenuPresenter actionMenuPresenter, byte b) {
        this(actionMenuPresenter);
    }

    public final ListPopupWindow a() {
        if (this.a.w != null) {
            return this.a.w.f();
        }
        return null;
    }
}
