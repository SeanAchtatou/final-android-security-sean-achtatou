package android.support.v7.widget;

import android.support.v4.view.z;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.View;

final class ct implements View.OnKeyListener {
    final /* synthetic */ SearchView a;

    ct(SearchView searchView) {
        this.a = searchView;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (this.a.K == null) {
            return false;
        }
        if (this.a.d.isPopupShowing() && this.a.d.getListSelection() != -1) {
            return SearchView.a(this.a, i, keyEvent);
        }
        if (SearchView.SearchAutoComplete.a(this.a.d) || !z.b(keyEvent) || keyEvent.getAction() != 1 || i != 66) {
            return false;
        }
        view.cancelLongPress();
        this.a.a(this.a.d.getText().toString());
        return true;
    }
}
