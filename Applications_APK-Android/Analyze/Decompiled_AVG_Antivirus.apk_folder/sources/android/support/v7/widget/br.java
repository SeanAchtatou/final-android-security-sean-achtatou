package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.os.Parcelable;
import android.support.v4.view.a.a;
import android.support.v4.view.a.af;
import android.support.v4.view.a.f;
import android.support.v4.view.a.r;
import android.support.v4.view.bx;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

public abstract class br {
    /* access modifiers changed from: private */
    public boolean a = false;
    private boolean b = false;
    p q;
    RecyclerView r;
    ca s;

    public static int a(int i, int i2, int i3, boolean z) {
        int i4 = 1073741824;
        int max = Math.max(0, i - i2);
        if (z) {
            if (i3 < 0) {
                i4 = 0;
                i3 = 0;
            }
        } else if (i3 < 0) {
            if (i3 == -1) {
                i3 = max;
            } else if (i3 == -2) {
                i4 = Integer.MIN_VALUE;
                i3 = max;
            } else {
                i4 = 0;
                i3 = 0;
            }
        }
        return View.MeasureSpec.makeMeasureSpec(i3, i4);
    }

    private void a(int i) {
        if (d(i) != null) {
            this.q.a(i);
        }
    }

    static /* synthetic */ void a(br brVar, ca caVar) {
        if (brVar.s == caVar) {
            brVar.s = null;
        }
    }

    public static void a(View view, int i, int i2, int i3, int i4) {
        Rect rect = ((RecyclerView.LayoutParams) view.getLayoutParams()).b;
        view.layout(rect.left + i, rect.top + i2, i3 - rect.right, i4 - rect.bottom);
    }

    private void a(View view, int i, boolean z) {
        cf b2 = RecyclerView.b(view);
        if (z || b2.m()) {
            this.r.d.a(b2);
        } else {
            this.r.d.b(b2);
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        if (b2.g() || b2.e()) {
            if (b2.e()) {
                b2.f();
            } else {
                b2.h();
            }
            this.q.a(view, i, view.getLayoutParams(), false);
        } else if (view.getParent() == this.r) {
            int c = this.q.c(view);
            if (i == -1) {
                i = this.q.a();
            }
            if (c == -1) {
                throw new IllegalStateException("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:" + this.r.indexOfChild(view));
            } else if (c != i) {
                br h = this.r.q;
                View d = h.d(c);
                if (d == null) {
                    throw new IllegalArgumentException("Cannot move a child from non-existing index:" + c);
                }
                h.h(c);
                RecyclerView.LayoutParams layoutParams2 = (RecyclerView.LayoutParams) d.getLayoutParams();
                cf b3 = RecyclerView.b(d);
                if (b3.m()) {
                    h.r.d.a(b3);
                } else {
                    h.r.d.b(b3);
                }
                h.q.a(d, i, layoutParams2, b3.m());
            }
        } else {
            this.q.a(view, i, false);
            layoutParams.c = true;
            if (this.s != null && this.s.e()) {
                this.s.a(view);
            }
        }
        if (layoutParams.d) {
            b2.a.invalidate();
            layoutParams.d = false;
        }
    }

    public static int e(View view) {
        return ((RecyclerView.LayoutParams) view.getLayoutParams()).a.e_();
    }

    public static int f(View view) {
        Rect rect = ((RecyclerView.LayoutParams) view.getLayoutParams()).b;
        return rect.right + view.getMeasuredWidth() + rect.left;
    }

    public static int g(View view) {
        Rect rect = ((RecyclerView.LayoutParams) view.getLayoutParams()).b;
        return rect.bottom + view.getMeasuredHeight() + rect.top;
    }

    public static int h(View view) {
        return view.getLeft() - n(view);
    }

    private void h(int i) {
        d(i);
        this.q.d(i);
    }

    public static int i(View view) {
        return view.getTop() - l(view);
    }

    public static int j(View view) {
        return view.getRight() + o(view);
    }

    public static int k(View view) {
        return view.getBottom() + m(view);
    }

    public static int l(View view) {
        return ((RecyclerView.LayoutParams) view.getLayoutParams()).b.top;
    }

    public static int m(View view) {
        return ((RecyclerView.LayoutParams) view.getLayoutParams()).b.bottom;
    }

    public static int n(View view) {
        return ((RecyclerView.LayoutParams) view.getLayoutParams()).b.left;
    }

    public static int o(View view) {
        return ((RecyclerView.LayoutParams) view.getLayoutParams()).b.right;
    }

    public int a(int i, bw bwVar, cc ccVar) {
        return 0;
    }

    public int a(bw bwVar, cc ccVar) {
        if (this.r == null || this.r.p == null || !f()) {
            return 1;
        }
        return this.r.p.a();
    }

    public int a(cc ccVar) {
        return 0;
    }

    public RecyclerView.LayoutParams a(Context context, AttributeSet attributeSet) {
        return new RecyclerView.LayoutParams(context, attributeSet);
    }

    public RecyclerView.LayoutParams a(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof RecyclerView.LayoutParams ? new RecyclerView.LayoutParams((RecyclerView.LayoutParams) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new RecyclerView.LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new RecyclerView.LayoutParams(layoutParams);
    }

    public void a() {
    }

    public void a(int i, int i2) {
    }

    public final void a(int i, bw bwVar) {
        View d = d(i);
        a(i);
        bwVar.a(d);
    }

    public void a(Parcelable parcelable) {
    }

    /* access modifiers changed from: package-private */
    public final void a(RecyclerView recyclerView) {
        if (recyclerView == null) {
            this.r = null;
            this.q = null;
            return;
        }
        this.r = recyclerView;
        this.q = recyclerView.c;
    }

    public void a(RecyclerView recyclerView, int i) {
        Log.e("RecyclerView", "You must override smoothScrollToPosition to support smooth scrolling");
    }

    public void a(RecyclerView recyclerView, bw bwVar) {
    }

    public final void a(bw bwVar) {
        for (int o = o() - 1; o >= 0; o--) {
            View d = d(o);
            cf b2 = RecyclerView.b(d);
            if (!b2.d_()) {
                if (!b2.j() || b2.m() || this.r.p.b_()) {
                    h(o);
                    bwVar.c(d);
                } else {
                    a(o);
                    bwVar.a(b2);
                }
            }
        }
    }

    public void a(bw bwVar, cc ccVar, int i, int i2) {
        this.r.e(i, i2);
    }

    public void a(bw bwVar, cc ccVar, View view, f fVar) {
        fVar.b(r.a(f() ? e(view) : 0, 1, e() ? e(view) : 0, 1, false));
    }

    public final void a(ca caVar) {
        if (!(this.s == null || caVar == this.s || !this.s.e())) {
            this.s.c();
        }
        this.s = caVar;
        this.s.a(this.r, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.br.a(android.view.View, int, boolean):void
     arg types: [android.view.View, int, int]
     candidates:
      android.support.v7.widget.br.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc):int
      android.support.v7.widget.br.a(android.view.View, int, boolean):void */
    public final void a(View view) {
        a(view, -1, true);
    }

    public final void a(View view, Rect rect) {
        if (this.r == null) {
            rect.set(0, 0, 0, 0);
        } else {
            rect.set(this.r.e(view));
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(View view, f fVar) {
        cf b2 = RecyclerView.b(view);
        if (b2 != null && !b2.m() && !this.q.d(b2.a)) {
            a(this.r.a, this.r.f, view, fVar);
        }
    }

    public final void a(View view, bw bwVar) {
        this.q.b(view);
        bwVar.a(view);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, int):boolean
     arg types: [android.support.v7.widget.RecyclerView, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, float):void
      android.support.v4.view.bx.b(android.view.View, boolean):void
      android.support.v4.view.bx.b(android.view.View, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.a(android.view.View, int):boolean
     arg types: [android.support.v7.widget.RecyclerView, int]
     candidates:
      android.support.v4.view.bx.a(int, int):int
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.a(android.view.View, float):void
      android.support.v4.view.bx.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.bi):void
      android.support.v4.view.bx.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.bx.a(android.view.View, boolean):void
      android.support.v4.view.bx.a(android.view.View, int):boolean */
    public void a(AccessibilityEvent accessibilityEvent) {
        boolean z = true;
        bw bwVar = this.r.a;
        cc ccVar = this.r.f;
        af a2 = a.a(accessibilityEvent);
        if (this.r != null) {
            if (!bx.b((View) this.r, 1) && !bx.b((View) this.r, -1) && !bx.a((View) this.r, -1) && !bx.a((View) this.r, 1)) {
                z = false;
            }
            a2.a(z);
            if (this.r.p != null) {
                a2.a(this.r.p.a());
            }
        }
    }

    public void a(String str) {
        if (this.r != null) {
            this.r.a(str);
        }
    }

    public boolean a(RecyclerView.LayoutParams layoutParams) {
        return layoutParams != null;
    }

    public final boolean a(Runnable runnable) {
        if (this.r != null) {
            return this.r.removeCallbacks(runnable);
        }
        return false;
    }

    public int b(int i, bw bwVar, cc ccVar) {
        return 0;
    }

    public int b(bw bwVar, cc ccVar) {
        if (this.r == null || this.r.p == null || !e()) {
            return 1;
        }
        return this.r.p.a();
    }

    public int b(cc ccVar) {
        return 0;
    }

    public abstract RecyclerView.LayoutParams b();

    public View b(int i) {
        int o = o();
        for (int i2 = 0; i2 < o; i2++) {
            View d = d(i2);
            cf b2 = RecyclerView.b(d);
            if (b2 != null && b2.e_() == i && !b2.d_() && (this.r.f.a() || !b2.m())) {
                return d;
            }
        }
        return null;
    }

    public void b(int i, int i2) {
    }

    /* access modifiers changed from: package-private */
    public final void b(RecyclerView recyclerView, bw bwVar) {
        this.b = false;
        a(recyclerView, bwVar);
    }

    /* access modifiers changed from: package-private */
    public final void b(bw bwVar) {
        int size = bwVar.a.size();
        for (int i = size - 1; i >= 0; i--) {
            View view = ((cf) bwVar.a.get(i)).a;
            cf b2 = RecyclerView.b(view);
            if (!b2.d_()) {
                b2.a(false);
                if (b2.n()) {
                    this.r.removeDetachedView(view, false);
                }
                if (this.r.e != null) {
                    this.r.e.c(b2);
                }
                b2.a(true);
                bwVar.b(view);
            }
        }
        bwVar.d();
        if (size > 0) {
            this.r.invalidate();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.br.a(android.view.View, int, boolean):void
     arg types: [android.view.View, int, int]
     candidates:
      android.support.v7.widget.br.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc):int
      android.support.v7.widget.br.a(android.view.View, int, boolean):void */
    public final void b(View view) {
        a(view, 0, true);
    }

    public int c(cc ccVar) {
        return 0;
    }

    public View c(int i, bw bwVar, cc ccVar) {
        return null;
    }

    public void c(int i) {
    }

    public void c(int i, int i2) {
    }

    public final void c(bw bwVar) {
        for (int o = o() - 1; o >= 0; o--) {
            if (!RecyclerView.b(d(o)).d_()) {
                a(o, bwVar);
            }
        }
    }

    public void c(bw bwVar, cc ccVar) {
        Log.e("RecyclerView", "You must override onLayoutChildren(Recycler recycler, State state) ");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.br.a(android.view.View, int, boolean):void
     arg types: [android.view.View, int, int]
     candidates:
      android.support.v7.widget.br.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc):int
      android.support.v7.widget.br.a(android.view.View, int, boolean):void */
    public final void c(View view) {
        a(view, -1, false);
    }

    public boolean c() {
        return false;
    }

    public int d(cc ccVar) {
        return 0;
    }

    public Parcelable d() {
        return null;
    }

    public final View d(int i) {
        if (this.q != null) {
            return this.q.b(i);
        }
        return null;
    }

    public void d(int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.br.a(android.view.View, int, boolean):void
     arg types: [android.view.View, int, int]
     candidates:
      android.support.v7.widget.br.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc):int
      android.support.v7.widget.br.a(android.view.View, int, boolean):void */
    public final void d(View view) {
        a(view, 0, false);
    }

    public int e(cc ccVar) {
        return 0;
    }

    public void e(int i) {
        if (this.r != null) {
            RecyclerView recyclerView = this.r;
            int a2 = recyclerView.c.a();
            for (int i2 = 0; i2 < a2; i2++) {
                recyclerView.c.b(i2).offsetLeftAndRight(i);
            }
        }
    }

    public boolean e() {
        return false;
    }

    public int f(cc ccVar) {
        return 0;
    }

    public void f(int i) {
        if (this.r != null) {
            RecyclerView recyclerView = this.r;
            int a2 = recyclerView.c.a();
            for (int i2 = 0; i2 < a2; i2++) {
                recyclerView.c.b(i2).offsetTopAndBottom(i);
            }
        }
    }

    public final void f(int i, int i2) {
        this.r.setMeasuredDimension(i, i2);
    }

    public boolean f() {
        return false;
    }

    public void g(int i) {
    }

    public final void k() {
        if (this.r != null) {
            this.r.requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        this.b = true;
    }

    public final boolean m() {
        return this.b;
    }

    public final boolean n() {
        return this.s != null && this.s.e();
    }

    public final int o() {
        if (this.q != null) {
            return this.q.a();
        }
        return 0;
    }

    public final int p() {
        if (this.r != null) {
            return this.r.getWidth();
        }
        return 0;
    }

    public final int q() {
        if (this.r != null) {
            return this.r.getHeight();
        }
        return 0;
    }

    public final int r() {
        if (this.r != null) {
            return this.r.getPaddingLeft();
        }
        return 0;
    }

    public final int s() {
        if (this.r != null) {
            return this.r.getPaddingTop();
        }
        return 0;
    }

    public final int t() {
        if (this.r != null) {
            return this.r.getPaddingRight();
        }
        return 0;
    }

    public final int u() {
        if (this.r != null) {
            return this.r.getPaddingBottom();
        }
        return 0;
    }

    public final int v() {
        bi a2 = this.r != null ? this.r.a() : null;
        if (a2 != null) {
            return a2.a();
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final void w() {
        if (this.s != null) {
            this.s.c();
        }
    }

    public final void x() {
        this.a = true;
    }
}
