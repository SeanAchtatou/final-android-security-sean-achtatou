package android.support.v7.widget;

import android.support.v4.view.bx;
import android.support.v4.widget.at;
import android.view.animation.Interpolator;

final class ce implements Runnable {
    final /* synthetic */ RecyclerView a;
    private int b;
    private int c;
    private at d;
    private Interpolator e = RecyclerView.ao;
    private boolean f = false;
    private boolean g = false;

    public ce(RecyclerView recyclerView) {
        this.a = recyclerView;
        this.d = at.a(recyclerView.getContext(), RecyclerView.ao);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.f) {
            this.g = true;
            return;
        }
        this.a.removeCallbacks(this);
        bx.a(this.a, this);
    }

    public final void a(int i, int i2) {
        this.a.d(2);
        this.c = 0;
        this.b = 0;
        this.d.a(0, i, i2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
        a();
    }

    public final void a(int i, int i2, int i3) {
        a(i, i2, i3, RecyclerView.ao);
    }

    public final void a(int i, int i2, int i3, Interpolator interpolator) {
        if (this.e != interpolator) {
            this.e = interpolator;
            this.d = at.a(this.a.getContext(), interpolator);
        }
        this.a.d(2);
        this.c = 0;
        this.b = 0;
        this.d.a(0, 0, i, i2, i3);
        a();
    }

    public final void b() {
        this.a.removeCallbacks(this);
        this.d.h();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final void b(int i, int i2) {
        int i3;
        int abs = Math.abs(i);
        int abs2 = Math.abs(i2);
        boolean z = abs > abs2;
        int sqrt = (int) Math.sqrt(0.0d);
        int sqrt2 = (int) Math.sqrt((double) ((i * i) + (i2 * i2)));
        int width = z ? this.a.getWidth() : this.a.getHeight();
        int i4 = width / 2;
        float sin = (((float) Math.sin((double) ((float) (((double) (Math.min(1.0f, (((float) sqrt2) * 1.0f) / ((float) width)) - 0.5f)) * 0.4712389167638204d)))) * ((float) i4)) + ((float) i4);
        if (sqrt > 0) {
            i3 = Math.round(1000.0f * Math.abs(sin / ((float) sqrt))) * 4;
        } else {
            i3 = (int) (((((float) (z ? abs : abs2)) / ((float) width)) + 1.0f) * 300.0f);
        }
        a(i, i2, Math.min(i3, 2000));
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r15 = this;
            r0 = 0
            r15.g = r0
            r0 = 1
            r15.f = r0
            android.support.v7.widget.RecyclerView r0 = r15.a
            r0.m()
            android.support.v4.widget.at r7 = r15.d
            android.support.v7.widget.RecyclerView r0 = r15.a
            android.support.v7.widget.br r0 = r0.q
            android.support.v7.widget.ca r8 = r0.s
            boolean r0 = r7.g()
            if (r0 == 0) goto L_0x0151
            int r9 = r7.b()
            int r10 = r7.c()
            int r0 = r15.b
            int r11 = r9 - r0
            int r0 = r15.c
            int r12 = r10 - r0
            r3 = 0
            r1 = 0
            r15.b = r9
            r15.c = r10
            r2 = 0
            r0 = 0
            android.support.v7.widget.RecyclerView r4 = r15.a
            android.support.v7.widget.bi r4 = r4.p
            if (r4 == 0) goto L_0x0182
            android.support.v7.widget.RecyclerView r4 = r15.a
            r4.d()
            android.support.v7.widget.RecyclerView r4 = r15.a
            r4.w()
            java.lang.String r4 = "RV Scroll"
            android.support.v4.os.k.a(r4)
            if (r11 == 0) goto L_0x0060
            android.support.v7.widget.RecyclerView r2 = r15.a
            android.support.v7.widget.br r2 = r2.q
            android.support.v7.widget.RecyclerView r3 = r15.a
            android.support.v7.widget.bw r3 = r3.a
            android.support.v7.widget.RecyclerView r4 = r15.a
            android.support.v7.widget.cc r4 = r4.f
            int r3 = r2.a(r11, r3, r4)
            int r2 = r11 - r3
        L_0x0060:
            if (r12 == 0) goto L_0x0076
            android.support.v7.widget.RecyclerView r0 = r15.a
            android.support.v7.widget.br r0 = r0.q
            android.support.v7.widget.RecyclerView r1 = r15.a
            android.support.v7.widget.bw r1 = r1.a
            android.support.v7.widget.RecyclerView r4 = r15.a
            android.support.v7.widget.cc r4 = r4.f
            int r1 = r0.b(r12, r1, r4)
            int r0 = r12 - r1
        L_0x0076:
            android.support.v4.os.k.a()
            android.support.v7.widget.RecyclerView r4 = r15.a
            r4.E()
            android.support.v7.widget.RecyclerView r4 = r15.a
            r4.x()
            android.support.v7.widget.RecyclerView r4 = r15.a
            r5 = 0
            r4.b(r5)
            if (r8 == 0) goto L_0x0182
            boolean r4 = r8.d()
            if (r4 != 0) goto L_0x0182
            boolean r4 = r8.e()
            if (r4 == 0) goto L_0x0182
            android.support.v7.widget.RecyclerView r4 = r15.a
            android.support.v7.widget.cc r4 = r4.f
            int r4 = r4.e()
            if (r4 != 0) goto L_0x0170
            r8.c()
            r14 = r2
            r2 = r1
            r1 = r14
        L_0x00a7:
            android.support.v7.widget.RecyclerView r4 = r15.a
            java.util.ArrayList r4 = r4.s
            boolean r4 = r4.isEmpty()
            if (r4 != 0) goto L_0x00b8
            android.support.v7.widget.RecyclerView r4 = r15.a
            r4.invalidate()
        L_0x00b8:
            android.support.v7.widget.RecyclerView r4 = r15.a
            int r4 = android.support.v4.view.bx.a(r4)
            r5 = 2
            if (r4 == r5) goto L_0x00c6
            android.support.v7.widget.RecyclerView r4 = r15.a
            r4.d(r11, r12)
        L_0x00c6:
            if (r1 != 0) goto L_0x00ca
            if (r0 == 0) goto L_0x0101
        L_0x00ca:
            float r4 = r7.f()
            int r5 = (int) r4
            r4 = 0
            if (r1 == r9) goto L_0x01a2
            if (r1 >= 0) goto L_0x0187
            int r4 = -r5
        L_0x00d5:
            r6 = r4
        L_0x00d6:
            r4 = 0
            if (r0 == r10) goto L_0x019f
            if (r0 >= 0) goto L_0x018f
            int r5 = -r5
        L_0x00dc:
            android.support.v7.widget.RecyclerView r4 = r15.a
            int r4 = android.support.v4.view.bx.a(r4)
            r13 = 2
            if (r4 == r13) goto L_0x00ea
            android.support.v7.widget.RecyclerView r4 = r15.a
            r4.b(r6, r5)
        L_0x00ea:
            if (r6 != 0) goto L_0x00f4
            if (r1 == r9) goto L_0x00f4
            int r1 = r7.d()
            if (r1 != 0) goto L_0x0101
        L_0x00f4:
            if (r5 != 0) goto L_0x00fe
            if (r0 == r10) goto L_0x00fe
            int r0 = r7.e()
            if (r0 != 0) goto L_0x0101
        L_0x00fe:
            r7.h()
        L_0x0101:
            if (r3 != 0) goto L_0x0105
            if (r2 == 0) goto L_0x010a
        L_0x0105:
            android.support.v7.widget.RecyclerView r0 = r15.a
            r0.c(r3, r2)
        L_0x010a:
            android.support.v7.widget.RecyclerView r0 = r15.a
            boolean r0 = r0.awakenScrollBars()
            if (r0 != 0) goto L_0x0117
            android.support.v7.widget.RecyclerView r0 = r15.a
            r0.invalidate()
        L_0x0117:
            if (r12 == 0) goto L_0x0194
            android.support.v7.widget.RecyclerView r0 = r15.a
            android.support.v7.widget.br r0 = r0.q
            boolean r0 = r0.f()
            if (r0 == 0) goto L_0x0194
            if (r2 != r12) goto L_0x0194
            r0 = 1
            r1 = r0
        L_0x0129:
            if (r11 == 0) goto L_0x0197
            android.support.v7.widget.RecyclerView r0 = r15.a
            android.support.v7.widget.br r0 = r0.q
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0197
            if (r3 != r11) goto L_0x0197
            r0 = 1
        L_0x013a:
            if (r11 != 0) goto L_0x013e
            if (r12 == 0) goto L_0x0142
        L_0x013e:
            if (r0 != 0) goto L_0x0142
            if (r1 == 0) goto L_0x0199
        L_0x0142:
            r0 = 1
        L_0x0143:
            boolean r1 = r7.a()
            if (r1 != 0) goto L_0x014b
            if (r0 != 0) goto L_0x019b
        L_0x014b:
            android.support.v7.widget.RecyclerView r0 = r15.a
            r1 = 0
            r0.d(r1)
        L_0x0151:
            if (r8 == 0) goto L_0x0165
            boolean r0 = r8.d()
            if (r0 == 0) goto L_0x015e
            r0 = 0
            r1 = 0
            android.support.v7.widget.ca.a(r8, r0, r1)
        L_0x015e:
            boolean r0 = r15.g
            if (r0 != 0) goto L_0x0165
            r8.c()
        L_0x0165:
            r0 = 0
            r15.f = r0
            boolean r0 = r15.g
            if (r0 == 0) goto L_0x016f
            r15.a()
        L_0x016f:
            return
        L_0x0170:
            int r5 = r8.f()
            if (r5 < r4) goto L_0x017b
            int r4 = r4 + -1
            r8.b(r4)
        L_0x017b:
            int r4 = r11 - r2
            int r5 = r12 - r0
            android.support.v7.widget.ca.a(r8, r4, r5)
        L_0x0182:
            r14 = r2
            r2 = r1
            r1 = r14
            goto L_0x00a7
        L_0x0187:
            if (r1 <= 0) goto L_0x018c
            r4 = r5
            goto L_0x00d5
        L_0x018c:
            r4 = 0
            goto L_0x00d5
        L_0x018f:
            if (r0 > 0) goto L_0x00dc
            r5 = 0
            goto L_0x00dc
        L_0x0194:
            r0 = 0
            r1 = r0
            goto L_0x0129
        L_0x0197:
            r0 = 0
            goto L_0x013a
        L_0x0199:
            r0 = 0
            goto L_0x0143
        L_0x019b:
            r15.a()
            goto L_0x0151
        L_0x019f:
            r5 = r4
            goto L_0x00dc
        L_0x01a2:
            r6 = r4
            goto L_0x00d6
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ce.run():void");
    }
}
