package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.a.b;
import android.support.v7.internal.widget.bc;
import android.support.v7.internal.widget.be;
import android.util.AttributeSet;
import android.widget.RadioButton;

public class AppCompatRadioButton extends RadioButton {
    private static final int[] a = {16843015};
    private bc b;
    private Drawable c;

    public AppCompatRadioButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.radioButtonStyle);
    }

    public AppCompatRadioButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (bc.a) {
            be a2 = be.a(getContext(), attributeSet, a, i);
            setButtonDrawable(a2.a(0));
            a2.b();
            this.b = a2.c();
        }
    }

    public int getCompoundPaddingLeft() {
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        return (Build.VERSION.SDK_INT >= 17 || this.c == null) ? compoundPaddingLeft : compoundPaddingLeft + this.c.getIntrinsicWidth();
    }

    public void setButtonDrawable(int i) {
        if (this.b != null) {
            setButtonDrawable(this.b.a(i));
        } else {
            super.setButtonDrawable(i);
        }
    }

    public void setButtonDrawable(Drawable drawable) {
        super.setButtonDrawable(drawable);
        this.c = drawable;
    }
}
