package android.support.v7.widget;

import android.util.Log;
import android.view.View;

public abstract class ca {
    private int a = -1;
    private RecyclerView b;
    private br c;
    private boolean d;
    private boolean e;
    private View f;
    private final cb g = new cb();

    static /* synthetic */ void a(ca caVar, int i, int i2) {
        RecyclerView recyclerView = caVar.b;
        if (!caVar.e || caVar.a == -1 || recyclerView == null) {
            caVar.c();
        }
        caVar.d = false;
        if (caVar.f != null) {
            if (RecyclerView.d(caVar.f) == caVar.a) {
                View view = caVar.f;
                cc ccVar = recyclerView.f;
                caVar.a(view, caVar.g);
                cb.a(caVar.g, recyclerView);
                caVar.c();
            } else {
                Log.e("RecyclerView", "Passed over target position while smooth scrolling.");
                caVar.f = null;
            }
        }
        if (caVar.e) {
            cc ccVar2 = recyclerView.f;
            caVar.a(i, i2, caVar.g);
            boolean a2 = caVar.g.a();
            cb.a(caVar.g, recyclerView);
            if (!a2) {
                return;
            }
            if (caVar.e) {
                caVar.d = true;
                recyclerView.ab.a();
                return;
            }
            caVar.c();
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void a(int i, int i2, cb cbVar);

    /* access modifiers changed from: package-private */
    public final void a(RecyclerView recyclerView, br brVar) {
        this.b = recyclerView;
        this.c = brVar;
        if (this.a == -1) {
            throw new IllegalArgumentException("Invalid target position");
        }
        int unused = this.b.f.b = this.a;
        this.e = true;
        this.d = true;
        this.f = this.b.q.b(this.a);
        this.b.ab.a();
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        if (RecyclerView.d(view) == this.a) {
            this.f = view;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(View view, cb cbVar);

    public final br b() {
        return this.c;
    }

    public final void b(int i) {
        this.a = i;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.e) {
            a();
            int unused = this.b.f.b = -1;
            this.f = null;
            this.a = -1;
            this.d = false;
            this.e = false;
            br.a(this.c, this);
            this.c = null;
            this.b = null;
        }
    }

    public final boolean d() {
        return this.d;
    }

    public final boolean e() {
        return this.e;
    }

    public final int f() {
        return this.a;
    }

    public final int g() {
        return this.b.q.o();
    }
}
