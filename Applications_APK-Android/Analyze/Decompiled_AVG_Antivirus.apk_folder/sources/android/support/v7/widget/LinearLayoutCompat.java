package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.bx;
import android.support.v4.view.v;
import android.support.v7.a.l;
import android.support.v7.internal.widget.be;
import android.support.v7.internal.widget.bj;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.core.view.GravityCompat;
import androidx.core.view.InputDeviceCompat;

public class LinearLayoutCompat extends ViewGroup {
    private boolean a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private float g;
    private boolean h;
    private int[] i;
    private int[] j;
    private Drawable k;
    private int l;
    private int m;
    private int n;
    private int o;

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        public float g;
        public int h;

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.h = -1;
            this.g = 0.0f;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.h = -1;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.U);
            this.g = obtainStyledAttributes.getFloat(l.W, 0.0f);
            this.h = obtainStyledAttributes.getInt(l.V, -1);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.h = -1;
        }
    }

    public LinearLayoutCompat(Context context) {
        this(context, null);
    }

    public LinearLayoutCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.be.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.be.a(int, int):int
      android.support.v7.internal.widget.be.a(int, boolean):boolean */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinearLayoutCompat(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        boolean z = true;
        this.a = true;
        this.b = -1;
        this.c = 0;
        this.e = 8388659;
        be a2 = be.a(context, attributeSet, l.T, i2);
        int a3 = a2.a(l.aa, -1);
        if (a3 >= 0 && this.d != a3) {
            this.d = a3;
            requestLayout();
        }
        int a4 = a2.a(l.Z, -1);
        if (a4 >= 0) {
            b(a4);
        }
        boolean a5 = a2.a(l.X, true);
        if (!a5) {
            this.a = a5;
        }
        this.g = a2.e(l.ab);
        this.b = a2.a(l.Y, -1);
        this.h = a2.a(l.ae, false);
        Drawable a6 = a2.a(l.ac);
        if (a6 != this.k) {
            this.k = a6;
            if (a6 != null) {
                this.l = a6.getIntrinsicWidth();
                this.m = a6.getIntrinsicHeight();
            } else {
                this.l = 0;
                this.m = 0;
            }
            setWillNotDraw(a6 != null ? false : z);
            requestLayout();
        }
        this.n = a2.a(l.af, 0);
        this.o = a2.d(l.ad, 0);
        a2.b();
    }

    private void a(int i2, int i3) {
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
        for (int i4 = 0; i4 < i2; i4++) {
            View childAt = getChildAt(i4);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.width == -1) {
                    int i5 = layoutParams.height;
                    layoutParams.height = childAt.getMeasuredHeight();
                    measureChildWithMargins(childAt, makeMeasureSpec, 0, i3, 0);
                    layoutParams.height = i5;
                }
            }
        }
    }

    private void a(Canvas canvas, int i2) {
        this.k.setBounds(getPaddingLeft() + this.o, i2, (getWidth() - getPaddingRight()) - this.o, this.m + i2);
        this.k.draw(canvas);
    }

    private void a(View view, int i2, int i3, int i4, int i5) {
        measureChildWithMargins(view, i2, i3, i4, i5);
    }

    private boolean a(int i2) {
        if (i2 == 0) {
            return (this.n & 1) != 0;
        }
        if (i2 == getChildCount()) {
            return (this.n & 4) != 0;
        }
        if ((this.n & 2) == 0) {
            return false;
        }
        for (int i3 = i2 - 1; i3 >= 0; i3--) {
            if (getChildAt(i3).getVisibility() != 8) {
                return true;
            }
        }
        return false;
    }

    private void b(int i2, int i3) {
        int i4;
        int i5;
        float f2;
        int i6;
        int i7;
        boolean z;
        int i8;
        int i9;
        int i10;
        int i11;
        float f3;
        int baseline;
        View view;
        int i12;
        int i13;
        boolean z2;
        float f4;
        boolean z3;
        int i14;
        int i15;
        boolean z4;
        int i16;
        int i17;
        int i18;
        int i19;
        boolean z5;
        int baseline2;
        this.f = 0;
        int i20 = 0;
        int i21 = 0;
        int i22 = 0;
        int i23 = 0;
        boolean z6 = true;
        float f5 = 0.0f;
        int childCount = getChildCount();
        int mode = View.MeasureSpec.getMode(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        boolean z7 = false;
        boolean z8 = false;
        if (this.i == null || this.j == null) {
            this.i = new int[4];
            this.j = new int[4];
        }
        int[] iArr = this.i;
        int[] iArr2 = this.j;
        iArr[3] = -1;
        iArr[2] = -1;
        iArr[1] = -1;
        iArr[0] = -1;
        iArr2[3] = -1;
        iArr2[2] = -1;
        iArr2[1] = -1;
        iArr2[0] = -1;
        boolean z9 = this.a;
        boolean z10 = this.h;
        boolean z11 = mode == 1073741824;
        int i24 = Integer.MIN_VALUE;
        int i25 = 0;
        while (i25 < childCount) {
            View childAt = getChildAt(i25);
            if (childAt == null) {
                this.f = this.f + 0;
                i18 = i25;
            } else {
                if (childAt.getVisibility() != 8) {
                    if (a(i25)) {
                        this.f = this.f + this.l;
                    }
                    LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                    float f6 = f5 + layoutParams.g;
                    if (mode == 1073741824 && layoutParams.width == 0 && layoutParams.g > 0.0f) {
                        if (z11) {
                            this.f = this.f + layoutParams.leftMargin + layoutParams.rightMargin;
                        } else {
                            int i26 = this.f;
                            this.f = Math.max(i26, layoutParams.leftMargin + i26 + layoutParams.rightMargin);
                        }
                        if (z9) {
                            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                            childAt.measure(makeMeasureSpec, makeMeasureSpec);
                            i19 = i24;
                            z5 = z8;
                        } else {
                            i19 = i24;
                            z5 = true;
                        }
                    } else {
                        int i27 = Integer.MIN_VALUE;
                        if (layoutParams.width == 0 && layoutParams.g > 0.0f) {
                            i27 = 0;
                            layoutParams.width = -2;
                        }
                        int i28 = i27;
                        a(childAt, i2, f6 == 0.0f ? this.f : 0, i3, 0);
                        if (i28 != Integer.MIN_VALUE) {
                            layoutParams.width = i28;
                        }
                        int measuredWidth = childAt.getMeasuredWidth();
                        if (z11) {
                            this.f = this.f + layoutParams.leftMargin + measuredWidth + layoutParams.rightMargin + 0;
                        } else {
                            int i29 = this.f;
                            this.f = Math.max(i29, i29 + measuredWidth + layoutParams.leftMargin + layoutParams.rightMargin + 0);
                        }
                        if (z10) {
                            i19 = Math.max(measuredWidth, i24);
                            z5 = z8;
                        } else {
                            i19 = i24;
                            z5 = z8;
                        }
                    }
                    boolean z12 = false;
                    if (mode2 == 1073741824 || layoutParams.height != -1) {
                        z4 = z7;
                    } else {
                        z4 = true;
                        z12 = true;
                    }
                    int i30 = layoutParams.topMargin + layoutParams.bottomMargin;
                    int measuredHeight = childAt.getMeasuredHeight() + i30;
                    i17 = bj.a(i21, bx.m(childAt));
                    if (z9 && (baseline2 = childAt.getBaseline()) != -1) {
                        int i31 = ((((layoutParams.h < 0 ? this.e : layoutParams.h) & 112) >> 4) & -2) >> 1;
                        iArr[i31] = Math.max(iArr[i31], baseline2);
                        iArr2[i31] = Math.max(iArr2[i31], measuredHeight - baseline2);
                    }
                    int max = Math.max(i20, measuredHeight);
                    boolean z13 = z6 && layoutParams.height == -1;
                    if (layoutParams.g > 0.0f) {
                        int i32 = z12 ? i30 : measuredHeight;
                        f4 = f6;
                        z3 = z13;
                        i15 = i22;
                        z2 = z5;
                        i16 = max;
                        int i33 = i19;
                        i14 = Math.max(i23, i32);
                        i13 = i33;
                    } else {
                        if (!z12) {
                            i30 = measuredHeight;
                        }
                        int max2 = Math.max(i22, i30);
                        f4 = f6;
                        z3 = z13;
                        i15 = max2;
                        z2 = z5;
                        i13 = i19;
                        i14 = i23;
                        i16 = max;
                    }
                } else {
                    i13 = i24;
                    z2 = z8;
                    f4 = f5;
                    z3 = z6;
                    i14 = i23;
                    i15 = i22;
                    z4 = z7;
                    i16 = i20;
                    i17 = i21;
                }
                z6 = z3;
                i23 = i14;
                i22 = i15;
                i21 = i17;
                i20 = i16;
                i24 = i13;
                z7 = z4;
                i18 = i25 + 0;
                f5 = f4;
                z8 = z2;
            }
            i25 = i18 + 1;
        }
        if (this.f > 0 && a(childCount)) {
            this.f = this.f + this.l;
        }
        int max3 = (iArr[1] == -1 && iArr[0] == -1 && iArr[2] == -1 && iArr[3] == -1) ? i20 : Math.max(i20, Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))) + Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))));
        if (z10 && (mode == Integer.MIN_VALUE || mode == 0)) {
            this.f = 0;
            int i34 = 0;
            while (i34 < childCount) {
                View childAt2 = getChildAt(i34);
                if (childAt2 == null) {
                    this.f = this.f + 0;
                    i12 = i34;
                } else if (childAt2.getVisibility() == 8) {
                    i12 = i34 + 0;
                } else {
                    LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                    if (z11) {
                        this.f = layoutParams2.rightMargin + layoutParams2.leftMargin + i24 + 0 + this.f;
                        i12 = i34;
                    } else {
                        int i35 = this.f;
                        this.f = Math.max(i35, layoutParams2.rightMargin + i35 + i24 + layoutParams2.leftMargin + 0);
                        i12 = i34;
                    }
                }
                i34 = i12 + 1;
            }
        }
        this.f = this.f + getPaddingLeft() + getPaddingRight();
        int a2 = bx.a(Math.max(this.f, getSuggestedMinimumWidth()), i2, 0);
        int i36 = (16777215 & a2) - this.f;
        if (z8 || (i36 != 0 && f5 > 0.0f)) {
            if (this.g > 0.0f) {
                f5 = this.g;
            }
            iArr[3] = -1;
            iArr[2] = -1;
            iArr[1] = -1;
            iArr[0] = -1;
            iArr2[3] = -1;
            iArr2[2] = -1;
            iArr2[1] = -1;
            iArr2[0] = -1;
            this.f = 0;
            int i37 = 0;
            boolean z14 = z6;
            int i38 = i22;
            int i39 = -1;
            int i40 = i21;
            while (i37 < childCount) {
                View childAt3 = getChildAt(i37);
                if (childAt3 == null || childAt3.getVisibility() == 8) {
                    f2 = f5;
                    i6 = i36;
                    i7 = i38;
                    z = z14;
                    i8 = i40;
                    i9 = i39;
                } else {
                    LayoutParams layoutParams3 = (LayoutParams) childAt3.getLayoutParams();
                    float f7 = layoutParams3.g;
                    if (f7 > 0.0f) {
                        int i41 = (int) ((((float) i36) * f7) / f5);
                        float f8 = f5 - f7;
                        int i42 = i36 - i41;
                        int childMeasureSpec = getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + layoutParams3.topMargin + layoutParams3.bottomMargin, layoutParams3.height);
                        if (layoutParams3.width != 0 || mode != 1073741824) {
                            i41 += childAt3.getMeasuredWidth();
                            if (i41 < 0) {
                                i41 = 0;
                            }
                            view = childAt3;
                        } else if (i41 > 0) {
                            view = childAt3;
                        } else {
                            i41 = 0;
                            view = childAt3;
                        }
                        view.measure(View.MeasureSpec.makeMeasureSpec(i41, 1073741824), childMeasureSpec);
                        i11 = bj.a(i40, bx.m(childAt3) & -16777216);
                        f3 = f8;
                        i10 = i42;
                    } else {
                        i10 = i36;
                        i11 = i40;
                        f3 = f5;
                    }
                    if (z11) {
                        this.f = this.f + childAt3.getMeasuredWidth() + layoutParams3.leftMargin + layoutParams3.rightMargin + 0;
                    } else {
                        int i43 = this.f;
                        this.f = Math.max(i43, childAt3.getMeasuredWidth() + i43 + layoutParams3.leftMargin + layoutParams3.rightMargin + 0);
                    }
                    boolean z15 = mode2 != 1073741824 && layoutParams3.height == -1;
                    int i44 = layoutParams3.topMargin + layoutParams3.bottomMargin;
                    int measuredHeight2 = childAt3.getMeasuredHeight() + i44;
                    int max4 = Math.max(i39, measuredHeight2);
                    int max5 = Math.max(i38, z15 ? i44 : measuredHeight2);
                    boolean z16 = z14 && layoutParams3.height == -1;
                    if (z9 && (baseline = childAt3.getBaseline()) != -1) {
                        int i45 = ((((layoutParams3.h < 0 ? this.e : layoutParams3.h) & 112) >> 4) & -2) >> 1;
                        iArr[i45] = Math.max(iArr[i45], baseline);
                        iArr2[i45] = Math.max(iArr2[i45], measuredHeight2 - baseline);
                    }
                    f2 = f3;
                    i7 = max5;
                    i8 = i11;
                    z = z16;
                    i9 = max4;
                    i6 = i10;
                }
                i37++;
                z14 = z;
                i38 = i7;
                i39 = i9;
                i40 = i8;
                f5 = f2;
                i36 = i6;
            }
            this.f = this.f + getPaddingLeft() + getPaddingRight();
            if (!(iArr[1] == -1 && iArr[0] == -1 && iArr[2] == -1 && iArr[3] == -1)) {
                i39 = Math.max(i39, Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))) + Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))));
            }
            i5 = i38;
            i21 = i40;
            i4 = i39;
            z6 = z14;
        } else {
            int max6 = Math.max(i22, i23);
            if (z10 && mode != 1073741824) {
                int i46 = 0;
                while (true) {
                    int i47 = i46;
                    if (i47 >= childCount) {
                        break;
                    }
                    View childAt4 = getChildAt(i47);
                    if (!(childAt4 == null || childAt4.getVisibility() == 8 || ((LayoutParams) childAt4.getLayoutParams()).g <= 0.0f)) {
                        childAt4.measure(View.MeasureSpec.makeMeasureSpec(i24, 1073741824), View.MeasureSpec.makeMeasureSpec(childAt4.getMeasuredHeight(), 1073741824));
                    }
                    i46 = i47 + 1;
                }
            }
            i5 = max6;
            i4 = max3;
        }
        if (z6 || mode2 == 1073741824) {
            i5 = i4;
        }
        setMeasuredDimension((-16777216 & i21) | a2, bx.a(Math.max(i5 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i3, i21 << 16));
        if (z7) {
            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
            int i48 = 0;
            while (true) {
                int i49 = i48;
                if (i49 < childCount) {
                    View childAt5 = getChildAt(i49);
                    if (childAt5.getVisibility() != 8) {
                        LayoutParams layoutParams4 = (LayoutParams) childAt5.getLayoutParams();
                        if (layoutParams4.height == -1) {
                            int i50 = layoutParams4.width;
                            layoutParams4.width = childAt5.getMeasuredWidth();
                            measureChildWithMargins(childAt5, i2, 0, makeMeasureSpec2, 0);
                            layoutParams4.width = i50;
                        }
                    }
                    i48 = i49 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private void b(Canvas canvas, int i2) {
        this.k.setBounds(i2, getPaddingTop() + this.o, this.l + i2, (getHeight() - getPaddingBottom()) - this.o);
        this.k.draw(canvas);
    }

    private static void b(View view, int i2, int i3, int i4, int i5) {
        view.layout(i2, i3, i2 + i4, i3 + i5);
    }

    /* renamed from: a */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public final void b(int i2) {
        if (this.e != i2) {
            int i3 = (8388615 & i2) == 0 ? 8388611 | i2 : i2;
            if ((i3 & 112) == 0) {
                i3 |= 48;
            }
            this.e = i3;
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public int getBaseline() {
        int i2;
        int i3;
        if (this.b < 0) {
            return super.getBaseline();
        }
        if (getChildCount() <= this.b) {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
        }
        View childAt = getChildAt(this.b);
        int baseline = childAt.getBaseline();
        if (baseline != -1) {
            int i4 = this.c;
            if (this.d == 1 && (i3 = this.e & 112) != 48) {
                switch (i3) {
                    case 16:
                        i2 = i4 + (((((getBottom() - getTop()) - getPaddingTop()) - getPaddingBottom()) - this.f) / 2);
                        break;
                    case 80:
                        i2 = ((getBottom() - getTop()) - getPaddingBottom()) - this.f;
                        break;
                }
                return ((LayoutParams) childAt.getLayoutParams()).topMargin + i2 + baseline;
            }
            i2 = i4;
            return ((LayoutParams) childAt.getLayoutParams()).topMargin + i2 + baseline;
        } else if (this.b == 0) {
            return -1;
        } else {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public LayoutParams generateDefaultLayoutParams() {
        if (this.d == 0) {
            return new LayoutParams(-2, -2);
        }
        if (this.d == 1) {
            return new LayoutParams(-1, -2);
        }
        return null;
    }

    public final int l() {
        return this.l;
    }

    public final void m() {
        this.a = false;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int left;
        if (this.k != null) {
            if (this.d == 1) {
                int childCount = getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = getChildAt(i2);
                    if (!(childAt == null || childAt.getVisibility() == 8 || !a(i2))) {
                        a(canvas, (childAt.getTop() - ((LayoutParams) childAt.getLayoutParams()).topMargin) - this.m);
                    }
                }
                if (a(childCount)) {
                    View childAt2 = getChildAt(childCount - 1);
                    a(canvas, childAt2 == null ? (getHeight() - getPaddingBottom()) - this.m : ((LayoutParams) childAt2.getLayoutParams()).bottomMargin + childAt2.getBottom());
                    return;
                }
                return;
            }
            int childCount2 = getChildCount();
            boolean a2 = bj.a(this);
            for (int i3 = 0; i3 < childCount2; i3++) {
                View childAt3 = getChildAt(i3);
                if (!(childAt3 == null || childAt3.getVisibility() == 8 || !a(i3))) {
                    LayoutParams layoutParams = (LayoutParams) childAt3.getLayoutParams();
                    b(canvas, a2 ? layoutParams.rightMargin + childAt3.getRight() : (childAt3.getLeft() - layoutParams.leftMargin) - this.l);
                }
            }
            if (a(childCount2)) {
                View childAt4 = getChildAt(childCount2 - 1);
                if (childAt4 == null) {
                    left = a2 ? getPaddingLeft() : (getWidth() - getPaddingRight()) - this.l;
                } else {
                    LayoutParams layoutParams2 = (LayoutParams) childAt4.getLayoutParams();
                    left = a2 ? (childAt4.getLeft() - layoutParams2.leftMargin) - this.l : layoutParams2.rightMargin + childAt4.getRight();
                }
                b(canvas, left);
            }
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (Build.VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(LinearLayoutCompat.class.getName());
        }
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        if (Build.VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName(LinearLayoutCompat.class.getName());
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int paddingLeft;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int paddingTop;
        int i11;
        int i12;
        if (this.d == 1) {
            int paddingLeft2 = getPaddingLeft();
            int i13 = i4 - i2;
            int paddingRight = i13 - getPaddingRight();
            int paddingRight2 = (i13 - paddingLeft2) - getPaddingRight();
            int childCount = getChildCount();
            int i14 = this.e & 112;
            int i15 = 8388615 & this.e;
            switch (i14) {
                case 16:
                    paddingTop = getPaddingTop() + (((i5 - i3) - this.f) / 2);
                    break;
                case 80:
                    paddingTop = ((getPaddingTop() + i5) - i3) - this.f;
                    break;
                default:
                    paddingTop = getPaddingTop();
                    break;
            }
            int i16 = 0;
            int i17 = paddingTop;
            while (i16 < childCount) {
                View childAt = getChildAt(i16);
                if (childAt == null) {
                    i17 += 0;
                    i11 = i16;
                } else if (childAt.getVisibility() != 8) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                    int i18 = layoutParams.h;
                    if (i18 < 0) {
                        i18 = i15;
                    }
                    switch (v.a(i18, bx.h(this)) & 7) {
                        case 1:
                            i12 = ((((paddingRight2 - measuredWidth) / 2) + paddingLeft2) + layoutParams.leftMargin) - layoutParams.rightMargin;
                            break;
                        case 5:
                            i12 = (paddingRight - measuredWidth) - layoutParams.rightMargin;
                            break;
                        default:
                            i12 = layoutParams.leftMargin + paddingLeft2;
                            break;
                    }
                    if (a(i16)) {
                        i17 += this.m;
                    }
                    int i19 = i17 + layoutParams.topMargin;
                    b(childAt, i12, i19 + 0, measuredWidth, measuredHeight);
                    i17 = i19 + layoutParams.bottomMargin + measuredHeight + 0;
                    i11 = i16 + 0;
                } else {
                    i11 = i16;
                }
                i16 = i11 + 1;
            }
            return;
        }
        boolean a2 = bj.a(this);
        int paddingTop2 = getPaddingTop();
        int i20 = i5 - i3;
        int paddingBottom = i20 - getPaddingBottom();
        int paddingBottom2 = (i20 - paddingTop2) - getPaddingBottom();
        int childCount2 = getChildCount();
        int i21 = this.e & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        int i22 = this.e & 112;
        boolean z2 = this.a;
        int[] iArr = this.i;
        int[] iArr2 = this.j;
        switch (v.a(i21, bx.h(this))) {
            case 1:
                paddingLeft = getPaddingLeft() + (((i4 - i2) - this.f) / 2);
                break;
            case 5:
                paddingLeft = ((getPaddingLeft() + i4) - i2) - this.f;
                break;
            default:
                paddingLeft = getPaddingLeft();
                break;
        }
        if (a2) {
            i6 = childCount2 - 1;
            i7 = -1;
        } else {
            i6 = 0;
            i7 = 1;
        }
        int i23 = 0;
        while (i23 < childCount2) {
            int i24 = i6 + (i7 * i23);
            View childAt2 = getChildAt(i24);
            if (childAt2 == null) {
                paddingLeft += 0;
                i8 = i23;
            } else if (childAt2.getVisibility() != 8) {
                int measuredWidth2 = childAt2.getMeasuredWidth();
                int measuredHeight2 = childAt2.getMeasuredHeight();
                int i25 = -1;
                LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                if (z2 && layoutParams2.height != -1) {
                    i25 = childAt2.getBaseline();
                }
                int i26 = layoutParams2.h;
                if (i26 < 0) {
                    i26 = i22;
                }
                switch (i26 & 112) {
                    case 16:
                        i10 = ((((paddingBottom2 - measuredHeight2) / 2) + paddingTop2) + layoutParams2.topMargin) - layoutParams2.bottomMargin;
                        break;
                    case 48:
                        i9 = layoutParams2.topMargin + paddingTop2;
                        if (i25 != -1) {
                            i10 = (iArr[1] - i25) + i9;
                            break;
                        }
                        i10 = i9;
                        break;
                    case 80:
                        i9 = (paddingBottom - measuredHeight2) - layoutParams2.bottomMargin;
                        if (i25 != -1) {
                            i10 = i9 - (iArr2[2] - (childAt2.getMeasuredHeight() - i25));
                            break;
                        }
                        i10 = i9;
                        break;
                    default:
                        i10 = paddingTop2;
                        break;
                }
                int i27 = (a(i24) ? this.l + paddingLeft : paddingLeft) + layoutParams2.leftMargin;
                b(childAt2, i27 + 0, i10, measuredWidth2, measuredHeight2);
                paddingLeft = i27 + layoutParams2.rightMargin + measuredWidth2 + 0;
                i8 = i23 + 0;
            } else {
                i8 = i23;
            }
            i23 = i8 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        float f2;
        boolean z;
        int i6;
        int i7;
        int i8;
        int i9;
        View view;
        int i10;
        int i11;
        boolean z2;
        float f3;
        boolean z3;
        int i12;
        int i13;
        boolean z4;
        int i14;
        int i15;
        int i16;
        int i17;
        boolean z5;
        if (this.d == 1) {
            this.f = 0;
            int i18 = 0;
            int i19 = 0;
            int i20 = 0;
            int i21 = 0;
            boolean z6 = true;
            float f4 = 0.0f;
            int childCount = getChildCount();
            int mode = View.MeasureSpec.getMode(i2);
            int mode2 = View.MeasureSpec.getMode(i3);
            boolean z7 = false;
            boolean z8 = false;
            int i22 = this.b;
            boolean z9 = this.h;
            int i23 = Integer.MIN_VALUE;
            int i24 = 0;
            while (i24 < childCount) {
                View childAt = getChildAt(i24);
                if (childAt == null) {
                    this.f = this.f + 0;
                    i16 = i24;
                } else {
                    if (childAt.getVisibility() != 8) {
                        if (a(i24)) {
                            this.f = this.f + this.m;
                        }
                        LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                        float f5 = f4 + layoutParams.g;
                        if (mode2 == 1073741824 && layoutParams.height == 0 && layoutParams.g > 0.0f) {
                            int i25 = this.f;
                            this.f = Math.max(i25, layoutParams.topMargin + i25 + layoutParams.bottomMargin);
                            i17 = i23;
                            z5 = true;
                        } else {
                            int i26 = Integer.MIN_VALUE;
                            if (layoutParams.height == 0 && layoutParams.g > 0.0f) {
                                i26 = 0;
                                layoutParams.height = -2;
                            }
                            int i27 = i26;
                            a(childAt, i2, 0, i3, f5 == 0.0f ? this.f : 0);
                            if (i27 != Integer.MIN_VALUE) {
                                layoutParams.height = i27;
                            }
                            int measuredHeight = childAt.getMeasuredHeight();
                            int i28 = this.f;
                            this.f = Math.max(i28, i28 + measuredHeight + layoutParams.topMargin + layoutParams.bottomMargin + 0);
                            if (z9) {
                                i17 = Math.max(measuredHeight, i23);
                                z5 = z8;
                            } else {
                                i17 = i23;
                                z5 = z8;
                            }
                        }
                        if (i22 >= 0 && i22 == i24 + 1) {
                            this.c = this.f;
                        }
                        if (i24 >= i22 || layoutParams.g <= 0.0f) {
                            boolean z10 = false;
                            if (mode == 1073741824 || layoutParams.width != -1) {
                                z4 = z7;
                            } else {
                                z4 = true;
                                z10 = true;
                            }
                            int i29 = layoutParams.leftMargin + layoutParams.rightMargin;
                            int measuredWidth = childAt.getMeasuredWidth() + i29;
                            int max = Math.max(i18, measuredWidth);
                            i15 = bj.a(i19, bx.m(childAt));
                            boolean z11 = z6 && layoutParams.width == -1;
                            if (layoutParams.g > 0.0f) {
                                int i30 = z10 ? i29 : measuredWidth;
                                f3 = f5;
                                z3 = z11;
                                i13 = i20;
                                z2 = z5;
                                i14 = max;
                                int i31 = i17;
                                i12 = Math.max(i21, i30);
                                i11 = i31;
                            } else {
                                if (!z10) {
                                    i29 = measuredWidth;
                                }
                                int max2 = Math.max(i20, i29);
                                f3 = f5;
                                z3 = z11;
                                i13 = max2;
                                z2 = z5;
                                i11 = i17;
                                i12 = i21;
                                i14 = max;
                            }
                        } else {
                            throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
                        }
                    } else {
                        i11 = i23;
                        z2 = z8;
                        f3 = f4;
                        z3 = z6;
                        i12 = i21;
                        i13 = i20;
                        z4 = z7;
                        i14 = i18;
                        i15 = i19;
                    }
                    z6 = z3;
                    i21 = i12;
                    i20 = i13;
                    i19 = i15;
                    i18 = i14;
                    i23 = i11;
                    z7 = z4;
                    i16 = i24 + 0;
                    f4 = f3;
                    z8 = z2;
                }
                i24 = i16 + 1;
            }
            if (this.f > 0 && a(childCount)) {
                this.f = this.f + this.m;
            }
            if (z9 && (mode2 == Integer.MIN_VALUE || mode2 == 0)) {
                this.f = 0;
                int i32 = 0;
                while (i32 < childCount) {
                    View childAt2 = getChildAt(i32);
                    if (childAt2 == null) {
                        this.f = this.f + 0;
                        i10 = i32;
                    } else if (childAt2.getVisibility() == 8) {
                        i10 = i32 + 0;
                    } else {
                        LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                        int i33 = this.f;
                        this.f = Math.max(i33, layoutParams2.bottomMargin + i33 + i23 + layoutParams2.topMargin + 0);
                        i10 = i32;
                    }
                    i32 = i10 + 1;
                }
            }
            this.f = this.f + getPaddingTop() + getPaddingBottom();
            int a2 = bx.a(Math.max(this.f, getSuggestedMinimumHeight()), i3, 0);
            int i34 = (16777215 & a2) - this.f;
            if (z8 || (i34 != 0 && f4 > 0.0f)) {
                if (this.g > 0.0f) {
                    f4 = this.g;
                }
                this.f = 0;
                int i35 = 0;
                boolean z12 = z6;
                int i36 = i20;
                int i37 = i19;
                int i38 = i18;
                while (i35 < childCount) {
                    View childAt3 = getChildAt(i35);
                    if (childAt3.getVisibility() != 8) {
                        LayoutParams layoutParams3 = (LayoutParams) childAt3.getLayoutParams();
                        float f6 = layoutParams3.g;
                        if (f6 > 0.0f) {
                            int i39 = (int) ((((float) i34) * f6) / f4);
                            float f7 = f4 - f6;
                            int i40 = i34 - i39;
                            int childMeasureSpec = getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + layoutParams3.leftMargin + layoutParams3.rightMargin, layoutParams3.width);
                            if (layoutParams3.height != 0 || mode2 != 1073741824) {
                                i39 += childAt3.getMeasuredHeight();
                                if (i39 < 0) {
                                    i39 = 0;
                                }
                                view = childAt3;
                            } else if (i39 > 0) {
                                view = childAt3;
                            } else {
                                i39 = 0;
                                view = childAt3;
                            }
                            view.measure(childMeasureSpec, View.MeasureSpec.makeMeasureSpec(i39, 1073741824));
                            i8 = i40;
                            i9 = bj.a(i37, bx.m(childAt3) & InputDeviceCompat.SOURCE_ANY);
                            f2 = f7;
                        } else {
                            f2 = f4;
                            i8 = i34;
                            i9 = i37;
                        }
                        int i41 = layoutParams3.leftMargin + layoutParams3.rightMargin;
                        int measuredWidth2 = childAt3.getMeasuredWidth() + i41;
                        int max3 = Math.max(i38, measuredWidth2);
                        if (!(mode != 1073741824 && layoutParams3.width == -1)) {
                            i41 = measuredWidth2;
                        }
                        int max4 = Math.max(i36, i41);
                        z = z12 && layoutParams3.width == -1;
                        int i42 = this.f;
                        this.f = Math.max(i42, layoutParams3.bottomMargin + childAt3.getMeasuredHeight() + i42 + layoutParams3.topMargin + 0);
                        i6 = max4;
                        i7 = max3;
                    } else {
                        f2 = f4;
                        z = z12;
                        i6 = i36;
                        i7 = i38;
                        i8 = i34;
                        i9 = i37;
                    }
                    i35++;
                    z12 = z;
                    i36 = i6;
                    i37 = i9;
                    i38 = i7;
                    i34 = i8;
                    f4 = f2;
                }
                this.f = this.f + getPaddingTop() + getPaddingBottom();
                i5 = i36;
                i19 = i37;
                i4 = i38;
                z6 = z12;
            } else {
                int max5 = Math.max(i20, i21);
                if (z9 && mode2 != 1073741824) {
                    int i43 = 0;
                    while (true) {
                        int i44 = i43;
                        if (i44 >= childCount) {
                            break;
                        }
                        View childAt4 = getChildAt(i44);
                        if (!(childAt4 == null || childAt4.getVisibility() == 8 || ((LayoutParams) childAt4.getLayoutParams()).g <= 0.0f)) {
                            childAt4.measure(View.MeasureSpec.makeMeasureSpec(childAt4.getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(i23, 1073741824));
                        }
                        i43 = i44 + 1;
                    }
                }
                i5 = max5;
                i4 = i18;
            }
            if (z6 || mode == 1073741824) {
                i5 = i4;
            }
            setMeasuredDimension(bx.a(Math.max(i5 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i2, i19), a2);
            if (z7) {
                a(childCount, i3);
                return;
            }
            return;
        }
        b(i2, i3);
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }
}
