package android.support.v7.widget;

final class q {
    long a = 0;
    q b;

    q() {
    }

    private void a() {
        if (this.b == null) {
            this.b = new q();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        while (i >= 64) {
            this.a();
            this = this.b;
            i -= 64;
        }
        this.a |= 1 << i;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i, boolean z) {
        while (true) {
            if (i >= 64) {
                r10.a();
                r10 = r10.b;
                i -= 64;
            } else {
                boolean z2 = (r10.a & Long.MIN_VALUE) != 0;
                long j = (1 << i) - 1;
                r10.a = (((j ^ -1) & r10.a) << 1) | (r10.a & j);
                if (z) {
                    r10.a(i);
                } else {
                    r10.b(i);
                }
                if (z2 || r10.b != null) {
                    r10.a();
                    r10 = r10.b;
                    i = 0;
                    z = z2;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i) {
        while (i >= 64) {
            if (this.b != null) {
                this = this.b;
                i -= 64;
            } else {
                return;
            }
        }
        this.a &= (1 << i) ^ -1;
    }

    /* access modifiers changed from: package-private */
    public final boolean c(int i) {
        while (i >= 64) {
            this.a();
            this = this.b;
            i -= 64;
        }
        return (this.a & (1 << i)) != 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean d(int i) {
        while (i >= 64) {
            this.a();
            this = this.b;
            i -= 64;
        }
        long j = 1 << i;
        boolean z = (this.a & j) != 0;
        this.a &= j ^ -1;
        long j2 = j - 1;
        this.a = Long.rotateRight((j2 ^ -1) & this.a, 1) | (this.a & j2);
        if (this.b != null) {
            if (this.b.c(0)) {
                this.a(63);
            }
            this.b.d(0);
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public final int e(int i) {
        return this.b == null ? i >= 64 ? Long.bitCount(this.a) : Long.bitCount(this.a & ((1 << i) - 1)) : i < 64 ? Long.bitCount(this.a & ((1 << i) - 1)) : this.b.e(i - 64) + Long.bitCount(this.a);
    }

    public final String toString() {
        return this.b == null ? Long.toBinaryString(this.a) : this.b.toString() + "xx" + Long.toBinaryString(this.a);
    }
}
