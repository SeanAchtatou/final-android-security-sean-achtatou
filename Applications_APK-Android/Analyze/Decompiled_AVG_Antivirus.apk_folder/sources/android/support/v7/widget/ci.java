package android.support.v7.widget;

import android.view.View;

final class ci {
    static int a(cc ccVar, az azVar, View view, View view2, br brVar, boolean z) {
        if (brVar.o() == 0 || ccVar.e() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(br.e(view) - br.e(view2)) + 1;
        }
        return Math.min(azVar.f(), azVar.b(view2) - azVar.a(view));
    }

    static int a(cc ccVar, az azVar, View view, View view2, br brVar, boolean z, boolean z2) {
        if (brVar.o() == 0 || ccVar.e() == 0 || view == null || view2 == null) {
            return 0;
        }
        int max = z2 ? Math.max(0, (ccVar.e() - Math.max(br.e(view), br.e(view2))) - 1) : Math.max(0, Math.min(br.e(view), br.e(view2)));
        if (!z) {
            return max;
        }
        return Math.round((((float) max) * (((float) Math.abs(azVar.b(view2) - azVar.a(view))) / ((float) (Math.abs(br.e(view) - br.e(view2)) + 1)))) + ((float) (azVar.c() - azVar.a(view))));
    }

    static int b(cc ccVar, az azVar, View view, View view2, br brVar, boolean z) {
        if (brVar.o() == 0 || ccVar.e() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return ccVar.e();
        }
        return (int) ((((float) (azVar.b(view2) - azVar.a(view))) / ((float) (Math.abs(br.e(view) - br.e(view2)) + 1))) * ((float) ccVar.e()));
    }
}
