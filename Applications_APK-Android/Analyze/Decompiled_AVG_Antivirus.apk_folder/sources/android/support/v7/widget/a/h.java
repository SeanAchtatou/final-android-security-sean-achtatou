package android.support.v7.widget.a;

import android.view.animation.Interpolator;

final class h implements Interpolator {
    h() {
    }

    public final float getInterpolation(float f) {
        return f * f * f * f * f;
    }
}
