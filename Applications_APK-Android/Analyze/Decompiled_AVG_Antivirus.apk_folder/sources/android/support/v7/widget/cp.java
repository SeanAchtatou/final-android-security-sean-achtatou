package android.support.v7.widget;

import android.view.View;

final class cp implements View.OnFocusChangeListener {
    final /* synthetic */ SearchView a;

    cp(SearchView searchView) {
        this.a = searchView;
    }

    public final void onFocusChange(View view, boolean z) {
        if (this.a.v != null) {
            this.a.v.onFocusChange(this.a, z);
        }
    }
}
