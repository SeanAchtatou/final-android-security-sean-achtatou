package android.support.v7.widget;

import java.util.ArrayList;
import java.util.Iterator;

final class v implements Runnable {
    final /* synthetic */ ArrayList a;
    final /* synthetic */ s b;

    v(s sVar, ArrayList arrayList) {
        this.b = sVar;
        this.a = arrayList;
    }

    public final void run() {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            s.a(this.b, (cf) it.next());
        }
        this.a.clear();
        this.b.f.remove(this.a);
    }
}
