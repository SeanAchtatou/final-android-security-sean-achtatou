package android.support.v7.widget;

import android.content.Context;
import android.os.Parcelable;
import android.support.v7.d.c;
import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.x;
import android.support.v7.widget.Toolbar;

final class dl implements x {
    i a;
    m b;
    final /* synthetic */ Toolbar c;

    private dl(Toolbar toolbar) {
        this.c = toolbar;
    }

    /* synthetic */ dl(Toolbar toolbar, byte b2) {
        this(toolbar);
    }

    public final void a(Context context, i iVar) {
        if (!(this.a == null || this.b == null)) {
            this.a.b(this.b);
        }
        this.a = iVar;
    }

    public final void a(Parcelable parcelable) {
    }

    public final void a(i iVar, boolean z) {
    }

    public final void a(boolean z) {
        boolean z2 = false;
        if (this.b != null) {
            if (this.a != null) {
                int size = this.a.size();
                int i = 0;
                while (true) {
                    if (i >= size) {
                        break;
                    } else if (this.a.getItem(i) == this.b) {
                        z2 = true;
                        break;
                    } else {
                        i++;
                    }
                }
            }
            if (!z2) {
                b(this.b);
            }
        }
    }

    public final boolean a() {
        return false;
    }

    public final boolean a(ad adVar) {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.a(android.support.v7.widget.Toolbar, boolean):void
     arg types: [android.support.v7.widget.Toolbar, int]
     candidates:
      android.support.v7.widget.Toolbar.a(android.view.View, int):int
      android.support.v7.widget.Toolbar.a(java.util.List, int):void
      android.support.v7.widget.Toolbar.a(int, int):void
      android.support.v7.widget.Toolbar.a(android.content.Context, int):void
      android.support.v7.widget.Toolbar.a(android.support.v7.internal.view.menu.i, android.support.v7.widget.ActionMenuPresenter):void
      android.support.v7.widget.Toolbar.a(android.support.v7.internal.view.menu.y, android.support.v7.internal.view.menu.j):void
      android.support.v7.widget.Toolbar.a(android.support.v7.widget.Toolbar, boolean):void */
    public final boolean a(m mVar) {
        Toolbar.b(this.c);
        if (this.c.i.getParent() != this.c) {
            this.c.addView(this.c.i);
        }
        this.c.a = mVar.getActionView();
        this.b = mVar;
        if (this.c.a.getParent() != this.c) {
            Toolbar.LayoutParams n = Toolbar.n();
            n.a = 8388611 | (this.c.n & 112);
            n.b = 2;
            this.c.a.setLayoutParams(n);
            this.c.addView(this.c.a);
        }
        Toolbar.a(this.c, true);
        this.c.requestLayout();
        mVar.e(true);
        if (this.c.a instanceof c) {
            ((c) this.c.a).a();
        }
        return true;
    }

    public final int b() {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.a(android.support.v7.widget.Toolbar, boolean):void
     arg types: [android.support.v7.widget.Toolbar, int]
     candidates:
      android.support.v7.widget.Toolbar.a(android.view.View, int):int
      android.support.v7.widget.Toolbar.a(java.util.List, int):void
      android.support.v7.widget.Toolbar.a(int, int):void
      android.support.v7.widget.Toolbar.a(android.content.Context, int):void
      android.support.v7.widget.Toolbar.a(android.support.v7.internal.view.menu.i, android.support.v7.widget.ActionMenuPresenter):void
      android.support.v7.widget.Toolbar.a(android.support.v7.internal.view.menu.y, android.support.v7.internal.view.menu.j):void
      android.support.v7.widget.Toolbar.a(android.support.v7.widget.Toolbar, boolean):void */
    public final boolean b(m mVar) {
        if (this.c.a instanceof c) {
            ((c) this.c.a).b();
        }
        this.c.removeView(this.c.a);
        this.c.removeView(this.c.i);
        this.c.a = null;
        Toolbar.a(this.c, false);
        this.b = null;
        this.c.requestLayout();
        mVar.e(false);
        return true;
    }

    public final Parcelable d() {
        return null;
    }
}
