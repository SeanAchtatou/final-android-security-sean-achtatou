package android.support.v7.widget;

final class bp implements bn {
    final /* synthetic */ RecyclerView a;

    private bp(RecyclerView recyclerView) {
        this.a = recyclerView;
    }

    /* synthetic */ bp(RecyclerView recyclerView, byte b) {
        this(recyclerView);
    }

    public final void a(cf cfVar) {
        cfVar.a(true);
        if (cfVar.g != null && cfVar.h == null) {
            cfVar.g = null;
        }
        cfVar.h = null;
        if (!cf.g(cfVar) && !RecyclerView.c(this.a, cfVar.a) && cfVar.n()) {
            this.a.removeDetachedView(cfVar.a, false);
        }
    }
}
