package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.n;
import android.support.v4.view.o;
import android.support.v7.a.h;
import android.support.v7.a.i;
import android.support.v7.internal.view.a;
import android.support.v7.internal.view.menu.ActionMenuItemView;
import android.support.v7.internal.view.menu.aa;
import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.d;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.z;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public final class ActionMenuPresenter extends d implements o {
    final g g = new g(this, (byte) 0);
    int h;
    /* access modifiers changed from: private */
    public View i;
    private boolean j;
    private boolean k;
    private int l;
    private int m;
    private int n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    private final SparseBooleanArray t = new SparseBooleanArray();
    private View u;
    /* access modifiers changed from: private */
    public f v;
    /* access modifiers changed from: private */
    public a w;
    /* access modifiers changed from: private */
    public c x;
    private b y;

    class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new h();
        public int a;

        SavedState() {
        }

        SavedState(Parcel parcel) {
            this.a = parcel.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
        }
    }

    public ActionMenuPresenter(Context context) {
        super(context, i.c, i.b);
    }

    public final z a(ViewGroup viewGroup) {
        z a = super.a(viewGroup);
        ((ActionMenuView) a).a(this);
        return a;
    }

    public final View a(m mVar, View view, ViewGroup viewGroup) {
        View actionView = mVar.getActionView();
        if (actionView == null || mVar.m()) {
            actionView = super.a(mVar, view, viewGroup);
        }
        actionView.setVisibility(mVar.isActionViewExpanded() ? 8 : 0);
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!((ActionMenuView) viewGroup).checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(ActionMenuView.a(layoutParams));
        }
        return actionView;
    }

    public final void a(Context context, android.support.v7.internal.view.menu.i iVar) {
        super.a(context, iVar);
        Resources resources = context.getResources();
        a a = a.a(context);
        if (!this.k) {
            this.j = a.b();
        }
        if (!this.q) {
            this.l = a.c();
        }
        if (!this.o) {
            this.n = a.a();
        }
        int i2 = this.l;
        if (this.j) {
            if (this.i == null) {
                this.i = new d(this, this.a);
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.i.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i2 -= this.i.getMeasuredWidth();
        } else {
            this.i = null;
        }
        this.m = i2;
        this.s = (int) (56.0f * resources.getDisplayMetrics().density);
        this.u = null;
    }

    public final void a(Parcelable parcelable) {
        MenuItem findItem;
        SavedState savedState = (SavedState) parcelable;
        if (savedState.a > 0 && (findItem = this.c.findItem(savedState.a)) != null) {
            a((ad) findItem.getSubMenu());
        }
    }

    public final void a(android.support.v7.internal.view.menu.i iVar, boolean z) {
        k();
        super.a(iVar, z);
    }

    public final void a(m mVar, aa aaVar) {
        aaVar.a(mVar);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aaVar;
        actionMenuItemView.a((ActionMenuView) this.f);
        if (this.y == null) {
            this.y = new b(this, (byte) 0);
        }
        actionMenuItemView.a(this.y);
    }

    public final void a(ActionMenuView actionMenuView) {
        this.f = actionMenuView;
        actionMenuView.a(this.c);
    }

    public final void a(boolean z) {
        boolean z2 = true;
        boolean z3 = false;
        ((View) this.f).getParent();
        super.a(z);
        ((View) this.f).requestLayout();
        if (this.c != null) {
            ArrayList m2 = this.c.m();
            int size = m2.size();
            for (int i2 = 0; i2 < size; i2++) {
                n a = ((m) m2.get(i2)).a();
                if (a != null) {
                    a.a(this);
                }
            }
        }
        ArrayList n2 = this.c != null ? this.c.n() : null;
        if (this.j && n2 != null) {
            int size2 = n2.size();
            if (size2 == 1) {
                z3 = !((m) n2.get(0)).isActionViewExpanded();
            } else {
                if (size2 <= 0) {
                    z2 = false;
                }
                z3 = z2;
            }
        }
        if (z3) {
            if (this.i == null) {
                this.i = new d(this, this.a);
            }
            ViewGroup viewGroup = (ViewGroup) this.i.getParent();
            if (viewGroup != this.f) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.i);
                }
                ((ActionMenuView) this.f).addView(this.i, ActionMenuView.b());
            }
        } else if (this.i != null && this.i.getParent() == this.f) {
            ((ViewGroup) this.f).removeView(this.i);
        }
        ((ActionMenuView) this.f).a(this.j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ActionMenuView.a(android.view.View, int, int, int, int):int
     arg types: [android.view.View, int, int, int, int]
     candidates:
      android.support.v7.widget.LinearLayoutCompat.a(android.view.View, int, int, int, int):void
      android.support.v7.widget.ActionMenuView.a(android.view.View, int, int, int, int):int */
    public final boolean a() {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        boolean z;
        int i8;
        int i9;
        ArrayList k2 = this.c.k();
        int size = k2.size();
        int i10 = this.n;
        int i11 = this.m;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.f;
        int i12 = 0;
        int i13 = 0;
        boolean z2 = false;
        int i14 = 0;
        while (i14 < size) {
            m mVar = (m) k2.get(i14);
            if (mVar.k()) {
                i12++;
            } else if (mVar.j()) {
                i13++;
            } else {
                z2 = true;
            }
            i14++;
            i10 = (!this.r || !mVar.isActionViewExpanded()) ? i10 : 0;
        }
        if (this.j && (z2 || i12 + i13 > i10)) {
            i10--;
        }
        int i15 = i10 - i12;
        SparseBooleanArray sparseBooleanArray = this.t;
        sparseBooleanArray.clear();
        if (this.p) {
            int i16 = i11 / this.s;
            i2 = ((i11 % this.s) / i16) + this.s;
            i3 = i16;
        } else {
            i2 = 0;
            i3 = 0;
        }
        int i17 = 0;
        int i18 = 0;
        int i19 = i3;
        while (i18 < size) {
            m mVar2 = (m) k2.get(i18);
            if (mVar2.k()) {
                View a = a(mVar2, this.u, viewGroup);
                if (this.u == null) {
                    this.u = a;
                }
                if (this.p) {
                    i4 = i19 - ActionMenuView.a(a, i2, i19, makeMeasureSpec, 0);
                } else {
                    a.measure(makeMeasureSpec, makeMeasureSpec);
                    i4 = i19;
                }
                i6 = a.getMeasuredWidth();
                int i20 = i11 - i6;
                if (i17 != 0) {
                    i6 = i17;
                }
                int groupId = mVar2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                mVar2.d(true);
                i5 = i20;
                i7 = i15;
            } else if (mVar2.j()) {
                int groupId2 = mVar2.getGroupId();
                boolean z3 = sparseBooleanArray.get(groupId2);
                boolean z4 = (i15 > 0 || z3) && i11 > 0 && (!this.p || i19 > 0);
                if (z4) {
                    View a2 = a(mVar2, this.u, viewGroup);
                    if (this.u == null) {
                        this.u = a2;
                    }
                    if (this.p) {
                        int a3 = ActionMenuView.a(a2, i2, i19, makeMeasureSpec, 0);
                        i19 -= a3;
                        if (a3 == 0) {
                            z4 = false;
                        }
                    } else {
                        a2.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    int measuredWidth = a2.getMeasuredWidth();
                    i11 -= measuredWidth;
                    if (i17 == 0) {
                        i17 = measuredWidth;
                    }
                    if (this.p) {
                        z = z4 & (i11 >= 0);
                        i8 = i19;
                    } else {
                        z = z4 & (i11 + i17 > 0);
                        i8 = i19;
                    }
                } else {
                    z = z4;
                    i8 = i19;
                }
                if (z && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                    i9 = i15;
                } else if (z3) {
                    sparseBooleanArray.put(groupId2, false);
                    int i21 = i15;
                    for (int i22 = 0; i22 < i18; i22++) {
                        m mVar3 = (m) k2.get(i22);
                        if (mVar3.getGroupId() == groupId2) {
                            if (mVar3.i()) {
                                i21++;
                            }
                            mVar3.d(false);
                        }
                    }
                    i9 = i21;
                } else {
                    i9 = i15;
                }
                if (z) {
                    i9--;
                }
                mVar2.d(z);
                i6 = i17;
                i5 = i11;
                i7 = i9;
                i4 = i8;
            } else {
                mVar2.d(false);
                i4 = i19;
                i5 = i11;
                i6 = i17;
                i7 = i15;
            }
            i18++;
            i11 = i5;
            i15 = i7;
            i17 = i6;
            i19 = i4;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.support.v7.internal.view.menu.ad r8) {
        /*
            r7 = this;
            r3 = 0
            boolean r0 = r8.hasVisibleItems()
            if (r0 != 0) goto L_0x0009
            r0 = r3
        L_0x0008:
            return r0
        L_0x0009:
            r0 = r8
        L_0x000a:
            android.view.Menu r1 = r0.r()
            android.support.v7.internal.view.menu.i r2 = r7.c
            if (r1 == r2) goto L_0x0019
            android.view.Menu r0 = r0.r()
            android.support.v7.internal.view.menu.ad r0 = (android.support.v7.internal.view.menu.ad) r0
            goto L_0x000a
        L_0x0019:
            android.view.MenuItem r5 = r0.getItem()
            android.support.v7.internal.view.menu.z r0 = r7.f
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            if (r0 == 0) goto L_0x0048
            int r6 = r0.getChildCount()
            r4 = r3
        L_0x0028:
            if (r4 >= r6) goto L_0x0048
            android.view.View r2 = r0.getChildAt(r4)
            boolean r1 = r2 instanceof android.support.v7.internal.view.menu.aa
            if (r1 == 0) goto L_0x0044
            r1 = r2
            android.support.v7.internal.view.menu.aa r1 = (android.support.v7.internal.view.menu.aa) r1
            android.support.v7.internal.view.menu.m r1 = r1.a()
            if (r1 != r5) goto L_0x0044
            r0 = r2
        L_0x003c:
            if (r0 != 0) goto L_0x004c
            android.view.View r0 = r7.i
            if (r0 != 0) goto L_0x004a
            r0 = r3
            goto L_0x0008
        L_0x0044:
            int r1 = r4 + 1
            r4 = r1
            goto L_0x0028
        L_0x0048:
            r0 = 0
            goto L_0x003c
        L_0x004a:
            android.view.View r0 = r7.i
        L_0x004c:
            android.view.MenuItem r1 = r8.getItem()
            int r1 = r1.getItemId()
            r7.h = r1
            android.support.v7.widget.a r1 = new android.support.v7.widget.a
            android.content.Context r2 = r7.b
            r1.<init>(r7, r2, r8)
            r7.w = r1
            android.support.v7.widget.a r1 = r7.w
            r1.a(r0)
            android.support.v7.widget.a r0 = r7.w
            r0.e()
            super.a(r8)
            r0 = 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.internal.view.menu.ad):boolean");
    }

    public final boolean a(ViewGroup viewGroup, int i2) {
        if (viewGroup.getChildAt(i2) == this.i) {
            return false;
        }
        return super.a(viewGroup, i2);
    }

    public final void b(int i2) {
        this.l = i2;
        this.p = true;
        this.q = true;
    }

    public final void b(boolean z) {
        if (z) {
            super.a((ad) null);
        } else {
            this.c.a(false);
        }
    }

    public final boolean c(m mVar) {
        return mVar.i();
    }

    public final Parcelable d() {
        SavedState savedState = new SavedState();
        savedState.a = this.h;
        return savedState;
    }

    public final void e() {
        if (!this.o) {
            this.n = this.b.getResources().getInteger(h.abc_max_action_buttons);
        }
        if (this.c != null) {
            this.c.b(true);
        }
    }

    public final void f() {
        this.j = true;
        this.k = true;
    }

    public final void g() {
        this.n = Integer.MAX_VALUE;
        this.o = true;
    }

    public final void h() {
        this.r = true;
    }

    public final boolean i() {
        if (!this.j || m() || this.c == null || this.f == null || this.x != null || this.c.n().isEmpty()) {
            return false;
        }
        this.x = new c(this, new f(this, this.b, this.c, this.i));
        ((View) this.f).post(this.x);
        super.a((ad) null);
        return true;
    }

    public final boolean j() {
        if (this.x == null || this.f == null) {
            f fVar = this.v;
            if (fVar == null) {
                return false;
            }
            fVar.h();
            return true;
        }
        ((View) this.f).removeCallbacks(this.x);
        this.x = null;
        return true;
    }

    public final boolean k() {
        return j() | l();
    }

    public final boolean l() {
        if (this.w == null) {
            return false;
        }
        this.w.h();
        return true;
    }

    public final boolean m() {
        return this.v != null && this.v.i();
    }

    public final boolean n() {
        return this.x != null || m();
    }
}
