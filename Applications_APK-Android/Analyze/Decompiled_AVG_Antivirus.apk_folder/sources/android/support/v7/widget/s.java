package android.support.v7.widget;

import android.support.v4.a.a;
import android.support.v4.view.bx;
import android.support.v4.view.dv;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class s extends cz {
    private ArrayList b = new ArrayList();
    private ArrayList c = new ArrayList();
    private ArrayList d = new ArrayList();
    private ArrayList e = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList f = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList g = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList h = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList i = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList j = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList k = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList l = new ArrayList();

    private void a(ab abVar) {
        if (abVar.a != null) {
            a(abVar, abVar.a);
        }
        if (abVar.b != null) {
            a(abVar, abVar.b);
        }
    }

    static /* synthetic */ void a(s sVar, ab abVar) {
        View view = null;
        cf cfVar = abVar.a;
        View view2 = cfVar == null ? null : cfVar.a;
        cf cfVar2 = abVar.b;
        if (cfVar2 != null) {
            view = cfVar2.a;
        }
        if (view2 != null) {
            dv a = bx.t(view2).a(sVar.i());
            sVar.l.add(abVar.a);
            a.b((float) (abVar.e - abVar.c));
            a.c((float) (abVar.f - abVar.d));
            a.a(0.0f).a(new z(sVar, abVar, a)).c();
        }
        if (view != null) {
            dv t = bx.t(view);
            sVar.l.add(abVar.b);
            t.b(0.0f).c(0.0f).a(sVar.i()).a(1.0f).a(new aa(sVar, abVar, t, view)).c();
        }
    }

    static /* synthetic */ void a(s sVar, cf cfVar) {
        dv t = bx.t(cfVar.a);
        sVar.i.add(cfVar);
        t.a(1.0f).a(sVar.f()).a(new x(sVar, cfVar, t)).c();
    }

    static /* synthetic */ void a(s sVar, cf cfVar, int i2, int i3, int i4, int i5) {
        View view = cfVar.a;
        int i6 = i4 - i2;
        int i7 = i5 - i3;
        if (i6 != 0) {
            bx.t(view).b(0.0f);
        }
        if (i7 != 0) {
            bx.t(view).c(0.0f);
        }
        dv t = bx.t(view);
        sVar.j.add(cfVar);
        t.a(sVar.d()).a(new y(sVar, cfVar, i6, i7, t)).c();
    }

    private static void a(List list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            bx.t(((cf) list.get(size)).a).b();
        }
    }

    private void a(List list, cf cfVar) {
        for (int size = list.size() - 1; size >= 0; size--) {
            ab abVar = (ab) list.get(size);
            if (a(abVar, cfVar) && abVar.a == null && abVar.b == null) {
                list.remove(abVar);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, int):void
      android.support.v4.view.bx.c(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.a(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.a(int, int):int
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.bi):void
      android.support.v4.view.bx.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.bx.a(android.view.View, boolean):void
      android.support.v4.view.bx.a(android.view.View, int):boolean
      android.support.v4.view.bx.a(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, boolean):void
      android.support.v4.view.bx.b(android.view.View, int):boolean
      android.support.v4.view.bx.b(android.view.View, float):void */
    private boolean a(ab abVar, cf cfVar) {
        if (abVar.b == cfVar) {
            abVar.b = null;
        } else if (abVar.a != cfVar) {
            return false;
        } else {
            abVar.a = null;
        }
        bx.c(cfVar.a, 1.0f);
        bx.a(cfVar.a, 0.0f);
        bx.b(cfVar.a, 0.0f);
        e(cfVar);
        return true;
    }

    private void h(cf cfVar) {
        a.a(cfVar.a);
        c(cfVar);
    }

    /* access modifiers changed from: private */
    public void l() {
        if (!b()) {
            j();
        }
    }

    public final void a() {
        boolean z = !this.b.isEmpty();
        boolean z2 = !this.d.isEmpty();
        boolean z3 = !this.e.isEmpty();
        boolean z4 = !this.c.isEmpty();
        if (z || z2 || z4 || z3) {
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                cf cfVar = (cf) it.next();
                dv t = bx.t(cfVar.a);
                this.k.add(cfVar);
                t.a(g()).a(0.0f).a(new w(this, cfVar, t)).c();
            }
            this.b.clear();
            if (z2) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.d);
                this.g.add(arrayList);
                this.d.clear();
                t tVar = new t(this, arrayList);
                if (z) {
                    bx.a(((ac) arrayList.get(0)).a.a, tVar, g());
                } else {
                    tVar.run();
                }
            }
            if (z3) {
                ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(this.e);
                this.h.add(arrayList2);
                this.e.clear();
                u uVar = new u(this, arrayList2);
                if (z) {
                    bx.a(((ab) arrayList2.get(0)).a.a, uVar, g());
                } else {
                    uVar.run();
                }
            }
            if (z4) {
                ArrayList arrayList3 = new ArrayList();
                arrayList3.addAll(this.c);
                this.f.add(arrayList3);
                this.c.clear();
                v vVar = new v(this, arrayList3);
                if (z || z2 || z3) {
                    bx.a(((cf) arrayList3.get(0)).a, vVar, (z ? g() : 0) + Math.max(z2 ? d() : 0, z3 ? i() : 0));
                } else {
                    vVar.run();
                }
            }
        }
    }

    public final boolean a(cf cfVar) {
        h(cfVar);
        this.b.add(cfVar);
        return true;
    }

    public final boolean a(cf cfVar, int i2, int i3, int i4, int i5) {
        View view = cfVar.a;
        int p = (int) (((float) i2) + bx.p(cfVar.a));
        int q = (int) (((float) i3) + bx.q(cfVar.a));
        h(cfVar);
        int i6 = i4 - p;
        int i7 = i5 - q;
        if (i6 == 0 && i7 == 0) {
            e(cfVar);
            return false;
        }
        if (i6 != 0) {
            bx.a(view, (float) (-i6));
        }
        if (i7 != 0) {
            bx.b(view, (float) (-i7));
        }
        this.d.add(new ac(cfVar, p, q, i4, i5, (byte) 0));
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, int):void
      android.support.v4.view.bx.c(android.view.View, float):void */
    public final boolean a(cf cfVar, cf cfVar2, int i2, int i3, int i4, int i5) {
        if (cfVar == cfVar2) {
            return a(cfVar, i2, i3, i4, i5);
        }
        float p = bx.p(cfVar.a);
        float q = bx.q(cfVar.a);
        float f2 = bx.f(cfVar.a);
        h(cfVar);
        int i6 = (int) (((float) (i4 - i2)) - p);
        int i7 = (int) (((float) (i5 - i3)) - q);
        bx.a(cfVar.a, p);
        bx.b(cfVar.a, q);
        bx.c(cfVar.a, f2);
        if (cfVar2 != null) {
            h(cfVar2);
            bx.a(cfVar2.a, (float) (-i6));
            bx.b(cfVar2.a, (float) (-i7));
            bx.c(cfVar2.a, 0.0f);
        }
        this.e.add(new ab(cfVar, cfVar2, i2, i3, i4, i5, (byte) 0));
        return true;
    }

    public final boolean b() {
        return !this.c.isEmpty() || !this.e.isEmpty() || !this.d.isEmpty() || !this.b.isEmpty() || !this.j.isEmpty() || !this.k.isEmpty() || !this.i.isEmpty() || !this.l.isEmpty() || !this.g.isEmpty() || !this.f.isEmpty() || !this.h.isEmpty();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, int):void
      android.support.v4.view.bx.c(android.view.View, float):void */
    public final boolean b(cf cfVar) {
        h(cfVar);
        bx.c(cfVar.a, 0.0f);
        this.c.add(cfVar);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, boolean):void
      android.support.v4.view.bx.b(android.view.View, int):boolean
      android.support.v4.view.bx.b(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.a(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.a(int, int):int
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.bi):void
      android.support.v4.view.bx.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.bx.a(android.view.View, boolean):void
      android.support.v4.view.bx.a(android.view.View, int):boolean
      android.support.v4.view.bx.a(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, int):void
      android.support.v4.view.bx.c(android.view.View, float):void */
    public final void c() {
        for (int size = this.d.size() - 1; size >= 0; size--) {
            ac acVar = (ac) this.d.get(size);
            View view = acVar.a.a;
            bx.b(view, 0.0f);
            bx.a(view, 0.0f);
            e(acVar.a);
            this.d.remove(size);
        }
        for (int size2 = this.b.size() - 1; size2 >= 0; size2--) {
            e((cf) this.b.get(size2));
            this.b.remove(size2);
        }
        for (int size3 = this.c.size() - 1; size3 >= 0; size3--) {
            cf cfVar = (cf) this.c.get(size3);
            bx.c(cfVar.a, 1.0f);
            e(cfVar);
            this.c.remove(size3);
        }
        for (int size4 = this.e.size() - 1; size4 >= 0; size4--) {
            a((ab) this.e.get(size4));
        }
        this.e.clear();
        if (b()) {
            for (int size5 = this.g.size() - 1; size5 >= 0; size5--) {
                ArrayList arrayList = (ArrayList) this.g.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    ac acVar2 = (ac) arrayList.get(size6);
                    View view2 = acVar2.a.a;
                    bx.b(view2, 0.0f);
                    bx.a(view2, 0.0f);
                    e(acVar2.a);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.g.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.f.size() - 1; size7 >= 0; size7--) {
                ArrayList arrayList2 = (ArrayList) this.f.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    cf cfVar2 = (cf) arrayList2.get(size8);
                    bx.c(cfVar2.a, 1.0f);
                    e(cfVar2);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.f.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.h.size() - 1; size9 >= 0; size9--) {
                ArrayList arrayList3 = (ArrayList) this.h.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    a((ab) arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.h.remove(arrayList3);
                    }
                }
            }
            a(this.k);
            a(this.j);
            a(this.i);
            a(this.l);
            j();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, boolean):void
      android.support.v4.view.bx.b(android.view.View, int):boolean
      android.support.v4.view.bx.b(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.a(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.a(int, int):int
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.bx.a(android.view.View, android.support.v4.view.bi):void
      android.support.v4.view.bx.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.bx.a(android.view.View, boolean):void
      android.support.v4.view.bx.a(android.view.View, int):boolean
      android.support.v4.view.bx.a(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, int):void
      android.support.v4.view.bx.c(android.view.View, float):void */
    public final void c(cf cfVar) {
        View view = cfVar.a;
        bx.t(view).b();
        for (int size = this.d.size() - 1; size >= 0; size--) {
            if (((ac) this.d.get(size)).a == cfVar) {
                bx.b(view, 0.0f);
                bx.a(view, 0.0f);
                e(cfVar);
                this.d.remove(size);
            }
        }
        a(this.e, cfVar);
        if (this.b.remove(cfVar)) {
            bx.c(view, 1.0f);
            e(cfVar);
        }
        if (this.c.remove(cfVar)) {
            bx.c(view, 1.0f);
            e(cfVar);
        }
        for (int size2 = this.h.size() - 1; size2 >= 0; size2--) {
            ArrayList arrayList = (ArrayList) this.h.get(size2);
            a(arrayList, cfVar);
            if (arrayList.isEmpty()) {
                this.h.remove(size2);
            }
        }
        for (int size3 = this.g.size() - 1; size3 >= 0; size3--) {
            ArrayList arrayList2 = (ArrayList) this.g.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (((ac) arrayList2.get(size4)).a == cfVar) {
                    bx.b(view, 0.0f);
                    bx.a(view, 0.0f);
                    e(cfVar);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.g.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.f.size() - 1; size5 >= 0; size5--) {
            ArrayList arrayList3 = (ArrayList) this.f.get(size5);
            if (arrayList3.remove(cfVar)) {
                bx.c(view, 1.0f);
                e(cfVar);
                if (arrayList3.isEmpty()) {
                    this.f.remove(size5);
                }
            }
        }
        this.k.remove(cfVar);
        this.i.remove(cfVar);
        this.l.remove(cfVar);
        this.j.remove(cfVar);
        l();
    }
}
