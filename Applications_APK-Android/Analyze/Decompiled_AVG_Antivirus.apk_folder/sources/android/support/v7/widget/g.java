package android.support.v7.widget;

import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;

final class g implements y {
    final /* synthetic */ ActionMenuPresenter a;

    private g(ActionMenuPresenter actionMenuPresenter) {
        this.a = actionMenuPresenter;
    }

    /* synthetic */ g(ActionMenuPresenter actionMenuPresenter, byte b) {
        this(actionMenuPresenter);
    }

    public final void a(i iVar, boolean z) {
        if (iVar instanceof ad) {
            ((ad) iVar).o().a(false);
        }
        y c = this.a.c();
        if (c != null) {
            c.a(iVar, z);
        }
    }

    public final boolean a(i iVar) {
        if (iVar == null) {
            return false;
        }
        this.a.h = ((ad) iVar).getItem().getItemId();
        y c = this.a.c();
        if (c != null) {
            return c.a(iVar);
        }
        return false;
    }
}
