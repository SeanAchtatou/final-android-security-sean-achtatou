package android.support.v7.widget;

import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.a;
import android.support.v4.widget.as;
import android.support.v7.a.g;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.WeakHashMap;

final class df extends as implements View.OnClickListener {
    private final SearchView j;
    private final SearchableInfo k;
    private final Context l;
    private final WeakHashMap m;
    private final int n;
    private boolean o;
    private int p;
    private ColorStateList q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;

    private Drawable a(ComponentName componentName) {
        PackageManager packageManager = this.d.getPackageManager();
        try {
            ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 128);
            int iconResource = activityInfo.getIconResource();
            if (iconResource == 0) {
                return null;
            }
            Drawable drawable = packageManager.getDrawable(componentName.getPackageName(), iconResource, activityInfo.applicationInfo);
            if (drawable != null) {
                return drawable;
            }
            Log.w("SuggestionsAdapter", "Invalid icon resource " + iconResource + " for " + componentName.flattenToShortString());
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("SuggestionsAdapter", e.toString());
            return null;
        }
    }

    private Drawable a(Uri uri) {
        InputStream openInputStream;
        try {
            if ("android.resource".equals(uri.getScheme())) {
                return b(uri);
            }
            openInputStream = this.l.getContentResolver().openInputStream(uri);
            if (openInputStream == null) {
                throw new FileNotFoundException("Failed to open " + uri);
            }
            Drawable createFromStream = Drawable.createFromStream(openInputStream, null);
            try {
                openInputStream.close();
                return createFromStream;
            } catch (IOException e) {
                Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e);
                return createFromStream;
            }
        } catch (Resources.NotFoundException e2) {
            throw new FileNotFoundException("Resource does not exist: " + uri);
        } catch (FileNotFoundException e3) {
            Log.w("SuggestionsAdapter", "Icon not found: " + uri + ", " + e3.getMessage());
            return null;
        } catch (Throwable th) {
            try {
                openInputStream.close();
            } catch (IOException e4) {
                Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e4);
            }
            throw th;
        }
    }

    private Drawable a(String str) {
        if (str == null || str.length() == 0 || "0".equals(str)) {
            return null;
        }
        try {
            int parseInt = Integer.parseInt(str);
            String str2 = "android.resource://" + this.l.getPackageName() + "/" + parseInt;
            Drawable b = b(str2);
            if (b != null) {
                return b;
            }
            Drawable a = a.a(this.l, parseInt);
            a(str2, a);
            return a;
        } catch (NumberFormatException e) {
            Drawable b2 = b(str);
            if (b2 != null) {
                return b2;
            }
            Drawable a2 = a(Uri.parse(str));
            a(str, a2);
            return a2;
        } catch (Resources.NotFoundException e2) {
            Log.w("SuggestionsAdapter", "Icon resource not found: " + str);
            return null;
        }
    }

    private static String a(Cursor cursor, int i) {
        if (i == -1) {
            return null;
        }
        try {
            return cursor.getString(i);
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", e);
            return null;
        }
    }

    public static String a(Cursor cursor, String str) {
        return a(cursor, cursor.getColumnIndex(str));
    }

    private static void a(ImageView imageView, Drawable drawable, int i) {
        imageView.setImageDrawable(drawable);
        if (drawable == null) {
            imageView.setVisibility(i);
            return;
        }
        imageView.setVisibility(0);
        drawable.setVisible(false, false);
        drawable.setVisible(true, false);
    }

    private static void a(TextView textView, CharSequence charSequence) {
        textView.setText(charSequence);
        if (TextUtils.isEmpty(charSequence)) {
            textView.setVisibility(8);
        } else {
            textView.setVisibility(0);
        }
    }

    private void a(String str, Drawable drawable) {
        if (drawable != null) {
            this.m.put(str, drawable.getConstantState());
        }
    }

    private Drawable b(Uri uri) {
        int identifier;
        String authority = uri.getAuthority();
        if (TextUtils.isEmpty(authority)) {
            throw new FileNotFoundException("No authority: " + uri);
        }
        try {
            Resources resourcesForApplication = this.d.getPackageManager().getResourcesForApplication(authority);
            List<String> pathSegments = uri.getPathSegments();
            if (pathSegments == null) {
                throw new FileNotFoundException("No path: " + uri);
            }
            int size = pathSegments.size();
            if (size == 1) {
                try {
                    identifier = Integer.parseInt(pathSegments.get(0));
                } catch (NumberFormatException e) {
                    throw new FileNotFoundException("Single path segment is not a resource ID: " + uri);
                }
            } else if (size == 2) {
                identifier = resourcesForApplication.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
            } else {
                throw new FileNotFoundException("More than two path segments: " + uri);
            }
            if (identifier != 0) {
                return resourcesForApplication.getDrawable(identifier);
            }
            throw new FileNotFoundException("No resource found for: " + uri);
        } catch (PackageManager.NameNotFoundException e2) {
            throw new FileNotFoundException("No package found for authority: " + uri);
        }
    }

    private Drawable b(String str) {
        Drawable.ConstantState constantState = (Drawable.ConstantState) this.m.get(str);
        if (constantState == null) {
            return null;
        }
        return constantState.newDrawable();
    }

    private static void c(Cursor cursor) {
        Bundle extras = cursor != null ? cursor.getExtras() : null;
        if (extras == null || extras.getBoolean("in_progress")) {
        }
    }

    public final Cursor a(CharSequence charSequence) {
        String[] strArr;
        Cursor query;
        String charSequence2 = charSequence == null ? "" : charSequence.toString();
        if (this.j.getVisibility() != 0 || this.j.getWindowVisibility() != 0) {
            return null;
        }
        try {
            SearchableInfo searchableInfo = this.k;
            if (searchableInfo == null) {
                query = null;
            } else {
                String suggestAuthority = searchableInfo.getSuggestAuthority();
                if (suggestAuthority == null) {
                    query = null;
                } else {
                    Uri.Builder fragment = new Uri.Builder().scheme("content").authority(suggestAuthority).query("").fragment("");
                    String suggestPath = searchableInfo.getSuggestPath();
                    if (suggestPath != null) {
                        fragment.appendEncodedPath(suggestPath);
                    }
                    fragment.appendPath("search_suggest_query");
                    String suggestSelection = searchableInfo.getSuggestSelection();
                    if (suggestSelection != null) {
                        strArr = new String[]{charSequence2};
                    } else {
                        fragment.appendPath(charSequence2);
                        strArr = null;
                    }
                    fragment.appendQueryParameter("limit", "50");
                    query = this.d.getContentResolver().query(fragment.build(), null, suggestSelection, strArr, null);
                }
            }
            if (query != null) {
                query.getCount();
                return query;
            }
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", e);
        }
        return null;
    }

    public final View a(Context context, Cursor cursor, ViewGroup viewGroup) {
        View a = super.a(context, cursor, viewGroup);
        a.setTag(new dg(a));
        ((ImageView) a.findViewById(g.edit_query)).setImageResource(this.n);
        return a;
    }

    public final void a(Cursor cursor) {
        if (this.o) {
            Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
            if (cursor != null) {
                cursor.close();
                return;
            }
            return;
        }
        try {
            super.a(cursor);
            if (cursor != null) {
                this.r = cursor.getColumnIndex("suggest_text_1");
                this.s = cursor.getColumnIndex("suggest_text_2");
                this.t = cursor.getColumnIndex("suggest_text_2_url");
                this.u = cursor.getColumnIndex("suggest_icon_1");
                this.v = cursor.getColumnIndex("suggest_icon_2");
                this.w = cursor.getColumnIndex("suggest_flags");
            }
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "error changing cursor and caching columns", e);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v37, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v38, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v1, resolved type: android.text.SpannableString} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v41, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.view.View r14, android.database.Cursor r15) {
        /*
            r13 = this;
            r12 = 2
            r11 = -1
            r10 = 1
            r1 = 0
            r2 = 0
            java.lang.Object r0 = r14.getTag()
            r6 = r0
            android.support.v7.widget.dg r6 = (android.support.v7.widget.dg) r6
            int r0 = r13.w
            if (r0 == r11) goto L_0x0143
            int r0 = r13.w
            int r0 = r15.getInt(r0)
            r7 = r0
        L_0x0017:
            android.widget.TextView r0 = r6.a
            if (r0 == 0) goto L_0x0026
            int r0 = r13.r
            java.lang.String r0 = a(r15, r0)
            android.widget.TextView r3 = r6.a
            a(r3, r0)
        L_0x0026:
            android.widget.TextView r0 = r6.b
            if (r0 == 0) goto L_0x0085
            int r0 = r13.t
            java.lang.String r9 = a(r15, r0)
            if (r9 == 0) goto L_0x00c5
            android.content.res.ColorStateList r0 = r13.q
            if (r0 != 0) goto L_0x0054
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            android.content.Context r3 = r13.d
            android.content.res.Resources$Theme r3 = r3.getTheme()
            int r4 = android.support.v7.a.b.textColorSearchUrl
            r3.resolveAttribute(r4, r0, r10)
            android.content.Context r3 = r13.d
            android.content.res.Resources r3 = r3.getResources()
            int r0 = r0.resourceId
            android.content.res.ColorStateList r0 = r3.getColorStateList(r0)
            r13.q = r0
        L_0x0054:
            android.text.SpannableString r8 = new android.text.SpannableString
            r8.<init>(r9)
            android.text.style.TextAppearanceSpan r0 = new android.text.style.TextAppearanceSpan
            android.content.res.ColorStateList r4 = r13.q
            r3 = r2
            r5 = r1
            r0.<init>(r1, r2, r3, r4, r5)
            int r3 = r9.length()
            r4 = 33
            r8.setSpan(r0, r2, r3, r4)
            r0 = r8
        L_0x006c:
            boolean r3 = android.text.TextUtils.isEmpty(r0)
            if (r3 == 0) goto L_0x00cc
            android.widget.TextView r3 = r6.a
            if (r3 == 0) goto L_0x0080
            android.widget.TextView r3 = r6.a
            r3.setSingleLine(r2)
            android.widget.TextView r3 = r6.a
            r3.setMaxLines(r12)
        L_0x0080:
            android.widget.TextView r3 = r6.b
            a(r3, r0)
        L_0x0085:
            android.widget.ImageView r0 = r6.c
            if (r0 == 0) goto L_0x0094
            android.widget.ImageView r4 = r6.c
            int r0 = r13.u
            if (r0 != r11) goto L_0x00db
            r0 = r1
        L_0x0090:
            r3 = 4
            a(r4, r0, r3)
        L_0x0094:
            android.widget.ImageView r0 = r6.d
            if (r0 == 0) goto L_0x00a3
            android.widget.ImageView r0 = r6.d
            int r3 = r13.v
            if (r3 != r11) goto L_0x012f
        L_0x009e:
            r3 = 8
            a(r0, r1, r3)
        L_0x00a3:
            int r0 = r13.p
            if (r0 == r12) goto L_0x00af
            int r0 = r13.p
            if (r0 != r10) goto L_0x013b
            r0 = r7 & 1
            if (r0 == 0) goto L_0x013b
        L_0x00af:
            android.widget.ImageView r0 = r6.e
            r0.setVisibility(r2)
            android.widget.ImageView r0 = r6.e
            android.widget.TextView r1 = r6.a
            java.lang.CharSequence r1 = r1.getText()
            r0.setTag(r1)
            android.widget.ImageView r0 = r6.e
            r0.setOnClickListener(r13)
        L_0x00c4:
            return
        L_0x00c5:
            int r0 = r13.s
            java.lang.String r0 = a(r15, r0)
            goto L_0x006c
        L_0x00cc:
            android.widget.TextView r3 = r6.a
            if (r3 == 0) goto L_0x0080
            android.widget.TextView r3 = r6.a
            r3.setSingleLine(r10)
            android.widget.TextView r3 = r6.a
            r3.setMaxLines(r10)
            goto L_0x0080
        L_0x00db:
            int r0 = r13.u
            java.lang.String r0 = r15.getString(r0)
            android.graphics.drawable.Drawable r0 = r13.a(r0)
            if (r0 != 0) goto L_0x0090
            android.app.SearchableInfo r0 = r13.k
            android.content.ComponentName r0 = r0.getSearchActivity()
            java.lang.String r5 = r0.flattenToShortString()
            java.util.WeakHashMap r3 = r13.m
            boolean r3 = r3.containsKey(r5)
            if (r3 == 0) goto L_0x011c
            java.util.WeakHashMap r0 = r13.m
            java.lang.Object r0 = r0.get(r5)
            android.graphics.drawable.Drawable$ConstantState r0 = (android.graphics.drawable.Drawable.ConstantState) r0
            if (r0 != 0) goto L_0x0111
            r0 = r1
        L_0x0104:
            if (r0 != 0) goto L_0x0090
            android.content.Context r0 = r13.d
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            android.graphics.drawable.Drawable r0 = r0.getDefaultActivityIcon()
            goto L_0x0090
        L_0x0111:
            android.content.Context r3 = r13.l
            android.content.res.Resources r3 = r3.getResources()
            android.graphics.drawable.Drawable r0 = r0.newDrawable(r3)
            goto L_0x0104
        L_0x011c:
            android.graphics.drawable.Drawable r3 = r13.a(r0)
            if (r3 != 0) goto L_0x012a
            r0 = r1
        L_0x0123:
            java.util.WeakHashMap r8 = r13.m
            r8.put(r5, r0)
            r0 = r3
            goto L_0x0104
        L_0x012a:
            android.graphics.drawable.Drawable$ConstantState r0 = r3.getConstantState()
            goto L_0x0123
        L_0x012f:
            int r1 = r13.v
            java.lang.String r1 = r15.getString(r1)
            android.graphics.drawable.Drawable r1 = r13.a(r1)
            goto L_0x009e
        L_0x013b:
            android.widget.ImageView r0 = r6.e
            r1 = 8
            r0.setVisibility(r1)
            goto L_0x00c4
        L_0x0143:
            r7 = r2
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.df.a(android.view.View, android.database.Cursor):void");
    }

    public final CharSequence b(Cursor cursor) {
        String a;
        String a2;
        if (cursor == null) {
            return null;
        }
        String a3 = a(cursor, "suggest_intent_query");
        if (a3 != null) {
            return a3;
        }
        if (this.k.shouldRewriteQueryFromData() && (a2 = a(cursor, "suggest_intent_data")) != null) {
            return a2;
        }
        if (!this.k.shouldRewriteQueryFromText() || (a = a(cursor, "suggest_text_1")) == null) {
            return null;
        }
        return a;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View a = a(this.d, this.c, viewGroup);
            if (a != null) {
                ((dg) a.getTag()).a.setText(e.toString());
            }
            return a;
        }
    }

    public final boolean hasStableIds() {
        return false;
    }

    public final void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        c(a());
    }

    public final void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        c(a());
    }

    public final void onClick(View view) {
        Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.j.a((CharSequence) tag);
        }
    }
}
