package android.support.v7.widget;

import android.view.View;

final class c implements Runnable {
    final /* synthetic */ ActionMenuPresenter a;
    private f b;

    public c(ActionMenuPresenter actionMenuPresenter, f fVar) {
        this.a = actionMenuPresenter;
        this.b = fVar;
    }

    public final void run() {
        this.a.c.f();
        View view = (View) this.a.f;
        if (!(view == null || view.getWindowToken() == null || !this.b.g())) {
            f unused = this.a.v = this.b;
        }
        c unused2 = this.a.x = null;
    }
}
