package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.util.List;

final class aj {
    boolean a = true;
    int b;
    int c;
    int d;
    int e;
    int f;
    int g;
    int h = 0;
    boolean i = false;
    int j;
    List k = null;

    aj() {
    }

    /* access modifiers changed from: package-private */
    public final View a(bw bwVar) {
        if (this.k != null) {
            int size = this.k.size();
            int i2 = 0;
            while (i2 < size) {
                View view = ((cf) this.k.get(i2)).a;
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                if (layoutParams.a.m() || this.d != layoutParams.a.e_()) {
                    i2++;
                } else {
                    a(view);
                    return view;
                }
            }
            return null;
        }
        View b2 = bwVar.b(this.d);
        this.d += this.e;
        return b2;
    }

    public final void a(View view) {
        View view2;
        int i2;
        View view3;
        int size = this.k.size();
        View view4 = null;
        int i3 = Integer.MAX_VALUE;
        int i4 = 0;
        while (true) {
            if (i4 >= size) {
                view2 = view4;
                break;
            }
            view2 = ((cf) this.k.get(i4)).a;
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view2.getLayoutParams();
            if (view2 != view && !layoutParams.a.m() && (i2 = (layoutParams.a.e_() - this.d) * this.e) >= 0 && i2 < i3) {
                if (i2 == 0) {
                    break;
                }
                view3 = view2;
            } else {
                i2 = i3;
                view3 = view4;
            }
            i4++;
            view4 = view3;
            i3 = i2;
        }
        if (view2 == null) {
            this.d = -1;
        } else {
            this.d = ((RecyclerView.LayoutParams) view2.getLayoutParams()).a.e_();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(cc ccVar) {
        return this.d >= 0 && this.d < ccVar.e();
    }
}
