package android.support.v7.widget;

import android.support.v4.e.o;
import android.support.v4.e.p;
import java.util.ArrayList;
import java.util.List;

final class m implements ay {
    final ArrayList a;
    final ArrayList b;
    final n c;
    Runnable d;
    final boolean e;
    final ax f;
    private o g;
    private int h;

    m(n nVar) {
        this(nVar, (byte) 0);
    }

    private m(n nVar, byte b2) {
        this.g = new p(30);
        this.a = new ArrayList();
        this.b = new ArrayList();
        this.h = 0;
        this.c = nVar;
        this.e = false;
        this.f = new ax(this);
    }

    private void a(o oVar, int i) {
        this.c.a(oVar);
        switch (oVar.a) {
            case 2:
                this.c.a(i, oVar.d);
                return;
            case 3:
            default:
                throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
            case 4:
                this.c.a(i, oVar.d, oVar.c);
                return;
        }
    }

    private void a(List list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a((o) list.get(i));
        }
        list.clear();
    }

    private void b(o oVar) {
        int i;
        boolean z;
        if (oVar.a == 1 || oVar.a == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int d2 = d(oVar.b, oVar.a);
        int i2 = oVar.b;
        switch (oVar.a) {
            case 2:
                i = 0;
                break;
            case 3:
            default:
                throw new IllegalArgumentException("op should be remove or update." + oVar);
            case 4:
                i = 1;
                break;
        }
        int i3 = 1;
        int i4 = d2;
        int i5 = i2;
        for (int i6 = 1; i6 < oVar.d; i6++) {
            int d3 = d(oVar.b + (i * i6), oVar.a);
            switch (oVar.a) {
                case 2:
                    if (d3 != i4) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
                case 3:
                default:
                    z = false;
                    break;
                case 4:
                    if (d3 != i4 + 1) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
            }
            if (z) {
                i3++;
            } else {
                o a2 = a(oVar.a, i4, i3, oVar.c);
                a(a2, i5);
                a(a2);
                if (oVar.a == 4) {
                    i5 += i3;
                }
                i3 = 1;
                i4 = d3;
            }
        }
        Object obj = oVar.c;
        a(oVar);
        if (i3 > 0) {
            o a3 = a(oVar.a, i4, i3, obj);
            a(a3, i5);
            a(a3);
        }
    }

    private void c(o oVar) {
        this.b.add(oVar);
        switch (oVar.a) {
            case 1:
                this.c.c(oVar.b, oVar.d);
                return;
            case 2:
                this.c.b(oVar.b, oVar.d);
                return;
            case 3:
            case 5:
            case 6:
            case 7:
            default:
                throw new IllegalArgumentException("Unknown update op type for " + oVar);
            case 4:
                this.c.a(oVar.b, oVar.d, oVar.c);
                return;
            case 8:
                this.c.d(oVar.b, oVar.d);
                return;
        }
    }

    private int d(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int size = this.b.size() - 1;
        int i6 = i;
        while (size >= 0) {
            o oVar = (o) this.b.get(size);
            if (oVar.a == 8) {
                if (oVar.b < oVar.d) {
                    i4 = oVar.b;
                    i5 = oVar.d;
                } else {
                    i4 = oVar.d;
                    i5 = oVar.b;
                }
                if (i6 < i4 || i6 > i5) {
                    if (i6 < oVar.b) {
                        if (i2 == 1) {
                            oVar.b++;
                            oVar.d++;
                            i3 = i6;
                        } else if (i2 == 2) {
                            oVar.b--;
                            oVar.d--;
                        }
                    }
                    i3 = i6;
                } else if (i4 == oVar.b) {
                    if (i2 == 1) {
                        oVar.d++;
                    } else if (i2 == 2) {
                        oVar.d--;
                    }
                    i3 = i6 + 1;
                } else {
                    if (i2 == 1) {
                        oVar.b++;
                    } else if (i2 == 2) {
                        oVar.b--;
                    }
                    i3 = i6 - 1;
                }
            } else {
                if (oVar.b <= i6) {
                    if (oVar.a == 1) {
                        i3 = i6 - oVar.d;
                    } else if (oVar.a == 2) {
                        i3 = oVar.d + i6;
                    }
                } else if (i2 == 1) {
                    oVar.b++;
                    i3 = i6;
                } else if (i2 == 2) {
                    oVar.b--;
                }
                i3 = i6;
            }
            size--;
            i6 = i3;
        }
        for (int size2 = this.b.size() - 1; size2 >= 0; size2--) {
            o oVar2 = (o) this.b.get(size2);
            if (oVar2.a == 8) {
                if (oVar2.d == oVar2.b || oVar2.d < 0) {
                    this.b.remove(size2);
                    a(oVar2);
                }
            } else if (oVar2.d <= 0) {
                this.b.remove(size2);
                a(oVar2);
            }
        }
        return i6;
    }

    private boolean e(int i) {
        int size = this.b.size();
        for (int i2 = 0; i2 < size; i2++) {
            o oVar = (o) this.b.get(i2);
            if (oVar.a == 8) {
                if (a(oVar.d, i2 + 1) == i) {
                    return true;
                }
            } else if (oVar.a == 1) {
                int i3 = oVar.b + oVar.d;
                for (int i4 = oVar.b; i4 < i3; i4++) {
                    if (a(i4, i2 + 1) == i) {
                        return true;
                    }
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final int a(int i, int i2) {
        int size = this.b.size();
        int i3 = i;
        while (i2 < size) {
            o oVar = (o) this.b.get(i2);
            if (oVar.a == 8) {
                if (oVar.b == i3) {
                    i3 = oVar.d;
                } else {
                    if (oVar.b < i3) {
                        i3--;
                    }
                    if (oVar.d <= i3) {
                        i3++;
                    }
                }
            } else if (oVar.b > i3) {
                continue;
            } else if (oVar.a == 2) {
                if (i3 < oVar.b + oVar.d) {
                    return -1;
                }
                i3 -= oVar.d;
            } else if (oVar.a == 1) {
                i3 += oVar.d;
            }
            i2++;
        }
        return i3;
    }

    public final o a(int i, int i2, int i3, Object obj) {
        o oVar = (o) this.g.a();
        if (oVar == null) {
            return new o(i, i2, i3, obj);
        }
        oVar.a = i;
        oVar.b = i2;
        oVar.d = i3;
        oVar.c = obj;
        return oVar;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        a(this.a);
        a(this.b);
        this.h = 0;
    }

    public final void a(o oVar) {
        if (!this.e) {
            oVar.c = null;
            this.g.a(oVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i) {
        return (this.h & i) != 0;
    }

    /* access modifiers changed from: package-private */
    public final int b(int i) {
        return a(i, 0);
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        int i;
        boolean z;
        int i2;
        int i3;
        int i4;
        boolean z2;
        boolean z3;
        ax axVar = this.f;
        ArrayList arrayList = this.a;
        while (true) {
            boolean z4 = false;
            int size = arrayList.size() - 1;
            while (true) {
                if (size >= 0) {
                    if (((o) arrayList.get(size)).a != 8) {
                        z3 = true;
                    } else if (z4) {
                        i = size;
                    } else {
                        z3 = z4;
                    }
                    size--;
                    z4 = z3;
                } else {
                    i = -1;
                }
            }
            if (i != -1) {
                int i5 = i + 1;
                o oVar = (o) arrayList.get(i);
                o oVar2 = (o) arrayList.get(i5);
                switch (oVar2.a) {
                    case 1:
                        int i6 = 0;
                        if (oVar.d < oVar2.b) {
                            i6 = -1;
                        }
                        if (oVar.b < oVar2.b) {
                            i6++;
                        }
                        if (oVar2.b <= oVar.b) {
                            oVar.b += oVar2.d;
                        }
                        if (oVar2.b <= oVar.d) {
                            oVar.d += oVar2.d;
                        }
                        oVar2.b = i6 + oVar2.b;
                        arrayList.set(i, oVar2);
                        arrayList.set(i5, oVar);
                        break;
                    case 2:
                        o oVar3 = null;
                        boolean z5 = false;
                        if (oVar.b < oVar.d) {
                            z2 = false;
                            if (oVar2.b == oVar.b && oVar2.d == oVar.d - oVar.b) {
                                z5 = true;
                            }
                        } else {
                            z2 = true;
                            if (oVar2.b == oVar.d + 1 && oVar2.d == oVar.b - oVar.d) {
                                z5 = true;
                            }
                        }
                        if (oVar.d >= oVar2.b) {
                            if (oVar.d < oVar2.b + oVar2.d) {
                                oVar2.d--;
                                oVar.a = 2;
                                oVar.d = 1;
                                if (oVar2.d != 0) {
                                    break;
                                } else {
                                    arrayList.remove(i5);
                                    axVar.a.a(oVar2);
                                    break;
                                }
                            }
                        } else {
                            oVar2.b--;
                        }
                        if (oVar.b <= oVar2.b) {
                            oVar2.b++;
                        } else if (oVar.b < oVar2.b + oVar2.d) {
                            oVar3 = axVar.a.a(2, oVar.b + 1, (oVar2.b + oVar2.d) - oVar.b, null);
                            oVar2.d = oVar.b - oVar2.b;
                        }
                        if (!z5) {
                            if (z2) {
                                if (oVar3 != null) {
                                    if (oVar.b > oVar3.b) {
                                        oVar.b -= oVar3.d;
                                    }
                                    if (oVar.d > oVar3.b) {
                                        oVar.d -= oVar3.d;
                                    }
                                }
                                if (oVar.b > oVar2.b) {
                                    oVar.b -= oVar2.d;
                                }
                                if (oVar.d > oVar2.b) {
                                    oVar.d -= oVar2.d;
                                }
                            } else {
                                if (oVar3 != null) {
                                    if (oVar.b >= oVar3.b) {
                                        oVar.b -= oVar3.d;
                                    }
                                    if (oVar.d >= oVar3.b) {
                                        oVar.d -= oVar3.d;
                                    }
                                }
                                if (oVar.b >= oVar2.b) {
                                    oVar.b -= oVar2.d;
                                }
                                if (oVar.d >= oVar2.b) {
                                    oVar.d -= oVar2.d;
                                }
                            }
                            arrayList.set(i, oVar2);
                            if (oVar.b != oVar.d) {
                                arrayList.set(i5, oVar);
                            } else {
                                arrayList.remove(i5);
                            }
                            if (oVar3 == null) {
                                break;
                            } else {
                                arrayList.add(i, oVar3);
                                break;
                            }
                        } else {
                            arrayList.set(i, oVar2);
                            arrayList.remove(i5);
                            axVar.a.a(oVar);
                            break;
                        }
                    case 4:
                        o oVar4 = null;
                        o oVar5 = null;
                        if (oVar.d < oVar2.b) {
                            oVar2.b--;
                        } else if (oVar.d < oVar2.b + oVar2.d) {
                            oVar2.d--;
                            oVar4 = axVar.a.a(4, oVar.b, 1, oVar2.c);
                        }
                        if (oVar.b <= oVar2.b) {
                            oVar2.b++;
                        } else if (oVar.b < oVar2.b + oVar2.d) {
                            int i7 = (oVar2.b + oVar2.d) - oVar.b;
                            oVar5 = axVar.a.a(4, oVar.b + 1, i7, oVar2.c);
                            oVar2.d -= i7;
                        }
                        arrayList.set(i5, oVar);
                        if (oVar2.d > 0) {
                            arrayList.set(i, oVar2);
                        } else {
                            arrayList.remove(i);
                            axVar.a.a(oVar2);
                        }
                        if (oVar4 != null) {
                            arrayList.add(i, oVar4);
                        }
                        if (oVar5 == null) {
                            break;
                        } else {
                            arrayList.add(i, oVar5);
                            break;
                        }
                }
            } else {
                int size2 = this.a.size();
                for (int i8 = 0; i8 < size2; i8++) {
                    o oVar6 = (o) this.a.get(i8);
                    switch (oVar6.a) {
                        case 1:
                            c(oVar6);
                            break;
                        case 2:
                            int i9 = oVar6.b;
                            int i10 = oVar6.d + oVar6.b;
                            char c2 = 65535;
                            int i11 = oVar6.b;
                            int i12 = 0;
                            while (i11 < i10) {
                                boolean z6 = false;
                                if (this.c.a(i11) != null || e(i11)) {
                                    if (c2 == 0) {
                                        b(a(2, i9, i12, null));
                                        z6 = true;
                                    }
                                    c2 = 1;
                                } else {
                                    if (c2 == 1) {
                                        c(a(2, i9, i12, null));
                                        z6 = true;
                                    }
                                    c2 = 0;
                                }
                                if (z6) {
                                    i4 = i11 - i12;
                                    i2 = i10 - i12;
                                    i3 = 1;
                                } else {
                                    int i13 = i11;
                                    i2 = i10;
                                    i3 = i12 + 1;
                                    i4 = i13;
                                }
                                i12 = i3;
                                i10 = i2;
                                i11 = i4 + 1;
                            }
                            if (i12 != oVar6.d) {
                                a(oVar6);
                                oVar6 = a(2, i9, i12, null);
                            }
                            if (c2 != 0) {
                                c(oVar6);
                                break;
                            } else {
                                b(oVar6);
                                break;
                            }
                            break;
                        case 4:
                            int i14 = oVar6.b;
                            int i15 = oVar6.b + oVar6.d;
                            int i16 = oVar6.b;
                            int i17 = 0;
                            int i18 = i14;
                            boolean z7 = true;
                            while (i16 < i15) {
                                if (this.c.a(i16) != null || e(i16)) {
                                    if (!z7) {
                                        b(a(4, i18, i17, oVar6.c));
                                        i17 = 0;
                                        i18 = i16;
                                    }
                                    z = true;
                                } else {
                                    if (z7) {
                                        c(a(4, i18, i17, oVar6.c));
                                        i17 = 0;
                                        i18 = i16;
                                    }
                                    z = false;
                                }
                                boolean z8 = z;
                                i16++;
                                i17++;
                                i18 = i18;
                                z7 = z8;
                            }
                            if (i17 != oVar6.d) {
                                Object obj = oVar6.c;
                                a(oVar6);
                                oVar6 = a(4, i18, i17, obj);
                            }
                            if (z7) {
                                c(oVar6);
                                break;
                            } else {
                                b(oVar6);
                                break;
                            }
                            break;
                        case 8:
                            c(oVar6);
                            break;
                    }
                    if (this.d != null) {
                        this.d.run();
                    }
                }
                this.a.clear();
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean b(int i, int i2) {
        this.a.add(a(4, i, i2, null));
        this.h |= 4;
        return this.a.size() == 1;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            this.c.b((o) this.b.get(i));
        }
        a(this.b);
        this.h = 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean c(int i) {
        this.a.add(a(1, i, 1, null));
        this.h |= 1;
        return this.a.size() == 1;
    }

    /* access modifiers changed from: package-private */
    public final boolean c(int i, int i2) {
        if (i == i2) {
            return false;
        }
        this.a.add(a(8, i, i2, null));
        this.h |= 8;
        return this.a.size() == 1;
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return this.a.size() > 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean d(int i) {
        this.a.add(a(2, i, 1, null));
        this.h |= 2;
        return this.a.size() == 1;
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        c();
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            o oVar = (o) this.a.get(i);
            switch (oVar.a) {
                case 1:
                    this.c.b(oVar);
                    this.c.c(oVar.b, oVar.d);
                    break;
                case 2:
                    this.c.b(oVar);
                    this.c.a(oVar.b, oVar.d);
                    break;
                case 4:
                    this.c.b(oVar);
                    this.c.a(oVar.b, oVar.d, oVar.c);
                    break;
                case 8:
                    this.c.b(oVar);
                    this.c.d(oVar.b, oVar.d);
                    break;
            }
            if (this.d != null) {
                this.d.run();
            }
        }
        a(this.a);
        this.h = 0;
    }
}
