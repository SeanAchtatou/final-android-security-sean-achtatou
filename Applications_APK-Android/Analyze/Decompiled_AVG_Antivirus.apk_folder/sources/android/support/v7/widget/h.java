package android.support.v7.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.ActionMenuPresenter;

final class h implements Parcelable.Creator {
    h() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new ActionMenuPresenter.SavedState(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new ActionMenuPresenter.SavedState[i];
    }
}
