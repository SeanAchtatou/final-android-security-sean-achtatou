package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.d.p;
import android.support.v7.a.b;
import android.support.v7.a.l;
import android.support.v7.internal.widget.AppCompatPopupWindow;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

public class ListPopupWindow {
    private static Method a;
    /* access modifiers changed from: private */
    public Handler A;
    private Rect B;
    private boolean C;
    private int D;
    int b;
    private Context c;
    /* access modifiers changed from: private */
    public PopupWindow d;
    private ListAdapter e;
    /* access modifiers changed from: private */
    public ao f;
    private int g;
    private int h;
    private int i;
    private int j;
    private boolean k;
    private int l;
    private boolean m;
    private boolean n;
    private View o;
    private int p;
    private DataSetObserver q;
    private View r;
    private Drawable s;
    private AdapterView.OnItemClickListener t;
    private AdapterView.OnItemSelectedListener u;
    /* access modifiers changed from: private */
    public final aw v;
    private final av w;
    private final au x;
    private final as y;
    private Runnable z;

    static {
        Class<PopupWindow> cls = PopupWindow.class;
        try {
            a = cls.getDeclaredMethod("setClipToScreenEnabled", Boolean.TYPE);
        } catch (NoSuchMethodException e2) {
            Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
        }
    }

    public ListPopupWindow(Context context) {
        this(context, null, b.listPopupWindowStyle);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.listPopupWindowStyle);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.g = -2;
        this.h = -2;
        this.l = 0;
        this.m = false;
        this.n = false;
        this.b = Integer.MAX_VALUE;
        this.p = 0;
        this.v = new aw(this, (byte) 0);
        this.w = new av(this, (byte) 0);
        this.x = new au(this, (byte) 0);
        this.y = new as(this, (byte) 0);
        this.A = new Handler();
        this.B = new Rect();
        this.c = context;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.ag, i2, i3);
        this.i = obtainStyledAttributes.getDimensionPixelOffset(l.ah, 0);
        this.j = obtainStyledAttributes.getDimensionPixelOffset(l.ai, 0);
        if (this.j != 0) {
            this.k = true;
        }
        obtainStyledAttributes.recycle();
        this.d = new AppCompatPopupWindow(context, attributeSet, i2);
        this.d.setInputMethodMode(1);
        this.D = p.a(this.c.getResources().getConfiguration().locale);
    }

    public final void a() {
        this.d.dismiss();
        if (this.o != null) {
            ViewParent parent = this.o.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.o);
            }
        }
        this.d.setContentView(null);
        this.f = null;
        this.A.removeCallbacks(this.v);
    }

    public final void a(int i2) {
        this.l = i2;
    }

    public final void a(Drawable drawable) {
        this.d.setBackgroundDrawable(drawable);
    }

    public final void a(View view) {
        this.r = view;
    }

    public final void a(AdapterView.OnItemClickListener onItemClickListener) {
        this.t = onItemClickListener;
    }

    public void a(ListAdapter listAdapter) {
        if (this.q == null) {
            this.q = new at(this, (byte) 0);
        } else if (this.e != null) {
            this.e.unregisterDataSetObserver(this.q);
        }
        this.e = listAdapter;
        if (this.e != null) {
            listAdapter.registerDataSetObserver(this.q);
        }
        if (this.f != null) {
            this.f.setAdapter(this.e);
        }
    }

    public final void a(PopupWindow.OnDismissListener onDismissListener) {
        this.d.setOnDismissListener(onDismissListener);
    }

    public final void b(int i2) {
        Drawable background = this.d.getBackground();
        if (background != null) {
            background.getPadding(this.B);
            this.h = this.B.left + this.B.right + i2;
            return;
        }
        this.h = i2;
    }

    public final boolean b() {
        return this.d.isShowing();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v42, resolved type: android.support.v7.widget.ao} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v43, resolved type: android.support.v7.widget.ao} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v44, resolved type: android.widget.LinearLayout} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v47, resolved type: android.support.v7.widget.ao} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c() {
        /*
            r11 = this;
            r10 = -2147483648(0xffffffff80000000, float:-0.0)
            r9 = -2
            r1 = 1
            r2 = 0
            r5 = -1
            android.support.v7.widget.ao r0 = r11.f
            if (r0 != 0) goto L_0x0136
            android.content.Context r4 = r11.c
            android.support.v7.widget.am r0 = new android.support.v7.widget.am
            r0.<init>(r11)
            r11.z = r0
            android.support.v7.widget.ao r3 = new android.support.v7.widget.ao
            boolean r0 = r11.C
            if (r0 != 0) goto L_0x0123
            r0 = r1
        L_0x001a:
            r3.<init>(r4, r0)
            r11.f = r3
            android.graphics.drawable.Drawable r0 = r11.s
            if (r0 == 0) goto L_0x002a
            android.support.v7.widget.ao r0 = r11.f
            android.graphics.drawable.Drawable r3 = r11.s
            r0.setSelector(r3)
        L_0x002a:
            android.support.v7.widget.ao r0 = r11.f
            android.widget.ListAdapter r3 = r11.e
            r0.setAdapter(r3)
            android.support.v7.widget.ao r0 = r11.f
            android.widget.AdapterView$OnItemClickListener r3 = r11.t
            r0.setOnItemClickListener(r3)
            android.support.v7.widget.ao r0 = r11.f
            r0.setFocusable(r1)
            android.support.v7.widget.ao r0 = r11.f
            r0.setFocusableInTouchMode(r1)
            android.support.v7.widget.ao r0 = r11.f
            android.support.v7.widget.an r3 = new android.support.v7.widget.an
            r3.<init>(r11)
            r0.setOnItemSelectedListener(r3)
            android.support.v7.widget.ao r0 = r11.f
            android.support.v7.widget.au r3 = r11.x
            r0.setOnScrollListener(r3)
            android.widget.AdapterView$OnItemSelectedListener r0 = r11.u
            if (r0 == 0) goto L_0x005e
            android.support.v7.widget.ao r0 = r11.f
            android.widget.AdapterView$OnItemSelectedListener r3 = r11.u
            r0.setOnItemSelectedListener(r3)
        L_0x005e:
            android.support.v7.widget.ao r0 = r11.f
            android.view.View r6 = r11.o
            if (r6 == 0) goto L_0x0283
            android.widget.LinearLayout r3 = new android.widget.LinearLayout
            r3.<init>(r4)
            r3.setOrientation(r1)
            android.widget.LinearLayout$LayoutParams r4 = new android.widget.LinearLayout$LayoutParams
            r7 = 1065353216(0x3f800000, float:1.0)
            r4.<init>(r5, r2, r7)
            int r7 = r11.p
            switch(r7) {
                case 0: goto L_0x012e;
                case 1: goto L_0x0126;
                default: goto L_0x0078;
            }
        L_0x0078:
            java.lang.String r0 = "ListPopupWindow"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r7 = "Invalid hint position "
            r4.<init>(r7)
            int r7 = r11.p
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.String r4 = r4.toString()
            android.util.Log.e(r0, r4)
        L_0x008e:
            int r0 = r11.h
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r10)
            r6.measure(r0, r2)
            android.view.ViewGroup$LayoutParams r0 = r6.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r4 = r6.getMeasuredHeight()
            int r6 = r0.topMargin
            int r4 = r4 + r6
            int r0 = r0.bottomMargin
            int r0 = r0 + r4
        L_0x00a7:
            android.widget.PopupWindow r4 = r11.d
            r4.setContentView(r3)
        L_0x00ac:
            android.widget.PopupWindow r3 = r11.d
            android.graphics.drawable.Drawable r3 = r3.getBackground()
            if (r3 == 0) goto L_0x0151
            android.graphics.Rect r4 = r11.B
            r3.getPadding(r4)
            android.graphics.Rect r3 = r11.B
            int r3 = r3.top
            android.graphics.Rect r4 = r11.B
            int r4 = r4.bottom
            int r3 = r3 + r4
            boolean r4 = r11.k
            if (r4 != 0) goto L_0x00cd
            android.graphics.Rect r4 = r11.B
            int r4 = r4.top
            int r4 = -r4
            r11.j = r4
        L_0x00cd:
            android.widget.PopupWindow r4 = r11.d
            r4.getInputMethodMode()
            android.widget.PopupWindow r4 = r11.d
            android.view.View r6 = r11.r
            int r7 = r11.j
            int r6 = r4.getMaxAvailableHeight(r6, r7)
            boolean r4 = r11.m
            if (r4 != 0) goto L_0x00e4
            int r4 = r11.g
            if (r4 != r5) goto L_0x0159
        L_0x00e4:
            int r0 = r6 + r3
        L_0x00e6:
            boolean r6 = r11.i()
            android.widget.PopupWindow r3 = r11.d
            boolean r3 = r3.isShowing()
            if (r3 == 0) goto L_0x01de
            int r3 = r11.h
            if (r3 != r5) goto L_0x01ab
            r4 = r5
        L_0x00f7:
            int r3 = r11.g
            if (r3 != r5) goto L_0x01d0
            if (r6 == 0) goto L_0x01bb
            r3 = r0
        L_0x00fe:
            if (r6 == 0) goto L_0x01c1
            android.widget.PopupWindow r0 = r11.d
            int r6 = r11.h
            if (r6 != r5) goto L_0x01be
        L_0x0106:
            r0.setWindowLayoutMode(r5, r2)
            r5 = r3
        L_0x010a:
            android.widget.PopupWindow r0 = r11.d
            boolean r3 = r11.n
            if (r3 != 0) goto L_0x01db
            boolean r3 = r11.m
            if (r3 != 0) goto L_0x01db
        L_0x0114:
            r0.setOutsideTouchable(r1)
            android.widget.PopupWindow r0 = r11.d
            android.view.View r1 = r11.r
            int r2 = r11.i
            int r3 = r11.j
            r0.update(r1, r2, r3, r4, r5)
        L_0x0122:
            return
        L_0x0123:
            r0 = r2
            goto L_0x001a
        L_0x0126:
            r3.addView(r0, r4)
            r3.addView(r6)
            goto L_0x008e
        L_0x012e:
            r3.addView(r6)
            r3.addView(r0, r4)
            goto L_0x008e
        L_0x0136:
            android.widget.PopupWindow r0 = r11.d
            r0.getContentView()
            android.view.View r3 = r11.o
            if (r3 == 0) goto L_0x0280
            android.view.ViewGroup$LayoutParams r0 = r3.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r3 = r3.getMeasuredHeight()
            int r4 = r0.topMargin
            int r3 = r3 + r4
            int r0 = r0.bottomMargin
            int r0 = r0 + r3
            goto L_0x00ac
        L_0x0151:
            android.graphics.Rect r3 = r11.B
            r3.setEmpty()
            r3 = r2
            goto L_0x00cd
        L_0x0159:
            int r4 = r11.h
            switch(r4) {
                case -2: goto L_0x0173;
                case -1: goto L_0x018e;
                default: goto L_0x015e;
            }
        L_0x015e:
            int r4 = r11.h
            r7 = 1073741824(0x40000000, float:2.0)
            int r4 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r7)
        L_0x0166:
            android.support.v7.widget.ao r7 = r11.f
            int r6 = r6 - r0
            int r4 = r7.a(r4, r6)
            if (r4 <= 0) goto L_0x0170
            int r0 = r0 + r3
        L_0x0170:
            int r0 = r0 + r4
            goto L_0x00e6
        L_0x0173:
            android.content.Context r4 = r11.c
            android.content.res.Resources r4 = r4.getResources()
            android.util.DisplayMetrics r4 = r4.getDisplayMetrics()
            int r4 = r4.widthPixels
            android.graphics.Rect r7 = r11.B
            int r7 = r7.left
            android.graphics.Rect r8 = r11.B
            int r8 = r8.right
            int r7 = r7 + r8
            int r4 = r4 - r7
            int r4 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r10)
            goto L_0x0166
        L_0x018e:
            android.content.Context r4 = r11.c
            android.content.res.Resources r4 = r4.getResources()
            android.util.DisplayMetrics r4 = r4.getDisplayMetrics()
            int r4 = r4.widthPixels
            android.graphics.Rect r7 = r11.B
            int r7 = r7.left
            android.graphics.Rect r8 = r11.B
            int r8 = r8.right
            int r7 = r7 + r8
            int r4 = r4 - r7
            r7 = 1073741824(0x40000000, float:2.0)
            int r4 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r7)
            goto L_0x0166
        L_0x01ab:
            int r3 = r11.h
            if (r3 != r9) goto L_0x01b7
            android.view.View r3 = r11.r
            int r4 = r3.getWidth()
            goto L_0x00f7
        L_0x01b7:
            int r4 = r11.h
            goto L_0x00f7
        L_0x01bb:
            r3 = r5
            goto L_0x00fe
        L_0x01be:
            r5 = r2
            goto L_0x0106
        L_0x01c1:
            android.widget.PopupWindow r6 = r11.d
            int r0 = r11.h
            if (r0 != r5) goto L_0x01ce
            r0 = r5
        L_0x01c8:
            r6.setWindowLayoutMode(r0, r5)
            r5 = r3
            goto L_0x010a
        L_0x01ce:
            r0 = r2
            goto L_0x01c8
        L_0x01d0:
            int r3 = r11.g
            if (r3 != r9) goto L_0x01d7
            r5 = r0
            goto L_0x010a
        L_0x01d7:
            int r5 = r11.g
            goto L_0x010a
        L_0x01db:
            r1 = r2
            goto L_0x0114
        L_0x01de:
            int r3 = r11.h
            if (r3 != r5) goto L_0x0245
            r3 = r5
        L_0x01e3:
            int r4 = r11.g
            if (r4 != r5) goto L_0x025f
            r0 = r5
        L_0x01e8:
            android.widget.PopupWindow r4 = r11.d
            r4.setWindowLayoutMode(r3, r0)
            java.lang.reflect.Method r0 = android.support.v7.widget.ListPopupWindow.a
            if (r0 == 0) goto L_0x0203
            java.lang.reflect.Method r0 = android.support.v7.widget.ListPopupWindow.a     // Catch:{ Exception -> 0x0275 }
            android.widget.PopupWindow r3 = r11.d     // Catch:{ Exception -> 0x0275 }
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0275 }
            r6 = 0
            r7 = 1
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Exception -> 0x0275 }
            r4[r6] = r7     // Catch:{ Exception -> 0x0275 }
            r0.invoke(r3, r4)     // Catch:{ Exception -> 0x0275 }
        L_0x0203:
            android.widget.PopupWindow r0 = r11.d
            boolean r3 = r11.n
            if (r3 != 0) goto L_0x027e
            boolean r3 = r11.m
            if (r3 != 0) goto L_0x027e
        L_0x020d:
            r0.setOutsideTouchable(r1)
            android.widget.PopupWindow r0 = r11.d
            android.support.v7.widget.av r1 = r11.w
            r0.setTouchInterceptor(r1)
            android.widget.PopupWindow r0 = r11.d
            android.view.View r1 = r11.r
            int r2 = r11.i
            int r3 = r11.j
            int r4 = r11.l
            android.support.v4.widget.al.a(r0, r1, r2, r3, r4)
            android.support.v7.widget.ao r0 = r11.f
            r0.setSelection(r5)
            boolean r0 = r11.C
            if (r0 == 0) goto L_0x0235
            android.support.v7.widget.ao r0 = r11.f
            boolean r0 = r0.isInTouchMode()
            if (r0 == 0) goto L_0x0238
        L_0x0235:
            r11.h()
        L_0x0238:
            boolean r0 = r11.C
            if (r0 != 0) goto L_0x0122
            android.os.Handler r0 = r11.A
            android.support.v7.widget.as r1 = r11.y
            r0.post(r1)
            goto L_0x0122
        L_0x0245:
            int r3 = r11.h
            if (r3 != r9) goto L_0x0256
            android.widget.PopupWindow r3 = r11.d
            android.view.View r4 = r11.r
            int r4 = r4.getWidth()
            r3.setWidth(r4)
            r3 = r2
            goto L_0x01e3
        L_0x0256:
            android.widget.PopupWindow r3 = r11.d
            int r4 = r11.h
            r3.setWidth(r4)
            r3 = r2
            goto L_0x01e3
        L_0x025f:
            int r4 = r11.g
            if (r4 != r9) goto L_0x026b
            android.widget.PopupWindow r4 = r11.d
            r4.setHeight(r0)
            r0 = r2
            goto L_0x01e8
        L_0x026b:
            android.widget.PopupWindow r0 = r11.d
            int r4 = r11.g
            r0.setHeight(r4)
            r0 = r2
            goto L_0x01e8
        L_0x0275:
            r0 = move-exception
            java.lang.String r0 = "ListPopupWindow"
            java.lang.String r3 = "Could not call setClipToScreenEnabled() on PopupWindow. Oh well."
            android.util.Log.i(r0, r3)
            goto L_0x0203
        L_0x027e:
            r1 = r2
            goto L_0x020d
        L_0x0280:
            r0 = r2
            goto L_0x00ac
        L_0x0283:
            r3 = r0
            r0 = r2
            goto L_0x00a7
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ListPopupWindow.c():void");
    }

    public final void d() {
        this.p = 0;
    }

    public final void e() {
        this.C = true;
        this.d.setFocusable(true);
    }

    public final View f() {
        return this.r;
    }

    public final void g() {
        this.d.setInputMethodMode(2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ao.a(android.support.v7.widget.ao, boolean):boolean
     arg types: [android.support.v7.widget.ao, int]
     candidates:
      android.support.v7.widget.ao.a(android.view.MotionEvent, int):boolean
      android.support.v7.internal.widget.ListViewCompat.a(int, int):int
      android.support.v7.widget.ao.a(android.support.v7.widget.ao, boolean):boolean */
    public final void h() {
        ao aoVar = this.f;
        if (aoVar != null) {
            boolean unused = aoVar.f = true;
            aoVar.requestLayout();
        }
    }

    public final boolean i() {
        return this.d.getInputMethodMode() == 2;
    }

    public final ListView j() {
        return this.f;
    }
}
