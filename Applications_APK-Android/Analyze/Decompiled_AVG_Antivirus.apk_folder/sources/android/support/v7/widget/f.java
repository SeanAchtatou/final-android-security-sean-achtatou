package android.support.v7.widget;

import android.content.Context;
import android.support.v7.a.b;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.v;
import android.view.View;

final class f extends v {
    final /* synthetic */ ActionMenuPresenter c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(ActionMenuPresenter actionMenuPresenter, Context context, i iVar, View view) {
        super(context, iVar, view, true, b.actionOverflowMenuStyle);
        this.c = actionMenuPresenter;
        c();
        a(actionMenuPresenter.g);
    }

    public final void onDismiss() {
        super.onDismiss();
        this.c.c.close();
        f unused = this.c.v = (f) null;
    }
}
