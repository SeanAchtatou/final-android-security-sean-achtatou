package android.support.v7.widget.a;

import android.support.v7.widget.bl;

final class f implements bl {
    final /* synthetic */ a a;

    f(a aVar) {
        this.a = aVar;
    }

    public final int a(int i, int i2) {
        if (this.a.v == null) {
            return i2;
        }
        int h = this.a.w;
        if (h == -1) {
            h = this.a.p.indexOfChild(this.a.v);
            int unused = this.a.w = h;
        }
        return i2 == i + -1 ? h : i2 >= h ? i2 + 1 : i2;
    }
}
