package android.support.v7.widget;

import android.database.DataSetObserver;

final class at extends DataSetObserver {
    final /* synthetic */ ListPopupWindow a;

    private at(ListPopupWindow listPopupWindow) {
        this.a = listPopupWindow;
    }

    /* synthetic */ at(ListPopupWindow listPopupWindow, byte b) {
        this(listPopupWindow);
    }

    public final void onChanged() {
        if (this.a.b()) {
            this.a.c();
        }
    }

    public final void onInvalidated() {
        this.a.a();
    }
}
