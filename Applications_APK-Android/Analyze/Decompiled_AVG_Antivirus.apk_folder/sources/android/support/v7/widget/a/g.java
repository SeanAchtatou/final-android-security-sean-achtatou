package android.support.v7.widget.a;

import android.graphics.Canvas;
import android.os.Build;
import android.support.v4.view.bx;
import android.support.v7.c.b;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.bm;
import android.support.v7.widget.br;
import android.support.v7.widget.cf;
import android.view.animation.Interpolator;
import java.util.List;

public abstract class g {
    private static final n a;
    private static final Interpolator b = new h();
    private static final Interpolator c = new i();
    private int d = -1;

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            a = new r();
        } else if (Build.VERSION.SDK_INT >= 11) {
            a = new q();
        } else {
            a = new p();
        }
    }

    public static int a(int i, int i2) {
        int i3 = i & 789516;
        if (i3 == 0) {
            return i;
        }
        int i4 = (i3 ^ -1) & i;
        return i2 == 0 ? i4 | (i3 << 2) : i4 | ((i3 << 1) & -789517) | (((i3 << 1) & 789516) << 2);
    }

    public static long a(RecyclerView recyclerView, int i) {
        bm i2 = recyclerView.i();
        return i2 == null ? i == 8 ? 200 : 250 : i == 8 ? i2.d() : i2.g();
    }

    public static cf a(cf cfVar, List list, int i, int i2) {
        cf cfVar2;
        int i3;
        int i4;
        int i5;
        int i6;
        cf cfVar3;
        int bottom;
        int abs;
        int top;
        int left;
        int right;
        int abs2;
        int width = i + cfVar.a.getWidth();
        int height = i2 + cfVar.a.getHeight();
        cf cfVar4 = null;
        int i7 = -1;
        int left2 = i - cfVar.a.getLeft();
        int top2 = i2 - cfVar.a.getTop();
        int size = list.size();
        int i8 = 0;
        while (i8 < size) {
            cf cfVar5 = (cf) list.get(i8);
            if (left2 <= 0 || (right = cfVar5.a.getRight() - width) >= 0 || cfVar5.a.getRight() <= cfVar.a.getRight() || (abs2 = Math.abs(right)) <= i7) {
                cfVar2 = cfVar4;
                i3 = i7;
            } else {
                i3 = abs2;
                cfVar2 = cfVar5;
            }
            if (left2 >= 0 || (left = cfVar5.a.getLeft() - i) <= 0 || cfVar5.a.getLeft() >= cfVar.a.getLeft() || (i4 = Math.abs(left)) <= i3) {
                i4 = i3;
            } else {
                cfVar2 = cfVar5;
            }
            if (top2 >= 0 || (top = cfVar5.a.getTop() - i2) <= 0 || cfVar5.a.getTop() >= cfVar.a.getTop() || (i5 = Math.abs(top)) <= i4) {
                i5 = i4;
            } else {
                cfVar2 = cfVar5;
            }
            if (top2 <= 0 || (bottom = cfVar5.a.getBottom() - height) >= 0 || cfVar5.a.getBottom() <= cfVar.a.getBottom() || (abs = Math.abs(bottom)) <= i5) {
                i6 = i5;
                cfVar3 = cfVar2;
            } else {
                int i9 = abs;
                cfVar3 = cfVar5;
                i6 = i9;
            }
            i8++;
            cfVar4 = cfVar3;
            i7 = i6;
        }
        return cfVar4;
    }

    private static void a(Canvas canvas, RecyclerView recyclerView, cf cfVar, float f, float f2, int i) {
        a.a(canvas, recyclerView, cfVar.a, f, f2, i);
    }

    static /* synthetic */ void a(Canvas canvas, RecyclerView recyclerView, cf cfVar, List list, int i, float f, float f2) {
        boolean z;
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            k kVar = (k) list.get(i2);
            int save = canvas.save();
            a(canvas, recyclerView, kVar.h, kVar.k, kVar.l, kVar.i);
            canvas.restoreToCount(save);
        }
        if (cfVar != null) {
            int save2 = canvas.save();
            a(canvas, recyclerView, cfVar, f, f2, i);
            canvas.restoreToCount(save2);
        }
        boolean z2 = false;
        int i3 = size - 1;
        while (i3 >= 0) {
            k kVar2 = (k) list.get(i3);
            if (!kVar2.c || kVar2.j) {
                z = !kVar2.c ? true : z2;
            } else {
                list.remove(i3);
                z = z2;
            }
            i3--;
            z2 = z;
        }
        if (z2) {
            recyclerView.invalidate();
        }
    }

    public static void a(RecyclerView recyclerView, cf cfVar, cf cfVar2, int i) {
        br b2 = recyclerView.b();
        if (b2 instanceof m) {
            ((m) b2).a(cfVar.a, cfVar2.a);
            return;
        }
        if (b2.e()) {
            if (br.h(cfVar2.a) <= recyclerView.getPaddingLeft()) {
                recyclerView.a(i);
            }
            if (br.j(cfVar2.a) >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                recyclerView.a(i);
            }
        }
        if (b2.f()) {
            if (br.i(cfVar2.a) <= recyclerView.getPaddingTop()) {
                recyclerView.a(i);
            }
            if (br.k(cfVar2.a) >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                recyclerView.a(i);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.a.g.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.cf, float, float, int, boolean):void
     arg types: [android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.cf, float, float, int, int]
     candidates:
      android.support.v7.widget.a.g.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.cf, java.util.List, int, float, float):void
      android.support.v7.widget.a.g.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.cf, float, float, int, boolean):void */
    static /* synthetic */ void a(g gVar, Canvas canvas, RecyclerView recyclerView, cf cfVar, List list, int i, float f, float f2) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            k kVar = (k) list.get(i2);
            kVar.d();
            int save = canvas.save();
            gVar.a(canvas, recyclerView, kVar.h, kVar.k, kVar.l, kVar.i, false);
            canvas.restoreToCount(save);
        }
        if (cfVar != null) {
            int save2 = canvas.save();
            gVar.a(canvas, recyclerView, cfVar, f, f2, i, true);
            canvas.restoreToCount(save2);
        }
    }

    static /* synthetic */ boolean a(g gVar, RecyclerView recyclerView) {
        return (gVar.a(recyclerView) & 16711680) != 0;
    }

    public static int b(int i, int i2) {
        return ((i2 | i) << 0) | (i2 << 8) | (i << 16);
    }

    public static boolean b() {
        return true;
    }

    public static int c(int i, int i2) {
        int i3 = i & 3158064;
        if (i3 == 0) {
            return i;
        }
        int i4 = (i3 ^ -1) & i;
        return i2 == 0 ? i4 | (i3 >> 2) : i4 | ((i3 >> 1) & -3158065) | (((i3 >> 1) & 3158064) >> 2);
    }

    public static int e() {
        return 0;
    }

    public static float f() {
        return 0.5f;
    }

    public static float g() {
        return 0.5f;
    }

    public abstract int a();

    /* access modifiers changed from: package-private */
    public final int a(RecyclerView recyclerView) {
        return c(a(), bx.h(recyclerView));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final int a(RecyclerView recyclerView, int i, int i2, long j) {
        float f = 1.0f;
        if (this.d == -1) {
            this.d = recyclerView.getResources().getDimensionPixelSize(b.item_touch_helper_max_drag_scroll_per_frame);
        }
        int signum = (int) (((float) (this.d * ((int) Math.signum((float) i2)))) * c.getInterpolation(Math.min(1.0f, (((float) Math.abs(i2)) * 1.0f) / ((float) i))));
        if (j <= 2000) {
            f = ((float) j) / 2000.0f;
        }
        int interpolation = (int) (b.getInterpolation(f) * ((float) signum));
        return interpolation == 0 ? i2 > 0 ? 1 : -1 : interpolation;
    }

    public void a(Canvas canvas, RecyclerView recyclerView, cf cfVar, float f, float f2, int i, boolean z) {
        a.a(canvas, recyclerView, cfVar.a, f, f2, i, z);
    }

    public void a(RecyclerView recyclerView, cf cfVar) {
        a.a(cfVar.a);
    }

    public abstract void a(cf cfVar);

    public void a(cf cfVar, int i) {
        if (cfVar != null) {
            a.b(cfVar.a);
        }
    }

    public abstract boolean a(cf cfVar, cf cfVar2);

    public boolean c() {
        return true;
    }

    public boolean d() {
        return true;
    }
}
