package android.support.v7.widget;

import android.view.View;

final class e extends ap {
    final /* synthetic */ ActionMenuPresenter a;
    final /* synthetic */ d b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(d dVar, View view, ActionMenuPresenter actionMenuPresenter) {
        super(view);
        this.b = dVar;
        this.a = actionMenuPresenter;
    }

    public final ListPopupWindow a() {
        if (this.b.a.v == null) {
            return null;
        }
        return this.b.a.v.f();
    }

    public final boolean b() {
        this.b.a.i();
        return true;
    }

    public final boolean c() {
        if (this.b.a.x != null) {
            return false;
        }
        this.b.a.j();
        return true;
    }
}
