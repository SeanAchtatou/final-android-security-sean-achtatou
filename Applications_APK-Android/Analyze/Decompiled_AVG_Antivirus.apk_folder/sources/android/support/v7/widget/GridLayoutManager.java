package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.a.f;
import android.support.v4.view.a.r;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import java.util.Arrays;

public final class GridLayoutManager extends LinearLayoutManager {
    static final int a = View.MeasureSpec.makeMeasureSpec(0, 0);
    boolean b;
    int c;
    int[] d;
    View[] e;
    final SparseIntArray f;
    final SparseIntArray g;
    ae h;
    final Rect i;

    public class LayoutParams extends RecyclerView.LayoutParams {
        /* access modifiers changed from: private */
        public int e = -1;
        /* access modifiers changed from: private */
        public int f = 0;

        public LayoutParams() {
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public final int a() {
            return this.e;
        }

        public final int b() {
            return this.f;
        }
    }

    private static int a(int i2, int i3, int i4) {
        if (i3 == 0 && i4 == 0) {
            return i2;
        }
        int mode = View.MeasureSpec.getMode(i2);
        return (mode == Integer.MIN_VALUE || mode == 1073741824) ? View.MeasureSpec.makeMeasureSpec((View.MeasureSpec.getSize(i2) - i3) - i4, mode) : i2;
    }

    private int a(bw bwVar, cc ccVar, int i2) {
        if (!ccVar.a()) {
            return this.h.b(i2, this.c);
        }
        int a2 = bwVar.a(i2);
        if (a2 != -1) {
            return this.h.b(a2, this.c);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. " + i2);
        return 0;
    }

    private void a(bw bwVar, cc ccVar, int i2, boolean z) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        if (z) {
            i4 = 1;
            i3 = 0;
        } else {
            i3 = i2 - 1;
            i4 = -1;
            i2 = -1;
        }
        if (this.j != 1 || !h()) {
            i6 = 1;
            i5 = 0;
            i7 = i3;
        } else {
            i6 = -1;
            i5 = this.c - 1;
            i7 = i3;
        }
        while (i7 != i2) {
            View view = this.e[i7];
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            int unused = layoutParams.f = c(bwVar, ccVar, e(view));
            if (i6 != -1 || layoutParams.f <= 1) {
                int unused2 = layoutParams.e = i5;
            } else {
                int unused3 = layoutParams.e = i5 - (layoutParams.f - 1);
            }
            i5 += layoutParams.f * i6;
            i7 += i4;
        }
    }

    private void a(View view, int i2, int i3, boolean z) {
        a(view, this.i);
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        if (z || this.j == 1) {
            i2 = a(i2, layoutParams.leftMargin + this.i.left, layoutParams.rightMargin + this.i.right);
        }
        if (z || this.j == 0) {
            i3 = a(i3, layoutParams.topMargin + this.i.top, layoutParams.bottomMargin + this.i.bottom);
        }
        view.measure(i2, i3);
    }

    private int b(bw bwVar, cc ccVar, int i2) {
        if (!ccVar.a()) {
            return this.h.a(i2, this.c);
        }
        int i3 = this.g.get(i2, -1);
        if (i3 != -1) {
            return i3;
        }
        int a2 = bwVar.a(i2);
        if (a2 != -1) {
            return this.h.a(a2, this.c);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i2);
        return 0;
    }

    private int c(bw bwVar, cc ccVar, int i2) {
        if (!ccVar.a()) {
            return this.h.a();
        }
        int i3 = this.f.get(i2, -1);
        if (i3 != -1) {
            return i3;
        }
        if (bwVar.a(i2) != -1) {
            return this.h.a();
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i2);
        return 1;
    }

    private static int h(int i2) {
        return i2 < 0 ? a : View.MeasureSpec.makeMeasureSpec(i2, 1073741824);
    }

    private void y() {
        int i2;
        int i3 = 0;
        int p = this.j == 1 ? (p() - t()) - r() : (q() - u()) - s();
        if (!(this.d != null && this.d.length == this.c + 1 && this.d[this.d.length - 1] == p)) {
            this.d = new int[(this.c + 1)];
        }
        this.d[0] = 0;
        int i4 = p / this.c;
        int i5 = p % this.c;
        int i6 = 0;
        for (int i7 = 1; i7 <= this.c; i7++) {
            int i8 = i6 + i5;
            if (i8 <= 0 || this.c - i8 >= i5) {
                i6 = i8;
                i2 = i4;
            } else {
                i6 = i8 - this.c;
                i2 = i4 + 1;
            }
            i3 += i2;
            this.d[i7] = i3;
        }
    }

    private void z() {
        if (this.e == null || this.e.length != this.c) {
            this.e = new View[this.c];
        }
    }

    public final int a(int i2, bw bwVar, cc ccVar) {
        y();
        z();
        return super.a(i2, bwVar, ccVar);
    }

    public final int a(bw bwVar, cc ccVar) {
        if (this.j == 0) {
            return this.c;
        }
        if (ccVar.e() <= 0) {
            return 0;
        }
        return a(bwVar, ccVar, ccVar.e() - 1);
    }

    public final RecyclerView.LayoutParams a(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    public final RecyclerView.LayoutParams a(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    public final View a(bw bwVar, cc ccVar, int i2, int i3, int i4) {
        View view;
        View view2 = null;
        i();
        int c2 = this.k.c();
        int d2 = this.k.d();
        int i5 = i3 > i2 ? 1 : -1;
        View view3 = null;
        while (i2 != i3) {
            View d3 = d(i2);
            int e2 = e(d3);
            if (e2 >= 0 && e2 < i4 && b(bwVar, ccVar, e2) == 0) {
                if (((RecyclerView.LayoutParams) d3.getLayoutParams()).a.m()) {
                    if (view3 == null) {
                        view = view2;
                        i2 += i5;
                        view2 = view;
                        view3 = d3;
                    }
                } else if (this.k.a(d3) < d2 && this.k.b(d3) >= c2) {
                    return d3;
                } else {
                    if (view2 == null) {
                        view = d3;
                        d3 = view3;
                        i2 += i5;
                        view2 = view;
                        view3 = d3;
                    }
                }
            }
            view = view2;
            d3 = view3;
            i2 += i5;
            view2 = view;
            view3 = d3;
        }
        return view2 != null ? view2 : view3;
    }

    public final void a() {
        this.h.a.clear();
    }

    public final void a(int i2, int i3) {
        this.h.a.clear();
    }

    /* access modifiers changed from: package-private */
    public final void a(bw bwVar, cc ccVar, ah ahVar) {
        super.a(bwVar, ccVar, ahVar);
        y();
        if (ccVar.e() > 0 && !ccVar.a()) {
            int b2 = b(bwVar, ccVar, ahVar.a);
            while (b2 > 0 && ahVar.a > 0) {
                ahVar.a--;
                b2 = b(bwVar, ccVar, ahVar.a);
            }
        }
        z();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.GridLayoutManager.a(android.view.View, int, int, boolean):void
     arg types: [android.view.View, int, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, boolean):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.GridLayoutManager.a(android.view.View, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(bw bwVar, cc ccVar, aj ajVar, ai aiVar) {
        int i2;
        int i3;
        View a2;
        boolean z = ajVar.e == 1;
        int i4 = 0;
        int i5 = this.c;
        if (!z) {
            i5 = b(bwVar, ccVar, ajVar.d) + c(bwVar, ccVar, ajVar.d);
        }
        while (i4 < this.c && ajVar.a(ccVar) && i5 > 0) {
            int i6 = ajVar.d;
            int c2 = c(bwVar, ccVar, i6);
            if (c2 <= this.c) {
                i5 -= c2;
                if (i5 < 0 || (a2 = ajVar.a(bwVar)) == null) {
                    break;
                }
                this.e[i4] = a2;
                i4++;
            } else {
                throw new IllegalArgumentException("Item at position " + i6 + " requires " + c2 + " spans but GridLayoutManager has only " + this.c + " spans.");
            }
        }
        if (i4 == 0) {
            aiVar.b = true;
            return;
        }
        int i7 = 0;
        a(bwVar, ccVar, i4, z);
        int i8 = 0;
        while (i8 < i4) {
            View view = this.e[i8];
            if (ajVar.k == null) {
                if (z) {
                    c(view);
                } else {
                    d(view);
                }
            } else if (z) {
                a(view);
            } else {
                b(view);
            }
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.d[layoutParams.e + layoutParams.f] - this.d[layoutParams.e], 1073741824);
            if (this.j == 1) {
                a(view, makeMeasureSpec, h(layoutParams.height), false);
            } else {
                a(view, h(layoutParams.width), makeMeasureSpec, false);
            }
            int c3 = this.k.c(view);
            if (c3 <= i7) {
                c3 = i7;
            }
            i8++;
            i7 = c3;
        }
        int h2 = h(i7);
        for (int i9 = 0; i9 < i4; i9++) {
            View view2 = this.e[i9];
            if (this.k.c(view2) != i7) {
                LayoutParams layoutParams2 = (LayoutParams) view2.getLayoutParams();
                int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(this.d[layoutParams2.e + layoutParams2.f] - this.d[layoutParams2.e], 1073741824);
                if (this.j == 1) {
                    a(view2, makeMeasureSpec2, h2, true);
                } else {
                    a(view2, h2, makeMeasureSpec2, true);
                }
            }
        }
        aiVar.a = i7;
        int i10 = 0;
        int i11 = 0;
        if (this.j == 1) {
            if (ajVar.f == -1) {
                i11 = ajVar.b;
                i3 = i11 - i7;
                i2 = 0;
            } else {
                int i12 = ajVar.b;
                i11 = i12 + i7;
                i3 = i12;
                i2 = 0;
            }
        } else if (ajVar.f == -1) {
            int i13 = ajVar.b;
            i10 = i13 - i7;
            i3 = 0;
            i2 = i13;
        } else {
            i10 = ajVar.b;
            i2 = i7 + i10;
            i3 = 0;
        }
        int i14 = i2;
        int i15 = i3;
        int i16 = i11;
        int i17 = i10;
        for (int i18 = 0; i18 < i4; i18++) {
            View view3 = this.e[i18];
            LayoutParams layoutParams3 = (LayoutParams) view3.getLayoutParams();
            if (this.j == 1) {
                i17 = r() + this.d[layoutParams3.e];
                i14 = this.k.d(view3) + i17;
            } else {
                i15 = this.d[layoutParams3.e] + s();
                i16 = this.k.d(view3) + i15;
            }
            a(view3, layoutParams3.leftMargin + i17, layoutParams3.topMargin + i15, i14 - layoutParams3.rightMargin, i16 - layoutParams3.bottomMargin);
            if (layoutParams3.a.m() || layoutParams3.a.s()) {
                aiVar.c = true;
            }
            aiVar.d |= view3.isFocusable();
        }
        Arrays.fill(this.e, (Object) null);
    }

    public final void a(bw bwVar, cc ccVar, View view, f fVar) {
        boolean z = false;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            super.a(view, fVar);
            return;
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        int a2 = a(bwVar, ccVar, layoutParams2.a.e_());
        if (this.j == 0) {
            fVar.b(r.a(layoutParams2.a(), layoutParams2.b(), a2, 1, this.c > 1 && layoutParams2.b() == this.c));
            return;
        }
        int a3 = layoutParams2.a();
        int b2 = layoutParams2.b();
        if (this.c > 1 && layoutParams2.b() == this.c) {
            z = true;
        }
        fVar.b(r.a(a2, 1, a3, b2, z));
    }

    public final void a(boolean z) {
        if (z) {
            throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
        }
        super.a(false);
    }

    public final boolean a(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public final int b(int i2, bw bwVar, cc ccVar) {
        y();
        z();
        return super.b(i2, bwVar, ccVar);
    }

    public final int b(bw bwVar, cc ccVar) {
        if (this.j == 1) {
            return this.c;
        }
        if (ccVar.e() <= 0) {
            return 0;
        }
        return a(bwVar, ccVar, ccVar.e() - 1);
    }

    public final RecyclerView.LayoutParams b() {
        return new LayoutParams();
    }

    public final void b(int i2, int i3) {
        this.h.a.clear();
    }

    public final void c(int i2, int i3) {
        this.h.a.clear();
    }

    public final void c(bw bwVar, cc ccVar) {
        if (ccVar.a()) {
            int o = o();
            for (int i2 = 0; i2 < o; i2++) {
                LayoutParams layoutParams = (LayoutParams) d(i2).getLayoutParams();
                int e_ = layoutParams.a.e_();
                this.f.put(e_, layoutParams.b());
                this.g.put(e_, layoutParams.a());
            }
        }
        super.c(bwVar, ccVar);
        this.f.clear();
        this.g.clear();
        if (!ccVar.a()) {
            this.b = false;
        }
    }

    public final boolean c() {
        return this.o == null && !this.b;
    }

    public final void d(int i2, int i3) {
        this.h.a.clear();
    }
}
