package android.support.v7.widget;

import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

final class p {
    final r a;
    final q b = new q();
    final List c = new ArrayList();

    p(r rVar) {
        this.a = rVar;
    }

    private int e(int i) {
        if (i < 0) {
            return -1;
        }
        int a2 = this.a.a();
        int i2 = i;
        while (i2 < a2) {
            int e = i - (i2 - this.b.e(i2));
            if (e == 0) {
                while (this.b.c(i2)) {
                    i2++;
                }
                return i2;
            }
            i2 += e;
        }
        return -1;
    }

    private void h(View view) {
        this.c.add(view);
        this.a.c(view);
    }

    private boolean i(View view) {
        if (!this.c.remove(view)) {
            return false;
        }
        this.a.d(view);
        return true;
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        return this.a.a() - this.c.size();
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        int e = e(i);
        View b2 = this.a.b(e);
        if (b2 != null) {
            if (this.b.d(e)) {
                i(b2);
            }
            this.a.a(e);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(View view) {
        a(view, -1, true);
    }

    /* access modifiers changed from: package-private */
    public final void a(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        int a2 = i < 0 ? this.a.a() : e(i);
        this.b.a(a2, z);
        if (z) {
            h(view);
        }
        this.a.a(view, a2, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public final void a(View view, int i, boolean z) {
        int a2 = i < 0 ? this.a.a() : e(i);
        this.b.a(a2, z);
        if (z) {
            h(view);
        }
        this.a.a(view, a2);
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        return this.a.a();
    }

    /* access modifiers changed from: package-private */
    public final View b(int i) {
        return this.a.b(e(i));
    }

    /* access modifiers changed from: package-private */
    public final void b(View view) {
        int a2 = this.a.a(view);
        if (a2 >= 0) {
            if (this.b.d(a2)) {
                i(view);
            }
            this.a.a(a2);
        }
    }

    /* access modifiers changed from: package-private */
    public final int c(View view) {
        int a2 = this.a.a(view);
        if (a2 != -1 && !this.b.c(a2)) {
            return a2 - this.b.e(a2);
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public final View c(int i) {
        return this.a.b(i);
    }

    /* access modifiers changed from: package-private */
    public final void d(int i) {
        int e = e(i);
        this.b.d(e);
        this.a.c(e);
    }

    /* access modifiers changed from: package-private */
    public final boolean d(View view) {
        return this.c.contains(view);
    }

    /* access modifiers changed from: package-private */
    public final void e(View view) {
        int a2 = this.a.a(view);
        if (a2 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        }
        this.b.a(a2);
        h(view);
    }

    /* access modifiers changed from: package-private */
    public final void f(View view) {
        int a2 = this.a.a(view);
        if (a2 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        } else if (!this.b.c(a2)) {
            throw new RuntimeException("trying to unhide a view that was not hidden" + view);
        } else {
            this.b.b(a2);
            i(view);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean g(View view) {
        int a2 = this.a.a(view);
        if (a2 == -1) {
            i(view);
            return true;
        } else if (!this.b.c(a2)) {
            return false;
        } else {
            this.b.d(a2);
            i(view);
            this.a.a(a2);
            return true;
        }
    }

    public final String toString() {
        return this.b.toString() + ", hidden list:" + this.c.size();
    }
}
