package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

final class bb extends az {
    bb(br brVar) {
        super(brVar, (byte) 0);
    }

    public final int a(View view) {
        return br.i(view) - ((RecyclerView.LayoutParams) view.getLayoutParams()).topMargin;
    }

    public final void a(int i) {
        this.a.f(i);
    }

    public final int b(View view) {
        return ((RecyclerView.LayoutParams) view.getLayoutParams()).bottomMargin + br.k(view);
    }

    public final int c() {
        return this.a.s();
    }

    public final int c(View view) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        return layoutParams.bottomMargin + br.g(view) + layoutParams.topMargin;
    }

    public final int d() {
        return this.a.q() - this.a.u();
    }

    public final int d(View view) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        return layoutParams.rightMargin + br.f(view) + layoutParams.leftMargin;
    }

    public final int e() {
        return this.a.q();
    }

    public final int f() {
        return (this.a.q() - this.a.s()) - this.a.u();
    }

    public final int g() {
        return this.a.u();
    }
}
