package android.support.v7.widget;

import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import java.util.ArrayList;

final class de {
    int a;
    int b;
    int c;
    final int d;
    final /* synthetic */ StaggeredGridLayoutManager e;
    /* access modifiers changed from: private */
    public ArrayList f;

    private void f() {
        StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem d2;
        View view = (View) this.f.get(0);
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
        this.a = this.e.a.a(view);
        if (layoutParams.f && (d2 = this.e.f.d(layoutParams.a.e_())) != null && d2.b == -1) {
            this.a -= d2.a(this.d);
        }
    }

    private void g() {
        StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem d2;
        View view = (View) this.f.get(this.f.size() - 1);
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
        this.b = this.e.a.b(view);
        if (layoutParams.f && (d2 = this.e.f.d(layoutParams.a.e_())) != null && d2.b == 1) {
            this.b = d2.a(this.d) + this.b;
        }
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        if (this.a != Integer.MIN_VALUE) {
            return this.a;
        }
        f();
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final int a(int i) {
        if (this.a != Integer.MIN_VALUE) {
            return this.a;
        }
        if (this.f.size() == 0) {
            return i;
        }
        f();
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final void a(View view) {
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
        layoutParams.e = this;
        this.f.add(0, view);
        this.a = Integer.MIN_VALUE;
        if (this.f.size() == 1) {
            this.b = Integer.MIN_VALUE;
        }
        if (layoutParams.a.m() || layoutParams.a.s()) {
            this.c += this.e.a.c(view);
        }
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        if (this.b != Integer.MIN_VALUE) {
            return this.b;
        }
        g();
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final int b(int i) {
        if (this.b != Integer.MIN_VALUE) {
            return this.b;
        }
        if (this.f.size() == 0) {
            return i;
        }
        g();
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void b(View view) {
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
        layoutParams.e = this;
        this.f.add(view);
        this.b = Integer.MIN_VALUE;
        if (this.f.size() == 1) {
            this.a = Integer.MIN_VALUE;
        }
        if (layoutParams.a.m() || layoutParams.a.s()) {
            this.c += this.e.a.c(view);
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.f.clear();
        this.a = Integer.MIN_VALUE;
        this.b = Integer.MIN_VALUE;
        this.c = 0;
    }

    /* access modifiers changed from: package-private */
    public final void c(int i) {
        this.a = i;
        this.b = i;
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        int size = this.f.size();
        View view = (View) this.f.remove(size - 1);
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
        layoutParams.e = null;
        if (layoutParams.a.m() || layoutParams.a.s()) {
            this.c -= this.e.a.c(view);
        }
        if (size == 1) {
            this.a = Integer.MIN_VALUE;
        }
        this.b = Integer.MIN_VALUE;
    }

    /* access modifiers changed from: package-private */
    public final void d(int i) {
        if (this.a != Integer.MIN_VALUE) {
            this.a += i;
        }
        if (this.b != Integer.MIN_VALUE) {
            this.b += i;
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        View view = (View) this.f.remove(0);
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
        layoutParams.e = null;
        if (this.f.size() == 0) {
            this.b = Integer.MIN_VALUE;
        }
        if (layoutParams.a.m() || layoutParams.a.s()) {
            this.c -= this.e.a.c(view);
        }
        this.a = Integer.MIN_VALUE;
    }
}
