package android.support.v7.widget;

import android.view.View;

public abstract class az {
    protected final br a;
    private int b;

    private az(br brVar) {
        this.b = Integer.MIN_VALUE;
        this.a = brVar;
    }

    /* synthetic */ az(br brVar, byte b2) {
        this(brVar);
    }

    public static az a(br brVar, int i) {
        switch (i) {
            case 0:
                return new ba(brVar);
            case 1:
                return new bb(brVar);
            default:
                throw new IllegalArgumentException("invalid orientation");
        }
    }

    public abstract int a(View view);

    public final void a() {
        this.b = f();
    }

    public abstract void a(int i);

    public final int b() {
        if (Integer.MIN_VALUE == this.b) {
            return 0;
        }
        return f() - this.b;
    }

    public abstract int b(View view);

    public abstract int c();

    public abstract int c(View view);

    public abstract int d();

    public abstract int d(View view);

    public abstract int e();

    public abstract int f();

    public abstract int g();
}
