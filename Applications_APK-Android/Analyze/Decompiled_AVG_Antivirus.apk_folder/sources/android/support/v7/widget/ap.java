package android.support.v7.widget;

import android.os.SystemClock;
import android.support.v4.view.ay;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

public abstract class ap implements View.OnTouchListener {
    private final float a;
    private final int b;
    private final int c;
    /* access modifiers changed from: private */
    public final View d;
    private Runnable e;
    private Runnable f;
    private boolean g;
    private boolean h;
    private int i;
    private final int[] j = new int[2];

    public ap(View view) {
        this.d = view;
        this.a = (float) ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        this.b = ViewConfiguration.getTapTimeout();
        this.c = (this.b + ViewConfiguration.getLongPressTimeout()) / 2;
    }

    private boolean a(MotionEvent motionEvent) {
        View view = this.d;
        ListPopupWindow a2 = a();
        if (a2 == null || !a2.b()) {
            return false;
        }
        ao a3 = a2.f;
        if (a3 == null || !a3.isShown()) {
            return false;
        }
        MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
        int[] iArr = this.j;
        view.getLocationOnScreen(iArr);
        obtainNoHistory.offsetLocation((float) iArr[0], (float) iArr[1]);
        int[] iArr2 = this.j;
        a3.getLocationOnScreen(iArr2);
        obtainNoHistory.offsetLocation((float) (-iArr2[0]), (float) (-iArr2[1]));
        boolean a4 = a3.a(obtainNoHistory, this.i);
        obtainNoHistory.recycle();
        int a5 = ay.a(motionEvent);
        return a4 && (a5 != 1 && a5 != 3);
    }

    static /* synthetic */ void b(ap apVar) {
        apVar.d();
        View view = apVar.d;
        if (view.isEnabled() && !view.isLongClickable() && apVar.b()) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            view.onTouchEvent(obtain);
            obtain.recycle();
            apVar.g = true;
            apVar.h = true;
        }
    }

    private void d() {
        if (this.f != null) {
            this.d.removeCallbacks(this.f);
        }
        if (this.e != null) {
            this.d.removeCallbacks(this.e);
        }
    }

    public abstract ListPopupWindow a();

    /* access modifiers changed from: protected */
    public boolean b() {
        ListPopupWindow a2 = a();
        if (a2 == null || a2.b()) {
            return true;
        }
        a2.c();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        ListPopupWindow a2 = a();
        if (a2 == null || !a2.b()) {
            return true;
        }
        a2.a();
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r12, android.view.MotionEvent r13) {
        /*
            r11 = this;
            r5 = 0
            r8 = 1
            r7 = 0
            boolean r10 = r11.g
            if (r10 == 0) goto L_0x0027
            boolean r0 = r11.h
            if (r0 == 0) goto L_0x0017
            boolean r0 = r11.a(r13)
        L_0x000f:
            r11.g = r0
            if (r0 != 0) goto L_0x0015
            if (r10 == 0) goto L_0x0016
        L_0x0015:
            r7 = r8
        L_0x0016:
            return r7
        L_0x0017:
            boolean r0 = r11.a(r13)
            if (r0 != 0) goto L_0x0023
            boolean r0 = r11.c()
            if (r0 != 0) goto L_0x0025
        L_0x0023:
            r0 = r8
            goto L_0x000f
        L_0x0025:
            r0 = r7
            goto L_0x000f
        L_0x0027:
            android.view.View r1 = r11.d
            boolean r0 = r1.isEnabled()
            if (r0 == 0) goto L_0x0036
            int r0 = android.support.v4.view.ay.a(r13)
            switch(r0) {
                case 0: goto L_0x0057;
                case 1: goto L_0x00d2;
                case 2: goto L_0x0086;
                case 3: goto L_0x00d2;
                default: goto L_0x0036;
            }
        L_0x0036:
            r0 = r7
        L_0x0037:
            if (r0 == 0) goto L_0x00d7
            boolean r0 = r11.b()
            if (r0 == 0) goto L_0x00d7
            r9 = r8
        L_0x0040:
            if (r9 == 0) goto L_0x0055
            long r0 = android.os.SystemClock.uptimeMillis()
            r4 = 3
            r2 = r0
            r6 = r5
            android.view.MotionEvent r0 = android.view.MotionEvent.obtain(r0, r2, r4, r5, r6, r7)
            android.view.View r1 = r11.d
            r1.onTouchEvent(r0)
            r0.recycle()
        L_0x0055:
            r0 = r9
            goto L_0x000f
        L_0x0057:
            int r0 = r13.getPointerId(r7)
            r11.i = r0
            r11.h = r7
            java.lang.Runnable r0 = r11.e
            if (r0 != 0) goto L_0x006a
            android.support.v7.widget.aq r0 = new android.support.v7.widget.aq
            r0.<init>(r11, r7)
            r11.e = r0
        L_0x006a:
            java.lang.Runnable r0 = r11.e
            int r2 = r11.b
            long r2 = (long) r2
            r1.postDelayed(r0, r2)
            java.lang.Runnable r0 = r11.f
            if (r0 != 0) goto L_0x007d
            android.support.v7.widget.ar r0 = new android.support.v7.widget.ar
            r0.<init>(r11, r7)
            r11.f = r0
        L_0x007d:
            java.lang.Runnable r0 = r11.f
            int r2 = r11.c
            long r2 = (long) r2
            r1.postDelayed(r0, r2)
            goto L_0x0036
        L_0x0086:
            int r0 = r11.i
            int r0 = r13.findPointerIndex(r0)
            if (r0 < 0) goto L_0x0036
            float r2 = r13.getX(r0)
            float r0 = r13.getY(r0)
            float r3 = r11.a
            float r4 = -r3
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 < 0) goto L_0x00d0
            float r4 = -r3
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 < 0) goto L_0x00d0
            int r4 = r1.getRight()
            int r6 = r1.getLeft()
            int r4 = r4 - r6
            float r4 = (float) r4
            float r4 = r4 + r3
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x00d0
            int r2 = r1.getBottom()
            int r4 = r1.getTop()
            int r2 = r2 - r4
            float r2 = (float) r2
            float r2 = r2 + r3
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x00d0
            r0 = r8
        L_0x00c1:
            if (r0 != 0) goto L_0x0036
            r11.d()
            android.view.ViewParent r0 = r1.getParent()
            r0.requestDisallowInterceptTouchEvent(r8)
            r0 = r8
            goto L_0x0037
        L_0x00d0:
            r0 = r7
            goto L_0x00c1
        L_0x00d2:
            r11.d()
            goto L_0x0036
        L_0x00d7:
            r9 = r7
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ap.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }
}
