package android.support.v7.widget;

final class bf implements dq {
    final /* synthetic */ RecyclerView a;

    bf(RecyclerView recyclerView) {
        this.a = recyclerView;
    }

    public final void a(cf cfVar) {
        this.a.q.a(cfVar.a, this.a.a);
    }

    public final void a(cf cfVar, bo boVar, bo boVar2) {
        this.a.a.b(cfVar);
        RecyclerView.a(this.a, cfVar, boVar, boVar2);
    }

    public final void b(cf cfVar, bo boVar, bo boVar2) {
        RecyclerView.b(this.a, cfVar, boVar, boVar2);
    }

    public final void c(cf cfVar, bo boVar, bo boVar2) {
        cfVar.a(false);
        if (this.a.H) {
            if (this.a.e.a(cfVar, cfVar, boVar, boVar2)) {
                this.a.y();
            }
        } else if (this.a.e.c(cfVar, boVar, boVar2)) {
            this.a.y();
        }
    }
}
