package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;

final class ag extends al {
    final /* synthetic */ LinearLayoutManager a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ag(LinearLayoutManager linearLayoutManager, Context context) {
        super(context);
        this.a = linearLayoutManager;
    }

    public final PointF a(int i) {
        int i2 = 1;
        boolean z = false;
        LinearLayoutManager linearLayoutManager = this.a;
        if (linearLayoutManager.o() == 0) {
            return null;
        }
        if (i < LinearLayoutManager.e(linearLayoutManager.d(0))) {
            z = true;
        }
        if (z != linearLayoutManager.l) {
            i2 = -1;
        }
        return linearLayoutManager.j == 0 ? new PointF((float) i2, 0.0f) : new PointF(0.0f, (float) i2);
    }
}
