package android.support.v7.widget.a;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v4.view.ay;
import android.support.v4.view.bx;
import android.support.v4.view.q;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.bl;
import android.support.v7.widget.bq;
import android.support.v7.widget.br;
import android.support.v7.widget.bs;
import android.support.v7.widget.bt;
import android.support.v7.widget.cf;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.core.view.MotionEventCompat;
import java.util.ArrayList;
import java.util.List;

public final class a extends bq implements bs {
    private long A;
    final List a = new ArrayList();
    cf b = null;
    float c;
    float d;
    float e;
    float f;
    float g;
    float h;
    int i = -1;
    g j;
    int k = 0;
    int l;
    List m = new ArrayList();
    private final float[] n = new float[2];
    private int o;
    /* access modifiers changed from: private */
    public RecyclerView p;
    /* access modifiers changed from: private */
    public final Runnable q = new b(this);
    /* access modifiers changed from: private */
    public VelocityTracker r;
    private List s;
    private List t;
    private bl u = null;
    /* access modifiers changed from: private */
    public View v = null;
    /* access modifiers changed from: private */
    public int w = -1;
    /* access modifiers changed from: private */
    public q x;
    private final bt y = new c(this);
    private Rect z;

    public a(g gVar) {
        this.j = gVar;
    }

    private int a(int i2) {
        int i3 = 8;
        if ((i2 & 12) != 0) {
            int i4 = this.e > 0.0f ? 8 : 4;
            if (this.r != null && this.i >= 0) {
                float a2 = android.support.v4.view.bt.a(this.r, this.i);
                if (a2 <= 0.0f) {
                    i3 = 4;
                }
                if ((i3 & i2) != 0 && i4 == i3 && Math.abs(a2) >= ((float) this.p.e())) {
                    return i3;
                }
            }
            float width = ((float) this.p.getWidth()) * g.f();
            if ((i2 & i4) == 0 || Math.abs(this.e) <= width) {
                return 0;
            }
            return i4;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public int a(cf cfVar, boolean z2) {
        for (int size = this.m.size() - 1; size >= 0; size--) {
            k kVar = (k) this.m.get(size);
            if (kVar.h == cfVar) {
                kVar.m |= z2;
                if (!kVar.c) {
                    kVar.c();
                }
                this.m.remove(size);
                return kVar.b;
            }
        }
        return 0;
    }

    static /* synthetic */ k a(a aVar, MotionEvent motionEvent) {
        if (!aVar.m.isEmpty()) {
            View a2 = aVar.a(motionEvent);
            for (int size = aVar.m.size() - 1; size >= 0; size--) {
                k kVar = (k) aVar.m.get(size);
                if (kVar.h.a == a2) {
                    return kVar;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public View a(MotionEvent motionEvent) {
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        if (this.b != null) {
            View view = this.b.a;
            if (a(view, x2, y2, this.g + this.e, this.h + this.f)) {
                return view;
            }
        }
        for (int size = this.m.size() - 1; size >= 0; size--) {
            k kVar = (k) this.m.get(size);
            View view2 = kVar.h.a;
            if (a(view2, x2, y2, kVar.k, kVar.l)) {
                return view2;
            }
        }
        return this.p.a(x2, y2);
    }

    private void a() {
        if (this.r != null) {
            this.r.recycle();
            this.r = null;
        }
    }

    static /* synthetic */ void a(a aVar, cf cfVar) {
        if (!aVar.p.isLayoutRequested() && aVar.k == 2) {
            g.g();
            int i2 = (int) (aVar.g + aVar.e);
            int i3 = (int) (aVar.h + aVar.f);
            if (((float) Math.abs(i3 - cfVar.a.getTop())) >= ((float) cfVar.a.getHeight()) * 0.5f || ((float) Math.abs(i2 - cfVar.a.getLeft())) >= ((float) cfVar.a.getWidth()) * 0.5f) {
                if (aVar.s == null) {
                    aVar.s = new ArrayList();
                    aVar.t = new ArrayList();
                } else {
                    aVar.s.clear();
                    aVar.t.clear();
                }
                g.e();
                int round = Math.round(aVar.g + aVar.e) + 0;
                int round2 = Math.round(aVar.h + aVar.f) + 0;
                int width = cfVar.a.getWidth() + round + 0;
                int height = cfVar.a.getHeight() + round2 + 0;
                int i4 = (round + width) / 2;
                int i5 = (round2 + height) / 2;
                br b2 = aVar.p.b();
                int o2 = b2.o();
                for (int i6 = 0; i6 < o2; i6++) {
                    View d2 = b2.d(i6);
                    if (d2 != cfVar.a && d2.getBottom() >= round2 && d2.getTop() <= height && d2.getRight() >= round && d2.getLeft() <= width) {
                        cf a2 = aVar.p.a(d2);
                        g.b();
                        int abs = Math.abs(i4 - ((d2.getLeft() + d2.getRight()) / 2));
                        int abs2 = Math.abs(i5 - ((d2.getBottom() + d2.getTop()) / 2));
                        int i7 = (abs * abs) + (abs2 * abs2);
                        int size = aVar.s.size();
                        int i8 = 0;
                        int i9 = 0;
                        while (true) {
                            int i10 = i8;
                            if (i10 >= size || i7 <= ((Integer) aVar.t.get(i10)).intValue()) {
                                aVar.s.add(i9, a2);
                                aVar.t.add(i9, Integer.valueOf(i7));
                            } else {
                                i9++;
                                i8 = i10 + 1;
                            }
                        }
                        aVar.s.add(i9, a2);
                        aVar.t.add(i9, Integer.valueOf(i7));
                    }
                }
                List list = aVar.s;
                if (list.size() != 0) {
                    cf a3 = g.a(cfVar, list, i2, i3);
                    if (a3 == null) {
                        aVar.s.clear();
                        aVar.t.clear();
                        return;
                    }
                    int d3 = a3.d();
                    cfVar.d();
                    aVar.j.a(cfVar, a3);
                    g.a(aVar.p, cfVar, a3, d3);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    static /* synthetic */ void a(a aVar, MotionEvent motionEvent, int i2, int i3) {
        float c2 = ay.c(motionEvent, i3);
        float d2 = ay.d(motionEvent, i3);
        aVar.e = c2 - aVar.c;
        aVar.f = d2 - aVar.d;
        if ((i2 & 4) == 0) {
            aVar.e = Math.max(0.0f, aVar.e);
        }
        if ((i2 & 8) == 0) {
            aVar.e = Math.min(0.0f, aVar.e);
        }
        if ((i2 & 1) == 0) {
            aVar.f = Math.max(0.0f, aVar.f);
        }
        if ((i2 & 2) == 0) {
            aVar.f = Math.min(0.0f, aVar.f);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.a.a.a(android.support.v7.widget.cf, boolean):int
     arg types: [android.support.v7.widget.cf, int]
     candidates:
      android.support.v7.widget.a.a.a(android.support.v7.widget.a.a, int):int
      android.support.v7.widget.a.a.a(android.support.v7.widget.a.a, android.view.MotionEvent):android.support.v7.widget.a.k
      android.support.v7.widget.a.a.a(android.support.v7.widget.a.a, android.support.v7.widget.cf):void
      android.support.v7.widget.a.a.a(android.support.v7.widget.a.a, android.view.View):void
      android.support.v7.widget.a.a.a(android.support.v7.widget.cf, int):void
      android.support.v7.widget.a.a.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView):void
      android.support.v7.widget.a.a.a(android.graphics.Rect, android.view.View):void
      android.support.v7.widget.bq.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView):void
      android.support.v7.widget.bq.a(android.graphics.Rect, android.view.View):void
      android.support.v7.widget.a.a.a(android.support.v7.widget.cf, boolean):int */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x012d, code lost:
        if (r9 <= 0) goto L_0x012f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0171  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.support.v7.widget.cf r13, int r14) {
        /*
            r12 = this;
            android.support.v7.widget.cf r0 = r12.b
            if (r13 != r0) goto L_0x0009
            int r0 = r12.k
            if (r14 != r0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            r0 = -9223372036854775808
            r12.A = r0
            int r4 = r12.k
            r0 = 1
            r12.a(r13, r0)
            r12.k = r14
            r0 = 2
            if (r14 != r0) goto L_0x0034
            android.view.View r0 = r13.a
            r12.v = r0
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 21
            if (r0 >= r1) goto L_0x0034
            android.support.v7.widget.bl r0 = r12.u
            if (r0 != 0) goto L_0x002d
            android.support.v7.widget.a.f r0 = new android.support.v7.widget.a.f
            r0.<init>(r12)
            r12.u = r0
        L_0x002d:
            android.support.v7.widget.RecyclerView r0 = r12.p
            android.support.v7.widget.bl r1 = r12.u
            r0.a(r1)
        L_0x0034:
            r0 = 1
            int r1 = r14 * 8
            int r1 = r1 + 8
            int r0 = r0 << r1
            int r11 = r0 + -1
            r0 = 0
            android.support.v7.widget.cf r1 = r12.b
            if (r1 == 0) goto L_0x00cc
            android.support.v7.widget.cf r2 = r12.b
            android.view.View r1 = r2.a
            android.view.ViewParent r1 = r1.getParent()
            if (r1 == 0) goto L_0x0179
            r0 = 2
            if (r4 == r0) goto L_0x012f
            int r0 = r12.k
            r1 = 2
            if (r0 == r1) goto L_0x012f
            android.support.v7.widget.a.g r0 = r12.j
            int r0 = r0.a()
            android.support.v7.widget.RecyclerView r1 = r12.p
            int r1 = android.support.v4.view.bx.h(r1)
            int r1 = android.support.v7.widget.a.g.c(r0, r1)
            r3 = 65280(0xff00, float:9.1477E-41)
            r1 = r1 & r3
            int r1 = r1 >> 8
            if (r1 == 0) goto L_0x012f
            r3 = 65280(0xff00, float:9.1477E-41)
            r0 = r0 & r3
            int r0 = r0 >> 8
            float r3 = r12.e
            float r3 = java.lang.Math.abs(r3)
            float r5 = r12.f
            float r5 = java.lang.Math.abs(r5)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x0132
            int r9 = r12.a(r1)
            if (r9 <= 0) goto L_0x0129
            r0 = r0 & r9
            if (r0 != 0) goto L_0x0094
            android.support.v7.widget.RecyclerView r0 = r12.p
            int r0 = android.support.v4.view.bx.h(r0)
            int r9 = android.support.v7.widget.a.g.a(r9, r0)
        L_0x0094:
            r12.a()
            switch(r9) {
                case 1: goto L_0x015f;
                case 2: goto L_0x015f;
                case 4: goto L_0x014d;
                case 8: goto L_0x014d;
                case 16: goto L_0x014d;
                case 32: goto L_0x014d;
                default: goto L_0x009a;
            }
        L_0x009a:
            r7 = 0
            r8 = 0
        L_0x009c:
            r0 = 2
            if (r4 != r0) goto L_0x0171
            r3 = 8
        L_0x00a1:
            float[] r0 = r12.n
            r12.a(r0)
            float[] r0 = r12.n
            r1 = 0
            r5 = r0[r1]
            float[] r0 = r12.n
            r1 = 1
            r6 = r0[r1]
            android.support.v7.widget.a.d r0 = new android.support.v7.widget.a.d
            r1 = r12
            r10 = r2
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10)
            android.support.v7.widget.RecyclerView r1 = r12.p
            long r2 = android.support.v7.widget.a.g.a(r1, r3)
            r0.a(r2)
            java.util.List r1 = r12.m
            r1.add(r0)
            r0.b()
            r0 = 1
        L_0x00c9:
            r1 = 0
            r12.b = r1
        L_0x00cc:
            r1 = r0
            if (r13 == 0) goto L_0x00fe
            android.support.v7.widget.a.g r0 = r12.j
            android.support.v7.widget.RecyclerView r2 = r12.p
            int r0 = r0.a(r2)
            r0 = r0 & r11
            int r2 = r12.k
            int r2 = r2 * 8
            int r0 = r0 >> r2
            r12.l = r0
            android.view.View r0 = r13.a
            int r0 = r0.getLeft()
            float r0 = (float) r0
            r12.g = r0
            android.view.View r0 = r13.a
            int r0 = r0.getTop()
            float r0 = (float) r0
            r12.h = r0
            r12.b = r13
            r0 = 2
            if (r14 != r0) goto L_0x00fe
            android.support.v7.widget.cf r0 = r12.b
            android.view.View r0 = r0.a
            r2 = 0
            r0.performHapticFeedback(r2)
        L_0x00fe:
            android.support.v7.widget.RecyclerView r0 = r12.p
            android.view.ViewParent r2 = r0.getParent()
            if (r2 == 0) goto L_0x010e
            android.support.v7.widget.cf r0 = r12.b
            if (r0 == 0) goto L_0x0187
            r0 = 1
        L_0x010b:
            r2.requestDisallowInterceptTouchEvent(r0)
        L_0x010e:
            if (r1 != 0) goto L_0x0119
            android.support.v7.widget.RecyclerView r0 = r12.p
            android.support.v7.widget.br r0 = r0.b()
            r0.x()
        L_0x0119:
            android.support.v7.widget.a.g r0 = r12.j
            android.support.v7.widget.cf r1 = r12.b
            int r2 = r12.k
            r0.a(r1, r2)
            android.support.v7.widget.RecyclerView r0 = r12.p
            r0.invalidate()
            goto L_0x0008
        L_0x0129:
            int r9 = r12.b(r1)
            if (r9 > 0) goto L_0x0094
        L_0x012f:
            r9 = 0
            goto L_0x0094
        L_0x0132:
            int r9 = r12.b(r1)
            if (r9 > 0) goto L_0x0094
            int r9 = r12.a(r1)
            if (r9 <= 0) goto L_0x012f
            r0 = r0 & r9
            if (r0 != 0) goto L_0x0094
            android.support.v7.widget.RecyclerView r0 = r12.p
            int r0 = android.support.v4.view.bx.h(r0)
            int r9 = android.support.v7.widget.a.g.a(r9, r0)
            goto L_0x0094
        L_0x014d:
            r8 = 0
            float r0 = r12.e
            float r0 = java.lang.Math.signum(r0)
            android.support.v7.widget.RecyclerView r1 = r12.p
            int r1 = r1.getWidth()
            float r1 = (float) r1
            float r7 = r0 * r1
            goto L_0x009c
        L_0x015f:
            r7 = 0
            float r0 = r12.f
            float r0 = java.lang.Math.signum(r0)
            android.support.v7.widget.RecyclerView r1 = r12.p
            int r1 = r1.getHeight()
            float r1 = (float) r1
            float r8 = r0 * r1
            goto L_0x009c
        L_0x0171:
            if (r9 <= 0) goto L_0x0176
            r3 = 2
            goto L_0x00a1
        L_0x0176:
            r3 = 4
            goto L_0x00a1
        L_0x0179:
            android.view.View r1 = r2.a
            r12.b(r1)
            android.support.v7.widget.a.g r1 = r12.j
            android.support.v7.widget.RecyclerView r3 = r12.p
            r1.a(r3, r2)
            goto L_0x00c9
        L_0x0187:
            r0 = 0
            goto L_0x010b
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.a.a.a(android.support.v7.widget.cf, int):void");
    }

    private void a(float[] fArr) {
        if ((this.l & 12) != 0) {
            fArr[0] = (this.g + this.e) - ((float) this.b.a.getLeft());
        } else {
            fArr[0] = bx.p(this.b.a);
        }
        if ((this.l & 3) != 0) {
            fArr[1] = (this.h + this.f) - ((float) this.b.a.getTop());
        } else {
            fArr[1] = bx.q(this.b.a);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x004e, code lost:
        if (r3 < 0) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0070, code lost:
        if (r7 < 0) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00de, code lost:
        if (r3 > 0) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0106, code lost:
        if (r7 > 0) goto L_0x0072;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ boolean a(android.support.v7.widget.a.a r14) {
        /*
            r12 = -9223372036854775808
            r6 = 0
            r8 = 0
            android.support.v7.widget.cf r0 = r14.b
            if (r0 == 0) goto L_0x010b
            long r10 = java.lang.System.currentTimeMillis()
            long r0 = r14.A
            int r0 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
            if (r0 != 0) goto L_0x00b5
            r4 = 0
        L_0x0014:
            android.support.v7.widget.RecyclerView r0 = r14.p
            android.support.v7.widget.br r0 = r0.b()
            android.graphics.Rect r1 = r14.z
            if (r1 != 0) goto L_0x0025
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            r14.z = r1
        L_0x0025:
            android.support.v7.widget.cf r1 = r14.b
            android.view.View r1 = r1.a
            android.graphics.Rect r2 = r14.z
            r0.a(r1, r2)
            boolean r1 = r0.e()
            if (r1 == 0) goto L_0x00e0
            float r1 = r14.g
            float r2 = r14.e
            float r1 = r1 + r2
            int r1 = (int) r1
            android.graphics.Rect r2 = r14.z
            int r2 = r2.left
            int r2 = r1 - r2
            android.support.v7.widget.RecyclerView r3 = r14.p
            int r3 = r3.getPaddingLeft()
            int r3 = r2 - r3
            float r2 = r14.e
            int r2 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r2 >= 0) goto L_0x00bb
            if (r3 >= 0) goto L_0x00bb
        L_0x0050:
            boolean r0 = r0.f()
            if (r0 == 0) goto L_0x0108
            float r0 = r14.h
            float r1 = r14.f
            float r0 = r0 + r1
            int r0 = (int) r0
            android.graphics.Rect r1 = r14.z
            int r1 = r1.top
            int r1 = r0 - r1
            android.support.v7.widget.RecyclerView r2 = r14.p
            int r2 = r2.getPaddingTop()
            int r7 = r1 - r2
            float r1 = r14.f
            int r1 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r1 >= 0) goto L_0x00e3
            if (r7 >= 0) goto L_0x00e3
        L_0x0072:
            if (r3 == 0) goto L_0x0110
            android.support.v7.widget.a.g r0 = r14.j
            android.support.v7.widget.RecyclerView r1 = r14.p
            android.support.v7.widget.cf r2 = r14.b
            android.view.View r2 = r2.a
            int r2 = r2.getWidth()
            android.support.v7.widget.RecyclerView r8 = r14.p
            r8.getWidth()
            int r3 = r0.a(r1, r2, r3, r4)
            r8 = r3
        L_0x008a:
            if (r7 == 0) goto L_0x010e
            android.support.v7.widget.a.g r0 = r14.j
            android.support.v7.widget.RecyclerView r1 = r14.p
            android.support.v7.widget.cf r2 = r14.b
            android.view.View r2 = r2.a
            int r2 = r2.getHeight()
            android.support.v7.widget.RecyclerView r3 = r14.p
            r3.getHeight()
            r3 = r7
            int r0 = r0.a(r1, r2, r3, r4)
        L_0x00a2:
            if (r8 != 0) goto L_0x00a6
            if (r0 == 0) goto L_0x010b
        L_0x00a6:
            long r2 = r14.A
            int r1 = (r2 > r12 ? 1 : (r2 == r12 ? 0 : -1))
            if (r1 != 0) goto L_0x00ae
            r14.A = r10
        L_0x00ae:
            android.support.v7.widget.RecyclerView r1 = r14.p
            r1.scrollBy(r8, r0)
            r6 = 1
        L_0x00b4:
            return r6
        L_0x00b5:
            long r0 = r14.A
            long r4 = r10 - r0
            goto L_0x0014
        L_0x00bb:
            float r2 = r14.e
            int r2 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r2 <= 0) goto L_0x00e0
            android.support.v7.widget.cf r2 = r14.b
            android.view.View r2 = r2.a
            int r2 = r2.getWidth()
            int r1 = r1 + r2
            android.graphics.Rect r2 = r14.z
            int r2 = r2.right
            int r1 = r1 + r2
            android.support.v7.widget.RecyclerView r2 = r14.p
            int r2 = r2.getWidth()
            android.support.v7.widget.RecyclerView r3 = r14.p
            int r3 = r3.getPaddingRight()
            int r2 = r2 - r3
            int r3 = r1 - r2
            if (r3 > 0) goto L_0x0050
        L_0x00e0:
            r3 = r6
            goto L_0x0050
        L_0x00e3:
            float r1 = r14.f
            int r1 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r1 <= 0) goto L_0x0108
            android.support.v7.widget.cf r1 = r14.b
            android.view.View r1 = r1.a
            int r1 = r1.getHeight()
            int r0 = r0 + r1
            android.graphics.Rect r1 = r14.z
            int r1 = r1.bottom
            int r0 = r0 + r1
            android.support.v7.widget.RecyclerView r1 = r14.p
            int r1 = r1.getHeight()
            android.support.v7.widget.RecyclerView r2 = r14.p
            int r2 = r2.getPaddingBottom()
            int r1 = r1 - r2
            int r7 = r0 - r1
            if (r7 > 0) goto L_0x0072
        L_0x0108:
            r7 = r6
            goto L_0x0072
        L_0x010b:
            r14.A = r12
            goto L_0x00b4
        L_0x010e:
            r0 = r7
            goto L_0x00a2
        L_0x0110:
            r8 = r3
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.a.a.a(android.support.v7.widget.a.a):boolean");
    }

    static /* synthetic */ boolean a(a aVar, int i2, MotionEvent motionEvent, int i3) {
        View a2;
        int a3;
        cf cfVar = null;
        if (aVar.b != null || i2 != 2 || aVar.k == 2 || !aVar.j.d() || aVar.p.c() == 1) {
            return false;
        }
        br b2 = aVar.p.b();
        if (aVar.i != -1) {
            int a4 = ay.a(motionEvent, aVar.i);
            float abs = Math.abs(ay.c(motionEvent, a4) - aVar.c);
            float abs2 = Math.abs(ay.d(motionEvent, a4) - aVar.d);
            if ((abs >= ((float) aVar.o) || abs2 >= ((float) aVar.o)) && ((abs <= abs2 || !b2.e()) && ((abs2 <= abs || !b2.f()) && (a2 = aVar.a(motionEvent)) != null))) {
                cfVar = aVar.p.a(a2);
            }
        }
        if (cfVar == null || (a3 = (aVar.j.a(aVar.p) & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8) == 0) {
            return false;
        }
        float c2 = ay.c(motionEvent, i3);
        float d2 = ay.d(motionEvent, i3);
        float f2 = c2 - aVar.c;
        float f3 = d2 - aVar.d;
        float abs3 = Math.abs(f2);
        float abs4 = Math.abs(f3);
        if (abs3 < ((float) aVar.o) && abs4 < ((float) aVar.o)) {
            return false;
        }
        if (abs3 > abs4) {
            if (f2 < 0.0f && (a3 & 4) == 0) {
                return false;
            }
            if (f2 > 0.0f && (a3 & 8) == 0) {
                return false;
            }
        } else if (f3 < 0.0f && (a3 & 1) == 0) {
            return false;
        } else {
            if (f3 > 0.0f && (a3 & 2) == 0) {
                return false;
            }
        }
        aVar.f = 0.0f;
        aVar.e = 0.0f;
        aVar.i = ay.b(motionEvent, 0);
        aVar.a(cfVar, 1);
        return true;
    }

    private static boolean a(View view, float f2, float f3, float f4, float f5) {
        return f2 >= f4 && f2 <= ((float) view.getWidth()) + f4 && f3 >= f5 && f3 <= ((float) view.getHeight()) + f5;
    }

    private int b(int i2) {
        int i3 = 2;
        if ((i2 & 3) != 0) {
            int i4 = this.f > 0.0f ? 2 : 1;
            if (this.r != null && this.i >= 0) {
                float b2 = android.support.v4.view.bt.b(this.r, this.i);
                if (b2 <= 0.0f) {
                    i3 = 1;
                }
                if ((i3 & i2) != 0 && i3 == i4 && Math.abs(b2) >= ((float) this.p.e())) {
                    return i3;
                }
            }
            float height = ((float) this.p.getHeight()) * g.f();
            if ((i2 & i4) == 0 || Math.abs(this.f) <= height) {
                return 0;
            }
            return i4;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void b(View view) {
        if (view == this.v) {
            this.v = null;
            if (this.u != null) {
                this.p.a((bl) null);
            }
        }
    }

    static /* synthetic */ void e(a aVar) {
        if (aVar.r != null) {
            aVar.r.recycle();
        }
        aVar.r = VelocityTracker.obtain();
    }

    public final void a(Canvas canvas, RecyclerView recyclerView) {
        float f2;
        float f3 = 0.0f;
        this.w = -1;
        if (this.b != null) {
            a(this.n);
            f2 = this.n[0];
            f3 = this.n[1];
        } else {
            f2 = 0.0f;
        }
        g.a(this.j, canvas, recyclerView, this.b, this.m, this.k, f2, f3);
    }

    public final void a(Rect rect, View view) {
        rect.setEmpty();
    }

    public final void a(RecyclerView recyclerView) {
        if (this.p != recyclerView) {
            if (this.p != null) {
                this.p.c(this);
                this.p.b(this.y);
                this.p.b((bs) this);
                for (int size = this.m.size() - 1; size >= 0; size--) {
                    this.j.a(this.p, ((k) this.m.get(0)).h);
                }
                this.m.clear();
                this.v = null;
                this.w = -1;
                a();
            }
            this.p = recyclerView;
            if (this.p != null) {
                this.o = ViewConfiguration.get(this.p.getContext()).getScaledTouchSlop();
                this.p.b((bq) this);
                this.p.a(this.y);
                this.p.a((bs) this);
                if (this.x == null) {
                    this.x = new q(this.p.getContext(), new j(this, (byte) 0));
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.a.a.a(android.support.v7.widget.cf, boolean):int
     arg types: [android.support.v7.widget.cf, int]
     candidates:
      android.support.v7.widget.a.a.a(android.support.v7.widget.a.a, int):int
      android.support.v7.widget.a.a.a(android.support.v7.widget.a.a, android.view.MotionEvent):android.support.v7.widget.a.k
      android.support.v7.widget.a.a.a(android.support.v7.widget.a.a, android.support.v7.widget.cf):void
      android.support.v7.widget.a.a.a(android.support.v7.widget.a.a, android.view.View):void
      android.support.v7.widget.a.a.a(android.support.v7.widget.cf, int):void
      android.support.v7.widget.a.a.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView):void
      android.support.v7.widget.a.a.a(android.graphics.Rect, android.view.View):void
      android.support.v7.widget.bq.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView):void
      android.support.v7.widget.bq.a(android.graphics.Rect, android.view.View):void
      android.support.v7.widget.a.a.a(android.support.v7.widget.cf, boolean):int */
    public final void a(View view) {
        b(view);
        cf a2 = this.p.a(view);
        if (a2 != null) {
            if (this.b == null || a2 != this.b) {
                a(a2, false);
                if (this.a.remove(a2.a)) {
                    this.j.a(this.p, a2);
                    return;
                }
                return;
            }
            a((cf) null, 0);
        }
    }

    public final void b(Canvas canvas, RecyclerView recyclerView) {
        float f2;
        float f3 = 0.0f;
        if (this.b != null) {
            a(this.n);
            f2 = this.n[0];
            f3 = this.n[1];
        } else {
            f2 = 0.0f;
        }
        g.a(canvas, recyclerView, this.b, this.m, this.k, f2, f3);
    }
}
