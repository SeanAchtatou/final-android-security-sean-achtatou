package android.support.v7.widget;

import android.widget.AbsListView;

final class au implements AbsListView.OnScrollListener {
    final /* synthetic */ ListPopupWindow a;

    private au(ListPopupWindow listPopupWindow) {
        this.a = listPopupWindow;
    }

    /* synthetic */ au(ListPopupWindow listPopupWindow, byte b) {
        this(listPopupWindow);
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 1 && !this.a.i() && this.a.d.getContentView() != null) {
            this.a.A.removeCallbacks(this.a.v);
            this.a.v.run();
        }
    }
}
