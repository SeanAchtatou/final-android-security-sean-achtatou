package android.support.v7.widget.a;

import android.support.v4.view.ay;
import android.support.v7.widget.cf;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

final class j extends GestureDetector.SimpleOnGestureListener {
    final /* synthetic */ a a;

    private j(a aVar) {
        this.a = aVar;
    }

    /* synthetic */ j(a aVar, byte b) {
        this(aVar);
    }

    public final boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    public final void onLongPress(MotionEvent motionEvent) {
        cf a2;
        View b = this.a.a(motionEvent);
        if (b != null && (a2 = this.a.p.a(b)) != null && g.a(this.a.j, this.a.p) && ay.b(motionEvent, 0) == this.a.i) {
            int a3 = ay.a(motionEvent, this.a.i);
            float c = ay.c(motionEvent, a3);
            float d = ay.d(motionEvent, a3);
            this.a.c = c;
            this.a.d = d;
            a aVar = this.a;
            this.a.f = 0.0f;
            aVar.e = 0.0f;
            this.a.j.c();
            this.a.a(a2, 2);
        }
    }
}
