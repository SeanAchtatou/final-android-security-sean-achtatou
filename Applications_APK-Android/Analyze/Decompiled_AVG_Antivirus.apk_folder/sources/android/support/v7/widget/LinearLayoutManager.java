package android.support.v7.widget;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.a.a;
import android.support.v4.view.a.af;
import android.support.v4.view.bx;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.a.m;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import androidx.core.view.MotionEventCompat;

public class LinearLayoutManager extends br implements m {
    private aj a;
    private boolean b;
    private boolean c;
    private boolean d;
    private boolean e;
    private boolean f;
    int j;
    az k;
    boolean l;
    int m;
    int n;
    SavedState o;
    final ah p;

    public class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new ak();
        int a;
        int b;
        boolean c;

        public SavedState() {
        }

        SavedState(Parcel parcel) {
            boolean z = true;
            this.a = parcel.readInt();
            this.b = parcel.readInt();
            this.c = parcel.readInt() != 1 ? false : z;
        }

        public SavedState(SavedState savedState) {
            this.a = savedState.a;
            this.b = savedState.b;
            this.c = savedState.c;
        }

        /* access modifiers changed from: package-private */
        public final boolean a() {
            return this.a >= 0;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeInt(this.b);
            parcel.writeInt(this.c ? 1 : 0);
        }
    }

    public LinearLayoutManager() {
        this(1);
    }

    public LinearLayoutManager(int i) {
        this.c = false;
        this.l = false;
        this.d = false;
        this.e = true;
        this.m = -1;
        this.n = Integer.MIN_VALUE;
        this.o = null;
        this.p = new ah(this);
        a(i);
        a((String) null);
        if (this.c) {
            this.c = false;
            k();
        }
    }

    private View A() {
        return d(this.l ? 0 : o() - 1);
    }

    private int a(int i, bw bwVar, cc ccVar, boolean z) {
        int d2;
        int d3 = this.k.d() - i;
        if (d3 <= 0) {
            return 0;
        }
        int i2 = -d(-d3, bwVar, ccVar);
        int i3 = i + i2;
        if (!z || (d2 = this.k.d() - i3) <= 0) {
            return i2;
        }
        this.k.a(d2);
        return i2 + d2;
    }

    private int a(bw bwVar, aj ajVar, cc ccVar, boolean z) {
        int i = ajVar.c;
        if (ajVar.g != Integer.MIN_VALUE) {
            if (ajVar.c < 0) {
                ajVar.g += ajVar.c;
            }
            a(bwVar, ajVar);
        }
        int i2 = ajVar.c + ajVar.h;
        ai aiVar = new ai();
        while (i2 > 0 && ajVar.a(ccVar)) {
            aiVar.a = 0;
            aiVar.b = false;
            aiVar.c = false;
            aiVar.d = false;
            a(bwVar, ccVar, ajVar, aiVar);
            if (aiVar.b) {
                break;
            }
            ajVar.b += aiVar.a * ajVar.f;
            if (!aiVar.c || this.a.k != null || !ccVar.a()) {
                ajVar.c -= aiVar.a;
                i2 -= aiVar.a;
            }
            if (ajVar.g != Integer.MIN_VALUE) {
                ajVar.g += aiVar.a;
                if (ajVar.c < 0) {
                    ajVar.g += ajVar.c;
                }
                a(bwVar, ajVar);
            }
            if (z && aiVar.d) {
                break;
            }
        }
        return i - ajVar.c;
    }

    private View a(int i, int i2, boolean z, boolean z2) {
        i();
        int c2 = this.k.c();
        int d2 = this.k.d();
        int i3 = i2 > i ? 1 : -1;
        View view = null;
        while (i != i2) {
            View d3 = d(i);
            int a2 = this.k.a(d3);
            int b2 = this.k.b(d3);
            if (a2 < d2 && b2 > c2) {
                if (!z) {
                    return d3;
                }
                if (a2 >= c2 && b2 <= d2) {
                    return d3;
                }
                if (z2 && view == null) {
                    i += i3;
                    view = d3;
                }
            }
            d3 = view;
            i += i3;
            view = d3;
        }
        return view;
    }

    private void a(int i, int i2, boolean z, cc ccVar) {
        int c2;
        int i3 = -1;
        int i4 = 1;
        this.a.h = g(ccVar);
        this.a.f = i;
        if (i == 1) {
            this.a.h += this.k.g();
            View A = A();
            aj ajVar = this.a;
            if (!this.l) {
                i3 = 1;
            }
            ajVar.e = i3;
            this.a.d = e(A) + this.a.e;
            this.a.b = this.k.b(A);
            c2 = this.k.b(A) - this.k.d();
        } else {
            View z2 = z();
            this.a.h += this.k.c();
            aj ajVar2 = this.a;
            if (!this.l) {
                i4 = -1;
            }
            ajVar2.e = i4;
            this.a.d = e(z2) + this.a.e;
            this.a.b = this.k.a(z2);
            c2 = (-this.k.a(z2)) + this.k.c();
        }
        this.a.c = i2;
        if (z) {
            this.a.c -= c2;
        }
        this.a.g = c2;
    }

    private void a(ah ahVar) {
        g(ahVar.a, ahVar.b);
    }

    private void a(bw bwVar, int i, int i2) {
        if (i != i2) {
            if (i2 > i) {
                for (int i3 = i2 - 1; i3 >= i; i3--) {
                    a(i3, bwVar);
                }
                return;
            }
            while (i > i2) {
                a(i, bwVar);
                i--;
            }
        }
    }

    private void a(bw bwVar, aj ajVar) {
        if (ajVar.a) {
            if (ajVar.f == -1) {
                int i = ajVar.g;
                int o2 = o();
                if (i >= 0) {
                    int e2 = this.k.e() - i;
                    if (this.l) {
                        for (int i2 = 0; i2 < o2; i2++) {
                            if (this.k.a(d(i2)) < e2) {
                                a(bwVar, 0, i2);
                                return;
                            }
                        }
                        return;
                    }
                    for (int i3 = o2 - 1; i3 >= 0; i3--) {
                        if (this.k.a(d(i3)) < e2) {
                            a(bwVar, o2 - 1, i3);
                            return;
                        }
                    }
                    return;
                }
                return;
            }
            int i4 = ajVar.g;
            if (i4 >= 0) {
                int o3 = o();
                if (this.l) {
                    for (int i5 = o3 - 1; i5 >= 0; i5--) {
                        if (this.k.b(d(i5)) > i4) {
                            a(bwVar, o3 - 1, i5);
                            return;
                        }
                    }
                    return;
                }
                for (int i6 = 0; i6 < o3; i6++) {
                    if (this.k.b(d(i6)) > i4) {
                        a(bwVar, 0, i6);
                        return;
                    }
                }
            }
        }
    }

    private int b(int i, bw bwVar, cc ccVar, boolean z) {
        int c2;
        int c3 = i - this.k.c();
        if (c3 <= 0) {
            return 0;
        }
        int i2 = -d(c3, bwVar, ccVar);
        int i3 = i + i2;
        if (!z || (c2 = i3 - this.k.c()) <= 0) {
            return i2;
        }
        this.k.a(-c2);
        return i2 - c2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
     arg types: [int, int, boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View */
    private View b(boolean z) {
        return this.l ? a(o() - 1, -1, z, true) : a(0, o(), z, true);
    }

    private void b(ah ahVar) {
        h(ahVar.a, ahVar.b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
     arg types: [int, int, boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View */
    private View c(boolean z) {
        return this.l ? a(0, o(), z, true) : a(o() - 1, -1, z, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
     arg types: [int, int, int, android.support.v7.widget.cc]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
     arg types: [android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int */
    private int d(int i, bw bwVar, cc ccVar) {
        if (o() == 0 || i == 0) {
            return 0;
        }
        this.a.a = true;
        i();
        int i2 = i > 0 ? 1 : -1;
        int abs = Math.abs(i);
        a(i2, abs, true, ccVar);
        int a2 = this.a.g + a(bwVar, this.a, ccVar, false);
        if (a2 < 0) {
            return 0;
        }
        if (abs > a2) {
            i = i2 * a2;
        }
        this.k.a(-i);
        this.a.j = i;
        return i;
    }

    private View d(bw bwVar, cc ccVar) {
        return this.l ? f(bwVar, ccVar) : g(bwVar, ccVar);
    }

    private View e(bw bwVar, cc ccVar) {
        return this.l ? g(bwVar, ccVar) : f(bwVar, ccVar);
    }

    private View f(bw bwVar, cc ccVar) {
        return a(bwVar, ccVar, 0, o(), ccVar.e());
    }

    private int g(cc ccVar) {
        if (ccVar.d()) {
            return this.k.f();
        }
        return 0;
    }

    private View g(bw bwVar, cc ccVar) {
        return a(bwVar, ccVar, o() - 1, -1, ccVar.e());
    }

    private void g(int i, int i2) {
        this.a.c = this.k.d() - i2;
        this.a.e = this.l ? -1 : 1;
        this.a.d = i;
        this.a.f = 1;
        this.a.b = i2;
        this.a.g = Integer.MIN_VALUE;
    }

    private int h(cc ccVar) {
        boolean z = true;
        if (o() == 0) {
            return 0;
        }
        i();
        az azVar = this.k;
        View b2 = b(!this.e);
        if (this.e) {
            z = false;
        }
        return ci.a(ccVar, azVar, b2, c(z), this, this.e, this.l);
    }

    private void h(int i, int i2) {
        this.a.c = i2 - this.k.c();
        this.a.d = i;
        this.a.e = this.l ? 1 : -1;
        this.a.f = -1;
        this.a.b = i2;
        this.a.g = Integer.MIN_VALUE;
    }

    private int i(cc ccVar) {
        boolean z = true;
        if (o() == 0) {
            return 0;
        }
        i();
        az azVar = this.k;
        View b2 = b(!this.e);
        if (this.e) {
            z = false;
        }
        return ci.a(ccVar, azVar, b2, c(z), this, this.e);
    }

    private int j(cc ccVar) {
        boolean z = true;
        if (o() == 0) {
            return 0;
        }
        i();
        az azVar = this.k;
        View b2 = b(!this.e);
        if (this.e) {
            z = false;
        }
        return ci.b(ccVar, azVar, b2, c(z), this, this.e);
    }

    private void y() {
        boolean z = true;
        if (this.j == 1 || !h()) {
            z = this.c;
        } else if (this.c) {
            z = false;
        }
        this.l = z;
    }

    private View z() {
        return d(this.l ? o() - 1 : 0);
    }

    public int a(int i, bw bwVar, cc ccVar) {
        if (this.j == 1) {
            return 0;
        }
        return d(i, bwVar, ccVar);
    }

    public final int a(cc ccVar) {
        return h(ccVar);
    }

    /* access modifiers changed from: package-private */
    public View a(bw bwVar, cc ccVar, int i, int i2, int i3) {
        View view;
        View view2 = null;
        i();
        int c2 = this.k.c();
        int d2 = this.k.d();
        int i4 = i2 > i ? 1 : -1;
        View view3 = null;
        while (i != i2) {
            View d3 = d(i);
            int e2 = e(d3);
            if (e2 >= 0 && e2 < i3) {
                if (((RecyclerView.LayoutParams) d3.getLayoutParams()).a.m()) {
                    if (view3 == null) {
                        view = view2;
                        i += i4;
                        view2 = view;
                        view3 = d3;
                    }
                } else if (this.k.a(d3) < d2 && this.k.b(d3) >= c2) {
                    return d3;
                } else {
                    if (view2 == null) {
                        view = d3;
                        d3 = view3;
                        i += i4;
                        view2 = view;
                        view3 = d3;
                    }
                }
            }
            view = view2;
            d3 = view3;
            i += i4;
            view2 = view;
            view3 = d3;
        }
        return view2 != null ? view2 : view3;
    }

    public void a(int i) {
        if (i == 0 || i == 1) {
            a((String) null);
            if (i != this.j) {
                this.j = i;
                this.k = null;
                k();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation:" + i);
    }

    public final void a(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.o = (SavedState) parcelable;
            k();
        }
    }

    public final void a(RecyclerView recyclerView, int i) {
        ag agVar = new ag(this, recyclerView.getContext());
        agVar.b(i);
        a(agVar);
    }

    public final void a(RecyclerView recyclerView, bw bwVar) {
        super.a(recyclerView, bwVar);
        if (this.f) {
            c(bwVar);
            bwVar.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(bw bwVar, cc ccVar, ah ahVar) {
    }

    /* access modifiers changed from: package-private */
    public void a(bw bwVar, cc ccVar, aj ajVar, ai aiVar) {
        int s;
        int d2;
        int i;
        int i2;
        int r;
        int d3;
        boolean z = false;
        View a2 = ajVar.a(bwVar);
        if (a2 == null) {
            aiVar.b = true;
            return;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) a2.getLayoutParams();
        if (ajVar.k == null) {
            boolean z2 = this.l;
            if (ajVar.f == -1) {
                z = true;
            }
            if (z2 == z) {
                c(a2);
            } else {
                d(a2);
            }
        } else {
            boolean z3 = this.l;
            if (ajVar.f == -1) {
                z = true;
            }
            if (z3 == z) {
                a(a2);
            } else {
                b(a2);
            }
        }
        RecyclerView.LayoutParams layoutParams2 = (RecyclerView.LayoutParams) a2.getLayoutParams();
        Rect e2 = this.r.e(a2);
        a2.measure(br.a(p(), e2.left + e2.right + 0 + r() + t() + layoutParams2.leftMargin + layoutParams2.rightMargin, layoutParams2.width, e()), br.a(q(), e2.bottom + e2.top + 0 + s() + u() + layoutParams2.topMargin + layoutParams2.bottomMargin, layoutParams2.height, f()));
        aiVar.a = this.k.c(a2);
        if (this.j == 1) {
            if (h()) {
                d3 = p() - t();
                r = d3 - this.k.d(a2);
            } else {
                r = r();
                d3 = this.k.d(a2) + r;
            }
            if (ajVar.f == -1) {
                int i3 = ajVar.b;
                s = ajVar.b - aiVar.a;
                i = r;
                i2 = d3;
                d2 = i3;
            } else {
                s = ajVar.b;
                i = r;
                i2 = d3;
                d2 = ajVar.b + aiVar.a;
            }
        } else {
            s = s();
            d2 = this.k.d(a2) + s;
            if (ajVar.f == -1) {
                i2 = ajVar.b;
                i = ajVar.b - aiVar.a;
            } else {
                i = ajVar.b;
                i2 = ajVar.b + aiVar.a;
            }
        }
        a(a2, i + layoutParams.leftMargin, s + layoutParams.topMargin, i2 - layoutParams.rightMargin, d2 - layoutParams.bottomMargin);
        if (layoutParams.a.m() || layoutParams.a.s()) {
            aiVar.c = true;
        }
        aiVar.d = a2.isFocusable();
    }

    public final void a(View view, View view2) {
        a("Cannot drop a view during a scroll or layout calculation");
        i();
        y();
        int e2 = e(view);
        int e3 = e(view2);
        boolean z = e2 < e3 ? true : true;
        if (this.l) {
            if (z) {
                e(e3, this.k.d() - (this.k.a(view2) + this.k.c(view)));
            } else {
                e(e3, this.k.d() - this.k.b(view2));
            }
        } else if (z) {
            e(e3, this.k.a(view2));
        } else {
            e(e3, this.k.b(view2) - this.k.c(view));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
     arg types: [int, int, int, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View */
    public final void a(AccessibilityEvent accessibilityEvent) {
        int i = -1;
        super.a(accessibilityEvent);
        if (o() > 0) {
            af a2 = a.a(accessibilityEvent);
            View a3 = a(0, o(), false, true);
            a2.b(a3 == null ? -1 : e(a3));
            View a4 = a(o() - 1, -1, false, true);
            if (a4 != null) {
                i = e(a4);
            }
            a2.c(i);
        }
    }

    public final void a(String str) {
        if (this.o == null) {
            super.a(str);
        }
    }

    public void a(boolean z) {
        a((String) null);
        if (this.d != z) {
            this.d = z;
            k();
        }
    }

    public int b(int i, bw bwVar, cc ccVar) {
        if (this.j == 0) {
            return 0;
        }
        return d(i, bwVar, ccVar);
    }

    public final int b(cc ccVar) {
        return h(ccVar);
    }

    public RecyclerView.LayoutParams b() {
        return new RecyclerView.LayoutParams();
    }

    public final View b(int i) {
        int o2 = o();
        if (o2 == 0) {
            return null;
        }
        int e2 = i - e(d(0));
        if (e2 >= 0 && e2 < o2) {
            View d2 = d(e2);
            if (e(d2) == i) {
                return d2;
            }
        }
        return super.b(i);
    }

    public final int c(cc ccVar) {
        return i(ccVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
     arg types: [int, int, int, android.support.v7.widget.cc]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
     arg types: [android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int */
    public final View c(int i, bw bwVar, cc ccVar) {
        int i2;
        y();
        if (o() == 0) {
            return null;
        }
        switch (i) {
            case 1:
                i2 = -1;
                break;
            case 2:
                i2 = 1;
                break;
            case 17:
                if (this.j != 0) {
                    i2 = Integer.MIN_VALUE;
                    break;
                } else {
                    i2 = -1;
                    break;
                }
            case MotionEventCompat.AXIS_GENERIC_2 /*33*/:
                if (this.j != 1) {
                    i2 = Integer.MIN_VALUE;
                    break;
                } else {
                    i2 = -1;
                    break;
                }
            case 66:
                if (this.j != 0) {
                    i2 = Integer.MIN_VALUE;
                    break;
                } else {
                    i2 = 1;
                    break;
                }
            case 130:
                if (this.j != 1) {
                    i2 = Integer.MIN_VALUE;
                    break;
                } else {
                    i2 = 1;
                    break;
                }
            default:
                i2 = Integer.MIN_VALUE;
                break;
        }
        if (i2 == Integer.MIN_VALUE) {
            return null;
        }
        i();
        View e2 = i2 == -1 ? e(bwVar, ccVar) : d(bwVar, ccVar);
        if (e2 == null) {
            return null;
        }
        i();
        a(i2, (int) (0.33f * ((float) this.k.f())), false, ccVar);
        this.a.g = Integer.MIN_VALUE;
        this.a.a = false;
        a(bwVar, this.a, ccVar, true);
        View z = i2 == -1 ? z() : A();
        if (z == e2 || !z.isFocusable()) {
            return null;
        }
        return z;
    }

    public final void c(int i) {
        this.m = i;
        this.n = Integer.MIN_VALUE;
        if (this.o != null) {
            this.o.a = -1;
        }
        k();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
     arg types: [android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
     arg types: [int, android.support.v7.widget.bw, android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0098  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(android.support.v7.widget.bw r13, android.support.v7.widget.cc r14) {
        /*
            r12 = this;
            android.support.v7.widget.LinearLayoutManager$SavedState r0 = r12.o
            if (r0 != 0) goto L_0x0009
            int r0 = r12.m
            r1 = -1
            if (r0 == r1) goto L_0x0013
        L_0x0009:
            int r0 = r14.e()
            if (r0 != 0) goto L_0x0013
            r12.c(r13)
        L_0x0012:
            return
        L_0x0013:
            android.support.v7.widget.LinearLayoutManager$SavedState r0 = r12.o
            if (r0 == 0) goto L_0x0025
            android.support.v7.widget.LinearLayoutManager$SavedState r0 = r12.o
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x0025
            android.support.v7.widget.LinearLayoutManager$SavedState r0 = r12.o
            int r0 = r0.a
            r12.m = r0
        L_0x0025:
            r12.i()
            android.support.v7.widget.aj r0 = r12.a
            r1 = 0
            r0.a = r1
            r12.y()
            android.support.v7.widget.ah r0 = r12.p
            r1 = -1
            r0.a = r1
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r0.b = r1
            r1 = 0
            r0.c = r1
            android.support.v7.widget.ah r0 = r12.p
            boolean r1 = r12.l
            boolean r2 = r12.d
            r1 = r1 ^ r2
            r0.c = r1
            android.support.v7.widget.ah r2 = r12.p
            boolean r0 = r14.a()
            if (r0 != 0) goto L_0x0052
            int r0 = r12.m
            r1 = -1
            if (r0 != r1) goto L_0x01b4
        L_0x0052:
            r0 = 0
        L_0x0053:
            if (r0 != 0) goto L_0x00a7
            int r0 = r12.o()
            if (r0 == 0) goto L_0x03df
            android.support.v7.widget.RecyclerView r0 = r12.r
            if (r0 != 0) goto L_0x02bc
            r0 = 0
            r1 = r0
        L_0x0061:
            if (r1 == 0) goto L_0x0384
            android.view.ViewGroup$LayoutParams r0 = r1.getLayoutParams()
            android.support.v7.widget.RecyclerView$LayoutParams r0 = (android.support.v7.widget.RecyclerView.LayoutParams) r0
            android.support.v7.widget.cf r3 = r0.a
            boolean r3 = r3.m()
            if (r3 != 0) goto L_0x02d3
            android.support.v7.widget.cf r3 = r0.a
            int r3 = r3.e_()
            if (r3 < 0) goto L_0x02d3
            android.support.v7.widget.cf r0 = r0.a
            int r0 = r0.e_()
            int r3 = r14.e()
            if (r0 >= r3) goto L_0x02d3
            r0 = 1
        L_0x0086:
            if (r0 == 0) goto L_0x0384
            android.support.v7.widget.LinearLayoutManager r0 = r2.d
            android.support.v7.widget.az r0 = r0.k
            int r0 = r0.b()
            if (r0 < 0) goto L_0x02d6
            r2.a(r1)
        L_0x0095:
            r0 = 1
        L_0x0096:
            if (r0 != 0) goto L_0x00a7
            r2.a()
            boolean r0 = r12.d
            if (r0 == 0) goto L_0x03e2
            int r0 = r14.e()
            int r0 = r0 + -1
        L_0x00a5:
            r2.a = r0
        L_0x00a7:
            int r0 = r12.g(r14)
            android.support.v7.widget.aj r1 = r12.a
            int r1 = r1.j
            if (r1 < 0) goto L_0x03e5
            r1 = 0
        L_0x00b2:
            android.support.v7.widget.az r2 = r12.k
            int r2 = r2.c()
            int r1 = r1 + r2
            android.support.v7.widget.az r2 = r12.k
            int r2 = r2.g()
            int r0 = r0 + r2
            boolean r2 = r14.a()
            if (r2 == 0) goto L_0x00f1
            int r2 = r12.m
            r3 = -1
            if (r2 == r3) goto L_0x00f1
            int r2 = r12.n
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r2 == r3) goto L_0x00f1
            int r2 = r12.m
            android.view.View r2 = r12.b(r2)
            if (r2 == 0) goto L_0x00f1
            boolean r3 = r12.l
            if (r3 == 0) goto L_0x03eb
            android.support.v7.widget.az r3 = r12.k
            int r3 = r3.d()
            android.support.v7.widget.az r4 = r12.k
            int r2 = r4.b(r2)
            int r2 = r3 - r2
            int r3 = r12.n
            int r2 = r2 - r3
        L_0x00ee:
            if (r2 <= 0) goto L_0x03fe
            int r1 = r1 + r2
        L_0x00f1:
            android.support.v7.widget.ah r2 = r12.p
            r12.a(r13, r14, r2)
            r12.a(r13)
            android.support.v7.widget.aj r2 = r12.a
            boolean r3 = r14.a()
            r2.i = r3
            android.support.v7.widget.ah r2 = r12.p
            boolean r2 = r2.c
            if (r2 == 0) goto L_0x0401
            android.support.v7.widget.ah r2 = r12.p
            r12.b(r2)
            android.support.v7.widget.aj r2 = r12.a
            r2.h = r1
            android.support.v7.widget.aj r1 = r12.a
            r2 = 0
            r12.a(r13, r1, r14, r2)
            android.support.v7.widget.aj r1 = r12.a
            int r1 = r1.b
            android.support.v7.widget.aj r2 = r12.a
            int r3 = r2.d
            android.support.v7.widget.aj r2 = r12.a
            int r2 = r2.c
            if (r2 <= 0) goto L_0x0129
            android.support.v7.widget.aj r2 = r12.a
            int r2 = r2.c
            int r0 = r0 + r2
        L_0x0129:
            android.support.v7.widget.ah r2 = r12.p
            r12.a(r2)
            android.support.v7.widget.aj r2 = r12.a
            r2.h = r0
            android.support.v7.widget.aj r0 = r12.a
            int r2 = r0.d
            android.support.v7.widget.aj r4 = r12.a
            int r4 = r4.e
            int r2 = r2 + r4
            r0.d = r2
            android.support.v7.widget.aj r0 = r12.a
            r2 = 0
            r12.a(r13, r0, r14, r2)
            android.support.v7.widget.aj r0 = r12.a
            int r2 = r0.b
            android.support.v7.widget.aj r0 = r12.a
            int r0 = r0.c
            if (r0 <= 0) goto L_0x0518
            android.support.v7.widget.aj r0 = r12.a
            int r0 = r0.c
            r12.h(r3, r1)
            android.support.v7.widget.aj r1 = r12.a
            r1.h = r0
            android.support.v7.widget.aj r0 = r12.a
            r1 = 0
            r12.a(r13, r0, r14, r1)
            android.support.v7.widget.aj r0 = r12.a
            int r0 = r0.b
        L_0x0162:
            r1 = r0
            r0 = r2
        L_0x0164:
            int r2 = r12.o()
            if (r2 <= 0) goto L_0x0514
            boolean r2 = r12.l
            boolean r3 = r12.d
            r2 = r2 ^ r3
            if (r2 == 0) goto L_0x045e
            r2 = 1
            int r2 = r12.a(r0, r13, r14, r2)
            int r1 = r1 + r2
            int r0 = r0 + r2
            r2 = 0
            int r2 = r12.b(r1, r13, r14, r2)
            int r1 = r1 + r2
            int r0 = r0 + r2
            r2 = r1
            r1 = r0
        L_0x0181:
            boolean r0 = r14.b()
            if (r0 == 0) goto L_0x0199
            int r0 = r12.o()
            if (r0 == 0) goto L_0x0199
            boolean r0 = r14.a()
            if (r0 != 0) goto L_0x0199
            boolean r0 = r12.c()
            if (r0 != 0) goto L_0x0470
        L_0x0199:
            boolean r0 = r14.a()
            if (r0 != 0) goto L_0x01ab
            r0 = -1
            r12.m = r0
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r12.n = r0
            android.support.v7.widget.az r0 = r12.k
            r0.a()
        L_0x01ab:
            boolean r0 = r12.d
            r12.b = r0
            r0 = 0
            r12.o = r0
            goto L_0x0012
        L_0x01b4:
            int r0 = r12.m
            if (r0 < 0) goto L_0x01c0
            int r0 = r12.m
            int r1 = r14.e()
            if (r0 < r1) goto L_0x01ca
        L_0x01c0:
            r0 = -1
            r12.m = r0
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r12.n = r0
            r0 = 0
            goto L_0x0053
        L_0x01ca:
            int r0 = r12.m
            r2.a = r0
            android.support.v7.widget.LinearLayoutManager$SavedState r0 = r12.o
            if (r0 == 0) goto L_0x0202
            android.support.v7.widget.LinearLayoutManager$SavedState r0 = r12.o
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x0202
            android.support.v7.widget.LinearLayoutManager$SavedState r0 = r12.o
            boolean r0 = r0.c
            r2.c = r0
            boolean r0 = r2.c
            if (r0 == 0) goto L_0x01f4
            android.support.v7.widget.az r0 = r12.k
            int r0 = r0.d()
            android.support.v7.widget.LinearLayoutManager$SavedState r1 = r12.o
            int r1 = r1.b
            int r0 = r0 - r1
            r2.b = r0
        L_0x01f1:
            r0 = 1
            goto L_0x0053
        L_0x01f4:
            android.support.v7.widget.az r0 = r12.k
            int r0 = r0.c()
            android.support.v7.widget.LinearLayoutManager$SavedState r1 = r12.o
            int r1 = r1.b
            int r0 = r0 + r1
            r2.b = r0
            goto L_0x01f1
        L_0x0202:
            int r0 = r12.n
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r0 != r1) goto L_0x029a
            int r0 = r12.m
            android.view.View r0 = r12.b(r0)
            if (r0 == 0) goto L_0x0277
            android.support.v7.widget.az r1 = r12.k
            int r1 = r1.c(r0)
            android.support.v7.widget.az r3 = r12.k
            int r3 = r3.f()
            if (r1 <= r3) goto L_0x0224
            r2.a()
        L_0x0221:
            r0 = 1
            goto L_0x0053
        L_0x0224:
            android.support.v7.widget.az r1 = r12.k
            int r1 = r1.a(r0)
            android.support.v7.widget.az r3 = r12.k
            int r3 = r3.c()
            int r1 = r1 - r3
            if (r1 >= 0) goto L_0x023f
            android.support.v7.widget.az r0 = r12.k
            int r0 = r0.c()
            r2.b = r0
            r0 = 0
            r2.c = r0
            goto L_0x0221
        L_0x023f:
            android.support.v7.widget.az r1 = r12.k
            int r1 = r1.d()
            android.support.v7.widget.az r3 = r12.k
            int r3 = r3.b(r0)
            int r1 = r1 - r3
            if (r1 >= 0) goto L_0x025a
            android.support.v7.widget.az r0 = r12.k
            int r0 = r0.d()
            r2.b = r0
            r0 = 1
            r2.c = r0
            goto L_0x0221
        L_0x025a:
            boolean r1 = r2.c
            if (r1 == 0) goto L_0x0270
            android.support.v7.widget.az r1 = r12.k
            int r0 = r1.b(r0)
            android.support.v7.widget.az r1 = r12.k
            int r1 = r1.b()
            int r0 = r0 + r1
        L_0x026b:
            r2.b = r0
        L_0x026d:
            r0 = 1
            goto L_0x0053
        L_0x0270:
            android.support.v7.widget.az r1 = r12.k
            int r0 = r1.a(r0)
            goto L_0x026b
        L_0x0277:
            int r0 = r12.o()
            if (r0 <= 0) goto L_0x0292
            r0 = 0
            android.view.View r0 = r12.d(r0)
            int r0 = e(r0)
            int r1 = r12.m
            if (r1 >= r0) goto L_0x0296
            r0 = 1
        L_0x028b:
            boolean r1 = r12.l
            if (r0 != r1) goto L_0x0298
            r0 = 1
        L_0x0290:
            r2.c = r0
        L_0x0292:
            r2.a()
            goto L_0x026d
        L_0x0296:
            r0 = 0
            goto L_0x028b
        L_0x0298:
            r0 = 0
            goto L_0x0290
        L_0x029a:
            boolean r0 = r12.l
            r2.c = r0
            boolean r0 = r12.l
            if (r0 == 0) goto L_0x02af
            android.support.v7.widget.az r0 = r12.k
            int r0 = r0.d()
            int r1 = r12.n
            int r0 = r0 - r1
            r2.b = r0
            goto L_0x0221
        L_0x02af:
            android.support.v7.widget.az r0 = r12.k
            int r0 = r0.c()
            int r1 = r12.n
            int r0 = r0 + r1
            r2.b = r0
            goto L_0x0221
        L_0x02bc:
            android.support.v7.widget.RecyclerView r0 = r12.r
            android.view.View r0 = r0.getFocusedChild()
            if (r0 == 0) goto L_0x02cc
            android.support.v7.widget.p r1 = r12.q
            boolean r1 = r1.d(r0)
            if (r1 == 0) goto L_0x02d0
        L_0x02cc:
            r0 = 0
            r1 = r0
            goto L_0x0061
        L_0x02d0:
            r1 = r0
            goto L_0x0061
        L_0x02d3:
            r0 = 0
            goto L_0x0086
        L_0x02d6:
            int r3 = e(r1)
            r2.a = r3
            boolean r3 = r2.c
            if (r3 == 0) goto L_0x0333
            android.support.v7.widget.LinearLayoutManager r3 = r2.d
            android.support.v7.widget.az r3 = r3.k
            int r3 = r3.d()
            int r0 = r3 - r0
            android.support.v7.widget.LinearLayoutManager r3 = r2.d
            android.support.v7.widget.az r3 = r3.k
            int r3 = r3.b(r1)
            int r0 = r0 - r3
            android.support.v7.widget.LinearLayoutManager r3 = r2.d
            android.support.v7.widget.az r3 = r3.k
            int r3 = r3.d()
            int r3 = r3 - r0
            r2.b = r3
            if (r0 <= 0) goto L_0x0095
            android.support.v7.widget.LinearLayoutManager r3 = r2.d
            android.support.v7.widget.az r3 = r3.k
            int r3 = r3.c(r1)
            int r4 = r2.b
            int r3 = r4 - r3
            android.support.v7.widget.LinearLayoutManager r4 = r2.d
            android.support.v7.widget.az r4 = r4.k
            int r4 = r4.c()
            android.support.v7.widget.LinearLayoutManager r5 = r2.d
            android.support.v7.widget.az r5 = r5.k
            int r1 = r5.a(r1)
            int r1 = r1 - r4
            r5 = 0
            int r1 = java.lang.Math.min(r1, r5)
            int r1 = r1 + r4
            int r1 = r3 - r1
            if (r1 >= 0) goto L_0x0095
            int r3 = r2.b
            int r1 = -r1
            int r0 = java.lang.Math.min(r0, r1)
            int r0 = r0 + r3
            r2.b = r0
            goto L_0x0095
        L_0x0333:
            android.support.v7.widget.LinearLayoutManager r3 = r2.d
            android.support.v7.widget.az r3 = r3.k
            int r3 = r3.a(r1)
            android.support.v7.widget.LinearLayoutManager r4 = r2.d
            android.support.v7.widget.az r4 = r4.k
            int r4 = r4.c()
            int r4 = r3 - r4
            r2.b = r3
            if (r4 <= 0) goto L_0x0095
            android.support.v7.widget.LinearLayoutManager r5 = r2.d
            android.support.v7.widget.az r5 = r5.k
            int r5 = r5.c(r1)
            int r3 = r3 + r5
            android.support.v7.widget.LinearLayoutManager r5 = r2.d
            android.support.v7.widget.az r5 = r5.k
            int r5 = r5.d()
            int r0 = r5 - r0
            android.support.v7.widget.LinearLayoutManager r5 = r2.d
            android.support.v7.widget.az r5 = r5.k
            int r1 = r5.b(r1)
            int r0 = r0 - r1
            android.support.v7.widget.LinearLayoutManager r1 = r2.d
            android.support.v7.widget.az r1 = r1.k
            int r1 = r1.d()
            r5 = 0
            int r0 = java.lang.Math.min(r5, r0)
            int r0 = r1 - r0
            int r0 = r0 - r3
            if (r0 >= 0) goto L_0x0095
            int r1 = r2.b
            int r0 = -r0
            int r0 = java.lang.Math.min(r4, r0)
            int r0 = r1 - r0
            r2.b = r0
            goto L_0x0095
        L_0x0384:
            boolean r0 = r12.b
            boolean r1 = r12.d
            if (r0 != r1) goto L_0x03df
            boolean r0 = r2.c
            if (r0 == 0) goto L_0x03d1
            android.view.View r0 = r12.d(r13, r14)
        L_0x0392:
            if (r0 == 0) goto L_0x03df
            r2.a(r0)
            boolean r1 = r14.a()
            if (r1 != 0) goto L_0x03ce
            boolean r1 = r12.c()
            if (r1 == 0) goto L_0x03ce
            android.support.v7.widget.az r1 = r12.k
            int r1 = r1.a(r0)
            android.support.v7.widget.az r3 = r12.k
            int r3 = r3.d()
            if (r1 >= r3) goto L_0x03bf
            android.support.v7.widget.az r1 = r12.k
            int r0 = r1.b(r0)
            android.support.v7.widget.az r1 = r12.k
            int r1 = r1.c()
            if (r0 >= r1) goto L_0x03d6
        L_0x03bf:
            r0 = 1
        L_0x03c0:
            if (r0 == 0) goto L_0x03ce
            boolean r0 = r2.c
            if (r0 == 0) goto L_0x03d8
            android.support.v7.widget.az r0 = r12.k
            int r0 = r0.d()
        L_0x03cc:
            r2.b = r0
        L_0x03ce:
            r0 = 1
            goto L_0x0096
        L_0x03d1:
            android.view.View r0 = r12.e(r13, r14)
            goto L_0x0392
        L_0x03d6:
            r0 = 0
            goto L_0x03c0
        L_0x03d8:
            android.support.v7.widget.az r0 = r12.k
            int r0 = r0.c()
            goto L_0x03cc
        L_0x03df:
            r0 = 0
            goto L_0x0096
        L_0x03e2:
            r0 = 0
            goto L_0x00a5
        L_0x03e5:
            r1 = 0
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x00b2
        L_0x03eb:
            android.support.v7.widget.az r3 = r12.k
            int r2 = r3.a(r2)
            android.support.v7.widget.az r3 = r12.k
            int r3 = r3.c()
            int r2 = r2 - r3
            int r3 = r12.n
            int r2 = r3 - r2
            goto L_0x00ee
        L_0x03fe:
            int r0 = r0 - r2
            goto L_0x00f1
        L_0x0401:
            android.support.v7.widget.ah r2 = r12.p
            r12.a(r2)
            android.support.v7.widget.aj r2 = r12.a
            r2.h = r0
            android.support.v7.widget.aj r0 = r12.a
            r2 = 0
            r12.a(r13, r0, r14, r2)
            android.support.v7.widget.aj r0 = r12.a
            int r0 = r0.b
            android.support.v7.widget.aj r2 = r12.a
            int r2 = r2.d
            android.support.v7.widget.aj r3 = r12.a
            int r3 = r3.c
            if (r3 <= 0) goto L_0x0423
            android.support.v7.widget.aj r3 = r12.a
            int r3 = r3.c
            int r1 = r1 + r3
        L_0x0423:
            android.support.v7.widget.ah r3 = r12.p
            r12.b(r3)
            android.support.v7.widget.aj r3 = r12.a
            r3.h = r1
            android.support.v7.widget.aj r1 = r12.a
            int r3 = r1.d
            android.support.v7.widget.aj r4 = r12.a
            int r4 = r4.e
            int r3 = r3 + r4
            r1.d = r3
            android.support.v7.widget.aj r1 = r12.a
            r3 = 0
            r12.a(r13, r1, r14, r3)
            android.support.v7.widget.aj r1 = r12.a
            int r1 = r1.b
            android.support.v7.widget.aj r3 = r12.a
            int r3 = r3.c
            if (r3 <= 0) goto L_0x0164
            android.support.v7.widget.aj r3 = r12.a
            int r3 = r3.c
            r12.g(r2, r0)
            android.support.v7.widget.aj r0 = r12.a
            r0.h = r3
            android.support.v7.widget.aj r0 = r12.a
            r2 = 0
            r12.a(r13, r0, r14, r2)
            android.support.v7.widget.aj r0 = r12.a
            int r0 = r0.b
            goto L_0x0164
        L_0x045e:
            r2 = 1
            int r2 = r12.b(r1, r13, r14, r2)
            int r1 = r1 + r2
            int r0 = r0 + r2
            r2 = 0
            int r2 = r12.a(r0, r13, r14, r2)
            int r1 = r1 + r2
            int r0 = r0 + r2
            r2 = r1
            r1 = r0
            goto L_0x0181
        L_0x0470:
            r5 = 0
            r4 = 0
            java.util.List r7 = r13.b()
            int r8 = r7.size()
            r0 = 0
            android.view.View r0 = r12.d(r0)
            int r9 = e(r0)
            r0 = 0
            r6 = r0
        L_0x0485:
            if (r6 >= r8) goto L_0x04c2
            java.lang.Object r0 = r7.get(r6)
            android.support.v7.widget.cf r0 = (android.support.v7.widget.cf) r0
            boolean r3 = r0.m()
            if (r3 != 0) goto L_0x0511
            int r3 = r0.e_()
            if (r3 >= r9) goto L_0x04b3
            r3 = 1
        L_0x049a:
            boolean r10 = r12.l
            if (r3 == r10) goto L_0x04b5
            r3 = -1
        L_0x049f:
            r10 = -1
            if (r3 != r10) goto L_0x04b7
            android.support.v7.widget.az r3 = r12.k
            android.view.View r0 = r0.a
            int r0 = r3.c(r0)
            int r0 = r0 + r5
            r3 = r0
            r0 = r4
        L_0x04ad:
            int r4 = r6 + 1
            r5 = r3
            r6 = r4
            r4 = r0
            goto L_0x0485
        L_0x04b3:
            r3 = 0
            goto L_0x049a
        L_0x04b5:
            r3 = 1
            goto L_0x049f
        L_0x04b7:
            android.support.v7.widget.az r3 = r12.k
            android.view.View r0 = r0.a
            int r0 = r3.c(r0)
            int r0 = r0 + r4
            r3 = r5
            goto L_0x04ad
        L_0x04c2:
            android.support.v7.widget.aj r0 = r12.a
            r0.k = r7
            if (r5 <= 0) goto L_0x04e8
            android.view.View r0 = r12.z()
            int r0 = e(r0)
            r12.h(r0, r2)
            android.support.v7.widget.aj r0 = r12.a
            r0.h = r5
            android.support.v7.widget.aj r0 = r12.a
            r2 = 0
            r0.c = r2
            android.support.v7.widget.aj r0 = r12.a
            r2 = 0
            r0.a(r2)
            android.support.v7.widget.aj r0 = r12.a
            r2 = 0
            r12.a(r13, r0, r14, r2)
        L_0x04e8:
            if (r4 <= 0) goto L_0x050a
            android.view.View r0 = r12.A()
            int r0 = e(r0)
            r12.g(r0, r1)
            android.support.v7.widget.aj r0 = r12.a
            r0.h = r4
            android.support.v7.widget.aj r0 = r12.a
            r1 = 0
            r0.c = r1
            android.support.v7.widget.aj r0 = r12.a
            r1 = 0
            r0.a(r1)
            android.support.v7.widget.aj r0 = r12.a
            r1 = 0
            r12.a(r13, r0, r14, r1)
        L_0x050a:
            android.support.v7.widget.aj r0 = r12.a
            r1 = 0
            r0.k = r1
            goto L_0x0199
        L_0x0511:
            r0 = r4
            r3 = r5
            goto L_0x04ad
        L_0x0514:
            r2 = r1
            r1 = r0
            goto L_0x0181
        L_0x0518:
            r0 = r1
            goto L_0x0162
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.LinearLayoutManager.c(android.support.v7.widget.bw, android.support.v7.widget.cc):void");
    }

    public boolean c() {
        return this.o == null && this.b == this.d;
    }

    public final int d(cc ccVar) {
        return i(ccVar);
    }

    public final Parcelable d() {
        if (this.o != null) {
            return new SavedState(this.o);
        }
        SavedState savedState = new SavedState();
        if (o() > 0) {
            i();
            boolean z = this.b ^ this.l;
            savedState.c = z;
            if (z) {
                View A = A();
                savedState.b = this.k.d() - this.k.b(A);
                savedState.a = e(A);
                return savedState;
            }
            View z2 = z();
            savedState.a = e(z2);
            savedState.b = this.k.a(z2) - this.k.c();
            return savedState;
        }
        savedState.a = -1;
        return savedState;
    }

    public final int e(cc ccVar) {
        return j(ccVar);
    }

    public final void e(int i, int i2) {
        this.m = i;
        this.n = i2;
        if (this.o != null) {
            this.o.a = -1;
        }
        k();
    }

    public final boolean e() {
        return this.j == 0;
    }

    public final int f(cc ccVar) {
        return j(ccVar);
    }

    public final boolean f() {
        return this.j == 1;
    }

    public final int g() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final boolean h() {
        return bx.h(this.r) == 1;
    }

    /* access modifiers changed from: package-private */
    public final void i() {
        if (this.a == null) {
            this.a = new aj();
        }
        if (this.k == null) {
            this.k = az.a(this, this.j);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
     arg types: [int, int, int, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.aj, android.support.v7.widget.cc, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.cc):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.support.v7.widget.aj, android.support.v7.widget.ai):void
      android.support.v7.widget.br.a(int, int, int, boolean):int
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, int, int):void
      android.support.v7.widget.br.a(android.support.v7.widget.bw, android.support.v7.widget.cc, android.view.View, android.support.v4.view.a.f):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View */
    public final int j() {
        View a2 = a(0, o(), true, false);
        if (a2 == null) {
            return -1;
        }
        return e(a2);
    }
}
