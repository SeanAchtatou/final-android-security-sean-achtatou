package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;

public abstract class al extends ca {
    private final float a;
    protected final LinearInterpolator b = new LinearInterpolator();
    protected final DecelerateInterpolator c = new DecelerateInterpolator();
    protected PointF d;
    protected int e = 0;
    protected int f = 0;

    public al(Context context) {
        this.a = 25.0f / ((float) context.getResources().getDisplayMetrics().densityDpi);
    }

    private static int a(int i, int i2) {
        int i3 = i - i2;
        if (i * i3 <= 0) {
            return 0;
        }
        return i3;
    }

    private static int a(int i, int i2, int i3, int i4, int i5) {
        switch (i5) {
            case -1:
                return i3 - i;
            case 0:
                int i6 = i3 - i;
                if (i6 > 0) {
                    return i6;
                }
                int i7 = i4 - i2;
                if (i7 >= 0) {
                    return 0;
                }
                return i7;
            case 1:
                return i4 - i2;
            default:
                throw new IllegalArgumentException("snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_");
        }
    }

    private int c(int i) {
        return (int) Math.ceil((double) (((float) Math.abs(i)) * this.a));
    }

    public abstract PointF a(int i);

    /* access modifiers changed from: protected */
    public final void a() {
        this.f = 0;
        this.e = 0;
        this.d = null;
    }

    /* access modifiers changed from: protected */
    public final void a(int i, int i2, cb cbVar) {
        if (g() == 0) {
            c();
            return;
        }
        this.e = a(this.e, i);
        this.f = a(this.f, i2);
        if (this.e == 0 && this.f == 0) {
            PointF a2 = a(f());
            if (a2 == null || (a2.x == 0.0f && a2.y == 0.0f)) {
                Log.e("LinearSmoothScroller", "To support smooth scrolling, you should override \nLayoutManager#computeScrollVectorForPosition.\nFalling back to instant scroll");
                cbVar.a(f());
                c();
                return;
            }
            double sqrt = Math.sqrt((double) ((a2.x * a2.x) + (a2.y * a2.y)));
            a2.x = (float) (((double) a2.x) / sqrt);
            a2.y = (float) (((double) a2.y) / sqrt);
            this.d = a2;
            this.e = (int) (a2.x * 10000.0f);
            this.f = (int) (a2.y * 10000.0f);
            cbVar.a((int) (((float) this.e) * 1.2f), (int) (((float) this.f) * 1.2f), (int) (((float) c(10000)) * 1.2f), this.b);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(View view, cb cbVar) {
        int a2;
        int i = 1;
        int i2 = 0;
        int i3 = (this.d == null || this.d.x == 0.0f) ? 0 : this.d.x > 0.0f ? 1 : -1;
        br b2 = b();
        if (!b2.e()) {
            a2 = 0;
        } else {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            a2 = a(br.h(view) - layoutParams.leftMargin, layoutParams.rightMargin + br.j(view), b2.r(), b2.p() - b2.t(), i3);
        }
        if (this.d == null || this.d.y == 0.0f) {
            i = 0;
        } else if (this.d.y <= 0.0f) {
            i = -1;
        }
        br b3 = b();
        if (b3.f()) {
            RecyclerView.LayoutParams layoutParams2 = (RecyclerView.LayoutParams) view.getLayoutParams();
            i2 = a(br.i(view) - layoutParams2.topMargin, layoutParams2.bottomMargin + br.k(view), b3.s(), b3.q() - b3.u(), i);
        }
        int ceil = (int) Math.ceil(((double) c((int) Math.sqrt((double) ((a2 * a2) + (i2 * i2))))) / 0.3356d);
        if (ceil > 0) {
            cbVar.a(-a2, -i2, ceil, this.c);
        }
    }
}
