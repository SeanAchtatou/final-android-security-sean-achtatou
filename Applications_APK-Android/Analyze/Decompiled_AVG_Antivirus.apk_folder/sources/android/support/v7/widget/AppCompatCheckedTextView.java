package android.support.v7.widget;

import android.content.Context;
import android.support.v7.internal.widget.bc;
import android.support.v7.internal.widget.be;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

public class AppCompatCheckedTextView extends CheckedTextView {
    private static final int[] a = {16843016};
    private bc b;

    public AppCompatCheckedTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16843720);
    }

    public AppCompatCheckedTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (bc.a) {
            be a2 = be.a(getContext(), attributeSet, a, i);
            setCheckMarkDrawable(a2.a(0));
            a2.b();
            this.b = a2.c();
        }
    }

    public void setCheckMarkDrawable(int i) {
        if (this.b != null) {
            setCheckMarkDrawable(this.b.a(i));
        } else {
            super.setCheckMarkDrawable(i);
        }
    }
}
