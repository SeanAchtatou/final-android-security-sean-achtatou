package android.support.v7.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;

final class ak implements Parcelable.Creator {
    ak() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new LinearLayoutManager.SavedState(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new LinearLayoutManager.SavedState[i];
    }
}
