package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.a.b;
import android.support.v7.a.l;
import android.support.v7.internal.b.a;
import android.support.v7.internal.widget.ay;
import android.support.v7.internal.widget.bb;
import android.support.v7.internal.widget.bc;
import android.support.v7.internal.widget.be;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;

public class AppCompatButton extends Button {
    private static final int[] a = {16842964};
    private bb b;
    private bb c;
    private bc d;

    public AppCompatButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.buttonStyle);
    }

    public AppCompatButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ColorStateList b2;
        if (bc.a) {
            be a2 = be.a(getContext(), attributeSet, a, i);
            if (a2.f(0) && (b2 = a2.c().b(a2.f(0, -1))) != null) {
                a(b2);
            }
            this.d = a2.c();
            a2.b();
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.P, i, 0);
        int resourceId = obtainStyledAttributes.getResourceId(l.Q, -1);
        obtainStyledAttributes.recycle();
        if (resourceId != -1) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(resourceId, l.bB);
            if (obtainStyledAttributes2.hasValue(l.bG)) {
                setAllCaps(obtainStyledAttributes2.getBoolean(l.bG, false));
            }
            obtainStyledAttributes2.recycle();
        }
        TypedArray obtainStyledAttributes3 = context.obtainStyledAttributes(attributeSet, l.P, i, 0);
        if (obtainStyledAttributes3.hasValue(l.R)) {
            setAllCaps(obtainStyledAttributes3.getBoolean(l.R, false));
        }
        obtainStyledAttributes3.recycle();
        ColorStateList textColors = getTextColors();
        if (textColors != null && !textColors.isStateful()) {
            setTextColor(ay.a(textColors.getDefaultColor(), Build.VERSION.SDK_INT < 21 ? ay.c(context, 16842808) : ay.a(context, 16842808)));
        }
    }

    private void a() {
        if (getBackground() == null) {
            return;
        }
        if (this.c != null) {
            bc.a(this, this.c);
        } else if (this.b != null) {
            bc.a(this, this.b);
        }
    }

    private void a(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.b == null) {
                this.b = new bb();
            }
            this.b.a = colorStateList;
            this.b.d = true;
        } else {
            this.b = null;
        }
        a();
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        a();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(Button.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(Button.class.getName());
    }

    public void setAllCaps(boolean z) {
        setTransformationMethod(z ? new a(getContext()) : null);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        a(null);
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        a(this.d != null ? this.d.b(i) : null);
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, l.bB);
        if (obtainStyledAttributes.hasValue(l.bG)) {
            setAllCaps(obtainStyledAttributes.getBoolean(l.bG, false));
        }
        obtainStyledAttributes.recycle();
    }
}
