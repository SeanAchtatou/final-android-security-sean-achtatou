package android.support.v7.widget;

import java.util.ArrayList;

public abstract class bm {
    private bn a = null;
    private ArrayList b = new ArrayList();
    private long c = 120;
    private long d = 120;
    private long e = 250;
    private long f = 250;

    static int d(cf cfVar) {
        int h = cfVar.l & 14;
        if (cfVar.j()) {
            return 4;
        }
        if ((h & 4) != 0) {
            return h;
        }
        int i = cfVar.c;
        int d2 = cfVar.d();
        return (i == -1 || d2 == -1 || i == d2) ? h : h | 2048;
    }

    public abstract void a();

    /* access modifiers changed from: package-private */
    public final void a(bn bnVar) {
        this.a = bnVar;
    }

    public abstract boolean a(cf cfVar, bo boVar, bo boVar2);

    public abstract boolean a(cf cfVar, cf cfVar2, bo boVar, bo boVar2);

    public abstract boolean b();

    public abstract boolean b(cf cfVar, bo boVar, bo boVar2);

    public abstract void c();

    public abstract void c(cf cfVar);

    public abstract boolean c(cf cfVar, bo boVar, bo boVar2);

    public final long d() {
        return this.e;
    }

    public final void e() {
        this.e = 140;
    }

    public final void e(cf cfVar) {
        if (this.a != null) {
            this.a.a(cfVar);
        }
    }

    public final long f() {
        return this.c;
    }

    public boolean f(cf cfVar) {
        return true;
    }

    public final long g() {
        return this.d;
    }

    public final void h() {
        this.d = 100;
    }

    public final long i() {
        return this.f;
    }

    public final void j() {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            this.b.get(i);
        }
        this.b.clear();
    }
}
