package android.support.v7.widget;

import android.support.v4.view.bx;

final class by extends bk {
    final /* synthetic */ RecyclerView a;

    private by(RecyclerView recyclerView) {
        this.a = recyclerView;
    }

    /* synthetic */ by(RecyclerView recyclerView, byte b) {
        this(recyclerView);
    }

    private void b() {
        if (!this.a.E || !this.a.w || !this.a.v) {
            boolean unused = this.a.D = true;
            this.a.requestLayout();
            return;
        }
        bx.a(this.a, this.a.n);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean
     arg types: [android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, int):int
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean */
    public final void a() {
        this.a.a((String) null);
        if (this.a.p.b_()) {
            boolean unused = this.a.f.f = true;
            RecyclerView.o(this.a);
        } else {
            boolean unused2 = this.a.f.f = true;
            RecyclerView.o(this.a);
        }
        if (!this.a.b.d()) {
            this.a.requestLayout();
        }
    }

    public final void a(int i) {
        this.a.a((String) null);
        if (this.a.b.c(i)) {
            b();
        }
    }

    public final void a(int i, int i2) {
        this.a.a((String) null);
        if (this.a.b.b(i, i2)) {
            b();
        }
    }

    public final void b(int i) {
        this.a.a((String) null);
        if (this.a.b.d(i)) {
            b();
        }
    }

    public final void b(int i, int i2) {
        this.a.a((String) null);
        if (this.a.b.c(i, i2)) {
            b();
        }
    }
}
