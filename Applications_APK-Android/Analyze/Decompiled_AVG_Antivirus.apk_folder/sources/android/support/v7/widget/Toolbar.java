package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ao;
import android.support.v4.view.as;
import android.support.v4.view.ay;
import android.support.v4.view.bx;
import android.support.v4.view.v;
import android.support.v7.a.b;
import android.support.v7.a.l;
import android.support.v7.app.ActionBar;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.y;
import android.support.v7.internal.widget.af;
import android.support.v7.internal.widget.al;
import android.support.v7.internal.widget.bc;
import android.support.v7.internal.widget.be;
import android.support.v7.internal.widget.bf;
import android.support.v7.internal.widget.bj;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class Toolbar extends ViewGroup {
    private boolean A;
    private final ArrayList B;
    private final int[] C;
    /* access modifiers changed from: private */
    public dm D;
    private final l E;
    private bf F;
    private ActionMenuPresenter G;
    private dl H;
    private y I;
    private j J;
    private boolean K;
    private final Runnable L;
    private final bc M;
    View a;
    private ActionMenuView b;
    private TextView c;
    private TextView d;
    private ImageButton e;
    private ImageView f;
    private Drawable g;
    private CharSequence h;
    /* access modifiers changed from: private */
    public ImageButton i;
    private Context j;
    private int k;
    private int l;
    private int m;
    /* access modifiers changed from: private */
    public int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private final al t;
    private int u;
    private CharSequence v;
    private CharSequence w;
    private int x;
    private int y;
    private boolean z;

    public class LayoutParams extends ActionBar.LayoutParams {
        int b = 0;

        public LayoutParams() {
            this.a = 8388627;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(ActionBar.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ActionBar.LayoutParams) layoutParams);
            this.b = layoutParams.b;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.leftMargin = marginLayoutParams.leftMargin;
            this.topMargin = marginLayoutParams.topMargin;
            this.rightMargin = marginLayoutParams.rightMargin;
            this.bottomMargin = marginLayoutParams.bottomMargin;
        }
    }

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new dn();
        int a;
        boolean b;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readInt();
            this.b = parcel.readInt() != 0;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
            parcel.writeInt(this.b ? 1 : 0);
        }
    }

    public Toolbar(Context context) {
        this(context, null);
    }

    public Toolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.toolbarStyle);
    }

    public Toolbar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.t = new al();
        this.u = 8388627;
        this.B = new ArrayList();
        this.C = new int[2];
        this.E = new di(this);
        this.L = new dj(this);
        be a2 = be.a(getContext(), attributeSet, l.bV, i2);
        this.l = a2.f(l.cp, 0);
        this.m = a2.f(l.ci, 0);
        this.u = a2.b(l.bW, this.u);
        this.n = 48;
        int c2 = a2.c(l.co, 0);
        this.s = c2;
        this.r = c2;
        this.q = c2;
        this.p = c2;
        int c3 = a2.c(l.cm, -1);
        if (c3 >= 0) {
            this.p = c3;
        }
        int c4 = a2.c(l.cl, -1);
        if (c4 >= 0) {
            this.q = c4;
        }
        int c5 = a2.c(l.cn, -1);
        if (c5 >= 0) {
            this.r = c5;
        }
        int c6 = a2.c(l.ck, -1);
        if (c6 >= 0) {
            this.s = c6;
        }
        this.o = a2.d(l.cd, -1);
        int c7 = a2.c(l.cc, Integer.MIN_VALUE);
        int c8 = a2.c(l.bZ, Integer.MIN_VALUE);
        this.t.b(a2.d(l.ca, 0), a2.d(l.cb, 0));
        if (!(c7 == Integer.MIN_VALUE && c8 == Integer.MIN_VALUE)) {
            this.t.a(c7, c8);
        }
        this.g = a2.a(l.bY);
        this.h = a2.c(l.bX);
        CharSequence c9 = a2.c(l.cj);
        if (!TextUtils.isEmpty(c9)) {
            a(c9);
        }
        CharSequence c10 = a2.c(l.ch);
        if (!TextUtils.isEmpty(c10)) {
            b(c10);
        }
        this.j = getContext();
        a(a2.f(l.cg, 0));
        Drawable a3 = a2.a(l.cf);
        if (a3 != null) {
            b(a3);
        }
        CharSequence c11 = a2.c(l.ce);
        if (!TextUtils.isEmpty(c11)) {
            c(c11);
        }
        a2.b();
        this.M = a2.c();
    }

    private int a(View view, int i2) {
        int max;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int measuredHeight = view.getMeasuredHeight();
        int i3 = i2 > 0 ? (measuredHeight - i2) / 2 : 0;
        int i4 = layoutParams.a & 112;
        switch (i4) {
            case 16:
            case 48:
            case 80:
                break;
            default:
                i4 = this.u & 112;
                break;
        }
        switch (i4) {
            case 48:
                return getPaddingTop() - i3;
            case 80:
                return (((getHeight() - getPaddingBottom()) - measuredHeight) - layoutParams.bottomMargin) - i3;
            default:
                int paddingTop = getPaddingTop();
                int paddingBottom = getPaddingBottom();
                int height = getHeight();
                int i5 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
                if (i5 < layoutParams.topMargin) {
                    max = layoutParams.topMargin;
                } else {
                    int i6 = (((height - paddingBottom) - measuredHeight) - i5) - paddingTop;
                    max = i6 < layoutParams.bottomMargin ? Math.max(0, i5 - (layoutParams.bottomMargin - i6)) : i5;
                }
                return max + paddingTop;
        }
    }

    private int a(View view, int i2, int i3, int i4, int i5, int[] iArr) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i6 = marginLayoutParams.leftMargin - iArr[0];
        int i7 = marginLayoutParams.rightMargin - iArr[1];
        int max = Math.max(0, i6) + Math.max(0, i7);
        iArr[0] = Math.max(0, -i6);
        iArr[1] = Math.max(0, -i7);
        view.measure(getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + max + i3, marginLayoutParams.width), getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height));
        return view.getMeasuredWidth() + max;
    }

    private int a(View view, int i2, int[] iArr, int i3) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int i4 = layoutParams.leftMargin - iArr[0];
        int max = Math.max(0, i4) + i2;
        iArr[0] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max, a2, max + measuredWidth, view.getMeasuredHeight() + a2);
        return layoutParams.rightMargin + measuredWidth + max;
    }

    private static LayoutParams a(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams ? new LayoutParams((LayoutParams) layoutParams) : layoutParams instanceof ActionBar.LayoutParams ? new LayoutParams((ActionBar.LayoutParams) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    static /* synthetic */ void a(Toolbar toolbar, boolean z2) {
        int childCount = toolbar.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = toolbar.getChildAt(i2);
            if (!(((LayoutParams) childAt.getLayoutParams()).b == 2 || childAt == toolbar.b)) {
                childAt.setVisibility(z2 ? 8 : 0);
            }
        }
    }

    private void a(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        LayoutParams layoutParams2 = layoutParams == null ? new LayoutParams() : !checkLayoutParams(layoutParams) ? a(layoutParams) : (LayoutParams) layoutParams;
        layoutParams2.b = 1;
        addView(view, layoutParams2);
    }

    private void a(View view, int i2, int i3, int i4, int i5) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int childMeasureSpec = getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i3, marginLayoutParams.width);
        int childMeasureSpec2 = getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + 0, marginLayoutParams.height);
        int mode = View.MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i5 >= 0) {
            if (mode != 0) {
                i5 = Math.min(View.MeasureSpec.getSize(childMeasureSpec2), i5);
            }
            childMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i5, 1073741824);
        }
        view.measure(childMeasureSpec, childMeasureSpec2);
    }

    private void a(List list, int i2) {
        boolean z2 = true;
        if (bx.h(this) != 1) {
            z2 = false;
        }
        int childCount = getChildCount();
        int a2 = v.a(i2, bx.h(this));
        list.clear();
        if (z2) {
            for (int i3 = childCount - 1; i3 >= 0; i3--) {
                View childAt = getChildAt(i3);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.b == 0 && b(childAt) && b(layoutParams.a) == a2) {
                    list.add(childAt);
                }
            }
            return;
        }
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt2 = getChildAt(i4);
            LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
            if (layoutParams2.b == 0 && b(childAt2) && b(layoutParams2.a) == a2) {
                list.add(childAt2);
            }
        }
    }

    private int b(int i2) {
        int h2 = bx.h(this);
        int a2 = v.a(i2, h2) & 7;
        switch (a2) {
            case 1:
            case 3:
            case 5:
                return a2;
            case 2:
            case 4:
            default:
                return h2 == 1 ? 5 : 3;
        }
    }

    private int b(View view, int i2, int[] iArr, int i3) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int i4 = layoutParams.rightMargin - iArr[1];
        int max = i2 - Math.max(0, i4);
        iArr[1] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max - measuredWidth, a2, max, view.getMeasuredHeight() + a2);
        return max - (layoutParams.leftMargin + measuredWidth);
    }

    static /* synthetic */ void b(Toolbar toolbar) {
        if (toolbar.i == null) {
            toolbar.i = new ImageButton(toolbar.getContext(), null, b.toolbarNavigationButtonStyle);
            toolbar.i.setImageDrawable(toolbar.g);
            toolbar.i.setContentDescription(toolbar.h);
            LayoutParams layoutParams = new LayoutParams();
            layoutParams.a = 8388611 | (toolbar.n & 112);
            layoutParams.b = 2;
            toolbar.i.setLayoutParams(layoutParams);
            toolbar.i.setOnClickListener(new dk(toolbar));
        }
    }

    private boolean b(View view) {
        return (view == null || view.getParent() != this || view.getVisibility() == 8) ? false : true;
    }

    private static int c(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return ao.b(marginLayoutParams) + ao.a(marginLayoutParams);
    }

    private static int d(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.bottomMargin + marginLayoutParams.topMargin;
    }

    private void e(View view) {
        if (((LayoutParams) view.getLayoutParams()).b != 2 && view != this.b) {
            view.setVisibility(this.a != null ? 8 : 0);
        }
    }

    protected static LayoutParams n() {
        return new LayoutParams();
    }

    private void p() {
        if (this.b == null) {
            this.b = new ActionMenuView(getContext());
            this.b.a(this.k);
            this.b.a(this.E);
            this.b.a(this.I, this.J);
            LayoutParams layoutParams = new LayoutParams();
            layoutParams.a = 8388613 | (this.n & 112);
            this.b.setLayoutParams(layoutParams);
            a(this.b);
        }
    }

    private void q() {
        if (this.e == null) {
            this.e = new ImageButton(getContext(), null, b.toolbarNavigationButtonStyle);
            LayoutParams layoutParams = new LayoutParams();
            layoutParams.a = 8388611 | (this.n & 112);
            this.e.setLayoutParams(layoutParams);
        }
    }

    public final void a(int i2) {
        if (this.k != i2) {
            this.k = i2;
            if (i2 == 0) {
                this.j = getContext();
            } else {
                this.j = new ContextThemeWrapper(getContext(), i2);
            }
        }
    }

    public final void a(int i2, int i3) {
        this.t.a(i2, i3);
    }

    public final void a(Context context, int i2) {
        this.l = i2;
        if (this.c != null) {
            this.c.setTextAppearance(context, i2);
        }
    }

    public final void a(Drawable drawable) {
        if (drawable != null) {
            if (this.f == null) {
                this.f = new ImageView(getContext());
            }
            if (this.f.getParent() == null) {
                a(this.f);
                e(this.f);
            }
        } else if (!(this.f == null || this.f.getParent() == null)) {
            removeView(this.f);
        }
        if (this.f != null) {
            this.f.setImageDrawable(drawable);
        }
    }

    public final void a(i iVar, ActionMenuPresenter actionMenuPresenter) {
        if (iVar != null || this.b != null) {
            p();
            i d2 = this.b.d();
            if (d2 != iVar) {
                if (d2 != null) {
                    d2.b(this.G);
                    d2.b(this.H);
                }
                if (this.H == null) {
                    this.H = new dl(this, (byte) 0);
                }
                actionMenuPresenter.h();
                if (iVar != null) {
                    iVar.a(actionMenuPresenter, this.j);
                    iVar.a(this.H, this.j);
                } else {
                    actionMenuPresenter.a(this.j, (i) null);
                    this.H.a(this.j, (i) null);
                    actionMenuPresenter.a(true);
                    this.H.a(true);
                }
                this.b.a(this.k);
                this.b.a(actionMenuPresenter);
                this.G = actionMenuPresenter;
            }
        }
    }

    public final void a(y yVar, j jVar) {
        this.I = yVar;
        this.J = jVar;
    }

    public final void a(dm dmVar) {
        this.D = dmVar;
    }

    public final void a(View.OnClickListener onClickListener) {
        q();
        this.e.setOnClickListener(onClickListener);
    }

    public final void a(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.c == null) {
                Context context = getContext();
                this.c = new TextView(context);
                this.c.setSingleLine();
                this.c.setEllipsize(TextUtils.TruncateAt.END);
                if (this.l != 0) {
                    this.c.setTextAppearance(context, this.l);
                }
                if (this.x != 0) {
                    this.c.setTextColor(this.x);
                }
            }
            if (this.c.getParent() == null) {
                a(this.c);
                e(this.c);
            }
        } else if (!(this.c == null || this.c.getParent() == null)) {
            removeView(this.c);
        }
        if (this.c != null) {
            this.c.setText(charSequence);
        }
        this.v = charSequence;
    }

    public final void a(boolean z2) {
        this.K = z2;
        requestLayout();
    }

    public final boolean a() {
        return getVisibility() == 0 && this.b != null && this.b.a();
    }

    public final void b(Context context, int i2) {
        this.m = i2;
        if (this.d != null) {
            this.d.setTextAppearance(context, i2);
        }
    }

    public final void b(Drawable drawable) {
        if (drawable != null) {
            q();
            if (this.e.getParent() == null) {
                a(this.e);
                e(this.e);
            }
        } else if (!(this.e == null || this.e.getParent() == null)) {
            removeView(this.e);
        }
        if (this.e != null) {
            this.e.setImageDrawable(drawable);
        }
    }

    public final void b(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.d == null) {
                Context context = getContext();
                this.d = new TextView(context);
                this.d.setSingleLine();
                this.d.setEllipsize(TextUtils.TruncateAt.END);
                if (this.m != 0) {
                    this.d.setTextAppearance(context, this.m);
                }
                if (this.y != 0) {
                    this.d.setTextColor(this.y);
                }
            }
            if (this.d.getParent() == null) {
                a(this.d);
                e(this.d);
            }
        } else if (!(this.d == null || this.d.getParent() == null)) {
            removeView(this.d);
        }
        if (this.d != null) {
            this.d.setText(charSequence);
        }
        this.w = charSequence;
    }

    public final boolean b() {
        return this.b != null && this.b.g();
    }

    public final void c(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            q();
        }
        if (this.e != null) {
            this.e.setContentDescription(charSequence);
        }
    }

    public final boolean c() {
        return this.b != null && this.b.h();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof LayoutParams);
    }

    public final boolean d() {
        return this.b != null && this.b.e();
    }

    public final boolean e() {
        return this.b != null && this.b.f();
    }

    public final void f() {
        if (this.b != null) {
            this.b.i();
        }
    }

    public final boolean g() {
        return (this.H == null || this.H.b == null) ? false : true;
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    public /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return a(layoutParams);
    }

    public final void h() {
        m mVar = this.H == null ? null : this.H.b;
        if (mVar != null) {
            mVar.collapseActionView();
        }
    }

    public final CharSequence i() {
        return this.v;
    }

    public final CharSequence j() {
        return this.w;
    }

    public final CharSequence k() {
        if (this.e != null) {
            return this.e.getContentDescription();
        }
        return null;
    }

    public final Drawable l() {
        if (this.e != null) {
            return this.e.getDrawable();
        }
        return null;
    }

    public final Menu m() {
        p();
        if (this.b.d() == null) {
            i iVar = (i) this.b.c();
            if (this.H == null) {
                this.H = new dl(this, (byte) 0);
            }
            this.b.j();
            iVar.a(this.H, this.j);
        }
        return this.b.c();
    }

    public final af o() {
        if (this.F == null) {
            this.F = new bf(this, true);
        }
        return this.F;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.L);
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int a2 = ay.a(motionEvent);
        if (a2 == 9) {
            this.A = false;
        }
        if (!this.A) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (a2 == 9 && !onHoverEvent) {
                this.A = true;
            }
        }
        if (a2 == 10 || a2 == 3) {
            this.A = false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int max;
        boolean z3 = bx.h(this) == 1;
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i16 = width - paddingRight;
        int[] iArr = this.C;
        iArr[1] = 0;
        iArr[0] = 0;
        int s2 = bx.s(this);
        if (!b(this.e)) {
            i6 = paddingLeft;
        } else if (z3) {
            i16 = b(this.e, i16, iArr, s2);
            i6 = paddingLeft;
        } else {
            i6 = a(this.e, paddingLeft, iArr, s2);
        }
        if (b(this.i)) {
            if (z3) {
                i16 = b(this.i, i16, iArr, s2);
            } else {
                i6 = a(this.i, i6, iArr, s2);
            }
        }
        if (b(this.b)) {
            if (z3) {
                i6 = a(this.b, i6, iArr, s2);
            } else {
                i16 = b(this.b, i16, iArr, s2);
            }
        }
        iArr[0] = Math.max(0, this.t.a() - i6);
        iArr[1] = Math.max(0, this.t.b() - ((width - paddingRight) - i16));
        int max2 = Math.max(i6, this.t.a());
        int min = Math.min(i16, (width - paddingRight) - this.t.b());
        if (b(this.a)) {
            if (z3) {
                min = b(this.a, min, iArr, s2);
            } else {
                max2 = a(this.a, max2, iArr, s2);
            }
        }
        if (!b(this.f)) {
            i7 = min;
            i8 = max2;
        } else if (z3) {
            i7 = b(this.f, min, iArr, s2);
            i8 = max2;
        } else {
            i7 = min;
            i8 = a(this.f, max2, iArr, s2);
        }
        boolean b2 = b(this.c);
        boolean b3 = b(this.d);
        int i17 = 0;
        if (b2) {
            LayoutParams layoutParams = (LayoutParams) this.c.getLayoutParams();
            i17 = layoutParams.bottomMargin + layoutParams.topMargin + this.c.getMeasuredHeight() + 0;
        }
        if (b3) {
            LayoutParams layoutParams2 = (LayoutParams) this.d.getLayoutParams();
            i9 = layoutParams2.bottomMargin + layoutParams2.topMargin + this.d.getMeasuredHeight() + i17;
        } else {
            i9 = i17;
        }
        if (b2 || b3) {
            TextView textView = b2 ? this.c : this.d;
            TextView textView2 = b3 ? this.d : this.c;
            LayoutParams layoutParams3 = (LayoutParams) textView.getLayoutParams();
            LayoutParams layoutParams4 = (LayoutParams) textView2.getLayoutParams();
            boolean z4 = (b2 && this.c.getMeasuredWidth() > 0) || (b3 && this.d.getMeasuredWidth() > 0);
            switch (this.u & 112) {
                case 48:
                    i10 = layoutParams3.topMargin + getPaddingTop() + this.r;
                    break;
                case 80:
                    i10 = (((height - paddingBottom) - layoutParams4.bottomMargin) - this.s) - i9;
                    break;
                default:
                    int i18 = (((height - paddingTop) - paddingBottom) - i9) / 2;
                    if (i18 < layoutParams3.topMargin + this.r) {
                        max = layoutParams3.topMargin + this.r;
                    } else {
                        int i19 = (((height - paddingBottom) - i9) - i18) - paddingTop;
                        max = i19 < layoutParams3.bottomMargin + this.s ? Math.max(0, i18 - ((layoutParams4.bottomMargin + this.s) - i19)) : i18;
                    }
                    i10 = paddingTop + max;
                    break;
            }
            if (z3) {
                int i20 = (z4 ? this.p : 0) - iArr[1];
                int max3 = i7 - Math.max(0, i20);
                iArr[1] = Math.max(0, -i20);
                if (b2) {
                    int measuredWidth = max3 - this.c.getMeasuredWidth();
                    int measuredHeight = this.c.getMeasuredHeight() + i10;
                    this.c.layout(measuredWidth, i10, max3, measuredHeight);
                    int i21 = measuredWidth - this.q;
                    i10 = measuredHeight + ((LayoutParams) this.c.getLayoutParams()).bottomMargin;
                    i14 = i21;
                } else {
                    i14 = max3;
                }
                if (b3) {
                    LayoutParams layoutParams5 = (LayoutParams) this.d.getLayoutParams();
                    int i22 = layoutParams5.topMargin + i10;
                    this.d.layout(max3 - this.d.getMeasuredWidth(), i22, max3, this.d.getMeasuredHeight() + i22);
                    int i23 = layoutParams5.bottomMargin;
                    i15 = max3 - this.q;
                } else {
                    i15 = max3;
                }
                i7 = z4 ? Math.min(i14, i15) : max3;
            } else {
                int i24 = (z4 ? this.p : 0) - iArr[0];
                i8 += Math.max(0, i24);
                iArr[0] = Math.max(0, -i24);
                if (b2) {
                    int measuredWidth2 = this.c.getMeasuredWidth() + i8;
                    int measuredHeight2 = this.c.getMeasuredHeight() + i10;
                    this.c.layout(i8, i10, measuredWidth2, measuredHeight2);
                    int i25 = ((LayoutParams) this.c.getLayoutParams()).bottomMargin + measuredHeight2;
                    i11 = measuredWidth2 + this.q;
                    i12 = i25;
                } else {
                    i11 = i8;
                    i12 = i10;
                }
                if (b3) {
                    LayoutParams layoutParams6 = (LayoutParams) this.d.getLayoutParams();
                    int i26 = i12 + layoutParams6.topMargin;
                    int measuredWidth3 = this.d.getMeasuredWidth() + i8;
                    this.d.layout(i8, i26, measuredWidth3, this.d.getMeasuredHeight() + i26);
                    int i27 = layoutParams6.bottomMargin;
                    i13 = this.q + measuredWidth3;
                } else {
                    i13 = i8;
                }
                if (z4) {
                    i8 = Math.max(i11, i13);
                }
            }
        }
        a(this.B, 3);
        int size = this.B.size();
        int i28 = i8;
        for (int i29 = 0; i29 < size; i29++) {
            i28 = a((View) this.B.get(i29), i28, iArr, s2);
        }
        a(this.B, 5);
        int size2 = this.B.size();
        int i30 = 0;
        int i31 = i7;
        while (i30 < size2) {
            int b4 = b((View) this.B.get(i30), i31, iArr, s2);
            i30++;
            i31 = b4;
        }
        a(this.B, 1);
        ArrayList arrayList = this.B;
        int i32 = iArr[0];
        int i33 = iArr[1];
        int size3 = arrayList.size();
        int i34 = i32;
        int i35 = i33;
        int i36 = 0;
        int i37 = 0;
        while (i36 < size3) {
            View view = (View) arrayList.get(i36);
            LayoutParams layoutParams7 = (LayoutParams) view.getLayoutParams();
            int i38 = layoutParams7.leftMargin - i34;
            int i39 = layoutParams7.rightMargin - i35;
            int max4 = Math.max(0, i38);
            int max5 = Math.max(0, i39);
            i34 = Math.max(0, -i38);
            i35 = Math.max(0, -i39);
            i36++;
            i37 += view.getMeasuredWidth() + max4 + max5;
        }
        int i40 = ((((width - paddingLeft) - paddingRight) / 2) + paddingLeft) - (i37 / 2);
        int i41 = i40 + i37;
        if (i40 < i28) {
            i40 = i28;
        } else if (i41 > i31) {
            i40 -= i41 - i31;
        }
        int size4 = this.B.size();
        int i42 = 0;
        int i43 = i40;
        while (i42 < size4) {
            int a2 = a((View) this.B.get(i42), i43, iArr, s2);
            i42++;
            i43 = a2;
        }
        this.B.clear();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        char c2;
        char c3;
        int i4;
        int i5;
        boolean z2;
        int i6;
        int i7;
        int[] iArr = this.C;
        if (bj.a(this)) {
            c2 = 0;
            c3 = 1;
        } else {
            c2 = 1;
            c3 = 0;
        }
        int i8 = 0;
        if (b(this.e)) {
            a(this.e, i2, 0, i3, this.o);
            i8 = this.e.getMeasuredWidth() + c(this.e);
            int max = Math.max(0, this.e.getMeasuredHeight() + d(this.e));
            i4 = bj.a(0, bx.m(this.e));
            i5 = max;
        } else {
            i4 = 0;
            i5 = 0;
        }
        if (b(this.i)) {
            a(this.i, i2, 0, i3, this.o);
            i8 = this.i.getMeasuredWidth() + c(this.i);
            i5 = Math.max(i5, this.i.getMeasuredHeight() + d(this.i));
            i4 = bj.a(i4, bx.m(this.i));
        }
        int c4 = this.t.c();
        int max2 = Math.max(c4, i8) + 0;
        iArr[c3] = Math.max(0, c4 - i8);
        int i9 = 0;
        if (b(this.b)) {
            a(this.b, i2, max2, i3, this.o);
            i9 = this.b.getMeasuredWidth() + c(this.b);
            i5 = Math.max(i5, this.b.getMeasuredHeight() + d(this.b));
            i4 = bj.a(i4, bx.m(this.b));
        }
        int d2 = this.t.d();
        int max3 = max2 + Math.max(d2, i9);
        iArr[c2] = Math.max(0, d2 - i9);
        if (b(this.a)) {
            max3 += a(this.a, i2, max3, i3, 0, iArr);
            i5 = Math.max(i5, this.a.getMeasuredHeight() + d(this.a));
            i4 = bj.a(i4, bx.m(this.a));
        }
        if (b(this.f)) {
            max3 += a(this.f, i2, max3, i3, 0, iArr);
            i5 = Math.max(i5, this.f.getMeasuredHeight() + d(this.f));
            i4 = bj.a(i4, bx.m(this.f));
        }
        int childCount = getChildCount();
        int i10 = 0;
        int i11 = i4;
        int i12 = i5;
        while (i10 < childCount) {
            View childAt = getChildAt(i10);
            if (((LayoutParams) childAt.getLayoutParams()).b != 0 || !b(childAt)) {
                i6 = i11;
                i7 = i12;
            } else {
                max3 += a(childAt, i2, max3, i3, 0, iArr);
                int max4 = Math.max(i12, childAt.getMeasuredHeight() + d(childAt));
                i6 = bj.a(i11, bx.m(childAt));
                i7 = max4;
            }
            i10++;
            i11 = i6;
            i12 = i7;
        }
        int i13 = 0;
        int i14 = 0;
        int i15 = this.r + this.s;
        int i16 = this.p + this.q;
        if (b(this.c)) {
            a(this.c, i2, max3 + i16, i3, i15, iArr);
            i13 = c(this.c) + this.c.getMeasuredWidth();
            i14 = this.c.getMeasuredHeight() + d(this.c);
            i11 = bj.a(i11, bx.m(this.c));
        }
        if (b(this.d)) {
            i13 = Math.max(i13, a(this.d, i2, max3 + i16, i3, i15 + i14, iArr));
            i14 += this.d.getMeasuredHeight() + d(this.d);
            i11 = bj.a(i11, bx.m(this.d));
        }
        int max5 = Math.max(i12, i14);
        int paddingLeft = i13 + max3 + getPaddingLeft() + getPaddingRight();
        int paddingTop = max5 + getPaddingTop() + getPaddingBottom();
        int a2 = bx.a(Math.max(paddingLeft, getSuggestedMinimumWidth()), i2, -16777216 & i11);
        int a3 = bx.a(Math.max(paddingTop, getSuggestedMinimumHeight()), i3, i11 << 16);
        if (this.K) {
            int childCount2 = getChildCount();
            int i17 = 0;
            while (true) {
                if (i17 >= childCount2) {
                    z2 = true;
                    break;
                }
                View childAt2 = getChildAt(i17);
                if (b(childAt2) && childAt2.getMeasuredWidth() > 0 && childAt2.getMeasuredHeight() > 0) {
                    z2 = false;
                    break;
                }
                i17++;
            }
        } else {
            z2 = false;
        }
        if (z2) {
            a3 = 0;
        }
        setMeasuredDimension(a2, a3);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        i d2 = this.b != null ? this.b.d() : null;
        if (!(savedState.a == 0 || this.H == null || d2 == null || (findItem = d2.findItem(savedState.a)) == null)) {
            as.b(findItem);
        }
        if (savedState.b) {
            removeCallbacks(this.L);
            post(this.L);
        }
    }

    public void onRtlPropertiesChanged(int i2) {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT >= 17) {
            super.onRtlPropertiesChanged(i2);
        }
        al alVar = this.t;
        if (i2 != 1) {
            z2 = false;
        }
        alVar.a(z2);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (!(this.H == null || this.H.b == null)) {
            savedState.a = this.H.b.getItemId();
        }
        savedState.b = b();
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a2 = ay.a(motionEvent);
        if (a2 == 0) {
            this.z = false;
        }
        if (!this.z) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (a2 == 0 && !onTouchEvent) {
                this.z = true;
            }
        }
        if (a2 == 1 || a2 == 3) {
            this.z = false;
        }
        return true;
    }
}
