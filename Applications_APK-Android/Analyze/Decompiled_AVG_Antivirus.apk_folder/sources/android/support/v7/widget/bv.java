package android.support.v7.widget;

import android.util.SparseArray;
import android.util.SparseIntArray;
import java.util.ArrayList;

public final class bv {
    private SparseArray a = new SparseArray();
    private SparseIntArray b = new SparseIntArray();
    private int c = 0;

    public final cf a(int i) {
        ArrayList arrayList = (ArrayList) this.a.get(i);
        if (arrayList == null || arrayList.isEmpty()) {
            return null;
        }
        int size = arrayList.size() - 1;
        cf cfVar = (cf) arrayList.get(size);
        arrayList.remove(size);
        return cfVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(bi biVar, bi biVar2) {
        if (biVar != null) {
            this.c--;
        }
        if (this.c == 0) {
            this.a.clear();
        }
        if (biVar2 != null) {
            this.c++;
        }
    }

    public final void a(cf cfVar) {
        int i = cfVar.e;
        ArrayList arrayList = (ArrayList) this.a.get(i);
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.a.put(i, arrayList);
            if (this.b.indexOfKey(i) < 0) {
                this.b.put(i, 5);
            }
        }
        if (this.b.get(i) > arrayList.size()) {
            cfVar.q();
            arrayList.add(cfVar);
        }
    }
}
