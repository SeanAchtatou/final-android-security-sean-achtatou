package android.support.v7.widget;

import android.util.SparseArray;

public final class cc {
    int a = 0;
    /* access modifiers changed from: private */
    public int b = -1;
    private SparseArray c;
    /* access modifiers changed from: private */
    public int d = 0;
    /* access modifiers changed from: private */
    public int e = 0;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = false;

    static /* synthetic */ int a(cc ccVar, int i2) {
        int i3 = ccVar.e + i2;
        ccVar.e = i3;
        return i3;
    }

    public final boolean a() {
        return this.g;
    }

    public final boolean b() {
        return this.i;
    }

    public final int c() {
        return this.b;
    }

    public final boolean d() {
        return this.b != -1;
    }

    public final int e() {
        return this.g ? this.d - this.e : this.a;
    }

    public final String toString() {
        return "State{mTargetPosition=" + this.b + ", mData=" + this.c + ", mItemCount=" + this.a + ", mPreviousLayoutItemCount=" + this.d + ", mDeletedInvisibleItemCountSincePreviousLayout=" + this.e + ", mStructureChanged=" + this.f + ", mInPreLayout=" + this.g + ", mRunSimpleAnimations=" + this.h + ", mRunPredictiveAnimations=" + this.i + '}';
    }
}
