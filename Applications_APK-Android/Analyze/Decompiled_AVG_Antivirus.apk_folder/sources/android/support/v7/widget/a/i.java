package android.support.v7.widget.a;

import android.view.animation.Interpolator;

final class i implements Interpolator {
    i() {
    }

    public final float getInterpolation(float f) {
        float f2 = f - 1.0f;
        return (f2 * f2 * f2 * f2 * f2) + 1.0f;
    }
}
