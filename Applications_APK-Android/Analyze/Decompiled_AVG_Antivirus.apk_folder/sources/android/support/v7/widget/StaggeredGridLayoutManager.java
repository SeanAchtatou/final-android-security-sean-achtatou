package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.a.a;
import android.support.v4.view.a.af;
import android.support.v4.view.a.f;
import android.support.v4.view.a.r;
import android.support.v4.view.bx;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public final class StaggeredGridLayoutManager extends br {
    private boolean A;
    private final Runnable B;
    az a;
    az b;
    boolean c;
    int d;
    int e;
    LazySpanLookup f;
    private int g;
    private de[] h;
    /* access modifiers changed from: private */
    public int i;
    private int j;
    private af k;
    private boolean l;
    private BitSet m;
    private int n;
    private boolean o;
    private boolean p;
    private SavedState t;
    private int u;
    private int v;
    private int w;
    private final Rect x;
    private final db y;
    private boolean z;

    public class LayoutParams extends RecyclerView.LayoutParams {
        de e;
        boolean f;

        public LayoutParams() {
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public final int a() {
            if (this.e == null) {
                return -1;
            }
            return this.e.d;
        }
    }

    final class LazySpanLookup {
        int[] a;
        List b;

        class FullSpanItem implements Parcelable {
            public static final Parcelable.Creator CREATOR = new dc();
            int a;
            int b;
            int[] c;
            boolean d;

            public FullSpanItem() {
            }

            public FullSpanItem(Parcel parcel) {
                boolean z = true;
                this.a = parcel.readInt();
                this.b = parcel.readInt();
                this.d = parcel.readInt() != 1 ? false : z;
                int readInt = parcel.readInt();
                if (readInt > 0) {
                    this.c = new int[readInt];
                    parcel.readIntArray(this.c);
                }
            }

            /* access modifiers changed from: package-private */
            public final int a(int i) {
                if (this.c == null) {
                    return 0;
                }
                return this.c[i];
            }

            public int describeContents() {
                return 0;
            }

            public String toString() {
                return "FullSpanItem{mPosition=" + this.a + ", mGapDir=" + this.b + ", mHasUnwantedGapAfter=" + this.d + ", mGapPerSpan=" + Arrays.toString(this.c) + '}';
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.a);
                parcel.writeInt(this.b);
                parcel.writeInt(this.d ? 1 : 0);
                if (this.c == null || this.c.length <= 0) {
                    parcel.writeInt(0);
                    return;
                }
                parcel.writeInt(this.c.length);
                parcel.writeIntArray(this.c);
            }
        }

        /* access modifiers changed from: package-private */
        public final int a(int i) {
            if (this.b != null) {
                for (int size = this.b.size() - 1; size >= 0; size--) {
                    if (((FullSpanItem) this.b.get(size)).a >= i) {
                        this.b.remove(size);
                    }
                }
            }
            return b(i);
        }

        public final FullSpanItem a(int i, int i2, int i3) {
            if (this.b == null) {
                return null;
            }
            int size = this.b.size();
            for (int i4 = 0; i4 < size; i4++) {
                FullSpanItem fullSpanItem = (FullSpanItem) this.b.get(i4);
                if (fullSpanItem.a >= i2) {
                    return null;
                }
                if (fullSpanItem.a >= i && (i3 == 0 || fullSpanItem.b == i3 || fullSpanItem.d)) {
                    return fullSpanItem;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public final void a() {
            if (this.a != null) {
                Arrays.fill(this.a, -1);
            }
            this.b = null;
        }

        /* access modifiers changed from: package-private */
        public final void a(int i, int i2) {
            if (this.a != null && i < this.a.length) {
                c(i + i2);
                System.arraycopy(this.a, i + i2, this.a, i, (this.a.length - i) - i2);
                Arrays.fill(this.a, this.a.length - i2, this.a.length, -1);
                if (this.b != null) {
                    int i3 = i + i2;
                    for (int size = this.b.size() - 1; size >= 0; size--) {
                        FullSpanItem fullSpanItem = (FullSpanItem) this.b.get(size);
                        if (fullSpanItem.a >= i) {
                            if (fullSpanItem.a < i3) {
                                this.b.remove(size);
                            } else {
                                fullSpanItem.a -= i2;
                            }
                        }
                    }
                }
            }
        }

        public final void a(FullSpanItem fullSpanItem) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                FullSpanItem fullSpanItem2 = (FullSpanItem) this.b.get(i);
                if (fullSpanItem2.a == fullSpanItem.a) {
                    this.b.remove(i);
                }
                if (fullSpanItem2.a >= fullSpanItem.a) {
                    this.b.add(i, fullSpanItem);
                    return;
                }
            }
            this.b.add(fullSpanItem);
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0045  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0056  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final int b(int r5) {
            /*
                r4 = this;
                r1 = -1
                int[] r0 = r4.a
                if (r0 != 0) goto L_0x0007
                r0 = r1
            L_0x0006:
                return r0
            L_0x0007:
                int[] r0 = r4.a
                int r0 = r0.length
                if (r5 < r0) goto L_0x000e
                r0 = r1
                goto L_0x0006
            L_0x000e:
                java.util.List r0 = r4.b
                if (r0 == 0) goto L_0x0054
                android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem r0 = r4.d(r5)
                if (r0 == 0) goto L_0x001d
                java.util.List r2 = r4.b
                r2.remove(r0)
            L_0x001d:
                java.util.List r0 = r4.b
                int r3 = r0.size()
                r2 = 0
            L_0x0024:
                if (r2 >= r3) goto L_0x0060
                java.util.List r0 = r4.b
                java.lang.Object r0 = r0.get(r2)
                android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem r0 = (android.support.v7.widget.StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem) r0
                int r0 = r0.a
                if (r0 < r5) goto L_0x0051
            L_0x0032:
                if (r2 == r1) goto L_0x0054
                java.util.List r0 = r4.b
                java.lang.Object r0 = r0.get(r2)
                android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem r0 = (android.support.v7.widget.StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem) r0
                java.util.List r3 = r4.b
                r3.remove(r2)
                int r0 = r0.a
            L_0x0043:
                if (r0 != r1) goto L_0x0056
                int[] r0 = r4.a
                int[] r2 = r4.a
                int r2 = r2.length
                java.util.Arrays.fill(r0, r5, r2, r1)
                int[] r0 = r4.a
                int r0 = r0.length
                goto L_0x0006
            L_0x0051:
                int r2 = r2 + 1
                goto L_0x0024
            L_0x0054:
                r0 = r1
                goto L_0x0043
            L_0x0056:
                int[] r2 = r4.a
                int r3 = r0 + 1
                java.util.Arrays.fill(r2, r5, r3, r1)
                int r0 = r0 + 1
                goto L_0x0006
            L_0x0060:
                r2 = r1
                goto L_0x0032
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.StaggeredGridLayoutManager.LazySpanLookup.b(int):int");
        }

        /* access modifiers changed from: package-private */
        public final void b(int i, int i2) {
            if (this.a != null && i < this.a.length) {
                c(i + i2);
                System.arraycopy(this.a, i, this.a, i + i2, (this.a.length - i) - i2);
                Arrays.fill(this.a, i, i + i2, -1);
                if (this.b != null) {
                    for (int size = this.b.size() - 1; size >= 0; size--) {
                        FullSpanItem fullSpanItem = (FullSpanItem) this.b.get(size);
                        if (fullSpanItem.a >= i) {
                            fullSpanItem.a += i2;
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public final void c(int i) {
            if (this.a == null) {
                this.a = new int[(Math.max(i, 10) + 1)];
                Arrays.fill(this.a, -1);
            } else if (i >= this.a.length) {
                int[] iArr = this.a;
                int length = this.a.length;
                while (length <= i) {
                    length *= 2;
                }
                this.a = new int[length];
                System.arraycopy(iArr, 0, this.a, 0, iArr.length);
                Arrays.fill(this.a, iArr.length, this.a.length, -1);
            }
        }

        public final FullSpanItem d(int i) {
            if (this.b == null) {
                return null;
            }
            for (int size = this.b.size() - 1; size >= 0; size--) {
                FullSpanItem fullSpanItem = (FullSpanItem) this.b.get(size);
                if (fullSpanItem.a == i) {
                    return fullSpanItem;
                }
            }
            return null;
        }
    }

    public class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new dd();
        int a;
        int b;
        int c;
        int[] d;
        int e;
        int[] f;
        List g;
        boolean h;
        boolean i;
        boolean j;

        public SavedState() {
        }

        SavedState(Parcel parcel) {
            boolean z = true;
            this.a = parcel.readInt();
            this.b = parcel.readInt();
            this.c = parcel.readInt();
            if (this.c > 0) {
                this.d = new int[this.c];
                parcel.readIntArray(this.d);
            }
            this.e = parcel.readInt();
            if (this.e > 0) {
                this.f = new int[this.e];
                parcel.readIntArray(this.f);
            }
            this.h = parcel.readInt() == 1;
            this.i = parcel.readInt() == 1;
            this.j = parcel.readInt() != 1 ? false : z;
            this.g = parcel.readArrayList(LazySpanLookup.FullSpanItem.class.getClassLoader());
        }

        public SavedState(SavedState savedState) {
            this.c = savedState.c;
            this.a = savedState.a;
            this.b = savedState.b;
            this.d = savedState.d;
            this.e = savedState.e;
            this.f = savedState.f;
            this.h = savedState.h;
            this.i = savedState.i;
            this.j = savedState.j;
            this.g = savedState.g;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            int i3 = 1;
            parcel.writeInt(this.a);
            parcel.writeInt(this.b);
            parcel.writeInt(this.c);
            if (this.c > 0) {
                parcel.writeIntArray(this.d);
            }
            parcel.writeInt(this.e);
            if (this.e > 0) {
                parcel.writeIntArray(this.f);
            }
            parcel.writeInt(this.h ? 1 : 0);
            parcel.writeInt(this.i ? 1 : 0);
            if (!this.j) {
                i3 = 0;
            }
            parcel.writeInt(i3);
            parcel.writeList(this.g);
        }
    }

    private static int a(int i2, int i3, int i4) {
        if (i3 == 0 && i4 == 0) {
            return i2;
        }
        int mode = View.MeasureSpec.getMode(i2);
        return (mode == Integer.MIN_VALUE || mode == 1073741824) ? View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i2) - i3) - i4), mode) : i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:178:0x0321  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a(android.support.v7.widget.bw r16, android.support.v7.widget.af r17, android.support.v7.widget.cc r18) {
        /*
            r15 = this;
            java.util.BitSet r1 = r15.m
            r2 = 0
            int r3 = r15.g
            r4 = 1
            r1.set(r2, r3, r4)
            r0 = r17
            int r1 = r0.d
            r2 = 1
            if (r1 != r2) goto L_0x00f3
            r0 = r17
            int r1 = r0.f
            r0 = r17
            int r2 = r0.a
            int r1 = r1 + r2
            r2 = r1
        L_0x001a:
            r0 = r17
            int r1 = r0.d
            r15.g(r1, r2)
            boolean r1 = r15.c
            if (r1 == 0) goto L_0x00ff
            android.support.v7.widget.az r1 = r15.a
            int r1 = r1.d()
            r3 = r1
        L_0x002c:
            r1 = 0
        L_0x002d:
            r0 = r17
            int r4 = r0.b
            if (r4 < 0) goto L_0x0108
            r0 = r17
            int r4 = r0.b
            int r5 = r18.e()
            if (r4 >= r5) goto L_0x0108
            r4 = 1
        L_0x003e:
            if (r4 == 0) goto L_0x031f
            java.util.BitSet r4 = r15.m
            boolean r4 = r4.isEmpty()
            if (r4 != 0) goto L_0x031f
            r0 = r17
            int r1 = r0.b
            r0 = r16
            android.view.View r12 = r0.b(r1)
            r0 = r17
            int r1 = r0.b
            r0 = r17
            int r4 = r0.c
            int r1 = r1 + r4
            r0 = r17
            r0.b = r1
            android.view.ViewGroup$LayoutParams r1 = r12.getLayoutParams()
            android.support.v7.widget.StaggeredGridLayoutManager$LayoutParams r1 = (android.support.v7.widget.StaggeredGridLayoutManager.LayoutParams) r1
            android.support.v7.widget.cf r4 = r1.a
            int r13 = r4.e_()
            android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup r4 = r15.f
            int[] r5 = r4.a
            if (r5 == 0) goto L_0x0076
            int[] r5 = r4.a
            int r5 = r5.length
            if (r13 < r5) goto L_0x010b
        L_0x0076:
            r4 = -1
            r5 = r4
        L_0x0078:
            r4 = -1
            if (r5 != r4) goto L_0x0112
            r4 = 1
            r11 = r4
        L_0x007d:
            if (r11 == 0) goto L_0x0191
            boolean r4 = r1.f
            if (r4 == 0) goto L_0x0116
            android.support.v7.widget.de[] r4 = r15.h
            r5 = 0
            r7 = r4[r5]
        L_0x0088:
            android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup r4 = r15.f
            r4.c(r13)
            int[] r4 = r4.a
            int r5 = r7.d
            r4[r13] = r5
        L_0x0093:
            r1.e = r7
            r0 = r17
            int r4 = r0.d
            r5 = 1
            if (r4 != r5) goto L_0x0197
            r15.c(r12)
        L_0x009f:
            boolean r4 = r1.f
            if (r4 == 0) goto L_0x01ab
            int r4 = r15.i
            r5 = 1
            if (r4 != r5) goto L_0x019c
            int r4 = r15.u
            int r5 = r1.height
            int r6 = r15.w
            int r5 = e(r5, r6)
            r15.a(r12, r4, r5)
        L_0x00b5:
            r0 = r17
            int r4 = r0.d
            r5 = 1
            if (r4 != r5) goto L_0x023f
            boolean r4 = r1.f
            if (r4 == 0) goto L_0x01ce
            int r4 = r15.i(r3)
        L_0x00c4:
            android.support.v7.widget.az r5 = r15.a
            int r5 = r5.c(r12)
            int r6 = r4 + r5
            if (r11 == 0) goto L_0x0360
            boolean r5 = r1.f
            if (r5 == 0) goto L_0x0360
            android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem r8 = new android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem
            r8.<init>()
            int r5 = r15.g
            int[] r5 = new int[r5]
            r8.c = r5
            r5 = 0
        L_0x00de:
            int r9 = r15.g
            if (r5 >= r9) goto L_0x01d4
            int[] r9 = r8.c
            android.support.v7.widget.de[] r10 = r15.h
            r10 = r10[r5]
            int r10 = r10.b(r4)
            int r10 = r4 - r10
            r9[r5] = r10
            int r5 = r5 + 1
            goto L_0x00de
        L_0x00f3:
            r0 = r17
            int r1 = r0.e
            r0 = r17
            int r2 = r0.a
            int r1 = r1 - r2
            r2 = r1
            goto L_0x001a
        L_0x00ff:
            android.support.v7.widget.az r1 = r15.a
            int r1 = r1.c()
            r3 = r1
            goto L_0x002c
        L_0x0108:
            r4 = 0
            goto L_0x003e
        L_0x010b:
            int[] r4 = r4.a
            r4 = r4[r13]
            r5 = r4
            goto L_0x0078
        L_0x0112:
            r4 = 0
            r11 = r4
            goto L_0x007d
        L_0x0116:
            r0 = r17
            int r4 = r0.d
            int r5 = r15.i
            if (r5 != 0) goto L_0x0158
            r5 = -1
            if (r4 != r5) goto L_0x0154
            r4 = 1
        L_0x0122:
            boolean r5 = r15.c
            if (r4 == r5) goto L_0x0156
            r4 = 1
        L_0x0127:
            if (r4 == 0) goto L_0x016f
            int r4 = r15.g
            int r5 = r4 + -1
            r6 = -1
            r4 = -1
        L_0x012f:
            r0 = r17
            int r7 = r0.d
            r8 = 1
            if (r7 != r8) goto L_0x0174
            r7 = 0
            r9 = 2147483647(0x7fffffff, float:NaN)
            android.support.v7.widget.az r8 = r15.a
            int r14 = r8.c()
            r10 = r5
        L_0x0141:
            if (r10 == r6) goto L_0x0088
            android.support.v7.widget.de[] r5 = r15.h
            r5 = r5[r10]
            int r8 = r5.b(r14)
            if (r8 >= r9) goto L_0x0368
            r7 = r8
        L_0x014e:
            int r8 = r10 + r4
            r10 = r8
            r9 = r7
            r7 = r5
            goto L_0x0141
        L_0x0154:
            r4 = 0
            goto L_0x0122
        L_0x0156:
            r4 = 0
            goto L_0x0127
        L_0x0158:
            r5 = -1
            if (r4 != r5) goto L_0x0169
            r4 = 1
        L_0x015c:
            boolean r5 = r15.c
            if (r4 != r5) goto L_0x016b
            r4 = 1
        L_0x0161:
            boolean r5 = r15.j()
            if (r4 != r5) goto L_0x016d
            r4 = 1
            goto L_0x0127
        L_0x0169:
            r4 = 0
            goto L_0x015c
        L_0x016b:
            r4 = 0
            goto L_0x0161
        L_0x016d:
            r4 = 0
            goto L_0x0127
        L_0x016f:
            r5 = 0
            int r6 = r15.g
            r4 = 1
            goto L_0x012f
        L_0x0174:
            r7 = 0
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
            android.support.v7.widget.az r8 = r15.a
            int r14 = r8.d()
            r10 = r5
        L_0x017e:
            if (r10 == r6) goto L_0x0088
            android.support.v7.widget.de[] r5 = r15.h
            r5 = r5[r10]
            int r8 = r5.a(r14)
            if (r8 <= r9) goto L_0x0364
            r7 = r8
        L_0x018b:
            int r8 = r10 + r4
            r10 = r8
            r9 = r7
            r7 = r5
            goto L_0x017e
        L_0x0191:
            android.support.v7.widget.de[] r4 = r15.h
            r7 = r4[r5]
            goto L_0x0093
        L_0x0197:
            r15.d(r12)
            goto L_0x009f
        L_0x019c:
            int r4 = r1.width
            int r5 = r15.v
            int r4 = e(r4, r5)
            int r5 = r15.u
            r15.a(r12, r4, r5)
            goto L_0x00b5
        L_0x01ab:
            int r4 = r15.i
            r5 = 1
            if (r4 != r5) goto L_0x01bf
            int r4 = r15.v
            int r5 = r1.height
            int r6 = r15.w
            int r5 = e(r5, r6)
            r15.a(r12, r4, r5)
            goto L_0x00b5
        L_0x01bf:
            int r4 = r1.width
            int r5 = r15.v
            int r4 = e(r4, r5)
            int r5 = r15.w
            r15.a(r12, r4, r5)
            goto L_0x00b5
        L_0x01ce:
            int r4 = r7.b(r3)
            goto L_0x00c4
        L_0x01d4:
            r5 = -1
            r8.b = r5
            r8.a = r13
            android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup r5 = r15.f
            r5.a(r8)
            r5 = r4
            r4 = r6
        L_0x01e0:
            boolean r6 = r1.f
            if (r6 == 0) goto L_0x0224
            r0 = r17
            int r6 = r0.c
            r8 = -1
            if (r6 != r8) goto L_0x0224
            if (r11 != 0) goto L_0x0221
            r0 = r17
            int r6 = r0.d
            r8 = 1
            if (r6 != r8) goto L_0x028f
            android.support.v7.widget.de[] r6 = r15.h
            r8 = 0
            r6 = r6[r8]
            r8 = -2147483648(0xffffffff80000000, float:-0.0)
            int r8 = r6.b(r8)
            r6 = 1
        L_0x0200:
            int r9 = r15.g
            if (r6 >= r9) goto L_0x028b
            android.support.v7.widget.de[] r9 = r15.h
            r9 = r9[r6]
            r10 = -2147483648(0xffffffff80000000, float:-0.0)
            int r9 = r9.b(r10)
            if (r9 == r8) goto L_0x0287
            r6 = 0
        L_0x0211:
            if (r6 != 0) goto L_0x028d
            r6 = 1
        L_0x0214:
            if (r6 == 0) goto L_0x0224
            android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup r6 = r15.f
            android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem r6 = r6.d(r13)
            if (r6 == 0) goto L_0x0221
            r8 = 1
            r6.d = r8
        L_0x0221:
            r6 = 1
            r15.z = r6
        L_0x0224:
            r0 = r17
            int r6 = r0.d
            r8 = 1
            if (r6 != r8) goto L_0x02ec
            boolean r6 = r1.f
            if (r6 == 0) goto L_0x02b9
            int r6 = r15.g
            int r6 = r6 + -1
        L_0x0233:
            if (r6 < 0) goto L_0x02be
            android.support.v7.widget.de[] r8 = r15.h
            r8 = r8[r6]
            r8.b(r12)
            int r6 = r6 + -1
            goto L_0x0233
        L_0x023f:
            boolean r4 = r1.f
            if (r4 == 0) goto L_0x0275
            int r4 = r15.h(r3)
        L_0x0247:
            android.support.v7.widget.az r5 = r15.a
            int r5 = r5.c(r12)
            int r6 = r4 - r5
            if (r11 == 0) goto L_0x0284
            boolean r5 = r1.f
            if (r5 == 0) goto L_0x0284
            android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem r8 = new android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem
            r8.<init>()
            int r5 = r15.g
            int[] r5 = new int[r5]
            r8.c = r5
            r5 = 0
        L_0x0261:
            int r9 = r15.g
            if (r5 >= r9) goto L_0x027a
            int[] r9 = r8.c
            android.support.v7.widget.de[] r10 = r15.h
            r10 = r10[r5]
            int r10 = r10.a(r4)
            int r10 = r10 - r4
            r9[r5] = r10
            int r5 = r5 + 1
            goto L_0x0261
        L_0x0275:
            int r4 = r7.a(r3)
            goto L_0x0247
        L_0x027a:
            r5 = 1
            r8.b = r5
            r8.a = r13
            android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup r5 = r15.f
            r5.a(r8)
        L_0x0284:
            r5 = r6
            goto L_0x01e0
        L_0x0287:
            int r6 = r6 + 1
            goto L_0x0200
        L_0x028b:
            r6 = 1
            goto L_0x0211
        L_0x028d:
            r6 = 0
            goto L_0x0214
        L_0x028f:
            android.support.v7.widget.de[] r6 = r15.h
            r8 = 0
            r6 = r6[r8]
            r8 = -2147483648(0xffffffff80000000, float:-0.0)
            int r8 = r6.a(r8)
            r6 = 1
        L_0x029b:
            int r9 = r15.g
            if (r6 >= r9) goto L_0x02b4
            android.support.v7.widget.de[] r9 = r15.h
            r9 = r9[r6]
            r10 = -2147483648(0xffffffff80000000, float:-0.0)
            int r9 = r9.a(r10)
            if (r9 == r8) goto L_0x02b1
            r6 = 0
        L_0x02ac:
            if (r6 != 0) goto L_0x02b6
            r6 = 1
            goto L_0x0214
        L_0x02b1:
            int r6 = r6 + 1
            goto L_0x029b
        L_0x02b4:
            r6 = 1
            goto L_0x02ac
        L_0x02b6:
            r6 = 0
            goto L_0x0214
        L_0x02b9:
            android.support.v7.widget.de r6 = r1.e
            r6.b(r12)
        L_0x02be:
            boolean r6 = r1.f
            if (r6 == 0) goto L_0x0306
            android.support.v7.widget.az r6 = r15.b
            int r6 = r6.c()
        L_0x02c8:
            android.support.v7.widget.az r8 = r15.b
            int r8 = r8.c(r12)
            int r8 = r8 + r6
            int r9 = r15.i
            r10 = 1
            if (r9 != r10) goto L_0x0313
            b(r12, r6, r5, r8, r4)
        L_0x02d7:
            boolean r1 = r1.f
            if (r1 == 0) goto L_0x0317
            android.support.v7.widget.af r1 = r15.k
            int r1 = r1.d
            r15.g(r1, r2)
        L_0x02e2:
            android.support.v7.widget.af r1 = r15.k
            r0 = r16
            r15.a(r0, r1)
            r1 = 1
            goto L_0x002d
        L_0x02ec:
            boolean r6 = r1.f
            if (r6 == 0) goto L_0x0300
            int r6 = r15.g
            int r6 = r6 + -1
        L_0x02f4:
            if (r6 < 0) goto L_0x02be
            android.support.v7.widget.de[] r8 = r15.h
            r8 = r8[r6]
            r8.a(r12)
            int r6 = r6 + -1
            goto L_0x02f4
        L_0x0300:
            android.support.v7.widget.de r6 = r1.e
            r6.a(r12)
            goto L_0x02be
        L_0x0306:
            int r6 = r7.d
            int r8 = r15.j
            int r6 = r6 * r8
            android.support.v7.widget.az r8 = r15.b
            int r8 = r8.c()
            int r6 = r6 + r8
            goto L_0x02c8
        L_0x0313:
            b(r12, r5, r6, r4, r8)
            goto L_0x02d7
        L_0x0317:
            android.support.v7.widget.af r1 = r15.k
            int r1 = r1.d
            r15.a(r7, r1, r2)
            goto L_0x02e2
        L_0x031f:
            if (r1 != 0) goto L_0x0328
            android.support.v7.widget.af r1 = r15.k
            r0 = r16
            r15.a(r0, r1)
        L_0x0328:
            android.support.v7.widget.af r1 = r15.k
            int r1 = r1.d
            r2 = -1
            if (r1 != r2) goto L_0x034c
            android.support.v7.widget.az r1 = r15.a
            int r1 = r1.c()
            int r1 = r15.h(r1)
            android.support.v7.widget.az r2 = r15.a
            int r2 = r2.c()
            int r1 = r2 - r1
        L_0x0341:
            if (r1 <= 0) goto L_0x035e
            r0 = r17
            int r2 = r0.a
            int r1 = java.lang.Math.min(r2, r1)
        L_0x034b:
            return r1
        L_0x034c:
            android.support.v7.widget.az r1 = r15.a
            int r1 = r1.d()
            int r1 = r15.i(r1)
            android.support.v7.widget.az r2 = r15.a
            int r2 = r2.d()
            int r1 = r1 - r2
            goto L_0x0341
        L_0x035e:
            r1 = 0
            goto L_0x034b
        L_0x0360:
            r5 = r4
            r4 = r6
            goto L_0x01e0
        L_0x0364:
            r5 = r7
            r7 = r9
            goto L_0x018b
        L_0x0368:
            r5 = r7
            r7 = r9
            goto L_0x014e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.af, android.support.v7.widget.cc):int");
    }

    private View a(boolean z2) {
        h();
        int c2 = this.a.c();
        int d2 = this.a.d();
        int o2 = o();
        View view = null;
        int i2 = 0;
        while (i2 < o2) {
            View d3 = d(i2);
            int a2 = this.a.a(d3);
            if (this.a.b(d3) > c2 && a2 < d2) {
                if (a2 >= c2 || !z2) {
                    return d3;
                }
                if (view == null) {
                    i2++;
                    view = d3;
                }
            }
            d3 = view;
            i2++;
            view = d3;
        }
        return view;
    }

    private void a(int i2) {
        int i3 = 1;
        this.k.d = i2;
        af afVar = this.k;
        if (this.c != (i2 == -1)) {
            i3 = -1;
        }
        afVar.c = i3;
    }

    private void a(int i2, cc ccVar) {
        int i3;
        int i4;
        int c2;
        boolean z2 = false;
        this.k.a = 0;
        this.k.b = i2;
        if (!n() || (c2 = ccVar.c()) == -1) {
            i3 = 0;
            i4 = 0;
        } else {
            if (this.c == (c2 < i2)) {
                i3 = this.a.f();
                i4 = 0;
            } else {
                i4 = this.a.f();
                i3 = 0;
            }
        }
        if (this.r != null && this.r.m) {
            z2 = true;
        }
        if (z2) {
            this.k.e = this.a.c() - i4;
            this.k.f = i3 + this.a.d();
            return;
        }
        this.k.f = i3 + this.a.e();
        this.k.e = -i4;
    }

    private void a(bw bwVar, int i2) {
        while (o() > 0) {
            View d2 = d(0);
            if (this.a.b(d2) <= i2) {
                LayoutParams layoutParams = (LayoutParams) d2.getLayoutParams();
                if (layoutParams.f) {
                    int i3 = 0;
                    while (i3 < this.g) {
                        if (this.h[i3].f.size() != 1) {
                            i3++;
                        } else {
                            return;
                        }
                    }
                    for (int i4 = 0; i4 < this.g; i4++) {
                        this.h[i4].e();
                    }
                } else if (layoutParams.e.f.size() != 1) {
                    layoutParams.e.e();
                } else {
                    return;
                }
                a(d2, bwVar);
            } else {
                return;
            }
        }
    }

    private void a(bw bwVar, af afVar) {
        int i2 = 1;
        if (afVar.a == 0) {
            if (afVar.d == -1) {
                b(bwVar, afVar.f);
            } else {
                a(bwVar, afVar.e);
            }
        } else if (afVar.d == -1) {
            int i3 = afVar.e;
            int i4 = afVar.e;
            int a2 = this.h[0].a(i4);
            while (i2 < this.g) {
                int a3 = this.h[i2].a(i4);
                if (a3 > a2) {
                    a2 = a3;
                }
                i2++;
            }
            int i5 = i3 - a2;
            b(bwVar, i5 < 0 ? afVar.f : afVar.f - Math.min(i5, afVar.a));
        } else {
            int i6 = afVar.f;
            int b2 = this.h[0].b(i6);
            while (i2 < this.g) {
                int b3 = this.h[i2].b(i6);
                if (b3 < b2) {
                    b2 = b3;
                }
                i2++;
            }
            int i7 = b2 - afVar.f;
            a(bwVar, i7 < 0 ? afVar.e : Math.min(i7, afVar.a) + afVar.e);
        }
    }

    private void a(bw bwVar, cc ccVar, boolean z2) {
        int d2 = this.a.d() - i(this.a.d());
        if (d2 > 0) {
            int i2 = d2 - (-d(-d2, bwVar, ccVar));
            if (z2 && i2 > 0) {
                this.a.a(i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    private void a(de deVar, int i2, int i3) {
        int i4 = deVar.c;
        if (i2 == -1) {
            if (i4 + deVar.a() <= i3) {
                this.m.set(deVar.d, false);
            }
        } else if (deVar.b() - i4 >= i3) {
            this.m.set(deVar.d, false);
        }
    }

    private void a(View view, int i2, int i3) {
        a(view, this.x);
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        view.measure(a(i2, layoutParams.leftMargin + this.x.left, layoutParams.rightMargin + this.x.right), a(i3, layoutParams.topMargin + this.x.top, layoutParams.bottomMargin + this.x.bottom));
    }

    private View b(boolean z2) {
        h();
        int c2 = this.a.c();
        int d2 = this.a.d();
        View view = null;
        int o2 = o() - 1;
        while (o2 >= 0) {
            View d3 = d(o2);
            int a2 = this.a.a(d3);
            int b2 = this.a.b(d3);
            if (b2 > c2 && a2 < d2) {
                if (b2 <= d2 || !z2) {
                    return d3;
                }
                if (view == null) {
                    o2--;
                    view = d3;
                }
            }
            d3 = view;
            o2--;
            view = d3;
        }
        return view;
    }

    private void b(int i2, int i3, int i4) {
        int i5;
        int i6;
        int y2 = this.c ? y() : z();
        if (i4 != 8) {
            i5 = i2 + i3;
            i6 = i2;
        } else if (i2 < i3) {
            i5 = i3 + 1;
            i6 = i2;
        } else {
            i5 = i2 + 1;
            i6 = i3;
        }
        this.f.b(i6);
        switch (i4) {
            case 1:
                this.f.b(i2, i3);
                break;
            case 2:
                this.f.a(i2, i3);
                break;
            case 8:
                this.f.a(i2, 1);
                this.f.b(i3, 1);
                break;
        }
        if (i5 > y2) {
            if (i6 <= (this.c ? z() : y())) {
                k();
            }
        }
    }

    private void b(bw bwVar, int i2) {
        int o2 = o() - 1;
        while (o2 >= 0) {
            View d2 = d(o2);
            if (this.a.a(d2) >= i2) {
                LayoutParams layoutParams = (LayoutParams) d2.getLayoutParams();
                if (layoutParams.f) {
                    int i3 = 0;
                    while (i3 < this.g) {
                        if (this.h[i3].f.size() != 1) {
                            i3++;
                        } else {
                            return;
                        }
                    }
                    for (int i4 = 0; i4 < this.g; i4++) {
                        this.h[i4].d();
                    }
                } else if (layoutParams.e.f.size() != 1) {
                    layoutParams.e.d();
                } else {
                    return;
                }
                a(d2, bwVar);
                o2--;
            } else {
                return;
            }
        }
    }

    private void b(bw bwVar, cc ccVar, boolean z2) {
        int h2 = h(this.a.c()) - this.a.c();
        if (h2 > 0) {
            int d2 = h2 - d(h2, bwVar, ccVar);
            if (z2 && d2 > 0) {
                this.a.a(-d2);
            }
        }
    }

    private static void b(View view, int i2, int i3, int i4, int i5) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        a(view, layoutParams.leftMargin + i2, layoutParams.topMargin + i3, i4 - layoutParams.rightMargin, i5 - layoutParams.bottomMargin);
    }

    private int d(int i2, bw bwVar, cc ccVar) {
        int i3;
        int z2;
        h();
        if (i2 > 0) {
            i3 = 1;
            z2 = y();
        } else {
            i3 = -1;
            z2 = z();
        }
        a(z2, ccVar);
        a(i3);
        this.k.b = z2 + this.k.c;
        int abs = Math.abs(i2);
        this.k.a = abs;
        int a2 = a(bwVar, this.k, ccVar);
        if (abs >= a2) {
            i2 = i2 < 0 ? -a2 : a2;
        }
        this.a.a(-i2);
        this.o = this.c;
        return i2;
    }

    private static int e(int i2, int i3) {
        return i2 < 0 ? i3 : View.MeasureSpec.makeMeasureSpec(i2, 1073741824);
    }

    private int g(cc ccVar) {
        boolean z2 = true;
        if (o() == 0) {
            return 0;
        }
        h();
        az azVar = this.a;
        View a2 = a(!this.A);
        if (this.A) {
            z2 = false;
        }
        return ci.a(ccVar, azVar, a2, b(z2), this, this.A, this.c);
    }

    private View g() {
        int i2;
        boolean z2;
        boolean z3;
        int o2 = o() - 1;
        BitSet bitSet = new BitSet(this.g);
        bitSet.set(0, this.g, true);
        char c2 = (this.i != 1 || !j()) ? (char) 65535 : 1;
        if (this.c) {
            i2 = -1;
        } else {
            i2 = o2 + 1;
            o2 = 0;
        }
        int i3 = o2 < i2 ? 1 : -1;
        for (int i4 = o2; i4 != i2; i4 += i3) {
            View d2 = d(i4);
            LayoutParams layoutParams = (LayoutParams) d2.getLayoutParams();
            if (bitSet.get(layoutParams.e.d)) {
                de deVar = layoutParams.e;
                if (this.c) {
                    if (deVar.b() < this.a.d()) {
                        z3 = true;
                    }
                    z3 = false;
                } else {
                    if (deVar.a() > this.a.c()) {
                        z3 = true;
                    }
                    z3 = false;
                }
                if (z3) {
                    return d2;
                }
                bitSet.clear(layoutParams.e.d);
            }
            if (!layoutParams.f && i4 + i3 != i2) {
                View d3 = d(i4 + i3);
                if (this.c) {
                    int b2 = this.a.b(d2);
                    int b3 = this.a.b(d3);
                    if (b2 < b3) {
                        return d2;
                    }
                    if (b2 == b3) {
                        z2 = true;
                    }
                    z2 = false;
                } else {
                    int a2 = this.a.a(d2);
                    int a3 = this.a.a(d3);
                    if (a2 > a3) {
                        return d2;
                    }
                    if (a2 == a3) {
                        z2 = true;
                    }
                    z2 = false;
                }
                if (!z2) {
                    continue;
                } else {
                    if ((layoutParams.e.d - ((LayoutParams) d3.getLayoutParams()).e.d < 0) != (c2 < 0)) {
                        return d2;
                    }
                }
            }
        }
        return null;
    }

    private void g(int i2, int i3) {
        for (int i4 = 0; i4 < this.g; i4++) {
            if (!this.h[i4].f.isEmpty()) {
                a(this.h[i4], i2, i3);
            }
        }
    }

    private int h(int i2) {
        int a2 = this.h[0].a(i2);
        for (int i3 = 1; i3 < this.g; i3++) {
            int a3 = this.h[i3].a(i2);
            if (a3 < a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    private int h(cc ccVar) {
        boolean z2 = true;
        if (o() == 0) {
            return 0;
        }
        h();
        az azVar = this.a;
        View a2 = a(!this.A);
        if (this.A) {
            z2 = false;
        }
        return ci.a(ccVar, azVar, a2, b(z2), this, this.A);
    }

    private void h() {
        if (this.a == null) {
            this.a = az.a(this, this.i);
            this.b = az.a(this, 1 - this.i);
            this.k = new af();
        }
    }

    private int i(int i2) {
        int b2 = this.h[0].b(i2);
        for (int i3 = 1; i3 < this.g; i3++) {
            int b3 = this.h[i3].b(i2);
            if (b3 > b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private int i(cc ccVar) {
        boolean z2 = true;
        if (o() == 0) {
            return 0;
        }
        h();
        az azVar = this.a;
        View a2 = a(!this.A);
        if (this.A) {
            z2 = false;
        }
        return ci.b(ccVar, azVar, a2, b(z2), this, this.A);
    }

    private void i() {
        boolean z2 = true;
        if (this.i == 1 || !j()) {
            z2 = this.l;
        } else if (this.l) {
            z2 = false;
        }
        this.c = z2;
    }

    /* access modifiers changed from: private */
    public int j(int i2) {
        if (o() == 0) {
            return this.c ? 1 : -1;
        }
        return (i2 < z()) != this.c ? -1 : 1;
    }

    private boolean j() {
        return bx.h(this.r) == 1;
    }

    private int y() {
        int o2 = o();
        if (o2 == 0) {
            return 0;
        }
        return e(d(o2 - 1));
    }

    private int z() {
        if (o() == 0) {
            return 0;
        }
        return e(d(0));
    }

    public final int a(int i2, bw bwVar, cc ccVar) {
        return d(i2, bwVar, ccVar);
    }

    public final int a(bw bwVar, cc ccVar) {
        return this.i == 0 ? this.g : super.a(bwVar, ccVar);
    }

    public final int a(cc ccVar) {
        return g(ccVar);
    }

    public final RecyclerView.LayoutParams a(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    public final RecyclerView.LayoutParams a(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    public final void a() {
        this.f.a();
        k();
    }

    public final void a(int i2, int i3) {
        b(i2, i3, 1);
    }

    public final void a(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.t = (SavedState) parcelable;
            k();
        }
    }

    public final void a(RecyclerView recyclerView, int i2) {
        da daVar = new da(this, recyclerView.getContext());
        daVar.b(i2);
        a(daVar);
    }

    public final void a(RecyclerView recyclerView, bw bwVar) {
        a(this.B);
        for (int i2 = 0; i2 < this.g; i2++) {
            this.h[i2].c();
        }
    }

    public final void a(bw bwVar, cc ccVar, View view, f fVar) {
        int i2;
        int i3;
        int i4 = 1;
        int i5 = -1;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            super.a(view, fVar);
            return;
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        if (this.i == 0) {
            int a2 = layoutParams2.a();
            if (layoutParams2.f) {
                i4 = this.g;
            }
            i2 = a2;
            i3 = i4;
            i4 = -1;
        } else {
            int a3 = layoutParams2.a();
            if (layoutParams2.f) {
                i4 = this.g;
                i2 = -1;
                i5 = a3;
                i3 = -1;
            } else {
                i2 = -1;
                i5 = a3;
                i3 = -1;
            }
        }
        fVar.b(r.a(i2, i3, i5, i4, layoutParams2.f));
    }

    public final void a(AccessibilityEvent accessibilityEvent) {
        super.a(accessibilityEvent);
        if (o() > 0) {
            af a2 = a.a(accessibilityEvent);
            View a3 = a(false);
            View b2 = b(false);
            if (a3 != null && b2 != null) {
                int e2 = e(a3);
                int e3 = e(b2);
                if (e2 < e3) {
                    a2.b(e2);
                    a2.c(e3);
                    return;
                }
                a2.b(e3);
                a2.c(e2);
            }
        }
    }

    public final void a(String str) {
        if (this.t == null) {
            super.a(str);
        }
    }

    public final boolean a(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public final int b(int i2, bw bwVar, cc ccVar) {
        return d(i2, bwVar, ccVar);
    }

    public final int b(bw bwVar, cc ccVar) {
        return this.i == 1 ? this.g : super.b(bwVar, ccVar);
    }

    public final int b(cc ccVar) {
        return g(ccVar);
    }

    public final RecyclerView.LayoutParams b() {
        return new LayoutParams();
    }

    public final void b(int i2, int i3) {
        b(i2, i3, 2);
    }

    public final int c(cc ccVar) {
        return h(ccVar);
    }

    public final void c(int i2) {
        if (!(this.t == null || this.t.a == i2)) {
            SavedState savedState = this.t;
            savedState.d = null;
            savedState.c = 0;
            savedState.a = -1;
            savedState.b = -1;
        }
        this.d = i2;
        this.e = Integer.MIN_VALUE;
        k();
    }

    public final void c(int i2, int i3) {
        b(i2, i3, 4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):void
     arg types: [android.support.v7.widget.bw, android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.a(int, int, int):int
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.af, android.support.v7.widget.cc):int
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.de, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.view.View, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc):int
      android.support.v7.widget.br.a(android.view.View, int, boolean):void
      android.support.v7.widget.br.a(int, android.support.v7.widget.bw, android.support.v7.widget.cc):int
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.b(android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):void
     arg types: [android.support.v7.widget.bw, android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.b(int, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.b(int, android.support.v7.widget.bw, android.support.v7.widget.cc):int
      android.support.v7.widget.br.b(int, android.support.v7.widget.bw, android.support.v7.widget.cc):int
      android.support.v7.widget.StaggeredGridLayoutManager.b(android.support.v7.widget.bw, android.support.v7.widget.cc, boolean):void */
    public final void c(bw bwVar, cc ccVar) {
        boolean z2;
        int i2;
        boolean z3 = true;
        h();
        db dbVar = this.y;
        dbVar.a = -1;
        dbVar.b = Integer.MIN_VALUE;
        dbVar.c = false;
        dbVar.d = false;
        if (!(this.t == null && this.d == -1) && ccVar.e() == 0) {
            c(bwVar);
            return;
        }
        if (this.t != null) {
            if (this.t.c > 0) {
                if (this.t.c == this.g) {
                    for (int i3 = 0; i3 < this.g; i3++) {
                        this.h[i3].c();
                        int i4 = this.t.d[i3];
                        if (i4 != Integer.MIN_VALUE) {
                            i4 = this.t.i ? i4 + this.a.d() : i4 + this.a.c();
                        }
                        this.h[i3].c(i4);
                    }
                } else {
                    SavedState savedState = this.t;
                    savedState.d = null;
                    savedState.c = 0;
                    savedState.e = 0;
                    savedState.f = null;
                    savedState.g = null;
                    this.t.a = this.t.b;
                }
            }
            this.p = this.t.j;
            boolean z4 = this.t.h;
            a((String) null);
            if (!(this.t == null || this.t.h == z4)) {
                this.t.h = z4;
            }
            this.l = z4;
            k();
            i();
            if (this.t.a != -1) {
                this.d = this.t.a;
                dbVar.c = this.t.i;
            } else {
                dbVar.c = this.c;
            }
            if (this.t.e > 1) {
                this.f.a = this.t.f;
                this.f.b = this.t.g;
            }
        } else {
            i();
            dbVar.c = this.c;
        }
        if (ccVar.a() || this.d == -1) {
            z2 = false;
        } else if (this.d < 0 || this.d >= ccVar.e()) {
            this.d = -1;
            this.e = Integer.MIN_VALUE;
            z2 = false;
        } else {
            if (this.t == null || this.t.a == -1 || this.t.c <= 0) {
                View b2 = b(this.d);
                if (b2 != null) {
                    dbVar.a = this.c ? y() : z();
                    if (this.e != Integer.MIN_VALUE) {
                        if (dbVar.c) {
                            dbVar.b = (this.a.d() - this.e) - this.a.b(b2);
                        } else {
                            dbVar.b = (this.a.c() + this.e) - this.a.a(b2);
                        }
                        z2 = true;
                    } else if (this.a.c(b2) > this.a.f()) {
                        dbVar.b = dbVar.c ? this.a.d() : this.a.c();
                    } else {
                        int a2 = this.a.a(b2) - this.a.c();
                        if (a2 < 0) {
                            dbVar.b = -a2;
                        } else {
                            int d2 = this.a.d() - this.a.b(b2);
                            if (d2 < 0) {
                                dbVar.b = d2;
                            } else {
                                dbVar.b = Integer.MIN_VALUE;
                            }
                        }
                    }
                } else {
                    dbVar.a = this.d;
                    if (this.e == Integer.MIN_VALUE) {
                        dbVar.c = j(dbVar.a) == 1;
                        dbVar.b = dbVar.c ? dbVar.e.a.d() : dbVar.e.a.c();
                    } else {
                        int i5 = this.e;
                        if (dbVar.c) {
                            dbVar.b = dbVar.e.a.d() - i5;
                        } else {
                            dbVar.b = i5 + dbVar.e.a.c();
                        }
                    }
                    dbVar.d = true;
                }
            } else {
                dbVar.b = Integer.MIN_VALUE;
                dbVar.a = this.d;
            }
            z2 = true;
        }
        if (!z2) {
            if (!this.o) {
                int e2 = ccVar.e();
                int o2 = o();
                int i6 = 0;
                while (true) {
                    if (i6 < o2) {
                        i2 = e(d(i6));
                        if (i2 >= 0 && i2 < e2) {
                            break;
                        }
                        i6++;
                    } else {
                        i2 = 0;
                        break;
                    }
                }
            } else {
                int e3 = ccVar.e();
                int o3 = o() - 1;
                while (true) {
                    if (o3 >= 0) {
                        i2 = e(d(o3));
                        if (i2 >= 0 && i2 < e3) {
                            break;
                        }
                        o3--;
                    } else {
                        i2 = 0;
                        break;
                    }
                }
            }
            dbVar.a = i2;
            dbVar.b = Integer.MIN_VALUE;
        }
        if (this.t == null && !(dbVar.c == this.o && j() == this.p)) {
            this.f.a();
            dbVar.d = true;
        }
        if (o() > 0 && (this.t == null || this.t.c <= 0)) {
            if (dbVar.d) {
                for (int i7 = 0; i7 < this.g; i7++) {
                    this.h[i7].c();
                    if (dbVar.b != Integer.MIN_VALUE) {
                        this.h[i7].c(dbVar.b);
                    }
                }
            } else {
                for (int i8 = 0; i8 < this.g; i8++) {
                    de deVar = this.h[i8];
                    boolean z5 = this.c;
                    int i9 = dbVar.b;
                    int b3 = z5 ? deVar.b(Integer.MIN_VALUE) : deVar.a(Integer.MIN_VALUE);
                    deVar.c();
                    if (b3 != Integer.MIN_VALUE && ((!z5 || b3 >= deVar.e.a.d()) && (z5 || b3 <= deVar.e.a.c()))) {
                        if (i9 != Integer.MIN_VALUE) {
                            b3 += i9;
                        }
                        deVar.b = b3;
                        deVar.a = b3;
                    }
                }
            }
        }
        a(bwVar);
        this.z = false;
        this.j = this.b.f() / this.g;
        this.u = View.MeasureSpec.makeMeasureSpec(this.b.f(), 1073741824);
        if (this.i == 1) {
            this.v = View.MeasureSpec.makeMeasureSpec(this.j, 1073741824);
            this.w = View.MeasureSpec.makeMeasureSpec(0, 0);
        } else {
            this.w = View.MeasureSpec.makeMeasureSpec(this.j, 1073741824);
            this.v = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        a(dbVar.a, ccVar);
        if (dbVar.c) {
            a(-1);
            a(bwVar, this.k, ccVar);
            a(1);
            this.k.b = dbVar.a + this.k.c;
            a(bwVar, this.k, ccVar);
        } else {
            a(1);
            a(bwVar, this.k, ccVar);
            a(-1);
            this.k.b = dbVar.a + this.k.c;
            a(bwVar, this.k, ccVar);
        }
        if (o() > 0) {
            if (this.c) {
                a(bwVar, ccVar, true);
                b(bwVar, ccVar, false);
            } else {
                b(bwVar, ccVar, true);
                a(bwVar, ccVar, false);
            }
        }
        if (!ccVar.a()) {
            if (this.n == 0 || o() <= 0 || (!this.z && g() == null)) {
                z3 = false;
            }
            if (z3) {
                a(this.B);
                Runnable runnable = this.B;
                if (this.r != null) {
                    bx.a(this.r, runnable);
                }
            }
            this.d = -1;
            this.e = Integer.MIN_VALUE;
        }
        this.o = dbVar.c;
        this.p = j();
        this.t = null;
    }

    public final boolean c() {
        return this.t == null;
    }

    public final int d(cc ccVar) {
        return h(ccVar);
    }

    public final Parcelable d() {
        int a2;
        if (this.t != null) {
            return new SavedState(this.t);
        }
        SavedState savedState = new SavedState();
        savedState.h = this.l;
        savedState.i = this.o;
        savedState.j = this.p;
        if (this.f == null || this.f.a == null) {
            savedState.e = 0;
        } else {
            savedState.f = this.f.a;
            savedState.e = savedState.f.length;
            savedState.g = this.f.b;
        }
        if (o() > 0) {
            h();
            savedState.a = this.o ? y() : z();
            View b2 = this.c ? b(true) : a(true);
            savedState.b = b2 == null ? -1 : e(b2);
            savedState.c = this.g;
            savedState.d = new int[this.g];
            for (int i2 = 0; i2 < this.g; i2++) {
                if (this.o) {
                    a2 = this.h[i2].b(Integer.MIN_VALUE);
                    if (a2 != Integer.MIN_VALUE) {
                        a2 -= this.a.d();
                    }
                } else {
                    a2 = this.h[i2].a(Integer.MIN_VALUE);
                    if (a2 != Integer.MIN_VALUE) {
                        a2 -= this.a.c();
                    }
                }
                savedState.d[i2] = a2;
            }
        } else {
            savedState.a = -1;
            savedState.b = -1;
            savedState.c = 0;
        }
        return savedState;
    }

    public final void d(int i2, int i3) {
        b(i2, i3, 8);
    }

    public final int e(cc ccVar) {
        return i(ccVar);
    }

    public final void e(int i2) {
        super.e(i2);
        for (int i3 = 0; i3 < this.g; i3++) {
            this.h[i3].d(i2);
        }
    }

    public final boolean e() {
        return this.i == 0;
    }

    public final int f(cc ccVar) {
        return i(ccVar);
    }

    public final void f(int i2) {
        super.f(i2);
        for (int i3 = 0; i3 < this.g; i3++) {
            this.h[i3].d(i2);
        }
    }

    public final boolean f() {
        return this.i == 1;
    }

    public final void g(int i2) {
        int z2;
        int y2;
        if (i2 == 0 && o() != 0 && this.n != 0 && m()) {
            if (this.c) {
                z2 = y();
                y2 = z();
            } else {
                z2 = z();
                y2 = y();
            }
            if (z2 == 0 && g() != null) {
                this.f.a();
            } else if (this.z) {
                int i3 = this.c ? -1 : 1;
                LazySpanLookup.FullSpanItem a2 = this.f.a(z2, y2 + 1, i3);
                if (a2 == null) {
                    this.z = false;
                    this.f.a(y2 + 1);
                    return;
                }
                LazySpanLookup.FullSpanItem a3 = this.f.a(z2, a2.a, i3 * -1);
                if (a3 == null) {
                    this.f.a(a2.a);
                } else {
                    this.f.a(a3.a + 1);
                }
            } else {
                return;
            }
            x();
            k();
        }
    }
}
