package android.support.v7.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.StaggeredGridLayoutManager;

final class dd implements Parcelable.Creator {
    dd() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new StaggeredGridLayoutManager.SavedState(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new StaggeredGridLayoutManager.SavedState[i];
    }
}
