package android.support.v7.widget;

import android.view.View;

final class cs implements View.OnClickListener {
    final /* synthetic */ SearchView a;

    cs(SearchView searchView) {
        this.a = searchView;
    }

    public final void onClick(View view) {
        if (view == this.a.h) {
            this.a.j();
        } else if (view == this.a.j) {
            SearchView.h(this.a);
        } else if (view == this.a.i) {
            SearchView.j(this.a);
        } else if (view == this.a.k) {
            SearchView.l(this.a);
        } else if (view == this.a.d) {
            this.a.n();
        }
    }
}
