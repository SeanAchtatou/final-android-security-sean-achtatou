package android.support.v7.widget;

import android.os.ResultReceiver;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import java.lang.reflect.Method;

final class cv {
    private Method a;
    private Method b;
    private Method c;
    private Method d;

    cv() {
        try {
            this.a = AutoCompleteTextView.class.getDeclaredMethod("doBeforeTextChanged", new Class[0]);
            this.a.setAccessible(true);
        } catch (NoSuchMethodException e) {
        }
        try {
            this.b = AutoCompleteTextView.class.getDeclaredMethod("doAfterTextChanged", new Class[0]);
            this.b.setAccessible(true);
        } catch (NoSuchMethodException e2) {
        }
        Class<AutoCompleteTextView> cls = AutoCompleteTextView.class;
        try {
            this.c = cls.getMethod("ensureImeVisible", Boolean.TYPE);
            this.c.setAccessible(true);
        } catch (NoSuchMethodException e3) {
        }
        Class<InputMethodManager> cls2 = InputMethodManager.class;
        try {
            this.d = cls2.getMethod("showSoftInputUnchecked", Integer.TYPE, ResultReceiver.class);
            this.d.setAccessible(true);
        } catch (NoSuchMethodException e4) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(InputMethodManager inputMethodManager, View view) {
        if (this.d != null) {
            try {
                this.d.invoke(inputMethodManager, 0, null);
                return;
            } catch (Exception e) {
            }
        }
        inputMethodManager.showSoftInput(view, 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(AutoCompleteTextView autoCompleteTextView) {
        if (this.a != null) {
            try {
                this.a.invoke(autoCompleteTextView, new Object[0]);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(AutoCompleteTextView autoCompleteTextView) {
        if (this.b != null) {
            try {
                this.b.invoke(autoCompleteTextView, new Object[0]);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(AutoCompleteTextView autoCompleteTextView) {
        if (this.c != null) {
            try {
                this.c.invoke(autoCompleteTextView, true);
            } catch (Exception e) {
            }
        }
    }
}
