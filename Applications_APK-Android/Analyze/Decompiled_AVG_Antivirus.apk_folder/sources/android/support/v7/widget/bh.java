package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

final class bh implements n {
    final /* synthetic */ RecyclerView a;

    bh(RecyclerView recyclerView) {
        this.a = recyclerView;
    }

    private void c(o oVar) {
        switch (oVar.a) {
            case 1:
                this.a.q.a(oVar.b, oVar.d);
                return;
            case 2:
                this.a.q.b(oVar.b, oVar.d);
                return;
            case 3:
            case 5:
            case 6:
            case 7:
            default:
                return;
            case 4:
                this.a.q.c(oVar.b, oVar.d);
                return;
            case 8:
                this.a.q.d(oVar.b, oVar.d);
                return;
        }
    }

    public final cf a(int i) {
        cf cfVar;
        RecyclerView recyclerView = this.a;
        int b = recyclerView.c.b();
        int i2 = 0;
        while (true) {
            if (i2 < b) {
                cfVar = RecyclerView.b(recyclerView.c.c(i2));
                if (cfVar != null && !cfVar.m() && cfVar.b == i) {
                    break;
                }
                i2++;
            } else {
                cfVar = null;
                break;
            }
        }
        if (cfVar != null && !this.a.c.d(cfVar.a)) {
            return cfVar;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.RecyclerView.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v7.widget.RecyclerView.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.RecyclerView.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.cf, android.support.v7.widget.bo):void
      android.support.v7.widget.RecyclerView.a(int, int, android.view.MotionEvent):boolean
      android.support.v7.widget.RecyclerView.a(int, int, boolean):void */
    public final void a(int i, int i2) {
        this.a.a(i, i2, true);
        this.a.g = true;
        cc.a(this.a.f, i2);
    }

    public final void a(int i, int i2, Object obj) {
        int e_;
        RecyclerView recyclerView = this.a;
        int b = recyclerView.c.b();
        int i3 = i + i2;
        for (int i4 = 0; i4 < b; i4++) {
            View c = recyclerView.c.c(i4);
            cf b2 = RecyclerView.b(c);
            if (b2 != null && !b2.d_() && b2.b >= i && b2.b < i3) {
                b2.b(2);
                b2.a(obj);
                ((RecyclerView.LayoutParams) c.getLayoutParams()).c = true;
            }
        }
        bw bwVar = recyclerView.a;
        int i5 = i + i2;
        for (int size = bwVar.b.size() - 1; size >= 0; size--) {
            cf cfVar = (cf) bwVar.b.get(size);
            if (cfVar != null && (e_ = cfVar.e_()) >= i && e_ < i5) {
                cfVar.b(2);
                bwVar.c(size);
            }
        }
        this.a.h = true;
    }

    public final void a(o oVar) {
        c(oVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.RecyclerView.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v7.widget.RecyclerView.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.RecyclerView.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.cf, android.support.v7.widget.bo):void
      android.support.v7.widget.RecyclerView.a(int, int, android.view.MotionEvent):boolean
      android.support.v7.widget.RecyclerView.a(int, int, boolean):void */
    public final void b(int i, int i2) {
        this.a.a(i, i2, false);
        this.a.g = true;
    }

    public final void b(o oVar) {
        c(oVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cf.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.widget.cf.a(int, int):void
      android.support.v7.widget.cf.a(android.support.v7.widget.bw, boolean):void
      android.support.v7.widget.cf.a(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean
     arg types: [android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, int):int
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean */
    public final void c(int i, int i2) {
        RecyclerView recyclerView = this.a;
        int b = recyclerView.c.b();
        for (int i3 = 0; i3 < b; i3++) {
            cf b2 = RecyclerView.b(recyclerView.c.c(i3));
            if (b2 != null && !b2.d_() && b2.b >= i) {
                b2.a(i2, false);
                boolean unused = recyclerView.f.f = true;
            }
        }
        bw bwVar = recyclerView.a;
        int size = bwVar.b.size();
        for (int i4 = 0; i4 < size; i4++) {
            cf cfVar = (cf) bwVar.b.get(i4);
            if (cfVar != null && cfVar.e_() >= i) {
                cfVar.a(i2, true);
            }
        }
        recyclerView.requestLayout();
        this.a.g = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cf.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.widget.cf.a(int, int):void
      android.support.v7.widget.cf.a(android.support.v7.widget.bw, boolean):void
      android.support.v7.widget.cf.a(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean
     arg types: [android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, int):int
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean */
    public final void d(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8 = -1;
        RecyclerView recyclerView = this.a;
        int b = recyclerView.c.b();
        if (i < i2) {
            i3 = -1;
            i4 = i2;
            i5 = i;
        } else {
            i3 = 1;
            i4 = i;
            i5 = i2;
        }
        for (int i9 = 0; i9 < b; i9++) {
            cf b2 = RecyclerView.b(recyclerView.c.c(i9));
            if (b2 != null && b2.b >= i5 && b2.b <= i4) {
                if (b2.b == i) {
                    b2.a(i2 - i, false);
                } else {
                    b2.a(i3, false);
                }
                boolean unused = recyclerView.f.f = true;
            }
        }
        bw bwVar = recyclerView.a;
        if (i < i2) {
            i6 = i2;
            i7 = i;
        } else {
            i8 = 1;
            i6 = i;
            i7 = i2;
        }
        int size = bwVar.b.size();
        for (int i10 = 0; i10 < size; i10++) {
            cf cfVar = (cf) bwVar.b.get(i10);
            if (cfVar != null && cfVar.b >= i7 && cfVar.b <= i6) {
                if (cfVar.b == i) {
                    cfVar.a(i2 - i, false);
                } else {
                    cfVar.a(i8, false);
                }
            }
        }
        recyclerView.requestLayout();
        this.a.g = true;
    }
}
