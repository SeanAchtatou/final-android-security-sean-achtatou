package android.support.v7.widget;

import java.util.ArrayList;
import java.util.Iterator;

final class t implements Runnable {
    final /* synthetic */ ArrayList a;
    final /* synthetic */ s b;

    t(s sVar, ArrayList arrayList) {
        this.b = sVar;
        this.a = arrayList;
    }

    public final void run() {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            ac acVar = (ac) it.next();
            s.a(this.b, acVar.a, acVar.b, acVar.c, acVar.d, acVar.e);
        }
        this.a.clear();
        this.b.g.remove(this.a);
    }
}
