package android.support.v7.widget;

import android.support.v4.e.a;
import android.support.v4.e.f;

/* renamed from: android.support.v7.widget.do  reason: invalid class name */
final class Cdo {
    final a a = new a();
    final f b = new f();

    Cdo() {
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.a.clear();
        this.b.b();
    }

    /* access modifiers changed from: package-private */
    public final void a(long j, cf cfVar) {
        this.b.a(j, cfVar);
    }

    /* access modifiers changed from: package-private */
    public final void a(cf cfVar) {
        dp dpVar = (dp) this.a.get(cfVar);
        if (dpVar == null) {
            dpVar = dp.a();
            this.a.put(cfVar, dpVar);
        }
        dpVar.a |= 1;
    }

    /* access modifiers changed from: package-private */
    public final void a(cf cfVar, bo boVar) {
        dp dpVar = (dp) this.a.get(cfVar);
        if (dpVar == null) {
            dpVar = dp.a();
            this.a.put(cfVar, dpVar);
        }
        dpVar.b = boVar;
        dpVar.a |= 4;
    }

    /* access modifiers changed from: package-private */
    public final void b(cf cfVar) {
        dp dpVar = (dp) this.a.get(cfVar);
        if (dpVar != null) {
            dpVar.a &= -2;
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(cf cfVar) {
        int a2 = this.b.a() - 1;
        while (true) {
            if (a2 < 0) {
                break;
            } else if (cfVar == this.b.b(a2)) {
                this.b.a(a2);
                break;
            } else {
                a2--;
            }
        }
        dp dpVar = (dp) this.a.remove(cfVar);
        if (dpVar != null) {
            dp.a(dpVar);
        }
    }
}
