package android.support.v7.widget.a;

import android.support.v4.view.ay;
import android.support.v7.widget.bt;
import android.support.v7.widget.cf;
import android.view.MotionEvent;

final class c implements bt {
    final /* synthetic */ a a;

    c(a aVar) {
        this.a = aVar;
    }

    public final void a(boolean z) {
        if (z) {
            this.a.a((cf) null, 0);
        }
    }

    public final boolean a(MotionEvent motionEvent) {
        int a2;
        k a3;
        this.a.x.a(motionEvent);
        int a4 = ay.a(motionEvent);
        if (a4 == 0) {
            this.a.i = ay.b(motionEvent, 0);
            this.a.c = motionEvent.getX();
            this.a.d = motionEvent.getY();
            a.e(this.a);
            if (this.a.b == null && (a3 = a.a(this.a, motionEvent)) != null) {
                this.a.c -= a3.k;
                this.a.d -= a3.l;
                int unused = this.a.a(a3.h, true);
                if (this.a.a.remove(a3.h.a)) {
                    this.a.j.a(this.a.p, a3.h);
                }
                this.a.a(a3.h, a3.i);
                a.a(this.a, motionEvent, this.a.l, 0);
            }
        } else if (a4 == 3 || a4 == 1) {
            this.a.i = -1;
            this.a.a((cf) null, 0);
        } else if (this.a.i != -1 && (a2 = ay.a(motionEvent, this.a.i)) >= 0) {
            a.a(this.a, a4, motionEvent, a2);
        }
        if (this.a.r != null) {
            this.a.r.addMovement(motionEvent);
        }
        return this.a.b != null;
    }

    public final void b(MotionEvent motionEvent) {
        int i = 0;
        this.a.x.a(motionEvent);
        if (this.a.r != null) {
            this.a.r.addMovement(motionEvent);
        }
        if (this.a.i != -1) {
            int a2 = ay.a(motionEvent);
            int a3 = ay.a(motionEvent, this.a.i);
            if (a3 >= 0) {
                a.a(this.a, a2, motionEvent, a3);
            }
            cf cfVar = this.a.b;
            if (cfVar != null) {
                switch (a2) {
                    case 1:
                    case 3:
                        if (this.a.r != null) {
                            this.a.r.computeCurrentVelocity(1000, (float) this.a.p.f());
                        }
                        this.a.a((cf) null, 0);
                        this.a.i = -1;
                        return;
                    case 2:
                        if (a3 >= 0) {
                            a.a(this.a, motionEvent, this.a.l, a3);
                            a.a(this.a, cfVar);
                            this.a.p.removeCallbacks(this.a.q);
                            this.a.q.run();
                            this.a.p.invalidate();
                            return;
                        }
                        return;
                    case 4:
                    case 5:
                    default:
                        return;
                    case 6:
                        int b = ay.b(motionEvent);
                        if (ay.b(motionEvent, b) == this.a.i) {
                            if (this.a.r != null) {
                                this.a.r.computeCurrentVelocity(1000, (float) this.a.p.f());
                            }
                            if (b == 0) {
                                i = 1;
                            }
                            this.a.i = ay.b(motionEvent, i);
                            a.a(this.a, motionEvent, this.a.l, b);
                            return;
                        }
                        return;
                }
            }
        }
    }
}
