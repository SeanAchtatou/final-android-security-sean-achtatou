package android.support.v7.widget;

import android.view.View;

final class ah {
    int a;
    int b;
    boolean c;
    final /* synthetic */ LinearLayoutManager d;

    ah(LinearLayoutManager linearLayoutManager) {
        this.d = linearLayoutManager;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.b = this.c ? this.d.k.d() : this.d.k.c();
    }

    public final void a(View view) {
        if (this.c) {
            this.b = this.d.k.b(view) + this.d.k.b();
        } else {
            this.b = this.d.k.a(view);
        }
        this.a = LinearLayoutManager.e(view);
    }

    public final String toString() {
        return "AnchorInfo{mPosition=" + this.a + ", mCoordinate=" + this.b + ", mLayoutFromEnd=" + this.c + '}';
    }
}
