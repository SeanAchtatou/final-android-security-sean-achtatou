package android.support.v7.widget.a;

import android.support.v4.a.a;
import android.support.v4.a.b;
import android.support.v4.a.l;
import android.support.v4.view.bx;
import android.support.v7.widget.cf;

class k implements b {
    private final l a;
    /* access modifiers changed from: private */
    public final int b;
    /* access modifiers changed from: private */
    public boolean c = false;
    final float d;
    final float e;
    final float f;
    final float g;
    final cf h;
    final int i;
    public boolean j;
    float k;
    float l;
    boolean m = false;
    final /* synthetic */ a n;
    private float o;

    public k(a aVar, cf cfVar, int i2, int i3, float f2, float f3, float f4, float f5) {
        this.n = aVar;
        this.i = i3;
        this.b = i2;
        this.h = cfVar;
        this.d = f2;
        this.e = f3;
        this.f = f4;
        this.g = f5;
        this.a = a.a();
        this.a.a(new l(this, aVar));
        this.a.a(cfVar.a);
        this.a.a(this);
        this.o = 0.0f;
    }

    public final void a() {
        this.o = 1.0f;
    }

    public final void a(float f2) {
        this.o = f2;
    }

    public final void a(long j2) {
        this.a.a(j2);
    }

    public void a(l lVar) {
        if (!this.c) {
            this.h.a(true);
        }
        this.c = true;
    }

    public final void b() {
        this.h.a(false);
        this.a.a();
    }

    public final void c() {
        this.a.b();
    }

    public final void d() {
        if (this.d == this.f) {
            this.k = bx.p(this.h.a);
        } else {
            this.k = this.d + (this.o * (this.f - this.d));
        }
        if (this.e == this.g) {
            this.l = bx.q(this.h.a);
        } else {
            this.l = this.e + (this.o * (this.g - this.e));
        }
    }
}
