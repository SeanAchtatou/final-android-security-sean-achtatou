package android.support.v7.widget;

import android.util.Log;
import android.view.animation.Interpolator;

public final class cb {
    private int a;
    private int b;
    private int c;
    private int d;
    private Interpolator e;
    private boolean f;
    private int g;

    public cb() {
        this((byte) 0);
    }

    private cb(byte b2) {
        this.d = -1;
        this.f = false;
        this.g = 0;
        this.a = 0;
        this.b = 0;
        this.c = Integer.MIN_VALUE;
        this.e = null;
    }

    static /* synthetic */ void a(cb cbVar, RecyclerView recyclerView) {
        if (cbVar.d >= 0) {
            int i = cbVar.d;
            cbVar.d = -1;
            RecyclerView.c(recyclerView, i);
            cbVar.f = false;
        } else if (!cbVar.f) {
            cbVar.g = 0;
        } else if (cbVar.e != null && cbVar.c <= 0) {
            throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
        } else if (cbVar.c <= 0) {
            throw new IllegalStateException("Scroll duration must be a positive number");
        } else {
            if (cbVar.e != null) {
                recyclerView.ab.a(cbVar.a, cbVar.b, cbVar.c, cbVar.e);
            } else if (cbVar.c == Integer.MIN_VALUE) {
                recyclerView.ab.b(cbVar.a, cbVar.b);
            } else {
                recyclerView.ab.a(cbVar.a, cbVar.b, cbVar.c);
            }
            cbVar.g++;
            if (cbVar.g > 10) {
                Log.e("RecyclerView", "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary");
            }
            cbVar.f = false;
        }
    }

    public final void a(int i) {
        this.d = i;
    }

    public final void a(int i, int i2, int i3, Interpolator interpolator) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.e = interpolator;
        this.f = true;
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.d >= 0;
    }
}
