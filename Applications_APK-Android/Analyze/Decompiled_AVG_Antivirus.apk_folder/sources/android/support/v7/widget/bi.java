package android.support.v7.widget;

import android.support.v4.os.k;
import android.view.ViewGroup;

public abstract class bi {
    private final bj a = new bj();
    private boolean b = false;

    public abstract int a();

    public int a(int i) {
        return 0;
    }

    public abstract cf a(ViewGroup viewGroup, int i);

    public final void a(bk bkVar) {
        this.a.registerObserver(bkVar);
    }

    public abstract void a(cf cfVar, int i);

    public final void a_(int i, int i2) {
        this.a.b(i, i2);
    }

    public final void b(int i) {
        this.a.a(i, 1);
    }

    public final void b(bk bkVar) {
        this.a.unregisterObserver(bkVar);
    }

    public final void b(cf cfVar, int i) {
        cfVar.b = i;
        if (this.b) {
            cfVar.d = -1;
        }
        cfVar.a(1, 519);
        k.a("RV OnBindView");
        cfVar.p();
        a(cfVar, i);
        cfVar.o();
        k.a();
    }

    public final boolean b_() {
        return this.b;
    }

    public final void c() {
        this.a.a();
    }

    public final void c(int i) {
        this.a.a(0, i);
    }

    public final void c_(int i) {
        this.a.a(i);
    }

    public final void d_(int i) {
        this.a.b(i);
    }

    public final void f(int i) {
        this.a.b(i);
    }
}
