package android.support.v7.widget.a;

import android.support.v7.widget.bm;

final class e implements Runnable {
    final /* synthetic */ k a;
    final /* synthetic */ int b;
    final /* synthetic */ a c;

    e(a aVar, k kVar, int i) {
        this.c = aVar;
        this.a = kVar;
        this.b = i;
    }

    public final void run() {
        boolean z;
        if (this.c.p != null && this.c.p.isAttachedToWindow() && !this.a.m && this.a.h.d() != -1) {
            bm i = this.c.p.i();
            if (i == null || !i.b()) {
                a aVar = this.c;
                int size = aVar.m.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        z = false;
                        break;
                    } else if (!((k) aVar.m.get(i2)).c) {
                        z = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (!z) {
                    this.c.j.a(this.a.h);
                    return;
                }
            }
            this.c.p.post(this);
        }
    }
}
