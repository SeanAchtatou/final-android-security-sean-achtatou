package android.support.v7.widget.a;

import android.support.v4.a.l;
import android.support.v7.widget.cf;

final class d extends k {
    final /* synthetic */ int a;
    final /* synthetic */ cf b;
    final /* synthetic */ a c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(a aVar, cf cfVar, int i, int i2, float f, float f2, float f3, float f4, int i3, cf cfVar2) {
        super(aVar, cfVar, i, i2, f, f2, f3, f4);
        this.c = aVar;
        this.a = i3;
        this.b = cfVar2;
    }

    public final void a(l lVar) {
        super.a(lVar);
        if (!this.m) {
            if (this.a <= 0) {
                this.c.j.a(this.c.p, this.b);
            } else {
                this.c.a.add(this.b.a);
                this.j = true;
                if (this.a > 0) {
                    this.c.p.post(new e(this.c, this, this.a));
                }
            }
            if (this.c.v == this.b.a) {
                this.c.b(this.b.a);
            }
        }
    }
}
