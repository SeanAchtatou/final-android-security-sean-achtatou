package android.support.v7.widget;

import android.view.MotionEvent;
import android.view.View;

final class av implements View.OnTouchListener {
    final /* synthetic */ ListPopupWindow a;

    private av(ListPopupWindow listPopupWindow) {
        this.a = listPopupWindow;
    }

    /* synthetic */ av(ListPopupWindow listPopupWindow, byte b) {
        this(listPopupWindow);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (action == 0 && this.a.d != null && this.a.d.isShowing() && x >= 0 && x < this.a.d.getWidth() && y >= 0 && y < this.a.d.getHeight()) {
            this.a.A.postDelayed(this.a.v, 250);
            return false;
        } else if (action != 1) {
            return false;
        } else {
            this.a.A.removeCallbacks(this.a.v);
            return false;
        }
    }
}
