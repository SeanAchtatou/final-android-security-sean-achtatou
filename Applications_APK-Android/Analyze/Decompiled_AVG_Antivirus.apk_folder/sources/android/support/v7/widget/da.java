package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;

final class da extends al {
    final /* synthetic */ StaggeredGridLayoutManager a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    da(StaggeredGridLayoutManager staggeredGridLayoutManager, Context context) {
        super(context);
        this.a = staggeredGridLayoutManager;
    }

    public final PointF a(int i) {
        int a2 = this.a.j(i);
        if (a2 == 0) {
            return null;
        }
        return this.a.i == 0 ? new PointF((float) a2, 0.0f) : new PointF(0.0f, (float) a2);
    }
}
