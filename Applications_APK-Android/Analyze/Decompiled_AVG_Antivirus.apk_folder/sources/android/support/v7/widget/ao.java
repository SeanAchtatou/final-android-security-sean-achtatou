package android.support.v7.widget;

import android.content.Context;
import android.support.v4.view.dv;
import android.support.v4.widget.ad;
import android.support.v7.a.b;
import android.support.v7.internal.widget.ListViewCompat;

final class ao extends ListViewCompat {
    /* access modifiers changed from: private */
    public boolean f;
    private boolean g;
    private boolean h;
    private dv i;
    private ad j;

    public ao(Context context, boolean z) {
        super(context, null, b.dropDownListViewStyle);
        this.g = z;
        setCacheColorHint(0);
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return this.h || super.a();
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0048  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.view.MotionEvent r9, int r10) {
        /*
            r8 = this;
            r2 = 1
            r1 = 0
            int r3 = android.support.v4.view.ay.a(r9)
            switch(r3) {
                case 1: goto L_0x003e;
                case 2: goto L_0x0090;
                case 3: goto L_0x003b;
                default: goto L_0x0009;
            }
        L_0x0009:
            r0 = r1
            r3 = r2
        L_0x000b:
            if (r3 == 0) goto L_0x000f
            if (r0 == 0) goto L_0x0023
        L_0x000f:
            r8.h = r1
            r8.setPressed(r1)
            r8.drawableStateChanged()
            android.support.v4.view.dv r0 = r8.i
            if (r0 == 0) goto L_0x0023
            android.support.v4.view.dv r0 = r8.i
            r0.b()
            r0 = 0
            r8.i = r0
        L_0x0023:
            if (r3 == 0) goto L_0x0086
            android.support.v4.widget.ad r0 = r8.j
            if (r0 != 0) goto L_0x0030
            android.support.v4.widget.ad r0 = new android.support.v4.widget.ad
            r0.<init>(r8)
            r8.j = r0
        L_0x0030:
            android.support.v4.widget.ad r0 = r8.j
            r0.a(r2)
            android.support.v4.widget.ad r0 = r8.j
            r0.onTouch(r8, r9)
        L_0x003a:
            return r3
        L_0x003b:
            r0 = r1
            r3 = r1
            goto L_0x000b
        L_0x003e:
            r0 = r1
        L_0x003f:
            int r4 = r9.findPointerIndex(r10)
            if (r4 >= 0) goto L_0x0048
            r0 = r1
            r3 = r1
            goto L_0x000b
        L_0x0048:
            float r5 = r9.getX(r4)
            int r5 = (int) r5
            float r4 = r9.getY(r4)
            int r4 = (int) r4
            int r6 = r8.pointToPosition(r5, r4)
            r7 = -1
            if (r6 != r7) goto L_0x005c
            r3 = r0
            r0 = r2
            goto L_0x000b
        L_0x005c:
            int r0 = r8.getFirstVisiblePosition()
            int r0 = r6 - r0
            android.view.View r0 = r8.getChildAt(r0)
            float r5 = (float) r5
            float r4 = (float) r4
            r8.h = r2
            r8.setPressed(r2)
            r8.layoutChildren()
            r8.setSelection(r6)
            r8.a(r6, r0, r5, r4)
            r8.a(r1)
            r8.refreshDrawableState()
            if (r3 != r2) goto L_0x0009
            long r4 = r8.getItemIdAtPosition(r6)
            r8.performItemClick(r0, r6, r4)
            goto L_0x0009
        L_0x0086:
            android.support.v4.widget.ad r0 = r8.j
            if (r0 == 0) goto L_0x003a
            android.support.v4.widget.ad r0 = r8.j
            r0.a(r1)
            goto L_0x003a
        L_0x0090:
            r0 = r2
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ao.a(android.view.MotionEvent, int):boolean");
    }

    public final boolean hasFocus() {
        return this.g || super.hasFocus();
    }

    public final boolean hasWindowFocus() {
        return this.g || super.hasWindowFocus();
    }

    public final boolean isFocused() {
        return this.g || super.isFocused();
    }

    public final boolean isInTouchMode() {
        return (this.g && this.f) || super.isInTouchMode();
    }
}
