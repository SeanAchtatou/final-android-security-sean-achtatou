package android.support.v7.widget;

import android.support.v4.view.bx;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class cf {
    private static final List m = Collections.EMPTY_LIST;
    public final View a;
    int b = -1;
    int c = -1;
    long d = -1;
    int e = -1;
    int f = -1;
    cf g = null;
    cf h = null;
    List i = null;
    List j = null;
    RecyclerView k;
    /* access modifiers changed from: private */
    public int l;
    private int n = 0;
    /* access modifiers changed from: private */
    public bw o = null;
    /* access modifiers changed from: private */
    public boolean p = false;
    private int q = 0;

    public cf(View view) {
        if (view == null) {
            throw new IllegalArgumentException("itemView may not be null");
        }
        this.a = view;
    }

    static /* synthetic */ void a(cf cfVar) {
        cfVar.q = bx.e(cfVar.a);
        bx.c(cfVar.a, 4);
    }

    static /* synthetic */ void b(cf cfVar) {
        bx.c(cfVar.a, cfVar.q);
        cfVar.q = 0;
    }

    static /* synthetic */ boolean c(cf cfVar) {
        return (cfVar.l & 16) == 0 && bx.c(cfVar.a);
    }

    static /* synthetic */ boolean g(cf cfVar) {
        return (cfVar.l & 16) != 0;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.c = -1;
        this.f = -1;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3) {
        this.l = (this.l & (i3 ^ -1)) | (i2 & i3);
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, boolean z) {
        if (this.c == -1) {
            this.c = this.b;
        }
        if (this.f == -1) {
            this.f = this.b;
        }
        if (z) {
            this.f += i2;
        }
        this.b += i2;
        if (this.a.getLayoutParams() != null) {
            ((RecyclerView.LayoutParams) this.a.getLayoutParams()).c = true;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(bw bwVar, boolean z) {
        this.o = bwVar;
        this.p = z;
    }

    /* access modifiers changed from: package-private */
    public final void a(Object obj) {
        if (obj == null) {
            b(1024);
        } else if ((this.l & 1024) == 0) {
            if (this.i == null) {
                this.i = new ArrayList();
                this.j = Collections.unmodifiableList(this.i);
            }
            this.i.add(obj);
        }
    }

    public final void a(boolean z) {
        this.n = z ? this.n - 1 : this.n + 1;
        if (this.n < 0) {
            this.n = 0;
            Log.e("View", "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for " + this);
        } else if (!z && this.n == 1) {
            this.l |= 16;
        } else if (z && this.n == 0) {
            this.l &= -17;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i2) {
        return (this.l & i2) != 0;
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2) {
        this.l |= i2;
    }

    public final int d() {
        if (this.k == null) {
            return -1;
        }
        return this.k.c(this);
    }

    /* access modifiers changed from: package-private */
    public final boolean d_() {
        return (this.l & 128) != 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        return this.o != null;
    }

    public final int e_() {
        return this.f == -1 ? this.b : this.f;
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.o.b(this);
    }

    /* access modifiers changed from: package-private */
    public final boolean g() {
        return (this.l & 32) != 0;
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        this.l &= -33;
    }

    /* access modifiers changed from: package-private */
    public final void i() {
        this.l &= -257;
    }

    /* access modifiers changed from: package-private */
    public final boolean j() {
        return (this.l & 4) != 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean k() {
        return (this.l & 2) != 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean l() {
        return (this.l & 1) != 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean m() {
        return (this.l & 8) != 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean n() {
        return (this.l & 256) != 0;
    }

    /* access modifiers changed from: package-private */
    public final void o() {
        if (this.i != null) {
            this.i.clear();
        }
        this.l &= -1025;
    }

    /* access modifiers changed from: package-private */
    public final List p() {
        return (this.l & 1024) == 0 ? (this.i == null || this.i.size() == 0) ? m : this.j : m;
    }

    /* access modifiers changed from: package-private */
    public final void q() {
        this.l = 0;
        this.b = -1;
        this.c = -1;
        this.d = -1;
        this.f = -1;
        this.n = 0;
        this.g = null;
        this.h = null;
        o();
        this.q = 0;
    }

    public final boolean r() {
        return (this.l & 16) == 0 && !bx.c(this.a);
    }

    /* access modifiers changed from: package-private */
    public final boolean s() {
        return (this.l & 2) != 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ViewHolder{" + Integer.toHexString(hashCode()) + " position=" + this.b + " id=" + this.d + ", oldPos=" + this.c + ", pLpos:" + this.f);
        if (e()) {
            sb.append(" scrap ").append(this.p ? "[changeScrap]" : "[attachedScrap]");
        }
        if (j()) {
            sb.append(" invalid");
        }
        if (!l()) {
            sb.append(" unbound");
        }
        if (k()) {
            sb.append(" update");
        }
        if (m()) {
            sb.append(" removed");
        }
        if (d_()) {
            sb.append(" ignored");
        }
        if (n()) {
            sb.append(" tmpDetached");
        }
        if (!r()) {
            sb.append(" not recyclable(" + this.n + ")");
        }
        if ((this.l & 512) != 0 || j()) {
            sb.append(" undefined adapter position");
        }
        if (this.a.getParent() == null) {
            sb.append(" no parent");
        }
        sb.append("}");
        return sb.toString();
    }
}
