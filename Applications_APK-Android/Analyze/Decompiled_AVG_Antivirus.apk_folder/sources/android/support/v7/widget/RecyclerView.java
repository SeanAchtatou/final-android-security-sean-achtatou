package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.os.k;
import android.support.v4.view.a.a;
import android.support.v4.view.ay;
import android.support.v4.view.be;
import android.support.v4.view.bf;
import android.support.v4.view.bs;
import android.support.v4.view.bx;
import android.support.v4.widget.x;
import android.support.v7.c.d;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class RecyclerView extends ViewGroup implements be, bs {
    /* access modifiers changed from: private */
    public static final Interpolator ao = new be();
    /* access modifiers changed from: private */
    public static final boolean i = (Build.VERSION.SDK_INT == 18 || Build.VERSION.SDK_INT == 19 || Build.VERSION.SDK_INT == 20);
    private static final Class[] j = {Context.class, AttributeSet.class, Integer.TYPE, Integer.TYPE};
    /* access modifiers changed from: private */
    public boolean A;
    private boolean B;
    private int C;
    /* access modifiers changed from: private */
    public boolean D;
    /* access modifiers changed from: private */
    public final boolean E;
    private final AccessibilityManager F;
    private List G;
    /* access modifiers changed from: private */
    public boolean H;
    private int I;
    private x J;
    private x K;
    private x L;
    private x M;
    private int N;
    private int O;
    private VelocityTracker P;
    private int Q;
    private int R;
    private int S;
    private int T;
    private int U;
    private final int V;
    private final int W;
    final bw a;
    private float aa;
    /* access modifiers changed from: private */
    public final ce ab;
    private bu ac;
    private List ad;
    private bn ae;
    /* access modifiers changed from: private */
    public boolean af;
    /* access modifiers changed from: private */
    public cg ag;
    private bl ah;
    private final int[] ai;
    private final bf aj;
    private final int[] ak;
    private final int[] al;
    private final int[] am;
    private Runnable an;
    private final dq ap;
    m b;
    p c;
    final Cdo d;
    bm e;
    final cc f;
    boolean g;
    boolean h;
    private final by k;
    private SavedState l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public final Runnable n;
    private final Rect o;
    /* access modifiers changed from: private */
    public bi p;
    /* access modifiers changed from: private */
    public br q;
    /* access modifiers changed from: private */
    public bx r;
    /* access modifiers changed from: private */
    public final ArrayList s;
    private final ArrayList t;
    private bt u;
    /* access modifiers changed from: private */
    public boolean v;
    /* access modifiers changed from: private */
    public boolean w;
    /* access modifiers changed from: private */
    public boolean x;
    private boolean y;
    /* access modifiers changed from: private */
    public boolean z;

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        cf a;
        final Rect b = new Rect();
        boolean c = true;
        boolean d = false;

        public LayoutParams() {
            super(-2, -2);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.LayoutParams) layoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }
    }

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new bz();
        /* access modifiers changed from: package-private */
        public Parcelable a;

        SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readParcelable(br.class.getClassLoader());
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeParcelable(this.a, 0);
        }
    }

    public RecyclerView(Context context) {
        this(context, null);
    }

    public RecyclerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, int):void
     arg types: [android.support.v7.widget.RecyclerView, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, float):void
      android.support.v4.view.bx.c(android.view.View, int):void */
    public RecyclerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Constructor<? extends U> constructor;
        Object[] objArr;
        this.k = new by(this, (byte) 0);
        this.a = new bw(this);
        this.d = new Cdo();
        this.n = new bc(this);
        this.o = new Rect();
        this.s = new ArrayList();
        this.t = new ArrayList();
        this.H = false;
        this.I = 0;
        this.e = new s();
        this.N = 0;
        this.O = -1;
        this.aa = Float.MIN_VALUE;
        this.ab = new ce(this);
        this.f = new cc();
        this.g = false;
        this.h = false;
        this.ae = new bp(this, (byte) 0);
        this.af = false;
        this.ai = new int[2];
        this.ak = new int[2];
        this.al = new int[2];
        this.am = new int[2];
        this.an = new bd(this);
        this.ap = new bf(this);
        setScrollContainer(true);
        setFocusableInTouchMode(true);
        this.E = Build.VERSION.SDK_INT >= 16;
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.U = viewConfiguration.getScaledTouchSlop();
        this.V = viewConfiguration.getScaledMinimumFlingVelocity();
        this.W = viewConfiguration.getScaledMaximumFlingVelocity();
        setWillNotDraw(bx.a(this) == 2);
        this.e.a(this.ae);
        this.b = new m(new bh(this));
        this.c = new p(new bg(this));
        if (bx.e(this) == 0) {
            bx.c((View) this, 1);
        }
        this.F = (AccessibilityManager) getContext().getSystemService("accessibility");
        this.ag = new cg(this);
        bx.a(this, this.ag);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d.a, i2, 0);
            String string = obtainStyledAttributes.getString(d.b);
            obtainStyledAttributes.recycle();
            if (string != null) {
                String trim = string.trim();
                if (trim.length() != 0) {
                    String str = trim.charAt(0) == '.' ? context.getPackageName() + trim : trim.contains(".") ? trim : RecyclerView.class.getPackage().getName() + '.' + trim;
                    try {
                        Class<? extends U> asSubclass = (isInEditMode() ? getClass().getClassLoader() : context.getClassLoader()).loadClass(str).asSubclass(br.class);
                        try {
                            Constructor<? extends U> constructor2 = asSubclass.getConstructor(j);
                            objArr = new Object[]{context, attributeSet, Integer.valueOf(i2), 0};
                            constructor = constructor2;
                        } catch (NoSuchMethodException e2) {
                            constructor = asSubclass.getConstructor(new Class[0]);
                            objArr = null;
                        }
                        constructor.setAccessible(true);
                        a((br) constructor.newInstance(objArr));
                    } catch (NoSuchMethodException e3) {
                        e3.initCause(e2);
                        throw new IllegalStateException(attributeSet.getPositionDescription() + ": Error creating LayoutManager " + str, e3);
                    } catch (ClassNotFoundException e4) {
                        throw new IllegalStateException(attributeSet.getPositionDescription() + ": Unable to find LayoutManager " + str, e4);
                    } catch (InvocationTargetException e5) {
                        throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + str, e5);
                    } catch (InstantiationException e6) {
                        throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + str, e6);
                    } catch (IllegalAccessException e7) {
                        throw new IllegalStateException(attributeSet.getPositionDescription() + ": Cannot access non-public constructor " + str, e7);
                    } catch (ClassCastException e8) {
                        throw new IllegalStateException(attributeSet.getPositionDescription() + ": Class is not a LayoutManager " + str, e8);
                    }
                }
            }
        }
        this.aj = new bf(this);
        setNestedScrollingEnabled(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean
     arg types: [android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, int):int
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cc.b(android.support.v7.widget.cc, boolean):boolean
     arg types: [android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.cc.b(android.support.v7.widget.cc, int):int
      android.support.v7.widget.cc.b(android.support.v7.widget.cc, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cc.c(android.support.v7.widget.cc, boolean):boolean
     arg types: [android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.cc.c(android.support.v7.widget.cc, int):int
      android.support.v7.widget.cc.c(android.support.v7.widget.cc, boolean):boolean */
    private void A() {
        int i2;
        int e_;
        bo boVar;
        dp dpVar;
        boolean z2 = true;
        if (this.p == null) {
            Log.e("RecyclerView", "No adapter attached; skipping layout");
        } else if (this.q == null) {
            Log.e("RecyclerView", "No layout manager attached; skipping layout");
        } else {
            this.d.a();
            d();
            w();
            z();
            boolean unused = this.f.j = this.f.h && this.h;
            this.h = false;
            this.g = false;
            boolean unused2 = this.f.g = this.f.i;
            this.f.a = this.p.a();
            int[] iArr = this.ai;
            int a2 = this.c.a();
            if (a2 == 0) {
                iArr[0] = 0;
                iArr[1] = 0;
            } else {
                int i3 = Integer.MAX_VALUE;
                int i4 = Integer.MIN_VALUE;
                int i5 = 0;
                while (i5 < a2) {
                    cf b2 = b(this.c.b(i5));
                    if (!b2.d_()) {
                        i2 = b2.e_();
                        if (i2 < i3) {
                            i3 = i2;
                        }
                        if (i2 > i4) {
                            i5++;
                            i3 = i3;
                            i4 = i2;
                        }
                    }
                    i2 = i4;
                    i5++;
                    i3 = i3;
                    i4 = i2;
                }
                iArr[0] = i3;
                iArr[1] = i4;
            }
            if (this.f.h) {
                int a3 = this.c.a();
                for (int i6 = 0; i6 < a3; i6++) {
                    cf b3 = b(this.c.b(i6));
                    if (!b3.d_() && (!b3.j() || this.p.b_())) {
                        bm.d(b3);
                        b3.p();
                        this.d.a(b3, new bo().a(b3));
                        if (this.f.j && b3.s() && !b3.m() && !b3.d_() && !b3.j()) {
                            this.d.a(b(b3), b3);
                        }
                    }
                }
            }
            if (this.f.i) {
                int b4 = this.c.b();
                for (int i7 = 0; i7 < b4; i7++) {
                    cf b5 = b(this.c.c(i7));
                    if (!b5.d_() && b5.c == -1) {
                        b5.c = b5.b;
                    }
                }
                boolean d2 = this.f.f;
                boolean unused3 = this.f.f = false;
                this.q.c(this.a, this.f);
                boolean unused4 = this.f.f = d2;
                for (int i8 = 0; i8 < this.c.a(); i8++) {
                    cf b6 = b(this.c.b(i8));
                    if (!b6.d_()) {
                        dp dpVar2 = (dp) this.d.a.get(b6);
                        if (!((dpVar2 == null || (dpVar2.a & 4) == 0) ? false : true)) {
                            bm.d(b6);
                            boolean a4 = b6.a(8192);
                            b6.p();
                            bo a5 = new bo().a(b6);
                            if (a4) {
                                a(b6, a5);
                            } else {
                                Cdo doVar = this.d;
                                dp dpVar3 = (dp) doVar.a.get(b6);
                                if (dpVar3 == null) {
                                    dpVar3 = dp.a();
                                    doVar.a.put(b6, dpVar3);
                                }
                                dpVar3.a |= 2;
                                dpVar3.b = a5;
                            }
                        }
                    }
                }
                C();
                this.b.c();
            } else {
                C();
            }
            this.f.a = this.p.a();
            int unused5 = this.f.e = 0;
            boolean unused6 = this.f.g = false;
            this.q.c(this.a, this.f);
            boolean unused7 = this.f.f = false;
            this.l = null;
            boolean unused8 = this.f.h = this.f.h && this.e != null;
            if (this.f.h) {
                int a6 = this.c.a();
                for (int i9 = 0; i9 < a6; i9++) {
                    cf b7 = b(this.c.b(i9));
                    if (!b7.d_()) {
                        long b8 = b(b7);
                        bo a7 = new bo().a(b7);
                        cf cfVar = (cf) this.d.b.a(b8);
                        if (cfVar == null || cfVar.d_()) {
                            Cdo doVar2 = this.d;
                            dp dpVar4 = (dp) doVar2.a.get(b7);
                            if (dpVar4 == null) {
                                dpVar4 = dp.a();
                                doVar2.a.put(b7, dpVar4);
                            }
                            dpVar4.c = a7;
                            dpVar4.a |= 8;
                        } else {
                            Cdo doVar3 = this.d;
                            int a8 = doVar3.a.a(cfVar);
                            if (a8 < 0 || (dpVar = (dp) doVar3.a.c(a8)) == null || (dpVar.a & 4) == 0) {
                                boVar = null;
                            } else {
                                dpVar.a &= -5;
                                bo boVar2 = dpVar.b;
                                if (dpVar.a == 0) {
                                    doVar3.a.d(a8);
                                    dp.a(dpVar);
                                }
                                boVar = boVar2;
                            }
                            cfVar.a(false);
                            if (cfVar != b7) {
                                cfVar.g = b7;
                                a(cfVar);
                                this.a.b(cfVar);
                                b7.a(false);
                                b7.h = cfVar;
                            }
                            if (this.e.a(cfVar, b7, boVar, a7)) {
                                y();
                            }
                        }
                    }
                }
                Cdo doVar4 = this.d;
                dq dqVar = this.ap;
                for (int size = doVar4.a.size() - 1; size >= 0; size--) {
                    cf cfVar2 = (cf) doVar4.a.b(size);
                    dp dpVar5 = (dp) doVar4.a.d(size);
                    if ((dpVar5.a & 3) == 3) {
                        dqVar.a(cfVar2);
                    } else if ((dpVar5.a & 1) != 0) {
                        dqVar.a(cfVar2, dpVar5.b, dpVar5.c);
                    } else if ((dpVar5.a & 14) == 14) {
                        dqVar.b(cfVar2, dpVar5.b, dpVar5.c);
                    } else if ((dpVar5.a & 12) == 12) {
                        dqVar.c(cfVar2, dpVar5.b, dpVar5.c);
                    } else if ((dpVar5.a & 4) != 0) {
                        dqVar.a(cfVar2, dpVar5.b, null);
                    } else if ((dpVar5.a & 8) != 0) {
                        dqVar.b(cfVar2, dpVar5.b, dpVar5.c);
                    } else {
                        int i10 = dpVar5.a;
                    }
                    dp.a(dpVar5);
                }
            }
            b(false);
            this.q.b(this.a);
            int unused9 = this.f.d = this.f.a;
            this.H = false;
            boolean unused10 = this.f.h = false;
            boolean unused11 = this.f.i = false;
            x();
            boolean unused12 = this.q.a = false;
            if (this.a.d != null) {
                this.a.d.clear();
            }
            this.d.a();
            int i11 = this.ai[0];
            int i12 = this.ai[1];
            int a9 = this.c.a();
            if (a9 != 0) {
                int i13 = 0;
                while (true) {
                    if (i13 >= a9) {
                        z2 = false;
                        break;
                    }
                    cf b9 = b(this.c.b(i13));
                    if (!b9.d_() && ((e_ = b9.e_()) < i11 || e_ > i12)) {
                        break;
                    }
                    i13++;
                }
            } else if (i11 == 0 && i12 == 0) {
                z2 = false;
            }
            if (z2) {
                c(0, 0);
            }
        }
    }

    private void B() {
        int b2 = this.c.b();
        for (int i2 = 0; i2 < b2; i2++) {
            ((LayoutParams) this.c.c(i2).getLayoutParams()).c = true;
        }
        bw bwVar = this.a;
        int size = bwVar.b.size();
        for (int i3 = 0; i3 < size; i3++) {
            LayoutParams layoutParams = (LayoutParams) ((cf) bwVar.b.get(i3)).a.getLayoutParams();
            if (layoutParams != null) {
                layoutParams.c = true;
            }
        }
    }

    private void C() {
        int b2 = this.c.b();
        for (int i2 = 0; i2 < b2; i2++) {
            cf b3 = b(this.c.c(i2));
            if (!b3.d_()) {
                b3.a();
            }
        }
        this.a.f();
    }

    private void D() {
        int b2 = this.c.b();
        for (int i2 = 0; i2 < b2; i2++) {
            cf b3 = b(this.c.c(i2));
            if (b3 != null && !b3.d_()) {
                b3.b(6);
            }
        }
        B();
        bw bwVar = this.a;
        if (bwVar.c.p == null || !bwVar.c.p.b_()) {
            bwVar.c();
            return;
        }
        int size = bwVar.b.size();
        for (int i3 = 0; i3 < size; i3++) {
            cf cfVar = (cf) bwVar.b.get(i3);
            if (cfVar != null) {
                cfVar.b(6);
                cfVar.a((Object) null);
            }
        }
    }

    /* access modifiers changed from: private */
    public void E() {
        int a2 = this.c.a();
        for (int i2 = 0; i2 < a2; i2++) {
            View b2 = this.c.b(i2);
            cf a3 = a(b2);
            if (!(a3 == null || a3.h == null)) {
                View view = a3.h.a;
                int left = b2.getLeft();
                int top = b2.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                }
            }
        }
    }

    static /* synthetic */ void a(RecyclerView recyclerView, cf cfVar, bo boVar, bo boVar2) {
        recyclerView.a(cfVar);
        cfVar.a(false);
        if (recyclerView.e.a(cfVar, boVar, boVar2)) {
            recyclerView.y();
        }
    }

    static /* synthetic */ void a(RecyclerView recyclerView, View view) {
        b(view);
        if (recyclerView.G != null) {
            for (int size = recyclerView.G.size() - 1; size >= 0; size--) {
                recyclerView.G.get(size);
            }
        }
    }

    private void a(cf cfVar) {
        View view = cfVar.a;
        boolean z2 = view.getParent() == this;
        this.a.b(a(view));
        if (cfVar.n()) {
            this.c.a(view, -1, view.getLayoutParams(), true);
        } else if (!z2) {
            this.c.a(view);
        } else {
            this.c.e(view);
        }
    }

    /* access modifiers changed from: private */
    public void a(cf cfVar, bo boVar) {
        cfVar.a(0, 8192);
        if (this.f.j && cfVar.s() && !cfVar.m() && !cfVar.d_()) {
            this.d.a(b(cfVar), cfVar);
        }
        this.d.a(cfVar, boVar);
    }

    private void a(MotionEvent motionEvent) {
        int b2 = ay.b(motionEvent);
        if (ay.b(motionEvent, b2) == this.O) {
            int i2 = b2 == 0 ? 1 : 0;
            this.O = ay.b(motionEvent, i2);
            int c2 = (int) (ay.c(motionEvent, i2) + 0.5f);
            this.S = c2;
            this.Q = c2;
            int d2 = (int) (ay.d(motionEvent, i2) + 0.5f);
            this.T = d2;
            this.R = d2;
        }
    }

    private boolean a(int i2, int i3, MotionEvent motionEvent) {
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        m();
        if (this.p != null) {
            d();
            w();
            k.a("RV Scroll");
            if (i2 != 0) {
                i6 = this.q.a(i2, this.a, this.f);
                i4 = i2 - i6;
            }
            if (i3 != 0) {
                i7 = this.q.b(i3, this.a, this.f);
                i5 = i3 - i7;
            }
            k.a();
            E();
            x();
            b(false);
        }
        int i8 = i5;
        int i9 = i6;
        int i10 = i7;
        if (!this.s.isEmpty()) {
            invalidate();
        }
        if (dispatchNestedScroll(i9, i10, i4, i8, this.ak)) {
            this.S -= this.ak[0];
            this.T -= this.ak[1];
            if (motionEvent != null) {
                motionEvent.offsetLocation((float) this.ak[0], (float) this.ak[1]);
            }
            int[] iArr = this.am;
            iArr[0] = iArr[0] + this.ak[0];
            int[] iArr2 = this.am;
            iArr2[1] = iArr2[1] + this.ak[1];
        } else if (bx.a((View) this) != 2) {
            if (motionEvent != null) {
                float x2 = motionEvent.getX();
                float f2 = (float) i4;
                float y2 = motionEvent.getY();
                float f3 = (float) i8;
                boolean z2 = false;
                if (f2 < 0.0f) {
                    p();
                    if (this.J.a((-f2) / ((float) getWidth()), 1.0f - (y2 / ((float) getHeight())))) {
                        z2 = true;
                    }
                } else if (f2 > 0.0f) {
                    q();
                    if (this.L.a(f2 / ((float) getWidth()), y2 / ((float) getHeight()))) {
                        z2 = true;
                    }
                }
                if (f3 < 0.0f) {
                    r();
                    if (this.K.a((-f3) / ((float) getHeight()), x2 / ((float) getWidth()))) {
                        z2 = true;
                    }
                } else if (f3 > 0.0f) {
                    s();
                    if (this.M.a(f3 / ((float) getHeight()), 1.0f - (x2 / ((float) getWidth())))) {
                        z2 = true;
                    }
                }
                if (!(!z2 && f2 == 0.0f && f3 == 0.0f)) {
                    bx.d(this);
                }
            }
            d(i2, i3);
        }
        if (!(i9 == 0 && i10 == 0)) {
            c(i9, i10);
        }
        if (!awakenScrollBars()) {
            invalidate();
        }
        return (i9 == 0 && i10 == 0) ? false : true;
    }

    static /* synthetic */ boolean a(RecyclerView recyclerView, cf cfVar) {
        return recyclerView.e == null || recyclerView.e.f(cfVar);
    }

    private long b(cf cfVar) {
        return this.p.b_() ? cfVar.d : (long) cfVar.b;
    }

    static cf b(View view) {
        if (view == null) {
            return null;
        }
        return ((LayoutParams) view.getLayoutParams()).a;
    }

    static /* synthetic */ void b(RecyclerView recyclerView, cf cfVar, bo boVar, bo boVar2) {
        cfVar.a(false);
        if (recyclerView.e.b(cfVar, boVar, boVar2)) {
            recyclerView.y();
        }
    }

    /* access modifiers changed from: private */
    public int c(cf cfVar) {
        if (cfVar.a(524) || !cfVar.l()) {
            return -1;
        }
        m mVar = this.b;
        int i2 = cfVar.b;
        int size = mVar.a.size();
        for (int i3 = 0; i3 < size; i3++) {
            o oVar = (o) mVar.a.get(i3);
            switch (oVar.a) {
                case 1:
                    if (oVar.b > i2) {
                        break;
                    } else {
                        i2 += oVar.d;
                        break;
                    }
                case 2:
                    if (oVar.b <= i2) {
                        if (oVar.b + oVar.d <= i2) {
                            i2 -= oVar.d;
                            break;
                        } else {
                            return -1;
                        }
                    } else {
                        continue;
                    }
                case 8:
                    if (oVar.b != i2) {
                        if (oVar.b < i2) {
                            i2--;
                        }
                        if (oVar.d > i2) {
                            break;
                        } else {
                            i2++;
                            break;
                        }
                    } else {
                        i2 = oVar.d;
                        break;
                    }
            }
        }
        return i2;
    }

    public static int c(View view) {
        cf b2 = b(view);
        if (b2 != null) {
            return b2.d();
        }
        return -1;
    }

    static /* synthetic */ void c(RecyclerView recyclerView, int i2) {
        if (recyclerView.q != null) {
            recyclerView.q.c(i2);
            recyclerView.awakenScrollBars();
        }
    }

    static /* synthetic */ boolean c(RecyclerView recyclerView, View view) {
        recyclerView.d();
        boolean g2 = recyclerView.c.g(view);
        if (g2) {
            cf b2 = b(view);
            recyclerView.a.b(b2);
            recyclerView.a.a(b2);
        }
        recyclerView.b(false);
        return g2;
    }

    public static int d(View view) {
        cf b2 = b(view);
        if (b2 != null) {
            return b2.e_();
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        if (i2 != this.N) {
            this.N = i2;
            if (i2 != 2) {
                o();
            }
            if (this.q != null) {
                this.q.g(i2);
            }
            if (this.ac != null) {
                this.ac.a(this, i2);
            }
            if (this.ad != null) {
                for (int size = this.ad.size() - 1; size >= 0; size--) {
                    ((bu) this.ad.get(size)).a(this, i2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void d(int i2, int i3) {
        boolean z2 = false;
        if (this.J != null && !this.J.a() && i2 > 0) {
            z2 = this.J.c();
        }
        if (this.L != null && !this.L.a() && i2 < 0) {
            z2 |= this.L.c();
        }
        if (this.K != null && !this.K.a() && i3 > 0) {
            z2 |= this.K.c();
        }
        if (this.M != null && !this.M.a() && i3 < 0) {
            z2 |= this.M.c();
        }
        if (z2) {
            bx.d(this);
        }
    }

    /* access modifiers changed from: private */
    public void e(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        switch (mode) {
            case Integer.MIN_VALUE:
            case 1073741824:
                break;
            default:
                size = bx.r(this);
                break;
        }
        switch (mode2) {
            case Integer.MIN_VALUE:
            case 1073741824:
                break;
            default:
                size2 = bx.s(this);
                break;
        }
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: private */
    public void f(View view) {
        b(view);
        if (this.G != null) {
            for (int size = this.G.size() - 1; size >= 0; size--) {
                ((bs) this.G.get(size)).a(view);
            }
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        boolean z2 = false;
        if (this.x) {
            if (this.H) {
                k.a("RV FullInvalidate");
                A();
                k.a();
            } else if (!this.b.d()) {
            } else {
                if (this.b.a(4) && !this.b.a(11)) {
                    k.a("RV PartialInvalidate");
                    d();
                    this.b.b();
                    if (!this.z) {
                        int a2 = this.c.a();
                        int i2 = 0;
                        while (true) {
                            if (i2 < a2) {
                                cf b2 = b(this.c.b(i2));
                                if (b2 != null && !b2.d_() && b2.s()) {
                                    z2 = true;
                                    break;
                                }
                                i2++;
                            } else {
                                break;
                            }
                        }
                        if (z2) {
                            A();
                        } else {
                            this.b.c();
                        }
                    }
                    b(true);
                    k.a();
                } else if (this.b.d()) {
                    k.a("RV FullInvalidate");
                    A();
                    k.a();
                }
            }
        }
    }

    private void n() {
        d(0);
        o();
    }

    private void o() {
        this.ab.b();
        if (this.q != null) {
            this.q.w();
        }
    }

    static /* synthetic */ void o(RecyclerView recyclerView) {
        if (!recyclerView.H) {
            recyclerView.H = true;
            int b2 = recyclerView.c.b();
            for (int i2 = 0; i2 < b2; i2++) {
                cf b3 = b(recyclerView.c.c(i2));
                if (b3 != null && !b3.d_()) {
                    b3.b(512);
                }
            }
            bw bwVar = recyclerView.a;
            int size = bwVar.b.size();
            for (int i3 = 0; i3 < size; i3++) {
                cf cfVar = (cf) bwVar.b.get(i3);
                if (cfVar != null) {
                    cfVar.b(512);
                }
            }
        }
    }

    private void p() {
        if (this.J == null) {
            this.J = new x(getContext());
            if (this.m) {
                this.J.a((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.J.a(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    private void q() {
        if (this.L == null) {
            this.L = new x(getContext());
            if (this.m) {
                this.L.a((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.L.a(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    private void r() {
        if (this.K == null) {
            this.K = new x(getContext());
            if (this.m) {
                this.K.a((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.K.a(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    private void s() {
        if (this.M == null) {
            this.M = new x(getContext());
            if (this.m) {
                this.M.a((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.M.a(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    private void t() {
        this.M = null;
        this.K = null;
        this.L = null;
        this.J = null;
    }

    private void u() {
        if (this.P != null) {
            this.P.clear();
        }
        stopNestedScroll();
        boolean z2 = false;
        if (this.J != null) {
            z2 = this.J.c();
        }
        if (this.K != null) {
            z2 |= this.K.c();
        }
        if (this.L != null) {
            z2 |= this.L.c();
        }
        if (this.M != null) {
            z2 |= this.M.c();
        }
        if (z2) {
            bx.d(this);
        }
    }

    private void v() {
        u();
        d(0);
    }

    /* access modifiers changed from: private */
    public void w() {
        this.I++;
    }

    /* access modifiers changed from: private */
    public void x() {
        this.I--;
        if (this.I <= 0) {
            this.I = 0;
            int i2 = this.C;
            this.C = 0;
            if (i2 != 0 && g()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain();
                obtain.setEventType(2048);
                a.a(obtain, i2);
                sendAccessibilityEventUnchecked(obtain);
            }
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        if (!this.af && this.v) {
            bx.a(this, this.an);
            this.af = true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0072, code lost:
        if ((r5.e != null && r5.q.c()) != false) goto L_0x0074;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void z() {
        /*
            r5 = this;
            r2 = 1
            r1 = 0
            boolean r0 = r5.H
            if (r0 == 0) goto L_0x0013
            android.support.v7.widget.m r0 = r5.b
            r0.a()
            r5.D()
            android.support.v7.widget.br r0 = r5.q
            r0.a()
        L_0x0013:
            android.support.v7.widget.bm r0 = r5.e
            if (r0 == 0) goto L_0x0078
            android.support.v7.widget.br r0 = r5.q
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x0078
            android.support.v7.widget.m r0 = r5.b
            r0.b()
        L_0x0024:
            boolean r0 = r5.g
            if (r0 != 0) goto L_0x002c
            boolean r0 = r5.h
            if (r0 == 0) goto L_0x007e
        L_0x002c:
            r0 = r2
        L_0x002d:
            android.support.v7.widget.cc r4 = r5.f
            boolean r3 = r5.x
            if (r3 == 0) goto L_0x0080
            android.support.v7.widget.bm r3 = r5.e
            if (r3 == 0) goto L_0x0080
            boolean r3 = r5.H
            if (r3 != 0) goto L_0x0045
            if (r0 != 0) goto L_0x0045
            android.support.v7.widget.br r3 = r5.q
            boolean r3 = r3.a
            if (r3 == 0) goto L_0x0080
        L_0x0045:
            boolean r3 = r5.H
            if (r3 == 0) goto L_0x0051
            android.support.v7.widget.bi r3 = r5.p
            boolean r3 = r3.b_()
            if (r3 == 0) goto L_0x0080
        L_0x0051:
            r3 = r2
        L_0x0052:
            boolean unused = r4.h = r3
            android.support.v7.widget.cc r3 = r5.f
            android.support.v7.widget.cc r4 = r5.f
            boolean r4 = r4.h
            if (r4 == 0) goto L_0x0084
            if (r0 == 0) goto L_0x0084
            boolean r0 = r5.H
            if (r0 != 0) goto L_0x0084
            android.support.v7.widget.bm r0 = r5.e
            if (r0 == 0) goto L_0x0082
            android.support.v7.widget.br r0 = r5.q
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x0082
            r0 = r2
        L_0x0072:
            if (r0 == 0) goto L_0x0084
        L_0x0074:
            boolean unused = r3.i = r2
            return
        L_0x0078:
            android.support.v7.widget.m r0 = r5.b
            r0.e()
            goto L_0x0024
        L_0x007e:
            r0 = r1
            goto L_0x002d
        L_0x0080:
            r3 = r1
            goto L_0x0052
        L_0x0082:
            r0 = r1
            goto L_0x0072
        L_0x0084:
            r2 = r1
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.z():void");
    }

    public final bi a() {
        return this.p;
    }

    public final cf a(View view) {
        ViewParent parent = view.getParent();
        if (parent == null || parent == this) {
            return b(view);
        }
        throw new IllegalArgumentException("View " + view + " is not a direct child of " + this);
    }

    public final View a(float f2, float f3) {
        for (int a2 = this.c.a() - 1; a2 >= 0; a2--) {
            View b2 = this.c.b(a2);
            float p2 = bx.p(b2);
            float q2 = bx.q(b2);
            if (f2 >= ((float) b2.getLeft()) + p2 && f2 <= p2 + ((float) b2.getRight()) && f3 >= ((float) b2.getTop()) + q2 && f3 <= ((float) b2.getBottom()) + q2) {
                return b2;
            }
        }
        return null;
    }

    public final void a(int i2) {
        if (!this.A) {
            n();
            if (this.q == null) {
                Log.e("RecyclerView", "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument.");
                return;
            }
            this.q.c(i2);
            awakenScrollBars();
        }
    }

    public final void a(int i2, int i3) {
        int i4 = 0;
        if (this.q == null) {
            Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.A) {
            if (!this.q.e()) {
                i2 = 0;
            }
            if (this.q.f()) {
                i4 = i3;
            }
            if (i2 != 0 || i4 != 0) {
                this.ab.b(i2, i4);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean
     arg types: [android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, int):int
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean */
    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3, boolean z2) {
        int i4 = i2 + i3;
        int b2 = this.c.b();
        for (int i5 = 0; i5 < b2; i5++) {
            cf b3 = b(this.c.c(i5));
            if (b3 != null && !b3.d_()) {
                if (b3.b >= i4) {
                    b3.a(-i3, z2);
                    boolean unused = this.f.f = true;
                } else if (b3.b >= i2) {
                    b3.b(8);
                    b3.a(-i3, z2);
                    b3.b = i2 - 1;
                    boolean unused2 = this.f.f = true;
                }
            }
        }
        bw bwVar = this.a;
        int i6 = i2 + i3;
        for (int size = bwVar.b.size() - 1; size >= 0; size--) {
            cf cfVar = (cf) bwVar.b.get(size);
            if (cfVar != null) {
                if (cfVar.e_() >= i6) {
                    cfVar.a(-i3, z2);
                } else if (cfVar.e_() >= i2) {
                    cfVar.b(8);
                    bwVar.c(size);
                }
            }
        }
        requestLayout();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean
     arg types: [android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, int):int
      android.support.v7.widget.cc.a(android.support.v7.widget.cc, boolean):boolean */
    public final void a(bi biVar) {
        if (this.A) {
            a("Do not setLayoutFrozen in layout or scroll");
            this.A = false;
            if (!(!this.z || this.q == null || this.p == null)) {
                requestLayout();
            }
            this.z = false;
        }
        if (this.p != null) {
            this.p.b(this.k);
        }
        if (this.e != null) {
            this.e.c();
        }
        if (this.q != null) {
            this.q.c(this.a);
            this.q.b(this.a);
        }
        this.a.a();
        this.b.a();
        bi biVar2 = this.p;
        this.p = biVar;
        if (biVar != null) {
            biVar.a(this.k);
        }
        bw bwVar = this.a;
        bi biVar3 = this.p;
        bwVar.a();
        bwVar.e().a(biVar2, biVar3);
        boolean unused = this.f.f = true;
        D();
        requestLayout();
    }

    public final void a(bl blVar) {
        if (blVar != this.ah) {
            this.ah = blVar;
            setChildrenDrawingOrderEnabled(this.ah != null);
        }
    }

    public final void a(bm bmVar) {
        if (this.e != null) {
            this.e.c();
            this.e.a(null);
        }
        this.e = bmVar;
        if (this.e != null) {
            this.e.a(this.ae);
        }
    }

    public final void a(bq bqVar) {
        if (this.q != null) {
            this.q.a("Cannot add item decoration during a scroll  or layout");
        }
        if (this.s.isEmpty()) {
            setWillNotDraw(false);
        }
        this.s.add(bqVar);
        B();
        requestLayout();
    }

    public final void a(br brVar) {
        if (brVar != this.q) {
            if (this.q != null) {
                if (this.v) {
                    this.q.b(this, this.a);
                }
                this.q.a((RecyclerView) null);
            }
            this.a.a();
            p pVar = this.c;
            q qVar = pVar.b;
            while (true) {
                qVar.a = 0;
                if (qVar.b == null) {
                    break;
                }
                qVar = qVar.b;
            }
            for (int size = pVar.c.size() - 1; size >= 0; size--) {
                pVar.a.d((View) pVar.c.get(size));
                pVar.c.remove(size);
            }
            pVar.a.b();
            this.q = brVar;
            if (brVar != null) {
                if (brVar.r != null) {
                    throw new IllegalArgumentException("LayoutManager " + brVar + " is already attached to a RecyclerView: " + brVar.r);
                }
                this.q.a(this);
                if (this.v) {
                    this.q.l();
                }
            }
            requestLayout();
        }
    }

    public final void a(bs bsVar) {
        if (this.G == null) {
            this.G = new ArrayList();
        }
        this.G.add(bsVar);
    }

    public final void a(bt btVar) {
        this.t.add(btVar);
    }

    public final void a(bu buVar) {
        if (this.ad == null) {
            this.ad = new ArrayList();
        }
        this.ad.add(buVar);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        if (!h()) {
            return;
        }
        if (str == null) {
            throw new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling");
        }
        throw new IllegalStateException(str);
    }

    public final void a(boolean z2) {
        this.w = z2;
    }

    public void addFocusables(ArrayList arrayList, int i2, int i3) {
        super.addFocusables(arrayList, i2, i3);
    }

    public final br b() {
        return this.q;
    }

    public final void b(int i2) {
        if (!this.A) {
            if (this.q == null) {
                Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            } else {
                this.q.a(this, i2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2, int i3) {
        if (i2 < 0) {
            p();
            this.J.a(-i2);
        } else if (i2 > 0) {
            q();
            this.L.a(i2);
        }
        if (i3 < 0) {
            r();
            this.K.a(-i3);
        } else if (i3 > 0) {
            s();
            this.M.a(i3);
        }
        if (i2 != 0 || i3 != 0) {
            bx.d(this);
        }
    }

    public final void b(bq bqVar) {
        a(bqVar);
    }

    public final void b(bs bsVar) {
        if (this.G != null) {
            this.G.remove(bsVar);
        }
    }

    public final void b(bt btVar) {
        this.t.remove(btVar);
        if (this.u == btVar) {
            this.u = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(boolean z2) {
        if (this.y) {
            if (z2 && this.z && !this.A && this.q != null && this.p != null) {
                A();
            }
            this.y = false;
            if (!this.A) {
                this.z = false;
            }
        }
    }

    public final int c() {
        return this.N;
    }

    public final cf c(int i2) {
        if (this.H) {
            return null;
        }
        int b2 = this.c.b();
        for (int i3 = 0; i3 < b2; i3++) {
            cf b3 = b(this.c.c(i3));
            if (b3 != null && !b3.m() && c(b3) == i2) {
                return b3;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void c(int i2, int i3) {
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX, scrollY);
        if (this.ac != null) {
            this.ac.a(this, i2, i3);
        }
        if (this.ad != null) {
            for (int size = this.ad.size() - 1; size >= 0; size--) {
                ((bu) this.ad.get(size)).a(this, i2, i3);
            }
        }
    }

    public final void c(bq bqVar) {
        if (this.q != null) {
            this.q.a("Cannot remove item decoration during a scroll  or layout");
        }
        this.s.remove(bqVar);
        if (this.s.isEmpty()) {
            setWillNotDraw(bx.a(this) == 2);
        }
        B();
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && this.q.a((LayoutParams) layoutParams);
    }

    public int computeHorizontalScrollExtent() {
        if (this.q.e()) {
            return this.q.c(this.f);
        }
        return 0;
    }

    public int computeHorizontalScrollOffset() {
        if (this.q.e()) {
            return this.q.a(this.f);
        }
        return 0;
    }

    public int computeHorizontalScrollRange() {
        if (this.q.e()) {
            return this.q.e(this.f);
        }
        return 0;
    }

    public int computeVerticalScrollExtent() {
        if (this.q.f()) {
            return this.q.d(this.f);
        }
        return 0;
    }

    public int computeVerticalScrollOffset() {
        if (this.q.f()) {
            return this.q.b(this.f);
        }
        return 0;
    }

    public int computeVerticalScrollRange() {
        if (this.q.f()) {
            return this.q.f(this.f);
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (!this.y) {
            this.y = true;
            if (!this.A) {
                this.z = false;
            }
        }
    }

    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return this.aj.a(f2, f3, z2);
    }

    public boolean dispatchNestedPreFling(float f2, float f3) {
        return this.aj.a(f2, f3);
    }

    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return this.aj.a(i2, i3, iArr, iArr2);
    }

    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return this.aj.a(i2, i3, i4, i5, iArr);
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    public void draw(Canvas canvas) {
        boolean z2;
        boolean z3 = true;
        boolean z4 = false;
        super.draw(canvas);
        int size = this.s.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((bq) this.s.get(i2)).b(canvas, this);
        }
        if (this.J == null || this.J.a()) {
            z2 = false;
        } else {
            int save = canvas.save();
            int paddingBottom = this.m ? getPaddingBottom() : 0;
            canvas.rotate(270.0f);
            canvas.translate((float) (paddingBottom + (-getHeight())), 0.0f);
            z2 = this.J != null && this.J.a(canvas);
            canvas.restoreToCount(save);
        }
        if (this.K != null && !this.K.a()) {
            int save2 = canvas.save();
            if (this.m) {
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
            }
            z2 |= this.K != null && this.K.a(canvas);
            canvas.restoreToCount(save2);
        }
        if (this.L != null && !this.L.a()) {
            int save3 = canvas.save();
            int width = getWidth();
            int paddingTop = this.m ? getPaddingTop() : 0;
            canvas.rotate(90.0f);
            canvas.translate((float) (-paddingTop), (float) (-width));
            z2 |= this.L != null && this.L.a(canvas);
            canvas.restoreToCount(save3);
        }
        if (this.M != null && !this.M.a()) {
            int save4 = canvas.save();
            canvas.rotate(180.0f);
            if (this.m) {
                canvas.translate((float) ((-getWidth()) + getPaddingRight()), (float) ((-getHeight()) + getPaddingBottom()));
            } else {
                canvas.translate((float) (-getWidth()), (float) (-getHeight()));
            }
            if (this.M != null && this.M.a(canvas)) {
                z4 = true;
            }
            z2 |= z4;
            canvas.restoreToCount(save4);
        }
        if (z2 || this.e == null || this.s.size() <= 0 || !this.e.b()) {
            z3 = z2;
        }
        if (z3) {
            bx.d(this);
        }
    }

    public boolean drawChild(Canvas canvas, View view, long j2) {
        return super.drawChild(canvas, view, j2);
    }

    public final int e() {
        return this.V;
    }

    /* access modifiers changed from: package-private */
    public final Rect e(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (!layoutParams.c) {
            return layoutParams.b;
        }
        Rect rect = layoutParams.b;
        rect.set(0, 0, 0, 0);
        int size = this.s.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.o.set(0, 0, 0, 0);
            ((bq) this.s.get(i2)).a(this.o, view);
            rect.left += this.o.left;
            rect.top += this.o.top;
            rect.right += this.o.right;
            rect.bottom += this.o.bottom;
        }
        layoutParams.c = false;
        return rect;
    }

    public final int f() {
        return this.W;
    }

    public View focusSearch(View view, int i2) {
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, view, i2);
        if (findNextFocus == null && this.p != null && this.q != null && !h() && !this.A) {
            d();
            findNextFocus = this.q.c(i2, this.a, this.f);
            b(false);
        }
        return findNextFocus != null ? findNextFocus : super.focusSearch(view, i2);
    }

    /* access modifiers changed from: package-private */
    public final boolean g() {
        return this.F != null && this.F.isEnabled();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        if (this.q != null) {
            return this.q.b();
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager");
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        if (this.q != null) {
            return this.q.a(getContext(), attributeSet);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager");
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (this.q != null) {
            return this.q.a(layoutParams);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager");
    }

    public int getBaseline() {
        if (this.q != null) {
            return -1;
        }
        return super.getBaseline();
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        return this.ah == null ? super.getChildDrawingOrder(i2, i3) : this.ah.a(i2, i3);
    }

    public final boolean h() {
        return this.I > 0;
    }

    public boolean hasNestedScrollingParent() {
        return this.aj.b();
    }

    public final bm i() {
        return this.e;
    }

    public boolean isAttachedToWindow() {
        return this.v;
    }

    public boolean isNestedScrollingEnabled() {
        return this.aj.a();
    }

    public final boolean j() {
        return !this.x || this.H || this.b.d();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.I = 0;
        this.v = true;
        this.x = false;
        if (this.q != null) {
            this.q.l();
        }
        this.af = false;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.e != null) {
            this.e.c();
        }
        this.x = false;
        n();
        this.v = false;
        if (this.q != null) {
            this.q.b(this, this.a);
        }
        removeCallbacks(this.an);
        dp.b();
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = this.s.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((bq) this.s.get(i2)).a(canvas, this);
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float f2 = 0.0f;
        if (this.q != null && !this.A && (ay.d(motionEvent) & 2) != 0 && motionEvent.getAction() == 8) {
            float f3 = this.q.f() ? -ay.e(motionEvent, 9) : 0.0f;
            float e2 = this.q.e() ? ay.e(motionEvent, 10) : 0.0f;
            if (!(f3 == 0.0f && e2 == 0.0f)) {
                if (this.aa == Float.MIN_VALUE) {
                    TypedValue typedValue = new TypedValue();
                    if (getContext().getTheme().resolveAttribute(16842829, typedValue, true)) {
                        this.aa = typedValue.getDimension(getContext().getResources().getDisplayMetrics());
                    }
                    a((int) (e2 * f2), (int) (f3 * f2), motionEvent);
                }
                f2 = this.aa;
                a((int) (e2 * f2), (int) (f3 * f2), motionEvent);
            }
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        boolean z3;
        int i2 = -1;
        if (this.A) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 3 || action == 0) {
            this.u = null;
        }
        int size = this.t.size();
        int i3 = 0;
        while (true) {
            if (i3 >= size) {
                z2 = false;
                break;
            }
            bt btVar = (bt) this.t.get(i3);
            if (btVar.a(motionEvent) && action != 3) {
                this.u = btVar;
                z2 = true;
                break;
            }
            i3++;
        }
        if (z2) {
            v();
            return true;
        } else if (this.q == null) {
            return false;
        } else {
            boolean e2 = this.q.e();
            boolean f2 = this.q.f();
            if (this.P == null) {
                this.P = VelocityTracker.obtain();
            }
            this.P.addMovement(motionEvent);
            int a2 = ay.a(motionEvent);
            int b2 = ay.b(motionEvent);
            switch (a2) {
                case 0:
                    if (this.B) {
                        this.B = false;
                    }
                    this.O = ay.b(motionEvent, 0);
                    int x2 = (int) (motionEvent.getX() + 0.5f);
                    this.S = x2;
                    this.Q = x2;
                    int y2 = (int) (motionEvent.getY() + 0.5f);
                    this.T = y2;
                    this.R = y2;
                    if (this.N == 2) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        d(1);
                    }
                    int[] iArr = this.am;
                    this.am[1] = 0;
                    iArr[0] = 0;
                    int i4 = e2 ? 1 : 0;
                    if (f2) {
                        i4 |= 2;
                    }
                    startNestedScroll(i4);
                    break;
                case 1:
                    this.P.clear();
                    stopNestedScroll();
                    break;
                case 2:
                    int a3 = ay.a(motionEvent, this.O);
                    if (a3 >= 0) {
                        int c2 = (int) (ay.c(motionEvent, a3) + 0.5f);
                        int d2 = (int) (ay.d(motionEvent, a3) + 0.5f);
                        if (this.N != 1) {
                            int i5 = c2 - this.Q;
                            int i6 = d2 - this.R;
                            if (!e2 || Math.abs(i5) <= this.U) {
                                z3 = false;
                            } else {
                                this.S = ((i5 < 0 ? -1 : 1) * this.U) + this.Q;
                                z3 = true;
                            }
                            if (f2 && Math.abs(i6) > this.U) {
                                int i7 = this.R;
                                int i8 = this.U;
                                if (i6 >= 0) {
                                    i2 = 1;
                                }
                                this.T = i7 + (i2 * i8);
                                z3 = true;
                            }
                            if (z3) {
                                d(1);
                                break;
                            }
                        }
                    } else {
                        Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.O + " not found. Did any MotionEvents get skipped?");
                        return false;
                    }
                    break;
                case 3:
                    v();
                    break;
                case 5:
                    this.O = ay.b(motionEvent, b2);
                    int c3 = (int) (ay.c(motionEvent, b2) + 0.5f);
                    this.S = c3;
                    this.Q = c3;
                    int d3 = (int) (ay.d(motionEvent, b2) + 0.5f);
                    this.T = d3;
                    this.R = d3;
                    break;
                case 6:
                    a(motionEvent);
                    break;
            }
            return this.N == 1;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        d();
        k.a("RV OnLayout");
        A();
        k.a();
        b(false);
        this.x = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.cc.b(android.support.v7.widget.cc, boolean):boolean
     arg types: [android.support.v7.widget.cc, int]
     candidates:
      android.support.v7.widget.cc.b(android.support.v7.widget.cc, int):int
      android.support.v7.widget.cc.b(android.support.v7.widget.cc, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.D) {
            d();
            z();
            if (this.f.i) {
                boolean unused = this.f.g = true;
            } else {
                this.b.e();
                boolean unused2 = this.f.g = false;
            }
            this.D = false;
            b(false);
        }
        if (this.p != null) {
            this.f.a = this.p.a();
        } else {
            this.f.a = 0;
        }
        if (this.q == null) {
            e(i2, i3);
        } else {
            this.q.a(this.a, this.f, i2, i3);
        }
        boolean unused3 = this.f.g = false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        this.l = (SavedState) parcelable;
        super.onRestoreInstanceState(this.l.getSuperState());
        if (this.q != null && this.l.a != null) {
            this.q.a(this.l.a);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (this.l != null) {
            savedState.a = this.l.a;
        } else if (this.q != null) {
            savedState.a = this.q.d();
        } else {
            savedState.a = null;
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4 || i3 != i5) {
            t();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01ec, code lost:
        if (r0 != false) goto L_0x01f1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0052  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r13) {
        /*
            r12 = this;
            r5 = 0
            r1 = 0
            r10 = 1056964608(0x3f000000, float:0.5)
            r3 = 1
            r2 = 0
            boolean r0 = r12.A
            if (r0 != 0) goto L_0x000e
            boolean r0 = r12.B
            if (r0 == 0) goto L_0x000f
        L_0x000e:
            return r2
        L_0x000f:
            int r0 = r13.getAction()
            android.support.v7.widget.bt r4 = r12.u
            if (r4 == 0) goto L_0x001b
            if (r0 != 0) goto L_0x003e
            r12.u = r5
        L_0x001b:
            if (r0 == 0) goto L_0x0050
            java.util.ArrayList r0 = r12.t
            int r5 = r0.size()
            r4 = r2
        L_0x0024:
            if (r4 >= r5) goto L_0x0050
            java.util.ArrayList r0 = r12.t
            java.lang.Object r0 = r0.get(r4)
            android.support.v7.widget.bt r0 = (android.support.v7.widget.bt) r0
            boolean r6 = r0.a(r13)
            if (r6 == 0) goto L_0x004c
            r12.u = r0
            r0 = r3
        L_0x0037:
            if (r0 == 0) goto L_0x0052
            r12.v()
            r2 = r3
            goto L_0x000e
        L_0x003e:
            android.support.v7.widget.bt r4 = r12.u
            r4.b(r13)
            r4 = 3
            if (r0 == r4) goto L_0x0048
            if (r0 != r3) goto L_0x004a
        L_0x0048:
            r12.u = r5
        L_0x004a:
            r0 = r3
            goto L_0x0037
        L_0x004c:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0024
        L_0x0050:
            r0 = r2
            goto L_0x0037
        L_0x0052:
            android.support.v7.widget.br r0 = r12.q
            if (r0 == 0) goto L_0x000e
            android.support.v7.widget.br r0 = r12.q
            boolean r5 = r0.e()
            android.support.v7.widget.br r0 = r12.q
            boolean r6 = r0.f()
            android.view.VelocityTracker r0 = r12.P
            if (r0 != 0) goto L_0x006c
            android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
            r12.P = r0
        L_0x006c:
            android.view.MotionEvent r7 = android.view.MotionEvent.obtain(r13)
            int r0 = android.support.v4.view.ay.a(r13)
            int r4 = android.support.v4.view.ay.b(r13)
            if (r0 != 0) goto L_0x0082
            int[] r8 = r12.am
            int[] r9 = r12.am
            r9[r3] = r2
            r8[r2] = r2
        L_0x0082:
            int[] r8 = r12.am
            r8 = r8[r2]
            float r8 = (float) r8
            int[] r9 = r12.am
            r9 = r9[r3]
            float r9 = (float) r9
            r7.offsetLocation(r8, r9)
            switch(r0) {
                case 0: goto L_0x009f;
                case 1: goto L_0x01b0;
                case 2: goto L_0x00df;
                case 3: goto L_0x025c;
                case 4: goto L_0x0092;
                case 5: goto L_0x00c4;
                case 6: goto L_0x01ab;
                default: goto L_0x0092;
            }
        L_0x0092:
            if (r2 != 0) goto L_0x0099
            android.view.VelocityTracker r0 = r12.P
            r0.addMovement(r7)
        L_0x0099:
            r7.recycle()
            r2 = r3
            goto L_0x000e
        L_0x009f:
            int r0 = android.support.v4.view.ay.b(r13, r2)
            r12.O = r0
            float r0 = r13.getX()
            float r0 = r0 + r10
            int r0 = (int) r0
            r12.S = r0
            r12.Q = r0
            float r0 = r13.getY()
            float r0 = r0 + r10
            int r0 = (int) r0
            r12.T = r0
            r12.R = r0
            if (r5 == 0) goto L_0x0268
            r0 = r3
        L_0x00bc:
            if (r6 == 0) goto L_0x00c0
            r0 = r0 | 2
        L_0x00c0:
            r12.startNestedScroll(r0)
            goto L_0x0092
        L_0x00c4:
            int r0 = android.support.v4.view.ay.b(r13, r4)
            r12.O = r0
            float r0 = android.support.v4.view.ay.c(r13, r4)
            float r0 = r0 + r10
            int r0 = (int) r0
            r12.S = r0
            r12.Q = r0
            float r0 = android.support.v4.view.ay.d(r13, r4)
            float r0 = r0 + r10
            int r0 = (int) r0
            r12.T = r0
            r12.R = r0
            goto L_0x0092
        L_0x00df:
            int r0 = r12.O
            int r0 = android.support.v4.view.ay.a(r13, r0)
            if (r0 >= 0) goto L_0x0105
            java.lang.String r0 = "RecyclerView"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "Error processing scroll; pointer index for id "
            r1.<init>(r3)
            int r3 = r12.O
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = " not found. Did any MotionEvents get skipped?"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            goto L_0x000e
        L_0x0105:
            float r1 = android.support.v4.view.ay.c(r13, r0)
            float r1 = r1 + r10
            int r8 = (int) r1
            float r0 = android.support.v4.view.ay.d(r13, r0)
            float r0 = r0 + r10
            int r9 = (int) r0
            int r0 = r12.S
            int r1 = r0 - r8
            int r0 = r12.T
            int r0 = r0 - r9
            int[] r4 = r12.al
            int[] r10 = r12.ak
            boolean r4 = r12.dispatchNestedPreScroll(r1, r0, r4, r10)
            if (r4 == 0) goto L_0x014f
            int[] r4 = r12.al
            r4 = r4[r2]
            int r1 = r1 - r4
            int[] r4 = r12.al
            r4 = r4[r3]
            int r0 = r0 - r4
            int[] r4 = r12.ak
            r4 = r4[r2]
            float r4 = (float) r4
            int[] r10 = r12.ak
            r10 = r10[r3]
            float r10 = (float) r10
            r7.offsetLocation(r4, r10)
            int[] r4 = r12.am
            r10 = r4[r2]
            int[] r11 = r12.ak
            r11 = r11[r2]
            int r10 = r10 + r11
            r4[r2] = r10
            int[] r4 = r12.am
            r10 = r4[r3]
            int[] r11 = r12.ak
            r11 = r11[r3]
            int r10 = r10 + r11
            r4[r3] = r10
        L_0x014f:
            int r4 = r12.N
            if (r4 == r3) goto L_0x0178
            if (r5 == 0) goto L_0x0265
            int r4 = java.lang.Math.abs(r1)
            int r10 = r12.U
            if (r4 <= r10) goto L_0x0265
            if (r1 <= 0) goto L_0x019f
            int r4 = r12.U
            int r1 = r1 - r4
        L_0x0162:
            r4 = r3
        L_0x0163:
            if (r6 == 0) goto L_0x0173
            int r10 = java.lang.Math.abs(r0)
            int r11 = r12.U
            if (r10 <= r11) goto L_0x0173
            if (r0 <= 0) goto L_0x01a3
            int r4 = r12.U
            int r0 = r0 - r4
        L_0x0172:
            r4 = r3
        L_0x0173:
            if (r4 == 0) goto L_0x0178
            r12.d(r3)
        L_0x0178:
            int r4 = r12.N
            if (r4 != r3) goto L_0x0092
            int[] r4 = r12.ak
            r4 = r4[r2]
            int r4 = r8 - r4
            r12.S = r4
            int[] r4 = r12.ak
            r4 = r4[r3]
            int r4 = r9 - r4
            r12.T = r4
            if (r5 == 0) goto L_0x01a7
        L_0x018e:
            if (r6 == 0) goto L_0x01a9
        L_0x0190:
            boolean r0 = r12.a(r1, r0, r7)
            if (r0 == 0) goto L_0x0092
            android.view.ViewParent r0 = r12.getParent()
            r0.requestDisallowInterceptTouchEvent(r3)
            goto L_0x0092
        L_0x019f:
            int r4 = r12.U
            int r1 = r1 + r4
            goto L_0x0162
        L_0x01a3:
            int r4 = r12.U
            int r0 = r0 + r4
            goto L_0x0172
        L_0x01a7:
            r1 = r2
            goto L_0x018e
        L_0x01a9:
            r0 = r2
            goto L_0x0190
        L_0x01ab:
            r12.a(r13)
            goto L_0x0092
        L_0x01b0:
            android.view.VelocityTracker r0 = r12.P
            r0.addMovement(r7)
            android.view.VelocityTracker r0 = r12.P
            r4 = 1000(0x3e8, float:1.401E-42)
            int r8 = r12.W
            float r8 = (float) r8
            r0.computeCurrentVelocity(r4, r8)
            if (r5 == 0) goto L_0x01f7
            android.view.VelocityTracker r0 = r12.P
            int r4 = r12.O
            float r0 = android.support.v4.view.bt.a(r0, r4)
            float r0 = -r0
            r4 = r0
        L_0x01cb:
            if (r6 == 0) goto L_0x01f9
            android.view.VelocityTracker r0 = r12.P
            int r5 = r12.O
            float r0 = android.support.v4.view.bt.b(r0, r5)
            float r0 = -r0
        L_0x01d6:
            int r5 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r5 != 0) goto L_0x01de
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 == 0) goto L_0x01ee
        L_0x01de:
            int r1 = (int) r4
            int r0 = (int) r0
            android.support.v7.widget.br r4 = r12.q
            if (r4 != 0) goto L_0x01fb
            java.lang.String r0 = "RecyclerView"
            java.lang.String r1 = "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument."
            android.util.Log.e(r0, r1)
        L_0x01eb:
            r0 = r2
        L_0x01ec:
            if (r0 != 0) goto L_0x01f1
        L_0x01ee:
            r12.d(r2)
        L_0x01f1:
            r12.u()
            r2 = r3
            goto L_0x0092
        L_0x01f7:
            r4 = r1
            goto L_0x01cb
        L_0x01f9:
            r0 = r1
            goto L_0x01d6
        L_0x01fb:
            boolean r4 = r12.A
            if (r4 != 0) goto L_0x01eb
            android.support.v7.widget.br r4 = r12.q
            boolean r5 = r4.e()
            android.support.v7.widget.br r4 = r12.q
            boolean r6 = r4.f()
            if (r5 == 0) goto L_0x0215
            int r4 = java.lang.Math.abs(r1)
            int r8 = r12.V
            if (r4 >= r8) goto L_0x0263
        L_0x0215:
            r4 = r2
        L_0x0216:
            if (r6 == 0) goto L_0x0220
            int r1 = java.lang.Math.abs(r0)
            int r8 = r12.V
            if (r1 >= r8) goto L_0x0261
        L_0x0220:
            r1 = r2
        L_0x0221:
            if (r4 != 0) goto L_0x0225
            if (r1 == 0) goto L_0x01eb
        L_0x0225:
            float r0 = (float) r4
            float r8 = (float) r1
            boolean r0 = r12.dispatchNestedPreFling(r0, r8)
            if (r0 != 0) goto L_0x01eb
            if (r5 != 0) goto L_0x0231
            if (r6 == 0) goto L_0x025a
        L_0x0231:
            r0 = r3
        L_0x0232:
            float r5 = (float) r4
            float r6 = (float) r1
            r12.dispatchNestedFling(r5, r6, r0)
            if (r0 == 0) goto L_0x01eb
            int r0 = r12.W
            int r0 = -r0
            int r5 = r12.W
            int r4 = java.lang.Math.min(r4, r5)
            int r0 = java.lang.Math.max(r0, r4)
            int r4 = r12.W
            int r4 = -r4
            int r5 = r12.W
            int r1 = java.lang.Math.min(r1, r5)
            int r1 = java.lang.Math.max(r4, r1)
            android.support.v7.widget.ce r4 = r12.ab
            r4.a(r0, r1)
            r0 = r3
            goto L_0x01ec
        L_0x025a:
            r0 = r2
            goto L_0x0232
        L_0x025c:
            r12.v()
            goto L_0x0092
        L_0x0261:
            r1 = r0
            goto L_0x0221
        L_0x0263:
            r4 = r1
            goto L_0x0216
        L_0x0265:
            r4 = r2
            goto L_0x0163
        L_0x0268:
            r0 = r2
            goto L_0x00bc
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: protected */
    public void removeDetachedView(View view, boolean z2) {
        cf b2 = b(view);
        if (b2 != null) {
            if (b2.n()) {
                b2.i();
            } else if (!b2.d_()) {
                throw new IllegalArgumentException("Called removeDetachedView with a view which is not flagged as tmp detached." + b2);
            }
        }
        f(view);
        super.removeDetachedView(view, z2);
    }

    public void requestChildFocus(View view, View view2) {
        if (!(this.q.n() || h()) && view2 != null) {
            this.o.set(0, 0, view2.getWidth(), view2.getHeight());
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams instanceof LayoutParams) {
                LayoutParams layoutParams2 = (LayoutParams) layoutParams;
                if (!layoutParams2.c) {
                    Rect rect = layoutParams2.b;
                    this.o.left -= rect.left;
                    this.o.right += rect.right;
                    this.o.top -= rect.top;
                    Rect rect2 = this.o;
                    rect2.bottom = rect.bottom + rect2.bottom;
                }
            }
            offsetDescendantRectToMyCoords(view2, this.o);
            offsetRectIntoDescendantCoords(view, this.o);
            requestChildRectangleOnScreen(view, this.o, !this.x);
        }
        super.requestChildFocus(view, view2);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z2) {
        int min;
        br brVar = this.q;
        int r2 = brVar.r();
        int s2 = brVar.s();
        int p2 = brVar.p() - brVar.t();
        int q2 = brVar.q() - brVar.u();
        int left = view.getLeft() + rect.left;
        int top = view.getTop() + rect.top;
        int width = left + rect.width();
        int height = top + rect.height();
        int min2 = Math.min(0, left - r2);
        int min3 = Math.min(0, top - s2);
        int max = Math.max(0, width - p2);
        int max2 = Math.max(0, height - q2);
        if (bx.h(brVar.r) == 1) {
            if (max == 0) {
                max = Math.max(min2, width - p2);
            }
            min = max;
        } else {
            min = min2 != 0 ? min2 : Math.min(left - r2, max);
        }
        int min4 = min3 != 0 ? min3 : Math.min(top - s2, max2);
        if (min == 0 && min4 == 0) {
            return false;
        }
        if (z2) {
            scrollBy(min, min4);
        } else {
            a(min, min4);
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        int size = this.t.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((bt) this.t.get(i2)).a(z2);
        }
        super.requestDisallowInterceptTouchEvent(z2);
    }

    public void requestLayout() {
        if (this.y || this.A) {
            this.z = true;
        } else {
            super.requestLayout();
        }
    }

    public void scrollBy(int i2, int i3) {
        if (this.q == null) {
            Log.e("RecyclerView", "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.A) {
            boolean e2 = this.q.e();
            boolean f2 = this.q.f();
            if (e2 || f2) {
                if (!e2) {
                    i2 = 0;
                }
                if (!f2) {
                    i3 = 0;
                }
                a(i2, i3, (MotionEvent) null);
            }
        }
    }

    public void scrollTo(int i2, int i3) {
        Log.w("RecyclerView", "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        int i2 = 0;
        if (h()) {
            int b2 = accessibilityEvent != null ? a.b(accessibilityEvent) : 0;
            if (b2 != 0) {
                i2 = b2;
            }
            this.C = i2 | this.C;
            i2 = 1;
        }
        if (i2 == 0) {
            super.sendAccessibilityEventUnchecked(accessibilityEvent);
        }
    }

    public void setClipToPadding(boolean z2) {
        if (z2 != this.m) {
            t();
        }
        this.m = z2;
        super.setClipToPadding(z2);
        if (this.x) {
            requestLayout();
        }
    }

    public void setNestedScrollingEnabled(boolean z2) {
        this.aj.a(z2);
    }

    public boolean startNestedScroll(int i2) {
        return this.aj.a(i2);
    }

    public void stopNestedScroll() {
        this.aj.c();
    }
}
