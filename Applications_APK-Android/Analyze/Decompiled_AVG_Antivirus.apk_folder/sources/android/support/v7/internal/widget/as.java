package android.support.v7.internal.widget;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ListAdapter;

final class as implements DialogInterface.OnClickListener, ax {
    final /* synthetic */ SpinnerCompat a;
    private AlertDialog b;
    private ListAdapter c;
    private CharSequence d;

    private as(SpinnerCompat spinnerCompat) {
        this.a = spinnerCompat;
    }

    /* synthetic */ as(SpinnerCompat spinnerCompat, byte b2) {
        this(spinnerCompat);
    }

    public final void a() {
        if (this.b != null) {
            this.b.dismiss();
            this.b = null;
        }
    }

    public final void a(ListAdapter listAdapter) {
        this.c = listAdapter;
    }

    public final void a(CharSequence charSequence) {
        this.d = charSequence;
    }

    public final boolean b() {
        if (this.b != null) {
            return this.b.isShowing();
        }
        return false;
    }

    public final void c() {
        if (this.c != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a.getContext());
            if (this.d != null) {
                builder.setTitle(this.d);
            }
            this.b = builder.setSingleChoiceItems(this.c, this.a.u, this).create();
            this.b.show();
        }
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a(i);
        if (this.a.s != null) {
            SpinnerCompat spinnerCompat = this.a;
            this.c.getItemId(i);
            spinnerCompat.a((View) null);
        }
        a();
    }
}
