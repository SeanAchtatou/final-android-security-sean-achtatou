package android.support.v7.internal.widget;

import android.view.View;

final class an implements Runnable {
    final /* synthetic */ View a;
    final /* synthetic */ am b;

    an(am amVar, View view) {
        this.b = amVar;
        this.a = view;
    }

    public final void run() {
        this.b.smoothScrollTo(this.a.getLeft() - ((this.b.getWidth() - this.a.getWidth()) / 2), 0);
        this.b.a = null;
    }
}
