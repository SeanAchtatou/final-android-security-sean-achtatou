package android.support.v7.internal.view;

import android.content.res.TypedArray;
import android.support.v4.view.as;
import android.support.v4.view.n;
import android.support.v7.a.l;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.o;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import androidx.core.internal.view.SupportMenu;

final class h {
    final /* synthetic */ f a;
    private Menu b;
    private int c;
    private int d;
    private int e;
    private int f;
    private boolean g;
    private boolean h;
    private boolean i;
    private int j;
    private int k;
    private CharSequence l;
    private CharSequence m;
    private int n;
    private char o;
    private char p;
    private int q;
    private boolean r;
    private boolean s;
    private boolean t;
    private int u;
    private int v;
    private String w;
    private String x;
    private String y;
    /* access modifiers changed from: private */
    public n z;

    public h(f fVar, Menu menu) {
        this.a = fVar;
        this.b = menu;
        a();
    }

    private static char a(String str) {
        if (str == null) {
            return 0;
        }
        return str.charAt(0);
    }

    private Object a(String str, Class[] clsArr, Object[] objArr) {
        try {
            return this.a.e.getClassLoader().loadClass(str).getConstructor(clsArr).newInstance(objArr);
        } catch (Exception e2) {
            Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e2);
            return null;
        }
    }

    private void a(MenuItem menuItem) {
        boolean z2 = true;
        menuItem.setChecked(this.r).setVisible(this.s).setEnabled(this.t).setCheckable(this.q > 0).setTitleCondensed(this.m).setIcon(this.n).setAlphabeticShortcut(this.o).setNumericShortcut(this.p);
        if (this.u >= 0) {
            as.a(menuItem, this.u);
        }
        if (this.y != null) {
            if (this.a.e.isRestricted()) {
                throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
            }
            menuItem.setOnMenuItemClickListener(new g(f.c(this.a), this.y));
        }
        if (this.q >= 2) {
            if (menuItem instanceof m) {
                ((m) menuItem).a(true);
            } else if (menuItem instanceof o) {
                ((o) menuItem).b();
            }
        }
        if (this.w != null) {
            as.a(menuItem, (View) a(this.w, f.a, this.a.c));
        } else {
            z2 = false;
        }
        if (this.v > 0) {
            if (!z2) {
                as.b(menuItem, this.v);
            } else {
                Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
            }
        }
        if (this.z != null) {
            as.a(menuItem, this.z);
        }
    }

    public final void a() {
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = true;
        this.h = true;
    }

    public final void a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = this.a.e.obtainStyledAttributes(attributeSet, l.aj);
        this.c = obtainStyledAttributes.getResourceId(l.am, 0);
        this.d = obtainStyledAttributes.getInt(l.an, 0);
        this.e = obtainStyledAttributes.getInt(l.ao, 0);
        this.f = obtainStyledAttributes.getInt(l.ak, 0);
        this.g = obtainStyledAttributes.getBoolean(l.ap, true);
        this.h = obtainStyledAttributes.getBoolean(l.al, true);
        obtainStyledAttributes.recycle();
    }

    public final void b() {
        this.i = true;
        a(this.b.add(this.c, this.j, this.k, this.l));
    }

    public final void b(AttributeSet attributeSet) {
        boolean z2 = true;
        TypedArray obtainStyledAttributes = this.a.e.obtainStyledAttributes(attributeSet, l.aq);
        this.j = obtainStyledAttributes.getResourceId(l.az, 0);
        this.k = (obtainStyledAttributes.getInt(l.aA, this.d) & SupportMenu.CATEGORY_MASK) | (obtainStyledAttributes.getInt(l.aD, this.e) & SupportMenu.USER_MASK);
        this.l = obtainStyledAttributes.getText(l.aE);
        this.m = obtainStyledAttributes.getText(l.aF);
        this.n = obtainStyledAttributes.getResourceId(l.ay, 0);
        this.o = a(obtainStyledAttributes.getString(l.au));
        this.p = a(obtainStyledAttributes.getString(l.aB));
        if (obtainStyledAttributes.hasValue(l.av)) {
            this.q = obtainStyledAttributes.getBoolean(l.av, false) ? 1 : 0;
        } else {
            this.q = this.f;
        }
        this.r = obtainStyledAttributes.getBoolean(l.aw, false);
        this.s = obtainStyledAttributes.getBoolean(l.aG, this.g);
        this.t = obtainStyledAttributes.getBoolean(l.ax, this.h);
        this.u = obtainStyledAttributes.getInt(l.aH, -1);
        this.y = obtainStyledAttributes.getString(l.aC);
        this.v = obtainStyledAttributes.getResourceId(l.ar, 0);
        this.w = obtainStyledAttributes.getString(l.at);
        this.x = obtainStyledAttributes.getString(l.as);
        if (this.x == null) {
            z2 = false;
        }
        if (z2 && this.v == 0 && this.w == null) {
            this.z = (n) a(this.x, f.b, this.a.d);
        } else {
            if (z2) {
                Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
            }
            this.z = null;
        }
        obtainStyledAttributes.recycle();
        this.i = false;
    }

    public final SubMenu c() {
        this.i = true;
        SubMenu addSubMenu = this.b.addSubMenu(this.c, this.j, this.k, this.l);
        a(addSubMenu.getItem());
        return addSubMenu;
    }

    public final boolean d() {
        return this.i;
    }
}
