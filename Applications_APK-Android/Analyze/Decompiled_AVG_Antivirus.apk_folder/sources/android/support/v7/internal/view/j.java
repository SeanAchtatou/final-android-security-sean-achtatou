package android.support.v7.internal.view;

import android.support.v4.view.em;
import android.view.View;

final class j extends em {
    final /* synthetic */ i a;
    private boolean b = false;
    private int c = 0;

    j(i iVar) {
        this.a = iVar;
    }

    public final void a(View view) {
        if (!this.b) {
            this.b = true;
            if (this.a.d != null) {
                this.a.d.a(null);
            }
        }
    }

    public final void b(View view) {
        int i = this.c + 1;
        this.c = i;
        if (i == this.a.a.size()) {
            if (this.a.d != null) {
                this.a.d.b(null);
            }
            this.c = 0;
            this.b = false;
            this.a.e = false;
        }
    }
}
