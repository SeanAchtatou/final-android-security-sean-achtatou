package android.support.v7.internal.widget;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.bx;
import android.support.v4.view.v;
import android.support.v7.a.l;
import android.support.v7.internal.widget.AbsSpinnerCompat;
import android.support.v7.widget.ap;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.SpinnerAdapter;

final class SpinnerCompat extends AbsSpinnerCompat implements DialogInterface.OnClickListener {
    int D;
    private ap E;
    /* access modifiers changed from: private */
    public ax F;
    private at G;
    private int H;
    private boolean I;
    private Rect J;
    private final bc K;

    class SavedState extends AbsSpinnerCompat.SavedState {
        public static final Parcelable.Creator CREATOR = new aw();
        boolean c;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.c = parcel.readByte() != 0;
        }

        /* synthetic */ SavedState(Parcel parcel, byte b) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeByte((byte) (this.c ? 1 : 0));
        }
    }

    SpinnerCompat(Context context, int i) {
        this(context, i, (byte) 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.be.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.be.a(int, int):int
      android.support.v7.internal.widget.be.a(int, boolean):boolean */
    private SpinnerCompat(Context context, int i, byte b) {
        super(context, i);
        this.J = new Rect();
        be a = be.a(context, null, l.bi, i);
        if (a.f(l.bj)) {
            setBackgroundDrawable(a.a(l.bj));
        }
        switch (a.a(l.bp, 0)) {
            case 0:
                this.F = new as(this, (byte) 0);
                break;
            case 1:
                au auVar = new au(this, context, i);
                this.D = a.e(l.bk, -2);
                auVar.a(a.a(l.bm));
                this.F = auVar;
                this.E = new aq(this, this, auVar);
                break;
        }
        this.H = a.a(l.bl, 17);
        this.F.a(a.d(l.bo));
        this.I = a.a(l.bn, false);
        a.b();
        if (this.G != null) {
            this.F.a(this.G);
            this.G = null;
        }
        this.K = a.c();
    }

    private View a(int i, boolean z) {
        View a;
        if (this.t || (a = this.i.a(i)) == null) {
            View view = this.a.getView(i, null, this);
            a(view, z);
            return view;
        }
        a(a, z);
        return a;
    }

    private void a(View view, boolean z) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = generateDefaultLayoutParams();
        }
        if (z) {
            addViewInLayout(view, 0, layoutParams);
        }
        view.setSelected(hasFocus());
        if (this.I) {
            view.setEnabled(isEnabled());
        }
        view.measure(ViewGroup.getChildMeasureSpec(this.c, this.h.left + this.h.right, layoutParams.width), ViewGroup.getChildMeasureSpec(this.b, this.h.top + this.h.bottom, layoutParams.height));
        int measuredHeight = this.h.top + ((((getMeasuredHeight() - this.h.bottom) - this.h.top) - view.getMeasuredHeight()) / 2);
        view.layout(0, measuredHeight, view.getMeasuredWidth() + 0, view.getMeasuredHeight() + measuredHeight);
    }

    public final void a(aa aaVar) {
        throw new RuntimeException("setOnItemClickListener cannot be used with a spinner.");
    }

    public final void a(SpinnerAdapter spinnerAdapter) {
        super.a(spinnerAdapter);
        this.i.a();
        if (getContext().getApplicationInfo().targetSdkVersion >= 21 && spinnerAdapter != null && spinnerAdapter.getViewTypeCount() != 1) {
            throw new IllegalArgumentException("Spinner adapter view type count must be 1");
        } else if (this.F != null) {
            this.F.a(new at(spinnerAdapter));
        } else {
            this.G = new at(spinnerAdapter);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(aa aaVar) {
        super.a(aaVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.SpinnerCompat.a(int, boolean):android.view.View
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.SpinnerCompat.a(android.view.View, boolean):void
      android.support.v7.internal.widget.AbsSpinnerCompat.a(android.support.v7.internal.widget.AbsSpinnerCompat, android.view.View):void
      android.support.v7.internal.widget.y.a(android.support.v7.internal.widget.y, android.os.Parcelable):void
      android.support.v7.internal.widget.SpinnerCompat.a(int, boolean):android.view.View */
    public final int getBaseline() {
        int baseline;
        View view = null;
        if (getChildCount() > 0) {
            view = getChildAt(0);
        } else if (this.a != null && this.a.getCount() > 0) {
            view = a(0, false);
            this.i.a(0, view);
        }
        if (view == null || (baseline = view.getBaseline()) < 0) {
            return -1;
        }
        return view.getTop() + baseline;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        a(i);
        dialogInterface.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.F != null && this.F.b()) {
            this.F.a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.SpinnerCompat.a(int, boolean):android.view.View
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.SpinnerCompat.a(android.view.View, boolean):void
      android.support.v7.internal.widget.AbsSpinnerCompat.a(android.support.v7.internal.widget.AbsSpinnerCompat, android.view.View):void
      android.support.v7.internal.widget.y.a(android.support.v7.internal.widget.y, android.os.Parcelable):void
      android.support.v7.internal.widget.SpinnerCompat.a(int, boolean):android.view.View */
    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.q = true;
        int i5 = this.h.left;
        int right = ((getRight() - getLeft()) - this.h.left) - this.h.right;
        if (this.t) {
            e();
        }
        if (this.y == 0) {
            a();
        } else {
            if (this.u >= 0) {
                b(this.u);
            }
            int childCount = getChildCount();
            c cVar = this.i;
            int i6 = this.j;
            for (int i7 = 0; i7 < childCount; i7++) {
                cVar.a(i6 + i7, getChildAt(i7));
            }
            removeAllViewsInLayout();
            this.j = this.w;
            if (this.a != null) {
                View a = a(this.w, true);
                int measuredWidth = a.getMeasuredWidth();
                switch (v.a(this.H, bx.h(this)) & 7) {
                    case 1:
                        i5 = (i5 + (right / 2)) - (measuredWidth / 2);
                        break;
                    case 5:
                        i5 = (i5 + right) - measuredWidth;
                        break;
                }
                a.offsetLeftAndRight(i5);
            }
            this.i.a();
            invalidate();
            f();
            this.t = false;
            this.o = false;
            c(this.w);
        }
        this.q = false;
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        View view;
        int i3 = 0;
        super.onMeasure(i, i2);
        if (this.F != null && View.MeasureSpec.getMode(i) == Integer.MIN_VALUE) {
            int measuredWidth = getMeasuredWidth();
            SpinnerAdapter spinnerAdapter = this.a;
            Drawable background = getBackground();
            if (spinnerAdapter != null) {
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
                int max = Math.max(0, this.u);
                int min = Math.min(spinnerAdapter.getCount(), max + 15);
                int max2 = Math.max(0, max - (15 - (min - max)));
                View view2 = null;
                int i4 = 0;
                int i5 = 0;
                while (max2 < min) {
                    int itemViewType = spinnerAdapter.getItemViewType(max2);
                    if (itemViewType != i5) {
                        view = null;
                    } else {
                        itemViewType = i5;
                        view = view2;
                    }
                    view2 = spinnerAdapter.getView(max2, view, this);
                    if (view2.getLayoutParams() == null) {
                        view2.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
                    }
                    view2.measure(makeMeasureSpec, makeMeasureSpec2);
                    i4 = Math.max(i4, view2.getMeasuredWidth());
                    max2++;
                    i5 = itemViewType;
                }
                if (background != null) {
                    background.getPadding(this.J);
                    i3 = this.J.left + this.J.right + i4;
                } else {
                    i3 = i4;
                }
            }
            setMeasuredDimension(Math.min(Math.max(measuredWidth, i3), View.MeasureSpec.getSize(i)), getMeasuredHeight());
        }
    }

    public final void onRestoreInstanceState(Parcelable parcelable) {
        ViewTreeObserver viewTreeObserver;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.c && (viewTreeObserver = getViewTreeObserver()) != null) {
            viewTreeObserver.addOnGlobalLayoutListener(new ar(this));
        }
    }

    public final Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.c = this.F != null && this.F.b();
        return savedState;
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.E == null || !this.E.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public final boolean performClick() {
        boolean performClick = super.performClick();
        if (!performClick) {
            performClick = true;
            if (!this.F.b()) {
                this.F.c();
            }
        }
        return performClick;
    }

    public final void setEnabled(boolean z) {
        super.setEnabled(z);
        if (this.I) {
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                getChildAt(i).setEnabled(z);
            }
        }
    }
}
