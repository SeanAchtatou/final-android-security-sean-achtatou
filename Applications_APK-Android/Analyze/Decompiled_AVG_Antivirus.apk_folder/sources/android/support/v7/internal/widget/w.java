package android.support.v7.internal.widget;

import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v4.view.bx;
import android.support.v7.a.g;
import android.support.v7.a.i;
import android.support.v7.a.j;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

final class w extends BaseAdapter {
    final /* synthetic */ ActivityChooserView a;
    private m b;
    private int c;
    private boolean d;
    private boolean e;
    private boolean f;

    private w(ActivityChooserView activityChooserView) {
        this.a = activityChooserView;
        this.c = 4;
    }

    /* synthetic */ w(ActivityChooserView activityChooserView, byte b2) {
        this(activityChooserView);
    }

    public final int a() {
        int i = this.c;
        this.c = Integer.MAX_VALUE;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int count = getCount();
        View view = null;
        int i2 = 0;
        for (int i3 = 0; i3 < count; i3++) {
            view = getView(i3, view, null);
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            i2 = Math.max(i2, view.getMeasuredWidth());
        }
        this.c = i;
        return i2;
    }

    public final void a(int i) {
        if (this.c != i) {
            this.c = i;
            notifyDataSetChanged();
        }
    }

    public final void a(boolean z) {
        if (this.f != z) {
            this.f = z;
            notifyDataSetChanged();
        }
    }

    public final void a(boolean z, boolean z2) {
        if (this.d != z || this.e != z2) {
            this.d = z;
            this.e = z2;
            notifyDataSetChanged();
        }
    }

    public final ResolveInfo b() {
        return this.b.b();
    }

    public final int c() {
        return this.b.a();
    }

    public final int d() {
        return this.b.c();
    }

    public final m e() {
        return this.b;
    }

    public final boolean f() {
        return this.d;
    }

    public final int getCount() {
        int a2 = this.b.a();
        if (!this.d && this.b.b() != null) {
            a2--;
        }
        int min = Math.min(a2, this.c);
        return this.f ? min + 1 : min;
    }

    public final Object getItem(int i) {
        switch (getItemViewType(i)) {
            case 0:
                if (!this.d && this.b.b() != null) {
                    i++;
                }
                return this.b.a(i);
            case 1:
                return null;
            default:
                throw new IllegalArgumentException();
        }
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final int getItemViewType(int i) {
        return (!this.f || i != getCount() + -1) ? 0 : 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, float):void
      android.support.v4.view.bx.b(android.view.View, int):boolean
      android.support.v4.view.bx.b(android.view.View, boolean):void */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        switch (getItemViewType(i)) {
            case 0:
                if (view == null || view.getId() != g.list_item) {
                    view = LayoutInflater.from(this.a.getContext()).inflate(i.f, viewGroup, false);
                }
                PackageManager packageManager = this.a.getContext().getPackageManager();
                ResolveInfo resolveInfo = (ResolveInfo) getItem(i);
                ((ImageView) view.findViewById(g.icon)).setImageDrawable(resolveInfo.loadIcon(packageManager));
                ((TextView) view.findViewById(g.title)).setText(resolveInfo.loadLabel(packageManager));
                if (!this.d || i != 0 || !this.e) {
                    bx.b(view, false);
                    return view;
                }
                bx.b(view, true);
                return view;
            case 1:
                if (view != null && view.getId() == 1) {
                    return view;
                }
                View inflate = LayoutInflater.from(this.a.getContext()).inflate(i.f, viewGroup, false);
                inflate.setId(1);
                ((TextView) inflate.findViewById(g.title)).setText(this.a.getContext().getString(j.abc_activity_chooser_view_see_all));
                return inflate;
            default:
                throw new IllegalArgumentException();
        }
    }

    public final int getViewTypeCount() {
        return 3;
    }
}
