package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.bx;
import android.support.v4.view.dv;
import android.support.v4.view.el;
import android.support.v7.a.b;
import android.support.v7.a.g;
import android.support.v7.a.l;
import android.support.v7.d.a;
import android.support.v7.internal.view.i;
import android.support.v7.widget.ActionMenuPresenter;
import android.support.v7.widget.ActionMenuView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActionBarContextView extends a implements el {
    private CharSequence i;
    private CharSequence j;
    private View k;
    private View l;
    private LinearLayout m;
    private TextView n;
    private TextView o;
    private int p;
    private int q;
    private Drawable r;
    private boolean s;
    private int t;
    private i u;
    private boolean v;
    private int w;

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.actionModeStyle);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        be a = be.a(context, attributeSet, l.y, i2);
        setBackgroundDrawable(a.a(l.z));
        this.p = a.f(l.E, 0);
        this.q = a.f(l.D, 0);
        this.g = a.e(l.C, 0);
        this.r = a.a(l.A);
        this.t = a.f(l.B, android.support.v7.a.i.d);
        a.b();
    }

    private void g() {
        int i2 = 8;
        boolean z = true;
        if (this.m == null) {
            LayoutInflater.from(getContext()).inflate(android.support.v7.a.i.a, this);
            this.m = (LinearLayout) getChildAt(getChildCount() - 1);
            this.n = (TextView) this.m.findViewById(g.action_bar_title);
            this.o = (TextView) this.m.findViewById(g.action_bar_subtitle);
            if (this.p != 0) {
                this.n.setTextAppearance(getContext(), this.p);
            }
            if (this.q != 0) {
                this.o.setTextAppearance(getContext(), this.q);
            }
        }
        this.n.setText(this.i);
        this.o.setText(this.j);
        boolean z2 = !TextUtils.isEmpty(this.i);
        if (TextUtils.isEmpty(this.j)) {
            z = false;
        }
        this.o.setVisibility(z ? 0 : 8);
        LinearLayout linearLayout = this.m;
        if (z2 || z) {
            i2 = 0;
        }
        linearLayout.setVisibility(i2);
        if (this.m.getParent() == null) {
            addView(this.m);
        }
    }

    private void h() {
        i iVar = this.u;
        if (iVar != null) {
            this.u = null;
            iVar.b();
        }
    }

    public final void a(int i2) {
        this.g = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.internal.widget.ActionBarContextView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final void a(a aVar) {
        if (this.k == null) {
            this.k = LayoutInflater.from(getContext()).inflate(this.t, (ViewGroup) this, false);
            addView(this.k);
        } else if (this.k.getParent() == null) {
            addView(this.k);
        }
        this.k.findViewById(g.action_mode_close_button).setOnClickListener(new g(this, aVar));
        android.support.v7.internal.view.menu.i iVar = (android.support.v7.internal.view.menu.i) aVar.b();
        if (this.d != null) {
            this.d.k();
        }
        this.d = new ActionMenuPresenter(getContext());
        this.d.f();
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
        if (!this.f) {
            iVar.a(this.d, this.b);
            this.c = (ActionMenuView) this.d.a(this);
            this.c.setBackgroundDrawable(null);
            addView(this.c, layoutParams);
        } else {
            this.d.b(getContext().getResources().getDisplayMetrics().widthPixels);
            this.d.g();
            layoutParams.width = -1;
            layoutParams.height = this.g;
            iVar.a(this.d, this.b);
            this.c = (ActionMenuView) this.d.a(this);
            this.c.setBackgroundDrawable(this.r);
            this.e.addView(this.c, layoutParams);
        }
        this.v = true;
    }

    public final void a(View view) {
    }

    public final void a(CharSequence charSequence) {
        this.i = charSequence;
        g();
    }

    public final void a(boolean z) {
        if (z != this.s) {
            requestLayout();
        }
        this.s = z;
    }

    public final boolean a() {
        if (this.d != null) {
            return this.d.i();
        }
        return false;
    }

    public final CharSequence b() {
        return this.i;
    }

    public final /* bridge */ /* synthetic */ void b(int i2) {
        super.b(i2);
    }

    public final void b(View view) {
        if (this.w == 2) {
            e();
        }
        this.w = 0;
    }

    public final void b(CharSequence charSequence) {
        this.j = charSequence;
        g();
    }

    public final CharSequence c() {
        return this.j;
    }

    public final void c(View view) {
    }

    public final void d() {
        if (this.w != 2) {
            if (this.k == null) {
                e();
                return;
            }
            h();
            this.w = 2;
            dv b = bx.t(this.k).b((float) ((-this.k.getWidth()) - ((ViewGroup.MarginLayoutParams) this.k.getLayoutParams()).leftMargin));
            b.a(200L);
            b.a(this);
            b.a(new DecelerateInterpolator());
            i iVar = new i();
            iVar.a(b);
            if (this.c != null) {
                this.c.getChildCount();
            }
            this.u = iVar;
            this.u.a();
        }
    }

    public final void d(View view) {
        if (this.l != null) {
            removeView(this.l);
        }
        this.l = view;
        if (this.m != null) {
            removeView(this.m);
            this.m = null;
        }
        if (view != null) {
            addView(view);
        }
        requestLayout();
    }

    public final void e() {
        h();
        removeAllViews();
        if (this.e != null) {
            this.e.removeView(this.c);
        }
        this.l = null;
        this.c = null;
        this.v = false;
    }

    public final boolean f() {
        return this.s;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.d != null) {
            this.d.j();
            this.d.l();
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (Build.VERSION.SDK_INT < 14) {
            return;
        }
        if (accessibilityEvent.getEventType() == 32) {
            accessibilityEvent.setSource(this);
            accessibilityEvent.setClassName(getClass().getName());
            accessibilityEvent.setPackageName(getContext().getPackageName());
            accessibilityEvent.setContentDescription(this.i);
            return;
        }
        super.onInitializeAccessibilityEvent(accessibilityEvent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.e(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.e(android.view.View, int):void
      android.support.v4.view.bx.e(android.view.View, float):void */
    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount;
        boolean a = bj.a(this);
        int paddingRight = a ? (i4 - i2) - getPaddingRight() : getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i5 - i3) - getPaddingTop()) - getPaddingBottom();
        if (!(this.k == null || this.k.getVisibility() == 8)) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.k.getLayoutParams();
            int i6 = a ? marginLayoutParams.rightMargin : marginLayoutParams.leftMargin;
            int i7 = a ? marginLayoutParams.leftMargin : marginLayoutParams.rightMargin;
            int a2 = a(paddingRight, i6, a);
            paddingRight = a(a2 + a(this.k, a2, paddingTop, paddingTop2, a), i7, a);
            if (this.v) {
                this.w = 1;
                bx.a(this.k, (float) ((-this.k.getWidth()) - ((ViewGroup.MarginLayoutParams) this.k.getLayoutParams()).leftMargin));
                dv b = bx.t(this.k).b(0.0f);
                b.a(200L);
                b.a(this);
                b.a(new DecelerateInterpolator());
                i iVar = new i();
                iVar.a(b);
                if (this.c != null && (childCount = this.c.getChildCount()) > 0) {
                    for (int i8 = childCount - 1; i8 >= 0; i8--) {
                        View childAt = this.c.getChildAt(i8);
                        bx.e(childAt, 0.0f);
                        dv a3 = bx.t(childAt).a();
                        a3.a(300L);
                        iVar.a(a3);
                    }
                }
                this.u = iVar;
                this.u.a();
                this.v = false;
            }
        }
        if (!(this.m == null || this.l != null || this.m.getVisibility() == 8)) {
            paddingRight += a(this.m, paddingRight, paddingTop, paddingTop2, a);
        }
        if (this.l != null) {
            a(this.l, paddingRight, paddingTop, paddingTop2, a);
        }
        int paddingLeft = a ? getPaddingLeft() : (i4 - i2) - getPaddingRight();
        if (this.c != null) {
            a(this.c, paddingLeft, paddingTop, paddingTop2, !a);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4 = 1073741824;
        int i5 = 0;
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used with android:layout_width=\"match_parent\" (or fill_parent)");
        } else if (View.MeasureSpec.getMode(i3) == 0) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used with android:layout_height=\"wrap_content\"");
        } else {
            int size = View.MeasureSpec.getSize(i2);
            int size2 = this.g > 0 ? this.g : View.MeasureSpec.getSize(i3);
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
            int i6 = size2 - paddingTop;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i6, Integer.MIN_VALUE);
            if (this.k != null) {
                int a = a(this.k, paddingLeft, makeMeasureSpec);
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.k.getLayoutParams();
                paddingLeft = a - (marginLayoutParams.rightMargin + marginLayoutParams.leftMargin);
            }
            if (this.c != null && this.c.getParent() == this) {
                paddingLeft = a(this.c, paddingLeft, makeMeasureSpec);
            }
            if (this.m != null && this.l == null) {
                if (this.s) {
                    this.m.measure(View.MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                    int measuredWidth = this.m.getMeasuredWidth();
                    boolean z = measuredWidth <= paddingLeft;
                    if (z) {
                        paddingLeft -= measuredWidth;
                    }
                    this.m.setVisibility(z ? 0 : 8);
                } else {
                    paddingLeft = a(this.m, paddingLeft, makeMeasureSpec);
                }
            }
            if (this.l != null) {
                ViewGroup.LayoutParams layoutParams = this.l.getLayoutParams();
                int i7 = layoutParams.width != -2 ? 1073741824 : Integer.MIN_VALUE;
                if (layoutParams.width >= 0) {
                    paddingLeft = Math.min(layoutParams.width, paddingLeft);
                }
                if (layoutParams.height == -2) {
                    i4 = Integer.MIN_VALUE;
                }
                this.l.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, i7), View.MeasureSpec.makeMeasureSpec(layoutParams.height >= 0 ? Math.min(layoutParams.height, i6) : i6, i4));
            }
            if (this.g <= 0) {
                int childCount = getChildCount();
                int i8 = 0;
                while (i5 < childCount) {
                    int measuredHeight = getChildAt(i5).getMeasuredHeight() + paddingTop;
                    if (measuredHeight <= i8) {
                        measuredHeight = i8;
                    }
                    i5++;
                    i8 = measuredHeight;
                }
                setMeasuredDimension(size, i8);
                return;
            }
            setMeasuredDimension(size, size2);
        }
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }
}
