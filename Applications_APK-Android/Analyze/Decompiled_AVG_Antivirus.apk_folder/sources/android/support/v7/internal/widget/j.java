package android.support.v7.internal.widget;

import android.support.v4.view.bx;
import android.support.v4.view.dv;

final class j implements Runnable {
    final /* synthetic */ ActionBarOverlayLayout a;

    j(ActionBarOverlayLayout actionBarOverlayLayout) {
        this.a = actionBarOverlayLayout;
    }

    public final void run() {
        this.a.k();
        dv unused = this.a.y = bx.t(this.a.f).c(0.0f).a(this.a.A);
        if (this.a.e != null && this.a.e.getVisibility() != 8) {
            dv unused2 = this.a.z = bx.t(this.a.e).c(0.0f).a(this.a.B);
        }
    }
}
