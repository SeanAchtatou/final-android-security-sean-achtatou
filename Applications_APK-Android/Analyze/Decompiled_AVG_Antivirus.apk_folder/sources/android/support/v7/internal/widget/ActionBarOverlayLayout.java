package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.bg;
import android.support.v4.view.bh;
import android.support.v4.view.bx;
import android.support.v4.view.dv;
import android.support.v4.view.el;
import android.support.v4.widget.at;
import android.support.v7.a.b;
import android.support.v7.a.g;
import android.support.v7.internal.view.menu.y;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

public class ActionBarOverlayLayout extends ViewGroup implements bg, ae {
    static final int[] a = {b.actionBarSize, 16842841};
    /* access modifiers changed from: private */
    public final el A;
    /* access modifiers changed from: private */
    public final el B;
    private final Runnable C;
    private final Runnable D;
    private final bh E;
    private int b;
    private int c;
    private ContentFrameLayout d;
    /* access modifiers changed from: private */
    public ActionBarContainer e;
    /* access modifiers changed from: private */
    public ActionBarContainer f;
    private af g;
    private Drawable h;
    private boolean i;
    private boolean j;
    private boolean k;
    private boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    private int n;
    private int o;
    private final Rect p;
    private final Rect q;
    private final Rect r;
    private final Rect s;
    private final Rect t;
    private final Rect u;
    private l v;
    private final int w;
    private at x;
    /* access modifiers changed from: private */
    public dv y;
    /* access modifiers changed from: private */
    public dv z;

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public ActionBarOverlayLayout(Context context) {
        this(context, null);
    }

    public ActionBarOverlayLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = 0;
        this.p = new Rect();
        this.q = new Rect();
        this.r = new Rect();
        this.s = new Rect();
        this.t = new Rect();
        this.u = new Rect();
        this.w = 600;
        this.A = new h(this);
        this.B = new i(this);
        this.C = new j(this);
        this.D = new k(this);
        a(context);
        this.E = new bh(this);
    }

    private void a(Context context) {
        boolean z2 = true;
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(a);
        this.b = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        this.h = obtainStyledAttributes.getDrawable(1);
        setWillNotDraw(this.h == null);
        obtainStyledAttributes.recycle();
        if (context.getApplicationInfo().targetSdkVersion >= 19) {
            z2 = false;
        }
        this.i = z2;
        this.x = at.a(context);
    }

    private static boolean a(View view, Rect rect, boolean z2, boolean z3) {
        boolean z4 = false;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (layoutParams.leftMargin != rect.left) {
            layoutParams.leftMargin = rect.left;
            z4 = true;
        }
        if (z2 && layoutParams.topMargin != rect.top) {
            layoutParams.topMargin = rect.top;
            z4 = true;
        }
        if (layoutParams.rightMargin != rect.right) {
            layoutParams.rightMargin = rect.right;
            z4 = true;
        }
        if (!z3 || layoutParams.bottomMargin == rect.bottom) {
            return z4;
        }
        layoutParams.bottomMargin = rect.bottom;
        return true;
    }

    public static void b() {
    }

    private void b(int i2) {
        k();
        int height = this.f.getHeight();
        int max = Math.max(0, Math.min(i2, height));
        bx.b(this.f, (float) (-max));
        if (this.e != null && this.e.getVisibility() != 8) {
            bx.b(this.e, (float) ((int) ((((float) max) / ((float) height)) * ((float) this.e.getHeight()))));
        }
    }

    private void j() {
        af o2;
        if (this.d == null) {
            this.d = (ContentFrameLayout) findViewById(g.action_bar_activity_content);
            this.f = (ActionBarContainer) findViewById(g.action_bar_container);
            View findViewById = findViewById(g.action_bar);
            if (findViewById instanceof af) {
                o2 = (af) findViewById;
            } else if (findViewById instanceof Toolbar) {
                o2 = ((Toolbar) findViewById).o();
            } else {
                throw new IllegalStateException("Can't make a decor toolbar out of " + findViewById.getClass().getSimpleName());
            }
            this.g = o2;
            this.e = (ActionBarContainer) findViewById(g.split_action_bar);
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        removeCallbacks(this.C);
        removeCallbacks(this.D);
        if (this.y != null) {
            this.y.b();
        }
        if (this.z != null) {
            this.z.b();
        }
    }

    public final void a(int i2) {
        boolean z2 = true;
        j();
        switch (i2) {
            case 2:
                this.g.e();
                return;
            case 5:
                this.g.f();
                return;
            case 9:
                this.j = true;
                if (getContext().getApplicationInfo().targetSdkVersion >= 19) {
                    z2 = false;
                }
                this.i = z2;
                return;
            default:
                return;
        }
    }

    public final void a(l lVar) {
        this.v = lVar;
        if (getWindowToken() != null) {
            this.v.a(this.c);
            if (this.o != 0) {
                onWindowSystemUiVisibilityChanged(this.o);
                bx.x(this);
            }
        }
    }

    public final void a(Menu menu, y yVar) {
        j();
        this.g.a(menu, yVar);
    }

    public final void a(Window.Callback callback) {
        j();
        this.g.a(callback);
    }

    public final void a(CharSequence charSequence) {
        j();
        this.g.a(charSequence);
    }

    public final void a(boolean z2) {
        this.k = z2;
    }

    public final boolean a() {
        return this.j;
    }

    public final void b(boolean z2) {
        if (z2 != this.l) {
            this.l = z2;
            if (!z2) {
                k();
                b(0);
            }
        }
    }

    public final boolean c() {
        j();
        return this.g.g();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public final boolean d() {
        j();
        return this.g.h();
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.h != null && !this.i) {
            int bottom = this.f.getVisibility() == 0 ? (int) (((float) this.f.getBottom()) + bx.q(this.f) + 0.5f) : 0;
            this.h.setBounds(0, bottom, getWidth(), this.h.getIntrinsicHeight() + bottom);
            this.h.draw(canvas);
        }
    }

    public final boolean e() {
        j();
        return this.g.i();
    }

    public final boolean f() {
        j();
        return this.g.j();
    }

    /* access modifiers changed from: protected */
    public boolean fitSystemWindows(Rect rect) {
        j();
        bx.w(this);
        boolean a2 = a(this.f, rect, true, false);
        if (this.e != null) {
            a2 |= a(this.e, rect, false, true);
        }
        this.s.set(rect);
        bj.a(this, this.s, this.p);
        if (!this.q.equals(this.p)) {
            this.q.set(this.p);
            a2 = true;
        }
        if (a2) {
            requestLayout();
        }
        return true;
    }

    public final boolean g() {
        j();
        return this.g.k();
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    public /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public int getNestedScrollAxes() {
        return this.E.a();
    }

    public final void h() {
        j();
        this.g.l();
    }

    public final void i() {
        j();
        this.g.m();
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        a(getContext());
        bx.x(this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        k();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int paddingLeft = getPaddingLeft();
        getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = (i5 - i3) - getPaddingBottom();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                int i7 = layoutParams.leftMargin + paddingLeft;
                int i8 = childAt == this.e ? (paddingBottom - measuredHeight) - layoutParams.bottomMargin : layoutParams.topMargin + paddingTop;
                childAt.layout(i7, i8, measuredWidth + i7, measuredHeight + i8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int measuredHeight;
        j();
        measureChildWithMargins(this.f, i2, 0, i3, 0);
        LayoutParams layoutParams = (LayoutParams) this.f.getLayoutParams();
        int max = Math.max(0, this.f.getMeasuredWidth() + layoutParams.leftMargin + layoutParams.rightMargin);
        int max2 = Math.max(0, layoutParams.bottomMargin + this.f.getMeasuredHeight() + layoutParams.topMargin);
        int a2 = bj.a(0, bx.m(this.f));
        if (this.e != null) {
            measureChildWithMargins(this.e, i2, 0, i3, 0);
            LayoutParams layoutParams2 = (LayoutParams) this.e.getLayoutParams();
            int max3 = Math.max(max, this.e.getMeasuredWidth() + layoutParams2.leftMargin + layoutParams2.rightMargin);
            int max4 = Math.max(max2, layoutParams2.bottomMargin + this.e.getMeasuredHeight() + layoutParams2.topMargin);
            a2 = bj.a(a2, bx.m(this.e));
            max = max3;
            max2 = max4;
        }
        boolean z2 = (bx.w(this) & 256) != 0;
        if (z2) {
            measuredHeight = this.b;
            if (this.k && this.f.a() != null) {
                measuredHeight += this.b;
            }
        } else {
            measuredHeight = this.f.getVisibility() != 8 ? this.f.getMeasuredHeight() : 0;
        }
        this.r.set(this.p);
        this.t.set(this.s);
        if (this.j || z2) {
            Rect rect = this.t;
            rect.top = measuredHeight + rect.top;
            this.t.bottom += 0;
        } else {
            Rect rect2 = this.r;
            rect2.top = measuredHeight + rect2.top;
            this.r.bottom += 0;
        }
        a(this.d, this.r, true, true);
        if (!this.u.equals(this.t)) {
            this.u.set(this.t);
            this.d.a(this.t);
        }
        measureChildWithMargins(this.d, i2, 0, i3, 0);
        LayoutParams layoutParams3 = (LayoutParams) this.d.getLayoutParams();
        int max5 = Math.max(max, this.d.getMeasuredWidth() + layoutParams3.leftMargin + layoutParams3.rightMargin);
        int max6 = Math.max(max2, layoutParams3.bottomMargin + this.d.getMeasuredHeight() + layoutParams3.topMargin);
        int a3 = bj.a(a2, bx.m(this.d));
        setMeasuredDimension(bx.a(Math.max(max5 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i2, a3), bx.a(Math.max(max6 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i3, a3 << 16));
    }

    public boolean onNestedFling(View view, float f2, float f3, boolean z2) {
        boolean z3 = false;
        if (!this.l || !z2) {
            return false;
        }
        this.x.a(0, 0, (int) f3, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        if (this.x.e() > this.f.getHeight()) {
            z3 = true;
        }
        if (z3) {
            k();
            this.D.run();
        } else {
            k();
            this.C.run();
        }
        this.m = true;
        return true;
    }

    public boolean onNestedPreFling(View view, float f2, float f3) {
        return false;
    }

    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
    }

    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        this.n += i3;
        b(this.n);
    }

    public void onNestedScrollAccepted(View view, View view2, int i2) {
        this.E.a(i2);
        this.n = this.f != null ? -((int) bx.q(this.f)) : 0;
        k();
        if (this.v != null) {
            this.v.g();
        }
    }

    public boolean onStartNestedScroll(View view, View view2, int i2) {
        if ((i2 & 2) == 0 || this.f.getVisibility() != 0) {
            return false;
        }
        return this.l;
    }

    public void onStopNestedScroll(View view) {
        if (this.l && !this.m) {
            if (this.n <= this.f.getHeight()) {
                k();
                postDelayed(this.C, 600);
                return;
            }
            k();
            postDelayed(this.D, 600);
        }
    }

    public void onWindowSystemUiVisibilityChanged(int i2) {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT >= 16) {
            super.onWindowSystemUiVisibilityChanged(i2);
        }
        j();
        int i3 = this.o ^ i2;
        this.o = i2;
        boolean z3 = (i2 & 4) == 0;
        boolean z4 = (i2 & 256) != 0;
        if (this.v != null) {
            l lVar = this.v;
            if (z4) {
                z2 = false;
            }
            lVar.e(z2);
            if (z3 || !z4) {
                this.v.e();
            } else {
                this.v.f();
            }
        }
        if ((i3 & 256) != 0 && this.v != null) {
            bx.x(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        this.c = i2;
        if (this.v != null) {
            this.v.a(i2);
        }
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }
}
