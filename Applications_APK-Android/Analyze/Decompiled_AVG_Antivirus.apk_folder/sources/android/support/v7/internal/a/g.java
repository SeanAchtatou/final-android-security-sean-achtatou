package android.support.v7.internal.a;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;

final class g implements y {
    final /* synthetic */ b a;

    private g(b bVar) {
        this.a = bVar;
    }

    /* synthetic */ g(b bVar, byte b) {
        this(bVar);
    }

    public final void a(i iVar, boolean z) {
        if (this.a.c != null) {
            this.a.c.onPanelClosed(0, iVar);
        }
    }

    public final boolean a(i iVar) {
        if (iVar != null || this.a.c == null) {
            return true;
        }
        this.a.c.onMenuOpened(0, iVar);
        return true;
    }
}
