package android.support.v7.internal.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.v7.a.b;
import android.support.v7.a.e;
import android.support.v7.a.i;
import android.support.v7.widget.ListPopupWindow;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import androidx.core.view.GravityCompat;

public class v implements x, View.OnKeyListener, ViewTreeObserver.OnGlobalLayoutListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {
    static final int a = i.m;
    boolean b;
    private final Context c;
    /* access modifiers changed from: private */
    public final LayoutInflater d;
    /* access modifiers changed from: private */
    public final i e;
    private final w f;
    /* access modifiers changed from: private */
    public final boolean g;
    private final int h;
    private final int i;
    private final int j;
    private View k;
    private ListPopupWindow l;
    private ViewTreeObserver m;
    private y n;
    private ViewGroup o;
    private boolean p;
    private int q;
    private int r;

    private v(Context context, i iVar, View view) {
        this(context, iVar, view, false, b.popupMenuStyle);
    }

    public v(Context context, i iVar, View view, boolean z, int i2) {
        this(context, iVar, view, z, i2, (byte) 0);
    }

    private v(Context context, i iVar, View view, boolean z, int i2, byte b2) {
        this.r = 0;
        this.c = context;
        this.d = LayoutInflater.from(context);
        this.e = iVar;
        this.f = new w(this, this.e);
        this.g = z;
        this.i = i2;
        this.j = 0;
        Resources resources = context.getResources();
        this.h = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(e.abc_config_prefDialogWidth));
        this.k = view;
        iVar.a(this, context);
    }

    public final void a(Context context, i iVar) {
    }

    public final void a(Parcelable parcelable) {
    }

    public final void a(i iVar, boolean z) {
        if (iVar == this.e) {
            h();
            if (this.n != null) {
                this.n.a(iVar, z);
            }
        }
    }

    public final void a(y yVar) {
        this.n = yVar;
    }

    public final void a(View view) {
        this.k = view;
    }

    public final void a(boolean z) {
        this.p = false;
        if (this.f != null) {
            this.f.notifyDataSetChanged();
        }
    }

    public final boolean a() {
        return false;
    }

    public final boolean a(ad adVar) {
        boolean z;
        if (adVar.hasVisibleItems()) {
            v vVar = new v(this.c, adVar, this.k);
            vVar.n = this.n;
            int size = adVar.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    z = false;
                    break;
                }
                MenuItem item = adVar.getItem(i2);
                if (item.isVisible() && item.getIcon() != null) {
                    z = true;
                    break;
                }
                i2++;
            }
            vVar.b = z;
            if (vVar.g()) {
                if (this.n == null) {
                    return true;
                }
                this.n.a(adVar);
                return true;
            }
        }
        return false;
    }

    public final boolean a(m mVar) {
        return false;
    }

    public final int b() {
        return 0;
    }

    public final void b(boolean z) {
        this.b = z;
    }

    public final boolean b(m mVar) {
        return false;
    }

    public final void c() {
        this.r = GravityCompat.END;
    }

    public final Parcelable d() {
        return null;
    }

    public final void e() {
        if (!g()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    public final ListPopupWindow f() {
        return this.l;
    }

    public final boolean g() {
        View view;
        int i2 = 0;
        this.l = new ListPopupWindow(this.c, null, this.i, this.j);
        this.l.a((PopupWindow.OnDismissListener) this);
        this.l.a((AdapterView.OnItemClickListener) this);
        this.l.a(this.f);
        this.l.e();
        View view2 = this.k;
        if (view2 == null) {
            return false;
        }
        boolean z = this.m == null;
        this.m = view2.getViewTreeObserver();
        if (z) {
            this.m.addOnGlobalLayoutListener(this);
        }
        this.l.a(view2);
        this.l.a(this.r);
        if (!this.p) {
            w wVar = this.f;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
            int count = wVar.getCount();
            int i3 = 0;
            int i4 = 0;
            View view3 = null;
            while (true) {
                if (i3 >= count) {
                    break;
                }
                int itemViewType = wVar.getItemViewType(i3);
                if (itemViewType != i4) {
                    i4 = itemViewType;
                    view = null;
                } else {
                    view = view3;
                }
                if (this.o == null) {
                    this.o = new FrameLayout(this.c);
                }
                view3 = wVar.getView(i3, view, this.o);
                view3.measure(makeMeasureSpec, makeMeasureSpec2);
                int measuredWidth = view3.getMeasuredWidth();
                if (measuredWidth >= this.h) {
                    i2 = this.h;
                    break;
                }
                if (measuredWidth <= i2) {
                    measuredWidth = i2;
                }
                i3++;
                i2 = measuredWidth;
            }
            this.q = i2;
            this.p = true;
        }
        this.l.b(this.q);
        this.l.g();
        this.l.c();
        this.l.j().setOnKeyListener(this);
        return true;
    }

    public final void h() {
        if (i()) {
            this.l.a();
        }
    }

    public final boolean i() {
        return this.l != null && this.l.b();
    }

    public void onDismiss() {
        this.l = null;
        this.e.close();
        if (this.m != null) {
            if (!this.m.isAlive()) {
                this.m = this.k.getViewTreeObserver();
            }
            this.m.removeGlobalOnLayoutListener(this);
            this.m = null;
        }
    }

    public void onGlobalLayout() {
        if (i()) {
            View view = this.k;
            if (view == null || !view.isShown()) {
                h();
            } else if (i()) {
                this.l.c();
            }
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        w wVar = this.f;
        wVar.b.b((MenuItem) wVar.getItem(i2));
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        h();
        return true;
    }
}
