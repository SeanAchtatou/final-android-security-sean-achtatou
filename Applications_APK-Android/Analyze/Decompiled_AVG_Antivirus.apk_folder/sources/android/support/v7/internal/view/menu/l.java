package android.support.v7.internal.view.menu;

import android.content.DialogInterface;
import android.support.v7.a.i;
import android.support.v7.app.k;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.core.view.PointerIconCompat;

public final class l implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, DialogInterface.OnKeyListener, y {
    g a;
    private i b;
    private k c;
    private y d;

    public l(i iVar) {
        this.b = iVar;
    }

    public final void a() {
        i iVar = this.b;
        android.support.v7.app.l lVar = new android.support.v7.app.l(iVar.e());
        this.a = new g(lVar.a(), i.k);
        this.a.a(this);
        this.b.a(this.a);
        lVar.a(this.a.c(), this);
        View view = iVar.c;
        if (view != null) {
            lVar.a(view);
        } else {
            lVar.a(iVar.b).a(iVar.a);
        }
        lVar.a(this);
        this.c = lVar.c();
        this.c.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.c.getWindow().getAttributes();
        attributes.type = PointerIconCompat.TYPE_HELP;
        attributes.flags |= 131072;
        this.c.show();
    }

    public final void a(i iVar, boolean z) {
        if ((z || iVar == this.b) && this.c != null) {
            this.c.dismiss();
        }
        if (this.d != null) {
            this.d.a(iVar, z);
        }
    }

    public final boolean a(i iVar) {
        if (this.d != null) {
            return this.d.a(iVar);
        }
        return false;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.b((MenuItem) ((m) this.a.c().getItem(i)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.g.a(android.support.v7.internal.view.menu.i, boolean):void
     arg types: [android.support.v7.internal.view.menu.i, int]
     candidates:
      android.support.v7.internal.view.menu.g.a(android.content.Context, android.support.v7.internal.view.menu.i):void
      android.support.v7.internal.view.menu.x.a(android.content.Context, android.support.v7.internal.view.menu.i):void
      android.support.v7.internal.view.menu.g.a(android.support.v7.internal.view.menu.i, boolean):void */
    public final void onDismiss(DialogInterface dialogInterface) {
        this.a.a(this.b, true);
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.c.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.c.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.b.a(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.b.performShortcut(i, keyEvent, 0);
    }
}
