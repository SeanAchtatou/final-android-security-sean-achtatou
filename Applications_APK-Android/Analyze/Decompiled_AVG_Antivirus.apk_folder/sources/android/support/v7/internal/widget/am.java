package android.support.v7.internal.widget;

import android.content.res.Configuration;
import android.os.Build;
import android.support.v7.a.b;
import android.support.v7.app.a;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.HorizontalScrollView;

public final class am extends HorizontalScrollView implements aa {
    private static final Interpolator i = new DecelerateInterpolator();
    Runnable a;
    int b;
    int c;
    /* access modifiers changed from: private */
    public LinearLayoutCompat d;
    private SpinnerCompat e;
    private boolean f;
    private int g;
    private int h;

    static /* synthetic */ ap a(am amVar, a aVar) {
        ap apVar = new ap(amVar, amVar.getContext(), aVar);
        apVar.setBackgroundDrawable(null);
        apVar.setLayoutParams(new AbsListView.LayoutParams(-1, amVar.g));
        return apVar;
    }

    private void a(int i2) {
        this.h = i2;
        int childCount = this.d.getChildCount();
        int i3 = 0;
        while (i3 < childCount) {
            View childAt = this.d.getChildAt(i3);
            boolean z = i3 == i2;
            childAt.setSelected(z);
            if (z) {
                View childAt2 = this.d.getChildAt(i2);
                if (this.a != null) {
                    removeCallbacks(this.a);
                }
                this.a = new an(this, childAt2);
                post(this.a);
            }
            i3++;
        }
        if (this.e != null && i2 >= 0) {
            this.e.a(i2);
        }
    }

    private boolean a() {
        return this.e != null && this.e.getParent() == this;
    }

    private boolean b() {
        if (a()) {
            removeView(this.e);
            addView(this.d, new ViewGroup.LayoutParams(-2, -1));
            a(this.e.u);
        }
        return false;
    }

    public final void a(boolean z) {
        this.f = z;
    }

    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.a != null) {
            post(this.a);
        }
    }

    /* access modifiers changed from: protected */
    public final void onConfigurationChanged(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        android.support.v7.internal.view.a a2 = android.support.v7.internal.view.a.a(getContext());
        this.g = a2.e();
        requestLayout();
        this.c = a2.g();
    }

    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.a != null) {
            removeCallbacks(this.a);
        }
    }

    public final void onMeasure(int i2, int i3) {
        boolean z = true;
        int mode = View.MeasureSpec.getMode(i2);
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.d.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.b = -1;
        } else {
            if (childCount > 2) {
                this.b = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.b = View.MeasureSpec.getSize(i2) / 2;
            }
            this.b = Math.min(this.b, this.c);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.g, 1073741824);
        if (z2 || !this.f) {
            z = false;
        }
        if (z) {
            this.d.measure(0, makeMeasureSpec);
            if (this.d.getMeasuredWidth() <= View.MeasureSpec.getSize(i2)) {
                b();
            } else if (!a()) {
                if (this.e == null) {
                    SpinnerCompat spinnerCompat = new SpinnerCompat(getContext(), b.actionDropDownStyle);
                    spinnerCompat.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
                    spinnerCompat.b(this);
                    this.e = spinnerCompat;
                }
                removeView(this.d);
                addView(this.e, new ViewGroup.LayoutParams(-2, -1));
                if (this.e.a == null) {
                    this.e.a(new ao(this, (byte) 0));
                }
                if (this.a != null) {
                    removeCallbacks(this.a);
                    this.a = null;
                }
                this.e.a(this.h);
            }
        } else {
            b();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            a(this.h);
        }
    }
}
