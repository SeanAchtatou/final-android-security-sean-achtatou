package android.support.v7.internal.a;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;

final class e implements y {
    final /* synthetic */ b a;
    private boolean b;

    private e(b bVar) {
        this.a = bVar;
    }

    /* synthetic */ e(b bVar, byte b2) {
        this(bVar);
    }

    public final void a(i iVar, boolean z) {
        if (!this.b) {
            this.b = true;
            this.a.a.m();
            if (this.a.c != null) {
                this.a.c.onPanelClosed(8, iVar);
            }
            this.b = false;
        }
    }

    public final boolean a(i iVar) {
        if (this.a.c == null) {
            return false;
        }
        this.a.c.onMenuOpened(8, iVar);
        return true;
    }
}
