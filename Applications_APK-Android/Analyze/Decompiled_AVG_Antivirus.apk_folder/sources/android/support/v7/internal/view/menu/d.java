package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v4.view.bx;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class d implements x {
    protected Context a;
    protected Context b;
    protected i c;
    protected LayoutInflater d;
    protected LayoutInflater e;
    protected z f;
    private y g;
    private int h;
    private int i;
    private int j;

    public d(Context context, int i2, int i3) {
        this.a = context;
        this.d = LayoutInflater.from(context);
        this.h = i2;
        this.i = i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public z a(ViewGroup viewGroup) {
        if (this.f == null) {
            this.f = (z) this.d.inflate(this.h, viewGroup, false);
            this.f.a(this.c);
            a(true);
        }
        return this.f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(m mVar, View view, ViewGroup viewGroup) {
        aa aaVar = view instanceof aa ? (aa) view : (aa) this.d.inflate(this.i, viewGroup, false);
        a(mVar, aaVar);
        return (View) aaVar;
    }

    public final void a(int i2) {
        this.j = i2;
    }

    public void a(Context context, i iVar) {
        this.b = context;
        this.e = LayoutInflater.from(this.b);
        this.c = iVar;
    }

    public void a(i iVar, boolean z) {
        if (this.g != null) {
            this.g.a(iVar, z);
        }
    }

    public abstract void a(m mVar, aa aaVar);

    public final void a(y yVar) {
        this.g = yVar;
    }

    public void a(boolean z) {
        int i2;
        int i3;
        ViewGroup viewGroup = (ViewGroup) this.f;
        if (viewGroup != null) {
            if (this.c != null) {
                this.c.l();
                ArrayList k = this.c.k();
                int size = k.size();
                int i4 = 0;
                i2 = 0;
                while (i4 < size) {
                    m mVar = (m) k.get(i4);
                    if (c(mVar)) {
                        View childAt = viewGroup.getChildAt(i2);
                        m a2 = childAt instanceof aa ? ((aa) childAt).a() : null;
                        View a3 = a(mVar, childAt, viewGroup);
                        if (mVar != a2) {
                            a3.setPressed(false);
                            bx.z(a3);
                        }
                        if (a3 != childAt) {
                            ViewGroup viewGroup2 = (ViewGroup) a3.getParent();
                            if (viewGroup2 != null) {
                                viewGroup2.removeView(a3);
                            }
                            ((ViewGroup) this.f).addView(a3, i2);
                        }
                        i3 = i2 + 1;
                    } else {
                        i3 = i2;
                    }
                    i4++;
                    i2 = i3;
                }
            } else {
                i2 = 0;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    public boolean a() {
        return false;
    }

    public boolean a(ad adVar) {
        if (this.g != null) {
            return this.g.a(adVar);
        }
        return false;
    }

    public final boolean a(m mVar) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    public final int b() {
        return this.j;
    }

    public final boolean b(m mVar) {
        return false;
    }

    public final y c() {
        return this.g;
    }

    public boolean c(m mVar) {
        return true;
    }
}
