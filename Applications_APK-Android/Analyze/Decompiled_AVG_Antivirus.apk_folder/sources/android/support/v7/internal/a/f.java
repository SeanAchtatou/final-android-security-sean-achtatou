package android.support.v7.internal.a;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.view.MenuItem;

final class f implements j {
    final /* synthetic */ b a;

    private f(b bVar) {
        this.a = bVar;
    }

    /* synthetic */ f(b bVar, byte b) {
        this(bVar);
    }

    public final void a(i iVar) {
        if (this.a.c == null) {
            return;
        }
        if (this.a.a.h()) {
            this.a.c.onPanelClosed(8, iVar);
        } else if (this.a.c.onPreparePanel(0, null, iVar)) {
            this.a.c.onMenuOpened(8, iVar);
        }
    }

    public final boolean a(i iVar, MenuItem menuItem) {
        return false;
    }
}
