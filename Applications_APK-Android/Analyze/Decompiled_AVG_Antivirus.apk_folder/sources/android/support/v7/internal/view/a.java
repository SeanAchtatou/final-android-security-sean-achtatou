package android.support.v7.internal.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.cp;
import android.support.v7.a.b;
import android.support.v7.a.c;
import android.support.v7.a.e;
import android.support.v7.a.h;
import android.support.v7.a.l;
import android.view.ViewConfiguration;

public final class a {
    private Context a;

    private a(Context context) {
        this.a = context;
    }

    public static a a(Context context) {
        return new a(context);
    }

    public final int a() {
        return this.a.getResources().getInteger(h.abc_max_action_buttons);
    }

    public final boolean b() {
        return Build.VERSION.SDK_INT >= 19 || !cp.b(ViewConfiguration.get(this.a));
    }

    public final int c() {
        return this.a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    public final boolean d() {
        return this.a.getApplicationInfo().targetSdkVersion >= 16 ? this.a.getResources().getBoolean(c.abc_action_bar_embed_tabs) : this.a.getResources().getBoolean(c.abc_action_bar_embed_tabs_pre_jb);
    }

    public final int e() {
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(null, l.a, b.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(l.l, 0);
        Resources resources = this.a.getResources();
        if (!d()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(e.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    public final boolean f() {
        return this.a.getApplicationInfo().targetSdkVersion < 14;
    }

    public final int g() {
        return this.a.getResources().getDimensionPixelSize(e.abc_action_bar_stacked_tab_max_width);
    }
}
