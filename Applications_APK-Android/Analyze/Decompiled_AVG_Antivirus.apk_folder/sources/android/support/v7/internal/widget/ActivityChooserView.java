package android.support.v7.internal.widget;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.support.v4.view.n;
import android.support.v7.a.e;
import android.support.v7.a.g;
import android.support.v7.a.i;
import android.support.v7.a.j;
import android.support.v7.a.l;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.ListPopupWindow;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;

public class ActivityChooserView extends ViewGroup {
    n a;
    /* access modifiers changed from: private */
    public final w b;
    private final x c;
    private final LinearLayoutCompat d;
    private final Drawable e;
    /* access modifiers changed from: private */
    public final FrameLayout f;
    private final ImageView g;
    /* access modifiers changed from: private */
    public final FrameLayout h;
    private final ImageView i;
    private final int j;
    private final DataSetObserver k;
    private final ViewTreeObserver.OnGlobalLayoutListener l;
    private ListPopupWindow m;
    /* access modifiers changed from: private */
    public PopupWindow.OnDismissListener n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public int p;
    private boolean q;
    private int r;

    public class InnerLayout extends LinearLayoutCompat {
        private static final int[] a = {16842964};

        public InnerLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            be a2 = be.a(context, attributeSet, a);
            setBackgroundDrawable(a2.a(0));
            a2.b();
        }
    }

    public ActivityChooserView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.internal.widget.ActivityChooserView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ActivityChooserView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.k = new s(this);
        this.l = new t(this);
        this.p = 4;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.F, i2, 0);
        this.p = obtainStyledAttributes.getInt(l.H, 4);
        Drawable drawable = obtainStyledAttributes.getDrawable(l.G);
        obtainStyledAttributes.recycle();
        LayoutInflater.from(getContext()).inflate(i.e, (ViewGroup) this, true);
        this.c = new x(this, (byte) 0);
        this.d = (LinearLayoutCompat) findViewById(g.activity_chooser_view_content);
        this.e = this.d.getBackground();
        this.h = (FrameLayout) findViewById(g.default_activity_button);
        this.h.setOnClickListener(this.c);
        this.h.setOnLongClickListener(this.c);
        this.i = (ImageView) this.h.findViewById(g.image);
        FrameLayout frameLayout = (FrameLayout) findViewById(g.expand_activities_button);
        frameLayout.setOnClickListener(this.c);
        frameLayout.setOnTouchListener(new u(this, frameLayout));
        this.f = frameLayout;
        this.g = (ImageView) frameLayout.findViewById(g.image);
        this.g.setImageDrawable(drawable);
        this.b = new w(this, (byte) 0);
        this.b.registerDataSetObserver(new v(this));
        Resources resources = context.getResources();
        this.j = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(e.abc_config_prefDialogWidth));
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.b.e() == null) {
            throw new IllegalStateException("No data model. Did you call #setDataModel?");
        }
        getViewTreeObserver().addOnGlobalLayoutListener(this.l);
        boolean z = this.h.getVisibility() == 0;
        int c2 = this.b.c();
        int i3 = z ? 1 : 0;
        if (i2 == Integer.MAX_VALUE || c2 <= i3 + i2) {
            this.b.a(false);
            this.b.a(i2);
        } else {
            this.b.a(true);
            this.b.a(i2 - 1);
        }
        ListPopupWindow d2 = d();
        if (!d2.b()) {
            if (this.o || !z) {
                this.b.a(true, z);
            } else {
                this.b.a(false, false);
            }
            d2.b(Math.min(this.b.a(), this.j));
            d2.c();
            if (this.a != null) {
                this.a.a(true);
            }
            d2.j().setContentDescription(getContext().getString(j.abc_activitychooserview_choose_application));
        }
    }

    static /* synthetic */ void c(ActivityChooserView activityChooserView) {
        if (activityChooserView.b.getCount() > 0) {
            activityChooserView.f.setEnabled(true);
        } else {
            activityChooserView.f.setEnabled(false);
        }
        int c2 = activityChooserView.b.c();
        int d2 = activityChooserView.b.d();
        if (c2 == 1 || (c2 > 1 && d2 > 0)) {
            activityChooserView.h.setVisibility(0);
            ResolveInfo b2 = activityChooserView.b.b();
            PackageManager packageManager = activityChooserView.getContext().getPackageManager();
            activityChooserView.i.setImageDrawable(b2.loadIcon(packageManager));
            if (activityChooserView.r != 0) {
                CharSequence loadLabel = b2.loadLabel(packageManager);
                activityChooserView.h.setContentDescription(activityChooserView.getContext().getString(activityChooserView.r, loadLabel));
            }
        } else {
            activityChooserView.h.setVisibility(8);
        }
        if (activityChooserView.h.getVisibility() == 0) {
            activityChooserView.d.setBackgroundDrawable(activityChooserView.e);
        } else {
            activityChooserView.d.setBackgroundDrawable(null);
        }
    }

    /* access modifiers changed from: private */
    public ListPopupWindow d() {
        if (this.m == null) {
            this.m = new ListPopupWindow(getContext());
            this.m.a(this.b);
            this.m.a(this);
            this.m.e();
            this.m.a((AdapterView.OnItemClickListener) this.c);
            this.m.a((PopupWindow.OnDismissListener) this.c);
        }
        return this.m;
    }

    public final boolean a() {
        if (d().b() || !this.q) {
            return false;
        }
        this.o = false;
        a(this.p);
        return true;
    }

    public final boolean b() {
        if (!d().b()) {
            return true;
        }
        d().a();
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            return true;
        }
        viewTreeObserver.removeGlobalOnLayoutListener(this.l);
        return true;
    }

    public final boolean c() {
        return d().b();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        m e2 = this.b.e();
        if (e2 != null) {
            e2.registerObserver(this.k);
        }
        this.q = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m e2 = this.b.e();
        if (e2 != null) {
            e2.unregisterObserver(this.k);
        }
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.removeGlobalOnLayoutListener(this.l);
        }
        if (d().b()) {
            b();
        }
        this.q = false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        this.d.layout(0, 0, i4 - i2, i5 - i3);
        if (!d().b()) {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        LinearLayoutCompat linearLayoutCompat = this.d;
        if (this.h.getVisibility() != 0) {
            i3 = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i3), 1073741824);
        }
        measureChild(linearLayoutCompat, i2, i3);
        setMeasuredDimension(linearLayoutCompat.getMeasuredWidth(), linearLayoutCompat.getMeasuredHeight());
    }
}
