package android.support.v7.internal.widget;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.PopupWindow;

final class x implements View.OnClickListener, View.OnLongClickListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {
    final /* synthetic */ ActivityChooserView a;

    private x(ActivityChooserView activityChooserView) {
        this.a = activityChooserView;
    }

    /* synthetic */ x(ActivityChooserView activityChooserView, byte b) {
        this(activityChooserView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.ActivityChooserView.a(android.support.v7.internal.widget.ActivityChooserView, boolean):boolean
     arg types: [android.support.v7.internal.widget.ActivityChooserView, int]
     candidates:
      android.support.v7.internal.widget.ActivityChooserView.a(android.support.v7.internal.widget.ActivityChooserView, int):void
      android.support.v7.internal.widget.ActivityChooserView.a(android.support.v7.internal.widget.ActivityChooserView, boolean):boolean */
    public final void onClick(View view) {
        if (view == this.a.h) {
            this.a.b();
            Intent b = this.a.b.e().b(this.a.b.e().a(this.a.b.b()));
            if (b != null) {
                b.addFlags(524288);
                this.a.getContext().startActivity(b);
            }
        } else if (view == this.a.f) {
            boolean unused = this.a.o = false;
            this.a.a(this.a.p);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public final void onDismiss() {
        if (this.a.n != null) {
            this.a.n.onDismiss();
        }
        if (this.a.a != null) {
            this.a.a.a(false);
        }
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        switch (((w) adapterView.getAdapter()).getItemViewType(i)) {
            case 0:
                this.a.b();
                if (!this.a.o) {
                    if (!this.a.b.f()) {
                        i++;
                    }
                    Intent b = this.a.b.e().b(i);
                    if (b != null) {
                        b.addFlags(524288);
                        this.a.getContext().startActivity(b);
                        return;
                    }
                    return;
                } else if (i > 0) {
                    this.a.b.e().c(i);
                    return;
                } else {
                    return;
                }
            case 1:
                this.a.a(Integer.MAX_VALUE);
                return;
            default:
                throw new IllegalArgumentException();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.ActivityChooserView.a(android.support.v7.internal.widget.ActivityChooserView, boolean):boolean
     arg types: [android.support.v7.internal.widget.ActivityChooserView, int]
     candidates:
      android.support.v7.internal.widget.ActivityChooserView.a(android.support.v7.internal.widget.ActivityChooserView, int):void
      android.support.v7.internal.widget.ActivityChooserView.a(android.support.v7.internal.widget.ActivityChooserView, boolean):boolean */
    public final boolean onLongClick(View view) {
        if (view == this.a.h) {
            if (this.a.b.getCount() > 0) {
                boolean unused = this.a.o = true;
                this.a.a(this.a.p);
            }
            return true;
        }
        throw new IllegalArgumentException();
    }
}
