package android.support.v7.internal.view.menu;

import android.view.MenuItem;

final class s extends f implements MenuItem.OnMenuItemClickListener {
    final /* synthetic */ o a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    s(o oVar, MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        super(onMenuItemClickListener);
        this.a = oVar;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        return ((MenuItem.OnMenuItemClickListener) this.b).onMenuItemClick(this.a.a(menuItem));
    }
}
