package android.support.v7.internal.widget;

import android.content.Context;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.view.menu.y;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;

public interface af {
    ViewGroup a();

    void a(int i);

    void a(y yVar, j jVar);

    void a(am amVar);

    void a(Menu menu, y yVar);

    void a(Window.Callback callback);

    void a(CharSequence charSequence);

    void a(boolean z);

    Context b();

    void b(int i);

    void b(CharSequence charSequence);

    boolean c();

    void d();

    void e();

    void f();

    boolean g();

    boolean h();

    boolean i();

    boolean j();

    boolean k();

    void l();

    void m();

    int n();

    int o();

    Menu p();
}
