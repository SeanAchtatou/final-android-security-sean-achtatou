package android.support.v7.internal.view.menu;

import android.content.Context;
import android.os.Parcelable;

public interface x {
    void a(Context context, i iVar);

    void a(Parcelable parcelable);

    void a(i iVar, boolean z);

    void a(boolean z);

    boolean a();

    boolean a(ad adVar);

    boolean a(m mVar);

    int b();

    boolean b(m mVar);

    Parcelable d();
}
