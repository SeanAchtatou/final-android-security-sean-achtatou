package android.support.v7.internal.a;

import android.support.v4.view.bx;
import android.support.v4.view.em;
import android.support.v7.internal.view.i;
import android.view.View;

final class j extends em {
    final /* synthetic */ i a;

    j(i iVar) {
        this.a = iVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, boolean):void
      android.support.v4.view.bx.b(android.view.View, int):boolean
      android.support.v4.view.bx.b(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, float):void
     arg types: [android.support.v7.internal.widget.ActionBarContainer, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, boolean):void
      android.support.v4.view.bx.b(android.view.View, int):boolean
      android.support.v4.view.bx.b(android.view.View, float):void */
    public final void b(View view) {
        if (this.a.C && this.a.s != null) {
            bx.b(this.a.s, 0.0f);
            bx.b((View) this.a.o, 0.0f);
        }
        if (this.a.r != null && this.a.z == 1) {
            this.a.r.setVisibility(8);
        }
        this.a.o.setVisibility(8);
        this.a.o.a(false);
        i unused = this.a.H = null;
        i iVar = this.a;
        if (iVar.c != null) {
            iVar.c.a(iVar.b);
            iVar.b = null;
            iVar.c = null;
        }
        if (this.a.n != null) {
            bx.x(this.a.n);
        }
    }
}
