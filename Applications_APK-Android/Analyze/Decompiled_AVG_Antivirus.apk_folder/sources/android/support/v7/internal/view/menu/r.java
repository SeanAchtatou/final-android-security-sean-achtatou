package android.support.v7.internal.view.menu;

import android.support.v4.view.ax;
import android.view.MenuItem;

final class r extends f implements ax {
    final /* synthetic */ o a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    r(o oVar, MenuItem.OnActionExpandListener onActionExpandListener) {
        super(onActionExpandListener);
        this.a = oVar;
    }

    public final boolean a(MenuItem menuItem) {
        return ((MenuItem.OnActionExpandListener) this.b).onMenuItemActionExpand(this.a.a(menuItem));
    }

    public final boolean b(MenuItem menuItem) {
        return ((MenuItem.OnActionExpandListener) this.b).onMenuItemActionCollapse(this.a.a(menuItem));
    }
}
