package android.support.v7.internal.widget;

import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.ap;
import android.view.View;

final class aq extends ap {
    final /* synthetic */ au a;
    final /* synthetic */ SpinnerCompat b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    aq(SpinnerCompat spinnerCompat, View view, au auVar) {
        super(view);
        this.b = spinnerCompat;
        this.a = auVar;
    }

    public final ListPopupWindow a() {
        return this.a;
    }

    public final boolean b() {
        if (this.b.F.b()) {
            return true;
        }
        this.b.F.c();
        return true;
    }
}
