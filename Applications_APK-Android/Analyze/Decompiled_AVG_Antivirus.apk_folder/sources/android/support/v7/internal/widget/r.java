package android.support.v7.internal.widget;

import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.xmlpull.v1.XmlSerializer;

final class r extends AsyncTask {
    final /* synthetic */ m a;

    private r(m mVar) {
        this.a = mVar;
    }

    /* synthetic */ r(m mVar, byte b) {
        this(mVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Void doInBackground(Object... objArr) {
        List list = (List) objArr[0];
        String str = (String) objArr[1];
        try {
            FileOutputStream openFileOutput = this.a.g.openFileOutput(str, 0);
            XmlSerializer newSerializer = Xml.newSerializer();
            try {
                newSerializer.setOutput(openFileOutput, null);
                newSerializer.startDocument("UTF-8", true);
                newSerializer.startTag(null, "historical-records");
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    p pVar = (p) list.remove(0);
                    newSerializer.startTag(null, "historical-record");
                    newSerializer.attribute(null, "activity", pVar.a.flattenToString());
                    newSerializer.attribute(null, "time", String.valueOf(pVar.b));
                    newSerializer.attribute(null, "weight", String.valueOf(pVar.c));
                    newSerializer.endTag(null, "historical-record");
                }
                newSerializer.endTag(null, "historical-records");
                newSerializer.endDocument();
                boolean unused = this.a.l = true;
                if (openFileOutput != null) {
                    try {
                        openFileOutput.close();
                    } catch (IOException e) {
                    }
                }
            } catch (IllegalArgumentException e2) {
                Log.e(m.a, "Error writing historical recrod file: " + this.a.h, e2);
                boolean unused2 = this.a.l = true;
                if (openFileOutput != null) {
                    try {
                        openFileOutput.close();
                    } catch (IOException e3) {
                    }
                }
            } catch (IllegalStateException e4) {
                Log.e(m.a, "Error writing historical recrod file: " + this.a.h, e4);
                boolean unused3 = this.a.l = true;
                if (openFileOutput != null) {
                    try {
                        openFileOutput.close();
                    } catch (IOException e5) {
                    }
                }
            } catch (IOException e6) {
                Log.e(m.a, "Error writing historical recrod file: " + this.a.h, e6);
                boolean unused4 = this.a.l = true;
                if (openFileOutput != null) {
                    try {
                        openFileOutput.close();
                    } catch (IOException e7) {
                    }
                }
            } catch (Throwable th) {
                boolean unused5 = this.a.l = true;
                if (openFileOutput != null) {
                    try {
                        openFileOutput.close();
                    } catch (IOException e8) {
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException e9) {
            Log.e(m.a, "Error writing historical recrod file: " + str, e9);
        }
        return null;
    }
}
