package android.support.v7.internal.widget;

import android.view.ViewTreeObserver;

final class t implements ViewTreeObserver.OnGlobalLayoutListener {
    final /* synthetic */ ActivityChooserView a;

    t(ActivityChooserView activityChooserView) {
        this.a = activityChooserView;
    }

    public final void onGlobalLayout() {
        if (!this.a.c()) {
            return;
        }
        if (!this.a.isShown()) {
            this.a.d().a();
            return;
        }
        this.a.d().c();
        if (this.a.a != null) {
            this.a.a.a(true);
        }
    }
}
