package android.support.v7.internal.widget;

import android.database.DataSetObserver;

final class v extends DataSetObserver {
    final /* synthetic */ ActivityChooserView a;

    v(ActivityChooserView activityChooserView) {
        this.a = activityChooserView;
    }

    public final void onChanged() {
        super.onChanged();
        ActivityChooserView.c(this.a);
    }
}
