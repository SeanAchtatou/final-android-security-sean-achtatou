package android.support.v7.internal.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;

public final class az extends ContextWrapper {
    private Resources a;

    private az(Context context) {
        super(context);
    }

    public static Context a(Context context) {
        return !(context instanceof az) ? new az(context) : context;
    }

    public final Resources getResources() {
        if (this.a == null) {
            this.a = new ba(super.getResources(), bc.a(this));
        }
        return this.a;
    }
}
