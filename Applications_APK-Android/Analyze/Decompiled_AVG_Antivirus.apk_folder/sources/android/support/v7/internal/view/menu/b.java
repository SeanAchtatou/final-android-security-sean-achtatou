package android.support.v7.internal.view.menu;

import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.ap;

final class b extends ap {
    final /* synthetic */ ActionMenuItemView a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(ActionMenuItemView actionMenuItemView) {
        super(actionMenuItemView);
        this.a = actionMenuItemView;
    }

    public final ListPopupWindow a() {
        if (this.a.f != null) {
            return this.a.f.a();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        ListPopupWindow a2;
        return this.a.d != null && this.a.d.a(this.a.a) && (a2 = a()) != null && a2.b();
    }
}
