package android.support.v7.internal.widget;

import android.database.DataSetObserver;
import android.os.Parcelable;

final class z extends DataSetObserver {
    final /* synthetic */ y a;
    private Parcelable b = null;

    z(y yVar) {
        this.a = yVar;
    }

    public final void onChanged() {
        this.a.t = true;
        this.a.z = this.a.y;
        this.a.y = this.a.c().getCount();
        if (!this.a.c().hasStableIds() || this.b == null || this.a.z != 0 || this.a.y <= 0) {
            this.a.g();
        } else {
            this.a.onRestoreInstanceState(this.b);
            this.b = null;
        }
        this.a.d();
        this.a.requestLayout();
    }

    public final void onInvalidated() {
        this.a.t = true;
        if (this.a.c().hasStableIds()) {
            this.b = this.a.onSaveInstanceState();
        }
        this.a.z = this.a.y;
        this.a.y = 0;
        this.a.w = -1;
        this.a.x = Long.MIN_VALUE;
        this.a.u = -1;
        this.a.v = Long.MIN_VALUE;
        this.a.o = false;
        this.a.d();
        this.a.requestLayout();
    }
}
