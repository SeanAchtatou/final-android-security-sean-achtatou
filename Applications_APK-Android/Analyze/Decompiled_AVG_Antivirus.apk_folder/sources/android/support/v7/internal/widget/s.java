package android.support.v7.internal.widget;

import android.database.DataSetObserver;

final class s extends DataSetObserver {
    final /* synthetic */ ActivityChooserView a;

    s(ActivityChooserView activityChooserView) {
        this.a = activityChooserView;
    }

    public final void onChanged() {
        super.onChanged();
        this.a.b.notifyDataSetChanged();
    }

    public final void onInvalidated() {
        super.onInvalidated();
        this.a.b.notifyDataSetInvalidated();
    }
}
