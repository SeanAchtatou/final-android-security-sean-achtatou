package android.support.v7.internal.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.internal.widget.SpinnerCompat;

final class aw implements Parcelable.Creator {
    aw() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new SpinnerCompat.SavedState(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new SpinnerCompat.SavedState[i];
    }
}
