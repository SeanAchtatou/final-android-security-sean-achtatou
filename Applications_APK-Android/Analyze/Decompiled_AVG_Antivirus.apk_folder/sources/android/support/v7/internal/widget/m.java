package android.support.v7.internal.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class m extends DataSetObservable {
    /* access modifiers changed from: private */
    public static final String a = m.class.getSimpleName();
    private static final Object b = new Object();
    private static final Map c = new HashMap();
    private final Object d;
    private final List e;
    private final List f;
    /* access modifiers changed from: private */
    public final Context g;
    /* access modifiers changed from: private */
    public final String h;
    private Intent i;
    private o j;
    private int k;
    /* access modifiers changed from: private */
    public boolean l;
    private boolean m;
    private boolean n;
    private boolean o;
    private q p;

    private boolean a(p pVar) {
        boolean add = this.f.add(pVar);
        if (add) {
            this.n = true;
            g();
            if (!this.m) {
                throw new IllegalStateException("No preceding call to #readHistoricalData");
            }
            if (this.n) {
                this.n = false;
                if (!TextUtils.isEmpty(this.h)) {
                    r rVar = new r(this, (byte) 0);
                    Object[] objArr = {this.f, this.h};
                    if (Build.VERSION.SDK_INT >= 11) {
                        rVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, objArr);
                    } else {
                        rVar.execute(objArr);
                    }
                }
            }
            f();
            notifyChanged();
        }
        return add;
    }

    private void e() {
        boolean z;
        boolean z2 = true;
        if (!this.o || this.i == null) {
            z = false;
        } else {
            this.o = false;
            this.e.clear();
            List<ResolveInfo> queryIntentActivities = this.g.getPackageManager().queryIntentActivities(this.i, 0);
            int size = queryIntentActivities.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.e.add(new n(this, queryIntentActivities.get(i2)));
            }
            z = true;
        }
        if (!this.l || !this.n || TextUtils.isEmpty(this.h)) {
            z2 = false;
        } else {
            this.l = false;
            this.m = true;
            h();
        }
        boolean z3 = z | z2;
        g();
        if (z3) {
            f();
            notifyChanged();
        }
    }

    private boolean f() {
        if (this.j == null || this.i == null || this.e.isEmpty() || this.f.isEmpty()) {
            return false;
        }
        Collections.unmodifiableList(this.f);
        return true;
    }

    private void g() {
        int size = this.f.size() - this.k;
        if (size > 0) {
            this.n = true;
            for (int i2 = 0; i2 < size; i2++) {
                this.f.remove(0);
            }
        }
    }

    private void h() {
        try {
            FileInputStream openFileInput = this.g.openFileInput(this.h);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(openFileInput, null);
                int i2 = 0;
                while (i2 != 1 && i2 != 2) {
                    i2 = newPullParser.next();
                }
                if (!"historical-records".equals(newPullParser.getName())) {
                    throw new XmlPullParserException("Share records file does not start with historical-records tag.");
                }
                List list = this.f;
                list.clear();
                while (true) {
                    int next = newPullParser.next();
                    if (next != 1) {
                        if (!(next == 3 || next == 4)) {
                            if (!"historical-record".equals(newPullParser.getName())) {
                                throw new XmlPullParserException("Share records file not well-formed.");
                            }
                            list.add(new p(newPullParser.getAttributeValue(null, "activity"), Long.parseLong(newPullParser.getAttributeValue(null, "time")), Float.parseFloat(newPullParser.getAttributeValue(null, "weight"))));
                        }
                    } else if (openFileInput != null) {
                        try {
                            openFileInput.close();
                            return;
                        } catch (IOException e2) {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            } catch (XmlPullParserException e3) {
                Log.e(a, "Error reading historical recrod file: " + this.h, e3);
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e4) {
                    }
                }
            } catch (IOException e5) {
                Log.e(a, "Error reading historical recrod file: " + this.h, e5);
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e6) {
                    }
                }
            } catch (Throwable th) {
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e7) {
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException e8) {
        }
    }

    public final int a() {
        int size;
        synchronized (this.d) {
            e();
            size = this.e.size();
        }
        return size;
    }

    public final int a(ResolveInfo resolveInfo) {
        synchronized (this.d) {
            e();
            List list = this.e;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (((n) list.get(i2)).a == resolveInfo) {
                    return i2;
                }
            }
            return -1;
        }
    }

    public final ResolveInfo a(int i2) {
        ResolveInfo resolveInfo;
        synchronized (this.d) {
            e();
            resolveInfo = ((n) this.e.get(i2)).a;
        }
        return resolveInfo;
    }

    public final Intent b(int i2) {
        synchronized (this.d) {
            if (this.i == null) {
                return null;
            }
            e();
            n nVar = (n) this.e.get(i2);
            ComponentName componentName = new ComponentName(nVar.a.activityInfo.packageName, nVar.a.activityInfo.name);
            Intent intent = new Intent(this.i);
            intent.setComponent(componentName);
            if (this.p != null) {
                new Intent(intent);
                if (this.p.a()) {
                    return null;
                }
            }
            a(new p(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    public final ResolveInfo b() {
        synchronized (this.d) {
            e();
            if (this.e.isEmpty()) {
                return null;
            }
            ResolveInfo resolveInfo = ((n) this.e.get(0)).a;
            return resolveInfo;
        }
    }

    public final int c() {
        int size;
        synchronized (this.d) {
            e();
            size = this.f.size();
        }
        return size;
    }

    public final void c(int i2) {
        synchronized (this.d) {
            e();
            n nVar = (n) this.e.get(i2);
            n nVar2 = (n) this.e.get(0);
            a(new p(new ComponentName(nVar.a.activityInfo.packageName, nVar.a.activityInfo.name), System.currentTimeMillis(), nVar2 != null ? (nVar2.b - nVar.b) + 5.0f : 1.0f));
        }
    }
}
