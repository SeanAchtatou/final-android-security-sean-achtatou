package android.support.v7.internal.view.menu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.c.a.b;
import android.support.v4.view.ax;
import android.support.v4.view.n;
import android.support.v4.view.p;
import android.support.v7.internal.widget.bc;
import android.util.Log;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public final class m implements b {
    private static String w;
    private static String x;
    private static String y;
    private static String z;
    private final int a;
    private final int b;
    private final int c;
    private final int d;
    private CharSequence e;
    private CharSequence f;
    private Intent g;
    private char h;
    private char i;
    private Drawable j;
    private int k = 0;
    /* access modifiers changed from: private */
    public i l;
    private ad m;
    private Runnable n;
    private MenuItem.OnMenuItemClickListener o;
    private int p = 16;
    private int q = 0;
    private View r;
    private n s;
    private ax t;
    private boolean u = false;
    private ContextMenu.ContextMenuInfo v;

    m(i iVar, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        this.l = iVar;
        this.a = i3;
        this.b = i2;
        this.c = i4;
        this.d = i5;
        this.e = charSequence;
        this.q = i6;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public b setActionView(View view) {
        this.r = view;
        this.s = null;
        if (view != null && view.getId() == -1 && this.a > 0) {
            view.setId(this.a);
        }
        this.l.j();
        return this;
    }

    public final b a(ax axVar) {
        this.t = axVar;
        return this;
    }

    public final b a(n nVar) {
        if (this.s != null) {
            this.s.a((p) null);
        }
        this.r = null;
        this.s = nVar;
        this.l.b(true);
        if (this.s != null) {
            this.s.a(new n(this));
        }
        return this;
    }

    public final n a() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public final CharSequence a(aa aaVar) {
        return (aaVar == null || !aaVar.b()) ? getTitle() : getTitleCondensed();
    }

    /* access modifiers changed from: package-private */
    public final void a(ad adVar) {
        this.m = adVar;
        adVar.setHeaderTitle(getTitle());
    }

    /* access modifiers changed from: package-private */
    public final void a(ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.v = contextMenuInfo;
    }

    public final void a(boolean z2) {
        this.p = (z2 ? 4 : 0) | (this.p & -5);
    }

    /* access modifiers changed from: package-private */
    public final void b(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 2 : 0) | (this.p & -3);
        if (i2 != this.p) {
            this.l.b(false);
        }
    }

    public final boolean b() {
        if ((this.o != null && this.o.onMenuItemClick(this)) || this.l.a(this.l.o(), this)) {
            return true;
        }
        if (this.n != null) {
            this.n.run();
            return true;
        }
        if (this.g != null) {
            try {
                this.l.e().startActivity(this.g);
                return true;
            } catch (ActivityNotFoundException e2) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e2);
            }
        }
        return this.s != null && this.s.d();
    }

    public final int c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final boolean c(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 0 : 8) | (this.p & -9);
        return i2 != this.p;
    }

    public final boolean collapseActionView() {
        if ((this.q & 8) == 0) {
            return false;
        }
        if (this.r == null) {
            return true;
        }
        if (this.t == null || this.t.b(this)) {
            return this.l.b(this);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final char d() {
        return this.l.c() ? this.i : this.h;
    }

    public final void d(boolean z2) {
        if (z2) {
            this.p |= 32;
        } else {
            this.p &= -33;
        }
    }

    /* access modifiers changed from: package-private */
    public final String e() {
        char d2 = d();
        if (d2 == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(w);
        switch (d2) {
            case 8:
                sb.append(y);
                break;
            case 10:
                sb.append(x);
                break;
            case ' ':
                sb.append(z);
                break;
            default:
                sb.append(d2);
                break;
        }
        return sb.toString();
    }

    public final void e(boolean z2) {
        this.u = z2;
        this.l.b(false);
    }

    public final boolean expandActionView() {
        if (!m()) {
            return false;
        }
        if (this.t == null || this.t.a(this)) {
            return this.l.a(this);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return this.l.d() && d() != 0;
    }

    public final boolean g() {
        return (this.p & 4) != 0;
    }

    public final ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    public final View getActionView() {
        if (this.r != null) {
            return this.r;
        }
        if (this.s == null) {
            return null;
        }
        this.r = this.s.a(this);
        return this.r;
    }

    public final char getAlphabeticShortcut() {
        return this.i;
    }

    public final int getGroupId() {
        return this.b;
    }

    public final Drawable getIcon() {
        if (this.j != null) {
            return this.j;
        }
        if (this.k == 0) {
            return null;
        }
        Drawable a2 = bc.a(this.l.e(), this.k);
        this.k = 0;
        this.j = a2;
        return a2;
    }

    public final Intent getIntent() {
        return this.g;
    }

    @ViewDebug.CapturedViewProperty
    public final int getItemId() {
        return this.a;
    }

    public final ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.v;
    }

    public final char getNumericShortcut() {
        return this.h;
    }

    public final int getOrder() {
        return this.c;
    }

    public final SubMenu getSubMenu() {
        return this.m;
    }

    @ViewDebug.CapturedViewProperty
    public final CharSequence getTitle() {
        return this.e;
    }

    public final CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f != null ? this.f : this.e;
        return (Build.VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) ? charSequence : charSequence.toString();
    }

    public final boolean h() {
        return this.l.p();
    }

    public final boolean hasSubMenu() {
        return this.m != null;
    }

    public final boolean i() {
        return (this.p & 32) == 32;
    }

    public final boolean isActionViewExpanded() {
        return this.u;
    }

    public final boolean isCheckable() {
        return (this.p & 1) == 1;
    }

    public final boolean isChecked() {
        return (this.p & 2) == 2;
    }

    public final boolean isEnabled() {
        return (this.p & 16) != 0;
    }

    public final boolean isVisible() {
        return (this.s == null || !this.s.b()) ? (this.p & 8) == 0 : (this.p & 8) == 0 && this.s.c();
    }

    public final boolean j() {
        return (this.q & 1) == 1;
    }

    public final boolean k() {
        return (this.q & 2) == 2;
    }

    public final boolean l() {
        return (this.q & 4) == 4;
    }

    public final boolean m() {
        if ((this.q & 8) == 0) {
            return false;
        }
        if (this.r == null && this.s != null) {
            this.r = this.s.a(this);
        }
        return this.r != null;
    }

    public final MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final /* synthetic */ MenuItem setActionView(int i2) {
        Context e2 = this.l.e();
        setActionView(LayoutInflater.from(e2).inflate(i2, (ViewGroup) new LinearLayout(e2), false));
        return this;
    }

    public final MenuItem setAlphabeticShortcut(char c2) {
        if (this.i != c2) {
            this.i = Character.toLowerCase(c2);
            this.l.b(false);
        }
        return this;
    }

    public final MenuItem setCheckable(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 1 : 0) | (this.p & -2);
        if (i2 != this.p) {
            this.l.b(false);
        }
        return this;
    }

    public final MenuItem setChecked(boolean z2) {
        if ((this.p & 4) != 0) {
            this.l.a((MenuItem) this);
        } else {
            b(z2);
        }
        return this;
    }

    public final MenuItem setEnabled(boolean z2) {
        if (z2) {
            this.p |= 16;
        } else {
            this.p &= -17;
        }
        this.l.b(false);
        return this;
    }

    public final MenuItem setIcon(int i2) {
        this.j = null;
        this.k = i2;
        this.l.b(false);
        return this;
    }

    public final MenuItem setIcon(Drawable drawable) {
        this.k = 0;
        this.j = drawable;
        this.l.b(false);
        return this;
    }

    public final MenuItem setIntent(Intent intent) {
        this.g = intent;
        return this;
    }

    public final MenuItem setNumericShortcut(char c2) {
        if (this.h != c2) {
            this.h = c2;
            this.l.b(false);
        }
        return this;
    }

    public final MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setOnActionExpandListener()");
    }

    public final MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.o = onMenuItemClickListener;
        return this;
    }

    public final MenuItem setShortcut(char c2, char c3) {
        this.h = c2;
        this.i = Character.toLowerCase(c3);
        this.l.b(false);
        return this;
    }

    public final void setShowAsAction(int i2) {
        switch (i2 & 3) {
            case 0:
            case 1:
            case 2:
                break;
            default:
                throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
        this.q = i2;
        this.l.j();
    }

    public final /* synthetic */ MenuItem setShowAsActionFlags(int i2) {
        setShowAsAction(i2);
        return this;
    }

    public final MenuItem setTitle(int i2) {
        return setTitle(this.l.e().getString(i2));
    }

    public final MenuItem setTitle(CharSequence charSequence) {
        this.e = charSequence;
        this.l.b(false);
        if (this.m != null) {
            this.m.setHeaderTitle(charSequence);
        }
        return this;
    }

    public final MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f = charSequence;
        this.l.b(false);
        return this;
    }

    public final MenuItem setVisible(boolean z2) {
        if (c(z2)) {
            this.l.i();
        }
        return this;
    }

    public final String toString() {
        return this.e.toString();
    }
}
