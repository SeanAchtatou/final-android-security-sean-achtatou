package android.support.v7.internal.a;

import android.support.v7.internal.view.k;
import android.view.Menu;
import android.view.View;
import android.view.Window;

final class h extends k {
    final /* synthetic */ b a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(b bVar, Window.Callback callback) {
        super(callback);
        this.a = bVar;
    }

    public final View onCreatePanelView(int i) {
        switch (i) {
            case 0:
                Menu p = this.a.a.p();
                if (onPreparePanel(i, null, p) && onMenuOpened(i, p)) {
                    return b.a(this.a, p);
                }
        }
        return super.onCreatePanelView(i);
    }

    public final boolean onPreparePanel(int i, View view, Menu menu) {
        boolean onPreparePanel = super.onPreparePanel(i, view, menu);
        if (onPreparePanel && !this.a.b) {
            this.a.a.l();
            boolean unused = this.a.b = true;
        }
        return onPreparePanel;
    }
}
