package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.a.b;
import android.support.v7.app.a;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

final class ap extends LinearLayoutCompat implements View.OnLongClickListener {
    final /* synthetic */ am a;
    private final int[] b = {16842964};
    private a c;
    private TextView d;
    private ImageView e;
    private View f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ap(am amVar, Context context, a aVar) {
        super(context, null, b.actionBarTabStyle);
        this.a = amVar;
        this.c = aVar;
        be a2 = be.a(context, null, this.b, b.actionBarTabStyle);
        if (a2.f(0)) {
            setBackgroundDrawable(a2.a(0));
        }
        a2.b();
        b(8388627);
        b();
    }

    private void b() {
        a aVar = this.c;
        View c2 = aVar.c();
        if (c2 != null) {
            ViewParent parent = c2.getParent();
            if (parent != this) {
                if (parent != null) {
                    ((ViewGroup) parent).removeView(c2);
                }
                addView(c2);
            }
            this.f = c2;
            if (this.d != null) {
                this.d.setVisibility(8);
            }
            if (this.e != null) {
                this.e.setVisibility(8);
                this.e.setImageDrawable(null);
                return;
            }
            return;
        }
        if (this.f != null) {
            removeView(this.f);
            this.f = null;
        }
        Drawable a2 = aVar.a();
        CharSequence b2 = aVar.b();
        if (a2 != null) {
            if (this.e == null) {
                ImageView imageView = new ImageView(getContext());
                LinearLayoutCompat.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(-2, -2);
                layoutParams.h = 16;
                imageView.setLayoutParams(layoutParams);
                addView(imageView, 0);
                this.e = imageView;
            }
            this.e.setImageDrawable(a2);
            this.e.setVisibility(0);
        } else if (this.e != null) {
            this.e.setVisibility(8);
            this.e.setImageDrawable(null);
        }
        boolean z = !TextUtils.isEmpty(b2);
        if (z) {
            if (this.d == null) {
                AppCompatTextView appCompatTextView = new AppCompatTextView(getContext(), null, b.actionBarTabTextStyle);
                appCompatTextView.setEllipsize(TextUtils.TruncateAt.END);
                LinearLayoutCompat.LayoutParams layoutParams2 = new LinearLayoutCompat.LayoutParams(-2, -2);
                layoutParams2.h = 16;
                appCompatTextView.setLayoutParams(layoutParams2);
                addView(appCompatTextView);
                this.d = appCompatTextView;
            }
            this.d.setText(b2);
            this.d.setVisibility(0);
        } else if (this.d != null) {
            this.d.setVisibility(8);
            this.d.setText((CharSequence) null);
        }
        if (this.e != null) {
            this.e.setContentDescription(aVar.d());
        }
        if (z || TextUtils.isEmpty(aVar.d())) {
            setOnLongClickListener(null);
            setLongClickable(false);
            return;
        }
        setOnLongClickListener(this);
    }

    public final a a() {
        return this.c;
    }

    public final void a(a aVar) {
        this.c = aVar;
        b();
    }

    public final void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(a.class.getName());
    }

    public final void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        if (Build.VERSION.SDK_INT >= 14) {
            accessibilityNodeInfo.setClassName(a.class.getName());
        }
    }

    public final boolean onLongClick(View view) {
        int[] iArr = new int[2];
        getLocationOnScreen(iArr);
        Context context = getContext();
        int width = getWidth();
        int height = getHeight();
        int i = context.getResources().getDisplayMetrics().widthPixels;
        Toast makeText = Toast.makeText(context, this.c.d(), 0);
        makeText.setGravity(49, (iArr[0] + (width / 2)) - (i / 2), height);
        makeText.show();
        return true;
    }

    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.a.b > 0 && getMeasuredWidth() > this.a.b) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(this.a.b, 1073741824), i2);
        }
    }

    public final void setSelected(boolean z) {
        boolean z2 = isSelected() != z;
        super.setSelected(z);
        if (z2 && z) {
            sendAccessibilityEvent(4);
        }
    }
}
