package android.support.v7.internal.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.internal.widget.AbsSpinnerCompat;

final class d implements Parcelable.Creator {
    d() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new AbsSpinnerCompat.SavedState(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new AbsSpinnerCompat.SavedState[i];
    }
}
