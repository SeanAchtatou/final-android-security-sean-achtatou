package android.support.v7.internal.a;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.a.l;
import android.support.v7.internal.view.b;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatMultiAutoCompleteTextView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import java.lang.reflect.Constructor;
import java.util.Map;

public final class a {
    static final Class[] a = {Context.class, AttributeSet.class};
    private static final Map b = new android.support.v4.e.a();
    private final Object[] c = new Object[2];

    /* JADX INFO: finally extract failed */
    private View a(Context context, String str, AttributeSet attributeSet) {
        if (str.equals("view")) {
            str = attributeSet.getAttributeValue(null, "class");
        }
        try {
            this.c[0] = context;
            this.c[1] = attributeSet;
            if (-1 == str.indexOf(46)) {
                View a2 = a(context, str, "android.widget.");
                this.c[0] = null;
                this.c[1] = null;
                return a2;
            }
            View a3 = a(context, str, (String) null);
            this.c[0] = null;
            this.c[1] = null;
            return a3;
        } catch (Exception e) {
            this.c[0] = null;
            this.c[1] = null;
            return null;
        } catch (Throwable th) {
            this.c[0] = null;
            this.c[1] = null;
            throw th;
        }
    }

    private View a(Context context, String str, String str2) {
        Constructor<? extends U> constructor = (Constructor) b.get(str);
        if (constructor == null) {
            try {
                constructor = context.getClassLoader().loadClass(str2 != null ? str2 + str : str).asSubclass(View.class).getConstructor(a);
                b.put(str, constructor);
            } catch (Exception e) {
                return null;
            }
        }
        constructor.setAccessible(true);
        return (View) constructor.newInstance(this.c);
    }

    public final View a(View view, String str, Context context, AttributeSet attributeSet, boolean z, boolean z2) {
        b context2 = (!z || view == null) ? context : view.getContext();
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet, l.cq, 0, 0);
        int resourceId = z2 ? obtainStyledAttributes.getResourceId(l.cv, 0) : 0;
        if (resourceId == 0 && (resourceId = obtainStyledAttributes.getResourceId(l.cw, 0)) != 0) {
            Log.i("AppCompatViewInflater", "app:theme is now deprecated. Please move to using android:theme instead.");
        }
        int i = resourceId;
        obtainStyledAttributes.recycle();
        if (i != 0 && (!(context2 instanceof b) || ((b) context2).a() != i)) {
            context2 = new b(context2, i);
        }
        char c2 = 65535;
        switch (str.hashCode()) {
            case -1946472170:
                if (str.equals("RatingBar")) {
                    c2 = 7;
                    break;
                }
                break;
            case -1455429095:
                if (str.equals("CheckedTextView")) {
                    c2 = 4;
                    break;
                }
                break;
            case -1346021293:
                if (str.equals("MultiAutoCompleteTextView")) {
                    c2 = 6;
                    break;
                }
                break;
            case -938935918:
                if (str.equals("TextView")) {
                    c2 = 9;
                    break;
                }
                break;
            case -339785223:
                if (str.equals("Spinner")) {
                    c2 = 1;
                    break;
                }
                break;
            case 776382189:
                if (str.equals("RadioButton")) {
                    c2 = 3;
                    break;
                }
                break;
            case 1413872058:
                if (str.equals("AutoCompleteTextView")) {
                    c2 = 5;
                    break;
                }
                break;
            case 1601505219:
                if (str.equals("CheckBox")) {
                    c2 = 2;
                    break;
                }
                break;
            case 1666676343:
                if (str.equals("EditText")) {
                    c2 = 0;
                    break;
                }
                break;
            case 2001146706:
                if (str.equals("Button")) {
                    c2 = 8;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                return new AppCompatEditText(context2, attributeSet);
            case 1:
                return new AppCompatSpinner(context2, attributeSet);
            case 2:
                return new AppCompatCheckBox(context2, attributeSet);
            case 3:
                return new AppCompatRadioButton(context2, attributeSet);
            case 4:
                return new AppCompatCheckedTextView(context2, attributeSet);
            case 5:
                return new AppCompatAutoCompleteTextView(context2, attributeSet);
            case 6:
                return new AppCompatMultiAutoCompleteTextView(context2, attributeSet);
            case 7:
                return new AppCompatRatingBar(context2, attributeSet);
            case 8:
                return new AppCompatButton(context2, attributeSet);
            case 9:
                return new AppCompatTextView(context2, attributeSet);
            default:
                if (context != context2) {
                    return a(context2, str, attributeSet);
                }
                return null;
        }
    }
}
