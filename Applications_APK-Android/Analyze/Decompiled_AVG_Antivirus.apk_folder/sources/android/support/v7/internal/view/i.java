package android.support.v7.internal.view;

import android.support.v4.view.dv;
import android.support.v4.view.el;
import android.support.v4.view.em;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

public final class i {
    /* access modifiers changed from: private */
    public final ArrayList a = new ArrayList();
    private long b = -1;
    private Interpolator c;
    /* access modifiers changed from: private */
    public el d;
    /* access modifiers changed from: private */
    public boolean e;
    private final em f = new j(this);

    public final i a(dv dvVar) {
        if (!this.e) {
            this.a.add(dvVar);
        }
        return this;
    }

    public final i a(el elVar) {
        if (!this.e) {
            this.d = elVar;
        }
        return this;
    }

    public final i a(Interpolator interpolator) {
        if (!this.e) {
            this.c = interpolator;
        }
        return this;
    }

    public final void a() {
        if (!this.e) {
            Iterator it = this.a.iterator();
            while (it.hasNext()) {
                dv dvVar = (dv) it.next();
                if (this.b >= 0) {
                    dvVar.a(this.b);
                }
                if (this.c != null) {
                    dvVar.a(this.c);
                }
                if (this.d != null) {
                    dvVar.a(this.f);
                }
                dvVar.c();
            }
            this.e = true;
        }
    }

    public final void b() {
        if (this.e) {
            Iterator it = this.a.iterator();
            while (it.hasNext()) {
                ((dv) it.next()).b();
            }
            this.e = false;
        }
    }

    public final i c() {
        if (!this.e) {
            this.b = 250;
        }
        return this;
    }
}
