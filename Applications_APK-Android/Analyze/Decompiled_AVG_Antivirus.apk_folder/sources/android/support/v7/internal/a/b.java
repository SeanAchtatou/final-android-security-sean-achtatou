package android.support.v7.internal.a;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v4.view.bx;
import android.support.v7.a.k;
import android.support.v7.app.ActionBar;
import android.support.v7.internal.view.menu.g;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.widget.af;
import android.support.v7.internal.widget.bf;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.dm;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import java.util.ArrayList;

public final class b extends ActionBar {
    /* access modifiers changed from: private */
    public af a;
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public Window.Callback c;
    private boolean d;
    private boolean e;
    private ArrayList f = new ArrayList();
    private g g;
    private final Runnable h = new c(this);
    private final dm i = new d(this);

    public b(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.a = new bf(toolbar, false);
        this.c = new h(this, callback);
        this.a.a(this.c);
        toolbar.a(this.i);
        this.a.a(charSequence);
    }

    static /* synthetic */ View a(b bVar, Menu menu) {
        if (bVar.g == null && (menu instanceof i)) {
            i iVar = (i) menu;
            Context b2 = bVar.a.b();
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = b2.getResources().newTheme();
            newTheme.setTo(b2.getTheme());
            newTheme.resolveAttribute(android.support.v7.a.b.actionBarPopupTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            }
            newTheme.resolveAttribute(android.support.v7.a.b.panelMenuListTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            } else {
                newTheme.applyStyle(k.Theme_AppCompat_CompactMenu, true);
            }
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(b2, 0);
            contextThemeWrapper.getTheme().setTo(newTheme);
            bVar.g = new g(contextThemeWrapper, android.support.v7.a.i.k);
            bVar.g.a(new g(bVar, (byte) 0));
            iVar.a(bVar.g);
        }
        if (menu == null || bVar.g == null) {
            return null;
        }
        if (bVar.g.c().getCount() > 0) {
            return (View) bVar.g.a(bVar.a.a());
        }
        return null;
    }

    private Menu g() {
        if (!this.d) {
            this.a.a(new e(this, (byte) 0), new f(this, (byte) 0));
            this.d = true;
        }
        return this.a.p();
    }

    public final int a() {
        return this.a.n();
    }

    public final void a(Configuration configuration) {
        super.a(configuration);
    }

    public final void a(CharSequence charSequence) {
        this.a.b(charSequence);
    }

    public final void a(boolean z) {
        this.a.a(((z ? 4 : 0) & 4) | (this.a.n() & -5));
    }

    public final boolean a(int i2, KeyEvent keyEvent) {
        Menu g2 = g();
        if (g2 != null) {
            return g2.performShortcut(i2, keyEvent, 0);
        }
        return false;
    }

    public final Context b() {
        return this.a.b();
    }

    public final void b(CharSequence charSequence) {
        this.a.a(charSequence);
    }

    public final void b(boolean z) {
    }

    public final void c(boolean z) {
    }

    public final boolean c() {
        this.a.a().removeCallbacks(this.h);
        bx.a(this.a.a(), this.h);
        return true;
    }

    public final void d(boolean z) {
        if (z != this.e) {
            this.e = z;
            int size = this.f.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f.get(i2);
            }
        }
    }

    public final boolean d() {
        if (!this.a.c()) {
            return false;
        }
        this.a.d();
        return true;
    }

    public final Window.Callback e() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        Menu g2 = g();
        i iVar = g2 instanceof i ? (i) g2 : null;
        if (iVar != null) {
            iVar.g();
        }
        try {
            g2.clear();
            if (!this.c.onCreatePanelMenu(0, g2) || !this.c.onPreparePanel(0, null, g2)) {
                g2.clear();
            }
        } finally {
            if (iVar != null) {
                iVar.h();
            }
        }
    }
}
