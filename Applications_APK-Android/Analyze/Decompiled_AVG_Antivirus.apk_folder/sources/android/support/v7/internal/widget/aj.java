package android.support.v7.internal.widget;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.b.a.a;

final class aj extends a {
    private boolean a = true;

    public aj(Drawable drawable) {
        super(drawable);
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.a = z;
    }

    public final void draw(Canvas canvas) {
        if (this.a) {
            super.draw(canvas);
        }
    }

    public final void setHotspot(float f, float f2) {
        if (this.a) {
            super.setHotspot(f, f2);
        }
    }

    public final void setHotspotBounds(int i, int i2, int i3, int i4) {
        if (this.a) {
            super.setHotspotBounds(i, i2, i3, i4);
        }
    }

    public final boolean setState(int[] iArr) {
        if (this.a) {
            return super.setState(iArr);
        }
        return false;
    }

    public final boolean setVisible(boolean z, boolean z2) {
        if (this.a) {
            return super.setVisible(z, z2);
        }
        return false;
    }
}
