package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v4.view.p;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;

final class u extends p implements ActionProvider.VisibilityListener {
    p c;
    final /* synthetic */ t d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u(t tVar, Context context, ActionProvider actionProvider) {
        super(tVar, context, actionProvider);
        this.d = tVar;
    }

    public final View a(MenuItem menuItem) {
        return this.a.onCreateActionView(menuItem);
    }

    public final void a(p pVar) {
        this.c = pVar;
        ActionProvider actionProvider = this.a;
        if (pVar == null) {
            this = null;
        }
        actionProvider.setVisibilityListener(this);
    }

    public final boolean b() {
        return this.a.overridesItemVisibility();
    }

    public final boolean c() {
        return this.a.isVisible();
    }

    public final void onActionProviderVisibilityChanged(boolean z) {
        if (this.c != null) {
            this.c.a();
        }
    }
}
