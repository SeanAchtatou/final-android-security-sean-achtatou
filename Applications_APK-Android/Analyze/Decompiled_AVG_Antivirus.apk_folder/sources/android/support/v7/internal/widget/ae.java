package android.support.v7.internal.widget;

import android.support.v7.internal.view.menu.y;
import android.view.Menu;
import android.view.Window;

public interface ae {
    void a(int i);

    void a(Menu menu, y yVar);

    void a(Window.Callback callback);

    void a(CharSequence charSequence);

    boolean c();

    boolean d();

    boolean e();

    boolean f();

    boolean g();

    void h();

    void i();
}
