package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v4.view.n;
import android.view.ActionProvider;
import android.view.SubMenu;
import android.view.View;

class p extends n {
    final ActionProvider a;
    final /* synthetic */ o b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(o oVar, Context context, ActionProvider actionProvider) {
        super(context);
        this.b = oVar;
        this.a = actionProvider;
    }

    public final View a() {
        return this.a.onCreateActionView();
    }

    public final void a(SubMenu subMenu) {
        this.a.onPrepareSubMenu(this.b.a(subMenu));
    }

    public final boolean d() {
        return this.a.onPerformDefaultAction();
    }

    public final boolean e() {
        return this.a.hasSubMenu();
    }
}
