package android.support.v7.internal.view.menu;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.c.a.b;
import android.support.v4.view.n;
import android.util.Log;
import android.view.ActionProvider;
import android.view.CollapsibleActionView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.lang.reflect.Method;

@TargetApi(14)
public class o extends e implements MenuItem {
    private Method c;

    o(Context context, b bVar) {
        super(context, bVar);
    }

    /* access modifiers changed from: package-private */
    public p a(ActionProvider actionProvider) {
        return new p(this, this.a, actionProvider);
    }

    public final void b() {
        try {
            if (this.c == null) {
                this.c = ((b) this.b).getClass().getDeclaredMethod("setExclusiveCheckable", Boolean.TYPE);
            }
            this.c.invoke(this.b, true);
        } catch (Exception e) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e);
        }
    }

    public boolean collapseActionView() {
        return ((b) this.b).collapseActionView();
    }

    public boolean expandActionView() {
        return ((b) this.b).expandActionView();
    }

    public ActionProvider getActionProvider() {
        n a = ((b) this.b).a();
        if (a instanceof p) {
            return ((p) a).a;
        }
        return null;
    }

    public View getActionView() {
        View actionView = ((b) this.b).getActionView();
        return actionView instanceof q ? (View) ((q) actionView).a : actionView;
    }

    public char getAlphabeticShortcut() {
        return ((b) this.b).getAlphabeticShortcut();
    }

    public int getGroupId() {
        return ((b) this.b).getGroupId();
    }

    public Drawable getIcon() {
        return ((b) this.b).getIcon();
    }

    public Intent getIntent() {
        return ((b) this.b).getIntent();
    }

    public int getItemId() {
        return ((b) this.b).getItemId();
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return ((b) this.b).getMenuInfo();
    }

    public char getNumericShortcut() {
        return ((b) this.b).getNumericShortcut();
    }

    public int getOrder() {
        return ((b) this.b).getOrder();
    }

    public SubMenu getSubMenu() {
        return a(((b) this.b).getSubMenu());
    }

    public CharSequence getTitle() {
        return ((b) this.b).getTitle();
    }

    public CharSequence getTitleCondensed() {
        return ((b) this.b).getTitleCondensed();
    }

    public boolean hasSubMenu() {
        return ((b) this.b).hasSubMenu();
    }

    public boolean isActionViewExpanded() {
        return ((b) this.b).isActionViewExpanded();
    }

    public boolean isCheckable() {
        return ((b) this.b).isCheckable();
    }

    public boolean isChecked() {
        return ((b) this.b).isChecked();
    }

    public boolean isEnabled() {
        return ((b) this.b).isEnabled();
    }

    public boolean isVisible() {
        return ((b) this.b).isVisible();
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        ((b) this.b).a(actionProvider != null ? a(actionProvider) : null);
        return this;
    }

    public MenuItem setActionView(int i) {
        ((b) this.b).setActionView(i);
        View actionView = ((b) this.b).getActionView();
        if (actionView instanceof CollapsibleActionView) {
            ((b) this.b).setActionView(new q(actionView));
        }
        return this;
    }

    public MenuItem setActionView(View view) {
        if (view instanceof CollapsibleActionView) {
            view = new q(view);
        }
        ((b) this.b).setActionView(view);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        ((b) this.b).setAlphabeticShortcut(c2);
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        ((b) this.b).setCheckable(z);
        return this;
    }

    public MenuItem setChecked(boolean z) {
        ((b) this.b).setChecked(z);
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        ((b) this.b).setEnabled(z);
        return this;
    }

    public MenuItem setIcon(int i) {
        ((b) this.b).setIcon(i);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        ((b) this.b).setIcon(drawable);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        ((b) this.b).setIntent(intent);
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        ((b) this.b).setNumericShortcut(c2);
        return this;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        ((b) this.b).a(onActionExpandListener != null ? new r(this, onActionExpandListener) : null);
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        ((b) this.b).setOnMenuItemClickListener(onMenuItemClickListener != null ? new s(this, onMenuItemClickListener) : null);
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        ((b) this.b).setShortcut(c2, c3);
        return this;
    }

    public void setShowAsAction(int i) {
        ((b) this.b).setShowAsAction(i);
    }

    public MenuItem setShowAsActionFlags(int i) {
        ((b) this.b).setShowAsActionFlags(i);
        return this;
    }

    public MenuItem setTitle(int i) {
        ((b) this.b).setTitle(i);
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        ((b) this.b).setTitle(charSequence);
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        ((b) this.b).setTitleCondensed(charSequence);
        return this;
    }

    public MenuItem setVisible(boolean z) {
        return ((b) this.b).setVisible(z);
    }
}
