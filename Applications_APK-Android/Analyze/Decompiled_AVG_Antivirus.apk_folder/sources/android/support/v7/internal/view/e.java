package android.support.v7.internal.view;

import android.content.Context;
import android.support.v4.c.a.a;
import android.support.v4.e.q;
import android.support.v7.d.b;
import android.support.v7.internal.view.menu.ab;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;

public final class e implements b {
    final ActionMode.Callback a;
    final Context b;
    final ArrayList c = new ArrayList();
    final q d = new q();

    public e(Context context, ActionMode.Callback callback) {
        this.b = context;
        this.a = callback;
    }

    private Menu a(Menu menu) {
        Menu menu2 = (Menu) this.d.get(menu);
        if (menu2 != null) {
            return menu2;
        }
        Menu a2 = ab.a(this.b, (a) menu);
        this.d.put(menu, a2);
        return a2;
    }

    public final void a(android.support.v7.d.a aVar) {
        this.a.onDestroyActionMode(b(aVar));
    }

    public final boolean a(android.support.v7.d.a aVar, Menu menu) {
        return this.a.onCreateActionMode(b(aVar), a(menu));
    }

    public final boolean a(android.support.v7.d.a aVar, MenuItem menuItem) {
        return this.a.onActionItemClicked(b(aVar), ab.a(this.b, (android.support.v4.c.a.b) menuItem));
    }

    public final ActionMode b(android.support.v7.d.a aVar) {
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            d dVar = (d) this.c.get(i);
            if (dVar != null && dVar.b == aVar) {
                return dVar;
            }
        }
        d dVar2 = new d(this.b, aVar);
        this.c.add(dVar2);
        return dVar2;
    }

    public final boolean b(android.support.v7.d.a aVar, Menu menu) {
        return this.a.onPrepareActionMode(b(aVar), a(menu));
    }
}
