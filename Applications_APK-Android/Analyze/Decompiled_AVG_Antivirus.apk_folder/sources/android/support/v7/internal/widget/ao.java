package android.support.v7.internal.widget;

import android.support.v7.app.a;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

final class ao extends BaseAdapter {
    final /* synthetic */ am a;

    private ao(am amVar) {
        this.a = amVar;
    }

    /* synthetic */ ao(am amVar, byte b) {
        this(amVar);
    }

    public final int getCount() {
        return this.a.d.getChildCount();
    }

    public final Object getItem(int i) {
        return ((ap) this.a.d.getChildAt(i)).a();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            return am.a(this.a, (a) getItem(i));
        }
        ((ap) view).a((a) getItem(i));
        return view;
    }
}
