package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.v4.content.a;
import android.support.v7.a.b;
import android.support.v7.a.f;
import android.util.SparseArray;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public final class bc {
    public static final boolean a = (Build.VERSION.SDK_INT < 21);
    private static final PorterDuff.Mode b = PorterDuff.Mode.SRC_IN;
    private static final WeakHashMap c = new WeakHashMap();
    private static final bd d = new bd();
    private static final int[] e = {f.abc_textfield_search_default_mtrl_alpha, f.abc_textfield_default_mtrl_alpha, f.abc_ab_share_pack_mtrl_alpha};
    private static final int[] f = {f.abc_ic_ab_back_mtrl_am_alpha, f.abc_ic_go_search_api_mtrl_alpha, f.abc_ic_search_api_mtrl_alpha, f.abc_ic_commit_search_api_mtrl_alpha, f.abc_ic_clear_mtrl_alpha, f.abc_ic_menu_share_mtrl_alpha, f.abc_ic_menu_copy_mtrl_am_alpha, f.abc_ic_menu_cut_mtrl_alpha, f.abc_ic_menu_selectall_mtrl_alpha, f.abc_ic_menu_paste_mtrl_am_alpha, f.abc_ic_menu_moreoverflow_mtrl_alpha, f.abc_ic_voice_search_api_mtrl_alpha};
    private static final int[] g = {f.abc_textfield_activated_mtrl_alpha, f.abc_textfield_search_activated_mtrl_alpha, f.abc_cab_background_top_mtrl_alpha, f.abc_text_cursor_mtrl_alpha};
    private static final int[] h = {f.abc_popup_background_mtrl_mult, f.abc_cab_background_internal_bg, f.abc_menu_hardkey_panel_mtrl_mult};
    private static final int[] i = {f.abc_edit_text_material, f.abc_tab_indicator_material, f.abc_textfield_search_material, f.abc_spinner_mtrl_am_alpha, f.abc_spinner_textfield_background_material, f.abc_ratingbar_full_material, f.abc_switch_track_mtrl_alpha, f.abc_switch_thumb_material, f.d, f.abc_btn_borderless_material};
    private static final int[] j = {f.abc_btn_check_material, f.e};
    private final WeakReference k;
    private SparseArray l;
    private ColorStateList m;

    private bc(Context context) {
        this.k = new WeakReference(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.bc.a(int, boolean):android.graphics.drawable.Drawable
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.bc.a(android.content.Context, int):android.graphics.drawable.Drawable
      android.support.v7.internal.widget.bc.a(android.view.View, android.support.v7.internal.widget.bb):void
      android.support.v7.internal.widget.bc.a(int[], int):boolean
      android.support.v7.internal.widget.bc.a(int, android.graphics.drawable.Drawable):boolean
      android.support.v7.internal.widget.bc.a(int, boolean):android.graphics.drawable.Drawable */
    public static Drawable a(Context context, int i2) {
        return a(f, i2) || a(e, i2) || a(g, i2) || a(i, i2) || a(h, i2) || a(j, i2) || i2 == f.abc_cab_background_top_material ? a(context).a(i2, false) : a.a(context, i2);
    }

    public static bc a(Context context) {
        bc bcVar = (bc) c.get(context);
        if (bcVar != null) {
            return bcVar;
        }
        bc bcVar2 = new bc(context);
        c.put(context, bcVar2);
        return bcVar2;
    }

    private static void a(Drawable drawable, int i2, PorterDuff.Mode mode) {
        if (mode == null) {
            mode = b;
        }
        PorterDuffColorFilter a2 = d.a(i2, mode);
        if (a2 == null) {
            a2 = new PorterDuffColorFilter(i2, mode);
            d.a(i2, mode, a2);
        }
        drawable.setColorFilter(a2);
    }

    public static void a(View view, bb bbVar) {
        Drawable background = view.getBackground();
        if (bbVar.d) {
            a(background, bbVar.a.getColorForState(view.getDrawableState(), bbVar.a.getDefaultColor()), bbVar.c ? bbVar.b : null);
        } else {
            background.clearColorFilter();
        }
        if (Build.VERSION.SDK_INT <= 10) {
            view.invalidate();
        }
    }

    private static boolean a(int[] iArr, int i2) {
        for (int i3 : iArr) {
            if (i3 == i2) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.bc.a(int, boolean):android.graphics.drawable.Drawable
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.bc.a(android.content.Context, int):android.graphics.drawable.Drawable
      android.support.v7.internal.widget.bc.a(android.view.View, android.support.v7.internal.widget.bb):void
      android.support.v7.internal.widget.bc.a(int[], int):boolean
      android.support.v7.internal.widget.bc.a(int, android.graphics.drawable.Drawable):boolean
      android.support.v7.internal.widget.bc.a(int, boolean):android.graphics.drawable.Drawable */
    public final Drawable a(int i2) {
        return a(i2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.bc.a(int, boolean):android.graphics.drawable.Drawable
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.bc.a(android.content.Context, int):android.graphics.drawable.Drawable
      android.support.v7.internal.widget.bc.a(android.view.View, android.support.v7.internal.widget.bb):void
      android.support.v7.internal.widget.bc.a(int[], int):boolean
      android.support.v7.internal.widget.bc.a(int, android.graphics.drawable.Drawable):boolean
      android.support.v7.internal.widget.bc.a(int, boolean):android.graphics.drawable.Drawable */
    public final Drawable a(int i2, boolean z) {
        PorterDuff.Mode mode = null;
        Context context = (Context) this.k.get();
        if (context == null) {
            return null;
        }
        Drawable a2 = a.a(context, i2);
        if (a2 != null) {
            if (Build.VERSION.SDK_INT >= 8) {
                a2 = a2.mutate();
            }
            ColorStateList b2 = b(i2);
            if (b2 != null) {
                a2 = android.support.v4.b.a.a.c(a2);
                android.support.v4.b.a.a.a(a2, b2);
                if (i2 == f.abc_switch_thumb_material) {
                    mode = PorterDuff.Mode.MULTIPLY;
                }
                if (mode != null) {
                    android.support.v4.b.a.a.a(a2, mode);
                }
            } else if (i2 == f.abc_cab_background_top_material) {
                return new LayerDrawable(new Drawable[]{a(f.abc_cab_background_internal_bg, false), a(f.abc_cab_background_top_mtrl_alpha, false)});
            } else if (!a(i2, a2) && z) {
                a2 = null;
            }
        }
        return a2;
    }

    public final boolean a(int i2, Drawable drawable) {
        int i3;
        int i4;
        PorterDuff.Mode mode;
        boolean z;
        Context context = (Context) this.k.get();
        if (context == null) {
            return false;
        }
        if (a(e, i2)) {
            i4 = b.colorControlNormal;
            mode = null;
            z = true;
            i3 = -1;
        } else if (a(g, i2)) {
            i4 = b.colorControlActivated;
            mode = null;
            z = true;
            i3 = -1;
        } else if (a(h, i2)) {
            z = true;
            mode = PorterDuff.Mode.MULTIPLY;
            i4 = 16842801;
            i3 = -1;
        } else if (i2 == f.abc_list_divider_mtrl_alpha) {
            i4 = 16842800;
            i3 = Math.round(40.8f);
            mode = null;
            z = true;
        } else {
            i3 = -1;
            i4 = 0;
            mode = null;
            z = false;
        }
        if (!z) {
            return false;
        }
        a(drawable, ay.a(context, i4), mode);
        if (i3 != -1) {
            drawable.setAlpha(i3);
        }
        return true;
    }

    public final ColorStateList b(int i2) {
        ColorStateList colorStateList;
        Context context = (Context) this.k.get();
        if (context == null) {
            return null;
        }
        ColorStateList colorStateList2 = this.l != null ? (ColorStateList) this.l.get(i2) : null;
        if (colorStateList2 != null) {
            return colorStateList2;
        }
        if (i2 == f.abc_edit_text_material) {
            colorStateList = new ColorStateList(new int[][]{ay.a, ay.g, ay.h}, new int[]{ay.c(context, b.colorControlNormal), ay.a(context, b.colorControlNormal), ay.a(context, b.colorControlActivated)});
        } else if (i2 == f.abc_switch_track_mtrl_alpha) {
            colorStateList = new ColorStateList(new int[][]{ay.a, ay.e, ay.h}, new int[]{ay.a(context, 16842800, 0.1f), ay.a(context, b.colorControlActivated, 0.3f), ay.a(context, 16842800, 0.3f)});
        } else if (i2 == f.abc_switch_thumb_material) {
            int[][] iArr = new int[3][];
            int[] iArr2 = new int[3];
            ColorStateList b2 = ay.b(context, b.colorSwitchThumbNormal);
            if (b2 == null || !b2.isStateful()) {
                iArr[0] = ay.a;
                iArr2[0] = ay.c(context, b.colorSwitchThumbNormal);
                iArr[1] = ay.e;
                iArr2[1] = ay.a(context, b.colorControlActivated);
                iArr[2] = ay.h;
                iArr2[2] = ay.a(context, b.colorSwitchThumbNormal);
            } else {
                iArr[0] = ay.a;
                iArr2[0] = b2.getColorForState(iArr[0], 0);
                iArr[1] = ay.e;
                iArr2[1] = ay.a(context, b.colorControlActivated);
                iArr[2] = ay.h;
                iArr2[2] = b2.getDefaultColor();
            }
            colorStateList = new ColorStateList(iArr, iArr2);
        } else if (i2 == f.d || i2 == f.abc_btn_borderless_material) {
            int a2 = ay.a(context, b.colorButtonNormal);
            int a3 = ay.a(context, b.u);
            colorStateList = new ColorStateList(new int[][]{ay.a, ay.d, ay.b, ay.h}, new int[]{ay.c(context, b.colorButtonNormal), android.support.v4.b.a.a(a3, a2), android.support.v4.b.a.a(a3, a2), a2});
        } else if (i2 == f.abc_spinner_mtrl_am_alpha || i2 == f.abc_spinner_textfield_background_material) {
            colorStateList = new ColorStateList(new int[][]{ay.a, ay.g, ay.h}, new int[]{ay.c(context, b.colorControlNormal), ay.a(context, b.colorControlNormal), ay.a(context, b.colorControlActivated)});
        } else if (a(f, i2)) {
            colorStateList = ay.b(context, b.colorControlNormal);
        } else if (a(i, i2)) {
            if (this.m == null) {
                int a4 = ay.a(context, b.colorControlNormal);
                int a5 = ay.a(context, b.colorControlActivated);
                this.m = new ColorStateList(new int[][]{ay.a, ay.b, ay.c, ay.d, ay.e, ay.f, ay.h}, new int[]{ay.c(context, b.colorControlNormal), a5, a5, a5, a5, a5, a4});
            }
            colorStateList = this.m;
        } else {
            colorStateList = a(j, i2) ? new ColorStateList(new int[][]{ay.a, ay.e, ay.h}, new int[]{ay.c(context, b.colorControlNormal), ay.a(context, b.colorControlActivated), ay.a(context, b.colorControlNormal)}) : colorStateList2;
        }
        if (colorStateList == null) {
            return colorStateList;
        }
        if (this.l == null) {
            this.l = new SparseArray();
        }
        this.l.append(i2, colorStateList);
        return colorStateList;
    }
}
