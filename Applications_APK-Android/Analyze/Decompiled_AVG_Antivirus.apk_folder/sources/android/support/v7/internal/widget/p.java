package android.support.v7.internal.widget;

import android.content.ComponentName;
import java.math.BigDecimal;

public final class p {
    public final ComponentName a;
    public final long b;
    public final float c;

    public p(ComponentName componentName, long j, float f) {
        this.a = componentName;
        this.b = j;
        this.c = f;
    }

    public p(String str, long j, float f) {
        this(ComponentName.unflattenFromString(str), j, f);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        p pVar = (p) obj;
        if (this.a == null) {
            if (pVar.a != null) {
                return false;
            }
        } else if (!this.a.equals(pVar.a)) {
            return false;
        }
        if (this.b != pVar.b) {
            return false;
        }
        return Float.floatToIntBits(this.c) == Float.floatToIntBits(pVar.c);
    }

    public final int hashCode() {
        return (((((this.a == null ? 0 : this.a.hashCode()) + 31) * 31) + ((int) (this.b ^ (this.b >>> 32)))) * 31) + Float.floatToIntBits(this.c);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append("; activity:").append(this.a);
        sb.append("; time:").append(this.b);
        sb.append("; weight:").append(new BigDecimal((double) this.c));
        sb.append("]");
        return sb.toString();
    }
}
