package android.support.v7.internal.widget;

import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.ap;
import android.view.View;

final class u extends ap {
    final /* synthetic */ ActivityChooserView a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    u(ActivityChooserView activityChooserView, View view) {
        super(view);
        this.a = activityChooserView;
    }

    public final ListPopupWindow a() {
        return this.a.d();
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        this.a.a();
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        this.a.b();
        return true;
    }
}
