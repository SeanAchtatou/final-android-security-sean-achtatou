package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.bx;
import android.support.v7.a.b;
import android.support.v7.a.f;
import android.support.v7.a.g;
import android.support.v7.a.j;
import android.support.v7.a.l;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;
import android.support.v7.widget.ActionMenuPresenter;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

public final class bf implements af {
    /* access modifiers changed from: private */
    public Toolbar a;
    private int b;
    private View c;
    private View d;
    private Drawable e;
    private Drawable f;
    private Drawable g;
    private boolean h;
    /* access modifiers changed from: private */
    public CharSequence i;
    private CharSequence j;
    private CharSequence k;
    /* access modifiers changed from: private */
    public Window.Callback l;
    /* access modifiers changed from: private */
    public boolean m;
    private ActionMenuPresenter n;
    private int o;
    private final bc p;
    private int q;
    private Drawable r;

    public bf(Toolbar toolbar, boolean z) {
        this(toolbar, z, j.a, f.abc_ic_ab_back_mtrl_am_alpha);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.Toolbar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private bf(Toolbar toolbar, boolean z, int i2, int i3) {
        this.o = 0;
        this.q = 0;
        this.a = toolbar;
        this.i = toolbar.i();
        this.j = toolbar.j();
        this.h = this.i != null;
        this.g = toolbar.l();
        if (z) {
            be a2 = be.a(toolbar.getContext(), null, l.a, b.actionBarStyle);
            CharSequence c2 = a2.c(l.t);
            if (!TextUtils.isEmpty(c2)) {
                b(c2);
            }
            CharSequence c3 = a2.c(l.r);
            if (!TextUtils.isEmpty(c3)) {
                this.j = c3;
                if ((this.b & 8) != 0) {
                    this.a.b(c3);
                }
            }
            Drawable a3 = a2.a(l.p);
            if (a3 != null) {
                this.f = a3;
                q();
            }
            Drawable a4 = a2.a(l.o);
            if (this.g == null && a4 != null) {
                this.e = a4;
                q();
            }
            Drawable a5 = a2.a(l.n);
            if (a5 != null) {
                this.g = a5;
                s();
            }
            a(a2.a(l.j, 0));
            int f2 = a2.f(l.i, 0);
            if (f2 != 0) {
                View inflate = LayoutInflater.from(this.a.getContext()).inflate(f2, (ViewGroup) this.a, false);
                if (!(this.d == null || (this.b & 16) == 0)) {
                    this.a.removeView(this.d);
                }
                this.d = inflate;
                if (!(inflate == null || (this.b & 16) == 0)) {
                    this.a.addView(this.d);
                }
                a(this.b | 16);
            }
            int e2 = a2.e(l.l, 0);
            if (e2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
                layoutParams.height = e2;
                this.a.setLayoutParams(layoutParams);
            }
            int c4 = a2.c(l.h, -1);
            int c5 = a2.c(l.g, -1);
            if (c4 >= 0 || c5 >= 0) {
                this.a.a(Math.max(c4, 0), Math.max(c5, 0));
            }
            int f3 = a2.f(l.u, 0);
            if (f3 != 0) {
                this.a.a(this.a.getContext(), f3);
            }
            int f4 = a2.f(l.s, 0);
            if (f4 != 0) {
                this.a.b(this.a.getContext(), f4);
            }
            int f5 = a2.f(l.q, 0);
            if (f5 != 0) {
                this.a.a(f5);
            }
            a2.b();
            this.p = a2.c();
        } else {
            this.b = this.a.l() != null ? 15 : 11;
            this.p = bc.a(toolbar.getContext());
        }
        if (i2 != this.q) {
            this.q = i2;
            if (TextUtils.isEmpty(this.a.k())) {
                int i4 = this.q;
                this.k = i4 == 0 ? null : this.a.getContext().getString(i4);
                r();
            }
        }
        this.k = this.a.k();
        Drawable a6 = this.p.a(i3);
        if (this.r != a6) {
            this.r = a6;
            s();
        }
        this.a.a(new bg(this));
    }

    private void c(CharSequence charSequence) {
        this.i = charSequence;
        if ((this.b & 8) != 0) {
            this.a.a(charSequence);
        }
    }

    private void q() {
        Drawable drawable = null;
        if ((this.b & 2) != 0) {
            drawable = (this.b & 1) != 0 ? this.f != null ? this.f : this.e : this.e;
        }
        this.a.a(drawable);
    }

    private void r() {
        if ((this.b & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.k)) {
            Toolbar toolbar = this.a;
            int i2 = this.q;
            toolbar.c(i2 != 0 ? toolbar.getContext().getText(i2) : null);
            return;
        }
        this.a.c(this.k);
    }

    private void s() {
        if ((this.b & 4) != 0) {
            this.a.b(this.g != null ? this.g : this.r);
        }
    }

    public final ViewGroup a() {
        return this.a;
    }

    public final void a(int i2) {
        int i3 = this.b ^ i2;
        this.b = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    s();
                    r();
                } else {
                    this.a.b((Drawable) null);
                }
            }
            if ((i3 & 3) != 0) {
                q();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.a.a(this.i);
                    this.a.b(this.j);
                } else {
                    this.a.a((CharSequence) null);
                    this.a.b((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && this.d != null) {
                if ((i2 & 16) != 0) {
                    this.a.addView(this.d);
                } else {
                    this.a.removeView(this.d);
                }
            }
        }
    }

    public final void a(y yVar, android.support.v7.internal.view.menu.j jVar) {
        this.a.a(yVar, jVar);
    }

    public final void a(am amVar) {
        if (this.c != null && this.c.getParent() == this.a) {
            this.a.removeView(this.c);
        }
        this.c = amVar;
        if (amVar != null && this.o == 2) {
            this.a.addView(this.c, 0);
            Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) this.c.getLayoutParams();
            layoutParams.width = -2;
            layoutParams.height = -2;
            layoutParams.a = 8388691;
            amVar.a(true);
        }
    }

    public final void a(Menu menu, y yVar) {
        if (this.n == null) {
            this.n = new ActionMenuPresenter(this.a.getContext());
            this.n.a(g.action_menu_presenter);
        }
        this.n.a(yVar);
        this.a.a((i) menu, this.n);
    }

    public final void a(Window.Callback callback) {
        this.l = callback;
    }

    public final void a(CharSequence charSequence) {
        if (!this.h) {
            c(charSequence);
        }
    }

    public final void a(boolean z) {
        this.a.a(z);
    }

    public final Context b() {
        return this.a.getContext();
    }

    public final void b(int i2) {
        if (i2 == 8) {
            bx.t(this.a).a(0.0f).a(new bh(this));
        } else if (i2 == 0) {
            bx.t(this.a).a(1.0f).a(new bi(this));
        }
    }

    public final void b(CharSequence charSequence) {
        this.h = true;
        c(charSequence);
    }

    public final boolean c() {
        return this.a.g();
    }

    public final void d() {
        this.a.h();
    }

    public final void e() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public final void f() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public final boolean g() {
        return this.a.a();
    }

    public final boolean h() {
        return this.a.b();
    }

    public final boolean i() {
        return this.a.c();
    }

    public final boolean j() {
        return this.a.d();
    }

    public final boolean k() {
        return this.a.e();
    }

    public final void l() {
        this.m = true;
    }

    public final void m() {
        this.a.f();
    }

    public final int n() {
        return this.b;
    }

    public final int o() {
        return this.o;
    }

    public final Menu p() {
        return this.a.m();
    }
}
