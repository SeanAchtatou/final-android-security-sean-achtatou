package android.support.v7.internal.widget;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v4.e.g;

final class bd extends g {
    private static int b(int i, PorterDuff.Mode mode) {
        return ((i + 31) * 31) + mode.hashCode();
    }

    /* access modifiers changed from: package-private */
    public final PorterDuffColorFilter a(int i, PorterDuff.Mode mode) {
        return (PorterDuffColorFilter) a(Integer.valueOf(b(i, mode)));
    }

    /* access modifiers changed from: package-private */
    public final PorterDuffColorFilter a(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
        return (PorterDuffColorFilter) a(Integer.valueOf(b(i, mode)), porterDuffColorFilter);
    }
}
