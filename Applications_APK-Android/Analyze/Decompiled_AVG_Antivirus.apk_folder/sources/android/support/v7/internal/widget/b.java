package android.support.v7.internal.widget;

import android.support.v4.view.dv;
import android.support.v4.view.el;
import android.view.View;

public final class b implements el {
    int a;
    final /* synthetic */ a b;
    private boolean c = false;

    protected b(a aVar) {
        this.b = aVar;
    }

    public final b a(dv dvVar, int i) {
        this.b.h = dvVar;
        this.a = i;
        return this;
    }

    public final void a(View view) {
        this.b.setVisibility(0);
        this.c = false;
    }

    public final void b(View view) {
        if (!this.c) {
            this.b.h = null;
            this.b.setVisibility(this.a);
            if (this.b.e != null && this.b.c != null) {
                this.b.c.setVisibility(this.a);
            }
        }
    }

    public final void c(View view) {
        this.c = true;
    }
}
