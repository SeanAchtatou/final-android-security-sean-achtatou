package android.support.v7.internal.widget;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

final class ba extends ak {
    private final bc a;

    public ba(Resources resources, bc bcVar) {
        super(resources);
        this.a = bcVar;
    }

    public final Drawable getDrawable(int i) {
        Drawable drawable = super.getDrawable(i);
        if (drawable != null) {
            this.a.a(i, drawable);
        }
        return drawable;
    }
}
