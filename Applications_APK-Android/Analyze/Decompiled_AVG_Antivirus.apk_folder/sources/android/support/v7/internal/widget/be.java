package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

public final class be {
    private final Context a;
    private final TypedArray b;
    private bc c;

    private be(Context context, TypedArray typedArray) {
        this.a = context;
        this.b = typedArray;
    }

    public static be a(Context context, AttributeSet attributeSet, int[] iArr) {
        return new be(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    public static be a(Context context, AttributeSet attributeSet, int[] iArr, int i) {
        return new be(context, context.obtainStyledAttributes(attributeSet, iArr, i, 0));
    }

    public final int a() {
        return this.b.length();
    }

    public final int a(int i, int i2) {
        return this.b.getInt(i, i2);
    }

    public final Drawable a(int i) {
        int resourceId;
        return (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0) ? this.b.getDrawable(i) : c().a(resourceId);
    }

    public final boolean a(int i, boolean z) {
        return this.b.getBoolean(i, z);
    }

    public final int b(int i, int i2) {
        return this.b.getInteger(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.bc.a(int, boolean):android.graphics.drawable.Drawable
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.bc.a(android.content.Context, int):android.graphics.drawable.Drawable
      android.support.v7.internal.widget.bc.a(android.view.View, android.support.v7.internal.widget.bb):void
      android.support.v7.internal.widget.bc.a(int[], int):boolean
      android.support.v7.internal.widget.bc.a(int, android.graphics.drawable.Drawable):boolean
      android.support.v7.internal.widget.bc.a(int, boolean):android.graphics.drawable.Drawable */
    public final Drawable b(int i) {
        int resourceId;
        if (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0) {
            return null;
        }
        return c().a(resourceId, true);
    }

    public final void b() {
        this.b.recycle();
    }

    public final int c(int i, int i2) {
        return this.b.getDimensionPixelOffset(i, i2);
    }

    public final bc c() {
        if (this.c == null) {
            this.c = bc.a(this.a);
        }
        return this.c;
    }

    public final CharSequence c(int i) {
        return this.b.getText(i);
    }

    public final int d(int i, int i2) {
        return this.b.getDimensionPixelSize(i, i2);
    }

    public final String d(int i) {
        return this.b.getString(i);
    }

    public final float e(int i) {
        return this.b.getFloat(i, -1.0f);
    }

    public final int e(int i, int i2) {
        return this.b.getLayoutDimension(i, i2);
    }

    public final int f(int i, int i2) {
        return this.b.getResourceId(i, i2);
    }

    public final boolean f(int i) {
        return this.b.hasValue(i);
    }
}
