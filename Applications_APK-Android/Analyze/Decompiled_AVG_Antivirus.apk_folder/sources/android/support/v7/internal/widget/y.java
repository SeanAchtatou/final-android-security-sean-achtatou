package android.support.v7.internal.widget;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Adapter;

public abstract class y extends ViewGroup {
    int A = -1;
    long B = Long.MIN_VALUE;
    boolean C = false;
    private int a;
    private View b;
    private boolean c;
    private boolean d;
    private ac e;
    @ViewDebug.ExportedProperty(category = "scrolling")
    int j = 0;
    int k;
    int l;
    long m = Long.MIN_VALUE;
    long n;
    boolean o = false;
    int p;
    boolean q = false;
    ab r;
    aa s;
    boolean t;
    @ViewDebug.ExportedProperty(category = "list")
    int u = -1;
    long v = Long.MIN_VALUE;
    @ViewDebug.ExportedProperty(category = "list")
    int w = -1;
    long x = Long.MIN_VALUE;
    @ViewDebug.ExportedProperty(category = "list")
    int y;
    int z;

    y(Context context, int i) {
        super(context, null, i);
    }

    private long a(int i) {
        Adapter c2 = c();
        if (c2 == null || i < 0) {
            return Long.MIN_VALUE;
        }
        return c2.getItemId(i);
    }

    /* access modifiers changed from: private */
    public void a() {
        int i;
        if (this.r != null && (i = this.u) >= 0) {
            b();
            c().getItemId(i);
        }
    }

    public void a(aa aaVar) {
        this.s = aaVar;
    }

    public final boolean a(View view) {
        if (this.s == null) {
            return false;
        }
        playSoundEffect(0);
        if (view == null) {
            return true;
        }
        view.sendAccessibilityEvent(1);
        return true;
    }

    public void addView(View view) {
        throw new UnsupportedOperationException("addView(View) is not supported in AdapterView");
    }

    public void addView(View view, int i) {
        throw new UnsupportedOperationException("addView(View, int) is not supported in AdapterView");
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        throw new UnsupportedOperationException("addView(View, int, LayoutParams) is not supported in AdapterView");
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        throw new UnsupportedOperationException("addView(View, LayoutParams) is not supported in AdapterView");
    }

    public abstract View b();

    /* access modifiers changed from: package-private */
    public final void b(int i) {
        this.w = i;
        this.x = a(i);
    }

    public abstract Adapter c();

    /* access modifiers changed from: package-private */
    public final void c(int i) {
        this.u = i;
        this.v = a(i);
        if (this.o && this.p == 0 && i >= 0) {
            this.l = i;
            this.m = this.v;
        }
    }

    /* access modifiers changed from: protected */
    public boolean canAnimate() {
        return super.canAnimate() && this.y > 0;
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        boolean z2 = true;
        Adapter c2 = c();
        boolean z3 = !(c2 == null || c2.getCount() == 0);
        super.setFocusableInTouchMode(z3 && this.d);
        super.setFocusable(z3 && this.c);
        if (this.b != null) {
            if (c2 != null && !c2.isEmpty()) {
                z2 = false;
            }
            if (z2) {
                if (this.b != null) {
                    this.b.setVisibility(0);
                    setVisibility(8);
                } else {
                    setVisibility(0);
                }
                if (this.t) {
                    onLayout(false, getLeft(), getTop(), getRight(), getBottom());
                    return;
                }
                return;
            }
            if (this.b != null) {
                this.b.setVisibility(8);
            }
            setVisibility(0);
        }
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        View b2 = b();
        return b2 != null && b2.getVisibility() == 0 && b2.dispatchPopulateAccessibilityEvent(accessibilityEvent);
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void e() {
        /*
            r18 = this;
            r0 = r18
            int r9 = r0.y
            r6 = 0
            if (r9 <= 0) goto L_0x00c3
            r0 = r18
            boolean r2 = r0.o
            if (r2 == 0) goto L_0x00c1
            r2 = 0
            r0 = r18
            r0.o = r2
            r0 = r18
            int r10 = r0.y
            if (r10 == 0) goto L_0x00bf
            r0 = r18
            long r12 = r0.m
            r0 = r18
            int r2 = r0.l
            r4 = -9223372036854775808
            int r3 = (r12 > r4 ? 1 : (r12 == r4 ? 0 : -1))
            if (r3 == 0) goto L_0x00bf
            r3 = 0
            int r2 = java.lang.Math.max(r3, r2)
            int r3 = r10 + -1
            int r3 = java.lang.Math.min(r3, r2)
            long r4 = android.os.SystemClock.uptimeMillis()
            r14 = 100
            long r14 = r14 + r4
            r2 = 0
            android.widget.Adapter r11 = r18.c()
            if (r11 == 0) goto L_0x00bf
            r4 = r3
            r5 = r3
        L_0x0041:
            long r16 = android.os.SystemClock.uptimeMillis()
            int r7 = (r16 > r14 ? 1 : (r16 == r14 ? 0 : -1))
            if (r7 > 0) goto L_0x00bf
            long r16 = r11.getItemId(r5)
            int r7 = (r16 > r12 ? 1 : (r16 == r12 ? 0 : -1))
            if (r7 != 0) goto L_0x0097
        L_0x0051:
            if (r5 < 0) goto L_0x00c1
            if (r5 != r5) goto L_0x00c1
            r0 = r18
            r0.c(r5)
            r6 = 1
            r2 = r6
        L_0x005c:
            if (r2 != 0) goto L_0x0076
            r0 = r18
            int r3 = r0.u
            if (r3 < r9) goto L_0x0066
            int r3 = r9 + -1
        L_0x0066:
            if (r3 >= 0) goto L_0x0069
            r3 = 0
        L_0x0069:
            if (r3 >= 0) goto L_0x006b
        L_0x006b:
            if (r3 < 0) goto L_0x0076
            r0 = r18
            r0.c(r3)
            r18.f()
            r2 = 1
        L_0x0076:
            if (r2 != 0) goto L_0x0096
            r2 = -1
            r0 = r18
            r0.w = r2
            r2 = -9223372036854775808
            r0 = r18
            r0.x = r2
            r2 = -1
            r0 = r18
            r0.u = r2
            r2 = -9223372036854775808
            r0 = r18
            r0.v = r2
            r2 = 0
            r0 = r18
            r0.o = r2
            r18.f()
        L_0x0096:
            return
        L_0x0097:
            int r7 = r10 + -1
            if (r3 != r7) goto L_0x00af
            r7 = 1
            r8 = r7
        L_0x009d:
            if (r4 != 0) goto L_0x00b2
            r7 = 1
        L_0x00a0:
            if (r8 == 0) goto L_0x00a4
            if (r7 != 0) goto L_0x00bf
        L_0x00a4:
            if (r7 != 0) goto L_0x00aa
            if (r2 == 0) goto L_0x00b4
            if (r8 != 0) goto L_0x00b4
        L_0x00aa:
            int r3 = r3 + 1
            r2 = 0
            r5 = r3
            goto L_0x0041
        L_0x00af:
            r7 = 0
            r8 = r7
            goto L_0x009d
        L_0x00b2:
            r7 = 0
            goto L_0x00a0
        L_0x00b4:
            if (r8 != 0) goto L_0x00ba
            if (r2 != 0) goto L_0x0041
            if (r7 != 0) goto L_0x0041
        L_0x00ba:
            int r4 = r4 + -1
            r2 = 1
            r5 = r4
            goto L_0x0041
        L_0x00bf:
            r5 = -1
            goto L_0x0051
        L_0x00c1:
            r2 = r6
            goto L_0x005c
        L_0x00c3:
            r2 = r6
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.y.e():void");
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        if (this.w != this.A || this.x != this.B) {
            if (this.r != null) {
                if (this.q || this.C) {
                    if (this.e == null) {
                        this.e = new ac(this, (byte) 0);
                    }
                    post(this.e);
                } else {
                    a();
                }
            }
            if (this.w != -1 && isShown() && !isInTouchMode()) {
                sendAccessibilityEvent(4);
            }
            this.A = this.w;
            this.B = this.x;
        }
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        if (getChildCount() > 0) {
            this.o = true;
            this.n = (long) this.a;
            if (this.w >= 0) {
                View childAt = getChildAt(this.w - this.j);
                this.m = this.v;
                this.l = this.u;
                if (childAt != null) {
                    this.k = childAt.getTop();
                }
                this.p = 0;
                return;
            }
            View childAt2 = getChildAt(0);
            Adapter c2 = c();
            if (this.j < 0 || this.j >= c2.getCount()) {
                this.m = -1;
            } else {
                this.m = c2.getItemId(this.j);
            }
            this.l = this.j;
            if (childAt2 != null) {
                this.k = childAt2.getTop();
            }
            this.p = 1;
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.e);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        this.a = getHeight();
    }

    public void removeAllViews() {
        throw new UnsupportedOperationException("removeAllViews() is not supported in AdapterView");
    }

    public void removeView(View view) {
        throw new UnsupportedOperationException("removeView(View) is not supported in AdapterView");
    }

    public void removeViewAt(int i) {
        throw new UnsupportedOperationException("removeViewAt(int) is not supported in AdapterView");
    }

    public void setFocusable(boolean z2) {
        boolean z3 = true;
        Adapter c2 = c();
        boolean z4 = c2 == null || c2.getCount() == 0;
        this.c = z2;
        if (!z2) {
            this.d = false;
        }
        if (!z2 || z4) {
            z3 = false;
        }
        super.setFocusable(z3);
    }

    public void setFocusableInTouchMode(boolean z2) {
        boolean z3 = true;
        Adapter c2 = c();
        boolean z4 = c2 == null || c2.getCount() == 0;
        this.d = z2;
        if (z2) {
            this.c = true;
        }
        if (!z2 || z4) {
            z3 = false;
        }
        super.setFocusableInTouchMode(z3);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        throw new RuntimeException("Don't call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead");
    }
}
