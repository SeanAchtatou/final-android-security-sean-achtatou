package android.support.v7.internal.a;

import android.content.Context;
import android.support.v7.d.a;
import android.support.v7.d.b;
import android.support.v7.internal.view.f;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

public final class m extends a implements j {
    final /* synthetic */ i a;
    private final Context b;
    private final i c;
    private b d;
    private WeakReference e;

    public m(i iVar, Context context, b bVar) {
        this.a = iVar;
        this.b = context;
        this.d = bVar;
        this.c = new i(context).a();
        this.c.a(this);
    }

    public final MenuInflater a() {
        return new f(this.b);
    }

    public final void a(int i) {
        b(this.a.j.getResources().getString(i));
    }

    public final void a(i iVar) {
        if (this.d != null) {
            d();
            this.a.q.a();
        }
    }

    public final void a(View view) {
        this.a.q.d(view);
        this.e = new WeakReference(view);
    }

    public final void a(CharSequence charSequence) {
        this.a.q.b(charSequence);
    }

    public final void a(boolean z) {
        super.a(z);
        this.a.q.a(z);
    }

    public final boolean a(i iVar, MenuItem menuItem) {
        if (this.d != null) {
            return this.d.a(this, menuItem);
        }
        return false;
    }

    public final Menu b() {
        return this.c;
    }

    public final void b(int i) {
        a((CharSequence) this.a.j.getResources().getString(i));
    }

    public final void b(CharSequence charSequence) {
        this.a.q.a(charSequence);
    }

    public final void c() {
        if (this.a.a == this) {
            if (!i.a(this.a.D, this.a.E, false)) {
                this.a.b = this;
                this.a.c = this.d;
            } else {
                this.d.a(this);
            }
            this.d = null;
            this.a.f(false);
            this.a.q.d();
            this.a.p.a().sendAccessibilityEvent(32);
            this.a.n.b(this.a.d);
            this.a.a = null;
        }
    }

    public final void d() {
        if (this.a.a == this) {
            this.c.g();
            try {
                this.d.b(this, this.c);
            } finally {
                this.c.h();
            }
        }
    }

    public final boolean e() {
        this.c.g();
        try {
            return this.d.a(this, this.c);
        } finally {
            this.c.h();
        }
    }

    public final CharSequence f() {
        return this.a.q.b();
    }

    public final CharSequence g() {
        return this.a.q.c();
    }

    public final boolean h() {
        return this.a.q.f();
    }

    public final View i() {
        if (this.e != null) {
            return (View) this.e.get();
        }
        return null;
    }
}
