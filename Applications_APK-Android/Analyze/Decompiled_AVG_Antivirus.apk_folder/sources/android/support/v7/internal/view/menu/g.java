package android.support.v7.internal.view.menu;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.a.i;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;

public final class g implements x, AdapterView.OnItemClickListener {
    Context a;
    LayoutInflater b;
    i c;
    ExpandedMenuView d;
    int e;
    int f;
    h g;
    /* access modifiers changed from: private */
    public int h;
    private y i;
    private int j;

    private g(int i2) {
        this.f = i2;
        this.e = 0;
    }

    public g(Context context, int i2) {
        this(i2);
        this.a = context;
        this.b = LayoutInflater.from(this.a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final z a(ViewGroup viewGroup) {
        if (this.d == null) {
            this.d = (ExpandedMenuView) this.b.inflate(i.h, viewGroup, false);
            if (this.g == null) {
                this.g = new h(this);
            }
            this.d.setAdapter((ListAdapter) this.g);
            this.d.setOnItemClickListener(this);
        }
        return this.d;
    }

    public final void a(Context context, i iVar) {
        if (this.e != 0) {
            this.a = new ContextThemeWrapper(context, this.e);
            this.b = LayoutInflater.from(this.a);
        } else if (this.a != null) {
            this.a = context;
            if (this.b == null) {
                this.b = LayoutInflater.from(this.a);
            }
        }
        this.c = iVar;
        if (this.g != null) {
            this.g.notifyDataSetChanged();
        }
    }

    public final void a(Parcelable parcelable) {
        SparseArray sparseParcelableArray = ((Bundle) parcelable).getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.d.restoreHierarchyState(sparseParcelableArray);
        }
    }

    public final void a(i iVar, boolean z) {
        if (this.i != null) {
            this.i.a(iVar, z);
        }
    }

    public final void a(y yVar) {
        this.i = yVar;
    }

    public final void a(boolean z) {
        if (this.g != null) {
            this.g.notifyDataSetChanged();
        }
    }

    public final boolean a() {
        return false;
    }

    public final boolean a(ad adVar) {
        if (!adVar.hasVisibleItems()) {
            return false;
        }
        new l(adVar).a();
        if (this.i != null) {
            this.i.a(adVar);
        }
        return true;
    }

    public final boolean a(m mVar) {
        return false;
    }

    public final int b() {
        return this.j;
    }

    public final boolean b(m mVar) {
        return false;
    }

    public final ListAdapter c() {
        if (this.g == null) {
            this.g = new h(this);
        }
        return this.g;
    }

    public final Parcelable d() {
        if (this.d == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        SparseArray sparseArray = new SparseArray();
        if (this.d != null) {
            this.d.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        return bundle;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        this.c.a(this.g.getItem(i2), this, 0);
    }
}
