package android.support.v7.internal.widget;

import android.view.ViewTreeObserver;

final class ar implements ViewTreeObserver.OnGlobalLayoutListener {
    final /* synthetic */ SpinnerCompat a;

    ar(SpinnerCompat spinnerCompat) {
        this.a = spinnerCompat;
    }

    public final void onGlobalLayout() {
        if (!this.a.F.b()) {
            this.a.F.c();
        }
        ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
        if (viewTreeObserver != null) {
            viewTreeObserver.removeGlobalOnLayoutListener(this);
        }
    }
}
