package android.support.v7.internal.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.a.l;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow;
import java.lang.reflect.Field;

public class AppCompatPopupWindow extends PopupWindow {
    private final boolean a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.be.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.be.a(int, int):int
      android.support.v7.internal.widget.be.a(int, boolean):boolean */
    public AppCompatPopupWindow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        be a2 = be.a(context, attributeSet, l.aM, i);
        this.a = a2.a(l.aP, false);
        setBackgroundDrawable(a2.a(l.aO));
        a2.b();
        if (Build.VERSION.SDK_INT < 14) {
            try {
                Field declaredField = PopupWindow.class.getDeclaredField("mAnchor");
                declaredField.setAccessible(true);
                Field declaredField2 = PopupWindow.class.getDeclaredField("mOnScrollChangedListener");
                declaredField2.setAccessible(true);
                declaredField2.set(this, new ad(declaredField, this, (ViewTreeObserver.OnScrollChangedListener) declaredField2.get(this)));
            } catch (Exception e) {
                Log.d("AppCompatPopupWindow", "Exception while installing workaround OnScrollChangedListener", e);
            }
        }
    }

    public void showAsDropDown(View view, int i, int i2) {
        if (Build.VERSION.SDK_INT < 21 && this.a) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2);
    }

    @TargetApi(19)
    public void showAsDropDown(View view, int i, int i2, int i3) {
        if (Build.VERSION.SDK_INT < 21 && this.a) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2, i3);
    }

    public void update(View view, int i, int i2, int i3, int i4) {
        super.update(view, i, (Build.VERSION.SDK_INT >= 21 || !this.a) ? i2 : i2 - view.getHeight(), i3, i4);
    }
}
