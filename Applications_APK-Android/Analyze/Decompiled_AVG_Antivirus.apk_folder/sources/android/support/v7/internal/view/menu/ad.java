package android.support.v7.internal.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.a;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public final class ad extends i implements SubMenu {
    private i d;
    private m e;

    public ad(Context context, i iVar, m mVar) {
        super(context);
        this.d = iVar;
        this.e = mVar;
    }

    public final void a(j jVar) {
        this.d.a(jVar);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(i iVar, MenuItem menuItem) {
        return super.a(iVar, menuItem) || this.d.a(iVar, menuItem);
    }

    public final boolean a(m mVar) {
        return this.d.a(mVar);
    }

    public final String b() {
        int itemId = this.e != null ? this.e.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.b() + ":" + itemId;
    }

    public final boolean b(m mVar) {
        return this.d.b(mVar);
    }

    public final boolean c() {
        return this.d.c();
    }

    public final boolean d() {
        return this.d.d();
    }

    public final MenuItem getItem() {
        return this.e;
    }

    public final i o() {
        return this.d;
    }

    public final Menu r() {
        return this.d;
    }

    public final SubMenu setHeaderIcon(int i) {
        super.a(a.a(e(), i));
        return this;
    }

    public final SubMenu setHeaderIcon(Drawable drawable) {
        super.a(drawable);
        return this;
    }

    public final SubMenu setHeaderTitle(int i) {
        super.a(e().getResources().getString(i));
        return this;
    }

    public final SubMenu setHeaderTitle(CharSequence charSequence) {
        super.a(charSequence);
        return this;
    }

    public final SubMenu setHeaderView(View view) {
        super.a(view);
        return this;
    }

    public final SubMenu setIcon(int i) {
        this.e.setIcon(i);
        return this;
    }

    public final SubMenu setIcon(Drawable drawable) {
        this.e.setIcon(drawable);
        return this;
    }

    public final void setQwertyMode(boolean z) {
        this.d.setQwertyMode(z);
    }
}
