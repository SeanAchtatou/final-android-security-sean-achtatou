package android.support.v7.internal.widget;

import android.content.Context;
import android.support.v7.widget.ListPopupWindow;
import android.widget.ListAdapter;

final class au extends ListPopupWindow implements ax {
    final /* synthetic */ SpinnerCompat a;
    private CharSequence c;
    /* access modifiers changed from: private */
    public ListAdapter d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public au(SpinnerCompat spinnerCompat, Context context, int i) {
        super(context, null, i);
        this.a = spinnerCompat;
        a(spinnerCompat);
        e();
        d();
        a(new av(this, spinnerCompat));
    }

    public final void a(ListAdapter listAdapter) {
        super.a(listAdapter);
        this.d = listAdapter;
    }

    public final void a(CharSequence charSequence) {
        this.c = charSequence;
    }
}
