package android.support.v7.internal.widget;

import android.view.View;
import android.widget.AdapterView;

final class av implements AdapterView.OnItemClickListener {
    final /* synthetic */ SpinnerCompat a;
    final /* synthetic */ au b;

    av(au auVar, SpinnerCompat spinnerCompat) {
        this.b = auVar;
        this.a = spinnerCompat;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.b.a.a(i);
        if (this.b.a.s != null) {
            SpinnerCompat spinnerCompat = this.b.a;
            this.b.d.getItemId(i);
            spinnerCompat.a(view);
        }
        this.b.a();
    }
}
