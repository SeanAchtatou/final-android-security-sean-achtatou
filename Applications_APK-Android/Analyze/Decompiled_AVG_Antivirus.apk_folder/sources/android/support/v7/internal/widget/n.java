package android.support.v7.internal.widget;

import android.content.pm.ResolveInfo;
import java.math.BigDecimal;

public final class n implements Comparable {
    public final ResolveInfo a;
    public float b;
    final /* synthetic */ m c;

    public n(m mVar, ResolveInfo resolveInfo) {
        this.c = mVar;
        this.a = resolveInfo;
    }

    public final /* synthetic */ int compareTo(Object obj) {
        return Float.floatToIntBits(((n) obj).b) - Float.floatToIntBits(this.b);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return Float.floatToIntBits(this.b) == Float.floatToIntBits(((n) obj).b);
    }

    public final int hashCode() {
        return Float.floatToIntBits(this.b) + 31;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append("resolveInfo:").append(this.a.toString());
        sb.append("; weight:").append(new BigDecimal((double) this.b));
        sb.append("]");
        return sb.toString();
    }
}
