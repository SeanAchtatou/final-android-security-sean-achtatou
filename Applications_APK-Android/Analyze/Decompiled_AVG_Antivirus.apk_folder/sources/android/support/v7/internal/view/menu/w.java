package android.support.v7.internal.view.menu;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

final class w extends BaseAdapter {
    final /* synthetic */ v a;
    /* access modifiers changed from: private */
    public i b;
    private int c = -1;

    public w(v vVar, i iVar) {
        this.a = vVar;
        this.b = iVar;
        a();
    }

    private void a() {
        m q = this.a.e.q();
        if (q != null) {
            ArrayList n = this.a.e.n();
            int size = n.size();
            for (int i = 0; i < size; i++) {
                if (((m) n.get(i)) == q) {
                    this.c = i;
                    return;
                }
            }
        }
        this.c = -1;
    }

    /* renamed from: a */
    public final m getItem(int i) {
        ArrayList n = this.a.g ? this.b.n() : this.b.k();
        if (this.c >= 0 && i >= this.c) {
            i++;
        }
        return (m) n.get(i);
    }

    public final int getCount() {
        ArrayList n = this.a.g ? this.b.n() : this.b.k();
        return this.c < 0 ? n.size() : n.size() - 1;
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.d.inflate(v.a, viewGroup, false) : view;
        aa aaVar = (aa) inflate;
        if (this.a.b) {
            ((ListMenuItemView) inflate).c();
        }
        aaVar.a(getItem(i));
        return inflate;
    }

    public final void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
