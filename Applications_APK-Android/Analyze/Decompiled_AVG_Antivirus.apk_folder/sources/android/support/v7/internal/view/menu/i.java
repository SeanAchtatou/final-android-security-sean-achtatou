package android.support.v7.internal.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.c.a.a;
import android.support.v4.view.as;
import android.support.v4.view.n;
import android.support.v7.a.c;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class i implements a {
    private static final int[] d = {1, 4, 5, 3, 2, 0};
    CharSequence a;
    Drawable b;
    View c;
    private final Context e;
    private final Resources f;
    private boolean g;
    private boolean h;
    private j i;
    private ArrayList j;
    private ArrayList k;
    private boolean l;
    private ArrayList m;
    private ArrayList n;
    private boolean o;
    private int p = 0;
    private ContextMenu.ContextMenuInfo q;
    private boolean r = false;
    private boolean s = false;
    private boolean t = false;
    private boolean u = false;
    private ArrayList v = new ArrayList();
    private CopyOnWriteArrayList w = new CopyOnWriteArrayList();
    private m x;
    private boolean y;

    public i(Context context) {
        boolean z = true;
        this.e = context;
        this.f = context.getResources();
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.l = true;
        this.m = new ArrayList();
        this.n = new ArrayList();
        this.o = true;
        this.h = (this.f.getConfiguration().keyboard == 1 || !this.f.getBoolean(c.abc_config_showMenuShortcutsWhenKeyboardPresent)) ? false : z;
    }

    private static int a(ArrayList arrayList, int i2) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (((m) arrayList.get(size)).c() <= i2) {
                return size + 1;
            }
        }
        return 0;
    }

    private m a(int i2, KeyEvent keyEvent) {
        ArrayList arrayList = this.v;
        arrayList.clear();
        a(arrayList, i2, keyEvent);
        if (arrayList.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        keyEvent.getKeyData(keyData);
        int size = arrayList.size();
        if (size == 1) {
            return (m) arrayList.get(0);
        }
        boolean c2 = c();
        for (int i3 = 0; i3 < size; i3++) {
            m mVar = (m) arrayList.get(i3);
            char alphabeticShortcut = c2 ? mVar.getAlphabeticShortcut() : mVar.getNumericShortcut();
            if (alphabeticShortcut == keyData.meta[0] && (metaState & 2) == 0) {
                return mVar;
            }
            if (alphabeticShortcut == keyData.meta[2] && (metaState & 2) != 0) {
                return mVar;
            }
            if (c2 && alphabeticShortcut == 8 && i2 == 67) {
                return mVar;
            }
        }
        return null;
    }

    private MenuItem a(int i2, int i3, int i4, CharSequence charSequence) {
        int i5 = (-65536 & i4) >> 16;
        if (i5 < 0 || i5 >= d.length) {
            throw new IllegalArgumentException("order does not contain a valid category.");
        }
        int i6 = (d[i5] << 16) | (65535 & i4);
        m mVar = new m(this, i2, i3, i4, i6, charSequence, this.p);
        if (this.q != null) {
            mVar.a(this.q);
        }
        this.j.add(a(this.j, i6), mVar);
        b(true);
        return mVar;
    }

    private void a(int i2, boolean z) {
        if (i2 >= 0 && i2 < this.j.size()) {
            this.j.remove(i2);
            if (z) {
                b(true);
            }
        }
    }

    private void a(CharSequence charSequence, Drawable drawable, View view) {
        if (view != null) {
            this.c = view;
            this.a = null;
            this.b = null;
        } else {
            if (charSequence != null) {
                this.a = charSequence;
            }
            if (drawable != null) {
                this.b = drawable;
            }
            this.c = null;
        }
        b(false);
    }

    private void a(List list, int i2, KeyEvent keyEvent) {
        boolean c2 = c();
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        if (keyEvent.getKeyData(keyData) || i2 == 67) {
            int size = this.j.size();
            for (int i3 = 0; i3 < size; i3++) {
                m mVar = (m) this.j.get(i3);
                if (mVar.hasSubMenu()) {
                    ((i) mVar.getSubMenu()).a(list, i2, keyEvent);
                }
                char alphabeticShortcut = c2 ? mVar.getAlphabeticShortcut() : mVar.getNumericShortcut();
                if ((metaState & 5) == 0 && alphabeticShortcut != 0 && ((alphabeticShortcut == keyData.meta[0] || alphabeticShortcut == keyData.meta[2] || (c2 && alphabeticShortcut == 8 && i2 == 67)) && mVar.isEnabled())) {
                    list.add(mVar);
                }
            }
        }
    }

    public final i a() {
        this.p = 1;
        return this;
    }

    /* access modifiers changed from: protected */
    public final i a(Drawable drawable) {
        a((CharSequence) null, drawable, (View) null);
        return this;
    }

    /* access modifiers changed from: protected */
    public final i a(View view) {
        a((CharSequence) null, (Drawable) null, view);
        return this;
    }

    /* access modifiers changed from: protected */
    public final i a(CharSequence charSequence) {
        a(charSequence, (Drawable) null, (View) null);
        return this;
    }

    public final void a(Bundle bundle) {
        Parcelable d2;
        if (!this.w.isEmpty()) {
            SparseArray sparseArray = new SparseArray();
            Iterator it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                } else {
                    int b2 = xVar.b();
                    if (b2 > 0 && (d2 = xVar.d()) != null) {
                        sparseArray.put(b2, d2);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:presenters", sparseArray);
        }
    }

    public void a(j jVar) {
        this.i = jVar;
    }

    public final void a(x xVar) {
        a(xVar, this.e);
    }

    public final void a(x xVar, Context context) {
        this.w.add(new WeakReference(xVar));
        xVar.a(context, this);
        this.o = true;
    }

    /* access modifiers changed from: package-private */
    public final void a(MenuItem menuItem) {
        int groupId = menuItem.getGroupId();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            m mVar = (m) this.j.get(i2);
            if (mVar.getGroupId() == groupId && mVar.g() && mVar.isCheckable()) {
                mVar.b(mVar == menuItem);
            }
        }
    }

    public final void a(boolean z) {
        if (!this.u) {
            this.u = true;
            Iterator it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                } else {
                    xVar.a(this, z);
                }
            }
            this.u = false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(i iVar, MenuItem menuItem) {
        return this.i != null && this.i.a(iVar, menuItem);
    }

    public boolean a(m mVar) {
        boolean z = false;
        if (!this.w.isEmpty()) {
            g();
            Iterator it = this.w.iterator();
            boolean z2 = false;
            while (true) {
                if (!it.hasNext()) {
                    z = z2;
                    break;
                }
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                } else {
                    z = xVar.a(mVar);
                    if (z) {
                        break;
                    }
                    z2 = z;
                }
            }
            h();
            if (z) {
                this.x = mVar;
            }
        }
        return z;
    }

    public final boolean a(MenuItem menuItem, x xVar, int i2) {
        boolean z = false;
        m mVar = (m) menuItem;
        if (mVar == null || !mVar.isEnabled()) {
            return false;
        }
        boolean b2 = mVar.b();
        n a2 = mVar.a();
        boolean z2 = a2 != null && a2.e();
        if (mVar.m()) {
            boolean expandActionView = mVar.expandActionView() | b2;
            if (!expandActionView) {
                return expandActionView;
            }
            a(true);
            return expandActionView;
        } else if (mVar.hasSubMenu() || z2) {
            a(false);
            if (!mVar.hasSubMenu()) {
                mVar.a(new ad(this.e, this, mVar));
            }
            ad adVar = (ad) mVar.getSubMenu();
            if (z2) {
                a2.a(adVar);
            }
            if (!this.w.isEmpty()) {
                if (xVar != null) {
                    z = xVar.a(adVar);
                }
                Iterator it = this.w.iterator();
                boolean z3 = z;
                while (it.hasNext()) {
                    WeakReference weakReference = (WeakReference) it.next();
                    x xVar2 = (x) weakReference.get();
                    if (xVar2 == null) {
                        this.w.remove(weakReference);
                    } else {
                        z3 = !z3 ? xVar2.a(adVar) : z3;
                    }
                }
                z = z3;
            }
            boolean z4 = b2 | z;
            if (z4) {
                return z4;
            }
            a(true);
            return z4;
        } else {
            if ((i2 & 1) == 0) {
                a(true);
            }
            return b2;
        }
    }

    public MenuItem add(int i2) {
        return a(0, 0, 0, this.f.getString(i2));
    }

    public MenuItem add(int i2, int i3, int i4, int i5) {
        return a(i2, i3, i4, this.f.getString(i5));
    }

    public MenuItem add(int i2, int i3, int i4, CharSequence charSequence) {
        return a(i2, i3, i4, charSequence);
    }

    public MenuItem add(CharSequence charSequence) {
        return a(0, 0, 0, charSequence);
    }

    public int addIntentOptions(int i2, int i3, int i4, ComponentName componentName, Intent[] intentArr, Intent intent, int i5, MenuItem[] menuItemArr) {
        PackageManager packageManager = this.e.getPackageManager();
        List<ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i5 & 1) == 0) {
            removeGroup(i2);
        }
        for (int i6 = 0; i6 < size; i6++) {
            ResolveInfo resolveInfo = queryIntentActivityOptions.get(i6);
            Intent intent2 = new Intent(resolveInfo.specificIndex < 0 ? intent : intentArr[resolveInfo.specificIndex]);
            intent2.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            MenuItem intent3 = add(i2, i3, i4, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent2);
            if (menuItemArr != null && resolveInfo.specificIndex >= 0) {
                menuItemArr[resolveInfo.specificIndex] = intent3;
            }
        }
        return size;
    }

    public SubMenu addSubMenu(int i2) {
        return addSubMenu(0, 0, 0, this.f.getString(i2));
    }

    public SubMenu addSubMenu(int i2, int i3, int i4, int i5) {
        return addSubMenu(i2, i3, i4, this.f.getString(i5));
    }

    public SubMenu addSubMenu(int i2, int i3, int i4, CharSequence charSequence) {
        m mVar = (m) a(i2, i3, i4, charSequence);
        ad adVar = new ad(this.e, this, mVar);
        mVar.a(adVar);
        return adVar;
    }

    public SubMenu addSubMenu(CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "android:menu:actionviewstates";
    }

    public final void b(Bundle bundle) {
        Parcelable parcelable;
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:presenters");
        if (sparseParcelableArray != null && !this.w.isEmpty()) {
            Iterator it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                } else {
                    int b2 = xVar.b();
                    if (b2 > 0 && (parcelable = (Parcelable) sparseParcelableArray.get(b2)) != null) {
                        xVar.a(parcelable);
                    }
                }
            }
        }
    }

    public final void b(x xVar) {
        Iterator it = this.w.iterator();
        while (it.hasNext()) {
            WeakReference weakReference = (WeakReference) it.next();
            x xVar2 = (x) weakReference.get();
            if (xVar2 == null || xVar2 == xVar) {
                this.w.remove(weakReference);
            }
        }
    }

    public final void b(boolean z) {
        if (!this.r) {
            if (z) {
                this.l = true;
                this.o = true;
            }
            if (!this.w.isEmpty()) {
                g();
                Iterator it = this.w.iterator();
                while (it.hasNext()) {
                    WeakReference weakReference = (WeakReference) it.next();
                    x xVar = (x) weakReference.get();
                    if (xVar == null) {
                        this.w.remove(weakReference);
                    } else {
                        xVar.a(z);
                    }
                }
                h();
                return;
            }
            return;
        }
        this.s = true;
    }

    public boolean b(m mVar) {
        boolean z = false;
        if (!this.w.isEmpty() && this.x == mVar) {
            g();
            Iterator it = this.w.iterator();
            boolean z2 = false;
            while (true) {
                if (!it.hasNext()) {
                    z = z2;
                    break;
                }
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                } else {
                    z = xVar.b(mVar);
                    if (z) {
                        break;
                    }
                    z2 = z;
                }
            }
            h();
            if (z) {
                this.x = null;
            }
        }
        return z;
    }

    public final boolean b(MenuItem menuItem) {
        return a(menuItem, (x) null, 0);
    }

    public final void c(Bundle bundle) {
        int size = size();
        int i2 = 0;
        SparseArray sparseArray = null;
        while (i2 < size) {
            MenuItem item = getItem(i2);
            View a2 = as.a(item);
            if (!(a2 == null || a2.getId() == -1)) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray();
                }
                a2.saveHierarchyState(sparseArray);
                if (as.c(item)) {
                    bundle.putInt("android:menu:expandedactionview", item.getItemId());
                }
            }
            SparseArray sparseArray2 = sparseArray;
            if (item.hasSubMenu()) {
                ((ad) item.getSubMenu()).c(bundle);
            }
            i2++;
            sparseArray = sparseArray2;
        }
        if (sparseArray != null) {
            bundle.putSparseParcelableArray(b(), sparseArray);
        }
    }

    public final void c(boolean z) {
        this.y = z;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.g;
    }

    public void clear() {
        if (this.x != null) {
            b(this.x);
        }
        this.j.clear();
        b(true);
    }

    public void clearHeader() {
        this.b = null;
        this.a = null;
        this.c = null;
        b(false);
    }

    public void close() {
        a(true);
    }

    public final void d(Bundle bundle) {
        MenuItem findItem;
        if (bundle != null) {
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray(b());
            int size = size();
            for (int i2 = 0; i2 < size; i2++) {
                MenuItem item = getItem(i2);
                View a2 = as.a(item);
                if (!(a2 == null || a2.getId() == -1)) {
                    a2.restoreHierarchyState(sparseParcelableArray);
                }
                if (item.hasSubMenu()) {
                    ((ad) item.getSubMenu()).d(bundle);
                }
            }
            int i3 = bundle.getInt("android:menu:expandedactionview");
            if (i3 > 0 && (findItem = findItem(i3)) != null) {
                as.b(findItem);
            }
        }
    }

    public boolean d() {
        return this.h;
    }

    public final Context e() {
        return this.e;
    }

    public final void f() {
        if (this.i != null) {
            this.i.a(this);
        }
    }

    public MenuItem findItem(int i2) {
        MenuItem findItem;
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            m mVar = (m) this.j.get(i3);
            if (mVar.getItemId() == i2) {
                return mVar;
            }
            if (mVar.hasSubMenu() && (findItem = mVar.getSubMenu().findItem(i2)) != null) {
                return findItem;
            }
        }
        return null;
    }

    public final void g() {
        if (!this.r) {
            this.r = true;
            this.s = false;
        }
    }

    public MenuItem getItem(int i2) {
        return (MenuItem) this.j.get(i2);
    }

    public final void h() {
        this.r = false;
        if (this.s) {
            this.s = false;
            b(true);
        }
    }

    public boolean hasVisibleItems() {
        if (this.y) {
            return true;
        }
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (((m) this.j.get(i2)).isVisible()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final void i() {
        this.l = true;
        b(true);
    }

    public boolean isShortcutKey(int i2, KeyEvent keyEvent) {
        return a(i2, keyEvent) != null;
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        this.o = true;
        b(true);
    }

    public final ArrayList k() {
        if (!this.l) {
            return this.k;
        }
        this.k.clear();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            m mVar = (m) this.j.get(i2);
            if (mVar.isVisible()) {
                this.k.add(mVar);
            }
        }
        this.l = false;
        this.o = true;
        return this.k;
    }

    public final void l() {
        ArrayList k2 = k();
        if (this.o) {
            Iterator it = this.w.iterator();
            boolean z = false;
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                } else {
                    z = xVar.a() | z;
                }
            }
            if (z) {
                this.m.clear();
                this.n.clear();
                int size = k2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    m mVar = (m) k2.get(i2);
                    if (mVar.i()) {
                        this.m.add(mVar);
                    } else {
                        this.n.add(mVar);
                    }
                }
            } else {
                this.m.clear();
                this.n.clear();
                this.n.addAll(k());
            }
            this.o = false;
        }
    }

    public final ArrayList m() {
        l();
        return this.m;
    }

    public final ArrayList n() {
        l();
        return this.n;
    }

    public i o() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public final boolean p() {
        return this.t;
    }

    public boolean performIdentifierAction(int i2, int i3) {
        return a(findItem(i2), (x) null, i3);
    }

    public boolean performShortcut(int i2, KeyEvent keyEvent, int i3) {
        m a2 = a(i2, keyEvent);
        boolean z = false;
        if (a2 != null) {
            z = a(a2, (x) null, i3);
        }
        if ((i3 & 2) != 0) {
            a(true);
        }
        return z;
    }

    public final m q() {
        return this.x;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.i.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.internal.view.menu.i.a(java.util.ArrayList, int):int
      android.support.v7.internal.view.menu.i.a(int, android.view.KeyEvent):android.support.v7.internal.view.menu.m
      android.support.v7.internal.view.menu.i.a(android.support.v7.internal.view.menu.x, android.content.Context):void
      android.support.v7.internal.view.menu.i.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.internal.view.menu.i.a(int, boolean):void */
    public void removeGroup(int i2) {
        int i3;
        int size = size();
        int i4 = 0;
        while (true) {
            if (i4 >= size) {
                i3 = -1;
                break;
            } else if (((m) this.j.get(i4)).getGroupId() == i2) {
                i3 = i4;
                break;
            } else {
                i4++;
            }
        }
        if (i3 >= 0) {
            int size2 = this.j.size() - i3;
            int i5 = 0;
            while (true) {
                int i6 = i5 + 1;
                if (i5 >= size2 || ((m) this.j.get(i3)).getGroupId() != i2) {
                    b(true);
                } else {
                    a(i3, false);
                    i5 = i6;
                }
            }
            b(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.i.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.internal.view.menu.i.a(java.util.ArrayList, int):int
      android.support.v7.internal.view.menu.i.a(int, android.view.KeyEvent):android.support.v7.internal.view.menu.m
      android.support.v7.internal.view.menu.i.a(android.support.v7.internal.view.menu.x, android.content.Context):void
      android.support.v7.internal.view.menu.i.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.internal.view.menu.i.a(int, boolean):void */
    public void removeItem(int i2) {
        int i3;
        int size = size();
        int i4 = 0;
        while (true) {
            if (i4 >= size) {
                i3 = -1;
                break;
            } else if (((m) this.j.get(i4)).getItemId() == i2) {
                i3 = i4;
                break;
            } else {
                i4++;
            }
        }
        a(i3, true);
    }

    public void setGroupCheckable(int i2, boolean z, boolean z2) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            m mVar = (m) this.j.get(i3);
            if (mVar.getGroupId() == i2) {
                mVar.a(z2);
                mVar.setCheckable(z);
            }
        }
    }

    public void setGroupEnabled(int i2, boolean z) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            m mVar = (m) this.j.get(i3);
            if (mVar.getGroupId() == i2) {
                mVar.setEnabled(z);
            }
        }
    }

    public void setGroupVisible(int i2, boolean z) {
        int size = this.j.size();
        int i3 = 0;
        boolean z2 = false;
        while (i3 < size) {
            m mVar = (m) this.j.get(i3);
            i3++;
            z2 = (mVar.getGroupId() != i2 || !mVar.c(z)) ? z2 : true;
        }
        if (z2) {
            b(true);
        }
    }

    public void setQwertyMode(boolean z) {
        this.g = z;
        b(false);
    }

    public int size() {
        return this.j.size();
    }
}
