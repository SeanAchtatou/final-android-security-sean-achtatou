package android.support.v7.internal.view.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.a.g;
import android.support.v7.a.i;
import android.support.v7.a.l;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class ListMenuItemView extends LinearLayout implements aa {
    private m a;
    private ImageView b;
    private RadioButton c;
    private TextView d;
    private CheckBox e;
    private TextView f;
    private Drawable g;
    private int h;
    private Context i;
    private boolean j;
    private int k;
    private Context l;
    private LayoutInflater m;
    private boolean n;

    public ListMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ListMenuItemView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        this.l = context;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.aI, i2, 0);
        this.g = obtainStyledAttributes.getDrawable(l.aJ);
        this.h = obtainStyledAttributes.getResourceId(l.aK, -1);
        this.j = obtainStyledAttributes.getBoolean(l.aL, false);
        this.i = context;
        obtainStyledAttributes.recycle();
    }

    private LayoutInflater d() {
        if (this.m == null) {
            this.m = LayoutInflater.from(this.l);
        }
        return this.m;
    }

    public final m a() {
        return this.a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.internal.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final void a(m mVar) {
        CompoundButton compoundButton;
        CompoundButton compoundButton2;
        this.a = mVar;
        this.k = 0;
        setVisibility(mVar.isVisible() ? 0 : 8);
        CharSequence a2 = mVar.a((aa) this);
        if (a2 != null) {
            this.d.setText(a2);
            if (this.d.getVisibility() != 0) {
                this.d.setVisibility(0);
            }
        } else if (this.d.getVisibility() != 8) {
            this.d.setVisibility(8);
        }
        boolean isCheckable = mVar.isCheckable();
        if (!(!isCheckable && this.c == null && this.e == null)) {
            if (this.a.g()) {
                if (this.c == null) {
                    this.c = (RadioButton) d().inflate(i.l, (ViewGroup) this, false);
                    addView(this.c);
                }
                compoundButton = this.c;
                compoundButton2 = this.e;
            } else {
                if (this.e == null) {
                    this.e = (CheckBox) d().inflate(i.i, (ViewGroup) this, false);
                    addView(this.e);
                }
                compoundButton = this.e;
                compoundButton2 = this.c;
            }
            if (isCheckable) {
                compoundButton.setChecked(this.a.isChecked());
                int i2 = isCheckable ? 0 : 8;
                if (compoundButton.getVisibility() != i2) {
                    compoundButton.setVisibility(i2);
                }
                if (!(compoundButton2 == null || compoundButton2.getVisibility() == 8)) {
                    compoundButton2.setVisibility(8);
                }
            } else {
                if (this.e != null) {
                    this.e.setVisibility(8);
                }
                if (this.c != null) {
                    this.c.setVisibility(8);
                }
            }
        }
        boolean f2 = mVar.f();
        mVar.d();
        int i3 = (!f2 || !this.a.f()) ? 8 : 0;
        if (i3 == 0) {
            this.f.setText(this.a.e());
        }
        if (this.f.getVisibility() != i3) {
            this.f.setVisibility(i3);
        }
        Drawable icon = mVar.getIcon();
        boolean z = this.a.h() || this.n;
        if ((z || this.j) && !(this.b == null && icon == null && !this.j)) {
            if (this.b == null) {
                this.b = (ImageView) d().inflate(i.j, (ViewGroup) this, false);
                addView(this.b, 0);
            }
            if (icon != null || this.j) {
                this.b.setImageDrawable(z ? icon : null);
                if (this.b.getVisibility() != 0) {
                    this.b.setVisibility(0);
                }
            } else {
                this.b.setVisibility(8);
            }
        }
        setEnabled(mVar.isEnabled());
    }

    public final boolean b() {
        return false;
    }

    public final void c() {
        this.n = true;
        this.j = true;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        setBackgroundDrawable(this.g);
        this.d = (TextView) findViewById(g.title);
        if (this.h != -1) {
            this.d.setTextAppearance(this.i, this.h);
        }
        this.f = (TextView) findViewById(g.shortcut);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.b != null && this.j) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.b.getLayoutParams();
            if (layoutParams.height > 0 && layoutParams2.width <= 0) {
                layoutParams2.width = layoutParams.height;
            }
        }
        super.onMeasure(i2, i3);
    }
}
