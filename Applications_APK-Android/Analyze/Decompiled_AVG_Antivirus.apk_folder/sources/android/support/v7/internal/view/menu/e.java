package android.support.v7.internal.view.menu;

import android.content.Context;
import android.os.Build;
import android.support.v4.c.a.b;
import android.support.v4.c.a.c;
import android.support.v4.e.a;
import android.view.MenuItem;
import android.view.SubMenu;
import java.util.Iterator;
import java.util.Map;

abstract class e extends f {
    final Context a;
    private Map c;
    private Map d;

    e(Context context, Object obj) {
        super(obj);
        this.a = context;
    }

    /* access modifiers changed from: package-private */
    public final MenuItem a(MenuItem menuItem) {
        if (!(menuItem instanceof b)) {
            return menuItem;
        }
        b bVar = (b) menuItem;
        if (this.c == null) {
            this.c = new a();
        }
        MenuItem menuItem2 = (MenuItem) this.c.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        MenuItem a2 = ab.a(this.a, bVar);
        this.c.put(bVar, a2);
        return a2;
    }

    /* access modifiers changed from: package-private */
    public final SubMenu a(SubMenu subMenu) {
        if (!(subMenu instanceof c)) {
            return subMenu;
        }
        c cVar = (c) subMenu;
        if (this.d == null) {
            this.d = new a();
        }
        SubMenu subMenu2 = (SubMenu) this.d.get(cVar);
        if (subMenu2 != null) {
            return subMenu2;
        }
        Context context = this.a;
        if (Build.VERSION.SDK_INT >= 14) {
            ae aeVar = new ae(context, cVar);
            this.d.put(cVar, aeVar);
            return aeVar;
        }
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.c != null) {
            this.c.clear();
        }
        if (this.d != null) {
            this.d.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        if (this.c != null) {
            Iterator it = this.c.keySet().iterator();
            while (it.hasNext()) {
                if (i == ((MenuItem) it.next()).getGroupId()) {
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i) {
        if (this.c != null) {
            Iterator it = this.c.keySet().iterator();
            while (it.hasNext()) {
                if (i == ((MenuItem) it.next()).getItemId()) {
                    it.remove();
                    return;
                }
            }
        }
    }
}
