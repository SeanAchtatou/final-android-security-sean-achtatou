package android.support.v7.internal.view;

import android.content.Context;
import android.support.v7.d.a;
import android.support.v7.d.b;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.widget.ActionBarContextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

public final class c extends a implements j {
    private Context a;
    private ActionBarContextView b;
    private b c;
    private WeakReference d;
    private boolean e;
    private boolean f;
    private i g;

    public c(Context context, ActionBarContextView actionBarContextView, b bVar, boolean z) {
        this.a = context;
        this.b = actionBarContextView;
        this.c = bVar;
        this.g = new i(actionBarContextView.getContext()).a();
        this.g.a(this);
        this.f = z;
    }

    public final MenuInflater a() {
        return new MenuInflater(this.b.getContext());
    }

    public final void a(int i) {
        b(this.a.getString(i));
    }

    public final void a(i iVar) {
        d();
        this.b.a();
    }

    public final void a(View view) {
        this.b.d(view);
        this.d = view != null ? new WeakReference(view) : null;
    }

    public final void a(CharSequence charSequence) {
        this.b.b(charSequence);
    }

    public final void a(boolean z) {
        super.a(z);
        this.b.a(z);
    }

    public final boolean a(i iVar, MenuItem menuItem) {
        return this.c.a(this, menuItem);
    }

    public final Menu b() {
        return this.g;
    }

    public final void b(int i) {
        a((CharSequence) this.a.getString(i));
    }

    public final void b(CharSequence charSequence) {
        this.b.a(charSequence);
    }

    public final void c() {
        if (!this.e) {
            this.e = true;
            this.b.sendAccessibilityEvent(32);
            this.c.a(this);
        }
    }

    public final void d() {
        this.c.b(this, this.g);
    }

    public final CharSequence f() {
        return this.b.b();
    }

    public final CharSequence g() {
        return this.b.c();
    }

    public final boolean h() {
        return this.b.f();
    }

    public final View i() {
        if (this.d != null) {
            return (View) this.d.get();
        }
        return null;
    }
}
