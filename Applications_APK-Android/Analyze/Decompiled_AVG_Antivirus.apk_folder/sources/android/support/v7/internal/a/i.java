package android.support.v7.internal.a;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.bx;
import android.support.v4.view.dv;
import android.support.v4.view.el;
import android.support.v4.view.en;
import android.support.v7.a.g;
import android.support.v7.app.ActionBar;
import android.support.v7.d.a;
import android.support.v7.d.b;
import android.support.v7.internal.widget.ActionBarContainer;
import android.support.v7.internal.widget.ActionBarContextView;
import android.support.v7.internal.widget.ActionBarOverlayLayout;
import android.support.v7.internal.widget.af;
import android.support.v7.internal.widget.am;
import android.support.v7.internal.widget.l;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.animation.AnimationUtils;
import java.util.ArrayList;

public class i extends ActionBar implements l {
    static final /* synthetic */ boolean h = (!i.class.desiredAssertionStatus());
    private static final boolean i;
    private boolean A;
    private int B = 0;
    /* access modifiers changed from: private */
    public boolean C = true;
    /* access modifiers changed from: private */
    public boolean D;
    /* access modifiers changed from: private */
    public boolean E;
    private boolean F;
    private boolean G = true;
    /* access modifiers changed from: private */
    public android.support.v7.internal.view.i H;
    private boolean I;
    m a;
    a b;
    b c;
    boolean d;
    final el e = new j(this);
    final el f = new k(this);
    final en g = new l(this);
    /* access modifiers changed from: private */
    public Context j;
    private Context k;
    private Activity l;
    private Dialog m;
    /* access modifiers changed from: private */
    public ActionBarOverlayLayout n;
    /* access modifiers changed from: private */
    public ActionBarContainer o;
    /* access modifiers changed from: private */
    public af p;
    /* access modifiers changed from: private */
    public ActionBarContextView q;
    /* access modifiers changed from: private */
    public ActionBarContainer r;
    /* access modifiers changed from: private */
    public View s;
    private am t;
    private ArrayList u = new ArrayList();
    private int v = -1;
    private boolean w;
    private boolean x;
    private ArrayList y = new ArrayList();
    /* access modifiers changed from: private */
    public int z;

    static {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT < 14) {
            z2 = false;
        }
        i = z2;
    }

    public i(Activity activity, boolean z2) {
        this.l = activity;
        View decorView = activity.getWindow().getDecorView();
        a(decorView);
        if (!z2) {
            this.s = decorView.findViewById(16908290);
        }
    }

    public i(Dialog dialog) {
        this.m = dialog;
        a(dialog.getWindow().getDecorView());
    }

    private void a(View view) {
        af o2;
        this.n = (ActionBarOverlayLayout) view.findViewById(g.decor_content_parent);
        if (this.n != null) {
            this.n.a(this);
        }
        View findViewById = view.findViewById(g.action_bar);
        if (findViewById instanceof af) {
            o2 = (af) findViewById;
        } else if (findViewById instanceof Toolbar) {
            o2 = ((Toolbar) findViewById).o();
        } else {
            throw new IllegalStateException(new StringBuilder("Can't make a decor toolbar out of ").append(findViewById).toString() != null ? findViewById.getClass().getSimpleName() : "null");
        }
        this.p = o2;
        this.q = (ActionBarContextView) view.findViewById(g.action_context_bar);
        this.o = (ActionBarContainer) view.findViewById(g.action_bar_container);
        this.r = (ActionBarContainer) view.findViewById(g.split_action_bar);
        if (this.p == null || this.q == null || this.o == null) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used with a compatible window decor layout");
        }
        this.j = this.p.b();
        this.z = 0;
        if ((this.p.n() & 4) != 0) {
            this.w = true;
        }
        android.support.v7.internal.view.a a2 = android.support.v7.internal.view.a.a(this.j);
        a2.f();
        g(a2.d());
        TypedArray obtainStyledAttributes = this.j.obtainStyledAttributes(null, android.support.v7.a.l.a, android.support.v7.a.b.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(android.support.v7.a.l.m, false)) {
            if (!this.n.a()) {
                throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
            }
            this.d = true;
            this.n.b(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(android.support.v7.a.l.k, 0);
        if (dimensionPixelSize != 0) {
            float f2 = (float) dimensionPixelSize;
            bx.f(this.o, f2);
            if (this.r != null) {
                bx.f(this.r, f2);
            }
        }
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: private */
    public static boolean a(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        return !z2 && !z3;
    }

    private void g(boolean z2) {
        boolean z3 = true;
        this.A = z2;
        if (!this.A) {
            this.p.a((am) null);
            this.o.a(this.t);
        } else {
            this.o.a((am) null);
            this.p.a(this.t);
        }
        boolean z4 = this.p.o() == 2;
        if (this.t != null) {
            if (z4) {
                this.t.setVisibility(0);
                if (this.n != null) {
                    bx.x(this.n);
                }
            } else {
                this.t.setVisibility(8);
            }
        }
        this.p.a(!this.A && z4);
        ActionBarOverlayLayout actionBarOverlayLayout = this.n;
        if (this.A || !z4) {
            z3 = false;
        }
        actionBarOverlayLayout.a(z3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, float):void
     arg types: [android.support.v7.internal.widget.ActionBarContainer, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, boolean):void
      android.support.v4.view.bx.b(android.view.View, int):boolean
      android.support.v4.view.bx.b(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, float):void
     arg types: [android.support.v7.internal.widget.ActionBarContainer, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, int):void
      android.support.v4.view.bx.c(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.bx.b(android.view.View, android.support.v4.view.eo):android.support.v4.view.eo
      android.support.v4.view.bx.b(android.view.View, boolean):void
      android.support.v4.view.bx.b(android.view.View, int):boolean
      android.support.v4.view.bx.b(android.view.View, float):void */
    private void h(boolean z2) {
        if (a(this.D, this.E, this.F)) {
            if (!this.G) {
                this.G = true;
                if (this.H != null) {
                    this.H.b();
                }
                this.o.setVisibility(0);
                if (this.B != 0 || !i || (!this.I && !z2)) {
                    bx.c((View) this.o, 1.0f);
                    bx.b((View) this.o, 0.0f);
                    if (this.C && this.s != null) {
                        bx.b(this.s, 0.0f);
                    }
                    if (this.r != null && this.z == 1) {
                        bx.c((View) this.r, 1.0f);
                        bx.b((View) this.r, 0.0f);
                        this.r.setVisibility(0);
                    }
                    this.f.b(null);
                } else {
                    bx.b((View) this.o, 0.0f);
                    float f2 = (float) (-this.o.getHeight());
                    if (z2) {
                        int[] iArr = {0, 0};
                        this.o.getLocationInWindow(iArr);
                        f2 -= (float) iArr[1];
                    }
                    bx.b(this.o, f2);
                    android.support.v7.internal.view.i iVar = new android.support.v7.internal.view.i();
                    dv c2 = bx.t(this.o).c(0.0f);
                    c2.a(this.g);
                    iVar.a(c2);
                    if (this.C && this.s != null) {
                        bx.b(this.s, f2);
                        iVar.a(bx.t(this.s).c(0.0f));
                    }
                    if (this.r != null && this.z == 1) {
                        bx.b(this.r, (float) this.r.getHeight());
                        this.r.setVisibility(0);
                        iVar.a(bx.t(this.r).c(0.0f));
                    }
                    iVar.a(AnimationUtils.loadInterpolator(this.j, 17432582));
                    iVar.c();
                    iVar.a(this.f);
                    this.H = iVar;
                    iVar.a();
                }
                if (this.n != null) {
                    bx.x(this.n);
                }
            }
        } else if (this.G) {
            this.G = false;
            if (this.H != null) {
                this.H.b();
            }
            if (this.B != 0 || !i || (!this.I && !z2)) {
                this.e.b(null);
                return;
            }
            bx.c((View) this.o, 1.0f);
            this.o.a(true);
            android.support.v7.internal.view.i iVar2 = new android.support.v7.internal.view.i();
            float f3 = (float) (-this.o.getHeight());
            if (z2) {
                int[] iArr2 = {0, 0};
                this.o.getLocationInWindow(iArr2);
                f3 -= (float) iArr2[1];
            }
            dv c3 = bx.t(this.o).c(f3);
            c3.a(this.g);
            iVar2.a(c3);
            if (this.C && this.s != null) {
                iVar2.a(bx.t(this.s).c(f3));
            }
            if (this.r != null && this.r.getVisibility() == 0) {
                bx.c((View) this.r, 1.0f);
                iVar2.a(bx.t(this.r).c((float) this.r.getHeight()));
            }
            iVar2.a(AnimationUtils.loadInterpolator(this.j, 17432581));
            iVar2.c();
            iVar2.a(this.e);
            this.H = iVar2;
            iVar2.a();
        }
    }

    public final int a() {
        return this.p.n();
    }

    public final a a(b bVar) {
        if (this.a != null) {
            this.a.c();
        }
        this.n.b(false);
        this.q.e();
        m mVar = new m(this, this.q.getContext(), bVar);
        if (!mVar.e()) {
            return null;
        }
        mVar.d();
        this.q.a(mVar);
        f(true);
        if (!(this.r == null || this.z != 1 || this.r.getVisibility() == 0)) {
            this.r.setVisibility(0);
            if (this.n != null) {
                bx.x(this.n);
            }
        }
        this.q.sendAccessibilityEvent(32);
        this.a = mVar;
        return mVar;
    }

    public final void a(int i2) {
        this.B = i2;
    }

    public final void a(Configuration configuration) {
        g(android.support.v7.internal.view.a.a(this.j).d());
    }

    public final void a(CharSequence charSequence) {
        this.p.b(charSequence);
    }

    public final void a(boolean z2) {
        int i2 = z2 ? 4 : 0;
        int n2 = this.p.n();
        this.w = true;
        this.p.a((i2 & 4) | (n2 & -5));
    }

    public final Context b() {
        if (this.k == null) {
            TypedValue typedValue = new TypedValue();
            this.j.getTheme().resolveAttribute(android.support.v7.a.b.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.k = new ContextThemeWrapper(this.j, i2);
            } else {
                this.k = this.j;
            }
        }
        return this.k;
    }

    public final void b(CharSequence charSequence) {
        this.p.a(charSequence);
    }

    public final void b(boolean z2) {
        if (!this.w) {
            a(z2);
        }
    }

    public final void c(boolean z2) {
        this.I = z2;
        if (!z2 && this.H != null) {
            this.H.b();
        }
    }

    public final void d(boolean z2) {
        if (z2 != this.x) {
            this.x = z2;
            int size = this.y.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.y.get(i2);
            }
        }
    }

    public final boolean d() {
        if (this.p == null || !this.p.c()) {
            return false;
        }
        this.p.d();
        return true;
    }

    public final void e() {
        if (this.E) {
            this.E = false;
            h(true);
        }
    }

    public final void e(boolean z2) {
        this.C = z2;
    }

    public final void f() {
        if (!this.E) {
            this.E = true;
            h(true);
        }
    }

    public final void f(boolean z2) {
        int i2 = 0;
        if (z2) {
            if (!this.F) {
                this.F = true;
                if (this.n != null) {
                    ActionBarOverlayLayout.b();
                }
                h(false);
            }
        } else if (this.F) {
            this.F = false;
            if (this.n != null) {
                ActionBarOverlayLayout.b();
            }
            h(false);
        }
        this.p.b(z2 ? 8 : 0);
        ActionBarContextView actionBarContextView = this.q;
        if (!z2) {
            i2 = 8;
        }
        actionBarContextView.b(i2);
    }

    public final void g() {
        if (this.H != null) {
            this.H.b();
            this.H = null;
        }
    }
}
