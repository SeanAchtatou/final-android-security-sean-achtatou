package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.bx;
import android.support.v4.view.dv;
import android.support.v7.a.b;
import android.support.v7.a.l;
import android.support.v7.internal.view.i;
import android.support.v7.widget.ActionMenuPresenter;
import android.support.v7.widget.ActionMenuView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

abstract class a extends ViewGroup {
    private static final Interpolator i = new DecelerateInterpolator();
    protected final b a;
    protected final Context b;
    protected ActionMenuView c;
    protected ActionMenuPresenter d;
    protected ViewGroup e;
    protected boolean f;
    protected int g;
    protected dv h;

    a(Context context) {
        this(context, null);
    }

    a(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    a(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = new b(this);
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(b.actionBarPopupTheme, typedValue, true) || typedValue.resourceId == 0) {
            this.b = context;
        } else {
            this.b = new ContextThemeWrapper(context, typedValue.resourceId);
        }
    }

    protected static int a(int i2, int i3, boolean z) {
        return z ? i2 - i3 : i2 + i3;
    }

    protected static int a(View view, int i2, int i3) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i2, Integer.MIN_VALUE), i3);
        return Math.max(0, (i2 - view.getMeasuredWidth()) + 0);
    }

    protected static int a(View view, int i2, int i3, int i4, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i5 = ((i4 - measuredHeight) / 2) + i3;
        if (z) {
            view.layout(i2 - measuredWidth, i5, i2, measuredHeight + i5);
        } else {
            view.layout(i2, i5, i2 + measuredWidth, measuredHeight + i5);
        }
        return z ? -measuredWidth : measuredWidth;
    }

    public void a(int i2) {
        this.g = i2;
        requestLayout();
    }

    public boolean a() {
        if (this.d != null) {
            return this.d.i();
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, float):void
     arg types: [android.support.v7.internal.widget.a, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, int):void
      android.support.v4.view.bx.c(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.bx.c(android.view.View, float):void
     arg types: [android.support.v7.widget.ActionMenuView, int]
     candidates:
      android.support.v4.view.bx.c(android.view.View, int):void
      android.support.v4.view.bx.c(android.view.View, float):void */
    public void b(int i2) {
        if (this.h != null) {
            this.h.b();
        }
        if (i2 == 0) {
            if (getVisibility() != 0) {
                bx.c((View) this, 0.0f);
                if (!(this.e == null || this.c == null)) {
                    bx.c((View) this.c, 0.0f);
                }
            }
            dv a2 = bx.t(this).a(1.0f);
            a2.a(200L);
            a2.a(i);
            if (this.e == null || this.c == null) {
                a2.a(this.a.a(a2, i2));
                a2.c();
                return;
            }
            i iVar = new i();
            dv a3 = bx.t(this.c).a(1.0f);
            a3.a(200L);
            iVar.a(this.a.a(a2, i2));
            iVar.a(a2).a(a3);
            iVar.a();
            return;
        }
        dv a4 = bx.t(this).a(0.0f);
        a4.a(200L);
        a4.a(i);
        if (this.e == null || this.c == null) {
            a4.a(this.a.a(a4, i2));
            a4.c();
            return;
        }
        i iVar2 = new i();
        dv a5 = bx.t(this.c).a(0.0f);
        a5.a(200L);
        iVar2.a(this.a.a(a4, i2));
        iVar2.a(a4).a(a5);
        iVar2.a();
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, l.a, b.actionBarStyle, 0);
        a(obtainStyledAttributes.getLayoutDimension(l.l, 0));
        obtainStyledAttributes.recycle();
        if (this.d != null) {
            this.d.e();
        }
    }
}
