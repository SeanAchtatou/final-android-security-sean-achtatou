package android.support.v7.internal.view.menu;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

final class h extends BaseAdapter {
    final /* synthetic */ g a;
    private int b = -1;

    public h(g gVar) {
        this.a = gVar;
        a();
    }

    private void a() {
        m q = this.a.c.q();
        if (q != null) {
            ArrayList n = this.a.c.n();
            int size = n.size();
            for (int i = 0; i < size; i++) {
                if (((m) n.get(i)) == q) {
                    this.b = i;
                    return;
                }
            }
        }
        this.b = -1;
    }

    /* renamed from: a */
    public final m getItem(int i) {
        ArrayList n = this.a.c.n();
        int a2 = this.a.h + i;
        if (this.b >= 0 && a2 >= this.b) {
            a2++;
        }
        return (m) n.get(a2);
    }

    public final int getCount() {
        int size = this.a.c.n().size() - this.a.h;
        return this.b < 0 ? size : size - 1;
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.b.inflate(this.a.f, viewGroup, false) : view;
        ((aa) inflate).a(getItem(i));
        return inflate;
    }

    public final void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
