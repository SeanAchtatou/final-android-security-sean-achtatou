package android.support.v7.internal.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.SpinnerAdapter;

abstract class AbsSpinnerCompat extends y {
    private DataSetObserver D;
    SpinnerAdapter a;
    int b;
    int c;
    int d = 0;
    int e = 0;
    int f = 0;
    int g = 0;
    final Rect h = new Rect();
    final c i = new c(this);

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new d();
        long a;
        int b;

        SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readLong();
            this.b = parcel.readInt();
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "AbsSpinner.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " selectedId=" + this.a + " position=" + this.b + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeLong(this.a);
            parcel.writeInt(this.b);
        }
    }

    AbsSpinnerCompat(Context context, int i2) {
        super(context, i2);
        setFocusable(true);
        setWillNotDraw(false);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.t = false;
        this.o = false;
        removeAllViewsInLayout();
        this.A = -1;
        this.B = Long.MIN_VALUE;
        b(-1);
        c(-1);
        invalidate();
    }

    public final void a(int i2) {
        c(i2);
        requestLayout();
        invalidate();
    }

    public void a(SpinnerAdapter spinnerAdapter) {
        int i2 = -1;
        if (this.a != null) {
            this.a.unregisterDataSetObserver(this.D);
            a();
        }
        this.a = spinnerAdapter;
        this.A = -1;
        this.B = Long.MIN_VALUE;
        if (this.a != null) {
            this.z = this.y;
            this.y = this.a.getCount();
            d();
            this.D = new z(this);
            this.a.registerDataSetObserver(this.D);
            if (this.y > 0) {
                i2 = 0;
            }
            b(i2);
            c(i2);
            if (this.y == 0) {
                f();
            }
        } else {
            d();
            a();
            f();
        }
        requestLayout();
    }

    public final View b() {
        if (this.y <= 0 || this.w < 0) {
            return null;
        }
        return getChildAt(this.w - this.j);
    }

    public final /* bridge */ /* synthetic */ Adapter c() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.LayoutParams(-1, -2);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r10, int r11) {
        /*
            r9 = this;
            r6 = 1
            r5 = 0
            int r7 = android.view.View.MeasureSpec.getMode(r10)
            int r1 = r9.getPaddingLeft()
            int r2 = r9.getPaddingTop()
            int r3 = r9.getPaddingRight()
            int r4 = r9.getPaddingBottom()
            android.graphics.Rect r8 = r9.h
            int r0 = r9.d
            if (r1 <= r0) goto L_0x00ce
            r0 = r1
        L_0x001d:
            r8.left = r0
            android.graphics.Rect r8 = r9.h
            int r0 = r9.e
            if (r2 <= r0) goto L_0x00d2
            r0 = r2
        L_0x0026:
            r8.top = r0
            android.graphics.Rect r2 = r9.h
            int r0 = r9.f
            if (r3 <= r0) goto L_0x00d6
            r0 = r3
        L_0x002f:
            r2.right = r0
            android.graphics.Rect r2 = r9.h
            int r0 = r9.g
            if (r4 <= r0) goto L_0x00da
            r0 = r4
        L_0x0038:
            r2.bottom = r0
            boolean r0 = r9.t
            if (r0 == 0) goto L_0x0041
            r9.e()
        L_0x0041:
            int r2 = r9.u
            if (r2 < 0) goto L_0x00de
            android.widget.SpinnerAdapter r0 = r9.a
            if (r0 == 0) goto L_0x00de
            android.widget.SpinnerAdapter r0 = r9.a
            int r0 = r0.getCount()
            if (r2 >= r0) goto L_0x00de
            android.support.v7.internal.widget.c r0 = r9.i
            android.view.View r0 = r0.a(r2)
            if (r0 != 0) goto L_0x0060
            android.widget.SpinnerAdapter r0 = r9.a
            r3 = 0
            android.view.View r0 = r0.getView(r2, r3, r9)
        L_0x0060:
            if (r0 == 0) goto L_0x00de
            android.support.v7.internal.widget.c r1 = r9.i
            r1.a(r2, r0)
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            if (r1 != 0) goto L_0x0078
            r9.C = r6
            android.view.ViewGroup$LayoutParams r1 = r9.generateDefaultLayoutParams()
            r0.setLayoutParams(r1)
            r9.C = r5
        L_0x0078:
            r9.measureChild(r0, r10, r11)
            int r1 = r0.getMeasuredHeight()
            android.graphics.Rect r2 = r9.h
            int r2 = r2.top
            int r1 = r1 + r2
            android.graphics.Rect r2 = r9.h
            int r2 = r2.bottom
            int r1 = r1 + r2
            int r0 = r0.getMeasuredWidth()
            android.graphics.Rect r2 = r9.h
            int r2 = r2.left
            int r0 = r0 + r2
            android.graphics.Rect r2 = r9.h
            int r2 = r2.right
            int r0 = r0 + r2
            r2 = r5
        L_0x0098:
            if (r2 == 0) goto L_0x00ae
            android.graphics.Rect r1 = r9.h
            int r1 = r1.top
            android.graphics.Rect r2 = r9.h
            int r2 = r2.bottom
            int r1 = r1 + r2
            if (r7 != 0) goto L_0x00ae
            android.graphics.Rect r0 = r9.h
            int r0 = r0.left
            android.graphics.Rect r2 = r9.h
            int r2 = r2.right
            int r0 = r0 + r2
        L_0x00ae:
            int r2 = r9.getSuggestedMinimumHeight()
            int r1 = java.lang.Math.max(r1, r2)
            int r2 = r9.getSuggestedMinimumWidth()
            int r0 = java.lang.Math.max(r0, r2)
            int r1 = android.support.v4.view.bx.a(r1, r11, r5)
            int r0 = android.support.v4.view.bx.a(r0, r10, r5)
            r9.setMeasuredDimension(r0, r1)
            r9.b = r11
            r9.c = r10
            return
        L_0x00ce:
            int r0 = r9.d
            goto L_0x001d
        L_0x00d2:
            int r0 = r9.e
            goto L_0x0026
        L_0x00d6:
            int r0 = r9.f
            goto L_0x002f
        L_0x00da:
            int r0 = r9.g
            goto L_0x0038
        L_0x00de:
            r2 = r6
            r0 = r5
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.AbsSpinnerCompat.onMeasure(int, int):void");
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.a >= 0) {
            this.t = true;
            this.o = true;
            this.m = savedState.a;
            this.l = savedState.b;
            this.p = 0;
            requestLayout();
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = this.v;
        if (savedState.a >= 0) {
            savedState.b = this.u;
        } else {
            savedState.b = -1;
        }
        return savedState;
    }

    public void requestLayout() {
        if (!this.C) {
            super.requestLayout();
        }
    }
}
