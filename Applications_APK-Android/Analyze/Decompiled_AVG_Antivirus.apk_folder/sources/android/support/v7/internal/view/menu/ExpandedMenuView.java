package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v7.internal.widget.be;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public final class ExpandedMenuView extends ListView implements k, z, AdapterView.OnItemClickListener {
    private static final int[] a = {16842964, 16843049};
    private i b;

    public ExpandedMenuView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842868);
    }

    public ExpandedMenuView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        setOnItemClickListener(this);
        be a2 = be.a(context, attributeSet, a, i);
        if (a2.f(0)) {
            setBackgroundDrawable(a2.a(0));
        }
        if (a2.f(1)) {
            setDivider(a2.a(1));
        }
        a2.b();
    }

    public final void a(i iVar) {
        this.b = iVar;
    }

    public final boolean a(m mVar) {
        return this.b.b((MenuItem) mVar);
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setChildrenDrawingCacheEnabled(false);
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        a((m) getAdapter().getItem(i));
    }
}
