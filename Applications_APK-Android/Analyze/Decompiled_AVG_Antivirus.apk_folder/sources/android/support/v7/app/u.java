package android.support.v7.app;

import android.support.v4.view.bi;
import android.support.v4.view.bx;
import android.support.v4.view.eo;
import android.view.View;

final class u implements bi {
    final /* synthetic */ AppCompatDelegateImplV7 a;

    u(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
        this.a = appCompatDelegateImplV7;
    }

    public final eo a(View view, eo eoVar) {
        int b = eoVar.b();
        int b2 = AppCompatDelegateImplV7.b(this.a, b);
        if (b != b2) {
            eoVar = eoVar.a(eoVar.a(), b2, eoVar.c(), eoVar.d());
        }
        return bx.a(view, eoVar);
    }
}
