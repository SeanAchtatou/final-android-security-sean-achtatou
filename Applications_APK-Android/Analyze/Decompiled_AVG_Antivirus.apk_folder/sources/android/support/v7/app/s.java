package android.support.v7.app;

import android.support.v7.d.a;
import android.support.v7.internal.view.e;
import android.view.ActionMode;
import android.view.Window;

final class s extends p {
    final /* synthetic */ r b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    s(r rVar, Window.Callback callback) {
        super(rVar, callback);
        this.b = rVar;
    }

    public final ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
        if (!this.b.o) {
            return super.onWindowStartingActionMode(callback);
        }
        e eVar = new e(this.b.a, callback);
        r rVar = this.b;
        if (rVar.k != null) {
            rVar.k.c();
        }
        y yVar = new y(rVar, eVar);
        ActionBar a = rVar.a();
        if (a != null) {
            rVar.k = a.a(yVar);
        }
        if (rVar.k == null) {
            rVar.k = rVar.a(yVar);
        }
        a aVar = rVar.k;
        if (aVar != null) {
            return eVar.b(aVar);
        }
        return null;
    }
}
