package android.support.v7.app;

import android.support.v7.internal.view.k;
import android.support.v7.internal.view.menu.i;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;

class p extends k {
    final /* synthetic */ o a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    p(o oVar, Window.Callback callback) {
        super(callback);
        this.a = oVar;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (this.a.a(keyEvent)) {
            return true;
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        if (this.a.a(keyEvent.getKeyCode(), keyEvent)) {
            return true;
        }
        return super.dispatchKeyShortcutEvent(keyEvent);
    }

    public void onContentChanged() {
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        if (i != 0 || (menu instanceof i)) {
            return super.onCreatePanelMenu(i, menu);
        }
        return false;
    }

    public boolean onMenuOpened(int i, Menu menu) {
        if (this.a.c(i)) {
            return true;
        }
        return super.onMenuOpened(i, menu);
    }

    public void onPanelClosed(int i, Menu menu) {
        if (!this.a.b(i)) {
            super.onPanelClosed(i, menu);
        }
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        i iVar = menu instanceof i ? (i) menu : null;
        if (i == 0 && iVar == null) {
            return false;
        }
        if (iVar != null) {
            iVar.c(true);
        }
        boolean onPreparePanel = super.onPreparePanel(i, view, menu);
        if (iVar == null) {
            return onPreparePanel;
        }
        iVar.c(false);
        return onPreparePanel;
    }
}
