package android.support.v7.app;

import android.support.v7.app.AppCompatDelegateImplV7;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;
import android.view.Menu;
import android.view.Window;

final class ab implements y {
    final /* synthetic */ AppCompatDelegateImplV7 a;

    private ab(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
        this.a = appCompatDelegateImplV7;
    }

    /* synthetic */ ab(AppCompatDelegateImplV7 appCompatDelegateImplV7, byte b) {
        this(appCompatDelegateImplV7);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.view.Menu):android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState
     arg types: [android.support.v7.app.AppCompatDelegateImplV7, android.support.v7.internal.view.menu.i]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, int):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV7.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.o.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.n.a(android.app.Activity, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.app.Dialog, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.view.Menu):android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV7, android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV7.a(int, android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, int, android.view.KeyEvent):boolean
      android.support.v7.app.n.a(android.content.Context, android.view.Window, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void */
    public final void a(i iVar, boolean z) {
        i o = iVar.o();
        boolean z2 = o != iVar;
        AppCompatDelegateImplV7 appCompatDelegateImplV7 = this.a;
        if (z2) {
            iVar = o;
        }
        AppCompatDelegateImplV7.PanelFeatureState a2 = appCompatDelegateImplV7.a((Menu) iVar);
        if (a2 == null) {
            return;
        }
        if (z2) {
            this.a.a(a2.a, a2, o);
            this.a.a(a2, true);
            return;
        }
        this.a.a(a2, z);
    }

    public final boolean a(i iVar) {
        Window.Callback callback;
        if (iVar != null || !this.a.f || (callback = this.a.b.getCallback()) == null || this.a.m()) {
            return true;
        }
        callback.onMenuOpened(8, iVar);
        return true;
    }
}
