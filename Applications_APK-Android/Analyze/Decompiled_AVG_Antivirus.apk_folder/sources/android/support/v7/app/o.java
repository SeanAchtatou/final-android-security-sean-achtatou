package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.a.l;
import android.support.v7.d.a;
import android.support.v7.d.b;
import android.support.v7.internal.a.i;
import android.support.v7.internal.view.f;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.Window;

abstract class o extends n {
    final Context a;
    final Window b;
    final Window.Callback c = this.b.getCallback();
    final Window.Callback d;
    final m e;
    boolean f;
    boolean g;
    boolean h;
    boolean i;
    boolean j;
    private ActionBar k;
    private MenuInflater l;
    private CharSequence m;
    private boolean n;

    o(Context context, Window window, m mVar) {
        this.a = context;
        this.b = window;
        this.e = mVar;
        if (this.c instanceof p) {
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        this.d = a(this.c);
        this.b.setCallback(this.d);
    }

    public final ActionBar a() {
        if (this.f) {
            if (this.k == null) {
                this.k = j();
            }
        } else if (this.k instanceof i) {
            this.k = null;
        }
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public abstract a a(b bVar);

    /* access modifiers changed from: package-private */
    public Window.Callback a(Window.Callback callback) {
        return new p(this, callback);
    }

    public void a(Bundle bundle) {
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(l.bH);
        if (!obtainStyledAttributes.hasValue(l.bL)) {
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
        if (obtainStyledAttributes.getBoolean(l.bL, false)) {
            this.f = true;
        }
        if (obtainStyledAttributes.getBoolean(l.bM, false)) {
            this.g = true;
        }
        if (obtainStyledAttributes.getBoolean(l.bN, false)) {
            this.h = true;
        }
        this.i = obtainStyledAttributes.getBoolean(l.bJ, false);
        this.j = obtainStyledAttributes.getBoolean(l.bU, false);
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: package-private */
    public final void a(ActionBar actionBar) {
        this.k = actionBar;
    }

    public final void a(CharSequence charSequence) {
        this.m = charSequence;
        b(charSequence);
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(int i2, KeyEvent keyEvent);

    /* access modifiers changed from: package-private */
    public abstract boolean a(KeyEvent keyEvent);

    public final MenuInflater b() {
        if (this.l == null) {
            this.l = new f(l());
        }
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public abstract void b(CharSequence charSequence);

    /* access modifiers changed from: package-private */
    public abstract boolean b(int i2);

    /* access modifiers changed from: package-private */
    public abstract boolean c(int i2);

    public final void g() {
        this.n = true;
    }

    /* access modifiers changed from: package-private */
    public abstract ActionBar j();

    /* access modifiers changed from: package-private */
    public final ActionBar k() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public final Context l() {
        Context context = null;
        ActionBar a2 = a();
        if (a2 != null) {
            context = a2.b();
        }
        return context == null ? this.a : context;
    }

    /* access modifiers changed from: package-private */
    public final boolean m() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public final CharSequence n() {
        return this.c instanceof Activity ? ((Activity) this.c).getTitle() : this.m;
    }
}
