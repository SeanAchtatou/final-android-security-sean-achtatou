package android.support.v7.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AppCompatDelegateImplV7;

final class aa implements Parcelable.Creator {
    aa() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return AppCompatDelegateImplV7.PanelFeatureState.SavedState.a(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new AppCompatDelegateImplV7.PanelFeatureState.SavedState[i];
    }
}
