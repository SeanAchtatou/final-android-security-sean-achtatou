package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.ar;
import android.support.v4.view.an;
import android.support.v4.view.bx;
import android.support.v4.view.cp;
import android.support.v7.a.b;
import android.support.v7.a.d;
import android.support.v7.a.l;
import android.support.v7.internal.a.a;
import android.support.v7.internal.view.c;
import android.support.v7.internal.view.menu.g;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.widget.ActionBarContextView;
import android.support.v7.internal.widget.ContentFrameLayout;
import android.support.v7.internal.widget.ViewStubCompat;
import android.support.v7.internal.widget.ae;
import android.support.v7.internal.widget.ah;
import android.support.v7.internal.widget.bj;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

class AppCompatDelegateImplV7 extends o implements an, j {
    private PanelFeatureState A;
    /* access modifiers changed from: private */
    public boolean B;
    /* access modifiers changed from: private */
    public int C;
    private final Runnable D = new t(this);
    private boolean E;
    private Rect F;
    private Rect G;
    private a H;
    android.support.v7.d.a k;
    ActionBarContextView l;
    PopupWindow m;
    Runnable n;
    private ae o;
    private x p;
    private ab q;
    private boolean r;
    private ViewGroup s;
    private ViewGroup t;
    private TextView u;
    private View v;
    private boolean w;
    private boolean x;
    private boolean y;
    private PanelFeatureState[] z;

    final class PanelFeatureState {
        int a;
        int b;
        int c;
        int d;
        int e;
        int f;
        ViewGroup g;
        View h;
        View i;
        i j;
        g k;
        Context l;
        boolean m;
        boolean n;
        boolean o;
        public boolean p;
        boolean q = false;
        boolean r;
        Bundle s;

        class SavedState implements Parcelable {
            public static final Parcelable.Creator CREATOR = new aa();
            int a;
            boolean b;
            Bundle c;

            private SavedState() {
            }

            static /* synthetic */ SavedState a(Parcel parcel) {
                boolean z = true;
                SavedState savedState = new SavedState();
                savedState.a = parcel.readInt();
                if (parcel.readInt() != 1) {
                    z = false;
                }
                savedState.b = z;
                if (savedState.b) {
                    savedState.c = parcel.readBundle();
                }
                return savedState;
            }

            public int describeContents() {
                return 0;
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.a);
                parcel.writeInt(this.b ? 1 : 0);
                if (this.b) {
                    parcel.writeBundle(this.c);
                }
            }
        }

        PanelFeatureState(int i2) {
            this.a = i2;
        }

        /* access modifiers changed from: package-private */
        public final void a(i iVar) {
            if (iVar != this.j) {
                if (this.j != null) {
                    this.j.b(this.k);
                }
                this.j = iVar;
                if (iVar != null && this.k != null) {
                    iVar.a(this.k);
                }
            }
        }
    }

    AppCompatDelegateImplV7(Context context, Window window, m mVar) {
        super(context, window, mVar);
    }

    /* access modifiers changed from: private */
    public PanelFeatureState a(Menu menu) {
        PanelFeatureState[] panelFeatureStateArr = this.z;
        int length = panelFeatureStateArr != null ? panelFeatureStateArr.length : 0;
        for (int i = 0; i < length; i++) {
            PanelFeatureState panelFeatureState = panelFeatureStateArr[i];
            if (panelFeatureState != null && panelFeatureState.j == menu) {
                return panelFeatureState;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void a(int i, PanelFeatureState panelFeatureState, Menu menu) {
        Window.Callback callback;
        if (menu == null) {
            if (panelFeatureState == null && i >= 0 && i < this.z.length) {
                panelFeatureState = this.z[i];
            }
            if (panelFeatureState != null) {
                menu = panelFeatureState.j;
            }
        }
        if ((panelFeatureState == null || panelFeatureState.o) && (callback = this.b.getCallback()) != null) {
            callback.onPanelClosed(i, menu);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.view.Menu):android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, int):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV7.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.o.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.n.a(android.app.Activity, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.app.Dialog, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:104:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00ef  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.support.v7.app.AppCompatDelegateImplV7.PanelFeatureState r11, android.view.KeyEvent r12) {
        /*
            r10 = this;
            r1 = -1
            r2 = -2
            r3 = 0
            r9 = 1
            boolean r0 = r11.o
            if (r0 != 0) goto L_0x000e
            boolean r0 = r10.m()
            if (r0 == 0) goto L_0x000f
        L_0x000e:
            return
        L_0x000f:
            int r0 = r11.a
            if (r0 != 0) goto L_0x0034
            android.content.Context r4 = r10.a
            android.content.res.Resources r0 = r4.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            int r0 = r0.screenLayout
            r0 = r0 & 15
            r5 = 4
            if (r0 != r5) goto L_0x004a
            r0 = r9
        L_0x0025:
            android.content.pm.ApplicationInfo r4 = r4.getApplicationInfo()
            int r4 = r4.targetSdkVersion
            r5 = 11
            if (r4 < r5) goto L_0x004c
            r4 = r9
        L_0x0030:
            if (r0 == 0) goto L_0x0034
            if (r4 != 0) goto L_0x000e
        L_0x0034:
            android.view.Window r0 = r10.b
            android.view.Window$Callback r0 = r0.getCallback()
            if (r0 == 0) goto L_0x004e
            int r4 = r11.a
            android.support.v7.internal.view.menu.i r5 = r11.j
            boolean r0 = r0.onMenuOpened(r4, r5)
            if (r0 != 0) goto L_0x004e
            r10.a(r11, r9)
            goto L_0x000e
        L_0x004a:
            r0 = r3
            goto L_0x0025
        L_0x004c:
            r4 = r3
            goto L_0x0030
        L_0x004e:
            android.content.Context r0 = r10.a
            java.lang.String r4 = "window"
            java.lang.Object r0 = r0.getSystemService(r4)
            r8 = r0
            android.view.WindowManager r8 = (android.view.WindowManager) r8
            if (r8 == 0) goto L_0x000e
            boolean r0 = r10.b(r11, r12)
            if (r0 == 0) goto L_0x000e
            android.view.ViewGroup r0 = r11.g
            if (r0 == 0) goto L_0x0069
            boolean r0 = r11.q
            if (r0 == 0) goto L_0x01c1
        L_0x0069:
            android.view.ViewGroup r0 = r11.g
            if (r0 != 0) goto L_0x0154
            android.content.Context r0 = r10.l()
            android.util.TypedValue r1 = new android.util.TypedValue
            r1.<init>()
            android.content.res.Resources r4 = r0.getResources()
            android.content.res.Resources$Theme r4 = r4.newTheme()
            android.content.res.Resources$Theme r5 = r0.getTheme()
            r4.setTo(r5)
            int r5 = android.support.v7.a.b.actionBarPopupTheme
            r4.resolveAttribute(r5, r1, r9)
            int r5 = r1.resourceId
            if (r5 == 0) goto L_0x0093
            int r5 = r1.resourceId
            r4.applyStyle(r5, r9)
        L_0x0093:
            int r5 = android.support.v7.a.b.panelMenuListTheme
            r4.resolveAttribute(r5, r1, r9)
            int r5 = r1.resourceId
            if (r5 == 0) goto L_0x014d
            int r1 = r1.resourceId
            r4.applyStyle(r1, r9)
        L_0x00a1:
            android.support.v7.internal.view.b r1 = new android.support.v7.internal.view.b
            r1.<init>(r0, r3)
            android.content.res.Resources$Theme r0 = r1.getTheme()
            r0.setTo(r4)
            r11.l = r1
            int[] r0 = android.support.v7.a.l.bH
            android.content.res.TypedArray r0 = r1.obtainStyledAttributes(r0)
            int r1 = android.support.v7.a.l.bK
            int r1 = r0.getResourceId(r1, r3)
            r11.b = r1
            int r1 = android.support.v7.a.l.bI
            int r1 = r0.getResourceId(r1, r3)
            r11.f = r1
            r0.recycle()
            android.support.v7.app.z r0 = new android.support.v7.app.z
            android.content.Context r1 = r11.l
            r0.<init>(r10, r1)
            r11.g = r0
            r0 = 81
            r11.c = r0
            android.view.ViewGroup r0 = r11.g
            if (r0 == 0) goto L_0x000e
        L_0x00d9:
            android.view.View r0 = r11.i
            if (r0 == 0) goto L_0x0167
            android.view.View r0 = r11.i
            r11.h = r0
            r0 = r9
        L_0x00e2:
            if (r0 == 0) goto L_0x000e
            android.view.View r0 = r11.h
            if (r0 == 0) goto L_0x01be
            android.view.View r0 = r11.i
            if (r0 == 0) goto L_0x01af
            r0 = r9
        L_0x00ed:
            if (r0 == 0) goto L_0x000e
            android.view.View r0 = r11.h
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            if (r0 != 0) goto L_0x01d4
            android.view.ViewGroup$LayoutParams r0 = new android.view.ViewGroup$LayoutParams
            r0.<init>(r2, r2)
            r1 = r0
        L_0x00fd:
            int r0 = r11.b
            android.view.ViewGroup r4 = r11.g
            r4.setBackgroundResource(r0)
            android.view.View r0 = r11.h
            android.view.ViewParent r0 = r0.getParent()
            if (r0 == 0) goto L_0x0117
            boolean r4 = r0 instanceof android.view.ViewGroup
            if (r4 == 0) goto L_0x0117
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            android.view.View r4 = r11.h
            r0.removeView(r4)
        L_0x0117:
            android.view.ViewGroup r0 = r11.g
            android.view.View r4 = r11.h
            r0.addView(r4, r1)
            android.view.View r0 = r11.h
            boolean r0 = r0.hasFocus()
            if (r0 != 0) goto L_0x012b
            android.view.View r0 = r11.h
            r0.requestFocus()
        L_0x012b:
            r1 = r2
        L_0x012c:
            r11.n = r3
            android.view.WindowManager$LayoutParams r0 = new android.view.WindowManager$LayoutParams
            int r3 = r11.d
            int r4 = r11.e
            r5 = 1002(0x3ea, float:1.404E-42)
            r6 = 8519680(0x820000, float:1.1938615E-38)
            r7 = -3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            int r1 = r11.c
            r0.gravity = r1
            int r1 = r11.f
            r0.windowAnimations = r1
            android.view.ViewGroup r1 = r11.g
            r8.addView(r1, r0)
            r11.o = r9
            goto L_0x000e
        L_0x014d:
            int r1 = android.support.v7.a.k.Theme_AppCompat_CompactMenu
            r4.applyStyle(r1, r9)
            goto L_0x00a1
        L_0x0154:
            boolean r0 = r11.q
            if (r0 == 0) goto L_0x00d9
            android.view.ViewGroup r0 = r11.g
            int r0 = r0.getChildCount()
            if (r0 <= 0) goto L_0x00d9
            android.view.ViewGroup r0 = r11.g
            r0.removeAllViews()
            goto L_0x00d9
        L_0x0167:
            android.support.v7.internal.view.menu.i r0 = r11.j
            if (r0 == 0) goto L_0x01ac
            android.support.v7.app.ab r0 = r10.q
            if (r0 != 0) goto L_0x0176
            android.support.v7.app.ab r0 = new android.support.v7.app.ab
            r0.<init>(r10, r3)
            r10.q = r0
        L_0x0176:
            android.support.v7.app.ab r0 = r10.q
            android.support.v7.internal.view.menu.i r1 = r11.j
            if (r1 != 0) goto L_0x0188
            r0 = 0
        L_0x017d:
            android.view.View r0 = (android.view.View) r0
            r11.h = r0
            android.view.View r0 = r11.h
            if (r0 == 0) goto L_0x01ac
            r0 = r9
            goto L_0x00e2
        L_0x0188:
            android.support.v7.internal.view.menu.g r1 = r11.k
            if (r1 != 0) goto L_0x01a3
            android.support.v7.internal.view.menu.g r1 = new android.support.v7.internal.view.menu.g
            android.content.Context r4 = r11.l
            int r5 = android.support.v7.a.i.k
            r1.<init>(r4, r5)
            r11.k = r1
            android.support.v7.internal.view.menu.g r1 = r11.k
            r1.a(r0)
            android.support.v7.internal.view.menu.i r0 = r11.j
            android.support.v7.internal.view.menu.g r1 = r11.k
            r0.a(r1)
        L_0x01a3:
            android.support.v7.internal.view.menu.g r0 = r11.k
            android.view.ViewGroup r1 = r11.g
            android.support.v7.internal.view.menu.z r0 = r0.a(r1)
            goto L_0x017d
        L_0x01ac:
            r0 = r3
            goto L_0x00e2
        L_0x01af:
            android.support.v7.internal.view.menu.g r0 = r11.k
            android.widget.ListAdapter r0 = r0.c()
            int r0 = r0.getCount()
            if (r0 <= 0) goto L_0x01be
            r0 = r9
            goto L_0x00ed
        L_0x01be:
            r0 = r3
            goto L_0x00ed
        L_0x01c1:
            android.view.View r0 = r11.i
            if (r0 == 0) goto L_0x01d1
            android.view.View r0 = r11.i
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            if (r0 == 0) goto L_0x01d1
            int r0 = r0.width
            if (r0 == r1) goto L_0x012c
        L_0x01d1:
            r1 = r2
            goto L_0x012c
        L_0x01d4:
            r1 = r0
            goto L_0x00fd
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, android.view.KeyEvent):void");
    }

    /* access modifiers changed from: private */
    public void a(PanelFeatureState panelFeatureState, boolean z2) {
        if (!z2 || panelFeatureState.a != 0 || this.o == null || !this.o.d()) {
            boolean z3 = panelFeatureState.o;
            WindowManager windowManager = (WindowManager) this.a.getSystemService("window");
            if (!(windowManager == null || !z3 || panelFeatureState.g == null)) {
                windowManager.removeView(panelFeatureState.g);
            }
            panelFeatureState.m = false;
            panelFeatureState.n = false;
            panelFeatureState.o = false;
            if (z3 && z2) {
                a(panelFeatureState.a, panelFeatureState, (Menu) null);
            }
            panelFeatureState.h = null;
            panelFeatureState.q = true;
            if (this.A == panelFeatureState) {
                this.A = null;
                return;
            }
            return;
        }
        b(panelFeatureState.j);
    }

    static /* synthetic */ void a(AppCompatDelegateImplV7 appCompatDelegateImplV7, int i) {
        PanelFeatureState d;
        PanelFeatureState d2 = appCompatDelegateImplV7.d(i);
        if (d2.j != null) {
            Bundle bundle = new Bundle();
            d2.j.c(bundle);
            if (bundle.size() > 0) {
                d2.s = bundle;
            }
            d2.j.g();
            d2.j.clear();
        }
        d2.r = true;
        d2.q = true;
        if ((i == 8 || i == 0) && appCompatDelegateImplV7.o != null && (d = appCompatDelegateImplV7.d(0)) != null) {
            d.m = false;
            appCompatDelegateImplV7.b(d, (KeyEvent) null);
        }
    }

    private boolean a(PanelFeatureState panelFeatureState, int i, KeyEvent keyEvent) {
        if (keyEvent.isSystem()) {
            return false;
        }
        if ((panelFeatureState.m || b(panelFeatureState, keyEvent)) && panelFeatureState.j != null) {
            return panelFeatureState.j.performShortcut(i, keyEvent, 1);
        }
        return false;
    }

    static /* synthetic */ int b(AppCompatDelegateImplV7 appCompatDelegateImplV7, int i) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = true;
        int i2 = 0;
        if (appCompatDelegateImplV7.l == null || !(appCompatDelegateImplV7.l.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) appCompatDelegateImplV7.l.getLayoutParams();
            if (appCompatDelegateImplV7.l.isShown()) {
                if (appCompatDelegateImplV7.F == null) {
                    appCompatDelegateImplV7.F = new Rect();
                    appCompatDelegateImplV7.G = new Rect();
                }
                Rect rect = appCompatDelegateImplV7.F;
                Rect rect2 = appCompatDelegateImplV7.G;
                rect.set(0, i, 0, 0);
                bj.a(appCompatDelegateImplV7.t, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i : 0)) {
                    marginLayoutParams.topMargin = i;
                    if (appCompatDelegateImplV7.v == null) {
                        appCompatDelegateImplV7.v = new View(appCompatDelegateImplV7.a);
                        appCompatDelegateImplV7.v.setBackgroundColor(appCompatDelegateImplV7.a.getResources().getColor(d.abc_input_method_navigation_guard));
                        appCompatDelegateImplV7.t.addView(appCompatDelegateImplV7.v, -1, new ViewGroup.LayoutParams(-1, i));
                        z4 = true;
                    } else {
                        ViewGroup.LayoutParams layoutParams = appCompatDelegateImplV7.v.getLayoutParams();
                        if (layoutParams.height != i) {
                            layoutParams.height = i;
                            appCompatDelegateImplV7.v.setLayoutParams(layoutParams);
                        }
                        z4 = true;
                    }
                } else {
                    z4 = false;
                }
                if (appCompatDelegateImplV7.v == null) {
                    z5 = false;
                }
                if (!appCompatDelegateImplV7.h && z5) {
                    i = 0;
                }
                boolean z6 = z4;
                z3 = z5;
                z5 = z6;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                z3 = false;
            } else {
                z5 = false;
                z3 = false;
            }
            if (z5) {
                appCompatDelegateImplV7.l.setLayoutParams(marginLayoutParams);
            }
            z2 = z3;
        }
        if (appCompatDelegateImplV7.v != null) {
            View view = appCompatDelegateImplV7.v;
            if (!z2) {
                i2 = 8;
            }
            view.setVisibility(i2);
        }
        return i;
    }

    /* access modifiers changed from: private */
    public void b(i iVar) {
        if (!this.y) {
            this.y = true;
            this.o.i();
            Window.Callback callback = this.b.getCallback();
            if (callback != null && !m()) {
                callback.onPanelClosed(8, iVar);
            }
            this.y = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.view.Menu):android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, int):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV7.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.o.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.n.a(android.app.Activity, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.app.Dialog, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:91:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(android.support.v7.app.AppCompatDelegateImplV7.PanelFeatureState r11, android.view.KeyEvent r12) {
        /*
            r10 = this;
            r5 = 8
            r1 = 0
            r3 = 1
            r4 = 0
            boolean r0 = r10.m()
            if (r0 == 0) goto L_0x000c
        L_0x000b:
            return r4
        L_0x000c:
            boolean r0 = r11.m
            if (r0 == 0) goto L_0x0012
            r4 = r3
            goto L_0x000b
        L_0x0012:
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState r0 = r10.A
            if (r0 == 0) goto L_0x001f
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState r0 = r10.A
            if (r0 == r11) goto L_0x001f
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState r0 = r10.A
            r10.a(r0, r4)
        L_0x001f:
            android.view.Window r0 = r10.b
            android.view.Window$Callback r7 = r0.getCallback()
            if (r7 == 0) goto L_0x002f
            int r0 = r11.a
            android.view.View r0 = r7.onCreatePanelView(r0)
            r11.i = r0
        L_0x002f:
            int r0 = r11.a
            if (r0 == 0) goto L_0x0037
            int r0 = r11.a
            if (r0 != r5) goto L_0x0101
        L_0x0037:
            r6 = r3
        L_0x0038:
            if (r6 == 0) goto L_0x0043
            android.support.v7.internal.widget.ae r0 = r10.o
            if (r0 == 0) goto L_0x0043
            android.support.v7.internal.widget.ae r0 = r10.o
            r0.h()
        L_0x0043:
            android.view.View r0 = r11.i
            if (r0 != 0) goto L_0x015c
            if (r6 == 0) goto L_0x0051
            android.support.v7.app.ActionBar r0 = r10.k()
            boolean r0 = r0 instanceof android.support.v7.internal.a.b
            if (r0 != 0) goto L_0x015c
        L_0x0051:
            android.support.v7.internal.view.menu.i r0 = r11.j
            if (r0 == 0) goto L_0x0059
            boolean r0 = r11.r
            if (r0 == 0) goto L_0x010d
        L_0x0059:
            android.support.v7.internal.view.menu.i r0 = r11.j
            if (r0 != 0) goto L_0x00c6
            android.content.Context r2 = r10.a
            int r0 = r11.a
            if (r0 == 0) goto L_0x0067
            int r0 = r11.a
            if (r0 != r5) goto L_0x0169
        L_0x0067:
            android.support.v7.internal.widget.ae r0 = r10.o
            if (r0 == 0) goto L_0x0169
            android.util.TypedValue r5 = new android.util.TypedValue
            r5.<init>()
            android.content.res.Resources$Theme r8 = r2.getTheme()
            int r0 = android.support.v7.a.b.actionBarTheme
            r8.resolveAttribute(r0, r5, r3)
            int r0 = r5.resourceId
            if (r0 == 0) goto L_0x0104
            android.content.res.Resources r0 = r2.getResources()
            android.content.res.Resources$Theme r0 = r0.newTheme()
            r0.setTo(r8)
            int r9 = r5.resourceId
            r0.applyStyle(r9, r3)
            int r9 = android.support.v7.a.b.actionBarWidgetTheme
            r0.resolveAttribute(r9, r5, r3)
        L_0x0092:
            int r9 = r5.resourceId
            if (r9 == 0) goto L_0x00a8
            if (r0 != 0) goto L_0x00a3
            android.content.res.Resources r0 = r2.getResources()
            android.content.res.Resources$Theme r0 = r0.newTheme()
            r0.setTo(r8)
        L_0x00a3:
            int r5 = r5.resourceId
            r0.applyStyle(r5, r3)
        L_0x00a8:
            r5 = r0
            if (r5 == 0) goto L_0x0169
            android.support.v7.internal.view.b r0 = new android.support.v7.internal.view.b
            r0.<init>(r2, r4)
            android.content.res.Resources$Theme r2 = r0.getTheme()
            r2.setTo(r5)
        L_0x00b7:
            android.support.v7.internal.view.menu.i r2 = new android.support.v7.internal.view.menu.i
            r2.<init>(r0)
            r2.a(r10)
            r11.a(r2)
            android.support.v7.internal.view.menu.i r0 = r11.j
            if (r0 == 0) goto L_0x000b
        L_0x00c6:
            if (r6 == 0) goto L_0x00e0
            android.support.v7.internal.widget.ae r0 = r10.o
            if (r0 == 0) goto L_0x00e0
            android.support.v7.app.x r0 = r10.p
            if (r0 != 0) goto L_0x00d7
            android.support.v7.app.x r0 = new android.support.v7.app.x
            r0.<init>(r10, r4)
            r10.p = r0
        L_0x00d7:
            android.support.v7.internal.widget.ae r0 = r10.o
            android.support.v7.internal.view.menu.i r2 = r11.j
            android.support.v7.app.x r5 = r10.p
            r0.a(r2, r5)
        L_0x00e0:
            android.support.v7.internal.view.menu.i r0 = r11.j
            r0.g()
            int r0 = r11.a
            android.support.v7.internal.view.menu.i r2 = r11.j
            boolean r0 = r7.onCreatePanelMenu(r0, r2)
            if (r0 != 0) goto L_0x010b
            r11.a(r1)
            if (r6 == 0) goto L_0x000b
            android.support.v7.internal.widget.ae r0 = r10.o
            if (r0 == 0) goto L_0x000b
            android.support.v7.internal.widget.ae r0 = r10.o
            android.support.v7.app.x r2 = r10.p
            r0.a(r1, r2)
            goto L_0x000b
        L_0x0101:
            r6 = r4
            goto L_0x0038
        L_0x0104:
            int r0 = android.support.v7.a.b.actionBarWidgetTheme
            r8.resolveAttribute(r0, r5, r3)
            r0 = r1
            goto L_0x0092
        L_0x010b:
            r11.r = r4
        L_0x010d:
            android.support.v7.internal.view.menu.i r0 = r11.j
            r0.g()
            android.os.Bundle r0 = r11.s
            if (r0 == 0) goto L_0x011f
            android.support.v7.internal.view.menu.i r0 = r11.j
            android.os.Bundle r2 = r11.s
            r0.d(r2)
            r11.s = r1
        L_0x011f:
            android.view.View r0 = r11.i
            android.support.v7.internal.view.menu.i r2 = r11.j
            boolean r0 = r7.onPreparePanel(r4, r0, r2)
            if (r0 != 0) goto L_0x013d
            if (r6 == 0) goto L_0x0136
            android.support.v7.internal.widget.ae r0 = r10.o
            if (r0 == 0) goto L_0x0136
            android.support.v7.internal.widget.ae r0 = r10.o
            android.support.v7.app.x r2 = r10.p
            r0.a(r1, r2)
        L_0x0136:
            android.support.v7.internal.view.menu.i r0 = r11.j
            r0.h()
            goto L_0x000b
        L_0x013d:
            if (r12 == 0) goto L_0x0165
            int r0 = r12.getDeviceId()
        L_0x0143:
            android.view.KeyCharacterMap r0 = android.view.KeyCharacterMap.load(r0)
            int r0 = r0.getKeyboardType()
            if (r0 == r3) goto L_0x0167
            r0 = r3
        L_0x014e:
            r11.p = r0
            android.support.v7.internal.view.menu.i r0 = r11.j
            boolean r1 = r11.p
            r0.setQwertyMode(r1)
            android.support.v7.internal.view.menu.i r0 = r11.j
            r0.h()
        L_0x015c:
            r11.m = r3
            r11.n = r4
            r10.A = r11
            r4 = r3
            goto L_0x000b
        L_0x0165:
            r0 = -1
            goto L_0x0143
        L_0x0167:
            r0 = r4
            goto L_0x014e
        L_0x0169:
            r0 = r2
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AppCompatDelegateImplV7.b(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, android.view.KeyEvent):boolean");
    }

    private PanelFeatureState d(int i) {
        PanelFeatureState[] panelFeatureStateArr = this.z;
        if (panelFeatureStateArr == null || panelFeatureStateArr.length <= i) {
            PanelFeatureState[] panelFeatureStateArr2 = new PanelFeatureState[(i + 1)];
            if (panelFeatureStateArr != null) {
                System.arraycopy(panelFeatureStateArr, 0, panelFeatureStateArr2, 0, panelFeatureStateArr.length);
            }
            this.z = panelFeatureStateArr2;
            panelFeatureStateArr = panelFeatureStateArr2;
        }
        PanelFeatureState panelFeatureState = panelFeatureStateArr[i];
        if (panelFeatureState != null) {
            return panelFeatureState;
        }
        PanelFeatureState panelFeatureState2 = new PanelFeatureState(i);
        panelFeatureStateArr[i] = panelFeatureState2;
        return panelFeatureState2;
    }

    private void e(int i) {
        this.C |= 1 << i;
        if (!this.B && this.s != null) {
            bx.a(this.s, this.D);
            this.B = true;
        }
    }

    private void o() {
        if (!this.r) {
            LayoutInflater from = LayoutInflater.from(this.a);
            if (this.j) {
                if (this.h) {
                    this.t = (ViewGroup) from.inflate(android.support.v7.a.i.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
                } else {
                    this.t = (ViewGroup) from.inflate(android.support.v7.a.i.abc_screen_simple, (ViewGroup) null);
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    bx.a(this.t, new u(this));
                } else {
                    ((ah) this.t).a(new v(this));
                }
            } else if (this.i) {
                this.t = (ViewGroup) from.inflate(android.support.v7.a.i.g, (ViewGroup) null);
                this.g = false;
                this.f = false;
            } else if (this.f) {
                TypedValue typedValue = new TypedValue();
                this.a.getTheme().resolveAttribute(b.actionBarTheme, typedValue, true);
                this.t = (ViewGroup) LayoutInflater.from(typedValue.resourceId != 0 ? new android.support.v7.internal.view.b(this.a, typedValue.resourceId) : this.a).inflate(android.support.v7.a.i.abc_screen_toolbar, (ViewGroup) null);
                this.o = (ae) this.t.findViewById(android.support.v7.a.g.decor_content_parent);
                this.o.a(this.b.getCallback());
                if (this.g) {
                    this.o.a(9);
                }
                if (this.w) {
                    this.o.a(2);
                }
                if (this.x) {
                    this.o.a(5);
                }
            }
            if (this.t == null) {
                throw new IllegalArgumentException("AppCompat does not support the current theme features");
            }
            if (this.o == null) {
                this.u = (TextView) this.t.findViewById(android.support.v7.a.g.title);
            }
            bj.b(this.t);
            ViewGroup viewGroup = (ViewGroup) this.b.findViewById(16908290);
            ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.t.findViewById(android.support.v7.a.g.action_bar_activity_content);
            while (viewGroup.getChildCount() > 0) {
                View childAt = viewGroup.getChildAt(0);
                viewGroup.removeViewAt(0);
                contentFrameLayout.addView(childAt);
            }
            this.b.setContentView(this.t);
            viewGroup.setId(-1);
            contentFrameLayout.setId(16908290);
            if (viewGroup instanceof FrameLayout) {
                ((FrameLayout) viewGroup).setForeground(null);
            }
            CharSequence n2 = n();
            if (!TextUtils.isEmpty(n2)) {
                b(n2);
            }
            contentFrameLayout.a(this.s.getPaddingLeft(), this.s.getPaddingTop(), this.s.getPaddingRight(), this.s.getPaddingBottom());
            TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(l.bH);
            obtainStyledAttributes.getValue(l.bS, contentFrameLayout.a());
            obtainStyledAttributes.getValue(l.bT, contentFrameLayout.b());
            if (obtainStyledAttributes.hasValue(l.bQ)) {
                obtainStyledAttributes.getValue(l.bQ, contentFrameLayout.c());
            }
            if (obtainStyledAttributes.hasValue(l.bR)) {
                obtainStyledAttributes.getValue(l.bR, contentFrameLayout.d());
            }
            if (obtainStyledAttributes.hasValue(l.bO)) {
                obtainStyledAttributes.getValue(l.bO, contentFrameLayout.e());
            }
            if (obtainStyledAttributes.hasValue(l.bP)) {
                obtainStyledAttributes.getValue(l.bP, contentFrameLayout.f());
            }
            obtainStyledAttributes.recycle();
            contentFrameLayout.requestLayout();
            this.r = true;
            PanelFeatureState d = d(0);
            if (m()) {
                return;
            }
            if (d == null || d.j == null) {
                e(8);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final android.support.v7.d.a a(android.support.v7.d.b bVar) {
        Context context;
        if (this.k != null) {
            this.k.c();
        }
        y yVar = new y(this, bVar);
        if (this.l == null) {
            if (this.i) {
                TypedValue typedValue = new TypedValue();
                Resources.Theme theme = this.a.getTheme();
                theme.resolveAttribute(b.actionBarTheme, typedValue, true);
                if (typedValue.resourceId != 0) {
                    Resources.Theme newTheme = this.a.getResources().newTheme();
                    newTheme.setTo(theme);
                    newTheme.applyStyle(typedValue.resourceId, true);
                    context = new android.support.v7.internal.view.b(this.a, 0);
                    context.getTheme().setTo(newTheme);
                } else {
                    context = this.a;
                }
                this.l = new ActionBarContextView(context);
                this.m = new PopupWindow(context, (AttributeSet) null, b.actionModePopupWindowStyle);
                this.m.setContentView(this.l);
                this.m.setWidth(-1);
                context.getTheme().resolveAttribute(b.actionBarSize, typedValue, true);
                this.l.a(TypedValue.complexToDimensionPixelSize(typedValue.data, context.getResources().getDisplayMetrics()));
                this.m.setHeight(-2);
                this.n = new w(this);
            } else {
                ViewStubCompat viewStubCompat = (ViewStubCompat) this.t.findViewById(android.support.v7.a.g.action_mode_bar_stub);
                if (viewStubCompat != null) {
                    viewStubCompat.a(LayoutInflater.from(l()));
                    this.l = (ActionBarContextView) viewStubCompat.a();
                }
            }
        }
        if (this.l != null) {
            this.l.e();
            c cVar = new c(this.l.getContext(), this.l, yVar, this.m == null);
            if (bVar.a(cVar, cVar.b())) {
                cVar.d();
                this.l.a(cVar);
                this.l.setVisibility(0);
                this.k = cVar;
                if (this.m != null) {
                    this.b.getDecorView().post(this.n);
                }
                this.l.sendAccessibilityEvent(32);
                if (this.l.getParent() != null) {
                    bx.x((View) this.l.getParent());
                }
            } else {
                this.k = null;
            }
        }
        return this.k;
    }

    public final View a(View view, String str, Context context, AttributeSet attributeSet) {
        View b = b(view, str, context, attributeSet);
        if (b != null) {
            return b;
        }
        boolean z2 = Build.VERSION.SDK_INT < 21;
        if (this.H == null) {
            this.H = new a();
        }
        return this.H.a(view, str, context, attributeSet, z2 && this.r && view != null && view.getId() != 16908290 && !bx.H(view), z2);
    }

    public final void a(int i) {
        o();
        ViewGroup viewGroup = (ViewGroup) this.t.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.a).inflate(i, viewGroup);
        this.c.onContentChanged();
    }

    public final void a(Configuration configuration) {
        ActionBar a;
        if (this.f && this.r && (a = a()) != null) {
            a.a(configuration);
        }
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        this.s = (ViewGroup) this.b.getDecorView();
        if ((this.c instanceof Activity) && ar.b((Activity) this.c) != null) {
            ActionBar k2 = k();
            if (k2 == null) {
                this.E = true;
            } else {
                k2.b(true);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.view.Menu):android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, int):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV7.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.o.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.n.a(android.app.Activity, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.app.Dialog, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void */
    public final void a(i iVar) {
        if (this.o == null || !this.o.c() || (cp.b(ViewConfiguration.get(this.a)) && !this.o.e())) {
            PanelFeatureState d = d(0);
            d.q = true;
            a(d, false);
            a(d, (KeyEvent) null);
            return;
        }
        Window.Callback callback = this.b.getCallback();
        if (this.o.d()) {
            this.o.g();
            if (!m()) {
                callback.onPanelClosed(8, d(0).j);
            }
        } else if (callback != null && !m()) {
            if (this.B && (this.C & 1) != 0) {
                this.s.removeCallbacks(this.D);
                this.D.run();
            }
            PanelFeatureState d2 = d(0);
            if (d2.j != null && !d2.r && callback.onPreparePanel(0, d2.i, d2.j)) {
                callback.onMenuOpened(8, d2.j);
                this.o.f();
            }
        }
    }

    public final void a(Toolbar toolbar) {
        if (this.c instanceof Activity) {
            if (a() instanceof android.support.v7.internal.a.i) {
                throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
            }
            android.support.v7.internal.a.b bVar = new android.support.v7.internal.a.b(toolbar, ((Activity) this.a).getTitle(), this.d);
            a(bVar);
            this.b.setCallback(bVar.e());
            bVar.c();
        }
    }

    public final void a(View view) {
        o();
        ViewGroup viewGroup = (ViewGroup) this.t.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.c.onContentChanged();
    }

    public final void a(View view, ViewGroup.LayoutParams layoutParams) {
        o();
        ViewGroup viewGroup = (ViewGroup) this.t.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.c.onContentChanged();
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i, KeyEvent keyEvent) {
        ActionBar a = a();
        if (a != null && a.a(i, keyEvent)) {
            return true;
        }
        if (this.A == null || !a(this.A, keyEvent.getKeyCode(), keyEvent)) {
            if (this.A == null) {
                PanelFeatureState d = d(0);
                b(d, keyEvent);
                boolean a2 = a(d, keyEvent.getKeyCode(), keyEvent);
                d.m = false;
                if (a2) {
                    return true;
                }
            }
            return false;
        } else if (this.A == null) {
            return true;
        } else {
            this.A.n = true;
            return true;
        }
    }

    public final boolean a(i iVar, MenuItem menuItem) {
        PanelFeatureState a;
        Window.Callback callback = this.b.getCallback();
        if (callback == null || m() || (a = a((Menu) iVar.o())) == null) {
            return false;
        }
        return callback.onMenuItemSelected(a.a, menuItem);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.view.Menu):android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, int):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV7.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.o.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.n.a(android.app.Activity, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.app.Dialog, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0033  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.view.KeyEvent r6) {
        /*
            r5 = this;
            r1 = 1
            r2 = 0
            int r3 = r6.getKeyCode()
            int r0 = r6.getAction()
            if (r0 != 0) goto L_0x001d
            r0 = r1
        L_0x000d:
            if (r0 == 0) goto L_0x0039
            switch(r3) {
                case 82: goto L_0x001f;
                default: goto L_0x0012;
            }
        L_0x0012:
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 11
            if (r0 >= r1) goto L_0x0037
            boolean r0 = r5.a(r3, r6)
        L_0x001c:
            return r0
        L_0x001d:
            r0 = r2
            goto L_0x000d
        L_0x001f:
            int r0 = r6.getRepeatCount()
            if (r0 != 0) goto L_0x0035
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState r0 = r5.d(r2)
            boolean r4 = r0.o
            if (r4 != 0) goto L_0x0035
            boolean r0 = r5.b(r0, r6)
        L_0x0031:
            if (r0 == 0) goto L_0x0012
            r0 = r1
            goto L_0x001c
        L_0x0035:
            r0 = r2
            goto L_0x0031
        L_0x0037:
            r0 = r2
            goto L_0x001c
        L_0x0039:
            switch(r3) {
                case 4: goto L_0x00c2;
                case 82: goto L_0x003e;
                default: goto L_0x003c;
            }
        L_0x003c:
            r0 = r2
            goto L_0x001c
        L_0x003e:
            android.support.v7.d.a r0 = r5.k
            if (r0 == 0) goto L_0x0047
            r0 = r2
        L_0x0043:
            if (r0 == 0) goto L_0x003c
            r0 = r1
            goto L_0x001c
        L_0x0047:
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState r4 = r5.d(r2)
            android.support.v7.internal.widget.ae r0 = r5.o
            if (r0 == 0) goto L_0x0097
            android.support.v7.internal.widget.ae r0 = r5.o
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x0097
            android.content.Context r0 = r5.a
            android.view.ViewConfiguration r0 = android.view.ViewConfiguration.get(r0)
            boolean r0 = android.support.v4.view.cp.b(r0)
            if (r0 != 0) goto L_0x0097
            android.support.v7.internal.widget.ae r0 = r5.o
            boolean r0 = r0.d()
            if (r0 != 0) goto L_0x0090
            boolean r0 = r5.m()
            if (r0 != 0) goto L_0x00f1
            boolean r0 = r5.b(r4, r6)
            if (r0 == 0) goto L_0x00f1
            android.support.v7.internal.widget.ae r0 = r5.o
            boolean r3 = r0.f()
        L_0x007d:
            if (r3 == 0) goto L_0x008e
            android.content.Context r0 = r5.a
            java.lang.String r4 = "audio"
            java.lang.Object r0 = r0.getSystemService(r4)
            android.media.AudioManager r0 = (android.media.AudioManager) r0
            if (r0 == 0) goto L_0x00ba
            r0.playSoundEffect(r2)
        L_0x008e:
            r0 = r3
            goto L_0x0043
        L_0x0090:
            android.support.v7.internal.widget.ae r0 = r5.o
            boolean r3 = r0.g()
            goto L_0x007d
        L_0x0097:
            boolean r0 = r4.o
            if (r0 != 0) goto L_0x009f
            boolean r0 = r4.n
            if (r0 == 0) goto L_0x00a5
        L_0x009f:
            boolean r3 = r4.o
            r5.a(r4, r1)
            goto L_0x007d
        L_0x00a5:
            boolean r0 = r4.m
            if (r0 == 0) goto L_0x00f1
            boolean r0 = r4.r
            if (r0 == 0) goto L_0x00f3
            r4.m = r2
            boolean r0 = r5.b(r4, r6)
        L_0x00b3:
            if (r0 == 0) goto L_0x00f1
            r5.a(r4, r6)
            r3 = r1
            goto L_0x007d
        L_0x00ba:
            java.lang.String r0 = "AppCompatDelegate"
            java.lang.String r4 = "Couldn't get audio manager"
            android.util.Log.w(r0, r4)
            goto L_0x008e
        L_0x00c2:
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState r0 = r5.d(r2)
            if (r0 == 0) goto L_0x00d2
            boolean r3 = r0.o
            if (r3 == 0) goto L_0x00d2
            r5.a(r0, r1)
            r0 = r1
            goto L_0x001c
        L_0x00d2:
            android.support.v7.d.a r0 = r5.k
            if (r0 == 0) goto L_0x00e1
            android.support.v7.d.a r0 = r5.k
            r0.c()
            r0 = r1
        L_0x00dc:
            if (r0 == 0) goto L_0x003c
            r0 = r1
            goto L_0x001c
        L_0x00e1:
            android.support.v7.app.ActionBar r0 = r5.a()
            if (r0 == 0) goto L_0x00ef
            boolean r0 = r0.d()
            if (r0 == 0) goto L_0x00ef
            r0 = r1
            goto L_0x00dc
        L_0x00ef:
            r0 = r2
            goto L_0x00dc
        L_0x00f1:
            r3 = r2
            goto L_0x007d
        L_0x00f3:
            r0 = r1
            goto L_0x00b3
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AppCompatDelegateImplV7.a(android.view.KeyEvent):boolean");
    }

    /* access modifiers changed from: package-private */
    public View b(View view, String str, Context context, AttributeSet attributeSet) {
        View onCreateView;
        if (!(this.c instanceof LayoutInflater.Factory) || (onCreateView = ((LayoutInflater.Factory) this.c).onCreateView(str, context, attributeSet)) == null) {
            return null;
        }
        return onCreateView;
    }

    public final void b(View view, ViewGroup.LayoutParams layoutParams) {
        o();
        ((ViewGroup) this.t.findViewById(16908290)).addView(view, layoutParams);
        this.c.onContentChanged();
    }

    /* access modifiers changed from: package-private */
    public final void b(CharSequence charSequence) {
        if (this.o != null) {
            this.o.a(charSequence);
        } else if (k() != null) {
            k().b(charSequence);
        } else if (this.u != null) {
            this.u.setText(charSequence);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.view.Menu):android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, int):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.AppCompatDelegateImplV7.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV7.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.o.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.n.a(android.app.Activity, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.app.Dialog, android.support.v7.app.m):android.support.v7.app.n
      android.support.v7.app.n.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.AppCompatDelegateImplV7.a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState, boolean):void */
    /* access modifiers changed from: package-private */
    public final boolean b(int i) {
        if (i == 8) {
            ActionBar a = a();
            if (a != null) {
                a.d(false);
            }
            return true;
        } else if (i != 0) {
            return false;
        } else {
            PanelFeatureState d = d(i);
            if (!d.o) {
                return false;
            }
            a(d, false);
            return false;
        }
    }

    public final void c() {
        o();
    }

    /* access modifiers changed from: package-private */
    public final boolean c(int i) {
        if (i != 8) {
            return false;
        }
        ActionBar a = a();
        if (a == null) {
            return true;
        }
        a.d(true);
        return true;
    }

    public final void d() {
        ActionBar a = a();
        if (a != null) {
            a.c(false);
        }
    }

    public final void e() {
        ActionBar a = a();
        if (a != null) {
            a.c(true);
        }
    }

    public final void f() {
        ActionBar a = a();
        if (a == null || !a.c()) {
            e(0);
        }
    }

    public final boolean h() {
        if (this.r) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
        this.j = true;
        return true;
    }

    public final void i() {
        LayoutInflater from = LayoutInflater.from(this.a);
        if (from.getFactory() == null) {
            android.support.v4.view.ae.a(from, this);
        } else {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    public final ActionBar j() {
        o();
        android.support.v7.internal.a.i iVar = null;
        if (this.c instanceof Activity) {
            iVar = new android.support.v7.internal.a.i((Activity) this.c, this.g);
        } else if (this.c instanceof Dialog) {
            iVar = new android.support.v7.internal.a.i((Dialog) this.c);
        }
        if (iVar != null) {
            iVar.b(this.E);
        }
        return iVar;
    }
}
