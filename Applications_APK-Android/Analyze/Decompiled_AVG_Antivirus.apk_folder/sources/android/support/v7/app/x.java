package android.support.v7.app;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;
import android.view.Window;

final class x implements y {
    final /* synthetic */ AppCompatDelegateImplV7 a;

    private x(AppCompatDelegateImplV7 appCompatDelegateImplV7) {
        this.a = appCompatDelegateImplV7;
    }

    /* synthetic */ x(AppCompatDelegateImplV7 appCompatDelegateImplV7, byte b) {
        this(appCompatDelegateImplV7);
    }

    public final void a(i iVar, boolean z) {
        this.a.b(iVar);
    }

    public final boolean a(i iVar) {
        Window.Callback callback = this.a.b.getCallback();
        if (callback == null) {
            return true;
        }
        callback.onMenuOpened(8, iVar);
        return true;
    }
}
