package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

public abstract class n {
    n() {
    }

    public static n a(Activity activity, m mVar) {
        return a(activity, activity.getWindow(), mVar);
    }

    public static n a(Dialog dialog, m mVar) {
        return a(dialog.getContext(), dialog.getWindow(), mVar);
    }

    private static n a(Context context, Window window, m mVar) {
        int i = Build.VERSION.SDK_INT;
        return i >= 14 ? new r(context, window, mVar) : i >= 11 ? new q(context, window, mVar) : new AppCompatDelegateImplV7(context, window, mVar);
    }

    public abstract ActionBar a();

    public abstract void a(int i);

    public abstract void a(Configuration configuration);

    public abstract void a(Bundle bundle);

    public abstract void a(Toolbar toolbar);

    public abstract void a(View view);

    public abstract void a(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void a(CharSequence charSequence);

    public abstract MenuInflater b();

    public abstract void b(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void c();

    public abstract void d();

    public abstract void e();

    public abstract void f();

    public abstract void g();

    public abstract boolean h();

    public abstract void i();
}
