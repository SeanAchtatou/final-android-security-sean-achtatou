package android.support.v7.app;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

public class ac extends Dialog implements m {
    private n a;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ac(android.content.Context r5, int r6) {
        /*
            r4 = this;
            if (r6 != 0) goto L_0x0013
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            android.content.res.Resources$Theme r1 = r5.getTheme()
            int r2 = android.support.v7.a.b.dialogTheme
            r3 = 1
            r1.resolveAttribute(r2, r0, r3)
            int r6 = r0.resourceId
        L_0x0013:
            r4.<init>(r5, r6)
            android.support.v7.app.n r0 = r4.a()
            r1 = 0
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.ac.<init>(android.content.Context, int):void");
    }

    public final n a() {
        if (this.a == null) {
            this.a = n.a(this, this);
        }
        return this.a;
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().b(view, layoutParams);
    }

    public void invalidateOptionsMenu() {
        a().f();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        a().i();
        super.onCreate(bundle);
        a().a(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        a().d();
    }

    public void setContentView(int i) {
        a().a(i);
    }

    public void setContentView(View view) {
        a().a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().a(view, layoutParams);
    }

    public void setTitle(int i) {
        super.setTitle(i);
        a().a(getContext().getString(i));
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        a().a(charSequence);
    }
}
