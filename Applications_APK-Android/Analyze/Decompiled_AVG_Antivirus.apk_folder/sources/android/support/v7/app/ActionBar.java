package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.support.v7.a.l;
import android.support.v7.d.a;
import android.support.v7.d.b;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.ViewGroup;

public abstract class ActionBar {

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int a;

        public LayoutParams() {
            super(-2, -2);
            this.a = 0;
            this.a = 8388627;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.a = 0;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.b);
            this.a = obtainStyledAttributes.getInt(l.c, 0);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.MarginLayoutParams) layoutParams);
            this.a = 0;
            this.a = layoutParams.a;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.a = 0;
        }
    }

    public abstract int a();

    public a a(b bVar) {
        return null;
    }

    public void a(Configuration configuration) {
    }

    public abstract void a(CharSequence charSequence);

    public abstract void a(boolean z);

    public boolean a(int i, KeyEvent keyEvent) {
        return false;
    }

    public Context b() {
        return null;
    }

    public void b(CharSequence charSequence) {
    }

    public void b(boolean z) {
    }

    public void c(boolean z) {
    }

    public boolean c() {
        return false;
    }

    public void d(boolean z) {
    }

    public boolean d() {
        return false;
    }
}
