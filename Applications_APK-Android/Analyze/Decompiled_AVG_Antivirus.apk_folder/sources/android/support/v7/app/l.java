package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public final class l {
    private final d a;
    private int b;

    public l(Context context) {
        this(context, k.a(context, 0));
    }

    private l(Context context, int i) {
        this.a = new d(new ContextThemeWrapper(context, k.a(context, i)));
        this.b = i;
    }

    public final Context a() {
        return this.a.a;
    }

    public final l a(DialogInterface.OnKeyListener onKeyListener) {
        this.a.r = onKeyListener;
        return this;
    }

    public final l a(Drawable drawable) {
        this.a.d = drawable;
        return this;
    }

    public final l a(View view) {
        this.a.g = view;
        return this;
    }

    public final l a(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
        this.a.t = listAdapter;
        this.a.u = onClickListener;
        return this;
    }

    public final l a(CharSequence charSequence) {
        this.a.f = charSequence;
        return this;
    }

    public final l a(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        this.a.i = charSequence;
        this.a.j = onClickListener;
        return this;
    }

    public final l b() {
        this.a.o = true;
        return this;
    }

    public final l b(View view) {
        this.a.w = view;
        this.a.v = 0;
        this.a.B = false;
        return this;
    }

    public final l b(CharSequence charSequence) {
        this.a.h = charSequence;
        return this;
    }

    public final l b(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        this.a.k = charSequence;
        this.a.l = onClickListener;
        return this;
    }

    public final k c() {
        ListAdapter jVar;
        k kVar = new k(this.a.a, this.b);
        d dVar = this.a;
        b a2 = kVar.a;
        if (dVar.g != null) {
            a2.a(dVar.g);
        } else {
            if (dVar.f != null) {
                a2.a(dVar.f);
            }
            if (dVar.d != null) {
                a2.a(dVar.d);
            }
            if (dVar.c != 0) {
                a2.b(dVar.c);
            }
            if (dVar.e != 0) {
                a2.b(a2.c(dVar.e));
            }
        }
        if (dVar.h != null) {
            a2.b(dVar.h);
        }
        if (dVar.i != null) {
            a2.a(-1, dVar.i, dVar.j, null);
        }
        if (dVar.k != null) {
            a2.a(-2, dVar.k, dVar.l, null);
        }
        if (dVar.m != null) {
            a2.a(-3, dVar.m, dVar.n, null);
        }
        if (!(dVar.s == null && dVar.H == null && dVar.t == null)) {
            ListView listView = (ListView) dVar.b.inflate(a2.H, (ViewGroup) null);
            if (dVar.D) {
                jVar = dVar.H == null ? new e(dVar, dVar.a, a2.I, dVar.s, listView) : new f(dVar, dVar.a, dVar.H, listView, a2);
            } else {
                int k = dVar.E ? a2.J : a2.K;
                jVar = dVar.H == null ? dVar.t != null ? dVar.t : new j(dVar.a, k, dVar.s) : new SimpleCursorAdapter(dVar.a, k, dVar.H, new String[]{dVar.I}, new int[]{16908308});
            }
            ListAdapter unused = a2.D = jVar;
            int unused2 = a2.E = dVar.F;
            if (dVar.u != null) {
                listView.setOnItemClickListener(new g(dVar, a2));
            } else if (dVar.G != null) {
                listView.setOnItemClickListener(new h(dVar, listView, a2));
            }
            if (dVar.K != null) {
                listView.setOnItemSelectedListener(dVar.K);
            }
            if (dVar.E) {
                listView.setChoiceMode(1);
            } else if (dVar.D) {
                listView.setChoiceMode(2);
            }
            ListView unused3 = a2.f = listView;
        }
        if (dVar.w != null) {
            if (dVar.B) {
                a2.a(dVar.w, dVar.x, dVar.y, dVar.z, dVar.A);
            } else {
                a2.b(dVar.w);
            }
        } else if (dVar.v != 0) {
            a2.a(dVar.v);
        }
        kVar.setCancelable(this.a.o);
        if (this.a.o) {
            kVar.setCanceledOnTouchOutside(true);
        }
        kVar.setOnCancelListener(this.a.p);
        kVar.setOnDismissListener(this.a.q);
        if (this.a.r != null) {
            kVar.setOnKeyListener(this.a.r);
        }
        return kVar;
    }

    public final k d() {
        k c = c();
        c.show();
        return c;
    }
}
