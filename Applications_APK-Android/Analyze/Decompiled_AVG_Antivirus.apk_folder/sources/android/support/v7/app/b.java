package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v7.a.g;
import android.support.v7.a.l;
import android.support.v7.internal.widget.be;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

final class b {
    private TextView A;
    private TextView B;
    private View C;
    /* access modifiers changed from: private */
    public ListAdapter D;
    /* access modifiers changed from: private */
    public int E = -1;
    private int F;
    private int G;
    /* access modifiers changed from: private */
    public int H;
    /* access modifiers changed from: private */
    public int I;
    /* access modifiers changed from: private */
    public int J;
    /* access modifiers changed from: private */
    public int K;
    private int L = 0;
    /* access modifiers changed from: private */
    public Handler M;
    private final View.OnClickListener N = new c(this);
    private final Context a;
    /* access modifiers changed from: private */
    public final ac b;
    private final Window c;
    private CharSequence d;
    private CharSequence e;
    /* access modifiers changed from: private */
    public ListView f;
    private View g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private boolean m = false;
    /* access modifiers changed from: private */
    public Button n;
    private CharSequence o;
    /* access modifiers changed from: private */
    public Message p;
    /* access modifiers changed from: private */
    public Button q;
    private CharSequence r;
    /* access modifiers changed from: private */
    public Message s;
    /* access modifiers changed from: private */
    public Button t;
    private CharSequence u;
    /* access modifiers changed from: private */
    public Message v;
    private ScrollView w;
    private int x = 0;
    private Drawable y;
    private ImageView z;

    public b(Context context, ac acVar, Window window) {
        this.a = context;
        this.b = acVar;
        this.c = window;
        this.M = new i(acVar);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, l.I, android.support.v7.a.b.alertDialogStyle, 0);
        this.F = obtainStyledAttributes.getResourceId(l.J, 0);
        this.G = obtainStyledAttributes.getResourceId(l.K, 0);
        this.H = obtainStyledAttributes.getResourceId(l.M, 0);
        this.I = obtainStyledAttributes.getResourceId(l.N, 0);
        this.J = obtainStyledAttributes.getResourceId(l.O, 0);
        this.K = obtainStyledAttributes.getResourceId(l.L, 0);
        obtainStyledAttributes.recycle();
    }

    private static void a(Button button) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) button.getLayoutParams();
        layoutParams.gravity = 1;
        layoutParams.weight = 0.5f;
        button.setLayoutParams(layoutParams);
    }

    private static boolean c(View view) {
        if (view.onCheckIsTextEditor()) {
            return true;
        }
        if (!(view instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        while (childCount > 0) {
            childCount--;
            if (c(viewGroup.getChildAt(childCount))) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final void a() {
        boolean z2;
        boolean z3 = false;
        this.b.a().h();
        this.b.setContentView((this.G == 0 || this.L != 1) ? this.F : this.G);
        ViewGroup viewGroup = (ViewGroup) this.c.findViewById(g.contentPanel);
        this.w = (ScrollView) this.c.findViewById(g.scrollView);
        this.w.setFocusable(false);
        this.B = (TextView) this.c.findViewById(16908299);
        if (this.B != null) {
            if (this.e != null) {
                this.B.setText(this.e);
            } else {
                this.B.setVisibility(8);
                this.w.removeView(this.B);
                if (this.f != null) {
                    ViewGroup viewGroup2 = (ViewGroup) this.w.getParent();
                    int indexOfChild = viewGroup2.indexOfChild(this.w);
                    viewGroup2.removeViewAt(indexOfChild);
                    viewGroup2.addView(this.f, indexOfChild, new ViewGroup.LayoutParams(-1, -1));
                } else {
                    viewGroup.setVisibility(8);
                }
            }
        }
        this.n = (Button) this.c.findViewById(16908313);
        this.n.setOnClickListener(this.N);
        if (TextUtils.isEmpty(this.o)) {
            this.n.setVisibility(8);
            z2 = false;
        } else {
            this.n.setText(this.o);
            this.n.setVisibility(0);
            z2 = true;
        }
        this.q = (Button) this.c.findViewById(16908314);
        this.q.setOnClickListener(this.N);
        if (TextUtils.isEmpty(this.r)) {
            this.q.setVisibility(8);
        } else {
            this.q.setText(this.r);
            this.q.setVisibility(0);
            z2 |= true;
        }
        this.t = (Button) this.c.findViewById(16908315);
        this.t.setOnClickListener(this.N);
        if (TextUtils.isEmpty(this.u)) {
            this.t.setVisibility(8);
        } else {
            this.t.setText(this.u);
            this.t.setVisibility(0);
            z2 |= true;
        }
        Context context = this.a;
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(android.support.v7.a.b.alertDialogCenterButtons, typedValue, true);
        if (typedValue.data != 0) {
            if (z2) {
                a(this.n);
            } else if (z2) {
                a(this.q);
            } else if (z2) {
                a(this.t);
            }
        }
        boolean z4 = z2;
        ViewGroup viewGroup3 = (ViewGroup) this.c.findViewById(g.topPanel);
        be a2 = be.a(this.a, null, l.I, android.support.v7.a.b.alertDialogStyle);
        if (this.C != null) {
            viewGroup3.addView(this.C, 0, new ViewGroup.LayoutParams(-1, -2));
            this.c.findViewById(g.title_template).setVisibility(8);
        } else {
            this.z = (ImageView) this.c.findViewById(16908294);
            if (!TextUtils.isEmpty(this.d)) {
                this.A = (TextView) this.c.findViewById(g.alertTitle);
                this.A.setText(this.d);
                if (this.x != 0) {
                    this.z.setImageResource(this.x);
                } else if (this.y != null) {
                    this.z.setImageDrawable(this.y);
                } else {
                    this.A.setPadding(this.z.getPaddingLeft(), this.z.getPaddingTop(), this.z.getPaddingRight(), this.z.getPaddingBottom());
                    this.z.setVisibility(8);
                }
            } else {
                this.c.findViewById(g.title_template).setVisibility(8);
                this.z.setVisibility(8);
                viewGroup3.setVisibility(8);
            }
        }
        View findViewById = this.c.findViewById(g.buttonPanel);
        if (!z4) {
            findViewById.setVisibility(8);
            View findViewById2 = this.c.findViewById(g.textSpacerNoButtons);
            if (findViewById2 != null) {
                findViewById2.setVisibility(0);
            }
        }
        FrameLayout frameLayout = (FrameLayout) this.c.findViewById(g.customPanel);
        View inflate = this.g != null ? this.g : this.h != 0 ? LayoutInflater.from(this.a).inflate(this.h, (ViewGroup) frameLayout, false) : null;
        if (inflate != null) {
            z3 = true;
        }
        if (!z3 || !c(inflate)) {
            this.c.setFlags(131072, 131072);
        }
        if (z3) {
            FrameLayout frameLayout2 = (FrameLayout) this.c.findViewById(g.custom);
            frameLayout2.addView(inflate, new ViewGroup.LayoutParams(-1, -1));
            if (this.m) {
                frameLayout2.setPadding(this.i, this.j, this.k, this.l);
            }
            if (this.f != null) {
                ((LinearLayout.LayoutParams) frameLayout.getLayoutParams()).weight = 0.0f;
            }
        } else {
            frameLayout.setVisibility(8);
        }
        ListView listView = this.f;
        if (!(listView == null || this.D == null)) {
            listView.setAdapter(this.D);
            int i2 = this.E;
            if (i2 >= 0) {
                listView.setItemChecked(i2, true);
                listView.setSelection(i2);
            }
        }
        a2.b();
    }

    public final void a(int i2) {
        this.g = null;
        this.h = i2;
        this.m = false;
    }

    public final void a(int i2, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, Message message) {
        if (onClickListener != null) {
            message = this.M.obtainMessage(i2, onClickListener);
        }
        switch (i2) {
            case -3:
                this.u = charSequence;
                this.v = message;
                return;
            case -2:
                this.r = charSequence;
                this.s = message;
                return;
            case -1:
                this.o = charSequence;
                this.p = message;
                return;
            default:
                throw new IllegalArgumentException("Button does not exist");
        }
    }

    public final void a(Drawable drawable) {
        this.y = drawable;
        this.x = 0;
        if (this.z == null) {
            return;
        }
        if (drawable != null) {
            this.z.setImageDrawable(drawable);
        } else {
            this.z.setVisibility(8);
        }
    }

    public final void a(View view) {
        this.C = view;
    }

    public final void a(View view, int i2, int i3, int i4, int i5) {
        this.g = view;
        this.h = 0;
        this.m = true;
        this.i = i2;
        this.j = i3;
        this.k = i4;
        this.l = i5;
    }

    public final void a(CharSequence charSequence) {
        this.d = charSequence;
        if (this.A != null) {
            this.A.setText(charSequence);
        }
    }

    public final boolean a(KeyEvent keyEvent) {
        return this.w != null && this.w.executeKeyEvent(keyEvent);
    }

    public final void b(int i2) {
        this.y = null;
        this.x = i2;
        if (this.z == null) {
            return;
        }
        if (i2 != 0) {
            this.z.setImageResource(this.x);
        } else {
            this.z.setVisibility(8);
        }
    }

    public final void b(View view) {
        this.g = view;
        this.h = 0;
        this.m = false;
    }

    public final void b(CharSequence charSequence) {
        this.e = charSequence;
        if (this.B != null) {
            this.B.setText(charSequence);
        }
    }

    public final boolean b(KeyEvent keyEvent) {
        return this.w != null && this.w.executeKeyEvent(keyEvent);
    }

    public final int c(int i2) {
        TypedValue typedValue = new TypedValue();
        this.a.getTheme().resolveAttribute(i2, typedValue, true);
        return typedValue.resourceId;
    }
}
