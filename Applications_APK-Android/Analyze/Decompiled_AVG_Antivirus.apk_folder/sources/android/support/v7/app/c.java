package android.support.v7.app;

import android.os.Message;
import android.view.View;

final class c implements View.OnClickListener {
    final /* synthetic */ b a;

    c(b bVar) {
        this.a = bVar;
    }

    public final void onClick(View view) {
        Message obtain = (view != this.a.n || this.a.p == null) ? (view != this.a.q || this.a.s == null) ? (view != this.a.t || this.a.v == null) ? null : Message.obtain(this.a.v) : Message.obtain(this.a.s) : Message.obtain(this.a.p);
        if (obtain != null) {
            obtain.sendToTarget();
        }
        this.a.M.obtainMessage(1, this.a.b).sendToTarget();
    }
}
