package android.support.v7.app;

import android.support.v4.view.bx;
import android.support.v7.d.a;
import android.support.v7.d.b;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

final class y implements b {
    final /* synthetic */ AppCompatDelegateImplV7 a;
    private b b;

    public y(AppCompatDelegateImplV7 appCompatDelegateImplV7, b bVar) {
        this.a = appCompatDelegateImplV7;
        this.b = bVar;
    }

    public final void a(a aVar) {
        this.b.a(aVar);
        if (this.a.m != null) {
            this.a.b.getDecorView().removeCallbacks(this.a.n);
            this.a.m.dismiss();
        } else if (this.a.l != null) {
            this.a.l.setVisibility(8);
            if (this.a.l.getParent() != null) {
                bx.x((View) this.a.l.getParent());
            }
        }
        if (this.a.l != null) {
            this.a.l.removeAllViews();
        }
        this.a.k = null;
    }

    public final boolean a(a aVar, Menu menu) {
        return this.b.a(aVar, menu);
    }

    public final boolean a(a aVar, MenuItem menuItem) {
        return this.b.a(aVar, menuItem);
    }

    public final boolean b(a aVar, Menu menu) {
        return this.b.b(aVar, menu);
    }
}
