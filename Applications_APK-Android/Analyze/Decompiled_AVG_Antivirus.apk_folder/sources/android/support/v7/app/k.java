package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.a.b;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;

public final class k extends ac implements DialogInterface {
    /* access modifiers changed from: private */
    public b a = new b(getContext(), this, getWindow());

    k(Context context, int i) {
        super(context, a(context, i));
    }

    static int a(Context context, int i) {
        if (i >= 16777216) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(b.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public final void a(View view) {
        this.a.b(view);
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.a.a();
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.a.a(keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public final boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.a.b(keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public final void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.a.a(charSequence);
    }
}
