package android.support.v7.app;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

final class h implements AdapterView.OnItemClickListener {
    final /* synthetic */ ListView a;
    final /* synthetic */ b b;
    final /* synthetic */ d c;

    h(d dVar, ListView listView, b bVar) {
        this.c = dVar;
        this.a = listView;
        this.b = bVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.c.C != null) {
            this.c.C[i] = this.a.isItemChecked(i);
        }
        this.c.G.onClick(this.b.b, i, this.a.isItemChecked(i));
    }
}
