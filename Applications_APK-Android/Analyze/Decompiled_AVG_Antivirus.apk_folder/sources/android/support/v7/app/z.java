package android.support.v7.app;

import android.content.Context;
import android.support.v7.internal.widget.bc;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.FrameLayout;

final class z extends FrameLayout {
    final /* synthetic */ AppCompatDelegateImplV7 a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z(AppCompatDelegateImplV7 appCompatDelegateImplV7, Context context) {
        super(context);
        this.a = appCompatDelegateImplV7;
    }

    public final boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.a.a(keyEvent);
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            if (x < -5 || y < -5 || x > getWidth() + 5 || y > getHeight() + 5) {
                this.a.a(this.a.d(0), true);
                return true;
            }
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public final void setBackgroundResource(int i) {
        setBackgroundDrawable(bc.a(getContext(), i));
    }
}
