package android.support.v7.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

class q extends AppCompatDelegateImplV7 {
    q(Context context, Window window, m mVar) {
        super(context, window, mVar);
    }

    /* access modifiers changed from: package-private */
    public final View b(View view, String str, Context context, AttributeSet attributeSet) {
        View b = super.b(view, str, context, attributeSet);
        if (b != null) {
            return b;
        }
        if (this.c instanceof LayoutInflater.Factory2) {
            return ((LayoutInflater.Factory2) this.c).onCreateView(view, str, context, attributeSet);
        }
        return null;
    }
}
