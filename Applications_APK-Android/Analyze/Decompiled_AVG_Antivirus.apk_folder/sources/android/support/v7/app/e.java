package android.support.v7.app;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

final class e extends ArrayAdapter {
    final /* synthetic */ ListView a;
    final /* synthetic */ d b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(d dVar, Context context, int i, CharSequence[] charSequenceArr, ListView listView) {
        super(context, i, 16908308, charSequenceArr);
        this.b = dVar;
        this.a = listView;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        if (this.b.C != null && this.b.C[i]) {
            this.a.setItemChecked(i, true);
        }
        return view2;
    }
}
