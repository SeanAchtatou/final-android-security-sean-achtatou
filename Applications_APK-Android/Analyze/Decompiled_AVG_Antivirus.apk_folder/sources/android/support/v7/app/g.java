package android.support.v7.app;

import android.view.View;
import android.widget.AdapterView;

final class g implements AdapterView.OnItemClickListener {
    final /* synthetic */ b a;
    final /* synthetic */ d b;

    g(d dVar, b bVar) {
        this.b = dVar;
        this.a = bVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.b.u.onClick(this.a.b, i);
        if (!this.b.E) {
            this.a.b.dismiss();
        }
    }
}
