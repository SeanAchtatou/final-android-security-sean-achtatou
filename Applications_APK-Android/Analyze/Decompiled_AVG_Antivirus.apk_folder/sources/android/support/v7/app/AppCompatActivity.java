package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ar;
import android.support.v4.app.cq;
import android.support.v4.app.cr;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class AppCompatActivity extends FragmentActivity implements cr, m {
    private n i;

    private boolean d() {
        Intent a = ar.a(this);
        if (a == null) {
            return false;
        }
        if (ar.a(this, a)) {
            cq a2 = cq.a((Context) this);
            a2.a((Activity) this);
            a2.a();
            try {
                if (Build.VERSION.SDK_INT >= 16) {
                    finishAffinity();
                } else {
                    finish();
                }
            } catch (IllegalStateException e) {
                finish();
            }
        } else {
            ar.b(this, a);
        }
        return true;
    }

    private n e() {
        if (this.i == null) {
            this.i = n.a(this, this);
        }
        return this.i;
    }

    public final void a() {
        e().f();
    }

    public final void a(Toolbar toolbar) {
        e().a(toolbar);
    }

    public final Intent a_() {
        return ar.a(this);
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        e().b(view, layoutParams);
    }

    public final ActionBar c() {
        return e().a();
    }

    public MenuInflater getMenuInflater() {
        return e().b();
    }

    public void invalidateOptionsMenu() {
        e().f();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        e().a(configuration);
    }

    public void onContentChanged() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        e().i();
        super.onCreate(bundle);
        e().a(bundle);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        e().g();
    }

    public final boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        if (super.onMenuItemSelected(i2, menuItem)) {
            return true;
        }
        ActionBar a = e().a();
        if (menuItem.getItemId() != 16908332 || a == null || (a.a() & 4) == 0) {
            return false;
        }
        return d();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        e().c();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        e().e();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        e().d();
    }

    /* access modifiers changed from: protected */
    public void onTitleChanged(CharSequence charSequence, int i2) {
        super.onTitleChanged(charSequence, i2);
        e().a(charSequence);
    }

    public void setContentView(int i2) {
        e().a(i2);
    }

    public void setContentView(View view) {
        e().a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        e().a(view, layoutParams);
    }
}
