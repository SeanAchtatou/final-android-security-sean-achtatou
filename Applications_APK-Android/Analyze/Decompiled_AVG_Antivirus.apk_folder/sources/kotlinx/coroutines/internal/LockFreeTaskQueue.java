package kotlinx.coroutines.internal;

import java.util.List;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0010\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0013\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00028\u0000¢\u0006\u0002\u0010\u0012J\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0015\u001a\u00020\u0004J&\u0010\u0016\u001a\b\u0012\u0004\u0012\u0002H\u00180\u0017\"\u0004\b\u0001\u0010\u00182\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H\u00180\u001aJ\r\u0010\u001b\u001a\u0004\u0018\u00018\u0000¢\u0006\u0002\u0010\u001cJ$\u0010\u001d\u001a\u0004\u0018\u00018\u00002\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00040\u001aH\b¢\u0006\u0002\u0010\u001fR$\u0010\u0006\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00028\u00000\bj\b\u0012\u0004\u0012\u00028\u0000`\t0\u0007X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\n\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\f\u001a\u00020\r8F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000f¨\u0006 "}, d2 = {"Lkotlinx/coroutines/internal/LockFreeTaskQueue;", "E", "", "singleConsumer", "", "(Z)V", "_cur", "Lkotlinx/atomicfu/AtomicRef;", "Lkotlinx/coroutines/internal/LockFreeTaskQueueCore;", "Lkotlinx/coroutines/internal/Core;", "isEmpty", "()Z", "size", "", "getSize", "()I", "addLast", "element", "(Ljava/lang/Object;)Z", "close", "", "isClosed", "map", "", "R", "transform", "Lkotlin/Function1;", "removeFirstOrNull", "()Ljava/lang/Object;", "removeFirstOrNullIf", "predicate", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
/* compiled from: LockFreeTaskQueue.kt */
public class LockFreeTaskQueue<E> {
    public static final /* synthetic */ AtomicReferenceFieldUpdater _cur$FU$internal = AtomicReferenceFieldUpdater.newUpdater(LockFreeTaskQueue.class, Object.class, "_cur$internal");
    public volatile /* synthetic */ Object _cur$internal;

    public LockFreeTaskQueue(boolean singleConsumer) {
        this._cur$internal = new LockFreeTaskQueueCore(8, singleConsumer);
    }

    public final boolean isEmpty() {
        return ((LockFreeTaskQueueCore) this._cur$internal).isEmpty();
    }

    public final int getSize() {
        return ((LockFreeTaskQueueCore) this._cur$internal).getSize();
    }

    public final void close() {
        while (true) {
            LockFreeTaskQueueCore cur = (LockFreeTaskQueueCore) this._cur$internal;
            if (!cur.close()) {
                _cur$FU$internal.compareAndSet(this, cur, cur.next());
            } else {
                return;
            }
        }
    }

    public final boolean addLast(@NotNull E element) {
        Intrinsics.checkParameterIsNotNull(element, "element");
        while (true) {
            LockFreeTaskQueueCore cur = (LockFreeTaskQueueCore) this._cur$internal;
            int addLast = cur.addLast(element);
            if (addLast == 0) {
                return true;
            }
            if (addLast == 1) {
                _cur$FU$internal.compareAndSet(this, cur, cur.next());
            } else if (addLast == 2) {
                return false;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00d8, code lost:
        r5 = r26;
     */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final E removeFirstOrNull() {
        /*
            r32 = this;
            r0 = r32
            r1 = 0
            r2 = r0
            r3 = 0
        L_0x0005:
            java.lang.Object r4 = r2._cur$internal
            kotlinx.coroutines.internal.LockFreeTaskQueueCore r4 = (kotlinx.coroutines.internal.LockFreeTaskQueueCore) r4
            r11 = 0
            r6 = r4
            r12 = 0
            r13 = r6
            r14 = 0
        L_0x000f:
            long r9 = r13._state$internal
            r15 = 0
            r7 = 1152921504606846976(0x1000000000000000, double:1.2882297539194267E-231)
            long r7 = r7 & r9
            r16 = 0
            int r18 = (r7 > r16 ? 1 : (r7 == r16 ? 0 : -1))
            if (r18 == 0) goto L_0x0026
            kotlinx.coroutines.internal.Symbol r5 = kotlinx.coroutines.internal.LockFreeTaskQueueCore.REMOVE_FROZEN
            r24 = r1
            r25 = r2
            r23 = r3
            goto L_0x00da
        L_0x0026:
            kotlinx.coroutines.internal.LockFreeTaskQueueCore$Companion r16 = kotlinx.coroutines.internal.LockFreeTaskQueueCore.Companion
            r17 = r9
            r19 = 0
            r7 = 1073741823(0x3fffffff, double:5.304989472E-315)
            long r7 = r17 & r7
            r20 = 0
            long r7 = r7 >> r20
            int r8 = (int) r7
            r20 = 1152921503533105152(0xfffffffc0000000, double:1.2882296003504729E-231)
            long r20 = r17 & r20
            r7 = 30
            r22 = r6
            long r5 = r20 >> r7
            int r7 = (int) r5
            r5 = r8
            r20 = r7
            r21 = 0
            r6 = r22
            int r22 = r6.mask
            r24 = r1
            r1 = r20 & r22
            int r22 = r6.mask
            r25 = r2
            r2 = r5 & r22
            if (r1 != r2) goto L_0x0062
            r23 = r3
            r5 = 0
            goto L_0x00da
        L_0x0062:
            java.util.concurrent.atomic.AtomicReferenceArray r1 = r6.array
            int r2 = r6.mask
            r2 = r2 & r5
            java.lang.Object r1 = r1.get(r2)
            if (r1 != 0) goto L_0x007f
            boolean r2 = r6.singleConsumer
            if (r2 == 0) goto L_0x007c
            r23 = r3
            r5 = 0
            goto L_0x00da
        L_0x007c:
            r23 = r3
            goto L_0x00c5
        L_0x007f:
            boolean r2 = r1 instanceof kotlinx.coroutines.internal.LockFreeTaskQueueCore.Placeholder
            if (r2 == 0) goto L_0x0087
            r23 = r3
            r5 = 0
            goto L_0x00da
        L_0x0087:
            r2 = r1
            r22 = 0
            int r2 = r5 + 1
            r22 = 1073741823(0x3fffffff, float:1.9999999)
            r2 = r2 & r22
            java.util.concurrent.atomic.AtomicLongFieldUpdater r22 = kotlinx.coroutines.internal.LockFreeTaskQueueCore._state$FU$internal
            r26 = r1
            kotlinx.coroutines.internal.LockFreeTaskQueueCore$Companion r1 = kotlinx.coroutines.internal.LockFreeTaskQueueCore.Companion
            long r27 = r1.updateHead(r9, r2)
            r23 = r3
            r1 = r5
            r3 = 0
            r5 = r22
            r29 = r7
            r22 = r8
            r7 = r9
            r30 = r9
            r9 = r27
            boolean r5 = r5.compareAndSet(r6, r7, r9)
            if (r5 == 0) goto L_0x00bf
            java.util.concurrent.atomic.AtomicReferenceArray r5 = r6.array
            int r7 = r6.mask
            r7 = r7 & r1
            r5.set(r7, r3)
            goto L_0x00d8
        L_0x00bf:
            boolean r3 = r6.singleConsumer
            if (r3 != 0) goto L_0x00cd
        L_0x00c5:
            r3 = r23
            r1 = r24
            r2 = r25
            goto L_0x000f
        L_0x00cd:
            r3 = r6
        L_0x00ce:
            kotlinx.coroutines.internal.LockFreeTaskQueueCore r5 = r3.removeSlowPath(r1, r2)
            if (r5 == 0) goto L_0x00d8
            r3 = r5
            goto L_0x00ce
        L_0x00d8:
            r5 = r26
        L_0x00da:
            r1 = r5
            kotlinx.coroutines.internal.Symbol r2 = kotlinx.coroutines.internal.LockFreeTaskQueueCore.REMOVE_FROZEN
            if (r1 == r2) goto L_0x00e0
            return r1
        L_0x00e0:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r2 = kotlinx.coroutines.internal.LockFreeTaskQueue._cur$FU$internal
            kotlinx.coroutines.internal.LockFreeTaskQueueCore r3 = r4.next()
            r2.compareAndSet(r0, r4, r3)
            r3 = r23
            r1 = r24
            r2 = r25
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.internal.LockFreeTaskQueue.removeFirstOrNull():java.lang.Object");
    }

    /* JADX INFO: Multiple debug info for r0v3 java.lang.Object: [D('head$iv' int), D('result' java.lang.Object)] */
    @Nullable
    public final E removeFirstOrNullIf(@NotNull Function1<? super E, Boolean> predicate) {
        int $i$f$removeFirstOrNullIf;
        LockFreeTaskQueue $receiver$iv;
        Object element$iv;
        Function1<? super E, Boolean> function1 = predicate;
        int $i$f$removeFirstOrNullIf2 = 0;
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        LockFreeTaskQueue $receiver$iv2 = this;
        while (true) {
            LockFreeTaskQueueCore cur = (LockFreeTaskQueueCore) $receiver$iv2._cur$internal;
            LockFreeTaskQueueCore this_$iv = cur;
            LockFreeTaskQueueCore $receiver$iv$iv = this_$iv;
            while (true) {
                long state$iv = $receiver$iv$iv._state$internal;
                if ((LockFreeTaskQueueCore.FROZEN_MASK & state$iv) != 0) {
                    $i$f$removeFirstOrNullIf = $i$f$removeFirstOrNullIf2;
                    $receiver$iv = $receiver$iv2;
                    element$iv = LockFreeTaskQueueCore.REMOVE_FROZEN;
                    break;
                }
                LockFreeTaskQueueCore.Companion companion = LockFreeTaskQueueCore.Companion;
                long $this$withState$iv$iv = state$iv;
                int head$iv$iv = (int) (($this$withState$iv$iv & LockFreeTaskQueueCore.HEAD_MASK) >> 0);
                int head$iv = head$iv$iv;
                this_$iv = this_$iv;
                $i$f$removeFirstOrNullIf = $i$f$removeFirstOrNullIf2;
                $receiver$iv = $receiver$iv2;
                if ((((int) (($this$withState$iv$iv & LockFreeTaskQueueCore.TAIL_MASK) >> 30)) & this_$iv.mask) == (head$iv & this_$iv.mask)) {
                    element$iv = null;
                    break;
                }
                Object element$iv2 = this_$iv.array.get(this_$iv.mask & head$iv);
                if (element$iv2 != null) {
                    if (!(element$iv2 instanceof LockFreeTaskQueueCore.Placeholder)) {
                        if (function1.invoke(element$iv2).booleanValue()) {
                            int newHead$iv = (head$iv + 1) & LockFreeTaskQueueCore.MAX_CAPACITY_MASK;
                            element$iv = element$iv2;
                            int head$iv2 = head$iv;
                            if (!LockFreeTaskQueueCore._state$FU$internal.compareAndSet(this_$iv, state$iv, LockFreeTaskQueueCore.Companion.updateHead(state$iv, newHead$iv))) {
                                if (this_$iv.singleConsumer) {
                                    LockFreeTaskQueueCore cur$iv = this_$iv;
                                    while (true) {
                                        LockFreeTaskQueueCore access$removeSlowPath = cur$iv.removeSlowPath(head$iv2, newHead$iv);
                                        if (access$removeSlowPath == null) {
                                            break;
                                        }
                                        cur$iv = access$removeSlowPath;
                                    }
                                }
                            } else {
                                this_$iv.array.set(this_$iv.mask & head$iv2, null);
                                break;
                            }
                        } else {
                            element$iv = null;
                            break;
                        }
                    } else {
                        element$iv = null;
                        break;
                    }
                } else if (this_$iv.singleConsumer) {
                    element$iv = null;
                    break;
                }
                function1 = predicate;
                $i$f$removeFirstOrNullIf2 = $i$f$removeFirstOrNullIf;
                $receiver$iv2 = $receiver$iv;
            }
            Object result = element$iv;
            if (result != LockFreeTaskQueueCore.REMOVE_FROZEN) {
                return result;
            }
            _cur$FU$internal.compareAndSet(this, cur, cur.next());
            function1 = predicate;
            $i$f$removeFirstOrNullIf2 = $i$f$removeFirstOrNullIf;
            $receiver$iv2 = $receiver$iv;
        }
    }

    @NotNull
    public final <R> List<R> map(@NotNull Function1<? super E, ? extends R> transform) {
        Intrinsics.checkParameterIsNotNull(transform, "transform");
        return ((LockFreeTaskQueueCore) this._cur$internal).map(transform);
    }

    public final boolean isClosed() {
        return ((LockFreeTaskQueueCore) this._cur$internal).isClosed();
    }
}
