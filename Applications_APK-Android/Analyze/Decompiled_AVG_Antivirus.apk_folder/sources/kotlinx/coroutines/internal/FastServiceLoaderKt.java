package kotlinx.coroutines.internal;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u0002"}, d2 = {"FAST_SERVICE_LOADER_PROPERTY_NAME", "", "kotlinx-coroutines-core"}, k = 2, mv = {1, 1, 15})
/* compiled from: FastServiceLoader.kt */
public final class FastServiceLoaderKt {
    private static final String FAST_SERVICE_LOADER_PROPERTY_NAME = "kotlinx.coroutines.fast.service.loader";
}
