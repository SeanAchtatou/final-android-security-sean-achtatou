package kotlinx.coroutines.internal;

import androidx.core.app.NotificationCompat;
import com.jobstrak.drawingfun.lib.mopub.common.Constants;
import java.io.BufferedReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bÀ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J1\u0010\u0007\u001a\u0002H\b\"\u0004\b\u0000\u0010\b2\u0006\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\b0\rH\u0002¢\u0006\u0002\u0010\u000eJ/\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\b0\u0010\"\u0004\b\u0000\u0010\b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\b0\r2\u0006\u0010\n\u001a\u00020\u000bH\u0000¢\u0006\u0002\b\u0011J/\u0010\u0012\u001a\b\u0012\u0004\u0012\u0002H\b0\u0010\"\u0004\b\u0000\u0010\b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\b0\r2\u0006\u0010\n\u001a\u00020\u000bH\u0000¢\u0006\u0002\b\u0013J\u0016\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00060\u00102\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0016\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00060\u00102\u0006\u0010\u0018\u001a\u00020\u0019H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lkotlinx/coroutines/internal/FastServiceLoader;", "", "()V", "FAST_SERVICE_LOADER_ENABLED", "", "PREFIX", "", "getProviderInstance", "S", "name", "loader", "Ljava/lang/ClassLoader;", "service", "Ljava/lang/Class;", "(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;", "load", "", "load$kotlinx_coroutines_core", "loadProviders", "loadProviders$kotlinx_coroutines_core", "parse", "url", "Ljava/net/URL;", "parseFile", "r", "Ljava/io/BufferedReader;", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
/* compiled from: FastServiceLoader.kt */
public final class FastServiceLoader {
    private static final boolean FAST_SERVICE_LOADER_ENABLED = SystemPropsKt.systemProp("kotlinx.coroutines.fast.service.loader", true);
    public static final FastServiceLoader INSTANCE = new FastServiceLoader();
    private static final String PREFIX = "META-INF/services/";

    private FastServiceLoader() {
    }

    @NotNull
    public final <S> List<S> load$kotlinx_coroutines_core(@NotNull Class<S> service, @NotNull ClassLoader loader) {
        Intrinsics.checkParameterIsNotNull(service, NotificationCompat.CATEGORY_SERVICE);
        Intrinsics.checkParameterIsNotNull(loader, "loader");
        if (!FAST_SERVICE_LOADER_ENABLED) {
            ServiceLoader<S> load = ServiceLoader.load(service, loader);
            Intrinsics.checkExpressionValueIsNotNull(load, "ServiceLoader.load(service, loader)");
            return CollectionsKt.toList(load);
        }
        try {
            return loadProviders$kotlinx_coroutines_core(service, loader);
        } catch (Throwable th) {
            ServiceLoader<S> load2 = ServiceLoader.load(service, loader);
            Intrinsics.checkExpressionValueIsNotNull(load2, "ServiceLoader.load(service, loader)");
            return CollectionsKt.toList(load2);
        }
    }

    /* JADX INFO: Multiple debug info for r9v5 java.lang.Iterable: [D('list$iv$iv' java.lang.Iterable), D('it' java.net.URL)] */
    @NotNull
    public final <S> List<S> loadProviders$kotlinx_coroutines_core(@NotNull Class<S> service, @NotNull ClassLoader loader) {
        Intrinsics.checkParameterIsNotNull(service, NotificationCompat.CATEGORY_SERVICE);
        Intrinsics.checkParameterIsNotNull(loader, "loader");
        Enumeration urls = loader.getResources(PREFIX + service.getName());
        Intrinsics.checkExpressionValueIsNotNull(urls, Constants.VIDEO_TRACKING_URLS_KEY);
        ArrayList<URL> $this$flatMap$iv = Collections.list(urls);
        Intrinsics.checkExpressionValueIsNotNull($this$flatMap$iv, "java.util.Collections.list(this)");
        Collection destination$iv$iv = new ArrayList();
        for (URL it : $this$flatMap$iv) {
            FastServiceLoader fastServiceLoader = INSTANCE;
            Intrinsics.checkExpressionValueIsNotNull(it, "it");
            CollectionsKt.addAll(destination$iv$iv, fastServiceLoader.parse(it));
        }
        Set providers = CollectionsKt.toSet((List) destination$iv$iv);
        if (!providers.isEmpty()) {
            Iterable<String> $this$map$iv = providers;
            Collection destination$iv$iv2 = new ArrayList(CollectionsKt.collectionSizeOrDefault($this$map$iv, 10));
            for (String it2 : $this$map$iv) {
                destination$iv$iv2.add(INSTANCE.getProviderInstance(it2, loader, service));
            }
            return (List) destination$iv$iv2;
        }
        throw new IllegalArgumentException("No providers were loaded with FastServiceLoader".toString());
    }

    private final <S> S getProviderInstance(String name, ClassLoader loader, Class<S> service) {
        Class clazz = Class.forName(name, false, loader);
        if (service.isAssignableFrom(clazz)) {
            return service.cast(clazz.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
        }
        throw new IllegalArgumentException(("Expected service of class " + service + ", but found " + clazz).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.text.StringsKt__StringsJVMKt.startsWith$default(java.lang.String, java.lang.String, boolean, int, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String, int, int, ?[OBJECT, ARRAY]]
     candidates:
      kotlin.text.StringsKt__StringsKt.startsWith$default(java.lang.CharSequence, char, boolean, int, java.lang.Object):boolean
      kotlin.text.StringsKt__StringsKt.startsWith$default(java.lang.CharSequence, java.lang.CharSequence, boolean, int, java.lang.Object):boolean
      kotlin.text.StringsKt__StringsJVMKt.startsWith$default(java.lang.String, java.lang.String, boolean, int, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.text.StringsKt__StringsKt.substringBefore$default(java.lang.String, char, java.lang.String, int, java.lang.Object):java.lang.String
     arg types: [java.lang.String, int, ?[OBJECT, ARRAY], int, ?[OBJECT, ARRAY]]
     candidates:
      kotlin.text.StringsKt__StringsKt.substringBefore$default(java.lang.String, java.lang.String, java.lang.String, int, java.lang.Object):java.lang.String
      kotlin.text.StringsKt__StringsKt.substringBefore$default(java.lang.String, char, java.lang.String, int, java.lang.Object):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.jar.JarFile.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.util.jar.JarFile.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.util.jar.JarFile.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0061, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        kotlin.io.CloseableKt.closeFinally(r8, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0068, code lost:
        throw r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0069, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006d, code lost:
        kotlin.io.CloseableKt.closeFinally(r5, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0070, code lost:
        throw r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.util.List<java.lang.String> parse(java.net.URL r13) {
        /*
            r12 = this;
            java.lang.String r0 = r13.toString()
            java.lang.String r1 = "url.toString()"
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(r0, r1)
            r1 = 0
            r2 = 2
            r3 = 0
            java.lang.String r4 = "jar"
            boolean r4 = kotlin.text.StringsKt.startsWith$default(r0, r4, r1, r2, r3)
            if (r4 == 0) goto L_0x0071
            java.lang.String r4 = "jar:file:"
            java.lang.String r4 = kotlin.text.StringsKt.substringAfter$default(r0, r4, r3, r2, r3)
            r5 = 33
            java.lang.String r4 = kotlin.text.StringsKt.substringBefore$default(r4, r5, r3, r2, r3)
            java.lang.String r5 = "!/"
            java.lang.String r2 = kotlin.text.StringsKt.substringAfter$default(r0, r5, r3, r2, r3)
            java.util.jar.JarFile r5 = new java.util.jar.JarFile
            r5.<init>(r4, r1)
            java.io.Closeable r5 = (java.io.Closeable) r5
            r1 = r3
            java.lang.Throwable r1 = (java.lang.Throwable) r1
            r6 = r5
            r7 = 0
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x006b }
            java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x006b }
            r10 = r6
            java.util.jar.JarFile r10 = (java.util.jar.JarFile) r10     // Catch:{ Throwable -> 0x006b }
            java.util.zip.ZipEntry r11 = new java.util.zip.ZipEntry     // Catch:{ Throwable -> 0x006b }
            r11.<init>(r2)     // Catch:{ Throwable -> 0x006b }
            java.io.InputStream r10 = r10.getInputStream(r11)     // Catch:{ Throwable -> 0x006b }
            java.lang.String r11 = "UTF-8"
            r9.<init>(r10, r11)     // Catch:{ Throwable -> 0x006b }
            java.io.Reader r9 = (java.io.Reader) r9     // Catch:{ Throwable -> 0x006b }
            r8.<init>(r9)     // Catch:{ Throwable -> 0x006b }
            java.io.Closeable r8 = (java.io.Closeable) r8     // Catch:{ Throwable -> 0x006b }
            java.lang.Throwable r3 = (java.lang.Throwable) r3     // Catch:{ Throwable -> 0x006b }
            r9 = r8
            java.io.BufferedReader r9 = (java.io.BufferedReader) r9     // Catch:{ Throwable -> 0x0063 }
            r10 = 0
            kotlinx.coroutines.internal.FastServiceLoader r11 = kotlinx.coroutines.internal.FastServiceLoader.INSTANCE     // Catch:{ Throwable -> 0x0063 }
            java.util.List r11 = r11.parseFile(r9)     // Catch:{ Throwable -> 0x0063 }
            kotlin.io.CloseableKt.closeFinally(r8, r3)     // Catch:{ Throwable -> 0x006b }
            kotlin.io.CloseableKt.closeFinally(r5, r1)
            return r11
        L_0x0061:
            r9 = move-exception
            goto L_0x0065
        L_0x0063:
            r3 = move-exception
            throw r3     // Catch:{ all -> 0x0061 }
        L_0x0065:
            kotlin.io.CloseableKt.closeFinally(r8, r3)     // Catch:{ Throwable -> 0x006b }
            throw r9     // Catch:{ Throwable -> 0x006b }
        L_0x0069:
            r3 = move-exception
            goto L_0x006d
        L_0x006b:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0069 }
        L_0x006d:
            kotlin.io.CloseableKt.closeFinally(r5, r1)
            throw r3
        L_0x0071:
            java.io.BufferedReader r1 = new java.io.BufferedReader
            java.io.InputStreamReader r2 = new java.io.InputStreamReader
            java.io.InputStream r4 = r13.openStream()
            r2.<init>(r4)
            java.io.Reader r2 = (java.io.Reader) r2
            r1.<init>(r2)
            java.io.Closeable r1 = (java.io.Closeable) r1
            java.lang.Throwable r3 = (java.lang.Throwable) r3
            r2 = r1
            java.io.BufferedReader r2 = (java.io.BufferedReader) r2     // Catch:{ Throwable -> 0x0095 }
            r4 = 0
            kotlinx.coroutines.internal.FastServiceLoader r5 = kotlinx.coroutines.internal.FastServiceLoader.INSTANCE     // Catch:{ Throwable -> 0x0095 }
            java.util.List r5 = r5.parseFile(r2)     // Catch:{ Throwable -> 0x0095 }
            kotlin.io.CloseableKt.closeFinally(r1, r3)
            return r5
        L_0x0093:
            r2 = move-exception
            goto L_0x0098
        L_0x0095:
            r2 = move-exception
            r3 = r2
            throw r3     // Catch:{ all -> 0x0093 }
        L_0x0098:
            kotlin.io.CloseableKt.closeFinally(r1, r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.internal.FastServiceLoader.parse(java.net.URL):java.util.List");
    }

    private final List<String> parseFile(BufferedReader r) {
        boolean z;
        Set names = new LinkedHashSet();
        while (true) {
            String line = r.readLine();
            if (line == null) {
                return CollectionsKt.toList(names);
            }
            String substringBefore$default = StringsKt.substringBefore$default(line, "#", (String) null, 2, (Object) null);
            if (substringBefore$default != null) {
                String serviceName = StringsKt.trim((CharSequence) substringBefore$default).toString();
                CharSequence $this$all$iv = serviceName;
                boolean z2 = false;
                int i = 0;
                while (true) {
                    if (i >= $this$all$iv.length()) {
                        z = true;
                        break;
                    }
                    char it = $this$all$iv.charAt(i);
                    if (((it == '.' || Character.isJavaIdentifierPart(it)) ? (char) 1 : 0) == 0) {
                        z = false;
                        break;
                    }
                    i++;
                }
                if (z) {
                    if (serviceName.length() > 0) {
                        z2 = true;
                    }
                    if (z2) {
                        names.add(serviceName);
                    }
                } else {
                    throw new IllegalArgumentException(("Illegal service provider class name: " + serviceName).toString());
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            }
        }
    }
}
