package kotlinx.coroutines.scheduling;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.internal.LockFreeTaskQueue;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0010\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lkotlinx/coroutines/scheduling/GlobalQueue;", "Lkotlinx/coroutines/internal/LockFreeTaskQueue;", "Lkotlinx/coroutines/scheduling/Task;", "()V", "removeFirstWithModeOrNull", "mode", "Lkotlinx/coroutines/scheduling/TaskMode;", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
/* compiled from: Tasks.kt */
public class GlobalQueue extends LockFreeTaskQueue<Task> {
    public GlobalQueue() {
        super(false);
    }

    @Nullable
    public final Task removeFirstWithModeOrNull(@NotNull TaskMode mode) {
        int $i$f$removeFirstOrNullIf;
        LockFreeTaskQueue $receiver$iv$iv;
        int $i$f$loop;
        Object obj;
        Object element$iv$iv;
        Object obj2 = mode;
        Intrinsics.checkParameterIsNotNull(obj2, "mode");
        int $i$f$removeFirstOrNullIf2 = 0;
        LockFreeTaskQueue $receiver$iv$iv2 = this;
        int $i$f$loop2 = 0;
        while (true) {
            LockFreeTaskQueueCore cur$iv = (LockFreeTaskQueueCore) $receiver$iv$iv2._cur$internal;
            LockFreeTaskQueueCore this_$iv$iv = cur$iv;
            LockFreeTaskQueueCore $receiver$iv$iv$iv = this_$iv$iv;
            while (true) {
                long state$iv$iv = $receiver$iv$iv$iv._state$internal;
                if ((LockFreeTaskQueueCore.FROZEN_MASK & state$iv$iv) != 0) {
                    $i$f$removeFirstOrNullIf = $i$f$removeFirstOrNullIf2;
                    $receiver$iv$iv = $receiver$iv$iv2;
                    $i$f$loop = $i$f$loop2;
                    obj = LockFreeTaskQueueCore.REMOVE_FROZEN;
                    break;
                }
                LockFreeTaskQueueCore.Companion companion = LockFreeTaskQueueCore.Companion;
                long $this$withState$iv$iv$iv = state$iv$iv;
                boolean z = false;
                int head$iv$iv$iv = (int) (($this$withState$iv$iv$iv & LockFreeTaskQueueCore.HEAD_MASK) >> 0);
                int head$iv$iv = head$iv$iv$iv;
                this_$iv$iv = this_$iv$iv;
                $i$f$removeFirstOrNullIf = $i$f$removeFirstOrNullIf2;
                $receiver$iv$iv = $receiver$iv$iv2;
                if ((((int) (($this$withState$iv$iv$iv & LockFreeTaskQueueCore.TAIL_MASK) >> 30)) & this_$iv$iv.mask) == (head$iv$iv & this_$iv$iv.mask)) {
                    $i$f$loop = $i$f$loop2;
                    obj = null;
                    break;
                }
                Object element$iv$iv2 = this_$iv$iv.array.get(this_$iv$iv.mask & head$iv$iv);
                if (element$iv$iv2 != null) {
                    if (!(element$iv$iv2 instanceof LockFreeTaskQueueCore.Placeholder)) {
                        element$iv$iv = element$iv$iv2;
                        if (((Task) element$iv$iv2).getMode() == obj2) {
                            z = true;
                        }
                        if (z) {
                            int newHead$iv$iv = (head$iv$iv + 1) & LockFreeTaskQueueCore.MAX_CAPACITY_MASK;
                            $i$f$loop = $i$f$loop2;
                            int head$iv$iv2 = head$iv$iv;
                            if (!LockFreeTaskQueueCore._state$FU$internal.compareAndSet(this_$iv$iv, state$iv$iv, LockFreeTaskQueueCore.Companion.updateHead(state$iv$iv, newHead$iv$iv))) {
                                if (this_$iv$iv.singleConsumer) {
                                    LockFreeTaskQueueCore cur$iv$iv = this_$iv$iv;
                                    while (true) {
                                        LockFreeTaskQueueCore access$removeSlowPath = cur$iv$iv.removeSlowPath(head$iv$iv2, newHead$iv$iv);
                                        if (access$removeSlowPath == null) {
                                            break;
                                        }
                                        cur$iv$iv = access$removeSlowPath;
                                    }
                                }
                            } else {
                                this_$iv$iv.array.set(this_$iv$iv.mask & head$iv$iv2, null);
                                break;
                            }
                        } else {
                            $i$f$loop = $i$f$loop2;
                            obj = null;
                            break;
                        }
                    } else {
                        $i$f$loop = $i$f$loop2;
                        obj = null;
                        break;
                    }
                } else if (this_$iv$iv.singleConsumer) {
                    $i$f$loop = $i$f$loop2;
                    obj = null;
                    break;
                } else {
                    $i$f$loop = $i$f$loop2;
                }
                obj2 = mode;
                $i$f$loop2 = $i$f$loop;
                $i$f$removeFirstOrNullIf2 = $i$f$removeFirstOrNullIf;
                $receiver$iv$iv2 = $receiver$iv$iv;
            }
            obj = element$iv$iv;
            Object result$iv = obj;
            if (result$iv != LockFreeTaskQueueCore.REMOVE_FROZEN) {
                return (Task) result$iv;
            }
            LockFreeTaskQueue._cur$FU$internal.compareAndSet(this, cur$iv, cur$iv.next());
            obj2 = mode;
            $i$f$loop2 = $i$f$loop;
            $i$f$removeFirstOrNullIf2 = $i$f$removeFirstOrNullIf;
            $receiver$iv$iv2 = $receiver$iv$iv;
        }
    }
}
