package kotlinx.coroutines;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0004\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0002\r\u000eB\u001b\u0012\u0014\u0010\u0003\u001a\u0010\u0012\f\b\u0001\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\u0004¢\u0006\u0002\u0010\u0006J\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\u000bH@ø\u0001\u0000¢\u0006\u0002\u0010\fR\u001e\u0010\u0003\u001a\u0010\u0012\f\b\u0001\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\u0004X\u0004¢\u0006\u0004\n\u0002\u0010\u0007R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u000f"}, d2 = {"Lkotlinx/coroutines/AwaitAll;", "T", "", "deferreds", "", "Lkotlinx/coroutines/Deferred;", "([Lkotlinx/coroutines/Deferred;)V", "[Lkotlinx/coroutines/Deferred;", "notCompletedCount", "Lkotlinx/atomicfu/AtomicInt;", "await", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "AwaitAllNode", "DisposeHandlersOnCancel", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
/* compiled from: Await.kt */
final class AwaitAll<T> {
    static final AtomicIntegerFieldUpdater notCompletedCount$FU = AtomicIntegerFieldUpdater.newUpdater(AwaitAll.class, "notCompletedCount");
    /* access modifiers changed from: private */
    public final Deferred<T>[] deferreds;
    volatile int notCompletedCount = this.deferreds.length;

    public AwaitAll(@NotNull Deferred<? extends T>[] deferreds2) {
        Intrinsics.checkParameterIsNotNull(deferreds2, "deferreds");
        this.deferreds = deferreds2;
    }

    /* JADX INFO: Multiple debug info for r1v2 kotlinx.coroutines.AwaitAll$AwaitAllNode[]: [D('nodes' kotlinx.coroutines.AwaitAll$AwaitAllNode[]), D('$i$f$suspendCancellableCoroutine' int)] */
    @Nullable
    public final Object await(@NotNull Continuation<? super List<? extends T>> $completion) {
        int $i$f$suspendCancellableCoroutine = 0;
        Continuation uCont$iv = $completion;
        CancellableContinuationImpl cancellable$iv = new CancellableContinuationImpl(IntrinsicsKt.intercepted(uCont$iv), 1);
        CancellableContinuation cont = cancellable$iv;
        AwaitAllNode[] awaitAllNodeArr = new AwaitAllNode[this.deferreds.length];
        int length = awaitAllNodeArr.length;
        int i$iv = 0;
        while (i$iv < length) {
            Deferred deferred = this.deferreds[Boxing.boxInt(i$iv).intValue()];
            deferred.start();
            int $i$f$suspendCancellableCoroutine2 = $i$f$suspendCancellableCoroutine;
            AwaitAllNode awaitAllNode = new AwaitAllNode(this, cont, deferred);
            AwaitAllNode $this$apply = awaitAllNode;
            $this$apply.setHandle(deferred.invokeOnCompletion($this$apply));
            awaitAllNodeArr[i$iv] = awaitAllNode;
            i$iv++;
            $i$f$suspendCancellableCoroutine = $i$f$suspendCancellableCoroutine2;
            uCont$iv = uCont$iv;
        }
        AwaitAllNode[] nodes = awaitAllNodeArr;
        DisposeHandlersOnCancel disposer = new DisposeHandlersOnCancel(this, nodes);
        for (AwaitAllNode it : nodes) {
            it.setDisposer(disposer);
        }
        if (cont.isCompleted()) {
            disposer.disposeAll();
        } else {
            cont.invokeOnCancellation(disposer);
        }
        Object result = cancellable$iv.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended($completion);
        }
        return result;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u001d\u0012\u0016\u0010\u0002\u001a\u0012\u0012\u000e\u0012\f0\u0004R\b\u0012\u0004\u0012\u00028\u00000\u00050\u0003¢\u0006\u0002\u0010\u0006J\u0006\u0010\b\u001a\u00020\tJ\u0013\u0010\n\u001a\u00020\t2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016R \u0010\u0002\u001a\u0012\u0012\u000e\u0012\f0\u0004R\b\u0012\u0004\u0012\u00028\u00000\u00050\u0003X\u0004¢\u0006\u0004\n\u0002\u0010\u0007¨\u0006\u000f"}, d2 = {"Lkotlinx/coroutines/AwaitAll$DisposeHandlersOnCancel;", "Lkotlinx/coroutines/CancelHandler;", "nodes", "", "Lkotlinx/coroutines/AwaitAll$AwaitAllNode;", "Lkotlinx/coroutines/AwaitAll;", "(Lkotlinx/coroutines/AwaitAll;[Lkotlinx/coroutines/AwaitAll$AwaitAllNode;)V", "[Lkotlinx/coroutines/AwaitAll$AwaitAllNode;", "disposeAll", "", "invoke", "cause", "", "toString", "", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Await.kt */
    private final class DisposeHandlersOnCancel extends CancelHandler {
        private final AwaitAll<T>.AwaitAllNode[] nodes;
        final /* synthetic */ AwaitAll this$0;

        public DisposeHandlersOnCancel(@NotNull AwaitAll $outer, AwaitAll<T>.AwaitAllNode[] nodes2) {
            Intrinsics.checkParameterIsNotNull(nodes2, "nodes");
            this.this$0 = $outer;
            this.nodes = nodes2;
        }

        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Throwable) obj);
            return Unit.INSTANCE;
        }

        public final void disposeAll() {
            for (AwaitAll<T>.AwaitAllNode it : this.nodes) {
                it.getHandle().dispose();
            }
        }

        public void invoke(@Nullable Throwable cause) {
            disposeAll();
        }

        @NotNull
        public String toString() {
            return "DisposeHandlersOnCancel[" + this.nodes + ']';
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B!\u0012\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0002¢\u0006\u0002\u0010\u0007J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0002R\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R&\u0010\b\u001a\u000e\u0018\u00010\tR\b\u0012\u0004\u0012\u00028\u00000\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014¨\u0006\u0019"}, d2 = {"Lkotlinx/coroutines/AwaitAll$AwaitAllNode;", "Lkotlinx/coroutines/JobNode;", "Lkotlinx/coroutines/Job;", "continuation", "Lkotlinx/coroutines/CancellableContinuation;", "", "job", "(Lkotlinx/coroutines/AwaitAll;Lkotlinx/coroutines/CancellableContinuation;Lkotlinx/coroutines/Job;)V", "disposer", "Lkotlinx/coroutines/AwaitAll$DisposeHandlersOnCancel;", "Lkotlinx/coroutines/AwaitAll;", "getDisposer", "()Lkotlinx/coroutines/AwaitAll$DisposeHandlersOnCancel;", "setDisposer", "(Lkotlinx/coroutines/AwaitAll$DisposeHandlersOnCancel;)V", "handle", "Lkotlinx/coroutines/DisposableHandle;", "getHandle", "()Lkotlinx/coroutines/DisposableHandle;", "setHandle", "(Lkotlinx/coroutines/DisposableHandle;)V", "invoke", "", "cause", "", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Await.kt */
    private final class AwaitAllNode extends JobNode<Job> {
        private final CancellableContinuation<List<? extends T>> continuation;
        @Nullable
        private volatile AwaitAll<T>.DisposeHandlersOnCancel disposer;
        @NotNull
        public DisposableHandle handle;
        final /* synthetic */ AwaitAll this$0;

        /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Object, kotlinx.coroutines.CancellableContinuation<? super java.util.List<? extends T>>, kotlinx.coroutines.CancellableContinuation<java.util.List<? extends T>>] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public AwaitAllNode(@org.jetbrains.annotations.NotNull kotlinx.coroutines.AwaitAll r2, @org.jetbrains.annotations.NotNull kotlinx.coroutines.CancellableContinuation<? super java.util.List<? extends T>> r3, kotlinx.coroutines.Job r4) {
            /*
                r1 = this;
                java.lang.String r0 = "continuation"
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r3, r0)
                java.lang.String r0 = "job"
                kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r4, r0)
                r1.this$0 = r2
                r1.<init>(r4)
                r1.continuation = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.AwaitAll.AwaitAllNode.<init>(kotlinx.coroutines.AwaitAll, kotlinx.coroutines.CancellableContinuation, kotlinx.coroutines.Job):void");
        }

        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Throwable) obj);
            return Unit.INSTANCE;
        }

        @NotNull
        public final DisposableHandle getHandle() {
            DisposableHandle disposableHandle = this.handle;
            if (disposableHandle == null) {
                Intrinsics.throwUninitializedPropertyAccessException("handle");
            }
            return disposableHandle;
        }

        public final void setHandle(@NotNull DisposableHandle disposableHandle) {
            Intrinsics.checkParameterIsNotNull(disposableHandle, "<set-?>");
            this.handle = disposableHandle;
        }

        @Nullable
        public final AwaitAll<T>.DisposeHandlersOnCancel getDisposer() {
            return this.disposer;
        }

        public final void setDisposer(@Nullable AwaitAll<T>.DisposeHandlersOnCancel disposeHandlersOnCancel) {
            this.disposer = disposeHandlersOnCancel;
        }

        public void invoke(@Nullable Throwable cause) {
            if (cause != null) {
                Object token = this.continuation.tryResumeWithException(cause);
                if (token != null) {
                    this.continuation.completeResume(token);
                    DisposeHandlersOnCancel disposer2 = this.disposer;
                    if (disposer2 != null) {
                        disposer2.disposeAll();
                        return;
                    }
                    return;
                }
                return;
            }
            if (AwaitAll.notCompletedCount$FU.decrementAndGet(this.this$0) == 0) {
                Continuation continuation2 = this.continuation;
                Deferred[] access$getDeferreds$p = this.this$0.deferreds;
                Collection destination$iv$iv = new ArrayList(access$getDeferreds$p.length);
                for (Deferred it : access$getDeferreds$p) {
                    destination$iv$iv.add(it.getCompleted());
                }
                Result.Companion companion = Result.Companion;
                continuation2.resumeWith(Result.m3constructorimpl((List) destination$iv$iv));
            }
        }
    }
}
