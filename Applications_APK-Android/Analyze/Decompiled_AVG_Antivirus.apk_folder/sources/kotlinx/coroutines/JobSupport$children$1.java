package kotlinx.coroutines;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.RestrictedSuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.SequenceScope;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00030\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "Lkotlin/sequences/SequenceScope;", "Lkotlinx/coroutines/ChildJob;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.JobSupport$children$1", f = "JobSupport.kt", i = {0, 1, 1, 1, 1, 1}, l = {869, 871}, m = "invokeSuspend", n = {"state", "state", "list", "this_$iv", "cur$iv", "it"}, s = {"L$0", "L$1", "L$2", "L$3", "L$4", "L$5"})
/* compiled from: JobSupport.kt */
final class JobSupport$children$1 extends RestrictedSuspendLambda implements Function2<SequenceScope<? super ChildJob>, Continuation<? super Unit>, Object> {
    Object L$0;
    Object L$1;
    Object L$2;
    Object L$3;
    Object L$4;
    Object L$5;
    int label;
    private SequenceScope p$;
    final /* synthetic */ JobSupport this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    JobSupport$children$1(JobSupport jobSupport, Continuation continuation) {
        super(2, continuation);
        this.this$0 = jobSupport;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        JobSupport$children$1 jobSupport$children$1 = new JobSupport$children$1(this.this$0, continuation);
        SequenceScope sequenceScope = (SequenceScope) obj;
        jobSupport$children$1.p$ = (SequenceScope) obj;
        return jobSupport$children$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((JobSupport$children$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v7, resolved type: kotlinx.coroutines.internal.LockFreeLinkedListNode} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x009d  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r18) {
        /*
            r17 = this;
            r0 = r17
            java.lang.Object r1 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x004c
            r5 = 0
            if (r2 == r4) goto L_0x0045
            if (r2 != r3) goto L_0x003d
            r2 = r5
            r6 = 0
            r7 = r6
            r8 = r5
            r9 = r5
            r10 = r6
            r11 = r5
            java.lang.Object r12 = r0.L$5
            r2 = r12
            kotlinx.coroutines.ChildHandleNode r2 = (kotlinx.coroutines.ChildHandleNode) r2
            java.lang.Object r12 = r0.L$4
            r8 = r12
            kotlinx.coroutines.internal.LockFreeLinkedListNode r8 = (kotlinx.coroutines.internal.LockFreeLinkedListNode) r8
            java.lang.Object r12 = r0.L$3
            r9 = r12
            kotlinx.coroutines.NodeList r9 = (kotlinx.coroutines.NodeList) r9
            java.lang.Object r12 = r0.L$2
            r11 = r12
            kotlinx.coroutines.NodeList r11 = (kotlinx.coroutines.NodeList) r11
            java.lang.Object r5 = r0.L$1
            java.lang.Object r12 = r0.L$0
            kotlin.sequences.SequenceScope r12 = (kotlin.sequences.SequenceScope) r12
            kotlin.ResultKt.throwOnFailure(r18)
            r13 = r1
            r14 = r7
            r1 = r0
            r6 = r5
            r5 = r18
            goto L_0x00c4
        L_0x003d:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0045:
            r1 = r5
            java.lang.Object r1 = r0.L$0
            kotlin.ResultKt.throwOnFailure(r18)
            goto L_0x006c
        L_0x004c:
            kotlin.ResultKt.throwOnFailure(r18)
            kotlin.sequences.SequenceScope r2 = r0.p$
            kotlinx.coroutines.JobSupport r5 = r0.this$0
            java.lang.Object r5 = r5.getState$kotlinx_coroutines_core()
            boolean r6 = r5 instanceof kotlinx.coroutines.ChildHandleNode
            if (r6 == 0) goto L_0x006e
            r3 = r5
            kotlinx.coroutines.ChildHandleNode r3 = (kotlinx.coroutines.ChildHandleNode) r3
            kotlinx.coroutines.ChildJob r3 = r3.childJob
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r2 = r2.yield(r3, r0)
            if (r2 != r1) goto L_0x006b
            return r1
        L_0x006b:
            r1 = r5
        L_0x006c:
            goto L_0x00d8
        L_0x006e:
            boolean r6 = r5 instanceof kotlinx.coroutines.Incomplete
            if (r6 == 0) goto L_0x00d8
            r6 = r5
            kotlinx.coroutines.Incomplete r6 = (kotlinx.coroutines.Incomplete) r6
            kotlinx.coroutines.NodeList r6 = r6.getList()
            if (r6 == 0) goto L_0x00d8
            r7 = 0
            r8 = r6
            r9 = 0
            java.lang.Object r10 = r8.getNext()
            if (r10 == 0) goto L_0x00d0
            kotlinx.coroutines.internal.LockFreeLinkedListNode r10 = (kotlinx.coroutines.internal.LockFreeLinkedListNode) r10
            r12 = r2
            r11 = r6
            r6 = r7
            r2 = r18
            r7 = r1
            r1 = r0
            r16 = r9
            r9 = r8
            r8 = r10
            r10 = r16
        L_0x0093:
            r13 = r9
            kotlinx.coroutines.internal.LockFreeLinkedListHead r13 = (kotlinx.coroutines.internal.LockFreeLinkedListHead) r13
            boolean r13 = kotlin.jvm.internal.Intrinsics.areEqual(r8, r13)
            r13 = r13 ^ r4
            if (r13 == 0) goto L_0x00cd
            boolean r13 = r8 instanceof kotlinx.coroutines.ChildHandleNode
            if (r13 == 0) goto L_0x00c8
            r13 = r8
            kotlinx.coroutines.ChildHandleNode r13 = (kotlinx.coroutines.ChildHandleNode) r13
            r14 = 0
            kotlinx.coroutines.ChildJob r15 = r13.childJob
            r1.L$0 = r12
            r1.L$1 = r5
            r1.L$2 = r11
            r1.L$3 = r9
            r1.L$4 = r8
            r1.L$5 = r13
            r1.label = r3
            java.lang.Object r15 = r12.yield(r15, r1)
            if (r15 != r7) goto L_0x00bc
            return r7
        L_0x00bc:
            r16 = r5
            r5 = r2
            r2 = r13
            r13 = r7
            r7 = r6
            r6 = r16
        L_0x00c4:
            r2 = r5
            r5 = r6
            r6 = r7
            r7 = r13
        L_0x00c8:
            kotlinx.coroutines.internal.LockFreeLinkedListNode r8 = r8.getNextNode()
            goto L_0x0093
        L_0x00cd:
            goto L_0x00db
        L_0x00d0:
            kotlin.TypeCastException r1 = new kotlin.TypeCastException
            java.lang.String r2 = "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"
            r1.<init>(r2)
            throw r1
        L_0x00d8:
            r2 = r18
            r1 = r0
        L_0x00db:
            kotlin.Unit r3 = kotlin.Unit.INSTANCE
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.JobSupport$children$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
