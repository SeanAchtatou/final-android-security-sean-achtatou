package kotlinx.coroutines.channels;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.PublishedApi;
import kotlin.Unit;
import kotlin.collections.IndexedValue;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlinx.coroutines.ObsoleteCoroutinesApi;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"kotlinx/coroutines/channels/ChannelsKt__ChannelsKt", "kotlinx/coroutines/channels/ChannelsKt__Channels_commonKt"}, k = 4, mv = {1, 1, 15})
public final class ChannelsKt {
    @NotNull
    public static final String DEFAULT_CLOSE_MESSAGE = "Channel was closed";

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object all(@NotNull ReceiveChannel<? extends E> $this$all, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super Boolean> $completion) {
        return ChannelsKt__Channels_commonKt.all($this$all, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object all$$forInline(@NotNull ReceiveChannel $this$all, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.all($this$all, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object any(@NotNull ReceiveChannel<? extends E> $this$any, @NotNull Continuation<? super Boolean> $completion) {
        return ChannelsKt__Channels_commonKt.any($this$any, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object any(@NotNull ReceiveChannel<? extends E> $this$any, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super Boolean> $completion) {
        return ChannelsKt__Channels_commonKt.any($this$any, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object any$$forInline(@NotNull ReceiveChannel $this$any, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.any($this$any, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, K, V> Object associate(@NotNull ReceiveChannel<? extends E> $this$associate, @NotNull Function1<? super E, ? extends Pair<? extends K, ? extends V>> transform, @NotNull Continuation<? super Map<K, ? extends V>> $completion) {
        return ChannelsKt__Channels_commonKt.associate($this$associate, transform, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object associate$$forInline(@NotNull ReceiveChannel $this$associate, @NotNull Function1 transform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associate($this$associate, transform, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, K> Object associateBy(@NotNull ReceiveChannel<? extends E> $this$associateBy, @NotNull Function1<? super E, ? extends K> keySelector, @NotNull Continuation<? super Map<K, ? extends E>> $completion) {
        return ChannelsKt__Channels_commonKt.associateBy($this$associateBy, keySelector, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, K, V> Object associateBy(@NotNull ReceiveChannel<? extends E> $this$associateBy, @NotNull Function1<? super E, ? extends K> keySelector, @NotNull Function1<? super E, ? extends V> valueTransform, @NotNull Continuation<? super Map<K, ? extends V>> $completion) {
        return ChannelsKt__Channels_commonKt.associateBy($this$associateBy, keySelector, valueTransform, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object associateBy$$forInline(@NotNull ReceiveChannel $this$associateBy, @NotNull Function1 keySelector, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associateBy($this$associateBy, keySelector, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object associateBy$$forInline(@NotNull ReceiveChannel $this$associateBy, @NotNull Function1 keySelector, @NotNull Function1 valueTransform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associateBy($this$associateBy, keySelector, valueTransform, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, K, M extends Map<? super K, ? super E>> Object associateByTo(@NotNull ReceiveChannel<? extends E> $this$associateByTo, @NotNull M destination, @NotNull Function1<? super E, ? extends K> keySelector, @NotNull Continuation<? super M> $completion) {
        return ChannelsKt__Channels_commonKt.associateByTo($this$associateByTo, destination, keySelector, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, K, V, M extends Map<? super K, ? super V>> Object associateByTo(@NotNull ReceiveChannel<? extends E> $this$associateByTo, @NotNull M destination, @NotNull Function1<? super E, ? extends K> keySelector, @NotNull Function1<? super E, ? extends V> valueTransform, @NotNull Continuation<? super M> $completion) {
        return ChannelsKt__Channels_commonKt.associateByTo($this$associateByTo, destination, keySelector, valueTransform, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object associateByTo$$forInline(@NotNull ReceiveChannel $this$associateByTo, @NotNull Map destination, @NotNull Function1 keySelector, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associateByTo($this$associateByTo, destination, keySelector, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object associateByTo$$forInline(@NotNull ReceiveChannel $this$associateByTo, @NotNull Map destination, @NotNull Function1 keySelector, @NotNull Function1 valueTransform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associateByTo($this$associateByTo, destination, keySelector, valueTransform, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, K, V, M extends Map<? super K, ? super V>> Object associateTo(@NotNull ReceiveChannel<? extends E> $this$associateTo, @NotNull M destination, @NotNull Function1<? super E, ? extends Pair<? extends K, ? extends V>> transform, @NotNull Continuation<? super M> $completion) {
        return ChannelsKt__Channels_commonKt.associateTo($this$associateTo, destination, transform, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object associateTo$$forInline(@NotNull ReceiveChannel $this$associateTo, @NotNull Map destination, @NotNull Function1 transform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associateTo($this$associateTo, destination, transform, continuation);
    }

    @PublishedApi
    public static final void cancelConsumed(@NotNull ReceiveChannel<?> $this$cancelConsumed, @Nullable Throwable cause) {
        ChannelsKt__Channels_commonKt.cancelConsumed($this$cancelConsumed, cause);
    }

    @ObsoleteCoroutinesApi
    public static final <E, R> R consume(@NotNull BroadcastChannel<E> $this$consume, @NotNull Function1<? super ReceiveChannel<? extends E>, ? extends R> block) {
        return ChannelsKt__Channels_commonKt.consume($this$consume, block);
    }

    @ObsoleteCoroutinesApi
    public static final <E, R> R consume(@NotNull ReceiveChannel<? extends E> $this$consume, @NotNull Function1<? super ReceiveChannel<? extends E>, ? extends R> block) {
        return ChannelsKt__Channels_commonKt.consume($this$consume, block);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object consumeEach(@NotNull BroadcastChannel<E> $this$consumeEach, @NotNull Function1<? super E, Unit> action, @NotNull Continuation<? super Unit> $completion) {
        return ChannelsKt__Channels_commonKt.consumeEach($this$consumeEach, action, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object consumeEach(@NotNull ReceiveChannel<? extends E> $this$consumeEach, @NotNull Function1<? super E, Unit> action, @NotNull Continuation<? super Unit> $completion) {
        return ChannelsKt__Channels_commonKt.consumeEach($this$consumeEach, action, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object consumeEach$$forInline(@NotNull BroadcastChannel $this$consumeEach, @NotNull Function1 action, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.consumeEach($this$consumeEach, action, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object consumeEach$$forInline(@NotNull ReceiveChannel $this$consumeEach, @NotNull Function1 action, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.consumeEach($this$consumeEach, action, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object consumeEachIndexed(@NotNull ReceiveChannel<? extends E> $this$consumeEachIndexed, @NotNull Function1<? super IndexedValue<? extends E>, Unit> action, @NotNull Continuation<? super Unit> $completion) {
        return ChannelsKt__Channels_commonKt.consumeEachIndexed($this$consumeEachIndexed, action, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object consumeEachIndexed$$forInline(@NotNull ReceiveChannel $this$consumeEachIndexed, @NotNull Function1 action, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.consumeEachIndexed($this$consumeEachIndexed, action, continuation);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final Function1<Throwable, Unit> consumes(@NotNull ReceiveChannel<?> $this$consumes) {
        return ChannelsKt__Channels_commonKt.consumes($this$consumes);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final Function1<Throwable, Unit> consumesAll(@NotNull ReceiveChannel<?>... channels) {
        return ChannelsKt__Channels_commonKt.consumesAll(channels);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object count(@NotNull ReceiveChannel<? extends E> $this$count, @NotNull Continuation<? super Integer> $completion) {
        return ChannelsKt__Channels_commonKt.count($this$count, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object count(@NotNull ReceiveChannel<? extends E> $this$count, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super Integer> $completion) {
        return ChannelsKt__Channels_commonKt.count($this$count, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object count$$forInline(@NotNull ReceiveChannel $this$count, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.count($this$count, predicate, continuation);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<E> distinct(@NotNull ReceiveChannel<? extends E> $this$distinct) {
        return ChannelsKt__Channels_commonKt.distinct($this$distinct);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E, K> ReceiveChannel<E> distinctBy(@NotNull ReceiveChannel<? extends E> $this$distinctBy, @NotNull CoroutineContext context, @NotNull Function2<? super E, ? super Continuation<? super K>, ? extends Object> selector) {
        return ChannelsKt__Channels_commonKt.distinctBy($this$distinctBy, context, selector);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<E> drop(@NotNull ReceiveChannel<? extends E> $this$drop, int n, @NotNull CoroutineContext context) {
        return ChannelsKt__Channels_commonKt.drop($this$drop, n, context);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<E> dropWhile(@NotNull ReceiveChannel<? extends E> $this$dropWhile, @NotNull CoroutineContext context, @NotNull Function2<? super E, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        return ChannelsKt__Channels_commonKt.dropWhile($this$dropWhile, context, predicate);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object elementAt(@NotNull ReceiveChannel<? extends E> $this$elementAt, int index, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.elementAt($this$elementAt, index, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object elementAtOrElse(@NotNull ReceiveChannel<? extends E> $this$elementAtOrElse, int index, @NotNull Function1<? super Integer, ? extends E> defaultValue, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.elementAtOrElse($this$elementAtOrElse, index, defaultValue, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object elementAtOrElse$$forInline(@NotNull ReceiveChannel $this$elementAtOrElse, int index, @NotNull Function1 defaultValue, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.elementAtOrElse($this$elementAtOrElse, index, defaultValue, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object elementAtOrNull(@NotNull ReceiveChannel<? extends E> $this$elementAtOrNull, int index, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.elementAtOrNull($this$elementAtOrNull, index, $completion);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<E> filter(@NotNull ReceiveChannel<? extends E> $this$filter, @NotNull CoroutineContext context, @NotNull Function2<? super E, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        return ChannelsKt__Channels_commonKt.filter($this$filter, context, predicate);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<E> filterIndexed(@NotNull ReceiveChannel<? extends E> $this$filterIndexed, @NotNull CoroutineContext context, @NotNull Function3<? super Integer, ? super E, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        return ChannelsKt__Channels_commonKt.filterIndexed($this$filterIndexed, context, predicate);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super E, java.lang.Boolean>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, C extends Collection<? super E>> Object filterIndexedTo(@NotNull ReceiveChannel<? extends E> $this$filterIndexedTo, @NotNull C destination, @NotNull Function2<? super Integer, ? super E, Boolean> predicate, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.filterIndexedTo((ReceiveChannel) $this$filterIndexedTo, (Collection) destination, (Function2) predicate, (Continuation) $completion);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super E, java.lang.Boolean>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, C extends SendChannel<? super E>> Object filterIndexedTo(@NotNull ReceiveChannel<? extends E> $this$filterIndexedTo, @NotNull C destination, @NotNull Function2<? super Integer, ? super E, Boolean> predicate, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.filterIndexedTo((ReceiveChannel) $this$filterIndexedTo, (SendChannel) destination, (Function2) predicate, (Continuation) $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object filterIndexedTo$$forInline(@NotNull ReceiveChannel $this$filterIndexedTo, @NotNull Collection destination, @NotNull Function2 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterIndexedTo($this$filterIndexedTo, destination, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object filterIndexedTo$$forInline(@NotNull ReceiveChannel $this$filterIndexedTo, @NotNull SendChannel destination, @NotNull Function2 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterIndexedTo($this$filterIndexedTo, destination, predicate, continuation);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<E> filterNot(@NotNull ReceiveChannel<? extends E> $this$filterNot, @NotNull CoroutineContext context, @NotNull Function2<? super E, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        return ChannelsKt__Channels_commonKt.filterNot($this$filterNot, context, predicate);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<E> filterNotNull(@NotNull ReceiveChannel<? extends E> $this$filterNotNull) {
        return ChannelsKt__Channels_commonKt.filterNotNull($this$filterNotNull);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, C extends Collection<? super E>> Object filterNotNullTo(@NotNull ReceiveChannel<? extends E> $this$filterNotNullTo, @NotNull C destination, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.filterNotNullTo((ReceiveChannel) $this$filterNotNullTo, (Collection) destination, (Continuation) $completion);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, C extends SendChannel<? super E>> Object filterNotNullTo(@NotNull ReceiveChannel<? extends E> $this$filterNotNullTo, @NotNull C destination, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.filterNotNullTo((ReceiveChannel) $this$filterNotNullTo, (SendChannel) destination, (Continuation) $completion);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function1<? super E, java.lang.Boolean>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, C extends Collection<? super E>> Object filterNotTo(@NotNull ReceiveChannel<? extends E> $this$filterNotTo, @NotNull C destination, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.filterNotTo((ReceiveChannel) $this$filterNotTo, (Collection) destination, (Function1) predicate, (Continuation) $completion);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function1<? super E, java.lang.Boolean>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterNotTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, C extends SendChannel<? super E>> Object filterNotTo(@NotNull ReceiveChannel<? extends E> $this$filterNotTo, @NotNull C destination, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.filterNotTo((ReceiveChannel) $this$filterNotTo, (SendChannel) destination, (Function1) predicate, (Continuation) $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object filterNotTo$$forInline(@NotNull ReceiveChannel $this$filterNotTo, @NotNull Collection destination, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterNotTo($this$filterNotTo, destination, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object filterNotTo$$forInline(@NotNull ReceiveChannel $this$filterNotTo, @NotNull SendChannel destination, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterNotTo($this$filterNotTo, destination, predicate, continuation);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function1<? super E, java.lang.Boolean>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, C extends Collection<? super E>> Object filterTo(@NotNull ReceiveChannel<? extends E> $this$filterTo, @NotNull C destination, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.filterTo((ReceiveChannel) $this$filterTo, (Collection) destination, (Function1) predicate, (Continuation) $completion);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function1<? super E, java.lang.Boolean>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.filterTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, C extends SendChannel<? super E>> Object filterTo(@NotNull ReceiveChannel<? extends E> $this$filterTo, @NotNull C destination, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.filterTo((ReceiveChannel) $this$filterTo, (SendChannel) destination, (Function1) predicate, (Continuation) $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object filterTo$$forInline(@NotNull ReceiveChannel $this$filterTo, @NotNull Collection destination, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterTo($this$filterTo, destination, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object filterTo$$forInline(@NotNull ReceiveChannel $this$filterTo, @NotNull SendChannel destination, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterTo($this$filterTo, destination, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object find(@NotNull ReceiveChannel<? extends E> $this$find, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.find($this$find, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object find$$forInline(@NotNull ReceiveChannel $this$find, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.find($this$find, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object findLast(@NotNull ReceiveChannel<? extends E> $this$findLast, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.findLast($this$findLast, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object findLast$$forInline(@NotNull ReceiveChannel $this$findLast, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.findLast($this$findLast, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object first(@NotNull ReceiveChannel<? extends E> $this$first, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.first($this$first, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object first(@NotNull ReceiveChannel<? extends E> $this$first, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.first($this$first, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object first$$forInline(@NotNull ReceiveChannel $this$first, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.first($this$first, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object firstOrNull(@NotNull ReceiveChannel<? extends E> $this$firstOrNull, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.firstOrNull($this$firstOrNull, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object firstOrNull(@NotNull ReceiveChannel<? extends E> $this$firstOrNull, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.firstOrNull($this$firstOrNull, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object firstOrNull$$forInline(@NotNull ReceiveChannel $this$firstOrNull, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.firstOrNull($this$firstOrNull, predicate, continuation);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E, R> ReceiveChannel<R> flatMap(@NotNull ReceiveChannel<? extends E> $this$flatMap, @NotNull CoroutineContext context, @NotNull Function2<? super E, ? super Continuation<? super ReceiveChannel<? extends R>>, ? extends Object> transform) {
        return ChannelsKt__Channels_commonKt.flatMap($this$flatMap, context, transform);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R> Object fold(@NotNull ReceiveChannel<? extends E> $this$fold, R initial, @NotNull Function2<? super R, ? super E, ? extends R> operation, @NotNull Continuation<? super R> $completion) {
        return ChannelsKt__Channels_commonKt.fold($this$fold, initial, operation, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object fold$$forInline(@NotNull ReceiveChannel $this$fold, Object initial, @NotNull Function2 operation, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.fold($this$fold, initial, operation, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R> Object foldIndexed(@NotNull ReceiveChannel<? extends E> $this$foldIndexed, R initial, @NotNull Function3<? super Integer, ? super R, ? super E, ? extends R> operation, @NotNull Continuation<? super R> $completion) {
        return ChannelsKt__Channels_commonKt.foldIndexed($this$foldIndexed, initial, operation, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object foldIndexed$$forInline(@NotNull ReceiveChannel $this$foldIndexed, Object initial, @NotNull Function3 operation, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.foldIndexed($this$foldIndexed, initial, operation, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, K> Object groupBy(@NotNull ReceiveChannel<? extends E> $this$groupBy, @NotNull Function1<? super E, ? extends K> keySelector, @NotNull Continuation<? super Map<K, ? extends List<? extends E>>> $completion) {
        return ChannelsKt__Channels_commonKt.groupBy($this$groupBy, keySelector, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, K, V> Object groupBy(@NotNull ReceiveChannel<? extends E> $this$groupBy, @NotNull Function1<? super E, ? extends K> keySelector, @NotNull Function1<? super E, ? extends V> valueTransform, @NotNull Continuation<? super Map<K, ? extends List<? extends V>>> $completion) {
        return ChannelsKt__Channels_commonKt.groupBy($this$groupBy, keySelector, valueTransform, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object groupBy$$forInline(@NotNull ReceiveChannel $this$groupBy, @NotNull Function1 keySelector, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.groupBy($this$groupBy, keySelector, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object groupBy$$forInline(@NotNull ReceiveChannel $this$groupBy, @NotNull Function1 keySelector, @NotNull Function1 valueTransform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.groupBy($this$groupBy, keySelector, valueTransform, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, K, M extends Map<? super K, List<E>>> Object groupByTo(@NotNull ReceiveChannel<? extends E> $this$groupByTo, @NotNull M destination, @NotNull Function1<? super E, ? extends K> keySelector, @NotNull Continuation<? super M> $completion) {
        return ChannelsKt__Channels_commonKt.groupByTo($this$groupByTo, destination, keySelector, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, K, V, M extends Map<? super K, List<V>>> Object groupByTo(@NotNull ReceiveChannel<? extends E> $this$groupByTo, @NotNull M destination, @NotNull Function1<? super E, ? extends K> keySelector, @NotNull Function1<? super E, ? extends V> valueTransform, @NotNull Continuation<? super M> $completion) {
        return ChannelsKt__Channels_commonKt.groupByTo($this$groupByTo, destination, keySelector, valueTransform, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object groupByTo$$forInline(@NotNull ReceiveChannel $this$groupByTo, @NotNull Map destination, @NotNull Function1 keySelector, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.groupByTo($this$groupByTo, destination, keySelector, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object groupByTo$$forInline(@NotNull ReceiveChannel $this$groupByTo, @NotNull Map destination, @NotNull Function1 keySelector, @NotNull Function1 valueTransform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.groupByTo($this$groupByTo, destination, keySelector, valueTransform, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object indexOf(@NotNull ReceiveChannel<? extends E> $this$indexOf, E element, @NotNull Continuation<? super Integer> $completion) {
        return ChannelsKt__Channels_commonKt.indexOf($this$indexOf, element, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object indexOfFirst(@NotNull ReceiveChannel<? extends E> $this$indexOfFirst, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super Integer> $completion) {
        return ChannelsKt__Channels_commonKt.indexOfFirst($this$indexOfFirst, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object indexOfFirst$$forInline(@NotNull ReceiveChannel $this$indexOfFirst, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.indexOfFirst($this$indexOfFirst, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object indexOfLast(@NotNull ReceiveChannel<? extends E> $this$indexOfLast, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super Integer> $completion) {
        return ChannelsKt__Channels_commonKt.indexOfLast($this$indexOfLast, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object indexOfLast$$forInline(@NotNull ReceiveChannel $this$indexOfLast, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.indexOfLast($this$indexOfLast, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object last(@NotNull ReceiveChannel<? extends E> $this$last, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.last($this$last, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object last(@NotNull ReceiveChannel<? extends E> $this$last, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.last($this$last, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object last$$forInline(@NotNull ReceiveChannel $this$last, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.last($this$last, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object lastIndexOf(@NotNull ReceiveChannel<? extends E> $this$lastIndexOf, E element, @NotNull Continuation<? super Integer> $completion) {
        return ChannelsKt__Channels_commonKt.lastIndexOf($this$lastIndexOf, element, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object lastOrNull(@NotNull ReceiveChannel<? extends E> $this$lastOrNull, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.lastOrNull($this$lastOrNull, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object lastOrNull(@NotNull ReceiveChannel<? extends E> $this$lastOrNull, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.lastOrNull($this$lastOrNull, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object lastOrNull$$forInline(@NotNull ReceiveChannel $this$lastOrNull, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.lastOrNull($this$lastOrNull, predicate, continuation);
    }

    @NotNull
    public static final <E, R> ReceiveChannel<R> map(@NotNull ReceiveChannel<? extends E> $this$map, @NotNull CoroutineContext context, @NotNull Function2<? super E, ? super Continuation<? super R>, ? extends Object> transform) {
        return ChannelsKt__Channels_commonKt.map($this$map, context, transform);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E, R> ReceiveChannel<R> mapIndexed(@NotNull ReceiveChannel<? extends E> $this$mapIndexed, @NotNull CoroutineContext context, @NotNull Function3<? super Integer, ? super E, ? super Continuation<? super R>, ? extends Object> transform) {
        return ChannelsKt__Channels_commonKt.mapIndexed($this$mapIndexed, context, transform);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E, R> ReceiveChannel<R> mapIndexedNotNull(@NotNull ReceiveChannel<? extends E> $this$mapIndexedNotNull, @NotNull CoroutineContext context, @NotNull Function3<? super Integer, ? super E, ? super Continuation<? super R>, ? extends Object> transform) {
        return ChannelsKt__Channels_commonKt.mapIndexedNotNull($this$mapIndexedNotNull, context, transform);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super E, ? extends R>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R, C extends Collection<? super R>> Object mapIndexedNotNullTo(@NotNull ReceiveChannel<? extends E> $this$mapIndexedNotNullTo, @NotNull C destination, @NotNull Function2<? super Integer, ? super E, ? extends R> transform, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.mapIndexedNotNullTo((ReceiveChannel) $this$mapIndexedNotNullTo, (Collection) destination, (Function2) transform, (Continuation) $completion);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super E, ? extends R>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R, C extends SendChannel<? super R>> Object mapIndexedNotNullTo(@NotNull ReceiveChannel<? extends E> $this$mapIndexedNotNullTo, @NotNull C destination, @NotNull Function2<? super Integer, ? super E, ? extends R> transform, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.mapIndexedNotNullTo((ReceiveChannel) $this$mapIndexedNotNullTo, (SendChannel) destination, (Function2) transform, (Continuation) $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object mapIndexedNotNullTo$$forInline(@NotNull ReceiveChannel $this$mapIndexedNotNullTo, @NotNull Collection destination, @NotNull Function2 transform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedNotNullTo($this$mapIndexedNotNullTo, destination, transform, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object mapIndexedNotNullTo$$forInline(@NotNull ReceiveChannel $this$mapIndexedNotNullTo, @NotNull SendChannel destination, @NotNull Function2 transform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedNotNullTo($this$mapIndexedNotNullTo, destination, transform, continuation);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super E, ? extends R>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R, C extends Collection<? super R>> Object mapIndexedTo(@NotNull ReceiveChannel<? extends E> $this$mapIndexedTo, @NotNull C destination, @NotNull Function2<? super Integer, ? super E, ? extends R> transform, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.mapIndexedTo((ReceiveChannel) $this$mapIndexedTo, (Collection) destination, (Function2) transform, (Continuation) $completion);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function2<? super java.lang.Integer, ? super E, ? extends R>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapIndexedTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R, C extends SendChannel<? super R>> Object mapIndexedTo(@NotNull ReceiveChannel<? extends E> $this$mapIndexedTo, @NotNull C destination, @NotNull Function2<? super Integer, ? super E, ? extends R> transform, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.mapIndexedTo((ReceiveChannel) $this$mapIndexedTo, (SendChannel) destination, (Function2) transform, (Continuation) $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object mapIndexedTo$$forInline(@NotNull ReceiveChannel $this$mapIndexedTo, @NotNull Collection destination, @NotNull Function2 transform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedTo($this$mapIndexedTo, destination, transform, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object mapIndexedTo$$forInline(@NotNull ReceiveChannel $this$mapIndexedTo, @NotNull SendChannel destination, @NotNull Function2 transform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedTo($this$mapIndexedTo, destination, transform, continuation);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E, R> ReceiveChannel<R> mapNotNull(@NotNull ReceiveChannel<? extends E> $this$mapNotNull, @NotNull CoroutineContext context, @NotNull Function2<? super E, ? super Continuation<? super R>, ? extends Object> transform) {
        return ChannelsKt__Channels_commonKt.mapNotNull($this$mapNotNull, context, transform);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function1<? super E, ? extends R>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R, C extends Collection<? super R>> Object mapNotNullTo(@NotNull ReceiveChannel<? extends E> $this$mapNotNullTo, @NotNull C destination, @NotNull Function1<? super E, ? extends R> transform, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.mapNotNullTo((ReceiveChannel) $this$mapNotNullTo, (Collection) destination, (Function1) transform, (Continuation) $completion);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function1<? super E, ? extends R>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapNotNullTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R, C extends SendChannel<? super R>> Object mapNotNullTo(@NotNull ReceiveChannel<? extends E> $this$mapNotNullTo, @NotNull C destination, @NotNull Function1<? super E, ? extends R> transform, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.mapNotNullTo((ReceiveChannel) $this$mapNotNullTo, (SendChannel) destination, (Function1) transform, (Continuation) $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object mapNotNullTo$$forInline(@NotNull ReceiveChannel $this$mapNotNullTo, @NotNull Collection destination, @NotNull Function1 transform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapNotNullTo($this$mapNotNullTo, destination, transform, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object mapNotNullTo$$forInline(@NotNull ReceiveChannel $this$mapNotNullTo, @NotNull SendChannel destination, @NotNull Function1 transform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapNotNullTo($this$mapNotNullTo, destination, transform, continuation);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function1<? super E, ? extends R>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R, C extends Collection<? super R>> Object mapTo(@NotNull ReceiveChannel<? extends E> $this$mapTo, @NotNull C destination, @NotNull Function1<? super E, ? extends R> transform, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.mapTo((ReceiveChannel) $this$mapTo, (Collection) destination, (Function1) transform, (Continuation) $completion);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
     arg types: [kotlinx.coroutines.channels.ReceiveChannel<? extends E>, C, kotlin.jvm.functions.Function1<? super E, ? extends R>, kotlin.coroutines.Continuation<? super C>]
     candidates:
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapTo(kotlinx.coroutines.channels.ReceiveChannel, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object
      kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt.mapTo(kotlinx.coroutines.channels.ReceiveChannel, kotlinx.coroutines.channels.SendChannel, kotlin.jvm.functions.Function1, kotlin.coroutines.Continuation):java.lang.Object */
    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R, C extends SendChannel<? super R>> Object mapTo(@NotNull ReceiveChannel<? extends E> $this$mapTo, @NotNull C destination, @NotNull Function1<? super E, ? extends R> transform, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.mapTo((ReceiveChannel) $this$mapTo, (SendChannel) destination, (Function1) transform, (Continuation) $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object mapTo$$forInline(@NotNull ReceiveChannel $this$mapTo, @NotNull Collection destination, @NotNull Function1 transform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapTo($this$mapTo, destination, transform, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object mapTo$$forInline(@NotNull ReceiveChannel $this$mapTo, @NotNull SendChannel destination, @NotNull Function1 transform, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapTo($this$mapTo, destination, transform, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R extends Comparable<? super R>> Object maxBy(@NotNull ReceiveChannel<? extends E> $this$maxBy, @NotNull Function1<? super E, ? extends R> selector, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.maxBy($this$maxBy, selector, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object maxBy$$forInline(@NotNull ReceiveChannel $this$maxBy, @NotNull Function1 selector, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.maxBy($this$maxBy, selector, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object maxWith(@NotNull ReceiveChannel<? extends E> $this$maxWith, @NotNull Comparator<? super E> comparator, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.maxWith($this$maxWith, comparator, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, R extends Comparable<? super R>> Object minBy(@NotNull ReceiveChannel<? extends E> $this$minBy, @NotNull Function1<? super E, ? extends R> selector, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.minBy($this$minBy, selector, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object minBy$$forInline(@NotNull ReceiveChannel $this$minBy, @NotNull Function1 selector, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.minBy($this$minBy, selector, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object minWith(@NotNull ReceiveChannel<? extends E> $this$minWith, @NotNull Comparator<? super E> comparator, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.minWith($this$minWith, comparator, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object none(@NotNull ReceiveChannel<? extends E> $this$none, @NotNull Continuation<? super Boolean> $completion) {
        return ChannelsKt__Channels_commonKt.none($this$none, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object none(@NotNull ReceiveChannel<? extends E> $this$none, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super Boolean> $completion) {
        return ChannelsKt__Channels_commonKt.none($this$none, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object none$$forInline(@NotNull ReceiveChannel $this$none, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.none($this$none, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object partition(@NotNull ReceiveChannel<? extends E> $this$partition, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super Pair<? extends List<? extends E>, ? extends List<? extends E>>> $completion) {
        return ChannelsKt__Channels_commonKt.partition($this$partition, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object partition$$forInline(@NotNull ReceiveChannel $this$partition, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.partition($this$partition, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <S, E extends S> Object reduce(@NotNull ReceiveChannel<? extends E> $this$reduce, @NotNull Function2<? super S, ? super E, ? extends S> operation, @NotNull Continuation<? super S> $completion) {
        return ChannelsKt__Channels_commonKt.reduce($this$reduce, operation, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object reduce$$forInline(@NotNull ReceiveChannel $this$reduce, @NotNull Function2 operation, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.reduce($this$reduce, operation, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <S, E extends S> Object reduceIndexed(@NotNull ReceiveChannel<? extends E> $this$reduceIndexed, @NotNull Function3<? super Integer, ? super S, ? super E, ? extends S> operation, @NotNull Continuation<? super S> $completion) {
        return ChannelsKt__Channels_commonKt.reduceIndexed($this$reduceIndexed, operation, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object reduceIndexed$$forInline(@NotNull ReceiveChannel $this$reduceIndexed, @NotNull Function3 operation, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.reduceIndexed($this$reduceIndexed, operation, continuation);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<E> requireNoNulls(@NotNull ReceiveChannel<? extends E> $this$requireNoNulls) {
        return ChannelsKt__Channels_commonKt.requireNoNulls($this$requireNoNulls);
    }

    public static final <E> void sendBlocking(@NotNull SendChannel<? super E> $this$sendBlocking, E element) {
        ChannelsKt__ChannelsKt.sendBlocking($this$sendBlocking, element);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object single(@NotNull ReceiveChannel<? extends E> $this$single, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.single($this$single, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object single(@NotNull ReceiveChannel<? extends E> $this$single, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.single($this$single, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object single$$forInline(@NotNull ReceiveChannel $this$single, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.single($this$single, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object singleOrNull(@NotNull ReceiveChannel<? extends E> $this$singleOrNull, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.singleOrNull($this$singleOrNull, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object singleOrNull(@NotNull ReceiveChannel<? extends E> $this$singleOrNull, @NotNull Function1<? super E, Boolean> predicate, @NotNull Continuation<? super E> $completion) {
        return ChannelsKt__Channels_commonKt.singleOrNull($this$singleOrNull, predicate, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object singleOrNull$$forInline(@NotNull ReceiveChannel $this$singleOrNull, @NotNull Function1 predicate, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.singleOrNull($this$singleOrNull, predicate, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object sumBy(@NotNull ReceiveChannel<? extends E> $this$sumBy, @NotNull Function1<? super E, Integer> selector, @NotNull Continuation<? super Integer> $completion) {
        return ChannelsKt__Channels_commonKt.sumBy($this$sumBy, selector, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object sumBy$$forInline(@NotNull ReceiveChannel $this$sumBy, @NotNull Function1 selector, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.sumBy($this$sumBy, selector, continuation);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object sumByDouble(@NotNull ReceiveChannel<? extends E> $this$sumByDouble, @NotNull Function1<? super E, Double> selector, @NotNull Continuation<? super Double> $completion) {
        return ChannelsKt__Channels_commonKt.sumByDouble($this$sumByDouble, selector, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    private static final Object sumByDouble$$forInline(@NotNull ReceiveChannel $this$sumByDouble, @NotNull Function1 selector, @NotNull Continuation continuation) {
        return ChannelsKt__Channels_commonKt.sumByDouble($this$sumByDouble, selector, continuation);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<E> take(@NotNull ReceiveChannel<? extends E> $this$take, int n, @NotNull CoroutineContext context) {
        return ChannelsKt__Channels_commonKt.take($this$take, n, context);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<E> takeWhile(@NotNull ReceiveChannel<? extends E> $this$takeWhile, @NotNull CoroutineContext context, @NotNull Function2<? super E, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        return ChannelsKt__Channels_commonKt.takeWhile($this$takeWhile, context, predicate);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, C extends SendChannel<? super E>> Object toChannel(@NotNull ReceiveChannel<? extends E> $this$toChannel, @NotNull C destination, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.toChannel($this$toChannel, destination, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E, C extends Collection<? super E>> Object toCollection(@NotNull ReceiveChannel<? extends E> $this$toCollection, @NotNull C destination, @NotNull Continuation<? super C> $completion) {
        return ChannelsKt__Channels_commonKt.toCollection($this$toCollection, destination, $completion);
    }

    @Nullable
    public static final <E> Object toList(@NotNull ReceiveChannel<? extends E> $this$toList, @NotNull Continuation<? super List<? extends E>> $completion) {
        return ChannelsKt__Channels_commonKt.toList($this$toList, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <K, V, M extends Map<? super K, ? super V>> Object toMap(@NotNull ReceiveChannel<? extends Pair<? extends K, ? extends V>> $this$toMap, @NotNull M destination, @NotNull Continuation<? super M> $completion) {
        return ChannelsKt__Channels_commonKt.toMap($this$toMap, destination, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <K, V> Object toMap(@NotNull ReceiveChannel<? extends Pair<? extends K, ? extends V>> $this$toMap, @NotNull Continuation<? super Map<K, ? extends V>> $completion) {
        return ChannelsKt__Channels_commonKt.toMap($this$toMap, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object toMutableList(@NotNull ReceiveChannel<? extends E> $this$toMutableList, @NotNull Continuation<? super List<E>> $completion) {
        return ChannelsKt__Channels_commonKt.toMutableList($this$toMutableList, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object toMutableSet(@NotNull ReceiveChannel<? extends E> $this$toMutableSet, @NotNull Continuation<? super Set<E>> $completion) {
        return ChannelsKt__Channels_commonKt.toMutableSet($this$toMutableSet, $completion);
    }

    @Nullable
    @ObsoleteCoroutinesApi
    public static final <E> Object toSet(@NotNull ReceiveChannel<? extends E> $this$toSet, @NotNull Continuation<? super Set<? extends E>> $completion) {
        return ChannelsKt__Channels_commonKt.toSet($this$toSet, $completion);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E> ReceiveChannel<IndexedValue<E>> withIndex(@NotNull ReceiveChannel<? extends E> $this$withIndex, @NotNull CoroutineContext context) {
        return ChannelsKt__Channels_commonKt.withIndex($this$withIndex, context);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E, R> ReceiveChannel<Pair<E, R>> zip(@NotNull ReceiveChannel<? extends E> $this$zip, @NotNull ReceiveChannel<? extends R> other) {
        return ChannelsKt__Channels_commonKt.zip($this$zip, other);
    }

    @NotNull
    @ObsoleteCoroutinesApi
    public static final <E, R, V> ReceiveChannel<V> zip(@NotNull ReceiveChannel<? extends E> $this$zip, @NotNull ReceiveChannel<? extends R> other, @NotNull CoroutineContext context, @NotNull Function2<? super E, ? super R, ? extends V> transform) {
        return ChannelsKt__Channels_commonKt.zip($this$zip, other, context, transform);
    }
}
