package kotlinx.coroutines.channels;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\b\u0012\u0004\u0012\u0002H\u00030\u0004H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "E", "R", "Lkotlinx/coroutines/channels/ProducerScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$mapIndexed$1", f = "Channels.common.kt", i = {0, 1, 2, 2, 3, 3}, l = {1224, 1224, 1225, 1225}, m = "invokeSuspend", n = {"index", "index", "index", "e", "index", "e"}, s = {"I$0", "I$0", "I$0", "L$1", "I$0", "L$1"})
/* compiled from: Channels.common.kt */
final class ChannelsKt__Channels_commonKt$mapIndexed$1 extends SuspendLambda implements Function2<ProducerScope<? super R>, Continuation<? super Unit>, Object> {
    final /* synthetic */ ReceiveChannel $this_mapIndexed;
    final /* synthetic */ Function3 $transform;
    int I$0;
    Object L$0;
    Object L$1;
    Object L$2;
    Object L$3;
    int label;
    private ProducerScope p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ChannelsKt__Channels_commonKt$mapIndexed$1(ReceiveChannel receiveChannel, Function3 function3, Continuation continuation) {
        super(2, continuation);
        this.$this_mapIndexed = receiveChannel;
        this.$transform = function3;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        ChannelsKt__Channels_commonKt$mapIndexed$1 channelsKt__Channels_commonKt$mapIndexed$1 = new ChannelsKt__Channels_commonKt$mapIndexed$1(this.$this_mapIndexed, this.$transform, continuation);
        ProducerScope producerScope = (ProducerScope) obj;
        channelsKt__Channels_commonKt$mapIndexed$1.p$ = (ProducerScope) obj;
        return channelsKt__Channels_commonKt$mapIndexed$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((ChannelsKt__Channels_commonKt$mapIndexed$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e1  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r15) {
        /*
            r14 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r14.label
            r2 = 4
            r3 = 3
            r4 = 2
            r5 = 1
            if (r1 == 0) goto L_0x0072
            r6 = 0
            if (r1 == r5) goto L_0x0060
            if (r1 == r4) goto L_0x004e
            r7 = 0
            if (r1 == r3) goto L_0x0032
            if (r1 != r2) goto L_0x002a
            r1 = r7
            java.lang.Object r7 = r14.L$2
            kotlinx.coroutines.channels.ChannelIterator r7 = (kotlinx.coroutines.channels.ChannelIterator) r7
            java.lang.Object r1 = r14.L$1
            int r6 = r14.I$0
            java.lang.Object r8 = r14.L$0
            kotlinx.coroutines.channels.ProducerScope r8 = (kotlinx.coroutines.channels.ProducerScope) r8
            kotlin.ResultKt.throwOnFailure(r15)
            r11 = r15
            r15 = r14
            goto L_0x00e4
        L_0x002a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0032:
            r1 = r7
            java.lang.Object r7 = r14.L$3
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            java.lang.Object r8 = r14.L$2
            kotlinx.coroutines.channels.ChannelIterator r8 = (kotlinx.coroutines.channels.ChannelIterator) r8
            java.lang.Object r1 = r14.L$1
            int r6 = r14.I$0
            java.lang.Object r9 = r14.L$0
            kotlinx.coroutines.channels.ProducerScope r9 = (kotlinx.coroutines.channels.ProducerScope) r9
            kotlin.ResultKt.throwOnFailure(r15)
            r11 = r15
            r10 = r0
            r0 = r14
            r12 = r9
            r9 = r8
            r8 = r12
            goto L_0x00d0
        L_0x004e:
            r1 = r6
            java.lang.Object r6 = r14.L$1
            kotlinx.coroutines.channels.ChannelIterator r6 = (kotlinx.coroutines.channels.ChannelIterator) r6
            int r1 = r14.I$0
            java.lang.Object r7 = r14.L$0
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            kotlin.ResultKt.throwOnFailure(r15)
            r8 = r15
            r9 = r0
            r0 = r14
            goto L_0x00ae
        L_0x0060:
            r1 = r6
            java.lang.Object r6 = r14.L$1
            kotlinx.coroutines.channels.ChannelIterator r6 = (kotlinx.coroutines.channels.ChannelIterator) r6
            int r1 = r14.I$0
            java.lang.Object r7 = r14.L$0
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            kotlin.ResultKt.throwOnFailure(r15)
            r8 = r15
            r9 = r0
            r0 = r14
            goto L_0x0097
        L_0x0072:
            kotlin.ResultKt.throwOnFailure(r15)
            kotlinx.coroutines.channels.ProducerScope r1 = r14.p$
            r6 = 0
            kotlinx.coroutines.channels.ReceiveChannel r7 = r14.$this_mapIndexed
            kotlinx.coroutines.channels.ChannelIterator r7 = r7.iterator()
            r8 = r15
            r15 = r14
        L_0x0080:
            r15.L$0 = r1
            r15.I$0 = r6
            r15.L$1 = r7
            r15.label = r5
            java.lang.Object r9 = r7.hasNext(r15)
            if (r9 != r0) goto L_0x008f
            return r0
        L_0x008f:
            r12 = r0
            r0 = r15
            r15 = r9
            r9 = r12
            r13 = r7
            r7 = r1
            r1 = r6
            r6 = r13
        L_0x0097:
            java.lang.Boolean r15 = (java.lang.Boolean) r15
            boolean r15 = r15.booleanValue()
            if (r15 == 0) goto L_0x00e7
            r0.L$0 = r7
            r0.I$0 = r1
            r0.L$1 = r6
            r0.label = r4
            java.lang.Object r15 = r6.next(r0)
            if (r15 != r9) goto L_0x00ae
            return r9
        L_0x00ae:
            kotlin.jvm.functions.Function3 r10 = r0.$transform
            java.lang.Integer r11 = kotlin.coroutines.jvm.internal.Boxing.boxInt(r1)
            int r1 = r1 + r5
            r0.L$0 = r7
            r0.I$0 = r1
            r0.L$1 = r15
            r0.L$2 = r6
            r0.L$3 = r7
            r0.label = r3
            java.lang.Object r10 = r10.invoke(r11, r15, r0)
            if (r10 != r9) goto L_0x00c8
            return r9
        L_0x00c8:
            r11 = r8
            r8 = r7
            r12 = r1
            r1 = r15
            r15 = r10
            r10 = r9
            r9 = r6
            r6 = r12
        L_0x00d0:
            r0.L$0 = r8
            r0.I$0 = r6
            r0.L$1 = r1
            r0.L$2 = r9
            r0.label = r2
            java.lang.Object r15 = r7.send(r15, r0)
            if (r15 != r10) goto L_0x00e1
            return r10
        L_0x00e1:
            r15 = r0
            r7 = r9
            r0 = r10
        L_0x00e4:
            r1 = r8
            r8 = r11
            goto L_0x0080
        L_0x00e7:
            kotlin.Unit r15 = kotlin.Unit.INSTANCE
            return r15
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$mapIndexed$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
