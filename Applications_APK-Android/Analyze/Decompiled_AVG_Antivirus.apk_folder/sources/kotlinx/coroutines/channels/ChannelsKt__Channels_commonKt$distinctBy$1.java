package kotlinx.coroutines.channels;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\b\u0012\u0004\u0012\u0002H\u00020\u0004H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "E", "K", "Lkotlinx/coroutines/channels/ProducerScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$distinctBy$1", f = "Channels.common.kt", i = {0, 1, 2, 2, 3, 3, 3}, l = {1465, 1465, 1466, 1468}, m = "invokeSuspend", n = {"keys", "keys", "keys", "e", "keys", "e", "k"}, s = {"L$1", "L$1", "L$1", "L$2", "L$1", "L$2", "L$4"})
/* compiled from: Channels.common.kt */
final class ChannelsKt__Channels_commonKt$distinctBy$1 extends SuspendLambda implements Function2<ProducerScope<? super E>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Function2 $selector;
    final /* synthetic */ ReceiveChannel $this_distinctBy;
    Object L$0;
    Object L$1;
    Object L$2;
    Object L$3;
    Object L$4;
    int label;
    private ProducerScope p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ChannelsKt__Channels_commonKt$distinctBy$1(ReceiveChannel receiveChannel, Function2 function2, Continuation continuation) {
        super(2, continuation);
        this.$this_distinctBy = receiveChannel;
        this.$selector = function2;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        ChannelsKt__Channels_commonKt$distinctBy$1 channelsKt__Channels_commonKt$distinctBy$1 = new ChannelsKt__Channels_commonKt$distinctBy$1(this.$this_distinctBy, this.$selector, continuation);
        ProducerScope producerScope = (ProducerScope) obj;
        channelsKt__Channels_commonKt$distinctBy$1.p$ = (ProducerScope) obj;
        return channelsKt__Channels_commonKt$distinctBy$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((ChannelsKt__Channels_commonKt$distinctBy$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v10, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v12, resolved type: java.util.HashSet} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v15, resolved type: java.util.HashSet} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v13, resolved type: java.util.HashSet} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v10, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v15, resolved type: java.util.HashSet} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00f5 A[RETURN] */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r15) {
        /*
            r14 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r14.label
            r2 = 4
            r3 = 3
            r4 = 2
            r5 = 1
            if (r1 == 0) goto L_0x0081
            r6 = 0
            if (r1 == r5) goto L_0x006c
            if (r1 == r4) goto L_0x0057
            if (r1 == r3) goto L_0x003b
            if (r1 != r2) goto L_0x0033
            r1 = r6
            r7 = r6
            java.lang.Object r1 = r14.L$4
            java.lang.Object r8 = r14.L$3
            kotlinx.coroutines.channels.ChannelIterator r8 = (kotlinx.coroutines.channels.ChannelIterator) r8
            java.lang.Object r7 = r14.L$2
            java.lang.Object r9 = r14.L$1
            r6 = r9
            java.util.HashSet r6 = (java.util.HashSet) r6
            java.lang.Object r9 = r14.L$0
            kotlinx.coroutines.channels.ProducerScope r9 = (kotlinx.coroutines.channels.ProducerScope) r9
            kotlin.ResultKt.throwOnFailure(r15)
            r10 = r0
            r0 = r14
            r12 = r8
            r8 = r15
            r15 = r1
            r1 = r12
            goto L_0x00f6
        L_0x0033:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003b:
            r1 = r6
            java.lang.Object r7 = r14.L$3
            kotlinx.coroutines.channels.ChannelIterator r7 = (kotlinx.coroutines.channels.ChannelIterator) r7
            java.lang.Object r1 = r14.L$2
            java.lang.Object r8 = r14.L$1
            r6 = r8
            java.util.HashSet r6 = (java.util.HashSet) r6
            java.lang.Object r8 = r14.L$0
            kotlinx.coroutines.channels.ProducerScope r8 = (kotlinx.coroutines.channels.ProducerScope) r8
            kotlin.ResultKt.throwOnFailure(r15)
            r10 = r0
            r9 = r8
            r0 = r14
            r8 = r15
            r12 = r7
            r7 = r1
            r1 = r12
            goto L_0x00dc
        L_0x0057:
            r1 = r6
            java.lang.Object r6 = r14.L$2
            kotlinx.coroutines.channels.ChannelIterator r6 = (kotlinx.coroutines.channels.ChannelIterator) r6
            java.lang.Object r7 = r14.L$1
            r1 = r7
            java.util.HashSet r1 = (java.util.HashSet) r1
            java.lang.Object r7 = r14.L$0
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            kotlin.ResultKt.throwOnFailure(r15)
            r8 = r15
            r9 = r0
            r0 = r14
            goto L_0x00c1
        L_0x006c:
            r1 = r6
            java.lang.Object r6 = r14.L$2
            kotlinx.coroutines.channels.ChannelIterator r6 = (kotlinx.coroutines.channels.ChannelIterator) r6
            java.lang.Object r7 = r14.L$1
            r1 = r7
            java.util.HashSet r1 = (java.util.HashSet) r1
            java.lang.Object r7 = r14.L$0
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            kotlin.ResultKt.throwOnFailure(r15)
            r8 = r15
            r9 = r0
            r0 = r14
            goto L_0x00aa
        L_0x0081:
            kotlin.ResultKt.throwOnFailure(r15)
            kotlinx.coroutines.channels.ProducerScope r1 = r14.p$
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>()
            kotlinx.coroutines.channels.ReceiveChannel r7 = r14.$this_distinctBy
            kotlinx.coroutines.channels.ChannelIterator r7 = r7.iterator()
            r8 = r15
            r15 = r14
        L_0x0093:
            r15.L$0 = r1
            r15.L$1 = r6
            r15.L$2 = r7
            r15.label = r5
            java.lang.Object r9 = r7.hasNext(r15)
            if (r9 != r0) goto L_0x00a2
            return r0
        L_0x00a2:
            r12 = r0
            r0 = r15
            r15 = r9
            r9 = r12
            r13 = r7
            r7 = r1
            r1 = r6
            r6 = r13
        L_0x00aa:
            java.lang.Boolean r15 = (java.lang.Boolean) r15
            boolean r15 = r15.booleanValue()
            if (r15 == 0) goto L_0x0106
            r0.L$0 = r7
            r0.L$1 = r1
            r0.L$2 = r6
            r0.label = r4
            java.lang.Object r15 = r6.next(r0)
            if (r15 != r9) goto L_0x00c1
            return r9
        L_0x00c1:
            kotlin.jvm.functions.Function2 r10 = r0.$selector
            r0.L$0 = r7
            r0.L$1 = r1
            r0.L$2 = r15
            r0.L$3 = r6
            r0.label = r3
            java.lang.Object r10 = r10.invoke(r15, r0)
            if (r10 != r9) goto L_0x00d4
            return r9
        L_0x00d4:
            r12 = r7
            r7 = r15
            r15 = r10
            r10 = r9
            r9 = r12
            r13 = r6
            r6 = r1
            r1 = r13
        L_0x00dc:
            boolean r11 = r6.contains(r15)
            if (r11 != 0) goto L_0x0101
            r0.L$0 = r9
            r0.L$1 = r6
            r0.L$2 = r7
            r0.L$3 = r1
            r0.L$4 = r15
            r0.label = r2
            java.lang.Object r11 = r9.send(r7, r0)
            if (r11 != r10) goto L_0x00f6
            return r10
        L_0x00f6:
            r11 = r6
            java.util.Collection r11 = (java.util.Collection) r11
            r11.add(r15)
            r15 = r0
            r7 = r1
            r1 = r9
            r0 = r10
            goto L_0x0105
        L_0x0101:
            r15 = r0
            r7 = r1
            r1 = r9
            r0 = r10
        L_0x0105:
            goto L_0x0093
        L_0x0106:
            kotlin.Unit r15 = kotlin.Unit.INSTANCE
            return r15
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$distinctBy$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
