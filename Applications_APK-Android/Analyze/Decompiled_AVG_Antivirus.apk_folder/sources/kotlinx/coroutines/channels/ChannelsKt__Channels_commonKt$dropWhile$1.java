package kotlinx.coroutines.channels;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "E", "Lkotlinx/coroutines/channels/ProducerScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$dropWhile$1", f = "Channels.common.kt", i = {2, 3, 6}, l = {619, 619, 620, 621, 625, 625, 626}, m = "invokeSuspend", n = {"e", "e", "e"}, s = {"L$1", "L$1", "L$1"})
/* compiled from: Channels.common.kt */
final class ChannelsKt__Channels_commonKt$dropWhile$1 extends SuspendLambda implements Function2<ProducerScope<? super E>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Function2 $predicate;
    final /* synthetic */ ReceiveChannel $this_dropWhile;
    Object L$0;
    Object L$1;
    Object L$2;
    int label;
    private ProducerScope p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ChannelsKt__Channels_commonKt$dropWhile$1(ReceiveChannel receiveChannel, Function2 function2, Continuation continuation) {
        super(2, continuation);
        this.$this_dropWhile = receiveChannel;
        this.$predicate = function2;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        ChannelsKt__Channels_commonKt$dropWhile$1 channelsKt__Channels_commonKt$dropWhile$1 = new ChannelsKt__Channels_commonKt$dropWhile$1(this.$this_dropWhile, this.$predicate, continuation);
        ProducerScope producerScope = (ProducerScope) obj;
        channelsKt__Channels_commonKt$dropWhile$1.p$ = (ProducerScope) obj;
        return channelsKt__Channels_commonKt$dropWhile$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((ChannelsKt__Channels_commonKt$dropWhile$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00f4 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0111 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0141  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r8.label
            r2 = 0
            switch(r1) {
                case 0: goto L_0x008c;
                case 1: goto L_0x007d;
                case 2: goto L_0x006e;
                case 3: goto L_0x0059;
                case 4: goto L_0x004a;
                case 5: goto L_0x003a;
                case 6: goto L_0x0028;
                case 7: goto L_0x0012;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0012:
            r1 = r2
            java.lang.Object r2 = r8.L$2
            kotlinx.coroutines.channels.ChannelIterator r2 = (kotlinx.coroutines.channels.ChannelIterator) r2
            java.lang.Object r1 = r8.L$1
            java.lang.Object r3 = r8.L$0
            kotlinx.coroutines.channels.ProducerScope r3 = (kotlinx.coroutines.channels.ProducerScope) r3
            kotlin.ResultKt.throwOnFailure(r9)
            r4 = r9
            r9 = r1
            r1 = r2
            r2 = r3
            r3 = r0
            r0 = r8
            goto L_0x0140
        L_0x0028:
            java.lang.Object r1 = r8.L$1
            kotlinx.coroutines.channels.ChannelIterator r1 = (kotlinx.coroutines.channels.ChannelIterator) r1
            java.lang.Object r2 = r8.L$0
            kotlinx.coroutines.channels.ProducerScope r2 = (kotlinx.coroutines.channels.ProducerScope) r2
            kotlin.ResultKt.throwOnFailure(r9)
            r4 = r9
            r3 = r2
            r2 = r1
            r1 = r0
            r0 = r8
            goto L_0x012c
        L_0x003a:
            java.lang.Object r1 = r8.L$1
            kotlinx.coroutines.channels.ChannelIterator r1 = (kotlinx.coroutines.channels.ChannelIterator) r1
            java.lang.Object r2 = r8.L$0
            kotlinx.coroutines.channels.ProducerScope r2 = (kotlinx.coroutines.channels.ProducerScope) r2
            kotlin.ResultKt.throwOnFailure(r9)
            r4 = r9
            r3 = r0
            r0 = r8
            goto L_0x0112
        L_0x004a:
            r1 = r2
            java.lang.Object r1 = r8.L$1
            java.lang.Object r2 = r8.L$0
            kotlinx.coroutines.channels.ProducerScope r2 = (kotlinx.coroutines.channels.ProducerScope) r2
            kotlin.ResultKt.throwOnFailure(r9)
            r4 = r9
            r3 = r0
            r0 = r8
            goto L_0x00f7
        L_0x0059:
            r1 = r2
            java.lang.Object r2 = r8.L$2
            kotlinx.coroutines.channels.ChannelIterator r2 = (kotlinx.coroutines.channels.ChannelIterator) r2
            java.lang.Object r1 = r8.L$1
            java.lang.Object r3 = r8.L$0
            kotlinx.coroutines.channels.ProducerScope r3 = (kotlinx.coroutines.channels.ProducerScope) r3
            kotlin.ResultKt.throwOnFailure(r9)
            r5 = r0
            r4 = r2
            r2 = r3
            r0 = r8
            r3 = r9
            goto L_0x00df
        L_0x006e:
            java.lang.Object r1 = r8.L$1
            kotlinx.coroutines.channels.ChannelIterator r1 = (kotlinx.coroutines.channels.ChannelIterator) r1
            java.lang.Object r2 = r8.L$0
            kotlinx.coroutines.channels.ProducerScope r2 = (kotlinx.coroutines.channels.ProducerScope) r2
            kotlin.ResultKt.throwOnFailure(r9)
            r4 = r9
            r3 = r0
            r0 = r8
            goto L_0x00c7
        L_0x007d:
            java.lang.Object r1 = r8.L$1
            kotlinx.coroutines.channels.ChannelIterator r1 = (kotlinx.coroutines.channels.ChannelIterator) r1
            java.lang.Object r2 = r8.L$0
            kotlinx.coroutines.channels.ProducerScope r2 = (kotlinx.coroutines.channels.ProducerScope) r2
            kotlin.ResultKt.throwOnFailure(r9)
            r4 = r9
            r3 = r0
            r0 = r8
            goto L_0x00b1
        L_0x008c:
            kotlin.ResultKt.throwOnFailure(r9)
            kotlinx.coroutines.channels.ProducerScope r1 = r8.p$
            kotlinx.coroutines.channels.ReceiveChannel r2 = r8.$this_dropWhile
            kotlinx.coroutines.channels.ChannelIterator r2 = r2.iterator()
            r3 = r9
            r9 = r8
            r7 = r2
            r2 = r0
            r0 = r1
            r1 = r7
        L_0x009d:
            r9.L$0 = r0
            r9.L$1 = r1
            r4 = 1
            r9.label = r4
            java.lang.Object r4 = r1.hasNext(r9)
            if (r4 != r2) goto L_0x00ab
            return r2
        L_0x00ab:
            r7 = r0
            r0 = r9
            r9 = r4
            r4 = r3
            r3 = r2
            r2 = r7
        L_0x00b1:
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x00fd
            r0.L$0 = r2
            r0.L$1 = r1
            r9 = 2
            r0.label = r9
            java.lang.Object r9 = r1.next(r0)
            if (r9 != r3) goto L_0x00c7
            return r3
        L_0x00c7:
            kotlin.jvm.functions.Function2 r5 = r0.$predicate
            r0.L$0 = r2
            r0.L$1 = r9
            r0.L$2 = r1
            r6 = 3
            r0.label = r6
            java.lang.Object r5 = r5.invoke(r9, r0)
            if (r5 != r3) goto L_0x00d9
            return r3
        L_0x00d9:
            r7 = r1
            r1 = r9
            r9 = r5
            r5 = r3
            r3 = r4
            r4 = r7
        L_0x00df:
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 != 0) goto L_0x00f8
            r0.L$0 = r2
            r0.L$1 = r1
            r9 = 4
            r0.label = r9
            java.lang.Object r9 = r2.send(r1, r0)
            if (r9 != r5) goto L_0x00f5
            return r5
        L_0x00f5:
            r4 = r3
            r3 = r5
        L_0x00f7:
            goto L_0x00fd
        L_0x00f8:
            r9 = r0
            r0 = r2
            r1 = r4
            r2 = r5
            goto L_0x009d
        L_0x00fd:
            kotlinx.coroutines.channels.ReceiveChannel r9 = r0.$this_dropWhile
            kotlinx.coroutines.channels.ChannelIterator r9 = r9.iterator()
            r1 = r9
        L_0x0104:
            r0.L$0 = r2
            r0.L$1 = r1
            r9 = 5
            r0.label = r9
            java.lang.Object r9 = r1.hasNext(r0)
            if (r9 != r3) goto L_0x0112
            return r3
        L_0x0112:
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x0141
            r0.L$0 = r2
            r0.L$1 = r1
            r9 = 6
            r0.label = r9
            java.lang.Object r9 = r1.next(r0)
            if (r9 != r3) goto L_0x0128
            return r3
        L_0x0128:
            r7 = r2
            r2 = r1
            r1 = r3
            r3 = r7
        L_0x012c:
            r0.L$0 = r3
            r0.L$1 = r9
            r0.L$2 = r2
            r5 = 7
            r0.label = r5
            java.lang.Object r5 = r3.send(r9, r0)
            if (r5 != r1) goto L_0x013c
            return r1
        L_0x013c:
            r7 = r3
            r3 = r1
            r1 = r2
            r2 = r7
        L_0x0140:
            goto L_0x0104
        L_0x0141:
            kotlin.Unit r9 = kotlin.Unit.INSTANCE
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$dropWhile$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
