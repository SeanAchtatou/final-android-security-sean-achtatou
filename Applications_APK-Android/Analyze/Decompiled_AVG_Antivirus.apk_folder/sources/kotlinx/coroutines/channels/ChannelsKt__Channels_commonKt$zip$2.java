package kotlinx.coroutines.channels;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0004\b\u0002\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005H@ø\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"<anonymous>", "", "E", "R", "V", "Lkotlinx/coroutines/channels/ProducerScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2", f = "Channels.common.kt", i = {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4}, l = {1904, 1904, 1892, 1893, 1894}, m = "invokeSuspend", n = {"otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "element1", "otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "element1", "otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "element1", "element2"}, s = {"L$1", "L$2", "L$4", "L$5", "L$6", "L$1", "L$2", "L$4", "L$5", "L$6", "L$1", "L$2", "L$4", "L$5", "L$6", "L$8", "L$9", "L$1", "L$2", "L$4", "L$5", "L$6", "L$8", "L$9", "L$1", "L$2", "L$4", "L$5", "L$6", "L$8", "L$9", "L$10"})
/* compiled from: Channels.common.kt */
final class ChannelsKt__Channels_commonKt$zip$2 extends SuspendLambda implements Function2<ProducerScope<? super V>, Continuation<? super Unit>, Object> {
    final /* synthetic */ ReceiveChannel $other;
    final /* synthetic */ ReceiveChannel $this_zip;
    final /* synthetic */ Function2 $transform;
    Object L$0;
    Object L$1;
    Object L$10;
    Object L$2;
    Object L$3;
    Object L$4;
    Object L$5;
    Object L$6;
    Object L$7;
    Object L$8;
    Object L$9;
    int label;
    private ProducerScope p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ChannelsKt__Channels_commonKt$zip$2(ReceiveChannel receiveChannel, ReceiveChannel receiveChannel2, Function2 function2, Continuation continuation) {
        super(2, continuation);
        this.$this_zip = receiveChannel;
        this.$other = receiveChannel2;
        this.$transform = function2;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        ChannelsKt__Channels_commonKt$zip$2 channelsKt__Channels_commonKt$zip$2 = new ChannelsKt__Channels_commonKt$zip$2(this.$this_zip, this.$other, this.$transform, continuation);
        ProducerScope producerScope = (ProducerScope) obj;
        channelsKt__Channels_commonKt$zip$2.p$ = (ProducerScope) obj;
        return channelsKt__Channels_commonKt$zip$2;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((ChannelsKt__Channels_commonKt$zip$2) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* Debug info: failed to restart local var, previous not found, register: 24 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v19, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v14, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v20, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v12, resolved type: java.lang.Throwable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v12, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v20, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v10, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v21, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v10, resolved type: kotlinx.coroutines.channels.ChannelIterator} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v22, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v17, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v23, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v14, resolved type: java.lang.Throwable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v15, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v14, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v26, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v13, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v27, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v13, resolved type: kotlinx.coroutines.channels.ChannelIterator} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v17, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v34, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v18, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v18, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v17, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v17, resolved type: kotlinx.coroutines.channels.ChannelIterator} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v20, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v38, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v23, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v20, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v21, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v20, resolved type: kotlinx.coroutines.channels.ChannelIterator} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01d4 A[Catch:{ Throwable -> 0x02b1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0217 A[Catch:{ Throwable -> 0x02b1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0250  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0284  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r25) {
        /*
            r24 = this;
            r1 = r24
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r2 = r1.label
            r3 = 5
            r4 = 4
            r5 = 3
            r6 = 2
            r7 = 1
            r8 = 0
            if (r2 == 0) goto L_0x0182
            r9 = 0
            if (r2 == r7) goto L_0x013d
            if (r2 == r6) goto L_0x0105
            if (r2 == r5) goto L_0x00af
            if (r2 == r4) goto L_0x0069
            if (r2 != r3) goto L_0x0061
            r2 = r8
            r10 = r8
            r11 = r9
            r12 = r8
            r13 = r8
            r14 = r9
            r15 = r8
            r16 = r8
            r17 = r9
            r18 = r8
            java.lang.Object r2 = r1.L$10
            java.lang.Object r10 = r1.L$9
            java.lang.Object r12 = r1.L$8
            java.lang.Object r3 = r1.L$7
            kotlinx.coroutines.channels.ChannelIterator r3 = (kotlinx.coroutines.channels.ChannelIterator) r3
            java.lang.Object r4 = r1.L$6
            kotlinx.coroutines.channels.ReceiveChannel r4 = (kotlinx.coroutines.channels.ReceiveChannel) r4
            java.lang.Object r13 = r1.L$5
            java.lang.Throwable r13 = (java.lang.Throwable) r13
            java.lang.Object r15 = r1.L$4
            kotlinx.coroutines.channels.ReceiveChannel r15 = (kotlinx.coroutines.channels.ReceiveChannel) r15
            java.lang.Object r5 = r1.L$3
            kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2 r5 = (kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2) r5
            java.lang.Object r6 = r1.L$2
            kotlinx.coroutines.channels.ReceiveChannel r6 = (kotlinx.coroutines.channels.ReceiveChannel) r6
            java.lang.Object r7 = r1.L$1
            kotlinx.coroutines.channels.ChannelIterator r7 = (kotlinx.coroutines.channels.ChannelIterator) r7
            java.lang.Object r8 = r1.L$0
            kotlinx.coroutines.channels.ProducerScope r8 = (kotlinx.coroutines.channels.ProducerScope) r8
            kotlin.ResultKt.throwOnFailure(r25)     // Catch:{ Throwable -> 0x017c, all -> 0x0176 }
            r20 = r14
            r19 = r17
            r17 = r9
            r14 = r11
            r11 = r25
            r9 = r0
            r0 = r4
            r4 = r5
            r5 = r1
            r1 = 5
            goto L_0x028e
        L_0x0061:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0069:
            r2 = r8
            r3 = r9
            r4 = r8
            r5 = r8
            r6 = r9
            r7 = r8
            r10 = r8
            r17 = r9
            r11 = r8
            java.lang.Object r2 = r1.L$9
            java.lang.Object r4 = r1.L$8
            java.lang.Object r12 = r1.L$7
            kotlinx.coroutines.channels.ChannelIterator r12 = (kotlinx.coroutines.channels.ChannelIterator) r12
            java.lang.Object r13 = r1.L$6
            r5 = r13
            kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
            java.lang.Object r13 = r1.L$5
            java.lang.Throwable r13 = (java.lang.Throwable) r13
            java.lang.Object r7 = r1.L$4
            r15 = r7
            kotlinx.coroutines.channels.ReceiveChannel r15 = (kotlinx.coroutines.channels.ReceiveChannel) r15
            java.lang.Object r7 = r1.L$3
            kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2 r7 = (kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2) r7
            java.lang.Object r10 = r1.L$2
            kotlinx.coroutines.channels.ReceiveChannel r10 = (kotlinx.coroutines.channels.ReceiveChannel) r10
            java.lang.Object r11 = r1.L$1
            r8 = r11
            kotlinx.coroutines.channels.ChannelIterator r8 = (kotlinx.coroutines.channels.ChannelIterator) r8
            java.lang.Object r11 = r1.L$0
            kotlinx.coroutines.channels.ProducerScope r11 = (kotlinx.coroutines.channels.ProducerScope) r11
            kotlin.ResultKt.throwOnFailure(r25)     // Catch:{ Throwable -> 0x00fd, all -> 0x00f5 }
            r14 = r3
            r20 = r6
            r6 = r10
            r3 = r12
            r10 = r2
            r12 = r4
            r2 = r5
            r4 = r7
            r7 = r8
            r8 = r11
            r5 = r1
            r11 = r9
            r1 = r25
            r9 = r1
            goto L_0x0259
        L_0x00af:
            r2 = r8
            r3 = r9
            r4 = r8
            r5 = r8
            r6 = r9
            r7 = r8
            r10 = r8
            r17 = r9
            r11 = r8
            java.lang.Object r2 = r1.L$9
            java.lang.Object r4 = r1.L$8
            java.lang.Object r12 = r1.L$7
            kotlinx.coroutines.channels.ChannelIterator r12 = (kotlinx.coroutines.channels.ChannelIterator) r12
            java.lang.Object r13 = r1.L$6
            r5 = r13
            kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
            java.lang.Object r13 = r1.L$5
            java.lang.Throwable r13 = (java.lang.Throwable) r13
            java.lang.Object r7 = r1.L$4
            r15 = r7
            kotlinx.coroutines.channels.ReceiveChannel r15 = (kotlinx.coroutines.channels.ReceiveChannel) r15
            java.lang.Object r7 = r1.L$3
            kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2 r7 = (kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2) r7
            java.lang.Object r10 = r1.L$2
            kotlinx.coroutines.channels.ReceiveChannel r10 = (kotlinx.coroutines.channels.ReceiveChannel) r10
            java.lang.Object r11 = r1.L$1
            r8 = r11
            kotlinx.coroutines.channels.ChannelIterator r8 = (kotlinx.coroutines.channels.ChannelIterator) r8
            java.lang.Object r11 = r1.L$0
            kotlinx.coroutines.channels.ProducerScope r11 = (kotlinx.coroutines.channels.ProducerScope) r11
            kotlin.ResultKt.throwOnFailure(r25)     // Catch:{ Throwable -> 0x00fd, all -> 0x00f5 }
            r14 = r2
            r2 = r5
            r20 = r6
            r6 = r10
            r10 = r25
            r5 = r1
            r1 = r10
            r22 = r12
            r12 = r4
            r4 = r7
            r7 = r8
            r8 = r22
            goto L_0x021d
        L_0x00f5:
            r0 = move-exception
            r5 = r1
            r7 = r8
            r6 = r10
            r10 = r25
            goto L_0x02d2
        L_0x00fd:
            r0 = move-exception
            r5 = r1
            r7 = r8
            r6 = r10
            r10 = r25
            goto L_0x02ce
        L_0x0105:
            r2 = r8
            r3 = r9
            r4 = r8
            r5 = r8
            r17 = r9
            r6 = r8
            r7 = r8
            java.lang.Object r8 = r1.L$7
            kotlinx.coroutines.channels.ChannelIterator r8 = (kotlinx.coroutines.channels.ChannelIterator) r8
            java.lang.Object r10 = r1.L$6
            r2 = r10
            kotlinx.coroutines.channels.ReceiveChannel r2 = (kotlinx.coroutines.channels.ReceiveChannel) r2
            java.lang.Object r10 = r1.L$5
            r13 = r10
            java.lang.Throwable r13 = (java.lang.Throwable) r13
            java.lang.Object r4 = r1.L$4
            r15 = r4
            kotlinx.coroutines.channels.ReceiveChannel r15 = (kotlinx.coroutines.channels.ReceiveChannel) r15
            java.lang.Object r4 = r1.L$3
            kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2 r4 = (kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2) r4
            java.lang.Object r5 = r1.L$2
            r6 = r5
            kotlinx.coroutines.channels.ReceiveChannel r6 = (kotlinx.coroutines.channels.ReceiveChannel) r6
            java.lang.Object r5 = r1.L$1
            r7 = r5
            kotlinx.coroutines.channels.ChannelIterator r7 = (kotlinx.coroutines.channels.ChannelIterator) r7
            java.lang.Object r5 = r1.L$0
            kotlinx.coroutines.channels.ProducerScope r5 = (kotlinx.coroutines.channels.ProducerScope) r5
            kotlin.ResultKt.throwOnFailure(r25)     // Catch:{ Throwable -> 0x017c, all -> 0x0176 }
            r10 = r25
            r12 = r10
            r11 = r5
            r14 = 2
            r5 = r1
            goto L_0x01f3
        L_0x013d:
            r2 = r8
            r3 = r9
            r4 = r8
            r5 = r8
            r17 = r9
            r6 = r8
            r7 = r8
            java.lang.Object r8 = r1.L$7
            kotlinx.coroutines.channels.ChannelIterator r8 = (kotlinx.coroutines.channels.ChannelIterator) r8
            java.lang.Object r10 = r1.L$6
            r2 = r10
            kotlinx.coroutines.channels.ReceiveChannel r2 = (kotlinx.coroutines.channels.ReceiveChannel) r2
            java.lang.Object r10 = r1.L$5
            r13 = r10
            java.lang.Throwable r13 = (java.lang.Throwable) r13
            java.lang.Object r4 = r1.L$4
            r15 = r4
            kotlinx.coroutines.channels.ReceiveChannel r15 = (kotlinx.coroutines.channels.ReceiveChannel) r15
            java.lang.Object r4 = r1.L$3
            kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2 r4 = (kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2) r4
            java.lang.Object r5 = r1.L$2
            r6 = r5
            kotlinx.coroutines.channels.ReceiveChannel r6 = (kotlinx.coroutines.channels.ReceiveChannel) r6
            java.lang.Object r5 = r1.L$1
            r7 = r5
            kotlinx.coroutines.channels.ChannelIterator r7 = (kotlinx.coroutines.channels.ChannelIterator) r7
            java.lang.Object r5 = r1.L$0
            kotlinx.coroutines.channels.ProducerScope r5 = (kotlinx.coroutines.channels.ProducerScope) r5
            kotlin.ResultKt.throwOnFailure(r25)     // Catch:{ Throwable -> 0x017c, all -> 0x0176 }
            r10 = r25
            r14 = r10
            r11 = r0
            r0 = r5
            r12 = 1
            r5 = r1
            goto L_0x01cc
        L_0x0176:
            r0 = move-exception
            r10 = r25
            r5 = r1
            goto L_0x02d2
        L_0x017c:
            r0 = move-exception
            r10 = r25
            r5 = r1
            goto L_0x02ce
        L_0x0182:
            kotlin.ResultKt.throwOnFailure(r25)
            kotlinx.coroutines.channels.ProducerScope r2 = r1.p$
            kotlinx.coroutines.channels.ReceiveChannel r3 = r1.$other
            kotlinx.coroutines.channels.ChannelIterator r7 = r3.iterator()
            kotlinx.coroutines.channels.ReceiveChannel r6 = r1.$this_zip
            r9 = 0
            r15 = r6
            r17 = 0
            r13 = r8
            java.lang.Throwable r13 = (java.lang.Throwable) r13
            r3 = r15
            r4 = 0
            kotlinx.coroutines.channels.ChannelIterator r5 = r3.iterator()     // Catch:{ Throwable -> 0x02c9, all -> 0x02c3 }
            r11 = r4
            r8 = r5
            r10 = r9
            r9 = r25
            r4 = r1
            r5 = r2
            r2 = r4
        L_0x01a5:
            r2.L$0 = r5     // Catch:{ Throwable -> 0x02bb, all -> 0x02b3 }
            r2.L$1 = r7     // Catch:{ Throwable -> 0x02bb, all -> 0x02b3 }
            r2.L$2 = r6     // Catch:{ Throwable -> 0x02bb, all -> 0x02b3 }
            r2.L$3 = r4     // Catch:{ Throwable -> 0x02bb, all -> 0x02b3 }
            r2.L$4 = r15     // Catch:{ Throwable -> 0x02bb, all -> 0x02b3 }
            r2.L$5 = r13     // Catch:{ Throwable -> 0x02bb, all -> 0x02b3 }
            r2.L$6 = r3     // Catch:{ Throwable -> 0x02bb, all -> 0x02b3 }
            r2.L$7 = r8     // Catch:{ Throwable -> 0x02bb, all -> 0x02b3 }
            r12 = 1
            r2.label = r12     // Catch:{ Throwable -> 0x02bb, all -> 0x02b3 }
            java.lang.Object r14 = r8.hasNext(r4)     // Catch:{ Throwable -> 0x02bb, all -> 0x02b3 }
            if (r14 != r0) goto L_0x01bf
            return r0
        L_0x01bf:
            r22 = r11
            r11 = r0
            r0 = r5
            r5 = r2
            r2 = r3
            r3 = r22
            r23 = r10
            r10 = r9
            r9 = r23
        L_0x01cc:
            java.lang.Boolean r14 = (java.lang.Boolean) r14     // Catch:{ Throwable -> 0x02b1 }
            boolean r14 = r14.booleanValue()     // Catch:{ Throwable -> 0x02b1 }
            if (r14 == 0) goto L_0x02a8
            r5.L$0 = r0     // Catch:{ Throwable -> 0x02b1 }
            r5.L$1 = r7     // Catch:{ Throwable -> 0x02b1 }
            r5.L$2 = r6     // Catch:{ Throwable -> 0x02b1 }
            r5.L$3 = r4     // Catch:{ Throwable -> 0x02b1 }
            r5.L$4 = r15     // Catch:{ Throwable -> 0x02b1 }
            r5.L$5 = r13     // Catch:{ Throwable -> 0x02b1 }
            r5.L$6 = r2     // Catch:{ Throwable -> 0x02b1 }
            r5.L$7 = r8     // Catch:{ Throwable -> 0x02b1 }
            r14 = 2
            r5.label = r14     // Catch:{ Throwable -> 0x02b1 }
            java.lang.Object r12 = r8.next(r4)     // Catch:{ Throwable -> 0x02b1 }
            if (r12 != r11) goto L_0x01ee
            return r11
        L_0x01ee:
            r22 = r11
            r11 = r0
            r0 = r22
        L_0x01f3:
            r25 = r12
            r20 = 0
            r5.L$0 = r11     // Catch:{ Throwable -> 0x02b1 }
            r5.L$1 = r7     // Catch:{ Throwable -> 0x02b1 }
            r5.L$2 = r6     // Catch:{ Throwable -> 0x02b1 }
            r5.L$3 = r4     // Catch:{ Throwable -> 0x02b1 }
            r5.L$4 = r15     // Catch:{ Throwable -> 0x02b1 }
            r5.L$5 = r13     // Catch:{ Throwable -> 0x02b1 }
            r5.L$6 = r2     // Catch:{ Throwable -> 0x02b1 }
            r5.L$7 = r8     // Catch:{ Throwable -> 0x02b1 }
            r5.L$8 = r12     // Catch:{ Throwable -> 0x02b1 }
            r14 = r25
            r5.L$9 = r14     // Catch:{ Throwable -> 0x02b1 }
            r1 = 3
            r5.label = r1     // Catch:{ Throwable -> 0x02b1 }
            java.lang.Object r1 = r7.hasNext(r5)     // Catch:{ Throwable -> 0x02b1 }
            if (r1 != r0) goto L_0x0217
            return r0
        L_0x0217:
            r22 = r20
            r20 = r3
            r3 = r22
        L_0x021d:
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ Throwable -> 0x02b1 }
            boolean r1 = r1.booleanValue()     // Catch:{ Throwable -> 0x02b1 }
            if (r1 != 0) goto L_0x0232
            r3 = r2
            r2 = r5
            r5 = r11
            r11 = r20
            r1 = 5
            r22 = r10
            r10 = r9
            r9 = r22
            goto L_0x029a
        L_0x0232:
            r5.L$0 = r11     // Catch:{ Throwable -> 0x02b1 }
            r5.L$1 = r7     // Catch:{ Throwable -> 0x02b1 }
            r5.L$2 = r6     // Catch:{ Throwable -> 0x02b1 }
            r5.L$3 = r4     // Catch:{ Throwable -> 0x02b1 }
            r5.L$4 = r15     // Catch:{ Throwable -> 0x02b1 }
            r5.L$5 = r13     // Catch:{ Throwable -> 0x02b1 }
            r5.L$6 = r2     // Catch:{ Throwable -> 0x02b1 }
            r5.L$7 = r8     // Catch:{ Throwable -> 0x02b1 }
            r5.L$8 = r12     // Catch:{ Throwable -> 0x02b1 }
            r5.L$9 = r14     // Catch:{ Throwable -> 0x02b1 }
            r1 = 4
            r5.label = r1     // Catch:{ Throwable -> 0x02b1 }
            java.lang.Object r1 = r7.next(r5)     // Catch:{ Throwable -> 0x02b1 }
            if (r1 != r0) goto L_0x0250
            return r0
        L_0x0250:
            r22 = r14
            r14 = r3
            r3 = r8
            r8 = r11
            r11 = r9
            r9 = r10
            r10 = r22
        L_0x0259:
            r25 = r9
            kotlin.jvm.functions.Function2 r9 = r5.$transform     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            java.lang.Object r9 = r9.invoke(r10, r1)     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$0 = r8     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$1 = r7     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$2 = r6     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$3 = r4     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$4 = r15     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$5 = r13     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$6 = r2     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$7 = r3     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$8 = r12     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$9 = r10     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r5.L$10 = r1     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            r21 = r1
            r1 = 5
            r5.label = r1     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            java.lang.Object r9 = r8.send(r9, r5)     // Catch:{ Throwable -> 0x02a3, all -> 0x029e }
            if (r9 != r0) goto L_0x0284
            return r0
        L_0x0284:
            r9 = r0
            r0 = r2
            r19 = r17
            r2 = r21
            r17 = r11
            r11 = r25
        L_0x028e:
            r2 = r5
            r5 = r8
            r10 = r17
            r17 = r19
            r8 = r3
            r3 = r0
            r0 = r9
            r9 = r11
            r11 = r20
        L_0x029a:
            r1 = r24
            goto L_0x01a5
        L_0x029e:
            r0 = move-exception
            r10 = r25
            r9 = r11
            goto L_0x02d2
        L_0x02a3:
            r0 = move-exception
            r10 = r25
            r9 = r11
            goto L_0x02ce
        L_0x02a8:
            kotlin.Unit r0 = kotlin.Unit.INSTANCE     // Catch:{ Throwable -> 0x02b1 }
            kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r15, r13)
            kotlin.Unit r0 = kotlin.Unit.INSTANCE
            return r0
        L_0x02b1:
            r0 = move-exception
            goto L_0x02ce
        L_0x02b3:
            r0 = move-exception
            r5 = r2
            r22 = r10
            r10 = r9
            r9 = r22
            goto L_0x02d2
        L_0x02bb:
            r0 = move-exception
            r5 = r2
            r22 = r10
            r10 = r9
            r9 = r22
            goto L_0x02ce
        L_0x02c3:
            r0 = move-exception
            r5 = r24
            r10 = r25
            goto L_0x02d2
        L_0x02c9:
            r0 = move-exception
            r5 = r24
            r10 = r25
        L_0x02ce:
            r13 = r0
            throw r0     // Catch:{ all -> 0x02d1 }
        L_0x02d1:
            r0 = move-exception
        L_0x02d2:
            kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r15, r13)
            throw r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$zip$2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
