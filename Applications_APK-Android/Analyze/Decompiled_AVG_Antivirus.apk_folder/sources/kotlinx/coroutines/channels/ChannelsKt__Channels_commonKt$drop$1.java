package kotlinx.coroutines.channels;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "E", "Lkotlinx/coroutines/channels/ProducerScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$drop$1", f = "Channels.common.kt", i = {0, 1, 2, 3, 4, 4}, l = {596, 596, 601, 601, 602}, m = "invokeSuspend", n = {"remaining", "remaining", "remaining", "remaining", "remaining", "e"}, s = {"I$0", "I$0", "I$0", "I$0", "I$0", "L$1"})
/* compiled from: Channels.common.kt */
final class ChannelsKt__Channels_commonKt$drop$1 extends SuspendLambda implements Function2<ProducerScope<? super E>, Continuation<? super Unit>, Object> {
    final /* synthetic */ int $n;
    final /* synthetic */ ReceiveChannel $this_drop;
    int I$0;
    Object L$0;
    Object L$1;
    Object L$2;
    int label;
    private ProducerScope p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ChannelsKt__Channels_commonKt$drop$1(ReceiveChannel receiveChannel, int i, Continuation continuation) {
        super(2, continuation);
        this.$this_drop = receiveChannel;
        this.$n = i;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        ChannelsKt__Channels_commonKt$drop$1 channelsKt__Channels_commonKt$drop$1 = new ChannelsKt__Channels_commonKt$drop$1(this.$this_drop, this.$n, continuation);
        ProducerScope producerScope = (ProducerScope) obj;
        channelsKt__Channels_commonKt$drop$1.p$ = (ProducerScope) obj;
        return channelsKt__Channels_commonKt$drop$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((ChannelsKt__Channels_commonKt$drop$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ff A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0130  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r14) {
        /*
            r13 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r13.label
            r2 = 5
            r3 = 4
            r4 = 3
            r5 = 2
            r6 = 0
            r7 = 1
            if (r1 == 0) goto L_0x008a
            if (r1 == r7) goto L_0x0078
            if (r1 == r5) goto L_0x0064
            if (r1 == r4) goto L_0x0051
            if (r1 == r3) goto L_0x003b
            if (r1 != r2) goto L_0x0033
            r1 = 0
            r5 = r6
            java.lang.Object r6 = r13.L$2
            kotlinx.coroutines.channels.ChannelIterator r6 = (kotlinx.coroutines.channels.ChannelIterator) r6
            java.lang.Object r1 = r13.L$1
            int r5 = r13.I$0
            java.lang.Object r7 = r13.L$0
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            kotlin.ResultKt.throwOnFailure(r14)
            r8 = r0
            r0 = r13
            r11 = r7
            r7 = r14
            r14 = r1
            r1 = r5
            r5 = r6
            r6 = r11
            goto L_0x0135
        L_0x0033:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003b:
            r1 = r6
            java.lang.Object r5 = r13.L$1
            kotlinx.coroutines.channels.ChannelIterator r5 = (kotlinx.coroutines.channels.ChannelIterator) r5
            int r1 = r13.I$0
            java.lang.Object r6 = r13.L$0
            kotlinx.coroutines.channels.ProducerScope r6 = (kotlinx.coroutines.channels.ProducerScope) r6
            kotlin.ResultKt.throwOnFailure(r14)
            r8 = r0
            r7 = r6
            r0 = r13
            r6 = r5
            r5 = r1
            r1 = r14
            goto L_0x011f
        L_0x0051:
            r1 = r6
            java.lang.Object r5 = r13.L$1
            kotlinx.coroutines.channels.ChannelIterator r5 = (kotlinx.coroutines.channels.ChannelIterator) r5
            int r1 = r13.I$0
            java.lang.Object r6 = r13.L$0
            kotlinx.coroutines.channels.ProducerScope r6 = (kotlinx.coroutines.channels.ProducerScope) r6
            kotlin.ResultKt.throwOnFailure(r14)
            r7 = r14
            r8 = r0
            r0 = r13
            goto L_0x0103
        L_0x0064:
            r1 = r6
            java.lang.Object r6 = r13.L$1
            kotlinx.coroutines.channels.ChannelIterator r6 = (kotlinx.coroutines.channels.ChannelIterator) r6
            int r1 = r13.I$0
            java.lang.Object r8 = r13.L$0
            kotlinx.coroutines.channels.ProducerScope r8 = (kotlinx.coroutines.channels.ProducerScope) r8
            kotlin.ResultKt.throwOnFailure(r14)
            r9 = r14
            r10 = r6
            r6 = r0
            r0 = r13
            goto L_0x00d5
        L_0x0078:
            r1 = r6
            java.lang.Object r6 = r13.L$1
            kotlinx.coroutines.channels.ChannelIterator r6 = (kotlinx.coroutines.channels.ChannelIterator) r6
            int r1 = r13.I$0
            java.lang.Object r8 = r13.L$0
            kotlinx.coroutines.channels.ProducerScope r8 = (kotlinx.coroutines.channels.ProducerScope) r8
            kotlin.ResultKt.throwOnFailure(r14)
            r10 = r14
            r9 = r0
            r0 = r13
            goto L_0x00ba
        L_0x008a:
            kotlin.ResultKt.throwOnFailure(r14)
            kotlinx.coroutines.channels.ProducerScope r1 = r13.p$
            int r8 = r13.$n
            if (r8 < 0) goto L_0x0094
            r6 = 1
        L_0x0094:
            if (r6 == 0) goto L_0x013a
            int r6 = r13.$n
            if (r6 <= 0) goto L_0x00e4
            kotlinx.coroutines.channels.ReceiveChannel r8 = r13.$this_drop
            kotlinx.coroutines.channels.ChannelIterator r8 = r8.iterator()
            r9 = r14
            r14 = r13
        L_0x00a2:
            r14.L$0 = r1
            r14.I$0 = r6
            r14.L$1 = r8
            r14.label = r7
            java.lang.Object r10 = r8.hasNext(r14)
            if (r10 != r0) goto L_0x00b1
            return r0
        L_0x00b1:
            r11 = r0
            r0 = r14
            r14 = r10
            r10 = r9
            r9 = r11
            r12 = r8
            r8 = r1
            r1 = r6
            r6 = r12
        L_0x00ba:
            java.lang.Boolean r14 = (java.lang.Boolean) r14
            boolean r14 = r14.booleanValue()
            if (r14 == 0) goto L_0x00e1
            r0.L$0 = r8
            r0.I$0 = r1
            r0.L$1 = r6
            r0.label = r5
            java.lang.Object r14 = r6.next(r0)
            if (r14 != r9) goto L_0x00d1
            return r9
        L_0x00d1:
            r11 = r10
            r10 = r6
            r6 = r9
            r9 = r11
        L_0x00d5:
            int r1 = r1 + -1
            if (r1 != 0) goto L_0x00db
            r14 = r9
            goto L_0x00e8
        L_0x00db:
            r14 = r0
            r0 = r6
            r6 = r1
            r1 = r8
            r8 = r10
            goto L_0x00a2
        L_0x00e1:
            r6 = r9
            r14 = r10
            goto L_0x00e8
        L_0x00e4:
            r8 = r1
            r1 = r6
            r6 = r0
            r0 = r13
        L_0x00e8:
            kotlinx.coroutines.channels.ReceiveChannel r5 = r0.$this_drop
            kotlinx.coroutines.channels.ChannelIterator r5 = r5.iterator()
            r7 = r14
            r14 = r6
            r6 = r8
        L_0x00f1:
            r0.L$0 = r6
            r0.I$0 = r1
            r0.L$1 = r5
            r0.label = r4
            java.lang.Object r8 = r5.hasNext(r0)
            if (r8 != r14) goto L_0x0100
            return r14
        L_0x0100:
            r11 = r8
            r8 = r14
            r14 = r11
        L_0x0103:
            java.lang.Boolean r14 = (java.lang.Boolean) r14
            boolean r14 = r14.booleanValue()
            if (r14 == 0) goto L_0x0137
            r0.L$0 = r6
            r0.I$0 = r1
            r0.L$1 = r5
            r0.label = r3
            java.lang.Object r14 = r5.next(r0)
            if (r14 != r8) goto L_0x011a
            return r8
        L_0x011a:
            r11 = r5
            r5 = r1
            r1 = r7
            r7 = r6
            r6 = r11
        L_0x011f:
            r0.L$0 = r7
            r0.I$0 = r5
            r0.L$1 = r14
            r0.L$2 = r6
            r0.label = r2
            java.lang.Object r9 = r7.send(r14, r0)
            if (r9 != r8) goto L_0x0130
            return r8
        L_0x0130:
            r11 = r7
            r7 = r1
            r1 = r5
            r5 = r6
            r6 = r11
        L_0x0135:
            r14 = r8
            goto L_0x00f1
        L_0x0137:
            kotlin.Unit r14 = kotlin.Unit.INSTANCE
            return r14
        L_0x013a:
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Requested element count "
            r1.append(r2)
            int r2 = r13.$n
            r1.append(r2)
            java.lang.String r2 = " is less than zero."
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            java.lang.Throwable r1 = (java.lang.Throwable) r1
            throw r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$drop$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
