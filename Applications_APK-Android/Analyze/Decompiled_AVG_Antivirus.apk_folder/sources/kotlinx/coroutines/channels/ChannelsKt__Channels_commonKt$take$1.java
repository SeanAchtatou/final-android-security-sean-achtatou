package kotlinx.coroutines.channels;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "E", "Lkotlinx/coroutines/channels/ProducerScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$take$1", f = "Channels.common.kt", i = {0, 1, 2, 2}, l = {852, 852, 853}, m = "invokeSuspend", n = {"remaining", "remaining", "remaining", "e"}, s = {"I$0", "I$0", "I$0", "L$1"})
/* compiled from: Channels.common.kt */
final class ChannelsKt__Channels_commonKt$take$1 extends SuspendLambda implements Function2<ProducerScope<? super E>, Continuation<? super Unit>, Object> {
    final /* synthetic */ int $n;
    final /* synthetic */ ReceiveChannel $this_take;
    int I$0;
    Object L$0;
    Object L$1;
    Object L$2;
    int label;
    private ProducerScope p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ChannelsKt__Channels_commonKt$take$1(ReceiveChannel receiveChannel, int i, Continuation continuation) {
        super(2, continuation);
        this.$this_take = receiveChannel;
        this.$n = i;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        ChannelsKt__Channels_commonKt$take$1 channelsKt__Channels_commonKt$take$1 = new ChannelsKt__Channels_commonKt$take$1(this.$this_take, this.$n, continuation);
        ProducerScope producerScope = (ProducerScope) obj;
        channelsKt__Channels_commonKt$take$1.p$ = (ProducerScope) obj;
        return channelsKt__Channels_commonKt$take$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((ChannelsKt__Channels_commonKt$take$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c5  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r12) {
        /*
            r11 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r11.label
            r2 = 3
            r3 = 2
            r4 = 0
            r5 = 1
            if (r1 == 0) goto L_0x005b
            if (r1 == r5) goto L_0x0049
            if (r1 == r3) goto L_0x0033
            if (r1 != r2) goto L_0x002b
            r1 = 0
            java.lang.Object r6 = r11.L$2
            kotlinx.coroutines.channels.ChannelIterator r6 = (kotlinx.coroutines.channels.ChannelIterator) r6
            java.lang.Object r1 = r11.L$1
            int r4 = r11.I$0
            java.lang.Object r7 = r11.L$0
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            kotlin.ResultKt.throwOnFailure(r12)
            r8 = r0
            r0 = r11
            r10 = r7
            r7 = r12
            r12 = r1
            r1 = r6
            r6 = r10
            goto L_0x00be
        L_0x002b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0033:
            r1 = r4
            java.lang.Object r4 = r11.L$1
            kotlinx.coroutines.channels.ChannelIterator r4 = (kotlinx.coroutines.channels.ChannelIterator) r4
            int r1 = r11.I$0
            java.lang.Object r6 = r11.L$0
            kotlinx.coroutines.channels.ProducerScope r6 = (kotlinx.coroutines.channels.ProducerScope) r6
            kotlin.ResultKt.throwOnFailure(r12)
            r8 = r0
            r7 = r6
            r0 = r11
            r6 = r4
            r4 = r1
            r1 = r12
            goto L_0x00a9
        L_0x0049:
            r1 = r4
            java.lang.Object r4 = r11.L$1
            kotlinx.coroutines.channels.ChannelIterator r4 = (kotlinx.coroutines.channels.ChannelIterator) r4
            int r1 = r11.I$0
            java.lang.Object r6 = r11.L$0
            kotlinx.coroutines.channels.ProducerScope r6 = (kotlinx.coroutines.channels.ProducerScope) r6
            kotlin.ResultKt.throwOnFailure(r12)
            r7 = r12
            r8 = r0
            r0 = r11
            goto L_0x008d
        L_0x005b:
            kotlin.ResultKt.throwOnFailure(r12)
            kotlinx.coroutines.channels.ProducerScope r1 = r11.p$
            int r6 = r11.$n
            if (r6 != 0) goto L_0x0067
            kotlin.Unit r0 = kotlin.Unit.INSTANCE
            return r0
        L_0x0067:
            if (r6 < 0) goto L_0x006a
            r4 = 1
        L_0x006a:
            if (r4 == 0) goto L_0x00ce
            int r4 = r11.$n
            kotlinx.coroutines.channels.ReceiveChannel r6 = r11.$this_take
            kotlinx.coroutines.channels.ChannelIterator r6 = r6.iterator()
            r7 = r12
            r12 = r11
            r10 = r6
            r6 = r1
            r1 = r4
            r4 = r10
        L_0x007a:
            r12.L$0 = r6
            r12.I$0 = r1
            r12.L$1 = r4
            r12.label = r5
            java.lang.Object r8 = r4.hasNext(r12)
            if (r8 != r0) goto L_0x0089
            return r0
        L_0x0089:
            r10 = r0
            r0 = r12
            r12 = r8
            r8 = r10
        L_0x008d:
            java.lang.Boolean r12 = (java.lang.Boolean) r12
            boolean r12 = r12.booleanValue()
            if (r12 == 0) goto L_0x00cb
            r0.L$0 = r6
            r0.I$0 = r1
            r0.L$1 = r4
            r0.label = r3
            java.lang.Object r12 = r4.next(r0)
            if (r12 != r8) goto L_0x00a4
            return r8
        L_0x00a4:
            r10 = r4
            r4 = r1
            r1 = r7
            r7 = r6
            r6 = r10
        L_0x00a9:
            r0.L$0 = r7
            r0.I$0 = r4
            r0.L$1 = r12
            r0.L$2 = r6
            r0.label = r2
            java.lang.Object r9 = r7.send(r12, r0)
            if (r9 != r8) goto L_0x00ba
            return r8
        L_0x00ba:
            r10 = r7
            r7 = r1
            r1 = r6
            r6 = r10
        L_0x00be:
            int r4 = r4 + -1
            if (r4 != 0) goto L_0x00c5
            kotlin.Unit r1 = kotlin.Unit.INSTANCE
            return r1
        L_0x00c5:
            r12 = r0
            r0 = r8
            r10 = r4
            r4 = r1
            r1 = r10
            goto L_0x007a
        L_0x00cb:
            kotlin.Unit r12 = kotlin.Unit.INSTANCE
            return r12
        L_0x00ce:
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Requested element count "
            r1.append(r2)
            int r2 = r11.$n
            r1.append(r2)
            java.lang.String r2 = " is less than zero."
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            java.lang.Throwable r1 = (java.lang.Throwable) r1
            throw r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$take$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
