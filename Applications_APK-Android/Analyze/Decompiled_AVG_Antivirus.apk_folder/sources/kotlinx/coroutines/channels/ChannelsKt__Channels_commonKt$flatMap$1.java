package kotlinx.coroutines.channels;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\b\u0012\u0004\u0012\u0002H\u00030\u0004H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "E", "R", "Lkotlinx/coroutines/channels/ProducerScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$flatMap$1", f = "Channels.common.kt", i = {2, 3}, l = {1109, 1109, 1110, 1110}, m = "invokeSuspend", n = {"e", "e"}, s = {"L$1", "L$1"})
/* compiled from: Channels.common.kt */
final class ChannelsKt__Channels_commonKt$flatMap$1 extends SuspendLambda implements Function2<ProducerScope<? super R>, Continuation<? super Unit>, Object> {
    final /* synthetic */ ReceiveChannel $this_flatMap;
    final /* synthetic */ Function2 $transform;
    Object L$0;
    Object L$1;
    Object L$2;
    int label;
    private ProducerScope p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ChannelsKt__Channels_commonKt$flatMap$1(ReceiveChannel receiveChannel, Function2 function2, Continuation continuation) {
        super(2, continuation);
        this.$this_flatMap = receiveChannel;
        this.$transform = function2;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        ChannelsKt__Channels_commonKt$flatMap$1 channelsKt__Channels_commonKt$flatMap$1 = new ChannelsKt__Channels_commonKt$flatMap$1(this.$this_flatMap, this.$transform, continuation);
        ProducerScope producerScope = (ProducerScope) obj;
        channelsKt__Channels_commonKt$flatMap$1.p$ = (ProducerScope) obj;
        return channelsKt__Channels_commonKt$flatMap$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((ChannelsKt__Channels_commonKt$flatMap$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c1  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r14) {
        /*
            r13 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r13.label
            r2 = 4
            r3 = 3
            r4 = 2
            r5 = 1
            if (r1 == 0) goto L_0x0060
            if (r1 == r5) goto L_0x0051
            if (r1 == r4) goto L_0x0042
            r6 = 0
            if (r1 == r3) goto L_0x002f
            if (r1 != r2) goto L_0x0027
            r1 = r6
            java.lang.Object r6 = r13.L$2
            kotlinx.coroutines.channels.ChannelIterator r6 = (kotlinx.coroutines.channels.ChannelIterator) r6
            java.lang.Object r1 = r13.L$1
            java.lang.Object r7 = r13.L$0
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            kotlin.ResultKt.throwOnFailure(r14)
            r9 = r14
            r14 = r13
            goto L_0x00c3
        L_0x0027:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x002f:
            r1 = r6
            java.lang.Object r6 = r13.L$2
            kotlinx.coroutines.channels.ChannelIterator r6 = (kotlinx.coroutines.channels.ChannelIterator) r6
            java.lang.Object r1 = r13.L$1
            java.lang.Object r7 = r13.L$0
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            kotlin.ResultKt.throwOnFailure(r14)
            r9 = r14
            r8 = r0
            r0 = r13
            goto L_0x00ad
        L_0x0042:
            java.lang.Object r1 = r13.L$1
            kotlinx.coroutines.channels.ChannelIterator r1 = (kotlinx.coroutines.channels.ChannelIterator) r1
            java.lang.Object r6 = r13.L$0
            kotlinx.coroutines.channels.ProducerScope r6 = (kotlinx.coroutines.channels.ProducerScope) r6
            kotlin.ResultKt.throwOnFailure(r14)
            r7 = r14
            r8 = r0
            r0 = r13
            goto L_0x0096
        L_0x0051:
            java.lang.Object r1 = r13.L$1
            kotlinx.coroutines.channels.ChannelIterator r1 = (kotlinx.coroutines.channels.ChannelIterator) r1
            java.lang.Object r6 = r13.L$0
            kotlinx.coroutines.channels.ProducerScope r6 = (kotlinx.coroutines.channels.ProducerScope) r6
            kotlin.ResultKt.throwOnFailure(r14)
            r7 = r14
            r8 = r0
            r0 = r13
            goto L_0x0081
        L_0x0060:
            kotlin.ResultKt.throwOnFailure(r14)
            kotlinx.coroutines.channels.ProducerScope r1 = r13.p$
            kotlinx.coroutines.channels.ReceiveChannel r6 = r13.$this_flatMap
            kotlinx.coroutines.channels.ChannelIterator r6 = r6.iterator()
            r7 = r14
            r14 = r13
        L_0x006d:
            r14.L$0 = r1
            r14.L$1 = r6
            r14.label = r5
            java.lang.Object r8 = r6.hasNext(r14)
            if (r8 != r0) goto L_0x007a
            return r0
        L_0x007a:
            r11 = r0
            r0 = r14
            r14 = r8
            r8 = r11
            r12 = r6
            r6 = r1
            r1 = r12
        L_0x0081:
            java.lang.Boolean r14 = (java.lang.Boolean) r14
            boolean r14 = r14.booleanValue()
            if (r14 == 0) goto L_0x00c6
            r0.L$0 = r6
            r0.L$1 = r1
            r0.label = r4
            java.lang.Object r14 = r1.next(r0)
            if (r14 != r8) goto L_0x0096
            return r8
        L_0x0096:
            kotlin.jvm.functions.Function2 r9 = r0.$transform
            r0.L$0 = r6
            r0.L$1 = r14
            r0.L$2 = r1
            r0.label = r3
            java.lang.Object r9 = r9.invoke(r14, r0)
            if (r9 != r8) goto L_0x00a7
            return r8
        L_0x00a7:
            r11 = r1
            r1 = r14
            r14 = r9
            r9 = r7
            r7 = r6
            r6 = r11
        L_0x00ad:
            kotlinx.coroutines.channels.ReceiveChannel r14 = (kotlinx.coroutines.channels.ReceiveChannel) r14
            r10 = r7
            kotlinx.coroutines.channels.SendChannel r10 = (kotlinx.coroutines.channels.SendChannel) r10
            r0.L$0 = r7
            r0.L$1 = r1
            r0.L$2 = r6
            r0.label = r2
            java.lang.Object r14 = kotlinx.coroutines.channels.ChannelsKt.toChannel(r14, r10, r0)
            if (r14 != r8) goto L_0x00c1
            return r8
        L_0x00c1:
            r14 = r0
            r0 = r8
        L_0x00c3:
            r1 = r7
            r7 = r9
            goto L_0x006d
        L_0x00c6:
            kotlin.Unit r14 = kotlin.Unit.INSTANCE
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$flatMap$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
