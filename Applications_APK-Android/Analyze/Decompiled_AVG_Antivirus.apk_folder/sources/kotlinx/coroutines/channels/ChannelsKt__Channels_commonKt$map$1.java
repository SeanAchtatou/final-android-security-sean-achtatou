package kotlinx.coroutines.channels;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\b\u0012\u0004\u0012\u0002H\u00030\u0004H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "E", "R", "Lkotlinx/coroutines/channels/ProducerScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$map$1", f = "Channels.common.kt", i = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3}, l = {1904, 1904, 1203, 1203}, m = "invokeSuspend", n = {"$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "it", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "it"}, s = {"L$1", "L$3", "L$4", "L$5", "L$1", "L$3", "L$4", "L$5", "L$1", "L$3", "L$4", "L$5", "L$7", "L$8", "L$1", "L$3", "L$4", "L$5", "L$7", "L$8"})
/* compiled from: Channels.common.kt */
final class ChannelsKt__Channels_commonKt$map$1 extends SuspendLambda implements Function2<ProducerScope<? super R>, Continuation<? super Unit>, Object> {
    final /* synthetic */ ReceiveChannel $this_map;
    final /* synthetic */ Function2 $transform;
    Object L$0;
    Object L$1;
    Object L$2;
    Object L$3;
    Object L$4;
    Object L$5;
    Object L$6;
    Object L$7;
    Object L$8;
    Object L$9;
    int label;
    private ProducerScope p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ChannelsKt__Channels_commonKt$map$1(ReceiveChannel receiveChannel, Function2 function2, Continuation continuation) {
        super(2, continuation);
        this.$this_map = receiveChannel;
        this.$transform = function2;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        ChannelsKt__Channels_commonKt$map$1 channelsKt__Channels_commonKt$map$1 = new ChannelsKt__Channels_commonKt$map$1(this.$this_map, this.$transform, continuation);
        ProducerScope producerScope = (ProducerScope) obj;
        channelsKt__Channels_commonKt$map$1.p$ = (ProducerScope) obj;
        return channelsKt__Channels_commonKt$map$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((ChannelsKt__Channels_commonKt$map$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* Debug info: failed to restart local var, previous not found, register: 21 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v15, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v16, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v10, resolved type: java.lang.Throwable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v10, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v18, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v14, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v19, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v12, resolved type: java.lang.Throwable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v12, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v10, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v28, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v11, resolved type: java.lang.Throwable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v12, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v15, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0171 A[Catch:{ Throwable -> 0x0213 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01b1  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01e4  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r22) {
        /*
            r21 = this;
            r1 = r21
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r2 = r1.label
            r3 = 4
            r4 = 3
            r5 = 2
            r6 = 1
            r7 = 0
            if (r2 == 0) goto L_0x0127
            r8 = 0
            if (r2 == r6) goto L_0x00e7
            if (r2 == r5) goto L_0x00b5
            if (r2 == r4) goto L_0x005a
            if (r2 != r3) goto L_0x0052
            r2 = r7
            r9 = r8
            r10 = r7
            r11 = r7
            r12 = r8
            r13 = r7
            r14 = r7
            r15 = r8
            java.lang.Object r2 = r1.L$8
            java.lang.Object r10 = r1.L$7
            java.lang.Object r3 = r1.L$6
            kotlinx.coroutines.channels.ChannelIterator r3 = (kotlinx.coroutines.channels.ChannelIterator) r3
            java.lang.Object r4 = r1.L$5
            kotlinx.coroutines.channels.ReceiveChannel r4 = (kotlinx.coroutines.channels.ReceiveChannel) r4
            java.lang.Object r11 = r1.L$4
            java.lang.Throwable r11 = (java.lang.Throwable) r11
            java.lang.Object r13 = r1.L$3
            kotlinx.coroutines.channels.ReceiveChannel r13 = (kotlinx.coroutines.channels.ReceiveChannel) r13
            java.lang.Object r14 = r1.L$2
            kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$map$1 r14 = (kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$map$1) r14
            java.lang.Object r5 = r1.L$1
            kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
            java.lang.Object r7 = r1.L$0
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            kotlin.ResultKt.throwOnFailure(r22)     // Catch:{ Throwable -> 0x0120, all -> 0x0119 }
            r6 = r3
            r3 = r4
            r16 = r8
            r18 = r12
            r4 = r14
            r17 = r15
            r15 = r22
            r8 = r0
            r0 = 4
            goto L_0x01ea
        L_0x0052:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x005a:
            r2 = r7
            r3 = r8
            r4 = r7
            r5 = r7
            r9 = r8
            r10 = r7
            r11 = r7
            r15 = r8
            java.lang.Object r12 = r1.L$9
            kotlinx.coroutines.channels.ProducerScope r12 = (kotlinx.coroutines.channels.ProducerScope) r12
            java.lang.Object r2 = r1.L$8
            java.lang.Object r4 = r1.L$7
            java.lang.Object r13 = r1.L$6
            kotlinx.coroutines.channels.ChannelIterator r13 = (kotlinx.coroutines.channels.ChannelIterator) r13
            java.lang.Object r14 = r1.L$5
            r5 = r14
            kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
            java.lang.Object r14 = r1.L$4
            r10 = r14
            java.lang.Throwable r10 = (java.lang.Throwable) r10
            java.lang.Object r14 = r1.L$3
            r11 = r14
            kotlinx.coroutines.channels.ReceiveChannel r11 = (kotlinx.coroutines.channels.ReceiveChannel) r11
            java.lang.Object r14 = r1.L$2
            kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$map$1 r14 = (kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$map$1) r14
            java.lang.Object r6 = r1.L$1
            kotlinx.coroutines.channels.ReceiveChannel r6 = (kotlinx.coroutines.channels.ReceiveChannel) r6
            java.lang.Object r7 = r1.L$0
            kotlinx.coroutines.channels.ProducerScope r7 = (kotlinx.coroutines.channels.ProducerScope) r7
            kotlin.ResultKt.throwOnFailure(r22)     // Catch:{ Throwable -> 0x00ad, all -> 0x00a5 }
            r16 = r8
            r18 = r9
            r17 = r15
            r15 = r22
            r8 = r0
            r9 = r3
            r3 = r13
            r0 = 3
            r13 = r11
            r11 = r10
            r10 = r4
            r4 = r14
            r14 = r15
            r19 = r6
            r6 = r2
            r2 = r5
            r5 = r19
            goto L_0x01c8
        L_0x00a5:
            r0 = move-exception
            r9 = r22
            r5 = r1
            r13 = r11
            r11 = r10
            goto L_0x0238
        L_0x00ad:
            r0 = move-exception
            r9 = r22
            r5 = r1
            r13 = r11
            r11 = r10
            goto L_0x0234
        L_0x00b5:
            r2 = r7
            r3 = r8
            r4 = r7
            r5 = r7
            r15 = r8
            r6 = r7
            java.lang.Object r7 = r1.L$6
            kotlinx.coroutines.channels.ChannelIterator r7 = (kotlinx.coroutines.channels.ChannelIterator) r7
            java.lang.Object r9 = r1.L$5
            r2 = r9
            kotlinx.coroutines.channels.ReceiveChannel r2 = (kotlinx.coroutines.channels.ReceiveChannel) r2
            java.lang.Object r9 = r1.L$4
            r11 = r9
            java.lang.Throwable r11 = (java.lang.Throwable) r11
            java.lang.Object r4 = r1.L$3
            r13 = r4
            kotlinx.coroutines.channels.ReceiveChannel r13 = (kotlinx.coroutines.channels.ReceiveChannel) r13
            java.lang.Object r4 = r1.L$2
            kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$map$1 r4 = (kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$map$1) r4
            java.lang.Object r5 = r1.L$1
            kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
            java.lang.Object r6 = r1.L$0
            kotlinx.coroutines.channels.ProducerScope r6 = (kotlinx.coroutines.channels.ProducerScope) r6
            kotlin.ResultKt.throwOnFailure(r22)     // Catch:{ Throwable -> 0x0120, all -> 0x0119 }
            r9 = r22
            r12 = r9
            r10 = r0
            r0 = r6
            r14 = 2
            r6 = r5
            r5 = r1
            goto L_0x0189
        L_0x00e7:
            r2 = r7
            r3 = r8
            r4 = r7
            r5 = r7
            r15 = r8
            r6 = r7
            java.lang.Object r7 = r1.L$6
            kotlinx.coroutines.channels.ChannelIterator r7 = (kotlinx.coroutines.channels.ChannelIterator) r7
            java.lang.Object r9 = r1.L$5
            r2 = r9
            kotlinx.coroutines.channels.ReceiveChannel r2 = (kotlinx.coroutines.channels.ReceiveChannel) r2
            java.lang.Object r9 = r1.L$4
            r11 = r9
            java.lang.Throwable r11 = (java.lang.Throwable) r11
            java.lang.Object r4 = r1.L$3
            r13 = r4
            kotlinx.coroutines.channels.ReceiveChannel r13 = (kotlinx.coroutines.channels.ReceiveChannel) r13
            java.lang.Object r4 = r1.L$2
            kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$map$1 r4 = (kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$map$1) r4
            java.lang.Object r5 = r1.L$1
            kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
            java.lang.Object r6 = r1.L$0
            kotlinx.coroutines.channels.ProducerScope r6 = (kotlinx.coroutines.channels.ProducerScope) r6
            kotlin.ResultKt.throwOnFailure(r22)     // Catch:{ Throwable -> 0x0120, all -> 0x0119 }
            r9 = r22
            r14 = r9
            r10 = r0
            r0 = r6
            r12 = 1
            r6 = r5
            r5 = r1
            goto L_0x0169
        L_0x0119:
            r0 = move-exception
            r9 = r22
            r6 = r5
            r5 = r1
            goto L_0x0238
        L_0x0120:
            r0 = move-exception
            r9 = r22
            r6 = r5
            r5 = r1
            goto L_0x0234
        L_0x0127:
            kotlin.ResultKt.throwOnFailure(r22)
            kotlinx.coroutines.channels.ProducerScope r2 = r1.p$
            kotlinx.coroutines.channels.ReceiveChannel r5 = r1.$this_map
            r8 = 0
            r13 = r5
            r15 = 0
            r11 = r7
            java.lang.Throwable r11 = (java.lang.Throwable) r11
            r3 = r13
            r4 = 0
            kotlinx.coroutines.channels.ChannelIterator r6 = r3.iterator()     // Catch:{ Throwable -> 0x022e, all -> 0x0227 }
            r7 = r0
            r0 = r2
            r10 = r4
            r9 = r8
            r8 = r22
            r2 = r1
            r4 = r2
        L_0x0143:
            r2.L$0 = r0     // Catch:{ Throwable -> 0x021e, all -> 0x0215 }
            r2.L$1 = r5     // Catch:{ Throwable -> 0x021e, all -> 0x0215 }
            r2.L$2 = r4     // Catch:{ Throwable -> 0x021e, all -> 0x0215 }
            r2.L$3 = r13     // Catch:{ Throwable -> 0x021e, all -> 0x0215 }
            r2.L$4 = r11     // Catch:{ Throwable -> 0x021e, all -> 0x0215 }
            r2.L$5 = r3     // Catch:{ Throwable -> 0x021e, all -> 0x0215 }
            r2.L$6 = r6     // Catch:{ Throwable -> 0x021e, all -> 0x0215 }
            r12 = 1
            r2.label = r12     // Catch:{ Throwable -> 0x021e, all -> 0x0215 }
            java.lang.Object r14 = r6.hasNext(r4)     // Catch:{ Throwable -> 0x021e, all -> 0x0215 }
            if (r14 != r7) goto L_0x015b
            return r7
        L_0x015b:
            r19 = r5
            r5 = r2
            r2 = r3
            r3 = r10
            r10 = r7
            r7 = r6
            r6 = r19
            r20 = r9
            r9 = r8
            r8 = r20
        L_0x0169:
            java.lang.Boolean r14 = (java.lang.Boolean) r14     // Catch:{ Throwable -> 0x0213 }
            boolean r14 = r14.booleanValue()     // Catch:{ Throwable -> 0x0213 }
            if (r14 == 0) goto L_0x020a
            r5.L$0 = r0     // Catch:{ Throwable -> 0x0213 }
            r5.L$1 = r6     // Catch:{ Throwable -> 0x0213 }
            r5.L$2 = r4     // Catch:{ Throwable -> 0x0213 }
            r5.L$3 = r13     // Catch:{ Throwable -> 0x0213 }
            r5.L$4 = r11     // Catch:{ Throwable -> 0x0213 }
            r5.L$5 = r2     // Catch:{ Throwable -> 0x0213 }
            r5.L$6 = r7     // Catch:{ Throwable -> 0x0213 }
            r14 = 2
            r5.label = r14     // Catch:{ Throwable -> 0x0213 }
            java.lang.Object r12 = r7.next(r4)     // Catch:{ Throwable -> 0x0213 }
            if (r12 != r10) goto L_0x0189
            return r10
        L_0x0189:
            r22 = r12
            r17 = 0
            kotlin.jvm.functions.Function2 r14 = r5.$transform     // Catch:{ Throwable -> 0x0213 }
            r5.L$0 = r0     // Catch:{ Throwable -> 0x0213 }
            r5.L$1 = r6     // Catch:{ Throwable -> 0x0213 }
            r5.L$2 = r4     // Catch:{ Throwable -> 0x0213 }
            r5.L$3 = r13     // Catch:{ Throwable -> 0x0213 }
            r5.L$4 = r11     // Catch:{ Throwable -> 0x0213 }
            r5.L$5 = r2     // Catch:{ Throwable -> 0x0213 }
            r5.L$6 = r7     // Catch:{ Throwable -> 0x0213 }
            r5.L$7 = r12     // Catch:{ Throwable -> 0x0213 }
            r1 = r22
            r5.L$8 = r1     // Catch:{ Throwable -> 0x0213 }
            r5.L$9 = r0     // Catch:{ Throwable -> 0x0213 }
            r22 = r0
            r0 = 3
            r5.label = r0     // Catch:{ Throwable -> 0x0213 }
            java.lang.Object r14 = r14.invoke(r1, r5)     // Catch:{ Throwable -> 0x0213 }
            if (r14 != r10) goto L_0x01b1
            return r10
        L_0x01b1:
            r18 = r3
            r3 = r7
            r16 = r8
            r8 = r10
            r10 = r12
            r7 = r22
            r12 = r7
            r19 = r6
            r6 = r1
            r1 = r5
            r5 = r19
            r20 = r15
            r15 = r9
            r9 = r17
            r17 = r20
        L_0x01c8:
            r1.L$0 = r7     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            r1.L$1 = r5     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            r1.L$2 = r4     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            r1.L$3 = r13     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            r1.L$4 = r11     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            r1.L$5 = r2     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            r1.L$6 = r3     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            r1.L$7 = r10     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            r1.L$8 = r6     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            r0 = 4
            r1.label = r0     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            java.lang.Object r12 = r12.send(r14, r1)     // Catch:{ Throwable -> 0x0201, all -> 0x01f8 }
            if (r12 != r8) goto L_0x01e4
            return r8
        L_0x01e4:
            r19 = r3
            r3 = r2
            r2 = r6
            r6 = r19
        L_0x01ea:
            r2 = r1
            r0 = r7
            r7 = r8
            r8 = r15
            r9 = r16
            r15 = r17
            r10 = r18
            r1 = r21
            goto L_0x0143
        L_0x01f8:
            r0 = move-exception
            r6 = r5
            r9 = r15
            r8 = r16
            r15 = r17
            r5 = r1
            goto L_0x0238
        L_0x0201:
            r0 = move-exception
            r6 = r5
            r9 = r15
            r8 = r16
            r15 = r17
            r5 = r1
            goto L_0x0234
        L_0x020a:
            kotlin.Unit r0 = kotlin.Unit.INSTANCE     // Catch:{ Throwable -> 0x0213 }
            kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r13, r11)
            kotlin.Unit r0 = kotlin.Unit.INSTANCE
            return r0
        L_0x0213:
            r0 = move-exception
            goto L_0x0234
        L_0x0215:
            r0 = move-exception
            r6 = r5
            r5 = r2
            r19 = r9
            r9 = r8
            r8 = r19
            goto L_0x0238
        L_0x021e:
            r0 = move-exception
            r6 = r5
            r5 = r2
            r19 = r9
            r9 = r8
            r8 = r19
            goto L_0x0234
        L_0x0227:
            r0 = move-exception
            r9 = r22
            r6 = r5
            r5 = r21
            goto L_0x0238
        L_0x022e:
            r0 = move-exception
            r9 = r22
            r6 = r5
            r5 = r21
        L_0x0234:
            r11 = r0
            throw r0     // Catch:{ all -> 0x0237 }
        L_0x0237:
            r0 = move-exception
        L_0x0238:
            kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r13, r11)
            throw r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.channels.ChannelsKt__Channels_commonKt$map$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
