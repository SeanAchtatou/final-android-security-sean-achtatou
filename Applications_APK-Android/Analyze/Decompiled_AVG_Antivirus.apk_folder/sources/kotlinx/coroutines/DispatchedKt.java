package kotlinx.coroutines;

import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import kotlinx.coroutines.internal.Symbol;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0003\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\"\u0010\u0004\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00072\b\b\u0002\u0010\b\u001a\u00020\tH\u0000\u001a;\u0010\n\u001a\u00020\u000b*\u0006\u0012\u0002\b\u00030\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\u000f\u001a\u00020\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\u0011H\b\u001a.\u0010\u0012\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00072\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u0002H\u00060\u00142\u0006\u0010\u0015\u001a\u00020\tH\u0000\u001a%\u0010\u0016\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00142\u0006\u0010\u0017\u001a\u0002H\u0006H\u0000¢\u0006\u0002\u0010\u0018\u001a \u0010\u0019\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\u0000\u001a%\u0010\u001c\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00142\u0006\u0010\u0017\u001a\u0002H\u0006H\u0000¢\u0006\u0002\u0010\u0018\u001a \u0010\u001d\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\u0000\u001a\u0010\u0010\u001e\u001a\u00020\u0005*\u0006\u0012\u0002\b\u00030\u0007H\u0002\u001a\u0019\u0010\u001f\u001a\u00020\u0005*\u0006\u0012\u0002\b\u00030\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\b\u001a'\u0010 \u001a\u00020\u0005*\u0006\u0012\u0002\b\u00030\u00072\u0006\u0010!\u001a\u00020\"2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\u0011H\b\u001a\u0012\u0010#\u001a\u00020\u000b*\b\u0012\u0004\u0012\u00020\u00050\fH\u0000\"\u0016\u0010\u0000\u001a\u00020\u00018\u0002X\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0002\u0010\u0003¨\u0006$"}, d2 = {"UNDEFINED", "Lkotlinx/coroutines/internal/Symbol;", "UNDEFINED$annotations", "()V", "dispatch", "", "T", "Lkotlinx/coroutines/DispatchedTask;", "mode", "", "executeUnconfined", "", "Lkotlinx/coroutines/DispatchedContinuation;", "contState", "", "doYield", "block", "Lkotlin/Function0;", "resume", "delegate", "Lkotlin/coroutines/Continuation;", "useMode", "resumeCancellable", "value", "(Lkotlin/coroutines/Continuation;Ljava/lang/Object;)V", "resumeCancellableWithException", "exception", "", "resumeDirect", "resumeDirectWithException", "resumeUnconfined", "resumeWithStackTrace", "runUnconfinedEventLoop", "eventLoop", "Lkotlinx/coroutines/EventLoop;", "yieldUndispatched", "kotlinx-coroutines-core"}, k = 2, mv = {1, 1, 15})
/* compiled from: Dispatched.kt */
public final class DispatchedKt {
    /* access modifiers changed from: private */
    public static final Symbol UNDEFINED = new Symbol("UNDEFINED");

    private static /* synthetic */ void UNDEFINED$annotations() {
    }

    static /* synthetic */ boolean executeUnconfined$default(DispatchedContinuation $this$executeUnconfined, Object contState, int mode, boolean doYield, Function0 block, int i, Object obj) {
        if ((i & 4) != 0) {
            doYield = false;
        }
        EventLoop eventLoop = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
        if (doYield && eventLoop.isUnconfinedQueueEmpty()) {
            return false;
        }
        if (eventLoop.isUnconfinedLoopActive()) {
            $this$executeUnconfined._state = contState;
            $this$executeUnconfined.resumeMode = mode;
            eventLoop.dispatchUnconfined($this$executeUnconfined);
            return true;
        }
        DispatchedTask $this$runUnconfinedEventLoop$iv = $this$executeUnconfined;
        eventLoop.incrementUseCount(true);
        try {
            block.invoke();
            do {
            } while (eventLoop.processUnconfinedEvent());
            InlineMarker.finallyStart(1);
        } catch (Throwable th) {
            InlineMarker.finallyStart(1);
            eventLoop.decrementUseCount(true);
            InlineMarker.finallyEnd(1);
            throw th;
        }
        eventLoop.decrementUseCount(true);
        InlineMarker.finallyEnd(1);
        return false;
    }

    private static final boolean executeUnconfined(@NotNull DispatchedContinuation<?> $this$executeUnconfined, Object contState, int mode, boolean doYield, Function0<Unit> block) {
        EventLoop eventLoop = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
        if (doYield && eventLoop.isUnconfinedQueueEmpty()) {
            return false;
        }
        if (eventLoop.isUnconfinedLoopActive()) {
            $this$executeUnconfined._state = contState;
            $this$executeUnconfined.resumeMode = mode;
            eventLoop.dispatchUnconfined($this$executeUnconfined);
            return true;
        }
        DispatchedTask $this$runUnconfinedEventLoop$iv = $this$executeUnconfined;
        eventLoop.incrementUseCount(true);
        try {
            block.invoke();
            do {
            } while (eventLoop.processUnconfinedEvent());
            InlineMarker.finallyStart(1);
        } catch (Throwable th) {
            InlineMarker.finallyStart(1);
            eventLoop.decrementUseCount(true);
            InlineMarker.finallyEnd(1);
            throw th;
        }
        eventLoop.decrementUseCount(true);
        InlineMarker.finallyEnd(1);
        return false;
    }

    private static final void resumeUnconfined(@NotNull DispatchedTask<?> $this$resumeUnconfined) {
        EventLoop eventLoop = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
        if (eventLoop.isUnconfinedLoopActive()) {
            eventLoop.dispatchUnconfined($this$resumeUnconfined);
            return;
        }
        DispatchedTask $this$runUnconfinedEventLoop$iv = $this$resumeUnconfined;
        eventLoop.incrementUseCount(true);
        try {
            resume($this$resumeUnconfined, $this$resumeUnconfined.getDelegate$kotlinx_coroutines_core(), 3);
            do {
            } while (eventLoop.processUnconfinedEvent());
        } catch (Throwable th) {
            eventLoop.decrementUseCount(true);
            throw th;
        }
        eventLoop.decrementUseCount(true);
    }

    /* access modifiers changed from: private */
    public static final void runUnconfinedEventLoop(@NotNull DispatchedTask<?> $this$runUnconfinedEventLoop, EventLoop eventLoop, Function0<Unit> block) {
        eventLoop.incrementUseCount(true);
        try {
            block.invoke();
            do {
            } while (eventLoop.processUnconfinedEvent());
            InlineMarker.finallyStart(1);
        } catch (Throwable th) {
            InlineMarker.finallyStart(1);
            eventLoop.decrementUseCount(true);
            InlineMarker.finallyEnd(1);
            throw th;
        }
        eventLoop.decrementUseCount(true);
        InlineMarker.finallyEnd(1);
    }

    /* JADX INFO: Multiple debug info for r3v2 kotlin.coroutines.CoroutineContext: [D('context$iv$iv$iv' kotlin.coroutines.CoroutineContext), D('this_$iv' kotlinx.coroutines.DispatchedContinuation)] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00d4 A[Catch:{ all -> 0x00c7, Throwable -> 0x00e1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T> void resumeCancellable(@org.jetbrains.annotations.NotNull kotlin.coroutines.Continuation<? super T> r21, T r22) {
        /*
            r1 = r21
            r2 = r22
            java.lang.String r0 = "$this$resumeCancellable"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r1, r0)
            boolean r0 = r1 instanceof kotlinx.coroutines.DispatchedContinuation
            if (r0 == 0) goto L_0x00fe
            r3 = r1
            kotlinx.coroutines.DispatchedContinuation r3 = (kotlinx.coroutines.DispatchedContinuation) r3
            r4 = 0
            kotlinx.coroutines.CoroutineDispatcher r0 = r3.dispatcher
            kotlin.coroutines.CoroutineContext r5 = r3.getContext()
            boolean r0 = r0.isDispatchNeeded(r5)
            r5 = 1
            if (r0 == 0) goto L_0x0031
            r3._state = r2
            r3.resumeMode = r5
            kotlinx.coroutines.CoroutineDispatcher r0 = r3.dispatcher
            kotlin.coroutines.CoroutineContext r5 = r3.getContext()
            r6 = r3
            java.lang.Runnable r6 = (java.lang.Runnable) r6
            r0.dispatch(r5, r6)
            goto L_0x00f6
        L_0x0031:
            r0 = 1
            r6 = r3
            r7 = r0
            r8 = 0
            r9 = 0
            kotlinx.coroutines.ThreadLocalEventLoop r0 = kotlinx.coroutines.ThreadLocalEventLoop.INSTANCE
            kotlinx.coroutines.EventLoop r10 = r0.getEventLoop$kotlinx_coroutines_core()
            boolean r0 = r10.isUnconfinedLoopActive()
            if (r0 == 0) goto L_0x0051
            r6._state = r2
            r6.resumeMode = r7
            r0 = r6
            kotlinx.coroutines.DispatchedTask r0 = (kotlinx.coroutines.DispatchedTask) r0
            r10.dispatchUnconfined(r0)
            r18 = r3
            goto L_0x00f5
        L_0x0051:
            r11 = r6
            kotlinx.coroutines.DispatchedTask r11 = (kotlinx.coroutines.DispatchedTask) r11
            r12 = 0
            r10.incrementUseCount(r5)
            r13 = 0
            r0 = r3
            r14 = 0
            kotlin.coroutines.CoroutineContext r15 = r0.getContext()     // Catch:{ Throwable -> 0x00e7, all -> 0x00e3 }
            kotlinx.coroutines.Job$Key r16 = kotlinx.coroutines.Job.Key     // Catch:{ Throwable -> 0x00e7, all -> 0x00e3 }
            r5 = r16
            kotlin.coroutines.CoroutineContext$Key r5 = (kotlin.coroutines.CoroutineContext.Key) r5     // Catch:{ Throwable -> 0x00e7, all -> 0x00e3 }
            kotlin.coroutines.CoroutineContext$Element r5 = r15.get(r5)     // Catch:{ Throwable -> 0x00e7, all -> 0x00e3 }
            kotlinx.coroutines.Job r5 = (kotlinx.coroutines.Job) r5     // Catch:{ Throwable -> 0x00e7, all -> 0x00e3 }
            if (r5 == 0) goto L_0x0092
            boolean r15 = r5.isActive()     // Catch:{ Throwable -> 0x008e, all -> 0x0089 }
            if (r15 != 0) goto L_0x0092
            java.util.concurrent.CancellationException r15 = r5.getCancellationException()     // Catch:{ Throwable -> 0x008e, all -> 0x0089 }
            java.lang.Throwable r15 = (java.lang.Throwable) r15     // Catch:{ Throwable -> 0x008e, all -> 0x0089 }
            kotlin.Result$Companion r16 = kotlin.Result.Companion     // Catch:{ Throwable -> 0x008e, all -> 0x0089 }
            java.lang.Object r15 = kotlin.ResultKt.createFailure(r15)     // Catch:{ Throwable -> 0x008e, all -> 0x0089 }
            java.lang.Object r15 = kotlin.Result.m3constructorimpl(r15)     // Catch:{ Throwable -> 0x008e, all -> 0x0089 }
            r0.resumeWith(r15)     // Catch:{ Throwable -> 0x008e, all -> 0x0089 }
            r15 = 1
            goto L_0x0093
        L_0x0089:
            r0 = move-exception
            r18 = r3
            goto L_0x00f9
        L_0x008e:
            r0 = move-exception
            r18 = r3
            goto L_0x00ea
        L_0x0092:
            r15 = 0
        L_0x0093:
            if (r15 != 0) goto L_0x00d4
            r0 = r22
            r5 = r3
            r14 = r0
            r15 = 0
            kotlin.coroutines.CoroutineContext r0 = r5.getContext()     // Catch:{ Throwable -> 0x00e7, all -> 0x00e3 }
            java.lang.Object r2 = r5.countOrElement     // Catch:{ Throwable -> 0x00e7, all -> 0x00e3 }
            r16 = r0
            r17 = 0
            r18 = r3
            r3 = r16
            java.lang.Object r0 = kotlinx.coroutines.internal.ThreadContextKt.updateThreadContext(r3, r2)     // Catch:{ Throwable -> 0x00e1 }
            r16 = r0
            r0 = 0
            r19 = r0
            kotlin.coroutines.Continuation<T> r0 = r5.continuation     // Catch:{ all -> 0x00cb }
            kotlin.Result$Companion r20 = kotlin.Result.Companion     // Catch:{ all -> 0x00cb }
            r20 = r2
            java.lang.Object r2 = kotlin.Result.m3constructorimpl(r14)     // Catch:{ all -> 0x00c7 }
            r0.resumeWith(r2)     // Catch:{ all -> 0x00c7 }
            kotlin.Unit r0 = kotlin.Unit.INSTANCE     // Catch:{ all -> 0x00c7 }
            r2 = r16
            kotlinx.coroutines.internal.ThreadContextKt.restoreThreadContext(r3, r2)     // Catch:{ Throwable -> 0x00e1 }
            goto L_0x00d6
        L_0x00c7:
            r0 = move-exception
            r2 = r16
            goto L_0x00d0
        L_0x00cb:
            r0 = move-exception
            r20 = r2
            r2 = r16
        L_0x00d0:
            kotlinx.coroutines.internal.ThreadContextKt.restoreThreadContext(r3, r2)     // Catch:{ Throwable -> 0x00e1 }
            throw r0     // Catch:{ Throwable -> 0x00e1 }
        L_0x00d4:
            r18 = r3
        L_0x00d6:
        L_0x00d8:
            boolean r0 = r10.processUnconfinedEvent()     // Catch:{ Throwable -> 0x00e1 }
            if (r0 != 0) goto L_0x00e0
            goto L_0x00ee
        L_0x00e0:
            goto L_0x00d8
        L_0x00e1:
            r0 = move-exception
            goto L_0x00ea
        L_0x00e3:
            r0 = move-exception
            r18 = r3
            goto L_0x00f9
        L_0x00e7:
            r0 = move-exception
            r18 = r3
        L_0x00ea:
            r2 = 0
            r11.handleFatalException$kotlinx_coroutines_core(r0, r2)     // Catch:{ all -> 0x00f8 }
        L_0x00ee:
            r2 = 1
            r10.decrementUseCount(r2)
        L_0x00f5:
        L_0x00f6:
            goto L_0x0107
        L_0x00f8:
            r0 = move-exception
        L_0x00f9:
            r2 = 1
            r10.decrementUseCount(r2)
            throw r0
        L_0x00fe:
            kotlin.Result$Companion r0 = kotlin.Result.Companion
            java.lang.Object r0 = kotlin.Result.m3constructorimpl(r22)
            r1.resumeWith(r0)
        L_0x0107:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.DispatchedKt.resumeCancellable(kotlin.coroutines.Continuation, java.lang.Object):void");
    }

    /* JADX INFO: Multiple debug info for r0v25 kotlin.coroutines.Continuation<T>: [D('$this$resumeWithStackTrace$iv$iv$iv' kotlin.coroutines.Continuation), D('$i$a$-withCoroutineContext-DispatchedContinuation$resumeUndispatchedWithException$1' int)] */
    /* JADX INFO: Multiple debug info for r3v8 java.lang.Throwable: [D('this_$iv' kotlinx.coroutines.DispatchedContinuation), D('exception$iv$iv$iv' java.lang.Throwable)] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00fd A[Catch:{ all -> 0x00f0, Throwable -> 0x010a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T> void resumeCancellableWithException(@org.jetbrains.annotations.NotNull kotlin.coroutines.Continuation<? super T> r27, @org.jetbrains.annotations.NotNull java.lang.Throwable r28) {
        /*
            r1 = r27
            r2 = r28
            java.lang.String r0 = "$this$resumeCancellableWithException"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r1, r0)
            java.lang.String r0 = "exception"
            kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(r2, r0)
            boolean r0 = r1 instanceof kotlinx.coroutines.DispatchedContinuation
            if (r0 == 0) goto L_0x0127
            r3 = r1
            kotlinx.coroutines.DispatchedContinuation r3 = (kotlinx.coroutines.DispatchedContinuation) r3
            r4 = 0
            kotlin.coroutines.Continuation<T> r0 = r3.continuation
            kotlin.coroutines.CoroutineContext r5 = r0.getContext()
            kotlinx.coroutines.CompletedExceptionally r0 = new kotlinx.coroutines.CompletedExceptionally
            r6 = 2
            r7 = 0
            r8 = 0
            r0.<init>(r2, r8, r6, r7)
            r9 = r0
            kotlinx.coroutines.CoroutineDispatcher r0 = r3.dispatcher
            boolean r0 = r0.isDispatchNeeded(r5)
            r10 = 1
            if (r0 == 0) goto L_0x0042
            kotlinx.coroutines.CompletedExceptionally r0 = new kotlinx.coroutines.CompletedExceptionally
            r0.<init>(r2, r8, r6, r7)
            r3._state = r0
            r3.resumeMode = r10
            kotlinx.coroutines.CoroutineDispatcher r0 = r3.dispatcher
            r6 = r3
            java.lang.Runnable r6 = (java.lang.Runnable) r6
            r0.dispatch(r5, r6)
            goto L_0x011f
        L_0x0042:
            r0 = 1
            r6 = r3
            r11 = r0
            r12 = 0
            r13 = 0
            kotlinx.coroutines.ThreadLocalEventLoop r0 = kotlinx.coroutines.ThreadLocalEventLoop.INSTANCE
            kotlinx.coroutines.EventLoop r14 = r0.getEventLoop$kotlinx_coroutines_core()
            boolean r0 = r14.isUnconfinedLoopActive()
            if (r0 == 0) goto L_0x0062
            r6._state = r9
            r6.resumeMode = r11
            r0 = r6
            kotlinx.coroutines.DispatchedTask r0 = (kotlinx.coroutines.DispatchedTask) r0
            r14.dispatchUnconfined(r0)
            r25 = r3
            goto L_0x011e
        L_0x0062:
            r15 = r6
            kotlinx.coroutines.DispatchedTask r15 = (kotlinx.coroutines.DispatchedTask) r15
            r16 = 0
            r14.incrementUseCount(r10)
            r17 = 0
            r0 = r3
            r18 = 0
            kotlin.coroutines.CoroutineContext r8 = r0.getContext()     // Catch:{ Throwable -> 0x0110, all -> 0x010c }
            kotlinx.coroutines.Job$Key r20 = kotlinx.coroutines.Job.Key     // Catch:{ Throwable -> 0x0110, all -> 0x010c }
            r7 = r20
            kotlin.coroutines.CoroutineContext$Key r7 = (kotlin.coroutines.CoroutineContext.Key) r7     // Catch:{ Throwable -> 0x0110, all -> 0x010c }
            kotlin.coroutines.CoroutineContext$Element r7 = r8.get(r7)     // Catch:{ Throwable -> 0x0110, all -> 0x010c }
            kotlinx.coroutines.Job r7 = (kotlinx.coroutines.Job) r7     // Catch:{ Throwable -> 0x0110, all -> 0x010c }
            if (r7 == 0) goto L_0x00a8
            boolean r8 = r7.isActive()     // Catch:{ Throwable -> 0x00a3, all -> 0x009e }
            if (r8 != 0) goto L_0x00a8
            java.util.concurrent.CancellationException r8 = r7.getCancellationException()     // Catch:{ Throwable -> 0x00a3, all -> 0x009e }
            java.lang.Throwable r8 = (java.lang.Throwable) r8     // Catch:{ Throwable -> 0x00a3, all -> 0x009e }
            kotlin.Result$Companion r19 = kotlin.Result.Companion     // Catch:{ Throwable -> 0x00a3, all -> 0x009e }
            java.lang.Object r8 = kotlin.ResultKt.createFailure(r8)     // Catch:{ Throwable -> 0x00a3, all -> 0x009e }
            java.lang.Object r8 = kotlin.Result.m3constructorimpl(r8)     // Catch:{ Throwable -> 0x00a3, all -> 0x009e }
            r0.resumeWith(r8)     // Catch:{ Throwable -> 0x00a3, all -> 0x009e }
            r19 = 1
            goto L_0x00aa
        L_0x009e:
            r0 = move-exception
            r25 = r3
            goto L_0x0122
        L_0x00a3:
            r0 = move-exception
            r25 = r3
            goto L_0x0113
        L_0x00a8:
            r19 = 0
        L_0x00aa:
            if (r19 != 0) goto L_0x00fd
            r0 = r28
            r7 = r3
            r8 = r0
            r18 = 0
            kotlin.coroutines.CoroutineContext r0 = r7.getContext()     // Catch:{ Throwable -> 0x0110, all -> 0x010c }
            java.lang.Object r10 = r7.countOrElement     // Catch:{ Throwable -> 0x0110, all -> 0x010c }
            r20 = r0
            r21 = 0
            r1 = r20
            java.lang.Object r0 = kotlinx.coroutines.internal.ThreadContextKt.updateThreadContext(r1, r10)     // Catch:{ Throwable -> 0x0110, all -> 0x010c }
            r20 = r0
            r0 = 0
            r22 = r0
            kotlin.coroutines.Continuation<T> r0 = r7.continuation     // Catch:{ all -> 0x00f4 }
            r23 = r8
            r24 = r23
            r23 = 0
            kotlin.Result$Companion r25 = kotlin.Result.Companion     // Catch:{ all -> 0x00f4 }
            r25 = r3
            r3 = r24
            java.lang.Throwable r24 = kotlinx.coroutines.internal.StackTraceRecoveryKt.recoverStackTrace(r3, r0)     // Catch:{ all -> 0x00f0 }
            java.lang.Object r24 = kotlin.ResultKt.createFailure(r24)     // Catch:{ all -> 0x00f0 }
            r26 = r3
            java.lang.Object r3 = kotlin.Result.m3constructorimpl(r24)     // Catch:{ all -> 0x00f0 }
            r0.resumeWith(r3)     // Catch:{ all -> 0x00f0 }
            kotlin.Unit r0 = kotlin.Unit.INSTANCE     // Catch:{ all -> 0x00f0 }
            r3 = r20
            kotlinx.coroutines.internal.ThreadContextKt.restoreThreadContext(r1, r3)     // Catch:{ Throwable -> 0x010a }
            goto L_0x00ff
        L_0x00f0:
            r0 = move-exception
            r3 = r20
            goto L_0x00f9
        L_0x00f4:
            r0 = move-exception
            r25 = r3
            r3 = r20
        L_0x00f9:
            kotlinx.coroutines.internal.ThreadContextKt.restoreThreadContext(r1, r3)     // Catch:{ Throwable -> 0x010a }
            throw r0     // Catch:{ Throwable -> 0x010a }
        L_0x00fd:
            r25 = r3
        L_0x00ff:
        L_0x0101:
            boolean r0 = r14.processUnconfinedEvent()     // Catch:{ Throwable -> 0x010a }
            if (r0 != 0) goto L_0x0109
            goto L_0x0117
        L_0x0109:
            goto L_0x0101
        L_0x010a:
            r0 = move-exception
            goto L_0x0113
        L_0x010c:
            r0 = move-exception
            r25 = r3
            goto L_0x0122
        L_0x0110:
            r0 = move-exception
            r25 = r3
        L_0x0113:
            r1 = 0
            r15.handleFatalException$kotlinx_coroutines_core(r0, r1)     // Catch:{ all -> 0x0121 }
        L_0x0117:
            r1 = 1
            r14.decrementUseCount(r1)
        L_0x011e:
        L_0x011f:
            goto L_0x013c
        L_0x0121:
            r0 = move-exception
        L_0x0122:
            r1 = 1
            r14.decrementUseCount(r1)
            throw r0
        L_0x0127:
            r0 = r27
            r1 = 0
            kotlin.Result$Companion r3 = kotlin.Result.Companion
            java.lang.Throwable r3 = kotlinx.coroutines.internal.StackTraceRecoveryKt.recoverStackTrace(r2, r0)
            java.lang.Object r3 = kotlin.ResultKt.createFailure(r3)
            java.lang.Object r3 = kotlin.Result.m3constructorimpl(r3)
            r0.resumeWith(r3)
        L_0x013c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.DispatchedKt.resumeCancellableWithException(kotlin.coroutines.Continuation, java.lang.Throwable):void");
    }

    public static final <T> void resumeDirect(@NotNull Continuation<? super T> $this$resumeDirect, T value) {
        Intrinsics.checkParameterIsNotNull($this$resumeDirect, "$this$resumeDirect");
        if ($this$resumeDirect instanceof DispatchedContinuation) {
            Continuation<T> continuation = ((DispatchedContinuation) $this$resumeDirect).continuation;
            Result.Companion companion = Result.Companion;
            continuation.resumeWith(Result.m3constructorimpl(value));
            return;
        }
        Result.Companion companion2 = Result.Companion;
        $this$resumeDirect.resumeWith(Result.m3constructorimpl(value));
    }

    public static final <T> void resumeDirectWithException(@NotNull Continuation<? super T> $this$resumeDirectWithException, @NotNull Throwable exception) {
        Intrinsics.checkParameterIsNotNull($this$resumeDirectWithException, "$this$resumeDirectWithException");
        Intrinsics.checkParameterIsNotNull(exception, "exception");
        if ($this$resumeDirectWithException instanceof DispatchedContinuation) {
            Continuation $this$resumeWithStackTrace$iv = ((DispatchedContinuation) $this$resumeDirectWithException).continuation;
            Result.Companion companion = Result.Companion;
            $this$resumeWithStackTrace$iv.resumeWith(Result.m3constructorimpl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(exception, $this$resumeWithStackTrace$iv))));
            return;
        }
        Continuation $this$resumeWithStackTrace$iv2 = $this$resumeDirectWithException;
        Result.Companion companion2 = Result.Companion;
        $this$resumeWithStackTrace$iv2.resumeWith(Result.m3constructorimpl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(exception, $this$resumeWithStackTrace$iv2))));
    }

    public static final boolean yieldUndispatched(@NotNull DispatchedContinuation<? super Unit> $this$yieldUndispatched) {
        Intrinsics.checkParameterIsNotNull($this$yieldUndispatched, "$this$yieldUndispatched");
        Object contState$iv = Unit.INSTANCE;
        DispatchedContinuation<? super Unit> dispatchedContinuation = $this$yieldUndispatched;
        EventLoop eventLoop$iv = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
        if (eventLoop$iv.isUnconfinedQueueEmpty()) {
            return false;
        }
        if (eventLoop$iv.isUnconfinedLoopActive()) {
            dispatchedContinuation._state = contState$iv;
            dispatchedContinuation.resumeMode = 1;
            eventLoop$iv.dispatchUnconfined(dispatchedContinuation);
            return true;
        }
        DispatchedTask $this$runUnconfinedEventLoop$iv$iv = dispatchedContinuation;
        eventLoop$iv.incrementUseCount(true);
        try {
            $this$yieldUndispatched.run();
            do {
            } while (eventLoop$iv.processUnconfinedEvent());
        } catch (Throwable th) {
            eventLoop$iv.decrementUseCount(true);
            throw th;
        }
        eventLoop$iv.decrementUseCount(true);
        return false;
    }

    public static /* synthetic */ void dispatch$default(DispatchedTask dispatchedTask, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 1;
        }
        dispatch(dispatchedTask, i);
    }

    public static final <T> void dispatch(@NotNull DispatchedTask<? super T> $this$dispatch, int mode) {
        Intrinsics.checkParameterIsNotNull($this$dispatch, "$this$dispatch");
        Continuation<? super T> delegate$kotlinx_coroutines_core = $this$dispatch.getDelegate$kotlinx_coroutines_core();
        if (!ResumeModeKt.isDispatchedMode(mode) || !(delegate$kotlinx_coroutines_core instanceof DispatchedContinuation) || ResumeModeKt.isCancellableMode(mode) != ResumeModeKt.isCancellableMode($this$dispatch.resumeMode)) {
            resume($this$dispatch, delegate$kotlinx_coroutines_core, mode);
            return;
        }
        CoroutineDispatcher dispatcher = ((DispatchedContinuation) delegate$kotlinx_coroutines_core).dispatcher;
        CoroutineContext context = delegate$kotlinx_coroutines_core.getContext();
        if (dispatcher.isDispatchNeeded(context)) {
            dispatcher.dispatch(context, $this$dispatch);
        } else {
            resumeUnconfined($this$dispatch);
        }
    }

    public static final <T> void resume(@NotNull DispatchedTask<? super T> $this$resume, @NotNull Continuation<? super T> delegate, int useMode) {
        Intrinsics.checkParameterIsNotNull($this$resume, "$this$resume");
        Intrinsics.checkParameterIsNotNull(delegate, "delegate");
        Object state = $this$resume.takeState$kotlinx_coroutines_core();
        Throwable exception = $this$resume.getExceptionalResult$kotlinx_coroutines_core(state);
        if (exception != null) {
            ResumeModeKt.resumeWithExceptionMode(delegate, exception, useMode);
        } else {
            ResumeModeKt.resumeMode(delegate, $this$resume.getSuccessfulResult$kotlinx_coroutines_core(state), useMode);
        }
    }

    public static final void resumeWithStackTrace(@NotNull Continuation<?> $this$resumeWithStackTrace, @NotNull Throwable exception) {
        Intrinsics.checkParameterIsNotNull($this$resumeWithStackTrace, "$this$resumeWithStackTrace");
        Intrinsics.checkParameterIsNotNull(exception, "exception");
        Result.Companion companion = Result.Companion;
        $this$resumeWithStackTrace.resumeWith(Result.m3constructorimpl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(exception, $this$resumeWithStackTrace))));
    }
}
