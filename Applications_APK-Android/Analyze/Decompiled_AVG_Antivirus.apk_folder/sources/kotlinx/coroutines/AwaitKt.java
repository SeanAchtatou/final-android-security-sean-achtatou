package kotlinx.coroutines;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0002\u001a=\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u001e\u0010\u0003\u001a\u0010\u0012\f\b\u0001\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00050\u0004\"\b\u0012\u0004\u0012\u0002H\u00020\u0005H@ø\u0001\u0000¢\u0006\u0002\u0010\u0006\u001a%\u0010\u0007\u001a\u00020\b2\u0012\u0010\t\u001a\n\u0012\u0006\b\u0001\u0012\u00020\n0\u0004\"\u00020\nH@ø\u0001\u0000¢\u0006\u0002\u0010\u000b\u001a-\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00050\fH@ø\u0001\u0000¢\u0006\u0002\u0010\r\u001a\u001b\u0010\u0007\u001a\u00020\b*\b\u0012\u0004\u0012\u00020\n0\fH@ø\u0001\u0000¢\u0006\u0002\u0010\r\u0002\u0004\n\u0002\b\u0019¨\u0006\u000e"}, d2 = {"awaitAll", "", "T", "deferreds", "", "Lkotlinx/coroutines/Deferred;", "([Lkotlinx/coroutines/Deferred;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "joinAll", "", "jobs", "Lkotlinx/coroutines/Job;", "([Lkotlinx/coroutines/Job;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "", "(Ljava/util/Collection;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "kotlinx-coroutines-core"}, k = 2, mv = {1, 1, 15})
/* compiled from: Await.kt */
public final class AwaitKt {
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T> java.lang.Object awaitAll(@org.jetbrains.annotations.NotNull kotlinx.coroutines.Deferred<? extends T>[] r5, @org.jetbrains.annotations.NotNull kotlin.coroutines.Continuation<? super java.util.List<? extends T>> r6) {
        /*
            boolean r0 = r6 instanceof kotlinx.coroutines.AwaitKt$awaitAll$1
            if (r0 == 0) goto L_0x0014
            r0 = r6
            kotlinx.coroutines.AwaitKt$awaitAll$1 r0 = (kotlinx.coroutines.AwaitKt$awaitAll$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0014
            int r1 = r0.label
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0019
        L_0x0014:
            kotlinx.coroutines.AwaitKt$awaitAll$1 r0 = new kotlinx.coroutines.AwaitKt$awaitAll$1
            r0.<init>(r6)
        L_0x0019:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r3 = r0.label
            r4 = 1
            if (r3 == 0) goto L_0x0037
            if (r3 != r4) goto L_0x002f
            java.lang.Object r2 = r0.L$0
            r5 = r2
            kotlinx.coroutines.Deferred[] r5 = (kotlinx.coroutines.Deferred[]) r5
            kotlin.ResultKt.throwOnFailure(r1)
            goto L_0x0057
        L_0x002f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0037:
            kotlin.ResultKt.throwOnFailure(r1)
            int r1 = r5.length
            if (r1 != 0) goto L_0x003f
            r1 = 1
            goto L_0x0040
        L_0x003f:
            r1 = 0
        L_0x0040:
            if (r1 == 0) goto L_0x0047
            java.util.List r1 = kotlin.collections.CollectionsKt.emptyList()
            goto L_0x0059
        L_0x0047:
            kotlinx.coroutines.AwaitAll r1 = new kotlinx.coroutines.AwaitAll
            r1.<init>(r5)
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r1 = r1.await(r0)
            if (r1 != r2) goto L_0x0057
            return r2
        L_0x0057:
            java.util.List r1 = (java.util.List) r1
        L_0x0059:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.AwaitKt.awaitAll(kotlinx.coroutines.Deferred[], kotlin.coroutines.Continuation):java.lang.Object");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T> java.lang.Object awaitAll(@org.jetbrains.annotations.NotNull java.util.Collection<? extends kotlinx.coroutines.Deferred<? extends T>> r7, @org.jetbrains.annotations.NotNull kotlin.coroutines.Continuation<? super java.util.List<? extends T>> r8) {
        /*
            boolean r0 = r8 instanceof kotlinx.coroutines.AwaitKt$awaitAll$2
            if (r0 == 0) goto L_0x0014
            r0 = r8
            kotlinx.coroutines.AwaitKt$awaitAll$2 r0 = (kotlinx.coroutines.AwaitKt$awaitAll$2) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0014
            int r1 = r0.label
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0019
        L_0x0014:
            kotlinx.coroutines.AwaitKt$awaitAll$2 r0 = new kotlinx.coroutines.AwaitKt$awaitAll$2
            r0.<init>(r8)
        L_0x0019:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r3 = r0.label
            r4 = 1
            if (r3 == 0) goto L_0x0037
            if (r3 != r4) goto L_0x002f
            java.lang.Object r2 = r0.L$0
            r7 = r2
            java.util.Collection r7 = (java.util.Collection) r7
            kotlin.ResultKt.throwOnFailure(r1)
            goto L_0x0065
        L_0x002f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0037:
            kotlin.ResultKt.throwOnFailure(r1)
            boolean r1 = r7.isEmpty()
            if (r1 == 0) goto L_0x0045
            java.util.List r1 = kotlin.collections.CollectionsKt.emptyList()
            goto L_0x0067
        L_0x0045:
            r1 = r7
            r3 = 0
            if (r1 == 0) goto L_0x0070
            r5 = r1
            r6 = 0
            kotlinx.coroutines.Deferred[] r6 = new kotlinx.coroutines.Deferred[r6]
            java.lang.Object[] r6 = r5.toArray(r6)
            if (r6 == 0) goto L_0x0068
            kotlinx.coroutines.Deferred[] r6 = (kotlinx.coroutines.Deferred[]) r6
            kotlinx.coroutines.AwaitAll r1 = new kotlinx.coroutines.AwaitAll
            r1.<init>(r6)
            r0.L$0 = r7
            r0.label = r4
            java.lang.Object r1 = r1.await(r0)
            if (r1 != r2) goto L_0x0065
            return r2
        L_0x0065:
            java.util.List r1 = (java.util.List) r1
        L_0x0067:
            return r1
        L_0x0068:
            kotlin.TypeCastException r2 = new kotlin.TypeCastException
            java.lang.String r4 = "null cannot be cast to non-null type kotlin.Array<T>"
            r2.<init>(r4)
            throw r2
        L_0x0070:
            kotlin.TypeCastException r2 = new kotlin.TypeCastException
            java.lang.String r4 = "null cannot be cast to non-null type java.util.Collection<T>"
            r2.<init>(r4)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.AwaitKt.awaitAll(java.util.Collection, kotlin.coroutines.Continuation):java.lang.Object");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v0, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: kotlinx.coroutines.Job[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v3, resolved type: kotlinx.coroutines.Job[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Object joinAll(@org.jetbrains.annotations.NotNull kotlinx.coroutines.Job[] r13, @org.jetbrains.annotations.NotNull kotlin.coroutines.Continuation<? super kotlin.Unit> r14) {
        /*
            boolean r0 = r14 instanceof kotlinx.coroutines.AwaitKt$joinAll$1
            if (r0 == 0) goto L_0x0014
            r0 = r14
            kotlinx.coroutines.AwaitKt$joinAll$1 r0 = (kotlinx.coroutines.AwaitKt$joinAll$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0014
            int r1 = r0.label
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0019
        L_0x0014:
            kotlinx.coroutines.AwaitKt$joinAll$1 r0 = new kotlinx.coroutines.AwaitKt$joinAll$1
            r0.<init>(r14)
        L_0x0019:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r3 = r0.label
            r4 = 1
            r5 = 0
            if (r3 == 0) goto L_0x0053
            if (r3 != r4) goto L_0x004b
            r3 = 0
            r6 = r3
            r7 = r5
            r8 = r3
            java.lang.Object r9 = r0.L$4
            r6 = r9
            kotlinx.coroutines.Job r6 = (kotlinx.coroutines.Job) r6
            java.lang.Object r9 = r0.L$3
            r8 = r9
            kotlinx.coroutines.Job r8 = (kotlinx.coroutines.Job) r8
            int r9 = r0.I$1
            int r10 = r0.I$0
            java.lang.Object r11 = r0.L$2
            kotlinx.coroutines.Job[] r11 = (kotlinx.coroutines.Job[]) r11
            java.lang.Object r12 = r0.L$1
            r3 = r12
            kotlinx.coroutines.Job[] r3 = (kotlinx.coroutines.Job[]) r3
            java.lang.Object r12 = r0.L$0
            r13 = r12
            kotlinx.coroutines.Job[] r13 = (kotlinx.coroutines.Job[]) r13
            kotlin.ResultKt.throwOnFailure(r1)
            goto L_0x007b
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            kotlin.ResultKt.throwOnFailure(r1)
            r1 = r13
            r3 = 0
            int r6 = r1.length
            r11 = r1
            r5 = r3
            r10 = r6
            r9 = 0
            r3 = r11
        L_0x005e:
            if (r9 >= r10) goto L_0x007e
            r8 = r11[r9]
            r6 = r8
            r7 = 0
            r0.L$0 = r13
            r0.L$1 = r3
            r0.L$2 = r11
            r0.I$0 = r10
            r0.I$1 = r9
            r0.L$3 = r8
            r0.L$4 = r6
            r0.label = r4
            java.lang.Object r1 = r6.join(r0)
            if (r1 != r2) goto L_0x007b
            return r2
        L_0x007b:
            int r9 = r9 + r4
            goto L_0x005e
        L_0x007e:
            kotlin.Unit r1 = kotlin.Unit.INSTANCE
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.AwaitKt.joinAll(kotlinx.coroutines.Job[], kotlin.coroutines.Continuation):java.lang.Object");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v2, resolved type: java.util.Collection<? extends kotlinx.coroutines.Job>} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Object joinAll(@org.jetbrains.annotations.NotNull java.util.Collection<? extends kotlinx.coroutines.Job> r11, @org.jetbrains.annotations.NotNull kotlin.coroutines.Continuation<? super kotlin.Unit> r12) {
        /*
            boolean r0 = r12 instanceof kotlinx.coroutines.AwaitKt$joinAll$3
            if (r0 == 0) goto L_0x0014
            r0 = r12
            kotlinx.coroutines.AwaitKt$joinAll$3 r0 = (kotlinx.coroutines.AwaitKt$joinAll$3) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0014
            int r1 = r0.label
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0019
        L_0x0014:
            kotlinx.coroutines.AwaitKt$joinAll$3 r0 = new kotlinx.coroutines.AwaitKt$joinAll$3
            r0.<init>(r12)
        L_0x0019:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r3 = r0.label
            r4 = 1
            if (r3 == 0) goto L_0x004c
            if (r3 != r4) goto L_0x0044
            r3 = 0
            r5 = r3
            r6 = 0
            r7 = r6
            r8 = r3
            java.lang.Object r9 = r0.L$4
            r5 = r9
            kotlinx.coroutines.Job r5 = (kotlinx.coroutines.Job) r5
            java.lang.Object r8 = r0.L$3
            java.lang.Object r9 = r0.L$2
            java.util.Iterator r9 = (java.util.Iterator) r9
            java.lang.Object r10 = r0.L$1
            r3 = r10
            java.lang.Iterable r3 = (java.lang.Iterable) r3
            java.lang.Object r10 = r0.L$0
            r11 = r10
            java.util.Collection r11 = (java.util.Collection) r11
            kotlin.ResultKt.throwOnFailure(r1)
            goto L_0x007b
        L_0x0044:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004c:
            kotlin.ResultKt.throwOnFailure(r1)
            r1 = r11
            java.lang.Iterable r1 = (java.lang.Iterable) r1
            r3 = 0
            java.util.Iterator r5 = r1.iterator()
            r6 = r3
            r9 = r5
            r3 = r1
        L_0x005a:
            boolean r1 = r9.hasNext()
            if (r1 == 0) goto L_0x007c
            java.lang.Object r8 = r9.next()
            r5 = r8
            kotlinx.coroutines.Job r5 = (kotlinx.coroutines.Job) r5
            r7 = 0
            r0.L$0 = r11
            r0.L$1 = r3
            r0.L$2 = r9
            r0.L$3 = r8
            r0.L$4 = r5
            r0.label = r4
            java.lang.Object r1 = r5.join(r0)
            if (r1 != r2) goto L_0x007b
            return r2
        L_0x007b:
            goto L_0x005a
        L_0x007c:
            kotlin.Unit r1 = kotlin.Unit.INSTANCE
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.AwaitKt.joinAll(java.util.Collection, kotlin.coroutines.Continuation):java.lang.Object");
    }
}
