package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.LongRange;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00030\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "Lkotlinx/coroutines/flow/FlowCollector;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__BuildersKt$asFlow$10", f = "Builders.kt", i = {0, 0, 0}, l = {181}, m = "invokeSuspend", n = {"$this$forEach$iv", "element$iv", "value"}, s = {"L$1", "L$3", "J$0"})
/* compiled from: Builders.kt */
final class FlowKt__BuildersKt$asFlow$10 extends SuspendLambda implements Function2<FlowCollector<? super Long>, Continuation<? super Unit>, Object> {
    final /* synthetic */ LongRange $this_asFlow;
    long J$0;
    Object L$0;
    Object L$1;
    Object L$2;
    Object L$3;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__BuildersKt$asFlow$10(LongRange longRange, Continuation continuation) {
        super(2, continuation);
        this.$this_asFlow = longRange;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__BuildersKt$asFlow$10 flowKt__BuildersKt$asFlow$10 = new FlowKt__BuildersKt$asFlow$10(this.$this_asFlow, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__BuildersKt$asFlow$10.p$ = (FlowCollector) obj;
        return flowKt__BuildersKt$asFlow$10;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__BuildersKt$asFlow$10) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x004c  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r14) {
        /*
            r13 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r13.label
            r2 = 1
            if (r1 == 0) goto L_0x0031
            if (r1 != r2) goto L_0x0029
            r3 = 0
            r1 = 0
            r5 = r1
            r6 = 0
            r7 = r6
            long r3 = r13.J$0
            java.lang.Object r7 = r13.L$3
            java.lang.Object r8 = r13.L$2
            java.util.Iterator r8 = (java.util.Iterator) r8
            java.lang.Object r9 = r13.L$1
            r6 = r9
            java.lang.Iterable r6 = (java.lang.Iterable) r6
            java.lang.Object r9 = r13.L$0
            kotlinx.coroutines.flow.FlowCollector r9 = (kotlinx.coroutines.flow.FlowCollector) r9
            kotlin.ResultKt.throwOnFailure(r14)
            r10 = r0
            r0 = r14
            r14 = r13
            goto L_0x0073
        L_0x0029:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0031:
            kotlin.ResultKt.throwOnFailure(r14)
            kotlinx.coroutines.flow.FlowCollector r1 = r13.p$
            kotlin.ranges.LongRange r3 = r13.$this_asFlow
            java.lang.Iterable r3 = (java.lang.Iterable) r3
            r4 = 0
            java.util.Iterator r5 = r3.iterator()
            r9 = r1
            r6 = r3
            r1 = r4
            r8 = r5
            r3 = r0
            r0 = r14
            r14 = r13
        L_0x0046:
            boolean r4 = r8.hasNext()
            if (r4 == 0) goto L_0x0075
            java.lang.Object r7 = r8.next()
            r4 = r7
            java.lang.Number r4 = (java.lang.Number) r4
            long r4 = r4.longValue()
            r10 = 0
            java.lang.Long r11 = kotlin.coroutines.jvm.internal.Boxing.boxLong(r4)
            r14.L$0 = r9
            r14.L$1 = r6
            r14.L$2 = r8
            r14.L$3 = r7
            r14.J$0 = r4
            r14.label = r2
            java.lang.Object r11 = r9.emit(r11, r14)
            if (r11 != r3) goto L_0x006f
            return r3
        L_0x006f:
            r12 = r10
            r10 = r3
            r3 = r4
            r5 = r12
        L_0x0073:
            r3 = r10
            goto L_0x0046
        L_0x0075:
            kotlin.Unit r1 = kotlin.Unit.INSTANCE
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__BuildersKt$asFlow$10.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
