package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__BuildersKt$flowOf$1", f = "Builders.kt", i = {0}, l = {121}, m = "invokeSuspend", n = {"element"}, s = {"L$1"})
/* compiled from: Builders.kt */
final class FlowKt__BuildersKt$flowOf$1 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Object[] $elements;
    int I$0;
    int I$1;
    Object L$0;
    Object L$1;
    Object L$2;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__BuildersKt$flowOf$1(Object[] objArr, Continuation continuation) {
        super(2, continuation);
        this.$elements = objArr;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__BuildersKt$flowOf$1 flowKt__BuildersKt$flowOf$1 = new FlowKt__BuildersKt$flowOf$1(this.$elements, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__BuildersKt$flowOf$1.p$ = (FlowCollector) obj;
        return flowKt__BuildersKt$flowOf$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__BuildersKt$flowOf$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x003b  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r11) {
        /*
            r10 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r10.label
            r2 = 1
            if (r1 == 0) goto L_0x0029
            if (r1 != r2) goto L_0x0021
            r1 = 0
            int r3 = r10.I$1
            java.lang.Object r4 = r10.L$2
            java.lang.Object[] r4 = (java.lang.Object[]) r4
            int r5 = r10.I$0
            java.lang.Object r1 = r10.L$1
            java.lang.Object r6 = r10.L$0
            kotlinx.coroutines.flow.FlowCollector r6 = (kotlinx.coroutines.flow.FlowCollector) r6
            kotlin.ResultKt.throwOnFailure(r11)
            r7 = r0
            r0 = r11
            r11 = r10
            goto L_0x0053
        L_0x0021:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0029:
            kotlin.ResultKt.throwOnFailure(r11)
            kotlinx.coroutines.flow.FlowCollector r1 = r10.p$
            java.lang.Object[] r3 = r10.$elements
            int r4 = r3.length
            r5 = 0
            r6 = r1
            r1 = r0
            r0 = r11
            r11 = r10
            r9 = r4
            r4 = r3
            r3 = r9
        L_0x0039:
            if (r5 >= r3) goto L_0x0057
            r7 = r4[r5]
            r11.L$0 = r6
            r11.L$1 = r7
            r11.I$0 = r5
            r11.L$2 = r4
            r11.I$1 = r3
            r11.label = r2
            java.lang.Object r8 = r6.emit(r7, r11)
            if (r8 != r1) goto L_0x0050
            return r1
        L_0x0050:
            r9 = r7
            r7 = r1
            r1 = r9
        L_0x0053:
            int r5 = r5 + r2
            r1 = r7
            goto L_0x0039
        L_0x0057:
            kotlin.Unit r1 = kotlin.Unit.INSTANCE
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__BuildersKt$flowOf$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
