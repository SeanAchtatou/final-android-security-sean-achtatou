package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00030\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "Lkotlinx/coroutines/flow/FlowCollector;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__BuildersKt$asFlow$8", f = "Builders.kt", i = {0, 0, 0}, l = {161}, m = "invokeSuspend", n = {"$this$forEach$iv", "element$iv", "value"}, s = {"L$1", "J$0", "J$1"})
/* compiled from: Builders.kt */
final class FlowKt__BuildersKt$asFlow$8 extends SuspendLambda implements Function2<FlowCollector<? super Long>, Continuation<? super Unit>, Object> {
    final /* synthetic */ long[] $this_asFlow;
    int I$0;
    int I$1;
    long J$0;
    long J$1;
    Object L$0;
    Object L$1;
    Object L$2;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__BuildersKt$asFlow$8(long[] jArr, Continuation continuation) {
        super(2, continuation);
        this.$this_asFlow = jArr;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__BuildersKt$asFlow$8 flowKt__BuildersKt$asFlow$8 = new FlowKt__BuildersKt$asFlow$8(this.$this_asFlow, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__BuildersKt$asFlow$8.p$ = (FlowCollector) obj;
        return flowKt__BuildersKt$asFlow$8;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__BuildersKt$asFlow$8) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* Debug info: failed to restart local var, previous not found, register: 18 */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x004f  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r19) {
        /*
            r18 = this;
            r0 = r18
            java.lang.Object r1 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003a
            if (r2 != r3) goto L_0x0032
            r5 = 0
            r7 = r5
            r2 = r4
            r9 = 0
            long r7 = r0.J$1
            long r5 = r0.J$0
            int r10 = r0.I$1
            int r11 = r0.I$0
            java.lang.Object r12 = r0.L$2
            long[] r12 = (long[]) r12
            java.lang.Object r13 = r0.L$1
            r9 = r13
            long[] r9 = (long[]) r9
            java.lang.Object r13 = r0.L$0
            kotlinx.coroutines.flow.FlowCollector r13 = (kotlinx.coroutines.flow.FlowCollector) r13
            kotlin.ResultKt.throwOnFailure(r19)
            r3 = r19
            r14 = r7
            r7 = r1
            r1 = r0
            r0 = 1
            goto L_0x007f
        L_0x0032:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x003a:
            kotlin.ResultKt.throwOnFailure(r19)
            kotlinx.coroutines.flow.FlowCollector r2 = r0.p$
            long[] r5 = r0.$this_asFlow
            r6 = 0
            int r7 = r5.length
            r13 = r2
            r9 = r5
            r12 = r9
            r4 = r6
            r11 = r7
            r10 = 0
            r2 = r19
            r5 = r1
            r1 = r0
        L_0x004d:
            if (r10 >= r11) goto L_0x0087
            r6 = r12[r10]
            java.lang.Long r8 = kotlin.coroutines.jvm.internal.Boxing.boxLong(r6)
            java.lang.Number r8 = (java.lang.Number) r8
            long r14 = r8.longValue()
            r8 = 0
            java.lang.Long r3 = kotlin.coroutines.jvm.internal.Boxing.boxLong(r14)
            r1.L$0 = r13
            r1.L$1 = r9
            r1.L$2 = r12
            r1.I$0 = r11
            r1.I$1 = r10
            r1.J$0 = r6
            r1.J$1 = r14
            r0 = 1
            r1.label = r0
            java.lang.Object r3 = r13.emit(r3, r1)
            if (r3 != r5) goto L_0x0078
            return r5
        L_0x0078:
            r3 = r2
            r2 = r8
            r16 = r6
            r7 = r5
            r5 = r16
        L_0x007f:
            int r10 = r10 + r0
            r0 = r18
            r2 = r3
            r5 = r7
            r3 = 1
            goto L_0x004d
        L_0x0087:
            kotlin.Unit r0 = kotlin.Unit.INSTANCE
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__BuildersKt$asFlow$8.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
