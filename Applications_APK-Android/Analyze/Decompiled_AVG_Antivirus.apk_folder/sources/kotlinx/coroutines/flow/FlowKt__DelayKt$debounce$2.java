package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2", f = "Delay.kt", i = {}, l = {64}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Delay.kt */
final class FlowKt__DelayKt$debounce$2 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Flow $this_debounce;
    final /* synthetic */ long $timeoutMillis;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__DelayKt$debounce$2(Flow flow, long j, Continuation continuation) {
        super(2, continuation);
        this.$this_debounce = flow;
        this.$timeoutMillis = j;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__DelayKt$debounce$2 flowKt__DelayKt$debounce$2 = new FlowKt__DelayKt$debounce$2(this.$this_debounce, this.$timeoutMillis, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__DelayKt$debounce$2.p$ = (FlowCollector) obj;
        return flowKt__DelayKt$debounce$2;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__DelayKt$debounce$2) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
    @DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1", f = "Delay.kt", i = {0, 0, 0, 0}, l = {167}, m = "invokeSuspend", n = {"values", "collector", "isDone", "lastValue"}, s = {"L$0", "L$1", "L$2", "L$3"})
    /* renamed from: kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1  reason: invalid class name */
    /* compiled from: Delay.kt */
    static final class AnonymousClass1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        Object L$0;
        Object L$1;
        Object L$2;
        Object L$3;
        Object L$4;
        int label;
        private CoroutineScope p$;
        final /* synthetic */ FlowKt__DelayKt$debounce$2 this$0;

        {
            this.this$0 = r1;
        }

        @NotNull
        public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            AnonymousClass1 r0 = new AnonymousClass1(this.this$0, flowCollector, continuation);
            CoroutineScope coroutineScope = (CoroutineScope) obj;
            r0.p$ = (CoroutineScope) obj;
            return r0;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((AnonymousClass1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: kotlin.jvm.internal.Ref$ObjectRef} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v7, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: kotlin.jvm.internal.Ref$BooleanRef} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v8, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v12, resolved type: kotlinx.coroutines.Job} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v9, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: kotlinx.coroutines.channels.Channel} */
        /* JADX WARNING: Can't wrap try/catch for region: R(13:9|(2:10|11)|12|13|14|(4:16|17|18|19)(1:20)|21|22|29|(1:31)|32|(1:34)(4:35|36|7|(1:37)(0))|34) */
        /* JADX WARNING: Can't wrap try/catch for region: R(4:16|17|18|19) */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00f2, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00f4, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00f5, code lost:
            r25 = r14;
            r26 = r15;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0103, code lost:
            r3 = r21;
            r3.handleBuilderException(r0);
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0113  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x011f  */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0071  */
        @org.jetbrains.annotations.Nullable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r28) {
            /*
                r27 = this;
                r1 = r27
                java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
                int r2 = r1.label
                r3 = 1
                r4 = 0
                r5 = 0
                if (r2 == 0) goto L_0x0040
                if (r2 != r3) goto L_0x0038
                r2 = r4
                r4 = r5
                r6 = r5
                r7 = r5
                java.lang.Object r8 = r1.L$4
                kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1 r8 = (kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2.AnonymousClass1) r8
                java.lang.Object r8 = r1.L$3
                r4 = r8
                kotlin.jvm.internal.Ref$ObjectRef r4 = (kotlin.jvm.internal.Ref.ObjectRef) r4
                java.lang.Object r8 = r1.L$2
                r6 = r8
                kotlin.jvm.internal.Ref$BooleanRef r6 = (kotlin.jvm.internal.Ref.BooleanRef) r6
                java.lang.Object r8 = r1.L$1
                r7 = r8
                kotlinx.coroutines.Job r7 = (kotlinx.coroutines.Job) r7
                java.lang.Object r8 = r1.L$0
                r5 = r8
                kotlinx.coroutines.channels.Channel r5 = (kotlinx.coroutines.channels.Channel) r5
                kotlin.ResultKt.throwOnFailure(r28)
                r16 = r28
                r14 = r0
                r15 = r1
                r2 = r5
                r5 = r4
                r4 = r6
                r6 = r7
                goto L_0x0122
            L_0x0038:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r2)
                throw r0
            L_0x0040:
                kotlin.ResultKt.throwOnFailure(r28)
                kotlinx.coroutines.CoroutineScope r6 = r1.p$
                r2 = -1
                kotlinx.coroutines.channels.Channel r2 = kotlinx.coroutines.channels.ChannelKt.Channel(r2)
                r7 = 0
                r8 = 0
                kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1$collector$1 r9 = new kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1$collector$1
                r9.<init>(r1, r2, r5)
                kotlin.jvm.functions.Function2 r9 = (kotlin.jvm.functions.Function2) r9
                r10 = 3
                r11 = 0
                kotlinx.coroutines.Job r6 = kotlinx.coroutines.BuildersKt__Builders_commonKt.launch$default(r6, r7, r8, r9, r10, r11)
                kotlin.jvm.internal.Ref$BooleanRef r7 = new kotlin.jvm.internal.Ref$BooleanRef
                r7.<init>()
                r7.element = r4
                r4 = r7
                kotlin.jvm.internal.Ref$ObjectRef r7 = new kotlin.jvm.internal.Ref$ObjectRef
                r7.<init>()
                r7.element = r5
                r5 = r7
                r16 = r28
                r14 = r0
                r15 = r1
            L_0x006d:
                boolean r0 = r4.element
                if (r0 != 0) goto L_0x0126
                r17 = 0
                r15.L$0 = r2
                r15.L$1 = r6
                r15.L$2 = r4
                r15.L$3 = r5
                r15.L$4 = r15
                r15.label = r3
                r13 = r15
                kotlin.coroutines.Continuation r13 = (kotlin.coroutines.Continuation) r13
                r18 = 0
                kotlinx.coroutines.selects.SelectBuilderImpl r0 = new kotlinx.coroutines.selects.SelectBuilderImpl
                r0.<init>(r13)
                r12 = r0
                r0 = r12
                kotlinx.coroutines.selects.SelectBuilder r0 = (kotlinx.coroutines.selects.SelectBuilder) r0     // Catch:{ Throwable -> 0x00fa }
                r19 = 0
                kotlinx.coroutines.selects.SelectClause1 r11 = r2.getOnReceive()     // Catch:{ Throwable -> 0x00fa }
                kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1$invokeSuspend$$inlined$select$lambda$1 r20 = new kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1$invokeSuspend$$inlined$select$lambda$1     // Catch:{ Throwable -> 0x00fa }
                r8 = 0
                r7 = r20
                r9 = r15
                r10 = r2
                r3 = r11
                r11 = r5
                r21 = r12
                r12 = r6
                r22 = r13
                r13 = r4
                r7.<init>(r8, r9, r10, r11, r12, r13)     // Catch:{ Throwable -> 0x00f4 }
                r7 = r20
                kotlin.jvm.functions.Function2 r7 = (kotlin.jvm.functions.Function2) r7     // Catch:{ Throwable -> 0x00f4 }
                r0.invoke(r3, r7)     // Catch:{ Throwable -> 0x00f4 }
                T r8 = r5.element     // Catch:{ Throwable -> 0x00f4 }
                if (r8 == 0) goto L_0x00d4
                r3 = 0
                kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2 r7 = r15.this$0     // Catch:{ Throwable -> 0x00f4 }
                long r12 = r7.$timeoutMillis     // Catch:{ Throwable -> 0x00f4 }
                kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1$invokeSuspend$$inlined$select$lambda$2 r20 = new kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1$invokeSuspend$$inlined$select$lambda$2     // Catch:{ Throwable -> 0x00f4 }
                r9 = 0
                r7 = r20
                r10 = r0
                r11 = r15
                r23 = r12
                r12 = r2
                r13 = r5
                r25 = r14
                r14 = r6
                r26 = r15
                r15 = r4
                r7.<init>(r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ Throwable -> 0x00f2 }
                r7 = r20
                kotlin.jvm.functions.Function1 r7 = (kotlin.jvm.functions.Function1) r7     // Catch:{ Throwable -> 0x00f2 }
                r9 = r23
                r0.onTimeout(r9, r7)     // Catch:{ Throwable -> 0x00f2 }
                goto L_0x00d8
            L_0x00d4:
                r25 = r14
                r26 = r15
            L_0x00d8:
                kotlinx.coroutines.selects.SelectClause0 r3 = r6.getOnJoin()     // Catch:{ Throwable -> 0x00f2 }
                kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1$invokeSuspend$$inlined$select$lambda$3 r14 = new kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1$invokeSuspend$$inlined$select$lambda$3     // Catch:{ Throwable -> 0x00f2 }
                r8 = 0
                r7 = r14
                r9 = r26
                r10 = r2
                r11 = r5
                r12 = r6
                r13 = r4
                r7.<init>(r8, r9, r10, r11, r12, r13)     // Catch:{ Throwable -> 0x00f2 }
                kotlin.jvm.functions.Function1 r14 = (kotlin.jvm.functions.Function1) r14     // Catch:{ Throwable -> 0x00f2 }
                r0.invoke(r3, r14)     // Catch:{ Throwable -> 0x00f2 }
                r3 = r21
                goto L_0x0108
            L_0x00f2:
                r0 = move-exception
                goto L_0x0103
            L_0x00f4:
                r0 = move-exception
                r25 = r14
                r26 = r15
                goto L_0x0103
            L_0x00fa:
                r0 = move-exception
                r21 = r12
                r22 = r13
                r25 = r14
                r26 = r15
            L_0x0103:
                r3 = r21
                r3.handleBuilderException(r0)
            L_0x0108:
                java.lang.Object r0 = r3.getResult()
                java.lang.Object r3 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
                if (r0 != r3) goto L_0x011a
                r3 = r26
                kotlin.coroutines.Continuation r3 = (kotlin.coroutines.Continuation) r3
                kotlin.coroutines.jvm.internal.DebugProbesKt.probeCoroutineSuspended(r3)
            L_0x011a:
                r3 = r25
                if (r0 != r3) goto L_0x011f
                return r3
            L_0x011f:
                r14 = r3
                r15 = r26
            L_0x0122:
                r3 = 1
                goto L_0x006d
            L_0x0126:
                r26 = r15
                kotlin.Unit r0 = kotlin.Unit.INSTANCE
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            final FlowCollector flowCollector = this.p$;
            this.label = 1;
            if (CoroutineScopeKt.coroutineScope(new AnonymousClass1(this, null), this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            ResultKt.throwOnFailure(result);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }
}
