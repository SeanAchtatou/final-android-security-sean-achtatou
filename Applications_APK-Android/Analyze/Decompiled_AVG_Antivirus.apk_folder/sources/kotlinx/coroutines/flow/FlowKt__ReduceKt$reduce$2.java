package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;
import kotlinx.coroutines.flow.internal.NullSurrogate;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\b\b\u0001\u0010\u0003*\u0002H\u00022\u0006\u0010\u0004\u001a\u0002H\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "S", "T", "value", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ReduceKt$reduce$2", f = "Reduce.kt", i = {}, l = {26}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Reduce.kt */
final class FlowKt__ReduceKt$reduce$2 extends SuspendLambda implements Function2<T, Continuation<? super Unit>, Object> {
    final /* synthetic */ Ref.ObjectRef $accumulator;
    final /* synthetic */ Function3 $operation;
    Object L$0;
    int label;
    private Object p$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__ReduceKt$reduce$2(Ref.ObjectRef objectRef, Function3 function3, Continuation continuation) {
        super(2, continuation);
        this.$accumulator = objectRef;
        this.$operation = function3;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__ReduceKt$reduce$2 flowKt__ReduceKt$reduce$2 = new FlowKt__ReduceKt$reduce$2(this.$accumulator, this.$operation, continuation);
        flowKt__ReduceKt$reduce$2.p$0 = obj;
        return flowKt__ReduceKt$reduce$2;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__ReduceKt$reduce$2) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Ref.ObjectRef objectRef;
        Object obj;
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            obj = this.p$0;
            objectRef = this.$accumulator;
            if (objectRef.element != NullSurrogate.INSTANCE) {
                Function3 function3 = this.$operation;
                T t = this.$accumulator.element;
                this.L$0 = objectRef;
                this.label = 1;
                obj = function3.invoke(t, obj, this);
                if (obj == coroutine_suspended) {
                    return coroutine_suspended;
                }
            }
        } else if (i == 1) {
            ResultKt.throwOnFailure(result);
            obj = result;
            objectRef = (Ref.ObjectRef) this.L$0;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        objectRef.element = obj;
        return Unit.INSTANCE;
    }
}
