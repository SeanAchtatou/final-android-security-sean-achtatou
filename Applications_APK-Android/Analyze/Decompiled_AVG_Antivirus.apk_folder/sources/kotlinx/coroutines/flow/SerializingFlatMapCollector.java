package kotlinx.coroutines.flow;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.channels.Channel;
import kotlinx.coroutines.channels.ChannelKt;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0019\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u0000H@ø\u0001\u0000¢\u0006\u0002\u0010\u000fJ\u0011\u0010\u0010\u001a\u00020\rH@ø\u0001\u0000¢\u0006\u0002\u0010\u0011R\u0016\u0010\b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\u0012"}, d2 = {"Lkotlinx/coroutines/flow/SerializingFlatMapCollector;", "T", "", "downstream", "Lkotlinx/coroutines/flow/FlowCollector;", "bufferSize", "", "(Lkotlinx/coroutines/flow/FlowCollector;I)V", "channel", "Lkotlinx/coroutines/channels/Channel;", "inProgressLock", "Lkotlinx/atomicfu/AtomicBoolean;", "emit", "", "value", "(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "helpEmit", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
/* compiled from: Merge.kt */
final class SerializingFlatMapCollector<T> {
    private static final AtomicIntegerFieldUpdater inProgressLock$FU = AtomicIntegerFieldUpdater.newUpdater(SerializingFlatMapCollector.class, "inProgressLock");
    private final Channel<Object> channel;
    private final FlowCollector<T> downstream;
    private volatile int inProgressLock = 0;

    public SerializingFlatMapCollector(@NotNull FlowCollector<? super T> downstream2, int bufferSize) {
        Intrinsics.checkParameterIsNotNull(downstream2, "downstream");
        this.downstream = downstream2;
        this.channel = ChannelKt.Channel(bufferSize);
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00bc A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0028  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object emit(T r11, @org.jetbrains.annotations.NotNull kotlin.coroutines.Continuation<? super kotlin.Unit> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof kotlinx.coroutines.flow.SerializingFlatMapCollector$emit$1
            if (r0 == 0) goto L_0x0014
            r0 = r12
            kotlinx.coroutines.flow.SerializingFlatMapCollector$emit$1 r0 = (kotlinx.coroutines.flow.SerializingFlatMapCollector$emit$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0014
            int r1 = r0.label
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0019
        L_0x0014:
            kotlinx.coroutines.flow.SerializingFlatMapCollector$emit$1 r0 = new kotlinx.coroutines.flow.SerializingFlatMapCollector$emit$1
            r0.<init>(r10, r12)
        L_0x0019:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r3 = r0.label
            r4 = 0
            r5 = 4
            r6 = 3
            r7 = 2
            r8 = 1
            if (r3 == 0) goto L_0x0061
            if (r3 == r8) goto L_0x0057
            if (r3 == r7) goto L_0x004d
            if (r3 == r6) goto L_0x0043
            if (r3 != r5) goto L_0x003b
            java.lang.Object r11 = r0.L$1
            java.lang.Object r2 = r0.L$0
            kotlinx.coroutines.flow.SerializingFlatMapCollector r2 = (kotlinx.coroutines.flow.SerializingFlatMapCollector) r2
            kotlin.ResultKt.throwOnFailure(r1)
            goto L_0x00be
        L_0x003b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0043:
            java.lang.Object r11 = r0.L$1
            java.lang.Object r3 = r0.L$0
            kotlinx.coroutines.flow.SerializingFlatMapCollector r3 = (kotlinx.coroutines.flow.SerializingFlatMapCollector) r3
            kotlin.ResultKt.throwOnFailure(r1)
            goto L_0x00b0
        L_0x004d:
            java.lang.Object r11 = r0.L$1
            java.lang.Object r2 = r0.L$0
            kotlinx.coroutines.flow.SerializingFlatMapCollector r2 = (kotlinx.coroutines.flow.SerializingFlatMapCollector) r2
            kotlin.ResultKt.throwOnFailure(r1)
            goto L_0x009c
        L_0x0057:
            java.lang.Object r11 = r0.L$1
            java.lang.Object r3 = r0.L$0
            kotlinx.coroutines.flow.SerializingFlatMapCollector r3 = (kotlinx.coroutines.flow.SerializingFlatMapCollector) r3
            kotlin.ResultKt.throwOnFailure(r1)
            goto L_0x0084
        L_0x0061:
            kotlin.ResultKt.throwOnFailure(r1)
            r1 = r10
            r3 = 0
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r9 = kotlinx.coroutines.flow.SerializingFlatMapCollector.inProgressLock$FU
            boolean r1 = r9.compareAndSet(r1, r4, r8)
            if (r1 != 0) goto L_0x00a0
            kotlinx.coroutines.channels.Channel<java.lang.Object> r1 = r10.channel
            if (r11 == 0) goto L_0x0074
            r3 = r11
            goto L_0x0076
        L_0x0074:
            kotlinx.coroutines.flow.internal.NullSurrogate r3 = kotlinx.coroutines.flow.internal.NullSurrogate.INSTANCE
        L_0x0076:
            r0.L$0 = r10
            r0.L$1 = r11
            r0.label = r8
            java.lang.Object r1 = r1.send(r3, r0)
            if (r1 != r2) goto L_0x0083
            return r2
        L_0x0083:
            r3 = r10
        L_0x0084:
            r1 = r3
            r5 = 0
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r6 = kotlinx.coroutines.flow.SerializingFlatMapCollector.inProgressLock$FU
            boolean r1 = r6.compareAndSet(r1, r4, r8)
            if (r1 == 0) goto L_0x009d
            r0.L$0 = r3
            r0.L$1 = r11
            r0.label = r7
            java.lang.Object r1 = r3.helpEmit(r0)
            if (r1 != r2) goto L_0x009b
            return r2
        L_0x009b:
            r2 = r3
        L_0x009c:
            return r1
        L_0x009d:
            kotlin.Unit r1 = kotlin.Unit.INSTANCE
            return r1
        L_0x00a0:
            kotlinx.coroutines.flow.FlowCollector<T> r1 = r10.downstream
            r0.L$0 = r10
            r0.L$1 = r11
            r0.label = r6
            java.lang.Object r1 = r1.emit(r11, r0)
            if (r1 != r2) goto L_0x00af
            return r2
        L_0x00af:
            r3 = r10
        L_0x00b0:
            r0.L$0 = r3
            r0.L$1 = r11
            r0.label = r5
            java.lang.Object r1 = r3.helpEmit(r0)
            if (r1 != r2) goto L_0x00bd
            return r2
        L_0x00bd:
            r2 = r3
        L_0x00be:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.SerializingFlatMapCollector.emit(java.lang.Object, kotlin.coroutines.Continuation):java.lang.Object");
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0079, code lost:
        if (kotlinx.coroutines.flow.SerializingFlatMapCollector.inProgressLock$FU.compareAndSet(r10, 0, 1) == false) goto L_0x007d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object helpEmit(@org.jetbrains.annotations.NotNull kotlin.coroutines.Continuation<? super kotlin.Unit> r10) {
        /*
            r9 = this;
            boolean r0 = r10 instanceof kotlinx.coroutines.flow.SerializingFlatMapCollector$helpEmit$1
            if (r0 == 0) goto L_0x0014
            r0 = r10
            kotlinx.coroutines.flow.SerializingFlatMapCollector$helpEmit$1 r0 = (kotlinx.coroutines.flow.SerializingFlatMapCollector$helpEmit$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0014
            int r1 = r0.label
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0019
        L_0x0014:
            kotlinx.coroutines.flow.SerializingFlatMapCollector$helpEmit$1 r0 = new kotlinx.coroutines.flow.SerializingFlatMapCollector$helpEmit$1
            r0.<init>(r9, r10)
        L_0x0019:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r3 = r0.label
            r4 = 1
            if (r3 == 0) goto L_0x003b
            if (r3 != r4) goto L_0x0033
            r3 = 0
            java.lang.Object r3 = r0.L$1
            java.lang.Object r5 = r0.L$0
            kotlinx.coroutines.flow.SerializingFlatMapCollector r5 = (kotlinx.coroutines.flow.SerializingFlatMapCollector) r5
            kotlin.ResultKt.throwOnFailure(r1)
            r1 = r10
            r10 = r5
            goto L_0x005c
        L_0x0033:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003b:
            kotlin.ResultKt.throwOnFailure(r1)
            r1 = r10
            r10 = r9
        L_0x0040:
            kotlinx.coroutines.channels.Channel<java.lang.Object> r3 = r10.channel
            java.lang.Object r3 = r3.poll()
        L_0x0047:
            if (r3 == 0) goto L_0x0063
            kotlinx.coroutines.flow.FlowCollector<T> r5 = r10.downstream
            java.lang.Object r6 = kotlinx.coroutines.flow.internal.NullSurrogate.unbox$kotlinx_coroutines_core(r3)
            r0.L$0 = r10
            r0.L$1 = r3
            r0.label = r4
            java.lang.Object r5 = r5.emit(r6, r0)
            if (r5 != r2) goto L_0x005c
            return r2
        L_0x005c:
            kotlinx.coroutines.channels.Channel<java.lang.Object> r5 = r10.channel
            java.lang.Object r3 = r5.poll()
            goto L_0x0047
        L_0x0063:
            r5 = r10
            r6 = 0
            r7 = 0
            r5.inProgressLock = r7
            kotlinx.coroutines.channels.Channel<java.lang.Object> r5 = r10.channel
            boolean r5 = r5.isEmpty()
            if (r5 != 0) goto L_0x007d
            r5 = r10
            r6 = 0
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r8 = kotlinx.coroutines.flow.SerializingFlatMapCollector.inProgressLock$FU
            boolean r5 = r8.compareAndSet(r5, r7, r4)
            if (r5 != 0) goto L_0x007c
            goto L_0x007d
        L_0x007c:
            goto L_0x0040
        L_0x007d:
            kotlin.Unit r2 = kotlin.Unit.INSTANCE
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.SerializingFlatMapCollector.helpEmit(kotlin.coroutines.Continuation):java.lang.Object");
    }
}
