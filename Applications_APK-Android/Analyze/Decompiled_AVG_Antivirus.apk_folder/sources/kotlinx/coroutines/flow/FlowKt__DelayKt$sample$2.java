package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2", f = "Delay.kt", i = {}, l = {121}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Delay.kt */
final class FlowKt__DelayKt$sample$2 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ long $periodMillis;
    final /* synthetic */ Flow $this_sample;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__DelayKt$sample$2(Flow flow, long j, Continuation continuation) {
        super(2, continuation);
        this.$this_sample = flow;
        this.$periodMillis = j;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__DelayKt$sample$2 flowKt__DelayKt$sample$2 = new FlowKt__DelayKt$sample$2(this.$this_sample, this.$periodMillis, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__DelayKt$sample$2.p$ = (FlowCollector) obj;
        return flowKt__DelayKt$sample$2;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__DelayKt$sample$2) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
    @DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1", f = "Delay.kt", i = {0, 0, 0, 0}, l = {167}, m = "invokeSuspend", n = {"values", "isDone", "lastValue", "ticker"}, s = {"L$0", "L$1", "L$2", "L$3"})
    /* renamed from: kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1  reason: invalid class name */
    /* compiled from: Delay.kt */
    static final class AnonymousClass1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        Object L$0;
        Object L$1;
        Object L$2;
        Object L$3;
        Object L$4;
        int label;
        private CoroutineScope p$;
        final /* synthetic */ FlowKt__DelayKt$sample$2 this$0;

        {
            this.this$0 = r1;
        }

        @NotNull
        public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            AnonymousClass1 r0 = new AnonymousClass1(this.this$0, flowCollector, continuation);
            CoroutineScope coroutineScope = (CoroutineScope) obj;
            r0.p$ = (CoroutineScope) obj;
            return r0;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((AnonymousClass1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        /* Debug info: failed to restart local var, previous not found, register: 22 */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v11, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v8, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v6, resolved type: kotlin.jvm.internal.Ref$BooleanRef} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v9, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX WARNING: Can't wrap try/catch for region: R(11:9|10|11|12|13|14|15|20|(1:22)|(1:24)(4:25|26|7|(1:27)(0))|24) */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x00d3, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x00db, code lost:
            r3 = r20;
            r3.handleBuilderException(r0);
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00eb  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00f4  */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x007b  */
        @org.jetbrains.annotations.Nullable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r23) {
            /*
                r22 = this;
                r1 = r22
                java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
                int r2 = r1.label
                r3 = 1
                r4 = 0
                r5 = 0
                if (r2 == 0) goto L_0x0040
                if (r2 != r3) goto L_0x0038
                r2 = r4
                r4 = r5
                r6 = r5
                r7 = r5
                java.lang.Object r8 = r1.L$4
                kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1 r8 = (kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2.AnonymousClass1) r8
                java.lang.Object r8 = r1.L$3
                r4 = r8
                kotlinx.coroutines.channels.ReceiveChannel r4 = (kotlinx.coroutines.channels.ReceiveChannel) r4
                java.lang.Object r8 = r1.L$2
                r6 = r8
                kotlin.jvm.internal.Ref$ObjectRef r6 = (kotlin.jvm.internal.Ref.ObjectRef) r6
                java.lang.Object r8 = r1.L$1
                r7 = r8
                kotlin.jvm.internal.Ref$BooleanRef r7 = (kotlin.jvm.internal.Ref.BooleanRef) r7
                java.lang.Object r8 = r1.L$0
                r5 = r8
                kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
                kotlin.ResultKt.throwOnFailure(r23)
                r15 = r23
                r10 = r0
                r14 = r1
                r2 = r4
                r11 = r5
                r13 = r6
                r12 = r7
                goto L_0x00f5
            L_0x0038:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r2)
                throw r0
            L_0x0040:
                kotlin.ResultKt.throwOnFailure(r23)
                kotlinx.coroutines.CoroutineScope r2 = r1.p$
                r7 = 0
                r8 = -1
                kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1$values$1 r6 = new kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1$values$1
                r6.<init>(r1, r5)
                r9 = r6
                kotlin.jvm.functions.Function2 r9 = (kotlin.jvm.functions.Function2) r9
                r10 = 1
                r11 = 0
                r6 = r2
                kotlinx.coroutines.channels.ReceiveChannel r11 = kotlinx.coroutines.channels.ProduceKt.produce$default(r6, r7, r8, r9, r10, r11)
                kotlin.jvm.internal.Ref$BooleanRef r6 = new kotlin.jvm.internal.Ref$BooleanRef
                r6.<init>()
                r6.element = r4
                r12 = r6
                kotlin.jvm.internal.Ref$ObjectRef r4 = new kotlin.jvm.internal.Ref$ObjectRef
                r4.<init>()
                r4.element = r5
                r13 = r4
                kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2 r4 = r1.this$0
                long r5 = r4.$periodMillis
                r7 = 0
                r9 = 2
                r10 = 0
                r4 = r2
                kotlinx.coroutines.channels.ReceiveChannel r2 = kotlinx.coroutines.flow.FlowKt__DelayKt.fixedPeriodTicker$default(r4, r5, r7, r9, r10)
                r15 = r23
                r10 = r0
                r14 = r1
            L_0x0077:
                boolean r0 = r12.element
                if (r0 != 0) goto L_0x00fb
                r16 = 0
                r14.L$0 = r11
                r14.L$1 = r12
                r14.L$2 = r13
                r14.L$3 = r2
                r14.L$4 = r14
                r14.label = r3
                r9 = r14
                kotlin.coroutines.Continuation r9 = (kotlin.coroutines.Continuation) r9
                r17 = 0
                kotlinx.coroutines.selects.SelectBuilderImpl r0 = new kotlinx.coroutines.selects.SelectBuilderImpl
                r0.<init>(r9)
                r8 = r0
                r0 = r8
                kotlinx.coroutines.selects.SelectBuilder r0 = (kotlinx.coroutines.selects.SelectBuilder) r0     // Catch:{ Throwable -> 0x00d5 }
                r18 = 0
                kotlinx.coroutines.selects.SelectClause1 r7 = r11.getOnReceiveOrNull()     // Catch:{ Throwable -> 0x00d5 }
                kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$1 r19 = new kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$1     // Catch:{ Throwable -> 0x00d5 }
                r5 = 0
                r4 = r19
                r6 = r14
                r3 = r7
                r7 = r11
                r20 = r8
                r8 = r2
                r21 = r9
                r9 = r12
                r1 = r10
                r10 = r13
                r4.<init>(r5, r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x00d3 }
                r4 = r19
                kotlin.jvm.functions.Function2 r4 = (kotlin.jvm.functions.Function2) r4     // Catch:{ Throwable -> 0x00d3 }
                r0.invoke(r3, r4)     // Catch:{ Throwable -> 0x00d3 }
                kotlinx.coroutines.selects.SelectClause1 r3 = r2.getOnReceive()     // Catch:{ Throwable -> 0x00d3 }
                kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$2 r19 = new kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$2     // Catch:{ Throwable -> 0x00d3 }
                r5 = 0
                r4 = r19
                r6 = r14
                r7 = r11
                r8 = r2
                r9 = r12
                r10 = r13
                r4.<init>(r5, r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x00d3 }
                r4 = r19
                kotlin.jvm.functions.Function2 r4 = (kotlin.jvm.functions.Function2) r4     // Catch:{ Throwable -> 0x00d3 }
                r0.invoke(r3, r4)     // Catch:{ Throwable -> 0x00d3 }
                r3 = r20
                goto L_0x00e0
            L_0x00d3:
                r0 = move-exception
                goto L_0x00db
            L_0x00d5:
                r0 = move-exception
                r20 = r8
                r21 = r9
                r1 = r10
            L_0x00db:
                r3 = r20
                r3.handleBuilderException(r0)
            L_0x00e0:
                java.lang.Object r0 = r3.getResult()
                java.lang.Object r3 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
                if (r0 != r3) goto L_0x00f1
                r3 = r14
                kotlin.coroutines.Continuation r3 = (kotlin.coroutines.Continuation) r3
                kotlin.coroutines.jvm.internal.DebugProbesKt.probeCoroutineSuspended(r3)
            L_0x00f1:
                if (r0 != r1) goto L_0x00f4
                return r1
            L_0x00f4:
                r10 = r1
            L_0x00f5:
                r3 = 1
                r1 = r22
                goto L_0x0077
            L_0x00fb:
                kotlin.Unit r0 = kotlin.Unit.INSTANCE
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            final FlowCollector flowCollector = this.p$;
            this.label = 1;
            if (CoroutineScopeKt.coroutineScope(new AnonymousClass1(this, null), this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            ResultKt.throwOnFailure(result);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }
}
